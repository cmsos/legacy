package xdaq.tools.win;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;
import org.w3c.dom.*;
import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.UserCommandWindow;

public class QueryInterfaceAction
{
	xdaqWin win_;
	UserCommandWindow userCommandWin_;

	public QueryInterfaceAction (xdaqWin win, UserCommandWindow userCommandWin)
	{
		win_ = win;
		userCommandWin_ = userCommandWin;
	}
	
	public SOAPMessage sendCommand (String targetAddr, String url) 
	{
			try {
				xdaqSOAPMessage msg =  new xdaqSOAPMessage ( "InterfaceQuery", targetAddr );
				xdaqSOAPConnection con = new xdaqSOAPConnection();
				return con.call ( msg, url );
				
			} catch (xdaqSOAPException e) {
				System.out.println (e);
			} 
			
			return null;
	}
	
	public void actionPerformed ()
	{		
		TreePath[] paths = userCommandWin_.getSelection();
		
		win_.getStatusBar().setProgressItems (paths.length + 3);
		win_.getStatusBar().resetProgressBar();
		win_.getStatusBar().advanceProgressBar(1);
		
		String errorString = new String ();
		
		Vector commandsVector = new Vector();
		
		for (int i = 0; i < paths.length; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();
			
			if (node.toString().equals("Context") || node.toString().equals ("Application") || node.toString().equals ("Transport"))
			{
				String targetAddress;
				String url;
				
				if (node.toString().equals ("Application") || node.toString().equals ("Transport"))
				{
				  	xdaqWinTreeAdapterNode father = ((xdaqWinTreeAdapterNode) node.getUserObject()).getParent();
				  	targetAddress = aNode.getAttribute ("id");
				  	url = father.getAttribute ("url");
				} else {
					targetAddress = new String ("0"); // executive tid
					url = aNode.getAttribute ("url");
				}
				
				// Send the message that returns the parameters
				//	
				win_.getStatusBar().advanceProgressBar(1);
				
				SOAPMessage reply = sendCommand ( targetAddress, url );
				if (reply != null)
				{	
					xdaqSOAPMessage replyMsg = new xdaqSOAPMessage(reply);
					Document document = replyMsg.getDOM();
				
					// Check for xdaq error first
					//
					//NodeList faultList = document.getElementsByTagName ("faultstring");
					// match of faultstring tag in all namespaces
					NodeList faultList = document.getElementsByTagNameNS ("*", "faultstring");
					if (faultList.getLength() == 1)
					{
						errorString += url + " (";
						errorString += faultList.item(0).getFirstChild().getNodeValue();
						errorString += ")\n";	
					}
					
					NodeList parameterList = document.getElementsByTagNameNS ("*","Method");
				
					for (int j = 0; j < parameterList.getLength(); j++)
					{
						String value = parameterList.item(j).getFirstChild().getNodeValue();
						commandsVector.addElement(value);
						
					}
				} else {
					// The action failed...
					errorString += url;
					errorString += '\n';
				}
				
				
			  	
				
			} else {
				return; // don't update information
			}
		}
		
		
		if (!errorString.equals("")) {
			JOptionPane.showMessageDialog ( null, errorString, "Connection error", JOptionPane.ERROR_MESSAGE );
		} else {
			
		}
		
		// Put all commands into the JMenu of the Command Window.
		// Commands that cannot be executed on all nodes are put in grey
		// and cannot be selected.
		//
		win_.getStatusBar().advanceProgressBar(1);
		
		TreeSet commandsSet = new TreeSet(); 
		for ( int c =0; c < commandsVector.size(); c++ ) {
					
			String current = (String)commandsVector.elementAt(c);
			int counter = 0;
					
			for (int k = 0; k < commandsVector.size(); k++)
			{
				String method = (String)commandsVector.elementAt(k);
				if ( method.equals(current) ) 
				{ 
					counter++;
				}	
			}
					
			if (! commandsSet.contains(current) ) 
			{	
				if (counter < paths.length) 
				{
					userCommandWin_.addMethod ( current, false );
				}
				else 
				{
					userCommandWin_.addMethod (current, true );
				}	
				commandsSet.add(current);
			}	
		}
		
		win_.getStatusBar().resetProgressBar();
	}
	
			
}
