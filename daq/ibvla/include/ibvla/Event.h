// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_Event_h
#define _ibvla_Event_h

namespace ibvla
{
	typedef enum ibvla_event_number
	{
		IBVLA_UKNOWN_EVENT = 0x00000,
		IBVLA_CONNECTION_REQUEST_EVENT = 0x00001,
		IBVLA_CONNECTION_EVENT_ESTABLISHED = 0x00002,
		IBVLA_CONNECTION_EVENT_PEER_REJECTED = 0x00003,
		IBVLA_CONNECTION_EVENT_NON_PEER_REJECTED = 0x00004,
		IBVLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR = 0x00005,
		IBVLA_CONNECTION_EVENT_DISCONNECTED = 0x00006,
		IBVLA_CONNECTION_EVENT_BROKEN = 0x00007,
		IBVLA_CONNECTION_EVENT_TIMED_OUT = 0x00008,
		IBVLA_CONNECTION_EVENT_UNREACHABLE = 0x00009,
		IBVLA_CONNECTION_CLOSED_BY_PEER = 0x0000A,
		IBVLA_CONNECTION_RESET_BY_PEER = 0x0000B,
		IBVLA_CONNECTION_EVENT_ACCEPTED = 0x0000C,
		MAX_IBVLA_EVENT_NUMBER = IBVLA_CONNECTION_EVENT_ACCEPTED + 1
	} IBVLA_EVENT_NUMBER;

	const std::string IBVLA_UKNOWN_EVENT_STR = "IBVLA_UKNOWN_EVENT";
	const std::string IBVLA_CONNECTION_REQUEST_EVENT_STR = "IBVLA_CONNECTION_REQUEST_EVENT";
	const std::string IBVLA_CONNECTION_EVENT_ESTABLISHED_STR = "IBVLA_CONNECTION_EVENT_ESTABLISHED";
	const std::string IBVLA_CONNECTION_EVENT_PEER_REJECTED_STR = "IBVLA_CONNECTION_EVENT_PEER_REJECTED";
	const std::string IBVLA_CONNECTION_EVENT_NON_PEER_REJECTED_STR = "IBVLA_CONNECTION_EVENT_NON_PEER_REJECTED";
	const std::string IBVLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR_STR = "IBVLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR";
	const std::string IBVLA_CONNECTION_EVENT_DISCONNECTED_STR = "IBVLA_CONNECTION_EVENT_DISCONNECTED";
	const std::string IBVLA_CONNECTION_EVENT_BROKEN_STR = "IBVLA_CONNECTION_EVENT_BROKEN";
	const std::string IBVLA_CONNECTION_EVENT_TIMED_OUT_STR = "IBVLA_CONNECTION_EVENT_TIMED_OUT";
	const std::string IBVLA_CONNECTION_EVENT_UNREACHABLE_STR = "IBVLA_CONNECTION_EVENT_UNREACHABLE";
	const std::string IBVLA_CONNECTION_CLOSED_BY_PEER_STR = "IBVLA_CONNECTION_CLOSED_BY_PEER";
	const std::string IBVLA_CONNECTION_RESET_BY_PEER_STR = "IBVLA_CONNECTION_RESET_BY_PEER";
	const std::string IBVLA_CONNECTION_EVENT_ACCEPTED_STR = "IBVLA_CONNECTION_EVENT_ACCEPTED";

	typedef union ibvla_context
	{
			void * as_ptr;
			size_t as_64;
			size_t as_index;
	} IBVLA_CONTEXT;

	typedef struct ibvla_connection_request_event_data
	{
			ibvla::Acceptor* acceptor;
			struct rdma_cm_id *id;
	} IBVLA_CONNECTION_REQUEST_EVENT_DATA;

	typedef struct ibvla_connection_established_event_data
	{
			ibvla::QueuePair queue_pair;
			IBVLA_CONTEXT user_cookie;
	} IBVLA_CONNECTION_ESTABLISHED_EVENT_DATA;

	typedef struct ibvla_connection_broken_event_data
	{
			ibvla::QueuePair queue_pair;
			size_t reason;
			xcept::Exception * exception;
	} IBVLA_CONNECTION_BROKEN_EVENT_DATA;

	typedef union ibvla_event_data
	{
			IBVLA_CONNECTION_EVENT_BROKEN_DATA connection_broken_event_data;
			IBVLA_CONNECTION_REQUEST_EVENT_DATA connection_request_event_data;
			IBVLA_CONNECTION_ESTABLISHED_EVENT_DATA connect_established_event_data;
	} IBVLA_EVENT_DATA;

	class Event
	{
		public:
			IBVLA_EVENT_NUMBER event_number;
			IBVLA_EVENT_DATA event_data;
	};
}

#endif
