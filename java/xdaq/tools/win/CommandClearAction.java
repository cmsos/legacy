package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CommandClearAction extends AbstractAction
{
	xdaqWin win_;

	public CommandClearAction (xdaqWin win)
	{
		super ("Clear");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {
			win_.executeCommandOnSelection ("Clear", null, win_.getPaths());
		}
		catch(xdaqWinException ex )
		{
		
		}

	}
}
