package net.hep.cms.xdaqctl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Vector;

import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeader;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Document;

import junit.framework.TestCase;
import net.hep.cms.xdaqctl.xdata.FlashList;
import net.hep.cms.xdaqctl.xdata.SimpleItem;
import net.hep.cms.xdaqctl.xdata.XDataType;

public class XMASMessageTest extends TestCase {

	private static final String RCMS_ORIGINATOR = 
		"http://localhost:0/urn:rcms-fm:fullpath=/rcms/test";

	public XMASMessageTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(XMASMessageTest.class);
	}

	public void testXMASMessage() throws Exception
	{
		// create a simple flash list
		FlashList fl = new FlashList();

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);
		
		((SimpleItem)fl.get("string")).setValue("LongerThanFour");
		((SimpleItem)fl.get("integer")).setValue(3);
		
		XMASMessage message = new XMASMessage(fl, RCMS_ORIGINATOR);

		message.dump("report.dump");
	}


	public void testXMASMessageFromSOAP() throws Exception
	{
		// create a simple flash list
		FlashList fl = new FlashList();

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);
		
		((SimpleItem)fl.get("string")).setValue("LongerThanFour");
		((SimpleItem)fl.get("integer")).setValue(3);
		
		XMASMessage tmp = new XMASMessage(fl, RCMS_ORIGINATOR);
		
		XMASMessage message = new XMASMessage(tmp.message);
		assertNotNull(message);

		// testGetOriginator
		assertEquals(RCMS_ORIGINATOR, message.getOriginator());	
	}

	public void testTag() throws Exception
	{
		// create a simple flash list
		FlashList fl = new FlashList();

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);
		
		((SimpleItem)fl.get("string")).setValue("LongerThanFour");
		((SimpleItem)fl.get("integer")).setValue(3);
		
		XMASMessage message = new XMASMessage(fl, RCMS_ORIGINATOR);
		assertEquals("tag", message.getTag());
		
		message.setTag("testTag");
		assertEquals("testTag", message.getTag());
	}
	
	public void testSend() throws Exception
	{
		// create a simple flash list
		FlashList fl = new FlashList("test");

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);
		fl.add("table", XDataType.TABLE);

		((SimpleItem)fl.get("string")).setValue("LongerThanFour");
		((SimpleItem)fl.get("integer")).setValue(3);

		net.hep.cms.xdaqctl.xdata.Table table =
				(net.hep.cms.xdaqctl.xdata.Table)fl.get("table");
		table.addColumn("uint32", XDataType.UNSIGNED_INTEGER32);
		table.addColumn("string", XDataType.STRING);
		
		Vector<Object> row = new Vector<Object>();
		row.clear();
		row.add(0x80000000);
		row.add("the first");
		table.add(row);
		row.clear();
		row.add(0x7fffffff);
		row.add("the second");
		table.add(row);
	
		XMASMessage message = new XMASMessage(fl, RCMS_ORIGINATOR);

		Document document = message.send("http://localhost:40000");
		assertNotNull(document);
		
		// Contents of the message is created only at send()
		checkSOAPPart("test/xdaq/xmas-soap.xml", message);
		checkAttachmentHeader("test/xdaq/xmas-att.txt", message);

		String  good_result =
				"<?xml version='1.0' encoding='UTF-8'?>\n" +
				"<xmas:reportResponse xmlns:xmas='http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10'/>\n";

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), document);
		assertNull(result, result);
		
		// wait some time for the dash board to update
		URL url = new URL("http://localhost:40001/urn:xdaq-application:lid=10/");
		URLConnection c = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				c.getInputStream()));
			
		String dashboardPage = "";
		
		String line;
		while ((line = in.readLine()) != null) {
			dashboardPage += line + "\n";
		}
		in.close();

		
		String s = URLEncoder.encode(RCMS_ORIGINATOR, "US-ASCII");
		int i = dashboardPage.indexOf(s);
		assertTrue("Couldn't find " + s + " in the dashboard page.", -1 != i);
		
		System.out.println("sleeping 10s");
		Thread.sleep(10000);
		System.out.println("woke up");
		
		document = message.send("http://localhost:40000");
		assertNotNull(document);

	}

	public void testSendToMonitorServlet() throws Exception
	{
		// create a simple flash list
		FlashList fl = new FlashList("test");

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);

		((SimpleItem)fl.get("string")).setValue("LongerThanFour");
		((SimpleItem)fl.get("integer")).setValue(3);
	
		XMASMessage message = new XMASMessage(fl, RCMS_ORIGINATOR);
		
		Document document = message.send("http://localhost:8080/rcms/servlet/monitorreceiver");
		assertNotNull(document);

		String  good_result =
				"<?xml version='1.0' encoding='UTF-8'?>\n" +
				"<xmas:reportResponse xmlns:xmas='http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10'/>\n";

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), document);
		assertNull(result, result);
		
	}
	
	public void testGetFlashList() throws Exception 
	{
		// create a simple flash list
		FlashList fl = new FlashList();

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);
		
		((SimpleItem)fl.get("string")).setValue("LongerThanFour");
		((SimpleItem)fl.get("integer")).setValue(3);
		
		XMASMessage tmp = new XMASMessage(fl, RCMS_ORIGINATOR);
		
		XMASMessage message = new XMASMessage(tmp.message);
		
		FlashList fl2 = message.getFlashList();
		
		assertNotNull(fl2);
		assertEquals(fl.getItemNames(), fl2.getItemNames());
	}
	
	private void checkSOAPPart(String string, Message message)
			throws Exception
	{	
		assertNotNull(message);

		String result = TestUtil.compareDOMs(
				TestUtil.fileToDOM(string), message.getDOM());
		
		assertNull(result, result);
	}
	
	private void checkAttachmentHeader(String filename, XMASMessage message)
			throws Exception  {
	
		assertNotNull(message);
		
		AttachmentPart part = (AttachmentPart)message.message.getAttachments().next();
		BufferedReader in = new BufferedReader(new FileReader(filename));
		
		String line;
		while ((line = in.readLine()) != null) {
			int i = line.indexOf(':');
			
			String name = line.substring(0, i);
			String value = line.substring(i + 1).trim();
			
			String[] values = part.getMimeHeader(name);
			assertNotNull("No header of " + name, values);
			assertEquals("Found " + values.length + " values for " + name, 1, values.length);
			
			if (values[0].matches(".*\\d{4}-\\d{2}-\\d{2}.*")) {
				values[0] = values[0].replaceAll("\\d", "n"); // mask out date string
			}
			assertEquals("Header value for " + name, value, values[0]);
		}
		
	}


}
