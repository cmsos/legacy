/**
*      @file ATTSInterfaceCard.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:25 $
*
*
**/
#include "tts/atts/ATTSInterfaceCard.hh"
#include "xcept/Exception.h"

#include <iostream>
#include <sstream>
#include <iomanip>

tts::ATTSInterfaceCard::ATTSInterfaceCard( HAL::PCIDevice* attsdevice ) : _atts(attsdevice), _serialnumber(""), _geoslot(999) {};

tts::ATTSInterfaceCard::~ATTSInterfaceCard() {
  delete _atts;
};

uint32_t tts::ATTSInterfaceCard::getGeoSlot() 
  throw (HAL::HardwareAccessException) {
  
  if (_geoslot == 999) {

    uint32_t val;
    _atts->read("GeoSlot", &val);
    _geoslot = (uint32_t) val;

  }

  return _geoslot;
}

void tts::ATTSInterfaceCard::reset() 
    throw (HAL::HardwareAccessException) { 
  _atts->write("reset_all",0x7f); 
  _atts->write("reset_all",0x0); 
};


std::string tts::ATTSInterfaceCard::readFirmwareRev() 
    throw (HAL::HardwareAccessException) {

  uint32_t year;
  uint32_t month;
  uint32_t day;
  uint32_t rev_in_day;

  _atts->read("ID_year", &year);
  _atts->read("ID_month", &month);
  _atts->read("ID_day", &day);
  _atts->read("ID_rev_in_day", &rev_in_day);

  std::string rev =  
    BCDtoString( year ) +
    BCDtoString( month ) +
    BCDtoString( day) +
    "_" +
    BCDtoString( rev_in_day );

  return rev;
}

std::string tts::ATTSInterfaceCard::readFirmwareRevAltera() 
    throw (HAL::HardwareAccessException) {

  
  uint32_t fwid_altera;
  _atts->read("FWID_ALTERA", &fwid_altera);

  std::stringstream rev;
  rev << std::hex << std::setw(8) << std::setfill('0') << fwid_altera;

  return rev.str();
}

std::string tts::ATTSInterfaceCard::BCDtoString(uint32_t num, uint32_t ndigit) {

  std::stringstream txt;
  for (int i = (int) ndigit-1; i>=0; i--)
    txt << std::setw(1) << std::hex << ( ( num & ( 0xf << (4*i) ) ) >> (4*i) );

  return txt.str();
}

bool tts::ATTSInterfaceCard::isSimuMode() 
    throw (HAL::HardwareAccessException) {
  
  uint32_t simu_mode;
  _atts->read("Test_select", &simu_mode);

  return (simu_mode & 0x1) == 0x1;
}

void tts::ATTSInterfaceCard::toggleSimuMode(bool simu_mode) 
    throw (HAL::HardwareAccessException) {

  _atts->write("Test_select", simu_mode?0x1:0x0);

}


void tts::ATTSInterfaceCard::setLEDs(uint32_t led_setting) 
    throw (HAL::HardwareAccessException) {
  _atts->write("leds", led_setting);
};


void tts::ATTSInterfaceCard::setSimulatedInputStates(std::vector <tts::TTSState> const& states) 
    throw (HAL::HardwareAccessException) {

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  std::vector<tts::TTSState>::const_iterator it = states.begin();
  for (int i=0; it!= states.end() && i < 12; it++, i++) {
    ready |= ( ((*it) & 0x8) >> 3 ) << i;
    busy  |= ( ((*it) & 0x4) >> 2 ) << i;
    sync  |= ( ((*it) & 0x2) >> 1 ) << i;
    warn  |= ( ((*it) & 0x1)      ) << i;
  }

  _atts->write("Test_in_ready", ready);
  _atts->write("Test_in_busy", busy);
  _atts->write("Test_in_synch", sync);
  _atts->write("Test_in_overflow", warn);

  _atts->write("Update_test_reg", 0x1);
};


void tts::ATTSInterfaceCard::setSimulatedInputState(uint32_t channel, tts::TTSState const& state) 
    throw (HAL::HardwareAccessException, xcept::Exception) {

  if (channel > 11)
    XCEPT_RAISE(xcept::Exception, "channel out of range");

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _atts->read("Test_in_ready", &ready);
  _atts->read("Test_in_busy", &busy);
  _atts->read("Test_in_synch", &sync);
  _atts->read("Test_in_overflow", &warn);

  ready &= ~( ((uint32_t)1) << channel);
  busy  &= ~( ((uint32_t)1) << channel);
  sync  &= ~( ((uint32_t)1) << channel);
  warn  &= ~( ((uint32_t)1) << channel);

  ready |= ( (state & 0x8) >> 3 ) << channel;
  busy  |= ( (state & 0x4) >> 2 ) << channel;
  sync  |= ( (state & 0x2) >> 1 ) << channel;
  warn  |= ( (state & 0x1)      ) << channel;

  _atts->write("Test_in_ready", ready);
  _atts->write("Test_in_busy", busy);
  _atts->write("Test_in_synch", sync);
  _atts->write("Test_in_overflow", warn);

  _atts->write("Update_test_reg", 0x1);
};


std::vector<tts::TTSState> tts::ATTSInterfaceCard::readbackSimulatedInputStates() 
    throw (HAL::HardwareAccessException) {

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _atts->read("Test_in_ready", &ready);
  _atts->read("Test_in_busy", &busy);
  _atts->read("Test_in_synch", &sync);
  _atts->read("Test_in_overflow", &warn);

  std::vector<tts::TTSState> states(12, 0);

  for (int i=0;i<12;i++) {

    uint32_t state = 0;
    state |= ( (ready & (1<<i)) >> i ) << 3;
    state |= ( (busy  & (1<<i)) >> i ) << 2;
    state |= ( (sync  & (1<<i)) >> i ) << 1;
    state |= ( (warn  & (1<<i)) >> i ) ;
    
    states[i] = state;
  }

  return states;
};



void tts::ATTSInterfaceCard::setDAQPartitionState(uint32_t ipartition, tts::TTSState const& state) 
    throw (HAL::HardwareAccessException, xcept::Exception) {

  if (ipartition > 11)
    XCEPT_RAISE(xcept::Exception, "ipartition out of range");

  std::stringstream item;
  item << "Output" << ipartition;

  _atts->write(item.str(), (uint32_t) state);
}

void tts::ATTSInterfaceCard::pulseDAQPartitionState(uint32_t ipartition, tts::TTSState const& state)
  throw (HAL::HardwareAccessException, xcept::Exception) {

  XCEPT_RAISE(xcept::Exception, "pulsing of DAQ partition states not yet implemented.");

}



tts::TTSState tts::ATTSInterfaceCard::readbackDAQPartitionState(uint32_t ipartition) 
    throw (HAL::HardwareAccessException, xcept::Exception) {

  if (ipartition > 11)
    XCEPT_RAISE(xcept::Exception, "ipartition out of range");

  std::stringstream item;
  item << "Output" << ipartition;

  uint32_t state;
  _atts->read(item.str(), &state);

  return tts::TTSState(state);
}

std::vector<tts::TTSState> tts::ATTSInterfaceCard::readTCSPartitionStates() 
    throw (HAL::HardwareAccessException) {

  uint32_t ready=0;
  uint32_t busy=0;
  uint32_t sync=0;
  uint32_t warn=0;

  _atts->read("Input_ready", &ready);
  _atts->read("Input_busy", &busy);
  _atts->read("Input_synch", &sync);
  _atts->read("Input_overflow", &warn);

  std::vector<tts::TTSState> states(12, 0);

  for (int i=0;i<12;i++) {

    uint32_t state = 0;
    state |= ( (ready & (1<<i)) >> i ) << 3;
    state |= ( (busy  & (1<<i)) >> i ) << 2;
    state |= ( (sync  & (1<<i)) >> i ) << 1;
    state |= ( (warn  & (1<<i)) >> i ) ;
    
    states[i] = state;
  }

  return states;
};

tts::TTSState tts::ATTSInterfaceCard::readTCSPartitionState(uint32_t ipartition) 
    throw (HAL::HardwareAccessException, xcept::Exception) {

  if (ipartition > 11)
    XCEPT_RAISE(xcept::Exception, "ipartition out of range");

  return readTCSPartitionStates()[ipartition];
};

std::string tts::ATTSInterfaceCard::getSerialNumber() 
    throw (HAL::HardwareAccessException) {

  if (_serialnumber == "") {
    std::stringstream sn;
    uint32_t value;
    
    _atts->read("sn_c", &value);  
    sn << std::setw(3) << std::setfill('0') << std::hex << value;

    _atts->read("sn_b", &value);
    sn << std::setw(8) << std::setfill('0') << std::hex << value;

    _atts->read("sn_a", &value);
    sn << std::setw(8) << std::setfill('0') << std::hex << value;

    _serialnumber = sn.str();
  }
  
  return _serialnumber;

};

