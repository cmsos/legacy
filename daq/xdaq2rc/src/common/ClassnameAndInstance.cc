#include "xdaq2rc/ClassnameAndInstance.h"


void xdaq2rc::ClassnameAndInstance::registerFields
(
  xdata::Bag<ClassnameAndInstance> *bag
)
{
  bag->addField("classname", &classname);
  bag->addField("instance",  &instance);
  bag->addField("url", &url);
  bag->addField("doStartupNotification",  &doStartupNotification);
}
