// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_supervisor_version_h_
#define _bril_supervisor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILSUPERVISOR_VERSION_MAJOR 1
#define BRILSUPERVISOR_VERSION_MINOR 10
#define BRILSUPERVISOR_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILSUPERVISOR_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRIL_VERSION_CODE PACKAGE_VERSION_CODE(BRILSUPERVISOR_VERSION_MAJOR,BRILSUPERVISOR_VERSION_MINOR,BRILSUPERVISOR_VERSION_PATCH)
#ifndef BRILSUPERVISOR_PREVIOUS_VERSIONS
#define BRILSUPERVISOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILSUPERVISOR_VERSION_MAJOR,BRILSUPERVISOR_VERSION_MINOR,BRILSUPERVISOR_VERSION_PATCH)
#else
#define BRILSUPERVISOR_FULL_VERSION_LIST  BRILSUPERVISOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILSUPERVISOR_VERSION_MAJOR,BRILSUPERVISOR_VERSION_MINOR,BRILSUPERVISOR_VERSION_PATCH)
#endif

namespace brilsupervisor
{
	const std::string package = "brilsupervisor";
	const std::string versions = BRILSUPERVISOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ supervisor";
	const std::string description = "bril supervisor";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
