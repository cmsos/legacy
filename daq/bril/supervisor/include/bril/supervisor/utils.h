#ifndef _bril_supervisor_utils_h_
#define _bril_supervisor_utils_h_
#include <unistd.h>
namespace bril{
  namespace supervisor{
    namespace utils{
      struct RefDestroyer{
      RefDestroyer( toolbox::mem::Reference* ref ): m_ref(ref){}
	~RefDestroyer(){ if(m_ref) m_ref->release(); }
	toolbox::mem::Reference* m_ref;    
      };    
    }
  }  
}
#endif
