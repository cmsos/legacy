#ifndef _tcds_brildaqchecker_ConfigurationInfoSpaceHandler_h_
#define _tcds_brildaqchecker_ConfigurationInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/ConfigurationInfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace brildaqchecker {

    class ConfigurationInfoSpaceHandler :
      public tcds::utils::ConfigurationInfoSpaceHandler
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_ConfigurationInfoSpaceHandler_h_
