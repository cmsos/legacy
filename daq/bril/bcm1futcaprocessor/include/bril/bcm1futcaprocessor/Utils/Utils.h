#ifndef _bril_bcm1f_utcaprocessor_Utils_h_
#define _bril_bcm1f_utcaprocessor_Utils_h_

#include <iostream>
#include <string>
#include "ConsoleColor.h"

namespace bril {

    namespace bcm1futcaprocessor {

        static std::string expandEnvironmentVariables (  std::string s )
        {
            s.erase (std::remove_if (s.begin(), s.end(), ::isspace), s.end() );

            if ( s.find ( "${" ) == std::string::npos ) return s;

            std::string pre  = s.substr ( 0, s.find ( "${" ) );
            std::string post = s.substr ( s.find ( "${" ) + 2 );

            if ( post.find ( '}' ) == std::string::npos ) return s;

            std::string variable = post.substr ( 0, post.find ( '}' ) );
            std::string value    = "";

            post = post.substr ( post.find ( '}' ) + 1 );

            if ( getenv ( variable.c_str() ) != NULL ) value = std::string ( getenv ( variable.c_str() ) );

            return expandEnvironmentVariables ( pre + value + post );
        }

        //inline std::string parseExternalResource (std::string pStylesheet)
        //{
        //std::string cResult;

        //std::ifstream cStyleStream;
        //cStyleStream.open (pStylesheet.c_str() );

        //if (cStyleStream.is_open() )
        //{
        //cStyleStream.seekg (0, std::ios::end);
        //cResult.reserve (cStyleStream.tellg() );
        //cStyleStream.seekg (0, std::ios::beg);
        //cResult.assign ( (std::istreambuf_iterator<char> (cStyleStream) ), std::istreambuf_iterator<char>() );
        ////pStream << "Successfully parsed CSS Stylesheet " << pStylesheet << std::endl;
        //}
        //else
        //std::cout << RED << "Error, CSS Stylesheet " << pStylesheet << " could not be opened!" << RESET << std::endl;

        //return cResult;
        //}

        inline std::string convert (float number)
        {
            std::stringstream ss;
            ss << number;
            return ss.str();
        }

    } //namespace bcm1futcaprocessor
} //namespace bril
#endif
