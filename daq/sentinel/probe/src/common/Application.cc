// $Id$

/*************************************************************************
 * XDAQ Sentinel Probe		               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include "sentinel/probe/Application.h"
#include "sentinel/utils/Serializer.h"
#include "sentinel/utils/Alarm.h"
#include "sentinel/utils/NewsEvent.h"

#include "toolbox/stl.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/exception/InvalidListener.h"
#include "toolbox/task/exception/NotActive.h"
#include "toolbox/task/exception/InvalidSubmission.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "pt/PeerTransportAgent.h"
#include "pt/SOAPMessenger.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/Event.h"

#include "xcept/tools.h"

#include "xplore/Interface.h"
#include "xplore/DiscoveryEvent.h"
#include "xplore/exception/Exception.h"

#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/Method.h"
#include "xgi/Method.h"

#include "xgi/Utils.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

#include "b2in/nub/Method.h"

#include "b2in/utils/exception/SendFailure.h"
#include "b2in/utils/exception/CheckConnectionFailure.h"

XDAQ_INSTANTIATOR_IMPL (sentinel::probe::Application)

sentinel::probe::Application::Application (xdaq::ApplicationStub * s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), repositoryLock_(toolbox::BSem::FULL)
{
	s->getDescriptor()->setAttribute("icon", "/sentinel/probe/images/sentinel-probe-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/sentinel/probe/images/sentinel-probe-icon.png");
	// override default service name
	s->getDescriptor()->setAttribute("service", "sentinelprobe");

	watchdog_ = "PT5S"; // 5 seconds bys default
	committedPoolSize_ = 0x100000 * 5; // 5 MB
	maxExceptionMessageSize_ = 0x10000; // 64KB
	sentineldURL_ = "";

	outgoingCounter_ = 0;
	outgoingLossCounter_ = 0;

	// Static eventing configuration
	this->getApplicationInfoSpace()->fireItemAvailable("sentineldURL", &sentineldURL_);

	// General configuration parameters
	this->getApplicationInfoSpace()->fireItemAvailable("maxExceptionMessageSize", &maxExceptionMessageSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("watchdog", &watchdog_);
	this->getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);

	xgi::framework::deferredbind(this, this, &sentinel::probe::Application::Default, "Default");
	//xgi::framework::deferredbind(this, this, &sentinel::probe::Application::view, "view");
	xgi::bind(this, &sentinel::probe::Application::view, "view");
	// test function
	xgi::bind(this, &sentinel::probe::Application::inject, "inject");

	// Listen to events indicating the setting of the application's default values
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events

	// install a default error handler
	toolbox::exception::HandlerSignature* defaultExceptionHandler = toolbox::exception::bind(this, &sentinel::probe::Application::handleException, "sentinel::probe::Application::handleException");
	toolbox::exception::getProcessor()->setDefaultHandler(defaultExceptionHandler);

	// create infospace for alarms
	try
	{
		alarms_ = xdata::getInfoSpaceFactory()->create("urn:xdaq-sentinel:alarms");
		alarms_->addItemAvailableListener(this); // add listener to newly created alarms
		alarms_->addItemRevokedListener(this); // add listener for revoking alarms
	}
	catch (xdata::exception::Exception & e)
	{
		XCEPT_RETHROW(xdaq::exception::Exception, "failed to create alarms infospace", e);
	}

}

sentinel::probe::Application::~Application ()
{
}

bool sentinel::probe::Application::handleException (xcept::Exception& ex, void* context)
{
	repositoryLock_.take();

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Probe received exception '" << ex.name() << "'");

	for (std::list<std::pair<xcept::Exception, size_t> >::iterator i = exceptions_.begin(); i != exceptions_.end(); ++i)
	{
		// Send a single message for each exception accumulated
		// If the notifier and the stack are the same, increment counter (the exceptions are equal and therefore absorbed)
		//
		if ((ex.getProperty("notifier") == (*i).first.getProperty("notifier")) && (ex.equals((*i).first)))
		{
			((*i).second)++;
			repositoryLock_.give();
			return true;
		}
	}

	exceptions_.push_back(std::pair<xcept::Exception, size_t>(ex, 1));

	repositoryLock_.give();
	return true;
}

void sentinel::probe::Application::actionPerformed (toolbox::Event& e)
{
	if (e.type() == "xdaq::EndpointAvailableEvent")
	{
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName() << " on application network " << networkName);

		if (network->getName() == networkName)
		{
			try
			{
				sentineldMessengerCache_ = new b2in::utils::MessengerCache(this->getApplicationContext(), networkName, this);		//, watchdog_);
				std::stringstream ss;
				ss << "Messenger cache created on network " << networkName;
				LOG4CPLUS_WARN(this->getApplicationLogger(), ss.str());
			}
			catch (b2in::utils::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to create b2in messenger cache on network '" << network << "'";
				LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
				LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
				return;
			}
			try
			{
				sentineldMessengerCache_->createMessenger(sentineldURL_); // trigger caching of messenger for the remote endpoint
			}
			catch (b2in::utils::exception::Exception& e)
			{
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << sentineldURL_.toString() << ", " << e.what());
				this->notifyQualified("error", e);
				return;
			}

			if (!toolbox::task::getTimerFactory()->hasTimer("sentinel"))
			{
				(void) toolbox::task::getTimerFactory()->createTimer("sentinel");
			}
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("sentinel");

			// submit task
			toolbox::TimeInterval interval;
			try
			{
				interval.fromString(watchdog_.toString());
				toolbox::TimeVal start;
				start = toolbox::TimeVal::gettimeofday();
				timer->scheduleAtFixedRate(start, this, interval, 0, "sentinel-staging");
			}
			catch (toolbox::task::exception::InvalidListener& e)
			{
				// Failed to submit to timer
				LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
			catch (toolbox::task::exception::InvalidSubmission& e)
			{
				// Failed to submit to timer
				LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
			catch (toolbox::task::exception::NotActive& e)
			{
				// Failed to submit to timer
				LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
			catch (toolbox::exception::Exception& e)
			{
				// Failed to parse the watchdog time string
				LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
		}
		else
		{
			std::stringstream ss;
			ss << "No match found for network name, not creating messenger cache : configured network name = '" << networkName << "', available = '" << network->getName() << "'";
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());
			return;
		}

	}
}

void sentinel::probe::Application::actionPerformed (xdata::Event& e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			toolbox::net::URN urn("sentinel", "probe");
			pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to create b2in/sensor memory pool for size " << committedPoolSize_.toString();
			LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());

		}

	}
	else if (e.type() == "ItemAvailableEvent")
	{
		// an alarm has been fired into the infospace urn:xdaq-sentinel:alarms
		xdata::ItemAvailableEvent & available = dynamic_cast<xdata::ItemAvailableEvent&>(e);
		sentinel::utils::Alarm* alarm = dynamic_cast<sentinel::utils::Alarm*>(available.item());
		if (alarm != 0)
		{

			try
			{
				this->publishEvent("notify", alarm->getException());
			}
			catch (sentinel::probe::exception::Exception& e)
			{
				return;
			}

		}
		else
		{
			std::stringstream msg;
			msg << "Item in ItemAvailableEvent was not of type 'sentinel::utils::Alarm*'";
			LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
		}
	}
	else if (e.type() == "ItemRevokedEvent")
	{
		// an alarm has been revoked from the infospace urn:xdaq-sentinel:alarms
		xdata::ItemRevokedEvent & revoke = dynamic_cast<xdata::ItemRevokedEvent&>(e);
		sentinel::utils::Alarm* alarm = dynamic_cast<sentinel::utils::Alarm*>(revoke.item());
		if (alarm != 0)
		{

			try
			{
				this->publishEvent("revoke", alarm->getException());
				return;
			}
			catch (sentinel::probe::exception::Exception& e)
			{
				// fall through
			}

			repositoryLock_.take();
			revokes_.push_back(alarm->getException());
			repositoryLock_.give();
		}
		else
		{
			std::stringstream msg;
			msg << "Item in ItemAvailableEvent was not of type 'sentinel::utils::Alarm*'";
			LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to process unknown event type '" << e.type() << "'";
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
	}
}

void sentinel::probe::Application::publishEvent (const std::string & type, xcept::Exception& e) 
{

	toolbox::net::URL at(getApplicationContext()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN());

	xdata::Properties plist;
	plist.setProperty("urn:b2in-protocol:service", "sentineld");
	plist.setProperty("urn:b2in-eventing:topic", "sentinel");
	plist.setProperty("urn:b2in-eventing:action", "notify");
	plist.setProperty("urn:sentinel-event:name", type);
	plist.setProperty("urn:sentinel-exception:identifier", e.name());

	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxExceptionMessageSize_);
	}
	catch (toolbox::mem::exception::Exception & ex)
	{
		XCEPT_RETHROW(sentinel::probe::exception::Exception, "Failed to allocate messaage for monitor report", e);
	}

	try
	{
		xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) ref->getDataLocation(), maxExceptionMessageSize_);
		xdaq::XceptSerializer::writeTo(e, &outBuffer);
		ref->setDataSize(outBuffer.tellp());
	}
	catch (xdata::exception::Exception & e)
	{
		XCEPT_RETHROW(sentinel::probe::exception::Exception, "Failed to serialize exception", e);
	}
	if (!sentineldMessengerCache_->hasMessenger(sentineldURL_))
	{
		try
		{
			sentineldMessengerCache_->createMessenger(sentineldURL_); // trigger caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			std::stringstream ss;
			ss << "Failed to create messenger to " << sentineldURL_.toString();
			LOG4CPLUS_FATAL(this->getApplicationLogger(), ss.str() << " : " << xcept::stdformat_exception_history(e));
			XCEPT_RETHROW(sentinel::probe::exception::Exception, ss.str(), e);
		}
	}
	try
	{
		//sentineldMessengerWrapper_->send(ref, plist, (void*)&outgoingLossCounter_);
		sentineldMessengerCache_->send(sentineldURL_, ref, plist);
		outgoingCounter_++;
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		outgoingLossCounter_++;
		LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to publish exception to sentineld " << " : " << xcept::stdformat_exception_history(e));
		XCEPT_RETHROW(sentinel::probe::exception::Exception, "Failed to send", e);
	}
	catch (b2in::nub::exception::QueueFull & e)
	{
		outgoingLossCounter_++;
		LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to publish exception to sentineld " << " : " << xcept::stdformat_exception_history(e));
		XCEPT_RETHROW(sentinel::probe::exception::Exception, "Failed to send", e);
	}
	catch (b2in::nub::exception::OverThreshold & e)
	{
		outgoingLossCounter_++;
		LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to publish exception to sentineld " << " : " << xcept::stdformat_exception_history(e));
		XCEPT_RETHROW(sentinel::probe::exception::Exception, "Failed to send", e);
	}
}

void sentinel::probe::Application::asynchronousExceptionNotification (xcept::Exception& ex)
{
	//LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Sending an exception failed, " << ex.what());
	LOG4CPLUS_TRACE(this->getApplicationLogger(), "Sending an exception or messenger cache check connection failed, " << ex.what());

	if (sentineldURL_ != "")
	{
		if (!sentineldMessengerCache_->hasMessenger(sentineldURL_))
		{
			try
			{
				sentineldMessengerCache_->createMessenger(sentineldURL_); // trigger caching of messenger for the remote endpoint
			}
			catch (b2in::utils::exception::Exception& e)
			{
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << sentineldURL_.toString() << ", " << e.what());
				//this->notifyQualified("error", e); // recursion
				return;
			}
		}
		else
		{
			LOG4CPLUS_TRACE(this->getApplicationLogger(), "Network exception, messenger already exists");
		}
	}

	/*
	 //std::cout << "ex == " << ex.name() << std::endl;
	 if (context != 0 && ex.name() == "pt::exception::SendFailure")
	 {
	 xdata::UnsignedInteger64* counterPtr = static_cast<xdata::UnsignedInteger64*>(context);
	 (*counterPtr)++;
	 //std::cout << "counter incremented : ex == " << ex.name() << std::endl;
	 }
	 */

	if (ex.name() == "b2in::utils::exception::SendFailure")
	{
		outgoingLossCounter_++;
	}
}

void sentinel::probe::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;

	if (name == "sentinel-staging")
	{
		this->processPendingEvents();
	}
}

void sentinel::probe::Application::processPendingEvents ()
{
	// exceptions
	repositoryLock_.take();
	try
	{
		while (!exceptions_.empty())
		{
			std::pair < xcept::Exception, size_t > &item = exceptions_.front();
			// Set the occurences
			std::stringstream o;
			o << item.second;
			item.first.setProperty("occurrences", o.str());
			this->publishEvent("notify", item.first);
			exceptions_.pop_front();
		}
	}
	catch (sentinel::probe::exception::Exception& e)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		repositoryLock_.give();
		return;
	}
	repositoryLock_.give();

	// Alarms processing (fire)
	alarms_->lock();

	std::map<std::string, xdata::Serializable *, std::less<std::string> > alarms = alarms_->match(".*");
	for (std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = alarms.begin(); i != alarms.end(); i++)
	{
		sentinel::utils::Alarm* alarm = dynamic_cast<sentinel::utils::Alarm*>((*i).second);
		if (alarm != 0)
		{
			try
			{
				this->publishEvent("notify", alarm->getException());
			}
			catch (sentinel::probe::exception::Exception& e)
			{
				alarms_->unlock();
				return;
			}
		}
		else
		{
			std::stringstream msg;
			msg << "Item in infospace was not of type 'sentinel::utils::Alarm*'";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
		}
	}
	alarms_->unlock();

	// Alarms processing (revoke)
	repositoryLock_.take();
	try
	{
		while (!revokes_.empty())
		{
			xcept::Exception& item = revokes_.front();
			item.setProperty("occurrences", "1");
			this->publishEvent("revoke", item);
			revokes_.pop_front();
		}
	}
	catch (sentinel::probe::exception::Exception& e)
	{
		repositoryLock_.give();
		return;
	}
	repositoryLock_.give();
}

// Test function!!!
void sentinel::probe::Application::inject (xgi::Input * in, xgi::Output * out) 
{
	try
	{
		std::string identifier = "toolbox::exception::Test";
		//std::string notifier = "http://srv-c2d06-17.cms:2345/urn:xdaq-application:lid=10";
		std::stringstream notifierss;
		notifierss << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();
		std::string notifier = notifierss.str();
		std::string severity = "error";
		std::string className = "sentinel::probe::Application";
		std::string tag = "";
		std::string instance = "";
		std::string unamespace = "urn:jobcontrol:jid";
		std::string uvalue = "34";
		std::string group = this->getApplicationDescriptor()->getAttribute("group");

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("identifier");
		if (fi != cgi.getElements().end())
		{
			identifier = (*fi).getValue();
		}

		fi = cgi.getElement("notifier");
		if (fi != cgi.getElements().end())
		{
			notifier = (*fi).getValue();
		}

		fi = cgi.getElement("severity");
		if (fi != cgi.getElements().end())
		{
			severity = (*fi).getValue();
		}

		fi = cgi.getElement("class");
		if (fi != cgi.getElements().end())
		{
			className = (*fi).getValue();
		}

		fi = cgi.getElement("tag");
		if (fi != cgi.getElements().end())
		{
			tag = (*fi).getValue();
		}

		fi = cgi.getElement("instance");
		if (fi != cgi.getElements().end())
		{
			instance = (*fi).getValue();
		}

		fi = cgi.getElement("unamespace");
		if (fi != cgi.getElements().end())
		{
			unamespace = (*fi).getValue();
		}

		fi = cgi.getElement("uvalue");
		if (fi != cgi.getElements().end())
		{
			uvalue = (*fi).getValue();
		}

		fi = cgi.getElement("group");
		if (fi != cgi.getElements().end())
		{
			group = (*fi).getValue();
		}

		xcept::Exception exception1("toolbox::exception::TestLowLevel", "low level", __FILE__, __LINE__, __FUNCTION__);
		xcept::Exception exception2("xoap::exception::TestMidLevel", "mid level", __FILE__, __LINE__, __FUNCTION__, exception1);
		xcept::Exception exception3(identifier, "this is an exception", __FILE__, __LINE__, __FUNCTION__, exception2);

		exception3.setProperty("notifier", notifier);
		exception3.setProperty("severity", severity);
		exception3.setProperty("urn:xdaq-application:class", className);
		exception3.setProperty("tag", tag);
		exception3.setProperty("urn:xdaq-application:instance", instance);
		exception3.setProperty(unamespace, uvalue);
		exception3.setProperty("urn:xdaq-application:group", group);

		this->handleException(exception3, 0);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
}

// Hyperdaq

void sentinel::probe::Application::Default (xgi::Input * in, xgi::Output * out) 
{
	this->TabPanel(out);
}

void sentinel::probe::Application::TabPanel (xgi::Output * out)
{

	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Exceptions\">" << std::endl;
	this->ExceptionsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void sentinel::probe::Application::ExceptionsTabPage (xgi::Output * out)
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	*out << cgicc::label("Last update: ");
	*out << cgicc::label(toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));
	*out << cgicc::br();

	*out << cgicc::table().set("class","xdaq-table").set("style", "width: 100%;");
	*out << cgicc::caption("Raised Exceptions");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Time").set("style", "vertical-align: top; font-weight: bold;");
	*out << cgicc::th("Exception").set("style", "vertical-align: top; font-weight: bold;");
	*out << cgicc::th("Severity").set("style", "vertical-align: top; font-weight: bold;");
	*out << cgicc::th("Occurrences").set("style", "vertical-align: top; font-weight: bold;");
	*out << cgicc::th("Notifier").set("style", "vertical-align: top; font-weight: bold;");
	*out << cgicc::th("Message").set("style", "vertical-align: top; font-weight: bold;");
	*out << cgicc::th("Details").set("style", "vertical-align: top; font-weight: bold;");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	std::list<std::pair<xcept::Exception, size_t> >::iterator i;
	for (i = exceptions_.begin(); i != exceptions_.end(); i++)
	{
		std::string time = (*i).first.getProperty("dateTime");
		std::string type = (*i).first.name();
		std::string severity = (*i).first.getProperty("severity");
		std::stringstream occurrences;
		occurrences << (*i).second;
		std::string notifier = (*i).first.getProperty("notifier");
		std::string message = (*i).first.message();
		std::string id = (*i).first.getProperty("uniqueid");
		;
		std::string viewlink = "/" + this->getApplicationDescriptor()->getURN() + "/view?uniqueid=" + id;

		*out << cgicc::tr();
		*out << cgicc::td(time) << std::endl;
		*out << cgicc::td(type) << std::endl;
		*out << cgicc::td(severity) << std::endl;
		*out << cgicc::td(occurrences.str()) << std::endl;

		*out << cgicc::td(notifier) << std::endl;
		*out << cgicc::td(message) << std::endl;
		*out << cgicc::td();

		std::stringstream clickHandler;
		clickHandler << "window.open('" << viewlink << "', 'Sentinel Probe - Exception Viewer', 'toolbar=no, scrollbars=yes, resizable=yes, width=900, height=900'); return false;";

		*out << cgicc::a().set("href", viewlink).set("onClick", clickHandler.str());
		*out << cgicc::img().set("src", "/sentinel/images/alert.gif").set("height", "16").set("width", "16").set("border", "0");
		*out << cgicc::a();
		*out << cgicc::td();
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();

	repositoryLock_.give();
}

void sentinel::probe::Application::StatisticsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Network Status");
	*out << cgicc::tbody() << std::endl;

	// sentineld URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Sentineld address";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << sentineldURL_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Exceptions sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total outgoing reports";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingCounter_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Exceptions lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingLossCounter_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

//
// XGI Support
//
void sentinel::probe::Application::view (xgi::Input * in, xgi::Output * out) 
{
	try
	{
		cgicc::Cgicc cgi(in);

		std::string id = cgi["uniqueid"]->getValue();

		repositoryLock_.take();

		std::list<std::pair<xcept::Exception, size_t> >::iterator i;
		for (i = exceptions_.begin(); i != exceptions_.end(); i++)
		{
			if ((*i).first.getProperty("uniqueid") == id)
			{
				*out << xcept::htmlformat_exception_history((*i).first) << std::endl;
				break;
			}
		}
		if (i == exceptions_.end())
		{
			repositoryLock_.give();
			std::stringstream msg;
			msg << "Requested non existing exception identified by '" << id << "'";
			XCEPT_RAISE(xgi::exception::Exception, msg.str());
		}

		repositoryLock_.give();
	}
	catch (const std::exception & e)
	{
		repositoryLock_.give();
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}

