// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/
#include "PeerTransportAgent.h"
#include "Listener.h"

#include "pt/Listener.h"
#include "log4cplus/logger.h"

#include "HTTPSender.h"

using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Value;
//using v8::Arguments;
using v8::Handle;
using v8::HandleScope;


//using namespace v8;

Persistent<Function> nodejs::PeerTransportAgent::constructor;


/*

	 // callback
	 Isolate * isolate = Isolate::GetCurrent();
	 const unsigned argc = 1;
	  Local<Value> argv[argc] = { String::NewFromUtf8(isolate, "ciao sono io") };
	  //percy->Call(Null(isolate), argc, argv);
	  Local<Function>::New(isolate, percy)->Call(isolate->GetCurrentContext()->Global(), argc, argv);
*/

nodejs::PeerTransportAgent::PeerTransportAgent()
{
	 pta_ = pt::getPeerTransportAgent();
}

nodejs::PeerTransportAgent::~PeerTransportAgent() {
}

void nodejs::PeerTransportAgent::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "PeerTransportAgent"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
  NODE_SET_PROTOTYPE_METHOD(tpl, "getListener", getListener);
  NODE_SET_PROTOTYPE_METHOD(tpl, "addSenderSupport", addSenderSupport);

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "PeerTransportAgent"), tpl->GetFunction());
}

void nodejs::PeerTransportAgent::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  if (args.IsConstructCall()) {
    // Invoked as constructor: `new PeerTransportAgent(...)`
    //double value = args[0]->IsUndefined() ? 0 : args[0]->NumberValue();
    PeerTransportAgent* obj = new PeerTransportAgent();
    //callback
    //Isolate* isolate = args.GetIsolate();
    // Local<Function> cb = Local<Function>::Cast(args[0]);
   //  obj->percy_.Reset(isolate, cb);
    //
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    // Invoked as plain function `PeerTransportAgent(...)`, turn into construct call.
    const int argc = 1;
    Local<Value> argv[argc] = { args[0] };
    Local<Context> context = isolate->GetCurrentContext();
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    Local<Object> result =  cons->NewInstance(context, argc, argv).ToLocalChecked();
    args.GetReturnValue().Set(result);
  }
}

void nodejs::PeerTransportAgent::NewInstance(const FunctionCallbackInfo<Value>& args) {

 Isolate* isolate = args.GetIsolate();

  const unsigned argc = 1;
  Local<Value> argv[argc] = { args[0] };
  Local<Function> cons = Local<Function>::New(isolate, constructor);
  Local<Context> context = isolate->GetCurrentContext();
  Local<Object> instance = cons->NewInstance(context, argc, argv).ToLocalChecked();

  args.GetReturnValue().Set(instance);

}

void nodejs::PeerTransportAgent::getListener(const FunctionCallbackInfo<Value>& args) {

	  std::string service(*v8::String::Utf8Value(args[0]->ToString()));
	 // std::cout << " ***************** nodejs::PeerTransportAgent::getListener -> " <<  service << std::endl;
	 nodejs::PeerTransportAgent* object = ObjectWrap::Unwrap<nodejs::PeerTransportAgent>(args.Holder());

	 pt::Listener* l = object->pta_->getListener(service);

	 std::cout << "GOT Listener for " << l->getService() << std::endl;
	 // callback
	//	 Isolate * isolate = Isolate::GetCurrent();
	//	 const unsigned argc = 1;
	//	  Local<Value> argv[argc] = { String::NewFromUtf8(isolate, "ciao sono io") };
		  //percy->Call(Null(isolate), argc, argv);
	//	 Local<Function>::New(isolate, object->percy_)->Call(isolate->GetCurrentContext()->Global(), argc, argv);


	 nodejs::Listener::NewInstance(args,l);


}

void nodejs::PeerTransportAgent::addSenderSupport(const FunctionCallbackInfo<Value>& args)
{
	Isolate* isolate = args.GetIsolate();

	std::string service(*v8::String::Utf8Value(args[0]->ToString()));
	Local<Function> callback = Local<Function>::Cast(args[1]);

	std::cout << " Add " << service << " sender support" << std::endl;
	nodejs::PeerTransportAgent* object = ObjectWrap::Unwrap<nodejs::PeerTransportAgent>(args.Holder());
	Logger rootLogger = Logger::getRoot();

	nodejs::HTTPSender * hs = new nodejs::HTTPSender( rootLogger);

	hs->callback_.Reset(isolate, callback);

	object->pta_->addPeerTransport(hs);


	std::cout << "done adding sender support " << hs->getProtocol() << std::endl;


}







	// std::string service(*v8::String::Utf8Value(args[0]->ToString()));
//



