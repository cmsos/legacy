//
//  File Name:
//
//      FUInterface.h
//
//  File Type:
//
//      C++ Header file
//
//  Creative Authors:
//
//      Luciano B. ORSINI
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71615 
//      E-mail:Luciano.Orsini@cern.ch
//      
//      
//      Johannes Gutleber
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71536
//	E-mail:Johannes.Gutleber@cern.ch
//
//  Change History:
//
//      $Log: FUInterface.h,v $
//      Revision 1.2  1999/08/26 07:37:26  meschi
//      Reference: EM, LO
//      Software Change:
//      	Added FN interface modules. Added new definition of FragmentData
//      	and FragmentDataSet. Extended FUInterface according FN vs FU IRS.
//
//      Revision 1.1  1999/07/30 16:12:24  lorsini
//      Reference: LORSINI
//      Software Change:
//      	Source Created.
//

//
//
//
//  Version Number
//
//      @(#)$Id: FUInterface.h,v 1.2 1999/08/26 07:37:26 meschi Exp $
//
//

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// The following pre-processor directives ensure that spurious errors will not
// be generated in the event that this header file is included several times.
 
#ifndef __FUInterface_h__
#define __FUInterface_h__

//  ------------------------------------------------------------------------ 
//  ------------------------  Include Header Files  ------------------------
//  ------------------------------------------------------------------------
 
// Include the following system header files:
 

  
// Include the following vxRU  header files:
 
#include "DAQBinding.h" 
 
//  ------------------------------------------------------------------------ 
//  --------------------  Public Constant Declarations  -------------------- 
//  ------------------------------------------------------------------------ 
 

//  ------------------------------------------------------------------------ 
//  ----------------------  Public Class Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//


// 
//
//  Class:
//
//     FUI 
//
//  Superclasses:
//
//     NONE 
//
//
//  Class Arguments
//
//
//  Description:
//
//	The interface for the Filter Unit Input. From this file the
//	stubs  and skeletons have to be derived using an agreed protocol.
//	The FUI accepts requests for events matching a profile and 
//	sets of fragments belonging to an event from the FN. It retrieves
//	these fragments either through the EVM or the FUI and delivers
//	them as soon as they are complete to the FN. It acts as a cache
//	for the FN.
// 

	


class FUInterface
{

//
//  Member Functions:
//

//
//      This class defines the following public member functions:
//
//
public:
  

  //
  // The EVM can return an event identifier to the FUI. This is only necessary
  // if the FUI directly communicates with the RUO. Then it has to know for
  // which event it first requests data. In the case the data requests go
  // over the EVM, the event id is returned with the first implicit data set
  // originating from the RUO.
  //
  virtual void confirm (EventIdentifier* eid) =0;

  // 
  // The FNs request the FU to allocate an event. The EventProfile must be specified 
  // along with the Destination Node.
  //
  virtual void allocate(EventProfile* evp, DestinationCookie* dcookie) = 0;

  // 
  // The FNs request a given FragmentSet to the FU. The message must to specify the 
  // EventIdentifier for which the fragments have to be fetched and the DestinationCookie
  // The FU will issue a take in response to this message
  //
  virtual void collect(EventIdentifier* eid, FragmentSet* fs, DestinationCookie* dcookie) = 0;

  // 
  // The FN requests the FU to discard a given event previously allocated to it. 
  //
  virtual void discard(EventIdentifier* eid) = 0;

  //
  // A data message coming from a RUO. All information necessary to
  // process the data is contained in the class FragmentHeader. Its
  // members correspond directly to the parameters defined in the
  // TriDAS Event Data Flow Control ISR document.
  //
 virtual void cache (FragmentIdentifier* fid, EventIdentifier* eid,  DestinationCookie* dcookie, FragmentData* fdata) = 0;

};
  


//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// Terminate the #if ... #endif pre-processor control directive located at the
// start of this header file.
 
#endif /* ifndef  __FUI_h__ */

