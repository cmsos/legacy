#include "bril/webutils/WebUtils.h"

class yourApplication
 {

  yourConstructor() :  m_chart_vs_bx( 0 ), m_chart_vs_time( 0 )
   {
    xgi::framework::deferredbind( this, this, &Application::Default, "Default" ); //Default function for the webpage; "framework" provides xdaq decorations
    xgi::deferredbind( this, this, &Application::requestData, "requestData" ); //data callback, returns plain json, no "framework"
   }

  void someinitfunction()
   {

    m_bx_data["Beam 1"] = std::vector<float>( 3564 ); //how many points in your x axis
    m_bx_data["Beam 2"] = std::vector<float>( 3564 );

    m_timeseries_data["Beam 1"] = std::deque<float>();
    m_timeseries_data["Beam 2"] = std::deque<float>();
    //note that the x-axis size is dynamic. If you assign a different size, the plot will resize.
    //in particular, you could start with an empty timeseries, and only add data if/when it comes.

    //the complete URL of the data callback. This is the same for every plot in the application.
    std::string callback = getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN() + "/requestData";

    WebChart::property_map smap; //the keys need to correspond with the keys in the data ("Beam 1" and "Beam 2" here).
    smap["Beam 1"] = WebChart::series_properties( "#FF4040" ); //line color in HTML notation, you can also add a marker specification...
    smap["Beam 2"] = WebChart::series_properties( "#4040FF" );

    //below is just an example of some options that you may or may not want to use, most are optional
    m_chart_vs_bx = new WebChart( "chart0", //unique name
                                  callback,
                                  "type: 'column', zoomType: 'x', animation: false", //basic options, from http://api.highcharts.com/highcharts#chart. Don't assign 'renderTo', it is set internally
                                  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'FBCT' }, tooltip: { enable : false }", //other options
                                  smap );

    m_chart_vs_time = new WebChart( "chart1",
                                    callback,
                                    "type: 'line', zoomType: 'xy', animation: true",
                                    "plotOptions: { line: { animation: false } }, title: { text: 'A line chart' }, xAxis: { type: 'datetime' }", //the datetime is necessary to correctly format timestamps
                                    smap ); //re-using the same smap gives the same names, colors, markers. In general, you'd need a different one
   }

  void Default( xgi::Input* in, xgi::Output* out )
   {
    WebChart::pageHeader( out ); ///you need this at the top
    *out << "<h1>This Chart</h1><br/>";//you can mix your own html, the chart will be created in a div where you call write()
    m_chart_vs_bx->writeChart( out ); //default is 400px height, full width
    *out << "<div/>" << "<h1>This other Chart</h1><br/>";//you can mix your own html, the chart will be created in a div where you call write()
    m_chart_vs_time->writeChart( out, "height:300px; width:100%;" );//this is what gets written to the style attribute of the div. You can set a different size and width.
   }

  void requestData( xgi::Input* in, xgi::Output* out )
   {
    Cgicc cgi( in ); // just do this
    WebChart::dataHeader( out ); // and this
    //then call this function for each plot you have.
    m_chart_vs_bx->writeDataForQuery( cgi, out, m_bx_data );
    m_chart_vs_time->writeDataForQuery( cgi, out, m_timeseries_data );
   }

  void thefunctionthatcomputesthenewdata()
   {
    m_bx_data["Beam 1"] = ... //assign a vector, copy from an array of whatever
    //same for the other series
    m_chart_vs_bx->newDataReady();

    //special for a timeseries
    m_timeseries_data["Beam 1"].push_back( std::make_pair( MILLISECONDS_SINCE_EPOCH, YOUR_VALUE ) ); //add the latest value to the "left"
    if ( m_timeseries_data["Beam 1"].size() > THE_MAX_AMOUNT_OF_HISTORY_POINTS_YOU_WANT_TO_SAVE )
      m_timeseries_data["Beam 1"].pop_front(); //delete the oldest value from the "right"
    //same for the other series
    m_chart_vs_time->newDataReady();
   }

  private:
  WebChart* m_chart_vs_bx;
  WebChart* m_chart_vs_time;
  std::map<std::string, std::vector<float>> m_bx_data;
  std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_timeseries_data; in case its a time plot

 };

