// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _nodejs_Facade_h_
#define _nodejs_Facade_h_

#include "xdaq/WebApplication.h"
//#include "xgi/Utils.h"
#include "xgi/Method.h"

//#include "cgicc/CgiDefs.h"
//#include "cgicc/Cgicc.h"
//#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

namespace nodejs
{
class Facade: public xdaq::Application 
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	Facade(xdaq::ApplicationStub * s) throw (xdaq::exception::Exception);
	
	void Default(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception);
};
}

#endif
