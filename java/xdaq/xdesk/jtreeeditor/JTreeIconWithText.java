package xdaq.xdesk.jtreeeditor;

import javax.swing.ImageIcon;

public class JTreeIconWithText {
  private ImageIcon icon = null;
  private String text = "";

  /**Constructor*/
  public JTreeIconWithText(ImageIcon n_icon, String n_text) {
    icon = n_icon;
    text = n_text;
  }

//******************************************************************************
   public ImageIcon getIcon() {
     return icon;
   }

//******************************************************************************
   public String toString() {
     return text;
   }
}
