package xdaq.tools.daqlet;

import java.awt.event.ActionEvent;

public class DaqletEvent extends ActionEvent
{
	public DaqletEvent (Object target, int id, String command)
	{
		super (target, id, command);
	}
}
