###############################################################################

# The addresses of the boards/applications to use in this test.
CPM = "cpm"
ICIS = ['ici-hcal1', 'ici-hcal2', 'ici-hcal3', 'ici-ecal1', 'ici-ecal2', 'ici-gem']
APVES = []
PIS = ['pi-hcal1', 'pi-hcal2', 'pi-hcal3', 'pi-ecal1', 'pi-ecal2', 'pi-gem']

# The FED id of the CPM, so we can disable the readout.
CPM_FED_ID = 4095

###############################################################################
