/***************************************************************************** 
 * File  rhu_int_shmdata_fwd.hpp
 * created on 13.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/

#ifndef RHU_INT_SHMDATA_FWD_HPP_
#define RHU_INT_SHMDATA_FWD_HPP_

#include <iostream>
//#include "rhu/rhutypes.hpp"
#include "rhutypes.hpp"
//#include "tools/types.h"
#include "types.h"
//#include "tools/bits.hpp"
#include "bits.hpp"
#include <stdexcept>

namespace rhu {

  namespace shm {

    namespace impl {

      struct EventEntry {
	//enum { HISTOGRAM, POSTMORTEM } type;
	short histogramIndex;
	short postmortemIndex;
	EventEntry()
	  : histogramIndex(-1), postmortemIndex(-1)
	{
	}
	// returns true, if the structure contains real event data
	bool isValid()
	{
	  return (histogramIndex>=0) || (postmortemIndex>=0);
	}
      };

      typedef struct EventEntry EventEntry_t;

      // printer function
      inline static std::ostream& operator<<(std::ostream& out, struct EventEntry& entry)
      {
	out << "Event(hist: " << entry.histogramIndex << ":" << entry.postmortemIndex << ")" ;
	return out;
      }

      // Device Config Structure

      //struct DeviceConfig {
      //DeviceMode  mode;
      //u16_t channelEnableMask;
      //
      //void setChannelEnable(int index, bool enable)
      //{
      //if (index<0) throw std::range_error("channel index < 0");
      //if (index>9) throw std::range_error("channel index > 9");
      //channelEnableMask=tools::setBit(channelEnableMask, index, enable);
      //}
      //// returns true, if given channel is enabled
      //// Index must be >=0 <10 or an exception is thrown
      //// virtual
      //bool isChannelEnabled(int index) const
      //{
      //if (index<0) throw std::range_error("channel index < 0");
      //if (index>9) throw std::range_error("channel index > 9");
      //return tools::getBit(channelEnableMask, index);
      //}
      //
      //// Features
      //bool samplingModeEdge;
      //bool simulatorEnable;
      //bool postmortemRestart;
      //
      //int orbitTriggerDelay;
      //intbunchTriggerDelay;
      //intbeamAbortOrbits;
      //intbeamAbortTimeout;
      //inthistogramOrbitsThreshold;
      //};
      //
      //typedef struct DeviceConfig DeviceConfig_t;
      //
      //// Device Status Structure
      //struct DeviceStatus {
      //unsigned short firmwareRevision;
      //unsigned short statusRegister;
      //unsigned char  temperature;
      //
      //// *** Statistic
      //int histogramOverflowCount;// number of histogram where an overflow has been detected
      //int irqCount;// number of total irq
      //
      //// *** Status
      //u64_t actualOrbitCounter;
      //
      //boolorbitTriggerAvailable;
      //boolbunchClockLocked;
      //
      //// Non Device Specific Status
      //bool isOnline;// true, if a device is connected to the shared memory
      //};
      //
      //typedef struct DeviceStatus DeviceStatus_t;

    } // namespace impl {

    // Structure Visible to Client
    typedef impl::EventEntry_t EventEntry;

    //// Structure Visible to Client
    //typedef impl::DeviceConfig_t DeviceConfig;
    //
    //// Structure Visible to Client
    //typedef impl::DeviceStatus_t DeviceStatus;

  } // namespace shm {

} // namespace rhu {

#endif /* RHU_INT_SHMDATA_FWD_HPP_ */
