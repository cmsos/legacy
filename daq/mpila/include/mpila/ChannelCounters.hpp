/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelCounters.hpp
 *
 *  Created on: Mar 22, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include <string>
#include <sstream>
#include <mutex>
#include <atomic>

#include "mpila/WorkRequest.hpp"

namespace mpila
{

class ChannelCounters
{
   public:
    ChannelCounters(WorkRequestType workRequestType);

    void postedRequest();
    void completedRequest();

    std::string getStats();

    volatile std::atomic<size_t> idleWaitCycleCounter_;
    volatile std::atomic<size_t> idlePostCounter_;
    volatile std::atomic<size_t> starvingCycleCounter_;
    volatile std::atomic<size_t> postCounter_;
    volatile std::atomic<size_t> completionCounter_;
    volatile std::atomic<size_t> openRequestCounter_;
    std::string type_;
};

} /* namespace mpila */
