// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/api/admin/GetNestedViewConfiguration.h"
#include "tstore/api/NS.h"
#include "xoap/MessageFactory.h"
#include "toolbox/net/URN.h"

tstore::api::admin::GetNestedViewConfiguration::GetNestedViewConfiguration( const std::string & path, const std::string & view):
	path_(path),view_ (view)
{

	aheaders_.setAction(tstore::api::NamespaceUri + "/getConfiguration");	
}

tstore::api::admin::GetNestedViewConfiguration::~GetNestedViewConfiguration()
{
}
	
tstore::api::admin::GetNestedViewConfiguration::GetNestedViewConfiguration(xoap::MessageReference& msg) 
	
{
	
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName getConfigName("getConfiguration", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(getConfigName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::admin::exception::Exception, "Invalid message: missing or bad <getConfiguration/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName id = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName path = envelope.createName("path", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	toolbox::net::URN urn(bodyElements[0].getAttributeValue(id));
	view_ = urn.getNSS();
	path_ = bodyElements[0].getAttributeValue(path);
	
}

std::string tstore::api::admin::GetNestedViewConfiguration::getView()
{
	return view_;
}

std::string tstore::api::admin::GetNestedViewConfiguration::getPath()
{
	return path_;
}


ws::addressing::Headers& tstore::api::admin::GetNestedViewConfiguration::getAddressingHeaders()
{
	return aheaders_;
}
		

xoap::MessageReference tstore::api::admin::GetNestedViewConfiguration::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("getConfiguration",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	xoap::SOAPName id = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(id, tstore::api::nested::NamespaceUri + ":" + view_);
	xoap::SOAPName path = envelope.createName("path", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(path, path_);
	
	return request;
}
	
