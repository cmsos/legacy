package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class UnsignedInteger32 extends SimpleItem {
	
	private java.lang.Long value;
	
	@Override
	public java.lang.Long getValue() {
		return value;
	}
	
	@Override
	public void setValue(Object value) {
		this.value = ((Number)value).longValue() & 0xffffffffL;
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		out.writeInt(value.intValue());
	}

	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = (long)in.readInt() & 0xffffffffL;
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.UNSIGNED_INTEGER32.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
