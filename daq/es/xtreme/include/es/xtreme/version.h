// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, P. Roberts, D. Simelevicius          *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _es_xtreme_version_h_
#define _es_xtreme_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define ESXTREME_VERSION_MAJOR 4
#define ESXTREME_VERSION_MINOR 5
#define ESXTREME_VERSION_PATCH 1
// If any previous versions available E.g. #define ESXTREME_PREVIOUS_VERSIONS ""
#define ESXTREME_PREVIOUS_VERSIONS "3.0.0,3.0.1,3.0.2,3.1.0,3.2.0,3.2.1,3.3.0,4.0.0,4.1.0,4.2.0,4.2.1,4.3.0,4.4.0,4.5.0"


//
// Template macros
//
#define ESXTREME_VERSION_CODE PACKAGE_VERSION_CODE(ESXTREME_VERSION_MAJOR,ESXTREME_VERSION_MINOR,ESXTREME_VERSION_PATCH)
#ifndef ESXTREME_PREVIOUS_VERSIONS
#define ESXTREME_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(ESXTREME_VERSION_MAJOR,ESXTREME_VERSION_MINOR,ESXTREME_VERSION_PATCH)
#else 
#define ESXTREME_FULL_VERSION_LIST  ESXTREME_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(ESXTREME_VERSION_MAJOR,ESXTREME_VERSION_MINOR,ESXTREME_VERSION_PATCH)
#endif 

namespace esxtreme
{
	const std::string package  =  "esxtreme";
	const std::string versions =  ESXTREME_FULL_VERSION_LIST;
	const std::string description = "XDAQ plugin for enabling monitoring on Elasticsearch";
	const std::string authors = "Luciano Orsini, Penelope Roberts";
	const std::string summary = "Elasticsearch XDAQ streamer";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
