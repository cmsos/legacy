# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2016, CERN.			                #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius				#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=iaas-$(ZONE_NAME)-service-$(SERVICE_NAME)
Project=$(PROJECT_NAME)
Package=iaas/$(ZONE_NAME)/service/$(SERVICE_NAME)

Summary=IAAS configuration for $(ZONE_NAME) service $(SERVICE_NAME)

Description=This package provides IAAS configuration for service $(SERVICE_NAME) on zone $(ZONE_NAME)

Link=http://xdaq.web.cern.ch
#
# Template instantiate value
#
TEMPLATEDIR=$(BUILD_HOME)/$(PROJECT_NAME)/iaas/template
#IAAS_SERVICE_HOST=$(shell cpp -P -I$(BUILD_HOME)/$(Project)/$(Package)/../ -I$(TEMPLATEDIR)/ -DIAAS_MACRO_ZONE=$(ZONE_NAME) $(TEMPLATEDIR)/service/defaultvar.macros | grep IAAS_MACRO_SERVICE_HOST | awk -F' ' '{print $$2}')

_all: all

default: all

all: clean
	cp $(TEMPLATEDIR)/spec.template.$(SERVICE_NAME) spec.template 
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template

_cleanall: clean

clean:
	-rm -f spec.template 

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
