#ifndef _bril_lumistore_exception_Exception_h_
#define _bril_lumistore_exception_Exception_h_
#include "xcept/Exception.h"
namespace bril { 
  namespace lumistore {
    namespace exception { 
      class Exception: public xcept::Exception{
      public: 
      Exception( std::string name, std::string message, std::string module, int line, std::string function ):xcept::Exception(name, message, module, line, function) {} 
      Exception( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e ):xcept::Exception(name, message, module, line, function, e) {} 
      }; 
    }//ns exception
  }//ns lumistore

   XCEPT_DEFINE_EXCEPTION(lumistore,Application)

   XCEPT_DEFINE_EXCEPTION(lumistore,DiskWriter)

   XCEPT_DEFINE_EXCEPTION(lumistore,DataSourceError)

   XCEPT_DEFINE_EXCEPTION(lumistore,StreamError)

   XCEPT_DEFINE_EXCEPTION(lumistore,DataVersionError)
  
}//ns bril


//XCEPT_DEFINE_EXCEPTION(,)
#endif
