#ifndef _tcds_utils_SOAPCmdInitCyclicGenerator_h_
#define _tcds_utils_SOAPCmdInitCyclicGenerator_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdInitCyclicGenerator : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdInitCyclicGenerator(T& controller);

        /**
         * (Re)Initializes a single cyclic generator (i.e., resets the
         * pre-/postscale counters, etc.).
         *
         * NOTE: To be used with care! For most use cases the desired
         * behaviour is achieved by calling InitCyclicGenerators
         * instead.
         *
         * - SOAP command parameters:
         *   -  xsd:unsignedInt `genNumber': the cyclic generator number (e.g., '1').
         * - SOAP command return value: none.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdInitCyclicGenerator.hxx"

#endif // _tcds_utils_SOAPCmdInitCyclicGenerator_h_
