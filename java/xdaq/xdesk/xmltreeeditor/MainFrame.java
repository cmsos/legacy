package xmltreeeditor;

import java.io.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;

import org.w3c.dom.*;

public class MainFrame
    extends JFrame {

  JMenuBar menubar = new JMenuBar();
  JMenu menu = null;
  JScrollPane panel = new JScrollPane();
  JLabel statusbar;
  XMLTree tree;
  File file;
  TreePath p = null;
//==============================================================================
  public MainFrame()

  {
    super("XMLTREE");
    initialize();
    this.addWindowFocusListener(new Win_Listener());
  }

  class Win_Listener
      implements WindowFocusListener {
    public void windowGainedFocus(WindowEvent e) {

      if (tree != null) {
        tree.startEditingAtPath(p);
        tree.repaint();
      }
    }

    public void windowLostFocus(WindowEvent e) {

      if (tree != null) {
        System.out.println(tree.getSelectionPath());
        // tree.getCellEditor().stopCellEditing();
        p = tree.getSelectionPath();
      }
    }
  }

//==============================================================================
  public void initialize() {

    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    getContentPane().setLayout(new BorderLayout());
    statusbar = new JLabel("File : none");
    statusbar.setFocusable(false);
    statusbar.setFont(new Font("Arial", Font.BOLD, 12));
    setBounds(0, 0, 700, 500);
    menu = new MenuPanel(this);
    menubar.add(menu);
    setJMenuBar(menubar);
    getContentPane().add(panel);
    getContentPane().add("South", statusbar);
    setVisible(true);
  }

//==============================================================================
  void createTree(String fileName) {
    try {
      XMLTreeModel m = new XMLTreeModel(new File(fileName));
      tree = new XMLTree(m);
      tree.addMouseListener(new PopupTrigger());
      XMLTreeCellRenderer treeCellRenderer = new XMLTreeCellRenderer();
      tree.setCellRenderer(treeCellRenderer);
      tree.setCellEditor( (TreeCellEditor) treeCellRenderer);
    }
    catch (Exception ex) {}
  }

//==============================================================================

  File getFile() {
    JFileChooser filechooser = new JFileChooser("C:\\");
    filechooser.setAcceptAllFileFilterUsed(false);
    filechooser.setFileFilter(new XMLFileFilter());
    filechooser.setMultiSelectionEnabled(false);
    int x = filechooser.showOpenDialog(MainFrame.this);
    return x != filechooser.CANCEL_OPTION ? filechooser.getSelectedFile() : null;
  }

//==============================================================================

////////////////////////////////////////////////////////////////////////////////

  class MenuPanel
      extends JMenu {

    JMenuItem j_item = null;
    MainFrame adaptee;
    public MenuPanel(MainFrame adaptee) {
      this.adaptee = adaptee;
      createMenu();
      setVisible(true);
    }

    void createMenu() {
      setText("File     ");
      j_item = new JMenuItem("New File...", new ImageIcon("OpenFile16.gif"));
      j_item.addActionListener(new ActionNew());
      add(j_item);

      j_item = new JMenuItem("Load File...", new ImageIcon("OpenFile16.gif"));
      j_item.addActionListener(new ActionOpen());
      add(j_item);
      j_item = new JMenuItem("Unload File...", new ImageIcon("xprIcon16.gif"));
      j_item.addActionListener(new ActionClose());
      add(j_item);
      j_item = new JMenuItem("Save Work...", new ImageIcon("Save16.gif"));
      j_item.addActionListener(new ActionSave());
      add(j_item);
      j_item = new JMenuItem("Save Work As...", new ImageIcon("SaveAs16.gif"));
      j_item.addActionListener(new ActionSaveAs(this));
      add(j_item);
      addSeparator();
      j_item = new JMenuItem("Exit", new ImageIcon("exit.gif"));
      j_item.addActionListener(new ActionExit());
      add(j_item);
    }

//------------------------------------------------------------------------------
    class ActionClose
        implements ActionListener {
      public void actionPerformed(ActionEvent e) {
        if (panel.getViewport().getComponents().length > 0) {
          file = null;
          tree = null;
          panel.getViewport().remove(0);
          statusbar.setText("File : none");

        }
      }
    }

//------------------------------------------------------------------------------
    class ActionOpen
        implements ActionListener {
      public void actionPerformed(ActionEvent e) {
        file = getFile();
        actionOpen_ActionPerformed();
      }

    }

    public void actionOpen_ActionPerformed() {

      if (file != null) {

        if (panel.getViewport().getComponents().length > 0) {
          panel.getViewport().remove(0);
        }
        statusbar.setText("File : " + file.getAbsolutePath());
        createTree(file.getPath());
        panel.getViewport().add(tree);
        panel.repaint();
      }

    }

//------------------------------------------------------------------------------
    class ActionExit
        implements ActionListener {
      public void actionPerformed(ActionEvent e) {
        System.exit(0);
      }
    }
  }

//------------------------------------------------------------------------------
  class ActionNew implements ActionListener
  {
    public void actionPerformed(ActionEvent e) {
        tree=new XMLTree();
        tree.addMouseListener(new PopupTrigger());
        XMLTreeCellRenderer treeCellRenderer = new XMLTreeCellRenderer();
        tree.setCellRenderer(treeCellRenderer);
        tree.setCellEditor( (TreeCellEditor) treeCellRenderer);
        tree.setRowHeight(25);
        panel.getViewport().add(tree);
        panel.repaint();
      }

  }


  class ActionSave
      implements ActionListener {
    Document doc;

    public void actionPerformed(ActionEvent e) {
      DocumentBuilderFactory builderFactory =
          DocumentBuilderFactory.newInstance();
      TransformerFactory tFactory =
          TransformerFactory.newInstance();
      try {
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        XMLTreeModel model = (XMLTreeModel) tree.getModel();
        doc = model.getDocument();
        doc.normalize();
        //doc.getDocumentElement().normalize();
        Transformer transformer = tFactory.newTransformer();

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(file);
        transformer.transform(source, result);
      }
      catch (Exception ex) {
        System.out.println(ex.getMessage());
      }

    }

  }

  class ActionSaveAs
      implements ActionListener {
    Document doc;
    MenuPanel adaptee;

    public ActionSaveAs(MenuPanel adaptee) {
      this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
      JFileChooser filechooser = new JFileChooser(file);
      filechooser.setSelectedFile(file);
      filechooser.setAcceptAllFileFilterUsed(false);
      filechooser.setMultiSelectionEnabled(false);
      int x = filechooser.showSaveDialog(MainFrame.this);
      File newFile = filechooser.getSelectedFile();
      if (newFile != null) {
        file = newFile;
        DocumentBuilderFactory builderFactory =
            DocumentBuilderFactory.newInstance();
        TransformerFactory tFactory =
            TransformerFactory.newInstance();
        try {
          DocumentBuilder builder = builderFactory.newDocumentBuilder();
          XMLTreeModel model = (XMLTreeModel) tree.getModel();
          System.out.println("model + " );
          doc = model.getDocument();
          //doc.normalize();
          System.out.println("doc + " + doc.getChildNodes());
          Transformer transformer = tFactory.newTransformer();

          DOMSource source = new DOMSource(doc);
          StreamResult result = new StreamResult(newFile);
          transformer.transform(source, result);
          adaptee.actionOpen_ActionPerformed();
        }
        catch (Exception ex) {
          System.out.println("Error " + ex.getMessage());
        }
      }
    }
  }

////////////////////////////////////////////////////////////////////////////////
  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    new MainFrame();
  }
}
