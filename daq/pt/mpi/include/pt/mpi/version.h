// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _ptmpi_version_h_
#define _ptmpi_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PTMPI_VERSION_MAJOR 1
#define PTMPI_VERSION_MINOR 2
#define PTMPI_VERSION_PATCH 0
// If any previous versions available E.g. #define PTMPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef PTMPI_PREVIOUS_VERSIONS


//
// Template macros
//
#define PTMPI_VERSION_CODE PACKAGE_VERSION_CODE(PTMPI_VERSION_MAJOR,PTMPI_VERSION_MINOR,PTMPI_VERSION_PATCH)
#ifndef PTMPI_PREVIOUS_VERSIONS
#define PTMPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PTMPI_VERSION_MAJOR,PTMPI_VERSION_MINOR,PTMPI_VERSION_PATCH)
#else 
#define PTMPI_FULL_VERSION_LIST  PTMPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PTMPI_VERSION_MAJOR,PTMPI_VERSION_MINOR,PTMPI_VERSION_PATCH)
#endif 
namespace ptmpi
{
    const std::string package  =  "ptmpi";
    const std::string versions =  PTMPI_FULL_VERSION_LIST;
    const std::string summary = "Peer Transport for MPI";
    const std::string description = " MPI peer transport";
    const std::string authors = "Michael Lettrich, Luciano Orsini";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

