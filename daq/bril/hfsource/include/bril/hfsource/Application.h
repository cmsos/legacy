#ifndef _bril_hfsource_Application_h_
#define _bril_hfsource_Application_h_
#include <string>
#include <list>
#include <map>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer.h"
#include "xdata/Table.h"
#include "xdata/Serializable.h"
#include "toolbox/squeue.h"
#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "b2in/nub/exception/Exception.h"
#include "hcal/uhtr/version.h"
#include "hcal/uhtr/uHTR.hh"
#include "interface/bril/BEAMTopics.hh"



#include "toolbox/BSem.h"
//#include <TFile.h>
//#include <TH1.h>
//#include <TSystem.h>
/**
   A xdaq application that when configured as simulation mode simulates data on bril timing signal; as real mode, it pushes data into readout queue and publish from queue to bril eventing.
   In real mode, it is forseen to use instance=0 for channel 1-20; instanceid=1 for channel 21-40
*/
const unsigned int nChannels = 4;
const unsigned int nFibers = 24;
const unsigned int nElements  = 256;
const unsigned int Nbytes_allocated = 12*1024*1024;
const bool compressLUT=true;

//forward declaration
namespace hcal{
  namespace uhtr{
    class uHTR;
  }
}

namespace toolbox{
  namespace task{
    class WorkLoop;
  }
}

namespace bril{
  namespace hfsource{
    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener {
  
    public: 
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception);
      // timer callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);
      
    private:
      // members
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;
      #ifdef x86_64_centos7
      const
      #endif
      xdaq::ApplicationDescriptor *m_appDescriptor;
      xdata::String m_classname;
      xdata::String m_appURN;
      xdata::UnsignedInteger m_instanceid;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;
      
      // configuration parameters
      xdata::String m_signalTopic;  //FIXME I don't think I need
      xdata::String m_bus;
      xdata::String m_topics; 
      std::set<std::string> m_topiclist;

      xdata::String histsToRead; 
      std::set<std::string> histNames;

      xdata::Integer orbits_per_nibble;         // lumi nibble size (integer number of orbits)
      xdata::Integer integration_period_nb;     // integration period in lumi nibbles
      xdata::Integer nibbles_per_section;       // lumi section size (integer number of lumi nibbles)
      xdata::Integer cms1_threshold;            // threshold for filling CMS1 occupancy histograms
      xdata::Integer cms2_threshold;            // threshold for filling CMS2 occupancy histograms
      xdata::Integer BXOffset;            // threshold for filling CMS2 occupancy histograms
      xdata::Integer OCBXOffset;            // threshold for filling CMS2 occupancy histograms
      xdata::Integer orbit_phase;               // bx phase shift in histograms
      //xdata::Integer polls_per_nibble;          // number of times each uHTR board is polled every nibble
      xdata::String  uhtrAddress;               // addresses of uHTR boards
      //xdata::Vector<xdata::Boolean>  uhtrMask;  // bit masks of disabled channels in fibers
      xdata::String  addressTable;              // path to file with name-to-address map of uHTR endpoints
      xdata::UnsignedInteger crate;             // crate number -> unsigned char datasourceid;
      xdata::UnsignedInteger board;             // board number -> unsigned char channelid;  
      xdata::String  luts_path;                 // path to file with LUTs (empty for 1-to-1 LUTs)
     
      bool local_flag;
      bool global_flag;

      bool forceReconfigure;
      unsigned int nReconfigure;

      // Values derived from constants
      int integration_period_orbits; // integration period in orbits
      float integration_period_secs; // integration period in seconds
      std::vector<bool> mask;

      unsigned int occ1Data[interface::bril::MAX_NBX]; 
      unsigned int occ2Data[interface::bril::MAX_NBX]; 
      unsigned int sumETData[interface::bril::MAX_NBX]; 
      unsigned int validData[interface::bril::MAX_NBX]; 

      // real mode      
      // outgoing queue
      typedef std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* > QueueStore;
      typedef std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* >::iterator QueueStoreIt;
      toolbox::task::WorkLoop* m_CollectAndPackageData;
    
      std::map<std::string,toolbox::squeue<toolbox::mem::Reference* >* > m_topicoutqueues;
      toolbox::task::WorkLoop* m_publishing;

      // monitoring       
      // Note: monitorables must be of xdata types, headers can be found in daq/xdata  
      xdata::InfoSpace *m_monInfoSpacePull;
      xdata::UnsignedInteger m_outqueueSize;
      std::list<std::string> m_monItemListPull; //monInfoSpacePull metric

      xdata::InfoSpace *m_monInfoSpacePush;
      xdata::Float m_avgValue;
      std::list<std::string> m_monItemListPush; //monInfoSpacePush metric
      toolbox::BSem m_applock;
      std::map<std::string,bool> m_lumi_hAvailable;
      std::map<std::string,bool> m_lumi_hOverflow;
      
      std::string uhtrShortName;
      hcal::uhtr::uHTR* uHTR;

    private:

      // application configuration
      
      xdata::Integer m_norbit;
      //xdata::String m_crateid;
      //xdata::String m_thresholdstr;
      //xdata::String m_tdcmapstr;
      xdata::String m_firmwareversion;
      xdata::String m_histotype;
      //xdata::String m_lut;

      
      // current run status
      unsigned int m_currentfill;
      unsigned int m_currentrun;
      unsigned int m_currentls;
      unsigned int m_currentnibble;
      unsigned int m_timestamp;
      //
      // monitorables      
      //

      std::map<std::string,xdata::Serializable*> m_monConfig; //cached config map
      std::map<std::string,xdata::Serializable*> m_monRunStatus; //cached current runstatus
      xdata::Table m_montable;      

      void defineMonTable();
      void initializeMonTable(size_t nchannels);

      unsigned short hfCurrStatus;
      unsigned short hfPrevStatus;
      unsigned int hfPrevStatusTime;

      unsigned int lastPublish;
      unsigned int lastConfigure;
      bool configuring;
      toolbox::TimeVal timeNow;
      std::stringstream debugStatement;

    private:
      // methods

      bool readlut(std::vector<uint16_t> lut[nFibers][nChannels]);

      toolbox::mem::Reference* do_generatedata(const std::string& topic, unsigned int fillnum, unsigned int runnum,unsigned int lsnum,unsigned int nbnum, unsigned int ichannel);
      void do_publish(const std::string& busname,const std::string& topicname,toolbox::mem::Reference* bufRef);
      // workloop worker methods
      bool publishing(toolbox::task::WorkLoop* wl);
      bool CollectAndPackageData(toolbox::task::WorkLoop* wl);
      bool SanityChecks();
      bool Reconfigure();

      void Connect();
      void Disconnect();
      bool CheckFirmware();
      
      int  lumi_programlut();
      bool check_configure_uhtr();
      void HFReadoutStatus();

      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif
