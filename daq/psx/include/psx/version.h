// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_version_h_
#define _psx_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PSX_VERSION_MAJOR 3
#define PSX_VERSION_MINOR 2
#define PSX_VERSION_PATCH 0
// If any previous versions available E.g. #define PSX_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef PSX_PREVIOUS_VERSIONS


//
// Template macros
//
#define PSX_VERSION_CODE PACKAGE_VERSION_CODE(PSX_VERSION_MAJOR,PSX_VERSION_MINOR,PSX_VERSION_PATCH)
#ifndef PSX_PREVIOUS_VERSIONS
#define PSX_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PSX_VERSION_MAJOR,PSX_VERSION_MINOR,PSX_VERSION_PATCH)
#else 
#define PSX_FULL_VERSION_LIST  PSX_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PSX_VERSION_MAJOR,PSX_VERSION_MINOR,PSX_VERSION_PATCH)
#endif 
namespace psx
{
	const std::string package  =  "psx";
	const std::string versions =  PSX_FULL_VERSION_LIST;
	const std::string summary = " The PSX (PVSS SOAP eXchange) package";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/PSX";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
