#include "tcds/ici/ICICountersInfoSpaceHandler.h"

// #include <vector>
// #include <sstream>
// #include <stdint.h>
#include <string>

#include "toolbox/string.h"

// #include "tcds/ici/Definitions.h"
// #include "tcds/ici/HwICI.h"
// #include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::ici::ICICountersInfoSpaceHandler::ICICountersInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                    tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-ici-counters", updater)
{
  // Trigger counter.
  createUInt32("main.event_counter");

  // Orbit counter.
  createUInt32("main.orbit_counter");

  // All the individual B-go counters.
  for (int bgoNum = tcds::definitions::kBgoNumMin;
       bgoNum <= tcds::definitions::kBgoNumMax;
       ++bgoNum)
    {
      std::string const nameBase =
        toolbox::toString("bchannels.bchannel%d.status", bgoNum);
      createUInt32(nameBase + ".bgo_counter");
      createUInt32(nameBase + ".bgo_when_busy_counter");
      createUInt32(nameBase + ".cancel_counter");
    }
}

void
tcds::ici::ICICountersInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Trigger counter.
  monitor.newItemSet("itemset-trigger-counter");
  monitor.addItem("itemset-trigger-counter",
                  "main.event_counter",
                  "# L1As",
                  this,
                  "The number of L1As received after the last 'Zeroing' transition. "
                  "NOTE: This L1A count should equal the current run's L1A count.");

  // Orbit counter.
  monitor.newItemSet("itemset-orbit-counter");
  monitor.addItem("itemset-orbit-counter",
                  "main.orbit_counter",
                  "Orbit counter",
                  this,
                  "NOTE: The orbit counter is reset only by the OC0 B-go, "
                  "and not in the ICIController 'Zeroing' transition. "
                  "This means that in a run the orbit counter "
                  "will be slightly lower than the BC0 count. "
                  "(The BC0 counter is reset only in the 'Zeroing' transition.)");

  // All the individual B-go counters.
  for (int bgoNum = tcds::definitions::kBgoNumMin;
       bgoNum <= tcds::definitions::kBgoNumMax;
       ++bgoNum)
    {
      std::string const itemSetName =
        toolbox::toString("itemset-bgo%d-counters", bgoNum);
      monitor.newItemSet(itemSetName);
      std::string const nameBase =
        toolbox::toString("bchannels.bchannel%d.status", bgoNum);
      monitor.addItem(itemSetName,
                      nameBase + ".bgo_counter",
                      "# Requested",
                      this,
                      "The number of B-gos requested since the last 'Zeroing' transition.");
      monitor.addItem(itemSetName,
                      nameBase + ".bgo_when_busy_counter",
                      "# Requested while busy",
                      this);
      monitor.addItem(itemSetName,
                      nameBase + ".cancel_counter",
                      "# Cancelled",
                      this);
    }
}

void
tcds::ici::ICICountersInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Counters" : forceTabName;

  webServer.registerTab(tabName,
                        "All kinds of counters",
                        4);

  webServer.registerTable("Trigger counter",
                          "",
                          monitor,
                          "itemset-trigger-counter",
                          tabName,
                          4);

  webServer.registerTable("Orbit counter",
                          "",
                          monitor,
                          "itemset-orbit-counter",
                          tabName,
                          4);

  for (int bgoNum = tcds::definitions::kBgoNumMin;
       bgoNum <= tcds::definitions::kBgoNumMax;
       ++bgoNum)
    {
      std::string const itemSetName =
        toolbox::toString("itemset-bgo%d-counters", bgoNum);
      std::string bgoNameString =
        tcds::utils::formatBgoNameString(static_cast<tcds::definitions::BGO_NUM>(bgoNum));
      webServer.registerTable(toolbox::toString("%s counters", bgoNameString.c_str()),
                              "",
                              monitor,
                              itemSetName,
                              tabName);
    }
}
