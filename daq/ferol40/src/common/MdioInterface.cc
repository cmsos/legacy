#include "ferol40/MdioInterface.hh"
#include <sstream>
#include "d2s/utils/Exception.hh"
#include "ferol40/loggerMacros.h"

ferol40::MdioInterface::MdioInterface( HAL::PCIDevice *ferol40Device, 
                                     log4cplus::Logger logger )
    : mdioLock_( toolbox::BSem::FULL ),
      ferol40Device_P( ferol40Device ),
      logger_( logger )
{
}

void 
ferol40::MdioInterface::mdiolock()
{
    mdioLock_.take();
}

void 
ferol40::MdioInterface::mdiounlock()
{
    mdioLock_.give();
}

void ferol40::MdioInterface::mdioRead( uint32_t vitesseNo, uint32_t reg, uint32_t *value_ptr, uint32_t device )
{
    std::string linkStr;
    if (vitesseNo == 0)
        linkStr = "B0";
    else if( vitesseNo == 1)
        linkStr = "A0";
    else 
        {
            std::stringstream err;
            err  << "Illegal vitesseNo for MDIO operation (must be 0 or 1, but received: \"" << vitesseNo << "\").";
            XCEPT_DECLARE( utils::exception::Ferol40Exception, top, err.str() );
            throw(top);
        }
    std::string MDIO_ITEM = "MDIO_LINK10GB_" + linkStr;
    std::string MDIO_POLL = "MDIO_LINK10GB_POLL_" + linkStr;    
    uint32_t pollres;
    try
        {
            mdiolock();
            // See that the MDIO bus is free.
            ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            uint32_t cmd;

            // Set the MDIO address
            cmd = (reg & 0xffff) + 0x20000 + ((device & 0x1f) << 18);
            ferol40Device_P->write( MDIO_ITEM, cmd );
            // Wait until the address is set.
            ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

            // Issue the read command
            cmd = ((device & 0x1f) << 18) + 0x30000000;
            ferol40Device_P->write( MDIO_ITEM, cmd );
            // Wait until data is read by the 2 wire interface into the internal register.
            ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

            // read out the value
            ferol40Device_P->read( MDIO_ITEM, value_ptr );            
            //DEBUG( "mdioRead from " << MDIO_ITEM << " is " << std::hex << *value_ptr );
            mdiounlock();
        }
    catch( HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Timeout while while reading mdio interface ( mdioRead ). (poll result: " << pollres << " " << MDIO_POLL << " )";
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::Ferol40Exception, err.str(), e);
        }
    catch( HAL::HardwareAccessException &e )
        {
            std::stringstream err;
            err << "Problem accessing the MDIO hardware. " << e.what();
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::Ferol40Exception, err.str(), e);
        }
}

void ferol40::MdioInterface::mdioWrite( uint32_t vitesseNo, uint32_t reg, uint32_t value, uint32_t device  )
{
    std::string linkStr;
    if (vitesseNo == 0)
        linkStr = "B0";
    else if( vitesseNo == 1)
        linkStr = "A0";
    else 
        {
            std::stringstream err;
            err << "Illegal vitesseNo for MDIO operation (must be 0 or 1, but received: \"" << vitesseNo << "\").";
            XCEPT_DECLARE( utils::exception::Ferol40Exception, top, err.str() );
            throw(top);
        }
    std::string MDIO_ITEM = "MDIO_LINK10GB_" + linkStr;
    std::string MDIO_POLL = "MDIO_LINK10GB_POLL_" + linkStr;    
    uint32_t pollres;

    try
        {
            mdiolock();
            // See that the MDIO bus is free.
            //uint32_t tmp;
            //ferol40Device_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            uint32_t cmd;
            // Set the MDIO address
            cmd =(reg & 0xffff) + 0x20000 + ((device & 0x1f) << 18);
            //DEBUG("set address. write  " << MDIO_ITEM << " : " << std::hex << cmd);
            ferol40Device_P->write( MDIO_ITEM, cmd);
            // Wait until the address is set.
            //ferol40Device_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

            cmd = (0xffff & value) + 0x20000 + ((device & 0x1f) << 18) + 0x10000000;
            //DEBUG("Write command. write " << MDIO_ITEM << " : " << std::hex << cmd);
            ferol40Device_P->write( MDIO_ITEM, cmd);
            // Wait until the address is set.            
            //ferol40Device_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            //DEBUG( "MDIO write successfull" );
            mdiounlock();
        }
    catch( HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Timeout while writing to the MDIO interface. (poll result: " << pollres << " )";
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::Ferol40Exception, err.str(), e);
        }
    catch( HAL::HardwareAccessException &e )
        {
            std::stringstream err;
            err << "Problem accessing the MDIO hardware. " << e.what();
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::Ferol40Exception, err.str(), e);
        }
}

void ferol40::MdioInterface::mdioReadBlock( uint32_t vitesseNo, uint32_t reg, char data[], 
                                  uint32_t length, uint32_t device )
{
    std::string linkStr;
    if (vitesseNo == 0)
        linkStr = "B0";
    else if( vitesseNo == 1)
        linkStr = "A0";
    else 
        {
            std::stringstream err;
            err << "Illegal vitesseNo for MDIO operation (must be 0 or 1, but received: \"" << vitesseNo << "\").";
            XCEPT_DECLARE( utils::exception::Ferol40Exception, top, err.str() );
            throw(top);
        }
    std::string MDIO_ITEM = "MDIO_LINK10GB_" + linkStr;
    std::string MDIO_POLL = "MDIO_LINK10GB_POLL_" + linkStr;    
    uint32_t pollres;
    try
        {
            mdiolock();
            // See that the MDIO bus is free.
            //uint32_t tmp;
            //ferol40Device_P->read( MDIO_ITEM, &tmp );
            //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
            ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
            uint32_t cmd;

            for ( uint32_t ic = 0; ic < length; ic++ )
                {
                    // Set the address
                    cmd = ((reg + ic) & 0xffff)+ 0x20000 + ((device & 0x1f) << 18);
                    //DEBUG("set address. write  " << MDIO_ITEM << " : " << std::hex << cmd);
                    ferol40Device_P->write( MDIO_ITEM, cmd );
                    // Wait until the address is set.
                    //ferol40Device_P->read( MDIO_ITEM, &tmp );
                    //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
                    ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                   
                    // Issue the read command
                    cmd = ((device & 0x1f) << 18) + 0x30000000;
                    //DEBUG("Read command. write " << MDIO_ITEM << " : " << std::hex << cmd);
                    ferol40Device_P->write( MDIO_ITEM, cmd );
                    // Wait until data is read by the 2 wire interface into the internal register.
                    //ferol40Device_P->read( MDIO_ITEM, &tmp );
                    //DEBUG("Check if free. Initial read " << MDIO_ITEM << " : " <<  std::hex << tmp);
                    ferol40Device_P->pollItem( MDIO_POLL, 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );

                    // read out the value
                    uint32_t rv;
                    ferol40Device_P->read( MDIO_ITEM, &rv);
                    //DEBUG( "To get the result read " << MDIO_ITEM << " : " << std::hex << rv );
                    data[ic] = (char)(0xff & rv);
                }
            mdiounlock();
        }
    catch( HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Timeout while writing to the MDIO interface. (poll result: " << pollres << " )";
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::Ferol40Exception, err.str(), e);
        }
    catch( HAL::HardwareAccessException &e )
        {
            std::stringstream err;
            err << "Problem accessing the MDIO hardware. " << e.what();
            ERROR( err.str() );
            mdiounlock();
            XCEPT_RETHROW( utils::exception::Ferol40Exception, err.str(), e);
        }
}

