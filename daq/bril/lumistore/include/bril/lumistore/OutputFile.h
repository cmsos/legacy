#ifndef _bril_lumistore_OutputFile_h
#define _bril_lumistore_OutputFile_h
#include "bril/lumistore/OutputFileStatus.h"
#include <map>
#include <string>
namespace bril{
  namespace lumistore{
    class OutputFileConfig;
    class StorageUnit;
    class OutputFile{
    public:
      static std::string makeFilename(const std::string& filepath,const std::string& basename,const std::string& suffix, int filecount=0);
    public:
      OutputFile(const std::string& basename, const OutputFileConfig* fconfig,OutputFileStatus* fstatus);
      virtual ~OutputFile();
      void close(const OutputFileStatus::ClosingReason r);
      void write(const StorageUnit* u);
      bool tooOld(unsigned int nowsec) const;
      bool tooLarge(unsigned int datasize) const;
      size_t filesize() const;
    protected:
      virtual void do_writeStorageUnit(const StorageUnit* u) = 0;
      virtual void do_close() = 0;
      virtual size_t do_getfilesize() const = 0;
    protected:
      const OutputFileConfig* m_fconfig;
      OutputFileStatus* m_status;
    private:
      OutputFile(const OutputFile&); 
      OutputFile& operator=(const OutputFile&);
    };

  }}//ns
#endif
