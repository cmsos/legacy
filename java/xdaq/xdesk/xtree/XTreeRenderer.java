package xdaq.xdesk.xtree;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import java.util.*;
import org.w3c.dom.*;
import java.net.*;

public class XTreeRenderer extends DefaultTreeCellRenderer
{
	//HashSet filter_;

	public XTreeRenderer ()
	{
		//filter_ = new HashSet ();
	}
	
	/*public void addFilter (String nodeName)
	{
		filter_.add(nodeName);
	}
        */
	
	
	public Component getTreeCellRendererComponent (
		JTree tree,
		Object value,
		boolean sel,
		boolean expanded,
		boolean leaf,
		int row,
		boolean hasFocus )
	{
		super.getTreeCellRendererComponent (tree, value, sel, expanded, leaf, row, hasFocus);
			
		XTreeNode node = (XTreeNode)(((DefaultMutableTreeNode) value).getUserObject());
		
		String nodeName = node.domNode.getNodeName();
		
		String iconUrl = node.getAttribute ("iconUrl");
		
		// Check if node should be filtered out
		//if (filter_.contains (nodeName)) return this;
		
		
		if (nodeName.startsWith("Partition")) 
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/partition16x16.png");
			setIcon ( new ImageIcon ( url ));
			//this.setText ( node.getAttribute ("url"));
		}
		if (nodeName.startsWith("Host")) 
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/host.png");
			setIcon ( new ImageIcon ( url ));
			this.setText ( node.getAttribute ("url"));
		}
		if (nodeName.startsWith("Script")) 
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/Script.gif");
			setIcon ( new ImageIcon (url));
			this.setText ( node.getAttribute ("name"));
		}
		if (nodeName.startsWith("Transport"))
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/transport.gif");
			setIcon ( new ImageIcon (url));
			this.setText ( node.getAttribute ("class") + "(" +
					node.getAttribute ("instance") +  "," + node.getAttribute ("targetAddr")+ ")" );
		}
		if (nodeName.startsWith("Application"))
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/application16x16.png");
			setIcon ( new ImageIcon (url));
			this.setText ( node.getAttribute ("class") + "(" +
					node.getAttribute ("instance") +  "," + node.getAttribute ("targetAddr")+ ")" );
		}
		if (nodeName.startsWith("Parameter"))
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/Parameter.gif");
			setIcon ( new ImageIcon (url));
			this.setText ( node.getAttribute ("name") + "(" +
					node.domNode.getFirstChild().getNodeValue() + ")" );
		}
		if (nodeName.startsWith("ClassDef"))
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/ClassDef.gif");
			setIcon ( new ImageIcon (url));
		
			this.setText ( nodeName + ": " +
					node.domNode.getFirstChild().getNodeValue() + " (" +
					node.getAttribute ("id")+ ")" );
			
		}
		if (nodeName.startsWith("TransportDef"))
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/TransportDef.gif");
			setIcon ( new ImageIcon (url));
			
			this.setText ( nodeName + ": " +
					node.domNode.getFirstChild().getNodeValue() + " (" +
					node.getAttribute ("id")+ ")" );
					
			
		}
		if (nodeName.startsWith("urlApplication"))
		{	
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/urlApplication.gif");	
			setIcon ( new ImageIcon (url));
			this.setText ( node.domNode.getFirstChild().getNodeValue() );
		}
		if (nodeName.startsWith("urlTransport"))
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/urlApplication.gif");	
			setIcon ( new ImageIcon (url));
			this.setText ( node.domNode.getFirstChild().getNodeValue() );
		}
		if (nodeName.startsWith("Address"))
		{
			URL url = this.getClass().getResource("/xdaq/xdesk/xtree/icons/address.gif");		
			setIcon ( new ImageIcon (url));
			this.setText ( node.getAttribute ("type") );
		}
		// Override icon if provided by user
		//
		if ( !iconUrl.equals("") )
		{
			try {
				ImageIcon icon = new ImageIcon ( new URL ( iconUrl ) );
				setIcon ( icon );
			} catch ( MalformedURLException e )
			{
				System.out.println ("Cannot load icon: " + iconUrl);
			}
		}
		
		//tree.setRowHeight ( 50 );
		
		return this;	
	}
}
