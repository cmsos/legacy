// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tcpla_MemoryCache_h_
#define _tcpla_MemoryCache_h_

#include <string>
#include <sstream>
#include "toolbox/mem/Reference.h"
#include "tcpla/exception/Exception.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/BSem.h"
#include "toolbox/rlist.h"

namespace tcpla
{

	class MemoryCache
	{
		public:

			MemoryCache (const std::string & name, size_t frames, toolbox::mem::Pool * pool, size_t framesize) ;
			virtual ~MemoryCache ();

			toolbox::mem::Reference* waitFrame ();
			toolbox::mem::Reference* getFrame ();
			void grantFrame (toolbox::mem::Reference* frame) ;
			bool empty ();

		private:

			toolbox::rlist<toolbox::mem::Reference*>* cache_;

			toolbox::BSem mutex_;

			size_t frames_;
			size_t frameSize_;

			std::string name_;
	};

}

#endif
