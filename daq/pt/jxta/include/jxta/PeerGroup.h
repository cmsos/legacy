
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_PeerGroup_h
#define _jxta_PeerGroup_h

#include <string>
#include "jxta/DiscoveryService.h"
#include "jxta/RdvService.h"
#include "jxta/PipeService.h"

#include "toolbox/mem/CountingPtr.h"
#include "toolbox/mem/ThreadSafeReferenceCount.h"
#include "toolbox/mem/StandardObjectPolicy.h"

namespace jxta {

//! This class represents a Jxta Peer Group.
//! It provides all the services associated with a Jxta peer.
//! The Jxta NetPeerGroup is a singleton istance of this class.
class PeerGroup {
	public:
    typedef toolbox::mem::CountingPtr<PeerGroup, toolbox::mem::ThreadSafeReferenceCount, toolbox::mem::StandardObjectPolicy> Reference;

	virtual ~PeerGroup() {};
	
	//! Returns the local peer name.
	virtual std::string getPeerName() = 0;
	//! Returns the local peer ID as string.
	virtual std::string getPeerID() = 0;
	//! Returns the peer group name. 
	virtual std::string getPeerGroupName() = 0;
	//! Returns the peer group ID as string.
	virtual std::string getPeerGroupID() = 0;
	
	//! Returns the Discovery Service associated with the NetPeerGroup.
	virtual jxta::DiscoveryService* getDiscoveryService() = 0;
	//! Returns the Rendezvous Service associated with the NetPeerGroup.
	virtual jxta::RdvService* getRdvService() = 0;
	//! Returns the Pipe Service associated with the NetPeerGroup.
	virtual jxta::PipeService* getPipeService() = 0;
};


}

#endif
