// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_UnsignedInteger16_h_
#define _xdata_UnsignedInteger16_h_

#include <string>
#include <errno.h>
#include <stdint.h>
#include <limits>
#include "xdata/SimpleType.h"

namespace xdata {

template <class T>
class UnsignedInteger16Type: public xdata::SimpleType<T>
{
	public:
		//! Initialize with a number
		//
		UnsignedInteger16Type(T value);
		
		//! Uninitialized representing NaN
		//
		UnsignedInteger16Type();
		
		//! CTOR from string containing value
		//
		UnsignedInteger16Type (const std::string & value)
			throw (xdata::exception::Exception);

		virtual ~UnsignedInteger16Type();
		
		//! Prefix ++
		T& operator++();

		//! Prefix --
		T& operator--();

		//! Postfix ++
		T operator++(int);

		//! Postfix --
		T operator--(int);
		
		std::string type() const;

		std::string toString () const throw (xdata::exception::Exception);

		void  fromString (const std::string & value) 
			throw (xdata::exception::Exception);

};

typedef  UnsignedInteger16Type<uint16_t> UnsignedInteger16;
typedef  UnsignedInteger16Type<uint16_t &> UnsignedInteger16Ref;
typedef  uint16_t UnsignedInteger16T;

} // end namespace xdata

namespace std {
template<>
struct numeric_limits<xdata::UnsignedInteger16>
{
	static const bool is_specialized = true;

	static xdata::UnsignedInteger16 min() throw()
	{ 
		return xdata::UnsignedInteger16(std::numeric_limits<xdata::UnsignedInteger16T>::min());
	}

	static xdata::UnsignedInteger16 max() throw()
	{ 
		return xdata::UnsignedInteger16(std::numeric_limits<xdata::UnsignedInteger16T>::max());

	}

	static const int digits = std::numeric_limits<xdata::UnsignedInteger16T>::digits;
	static const int digits10 = std::numeric_limits<xdata::UnsignedInteger16T>::digits10;
	static const bool is_signed = false;
	static const bool is_integer = true;
	static const bool is_exact = true;
	static const int radix = 2;
	static xdata::UnsignedInteger16 epsilon() throw()
	{ 
		return xdata::UnsignedInteger16(0);
	}
	static xdata::UnsignedInteger16 round_error() throw()
	{ 
		return xdata::UnsignedInteger16(0);
	}

	static const int min_exponent = 0;
	static const int min_exponent10 = 0;
	static const int max_exponent = 0;
	static const int max_exponent10 = 0;

	static const bool has_infinity = true;
	static const bool has_quiet_NaN = true;
	static const bool has_signaling_NaN = false;
	static const float_denorm_style has_denorm = denorm_absent;
	static const bool has_denorm_loss = false;

	static xdata::UnsignedInteger16 infinity() throw()
	{ 
		xdata::UnsignedInteger16 i(0);
		i.limits_.set(xdata::Infinity);
		return i; 
	}
	static xdata::UnsignedInteger16 quiet_NaN() throw()
	{ 
		return xdata::UnsignedInteger16();
	}

	static xdata::UnsignedInteger16 signaling_NaN() throw()
	{ 
		return xdata::UnsignedInteger16(0);
	}

	static xdata::UnsignedInteger16 denorm_min() throw()
	{ 
		return xdata::UnsignedInteger16(0);
	}

	static const bool is_iec559 = false;
	static const bool is_bounded = true;
	static const bool is_modulo = true;

	static const bool traps = std::numeric_limits<xdata::UnsignedInteger16T>::traps;
	static const bool tinyness_before = false;
	static const float_round_style round_style = round_toward_zero;
};
} // end of namespace std

#include "xdata/UnsignedInteger16.i"


#endif
