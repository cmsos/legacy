#include "tcds/freqmon/FreqMonInfoSpaceHandler.h"

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::freqmon::FreqMonInfoSpaceHandler::FreqMonInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                                tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-freqmon-data", updater)
{
  createDouble("frequency", 0, "freq", tcds::utils::InfoSpaceItem::PROCESS);
}

tcds::freqmon::FreqMonInfoSpaceHandler::~FreqMonInfoSpaceHandler()
{
}

void
tcds::freqmon::FreqMonInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string const itemSetName = "itemset-freqmon-data";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "frequency",
                  "Clock frequency (Hz)",
                  this);
}

void
tcds::freqmon::FreqMonInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                      tcds::utils::Monitor& monitor,
                                                                      std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Frequency" : forceTabName;

  webServer.registerTab(tabName,
                        "Measured clock frequency",
                        1);
  webServer.registerTable("Measured frequency",
                          "Measured clock frequency",
                          monitor,
                          "itemset-freqmon-data",
                          tabName,
                          3);
}
