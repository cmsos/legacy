package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Boolean extends SimpleItem {
	
	private java.lang.Boolean value;
	
	@Override
	public java.lang.Boolean getValue() {
		return value;
	}
	
	@Override
	public void setValue(Object value) {
		if (value instanceof java.lang.String) {
			this.value = new java.lang.Boolean((java.lang.String)value);
		} else {
			this.value = (java.lang.Boolean)value;
		}
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		out.writeInt(value ? 1 : 0);
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = (in.readInt() == 1);
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.BOOLEAN.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:

