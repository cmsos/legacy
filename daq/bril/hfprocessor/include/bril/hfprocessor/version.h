// $Id$
#ifndef _bril_hfprocessor_version_h_
#define _bril_hfprocessor_version_h_
#include "config/PackageInfo.h"
#define BRILHFPROCESSOR_VERSION_MAJOR 1
#define BRILHFPROCESSOR_VERSION_MINOR 6
#define BRILHFPROCESSOR_VERSION_PATCH 1
#define BRILHFPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRILHFPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILHFPROCESSOR_VERSION_MAJOR,BRILHFPROCESSOR_VERSION_MINOR,BRILHFPROCESSOR_VERSION_PATCH)
#ifndef BRILHFPROCESSOR_PREVIOUS_VERSIONS
#define BRILHFPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILHFPROCESSOR_VERSION_MAJOR,BRILHFPROCESSOR_VERSION_MINOR,BRILHFPROCESSOR_VERSION_PATCH)
#else
#define BRILHFPROCESSOR_FULL_VERSION_LIST  BRILHFPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILHFPROCESSOR_VERSION_MAJOR,BRILHFPROCESSOR_VERSION_MINOR,BRILHFPROCESSOR_VERSION_PATCH)
#endif
namespace brilhfprocessor{
  const std::string package = "brilhfprocessor";
  const std::string versions = BRILHFPROCESSOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ hfprocessor";
  const std::string description = "collect and process hf histograms from hfsource";
  const std::string authors = "C.Palmer";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
