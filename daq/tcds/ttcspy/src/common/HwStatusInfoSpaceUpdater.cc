#include "tcds/ttcspy/HwStatusInfoSpaceUpdater.h"

#include "tcds/ttcspy/TCADeviceTTCSpy.h"

tcds::ttcspy::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                 tcds::ttcspy::TCADeviceTTCSpy const& hw) :
  tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA(xdaqApp, hw),
  hw_(hw)
{
}

tcds::ttcspy::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

tcds::ttcspy::TCADeviceTTCSpy const&
tcds::ttcspy::HwStatusInfoSpaceUpdater::getHw() const
{
  return hw_;
}
