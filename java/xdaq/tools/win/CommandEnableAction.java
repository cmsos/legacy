package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import xdaq.tools.win.xdaqWin;

public class CommandEnableAction extends AbstractAction
{
	xdaqWin win_;

	public CommandEnableAction (xdaqWin win)
	{
		super ("Enable");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {
			win_.executeCommandOnSelection ("Enable", null, win_.getPaths());
		}
		catch(xdaqWinException ex )
		{
		
		}

	}
}
