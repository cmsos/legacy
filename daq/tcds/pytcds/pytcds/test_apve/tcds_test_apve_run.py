#!/usr/bin/env python

###############################################################################
## Master script to configure several APVEs, iCIs and PIs to test the APVE.
###############################################################################

import sys

from pytcds.test_apve.tcds_test_apve_constants import APVES
from pytcds.test_apve.tcds_test_apve_constants import CPM
from pytcds.test_apve.tcds_test_apve_constants import FED_ID
from pytcds.test_apve.tcds_test_apve_constants import ICIS
from pytcds.test_apve.tcds_test_apve_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestAPVE(CmdLineBase):

    MODE_CHOICES = ["cpm", "lpm", "ici"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(TCDSConfigForTestAPVE, self).__init__(description, usage, epilog)
        self.mode = None
        # self.run_mode = "generator-driven"
        self.run_mode = "soap-driven"
        # End of __init__().

    def handle_args(self):
        # NOTE: This is a slight abuse of the CmdLineBase base class
        # since we don't require a target to be specified.

        # One arguments is expected: the choice of mode (cpm, lpm, or ici).
        if len(self.args) != 1:
            msg = "One arguments is required: " \
                  "the choice of Partition Manager mode. " \
                  "Options are: {0:s}."
            self.error(msg.format(", ".join(TCDSConfigForTestAPVE.MODE_CHOICES)))
        else:
            # Extract the mode.
            mode = self.args[0]
            if not mode in TCDSConfigForTestAPVE.MODE_CHOICES:
                msg = "'{0:s}' is not a valid mode. Options are: {1:s}."
                msg = msg.format(setup_type, ", ".join(TCDSConfigForTestAPVE.MODE_CHOICES))
                self.parser.error(msg)
            self.mode = mode
        # End of handle_args().

    def main(self):
        pm = None
        if self.mode == "lpm":
            pm = LPM
        elif self.mode == "cpm":
            pm = CPM

        sep_line = "-" * 70

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --setup-file=../utils/tcds_setup_tcdslab.json"
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --setup-file=../utils/tcds_setup_tcdslab.json"
        pi_force_ready_cmd = "../pi/tcds_pi_force_tts.py --setup-file={0:s}".format(self.setup_file_name)
        send_soap_cmd = "../utils/tcds_send_soap_command.py --setup-file=../utils/tcds_setup_tcdslab.json"
        # retri_disable_cmd = "../cpm/tcds_cpm_disable_retri.py"
        # backpressure_disable_cmd = "../cpm/tcds_cpm_disable_backpressure.py"
        read_spy_log_cmd = "../pi/tcds_pi_read_ttcspy_log.py --setup-file=../utils/tcds_setup_tcdslab.json"
        read_apve_history_cmd = "../apve/tcds_apve_read_pipeline_history.py --setup-file=../utils/tcds_setup_tcdslab.json"
        analyse_cmd = "./tcds_analyse_apve_vs_ttcspy.py"

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        targets = []
        if pm:
            targets = [pm]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure PM.
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Configure --fed-enable-mask='{2:d}&0%' --no-beam-active hw_cfg_{3:s}.txt".format(fsm_ctrl_cmd, pm, FED_ID, self.mode), ""))
            # Wait till the PM is in the Configured state.
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, pm), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            tmp = ""
            if pm:
                tmp = "_under_{0:s}".format(self.mode)
            cmds.append(("{0:s} {1:s} Configure hw_cfg_ici{2:s}.txt".format(fsm_ctrl_cmd, target, tmp), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_apve.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # Force PIs to be TTS READY.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} READY".format(pi_force_ready_cmd, target), ""))

        #----------

        # Enable PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Enabled state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable PM (as last!).
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling {0:s}'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, pm), ""))

        #----------

        # # Cyclic-generator mode.
        # if self.run_mode == "generator-driven":
        #     # Fire the cyclics.
        #     cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #                  ("python -c \"print 'Firing cyclics'\"", ""),
        #                  ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        #     targets = [pm]
        #     if self.mode == "ici":
        #         targets = list(ICIS)
        #     for target in targets:
        #         cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Stop".format(send_soap_cmd, target), ""))
        #         cmds.append(("{0:s} {1:s} InitCyclicGenerators".format(send_soap_cmd, target), ""))
        #         # cmds.append(("{0:s} {1:s} SendBgo bgoName:string:EC0".format(send_soap_cmd, target), ""))
        #         cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Start".format(send_soap_cmd, target), ""))

        #----------

        # NOTE: The generator-driven mode runs all by itself the
        # moment the Start sequence runs.

        # SOAP-driven mode.
        if self.run_mode == "soap-driven":
            # Run things by SOAP commands.
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Running things by SOAP commands'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            targets = [pm]
            if self.mode == "ici":
                targets = list(ICIS)
                # for target in targets:
                    # cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Stop".format(send_soap_cmd, target), ""))
                    # cmds.append(("{0:s} {1:s} SendBgo bgoName:string:Resync".format(send_soap_cmd, target), ""))
                    # cmds.append(("{0:s} {1:s} SendBgo bgoName:string:EC0".format(send_soap_cmd, target), ""))

            for i in xrange(1024):
                for target in targets:
                    cmds.append(("{0:s} {1:s} SendL1A".format(send_soap_cmd, target), ""))

        #----------

        # Wait a little.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Practicing patience...'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("python -c \"import time; time.sleep(5)\"", ""))

        #----------

        # Check the TTCSpy log in all PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Checking the TTCSpy log in all PIs'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            file_name = "{0:s}_spy_log.txt".format(target)
            cmds.append(("{0:s} {1:s} &> {2:s}".format(read_spy_log_cmd, target, file_name), ""))

        #----------

        # Check the pipeline history in all APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Checking the pipeline history in all APVEs'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            file_name = "{0:s}_pipeline_history.txt".format(target)
            cmds.append(("{0:s} {1:s} &> {2:s}".format(read_apve_history_cmd, target, file_name), ""))

        #----------

        # Check the pipeline history in the PM.
        if pm:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Checking the pipeline history in the PM'\"".format(self.mode.upper()), ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            file_name = "{0:s}_pipeline_history.txt".format(pm)
            cmds.append(("{0:s} {1:s} &> {2:s}".format(read_apve_history_cmd, pm, file_name), ""))

        #----------

        # Analyse the results.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Analysing results'\"".format(self.mode.upper()), ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = zip(APVES, PIS)
        for (target_apve, target_pi) in targets:
            cmds.extend([("python -c \"print '  {0:s} vs {1:s}'\"".format(target_apve, target_pi), "")])
            file_name_apve = "{0:s}_pipeline_history.txt".format(target_apve)
            file_name_pi = "{0:s}_spy_log.txt".format(target_pi)
            cmds.append(("{0:s} {1:s} {2:s}".format(analyse_cmd, file_name_apve, file_name_pi), ""))
        targets = list(PIS)
        for target_pi in targets:
            cmds.extend([("python -c \"print '  {0:s} vs {1:s}'\"".format(pm, target_pi), "")])
            file_name_pm = "{0:s}_pipeline_history.txt".format(pm)
            file_name_pi = "{0:s}_spy_log.txt".format(target_pi)
            cmds.append(("{0:s} {1:s} {2:s}".format(analyse_cmd, file_name_pm, file_name_pi), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTestAPVE.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure several APVEs, iCIs and PIs " \
                  "in order to (stress-)test the APVE."
    usage = "usage: %prog [options]"
    res = TCDSConfigForTestAPVE(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
