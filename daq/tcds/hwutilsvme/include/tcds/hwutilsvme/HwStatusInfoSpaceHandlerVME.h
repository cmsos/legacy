#ifndef _tcds_hwutilsvme_HwStatusInfoSpaceHandlerVME_h_
#define _tcds_hwutilsvme_HwStatusInfoSpaceHandlerVME_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilsvme {

    class HwStatusInfoSpaceHandlerVME : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwStatusInfoSpaceHandlerVME(xdaq::Application& xdaqApp,
                                  tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwStatusInfoSpaceHandlerVME();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      /* virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const; */

    };

  } // namespace hwutilsvme
} // namespace tcds

#endif // _tcds_hwutilsvme_HwStatusInfoSpaceHandler_h_
