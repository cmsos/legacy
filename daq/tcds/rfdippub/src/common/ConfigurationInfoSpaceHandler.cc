#include "tcds/rfdippub/ConfigurationInfoSpaceHandler.h"

#include "xdaq/Application.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::rfdippub::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // Where/how to find the LAS.
  createString("lasURL",
               "unknown",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createString("lasServiceName",
               "xmaslas2g",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  // createString("flashlistName",
  //              "tcds_common",
  //              "",
  //              tcds::utils::InfoSpaceItem::NOUPDATE,
  //              true);
}

void
tcds::rfdippub::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::cout << "DEBUG JGH 2018 rfdippub::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor()" << std::endl;
  std::string const itemSetName = "itemset-app-config";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "lasURL",
                  "LAS URL",
                  this);
  monitor.addItem(itemSetName,
                  "lasServiceName",
                  "LAS service name",
                  this);
  // monitor.addItem(itemSetName,
  //                 "flashlistName",
  //                 "Flashlist name",
  //                 this);
}

void
tcds::rfdippub::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                             tcds::utils::Monitor& monitor,
                                                                             std::string const& forceTabName)
{
  std::cout << "DEBUG JGH 2018 rfdippub::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer()" << std::endl;
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        2);
  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "itemset-app-config",
                          tabName);
}
