#include "ttc/utils/TTSInfo.hh"

#include <iostream>


#define RED "<span style=\"color: rgb(255, 0, 0);\">"
#define GREEN "<span style=\"color: rgb(51, 204, 0);\">"
#define BLUE "<span style=\"color: rgb(51, 51, 255);\">"
#define YELLOW "<span style=\"color: rgb(255, 255, 0);\">"
#define ORANGE "<span style=\"color: rgb(255, 153, 0);\">"
#define GREY "<span style=\"color: rgb(153, 153, 153);\">"
#define NOCOL "</span>"


using namespace std;


ttc::TTSInfo::TTSInfo(const uint32_t StatusWord)
:
    status_(StatusWord)
{}


uint32_t ttc::TTSInfo::GetStatusWord() const
{
  return status_;
}


size_t ttc::TTSInfo::NPartitions() const
{
  return 7;
}


size_t ttc::TTSInfo::GetStatus_Pattern(size_t idx) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::GetStatus_Pattern(): idx=" << idx << " out of range!" << endl;
    return 0;
  }

  uint32_t stat = 0;
  for (size_t i = 0; i < 4; ++i)
  {
    stat |= (((status_ >> (idx + i * NPartitions())) & 1) << i);
  }
  return stat;
}


string ttc::TTSInfo::GetStatus_String(size_t idx, bool htmlcolors) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::GetStatus_String(): idx=" << idx << "out of range!" << endl;
    return string("ERROR in TTSInfo(): GetStatus_String()");
  }
  size_t stat = GetStatus_Pattern(idx);
  string s = "";
  if (CheckReady(stat))
  {
    if (htmlcolors)
      s += GREEN;
    s += "Ready         ";
    if (htmlcolors)
      s += NOCOL;
  }
  else if (CheckDisconnected(stat))
  {
    s += "Disconnected  ";
  }
  else if (CheckOutOfSync(stat))
  {
    if (htmlcolors)
      s += RED;
    s += "OutOfSync     ";
    if (htmlcolors)
      s += NOCOL;
  }
  else if (CheckWarning(stat))
  {
    if (htmlcolors)
      s += RED;
    s += "Warning       ";
    if (htmlcolors)
      s += NOCOL;
  }
  else if (CheckBusy(stat))
  {
    if (htmlcolors)
      s += RED;
    s += "Busy          ";
    if (htmlcolors)
      s += NOCOL;
  }
  else if (CheckError(stat))
  {
    if (htmlcolors)
      s += RED;
    s += "Error          ";
    if (htmlcolors)
      s += NOCOL;
  }
  else
  {
    if (htmlcolors)
      s += RED;
    s += "Unknown (";
    for (int i = 3; i >= 0; --i)
      s += (((stat >> i) & 1) == (i == 3 ? 1 : 0) ? "1" : "0");
    s += ")";
    if (htmlcolors)
      s += NOCOL;
  }
  return s;
}


bool ttc::TTSInfo::IsReady(size_t idx) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::IsReady(): idx=" << idx << " out of range!" << endl;
    return false;
  }
  return CheckReady(GetStatus_Pattern(idx));
}


bool ttc::TTSInfo::IsDisconnected(size_t idx) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::IsDisconnected(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckDisconnected(GetStatus_Pattern(idx));
}


bool ttc::TTSInfo::IsOutOfSync(size_t idx) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::IsOutOfSync(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckOutOfSync(GetStatus_Pattern(idx));
}


bool ttc::TTSInfo::IsWarning(size_t idx) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::IsWarning(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckWarning(GetStatus_Pattern(idx));
}


bool ttc::TTSInfo::IsBusy(size_t idx) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::IsBusy(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckBusy(GetStatus_Pattern(idx));
}


bool ttc::TTSInfo::IsError(size_t idx) const
{
  if (idx >= NPartitions())
  {
    cout << "ERROR: TTSInfo::IsError(): idx=" << idx << " out of range!" << endl;
    return true;
  }
  return CheckError(GetStatus_Pattern(idx));
}


bool ttc::TTSInfo::CheckReady(size_t stat) const
{
  return (stat == 0xf);
}


bool ttc::TTSInfo::CheckDisconnected(size_t stat) const
{
  return (stat == 0x7);
}


bool ttc::TTSInfo::CheckOutOfSync(size_t stat) const
{
  return (stat == 0x5);
}

bool ttc::TTSInfo::CheckWarning(size_t stat) const
{
  return (stat == 0x6);
}


bool ttc::TTSInfo::CheckBusy(size_t stat) const
{
  return (stat == 0x3);
}


bool ttc::TTSInfo::CheckError(size_t stat) const
{
  return (stat == 0xb);
}
