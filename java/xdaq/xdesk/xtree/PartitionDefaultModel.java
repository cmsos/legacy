package xdaq.xdesk.xtree;

import javax.swing.*;
import java.util.*;
import javax.infobus.*;
import javax.swing.event.*;


public class PartitionDefaultModel
{

	EventListenerList listenerList_ = new EventListenerList();
	Hashtable configurations_ = new Hashtable();

	public void addPartitionModelListener(PartitionModelListener l )
	{
		listenerList_.add(PartitionModelListener.class, l );
	
	}
	
	public void removePartitionModelListener(PartitionModelListener l )
	{
		listenerList_.remove(PartitionModelListener.class, l );
	}
	
	public Enumeration  getConfigurationNames()
	{
		return configurations_.keys();
	
	}
	
	public Object getValue( String name )
	{
		return configurations_.get(name);
	
	}
	
	public void setValue(String name, Object item )
	{
		configurations_.put(name, item);
	}
	
	public Object removeValue(String name)
	{
		return configurations_.remove(name);
	}
	
	
	public void firePartitionModelDataChanged()
	{
	
		Object[] listeners = listenerList_.getListenerList();
		PartitionModelEvent event = null;
		for (int i = listeners.length-2; i>= 0; i-=2)
		{
			System.out.println("Notify listener "+ i);
			if (event == null )
			{
				event = new PartitionModelEvent(this);
			}
			((PartitionModelListener)listeners[i+1]).partitionChanged(event);
		
		}
	}
	
	
}	
	
