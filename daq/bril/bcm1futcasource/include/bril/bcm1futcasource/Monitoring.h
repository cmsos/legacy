// $Id$
#ifndef _BRIL_BCM1FUTCASOURCE_MONITORING_H_
#define _BRIL_BCM1FUTCASOURCE_MONITORING_H_

#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include "xdata/Serializable.h"
#include "xdata/InfoSpace.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Output.h"
#include "bril/webutils/WebUtils.h"
#include "xdaq//Application.h"

namespace bril { namespace bcm1futcasource {

class Monitoring{
 public:
    //
    explicit Monitoring(xdaq::Application * application);

    float getApplicationUptime() const;

    // Application uptime clock
    //
    std::chrono::time_point<std::chrono::system_clock> _rt_start;
};
}  // namespace bcm1futcasource
}  // namespace bril
#endif
