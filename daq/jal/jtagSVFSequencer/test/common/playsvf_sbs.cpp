/**
 *
 *     @short playsvf_sbs 
 *            
 *            Test application for loading PROMs on the Florida Sector Processor 2002
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.1 $
 *     $Date: 2007/05/15 13:03:21 $
 *
 *
**/
#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/DTTFJTAGController.h"
#include "jal/jtagController/ScanPSC100JTAGController.h"
#include "jal/jtagController/HALScanPSC100Adapter.h"
#include "jal/jtagSVFSequencer/JTAGSVFSequencer.h"

#include "hal/VMEDevice.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
#include "hal/SBS620x86LinuxBusAdapter.hh"
#include "hal/VMEDummyBusAdapter.hh"
#include "hal/linux/StopWatch.hh"

#include <iostream>
#include <iomanip>

#include <ctype.h>
#include <string>
#include <stdint.h>

using namespace std;
using namespace HAL;
using namespace jal;

uint32_t slot2address(int VMEslot)
{
   return (uint32_t) VMEslot << 19;
}

int main(int argc, char **argv) {
  
  // say hello
  cout << endl;
  cout << "=======================================" << endl;
  cout << "This is playsvf_sbs  ($Revision: 1.1 $)" << endl;
  cout << "=======================================" << endl;
  cout << "A utility to play SVF files to a JTAG chain via HAL and VME (SBS Interface)." << endl;
  cout << "Specific version for the Florida SP 2002." << endl;
  cout << "Use with care. Incorrect use may damage the hardware. No explicit or implied warranty." << endl;
  cout << endl;

  // the default parameters
  int VMEslot = 17;
  string addrtable_fn = "/home/xdaq/TriDAS/daq/jal/jtagSVFSequencer/test/AddressTable.txt";
  bool check_only = false;
  bool debug_mode = false;
  bool verbose_mode = false;
  bool override_errors = false;
  // The frequency of the ScanPSC on the SP is 10 MHz
  double SCK_frequency = 10.e6;
  double svf_frequency = 1.; // 1 MHz
  int chain_nr = 1;

  // display help
  if (argc==1) {
    cout << "Usage: " << argv[0] << " [-B] [-A=hex_addr] [-T=fn] [-J] [-C] [-D] [-V] [-O] filename.svf" << endl;
    cout << "     -S=slot         set slot of SP (default = " << VMEslot << ")" << endl;
    cout << "     -F=frequ        set the JTAG Clock Frequency (in MHz) that the SVF file was generated for (default = " << svf_frequency << " MHz)" << endl;
    cout << "                       (a FREQUENCY statement in the SVF file takes precedence over this)" << endl;
    cout << "     -T=fn           set address table file name (default=" << addrtable_fn << ")" << endl;
    cout << "                       (the file contains the base address of the ScanPSC Controller as item \"PSC100_BASE\")" << endl;
    cout << "     -j=n            set JTAG chain nr (0 to 3) (default = " << chain_nr << ")" << endl;          
    cout << "     -C              check only. Only read svf file and check syntax, no hardware access." << endl;
    cout << "     -D              debug only. No hardware access. This will use the dummy bus adapter." << endl;
    cout << "     -V              verbose. display lots of debug output." << endl;
    cout << "     -O              override errors. Continue upon compare error. Only advisable in debug mode." << endl;
    cout << endl;
    exit(0);
  }

  // parse options
  for (int i=1; i<argc;i++) {
    if (argv[i][0] == '-') {
      switch ( toupper( argv[i][1] ) ) {
      case 'S' : 
	if (argv[i][2] == '=') {
	  int success = sscanf(&(argv[i][3])," %d", &VMEslot);
	  if (!success) { cout << "error parsing VEM Slot" << endl; exit(-1);}
	}
	else { cout << "error parsing VMNE Slot" << endl; exit(-1);}
	break;
      case 'J' : 
	if (argv[i][2] == '=') {
	  int success = sscanf(&(argv[i][3])," %d", &chain_nr);
	  if (!success) { cout << "error parsing JTAG chain nr" << endl; exit(-1);}
	  if (chain_nr <0 || chain_nr >3) { cout << "JTAG chain nr out of range" << endl; exit(-1);}
	}
	else { cout << "error parsing vme base address" << endl; exit(-1);}
	break;
      case 'F' : 
	if (argv[i][2] == '=') {
	  int success = sscanf(&(argv[i][3])," %lf", &svf_frequency);
	  if (!success) { cout << "error parsing SVF frequency" << endl; exit(-1);}
	}
	else { cout << "error parsing SVF frequency" << endl; exit(-1);}
	break;
      case 'T' :
	if (argv[i][2] == '=') {
	  addrtable_fn = &(argv[i][3]);
	}
	else { cout << "error parsing address table file name" << endl; exit(-1);}
	break;
      case 'C' : check_only = true; break;
      case 'D' : debug_mode = true; break;
      case 'V' : verbose_mode = true; break;
      case 'O' : override_errors = true; break;
      }
    }
  }

  string svf_filename = argv[argc-1];

  // display options
  cout << "The following settings will be used:" << endl << endl;
  cout << "  VME Slot:               " << VMEslot 
       << " ( Base address = " << hex << slot2address(VMEslot) << dec << " ) " <<  endl;
  cout << "  Address table filename: " << addrtable_fn << endl;
  cout << "  JTAG Chain Nr:          " << chain_nr << endl;
  cout << "  System clock frequency: " << SCK_frequency/1.e6 << " MHz" << endl;
  cout << "  Frequency of SVF file:  " << svf_frequency << " MHz (unless specified in SVF file)" << endl;
  cout << "  Check only:             " << (check_only?"YES":"NO") << endl;
  cout << "  Debug only:             " << (debug_mode?"YES":"NO") << endl;
  cout << "  Verbose:                " << (verbose_mode?"YES":"NO") << endl;
  cout << "  Override Errors:        " << (override_errors?"YES":"NO") << endl;
  cout << endl;
  cout << "  SVF File: " << svf_filename << endl;
  cout << endl;
  cout << endl;

  try {
    StopWatch sw(0);

    /// Load the SVF file
    cout << "loading SVF file " << svf_filename << endl;
    JTAGSVFSequencer seq;
    sw.start();
    seq.loadSVFFile(svf_filename.c_str());
    sw.stop();

    // (an error will cause an exception and terminate the program)
    cout << "SVF file loaded into memory without problems. (time elapsed = " << (double)sw.read()/1.e6 << " sec)" << endl;
    cout << endl;

    if (check_only) {
      cout << "check complete. bye." << endl;
      exit(0);
    }

    // warn the user
    if (!debug_mode) {
      cout << "Hardware will now be accessed with the above parameters. " << endl;
      cout << " - Please check the parameters carefully. " << endl;
      cout << " - Make sure that the above system clock frequency is not below the actual frequency used in hardware." << endl;
      cout << " - Make sure that the above SVF file frequency is not above the frequency the file was generated for." << endl;
      string answer;
      do {
	cout << endl << "Are you sure to continue (yes/no)? ";
	cin >> answer;
	if (answer=="no") exit(0);
      } while (answer!="yes");
    
    }
  
    VMEAddressTableASCIIReader addressTableReader( addrtable_fn.c_str() );
    VMEAddressTable addressTable("Outcard address table", addressTableReader);

    // dynamically select bus adapter
    VMEBusAdapterInterface *busAdapter=0;
    if (debug_mode) busAdapter = new  VMEDummyBusAdapter(VMEDummyBusAdapter::VERBOSE_OFF);
    else 
      busAdapter = new SBS620x86LinuxBusAdapter(0);

    {
      VMEDevice outcard(addressTable, *busAdapter, slot2address(VMEslot));

      // set chain nr (CSC SP register)
      outcard.unmaskedWrite( "PSC100_BASE", (uint32_t) ((chain_nr&3) << 1) , HAL_NO_VERIFY,  0x10);

      // create a HAL adapter for a ScanPSC type JTAG Controller
      HALScanPSC100Adapter scanpsc_adapter (outcard, "PSC100_BASE"); 

      // dynamically select JTAG controller type
      ScanPSC100JTAGController *jtag_controller = new ScanPSC100JTAGController ( scanpsc_adapter, SCK_frequency );

      if (debug_mode) jtag_controller->setDebugFlag(5);
      else if (verbose_mode) jtag_controller->setDebugFlag(4);

      {
	// open JTAG Chain for chain 0 on the controller 
	// (The Florida SP always uses chain 0 of the controller and does the switching between chains in its firmware)
	JTAGChain jtag_chain( *jtag_controller, 0 );
      
	// set the frequency that the SVF file was generated for
	// this frequency may be changed by a FREQUENCY statement in the SVF file
	// ATTENTION: the frequency is reset to (-1.) after playing the SVF file 
	jtag_chain.setDesiredTCKFrequency( svf_frequency * 1.e6 );

	// reset the JTAG Chain
	jtag_chain.resetTAP();

	if (override_errors) 
	  seq.setOverrideError(true);

	// run it
	bool status = seq.execute( jtag_chain );

	// reset the JTAG Chain
	jtag_chain.resetTAP();

	if (status)
	  cout << "successfully played SVF file." << endl;
	else
	  cout << "error playing SVF file." << endl;

      }
      delete jtag_controller;
    }
    delete busAdapter;

  }
  catch (exception &e) {
    cout << "Unknown Exception caught: " << e.what() << endl;
    return 1;
  }
  return 0;
}

