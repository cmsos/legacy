package net.hep.cms.xdaqctl;

import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * WS-Eventing subscription
 * <p/>
 * Typical usage:
 * <pre>
 * // RCMS function manager URL
 * String ORIGINATOR = "http://localhost:8080/urn:rcms-fm:fullpath=/rcms/test";
 * // WS-Evening server URL
 * String ENDPOINT = "http://localhost:40000/urn:xdaq-application:lid=400"
 *
 * WSESubscription s = new WSESubscription(ORIGINATOR, ENDPOINT);
 * s.setFilter("//xmas:sample[@flashlist]");  // for all XMAS messages
 * s.setExpires("PT5M");  // renew every 5 minutes
 * Sting uuid = s.subscribe();
 * ... do other things ...
 * // check if the subscription is still valid
 * // if not, subscribing again.
 * if (s.isStopped()) { uuid = s.subscribe(); }
 * </pre>
 *
 * @author Alexander Oh (alexander.oh@cern.ch)
 * @author Ichiro Suzuki (ichiro@fnal.gov)
 */
public class WSESubscription extends Message {
	
	static final String DUMMY_FILTER = "FILTER";

	private String wseEndpoint;
	private SOAPElement filterText;
	private SOAPElement expires;
	
	private WSERenewal renewalMessage;

	private Timer timer = new Timer();
	private RenewalTask renewalTask = null;
	private static final int RENEWAL_MARGIN = 5000;  // 5 seconds
	private static final String RENEWAL_MINIMAL = "PT10S";

	private class RenewalTask extends TimerTask {
		public void run() {
			try {
				renew();
			} catch (Exception ignore) {
				stop();
			}
		}
	}
	
	/**
	 * @param originator URL to which WS-Eventing server connects
	 *  for SOAP messages
	 * @param wseEndpoint XDAQ URL of a WS-Eventing
	 * e.g. "http://localhost:40000/urn:xdaq-application:lid=400"
	 */
	public WSESubscription(String originator, String wseEndpoint)
			throws XDAQMessageException {

		super();

		this.wseEndpoint = wseEndpoint;
		String wseURL = wseEndpoint + "/" + WSE_TARGET;
		
		message.getMimeHeaders().addHeader("SOAPAction", WSE_TARGET);

		try {
			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
			SOAPElement e;
			
			// Header : { Action, To, ReplyTo }
			SOAPHeader header = envelope.getHeader();
			e = header.addChildElement("Action", "wsa", WSA_NS);
			e.addTextNode(WSE_NS + "/Subscribe");
			e = header.addChildElement("To", "wsa", WSA_NS);
			e.addTextNode(wseURL);
			e = header.addChildElement("ReplyTo", "wsa", WSA_NS);
			e.addTextNode(wseURL);
			
			// Body : Subsctibe : { EndTo, Delivery, Expires, Filter }
			// Filter is filled with a dummy, to be set by setFilter() later
			SOAPBody body = envelope.getBody();
			SOAPElement subscribe = body.addBodyElement(
					envelope.createName("Subscribe", "wse", WSE_NS));
			
			SOAPElement endto = subscribe.addChildElement(
					"EndTo", "wse", WSE_NS);
			e = endto.addChildElement("Address", "wsa", WSA_NS);
			e.addTextNode(originator);
			
			SOAPElement delivery = subscribe.addChildElement(
					"Delivery", "wse", WSE_NS);
			SOAPElement notifyto = delivery.addChildElement(
					"NotifyTo", "wse", WSE_NS);
			e = notifyto.addChildElement("Address", "wsa", WSA_NS);
			e.addTextNode(originator);
			
			e = subscribe.addChildElement("Expires", "wse", WSE_NS);
			expires = e.addTextNode("PT1M");

			e = subscribe.addChildElement("Filter", "wse", WSE_NS);
			e.addAttribute(envelope.createName("Dialect"), XPATH_DIALECT);
			filterText = e.addTextNode(DUMMY_FILTER);
			
		} catch (SOAPException e) {
			throw new XDAQMessageException(
					"Failed to create a WS-Eventing subscription SOAP message", e);
		}
	}

	/**
	 * @return the filter
	 */
	public String getFilter() {
		return filterText.getValue();
	}

	/**
	 * @param filter XPath expression to select messages
	 * <p/>
	 * e.g.<br/>
	 * "//xmas:sample[@flashlist]" - All XDAQ monitoring messages<br/>
	 * "//xmas:sample[@flashlist][@tag='urgent']" - messages with 'urgent' tag<br/>
	 */
	public void setFilter(String filter) {
		filterText.setValue(filter);
	}
	
	/**
	 * @return the expires
	 */
	public String getExpires() {
		return expires.getValue();
	}

	/**
	 * @param expires the expires to set
	 * <p/>
	 * Format: PT[number][unit] e.g.<br/>
	 * "PT30S" - 30 seconds<br/>
	 * "PT15M" - 15 minutes<br/>
	 */
	public void setExpires(String expires) {
		if (getIntervalSeconds(expires) < getIntervalSeconds(RENEWAL_MINIMAL)) {
			expires = RENEWAL_MINIMAL;
		}
		this.expires.setValue(expires);
	}

	/**
	 * subscribe to a WS-Eventing service
	 *
	 * @return UUID (16-digits unique ID) for the subscription
	 */
	public String subscribe()
			throws XDAQMessageException, XDAQTimeoutException {
		
		if (getFilter().equals(DUMMY_FILTER)) {
			throw new XDAQMessageException("filter expression is not set.");
		}
		
		Document reply = send(wseEndpoint, WSE_TARGET);
		if (getFaultString() != null) {
			throw new XDAQMessageException("SOAP fault occured when sending to " + wseEndpoint + ": " + getFaultString());
		}
		String uuid = getIdentifier(reply);
		renewalMessage = new WSERenewal(wseEndpoint, uuid);
		renewalMessage.setExpires(expires.getValue());
		
		long renewalPeriod =
				getIntervalSeconds(getExpires()) * 1000 - RENEWAL_MARGIN;
				// renew RENEWAL_MARGIN milliseconds earlier than expiration
		
		stop();
		renewalTask = new RenewalTask();
		timer.schedule(renewalTask, renewalPeriod, renewalPeriod);
		
		return uuid;
	}
	
	/**
	 * forced renewal
	 */
	public void renew() throws XDAQMessageException, XDAQTimeoutException {
		renewalMessage.renew();
	}
	
	/**
	 * stop renewal
	 */
	public void stop() {
		if (!isStopped()) {
			renewalTask.cancel();
			renewalTask = null;
		}
	}
	
	/**
	 * @return true if automatic renewal process is still running.
	 */
	public boolean isStopped() {
		return (renewalTask==null);
	}
	
	private String getIdentifier(Document document) {
		NodeList list = document.getElementsByTagNameNS(WSE_NS, "Identifier");
		
		if (list.getLength() != 1) { return null; }
		
		return list.item(0).getTextContent();
	}

	private int getIntervalSeconds(String line) {
		Matcher m = Pattern.compile("PT(\\d{1,})(S|M)").matcher(line);
		m.find();
		
		int interval = Integer.parseInt(m.group(1));
		if (m.group(2).equals("M")) {
			interval *= 60;
		}
		
		return interval;
	}
}

// End of file
// vim: set sw=4 ts=4:
