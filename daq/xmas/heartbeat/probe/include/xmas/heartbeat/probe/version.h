// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_heartbeat_probe_version_h_
#define _xmas_heartbeat_probe_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define XMASHEARTBEATPROBE_VERSION_MAJOR 2
#define XMASHEARTBEATPROBE_VERSION_MINOR 0
#define XMASHEARTBEATPROBE_VERSION_PATCH 1
// If any previous versions available E.g. #define XMASHEARTBEATPROBE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define XMASHEARTBEATPROBE_PREVIOUS_VERSIONS "2.0.0"


//
// Template macros
//
#define XMASHEARTBEATPROBE_VERSION_CODE PACKAGE_VERSION_CODE(XMASHEARTBEATPROBE_VERSION_MAJOR,XMASHEARTBEATPROBE_VERSION_MINOR,XMASHEARTBEATPROBE_VERSION_PATCH)
#ifndef XMASHEARTBEATPROBE_PREVIOUS_VERSIONS
#define XMASHEARTBEATPROBE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(XMASHEARTBEATPROBE_VERSION_MAJOR,XMASHEARTBEATPROBE_VERSION_MINOR,XMASHEARTBEATPROBE_VERSION_PATCH)
#else 
#define XMASHEARTBEATPROBE_FULL_VERSION_LIST  XMASHEARTBEATPROBE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(XMASHEARTBEATPROBE_VERSION_MAJOR,XMASHEARTBEATPROBE_VERSION_MINOR,XMASHEARTBEATPROBE_VERSION_PATCH)
#endif 

namespace xmasheartbeatprobe
{
	const std::string package  =  "xmasheartbeatprobe";
	const std::string versions =  XMASHEARTBEATPROBE_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string summary = "XDAQ Monitoring and Alarming System probe";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
