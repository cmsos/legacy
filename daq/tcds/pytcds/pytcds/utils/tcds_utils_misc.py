###############################################################################
## Some miscellaneous utilities used in the TCDS scripts.
###############################################################################

import re
import string

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def uint8_to_char(integer_in):
    """Re-interprets a 8-bit integer number as ASCII character."""
    char_out = chr(integer_in)
    if not char_out in string.printable:
        char_out = ""
    # End of uint8_to_char().
    return char_out

###############################################################################

def char_to_uint8(char_in):
    """Re-interprets an ASCII character as a single byte integer."""
    integer_out = ord(char_in)
    # End of uint8_to_string().
    return integer_out

###############################################################################

def uint32_to_string(integer_in):
    """Re-interprets a 32-bit integer number as four characters."""

    char_0 = uint8_to_char((integer_in & 0xff000000) >> 24)
    char_1 = uint8_to_char((integer_in & 0x00ff0000) >> 16)
    char_2 = uint8_to_char((integer_in & 0x0000ff00) >> 8)
    char_3 = uint8_to_char(integer_in & 0x000000ff)
    string_out = "".join([char_0, char_1, char_2, char_3])

    # End of uint32_to_string().
    return string_out

###############################################################################

def string_to_uint32(string_in):
    """Re-interprets a four-character (or shorter) string as a 32-bit integer."""

    # ASSERT ASSERT ASSERT
    assert (len(string_in) <= 4), \
        "ERROR Can only convert up-to-four-character strings into uint32."
    # ASSERT ASSERT ASSERT end

    int_0 = 0
    int_1 = 0
    int_2 = 0
    int_3 = 0
    if (len(string_in) > 0):
        int_0 = char_to_uint8(string_in[0])
    if (len(string_in) > 1):
        int_1 = char_to_uint8(string_in[1])
    if (len(string_in) > 2):
        int_2 = char_to_uint8(string_in[2])
    if (len(string_in) > 3):
        int_3 = char_to_uint8(string_in[3])
    integer_out = (int_0 << 24) + \
                  (int_1 << 16) + \
                  (int_2 << 8) + \
                  int_3

    # End of string_to_uint32().
    return integer_out

###############################################################################

def escape_string(string_in):
    res = []
    # End of escape_string().
    return res

###############################################################################

def name_matches_pattern(node_name, ignore_list):
    matching_pattern = None
    for pattern in ignore_list:
        if re.match(pattern, node_name):
            matching_pattern = pattern
            break
    # End of name_matches_pattern().
    return matching_pattern

###############################################################################

def chunkify(input_list, chunk_size):
    res = [input_list[i:i + chunk_size] for i in range(0, len(input_list), chunk_size)]
    # End of chunkify().
    return res

###############################################################################

def parse_as_int(str_in, num_type=None):
    """Parse a string as an integer."""

    if num_type == None:
        num_type = int

    tmp = str_in.lower()
    if tmp.startswith("0x"):
        radix = 16
    elif tmp.startswith("0o"):
        radix = 8
    elif tmp.startswith("0b"):
        radix = 2
    elif tmp.startswith("0"):
        radix = 8
    else:
        radix = 10

    res = num_type(tmp, radix)

    # End of parse_as_int().
    return res

###############################################################################

def format_eui(eui_bytes):
    """Takes an array of bytes and formats it as an EUI.

    Uses the 'normal' way of representing EUIs: two-digit hex numbers
    separated by dashes.

    """
    eui_string = "-".join(["{0:02X}".format(i) for i in eui_bytes])
    # End of format_eui().
    return eui_string

###############################################################################

def format_mac_address(mac_address_bytes):
    """Takes an array of bytes and formats it as a MAC address.

    Uses the representation of MAC addresses that can be copy-pasted
    straight into /etc/ethers: two-digit hex numbers separated by
    colons.

    """
    mac_address_string = ":".join(["{0:02X}".format(i) for i in mac_address_bytes])
    # End of format_mac_address().
    return mac_address_string

###############################################################################

def format_ip_address(ip_address_bytes):
    """Takes an array of bytes and formats it as a IP address.

    Uses the 'normal' way of representing IP addresses: decimal
    numbers separated by dots.

    """
    ip_address_string = ".".join(["{0:d}".format(i) for i in ip_address_bytes])
    # End of format_ip_address().
    return ip_address_string

###############################################################################

def format_timedelta(time_delta_in):

    seconds = int(time_delta_in.total_seconds())
    periods = [
        ('year', 60*60*24*365),
        ('month', 60*60*24*30),
        ('day', 60*60*24),
        ('hour', 60*60),
        ('minute', 60),
        ('second', 1)
    ]

    pieces = []
    for (period_name, period_seconds) in periods:
        if seconds >= period_seconds:
            (period_value, seconds) = divmod(seconds, period_seconds)
            # This works if 'period_value == 1.'
            fmt_str = "{0:d} {1:s}"
            if period_value != 1:
                fmt_str += "s"
            pieces.append(fmt_str.format(period_value, period_name))

    return ", ".join(pieces)

###############################################################################
