//$Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/


const addon = require('../alchemy/build/Release/alchemy');

var http = require("http");
var dns = require('dns');
var url = require('url');
const path = require('path');
const fs = require('fs');
var express = require('express');

var tools = require('./tools');
var Listener = require('./listener');
var layout = require('ejs-mate');
var util = require('util');



var querystring = require('querystring');
var bodyParser = require('body-parser');


//var db = require('../sorcerer/db');

var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var db = require('../sorcerer/db');
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;


//var context = addon.getApplicationContext();
var pta = addon.getPeerTransportAgent();
/*var xgil = pta.getListener('cgi');

function processIncoming (req, res) { 
	var xgiinput = {
			'url':  req.headers["request_uri"], 
			'method' : req.method,
			'headers': req.headers, 
			'environment': req.environment,
			'data' : req.body
	};	
	req.xgiinput = xgiinput;

	console.log(req.xgiinput)
	var xgiout = xgil.processIncoming(req.xgiinput);

	res.statusMessage = xgiout.reason;
	res.status(xgiout.status);

	xgiout.headers.forEach(function(value){
		var mimetype = value.split(':');
		res.setHeader(mimetype[0], mimetype[1]);

	});
	res.end(xgiout.data, 'utf-8');
}
 */
function endTransaction (req, res) {
	console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>> end of transaction');
	res.end(res.xgiout.data, 'utf-8');
}

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}


class Interact {
	constructor(parameters, descriptor) {
		this._descriptor = descriptor;
		this._app = express();
		this._router = express.Router();
		this._descriptor = descriptor;
		this._descriptor.icon = "/interact/images/http.png";

		//this._router.get('/applications/myapplication/whoami', this.whoami);
		this._router.get('/urn:xdaq-application:lid=' + descriptor.id, this.home.bind(this));
		this._router.get('/urn:xdaq-application:service=' + descriptor.service, this.home.bind(this));
		this._listener = new Listener();
		this._context = addon.getApplicationContext();
		console.log("*************************************** Interact CTYOR **********************************");

		console.log(this._context);
		console.log("*************************************** Interact CTYOR **********************************");

		
		var rootDir = process.env.XDAQ_DOCUMENT_ROOT;

		this._app.use(express.static(rootDir, {fallthrough: true}));


		console.log("*************************************** ROOT DIR IS **********************************");
		console.log(rootDir);
		console.log("*************************************** ROOT DIR IS**********************************");

		console.log("*************************************** Passport Begin **********************************");

		passport.use(new Strategy(
				  function(username, password, cb) {
					  console.log('i am strategy');
				    db.users.findByUsername(username, function(err, user) {
				      if (err) { return cb(err); }
				      if (!user) { return cb(null, false); }
				      if (user.password != password) { return cb(null, false); }
				      return cb(null, user);
				    });
				  }));

		passport.serializeUser(function(user, cb) {
			 console.log('i am serializeUser');
			  cb(null, user.id);
			});

			passport.deserializeUser(function(id, cb) {
				 console.log('i am deserializeUser');
			  db.users.findById(id, function (err, user) {
			    if (err) { return cb(err); }
			    cb(null, user);
			  });
			});


		//console.log(this._descriptor);
		//console.log(this._descriptor.redirect);
		var redirect = '/urn:xdaq-application:service=' + this._descriptor.redirect;
		console.log(redirect);
		//this._app.use(bodyParser.json()); // support json encoded bodies
		//this._app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
		//this._app.use(bodyParser.raw({type: 'application/x-www-form-urlencoded'})); // support encoded bodies
		//this._app.use(bodyParser.raw({type: '*/*'})); // support encoded bodies

		this._app.use(tools.processEnvironment({}));
		this._app.engine('ejs', layout );
		this._app.set('view engine', 'ejs');
		this._app.set('views','./sorcerer/html/views'); // this is a temporary solution, need to generalize for RPM installation
		
		this._app.use(require('morgan')('combined'));
		this._app.use(require('cookie-parser')('keyboard cat'));
		this._app.use(require('body-parser').urlencoded({ extended: true }));
		this._app.use(require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false }));
			

		// Initialize Passport and restore authentication state, if any, from the
		// session.
		this._app.use(passport.initialize());
		this._app.use(passport.session());

		this._app.use(function(req, res, next) {
			if (!req.isAuthenticated || !req.isAuthenticated()) {
				console.log("++++ I AM NOTE Authenticated");
			} else {
				console.log("++++ I AM Authenticated" + req.user);
				 console.log(util.inspect(req.user, true, null));
				}
			next();
		});
	  
			this._app.get('/logout',
					  function(req, res){
					    req.logout();
					    res.redirect('/');
					  });
			/*this._app.post('/login',
					  function(req, res){
					   console.log('i am login');
					   res.end('', 'utf-8');
					  });
			*/
		
			this._app.post('/login', 
					  passport.authenticate('local',  { successReturnToOrRedirect: '/', failureRedirect: '/login' }));
					  //function(req, res) {
					   // res.redirect('/');
					  //  res.redirect(req.session.returnTo || '/');
					  //  delete req.session.returnTo;
					  //});
			
			/*
			this._app.get('/login',
					  function(req, res){
					   // res.render('login');
					    var descriptors = context.operation({ action: 'descriptors'});
					   	res.render('login', { message: 'I am inside interact', applications: descriptors.data , zone: 'default' })
					  });
			*/
			
			this._app.get('/profile',
					  ensureLoggedIn('/login'),
					  function(req, res){
					console.log('the user is ' + req.user);
					 //console.log(util.inspect(req.session, true, null));
					 console.log(util.inspect(req.isAuthenticated, true, null));
					    res.render('profile', { user: req.user });
					  });

			


			console.log("*************************************** Passport  End**********************************");


		/*this._app.use(function (req, res, next) {
			console.log("*************************************** ANY REQUEST **********************************");
  			console.log(req.headers);
  			console.log(req.body.toString());
			console.log("*************************************** ANY REQUEST**********************************");
			next();
		}); */
		// Create the express router object for Photos
		//var api = express.Router();
		// A GET to the root of a resource returns a list of that resource
		//api.get('/application/:lid', tools.processByLID, this._listener.processIncoming.bind(this._listener), endTransaction);
		//api.post('/application/:lid', tools.processByLID, this._listener.processIncoming.bind(this._listener), endTransaction);
		//api.get('/application/:lid/:resource', tools.processByLID, this._listener.processIncoming.bind(this._listener), endTransaction);
		//api.post('/application/:lid/:resource', tools.processByLID, this._listener.processIncoming.bind(this._listener), endTransaction);

		//api.get('/service/:service/:resource', tools.processByService, this._listener.processIncoming.bind(this._listener), endTransaction);
		//api.post('/service/:service/:resource', tools.processByService, this._listener.processIncoming.bind(this._listener), endTransaction);
		//api.get('/service/:service', tools.processByService, this._listener.processIncoming.bind(this._listener), endTransaction);
		//api.post('/service/:service', tools.processByService, this._listener.processIncoming.bind(this._listener), endTransaction);

		//api.get('/class/:type/instance/:instance', tools.processByClass, this._listener.processIncoming.bind(this._listener), endTransaction);
		//api.post('/class/:type/instance/:instance', tools.processByClass, this._listener.processIncoming.bind(this._listener), endTransaction);

		//api.get('/class/:type/instance/:instance/:resource', tools.processByClass, this._listener.processIncoming, endTransaction);
		//api.post('/class/:type/instance/:instance/:resource', tools.processByClass, this._listener.processIncoming, endTransaction);


		// Attach the routers for their respective paths
		//this._app.use('/api', api);

		var standard = express.Router();
		standard.post("/", function (req, res, next) {
			res.redirect(redirect)
			next
		});

		standard.get("/", function (req, res, next) {
			res.redirect(redirect)
			next
		});

		//standard.get("/*", this._listener.processIncoming.bind(this._listener), endTransaction); 
		//standard.post("/*", this._listener.processIncoming.bind(this._listener), endTransaction); 
		
		// if I use Hyperdaq I need these paths, otherwise for sorcerer i need th remove, this has to be generalize
		if( this._descriptor.redirect == 'hyperdaq')
		{
			standard.get("/urn*",  tools.processEnvironment({}), this._listener.processIncoming.bind(this._listener), endTransaction); 
			standard.post("/urn*", tools.processEnvironment({}), this._listener.processIncoming.bind(this._listener), endTransaction); 
		}
		/*
		this._app.use(function(req, res) {
		    res.status(404).send('File not found');
		});
		 */

		standard.get("/mysecurity",  ensureLoggedIn('/login'), this.home.bind(this)); 

		//standard.get('/urn:xdaq-application:*', processIncoming);
		this._app .use('/', standard);
		//this._app.use(standard);
		
		var test = express.Router();
		//test.get("/yoursecurity",  ensureLoggedIn('/login'), this.home.bind(this)); 
		this._app .use('/', test);

	}

	home(req,res) {
		console.log(this._descriptor);
		var descriptors = this._context.operation({ action: 'descriptors'});
		//console.log(descriptors);
		//console.log(req.headers);
		if ( req.headers.referer != undefined)
		{
			const referer = url.parse(req.headers.referer);

			console.log(referer.pathname);
		}

		if (this._descriptor.redirect = 'sorcerer' )
		{
			res.render('server', { user: req.user, applications: descriptors.data, zone: 'default', html: '<h1>I am the Alchemy HTTP Server</h1>' })	
		}
		else
		{
			res.send('<h1>I am the Alchemy HTTP Server</h1>');
		}
	}


	display() {
		console.log(this._descriptor);
	}

	// peer transport specific
	service() {
		return "cgi";
	}

	protocol() {
		return "http";
	}

 


	config( host, port)
	{
		
	
		var server = this._app.listen(port, host, function () {
			// var host = server.address().address
			// var port = server.address().port

			console.log("Interact listening at http://%s:%s", host, port)
		})


	}


} // end of class

module.exports = class Instantiator {
	constructor() {
	}

	create (args, descriptor) {
		return new Interact(args, descriptor)
	}

	type() {
		return "Interact";
	}


}




