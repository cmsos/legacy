//
//  XPropMenuBar.java
//  
//
//  Created by Luciano Orsini on Sun Jun 29 2003.
//  Copyright (c) 2003 __CERN__. All rights reserved.
//

package xdaq.xdesk.xprop;

import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import java.io.*;
import org.w3c.dom.*;
import java.awt.*;
import java.awt.event.*;
import org.apache.log4j.*;
import xdaq.xdesk.tools.*;

public class XPropMenuBar extends xdaq.xdesk.tools.MenuBar 
{
        Logger logger = Logger.getLogger (XPropMenuBar.class);
	XProp controller_;
	XPropViewer viewer_;
	
	public XPropMenuBar(XPropViewer viewer)
	{
		super("XProp",(MenuBarListener)viewer.getController());
			
		controller_ = viewer.getController();
		viewer_ = viewer;
			
		JMenu menu;
		JMenuItem item;
		
		// FILE menu
		//
		menu = new JMenu ("File");
		menu.setMnemonic ('f');
				
		item = new JMenuItem ( new AbstractAction ("New") 
		{
                    public void actionPerformed (ActionEvent e)
                    {
                            controller_.newWindow();
                    }});
                
		menu.add(item);
	
		
		item = new JMenuItem (new AbstractAction ("Close") 
		{
                    public void actionPerformed (ActionEvent e)
                    {
		    	// Dispose the frame into which the viewer panel has been put.
			// This will invoke the proper shutdown function of the XPropWindowListener
                             controller_.closeWindow (viewer_);

                    }});

		menu.add (item);
				
		this.add (menu);
		
        }        
}
