// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/WorkLoopFactory.h"
#include "xmas/collector2g/Collector.h"
#include "xmas/collector2g/Application.h"
#include "xdata/TimeVal.h"
#include "xdata/TableAlgorithms.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xmas/collector2g/IncomingDataEvent.h"
#include "xmas/collector2g/exception/Exception.h"
#include "xcept/tools.h"

#include "toolbox/TimeVal.h"

xmas::collector2g::Collector::Collector(xmas::collector2g::Topic settings)
	
	: 
	dispatcher_("urn:xdaq-workloop:collector2g-" + settings.getSubscribeTopic(), "waiting",0.8),
	settings_(settings),
	mutex_(toolbox::BSem::FULL, true),
	hasData_(false),
	lastUpdate_(0,0),
	lastOutput_(0,0)
{
	index_ = 0;

	// initialize statistics parameters
	dataEventCounter_ = 0;

	monitorSerializationFailedCounter_ = 0;
	monitorFireCounter_ = 0;
	monitorInternalLossCounter_ = 0;
	monitorCommunicationLossCounter_ = 0;
	monitorMemoryLossCounter_ = 0;
	monitorUnassignedLossCounter_ = 0;

	averageMergeTime_ = 0.0;
	averageMergeRate_ = 0.0;
	averageDeserializeTime_ = 0.0;
	averageDeserializeRate_ = 0.0;
	dataEventLostCounter_ = 0;
	dispatcher_.addActionListener(this);

	try
	{
		(void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:collector2g-" + settings_.getSubscribeTopic(), "waiting")->activate();
	}
	catch(toolbox::task::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "activating workloop for collector staging failed";
		XCEPT_RETHROW(xmas::collector2g::exception::Exception, msg.str(), e);
	}
}

xmas::collector2g::Collector::~Collector()
{
	(void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:collector2g-" + settings_.getSubscribeTopic(), "waiting")->cancel();
}

bool xmas::collector2g::Collector::hasData()
{
	mutex_.take();
	bool retVal = hasData_;
	mutex_.give();
	return retVal;
}

void xmas::collector2g::Collector::setData(xdata::Table::Reference& data, xdata::Properties & plist)
{
	mutex_.take();
	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	firstUpdate_ = toolbox::TimeVal::gettimeofday();
	version_ = plist.getProperty("urn:xmas-flashlist:version");
	lastOriginator_ = plist.getProperty("urn:xmas-flashlist:originator");
	tags_ = plist.getProperty("urn:xmas-flashlist:tag");
	name_ = plist.getProperty("urn:xmas-flashlist:name");
	
	data_ = data;
	if(index_ != 0)
	{
		delete index_;
		index_ = 0;
	}
	hasData_ = true;
	mutex_.give();
}

void xmas::collector2g::Collector::mergeData(xdata::Table::Reference& data, xdata::Properties & plist)
	
{
	mutex_.take();
		
	if (data_.isNull())
	{
		data_ = data;
		firstUpdate_ = toolbox::TimeVal::gettimeofday();
		hasData_ = true;
	}
	else
	{
		// create index for merging when not available
		try
		{
			if (index_ == 0)
			{
				// index points to the local table and acts as a proxy to this table
				//
				// TODO/TBD really ugly set to list conversion - change API in xdata::TableIndex!
				std::set<std::string> hashKey = settings_.getHashKey();
				std::list<std::string> hashList;
				for(std::set<std::string>::iterator i = hashKey.begin() ; i != hashKey.end() ; i++)
				{
					hashList.push_back(*i);
				}
				index_ = new xdata::TableIndex(&(*data_), hashList);
			}
				    
			for(xdata::Table::iterator srci = data->begin() ; srci != data->end() ; srci++)
			{
				// Looping over the incoming table and calling the insert of the iterator will
				// either append the row if the index key does not exist or overwrite an existing
				// row if the key exists.
				//
				(void) index_->insert(*srci);
			}
		}
		catch (xdata::exception::Exception& e)
		{
			mutex_.give();
			
			std::stringstream msg;
			msg << "Failed to merge flashlist '" << plist.getProperty("urn:xmas-flashlist:name") << "' from originator '" << plist.getProperty("urn:xmas-flashlist:originator");
			msg << "', version '" << plist.getProperty("urn:xmas-flashlist:version") << "' at ";
			msg << toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::loc);
			XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
		}
	}
	
	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	version_ = plist.getProperty("urn:xmas-flashlist:version");
	lastOriginator_ = plist.getProperty("urn:xmas-flashlist:originator");
	tags_ = plist.getProperty("urn:xmas-flashlist:tag");
	name_ = plist.getProperty("urn:xmas-flashlist:name");

	/*if ( ((double)(lastUpdate_ - firstUpdate_)) >= settings_.getFlushPeriod())
	{
		firstUpdate_ = lastUpdate_;
		mutex_.give();
		return true;
	}
	*/
	
	mutex_.give();
}


void xmas::collector2g::Collector::extractDataCollection(toolbox::mem::Reference *  ref, xdata::Properties & plist)
	 
{
	mutex_.take();

	if ( data_.isNull())
	{
		mutex_.give();
		std::stringstream m;
		m << "No data to be serialized for flashlist " << name_;
		XCEPT_RAISE (xmas::collector2g::exception::NoData, m.str());

	}

	try
	{
		xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*)ref->getDataLocation(), ref->getBuffer()->getSize());
		serializer_.exportAll ( &(*data_), &outBuffer);
		
		if (outBuffer.tellp() > 0)
		{		
			ref->setDataSize(outBuffer.tellp()); 
		}
		else
		{
			mutex_.give();
			std::stringstream m;
			m << "No data to be serialized for flashlist " << name_;
			XCEPT_RAISE (xmas::collector2g::exception::NoData, m.str());
		}
	}
	catch(xdata::exception::Exception & e)
	{
		mutex_.give();
		std::stringstream msg;
		msg << "failed to serialize flashlist '" << name_ << "' into buffer of size " << ref->getBuffer()->getSize() << " (too small)";
		XCEPT_RETHROW (xmas::collector2g::exception::FailedSerialization, msg.str(), e);	
	}
	
	plist.setProperty("urn:xmas-flashlist:name", name_);
	plist.setProperty("urn:xmas-flashlist:version", version_);
	plist.setProperty("urn:b2in-eventing:topic", settings_.getPublishTopic());
	
	// If after output data needs to be cleared, delete currently cached table
	if (settings_.getAutoClear())
	{
		if ( ! data_.isNull() )
		{
			data_ = 0;
		}
		if(index_ != 0)
		{
			delete index_;
			index_ = 0;
		}
		hasData_ = false;
	}
	
	lastOutput_ = toolbox::TimeVal::gettimeofday();
	dataEventCounter_ = 0;	

	mutex_.give();
}


size_t xmas::collector2g::Collector::getRowCount()
{
	mutex_.take();
	if (data_.isNull())
	{
		mutex_.give();
		return 0;
	}		
	else
	{
		size_t count = data_->getRowCount();
		mutex_.give();
		return count;
	}	
}

toolbox::TimeVal xmas::collector2g::Collector::getLastUpdate()
{
	mutex_.take();
	toolbox::TimeVal lastUpdate;
	lastUpdate = lastUpdate_;
	mutex_.give();
	return lastUpdate;
}

std::string xmas::collector2g::Collector::getVersion()
{
	mutex_.take();
	std::string version = version_;
	mutex_.give();
	return version;
}

std::string xmas::collector2g::Collector::getTags()
{
	mutex_.take();
	std::string tags = tags_;
	mutex_.give();
	return tags;
}

std::string xmas::collector2g::Collector::getLastOriginator()
{
	mutex_.take();
	std::string lastOriginator = lastOriginator_;
	mutex_.give();
	return lastOriginator;
}

void xmas::collector2g::Collector::clear()

{
	mutex_.take();
	
	try
	{
		if( ! data_.isNull() )
		{
				// re-create a new table from existing definition, but with no data rows
				const std::map<std::string, std::string, xdata::Table::ci_less> & definition = data_->getTableDefinition();
				data_ = new xdata::Table(definition);
		}

		if(index_ != 0)
		{
			delete index_;
			index_ = 0;
		}
		
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		mutex_.give();
	}
	catch (xdata::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to clear flashlist '" << name_ << "'";
		mutex_.give();
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
	
}

void xmas::collector2g::Collector::reset()

{
	mutex_.take();

	try
	{
		if ( ! data_.isNull() )
		{
			data_ = 0;
		}
	
		hasData_ = false;
	
		if(index_ != 0)
		{
			delete index_;                                                                                                               
			index_ = 0;                                                                                                                  
		}                                                                                                                                    
                                                                                                                                      	
		lastUpdate_ = toolbox::TimeVal::gettimeofday();                                                                                      
		mutex_.give();                                                                                                                       
	}                                                                                                                                            
	catch (xdata::exception::Exception& e)                                                                                                       
	{                                                                                                                                            
        	std::stringstream msg;                                                                                                               
        	msg << "Failed to reset flashlist '" << name_ << "'";                                  
        	mutex_.give();                                                                                                                       
        	XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);                                                                            
	}                                                                                                                                            
                                                                                                                                                     
}                                                                                                                                                    

void xmas::collector2g::Collector::clear(const std::string& name, const std::string& value)

{
	// Clear all rows in the table that match a name value pair
	
	mutex_.take();
	
	// If the table is empty, just return
	if( data_.isNull() )
	{
		mutex_.give();
		return;
	}
	
	// Check if the column exists. If it doesn't silently return
	const std::map<std::string, std::string, xdata::Table::ci_less >& tableDef = data_->getTableDefinition();
	if (tableDef.find(name) == tableDef.end())
	{
		mutex_.give();
		return;
	}

	// Clear index before operation, since if an error happens, we have no
	// means to rewind the operation and the index must be invalidated anyway
	//
	if(index_ != 0)
	{
		delete index_;
		index_ = 0;
	}

	xdata::Table::iterator ti = data_->begin();
	while (ti != data_->end())
	{               
		try
		{ 
                	xdata::Serializable* s = (*ti).getField(name);
			if (s->toString() == value)
			{
				ti = data_->erase(ti);
			}
			else
			{
				ti++;
			}
		}
		catch (xdata::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to erase rows from flashlist '" << name_ << "' matching (name, value): (" << name << ", " << value << ")";
			mutex_.give();
			XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
		}
        }
	
	// Indicate that an erase has been performed and that therefore the data have changed
	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	
	mutex_.give();
}

bool xmas::collector2g::Collector::match 
(
	xdata::Table::Reference& table,
	size_t row, 
	std::map<std::string, std::string>& filter
)
{
	// Algorithm requires that all filter expressions match!
	//
	std::map<std::string, std::string>::iterator i;
	for (i = filter.begin(); i != filter.end(); ++i)
	{	
		try
		{
			xdata::Serializable * s = table->getValueAt(row, (*i).first);
			if (s->toString() != (*i).second)
			{
				return false;
			}
		}
		catch (xdata::exception::Exception& e)
		{
			// doesn't match the column name
			return false;
		}
	}
	return true;
}

toolbox::task::AsynchronousEventDispatcher& xmas::collector2g::Collector::getDispatcher()
{
	return dispatcher_;
}

bool  xmas::collector2g::Collector::hasChanged()
{
	toolbox::TimeVal current = toolbox::TimeVal::gettimeofday(); 
	bool changed = false;
	mutex_.take();
	changed = (lastUpdate_ > lastOutput_) && ( (double)(current - lastOutput_) > (double)(settings_.getStagingPeriod())); 
	mutex_.give();
	return changed;
}


void xmas::collector2g::Collector::actionPerformed(toolbox::Event& e) 
{
	if ( e.type() == "urn:xmas-collecto2g-event:IncomingData" )
	{	
		toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
		
		xmas::collector2g::IncomingDataEvent & event = dynamic_cast<xmas::collector2g::IncomingDataEvent&>(e);
		
		LOG4CPLUS_DEBUG(event.collector_->getApplicationLogger(), "Received data for flashlist "<< event.plist_.getProperty("urn:xmas-flashlist:name"));	
		
		xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) event.ref_->getDataLocation(), event.ref_->getDataSize());		
		xdata::Table * t = new xdata::Table();
		
		try
		{			
			serializer_.import(t, &inBuffer);			
			
		}
		catch (xdata::exception::Exception& ec)
		{
			delete t;
			event.ref_->release();
			std::stringstream msg;
			msg << "Failed to deserialize flashlist table '";
			msg << event.plist_.getProperty("urn:xmas-flashlist:name") << "'";
			XCEPT_DECLARE_NESTED (xmas::collector2g::exception::Exception, ex, msg.str(), ec);
			event.collector_->notifyQualified("error",ex);
			return;
		}
		
		xdata::Table::Reference  table (t);
		
		toolbox::TimeVal stopDeserialize = toolbox::TimeVal::gettimeofday();
		double deserializeTime = (double) stopDeserialize - (double) start;
		if (averageDeserializeTime_ > 0.0)
		{
			averageDeserializeTime_ = (averageDeserializeTime_ + deserializeTime)/2;			
		}
		else
		{
			averageDeserializeTime_ = deserializeTime;
		}
		
		event.ref_->release();
		
		start = toolbox::TimeVal::gettimeofday();
		
		try
		{
			this->mergeData(table, event.plist_);
		}
		catch (xmas::exception::Exception& ec)
		{
			std::stringstream msg;
			msg << "Failed to merge flashlist table '";
			msg << event.plist_.getProperty("urn:xmas-flashlist:name") << "'";
			XCEPT_DECLARE_NESTED (xmas::collector2g::exception::Exception, ex, msg.str(), ec);
			event.collector_->notifyQualified("fatal",ex);
			return;
		}
			
		mutex_.take();
		++dataEventCounter_;	

		toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
		double period = (now - lastOutput_);
			
		averageMergeRate_ = dataEventCounter_/period;
				
		toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
		
		double mergeTime = (double)(stop - start);
		
		if (averageMergeTime_ > 0.0)
		{
			averageMergeTime_ = (averageMergeTime_ + mergeTime)/2;			
		}
		else
		{
			averageMergeTime_ = mergeTime;
		}

		mutex_.give();
	}
}

xmas::collector2g::Topic  xmas::collector2g::Collector::getTopic()
{
	return settings_;
}

std::string xmas::collector2g::Collector::getName()
{
	return name_;
}

double xmas::collector2g::Collector::getAverageMergeTime()
{
	return averageMergeTime_;
}

double xmas::collector2g::Collector::getDataMergeRate()
{
	return averageMergeRate_;
}

double xmas::collector2g::Collector::getAverageDeserializeTime()
{
	return averageDeserializeTime_;
}

double xmas::collector2g::Collector::getDataDeserializeRate()
{
	return averageDeserializeRate_;
}


size_t xmas::collector2g::Collector::getDataEventCounter()
{
	return dataEventCounter_;
}

void xmas::collector2g::Collector::incrementSerializationFailedCounter()
{
        monitorSerializationFailedCounter_++;
}

void xmas::collector2g::Collector::incrementFireCounter()
{
        monitorFireCounter_++;
}

void xmas::collector2g::Collector::incrementInternalLossCounter()
{
        monitorInternalLossCounter_++;
}

void xmas::collector2g::Collector::incrementCommunicationLossCounter()
{
        monitorCommunicationLossCounter_++;
}

void xmas::collector2g::Collector::incrementMemoryLossCounter()
{
        monitorMemoryLossCounter_++;
}

void xmas::collector2g::Collector::incrementUnassignedLossCounter()
{
        monitorUnassignedLossCounter_++;
}

xdata::UnsignedInteger64T xmas::collector2g::Collector::getFireCounter()
{
        return monitorFireCounter_;
}

xdata::UnsignedInteger64T xmas::collector2g::Collector::getSerializationFailedCounter()
{
        return monitorSerializationFailedCounter_;
}

xdata::UnsignedInteger64T xmas::collector2g::Collector::getInternalLossCounter()
{
        return monitorInternalLossCounter_;
}

xdata::UnsignedInteger64T xmas::collector2g::Collector::getCommunicationLossCounter()
{
        return monitorCommunicationLossCounter_;
}

xdata::UnsignedInteger64T xmas::collector2g::Collector::getMemoryLossCounter()
{
        return monitorMemoryLossCounter_;
}

xdata::UnsignedInteger64T xmas::collector2g::Collector::getUnassignedLossCounter()
{
        return monitorUnassignedLossCounter_;
}

