###############################################################################

# The addresses of the boards/applications to connect to.
RF2TTC = "rf2ttc"
RF2TTC_VME = "vme:rf2ttc:9"
CPM = "cpm"
ICIS = ["ici41", "ici61", "ici62", "ici63", "ici64", "ici65", "ici71"]
APVES = ["apve41", "apve61", "apve62", "apve63", "apve64", "apve71"]
# PIS = ["pi41", "pi61", "pi62", "pi63", "pi64", "pi65", "pi71"]
PIS = ["pi41"]
PHASEMON = "phasemon"

###############################################################################
