#!/usr/bin/env python

###############################################################################
## A little helper to run the nose test runner for TCDS tests. The
## only really thing it does is fix the suppression of the uhal
## output.
###############################################################################

import nose
import os
import sys

from nose_plugin_manager import PythonPathPluginManager

###############################################################################

def redirect_stdout():
    # This is important when redirecting to files.
    sys.stdout.flush()

    # Duplicate stdout (file descriptor 1) to a different file
    # descriptor number
    newstdout = os.dup(1)

    # The /dev/null device is used just to discard what is being
    # printed.
    devnull = os.open("/dev/null", os.O_WRONLY)

    # Duplicate the file descriptor for /dev/null and overwrite the
    # value for stdout (file descriptor 1).
    os.dup2(devnull, 1)

    # Close devnull after duplication (since it's no longer needed).
    os.close(devnull)

    # Use the original stdout to still be able to print to stdout
    # within python.
    sys.stdout = os.fdopen(newstdout, "w")

    # End of redirect_stdout().

###############################################################################

if __name__ == "__main__":

    # NOTE: This is a bit of a trick. The uhal library sends a lot of
    # info to stdout that cannot be suppressed by the normal nose
    # capture plugin. So we play a little trick and already redirect
    # all of stdout before calling the nose test runner.
    args = sys.argv
    if not (("-s" in args) or ("--nocapture" in args)):
        redirect_stdout()
    manager = PythonPathPluginManager([("nose_nice_output", "NiceOutput")])
    nose.run(config=nose.config.Config(plugins=manager))

    print "Done"

###############################################################################
