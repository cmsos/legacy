#!/usr/bin/env python

###############################################################################
## Utility to quickly switch away and switch back the signal from one
## SFP RX to the TTC FMC input.
###############################################################################

import time
import uhal
from pytcds.phasemon.tcds_phasemon import get_phasemon_hw

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

if __name__ == "__main__":

    target = "phasemon"

    uhal.disableLogging()

    phasemon = get_phasemon_hw(target)
    phasemon.write("phasemon.ttc_fmc_signal_select", 0x1)
    time.sleep(2)
    phasemon.write("phasemon.ttc_fmc_signal_select", 0x0)
    time.sleep(1)
        # pi.write("pi.tts_output_control.tts_out_disabled_value", 0x8)

    print "Done"

###############################################################################
