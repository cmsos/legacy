#include "tcds/apve/APVHistContents.h"

#include <iomanip>
#include <sstream>

#include "tcds/apve/Definitions.h"
#include "tcds/apve/Utils.h"

tcds::apve::APVHistContents::APVHistContents(std::vector<uint32_t> const dataIn)
{
  size_t numEntries = dataIn.size();
  entries_.reserve(numEntries);
  entries_.clear();
  for (std::vector<uint32_t>::const_iterator it = dataIn.begin();
       it != dataIn.end();
       ++it)
    {
      APVHistEntry newEntry(*it);
      entries_.push_back(newEntry);
    }
}

std::string
tcds::apve::APVHistContents::getJSONString() const
{
  std::stringstream res;

  res << "[";
  for (std::vector<APVHistEntry>::const_iterator it = entries_.begin();
       it != entries_.end();
       ++it)
    {
      res << "{";

      // The event number.
      res << "\"EventNumber\": \"" << std::dec << it->eventNumber() << "\"";
      res << ", ";

      // The corresponding pipeline address.
      res << "\"PipelineAddress\": \"" << std::dec << int(it->pipelineAddress()) << "\"";
      res << ", ";

      // The Gray code of the pipeline address.
      res << "\"PipelineAddressGrayCode\": \""
          << "0x" << std::hex << std::setw(2) << std::setfill('0') << int(pipelineAddressToGrayCode(it->pipelineAddress()))
          << "\"";
      res << ", ";

      // The difference in pipeline address with respect to the
      // previous entry.
      // NOTE: The entries are logged in reverse chronological order.
      std::stringstream deltaStr;
      if (it != (entries_.end() - 1))
        {
          int delta = addressDifference(it->pipelineAddress(), (it + 1)->pipelineAddress());
          deltaStr << std::dec << delta;
        }
      else
        {
          deltaStr << "-";
        }
      res << "\"PipelineAddressDelta\": \"" << deltaStr.str() << "\"";

      res << "}";

      if (it != (entries_.end() - 1))
        {
          res << ", ";
        }
    }
  res << "]";

  return res.str();
}

int
tcds::apve::APVHistContents::addressDifference(uint8_t const address0, uint8_t const address1)
{
  int res = address0 - address1;
  if (res < 0)
    {
      res += tcds::definitions::kAPVPipelineLength;
    }
  return res;
}
