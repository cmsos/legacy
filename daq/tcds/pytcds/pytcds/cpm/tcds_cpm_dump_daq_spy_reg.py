#!/usr/bin/env python

###############################################################################
## A little script to dump the contents of the CPM DAQ spy register.
###############################################################################

# NOTE: This is a bit quick-n-dirty.

import sys

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Dumper(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Dumper, self).__init__(description, usage, epilog)
        # End of pre_hook().

    def handle_args(self):
        # NOTE: All targets are hard-coded at the moment, so this does
        # not take any arguments.
        pass
        # End of handle_args().

    def main(self):

        target = "cpm-t1"
        self.validate_target(target, target_type=CmdLineBase.TARGET_TYPE_HW)
        cpm_info = self.setup.hw_targets[target]
        cpm = get_cpm_hw(cpm_info.dns_alias,
                         cpm_info.controlhub,
                         verbose=self.verbose)

        for entry_number in xrange(8):
            reg_name_base = "cpmt1.ipm.main.daq_spy.entry{0:d}".format(entry_number)
            header_lo = cpm.read(reg_name_base + ".header_lo")
            header_hi = cpm.read(reg_name_base + ".header_hi")
            trailer_lo = cpm.read(reg_name_base + ".trailer_lo")
            trailer_hi = cpm.read(reg_name_base + ".trailer_hi")

            fed_id = (header_lo & 0xfff00) >> 8
            bx_id = (header_lo & 0xfff00000) >> 20
            lv1_id = (header_hi & 0xffffff)
            event_type = (header_hi & 0xf000000) >> 56

            print "Entry {0:2d} header : 0x{1:08x} {2:08x}" \
                " -> " \
                "FED id = {3:d}, BX id = {4:4d}, L1A id = {5:6d}, event type = {6:d}" \
                .format(entry_number, header_hi, header_lo,
                        fed_id, bx_id, lv1_id, event_type)
            print "Entry {0:2d} trailer: 0x{1:08x} {2:08x}" \
                .format(entry_number, trailer_hi, trailer_lo)

        # End of main().

    # End of class Dumper.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to dump the contents of the CPM DAQ spy register."
    usage_str = "usage: %prog [options]"

    res = Dumper(desc_str, usage_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
