/*
 * XPathAPITest
 *
 * $Id: XPathAPITest.java,v 4.1 2007/03/26 14:40:33 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import junit.framework.*;
import junit.extensions.TestSetup;

// DOM
import org.w3c.dom.*;

// for NamespaceContext
import javax.xml.*;
import javax.xml.namespace.NamespaceContext;

/**
 * XPathAPITest
 */
public class XPathAPITest extends TestCase
{
	private class MyNamespaceContext extends DefaultNamespaceContext
	{
		public MyNamespaceContext() {
			put("prefix", "http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30");
		}
	}

	public XPathAPITest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(XPathAPITest.class);
	}

	public void testNamespace() throws Exception
	{
		DefaultNamespaceContext namespace = new DefaultNamespaceContext();

		assertNotNull(namespace);

		assertEquals(
				XMLConstants.NULL_NS_URI, namespace.getNamespaceURI("foo"));

		namespace.put("foo", "http://net.hep.cms.xdaqctl");
		assertEquals(
				"http://net.hep.cms.xdaqctl", namespace.getNamespaceURI("foo"));

		namespace.put("foo", "http://rcms.fm.app.hcal");
		assertEquals(
				"http://rcms.fm.app.hcal", namespace.getNamespaceURI("foo"));
	}

	public void testSelect() throws Exception
	{
		Element root = TestUtil.fileToDOM("test/xdaq/auto1.xml");

		NodeList list;
		Node node;

		// select all applications
		list = XPathAPI.selectNodeList(root, "//*[local-name()='Application']");

		assertNotNull(list);
		assertEquals(4, list.getLength());
		assertEquals("2", ((Element)list.item(2)).getAttribute("instance"));

		// zero-length list when not match found
		list = XPathAPI.selectNodeList(root, "//*[local-name()='NotExist']");

		assertNotNull(list);
		assertEquals(0, list.getLength());

		// returns the first found
		node = XPathAPI.selectSingleNode(root, "//*[local-name()='Application']");

		assertNotNull(node);
		assertEquals("0", ((Element)node).getAttribute("instance"));

		// returns null when not match found
		node = XPathAPI.selectSingleNode(root, "//*[local-name()='NotExist']");

		assertNull(node);

		// use name space
		DefaultNamespaceContext nsContext = new DefaultNamespaceContext();
		nsContext.put("prefix",
				"http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30");

		list = XPathAPI.selectNodeList(root, "//prefix:Application", nsContext);

		assertNotNull(list);
		assertEquals(4, list.getLength());
		assertEquals("2", ((Element)list.item(2)).getAttribute("instance"));
	}

	private static void oneTimeSetUp() throws Exception {}

	private static void oneTimeTearDown() throws Exception {}

	public static Test suite()
	{
		TestSuite suite = new TestSuite(XPathAPITest.class);

		TestSetup wrapper = new TestSetup(suite) {
			protected void setUp() throws Exception { oneTimeSetUp() ; }
			protected void tearDown() throws Exception { oneTimeTearDown(); }
		};

		return wrapper;
	}
}

// End of file
// vim: set ts=4 sw=4:
