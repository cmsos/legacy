#ifndef _bril_dip_VerboseGuard_h_
#define _bril_dip_VerboseGuard_h_
#include "toolbox/task/Guard.h"
#include <string>
namespace bril{
  namespace dip{
    static int guardcounter = 0;
    template <typename Mutex>class VerboseGuard: public toolbox::task::Guard<Mutex>{
    public: 
      explicit VerboseGuard(const std::string& caller, Mutex& m) : toolbox::task::Guard<Mutex>(m), m_caller(caller){
	guardcounter+=1;
	printf("%s Enter guard %d\n",m_caller.c_str(), guardcounter);
      }
      ~VerboseGuard(){
	printf("%s Leave guard %d\n",m_caller.c_str(), guardcounter);
      }
    private:
      std::string m_caller;
    };
  }}
#endif
