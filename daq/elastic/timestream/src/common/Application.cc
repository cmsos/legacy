// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/


#include <sstream>
#include <string>
#include <iostream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"
#include "toolbox/net/URN.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "elastic/timestream/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "elastic/timestream/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xmas/exception/Exception.h"

#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/InputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdata/Integer.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/TimeVal.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdata/TableIterator.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/Guard.h"
#include "toolbox/string.h"

#include "xoap/DOMParserFactory.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

#include <stdio.h>
#include <curl/curl.h>

#include "elastic/api/Cluster.h"
#include "elastic/api/Stream.h"

XDAQ_INSTANTIATOR_IMPL (elastic::timestream::Application);

XERCES_CPP_NAMESPACE_USE

const std::string elastic::timestream::Application::ElasticNamespace = "http://xdaq.web.cern.ch/xdaq/xsd/2016/elastic";
const std::string elastic::timestream::Application::ElasticSignature = "urn:cmsos:xdaq-timestream";

elastic::timestream::Application::FlashListDefinition::FlashListDefinition(DOMDocument* doc, const std::string& href): xmas::FlashListDefinition(0)
{
	this->setProperty("location", href); // must be url#qname
	this->setProperty ("local", "false");
	this->parse(doc, href);
}

elastic::timestream::Application::Application(xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL, false)
{

	b2in::nub::bind(this, &elastic::timestream::Application::onMessage);

	toolbox::task::getWorkLoopFactory()->getWorkLoop("elastic-timestream-dispatcher", "waiting")->activate();

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/elastic/timestream/images/elasticsearch.png");

	sampleTime_ = "PT5S";
	getApplicationInfoSpace()->fireItemAvailable("sampleTime",&sampleTime_);

	enable_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("enable", &enable_);

	// optional broker properties
	flashlistSettingsBaseUrl_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("flashlistSettingsBaseUrl", &flashlistSettingsBaseUrl_);

	elasticsearchClusterUrl_ = "unknown";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchClusterUrl", &elasticsearchClusterUrl_);


	elasticsearchDefaultIndexStoreType_ = "niofs";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchDefaultIndexStoreType", &elasticsearchDefaultIndexStoreType_);


	elasticsearchConnectionForbidReuse_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchConnectionForbidReuse", &elasticsearchConnectionForbidReuse_);

	httpVerbose_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("httpVerbose", &httpVerbose_);

	tcpNoDelay_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("tcpNoDelay", &tcpNoDelay_);

	numberOfChannels_ = 1;
	this->getApplicationInfoSpace()->fireItemAvailable("numberOfChannels", &numberOfChannels_);

	maxBackoffRetries_ = 10;
	this->getApplicationInfoSpace()->fireItemAvailable("maxBackoffRetries", &maxBackoffRetries_);

	backoffTimeSlot_ = 99; //99 milliseconds
	this->getApplicationInfoSpace()->fireItemAvailable("backoffTimeSlot", &backoffTimeSlot_);

	this->committedPoolSize_ = 0x100000 * 20; // 20 MB default
	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);

	this->committedQueueSize_ = 2048;
	getApplicationInfoSpace()->fireItemAvailable("committedQueueSize", &committedQueueSize_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &elastic::timestream::Application::Default, "Default");

	xgi::bind(this,  &elastic::timestream::Application::displayFlashlist, "displayFlashlist");
	xgi::bind(this,  &elastic::timestream::Application::displayFlashlistMapping, "displayFlashlistMapping");
	//xgi::bind(this,  &elastic::timestream::Application::displayLatestData, "displayLatestData");

	// easy curl opt
	xgi::bind(this,  &elastic::timestream::Application::enable, "enable");
	xgi::bind(this,  &elastic::timestream::Application::disable, "disable");
	xgi::bind(this,  &elastic::timestream::Application::query, "query");
	xgi::bind(this,  &elastic::timestream::Application::stats, "stats");

	//SOAP Binding
	xoap::bind(this, &elastic::timestream::Application::report, "report",  xmas::NamespaceUri );

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);

	backoff_ = new toolbox::Backoff(::getpid(), toolbox::Backoff::Fibonacci);

	factory_ = toolbox::mem::getMemoryPoolFactory();

	deadBand_ = false;
}

elastic::timestream::Application::~Application()
{
	delete backoff_;
	delete adispatcher_;
}

void elastic::timestream::Application::actionPerformed(xdata::Event& event)
{
	counter_ = 0;
	rate_ = 0.0;
	totalDocumentsCreatedCounter_ = 0;

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		try
		{
			toolbox::net::URN urn("toolbox-mem-pool", "timestream");
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			spoolerPool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

			spoolerPool_->setHighThreshold((unsigned long) ((xdata::UnsignedLongT)committedPoolSize_ * 0.9));
			spoolerPool_->setLowThreshold((unsigned long) ((xdata::UnsignedLongT)committedPoolSize_ * 0.5));
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Cannot create default memory pool", e);
		}

		adispatcher_ = new toolbox::task::AsynchronousEventDispatcher ("elastic-timestream-dispatcher", "waiting", 0.6, committedQueueSize_);
		adispatcher_->addActionListener(this);
		toolbox::Properties properties;
		if ( httpVerbose_ )
		{
			properties.setProperty("urn:es-api-stream:CURLOPT_VERBOSE","true");
		}
		if ( tcpNoDelay_ )
		{
			properties.setProperty("urn:es-api-stream:CURLOPT_TCP_NODELAY","true");
		}

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Attaching to elastic search...");

		if ( (bool)(elasticsearchConnectionForbidReuse_) )
		{
			properties.setProperty("urn:es-api-stream:CURLOPT_FORBID_REUSE", "true");
		}

		properties.setProperty("urn:es-api-cluster:number-of-channels", numberOfChannels_.toString());

		member_ = new elastic::api::Member(this, properties);
		if ( enable_ )
		{
			this->applyCollectorSettings();
		}

		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if ( !toolbox::task::getTimerFactory()->hasTimer("urn:elastic:statistics-timer") )
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:elastic:statistics-timer");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:elastic:statistics-timer");
		}
		toolbox::TimeVal start(toolbox::TimeVal::gettimeofday().sec());
		toolbox::TimeInterval interval;
		interval.fromString(sampleTime_.toString());

		timer->scheduleAtFixedRate(start, this, interval, 0, "urn:elastic:statistics-task");
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(elastic::timestream::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void elastic::timestream::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	std::string timerTaskName = e.getTimerTask()->name;
	if ( timerTaskName == "urn:elastic:statistics-task" )
	{
		toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
		double delta = (double)now - (double)lastTime_;
		rate_ = (double)(counter_ / delta);
		totalDocumentsCreatedCounter_ = totalDocumentsCreatedCounter_ + counter_;
		counter_ = 0;
		lastTime_ = now;

		toolbox::task::Guard<toolbox::BSem> guard(mutex_);
		for (std::map<std::string, struct Statistics>::iterator i = statistics_.begin(); i != statistics_.end(); i++)
		{
			(*i).second.rate = (double)(((*i).second.successCounter - (*i).second.lastCount) / delta);
			(*i).second.lastCount = (*i).second.successCounter;

			(*i).second.indexOperationRate = (double)(((*i).second.indexOperationTotal - (*i).second.lastIndexOperationCount) / delta);
			(*i).second.lastIndexOperationCount = (*i).second.indexOperationTotal;
		}
	}
}

void elastic::timestream::Application::onMessage(toolbox::mem::Reference* ref, xdata::Properties& plist) 
{
	// some protections

	if (ref == 0)
		return;

	std::string qname = plist.getProperty("urn:xmas-flashlist:name");

	if ( qname == "" )
	{
		ref->release();
		return;
	}

	toolbox::mem::Reference * localRef;

	if (spoolerPool_->isHighThresholdExceeded())
	{
		// over threshold , discard incoming message and enter deadband
		deadBand_ = true;
		ref->release();
		statistics_[qname].lostMemoryFull++;

		std::stringstream msg;
		msg << "High memory usage, threshold reached, monitoring data is being discarded";
		XCEPT_DECLARE(elastic::timestream::exception::Exception, e, msg.str());
		this->notifyQualified("warning", e);

		return;
	}
	else
	{
		if ( deadBand_ )
		{
			if (spoolerPool_->isLowThresholdExceeded())
			{
				// we are in deadband, threfore discard incoming message
				ref->release();
				statistics_[qname].lostMemoryFull++;
				return;
			}
			else
			{
				deadBand_ = false;
			}
		}
	}

	size_t size = ref->getDataSize();

	try
	{
		localRef = factory_->getFrame(spoolerPool_, size);
		::memcpy(localRef->getDataLocation(),ref->getDataLocation(), size);
		ref->release();
	}
	catch (toolbox::mem::exception::Exception & ex)
	{

		ref->release();
		statistics_[qname].lostMemoryFull++;

		std::stringstream msg;
		msg << "Failed to allocate post receive buffer";
		XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, e, msg.str(), ex);
		this->notifyQualified("error", e);
		return;
	}


	elastic::timestream::Event * ep = new elastic::timestream::Event(plist, localRef);
	toolbox::task::EventReference e(ep);

	try
	{
		adispatcher_->fireEvent(e);
	}
	catch(toolbox::task::exception::Overflow & e)
	{
		localRef->release();
		statistics_[qname].lossQueueFullCounter++;
		std::stringstream msg;
		msg << "Failed to dispatch event report for flashlist '" << qname << "'";
		XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, msg.str(), e);
		this->notifyQualified("warning", q);
	}
	catch(toolbox::task::exception::OverThreshold& e)
	{
		localRef->release();
		statistics_[qname].lossQueueFullCounter++;
		// report is lost, keep counting
	}
	catch(toolbox::task::exception::InternalError& e)
	{
		// we do not know what happen to this reference, therefore we cannot free , this reference can be considered lost
		//localRef->release();

		statistics_[qname].lossQueueFullCounter++;
		std::stringstream  msg;
		msg << "Failed to dispatch event report for flashlist '" << qname << "' , a memory reference can be lost";
		XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
	}
}

void elastic::timestream::Application::processEventData(toolbox::mem::Reference* msg, xdata::Properties& plist) 
{
	toolbox::task::Guard<toolbox::BSem> guard(mutex_);

	if ( !enable_ )
	{
		if ( msg != 0 )
			msg->release();
		return;
	}


	if ( msg == 0 )
	{
		// these messages with properties only do not belong to this application ! Let's ignore them
		//std::map<std::string, std::string, std::less<std::string> >& p = plist.getProperties();

		//for (std::map<std::string, std::string, std::less<std::string> >::iterator i = p.begin(); i != p.end(); i++)
		//{
		//std::cout << (*i).first << " - " << (*i).second <<std::endl;
		//}
		//XCEPT_DECLARE(elastic::timestream::exception::Exception, q, "no data no valid monitoring work to be done, ignore it");
		//this->notifyQualified("warning",q);
		return;
	}

	// report data either direct receive from xmas::probe or through a notification  from b2in-eventing
	if ( (plist.getProperty("urn:b2in-protocol:action") == "flashlist") || (plist.hasProperty("urn:xmas-flashlist:name") && (plist.getProperty("urn:b2in-eventing:action") == "notify")) )
	{
		std::string zoneName = this->getApplicationContext()->getDefaultZoneName();

		std::string qname = plist.getProperty("urn:xmas-flashlist:name");
		toolbox::net::URN flashlistURN(qname);
		std::string fname = flashlistURN.getNSS();

		if ( blackFlashList_.find(qname) != blackFlashList_.end() )
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Flashlist " << qname << "is blacklisted");
			if ( msg != 0 )
				msg->release();
			return;
		}

		// check if flashlist is in collector.settings
		if ( activeFlashList_.find(qname) != activeFlashList_.end() )
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "check mapping already enabled '" << activeFlashList_[qname] << "' for '" << qname << "'");

			//******** Creating template *************
			//Check if and index has already been created
			if ( activeFlashList_[qname] == "" )
			{
				//Waiting for some random time in order to prevent every node from checking if template exists at the same time
				backoff_->backoff(1, backoffTimeSlot_);

				toolbox::net::URN flashlistURN(qname);
				std::string fname = flashlistURN.getNSS();
				unsigned int n = 1;
				try
				{
					while ( !this->templateExists(zoneName, fname) && (n < maxBackoffRetries_) )
					{
						if ( this->elasticHealthy() )
						{
							try
							{
								this->createTemplate(zoneName, fname, settings_[qname]);
							}
							catch(elastic::timestream::exception::Exception& e)
							{
								std::stringstream info;
								info << "Cannot create template for '" << qname << "' in ES cluster";
								XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, info.str(), e);
								this->notifyQualified("error", q);

								if ( msg != 0 )
									msg->release();
								statistics_[qname].lossCounter++;
								this->resetActiveFlashlists();
								return;
							}
							LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Template was created for " << qname);
						}
						else {
							//Function waits according to backoff algorithm
							backoff_->backoff(n++, backoffTimeSlot_);
						}
					}
				}
				catch(elastic::timestream::exception::Exception& e)
				{
					std::stringstream info;
					info << "Failed creating template in Elasticsearch cluster";
					XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, info.str(), e);
					this->notifyQualified("error", q);

					if ( msg != 0 )
						msg->release();
					statistics_[qname].lossCounter++;
					this->resetActiveFlashlists();
					return;
				}
				if ( n >= maxBackoffRetries_ )
				{
					LOG4CPLUS_WARN(this->getApplicationLogger(), "Timed out while trying to create a template");
					if ( msg != 0 )
						msg->release();
					return;
				}
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Mapping has been validated for " << qname);
			}

			//********** Creating index *****************
			std::string starttime;
			std::string currentIndexName = this->createTimedIndexName(zoneName, fname, settings_[qname], starttime);
			json_t* jsonproperties = json_pack("{s:{s:{s:{s:s}}}}","mappings", fname.c_str(), "_meta", "starttime", starttime.c_str());

			//Check if current index is still in the right (current) time interval
			if ( activeFlashList_[qname] != currentIndexName )
			{
				//Waiting for some random time in order to prevent every node from checking if index exists at the same time
				backoff_->backoff(1, backoffTimeSlot_);

				toolbox::TimeVal before = toolbox::TimeVal::gettimeofday();
				unsigned int n = 1;
				try
				{
					while ( !this->indexExists(currentIndexName) && (n < maxBackoffRetries_) )
					{
						if ( this->elasticHealthy() )
						{
							try
							{
								this->createIndex(currentIndexName, jsonproperties);
							}
							catch(elastic::timestream::exception::Exception& e)
							{
								std::stringstream info;
								info << "Cannot create index '" << currentIndexName << "' in ES cluster";
								XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, info.str(), e);
								this->notifyQualified("error", q);

								if ( msg != 0 )
									msg->release();
								statistics_[qname].lossCounter++;
								this->resetActiveFlashlists();

								json_decref(jsonproperties);
								return;
							}
						}
						else
						{
							//Function waits according to backoff algorithm
							backoff_->backoff(n++, backoffTimeSlot_);
						}
					}
				}
				catch(elastic::timestream::exception::Exception & e)
				{
					std::stringstream info;
					info << "Failed while creating index '" << currentIndexName << "' in ES cluster";
					XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, info.str(), e);
					this->notifyQualified("error", q);

					if ( msg != 0 )
						msg->release();
					statistics_[qname].lossCounter++;
					this->resetActiveFlashlists();

					json_decref(jsonproperties);
					return;
				}

				if ( n >= maxBackoffRetries_)
				{
					LOG4CPLUS_WARN(this->getApplicationLogger(), "Timed out while trying to create an index");
					if ( msg != 0 )
						msg->release();

					json_decref(jsonproperties);
					return;
				}

				try
				{
					//Checking shards in case we move to a new index
					this->checkShards(currentIndexName);
				}
				catch(elastic::timestream::exception::Exception& e)
				{
					std::stringstream info;
					info << "Shard check failed for index '" << currentIndexName << "' in ES cluster";
					XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, info.str(), e);
					this->notifyQualified("error", q);

					if ( msg != 0 )
						msg->release();
					statistics_[qname].lossCounter++;
					this->resetActiveFlashlists();

					json_decref(jsonproperties);
					return;
				}
				toolbox::TimeVal after = toolbox::TimeVal::gettimeofday();
				toolbox::TimeInterval delta = after - before;
				statistics_[qname].lastIndexCreationTime = (double)delta * 1000; // milliseconds
				size_t bin = 0;
				if ( statistics_[qname].lastIndexCreationTime  >= 1 )
				{
					bin = floor(log2(statistics_[qname].lastIndexCreationTime)) +1;
				}
				if ( statistics_[qname].indexCreationTime.find (bin) == statistics_[qname].indexCreationTime.end() )
				{
					statistics_[qname].indexCreationTime[bin] = 0; // initialize if not created
				}
				statistics_[qname].indexCreationTime[bin]++;

				statistics_[qname].maxIndexCreationTime = MAX(statistics_[qname].lastIndexCreationTime, statistics_[qname].maxIndexCreationTime);
				statistics_[qname].minIndexCreationTime = MIN(statistics_[qname].lastIndexCreationTime, statistics_[qname].minIndexCreationTime);

				activeFlashList_[qname] = currentIndexName;

				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Index has been validated");
			}
			json_decref(jsonproperties);

			//Creating document
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Going to create a document " << activeFlashList_[qname] << " for " << qname);

			try
			{
				toolbox::TimeVal before = toolbox::TimeVal::gettimeofday();

				this->createDocument((char*)msg->getDataLocation(), msg->getDataSize(), qname, currentIndexName);
				// collect statistics for document creation
				toolbox::TimeVal after = toolbox::TimeVal::gettimeofday();
				toolbox::TimeInterval delta = after - before;

				statistics_[qname].lastIndexOperationTime = (double)delta * 1000;  // milliseconds

				statistics_[qname].indexOperationTimeSum += statistics_[qname].lastIndexOperationTime;
				statistics_[qname].indexOperationTotal++;

				size_t bin = 0;
				if ( statistics_[qname].lastIndexOperationTime  >= 1 )
				{
					bin = floor(log2(statistics_[qname].lastIndexOperationTime)) + 1;
				}
				if ( statistics_[qname].indexOperationTime.find (bin) == statistics_[qname].indexOperationTime.end() )
				{
					statistics_[qname].indexOperationTime[bin] = 0; // initialize if not created
				}
				statistics_[qname].indexOperationTime[bin]++;

				statistics_[qname].maxIndexOperationTime = MAX(statistics_[qname].lastIndexOperationTime, statistics_[qname].maxIndexOperationTime);
				statistics_[qname].minIndexOperationTime = MIN(statistics_[qname].lastIndexOperationTime, statistics_[qname].minIndexOperationTime);

			}
			catch(elastic::timestream::exception::Exception& e)
			{
				std::stringstream info;
				info << "Cannot create document for  '" << qname << "' in ES cluster (flashlist)";
				XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, info.str(), e);
				this->notifyQualified("error", q);
				this->resetActiveFlashlists();
				if ( msg != 0 )
					msg->release();
				statistics_[qname].lossCounter++;
				return;
			}
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Flashlist data has been indexed for " << qname);

		}
		else
		{
			std::stringstream info;
			info << "Cannot validate template for flashlist: '" << qname << "', flashlist is not configured for collection (this flashlist is blacklisted)";
			XCEPT_DECLARE(elastic::timestream::exception::Exception, q, info.str());
			this->notifyQualified("warning", q);
			// blacklist this name
			if ( msg != 0 )
				msg->release();
			blackFlashList_[qname] = true;
			return;
		}


	} // valid message

	if ( msg != 0 )
		msg->release();
}

void elastic::timestream::Application::checkShards(const std::string& iname) 
{
	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	try
	{
		std::string cname = cluster.getClusterName();
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	try
	{
		/*
		expected
		{
		  "_shards" : {
		    "total" : 10,
		    "successful" : 10,
		    "failed" : 0
		  },
		  "_all" : {
		    "primaries" : { },
		    "total" : { }
		  },
		  "indices" : {
		    "pippo" : {
		      "primaries" : { },
		      "total" : { }
		    }
		  }
		}
		 */

		size_t retries = 0;
		size_t maxRetries = maxBackoffRetries_;
		unsigned int totalShards = 0;
		unsigned int successfulShards = 0;

		do
		{
			json_t * result = cluster.getIndexStats(iname, "_shards");
			json_t * shards = json_object_get(result, "_shards");
			json_t * total = json_object_get(shards, "total");
			totalShards = (unsigned int)json_number_value(total);
			json_t * successful = json_object_get(shards, "successful");
			successfulShards = (unsigned int)json_number_value(successful);

			//releasing references
			json_decref(result);

			if ( totalShards == successfulShards )
				break;

			backoff_->backoff(++retries, backoffTimeSlot_);
		}
		while ( retries < maxRetries );

		if ( retries >= maxRetries )
		{
			std::stringstream msg;
			msg << "Checked shards of index '" << iname << "' timed out! total = " << totalShards
					<< ", successful = " << successfulShards << ", retries = " << retries;
			XCEPT_RAISE(elastic::timestream::exception::Exception, msg.str());
		}
		else
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Checked shards of index '" << iname << "', total = " << totalShards
					<< ", successful = " << successfulShards << ", retries = " << retries);
		}
	}
	catch(elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to check shards of index: '" << iname << "'";
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}
}

void elastic::timestream::Application::createIndex(const std::string& iname, json_t* properties) 
{
	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	try
	{
		// check if index exists otherwise create it
		if ( !cluster.exists(iname, "") )
		{
			json_t * result = 0;
			try
			{
				//expected {"acknowledged":true}
				result = cluster.createIndex(iname, properties);
				json_t * acknowledged = json_object_get(result, "acknowledged");
				if ( !json_boolean_value(acknowledged) )
				{
					std::string errorType = json_string_value(json_object_get(json_object_get(result, "error"), "type"));
					if ( errorType != "resource_already_exists_exception" )
					{
						std::stringstream msg;

						char * s = json_dumps(result, 0);
						std::string dump(s);
						free(s);
						msg << "failed to create index: '" << dump << "'";
						json_decref(result);
						XCEPT_RAISE(elastic::timestream::exception::Exception, msg.str());
					}
				}
				char * s = json_dumps(result, 0);
				std::string dump(s);
				free(s);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Created index : " << dump);
				json_decref(result);
			}
			catch(elastic::api::exception::Exception& e)
			{
				json_decref(result);
				std::stringstream msg;
				msg << "failed to create index with : '" << iname;
				XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
			}
		}
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not check index:" << iname<< " exists on url: '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}
}

void elastic::timestream::Application::createDocument(char* buffer, size_t size, const std::string& qname, const std::string& indexName) 
{
	//this->randomSleep(10); // used to test deadband algorithm
	//pause();

	toolbox::net::URN flashlistURN(qname);
	std::string fname = flashlistURN.getNSS();
	/*
	 * $ curl -XPUT 'http://localhost:9200/twitter/tweet/1' -d '{
	 *   "user" : "kimchy",
	 *   "post_date" : "2009-11-15T14:12:12",
	 *   "message" : "trying out Elasticsearch"
	 * }'
	 */
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "joining cluster ... ");
	elastic::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());


	xdata::Table* t = 0;
	try
	{
		t = this->getDataTable(buffer, size);
	}
	catch (elastic::timestream::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not extract data table from message for " << indexName << " with type : " << fname;
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	//if bigger than one then bulk
	//std::cout << "bulk is:" << t->getRowCount() << std::endl;
	size_t bulkSize = t->getRowCount();
	if ( bulkSize > 1 )
	{
		//do bulk
		std::stringstream bulk;

		std::vector<std::string> columns = t->getColumns();
		for (xdata::Table::iterator ti = t->begin(); ti != t->end(); ti++)
		{
			json_t * jsondata = 0;

			try
			{
				jsondata = this->tableRowToJSON(ti, columns, qname);//full qualified flashlist name
			}
			catch(elastic::timestream::exception::Exception& e)
			{
				delete t;
				std::stringstream msg;
				msg << "could not create data entry for bulk: " << indexName << " with type : " << fname;
				XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
			}

			char * s = json_dumps(jsondata, 0);
			std::string dataEntry(s);
			free(s);
			bulk << "{\"index\" : { \"_type\" : \"" << fname.c_str() << "\"}}\n";
			bulk << dataEntry << "\n";
			//add metadata
			json_decref(jsondata);
		}

		//bulk the lot after
		try
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Bulk Indexing data : " << bulk.str());
			json_t * result =cluster.bulkIndex(indexName, "", bulk);

			json_t * errors = json_object_get(result, "errors");
			if ( (errors == NULL) || ((errors != NULL) && json_boolean_value(errors)) )
			{
				std::stringstream msg;
				char * s = json_dumps(result, 0);
				std::string dump(s);
				free(s);
				msg << "could not bulk index data for : " << indexName << " and type : " << fname << " result: " << dump;
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
				json_decref(result);
				XCEPT_RAISE(elastic::timestream::exception::Exception, msg.str());
			}

			char * s = json_dumps(result, 0);
			std::string dump(s);
			free(s);
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Bulk Indexing data result:  " << dump);
			json_decref(result);

			statistics_[qname].bulkCounter++;
			statistics_[qname].lastBulkSize = bulkSize;
			statistics_[qname].successCounter += bulkSize;
			if (bulkSize > statistics_[qname].maxBulkSize)
			{
				statistics_[qname].maxBulkSize = bulkSize;
			}
			counter_ = counter_ + bulkSize;
		}
		catch (elastic::api::exception::Exception& e)
		{
			delete t;
			std::stringstream msg;
			msg << "could not bulk index data for : " << indexName << " and type : " << fname;
			XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
		}

	}
	//else carry on as usual
	else
	{
		std::vector<std::string> columns = t->getColumns();
		for (xdata::Table::iterator ti = t->begin(); ti != t->end(); ti++)
		{
			json_t * jsondata = 0;
			try
			{
				jsondata = this->tableRowToJSON(ti, columns, qname);//full qualified flashlist name
			}
			catch(elastic::timestream::exception::Exception& e)
			{
				delete t;
				std::stringstream msg;
				msg << "could not create data entry for : " << indexName << " with type : " << fname;
				XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
			}

			// inject into elasticsearch here ...

			//carry on as normal
			try
			{
				char * s;
				s = json_dumps(jsondata, 0);
				std::string dump(s);
				free(s);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data : " << dump);
				json_t * result = cluster.index(indexName, fname, "", jsondata);
				s = json_dumps(result, 0);
				dump = s;
				free(s);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data result:  " << dump);

				json_t * error =json_object_get(result, "error");
				if ( error != NULL )
				{
					std::stringstream msg;
					s = json_dumps(result, 0);
					std::string dump(s);
					free(s);
					msg << "could not index data for : " << indexName << " and type : " << fname << " result: " << dump;
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
					XCEPT_RAISE(elastic::timestream::exception::Exception, msg.str());
				}

				json_t * resultField =json_object_get(result, "result");
				if ( (resultField == 0) || (strcmp(json_string_value(resultField), "created") != 0) )
				{
					std::stringstream msg;
					s = json_dumps(result, 0);
					std::string dump(s);
					free(s);
					msg << "Could not index data in index: '" << indexName << "' with type: '" << fname << "' result: " << dump;
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
					XCEPT_RAISE(elastic::timestream::exception::Exception, msg.str());
				}

				s = json_dumps(result, 0);
				dump = s;
				free(s);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Successfully indexed data :  " << dump);

				json_decref(result);

				statistics_[qname].successCounter++;
				counter_ = counter_ + 1;
			}
			catch (elastic::api::exception::Exception& e)
			{
				delete t;
				json_decref(jsondata);
				std::stringstream msg;
				msg << "could not index data for : " << indexName << " and type : " << fname;
				XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
			}
			json_decref(jsondata);
		}
	}

	delete t;
}

json_t* elastic::timestream::Application::templateToJSON(const std::string& zoneName, xmas::FlashListDefinition* flashlist, elastic::timestream::Application::SettingsT& settings) 
{
	std::string name = flashlist->getProperty("name");
	std::string version = flashlist->getProperty("version");
	std::string key = flashlist->getProperty("key");
	toolbox::net::URN flashlistURN(name);
	std::string fname = flashlistURN.getNSS();

	std::string prefix = toolbox::tolower(zoneName + "-" + fname);
	json_t* jsontemplate = json_object();

	//Settings
	json_t* jsonsettings = json_object();
	json_object_set_new(jsontemplate, "settings", jsonsettings);

	json_t* jsonindex = json_object();
	json_object_set_new(jsonsettings, "index", jsonindex);

	json_object_set_new(jsonindex, "number_of_replicas", json_string(settings.numberOfReplicas.c_str()));

	json_t* jsonstore = json_object();
	json_object_set_new(jsonindex, "store", jsonstore);

	json_object_set_new(jsonstore, "type", json_string(settings.indexStoreType.c_str()));

	json_t* jsonmapper = json_object();
	json_object_set_new(jsonindex, "mapper", jsonmapper);

	json_object_set_new(jsonmapper, "dynamic", json_boolean(false));

	json_t* jsontranslog = json_object();
	json_object_set_new(jsonindex, "translog", jsontranslog);

	json_object_set_new(jsontranslog, "durability", json_string("async"));

	//Index patterns
	std::string tname = prefix + "_*";
	json_t* jsonindexpatternarray = json_array();
	json_array_append_new(jsonindexpatternarray, json_string(tname.c_str()));
	json_object_set_new(jsontemplate, "index_patterns", jsonindexpatternarray);

	//Aliases
	json_t* jsonaliases = json_object();
	json_object_set_new(jsontemplate, "aliases", jsonaliases);

	json_object_set_new(jsonaliases, prefix.c_str(), json_object());
	std::string aliasflash = prefix + "-flash";
	json_object_set_new(jsonaliases, aliasflash.c_str(), json_object());
	std::string aliasall = zoneName + "-all";
	json_object_set_new(jsonaliases, aliasall.c_str(), json_object());

	//Mappings
	json_t* jsonmappings = json_object();
	json_object_set_new(jsontemplate, "mappings", jsonmappings);

	json_t* jsonproperties = json_object();
	json_t* jsonitems = json_object();

	json_object_set_new(jsonmappings, fname.c_str(), jsonproperties);

	json_object_set_new(jsonproperties, "properties", jsonitems);

	// { "<flashlistname>" : {
	//        "properties": {

	std::vector<xmas::ItemDefinition*> items = flashlist->getItems();

	for (std::vector<xmas::ItemDefinition*>::iterator i = items.begin(); i != items.end(); i++)
	{
		std::string iname = (*i)->getProperty("name");
		json_t* jsondef = 0;
		try
		{
			jsondef = this->itemToJSON(*i, name); // recursive
		}
		catch(elastic::timestream::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "could not convert flashlist : " << name;
			json_decref(jsonmappings);
			XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
		}
		json_object_set_new(jsonitems, iname.c_str(), jsondef);
	}

	// add meta data
	json_t* jsondef = json_object();
	json_object_set_new(jsondef, "type", json_string("keyword"));
	json_object_set_new(jsondef, "store", json_boolean(true));
	json_object_set_new(jsondef, "index", json_boolean(true));
	json_object_set_new(jsonitems, "meta_unique_key", jsondef);
	jsondef = json_object();
	json_object_set_new(jsondef, "type", json_string("keyword"));
	json_object_set_new(jsondef, "store", json_boolean(true));
	json_object_set_new(jsondef, "index", json_boolean(true));
	json_object_set_new(jsonitems, "meta_hash_key", jsondef);

	//custom copy_to for hash key used for quick aggregation
	jsondef = json_object();
	json_object_set_new(jsondef, "type", json_string("keyword"));
	json_object_set_new(jsondef, "store", json_boolean(true));
	json_object_set_new(jsondef, "index", json_boolean(true));
	json_object_set_new(jsonitems, "flash_key", jsondef);

	// add creationtime_ for replacement of deprecated _timestamp
	jsondef = json_object();
	json_object_set_new(jsondef, "type", json_string("date"));
	json_object_set_new(jsondef, "store", json_boolean(false));
	json_object_set_new(jsondef, "index", json_boolean(true));
	json_object_set_new(jsondef, "format", json_string("epoch_millis"));
	json_object_set_new(jsonitems, "creationtime_", jsondef);

	// expirationtime_
	jsondef = json_object();
	json_object_set_new(jsondef, "type", json_string("date"));
	json_object_set_new(jsondef, "store", json_boolean(false));
	json_object_set_new(jsondef, "index", json_boolean(true));
	json_object_set_new(jsondef, "format", json_string("epoch_millis"));
	json_object_set_new(jsonitems, "expirationtime_", jsondef);

	// withdrawtime_
	jsondef = json_object();
	json_object_set_new(jsondef, "type", json_string("date"));
	json_object_set_new(jsondef, "store", json_boolean(false) );
	json_object_set_new(jsondef, "index", json_boolean(true));
	json_object_set_new(jsondef, "format", json_string("epoch_millis"));
	json_object_set_new(jsonitems, "withdrawtime_", jsondef);

	// add _meta with original flashlist xdaq definition
	json_t* meta = json_object();
	json_object_set_new(meta, "id", json_string(name.c_str()));
	json_object_set_new(meta, "signature", json_string(ElasticSignature.c_str()));
	json_object_set_new(meta, "zone", json_string(zoneName.c_str()));
	json_object_set_new(meta, "version", json_string(version.c_str()));
	json_object_set_new(meta, "key", json_string(key.c_str()));
	std::string hashkey = toolbox::printTokenSet(settings.hashKey, ",");
	json_object_set_new(meta, "hashKey", json_string(hashkey.c_str()));
	json_object_set_new(meta, "timetolive", json_string(settings.timeToLive.c_str()));
	json_object_set_new(meta, "timeinterval", json_string(settings.timeInterval.c_str()));
	json_object_set_new(meta, "flashinterval", json_string(settings.flashInterval.c_str()));
	if ( settings.openIntervalIsSet )
	{
		json_object_set_new(meta, "openinterval", json_string(settings.openInterval.c_str()));
	}

	// build meta flashlist
	json_t* definition = json_object();
	for (std::vector<xmas::ItemDefinition*>::iterator i = items.begin(); i != items.end(); i++)
	{
		//std::cout << " processing item: " <<  (*i)->getProperty("name") << std::endl;
		std::string iname = (*i)->getProperty("name");
		json_t* metadef = 0;
		try
		{
			metadef = this->itemToMETAJSON(*i, name); // recursive
		}
		catch(elastic::timestream::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "could not convert meta flashlist : " << name;
			json_decref(jsonmappings);
			XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
		}
		json_object_set_new(definition, iname.c_str(), metadef);
	}

	json_object_set_new(meta, "definition", definition);
	json_object_set_new(jsonproperties, "_meta", meta);

	json_object_set_new(jsonproperties, "dynamic", json_string("strict"));

	return jsontemplate;
}

json_t* elastic::timestream::Application::itemToJSON(xmas::ItemDefinition* itemdef, const std::string& fname) 
{
	json_t* jsondef = json_object();

	std::string name = itemdef->getProperty("name");
	std::string primitivetype = itemdef->getProperty("type");

	bool index = false;
	if ( itemdef->hasProperty("elastic:index") )
	{
		if ( itemdef->getProperty("elastic:index") == "true" )
		{
			index = true;
		}
	}

	bool store = false;
	if ( itemdef->hasProperty("elastic:store") )
	{
		if ( itemdef->getProperty("elastic:store") == "true" )
		{
			store = true;
		}
	}

	bool docvalues = false;
	if ( itemdef->hasProperty("elastic:doc_values") )
	{
		if ( itemdef->getProperty("elastic:doc_values") == "true" )
		{
			docvalues = true;
		}
	}

	std::vector<xmas::ItemDefinition*> defs = itemdef->getItems();

	// sub properties
	if ( defs.size() > 0 )
	{
		json_t* jsonitems = json_object();
		// { "<name>" : {
		//        "properties": {
		for (std::vector<xmas::ItemDefinition*>::iterator i = defs.begin(); i != defs.end(); i++)
		{
			std::string iname = (*i)->getProperty("name");
			json_t* jsonitem = this->itemToJSON(*i, ""); // recursive
			json_object_set_new(jsonitems, iname.c_str(), jsonitem);
		}
		// }
		json_object_set_new(jsondef, "properties", jsonitems);
	}
	else
	{
		try
		{
			std::string elasticsearchType = this->xdaqToElasticsearchType(primitivetype.c_str());
			// { "<name>" : { "type" : "primitivetype", ... }
			json_object_set_new(jsondef, "type", json_string(elasticsearchType.c_str()));
			json_object_set_new(jsondef, "store", json_boolean(store));
			json_object_set_new(jsondef, "index", json_boolean(index));
			json_object_set_new(jsondef, "doc_values", json_boolean(docvalues));

			std::list<std::string> copy_to_list;
			if ( settings_.find(fname) != settings_.end() )
			{
				std::set<std::string>& key = settings_[fname].uniqueKey;
				if ( key.find(name) != key.end() ) // valid only for first level flashlist items
				{
					copy_to_list.push_back("meta_unique_key");
				}
			}

			if ( settings_.find(fname) != settings_.end() )
			{
				std::set<std::string>& key = settings_[fname].hashKey;
				if ( key.find(name) != key.end() ) // valid only for first level flashlist items
				{
					copy_to_list.push_back("meta_hash_key");
				}
			}
			if ( copy_to_list.size() > 0 )
			{
				json_t* jsonvector = json_array();
				for (std::list<std::string>::iterator i = copy_to_list.begin(); i != copy_to_list.end(); i++)
				{
					//std::cout << "adding copy_to for " << name <<  "to " <<  (*i) << std::endl;
					json_array_append_new(jsonvector, json_string((*i).c_str()));
				}
				json_object_set_new(jsondef, "copy_to", jsonvector);
			}

			if ( elasticsearchType == "date" )
			{
				json_object_set_new(jsondef, "format", json_string("yyyy-MM-dd'T'HH:mm:ss'Z'"));
			}
		}
		catch (elastic::timestream::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "failed to build item : "<< name << " and type : " << primitivetype;
			json_decref(jsondef);
			XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
		}
	}

	return jsondef;
}

json_t* elastic::timestream::Application::itemToMETAJSON(xmas::ItemDefinition* itemdef, const std::string& fname) 
{
	json_t* jsondef = json_object();

	std::string name = itemdef->getProperty("name");
	std::string primitivetype = itemdef->getProperty("type");

	std::vector<xmas::ItemDefinition*> defs = itemdef->getItems();

	// sub properties
	if ( defs.size() > 0 )
	{
		json_t* jsonitems = json_object();
		// { "name" : {
		//        "definition": [
		for (std::vector<xmas::ItemDefinition*>::iterator i = defs.begin(); i != defs.end(); i++)
		{
			std::string iname = (*i)->getProperty("name");
			json_t * jsonitem = this->itemToMETAJSON(*i, ""); // recursive
			json_object_set_new(jsonitems, iname.c_str(), jsonitem);
		}
		// ]
		json_object_set_new(jsondef, "definition", jsonitems);
	}

	std::vector<std::string> plist = itemdef->propertyNames();
	for (std::vector<std::string>::iterator i = plist.begin(); i != plist.end(); i++)
	{
		std::string pvalue = itemdef->getProperty(*i);
		try
		{
			// { "name" :  "type"  }
			json_object_set_new(jsondef, (*i).c_str(), json_string(pvalue.c_str()));
		}
		catch (elastic::timestream::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "failed to build META item : "<< name << " and type : " << primitivetype;
			json_decref(jsondef);
			XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
		}
	}

	return jsondef;
}

json_t* elastic::timestream::Application::tableRowToJSON(xdata::Table::iterator& ti, std::vector<std::string>& columns, const std::string& name) 
{
	json_t* document = json_object();

	for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++)
	{
		std::string localName = columns[k];
		//std::cout << "Printing the column : " << columns[k] << std::endl;

		xdata::Serializable* s = 0;
		try
		{
			s = (*ti).getField(columns[k]);
		}
		catch (xdata::exception::Exception& e)
		{
			json_decref(document);
			XCEPT_RETHROW (elastic::timestream::exception::Exception, "Cannot convert field '" + columns[k] + "'", e);
		}
		if ( s->type() == "string" )
		{
			std::string value = s->toString();
			json_object_set_new(document, localName.c_str(), json_string(value.c_str()));
		}
		else if ( s->type() == "bool" )
		{
			xdata::Boolean* b = dynamic_cast<xdata::Boolean*>(s);
			json_object_set_new(document, localName.c_str(), json_boolean((xdata::BooleanT)*b));
		}
		else if ( s->type() == "int" )
		{
			xdata::Integer* i = dynamic_cast<xdata::Integer*>(s);
			json_object_set_new(document, localName.c_str(), json_integer((xdata::IntegerT)*i));
		}
		else if ( s->type() == "int 32" )
		{
			xdata::Integer32* i = dynamic_cast<xdata::Integer32*>(s);
			json_object_set_new(document, localName.c_str(), json_integer((xdata::Integer32T)*i));
		}
		else if ( s->type() == "unsigned int" )
		{
			xdata::UnsignedInteger* i = dynamic_cast<xdata::UnsignedInteger*>(s);
			json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedIntegerT)*i));
		}
		else if ( s->type() == "unsigned int 32" )
		{
			xdata::UnsignedInteger32* i = dynamic_cast<xdata::UnsignedInteger32*>(s);
			json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i));
		}
		else if ( s->type() == "unsigned int 64" )
		{
			xdata::UnsignedInteger64* i = dynamic_cast<xdata::UnsignedInteger64*>(s);
			json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i));
		}
		else if ( s->type() == "unsigned long" )
		{
			xdata::UnsignedLong* i = dynamic_cast<xdata::UnsignedLong*>(s);
			json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedLongT)*i));
		}
		else if ( s->type() == "unsigned short" )
		{
			xdata::UnsignedShort* i = dynamic_cast<xdata::UnsignedShort*>(s);
			json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedShortT)*i));
		}
		//json_integer needs to change!
		else if ( s->type() == "time" )
		{
			xdata::TimeVal* i = dynamic_cast<xdata::TimeVal*>(s);
			std::string formattedTime = i->value_.toString("%FT%H:%M:%SZ", toolbox::TimeVal::gmt);
			json_object_set_new(document, localName.c_str(), json_string(formattedTime.c_str()));
		}
		else if ( s->type() == "double" )
		{
			xdata::Double* i = dynamic_cast<xdata::Double*>(s);
			json_object_set_new(document, localName.c_str(), json_real((xdata::DoubleT)*i));
		}
		else if ( s->type() == "float" )
		{
			xdata::Float* i = dynamic_cast<xdata::Float*>(s);
			json_object_set_new(document, localName.c_str(), json_real((xdata::FloatT)*i));
		}
		else if ( s->type().find("vector") != std::string::npos )
		{
			xdata::AbstractVector* v = dynamic_cast<xdata::AbstractVector*>(s);

			if ( v->getElementType() == "unsigned int 32" )
			{
				xdata::Vector<xdata::UnsignedInteger32>* v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(s);

				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::UnsignedInteger32>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i));
					json_t* val = json_integer((xdata::UnsignedInteger32T)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "unsigned int" )
			{
				xdata::Vector<xdata::UnsignedInteger>* v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger>*>(s);

				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::UnsignedInteger>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedIntegerT)*i));
					json_t* val = json_integer((xdata::UnsignedIntegerT)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "int 32" )
			{
				xdata::Vector<xdata::Integer32>* v = dynamic_cast<xdata::Vector<xdata::Integer32>*>(s);

				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::Integer32>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_integer((xdata::Integer32T)*i));
					json_t* val = json_integer((xdata::Integer32T)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "int" )
			{
				xdata::Vector<xdata::Integer>* v = dynamic_cast<xdata::Vector<xdata::Integer>*>(s);

				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::Integer>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_integer((xdata::IntegerT)*i));
					json_t* val = json_integer((xdata::IntegerT)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "unsigned int 64" )
			{
				xdata::Vector<xdata::UnsignedInteger64>* v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(s);
				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::UnsignedInteger64>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i));
					json_t * val = json_integer((xdata::UnsignedInteger64T)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "float" )
			{
				xdata::Vector<xdata::Float>* v = dynamic_cast<xdata::Vector<xdata::Float>*>(s);
				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::Float>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_real((xdata::FloatT)*i));
					json_t* val = json_real((xdata::FloatT)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "double" )
			{
				xdata::Vector<xdata::Double>* v = dynamic_cast<xdata::Vector<xdata::Double>*>(s);
				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::Double>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_real((xdata::DoubleT)*i));
					json_t* val = json_real((xdata::DoubleT)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "bool" )
			{
				xdata::Vector<xdata::Boolean>* v = dynamic_cast<xdata::Vector<xdata::Boolean>*>(s);
				json_t* jsonvector = json_array();
				for (xdata::Vector<xdata::Boolean>::iterator i = v->begin(); i != v->end(); i++)
				{
					json_object_set_new(document, localName.c_str(), json_boolean((xdata::BooleanT)*i));
					json_t* val = json_boolean((xdata::BooleanT)*i);
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else
			{
				XCEPT_RAISE (elastic::timestream::exception::Exception, "Failed to convert data entry, unsupported vector type as " + v->getElementType());
			}
		}
		else if (s->type() == "table")
		{
			xdata::Table* st = dynamic_cast<xdata::Table*>(s);
			std::vector<std::string> cols = st->getColumns();
			json_t* jsontable = json_array();
			for (xdata::Table::iterator sti = st->begin(); sti != st->end(); sti++)
			{
				try
				{
					json_t* jsonrow = this->tableRowToJSON(sti, cols, "");
					//tablename

					json_array_append_new(jsontable, jsonrow);
				}
				catch(elastic::timestream::exception::Exception& e)
				{
					json_decref(document);
					XCEPT_RETHROW (elastic::timestream::exception::Exception, "Failed to convert inner data table,  xdata to es type as " + s->type(), e );
				}

			}
			json_object_set_new(document, localName.c_str(), jsontable);
		}
		else
		{
			json_decref(document);
			XCEPT_RAISE (elastic::timestream::exception::Exception, "Failed to convert data entry,  xdata to es type as " + s->type());
		}
	}

	//adding the flash key, creationtime_, expirationtime_ and withdrawtime_
	std::map<std::string, SettingsT>::const_iterator settings_it;
	settings_it = settings_.find(name);
	if ( settings_it != settings_.end() )
	{
		const SettingsT& settings = settings_it->second;

		std::string flashkeyValue = "@";
		const std::set<std::string>& key = settings.hashKey;
		for (std::set<std::string>::iterator i = key.begin(); i != key.end(); i++) // valid only for first level flashlist items
		{
			xdata::Serializable* s = 0;
			try
			{
				s = (*ti).getField(*i);
			}
			catch (xdata::exception::Exception& e)
			{
				json_decref(document);
				XCEPT_RETHROW (elastic::timestream::exception::Exception, "Cannot convert field '" + *i + "' when adding flash key", e);
			}
			flashkeyValue += s->toString() + "-";
		}
		json_object_set_new(document, "flash_key", json_string(flashkeyValue.c_str()));

		// insert creationtime_
		toolbox::TimeVal creationtime = toolbox::TimeVal::gettimeofday();
		json_object_set_new(document, "creationtime_", json_integer(getMillisSinceEpoch(creationtime)));

		// insert expirationtime_
		toolbox::TimeVal expirationtime = creationtime + settings.timeToLive_o;
		json_object_set_new(document, "expirationtime_", json_integer(getMillisSinceEpoch(expirationtime)));

		// insert withdrawtime_
		toolbox::TimeVal withdrawtime = creationtime + settings.flashInterval_o;
		json_object_set_new(document, "withdrawtime_", json_integer(getMillisSinceEpoch(withdrawtime)));
	}

	return document;
}

uint64_t elastic::timestream::Application::getMillisSinceEpoch(const toolbox::TimeVal& time)
{
	return (uint64_t)time.sec() * (uint64_t)1000 + time.millisec();
}

xdata::Table* elastic::timestream::Application::getDataTable(char * buffer, size_t size) 
{
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buffer, size);

	xdata::Table* t = new xdata::Table();
	try
	{
		serializer_.import(t, &inBuffer);
		return t;
	}
	catch (xdata::exception::Exception& e)
	{
		delete t;
		XCEPT_RETHROW (elastic::timestream::exception::Exception, "Failed to deserialize flashlist table", e);
	}
}

std::string elastic::timestream::Application::xdaqToElasticsearchType(const std::string& type) 
{
	if ( type == "string" )
		return "keyword";
	else if ( type == "bool" )
	{
		return "boolean";
	}
	else if ( type == "int" )
	{
		return "integer";
	}
	else if ( type == "int 32" )
	{
		return "integer";
	}
	else if ( type == "unsigned int" )
	{
		return "long";
	}
	else if ( type == "unsigned int 32" )
	{
		return "long";
	}
	else if ( type == "unsigned int 64" )
	{
		return "long";
	}
	else if ( type == "unsigned long" )
	{
		return "long";
	}
	else if ( type == "unsigned short" )
	{
		return "integer";
	}
	else if ( type == "time" )
	{
		return "date";
	}
	/* this is resolved by list of sub items directly
	else if ( type == "table" )
	{
	return "string";
	}
	*/
	else if ( type == "float" )
	{
		return "float";
	}
	else if ( type == "double" )
	{
		return "double";
	}
	else if ( type == "vector unsigned int 32" )
	{
		return "long";
	}
	else if ( type == "vector unsigned int" )
	{
		return "long";
	}
	else if ( type == "vector int 32" )
	{
		return "integer";
	}
	else if ( type == "vector int" )
	{
		return "integer";
	}
	else if ( type == "vector unsigned int 64" )
	{
		return "long";
	}
	else if ( type == "vector float" )
	{
		return "float";
	}
	else if ( type == "vector double" )
	{
		return "double";
	}
	else if ( type == "vector bool" )
	{
		return "bool";
	}
	else
	{
		XCEPT_RAISE (elastic::timestream::exception::Exception, "Failed to convert type xdata to es type as " + type );

	}
}

void elastic::timestream::Application::actionPerformed(toolbox::Event& event)
{
	if ( event.type() == "xdaq::EndpointAvailableEvent" )
	{

	}
	else if ( event.type() == "urn:elastic-timestream:Event" )
	{
		// event contains flashlist and data
		elastic::timestream::Event& me = dynamic_cast<elastic::timestream::Event&>(event);
		this->processEventData(me.ref_, me.plist_);
	}
}

void elastic::timestream::Application::resetActiveFlashlists()
{
	for (std::map<std::string, std::string>::iterator i = activeFlashList_.begin(); i != activeFlashList_.end(); i++)
	{
		(*i).second = "";
	}
}

xmas::FlashListDefinition* elastic::timestream::Application::loadFlashlistDefinition(const std::string& href, const std::string& qname) 
{
	DOMDocument* doc = 0;
	try
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "load flashlist definition from file  '" << href << "'");
		//std::string filename = "file://" + href;
		//std::cout << "Load XML " << filename << std::endl;
		doc = xoap::getDOMParserFactory()->get("configure")->loadXML(href);
		if ( doc == 0 )
		{
			std::stringstream msg;
			msg << "Failed to load document ( null document) : '" << href << "'";
			XCEPT_RAISE(elastic::timestream::exception::Exception, msg.str());
		}
		xmas::FlashListDefinition* flashlist = new elastic::timestream::Application::FlashListDefinition(doc, href + "#" + qname );

		doc->release();
		doc = 0;

		return flashlist;
	}
	catch (xoap::exception::Exception& e)
	{
		if ( doc != 0 )
			doc->release();
		std::stringstream msg;
		msg << "Failed to load flashlist file from '" << href << "'";
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}
	catch (xmas::exception::Exception& e)
	{
		// this is a parse error
		if ( doc != 0 )
			doc->release();
		std::stringstream msg;
		msg << "Failed to parse configuration from '" << href << "'";
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}
}

void elastic::timestream::Application::applyCollectorSettings()
{
	std::string baseUrl = flashlistSettingsBaseUrl_.toString();

	std::vector<std::string> paths;
	try
	{
		paths = toolbox::getRuntime()->expandPathName(baseUrl);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand path name '" << baseUrl << "'";
		XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
		return;
	}

	if ( paths.size() != 1 )
	{
		std::stringstream msg;
		msg << "Ambiguous path name '" << baseUrl << "' resolve to multiple files";
		XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
		this->notifyQualified("fatal", q);
		return;
	}
	std::string base = paths[0];

	DOMDocument* doc = 0;
	std::string pathname = base + "/flash/" + "collector.settings";

	try
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "apply collector setting for file '" << pathname << "'");
		doc = xoap::getDOMParserFactory()->get("configure")->loadXML(pathname);
		DOMNodeList* flashlistList = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flashlist"));
		for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
		{
			DOMNode * flashlistNode = flashlistList->item(i);
			std::string qname = xoap::getNodeAttribute(flashlistNode, "name");

			// Load flash list definition
			toolbox::net::URN flashlistURN(qname);
			std::string fname = flashlistURN.getNSS();
			try
			{
				xmas::FlashListDefinition* flashlist = this->loadFlashlistDefinition(base + "/flash/" + fname + ".flash", qname);

				std::string qname = flashlist->getProperty("name");
				std::set<std::string> key = toolbox::parseTokenSet(flashlist->getProperty("key"), ",");

				if ( settings_.find(qname) == settings_.end() )
				{
					statistics_[qname].successCounter = 0;
					statistics_[qname].lossCounter = 0;
					statistics_[qname].lossQueueFullCounter = 0;
					statistics_[qname].bulkCounter = 0;
					statistics_[qname].lastBulkSize = 0;
					statistics_[qname].maxBulkSize = 0;
					statistics_[qname].lastIndexCreationTime = 0;
					statistics_[qname].minIndexCreationTime = ULLONG_MAX;
					statistics_[qname].maxIndexCreationTime = 0;

					statistics_[qname].lastIndexOperationTime = 0;
					statistics_[qname].minIndexOperationTime = ULLONG_MAX;
					statistics_[qname].maxIndexOperationTime = 0;

					statistics_[qname].indexOperationTimeSum = 0.0;
					statistics_[qname].indexOperationTotal = 0;
					statistics_[qname].lastIndexOperationCount = 0;

					statistics_[qname].lastCount = 0;
					statistics_[qname].rate = 0;

					statistics_[qname].lostMemoryFull = 0;

					activeFlashList_[qname] = "";
					settings_[qname].uniqueKey = key;

					settings_[qname].flashlistDefinition = flashlist;

					LOG4CPLUS_INFO(this->getApplicationLogger(), "Installed flashlist '" << qname << "'");
				}
				else
				{
					std::stringstream msg;
					msg << "Failed to create flashlist entry ( already existing) : '" << qname << "'";
					XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
					this->notifyQualified("fatal", q);
					return;
				}
			}
			catch(elastic::timestream::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to load flashlist definition for '" << qname << "' ";
				XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
				this->notifyQualified("fatal", q);
				return;
			}

			DOMNodeList* collectorList = flashlistNode->getChildNodes();
			for (XMLSize_t j = 0 ; j < collectorList->getLength() ; j++)
			{
				DOMNode* collectorNode = collectorList->item(j);
				std::string nodeName = xoap::XMLCh2String(collectorNode->getLocalName());
				if( nodeName == "collector" )
				{
					//Hash key
					if ( ((DOMElement*)collectorNode)->hasAttribute(xoap::XStr("hashkey")) )
					{
						std::set<std::string> key = toolbox::parseTokenSet(xoap::getNodeAttribute(collectorNode, "hashkey"), ",");
						settings_[qname].hashKey = key;
					}
					else
					{
						std::stringstream msg;
						msg << "Missing mandatory attribute 'hashkey' for flashlist '" << qname << "' ";
						XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
						this->notifyQualified("fatal", q);
						return;
					}

					//Time to live
					if ( ((DOMElement*)collectorNode)->hasAttributeNS(xoap::XStr(elastic::timestream::Application::ElasticNamespace),xoap::XStr("timetolive")) )
					{
						std::string timetolive = xoap::XMLCh2String(((DOMElement*)collectorNode)->getAttributeNS(xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("timetolive")));
						settings_[qname].timeToLive = timetolive;
						try
						{
							settings_[qname].timeToLive_o.fromString(timetolive);
						}
						catch (toolbox::exception::Exception& e)
						{
							std::stringstream msg;
							msg << "Failed to parse 'timetolive' value = " << timetolive;
							XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, msg.str(), e);
							this->notifyQualified("fatal", q);
							return;
						}
					}
					else
					{
						std::stringstream msg;
						msg << "Missing mandatory attribute 'timetolive' for flashlist '" << qname << "' ";
						XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
						this->notifyQualified("fatal", q);
						return;
					}

					//Time interval
					if ( ((DOMElement*)collectorNode)->hasAttributeNS(xoap::XStr(elastic::timestream::Application::ElasticNamespace),xoap::XStr("timeinterval")) )
					{
						std::string timeinterval = xoap::XMLCh2String(((DOMElement*)collectorNode)->getAttributeNS (xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("timeinterval")));
						settings_[qname].timeInterval = timeinterval;
						try
						{
							settings_[qname].timeInterval_o.fromString(timeinterval);
						}
						catch (toolbox::exception::Exception& e)
						{
							std::stringstream msg;
							msg << "Failed to parse 'timeinterval' value = " << timeinterval;
							XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
							this->notifyQualified("fatal", q);
							return;
						}
					}
					else
					{
						std::stringstream msg;
						msg << "Missing mandatory attribute 'timeinterval' for flashlist '" << qname << "' ";
						XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
						this->notifyQualified("fatal", q);
						return;
					}

					//Flash interval
					if ( ((DOMElement*)collectorNode)->hasAttributeNS(xoap::XStr(elastic::timestream::Application::ElasticNamespace),xoap::XStr("flashinterval")) )
					{
						std::string flashinterval = xoap::XMLCh2String(((DOMElement*)collectorNode)->getAttributeNS (xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("flashinterval")));
						settings_[qname].flashInterval = flashinterval;
						try
						{
							settings_[qname].flashInterval_o.fromString(flashinterval);
						}
						catch (toolbox::exception::Exception & e)
						{
							std::stringstream msg;
							msg << "Failed to parse 'flashinterval' value = " << flashinterval;
							XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
							this->notifyQualified("fatal", q);
							return;
						}
					}
					else
					{
						std::stringstream msg;
						msg << "Missing mandatory attribute 'flashinterval' for flashlist '" << qname << "' ";
						XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
						this->notifyQualified("fatal", q);
						return;
					}

					//Open interval
					settings_[qname].openIntervalIsSet = false;
					if ( ((DOMElement*)collectorNode)->hasAttributeNS(xoap::XStr(elastic::timestream::Application::ElasticNamespace),xoap::XStr("openinterval")) )
					{
						settings_[qname].openIntervalIsSet = true;
						std::string openinterval = xoap::XMLCh2String(((DOMElement*)collectorNode)->getAttributeNS (xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("openinterval")));
						settings_[qname].openInterval = openinterval;
						try
						{
							settings_[qname].openInterval_o.fromString(openinterval);
						}
						catch (toolbox::exception::Exception & e)
						{
							std::stringstream msg;
							msg << "Failed to parse 'openinterval' value = " << openinterval;
							XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
							this->notifyQualified("fatal", q);
							return;
						}
					}

					//Index store type
					if ( ((DOMElement*)collectorNode)->hasAttributeNS(xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("indexstoretype")) )
					{
						settings_[qname].indexStoreType = xoap::XMLCh2String(((DOMElement*)collectorNode)->getAttributeNS (xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("indexstoretype")));
					}
					else
					{
						settings_[qname].indexStoreType = elasticsearchDefaultIndexStoreType_.toString();
					}

					//Index store type
					if ( ((DOMElement*)collectorNode)->hasAttributeNS(xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("numberofreplicas")) )
					{
						settings_[qname].numberOfReplicas = xoap::XMLCh2String(((DOMElement*)collectorNode)->getAttributeNS (xoap::XStr(elastic::timestream::Application::ElasticNamespace), xoap::XStr("numberofreplicas")));
					}
					else
					{
						settings_[qname].numberOfReplicas = "1";
					}

					//Applying collector settings restrictions
					if ( settings_[qname].flashInterval_o > settings_[qname].timeToLive_o )
					{
						std::stringstream msg;
						msg << "Restriction 'flash interval <= time to live' is violated for a flashlist:" << qname;
						XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
						this->notifyQualified("fatal", q);
						return;
					}
					if ( settings_[qname].flashInterval_o > settings_[qname].timeToLive_o )
					{
						std::stringstream msg;
						msg << "Restriction 'flash interval <= time interval' is violated for a flashlist:" << qname;
						XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
						this->notifyQualified("fatal", q);
						return;
					}
					if ( settings_[qname].openIntervalIsSet )
					{
						if ( settings_[qname].openInterval_o > settings_[qname].timeToLive_o )
						{
							std::stringstream msg;
							msg << "Restriction 'open interval <= time to live' is violated for a flashlist:" << qname;
							XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
							this->notifyQualified("fatal", q);
							return;
						}
						if ( settings_[qname].openInterval_o < settings_[qname].flashInterval_o )
						{
							std::stringstream msg;
							msg << "Restriction 'open interval >= flash interval' is violated for a flashlist:" << qname;
							XCEPT_DECLARE(elastic::timestream::exception::Exception, q, msg.str());
							this->notifyQualified("fatal", q);
							return;
						}
					}
				}
			}
		}

		doc->release();
		doc = 0;
	}
	catch (xoap::exception::Exception& e)
	{
		// WE THINK, if directory services are not running, this is where it will fail. not fatal
		if ( doc != 0 )
			doc->release();
		std::stringstream msg;
		msg << "Failed to load collector file from '" << pathname << "'";
		XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
	}
	catch (xmas::exception::Exception& e)
	{
		// this is a parse error
		if ( doc != 0 )
			doc->release();
		std::stringstream msg;
		msg << "Failed to parse collector from '" << pathname << "'";
		XCEPT_DECLARE_NESTED(elastic::timestream::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
	}
}

void elastic::timestream::Application::clearCollectorSettings()
{
	for (std::map<std::string, struct Settings>::iterator i = settings_.begin(); i !=  settings_.end(); i++)
	{
		if ( (*i).second.flashlistDefinition != 0 )
		{
			delete (*i).second.flashlistDefinition;
		}
	}
	settings_.clear();
	blackFlashList_.clear();
}

void elastic::timestream::Application::displayFlashlist(xgi::Input* in, xgi::Output* out) 
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = "fake fake fake";

	try
	{
		json_t* flashlistData = cluster.search(indexName, flashlistName, "", 0);
		char * s = json_dumps(flashlistData, 0);
		std::string dump(s);
		free(s);
		*out << dump << std::endl;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Flashlist for " << flashlistName << " has been found. ");
		json_decref(flashlistData);
	}
	catch(elastic::timestream::exception::Exception& e)
	{
		XCEPT_RETHROW(elastic::timestream::exception::Exception, "failed to retrieve flashlist", e);
	}
}

void elastic::timestream::Application::displayFlashlistMapping(xgi::Input* in, xgi::Output* out) 
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = "fake fake";

	try
	{
		json_t* mappingData = cluster.getMapping(indexName, flashlistName);
		char * s = json_dumps(mappingData, 0);
		std::string dump(s);
		free(s);
		*out << dump << std::endl;
		json_decref(mappingData);
	}
	catch(elastic::timestream::exception::Exception& e)
	{
		XCEPT_RETHROW(elastic::timestream::exception::Exception, "failed to retrieve flashlist mapping", e);
	}
}

xoap::MessageReference elastic::timestream::Application::report (xoap::MessageReference msg) 
{
	// DEBUG
	//	msg->writeTo(std::cout);
	//
	DOMNodeList* bodyList = msg->getSOAPPart().getEnvelope().getBody().getDOMNode()->getChildNodes();
	std::string namespaceURI = "";
	std::string namespacePrefix = "";
	std::string commandName = "";
	for (XMLSize_t i = 0; i < bodyList->getLength(); i++)
	{
		DOMNode* command = bodyList->item(i);

		if ( command->getNodeType() == DOMNode::ELEMENT_NODE )
		{
			namespaceURI = xoap::XMLCh2String(command->getNamespaceURI());
			namespacePrefix = xoap::XMLCh2String(command->getPrefix());
			commandName = xoap::XMLCh2String(command->getLocalName());
			break;
		}
	}

	DOMDocument* document = msg->getSOAPPart().getEnvelope().getDOM()->getOwnerDocument();
	DOMNodeList* list = document->getElementsByTagNameNS(xoap::XStr(xmas::NamespaceUri), xoap::XStr("sample"));
	if ( list->getLength() == 0 )
	{
		XCEPT_RAISE(xoap::exception::Exception, "Could not find sample element");
	}

	std::string flashListName = xoap::getNodeAttribute(list->item(0), "flashlist");
	std::string originator = xoap::getNodeAttribute(list->item(0), "originator");
	std::string tagName = xoap::getNodeAttribute(list->item(0), "tag");
	std::string version = xoap::getNodeAttribute(list->item(0), "version");

	// extract attachment with flashlist data and serialize into a table
	std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();
	std::list<xoap::AttachmentPart*>::iterator j;
	for (j = attachments.begin(); j != attachments.end(); j++)
	{
		if ( (*j)->getSize() == 0 )
		{
			XCEPT_RAISE(xoap::exception::Exception, "empty attachment, cannot forward flashlist");
		}
		this->createDocument((char*) (*j)->getContent(), (*j)->getSize(), flashListName, "to be done index name bla bla");

	}

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName(commandName + "Response", namespacePrefix, namespaceURI);
	b.addBodyElement(responseName);
	return reply;
}


//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void elastic::timestream::Application::TabPanel(xgi::Output* out)
{
	*out << "<script type=\"text/javascript\" src=\"/elastic/timestream/html/js/elastic-timestream.js\"></script> " << std::endl;
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Flashlists\" id=\"tabPage3\">"<< std::endl;

	this->FlashlistsTabPage(out);
	*out << "</div> "<< std::endl;

	//panel-end-div
	*out << "</div>";
}

void elastic::timestream::Application::StatisticsTabPage(xgi::Output* out)
{
	//Dialup
	*out << cgicc::table().set("class", "xdaq-table");
	*out << cgicc::caption("Total Rate");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Total document creation rate (Hz)").set("class","xdaq-case").set("style", "min-width: 100px;");
	*out << cgicc::th("Total documents created").set("class","xdaq-case").set("style", "min-width: 100px;");
	*out << cgicc::th("Total index operations").set("class","xdaq-case").set("style", "min-width: 100px;");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	*out << cgicc::tbody();
	*out << cgicc::tr() << std::endl;
	*out << cgicc::td(toolbox::toString("%4.4f", (double)rate_));
	*out << cgicc::td(totalDocumentsCreatedCounter_.toString());
	size_t tot = 0;
	for (std::map<std::string, StatisticsT>::iterator ss = statistics_.begin(); ss != statistics_.end(); ss++)
	{
		tot += (*ss).second.indexOperationTotal;
	}
	*out << cgicc::td(toolbox::toString("%lu",tot));
	*out << cgicc::tr() << std::endl;
	*out << cgicc::tbody();
	*out << cgicc::table();

	// Per flashlist loss of reports
	*out << cgicc::table().set("class","xdaq-table");
	*out << cgicc::caption("Flashlist");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class","xdaq-case").set("style", "min-width: 100px;");
	*out << cgicc::th("Active");
	*out << cgicc::th("Documents");
	*out << cgicc::th("Bulk");
	*out << cgicc::th("Latest bulk size");
	*out << cgicc::th("Max bulk size");
	*out << cgicc::th("Loss memory full");
	*out << cgicc::th("Loss");
	*out << cgicc::th("Input loss (queue full)");
	*out << cgicc::th("Last index creation (ms)");
	*out << cgicc::th("MIN index creation (ms)");
	*out << cgicc::th("MAX index creation (ms)");
	*out << cgicc::th("Index creation time distribution");

	*out << cgicc::th("Last indexing operation (ms)");
	*out << cgicc::th("MIN indexing operation (ms)");
	*out << cgicc::th("MAX indexing operation (ms)");
	*out << cgicc::th("Index operation total");
	*out << cgicc::th("Indexing operation time distribution");
	*out << cgicc::th("Document creation rate (Hz)");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	*out << cgicc::tbody();

	for (std::map<std::string, struct Settings>::iterator i = settings_.begin(); i != settings_.end(); i++)
	{
		xmas::FlashListDefinition* flashlist = (*i).second.flashlistDefinition;
		std::string name = flashlist->getProperty("name");
		toolbox::net::URN flashlistURN(name);
		std::string fname = flashlistURN.getNSS();

		*out << cgicc::tr() << std::endl;
		*out << cgicc::td(fname);

		*out << cgicc::td(activeFlashList_[name]);

		*out << cgicc::td(toolbox::toString("%d", statistics_[name].successCounter));
		*out << cgicc::td(toolbox::toString("%d", statistics_[name].bulkCounter));
		*out << cgicc::td(toolbox::toString("%d", statistics_[name].lastBulkSize));
		*out << cgicc::td(toolbox::toString("%d", statistics_[name].maxBulkSize));
		*out << cgicc::td(toolbox::toString("%d", statistics_[name].lostMemoryFull));
		*out << cgicc::td(toolbox::toString("%d", statistics_[name].lossCounter));
		*out << cgicc::td(toolbox::toString("%d", statistics_[name].lossQueueFullCounter));


		// index creation statistics
		{
			*out << cgicc::td(toolbox::toString("%lu", statistics_[name].lastIndexCreationTime));
			if (statistics_[name].minIndexCreationTime != ULLONG_MAX )
				*out << cgicc::td(toolbox::toString("%lu", statistics_[name].minIndexCreationTime));
			else
				*out << cgicc::td("-");
			*out << cgicc::td(toolbox::toString("%lu", statistics_[name].maxIndexCreationTime));

			*out <<  "<td>";
			*out << "<table class=\"xdaq-table\">";
			*out << "<tbody>";
			*out << "<tr>";
			std::map<size_t,size_t> distribution = statistics_[name].indexCreationTime;
			for(std::map<size_t,size_t>::iterator i = distribution.begin(); i !=  distribution.end(); i++)
			{
				std::string display;
				if ((*i).first == 0 )
				{
					display= "0ms-1ms;";
				}
				else
				{
					size_t start = pow(2, (*i).first - 1);
					size_t end = start *2;
					*out << cgicc::td(toolbox::toString("%d-%dms",start, end));
				}
			}
			*out << "</tr>";
			*out << "<tr>";
			for(std::map<size_t,size_t>::iterator i = distribution.begin(); i !=  distribution.end(); i++)
			{
				*out << cgicc::td(toolbox::toString("%d", (*i).second));
			}
			*out << "</tr>";
			*out << "</tbody>";
			*out << "</table>";
			*out << "</td>";
		}
		// index operations  statistics
		{
			*out << cgicc::td(toolbox::toString("%lu", statistics_[name].lastIndexOperationTime));

			if (statistics_[name].minIndexOperationTime != ULLONG_MAX )
				*out << cgicc::td(toolbox::toString("%lu", statistics_[name].minIndexOperationTime));
			else
				*out << cgicc::td("-");

			*out << cgicc::td(toolbox::toString("%lu", statistics_[name].maxIndexOperationTime));
			*out << cgicc::td(toolbox::toString("%lu", statistics_[name].indexOperationTotal));

			*out <<  "<td>";

			if (statistics_[name].indexOperationTotal > 0)
			{
				*out << "<table class=\"xdaq-table\">";
				*out << "<tbody>";
				*out << "<tr>";
				std::map<size_t,size_t> distribution = statistics_[name].indexOperationTime;
				for(std::map<size_t,size_t>::iterator i = distribution.begin(); i !=  distribution.end(); i++)
				{
					std::string display;
					if ((*i).first == 0 )
					{
						display= "0-1ms;";
					}
					else
					{
						size_t start = pow(2, (*i).first - 1);
						size_t end = start *2;
						*out << cgicc::td(toolbox::toString("%d-%dms", start, end));
					}
				}
				*out << cgicc::td("Average (ms)");
				*out << cgicc::td("Rate (Hz)");
				*out << cgicc::td("Saturation Ratio");

				*out << "</tr>";
				*out << "<tr>";
				for(std::map<size_t,size_t>::iterator i = distribution.begin(); i !=  distribution.end(); i++)
				{
					*out << cgicc::td(toolbox::toString("%d", (*i).second));
				}
				double average =  statistics_[name].indexOperationTimeSum / statistics_[name].indexOperationTotal;

				*out << cgicc::td(toolbox::toString("%6.2lf", average));
				*out << cgicc::td(toolbox::toString("%lf", statistics_[name].indexOperationRate));
				*out << cgicc::td(toolbox::toString("%lf", (average * statistics_[name].indexOperationRate)/1000));
				*out << "</tr>";
				*out << "</tbody>";
				*out << "</table>";
			}
			*out << "</td>";
		}

		*out << cgicc::td(toolbox::toString("%4.4f", statistics_[name].rate));
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void elastic::timestream::Application::FlashlistsTabPage(xgi::Output* out)
{
	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	*out << "<div id=\"flashlist-selection\">" << std::endl;

	*out << "<button id=\"flashlist-refresh\" data-url=\"" << baseurl.str() << "\" >Reload Flashlist</button>" << std::endl;

	*out << " <select id=\"flashlist-combo\" style=\"width:350px\"> " << std::endl;
	*out << " <option  selected=\"selected\">Select a Flashlist ... </option>" << std::endl;
	for (std::map<std::string,struct Settings>::iterator i = settings_.begin(); i != settings_.end(); i++)
	{
		xmas::FlashListDefinition* flashlist = (*i).second.flashlistDefinition;
		std::string name = flashlist->getProperty("name");
		toolbox::net::URN flashlistURN(name);
		std::string fname = flashlistURN.getNSS();
		*out << "<option>" << fname << "</option>" << std::endl;
	}
	*out << " </select>" << std::endl;

	*out << " <select id=\"display-combo\" style=\"width:350px\"> " << std::endl;
	*out << " <option selected=\"selected\">Summary</option>" << std::endl;
	*out << " <option>Mapping</option>" << std::endl;
	*out << " <option>Table Data</option>" << std::endl;
	*out << " <option>JSON Data</option>" << std::endl;
	*out << " <option>Latest Data</option>" << std::endl;
	*out << " </select>" << std::endl;

	*out << "</div> " << std::endl;

	*out << "<div id=\"flashlist-data\">" << std::endl;
	*out << "</div> " << std::endl;
}

void elastic::timestream::Application::Default(xgi::Input* in, xgi::Output* out) 
{
	this->TabPanel(out);
}

void elastic::timestream::Application::enable (xgi::Input * in, xgi::Output * out) 
{
	toolbox::task::Guard<toolbox::BSem> guard(mutex_);

	if ( !enable_ )
	{
		enable_ = true;
		this->applyCollectorSettings();
	}
}

// allow to clear ES cluster ( mapping deletion) NB. when all clients are disabled
void elastic::timestream::Application::disable(xgi::Input* in, xgi::Output* out) 
{
	toolbox::task::Guard<toolbox::BSem> guard(mutex_);
	if ( enable_ )
	{
		enable_ = false;
		this->resetActiveFlashlists();
		this->clearCollectorSettings();
	}
}

void elastic::timestream::Application::createTemplate(const std::string & zoneName, const std::string & fname, elastic::timestream::Application::SettingsT& settings) 
{
	xmas::FlashListDefinition* flashlist = settings.flashlistDefinition;

	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Joined cluster: " << cname);
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Converting flashlist : " << fname << " to JSON mapping.");
	json_t* templateJSON = 0;
	try
	{
		templateJSON = this->templateToJSON(zoneName, flashlist, settings);
		char * s = json_dumps(templateJSON, 0);
		std::string dump(s);
		free(s);
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Template: " << dump);
	}
	catch(elastic::timestream::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not create template for zone: " << zoneName << " flashlist: " << fname;
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	try
	{
		std::string templateName = zoneName + "-" + fname + "-template";
		json_t* result = cluster.createTemplate(toolbox::tolower(templateName), templateJSON);
		json_t* acknowledged = json_object_get(result, "acknowledged");
		if ( !json_boolean_value(acknowledged) )
		{
		        json_t* status = json_object_get(result, "status");
                        json_t* error = json_object_get(result, "error");
			std::stringstream msg;
			std::unique_ptr<char> errorDump(json_dumps(error, 0));
			std::unique_ptr<char> templateDump(json_dumps(templateJSON, 0));
			msg << "Could not create mapping for flashlist : '" << fname  << "' with error '" << errorDump.get() << "' status result : " << json_real_value(status) << " mapping : '" << templateDump.get() << "'";
			json_decref(result);
			json_decref(templateJSON);
			XCEPT_RAISE(elastic::timestream::exception::Exception, msg.str());
		}

		json_decref(result);
		json_decref(templateJSON);

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Mapping has been actually created for " << fname);
	}
	catch(elastic::api::exception::Exception& nae)
	{
		std::stringstream msg;
		msg << "could not create mapping for flashlist : '" << fname  << "'";
		json_decref(templateJSON);
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), nae);
	}
	//curl -XPUT 'http://pc-c2e11-34-01:9200/_template/cdaq-hostinfo-template?pretty' -d '{"order": 0, "template": "cdaq-hostinfo-*", "mappings": {"hostinfo": {"properties": {"cpuUsage": {"type": "float"}}}}, "aliases": {"cdaq-hostinfo": {}, "cdaq-hostinfo-flash": {}}}'
}

std::string elastic::timestream::Application::createTimedIndexName(const std::string& zoneName, const std::string& fname, elastic::timestream::Application::SettingsT& settings, std::string& creationtime)
{
	//Current time
	toolbox::TimeVal c = toolbox::TimeVal::gettimeofday();
	time_t currenttime = c.sec();

	//Time interval
	time_t timeinterval = settings.timeInterval_o.sec();

	//Start time
	time_t starttime = currenttime - (currenttime % timeinterval);
	toolbox::TimeVal s(starttime, 0);
	creationtime = s.toString("%FT%H:%M:%SZ", toolbox::TimeVal::gmt);

	std::string indexName = zoneName + "-" + fname + "_" + toolbox::tolower(creationtime) + "_" + settings.timeInterval_o.toString("xs:duration");

	return toolbox::tolower(indexName);
}

std::string elastic::timestream::Application::timeToString(time_t& t)
{
	char buffer[256];
	strftime(buffer, 256, "%Ft%H:%M:%Sz", gmtime(&t));
	return buffer;
}

bool elastic::timestream::Application::indexExists(const std::string& iname) 
{
	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Joined cluster: " << cname);
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	try
	{
		return cluster.exists(iname, "");
	}
	catch(elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Cannot check if index '" << iname  << "' exists";
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}
}

bool elastic::timestream::Application::templateExists(const std::string & zoneName, const std::string & fname) 
{
	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Joined cluster: " << cname);
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	std::string templateName;
	try
	{
		templateName = toolbox::tolower(zoneName) + "-" + toolbox::tolower(fname) + "-template";
		return cluster.exists("_template", templateName);
	}
	catch(elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Cannot check if template '" << templateName  << "' exists";
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}
}

bool elastic::timestream::Application::elasticHealthy() 
{
	elastic::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Joined cluster: " << cname);
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}

	try
	{
		json_t * health = cluster.getClusterHealth();
		json_t * statusJ = json_object_get(health, "status");
		std::string status = json_string_value(statusJ);

		json_t * pendingTasksJ = json_object_get(health, "number_of_pending_tasks");
		json_int_t pendingTasks = json_number_value(pendingTasksJ);

		json_decref(health);

		if ( (status == "red") || (status == "yellow") )
		{
			return false;
		}
		else if ( pendingTasks > 0 )
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	catch (elastic::api::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Cannot check cluster health: '" << elasticsearchClusterUrl_.toString() << "'";
		XCEPT_RETHROW(elastic::timestream::exception::Exception, msg.str(), e);
	}
}

void elastic::timestream::Application::query (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");

	toolbox::task::Guard<toolbox::BSem> guard(mutex_);

	size_t totLost = 0;
	for (std::map<std::string, StatisticsT>::iterator ss = statistics_.begin(); ss != statistics_.end(); ss++)
	{
		totLost += (*ss).second.lostMemoryFull + (*ss).second.lossCounter + (*ss).second.lossQueueFullCounter;
	}

	 *out << "Total lost on host  " <<  getApplicationContext()->getContextDescriptor()->getURL() << " is #" << totLost << std::endl;;


}


void elastic::timestream::Application::stats (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
	cgicc::Cgicc cgi(in);
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");

	toolbox::task::Guard<toolbox::BSem> guard(mutex_);

	// { "TotalLost": 0, stats_ "lostMemoryFull" : 0,  "lossCounter": 0, lossQueueFullCounter: 0

	size_t totLost = 0;
	for (std::map<std::string, StatisticsT>::iterator ss = statistics_.begin(); ss != statistics_.end(); ss++)
	{
		totLost += (*ss).second.lostMemoryFull + (*ss).second.lossCounter + (*ss).second.lossQueueFullCounter;
	}
	json_t* stats = json_pack("{s:i}","TotalLost", totLost);

	char * s = json_dumps(stats, 0);
	std::string statsStr(s);

	free(s);
	json_decref(stats);

	*out << statsStr << std::endl;



}


int elastic::timestream::Application::randomSleep(time_t const nSeconds)
{
  double const scale = 1.e9;
  double const nNanoseconds = scale * nSeconds;

  std::srand(time(0));
  double const rnd = nNanoseconds * (std::rand() / double(RAND_MAX));

  time_t const nSec = rnd / scale;
  long const nNanoSec = rnd - (nSec * scale);
  struct timespec tsReq;
  struct timespec tsRem;
  tsReq.tv_sec = nSec;
  tsReq.tv_nsec = nNanoSec;
  std::cout << "nsleeping sec " << nSec  << " nsec " << nNanoSec << std::endl;
  return nanosleep(&tsReq, &tsRem);
}

