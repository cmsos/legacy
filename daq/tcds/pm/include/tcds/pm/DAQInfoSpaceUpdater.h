#ifndef _tcds_pm_DAQInfoSpaceUpdater_h_
#define _tcds_pm_DAQInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class TCADevicePMCommonBase;

    class DAQInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      DAQInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                          tcds::pm::TCADevicePMCommonBase const& hw);
      virtual ~DAQInfoSpaceUpdater();

    protected:
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::pm::TCADevicePMCommonBase const& getHw() const;

      tcds::pm::TCADevicePMCommonBase const& hw_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_DAQInfoSpaceUpdater_h_
