// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _nodejs_Listener_h_
#define _nodejs_Listener_h_

#include <node.h>
#include <node_object_wrap.h>

#include "pt/Listener.h"

namespace nodejs
{

class Listener: public node::ObjectWrap {

	public:

	  static void Init(v8::Local<v8::Object> exports);
	  static void NewInstance(const v8::FunctionCallbackInfo<v8::Value>& args, pt::Listener * listener);
	  static void processIncoming(const v8::FunctionCallbackInfo<v8::Value>& args);

	 private:

	  explicit Listener();
	  ~Listener();

	  static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
	  static v8::Persistent<v8::Function> constructor;

	  // XDAQ
	  pt::Listener * listener_;

};



}

#endif
