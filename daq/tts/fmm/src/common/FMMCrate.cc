/**
*      @file FMMCrate.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.12 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMCrate.hh"

#include "tts/fmm/FMMHardcodedAddressTableReader.hh"
#include "tts/fmmtd/FMMTDHardcodedAddressTableReader.hh"

#include "tts/fmm/FMMCard.hh"
#include "tts/fmmtd/FMMTDCard.hh"
#include "tts/ipcutils/SemaphoreArray.hh"

#include "hal/PCILinuxBusAdapter.hh"
#include "hal/PCIDummyBusAdapter.hh"
#include "hal/NoSuchDeviceException.hh"

#include <iostream>
#include <sstream>

#include "xcept/Exception.h"

static const char* key_file = "/dev/xpci";
static const uint32_t n_geoslots = 21;

tts::FMMCrate::FMMCrate(bool dummy, char project_id) 
  throw (HAL::HardwareAccessException, xcept::Exception)
  : _busadapter(0),
    _fmm_addresstable(0),
    _theFMMs(),
    _fmmtd_addresstable(0),
    _td(0),
    _crateLock(0)  {

  try {
    bool doCreate = true;
    _crateLock = new ipcutils::SemaphoreArray( key_file, project_id, doCreate, n_geoslots);

    if (dummy)
      _busadapter = new HAL::PCIDummyBusAdapter();
    else
      _busadapter = new HAL::PCILinuxBusAdapter();
  
    //
    // detect and instantiate FMMs
    //
    tts::FMMHardcodedAddressTableReader reader;
    _fmm_addresstable = new HAL::PCIAddressTable("FMM address table", reader);

    const uint32_t FMMVendorID = 0xecd6;
    const uint32_t FMMDeviceID = 0xfd4d;
    
    uint32_t index = 0;
    try {
      for (; index < 16; index++) {
	HAL::PCIDevice* dev = new HAL::PCIDevice(*_fmm_addresstable, *_busadapter, FMMVendorID, FMMDeviceID, index);
	_theFMMs.push_back( new tts::FMMCard(dev, *_crateLock) );
      }
    }
    catch(HAL::NoSuchDeviceException & e) {
    }

    //
    // detect and instantiate Trigger Distributors
    //
    tts::FMMTDHardcodedAddressTableReader treader;

    _fmmtd_addresstable = new HAL::PCIAddressTable("FMM TD address table", treader);

    _td = 0;
    try {
      uint32_t vendorID = 0xecd6;
      uint32_t deviceID = 0x1002;
      uint32_t index = 0;
      HAL::PCIDevice* dev = new HAL::PCIDevice(*_fmmtd_addresstable, *_busadapter, vendorID, deviceID, index);
      _td = new tts::FMMTDCard(dev, *_crateLock);
    }
    catch(HAL::NoSuchDeviceException & e) {
    }

    //
    // Check if we have geoslots and disable locking if not
    //
    try {
      checkGeoSlotsUnique();
    }
    catch (xcept::Exception &e) {
      std::cout << "FMMCrate::FMMCrate(). Warning: This crate does not support Geoslots. Disabling the locking of FMMs and FMMTD." << std::endl;

      for (uint32_t id1=0; id1<numFMMs(); id1++) 
	_theFMMs[id1]->disableLocking();
      
      if (_td)
	_td->disableLocking();
    };


  }
  catch (...) {
    cleanup();
    throw;   // rethrow any other exception after cleaning up
  }
}


// clean up all dynamically created objects belonging to FMMCrate.
// only called upon exception in constructor and in desutuctor
void tts::FMMCrate::cleanup() {
  if (_td) { 
    delete _td; _td=0; 
  }
  if (_fmmtd_addresstable) { 
    delete _fmmtd_addresstable; _fmmtd_addresstable=0; 
  }
  
  /// delete FMM Cards
  std::vector<tts::FMMCard*>::iterator it = _theFMMs.begin();
  for (; it != _theFMMs.end(); it++)
    delete (*it);

  _theFMMs.clear();

  if (_fmm_addresstable) {
    delete _fmm_addresstable;
    _fmm_addresstable = 0;
  }

  if (_busadapter) {
    delete _busadapter;
    _busadapter=0;
  }

  if (_crateLock) 
    delete _crateLock;
}

tts::FMMCrate::~FMMCrate() {
  cleanup();
}


tts::FMMCard& tts::FMMCrate::getFMM( uint32_t id ) 
  throw (xcept::Exception) {

  if ( id >= (uint32_t) _theFMMs.size() )
    XCEPT_RAISE( xcept::Exception, "FMMCrate::getFMM: FMM with requested ID is not present.");

  return * (_theFMMs[id] );
}

tts::FMMCard& tts::FMMCrate::getFMMbySN( std::string const& sn ) 
  throw (HAL::HardwareAccessException, xcept::Exception) {
    
  for (std::vector<tts::FMMCard*>::iterator it = _theFMMs.begin(); it!= _theFMMs.end(); ++it) {
    if ( (*it)->getSerialNumber() == sn ) 
      return *( *it );
  }

  XCEPT_RAISE( xcept::Exception, "FMMCrate::getFMMbySN: FMM with serial number '" + sn + "' not found in the crate." );
  
}

tts::FMMCard& tts::FMMCrate::getFMMbyGeoSlot( uint32_t geoslot ) 
  throw (HAL::HardwareAccessException, xcept::Exception) {

  checkGeoSlotsUnique();

  for (std::vector<tts::FMMCard*>::iterator it = _theFMMs.begin(); it!= _theFMMs.end(); ++it) {
    if ( (*it)->getGeoSlot() == geoslot ) 
      return *( *it );
  }
  
  std::stringstream msg;
  msg << "FMMCrate::getFMMbyGeoSlot: FMM with geoslot number '" 
      <<  geoslot << "' not found in the crate.";

  XCEPT_RAISE( xcept::Exception, msg.str() );
}

void tts::FMMCrate::checkGeoSlotsUnique() 
  throw (HAL::HardwareAccessException, xcept::Exception) {

  // check that geoslots are unique to be sure that we are in a crate with proper backplane
  for (std::vector<tts::FMMCard*>::size_type id1=0; id1<numFMMs(); id1++) {
    if ( _theFMMs[id1]->getGeoSlot() > 20 ) {
      std::stringstream msg;
      msg << "error: geoslot > 20 not valid. Found an FMM with geoslot=" << _theFMMs[id1]->getGeoSlot() << "; ";
      if ( _theFMMs[id1]->getGeoSlot() == 31 ) 
	msg << "Reading a geoslot of 31 may indicate that the CPCI crate has been power-cycled "
	    << "but the host controlling it has not been re-booted. Re-booting the PC may cure the problem.";      
      XCEPT_RAISE( xcept::Exception, msg.str() );
    }

    for (std::vector<tts::FMMCard*>::size_type id2=0; id2<id1; id2++) {
      if ( _theFMMs[id1]->getGeoSlot() == _theFMMs[id2]->getGeoSlot() ) {
	std::stringstream msg;
	msg << "FMMCrate::getFMMbyGeoSlot: GeoSlots not unique. FMMs on PCI index "
	    << id1 << " and " << id2 << " have same geoslot " << _theFMMs[id1]->getGeoSlot() ;

	XCEPT_RAISE( xcept::Exception, msg.str() );
      }
    }
  }


}

bool tts::FMMCrate::hasTD() {
  return (_td != 0);
}

tts::FMMTDCard& tts::FMMCrate::getTD() 
  throw (xcept::Exception) {

  if (_td == 0)
    XCEPT_RAISE( xcept::Exception, "FMMCrate::getTD(): No Trigger Distributor in crate.");

  return *_td;
} 
