/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * Channel.hpp
 *
 *  Created on: Feb 7, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once
#include <mpila/ChannelCounters.hpp>
#include <mutex>

#include "mpila/PendingRequest.hpp"
#include "mpila/rlist.hpp"

namespace mpila
{
class Channel
{
   public:
    Channel(size_t pipelineDepth, WorkRequestType type);

    ChannelCounters &getCounters();

    size_t getPipelineDepth() const;

   protected:
    ChannelCounters counters_;
    size_t pipelineDepth_;
};

} /* namespace mpila */
