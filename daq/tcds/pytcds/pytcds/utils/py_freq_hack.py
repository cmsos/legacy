#!/usr/bin/env python

###############################################################################
## Little Python telnet server to help with FreqMon development.
###############################################################################

import random
import socket
import sys
from thread import *
import time

HOST = ''
PORT = 8888
SLEEP_TIME = 1

###############################################################################

def client_thread(conn):
    # conn.send("Welcome to PyFreqHack\n")

    loop = False

    # i = 0
    while True:
        if not loop:
            data = conn.recv(1024)
            if data:
                if data.strip() == 'q':
                    loop = False
                elif data.strip() == 'loop':
                    loop = True
                reply = data
        else:
            rnd = random.randint(40000000, 41000000)
            reply = "{0:d}\n".format(rnd)

        try:
            conn.sendall(reply)
        except socket.error:
            conn.close()
            break
        time.sleep(SLEEP_TIME)
        # i += 1
    # End of client_thread().

###############################################################################

if __name__ == "__main__":

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        print >> sys.stderr, "Bind failed. Error code: '{0:d}: {1:s}.".format(msg[0], msg[1])
        sys.exit(1)

    s.listen(10)
    print "Listening..."

    while 1:
        (conn, addr) = s.accept()
        print "Accepted connection from {0:s}:{1:s}".format(addr[0], str(addr[1]))
        start_new_thread(client_thread ,(conn,))

    s.close()

    print "Done"

###############################################################################
