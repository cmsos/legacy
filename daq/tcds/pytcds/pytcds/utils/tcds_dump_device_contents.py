#!/usr/bin/env python

###############################################################################
## Utility to dump the full address table (with contents) for a device
## specified by its IP address.
###############################################################################

import sys

import uhal

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BOARD_TYPE_UNKNOWN
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_hw_id import connect_to_identified_board
from pytcds.utils.tcds_utils_hw_id import identify_board
from pytcds.utils.tcds_utils_misc import chunkify
from pytcds.utils.tcds_utils_misc import name_matches_pattern
from pytcds.utils.tcds_utils_misc import uint32_to_string
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class RegDumper(CmdLineBase):

    #----------

    SORT_MODE_CHOICES = ["name", "address"]
    OUTPUT_FORMAT_CHOICES = ["default", "configdump"]
    COMMENT_PREFIXES_BY_FORMAT = {
        "default" : "",
        "configdump" : "# "
        }

    #----------

    def __init__(self, description=None, usage=None, epilog=None):
        super(RegDumper, self).__init__(description, usage, epilog)
        self.dev = None
        self.output_file_name = None
        self.output_stream = sys.stdout

        # These two, the register sort mode and the output format, are
        # exposed as command-line options.
        self.sort_mode = None
        self.output_format = None

        self.ignore_read_only = False
        self.ignore_write_only = False
        self.ignore_non_leaf_nodes = False

        self.ignore_list = []
        self.comment_prefix = ""
        # End of __init__().

    def setup_parser_custom(self):
        # Start with the basic parser configuration.
        super(RegDumper, self).setup_parser_custom()
        # Add the option to choose the output format.
        help_str = "Output format to use. " \
                   "Options: '{0:s}'. " \
                   "[default: '%(default)s']"
        help_str = help_str.format("', '".join(RegDumper.OUTPUT_FORMAT_CHOICES))
        self.parser.add_argument("-f", "--format",
                                 action="store",
                                 dest="output_format",
                                 choices=RegDumper.OUTPUT_FORMAT_CHOICES,
                                 default="default",
                                 help=help_str)
        # Add the option to choose the sort mode.
        help_str = "Output format to use. " \
                   "Options: '{0:s}'. " \
                   "[default: '%(default)s']"
        help_str = help_str.format("', '".join(RegDumper.SORT_MODE_CHOICES))
        self.parser.add_argument("-s", "--sort-mode",
                                 action="store",
                                 dest="sort_mode",
                                 choices=RegDumper.SORT_MODE_CHOICES,
                                 default="name",
                                 help=help_str)
        # Add the option to redirect all output to a given file.
        help_str = "Redirect output to FILE."
        self.parser.add_argument("-o", "--output",
                                 action="store",
                                 dest="output_file",
                                 default=None,
                                 help=help_str)
        # Add options to ignore read-only/write-only registers.
        help_str = "Ignore read-only registers."
        self.parser.add_argument("--ignore-readonly",
                                 action="store_true",
                                 default=False,
                                 help=help_str)
        help_str = "Ignore write-only registers."
        self.parser.add_argument("--ignore-writeonly",
                                 action="store_true",
                                 default=False,
                                 help=help_str)
        # Add an option to ignore non-leaf registers.
        help_str = "Ignore non-leaf registers."
        self.parser.add_argument("--ignore-nonleaf",
                                 action="store_true",
                                 default=True,
                                 help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(RegDumper, self).handle_args()

        # Handle the options.
        self.sort_mode = self.args.sort_mode
        self.output_format = self.args.output_format
        self.output_file_name = self.args.output_file
        self.ignore_read_only = self.args.ignore_readonly
        self.ignore_write_only = self.args.ignore_writeonly
        self.ignore_non_leaf = self.args.ignore_nonleaf

        # Configdump mode automatically implies certain 'ignore'
        # settings.
        if self.output_format == "configdump":
            self.ignore_read_only = True
            self.ignore_write_only = True
            self.ignore_non_leaf = True

        # End of handle_args().

    def main(self):

        # Some after-market setup and configuration.
        self.comment_prefix = RegDumper.COMMENT_PREFIXES_BY_FORMAT[self.output_format]

        # There is a list of regular expressions for register names to
        # ignore. One typically does not want to read things like the
        # flash memory.
        self.ignore_list.append("^.*flash.*")
        self.ignore_list.append("^user.i2c_player_rx$")
        self.ignore_list.append("^user.i2c_player_tx$")

        # Try to identify what kind of board is at the given network
        # address.
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        (carrier_type, board_type) = identify_board(network_address,
                                                    controlhub_address,
                                                    verbose=self.verbose)
        if (carrier_type == BOARD_TYPE_UNKNOWN) and \
           (board_type == BOARD_TYPE_UNKNOWN):
            self.error("Could not identify device at {0:s}.".format(network_address))
        dev = connect_to_identified_board(carrier_type,
                                          board_type,
                                          network_address,
                                          controlhub_address,
                                          verbose=self.verbose)
        if dev is None:
            self.error("Could not connect to {0:s}.".format(network_address))
        self.dev = dev

        #----------

        # If we got here, the device must be something we recognize
        # and have managed to connect to with the full address table.
        if self.output_file_name:
            try:
                self.output_stream = open(self.output_file_name, "w")
            except IOError, err:
                msg = "Could not write to file '{0:s}': {1:s}."
                self.error(msg.format(self.output_file_name, err))
        self.dump_all_registers()
        if self.output_file_name:
            try:
                self.output_stream.close()
            except IOError, err:
                msg = "Could not close file '{0:s}': {1:s}."
                self.error(msg.format(self.output_file_name, err))

    # End of main().

    def output(self, msg, is_comment=False):
        comment_str = ""
        if is_comment:
            comment_str = self.comment_prefix
        print >> self.output_stream, "{0:s}{1:s}".format(comment_str, msg)
        # End of output().

    def dump_all_registers(self):

        hw = self.dev
        node_names = hw.get_nodes()
        node_names_to_print = None

        if self.sort_mode == "name":
            # Sort by node name.
            # NOTE: These are the full, long node names. So sorting
            # maintains the proper nesting.
            node_names.sort()
            node_names_to_print = self.build_indented_names(node_names)
        elif self.sort_mode == "address":
            # Sort by node address.
            tmp = []
            for node_name in node_names:
                node_address = hw.get_node(node_name).getAddress()
                node_mask = hw.get_node(node_name).getMask()
                # NOTE: Trick: use minus the mask value to sort in reverse
                # order.
                tmp.append((node_address, -node_mask, node_name))
            tmp.sort()
            node_names = [i[-1] for i in tmp]
            node_names_to_print = dict(zip(node_names, node_names))
        else:
            # BUG BUG BUG
            assert False, \
                "ERROR Don't know about sort mode '{0:s}'".format(self.sort_mode)
            # BUG BUG BUG end

        # Output step 1: prepare all output information.
        output_data = []
        reg_info_list = hw.create_reg_info_list()
        reg_info_map = dict(zip([i.name for i in reg_info_list], reg_info_list))
        for node_name in node_names:
            reg_info = reg_info_map[node_name]
            address_str = ""
            mask_str = ""
            val_str_pieces = []
            help_str_pieces = []
            info_str = ""

            include_this_node = True
            if include_this_node and self.ignore_read_only:
                if not reg_info.is_writable():
                    include_this_node = False
            if include_this_node and self.ignore_write_only:
                if not reg_info.is_readable():
                    include_this_node = False

            if include_this_node and self.ignore_non_leaf:
                if not reg_info.is_leaf():
                    include_this_node = False

            if name_matches_pattern(node_name, self.ignore_list):
                include_this_node = False

            if include_this_node:
                address_str = "0x{0:08x}".format(reg_info.address)
                mask_str = "0x{0:08x}".format(reg_info.mask)
                info_str = ""
                if reg_info.is_readable():
                    values = reg_info.contents
                    try:
                        val_str_pieces = ["0x{0:08x}".format(i) for i in values]
                    except Exception, err:
                        val_str_pieces = "Unreachable"
                else:
                    info_str = "write-only"
                # If the name of the register sounds like a version, a
                # date, or a character, add some additional
                # information to the output. Similar for the system_id
                # and board_id registers.
                if (node_name.find("date") > -1):
                    help_str_pieces = [" ({0:d})".format(i) for i in values]
                elif (node_name.find("ver_") > -1) or \
                     (node_name.find("version") > -1):
                    help_str_pieces = [" ({0:d})".format(i) for i in values]
                elif (node_name.find("char") > -1):
                    help_str_pieces = [" ('{0:s}')".format(chr(i)) for i in values]
                elif (node_name.find("system_id") > -1) or \
                     (node_name.find("board_id") > -1):
                    help_str_pieces = [" ('{0:s}')".format(uint32_to_string(i)) \
                                       for i in values]
                else:
                    help_str_pieces = [""] * len(values)

            for (val_str_piece, help_str_piece) \
                in zip(val_str_pieces, help_str_pieces):
                # Careful when appending the info for the current node
                # to the output data. Which columns to include depends
                # on the output format.
                # Default output format.
                columns_to_include = [node_name,
                                      address_str,
                                      mask_str,
                                      val_str_piece,
                                      help_str_piece,
                                      info_str]
                if self.output_format == "configdump":
                    # Configdump output format.
                    columns_to_include = [node_name,
                                          val_str_piece]
                output_data.append(columns_to_include)

        # Output step 2: format and output.
        self.output(self.sep_line, True)
        # address_str = network_address
        # if self.network_address != self.ip_address:
        #     address_str = "{0:s} ({1:s})".format(address_str, self.network_address)
        # msg = "Register dump for '{0:s}' device at {1:s} ({2:s} format)."
        # self.output(msg.format(self.dev.board_type_str(), address_str, self.output_format), True)
        msg = "Register dump for '{0:s}' device at {1:s} ({2:s} format)."
        self.output(msg.format(self.dev.board_type_str(), self.target, self.output_format), True)
        self.output(self.sep_line, True)
        rows = output_data
        cols = zip(*rows)
        col_sizes = [max(len(val) for val in col) for col in cols]
        fmt = " ".join(["{{{0:d}:<{1:d}s}}".format(i, j) \
                        for (i, j) in enumerate(col_sizes)])
        tmp = [fmt.format(*row) for row in rows]
        for line in tmp:
            self.output(line)
        self.output(self.sep_line, True)
        # End of dump_all_registers().

    def build_indented_names(self, node_names):
        indented_names = {}
        for node_name in node_names:
            indent_size = node_name.count(".")
            indent = "  " * indent_size
            short_name = node_name.split(".")[-1]
            indented_name = "{0:s}{1:s}".format(indent, short_name)
            indented_names[node_name] = indented_name
        # End of build_indentd_names().
        return indented_names

    # End of class RegDumper.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to dump the full register contents " \
        "of a TCDS IPbus device specified by its IP address."

    res = RegDumper(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
