#ifndef __TEST_PARAM_H__
#define __TEST_PARAM_H__

#include "xdaq/Application.h"

#include "xdata/String.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/Vector.h"
#include "xdata/Bag.h"

class TestParam : public xdaq::Application  
{
public:
	XDAQ_INSTANTIATOR();

	TestParam(xdaq::ApplicationStub *stub);

	class InnerParam
	{
	public:
		void registerFields(xdata::Bag<InnerParam> *bag);

		xdata::Double       double_;
		xdata::Vector<xdata::Boolean> vector_;
	};

	xdata::String       string_;
	xdata::UnsignedLong ulong_;
	xdata::Integer      int_;
	xdata::Boolean      bool_;
	xdata::Double       double_;
	xdata::Float        float_;
	xdata::Vector<xdata::Integer> vector_;
	xdata::Bag<InnerParam> bag_;
};

#endif  // ifndef __TEST_XCTL_H__
// vim: set ai sw=4 ts=4:
