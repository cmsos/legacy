package net.hep.cms.xdaqctl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import junit.framework.TestCase;

public class WSESubscriptionTest  extends TestCase {
	
	final static String RCMS_ORIGINATOR = "http://localhost:0/urn:rcms-fm:fullpath=/rcms/test";
	final static String FILTER = "//xmas:sample[@flashlist]";
	final static String ENDPOINT = "http://localhost:40000/urn:xdaq-application:lid=400";
	
	public WSESubscriptionTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(WSESubscriptionTest.class);
	}

	public void testWSESubscription() throws Exception {
		final String FILTER = "//xmas:sample[@flashlist]";
		
		WSESubscription s = new WSESubscription(RCMS_ORIGINATOR, ENDPOINT);
		assertNotNull(s);
		
		s.setFilter(FILTER);
		assertEquals(FILTER, s.getFilter());
		
		String uuid = s.subscribe();
		assertNotNull(uuid);
		System.out.println("==== UUID: " + uuid);
		
		// wait for subscription to be processed by ws eventing
		Thread.sleep(1000);
		
		String page;
		page = getWebPage(ENDPOINT);

		int i = page.indexOf(RCMS_ORIGINATOR);
		int j = page.indexOf(FILTER);
		
		assertTrue("Couldn't find " + RCMS_ORIGINATOR + " in the wse page.", -1 != i);		
		assertTrue("Couldn't find " + FILTER + " in the wse page.", -1 != j);
		
		Date start = getExpirationTime(page, uuid);
		
		Thread.sleep(1000);
		s.renew();
		
		page = getWebPage(ENDPOINT);
		Date renew = getExpirationTime(page, uuid);
		
		System.out.println("==== " + start + " -> " + renew);
		assertTrue(renew.after(start));
		
		// second subscription forces new uuid
		String uuid2 = s.subscribe();
		assertFalse(uuid.equals(uuid2));
		System.out.println("==== UUID2: " + uuid2);

	}
	
	public void testAutomaticRenewal() throws Exception {
		final String FILTER = "//xmas:sample[@flashlist]";
		
		WSESubscription s = new WSESubscription(RCMS_ORIGINATOR, ENDPOINT);
		s.setFilter(FILTER);
		s.setExpires("PT5S");
		assertEquals("PT10S", s.getExpires());
		
		String uuid = s.subscribe();
		System.out.println("==== UUID: " + uuid);
	
		Date start = getExpirationTime(getWebPage(ENDPOINT), uuid);
		Thread.sleep(15000);
		Date renew = getExpirationTime(getWebPage(ENDPOINT), uuid);
		System.out.println("==== " + start + " -> " + renew);
		assertTrue(renew.after(start));

		assertFalse(s.isStopped());
		
		s.stop();
		
		assertTrue(s.isStopped());
		
		
		start = getExpirationTime(getWebPage(ENDPOINT), uuid);
		Thread.sleep(15000);
		renew = getExpirationTime(getWebPage(ENDPOINT), uuid);
		System.out.println("==== " + start + " -> " + renew);
		assertNull("Subscription is renewd (should not be).", renew);
	}
	
	private String getWebPage(String urlString) throws Exception {
		
		URL url = new URL(urlString);
		URLConnection c = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				c.getInputStream()));
			
		String page = "";
		String line;
		while ((line = in.readLine()) != null) {
			page += line + "\n";
		}
		in.close();
		
		return page;
	}

	private Date getExpirationTime(String page, String uuid) throws Exception
	{
		int i = page.indexOf(uuid.substring(5)); // remove leading "uuid:"
		if (i < 0) { return null; } // expired!
		
		Matcher m = Pattern.compile("\\d{4}-\\d{2}-\\d{2}T[\\d:]+")
				.matcher(page.substring(i));
		m.find();
		String s = m.group();
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		return format.parse(s);
	}
}
