#ifndef _tcds_pi_PIController_h_
#define _tcds_pi_PIController_h_

#include <memory>
#include <string>
#include <vector>

#include "toolbox/Event.h"
#include "xgi/exception/Exception.h"
#include "xoap/MessageReference.h"

#include "tcds/pi/PIDAQLoop.h"
#include "tcds/pi/TCADevicePI.h"
#include "tcds/utils/FSMSOAPParHelper.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdConfigureTTCSpy.h"
#include "tcds/utils/SOAPCmdDisableTTCSpy.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/SOAPCmdEnableTTCSpy.h"
#include "tcds/utils/SOAPCmdReadHardwareConfiguration.h"
#include "tcds/utils/SOAPCmdResetTTCSpyLog.h"
#include "tcds/utils/XDAQAppWithFSMBasic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace hwlayer {
    class RegisterInfo;
  }
}

namespace tcds {
  namespace hwutilstca {
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
  }
}

namespace tcds {
  namespace pi {

    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    class PITTCInfoSpaceHandler;
    class PITTCInfoSpaceUpdater;
    class PITTSInfoSpaceHandler;
    class PITTSInfoSpaceUpdater;
    class PITTSLinksInfoSpaceHandler;
    class PITTSLinksInfoSpaceUpdater;
    class TTCSpyInfoSpaceHandler;
    class TTCSpyInfoSpaceUpdater;
    class TTSLogInfoSpaceHandler;
    class TTSLogInfoSpaceUpdater;

    class PIController : public tcds::utils::XDAQAppWithFSMBasic
    {

      friend class PIDAQLoop;

    public:
      XDAQ_INSTANTIATOR();

      PIController(xdaq::ApplicationStub* stub);
      virtual ~PIController();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as TCADevicePI&.
       */
      virtual TCADevicePI& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void hwCfgInitializeImpl();
      virtual void hwCfgFinalizeImpl();

      virtual void configureActionImpl(toolbox::Event::Reference event);
      virtual void enableActionImpl(toolbox::Event::Reference event);
      virtual void haltActionImpl(toolbox::Event::Reference event);
      virtual void stopActionImpl(toolbox::Event::Reference event);
      virtual void zeroActionImpl(toolbox::Event::Reference event);

      virtual bool isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const;

      virtual std::vector<tcds::utils::FSMSOAPParHelper> expectedFSMSoapParsImpl(std::string const& commandName) const;
      virtual void loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                tcds::utils::FSMSOAPParHelper const& param);

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::auto_ptr<tcds::pi::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::auto_ptr<tcds::pi::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      std::auto_ptr<PITTCInfoSpaceUpdater> piInputInfoSpaceUpdaterP_;
      std::auto_ptr<PITTCInfoSpaceHandler> piInputInfoSpaceP_;
      std::auto_ptr<PITTSInfoSpaceUpdater> piTTSInfoSpaceUpdaterP_;
      std::auto_ptr<PITTSInfoSpaceHandler> piTTSInfoSpaceP_;
      std::auto_ptr<PITTSLinksInfoSpaceUpdater> piTTSLinksInfoSpaceUpdaterP_;
      std::auto_ptr<PITTSLinksInfoSpaceHandler> piTTSLinksInfoSpaceP_;
      std::auto_ptr<TTCSpyInfoSpaceUpdater> ttcspyInfoSpaceUpdaterP_;
      std::auto_ptr<TTCSpyInfoSpaceHandler> ttcspyInfoSpaceP_;
      std::auto_ptr<TTSLogInfoSpaceUpdater> ttslogInfoSpaceUpdaterP_;
      std::auto_ptr<TTSLogInfoSpaceHandler> ttslogInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdConfigureTTCSpy;
      template<typename> friend class tcds::utils::SOAPCmdDisableTTCSpy;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      template<typename> friend class tcds::utils::SOAPCmdEnableTTCSpy;
      template<typename> friend class tcds::utils::SOAPCmdReadHardwareConfiguration;
      template<typename> friend class tcds::utils::SOAPCmdResetTTCSpyLog;
      tcds::utils::SOAPCmdConfigureTTCSpy<PIController> soapCmdConfigureTTCSpy_;
      tcds::utils::SOAPCmdDisableTTCSpy<PIController> soapCmdDisableTTCSpy_;
      tcds::utils::SOAPCmdDumpHardwareState<PIController> soapCmdDumpHardwareState_;
      tcds::utils::SOAPCmdEnableTTCSpy<PIController> soapCmdEnableTTCSpy_;
      tcds::utils::SOAPCmdReadHardwareConfiguration<PIController> soapCmdReadHardwareConfiguration_;
      tcds::utils::SOAPCmdResetTTCSpyLog<PIController> soapCmdResetTTCSpyLog_;

      // A dedicated acquisition loop for the TTC and TTS logs.
      tcds::pi::PIDAQLoop piDAQLoop_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PIController_h_
