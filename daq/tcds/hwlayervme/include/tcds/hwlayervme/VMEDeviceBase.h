#ifndef _tcds_hwlayervme_VMEDeviceBase_h_
#define _tcds_hwlayervme_VMEDeviceBase_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/hwlayer/ConfigurationProcessor.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/IHwDevice.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwlayervme/HwDeviceVME.h"

namespace tcds {
  namespace hwlayervme {

    /**
     * Abstract base class for all VME hardware devices.
     */
    class VMEDeviceBase : public tcds::hwlayer::DeviceBase
    {

    public:
      virtual ~VMEDeviceBase();

      /**
       * Connect to the hardware. Does whatever is necessary to find
       * the hardware and setup the connection.
       */
      void hwConnect(std::string const& addressTableFileName,
                     int const unitNumber,
                     int const chainNumber,
                     uint32_t const slotNumber);

      /**
       * The inverse of hwConnect. Disconnects from the hardware,
       * releasing it for use by someone/something else.
       */
      void hwRelease();

      bool isHwConnected() const;

      std::string readBoardID() const;
      std::string readFirmwareVersion() const;

    protected:
      /**
       * @note
       * Protected constructor since this is an abstract base class.
       */
      VMEDeviceBase();

      virtual bool isReadyForUseImpl() const;

      virtual uint32_t getBaseAddress(uint32_t const slotNumber) const = 0;

      virtual void hwConnectImpl(std::string const& addressTableFileName,
                                 int const unitNumber,
                                 int const chainNumber,
                                 uint32_t const slotNumber);
      virtual void hwReleaseImpl();

      virtual std::string readBoardIDImpl() const;
      virtual std::string readFirmwareVersionImpl() const;

      virtual std::vector<std::string> getRegisterNamesImpl() const;
      virtual tcds::hwlayer::RegisterInfo::RegInfoVec getRegisterInfosImpl() const;

      virtual uint32_t readRegisterImpl(std::string const& regName) const;
      virtual void writeRegisterImpl(std::string const& regName,
                                     uint32_t const regVal) const;
      virtual std::vector<uint32_t> readBlockImpl(std::string const& regName,
                                                  uint32_t const nWords) const;
      virtual std::vector<uint32_t> readBlockOffsetImpl(std::string const& regName,
                                                        uint32_t const nWords,
                                                        uint32_t const offset) const;
      virtual void writeBlockImpl(std::string const& regName,
                                  std::vector<uint32_t> const& regVals) const;

      virtual uint32_t getBlockSizeImpl(std::string const& regName) const;

      virtual void writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const;
      virtual tcds::hwlayer::DeviceBase::RegContentsVec readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const;

      virtual tcds::hwlayer::RegDumpVec dumpRegisterContentsImpl() const;

      HwDeviceVME hwDevice_;

    };

  } // namespace hwlayervme
} // namespace tcds

#endif // _tcds_hwlayervme_VMEDeviceBase_h_
