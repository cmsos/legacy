package xdaq.xdesk.xcom;

public class XComSOAPException extends Exception
{
	XComSOAPException () { super(); }
	XComSOAPException ( String s ) { super(s); }
}
