#!/usr/bin/env python

###############################################################################
## Master script to configure a TCDS partition in Dominique's B40 lab
## for FEROL40 testing
###############################################################################

import os
import sys

from pytcds.test_ferol40.tcds_test_ferol40_constants import __file__ as constants_file_name
from pytcds.test_ferol40.tcds_test_ferol40_constants import APVES
from pytcds.test_ferol40.tcds_test_ferol40_constants import CPM
from pytcds.test_ferol40.tcds_test_ferol40_constants import FED_ID_CPM
from pytcds.test_ferol40.tcds_test_ferol40_constants import FED_ID_FEROL40
from pytcds.test_ferol40.tcds_test_ferol40_constants import FED_ID_TCDSADAPTER
from pytcds.test_ferol40.tcds_test_ferol40_constants import ICIS
from pytcds.test_ferol40.tcds_test_ferol40_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class TCDSConfig(CmdLineBase):

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def main(self):
        sep_line = "-" * 70

        test_dir_name = os.path.dirname(constants_file_name)
        fed_enable_mask = "{0:d}&0%{1:d}&2%{2:d}&2%".format(FED_ID_CPM, FED_ID_FEROL40, FED_ID_TCDSADAPTER)

        fsm_ctrl_cmd = self.build_python_cmd("../utils/tcds_fsm_control.py --setup-file {0:s}".format(self.setup_file_name))
        fsm_poll_cmd = self.build_python_cmd("../utils/tcds_fsm_poller.py --setup-file {0:s}".format(self.setup_file_name))
        # pi_force_ready_cmd = self.build_python_cmd("../pi/tcds_pi_force_tts.py --setup-file {0:s}".format(self.setup_file_name))

        cmds = []

        #----------

        # Halt everything.
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Halting all controllers'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

            # Wait till everything is in the Halted state.
            targets = []
            targets = [CPM]
            targets.extend(ICIS)
            targets.extend(APVES)
            targets.extend(PIS)
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure CPM.
        if CPM:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring CPM'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} --fed-enable-mask='{1:s}' --no-beam-active {2:s} Configure {3:s}/hw_cfg_cpm.txt".format(fsm_ctrl_cmd, fed_enable_mask, CPM, test_dir_name), ""))
            # Wait till the CPM is in the Configured state.
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, CPM), ""))

        # Configure iCIs.
        targets = list(ICIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring iCIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_ici.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

            # Wait till everything is in the Configured state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        targets = list(APVES)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring APVEs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Configure {2:s}/hw_cfg_apve.txt".format(fsm_ctrl_cmd, target, test_dir_name), ""))

            # Wait till everything is in the Configured state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        targets = list(PIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Configuring PIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} --fed-enable-mask='{1:s}' {2:s} Configure {3:s}/hw_cfg_pi.txt".format(fsm_ctrl_cmd, fed_enable_mask, target, test_dir_name), ""))

            # Wait till everything is in the Configured state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        #----------

        # # Force PIs to be TTS READY.
        # targets = list(PIS)
        # if len(targets):
        #     cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #                  ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
        #                  ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        #     for target in targets:
        #         cmds.append(("{0:s} {1:s} READY".format(pi_force_ready_cmd, target), ""))

        #----------

        # Enable PIs.
        targets = list(PIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling PIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

            # Wait till everything is in the Enabled state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable APVEs.
        targets = list(APVES)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling APVEs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

            # Wait till everything is in the Enabled state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable iCIs.
        targets = list(ICIS)
        if len(targets):
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling iCIs'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            for target in targets:
                cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

            # Wait till everything is in the Enabled state.
            for target in targets:
                cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, target), ""))

        # Enable CPM (as last!).
        if CPM:
            cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                         ("python -c \"print 'Enabling CPM'\"", ""),
                         ("python -c \"print '{0:s}'\"".format(sep_line), "")])
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, CPM), ""))

        #----------

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfig.

###############################################################################

if __name__ == "__main__":

    description = "Master script to configure a TCDS partition in Dominique's B40 lab for FEROL40 testing."
    res = TCDSConfig(description).run()
    print "Done"
    sys.exit(res)

###############################################################################
