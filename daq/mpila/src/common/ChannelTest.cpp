/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTest.cpp
 *
 *  Created on: Mar 22, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/ChannelTest.hpp"

namespace mpila
{
ChannelTest::ChannelTest(size_t pipelineDepth, WorkRequestType type)
    : Channel(pipelineDepth, type),
      pendingRequestRefCount_(pipelineDepth),
      freeList_(pipelineDepth),
      urgentList_(pipelineDepth)
{
    freeList_.setName("freeList");
    urgentList_.setName("urgentList");

    for (auto& pendingRequest : pendingRequestRefCount_)
    {
        freeList_.push_back(&pendingRequest);
    }
}

PendingRequestQueue& ChannelTest::getFreeList()
{
    return freeList_;
}

PendingRequestQueue& ChannelTest::getUrgentList()
{
    return urgentList_;
}

} /* namespace mpila */
