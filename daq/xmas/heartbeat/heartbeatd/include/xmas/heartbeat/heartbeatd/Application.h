// $Id$

/*************************************************************************
 * XDAQ XMAS Heartbeat Daemon              								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _xmas_heartbeat_heartbeatd_Application_h_
#define _xmas_heartbeat_heartbeatd_Application_h_

#include <string>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xmas/PulserSettings.h"
#include "xmas/exception/Exception.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/mem/Pool.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/EventingDialup.h"
#include "b2in/utils/TopicSplitupIncremental.h"
#include "b2in/utils/Statistics.h"

#include "xmas/heartbeat/heartbeatd/exception/Exception.h"

namespace xmas
{
	namespace heartbeat
	{
		namespace heartbeatd
		{

			class BrokerCommunicationErrorHandler;
			// forward declaration

			class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener
			{
					friend class BrokerCommunicationErrorHandler;

				public:

					XDAQ_INSTANTIATOR();

					Application (xdaq::ApplicationStub* s) ;
					~Application ();

					void actionPerformed (xdata::Event& e);

					void actionPerformed (toolbox::Event& event);
					void timeExpired (toolbox::task::TimerEvent& e);

					//void selfHeartbeat();

					void Default (xgi::Input * in, xgi::Output * out) ;

					void onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) ;

					void queryBroker ();

					void publishHeartbeat (toolbox::mem::Reference * msg, xdata::Properties & plist);


				protected:

					void StatisticsTabPage (xgi::Output * out);
					void TabPanel (xgi::Output * out);

				private:


					xdata::String brokerGroup_; // indicates the group the broker is running in
					xdata::String brokerURL_; // setting a broker URL avoids the use of service discovery for searching the broker
					xdata::String brokerProfile_; // indicates the profile to use for retrieving the services from the broker
					xdata::String brokerDialupWatchdog_;
					xdata::String eventingURL_;

					xdata::String publishGroup_;

					xdata::String heartbeatWatchdog_;

					xdata::UnsignedInteger32 lossReportCounter_; // only print send errors every (msg % lossReportCounter) = 1

					xdata::exdr::Serializer serializer_;

					b2in::utils::EventingDialup * dialup_;

					xdata::UnsignedInteger64 heartbeatInternalLossCounter_;
					xdata::UnsignedInteger64 heartbeatEnqueuedCounter_;
					xdata::UnsignedInteger64 heartbeatNoNetworkLossCounter_;
					xdata::UnsignedInteger64 heartbeatIncomingCounter_;
			};
		}
	}
}
#endif
