package xdaq.tools.win;

import org.w3c.dom.*;
import java.io.*;
import java.util.*;
import javax.xml.*;
import javax.xml.parsers.*;
import org.xml.sax.*;

public class StringToDOM
{
	public static Document dom ( String xml)
	{
		if ( xml.equals("") ) return null;

		Document document = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			document = documentBuilder.parse(new InputSource(new StringReader(xml)));
		} catch ( javax.xml.parsers.ParserConfigurationException e )
		{
			System.out.println(" error: malformed xml args");

		} catch (org.xml.sax.SAXException ex) {

			System.out.println(" error: malformed xml args");
		} catch ( java.io.IOException exx ) {

			System.out.println(" error: malformed xml args");
		}
		return document;
	}
}
