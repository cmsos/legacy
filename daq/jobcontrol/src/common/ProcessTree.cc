
#include "jobcontrol/ProcessTree.h"

#include "pstream.h"

#include <toolbox/regex.h>

#include <string>
#include <iterator>
#include <iostream>
#include <fstream>
#include <sstream>

// using namespace redi;

void jobcontrol::ProcessTree::takeSnapshot(){
  // Takes a snapshot of the processes and maps them
  
  // First clear process tree map of any previous snapshot 
  pid_.clear();
  
  // List the content of directory /proc
  redi::ipstream lsProc( "ls /proc" );
  std::string reply;
  std::list<std::string> replyLines;
  while ( std::getline(lsProc, reply) ) {
    replyLines.push_back(reply);
  }
  // copy( replyLines.begin(), replyLines.end(), ostream_iterator<string>(cout, "\n") );

  // Loop over all subdirectories that correspond to processes
  for ( std::list<std::string>::const_iterator p = replyLines.begin(); p != replyLines.end(); ++p ){
    // The relevant subdirectories are named after the process id
    if ( toolbox::regx_match( *p, "^[0-9][0-9]*$" ) ){
      int pid;
     std:: stringstream ss;
      ss << *p;
      ss >> pid;
      // Dig up the id of the parent of this process
      int ppid = getPPID( *p );
      // Add it to the ppid-->pid multivalued map
      pid_.insert( std::pair<int,int>(ppid,pid) );
    }
  }

//   for ( multimap<int, int>::const_iterator p = pid_.begin(); p != pid_.end(); ++p )
//     cout << "    " << p->first << "   " << p->second << endl;
}

int jobcontrol::ProcessTree::getPPID( const std::string pid ){
  // Get ppid from /proc/<pid>/stat

  std::string procStatFilePath = "/proc/" + pid + "/stat";
  std::ifstream pfs( procStatFilePath.c_str() );
  int ppid = 0;
  // ppid is the fourth field
  const int ppidFieldPosition = 4;
  int fieldCount = 0;
  // Read in up to the 'ppid' field
  while ( pfs.good() && fieldCount<ppidFieldPosition ){
    std::string field;
    pfs >> field;
    fieldCount++;
    if ( fieldCount == ppidFieldPosition ){
      std::stringstream ss;
      ss << field;
      ss >> ppid;
    }
  }
  pfs.close();
  return ppid;
}

std::list<int> jobcontrol::ProcessTree::getDescendantPIDs( const int pid, int indentDepth ){
  // Recursively collect all processes that are descended from process 'pid'.

  // Set indent for pretty printing the tree
  const std::string indentUnit("   ");
  std::string indent;
  for ( int i=indentDepth; i>=0; --i ) indent += indentUnit;

  // The collection of all descendants to be returned
  std::list<int> allDescendants;

  // Get child processes
  std::list<int> c = getChildPIDs( pid );
  // Loop over child processes
  for ( std::list<int>::const_iterator ci = c.begin(); ci != c.end(); ++ci ){
//    cout << indent << "|_ " << *ci << endl;
    ++indentDepth;
    // Get all descendants of this child
    std::list<int> d = getDescendantPIDs( *ci, indentDepth );
    --indentDepth;
    // Add this child...
    allDescendants.push_back( *ci );
    // ...and all its descendants to the collection of descendants
    allDescendants.splice( allDescendants.end(), d );
  }
  return allDescendants;
}

std::list<int> jobcontrol::ProcessTree::getChildPIDs( const int pid ){
  // Collect all immediate children of process 'pid'.

  std::list<int> c;
  std::pair<std::multimap<int,int>::iterator, std::multimap<int,int>::iterator> range;
  range = pid_.equal_range( pid );
  for ( std::multimap<int,int>::iterator i = range.first; i != range.second; ++i )
    c.push_back( i->second );
  return c;
}


std::string jobcontrol::ProcessTree::getDescendantTree( const int pid, int indentDepth ){
  // Recursively collect all processes that are descended from process 'pid'.

  // Set indent for pretty printing the tree
  const std::string indentUnit("   ");
  std::string indent;
  std::stringstream tree;
  for ( int i=indentDepth; i>=0; --i ) indent += indentUnit;

  // Get child processes
  std::list<int> c = getChildPIDs( pid );
  // Loop over child processes
  for ( std::list<int>::const_iterator ci = c.begin(); ci != c.end(); ++ci ){
    tree << indent << "|_ " << (*ci) << std::endl;
    ++indentDepth;
    // Get all descendants of this child
    std::string  childTree = getDescendantTree( *ci, indentDepth );
    --indentDepth;
    tree << childTree;
  }
  return tree.str();
}
