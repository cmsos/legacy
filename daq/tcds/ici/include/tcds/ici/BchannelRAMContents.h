#ifndef _tcds_ici_BchannelRAMContents_h_
#define _tcds_ici_BchannelRAMContents_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/ici/BchannelRAMEntry.h"

namespace tcds {
  namespace ici {

    class BchannelRAMContents
    {

    public:
      BchannelRAMContents(std::vector<uint32_t> const dataIn);

      std::string getJSONString() const;

    private:
      // This constructor is not supposed to be used.
      BchannelRAMContents();

      std::vector<uint32_t> const rawData_;
      std::vector<BchannelRAMEntry> entries_;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_BchannelRAMContents_h_
