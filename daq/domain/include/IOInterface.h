#ifndef __IOInterface_h__
#define __IOInterface_h__

#include "DAQBinding.h"

// class IOInterface defines the IO environment
// for a given configuration
// and provides the interface to send or
// receive messages of any type
// there must be a unique relation between
// a message type and a physical data link path
// (defined at configuration time)
// All extra methods to add private functionality
// (configuration, ...) can specified in the derived class 
// and does not need to be specified.
// UAddress is defined in DAQBinding.h
// 
// Two equivalent interface aproaches are defined, the first namely "I/O Manager"
// where all methods specify the logical addresses for source and
// destination, the second "I/O channel" where source and destination
// are not defined in the methods but belong to the channel itself.
// Both approaches provide traditional send and receive methods as
// well as and buffer loaning (zero-copy). 
// 
// The following macro definition should be defined to activate one
// particular implementation
//
// #define IO_MANAGER_TRADITONAL
// #define IO_MANAGER_ZBUF
// #define IO_CHANNEL_TRDITIONAL
// #define IO_CHANNEL_ZBUF
//  

class IOInterface
{

	public:
	
	//------------------------------------------------------------------
	// I/O Manager
	//------------------------------------------------------------------
	
	//
	// Traditional
	//
	// send a message to a logical destid
	// (logical destids are defined at configuration time)
	virtual void send(char *message, int len, UAddress source , UAddress destination) = 0;
	// receive a message from the I/O
	virtual void receive(char *message, int &len, UAddress & address) = 0;	
	// force transmission of messages 
	//
	// zero-copy
	//
	// 
	virtual void post_zbuf(char * zbuf, int len, UAddress source , UAddress destination) =0;
	virtual char * receive_zbuf(int & len, UAddress & source) =0;
	
	//------------------------------------------------------------------
	// I/O Channel
	//------------------------------------------------------------------
	
	//
	// Traditional
	//
	virtual void send(char *message, int len) = 0;
	// receive a message len == 0 if no data available
	virtual void receive(char *message, int &len) = 0;	
	//
	// zero-copy
	//
	virtual void post_zbuf(char * zbuf, int len) = 0;
	// return null and len == 0 if no data available
	virtual char * receive_zbuf(int & len) = 0;
	
	//
	// Zero Copy Common
	//
	virtual char * get_zbuf()  = 0;
	virtual void release_zbuf(char * buf ) = 0;

};


#endif
