// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "Alert.h"
#include "sentinel/utils/Sensor.h"
#include "xgi/Method.h"
#include "xgi/framework/Method.h"

//
// provides factory method for instantion of Guardian  application
//
XDAQ_INSTANTIATOR_IMPL (Alert)

Alert::Alert(xdaq::ApplicationStub * s)
		: xdaq::Application(s), xgi::framework::UIManager(this)
{

	//export a Web interface for easy self test command
	xgi::framework::deferredbind(this, this, &Alert::Default, "Default");
	xgi::bind(this, &Alert::raise, "raise");
	xgi::bind(this, &Alert::revoke, "revoke");

}

Alert::~Alert()
{

}

void Alert::raise(xgi::Input * in, xgi::Output * out) 
{
	std::string identifier = "myAlert";
	std::string severity = "error";
	std::string message = "this is my alert";
	std::string tag = "myTag";

	cgicc::Cgicc cgi(in);
	cgicc::const_form_iterator fi = cgi.getElement("identifier");
	if (fi != cgi.getElements().end())
	{
		identifier = (*fi).getValue();
	}

	fi = cgi.getElement("severity");
	if (fi != cgi.getElements().end())
	{
		severity = (*fi).getValue();
	}

	fi = cgi.getElement("message");
	if (fi != cgi.getElements().end())
	{
		message = (*fi).getValue();
	}

	fi = cgi.getElement("tag");
	if (fi != cgi.getElements().end())
	{
		tag = (*fi).getValue();
	}
	SENTINEL_RAISE_ALARM( xcept::Exception, identifier, severity, message, tag );
}

void Alert::revoke(xgi::Input * in, xgi::Output * out) 
{

	std::string identifier = "myAlert";

	cgicc::Cgicc cgi(in);
	cgicc::const_form_iterator fi = cgi.getElement("identifier");
	if (fi != cgi.getElements().end())
	{
		identifier = (*fi).getValue();
	}

	SENTINEL_REVOKE_ALARM(identifier);
}

void Alert::Default(xgi::Input * in, xgi::Output * out) 
{
}

