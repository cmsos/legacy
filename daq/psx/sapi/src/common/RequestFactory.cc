// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdlib.h>
 
#include "psx/sapi/DpSetRequest.h"
#include "psx/sapi/DpConnectRequest.h"
#include "psx/sapi/DpGetRequest.h"
#include "psx/sapi/DpGetNamesRequest.h"
#include "psx/sapi/DpGetAllSystemsRequest.h"
#include "psx/sapi/DpGetFieldsRequest.h"
#include "psx/sapi/ApplicationService.h"
#include "psx/sapi/DpDisconnectRequest.h"
#include "psx/sapi/RequestFactory.h"

#include "xoap/domutils.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"




psx::sapi::Request* psx::sapi::RequestFactory::createRequest(xoap::MessageReference message, psx::sapi::ApplicationService* as) 
	
{
	psx::sapi::Request* retVal = 0;

	DOMNode* body  = message->getSOAPPart().getEnvelope().getBody().getDOMNode();
	DOMNodeList * children = body->getChildNodes();
	for ( XMLSize_t i = 0 ; i < children->getLength(); i++ )
	{
		DOMNode * node = children->item(i);
				
		if ( node->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{ 
			if ( xoap::XMLCh2String(node->getLocalName()) == "dpGet" )
			{
				psx::sapi::DpGetRequest * rqst = new psx::sapi::DpGetRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::sapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
				} 					
				DOMNodeList * dpoints = node->getChildNodes();
				for ( XMLSize_t i = 0 ; i < dpoints->getLength(); i++ )
				{
					DOMNode * dp = dpoints->item(i);
					if (( node->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(dp->getLocalName()) == "dp" ) )
					{	
					
						std::string dpName = xoap::getNodeAttribute(dp, "name" );
						if ( dpName != "" )
						{
							rqst->addDpName( dpName );
						}
						else
						{
							delete rqst;
							std::string msg = "missing name attribute in <dp/> tag";
							msg += nsURI;
							XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
						}
					}	
				}
				
				retVal = rqst;
				break;
			
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "dpSet" )
			{
				psx::sapi::DpSetRequest * rqst = new psx::sapi::DpSetRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::sapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
				} 					
				DOMNodeList * dpoints = node->getChildNodes();
				for ( XMLSize_t i = 0 ; i < dpoints->getLength(); i++ )
				{
					DOMNode * dp = dpoints->item(i);
					if (( node->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(dp->getLocalName()) == "dp" ) )
					{	
					
						std::string dpName = xoap::getNodeAttribute(dp, "name" );
						std::string dpValue = xoap::XMLCh2String(dp->getTextContent());
						if ( dpName != "" )
						{
							rqst->addDpValue( dpName, dpValue );
						}
						else
						{
							delete rqst;
							std::string msg = "missing name attribute in <dp/> tag";
							msg += nsURI;
							XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
						}
					}	
				}	
				retVal = rqst;
				break;
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "dpGetNames" )
			{	
				psx::sapi::DpGetNamesRequest * rqst = new psx::sapi::DpGetNamesRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::sapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI, expected ";
					msg += psx::sapi::NSURI;
					msg += ", received ";
					msg += nsURI;
					XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
				} 					
				
				std::string pattern = xoap::XMLCh2String(node->getTextContent());
				

				
				// Set the pattern if given, otherwise the default is already "*"
				if (pattern != "" )
				{
					// Trim the pattern from leading 0, \n etc
					std::string::size_type pos = pattern.find_last_not_of ("\n\r\t ");
					if (pos != std::string::npos)
					{
						pattern.erase(pos + 1);
						pos = pattern.find_first_not_of ("\n\r\t ");
						if (pos != std::string::npos) pattern.erase (0, pos);
					}
					else
					{
						// bad string: onlu spaces or rubbish
						pattern.erase(pattern.begin(), pattern.end());
					}
								std::cout << "dpGetNames pattern: [" << pattern << "]" << std::endl;

					rqst->setPattern(pattern);
				}
				
				retVal = rqst;
				break;				
			}
			if ( xoap::XMLCh2String(node->getLocalName()) == "dpConnect" )
			{
				psx::sapi::DpConnectRequest * rqst = new psx::sapi::DpConnectRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::sapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
				} 
				// 
				std::string context = xoap::getNodeAttribute(node, "context" );
				
				std::string url = xoap::getNodeAttribute(node, "url" );
				if ( url == "" )
				{
					delete rqst;
					std::string msg = "missing url attribute or empty";		
					XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
				}
				
				std::string action = xoap::getNodeAttribute(node, "action" );				
				
				rqst->setContext(context);
				rqst->setURL(url);
				rqst->setAction(action);
				//					
				DOMNodeList * dpoints = node->getChildNodes();
				for ( XMLSize_t i = 0 ; i < dpoints->getLength(); i++ )
				{
					DOMNode * dp = dpoints->item(i);
					if (( node->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(dp->getLocalName()) == "dp" ) )
					{	
					
						std::string dpName = xoap::getNodeAttribute(dp, "name" );
						if ( dpName != "" )
						{
							rqst->addDpName( dpName );
						}
						else
						{
							delete rqst;
							std::string msg = "missing name attribute in <dp/> tag";
							msg += nsURI;
							XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
						}
					}	
				}
				
				retVal = rqst;
				break;
			
			}
			if ( xoap::XMLCh2String(node->getLocalName()) == "dpDisconnect" )
			{
				psx::sapi::DpDisconnectRequest * rqst = new psx::sapi::DpDisconnectRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::sapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
				} 
				
				std::string transactionId = xoap::getNodeAttribute(node, "id" );
				if ( transactionId != "" )
				{
				
					rqst->setTransactionId( transactionId );
				}
				else
				{
					delete rqst;
					std::string msg = "Missing transaction identifier";
					XCEPT_RAISE(psx::sapi::exception::Exception, msg ); 
				}
				
				retVal = rqst;
				break;
			
			}
			 else if ( xoap::XMLCh2String(node->getLocalName()) == "dpGetFields" )
                        {
                                psx::sapi::DpGetFieldsRequest * rqst = new psx::sapi::DpGetFieldsRequest( as );
                                std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());
                                if ( nsURI != psx::sapi::NSURI )
                                {
                                        delete rqst;
                                        std::string msg = "invalid namespace URI";
                                        msg += nsURI;
                                        XCEPT_RAISE(psx::sapi::exception::Exception, msg );
                                }

                                std::string name = xoap::XMLCh2String(node->getTextContent());



                                // Set the pattern if given, otherwise the default is already "*"
                                if (name != "" )
                                {
                                        // Trim the pattern from leading 0, \n etc
                                        std::string::size_type pos = name.find_last_not_of ("\n\r\t ");
                                        if (pos != std::string::npos)
                                        {
                                                name.erase(pos + 1);
                                                pos = name.find_first_not_of ("\n\r\t ");
                                                if (pos != std::string::npos) name.erase (0, pos);
                                        }
                                        else
                                        {
                                                // bad string: onlu spaces or rubbish
                                                name.erase(name.begin(), name.end());
                                        }

                                        rqst->setName(name);
                                }

                                retVal = rqst;
				break;
                        }
			else if ( xoap::XMLCh2String(node->getLocalName()) == "dpGetAllSystems" )
                        {
                                psx::sapi::DpGetAllSystemsRequest * rqst = new psx::sapi::DpGetAllSystemsRequest( as );
                                std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());
                                if ( nsURI != psx::sapi::NSURI )
                                {
                                        delete rqst;
                                        std::string msg = "invalid namespace URI, expected ";
                                        msg += psx::sapi::NSURI;
                                        msg += ", received ";
                                        msg += nsURI;
                                        XCEPT_RAISE(psx::sapi::exception::Exception, msg );
                                }

				 std::string pattern = xoap::XMLCh2String(node->getTextContent());



                                // Set the pattern if given, otherwise the default is already "*"
                                if (pattern != "" )
                                {
                                        // Trim the pattern from leading 0, \n etc
                                        std::string::size_type pos = pattern.find_last_not_of ("\n\r\t ");
                                        if (pos != std::string::npos)
                                        {
                                                pattern.erase(pos + 1);
                                                pos = pattern.find_first_not_of ("\n\r\t ");
                                                if (pos != std::string::npos) pattern.erase (0, pos);
                                        }
                                        else
                                        {
                                                // bad string: onlu spaces or rubbish
                                                pattern.erase(pattern.begin(), pattern.end());
                                        }
                                        std::cout << "dpGetAllSystems pattern: [" << pattern << "]" << std::endl;

                                        rqst->setPattern(pattern);
                                }


                                retVal = rqst;
                                break;
                        }

			else
			{
				XCEPT_RAISE(psx::sapi::exception::Exception,"unknown command");
			
			}

			
		}
		
	}
	return retVal;	
}
