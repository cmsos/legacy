#include "tcds/ttcspy/ConfigurationInfoSpaceHandler.h"

#include "tcds/ttcspy/TCADeviceTTCSpy.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::ttcspy::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // IPbus connection parameters.
  createString("controlhubAddress", "controlhub_tcds");
  createUInt32("controlhubPort", 10203);
  createString("deviceAddress", "ttcspy");
  createUInt32("devicePort", 50001);

  // The default is to spy on the TTC stream from the TTC FMC
  // front-panel input.
  // createUInt32("ttcSource", tcds::ttcspy::TCADeviceTTCSpy::TTC_SOURCE_FRONTPANEL);

  // // Default settings for the logging mode and logic.
  // createUInt32("loggingMode", tcds::ttcspy::TCADeviceTTCSpy::LOGGING_MODE_ONLY);
  // createUInt32("loggingLogic", tcds::ttcspy::TCADeviceTTCSpy::LOGGING_LOGIC_OR);

  // Default settings for the TTCSpy filter/trigger masks.
  createBool("filterCommunicationError", false);
  createBool("filterDoubleBitError", false);
  createBool("filterSingleBitError", false);
  createBool("filterL1A", false);
  createBool("filterLongCommandsAll", false);
  createBool("filterShortCommandsAll", false);
  createBool("filterShortUserDataAll", false);
  createBool("filterShortSystemDataAll", false);
  createUInt32("filterShortUserData", 0x0);
  createUInt32("filterShortSystemData", 0x0);
  createBool("filterEvCntRes", false);
  createBool("filterBCntRes", false);

  createBool("triggerCommunicationError", false);
  createBool("triggerDoubleBitError", false);
  createBool("triggerSingleBitError", false);
  createBool("triggerL1A", false);
  createBool("triggerLongCommandsAll", false);
  createBool("triggerShortCommandsAll", false);
  createBool("triggerShortUserDataAll", false);
  createBool("triggerShortSystemDataAll", false);
  createUInt32("triggerShortUserData", 0x0);
  createUInt32("triggerShortSystemData", 0x0);
  createBool("triggerEvCntRes", false);
  createBool("triggerBCntRes", false);
}

std::string
tcds::ttcspy::ConfigurationInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  // The TTCSpy ConfigurationInfoSpaceHandler implements specialized
  // formatting rules for our enums. Everything else is passed on for
  // formatting to the good old
  // tcds::utils::ConfigurationInfoSpaceHandler.

  std::string name = item->name();
  std::string res = "";
  // if (name == "ttcSource")
  //   {
  //     uint32_t val = getUInt32(name);
  //     switch (val)
  //       {
  //       case tcds::ttcspy::TCADeviceTTCSpy::TTC_SOURCE_FRONTPANEL:
  //         res = "front-panel input";
  //         break;
  //       case tcds::ttcspy::TCADeviceTTCSpy::TTC_SOURCE_BACKPLANE:
  //         res = "backplane (port 3)";
  //         break;
  //       default:
  //         res = "Unknown";
  //         break;
  //       }
  //   }
  // else if (name == "loggingMode")
  //   {
  //     uint32_t val = getUInt32(name);
  //     switch (val)
  //       {
  //       case tcds::ttcspy::TCADeviceTTCSpy::LOGGING_MODE_ONLY:
  //         res = "log-only";
  //         break;
  //       case tcds::ttcspy::TCADeviceTTCSpy::LOGGING_MODE_EXCEPT:
  //         res = "log-all-except";
  //         break;
  //       default:
  //         res = "Unknown";
  //         break;
  //       }
  //   }
  // else if (name == "loggingLogic")
  //   {
  //     uint32_t val(getUInt32(name));
  //     switch (val)
  //       {
  //       case tcds::ttcspy::TCADeviceTTCSpy::LOGGING_LOGIC_OR:
  //         res = "OR";
  //         break;
  //       case tcds::ttcspy::TCADeviceTTCSpy::LOGGING_LOGIC_AND:
  //         res = "AND";
  //         break;
  //       default:
  //         res = "Unknown";
  //         break;
  //       }
  //   }
  // else
  //   {
  // For all non-enums simply call the generic formatter.
  res = InfoSpaceHandler::formatItem(item);
  //   }

  return res;
}

void
tcds::ttcspy::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("Hardware Connection");
  monitor.addItem("Hardware Connection",
                  "controlhubAddress",
                  "IPbus control-hub address/alias",
                  this);
  monitor.addItem("Hardware Connection",
                  "controlhubPort",
                  "IPbus control-hub port number",
                  this);
  monitor.addItem("Hardware Connection",
                  "deviceAddress",
                  "IPbus device address/alias",
                  this);
  monitor.addItem("Hardware Connection",
                  "devicePort",
                  "IPbus device port number",
                  this);

  // The TTCSpy logging configuration.
  // - The source of the TTC stream being spied on.
  monitor.newItemSet("Logging Config");
  // monitor.addItem("Logging Config",
  //                  "ttcSource",
  //                  "TTC source",
  //                  this);
  // // - The TTCSpy logging and logic modes.
  // monitor.addItem("Logging Config",
  //                  "loggingMode",
  //                  "Logging mode",
  //                  this);
  // monitor.addItem("Logging Config",
  //                  "loggingLogic",
  //                  "Logging logic",
  //                  this);

  // The TTCSpy filter mask settings.
  monitor.newItemSet("Filter Mask");
  monitor.addItem("Filter Mask",
                  "filterCommunicationError",
                  "Filter communication error bit?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterDoubleBitError",
                  "Filter double-bit error bit?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterSingleBitError",
                  "Filter single-bit error bit?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterL1A",
                  "Filter L1A?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterLongCommandsAll",
                  "Filter all long B-commands?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterShortCommandsAll",
                  "Filter all short B-commands?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterShortUserDataAll",
                  "Filter all broadcast user data?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterShortSystemDataAll",
                  "Filter all broadcast system data?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterShortUserData",
                  "Filter value for broadcast user data",
                  this);
  monitor.addItem("Filter Mask",
                  "filterShortSystemData",
                  "Filter value for broadcast system data",
                  this);
  monitor.addItem("Filter Mask",
                  "filterEvCntRes",
                  "Filter EvCntRes bit?",
                  this);
  monitor.addItem("Filter Mask",
                  "filterBCntRes",
                  "Filter BCntRes bit?",
                  this);

  // The TTCSpy trigger mask settings.
  monitor.newItemSet("Trigger Mask");
  monitor.addItem("Trigger Mask",
                  "triggerCommunicationError",
                  "Trigger communication error bit?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerDoubleBitError",
                  "Trigger double-bit error bit?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerSingleBitError",
                  "Trigger single-bit error bit?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerL1A",
                  "Trigger on L1A?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerLongCommandsAll",
                  "Trigger on all long B-commands?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerShortCommandsAll",
                  "Trigger on all short B-commands?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerShortUserDataAll",
                  "Trigger all broadcast user data?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerShortSystemDataAll",
                  "Trigger all broadcast system data?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerShortUserData",
                  "Trigger value for broadcast user data",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerShortSystemData",
                  "Trigger value for broadcast system data",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerEvCntRes",
                  "Trigger on EvCntRes bit?",
                  this);
  monitor.addItem("Trigger Mask",
                  "triggerBCntRes",
                  "Trigger on BCntRes bit?",
                  this);
}

void
tcds::ttcspy::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                           tcds::utils::Monitor& monitor,
                                                                           std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  // One tab showing all configuration information.
  webServer.registerTab(tabName,
                        "The application configuration items",
                        3);
  webServer.registerTable("Hardware Connection",
                          "Hardware connection items",
                          monitor,
                          "Hardware Connection",
                          tabName);
  webServer.registerTable("Logging Config",
                          "Logging configuration items",
                          monitor,
                          "Logging Config",
                          tabName);
  webServer.registerTable("Filter Mask",
                          "TTCSpy logging filter mask",
                          monitor,
                          "Filter Mask",
                          tabName);
  webServer.registerTable("Trigger Mask",
                          "TTCSpy logging trigger mask",
                          monitor,
                          "Trigger Mask",
                          tabName);
}
