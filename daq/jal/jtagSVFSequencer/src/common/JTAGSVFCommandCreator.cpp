#include "jal/jtagSVFSequencer/JTAGSVFCommandCreator.h"

#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryScan.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryEnddr.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryEndir.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryState.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryRunTest.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryFrequency.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryTRST.h"

#include <iostream>

using namespace std;

jal::JTAGSVFCommandCreator::JTAGSVFCommandCreator () {

  _factory_map["SDR"]       = new jal::JTAGSVFCommandFactoryScan( jal::JTAGSVFCommandScan::SDR );
  _factory_map["SIR"]       = new jal::JTAGSVFCommandFactoryScan( jal::JTAGSVFCommandScan::SIR );
  _factory_map["HDR"]       = new jal::JTAGSVFCommandFactoryScan( jal::JTAGSVFCommandScan::HDR );
  _factory_map["HIR"]       = new jal::JTAGSVFCommandFactoryScan( jal::JTAGSVFCommandScan::HIR );
  _factory_map["TDR"]       = new jal::JTAGSVFCommandFactoryScan( jal::JTAGSVFCommandScan::TDR );
  _factory_map["TIR"]       = new jal::JTAGSVFCommandFactoryScan( jal::JTAGSVFCommandScan::TIR );
  _factory_map["ENDDR"]     = new jal::JTAGSVFCommandFactoryEnddr();
  _factory_map["ENDIR"]     = new jal::JTAGSVFCommandFactoryEndir();
  _factory_map["STATE"]     = new jal::JTAGSVFCommandFactoryState();
  _factory_map["RUNTEST"]   = new jal::JTAGSVFCommandFactoryRunTest();
  _factory_map["FREQUENCY"] = new jal::JTAGSVFCommandFactoryFrequency();
  _factory_map["TRST"]      = new jal::JTAGSVFCommandFactoryTRST();
}

jal::JTAGSVFCommandCreator::~JTAGSVFCommandCreator() {

  map<string, jal::JTAGSVFCommandFactory*>::iterator it = _factory_map.begin();
  
  for (;it != _factory_map.end(); it++)
    delete (*it).second;
}

void jal::JTAGSVFCommandCreator::reset() {

  map<string, jal::JTAGSVFCommandFactory*>::iterator it = _factory_map.begin();
  
  for (;it != _factory_map.end(); it++)
    (*it).second->reset();
  
}


jal::JTAGSVFCommand* jal::JTAGSVFCommandCreator::create(vector<string> const& args) 
  throw (jal::SVFSyntaxException) {
  
  if (args.size() < 1) 
    XCEPT_RAISE(jal::SVFSyntaxException, "Zero length command encountered.");

  if ( _factory_map.find(args[0]) == _factory_map.end() )
    XCEPT_RAISE(jal::SVFSyntaxException, "Unknown command encountered: " + args[0]);

  return _factory_map[args[0]]->create(args);
};
	     
