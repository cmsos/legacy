package xmltreeeditor;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import javax.swing.JScrollBar;

public class XMLTableCellEditor
    extends JScrollPane
    implements TableCellEditor, ChangeListener {

  public JTable tbl;
  public int col;
  JTextArea textArea=null;

  public XMLTableCellEditor() {
    super();

    this.setAutoscrolls(true);

    textArea=new JTextArea();
    textArea.setFont(new Font("Arial",Font.BOLD,11));
    textArea.setBorder(BorderFactory.createLineBorder(Color.white));
    textArea.setLineWrap(true);
    textArea.setBackground(Color.LIGHT_GRAY);
    this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    this.getViewport().add(textArea);
    this.setHorizontalScrollBar(new JScrollBar());
    this.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
  }

  public void stateChanged(ChangeEvent e) {

  }

  public Component getTableCellEditorComponent(JTable table, Object value,
                                               boolean isSelected, int row,
                                               int column) {

    col = column;
    tbl = table;
    textArea.setText(table.getValueAt(row, column).toString());
    return this;
  }

  public void removeCellEditorListener(CellEditorListener l) {

  }

  public void addCellEditorListener(CellEditorListener l) {

  }

  public void cancelCellEditing() {

  }

  public boolean stopCellEditing() {
    try {

      XMLTreeCellRenderer n_parent = (XMLTreeCellRenderer)this.getParent();
      XMLTree xml_tree = n_parent.get_Tree();
      TreePath n_path = n_parent.get_Tree().getSelectionPath();
      DefaultMutableTreeNode n_node = (DefaultMutableTreeNode) n_path.
          getLastPathComponent();
      DefaultMutableTreeNode n_parent_node = (DefaultMutableTreeNode) n_node.
          getParent();
      XMLTreeNodeUserObject n_obj = (XMLTreeNodeUserObject) n_node.
          getUserObject();
      XMLTreeNodeUserObject n_parent_obj = (XMLTreeNodeUserObject)
          n_parent_node.getUserObject();

      tbl.getModel().setValueAt(new EditableText(this.getCellEditorValue().
                                                 toString()), tbl.getEditingRow(),
                                tbl.getEditingColumn());

      n_parent.cancelCellEditing();
      xml_tree.startEditingAtPath(n_path);
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }

    return true;
  }

  public boolean shouldSelectCell(EventObject anEvent) {
    return true;
  }

  public boolean isCellEditable(EventObject anEvent) {
    boolean retValue = true;
    return retValue;
  }

  public Object getCellEditorValue() {
    return textArea.getText();
  }

}
