// $Id$
#ifndef _bril_bcm1fprocessor_version_h_
#define _bril_bcm1fprocessor_version_h_
#include "config/PackageInfo.h"
#define BRILBCM1FPROCESSOR_VERSION_MAJOR 1
#define BRILBCM1FPROCESSOR_VERSION_MINOR 6
#define BRILBCM1FPROCESSOR_VERSION_PATCH 1
#define BRILBCM1FPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRILBCM1FPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILBCM1FPROCESSOR_VERSION_MAJOR,BRILBCM1FPROCESSOR_VERSION_MINOR,BRILBCM1FPROCESSOR_VERSION_PATCH)
#ifndef BRILBCM1FPROCESSOR_PREVIOUS_VERSIONS
#define BRILBCM1FPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBCM1FPROCESSOR_VERSION_MAJOR,BRILBCM1FPROCESSOR_VERSION_MINOR,BRILBCM1FPROCESSOR_VERSION_PATCH)
#else
#define BRILBCM1FPROCESSOR_FULL_VERSION_LIST  BRILBCM1FPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBCM1FPROCESSOR_VERSION_MAJOR,BRILBCM1FPROCESSOR_VERSION_MINOR,BRILBCM1FPROCESSOR_VERSION_PATCH)
#endif
namespace brilbcm1fprocessor{
  const std::string package = "brilbcm1fprocessor";
  const std::string versions = BRILBCM1FPROCESSOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bcm1fprocessor";
  const std::string description = "collect and process bcm1f histograms from bcm1fsource";
  const std::string authors = "J. Leonard";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
