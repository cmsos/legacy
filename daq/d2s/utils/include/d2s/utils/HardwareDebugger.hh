//JRF TODO, this class really should just be an interface with generic methods. Specifics should go in the projects as a derived class such as AMC13HardwareDebugger.cc
//
#ifndef __HardwareDebugger
#define __HardwareDebugger

#include <list>
#include "d2s/utils/HardwareDebugItem.hh"
//#include "d2s/utils/HardwareDevice.hh"
#include "hal/PCIDevice.hh"
#include "xoap/Method.h"
#include "log4cplus/logger.h"
#include <vector>
namespace utils 
{

    class HardwareDebugger { 
    public:
        
        HardwareDebugger( log4cplus::Logger logger, std::vector<HAL::HardwareDeviceInterface  *> devices); 

        xoap::MessageReference readItem( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        xoap::MessageReference writeItem( xoap::MessageReference msg ) 
            throw( xoap::exception::Exception);

        std::list< utils::HardwareDebugItem > getSlinkExpressRegisters( uint32_t linkNo );

        void dumpHardwareRegisters( std::string suffix );

        std::list<utils::HardwareDebugItem> getAMC13Registers();

        std::list<utils::HardwareDebugItem> getFrlRegisters();

        std::list<utils::HardwareDebugItem> getBridgeRegisters();


    private:
        std::list< utils::HardwareDebugItem > getPCIRegisters( HAL::HardwareDeviceInterface *device );

        log4cplus::Logger logger_;
        void dumpRegistersToFile( std::string name, const std::list<utils::HardwareDebugItem> &regsisters, std::string suffix = "" );

        HAL::HardwareDeviceInterface *frlDevice_P;
        HAL::HardwareDeviceInterface *bridgeDevice_P;
        HAL::HardwareDeviceInterface *amc13Device_P;
        HAL::HardwareDeviceInterface *slexp_P;
// JRF TODO replace these with a vector of pointers to base class

        uint32_t slot_;

    };
}
#endif /* __HardwareDebugger */
