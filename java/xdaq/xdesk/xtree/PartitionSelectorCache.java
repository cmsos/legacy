package xdaq.xdesk.xtree;

import javax.swing.*;
import java.util.*;
import javax.swing.tree.*;
import javax.swing.event.*;

public class PartitionSelectorCache
{
	HashMap scrollPanes_;	
	HashMap treeModels_;
        HashMap  treePanels_;
        TreeSelectionListener listener_;
        XTreeFilter filter_;	
	
	public PartitionSelectorCache (TreeSelectionListener listener, String [] filters)
	{
                listener_ = listener;
		scrollPanes_ = new HashMap();
		treeModels_ = new HashMap();
                treePanels_ = new HashMap();

		filter_ = new XTreeFilter();
		
		for (int i = 0; i < filters.length; i++ )
			filter_.addFilter (filters[i]);
		
		//filter_.addFilter ("urlTransport");
		//filter_.addFilter ("Definitions");
		//filter_.addFilter ("Commands");			
		//filter_.addFilter ("urlApplication");
		//filter_.addFilter ("Address");
		
		// This can be filtered out, cause the XTreeNode functions
		// to get children operates on the underlying DOM tree.
		//filter_.addFilter ("DefaultParameters");
	}
	
	
	
	public JScrollPane getScrollPane (String name, TreeModel model)
	{
		JScrollPane pane = (JScrollPane)scrollPanes_.get(name);
		TreeModel current = (TreeModel)treeModels_.get(name);
		
		if ( pane == null )
		{
			XTreeModel submodel = ((XTreeModel)model).cloneModelWithFilter(filter_);
			XTree tree = new XTree(submodel);
			tree.setRootVisible(false); // don't show root node
			
                        tree.addTreeSelectionListener(listener_);
			scrollPanes_.put(name, new JScrollPane(tree));
			
			treeModels_.put(name,model);
                        treePanels_.put(name,tree);
                        tree.setSelectionRow(0);
                      			
		}
		else if ( model != current ) 
		{
			// Tree has changed, invalidate existing (remove from cache)
		
			this.invalidate(name);
                        
                        XTreeModel submodel = ((XTreeModel)model).cloneModelWithFilter(filter_);
			XTree tree = new XTree(submodel);
			tree.setRootVisible(false); // don't show root node
			
                        tree.addTreeSelectionListener(listener_);
			scrollPanes_.put(name, new JScrollPane(tree));
			
			treeModels_.put(name,model);
                        treePanels_.put(name,tree);
                        tree.setSelectionRow(0);
		}
                        
		return (JScrollPane)scrollPanes_.get(name);	
	}
        
	
	protected  void invalidate (String name)
	{
		scrollPanes_.remove(name);
		treeModels_.remove(name);
                treePanels_.remove(name);
	}
        
        public JTree getJTree (String name)
        {
                return (JTree)treePanels_.get(name);
	}
}
