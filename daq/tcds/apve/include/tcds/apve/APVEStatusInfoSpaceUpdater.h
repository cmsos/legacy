#ifndef _tcds_apve_APVEStatusInfoSpaceUpdater_h_
#define _tcds_apve_APVEStatusInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace apve {

    class TCADeviceAPVE;

    class APVEStatusInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      APVEStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                 TCADeviceAPVE const& hw);
      virtual ~APVEStatusInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      tcds::apve::TCADeviceAPVE const& getHw();

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVEStatusInfoSpaceUpdater_h_
