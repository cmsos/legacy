package xdaq.xdesk.xprop;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.infobus.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.Graphics;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import javax.naming.*;
import java.lang.*;
import java.net.URL;
import org.apache.log4j.*;
import xdaq.xdesk.tools.*;

public class XProp extends JApplet implements InfoBusDataConsumer, MenuBarListener
{
	Logger logger = Logger.getLogger (XProp.class);

	InfoBusMemberSupport infobusSupport_;
	DataItemChangeManagerSupport commandChangeManager_;
	
	Vector openViewers_ = new Vector(); // keep track of all viewers
	Vector windows_; // keep track of all windows
	
	XProp xprop_ = this; // allow access to XProp from inner classes
	
	XPropDefaultModel defaultModel_;
	
	HashMap clientConfigurationMapping_;   // source, name
	
	InfoBusPublisher frameworkCommandPublisher_ = new InfoBusPublisher("framework");
	
	
	// Window close listener
	class XPropWindowListener extends WindowAdapter
	{
		XPropViewer viewer_;
		
		public XPropWindowListener (XPropViewer viewer)
		{
			viewer_ = viewer;
		}
	
		// window adapter overriding method
		public void windowClosing (WindowEvent e) 
		{
			xprop_.closeWindow(viewer_);	
		}
	}
	
	// helper function to handle all publishing controllers
	protected String getConfigurationName(Object source )
	{
		return (String)clientConfigurationMapping_.get(source);
	}
	
	protected void setConfiguration( Object source, String name )
	{
		clientConfigurationMapping_.put(source, name);
	}
	
	protected void removeConfiguration( Object source )
	{
		clientConfigurationMapping_.remove(source);
	}
	
	// Keep track of all open viewers
	protected void addViewer (XPropViewer viewer)
	{
		openViewers_.add (viewer);
	}
	
	protected void removeViewer (XPropViewer viewer)
	{
		openViewers_.remove (viewer);
	}
	//
	
      
     
        
	public class XPropertiesListener extends DataItemChangeListenerSupport implements DataItemChangeListener
  	{
		XPropertiesListener ( DataItem item )
		{
			item_ = item;
		}
	
  		public void dataItemValueChanged (DataItemValueChangedEvent event)
		{
			Object o = event.getChangedItem();
			
			// REFRESH HERE!!!!!!!!!!!!!!!!!!!!!!!
			// SOMETHING HAS CHANGED !!!!!!!!!!!!!
			
			//debugXProperties((DefaultTableModel)o);
		}
		
		DataItem getItem()
		{
			return item_;
		}
		
		DataItem item_;
  	};
	
	public class XComReplyListener extends DataItemChangeListenerSupport implements DataItemChangeListener
  	{
  		public void dataItemValueChanged (DataItemValueChangedEvent event)
		{
			Object o = event.getChangedItem();
			
			// WORK HERE for new property published
			
		}
  	};
			
	         
         
	public void init()
	{
	
		frameworkCommandPublisher_.publishItem ("command");
                               
                // find Icons for this applet
               
                //System.out.println( "url is : " + url );     
               // url = this.getClass().getResource("/icons/Refresh24.gif");
                //System.out.println( "url is : " + url );    
                //url = this.getClass().getResource("Refresh24.gif");
                //System.out.println( "url is : " + url );  
                // url = this.getClass().getResource("xdaq.xdesk.xprop.icons.Refresh24.gif");
                //System.out.println( "url is : " + url );  
                //ImageIcon icon = new ImageIcon(Toolkit.getDefaultToolkit().createImage(url));      

                ////
		clientConfigurationMapping_ = new HashMap();
		windows_ = new Vector();
		defaultModel_ = new XPropDefaultModel();
		
		
		infobusSupport_ = new InfoBusMemberSupport(null);
		try {
			infobusSupport_.joinInfoBus("xcontrolbus");
			//infobusSupport_.getInfoBus().addDataProducer (this);
			infobusSupport_.getInfoBus().addDataConsumer (this);
			
			// Receive xproperties from XController
			Object[] dataItems = infobusSupport_.getInfoBus().findMultipleDataItems("xtree", null, this);
			if (dataItems != null)
			{
				System.out.println("Found " + dataItems.length + "open partitions");
				for (int i = 0; i < dataItems.length; i++)
				{
					if (dataItems[i] instanceof DataItemChangeManagerSupport)
					{
						System.out.println ("Add infobus properties item " + i);
      						XPropertiesListener l = new XPropertiesListener ( (DataItem)dataItems[i] );
      						((DataItemChangeManagerSupport)dataItems[i]).addDataItemChangeListener(l);
						//configuration_.put ( ((DataItem)dataItems[i]).getSource() ,l);
						
						ImmediateAccess item = (ImmediateAccess) l.getItem();
						String name = item.getValueAsString();
						this.setConfiguration(((DataItem)dataItems[i]).getSource(), name);
						defaultModel_.setValue( name, l);
					}
				}
			}
			
			
			//DataItem dataItem = (DataItem) infobusSupport_.getInfoBus().findDataItem("xreply", null, this);
       			//if (dataItem != null && dataItem instanceof DataItemChangeManagerSupport)
      			//((DataItemChangeManagerSupport)dataItem).addDataItemChangeListener(new XComReplyListener());

			// publish a xcommand items to XCom
			//infobusSupport_.getInfoBus().fireItemAvailable ("xcommand",null,this);
		
		} catch (Exception e) { 
			System.out.println("join failed: ");
			e.printStackTrace();
		}		
	}
	
	
	public void dataItemAvailable ( InfoBusItemAvailableEvent e )
	{
		String name = e.getDataItemName();

		if (name.equals ("xtree"))
		{
			DataItem item = (DataItem) e.requestDataItem (this, null);
			System.out.println ("Item is: " + item.toString());
			if ((item != null) && (item instanceof DataItemChangeManagerSupport))
			{
				XPropertiesListener l = new XPropertiesListener (item);
				((DataItemChangeManagerSupport) item).addDataItemChangeListener ( l );
				String itemName = ((ImmediateAccess)item).getValueAsString();
				//configuration_.put (item.getSource(), l);
				this.setConfiguration(item.getSource(), itemName);
				defaultModel_.setValue(itemName, l);
				// fire changes
				refreshPropertyWindows();
			}
			
			System.out.println ("xtree available");
		}
		//else if (name.equals ("xreply"))
		//{
		//	DataItemChangeManagerSupport cms = (DataItemChangeManagerSupport) e.requestDataItem (this, null);
		//	cms.addDataItemChangeListener( new XComReplyListener() );
		//} 
		else {
			System.out.println ("Received dataItemAvailable of unknown data item: " + name);
		}
	}
	
	public void dataItemRevoked ( InfoBusItemRevokedEvent e )
	{
		String name = e.getDataItemName();

		if (name.equals ("xtree"))
		{
			String itemName = this.getConfigurationName(e.getSource());
			
			XPropertiesListener l = (XPropertiesListener) defaultModel_.getValue ( itemName);
			if (l == null) {
				System.out.println ("Cannot revoke item, cause source not found.");
				return;
			}

			((DataItemChangeManagerSupport) l.getItem() ).removeDataItemChangeListener ( l );				

			defaultModel_.removeValue(itemName);
			this.removeConfiguration(e.getSource());
			// fire changes
			refreshPropertyWindows();
			
			//System.out.println ("Revoked " + name);
			// REFRESH HERE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			// SOMETHING HAS CHANGED !!!!!!!!!!!!!!!!!!!!!!!!
		}
		//else if (name.equals ("xreply"))
		//{
			
		//} 
		else {
			System.out.println ("Received dataItemRevoked of unknown data item: " + name);
		}
		
	
	}
	
   	public void propertyChange (PropertyChangeEvent e)
  	{
  		
  	}
	
	 public void dataItemRequested (InfoBusItemRequestedEvent e) {
  
  		//if (e.getDataItemName().equals ("xcommand"))
		//{
  		//	e.setDataItem ( commandChangeManager_ );
		//}
		//else {
		//	System.out.println ("Requested item unavailable: " + e.getDataItemName());
		//}
  
 	 }	
	


	// This start function is the same for all multiple windows applets
	//	
	public void start() 
	{
		if (windows_.size() > 0)
		{
			for (int i = 0; i < windows_.size(); i++)
			{
				JFrame f = (JFrame) windows_.elementAt (i);
				f.setVisible(true);
			}
		} else 
		{
				this.newWindow();	
		}
  	}
	
	public void newWindow()
	{
		JFrame frame = new JFrame();
		XPropViewer viewer = new XPropViewer ( frame, this );
		XPropMenuBar menuBar = new XPropMenuBar(viewer);
		
		this.addViewer (viewer);

		frame.setJMenuBar(menuBar);
		frame.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener (new XPropWindowListener(viewer));
		//frame.setJMenuBar(w.getJMenuBar());
		frame.getContentPane().add(viewer);
		frame.setSize (this.getWidth(), this.getHeight());
                frame.setTitle("XProp");
		
		defaultModel_.fireXPropModelDataChanged ();
		
		frame.setVisible(true);	
		windows_.add(frame);
                                      
	}
	
	public void closeWindow (XPropViewer viewer)
	{
		viewer.shutdown();
		xprop_.removeViewer(viewer);
		JFrame frame = viewer.getFrame();
			
		if (windows_.contains(frame))
		{
			windows_.remove(frame);
			frame.setVisible(false);
			frame.dispose();
		}	
	}
	
	public XPropDefaultModel getDefaultModel()
	{
		return defaultModel_;
	}
	
	public void refreshPropertyWindows()
	{
		defaultModel_.fireXPropModelDataChanged ();
	}

  /**
     Removes self as InfoBus data consumer. Removes self as data item changed 
     listener so it will no longer receive item value changed updates.
   */
  	public void stop() 
	{
   
  	}

  /**
     Removes the label, stops the applet, and leaves the InfoBus.
   */
  	public void destroy() 
	{
  		frameworkCommandPublisher_.shutdown();
		
		// remove all windows (standard)
		for (int i = 0; i < windows_.size(); i++)
		{
			JFrame f = (JFrame) windows_.elementAt (i);
			f.setVisible(false);
			f.dispose();
		}

		//shutdown all viewers
		for (int j = 0; j < openViewers_.size(); j++)
		{
			((XPropViewer)openViewers_.elementAt(j)).shutdown();
		}
		
		try {
		infobusSupport_.getInfoBus().removeDataConsumer (this);
		infobusSupport_.leaveInfoBus();
		}
		catch (Exception e )
		{
			logger.error("Infobus error "+ e.toString());
		}
  	}
	
	
	// Implements MenuBar Listeners	
	
	public void aboutAction()
	{
		System.out.println("About for this Daqlet");
	}
	
	public void switchAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("tasks",this) );
	}
	
	public void exitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("exit",this) );
	}
	public void quitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("quit",this) );
	}
	
}
