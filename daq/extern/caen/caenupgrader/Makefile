# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2018, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini, A. Petrucci and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

BUILD_HOME:=$(shell pwd)/../../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

ifndef MFDEFS_SUPPORT
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.hardware_worksuite
else
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)
endif

Project=$(PROJECT_NAME)
Package=extern/caen/caenupgrader
PackageName=caenupgrader

PACKAGE_VER_MAJOR=1
PACKAGE_VER_MINOR=4
PACKAGE_VER_PATCH=1

Summary=The CAENUpgrader is the new firmware upgrade tool for the CAEN boards or bridges.

Description=The CAENUpgrader is the new firmware upgrade tool for the CAEN boards or bridges.\
CAENUpgrader supersedes the old cvUpgrade, CAENBridgeUpgrade and PLLConfig tools,\
by offering a unified graphical user interface with the same functionality of the\
separate command line programs.\
The cvUpgrade and the CAENBridgeUpgrade are still provided as command line tools,\
while the old PLLConfig software is no longer available.

Link=http://www.caen.it

UNPACKDIR=CAENUpgrader-1.4.1
TARFILE=CAENUpgrader-1.4.1.tgz

IncludeDirs = \
	$(CAENVME_INCLUDE_PREFIX) \
	$(CAENCOMM_INCLUDE_PREFIX)

LibraryDirs = \
	$(CAENVME_LIB_PREFIX) \
	$(CAENCOMM_LIB_PREFIX) 

export LDFLAGS=$(LibraryDirs:%=-L%)
export CPPFLAGS=$(IncludeDirs:%=-I%)
export CFLAGS=$(IncludeDirs:%=-I%)

build: _buildall

_buildall: all


_all: all

default: all


$(UNPACKDIR)/configure:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(UNPACKDIR) 
	tar -zxvf $(TARFILE)
	cd $(UNPACKDIR); patch -p1 < ../cmsos2876.patch

all: $(UNPACKDIR)/configure
	cd ./$(UNPACKDIR); \
	./configure --prefix=$(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM) --exec-prefix=$(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM) --sysconfdir=$(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM);\
	make; make install
_installall: install
	
install: $(UNPACKDIR)/configure
	cd ./$(UNPACKDIR); \
	./configure --prefix=$(INSTALL_PREFIX)/$(XDAQ_PLATFORM) --exec-prefix=$(INSTALL_PREFIX)/$(XDAQ_PLATFORM); \
	make; make install
_cleanall: clean
	
clean:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(UNPACKDIR);

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules

