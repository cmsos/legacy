/**
*      @file FMMFullSelfTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMFullSelfTest.hh"

#include "tts/fmm/FMMRegisterTest.hh"
#include "tts/fmm/FMMFunctionalTest.hh"
#include "tts/fmm/FMMZBTTest.hh"
#include "tts/fmm/FMMTransitionTest.hh"
#include "tts/fmm/FMMPoissonTransitionGenerator.hh"
#include "tts/fmm/FMMRandomTransitionTest.hh"
#include "tts/fmm/FMMTTResetTest.hh"
#include "tts/fmm/FMMOpenInputsTest.hh"

#include <time.h>

#include <iostream>
#include <iomanip>
#include <fstream>


tts::FMMFullSelfTest::FMMFullSelfTest(tts::FMMCard & fmm, 
				 tts::FMMTDCard* td,
				 std::ostream& os, 
				 tts::FMMBasicTest::VerbosityLevel vlevel)
  : tts::FMMBasicTest(os, vlevel), _fmm(fmm) {

  _tests.push_back( new tts::FMMRegisterTest(_fmm.device(), os, vlevel) );
  _tests.push_back( new tts::FMMZBTTest(_fmm.device(), os, vlevel) );
  _tests.push_back( new tts::FMMFunctionalTest(_fmm, os, vlevel) );
  _tests.push_back( new tts::FMMRandomTransitionTest(_fmm, os, vlevel) );
  _tg = new tts::FMMPoissonTransitionGenerator();
  _tests.push_back( new tts::FMMTransitionTest(_fmm, *_tg, os, vlevel) );
  _tests.push_back( new tts::FMMOpenInputsTest(_fmm, os, vlevel) );
  if (td) {
    _tests.push_back( new tts::FMMTTResetTest(_fmm, *td, os, vlevel) );
  }
};


tts::FMMFullSelfTest::~FMMFullSelfTest() {

  std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
  for (; it != _tests.end(); it++)  
    delete (*it);

  delete _tg;
};



bool tts::FMMFullSelfTest::_run(uint32_t nloops) {

  for (uint32_t i=0; i<nloops; i++) {

    std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
    for (; it != _tests.end(); it++)  
      (*it)->run();

  }
  _nloops += nloops;

  return true;
}

void tts::FMMFullSelfTest::printStatus() {

  std::cout << "==== Test status for FMM " << _fmm.getSerialNumber() << " :" << std::endl;
  std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
  for (; it != _tests.end(); it++) {
    std::cout << std::setw(25) << (*it)->getName()
	      << "    nlp=" << std::setw(8) << std::dec << (*it)->getNumLoops() 
	      << "    nerr=" << std::setw(8) << std::dec << (*it)->getNumErrors()
	      << "    duration=" << std::setw(10) << ( (double) (*it)->getTotalTime() / 1.e6 ) << " seconds" << std::endl;

  }
};

void tts::FMMFullSelfTest::getResults(std::vector <tts::FMMBasicTestResult>& results) {
  results.clear();
  std::vector<tts::FMMBasicTest*>::iterator it = _tests.begin();
  for (; it != _tests.end(); it++) {
    tts::FMMBasicTestResult rs;
    (*it)->getTestResult(rs);
    results.push_back(rs);
  }
}
