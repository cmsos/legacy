#include "tcds/ici/ICIController.h"

#include <stdint.h>
#include <utility>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceHandler.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceUpdater.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/ici/ConfigurationInfoSpaceHandler.h"
#include "tcds/ici/Definitions.h"
#include "tcds/ici/HwStatusInfoSpaceHandler.h"
#include "tcds/ici/HwStatusInfoSpaceUpdater.h"
#include "tcds/ici/ICIBchannelInfoSpaceHandler.h"
#include "tcds/ici/ICIBchannelInfoSpaceUpdater.h"
#include "tcds/ici/ICIBdataInfoSpaceHandler.h"
#include "tcds/ici/ICIBdataInfoSpaceUpdater.h"
#include "tcds/ici/ICICountersInfoSpaceHandler.h"
#include "tcds/ici/ICICountersInfoSpaceUpdater.h"
#include "tcds/ici/ICIInfoSpaceHandler.h"
#include "tcds/ici/ICIInfoSpaceUpdater.h"
#include "tcds/ici/TCADeviceICI.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"


XDAQ_INSTANTIATOR_IMPL(tcds::ici::ICIController);

tcds::ici::ICIController::ICIController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMBasic(stub,
                                   std::auto_ptr<tcds::hwlayer::DeviceBase>(new tcds::ici::TCADeviceICI())),
    cyclicGensInfoSpaceUpdaterP_(0),
    cyclicGensInfoSpaceP_(0),
    hwIDInfoSpaceUpdaterP_(0),
    hwIDInfoSpaceP_(0),
    hwStatusInfoSpaceUpdaterP_(0),
    hwStatusInfoSpaceP_(0),
    iciInfoSpaceUpdaterP_(0),
    iciInfoSpaceP_(0),
    iciBchannelInfoSpaceUpdaterP_(0),
    iciBchannelInfoSpaceP_(0),
    iciBdataInfoSpaceUpdaterP_(0),
    iciBdataInfoSpaceP_(0),
    iciCountersInfoSpaceUpdaterP_(0),
    iciCountersInfoSpaceP_(0),
    soapCmdDisableBChannel_(*this),
    soapCmdDisableCyclicGenerator_(*this),
    soapCmdDumpHardwareState_(*this),
    soapCmdEnableBChannel_(*this),
    soapCmdEnableCyclicGenerator_(*this),
    soapCmdInitCyclicGenerator_(*this),
    soapCmdInitCyclicGenerators_(*this),
    soapCmdReadHardwareConfiguration_(*this),
    soapCmdSendBCommand_(*this),
    soapCmdSendBgo_(*this),
    soapCmdSendL1A_(*this)
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::auto_ptr<tcds::ici::ConfigurationInfoSpaceHandler>(new tcds::ici::ConfigurationInfoSpaceHandler(*this));

    // Make sure the correct default hardware configuration file is found.
    cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                              "${XDAQ_ROOT}/etc/tcds/ici/hw_cfg_default_ici.txt");
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the ICIController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::ici::ICIController::~ICIController()
{
  hwRelease();
}

void
tcds::ici::ICIController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-related InfoSpaceHandlers and InfoSpaceUpdaters.
  cyclicGensInfoSpaceUpdaterP_ = std::auto_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater>(new tcds::hwutilstca::CyclicGensInfoSpaceUpdater(*this, getHw()));
  cyclicGensInfoSpaceP_ =
    std::auto_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler>(new tcds::hwutilstca::CyclicGensInfoSpaceHandler(*this, cyclicGensInfoSpaceUpdaterP_.get(), tcds::definitions::kNumCyclicGensPerICI));
  hwIDInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::ici::HwStatusInfoSpaceUpdater>(new tcds::ici::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::auto_ptr<tcds::ici::HwStatusInfoSpaceHandler>(new tcds::ici::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
  iciInfoSpaceUpdaterP_ =
    std::auto_ptr<ICIInfoSpaceUpdater>(new ICIInfoSpaceUpdater(*this, getHw()));
  iciInfoSpaceP_ =
    std::auto_ptr<ICIInfoSpaceHandler>(new ICIInfoSpaceHandler(*this, iciInfoSpaceUpdaterP_.get()));
  iciBchannelInfoSpaceUpdaterP_ =
    std::auto_ptr<ICIBchannelInfoSpaceUpdater>(new ICIBchannelInfoSpaceUpdater(*this, getHw()));
  iciBchannelInfoSpaceP_ =
    std::auto_ptr<ICIBchannelInfoSpaceHandler>(new ICIBchannelInfoSpaceHandler(*this, iciBchannelInfoSpaceUpdaterP_.get()));
  iciBdataInfoSpaceUpdaterP_ =
    std::auto_ptr<ICIBdataInfoSpaceUpdater>(new ICIBdataInfoSpaceUpdater(*this, getHw()));
  iciBdataInfoSpaceP_ =
    std::auto_ptr<ICIBdataInfoSpaceHandler>(new ICIBdataInfoSpaceHandler(*this, iciBdataInfoSpaceUpdaterP_.get()));
  iciCountersInfoSpaceUpdaterP_ =
    std::auto_ptr<ICICountersInfoSpaceUpdater>(new ICICountersInfoSpaceUpdater(*this, getHw()));
  iciCountersInfoSpaceP_ =
    std::auto_ptr<ICICountersInfoSpaceHandler>(new ICICountersInfoSpaceHandler(*this, iciCountersInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor and the WebServer.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciBchannelInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciBdataInfoSpaceP_->registerItemSets(monitor_, webServer_);
  cyclicGensInfoSpaceP_->registerItemSets(monitor_, webServer_);
  iciCountersInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

tcds::ici::TCADeviceICI&
tcds::ici::ICIController::getHw() const
{
  return static_cast<tcds::ici::TCADeviceICI&>(*hwP_.get());
}

void
tcds::ici::ICIController::hwConnectImpl()
{
  tcds::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::ici::ICIController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::ici::ICIController::configureActionImpl(toolbox::Event::Reference event)
{
  // Extract the iCI number from the configuration and propagate that
  // into our hardware device instance.
  unsigned short const iciNumber = cfgInfoSpaceP_->getUInt32("iciNumber");
  getHw().setICINumber(iciNumber);

  // Then do what we normally do.
  tcds::utils::XDAQAppWithFSMBasic::configureActionImpl(event);

  // Disable the triggers completely. This guarantees a quiet time to
  // configure the subsystem electronics. We will re-enable triggers
  // in the 'Enable' transition.
  getHw().disableTrigger();
}

void
tcds::ici::ICIController::enableActionImpl(toolbox::Event::Reference event)
{
  // Enable triggers.
  getHw().enableTrigger();
}

void
tcds::ici::ICIController::stopActionImpl(toolbox::Event::Reference event)
{
  tcds::utils::XDAQAppWithFSMBasic::stopActionImpl(event);

  // Disable the triggers completely. This makes sure that a subsystem
  // leaving the run after stopping will not inadvertently still
  // receive L1As. Triggers will be re-enabled in the 'Enable'
  // transition.
  getHw().disableTrigger();
}

void
tcds::ici::ICIController::zeroActionImpl(toolbox::Event::Reference event)
{
  tcds::ici::TCADeviceICI& hw = getHw();

  hw.resetStatus();
  hw.resetCounters();
  hw.initCyclicGeneratorsAndBchannels();
}

void
tcds::ici::ICIController::hwCfgInitializeImpl()
{
  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).
  if (!(getHw().isTTCClockUp() && getHw().isTTCClockStable()))
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  //----------

  // Configure and enable the 'TTC-stream phase monitoring.'
  // NOTE: This really measures phases between clocks, not w.r.t. the
  // real TTC stream, so this can be enabled before selecting the TTC
  // stream source.
  getHw().enablePhaseMonitoring();

  //----------

  // Configure and enable the 'TTC-stream phase monitoring.'
  // NOTE: This really measures phases between clocks, not the real
  // TTC stream.
  getHw().enablePhaseMonitoring();
}

void
tcds::ici::ICIController::hwCfgFinalizeImpl()
{
  // Switch on the fast-track for triggers from the LPM.
  getHw().enableCPML1AFastTrack();

  // Configure the BRILDAQ information from the CPM on the backplane.
  getHw().enableCPMBRILDAQData();

  // Configure the cyclic trigger BX-alignment register with the right
  // value.
  getHw().configureTriggerAlignment();

  // Make sure the trigger rules are enabled.
  getHw().writeRegister("main.inselect.trigger_rule_checker_disable", 0x0);

  // Make sure that triggers are not accidentally inhibited.
  getHw().writeRegister("main.inselect.define_l1a_in1_as_trigger_inhibit", 0x0);

  // There is a certain bit in the B-data configuration that _has_ to
  // be set in order to produce valid long B-command frames.
  getHw().writeRegister("bchannels.main.bdata_config.reserved_keep_set_to_one", 0x1);

  // Make sure that the align mode for the PI-to-LPM TTS links is set
  // to automatic.
  // NOTE: This is an LPM-wide setting that applies to all incoming
  // PI-to-LPM TTS links. It can only be set at Configure time of the
  // ICIController, though, since there is no guarantee the
  // LPMController is ever configured.
  getHw().enableTTSLinkAutoAlignMode();
}

bool
tcds::ici::ICIController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  bool res = tcds::utils::XDAQAppBase::isRegisterAllowed(regInfo);

  // The overall trigger_enable register is handled by the XDAQ
  // control application.
  res = res && !toolbox::endsWith(regInfo.name(), ".trigger_enable");

  // One is not supposed to disable the trigger rules.
  res = res && !toolbox::endsWith(regInfo.name(), ".trigger_rule_checker_disable");

  // One is not supposed to disable the triggers.
  res = res && !toolbox::endsWith(regInfo.name(), ".define_l1a_in1_as_trigger_inhibit");

  return res;
}

unsigned short
tcds::ici::ICIController::iciNumber() const
{
  return getHw().iciNumber();
}
