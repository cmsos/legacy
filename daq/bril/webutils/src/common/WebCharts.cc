/**
 * @brief Webpage utility functions and highcharts based monitoring.
 *
 * @author N. Tosi \<nicolo.tosi@cern.ch\>
 *
 * This package includes various utility functions to create tables and plots in xdaq webpages.
 */

#include "bril/webutils/WebCharts.h"
#include "cgicc/Cgicc.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Output.h"

namespace bril
 {

  namespace webutils
   {

    using namespace cgicc;

/**
 * @param chart is the chart identifier. It has to be a valid variable name, unique in the page. It is not the displayed name/title.
 * @param cb is the callback URL.
 * Th object constructed here is not really complete. Use the convenience functions set* to set important parameters.
 */
    WebChart::WebChart( std::string chart, std::string cb ) :
     m_id( chart ), m_callback( cb ), m_chartoptions(), m_extraoptions(), m_series(), m_has_fresh_data( true )
     {}

/**
 * @param chart is the chart identifier. It has to be a valid variable name, unique in the page. It is not the displayed name/title.
 * @param cb is the callback URL.
 * @param copts are the chart global options, such as type, zoomType
 * @param eopts are the other things, basically everything except series
 * @param smap is an std::map<"series name", series_properties>
 */
    WebChart::WebChart( std::string chart, std::string cb, std::string copts, std::string eopts, const property_map& smap ) :
     m_id( chart ), m_callback( cb ), m_chartoptions( copts ), m_extraoptions( eopts ), m_series( smap ), m_has_fresh_data( true )
     {
      if ( !m_chartoptions.empty() )
        m_chartoptions = ", " + m_chartoptions;
      if ( !m_extraoptions.empty() )
        m_extraoptions = ", " + m_extraoptions;
     }

    WebChart& WebChart::setTitle( const std::string& title )
     {
      m_chartoptions += ", title: { text: '" + title + "' }";
      return *this;
     }

    WebChart& WebChart::setType( const std::string& type )
     {
      m_chartoptions += ", type: '" + type + '\'';
      return *this;
     }

    WebChart& WebChart::setAnimation( bool anim )
     {
      if ( anim )
        m_chartoptions += ", animation: false";
      return *this;
     }

    WebChart& WebChart::setLogScale( bool logy )
     {
      if ( logy )
        m_extraoptions += ", yAxis: { type: 'logarithmic' }";
      return *this;
     }

    WebChart& WebChart::setDatetime( bool dt )
     {
      if ( dt )
        m_extraoptions += ", xAxis: { type: 'datetime' }";
      return *this;
     }

    WebChart& WebChart::setPlotOptions( const std::string& po )
     {
      m_extraoptions += ", plotOptions: { " + po + " }";
      return *this;
     }

    std::string WebChart::makeQueryString( bool forcenewdata ) const
     {
      std::string r = '\'' + m_callback + "?chart=" + m_id;
      if ( forcenewdata )
        r += "&fnd=true";
      if ( !m_types.empty() )
        r += "&type=' + $( '#" + m_id + "_typesel option:selected' ).val()";
      else
        r += '\'';
      return r;
     }

///Call this funtion when there is new data
    void WebChart::newDataReady()
     {
      m_has_fresh_data = true;
     }

///Just put this at the top of "Default", or any other page with a plot on it.
    void WebChart::pageHeader( xgi::Output* out, bool useboost )
     {
      *out << script().set( "src", "/bril/highcharts/js/highcharts.js" ) << script() << std::endl;
      if ( useboost )
        *out << script().set( "src", "/bril/highcharts/js/modules/boost.js" ) << script() << std::endl;
      *out << script().set( "src", "/bril/highcharts/js/modules/exporting.js" ) << script() << std::endl;
      *out << script().set( "src", "/bril/highcharts/js/modules/heatmap.js" ) << script() << std::endl;
     }

///Just put this at the top of the data callback
    void WebChart::dataHeader( xgi::Output* out )
     {
      out->getHTTPResponseHeader().addHeader( "Content-Type", "text/json" );
     }

///use divstyle to set the style attribute of the <div> tag, such as height and width
    void WebChart::writeChart( xgi::Output* out, const std::string& divstyle ) const
     {
      //HTML
      *out << "Autorefresh: ";
      *out << cgicc::select().set( "id", m_id + "_ref" ) << std::endl;
      *out << option().set( "value", "-1" ) << "OFF" << option() << std::endl;
      *out << option().set( "value", "500" ) << "0.5s" << option() << std::endl;
      *out << option().set( "value", "1000" ) << "1s" << option() << std::endl;
      *out << option().set( "value", "3000" ).set( "selected", "selected" ) << "3s" << option() << std::endl;
      *out << option().set( "value", "5000" ) << "5s" << option() << std::endl;
      *out << option().set( "value", "10000" ) << "10s" << option() << std::endl;
      *out << option().set( "value", "30000" ) << "30s" << option() << std::endl;
      *out << cgicc::select() << std::endl;
      if ( !m_types.empty() )
       {
        *out << "Chart type: ";
        *out << cgicc::select().set( "id", m_id + "_typesel" ) << std::endl;
        auto it = m_types.begin();
        *out << option().set( "value", it->second ).set( "selected", "selected" ) << it->first << option() << std::endl;
        for ( ++it; it != m_types.end(); ++it )
          *out << option().set( "value", it->second ) << it->first << option() << std::endl;
        *out << cgicc::select() << std::endl;
       }
      *out << cgicc::div().set( "id", m_id ).set( "style", divstyle ) << cgicc::div() << std::endl;
      //js
      *out << script() <<
        "var " << m_id << " = null;\n"
        "var " << m_id << "_timerid = 0;\n"
        "var " << m_id << "_type = null;\n"
        "\n"
        "function onAsyncFail_" << m_id << "( jqXHR, textStatus, errorThrown ) {\n"
        "    if ( " << m_id << " != null )\n"
        "      " << m_id << ".showLoading( 'Loading data failed: Application may have crashed!' );\n"
        "    console.log( 'getJSON request failed: ' + textStatus );\n"
        "}\n"
        "\n"
        "function refresh_" << m_id << "() {\n"
        "  console.log( 'Executing refresh for " << m_id << ".' );\n"
        "  $.getJSON( " << makeQueryString() << ", function ( data ) {\n"
        "    if ( !data.updated )\n"
        "      return;\n"
        "    console.log( 'Redrawing chart' );\n"
        "    for ( s = 0; s < " << m_series.size() << "; s = s + 1 ) {\n"
        "      " << m_id << ".series[s].setData( data.series[s].data, false );\n"
        "     }\n"
        "    " << m_id << ".hideLoading();\n"
        "    " << m_id << ".redraw();\n"
        "   } ).fail( onAsyncFail_" << m_id << " );\n"
        " }\n"
        "\n"
        "function settimer_" << m_id << "() {\n"
        "  var t = Number( $( '#" << m_id << "_ref option:selected' ).val() );\n"
        "  console.log( 'Setting refresh rate for " << m_id << " to ' + t );\n"
        "  window.clearInterval( " << m_id << "_timerid );\n"
        "  if ( t > 0 )\n"
        "    " << m_id << "_timerid = window.setInterval( refresh_" << m_id << ", t );\n"
        " }\n"
        "\n"
        "$( document ).on( 'xdaq-post-load', function() {\n"
        "  console.log( 'Onload' );\n"
        "  $.getJSON( " << makeQueryString( true ) << ", function ( data ) {\n"
        "    console.log( 'Creating chart' );\n"
        "    " << m_id << " = new Highcharts.Chart( { chart : { renderTo: '" << m_id << "' " << m_chartoptions << " },\n"
        "                                             series : data.series" << m_extraoptions << " } );\n"
        "    settimer_" << m_id << "();\n"
        "   } ).fail( onAsyncFail_" << m_id << " );\n"
        " } );\n"
        "\n"
        "$( '#" << m_id << "_ref' ).change( settimer_" << m_id << " );\n"
        "$( '#" << m_id << "_typesel' ).change( refresh_" << m_id << " );\n"
       << script() << std::endl;
     }

   }

 }
