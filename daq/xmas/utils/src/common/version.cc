// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/utils/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xoap/version.h"

GETPACKAGEINFO(xmasutils)

void xmasutils::checkPackageDependencies() 
{
	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept); 
	CHECKDEPENDENCY(toolbox); 
	CHECKDEPENDENCY(xoap);  
}

std::set<std::string, std::less<std::string> > xmasutils::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept); 
	ADDDEPENDENCY(dependencies,toolbox); 
	ADDDEPENDENCY(dependencies,xoap); 

	return dependencies;
}	
	
