package xdaq.xdesk.xcom;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.infobus.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.Graphics;
import java.io.*;

import org.w3c.dom.*;
import javax.xml.soap.*;
//import javax.xml.messaging.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import javax.naming.*;
import java.net.*;

import org.apache.log4j.*;

import xdaq.xdesk.tools.*;


public class XCom extends JApplet implements ActionListener, XComMenuBarListener
{
	Logger logger = Logger.getLogger (XCom.class);
	
	XComProcessor consumer_;
	JFrame frame_ = null;
	// Infobus stuff
	InfoBusPublisher frameworkCommandPublisher_ = new InfoBusPublisher("framework");
	
	class WindowListener extends WindowAdapter
	{	
		public WindowListener ()
		{
	
		}
	
		// window adapter overriding method
		public void windowClosing (WindowEvent e) 
		{
			frame_.setVisible(false);			
		}
	}
	
	public void init() 
	{
		consumer_ = new XComProcessor(this);
		
		frameworkCommandPublisher_.publishItem ("command");
		
		
		
	}
	
	public void actionPerformed (ActionEvent e )
	{
	
		frame_.setVisible(false);
		frame_.validate();
		// close window here
		//JFrame father = (JFrame)((JPanel)this.getParent()).getRootPane().getParent();
		//System.out.println("------- " +father.getClass().toString());
		//father.setVisible(false);
		//WindowEvent event = new WindowEvent(father,WindowEvent.WINDOW_CLOSING);
		//father.dispatchEvent(event);
		//father.dispose();
	
	}
	
	public void start() 
	{
	
		if ( frame_ == null ) 
		{
		
			frame_ = new JFrame();
			frame_.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
			frame_.addWindowListener (new WindowListener());
		
   			JPanel display = new JPanel();
			display.setLayout(new BorderLayout());
		
		
			JPanel labelPanel = new JPanel();
			labelPanel.add(new JLabel("XCom Daqlet V1.0"), BorderLayout.CENTER);
		
			JPanel logoPanel = new JPanel();
			ImageIcon logo = new ImageIcon(this.getClass().getResource("/xdaq/xdesk/xcom/icons/logo.gif"));
			JLabel logoLabel = new JLabel(logo);
			logoPanel.add(logoLabel, BorderLayout.CENTER);


			JPanel buttonPanel = new JPanel();
			JButton okButton= new JButton("Ok");
			okButton.addActionListener(this);
			buttonPanel.add(okButton, BorderLayout.CENTER);

			display.add(labelPanel, BorderLayout.NORTH);
			display.add(logoPanel, BorderLayout.CENTER);
			display.add(buttonPanel, BorderLayout.SOUTH);


			this.getContentPane().add(display);
			frame_.getContentPane().add(this);
			frame_.setSize(this.getWidth(),this.getHeight()); // should use properties from applet spec
			frame_.setJMenuBar(new XComMenuBar(this));
			frame_.show();
		}
		else
		{
		
			frame_.setVisible(true);
		}	
  	}

  /**
     Removes self as InfoBus data consumer. Removes self as data item changed 
     listener so it will no longer receive item value changed updates.
   */
  	public void stop() 
	{
   
  	}

  /**
     Removes the label, stops the applet, and leaves the InfoBus.
   */
  	public void destroy() 
	{
  		frame_.setVisible(false);
		frame_.dispose();
		
		/// Shutdown of XComProcessor : stop the FIFO processor and leave INFOBUS
		consumer_.shutdown();
		// Leave the Info Bus	
		frameworkCommandPublisher_.shutdown();
  	}
	
	
	
	// Returns updated table model. content contains the reply and column state
	//
	public MessageSet xcommand (MessageSet messages)
	{
			
		for (int i = 0; i < messages.size(); i++)
		{
			Message msg = messages.getMessage(i);
			URL url = msg.getUrl();
	
			try 
			{				
				SOAPMessage reply = null;
				XComSOAPConnection con = new XComSOAPConnection();
				
				if (logger.isDebugEnabled())
				{
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					msg.getSOAP().writeTo (os);				
					logger.debug ("Sending SOAP message");
					logger.debug (os.toString());
					os.reset();
					
					reply = con.call ( msg.getSOAP(), url );
					
					if (reply != null)
					{					
						logger.debug ("Received SOAP reply");
						reply.writeTo (os);
						logger.debug (os.toString());
					} 
				} else 
				{				
					reply = con.call ( msg.getSOAP(), url );
				}				
				
				
				if (reply != null)
				{
					// Check for a server error
					//SOAPBody body = reply.getSOAPPart().getEnvelope().getBody();
					
					/*				
					if (body.hasFault())
					{
						SOAPFault fault = body.getFault();
						if (fault != null)
						{
							System.out.println ("Fault: " + fault.getFaultString());
							msg.setStatus (fault.getFaultString());
							
							
						} else {
							System.out.println ("There was no fault.");
						}
					}
					*/
					msg.setSOAP(reply);
					
					String faultString = msg.getFaultString();
					String faultCode = msg.getFaultCode();
					
					if (faultCode != null)
					{
						logger.error ("SOAP reply fault: " + faultString + ", code: " + faultCode + ", url: " + url.toString());
						msg.setStatus (faultCode + ": " + faultString);
					}
					
					
				} else 
				{
					logger.error ("Did not receive SOAP reply from url: " + url.toString());
					//statusPane_.setStatus ("Did not receive reply from " + url, "ERROR");
					msg.setStatus ("no reply");
				}
			} catch (XComSOAPException e) 
			{
				//statusPane_.setStatus ("Failed: sending " + command, "ERROR");
				logger.error ("SOAP reply exception from url " + url.toString() + ", :" + e.toString());
				msg.setStatus ("Connection error: " + url);
			} catch (Exception se) 
			{
				logger.error ("SOAP reply exception from url " + url.toString() + ", :" + se.toString());
				msg.setStatus ("SOAP error: " + se.toString());
			}
		}
	
		//if (!errorString.equals(""))
		//{
			//JOptionPane.showMessageDialog ( null, errorString, "Configure error", JOptionPane.ERROR_MESSAGE );
		//}
		
		return messages;
	}
	
	// Menu bar Listener methods
	
	public void aboutAction()
	{
		System.out.println("About for this Daqlet");
	}
	
	public void switchAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("tasks",this) );
	}
	
	public void exitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("exit",this) );
	}
	public void quitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("quit",this) );
	}
	
	public void closeAction()
	{
		// close the window frame
		frame_.setVisible(false);
		frame_.validate();
	
	}
	
}
