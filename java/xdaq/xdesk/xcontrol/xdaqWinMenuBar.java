package xdaq.tools.win;

import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import xdaq.tools.win.PropertiesWindow;
import xdaq.tools.win.FileSelectionListener;
import xdaq.tools.win.xdaqWinException;
import xdaq.tools.win.OpenFileAction;
import xdaq.tools.win.ExitFileAction;
import java.io.*;
//import xdaq.tools.win.*;
import org.w3c.dom.*;
import java.awt.*;
import javax.help.*;

public class xdaqWinMenuBar extends JMenuBar implements FileSelectionListener
{
	xdaqWin frame_;
	JMenu commandMenu_;
	JMenu partitionMenu_;
	JMenu applicationMenu_;
	
	
	public void fileSelected (File f)
	{
		try {
			frame_.displayConfiguration ( "file:" + f.toString() );
			preparePartitionMenu();
		} catch (xdaqWinException e)
		{
			// do nothing
		}
		
		
	}
	
	protected void preparePartitionMenu()
	{
		partitionItems_ = new HashMap();
		Document doc = (frame_.getTree()).getDocument();
		
		NodeList l = doc.getElementsByTagName ("Script");
		String name;
		
		for (int i = 0; i < l.getLength(); i++)
		{	
			String language;
			try {
				language = (l.item(i).getAttributes()).getNamedItem("language").getNodeValue();
			} catch (java.lang.NullPointerException e) {
				JOptionPane.showMessageDialog (null, "Script error", "Missing language attribute in script tag.", JOptionPane.ERROR_MESSAGE);
				return;
			}
			if ( !language.equals("tcl") ) {
			
				JOptionPane.showMessageDialog (null, "Script error", "Language "+ language+" not supported.", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			try {
				 name = (l.item(i).getAttributes()).getNamedItem("name").getNodeValue();
			} catch (java.lang.NullPointerException e) {
				JOptionPane.showMessageDialog (null, "Script error", "Missing name attribute in script tag.", JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			String script = "";
			try 
			{
				String src = (l.item(i).getAttributes()).getNamedItem("src").getNodeValue();
				URL u = new URL (src);
				script = (String) u.getContent();
			} catch (java.lang.NullPointerException e) 
			{
				// Did not find URL, so maybe the script is embedded.
				try {
					// First look for a CDATA node. There is only one
					// CDATA section allowed per script node.
					org.w3c.dom.Node node = l.item(i).getFirstChild();
					while (node != null)
					{
						//if (node.getNodeType() == Node.CDATA_SECTION_NODE)
						if (node instanceof org.w3c.dom.CDATASection)
						{
							org.w3c.dom.CDATASection cdata = (CDATASection) node;
							script = cdata.getData();
							//System.out.println ("*** CDATA script: ["+script+"]");
							break;					
						} else
						{
							// Otherwise, the script is in the value section of an
							// ordinary text node.
							org.w3c.dom.Text text = (Text) node;
							script = text.getNodeValue();
							//System.out.println ("*** Text script: ["+script+"]");
						}	
						node = node.getNextSibling();						
					}
				} catch (java.lang.NullPointerException ex1) 
				{
					System.out.println ("Empty script node.");
					// ignore this exception , it is an null command
				}	
			} catch (java.io.IOException ex) 
			{
				JOptionPane.showMessageDialog (null, "Script loading error", ex.toString(), JOptionPane.ERROR_MESSAGE);
				return;
			}
			
			JMenuItem item = new JMenuItem ( new TclInterpreterAction ( name, script, frame_ ) );
			partitionItems_.put ( name , item );
		}
		//this.setPartitionMenu() ;
	}
	
	
	public xdaqWinMenuBar(xdaqWin frame)
	{
		items_ = new HashMap();
		frame_ = frame;
	
		JMenu menu;
		JMenuItem item;
		
		// FILE menu
		//
		menu = new JMenu ("File");
		menu.setMnemonic ('f');
		
		item = new JMenuItem (new OpenFileAction( frame_, this, "xml", "XML file" ) );
		menu.add (item);
		items_.put ( "Open" , item );
		
		item = new JMenuItem (new CloseFileAction() );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("Close", item);
		
		item = new JMenuItem (new SaveFileAction() );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("Save", item);
		
		item = new JMenuItem (new SaveAsFileAction() );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("SaveAs", item);
		
		menu.addSeparator();
		
		item = new JMenuItem (new ExitFileAction(frame_) );
		menu.add (item);
		items_.put ("Exit", item);
		
		this.add (menu);
		
		// Edit menu
		//
		menu = new JMenu ("Edit");
		menu.setMnemonic ('e');
		
		item = new JMenuItem (new EditCutAction() );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("Cut", item);
		
		item = new JMenuItem (new EditCopyAction() );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("Copy", item);
		
		item = new JMenuItem (new EditPasteAction() );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("Paste", item);
		
		item = new JMenuItem (new EditSelectAction( frame ) );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("Select", item);
		
		menu.addSeparator();
		
		item = new JMenuItem (new EditPropertiesAction( frame ) );
		item.setEnabled (false);
		menu.add (item);
		items_.put ("Properties", item);
		
		menu.addSeparator();
		
		item = new JMenuItem (new EditPreferencesAction() );
		menu.add (item);
		items_.put ("Preferences", item);
		
		this.add (menu);
		
		
		// Command menu
		//
		
		commandMenu_ = new JMenu ("Command", true); // an empty menu
		this.add (commandMenu_);
		
		applicationMenu_ = new JMenu ("Command", true);
		applicationMenu_.setMnemonic ('c');
		
		// -----------------
		item = new JMenuItem (new CommandConfigureAction( frame ) );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Configure", item);
		
		item = new JMenuItem (new CommandEnableAction( frame ) );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Enable", item);
		
		item = new JMenuItem (new CommandDisableAction( frame ) );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Disable", item);

		
		item = new JMenuItem (new CommandSuspendAction( frame ) );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Suspend", item);
		
		item = new JMenuItem (new CommandResumeAction( frame ) );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Resume", item);
		
		item = new JMenuItem (new CommandHaltAction( frame ) );
		item.setEnabled(false);
		menu.add (item);
		items_.put ("Halt", item);
		
		item = new JMenuItem (new CommandClearAction( frame ) );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Clear", item);
		
		item = new JMenuItem (new CommandResetAction(frame) );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Reset", item);
		
		item = new JMenuItem ( new TclScriptSelectionAction ( "Execute", frame_ ));
		item.setEnabled(true);
		items_.put ("Execute", item);
		
		
		// ParamsGet, ParamsSet, ParamQuery and InterfaceQuery not supported here yet.
		
		menu = new JMenu ("Window");
		menu.setMnemonic ('w');
		
		item = new JMenuItem (new CommandUserAction(frame) );
		item.setEnabled(false);
		menu.add (item);
		items_.put ("User command...", item);
		
		item = new JMenuItem (new TclScriptAction(frame) );
		item.setEnabled(false);
		menu.add (item);
		items_.put ("Script...", item);
		
		menu.addSeparator();
		
		item = new JMenuItem (new WindowJobAction() );
		menu.add(item);
		item.setEnabled(false);
		items_.put ("Job", item);
		
		this.add (menu);
		
		// Help Menu
		menu = new JMenu ("Help");
		menu.setMnemonic ('h');
		
		// Setup help system
		try {
			URL hsURL = HelpSet.findHelpSet ( null, "./help/Help.hs");
			HelpSet hs = new HelpSet  ( null, hsURL );
			HelpBroker hb = hs.createHelpBroker();
			
			item = new JMenuItem ( "Topics" );
			item.addActionListener ( new CSH.DisplayHelpFromSource ( hb ) );
			menu.add (item);
			items_.put ("Topics", item);
			
		} catch (Exception e) {
			System.out.println ("HelpSet for xdaqWin not found");
		}
		
		item = new JMenuItem (new HelpAboutAction(frame) );
		menu.add (item);
		items_.put ("About", item);
		
		this.add (menu);
	}
	
	public void enable (String menuItem, boolean enabled)
	{
		JMenuItem item = (JMenuItem) items_.get ( (Object) menuItem );
		if (item != null) {
			item.setEnabled ( enabled );
		} else {
			System.out.println ("Did not find menu item: " + menuItem);
		}
	}
	
	
	
	void setItem(String menuItem) {
		commandMenu_.add((JMenuItem) items_.get ( (Object) menuItem ));
		enable(menuItem, true);

	}
	
	
	
	public void setApplicationMenu() {
		commandMenu_.removeAll();
		setItem( "Configure");
		setItem ( "Enable" );
		setItem ( "Disable");
		setItem ( "Suspend");
		setItem ( "Resume" );
		setItem ( "Halt" );
		enable("Properties", true);
		enable("User command...", true);
	}
	
	public void setPartitionMenu() {
		commandMenu_.removeAll();
		Collection c = partitionItems_.values();
		Iterator i = c.iterator();
		while( i.hasNext() ) {
			JMenuItem item = (JMenuItem) i.next();
			item.setEnabled ( true );
			commandMenu_.add(item);
		}
		enable("Properties", false);
		enable("User command...", false);	
	}
	
	public void setHostMenu() {
		commandMenu_.removeAll();
		setItem( "Configure");
		setItem ( "Enable" );
		setItem ( "Disable");
		setItem ( "Suspend");
		setItem ( "Resume" );
		setItem ( "Halt" );
		setItem ( "Reset" );
		setItem ( "Clear");
		enable("Properties", true);
		enable("User command...", true);

	}
	
	public void setScriptMenu() {
		commandMenu_.removeAll();
		setItem( "Execute");
		
	}
	
	
	public void setEmptyMenu() {
		commandMenu_.removeAll();
		enable("Properties", false);
		enable("User command...", false);
	}
	
	
	HashMap items_;
	HashMap partitionItems_;
}
