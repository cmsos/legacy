#ifndef _bril_dipprocessor_Magnet_h_
#define _bril_dipprocessor_Magnet_h_

#include <map>
#include "xdata/Float.h"
#include "xdata/String.h"
#include "bril/dipprocessor/DipAnalyzer.h"

namespace bril{
  namespace dip{

    class Magnet: public DipAnalyzer{
    public:
      Magnet(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName, xdaq::Application* owner);
      // destructor
      ~Magnet();
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // concrete implementation
      virtual void handleMessage(DipSubscription* dipsub, DipData& message);      
    private:
      //Variables associated with the Magnet task
      float m_current;
      std::string m_magnetPub;
      std::string m_dipRoot;
      
    
  
    private:    
      void publish_to_dip();
      // nodefaut constructor
      Magnet();
      // nocopy protection
      Magnet(const Magnet&);
      Magnet& operator=(const Magnet&);
    };
  }
}
#endif
