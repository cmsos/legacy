package xdaq.tools.daqlet;

import java.awt.event.*;
import java.util.*;

public interface Framework extends ActionListener
{
		  /**
     * Adds an actionListener to the Timer
     */
    public void addActionListener(ActionListener listener);


    /**
     * Removes an ActionListener from the Timer.
     */
    public void removeActionListener(ActionListener listener);
	
   /**
     * Notify all listeners that have registered interest for
     * notification on this event type.  The event instance 
     * is lazily created using the parameters passed into 
     * the fire method.
     * @see EventListenerList
     */
    public void fireActionPerformed(ActionEvent e) ;
    
}
