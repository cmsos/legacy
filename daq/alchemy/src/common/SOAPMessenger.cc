// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors:  L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <map>

#include "toolbox/string.h"
#include "SOAPMessenger.h"
#include "HTTPSender.h"
//#include "pt/http/Utils.h"
#include "Address.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/exception/Exception.h"
//#include "pt/http/exception/Exception.h"

//using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::Array;
using v8::String;
using v8::Value;
using v8::Handle;
using v8::HandleScope;
using v8::Context;

nodejs::SOAPMessenger::SOAPMessenger(Logger & logger, pt::Address::Reference destination, pt::Address::Reference local, HTTPSender * sender)
throw (pt::exception::Exception ) :
	logger_(logger), sender_(sender), mutex_(toolbox::BSem::FULL)
{


	// Check for address correct already done in PeerTRansportSender::getMessenger()
	//
	//destination_ = dynamic_cast<http::Address>(destination);
	destination_ = destination;
	local_ = local;

}

nodejs::SOAPMessenger::~SOAPMessenger()
{
	/*
	delete channel_;
	*/
}

pt::Address::Reference nodejs::SOAPMessenger::getLocalAddress()
{
	return local_;
}

pt::Address::Reference nodejs::SOAPMessenger::getDestinationAddress()
{
	return destination_;
}

xoap::MessageReference nodejs::SOAPMessenger::send (xoap::MessageReference message)
throw (pt::exception::Exception)
{
	std::string requestBuffer;

	try
	{
		// serialize message
		message->writeTo (requestBuffer);
	}
	catch (xoap::exception::Exception& e)
	{		
		// Invalid request, cannot handle it
		XCEPT_RETHROW(pt::exception::Exception, "SOAP request message invalid, cannot be serialized", e); 	
	}

	size_t size = requestBuffer.size();

	// Fill the headers
	xoap::MimeHeaders httpMimeHeadersRequest;
	std::multimap<std::string, std::string, std::less<std::string> >& allHeaders = message->getMimeHeaders()->getAllHeaders();
	std::multimap<std::string, std::string, std::less<std::string> >::iterator i;

	std::string headers = "";
	std::string path = "";
	for (i = allHeaders.begin(); i != allHeaders.end(); i++)
	{
			httpMimeHeadersRequest.setHeader((*i).first,(*i).second);
	}

	mutex_.take();

	nodejs::Address& d = dynamic_cast<nodejs::Address&>(*destination_);

	path = d.getPath(); //+ path;

	httpMimeHeadersRequest.setHeader("Connection","keep-alive");
	httpMimeHeadersRequest.setHeader("Content-length",toolbox::toString("%d",size));

	if ( message->countAttachments() > 0 )
	{
		httpMimeHeadersRequest.setHeader("Accept","text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2");
		httpMimeHeadersRequest.setHeader("Content-Description","XDAQ SOAP with attachments.");
		httpMimeHeadersRequest.setHeader("Content-type", "multipart/related; type=" + message->getImplementationFactory()->getMediaType() + ";charset=utf-8; boundary=" + message->getMimeBoundary());


	}
	else
	{
		httpMimeHeadersRequest.setHeader("Content-Description","XDAQ SOAP");
		httpMimeHeadersRequest.setHeader("Content-type",message->getImplementationFactory()->getMediaType() + ";charset=utf-8");
	}

	// send through nodejs magicks

	Isolate * isolate = Isolate::GetCurrent();
	const unsigned argc = 5;
	Local<Value> argv[argc];
	argv[0]= { String::NewFromUtf8(isolate, path.c_str()) };
	argv[1] = { String::NewFromUtf8(isolate, d.getHost().c_str()) };
	argv[2] = { String::NewFromUtf8(isolate, d.getPort().c_str()) };

	//Local<Array> headersArray = Array::New(isolate);
	Local<Object> headersObject = Object::New(isolate);

	std::multimap<std::string, std::string, std::less<std::string> >& fullheadersrmap = httpMimeHeadersRequest.getAllHeaders();
	std::multimap<std::string, std::string, std::less<std::string> >::iterator h;
	int n = 0;
	for (h = fullheadersrmap.begin(); h != fullheadersrmap.end(); h++)
	{
		// Local<Object> object = Object::New(isolate);
		//object->Set(String::NewFromUtf8(isolate, (*h).first.c_str()), String::NewFromUtf8(isolate,(*h).second.c_str()));
	    //headersArray->Set(n++,  object);
	    headersObject->Set(String::NewFromUtf8(isolate, (*h).first.c_str()), String::NewFromUtf8(isolate,(*h).second.c_str()));
	 }
	argv[3] = headersObject;
	argv[4] = { v8::String::NewFromUtf8	(isolate, requestBuffer.c_str(), v8::String::NewStringType::kNormalString, size) };

	// Prepare headers
	//percy->Call(Null(isolate), argc, argv);
	v8::Handle<v8::Value> fnResult;

	fnResult = Local<Function>::New(isolate, sender_->callback_)->Call(isolate->GetCurrentContext()->Global(), argc, argv);
	mutex_.give();

	if (fnResult->IsUndefined())
	{
		// Invalid reply, cannot handle it
		XCEPT_RAISE(pt::exception::Exception, "SOAP reply is undefined");
	}


	// Local<Object> input = Local<Array>::Cast(fnResult);
	Local<Context> context = isolate->GetCurrentContext();
	Local<Object> result = fnResult->ToObject(context).ToLocalChecked();

	if (result->Has(String::NewFromUtf8(isolate,"error")))
	{
		// Invalid reply, cannot handle it
		Handle<Value> errorString = result->Get(String::NewFromUtf8(isolate,"error"));
		std::string message = *String::Utf8Value(errorString);
		XCEPT_RAISE(pt::exception::Exception, message);
	}

	size = 0;
	Handle<Value> resultBody = result->Get(String::NewFromUtf8(isolate,"body"));
	std::string replyBuffer = *String::Utf8Value(resultBody);
	std::cout << "+++" << replyBuffer << "===" << std::endl;
	size = replyBuffer.length();


	xoap::MimeHeaders httpMimeHeadersResponse;
	Local<Value> resultHeaders = result->Get(String::NewFromUtf8(isolate,"headers"));
	Local<Object> resultHeadersObject = resultHeaders->ToObject(context).ToLocalChecked();
	Local<Array> resultHeadersObjects = resultHeadersObject->GetOwnPropertyNames(context).ToLocalChecked();

	for(int ho = 0, l = resultHeadersObjects->Length(); ho< l; ho++) {
		Local<Value> localKey = resultHeadersObjects->Get(ho);
		Local<Value> localVal = resultHeadersObject->Get(context, localKey).ToLocalChecked();
		std::string key = *String::Utf8Value(localKey);
		std::string val = *String::Utf8Value(localVal);
		std::cout << key << ":" << val << std::endl;

	}



	if (size == 0)
	{
		xoap::MessageReference reply(0);
		return reply;
	}

	try
	{
		xoap::MessageReference reply = xoap::createMessage((char*)replyBuffer.c_str() , size);
		return reply;
	} 
	catch (xoap::exception::Exception& e)
	{
		// Invalid reply, cannot handle it
		XCEPT_RETHROW(pt::exception::Exception, "SOAP reply cannot be processed, may be corrupted", e); 	
	}
}


