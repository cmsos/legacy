#ifndef _tcds_pm_APVEInfoSpaceUpdater_h_
#define _tcds_pm_APVEInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class TCADevicePMCommonBase;

    class APVEInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      APVEInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                           tcds::pm::TCADevicePMCommonBase const& hw);
      virtual ~APVEInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::pm::TCADevicePMCommonBase const& getHw() const;

      tcds::pm::TCADevicePMCommonBase const& hw_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_APVEInfoSpaceUpdater_h_
