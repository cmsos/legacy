#ifndef _tcds_cntrl_TCDSCentral_h_
#define _tcds_cntrl_TCDSCentral_h_

#include <string>
#include <vector>

#ifndef XDAQ_TARGET_XDAQ15
#include "xoap/exception/Exception.h"
#endif
#include "xoap/MessageReference.h"

#include "tcds/utils/XDAQAppBase.h"

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace toolbox {
  namespace task {
    class WorkLoop;
  }
}

namespace xdaq {
  class ApplicationStub;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace cntrl {

    class AppsInfoSpaceHandler;
    class AppsInfoSpaceUpdater;
    class TTSInfoSpaceHandler;
    class TTSInfoSpaceUpdater;

    /**
     * The TCDSCentral is the central monitoring application to keep
     * an eye on all TCDS control applications. The XDAQ application
     * itself is really only used to provide a common interface.
     */
    class TCDSCentral : public tcds::utils::XDAQAppBase
    {

    public:
      XDAQ_INSTANTIATOR();

      TCDSCentral(xdaq::ApplicationStub* const stub);
      virtual ~TCDSCentral();

#ifndef XDAQ_TARGET_XDAQ15
      xoap::MessageReference dumpSystemState(xoap::MessageReference const msg) throw (xoap::exception::Exception);
#else
      xoap::MessageReference dumpSystemState(xoap::MessageReference const msg);
#endif

    protected:
      virtual void setupInfoSpaces();

      // Dummy methods in this case.
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

      bool dumpSystemStateCore(toolbox::task::WorkLoop* wl);

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      // NOTE: We can't use auto_ptrs here, since we want to stuff
      // things into an STL vector.
      std::vector<AppsInfoSpaceUpdater*> appsInfoSpaceUpdaterPs_;
      std::vector<AppsInfoSpaceHandler*> appsInfoSpacePs_;
      std::vector<TTSInfoSpaceUpdater*> ttsInfoSpaceUpdaterPs_;
      std::vector<TTSInfoSpaceHandler*> ttsInfoSpacePs_;

      // A helper variable for the 'system dumps.'
      std::string dumpReason_;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_TCDSCentral_h_
