
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_AdvertisementList_h
#define _jxta_AdvertisementList_h

#include <string>
#include <vector>

#include "jxta/exception/Exception.h"
#include "jxta/Advertisement.h"

#include "toolbox/mem/CountingPtr.h"
#include "toolbox/mem/ThreadSafeReferenceCount.h"
#include "toolbox/mem/StandardObjectPolicy.h"

namespace jxta
{

//! This class wraps a list of advertisements.
class AdvertisementList
{
	public:
	typedef toolbox::mem::CountingPtr<AdvertisementList, toolbox::mem::ThreadSafeReferenceCount, toolbox::mem::StandardObjectPolicy> Reference;
	
	virtual ~AdvertisementList() {};
	
	//! Returns the list length.
	virtual int getLength() = 0;

	//! Returns the i-th element of this list as a reference.
	virtual jxta::Advertisement::Reference getItem(int i) = 0;

	//! Adds the provided element to the end of this list.
	virtual void addItem(jxta::Advertisement::Reference adv) = 0;

	//! Removes the i-th element from this list.
	virtual void removeItem(int i) = 0;
};

}

#endif
