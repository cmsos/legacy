/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_SQLView_h_
#define _tstore_SQLView_h_


#include <string>
#include <map>

#include "xoap/DOMParser.h"
#include "xoap/domutils.h"
#include "tstore/View.h"
#include "tstore/SQLViewInternal.h"

namespace tstore 
{

	typedef std::map<const std::string,tstore::SQLViewInternal> SubviewList;

	class SQLView : public tstore::View 
	{
		public:
		//standard View interface
		SQLView(std::string configurationPath,std::string name) ;
		virtual ~SQLView() {}
		virtual bool canQuery() const;
		virtual bool canUpdate() const;
		virtual bool canInsert() const;
		virtual bool canCreate() const;
		virtual bool canRemove() const;
		virtual bool canDestroy() const;

		virtual void create(TStoreAPI *API) ;
	   	virtual void query(TStoreAPI *API) ;
	    	virtual void update(TStoreAPI *API) ;
	    	virtual void insert(TStoreAPI *API) ;
	    	virtual void definition(TStoreAPI *API) ;
	    	virtual void clear(TStoreAPI *API) ;
	   	 virtual void remove(TStoreAPI *API) ;
		virtual void destroy(TStoreAPI *API) ;
		virtual void addTables(TStoreAPI *API) ;
		virtual void removeTables(TStoreAPI *API) ;
		virtual void setParameter(const std::string &parameterName,const std::string &value) ;
		virtual void resetParameters();
		virtual std::vector<std::string> tablesToDestroy();
		std::string namespaceURI();
		private:
		tstore::SQLViewInternal *currentSubview_;
		tstore::SubviewList subViews_;
		std::map<const std::string,std::string> parameters_;
		bool configure_;
		tstore::SQLViewInternal &getSubview() ;
		void throwOperationNotSupported(const std::string &operation,const std::string &tableName) ;
		tstore::SQLViewInternal &getSubviewWithName(const std::string &name) ;
		void removeSubviewWithName(DOMNode *viewNode,const std::string &name) ;
		void removeSubviewWithName(const std::string &name);
		tstore::SQLViewInternal &createSubviewWithName(const std::string &name);
		bool subviewExists(const std::string &name);
	};

}

#endif
