#include "XConfigurator.h"
#include "TcpDispatcherAutoAcceptorTask.h"
#include "Environment.h"
#include "DAQConfig.h"
#include "daqcEVM.h"

#ifdef vxworks
#include "MuxSingleCasterDispatcher.h"
#include "SensSingleCasterDispatcherTask.h"
#endif

#include "TaskAttributes.h"
void usage() {
cout << "usage: evm  [-rc host port | -auto] " << endl;
}
int G_allocated = 0;
int G_cleared = 0;
extern "C" int Main(int argc, char **argv)
{
	//
	// Parse Args
	//
	char  host[256];
	int port;
	int autoinit = 0;
 	for (int i = 1; i < argc; i++)
	{
		if  (strcmp(argv[i],"-auto") == 0 ) {
			autoinit = 1;
			Loader l;
			l.loadEnvironment();
			port = l.DaqCfgEVMPort_;
			strcpy(host,l.DaqCfgEVMIpAddr_);
		}
		else if (strcmp(argv[i],"-rc" ) == 0) {
			if ( argc < 4 ) {
				usage();
				return(-1);
			}
			strcpy(host,argv[i+1]);
			sscanf(argv[i+2],"%d",&port);

		}
	
	}
	

	Try
	{
		
		XConfigurator configurator; //general environment configurator
		TcpDispatcherAutoAcceptorTask  * dispatcher = new TcpDispatcherAutoAcceptorTask(host,port); //TCP run control dispatcher
#ifdef vxworks 
  		MuxSingleCasterDispatcher * t = new MuxSingleCasterDispatcher("dc",0,"dllInput",0x1922 );
		//SensSingleCasterDispatcherTask * t = new SensSingleCasterDispatcherTask("dc",0,"dllInput",0x1922 );
#endif
		daqcEVM * evm = new daqcEVM(dispatcher);


		TaskGroup g;
		dispatcher->initTaskGroup(&g);
		dispatcher->activate();
#ifdef vxworks
		t->setListener(evm);
#endif
  		dispatcher->addListener(&configurator);
		dispatcher->addListener(evm);
		//Sens task only
		TaskAttributes attrib;
		attrib.stacksize(40000);
		attrib.priority(100);
		attrib.name("MuxDisp");
		//t->set(&attrib);
		//t->activate();
		if ( autoinit ) {
			cout << "Autoinitialize..." << endl;
			evm->init("Autoinitialize");
		}
		cout << "Enter quit to stop" << endl;
		char str[20];
		cin >> str;
 		g.join();
	}
	Catch(vxException e)
	{
		cout << e;
		
	};
	return(0);	
}

//
// This include file permits command
// line argument passing under vxworks.
//

#ifndef vxworks
int main(int argc, char* argv[])
{
	return Main(argc,argv);
}
#else
#define MAINFUNC evm
#define MAINNAME "evm"
extern "C" int MAINFUNC(char *args) {
	int argc = 0;
  	char *argv[256];
  	int rv;

  	argv[argc++] = MAINNAME;
  	do {
   		while (*args == ' ') args++;
   		if (*args == '\0') break;
   		argv[argc++] = args;
   		while ((*args != ' ') && (*args != '\0')) args++;
   		if (*args == ' ') *args++ = '\0';
  	} while((*args != '\0') && (argc < 20));
	return Main(argc,argv);
}
#endif
