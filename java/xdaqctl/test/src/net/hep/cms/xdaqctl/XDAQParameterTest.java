/*
 * XDAQParameterTest
 *
 * $Id: XDAQParameterTest.java,v 4.3 2007/08/24 15:09:57 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import java.util.List;
import junit.framework.*;
import junit.extensions.TestSetup;

// DOM
import org.w3c.dom.*;

/**
 * XDAQParameterTest
 */
public class XDAQParameterTest extends TestCase
{
	private static final long WAIT = 1000; // [ms]

	final String PARAMETER_HEADER = 
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<p:properties " +
				"xmlns:p=\"urn:xdaq-application:TestXctl\" " +
				"xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding\" " +
				"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
				"xsi:type=\"soapenc:Struct\">";

	final String PARAMETER_TRAILER = 
				"</p:properties>\n";

	final String PARAMETER_DEFAULT =
				PARAMETER_HEADER +
				"<p:line xsi:type=\"xsd:string\">foo</p:line>" +
				"<p:nullstr xsi:type=\"xsd:string\"></p:nullstr>" +
				"<p:number xsi:type=\"xsd:unsignedLong\">99</p:number>" +
				PARAMETER_TRAILER;

	public XDAQParameterTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(XDAQParameterTest.class);
	}

	public void testConstructors() throws Exception
	{
		String good_result = PARAMETER_DEFAULT;

		XDAQParameter parameter;
		String result;
		
		// (String endpoint, String applicationClass, int instance)
		parameter = new XDAQParameter("http://localhost:40000", "TestXctl", 0);
		assertTrue(parameter != null);

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		// (String endpoint, String soapAction)
		parameter = new XDAQParameter("http://localhost:40000",
				"urn:xdaq-application:lid=11");
		assertTrue(parameter != null);

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		// (String endpoint, String soapAction), 2nd
		parameter = new XDAQParameter("http://localhost:40000",
				"urn:xdaq-application:class=TestXctl,instance=0");
		assertTrue(parameter != null);

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testXDAQParameterString() throws Exception
	{
		XDAQParameter parameter;
		String result;
		
		parameter = new XDAQParameter("http://localhost:40000",
				"TestXctl", 0, PARAMETER_DEFAULT);
		result = TestUtil.compareDOMs(TestUtil.stringToDOM(PARAMETER_DEFAULT),
				parameter.getDOMParameter());
		assertNull(result, result);

		parameter = new XDAQParameter("http://localhost:40000",
				"urn:xdaq-application:lid=11", PARAMETER_DEFAULT);
		result = TestUtil.compareDOMs(TestUtil.stringToDOM(PARAMETER_DEFAULT),
				parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testGetValue() throws Exception
	{
		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		String good_result = "foo";

		assertEquals(good_result, parameter.getValue("line"));
		assertEquals("", parameter.getValue("nullstr"));
		assertNull(parameter.getValue("not_exists"));
	}

	public void testRemove() throws Exception
	{
		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		assertEquals("99", parameter.remove("number"));

		String good_result =
				PARAMETER_HEADER +
				"<p:line xsi:type=\"xsd:string\">foo</p:line>" +
				"<p:nullstr xsi:type=\"xsd:string\"></p:nullstr>" +
				PARAMETER_TRAILER;

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testSelect() throws Exception
	{
		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		// XDAQParameter.select() returns # of removed parameters
		assertEquals(2, parameter.select("number"));

		String good_result =
				PARAMETER_HEADER +
				"<p:number xsi:type=\"xsd:unsignedLong\">99</p:number>" +
				PARAMETER_TRAILER;

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testSetValue() throws Exception
	{
		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		assertEquals("0", parameter.setValue("number", "0"));
		assertEquals("non-null", parameter.setValue("nullstr", "non-null"));

		String good_result = PARAMETER_DEFAULT.replaceFirst("99", "0")
				.replaceFirst("</p:nullstr", "non-null</p:nullstr");

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		assertEquals("99", parameter.setValue("number", "99"));
		assertEquals("", parameter.setValue("nullstr", ""));
	}

	public void testGet() throws Exception
	{
		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		parameter.select("line");
		parameter.setValue("line", "bar");

		assertTrue(parameter.get());

		String good_result =
				PARAMETER_HEADER +
				"<p:line xsi:type=\"xsd:string\">foo</p:line>" +
				PARAMETER_TRAILER;

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testSend() throws Exception
	{
		String good_result;
		String result;

		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		parameter.setValue("line", "bar");
		assertTrue(parameter.send());
		parameter.get();

		good_result = PARAMETER_DEFAULT.replaceFirst("foo", "bar");

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		parameter.setValue("line", "baz");
		assertTrue(parameter.send());
		parameter.get();

		good_result = PARAMETER_DEFAULT.replaceFirst("foo", "baz");

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		// reset the changes made in this test
		parameter.setValue("line", "foo");
		parameter.send();
	}

	public void testGetNames() throws Exception
	{
		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		List<String> names = parameter.getNames();

		assertNotNull(names);
		assertEquals(3, names.size());
		assertTrue(names.contains("line"));
		assertTrue(names.contains("nullstr"));
		assertTrue(names.contains("number"));
	}

	public void testXDAQParameterCopy() throws Exception
	{
		XDAQParameter original = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		XDAQParameter copy = new XDAQParameter(original);
		assertNotNull(copy);

		// Same contents ?
		assertEquals(TestUtil.domToString(original.getDOMParameter()),
				TestUtil.domToString(copy.getDOMParameter()));

		// Deep copy ?
		copy.setValue("number", "0");
		assertEquals("0", copy.getValue("number"));
		assertEquals("99", original.getValue("number"));

		// Works ?
		assertTrue(copy.send());
		assertTrue(copy.get());

		// Clean up
		original.send();
	}

	public void testDescriptor() throws Exception
	{
		// case 1: parameter creatred with ParameterQuery
		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);

		assertEquals("TestXctl", parameter.getDescriptor("className"));
		assertEquals("http://localhost:40000",
				parameter.getDescriptor("context"));
		assertEquals("10", parameter.getDescriptor("id"));
		assertEquals("0", parameter.getDescriptor("instance"));

		// case 2: parameter created with a String
		//   The descriptor part is NOT filled.
		parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0, PARAMETER_DEFAULT);
		assertNull(parameter.getDescriptor("className"));
	}

	public void testDifferentPrefix() throws Exception
	{
		String xml_string = PARAMETER_DEFAULT;
		xml_string = xml_string.replaceFirst(":p=", ":foo=");
		xml_string = xml_string.replaceAll("<p:", "<foo:");
		xml_string = xml_string.replaceAll("</p:", "</foo:");

		XDAQParameter parameter;
		String result;
		
		parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0, xml_string);
		assertEquals("baz", parameter.setValue("line", "baz"));

		parameter.send();
		parameter.setValue("line", "foo");
		parameter.get();
		assertEquals("baz", parameter.getValue("line"));
	}

	public void testTimeout() throws Exception
	{
		// Checking 1 second timeout for C'tor, get() and send()

		XDAQParameter parameter = new XDAQParameter(
				"http://localhost:40000", "TestXctl", 0);
		XDAQMessage freeze = new XDAQMessage("Freeze");

		// 'Freeze' the application
		freeze.send("http://localhost:40000", "TestXctl", 0);

		//
		// testing queryParameter() in getValue()
		parameter.setTimeout(1000);
		assertEquals(1000, parameter.getTimeout());

		long before, after;
		before = System.currentTimeMillis();
		try {
			assertNull(parameter.getValue("number")); // queryParameter()
		} catch (XDAQTimeoutException ignored) {}
		after = System.currentTimeMillis();
		assertEquals("expected 1000[ms] timeout, but was " +
				(after - before) + "[ms].",
				5, (after - before) / 200); // 1000..1199[ms]

		// Clean up the executive
		XDAQParameterTest.oneTimeTearDown();
		XDAQParameterTest.oneTimeSetUp();

		// 'Freeze' the application again
		freeze.send("http://localhost:40000", "TestXctl", 0);

		//
		// testing get()
		before = System.currentTimeMillis();
		try {
			assertFalse(parameter.get());
		} catch (XDAQTimeoutException ignored) {}
		after = System.currentTimeMillis();
		assertEquals("expected 1000[ms] timeout, but was " +
				(after - before) + " [ms].",
				5, (after - before) / 200); // 1000..1199[ms]

		// Clean up the executive
		XDAQParameterTest.oneTimeTearDown();
		XDAQParameterTest.oneTimeSetUp();

		// 'Freeze' the application again
		freeze.send("http://localhost:40000", "TestXctl", 0);

		//
		// testing send()
		before = System.currentTimeMillis();
		try {
			assertFalse(parameter.send());
		} catch (XDAQTimeoutException ignored) {}
		after = System.currentTimeMillis();
		assertEquals(10, (after - before) / 100); // 1000..1099[ms]

		// Clean up the executive
		XDAQParameterTest.oneTimeTearDown();
		XDAQParameterTest.oneTimeSetUp();

		// 'Freeze' the application again
		freeze.send("http://localhost:40000", "TestXctl", 0);

		//
		// testing copy c'tor
		XDAQParameter copy = new XDAQParameter(parameter);

		before = System.currentTimeMillis();
		try {
			assertFalse(copy.get());
		} catch (XDAQTimeoutException ignored) {}
		after = System.currentTimeMillis();
		assertEquals(10, (after - before) / 100); // 1000..1099[ms]

		// Clean up the executive
		XDAQParameterTest.oneTimeTearDown();
		XDAQParameterTest.oneTimeSetUp();
	}

	private static void oneTimeSetUp() throws Exception
	{
		Runtime.getRuntime().exec("./test/xdaq/bootxdaq start").waitFor();
		Thread.sleep(WAIT);

		XDAQMessage message = new XDAQMessage(
				"Configure", TestUtil.fileToDOM("test/xdaq/config.xml"));
		message.send("http://localhost:40000", 0);
	}

	private static void oneTimeTearDown() throws Exception
	{
		Runtime.getRuntime().exec("./test/xdaq/bootxdaq stop").waitFor();
		Thread.sleep(WAIT);
	}

	public static Test suite()
	{
		TestSuite suite = new TestSuite(XDAQParameterTest.class);

		TestSetup wrapper = new TestSetup(suite) {
			protected void setUp() throws Exception { oneTimeSetUp() ; }
			protected void tearDown() throws Exception { oneTimeTearDown(); }
		};

		return wrapper;
	}
}

// End of file
// vim: set ts=4 sw=4:
