// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#ifndef _tstore_SyncManager_h_
#define _tstore_SyncManager_h_

namespace tstore 
{

	//perhaps TableDefinition and ForeignKey will eventually be part of the tstore framework, used as arguments to Connection methods etc.
	//but for now it's not necessary.
	struct TableDefinition {
		xdata::Table definition;
		std::string key;
		std::vector<tstore::ForeignKey> foreignKeys;
		std::string name;
	};

	typedef std::vector<TableDefinition> TableList;

	class SyncManager {
		protected:
		Logger logger_;
		Logger &getLogger() { return logger_; }
		public:
		SyncManager(Logger &logger) : logger_(logger) { }
		virtual ~SyncManager() {}
		//void setLogger(Logger &logger) { logger_=logger; }
		virtual bool writesToConfig() {
			return true;
		}
		virtual void syncTableInBothPlaces(TStoreAPI &API,tstore::TableDefinition &table,xdata::Table &definitionInDatabase,const std::string &keysInDatabase,const std::vector<tstore::ForeignKey> &foreignKeysInDatabase) =0;
		virtual void syncTableInConfig(TStoreAPI &API,tstore::TableDefinition &table) =0;
		virtual void syncTableInDatabase(TStoreAPI &API,const std::string &tableName)  {
			try {
				LOG4CPLUS_INFO(getLogger(),"adding metadata for table '"+tableName+"'");
				xdata::Table definition;
				std::string keys;
				std::vector<tstore::ForeignKey> foreignKeysInDatabase;
				API.connection()->getTableDefinition(tableName,definition);
				API.connection()->getKeysForTable(tableName,keys);
				API.getForeignKeysForTable(tableName,foreignKeysInDatabase);
				API.addTableMetadata(definition,tableName,keys,foreignKeysInDatabase);
			} catch (tstore::exception::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"Could not get column type or primary key data for table '"+tableName+"' which was not in the configuration.",e);
			}
		}
		virtual void columnsAreIncompatible(const std::string &columnName,const std::string &typeInDatabase,const std::string &typeInConfig)  {
			//this is usually fine because one type will override the other.
		}
	};


	class SyncToDatabase : public SyncManager {
		public:
		SyncToDatabase(Logger &logger) : SyncManager(logger) {}
		virtual bool writesToConfig() {
			return false;
		}
		virtual void syncTableInBothPlaces(TStoreAPI &API,tstore::TableDefinition &table,xdata::Table &definitionInDatabase,const std::string &keysInDatabase,const std::vector<tstore::ForeignKey> &foreignKeysInDatabase)  {
			try {
				//perhaps if the only thing that has changed is the key, and it's still the same columns but in a different order (so unique constraints are the same)
				//we should just alter the table in place rather than backing it up. In fact that might turn out to be the best thing to do
				//in any case, but we'll see what the users think.
				LOG4CPLUS_INFO(getLogger(),"backing up table '"+table.name+"'");
				API.connection()->backupTable(table.name);
				LOG4CPLUS_INFO(getLogger(),"creating new version of table '"+table.name+"'");
				API.connection()->createTable(table.name,table.key,table.definition);
				API.addForeignKeysToDatabase(table.name,table.foreignKeys.begin(),table.foreignKeys.end());
			} catch (tstore::exception::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"Could not create table '"+table.name+"' as specified in the configuration.",e);
			}
		}
		virtual void syncTableInConfig(TStoreAPI &API,tstore::TableDefinition &table)  {
			try {
				LOG4CPLUS_INFO(getLogger(),"creating table '"+table.name+"'");
				API.connection()->createTable(table.name,table.key,table.definition);
				API.addForeignKeysToDatabase(table.name,table.foreignKeys.begin(),table.foreignKeys.end());
			} catch (tstore::exception::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"Could not create table '"+table.name+"' as specified in the configuration.",e);
			}
		}
		virtual void syncTableInDatabase(TStoreAPI &API,const std::string &tableName)  {
			try {
				API.connection()->dropTable(tableName);
			} catch (tstore::exception::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"Could not drop table '"+tableName+"' which was removed from the configuration.",e);
			}
		}
	};


	class SyncFromDatabase : public SyncManager {
		public:
		SyncFromDatabase(Logger &logger) : SyncManager(logger) {}
		virtual void syncTableInBothPlaces(TStoreAPI &API,tstore::TableDefinition &table,xdata::Table &definitionInDatabase,const std::string &keysInDatabase,const std::vector<tstore::ForeignKey> &foreignKeysInDatabase)  {
			//The configuration will be changed to have the same columns as the database. If the columns exist in both places, 
			//but the column types are incompatible, an error will be returned.
			//actually, I don't think there should be an error in that case, we are synching from the database so the types in the config
			//do not matter. In any case if the types are incompatible, any future operations on this table will fail, so it's better to do something
			//about it. We will have a backup of the config file so no information is lost.
			LOG4CPLUS_INFO(getLogger(),"replacing metadata for table '"+table.name+"'");
			API.removeTableMetadata(table.name);
			API.addTableMetadata(definitionInDatabase,table.name,keysInDatabase,foreignKeysInDatabase);
		}
		virtual void syncTableInConfig(TStoreAPI &API,tstore::TableDefinition &table)  {
			LOG4CPLUS_INFO(getLogger(),"removing metadata for table '"+table.name+"'");
			API.removeTableMetadata(table.name);
		}
	};

	class SyncBothWays : public SyncManager {
		public:
		SyncBothWays(Logger &logger) : SyncManager(logger) {}
		virtual void syncTableInBothPlaces(TStoreAPI &API,tstore::TableDefinition &table,xdata::Table &definitionInDatabase,const std::string &keysInDatabase,const std::vector<tstore::ForeignKey> &foreignKeysInDatabase)  {
			//If the columns have different names or the datatypes are incompatible, TStore will back up the current table with a different name
			//and create a new one. The new table will have all of the columns which were in either the configuration or in the database. 
			//If the columns exist in both places, but the column types are incompatible, an error will be returned.
			std::map<std::string, std::string, xdata::Table::ci_less> columnsInConfig=table.definition.getTableDefinition();
			std::map<std::string, std::string, xdata::Table::ci_less> columnsInDatabase=definitionInDatabase.getTableDefinition();
			std::map<std::string, std::string, xdata::Table::ci_less>::iterator column;
			xdata::Table finalTableDefinition=definitionInDatabase;
			bool addedColumnToDatabase = false;
			bool addedForeignKeyToDatabase = false;

			if (keysInDatabase!=table.key) {
				 //if the key in the configuration is empty we just treat it as if the config does not know what the key is 
				 //(just like it doesn't know about any extra columns in the database) so we will add the key to the config
				 //and use it if the table is recreated.
				if (!table.key.empty()) {
					//otherwise, we do not know what the key should be. We could try to merge the keys to make a new key, but we don't know what 
					//order to put them in, and anyway the keys used should be chosen deliberately based on what is needed, not by an algorithm.
					XCEPT_RAISE(tstore::exception::Exception,"Keys for the table '"+table.name+"' are '"+table.key+"' in the configuration but '"+keysInDatabase+"' in the database. Please run a one-way sync to choose the keys you want.");
				}
			}
			for (column=columnsInConfig.begin();column!=columnsInConfig.end();++column) {
				std::string columnInConfig=toolbox::toupper((*column).first);
				if (!columnsInDatabase.count(columnInConfig)) {
					finalTableDefinition.addColumn(columnInConfig,(*column).second);
					addedColumnToDatabase=true;
					//perhaps if the type is compatible but it's only the default type and the table in the database does not have the best
					//type/check constraints, we should set addedColumnToDatabase to true just to make sure the table is recreated with the right type.
				} /*else if (!isColumnTypeCompatible(connection,columnsInDatabase[(*column).first],(*column).second,true)) {
					columnsAreIncompatible((*column).first,columnsInDatabase[(*column).first],(*column).second);
				} */
			}

			//could do all this using set operations if I defined < on ForeignKey, and sorted the two vectors first
			std::vector<tstore::ForeignKey> finalForeignKeyList(foreignKeysInDatabase);
			std::vector<tstore::ForeignKey> keysNotInDatabase;
			for (std::vector<tstore::ForeignKey>::iterator foreignKeyInConfig=table.foreignKeys.begin();foreignKeyInConfig!=table.foreignKeys.end();++foreignKeyInConfig) {
				bool found=false;
				for (std::vector<tstore::ForeignKey>::const_iterator foreignKeyInDatabase=foreignKeysInDatabase.begin();foreignKeyInDatabase!=foreignKeysInDatabase.end();++foreignKeyInDatabase) {
					if (*foreignKeyInDatabase==*foreignKeyInConfig) {
						found=true;
						break;
					}
				}
				if (!found) {
					finalForeignKeyList.push_back(*foreignKeyInConfig);
					keysNotInDatabase.push_back(*foreignKeyInConfig);
					addedForeignKeyToDatabase=true;
				}
			}

			try {
				if (addedColumnToDatabase) {
					LOG4CPLUS_INFO(getLogger(),"backing up table '"+table.name+"'");
					API.connection()->backupTable(table.name);
					LOG4CPLUS_INFO(getLogger(),"creating new version of table '"+table.name+"'");
					API.connection()->createTable(table.name,keysInDatabase,finalTableDefinition);
					API.addForeignKeysToDatabase(table.name,finalForeignKeyList.begin(),finalForeignKeyList.end());
				} else if (addedForeignKeyToDatabase) {
					API.addForeignKeysToDatabase(table.name,keysNotInDatabase.begin(),keysNotInDatabase.end());
				}
				LOG4CPLUS_INFO(getLogger(),"changing table '"+table.name+"' in the configuration");
				API.removeTableMetadata(table.name);
				API.addTableMetadata(finalTableDefinition,table.name,keysInDatabase,finalForeignKeyList);
			} catch (tstore::exception::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"Could not merge the two versions of table '"+table.name+"'",e);
			}
		}
		virtual void syncTableInConfig(TStoreAPI &API,tstore::TableDefinition &table)  {
			try {
				LOG4CPLUS_INFO(getLogger(),"creating table '"+table.name+"'");
				API.connection()->createTable(table.name,table.key,table.definition);
				API.addForeignKeysToDatabase(table.name,table.foreignKeys.begin(),table.foreignKeys.end());
			} catch (tstore::exception::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"Could not create table '"+table.name+"' as specified in the configuration.",e);
			}
		}
		virtual void columnsAreIncompatible(const std::string &columnName,const std::string &typeInDatabase,const std::string &typeInConfig)  {
			XCEPT_RAISE(tstore::exception::Exception,"Column '"+columnName+"' has type '"+typeInDatabase+"' in the database, but '"+typeInConfig+"' in the configuration. These types are incompatible.");
		}
	};

}

#endif
