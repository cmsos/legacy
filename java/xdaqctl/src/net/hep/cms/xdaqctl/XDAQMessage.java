/*
 * XDAQMessage
 *
 * $Id: XDAQMessage.java,v 4.4 2007/11/07 14:45:17 aohcms Exp $
 */

package net.hep.cms.xdaqctl;

// DOM
import org.w3c.dom.*;
import org.w3c.dom.Node;  // to solve ambiguity with soap.Node

// SOAP
import javax.xml.soap.*;

/**
 * XDAQ SOAP message class
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XDAQMessage extends Message
{
	/**
	 * Create a SOAP message.
	 * The message contains only one 'command' element.
	 * &lt;Body>-&lt;Command>
	 * This constructor is always called even with different argument types.
	 * 
	 * @param  command Simple command string to send.
	 */
	public XDAQMessage(String command)
			throws XDAQMessageException
	{
		try {
			initialize();
		} catch (XDAQMessageException e) {
			throw new XDAQMessageException(
					"Failed to initialize for command: " + command, e);
		}

		// "SOAPAction" MIME header should be added when the message is sent.
		// Set SOAPAction header with a default target
		message.getMimeHeaders().addHeader(
				"SOAPAction", "urn:xdaq-application:lid=0");

		// Add the command element to the SOAP body
		try {
			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
			SOAPBody body = envelope.getBody();

			body.addBodyElement(envelope.createName(command, "xdaq", XDAQ_NS));
		} catch (SOAPException e) {
			throw new XDAQMessageException(
					"Failed to add a command element: " + command + ".", e);
		}
	}

	/**
	 * Create a SOAP message.
	 * The message contains a tree, having a command at top and
	 * a tree underneath it.
	 * &lt;Body>-&lt;Command>-&lt;DOM>...
	 * 
	 * @param  command Simple command string to send.
	 * @param  domElement top DOM node to be place under the command element
	 */
	public XDAQMessage(String command, Element domElement)
			throws XDAQMessageException
	{
		this(command);

		// Add a XML document to the SOAP body
		SOAPPart part = message.getSOAPPart();

		Node commandNode =
				part.getElementsByTagNameNS(XDAQ_NS, command).item(0);

		Node newNode = part.importNode(domElement, true);
		commandNode.appendChild(newNode);
	}

	/**
	 * Create a SOAP message.
	 * The message contains a tree, specified in the argument
	 * &lt;Body>-&lt;DOM>...
	 * 
	 * @param  domElement top node of a DOM tree be place in the message
	 */
	public XDAQMessage(Element domElement)
			throws XDAQMessageException
	{
		this("_dummy");

		// replace the XML contents
		SOAPPart part = message.getSOAPPart();

		Node commandNode =
				part.getElementsByTagNameNS(XDAQ_NS, "_dummy").item(0);

		Node newNode = part.importNode(domElement, true);

		Node parentNode = commandNode.getParentNode();
		parentNode.replaceChild(newNode, commandNode);
	}

	/**
	 * Create a SOAP message from a SOAP message
	 * The message contains a copy of the SOAP message given in the argument.
	 * Only SOAPAction header is rewritten in send().
	 * 
	 * @param  soapMessage SOAP message
	 */
	public XDAQMessage(SOAPMessage soapMessage)
			throws XDAQMessageException
	{
		super(soapMessage);
	}
}

// End of file
// vim: set ts=4 sw=4:
