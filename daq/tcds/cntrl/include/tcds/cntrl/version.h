#ifndef _tcdscntrl_version_h_
#define _tcdscntrl_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSCNTRL_VERSION_MAJOR 3
#define TCDSCNTRL_VERSION_MINOR 13
#define TCDSCNTRL_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSCNTRL_PREVIOUS_VERSIONS "2.8.0"
// else:
#undef TCDSCNTRL_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSCNTRL_VERSION_CODE PACKAGE_VERSION_CODE(TCDSCNTRL_VERSION_MAJOR,TCDSCNTRL_VERSION_MINOR,TCDSCNTRL_VERSION_PATCH)
#ifndef TCDSCNTRL_PREVIOUS_VERSIONS
#define TCDSCNTRL_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSCNTRL_VERSION_MAJOR,TCDSCNTRL_VERSION_MINOR,TCDSCNTRL_VERSION_PATCH)
#else
#define TCDSCNTRL_FULL_VERSION_LIST TCDSCNTRL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSCNTRL_VERSION_MAJOR,TCDSCNTRL_VERSION_MINOR,TCDSCNTRL_VERSION_PATCH)
#endif

namespace tcdscntrl
{
  const std::string package = "tcdscntrl";
  const std::string versions = TCDSCNTRL_FULL_VERSION_LIST;
  const std::string description = "CMS TCDSCentral application.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS central monitoring application.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
