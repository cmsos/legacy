package xdaq.tools.win;

import xdaq.tools.win.xdaqWin;
import java.io.File;


public class WinMain
{
	public static void main (String args[])
	{
		File    partitionFile = null;
		xdaqWin mainWindow    = null;


		checkCommandLineArgs(args);

                // Create and display the main GUI window
                mainWindow = new xdaqWin ();
                mainWindow.show();

		// Open the partition file if it was on the command line
                if(args.length == 1)
                {
                    System.out.print("Opening partition file ");
                    System.out.println(args[0]);

                    partitionFile = new File(args[0]);
                    mainWindow.menuBar_.fileSelected(partitionFile);
                }
	}


	private static void checkCommandLineArgs(String args[])
	{
		File partitionFile = null;


		// Check the number of command line arguments
		if(args.length > 1)
		{
			System.out.println("Invalid number of arguments");
			System.out.println("Expected: 0 or 1");
			System.out.print("Found   : ");
			System.out.println(args.length);
			System.out.println();
			System.out.println("Usage: xdaqWin [FILE]");
			System.out.println("Where FILE is the partition file");
			System.exit(-1);
		}


		// Check the partition file exists if it is on the command line
		if(args.length == 1)
		{
			partitionFile = new File(args[0]);

			try
			{
				if(!partitionFile.exists())
				{
					System.out.println("Error");
                	                System.out.print("The file ");
        	                        System.out.print(args[0]);
	                                System.out.println(" does not exist");
					System.exit(-1);
				}
			}
			catch(Exception e)
			{
				System.out.print("Error when testing if file ");
				System.out.print(args[0]);
				System.out.println(" exists");
				System.out.println(e);
				System.exit(-1);
			}
                }
	}
}	
