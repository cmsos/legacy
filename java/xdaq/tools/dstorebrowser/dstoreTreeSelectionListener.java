package xdaq.tools.dstorebrowser;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import xdaq.tools.dstorebrowser.dstoreTree;
import xdaq.tools.dstorebrowser.dstorebrowser;

public class dstoreTreeSelectionListener implements TreeSelectionListener
{
	dstorebrowser win_;

	dstoreTreeSelectionListener(dstorebrowser win)
	{
		win_ = win;
	}
	
	public void valueChanged (TreeSelectionEvent e)
	{
		dstoreTree tree = (dstoreTree) e.getSource();
		TreePath[] paths = tree.getSelectionModel().getSelectionPaths();
		
		// Protect against empty selection
		if (paths == null) return;
		
		// consistency check
		// remember the last item of the first selection.
		// all other selected items must be the same
		
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
		String lastElement = node.toString();
		
		//System.out.println ("Last Element selected: " + lastElement);
		
		for (int i = 1; i < paths.length; i++) {
			node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			String currentElement =  node.toString();
			
			// Transport and Application are equivalent
			if (  currentElement.equals("Transport") ) currentElement = "Application";
			
			if (!lastElement.equals ( currentElement ))
			{
					JOptionPane.showMessageDialog (null,
				 		"Must select nodes of the same type",
                                       		 "Selection failed", JOptionPane.ERROR_MESSAGE);

					tree.getSelectionModel().removeSelectionPath (paths[i]);	
			}
		}
		
/*
		if (lastElement.equals("Application") || lastElement.equals("Transport"))
		{
			// Display something in the right pane 
		}
*/
		
	}
}
