#include "tcds/cpm/LumiSection.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Definitions.h"

tcds::cpm::LumiSection::LumiSection(tcds::cpm::LumiNibble const& nibble) :
  tcds::cpm::LumiNibbleGroupBase(nibble)
{
}

tcds::cpm::LumiSection::LumiSection(std::vector<tcds::cpm::LumiNibble> const& nibbles) :
  tcds::cpm::LumiNibbleGroupBase(nibbles)
{
}

tcds::cpm::LumiSection::~LumiSection()
{
}

void
tcds::cpm::LumiSection::addNibble(tcds::cpm::LumiNibble const& nibble)
{
  // NOTE: No check on the fill number. The fill number comes from the
  // BST, and can in principle change during a run. One does not want
  // that to crash the run. Similar for the run number (although this
  // comes from software and is not expected to change during runs).

  if (nibble.lumiSectionNumber() != nibbles_.front().lumiSectionNumber())
    {
      std::string msgBase = "Trying to add a lumi nibble from lumi section %d to lumi section %d.";
      std::string const msg = toolbox::toString(msgBase.c_str(),
                                                nibble.lumiSectionNumber(),
                                                nibbles_.front().lumiSectionNumber());
      XCEPT_RAISE(tcds::exception::SoftwareProblem, msg.c_str());
    }

  tcds::cpm::LumiNibbleGroupBase::addNibble(nibble);
}
