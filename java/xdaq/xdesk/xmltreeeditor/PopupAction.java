package xmltreeeditor;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.util.*;
import org.w3c.dom.*;

public class PopupAction
    extends AbstractAction {

  private XMLTree m_tree;
  private DefaultMutableTreeNode currentNode;
  private DefaultMutableTreeNode parentNode;
  private XMLTreeNodeUserObject currentNodeUserObject;
  private TreePath currentPath;

  public PopupAction(String name, ImageIcon icon, XMLTree tree) {
    super(name, icon);
    m_tree = tree;
    currentPath = m_tree.getSelectionPath();
    currentNode = (DefaultMutableTreeNode) currentPath.getLastPathComponent();
   // if (!currentNode.isRoot())
          parentNode = (DefaultMutableTreeNode) currentNode.getParent();
   // else
   // parentNode=currentNode;
   // System.out.println("Parent " + parentNode);
    currentNodeUserObject = (XMLTreeNodeUserObject) currentNode.getUserObject();
  }

  public void actionPerformed(ActionEvent e) {
    String actionName = e.getActionCommand();
    //   System.out.println("Hashcode " + actionName.hashCode());
//collapse selected node
    if (actionName == "Collapse") {
      m_tree.collapsePath(currentPath);
    }
//expand selected node
    if (actionName == "Expand") {
      m_tree.expandPath(currentPath);
    }
//delete selected node
    if (actionName == "Delete") {
      deleteNode();
    }
    if (actionName == "Move Up") {
      moveUp();
    }
    if (actionName == "Move Down") {
      moveDown();
    }
    if (actionName == "Insert Element After") {
      insertElementAfter();
    }
    if (actionName == "Insert Element Before") {
      insertElementBefore();
    }

    if (actionName == "Attribute") {
      addAttribute();
    }

    if ( (actionName == "Text") || (actionName == "Comment") ||
        (actionName == "CData") || (actionName == "Processing Instruction")) {
      addTextualNode(actionName);
    }

    if (actionName == "Element") {
      addElement();
    }
    if (actionName == "Normalize tree") {
     normalizeTree();
   }

  }

//Delete selected tree node
  private void deleteNode() {
    DefaultMutableTreeNode currentNode = (DefaultMutableTreeNode)
        (m_tree.getSelectionPath().getLastPathComponent());

    DefaultMutableTreeNode parent = (DefaultMutableTreeNode) (currentNode.
        getParent());
    XMLTreeNodeUserObject parent_UserObject = (XMLTreeNodeUserObject) parent.
        getUserObject();

    if (parent != null) {
      DefaultTreeModel mmodel = (DefaultTreeModel) m_tree.getModel();
      mmodel.removeNodeFromParent(currentNode);
      updateAttributesList(parent);
      m_tree.revalidate();
    }
  }

//move any node in the tree upstair
  private void moveUp() {
    DefaultMutableTreeNode newNode = currentNode;
    int index = parentNode.getIndex(currentNode);
    XMLTreeModel model = (XMLTreeModel) m_tree.getModel();
    System.out.println(model.getIndexOfChild(parentNode, currentNode));
    model.removeNodeFromParent(currentNode);

    model.insertNodeInto(newNode, parentNode, index - 1);
    m_tree.revalidate();
    m_tree.revalidate();
    m_tree.setSelectionPath(currentPath);
  }

//move any node in the tree downstair
  private void moveDown() {
    DefaultMutableTreeNode newNode = currentNode;

    int index = parentNode.getIndex(currentNode);
    XMLTreeModel model = (XMLTreeModel) m_tree.getModel();
    System.out.println(model.getIndexOfChild(parentNode, currentNode));
    model.removeNodeFromParent(currentNode);

    model.insertNodeInto(newNode, parentNode, index + 1);
    m_tree.revalidate();
    m_tree.setSelectionPath(currentPath);
  }

//insert element before selected node
  private void insertElementBefore() {
    //System.out.println("HERE");
    XMLTreeNodeUserObject newUserObject = new XMLTreeNodeUserObject(
        "New_Element", null, (short) 1);
    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(newUserObject);
    XMLTreeModel model = (XMLTreeModel) m_tree.getModel();
    model.insertNodeInto(newNode, parentNode, parentNode.getIndex(currentNode));
    TreePath newPath = new TreePath(newNode.getPath());
    m_tree.setSelectionPath(newPath);
    m_tree.revalidate();
  }

//insert element after selected node
  private void insertElementAfter() {
    //System.out.println("HERE");
    XMLTreeNodeUserObject newUserObject = new XMLTreeNodeUserObject(
        "New_Element", null, (short) 1);
    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(newUserObject);
    XMLTreeModel model = (XMLTreeModel) m_tree.getModel();
    model.insertNodeInto(newNode, parentNode,
                         parentNode.getIndex(currentNode) + 1);
    TreePath newPath = new TreePath(newNode.getPath());
    m_tree.setSelectionPath(newPath);
    m_tree.revalidate();
  }

//add an attribute to selected element
  private void addAttribute() {
    XMLTreeNodeUserObject userObject = (XMLTreeNodeUserObject) currentNode.
        getUserObject();

    int insert_index = 0;
    DefaultMutableTreeNode tmpnode;
    XMLTreeNodeUserObject tmpUserObject;
    for (int i = 0; i < currentNode.getChildCount(); i++) {
      tmpnode = (DefaultMutableTreeNode) currentNode.getChildAt(i);
      tmpUserObject = (XMLTreeNodeUserObject) tmpnode.getUserObject();
      if (tmpUserObject.getType() == 2) {
        insert_index++;
      }
    }

    DefaultMutableTreeNode new_node = new DefaultMutableTreeNode(new
        XMLTreeNodeUserObject("New_Attribute", "New Value", (short) 2));
    DefaultTreeModel mm = (DefaultTreeModel) m_tree.getModel();
    mm.insertNodeInto(new_node, currentNode, insert_index);
    updateAttributesList(currentNode);
    m_tree.revalidate();
    m_tree.repaint();
    m_tree.scrollPathToVisible(new TreePath(new_node.getPath()));
    m_tree.setSelectionPath(new TreePath(new_node.getPath()));

  }

  private void addElement() {
    DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(new
        XMLTreeNodeUserObject("New_Element", "", (short) 1));
    DefaultTreeModel model=(DefaultTreeModel) m_tree.getModel();
    model.insertNodeInto(newNode,currentNode,currentNode.getChildCount());
    m_tree.revalidate();
    m_tree.repaint();
    m_tree.scrollPathToVisible(new TreePath(newNode.getPath()));
    m_tree.setSelectionPath(new TreePath(newNode.getPath()));

  }

//add any Text, Comment,CData or Processing Instruction node
  private void addTextualNode(String _actionName) {
    Hashtable userobjects = new Hashtable();
    userobjects.put("Text",
                    new DefaultMutableTreeNode(new
                                               XMLTreeNodeUserObject("#text",
        "New Text", (short) 3)));
    userobjects.put("Comment",
                    new DefaultMutableTreeNode(new
                                               XMLTreeNodeUserObject("#comment",
        "New Comment", (short) 8)));
    userobjects.put("CData",
                    new DefaultMutableTreeNode(new
                                               XMLTreeNodeUserObject("#cdata",
        "New Cdata section", (short) 4)));
    userobjects.put("Processing Instruction",
                    new DefaultMutableTreeNode(new
                                               XMLTreeNodeUserObject("Target",
        "Data", (short) 7)));

    DefaultMutableTreeNode newNode = (DefaultMutableTreeNode) userobjects.get(
        _actionName);
    DefaultTreeModel model = (DefaultTreeModel) m_tree.getModel();
    model.insertNodeInto(newNode, currentNode, currentNode.getChildCount());
    m_tree.revalidate();
    m_tree.repaint();
    m_tree.scrollPathToVisible(new TreePath(newNode.getPath()));
    m_tree.setSelectionPath(new TreePath(newNode.getPath()));
  }

//Update parent node attributes list after deleting or adding some attribute

  private void updateAttributesList(DefaultMutableTreeNode node) {

    DefaultMutableTreeNode parent = node;
    XMLTreeNodeUserObject userObject = (XMLTreeNodeUserObject) node.
        getUserObject();
    String atrrString = "";
    DefaultMutableTreeNode tmpnode;
    XMLTreeNodeUserObject tmpUserObject;
    for (int i = 0; i < parent.getChildCount(); i++) { //update list of attributes
      tmpnode = (DefaultMutableTreeNode) parent.getChildAt(i);
      tmpUserObject = (XMLTreeNodeUserObject) tmpnode.getUserObject();
      if (tmpUserObject.getType() == 2) {
        atrrString = atrrString + tmpUserObject.getName() + "=" +
            tmpUserObject.getValue() + " ,";
      }
    }
    if (atrrString.length() > 0) {
      atrrString = atrrString.substring(0, atrrString.length() - 2);
      userObject.setAttrList("(" + atrrString.trim() + ")");
    }
    else {
      userObject.setAttrList("");
    }
  }

  private void normalizeTree(){
    Document document;
    XMLTreeModel model=(XMLTreeModel) m_tree.getModel();
    document=model.getDocument();
    document.normalize();
    JViewport viewPort=(JViewport) m_tree.getParent();
    viewPort.remove(0);
    m_tree=new XMLTree(new XMLTreeModel(document));
    m_tree.addMouseListener(new PopupTrigger());
        XMLTreeCellRenderer treeCellRenderer = new XMLTreeCellRenderer();
        m_tree.setCellRenderer(treeCellRenderer);
        m_tree.setCellEditor( (TreeCellEditor) treeCellRenderer);
        m_tree.setRowHeight(25);
    viewPort.add(m_tree);
    m_tree.revalidate();
    m_tree.repaint();
  }

}