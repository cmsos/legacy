#include "tcds/cpm/BRILDAQLoop.h"

#include <cmath>
#include <memory>
#include <sstream>
#include <stdint.h>
#include <unistd.h>
#include <vector>

#include "toolbox/BSem.h"
#include "toolbox/string.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/exception/Exception.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"

#include "tcds/cpm/BRILDAQPublisher.h"
#include "tcds/cpm/ConfigurationInfoSpaceHandler.h"
#include "tcds/cpm/CPMController.h"
#include "tcds/cpm/BRILDAQPacket.h"
#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/exception/Exception.h"
#include "tcds/utils/Definitions.h"
#include "tcds/utils/LockGuard.h"

tcds::cpm::BRILDAQLoop::BRILDAQLoop(tcds::cpm::CPMController& cpmController) :
  nibbleDAQAlarmName_("CPMNibbleDAQAlarm"),
  brilDAQAlarmName_("CPMBRILDAQAlarm"),
  cpmController_(cpmController),
  daqWorkLoopP_(0),
  workLoopName_(""),
  numNibblesMissed_(0),
  numNibblesReadTwice_(0),
  numNibblesInvalid_(0),
  numNibblesCorrupted_(0),
  numNibblesAlmostCorrupted_(0),
  lock_(toolbox::BSem::FULL),
  brildaqPublisherP_(0),
  swVersion_(0),
  fwVersion_(0),
  fedId_(0),
  brildaqStorage_(cpmController)// ,
  // sectionStorage_(&cpmController)
{
  std::stringstream workLoopName;
  uint32_t const instanceNumber = cpmController_.getApplicationDescriptor()->getInstance();
  workLoopName << "BRILDAQWorkLoop_" << instanceNumber;
  workLoopName_ = workLoopName.str();
  daqWorkLoopP_ = std::auto_ptr<toolbox::task::WorkLoop>(toolbox::task::getWorkLoopFactory()->getWorkLoop(workLoopName_, "waiting"));
  toolbox::task::ActionSignature* const updateAction =
    toolbox::task::bind(this, &tcds::cpm::BRILDAQLoop::update, "updateAction");
  daqWorkLoopP_->submit(updateAction);
}

tcds::cpm::BRILDAQLoop::~BRILDAQLoop()
{
  stop();
  daqWorkLoopP_.reset();
}

void
tcds::cpm::BRILDAQLoop::start()
{
  clearHistory();
  std::string const problemDescBase =
    "Failed to start NibbleDAQ/BRILDAQ workloop: '%s'.";
  std::string problemDesc = "";
  if (daqWorkLoopP_.get())
    {
      try
        {
          if (!daqWorkLoopP_->isActive())
            {
              daqWorkLoopP_->activate();
            }
        }
      catch (toolbox::task::exception::Exception const& err)
        {
          problemDesc = toolbox::toString(problemDescBase.c_str(), err.what());
        }
    }
  else
    {
      problemDesc = toolbox::toString(problemDescBase.c_str(), "Work loop does not exist.");
    }

  // If necessary: raise the alarms.
  if (problemDesc.size())
    {
      XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, alarmException0, problemDesc);
      cpmController_.raiseAlarm(brilDAQAlarmName_, "error", alarmException0);
      XCEPT_DECLARE(tcds::exception::NibbleDAQFailureAlarm, alarmException1, problemDesc);
      cpmController_.raiseAlarm(nibbleDAQAlarmName_, "error", alarmException1);
    }
}

void
tcds::cpm::BRILDAQLoop::stop()
{
  if (daqWorkLoopP_.get())
    {
      if (daqWorkLoopP_->isActive())
        {
          daqWorkLoopP_->cancel();
        }
    }
}

void
tcds::cpm::BRILDAQLoop::resetCounters()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  numNibblesMissed_ = 0;
  numNibblesReadTwice_ = 0;
  numNibblesInvalid_ = 0;
  numNibblesCorrupted_ = 0;
  numNibblesAlmostCorrupted_ = 0;
}

void
tcds::cpm::BRILDAQLoop::clearHistory()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  resetCounters();
  nibbles_.clear();
  sections_.clear();
}

bool
tcds::cpm::BRILDAQLoop::isHwConnected() const
{
  tcds::cpm::TCADeviceCPMT1 const& hw = cpmController_.getHwT1();
  return hw.isHwConnected();
}

bool
tcds::cpm::BRILDAQLoop::update(toolbox::task::WorkLoop* workLoop)
{
  std::auto_ptr<BRILDAQPacket> brildaqPacket;
  tcds::cpm::TCADeviceCPMT1 const& hw = cpmController_.getHwT1();

  if (!hw.isHwConnected())
    {
      brildaqPublisherP_.reset();
      swVersion_ = 0;
      fwVersion_ = 0;
      fedId_ = 0;
    }

  if (hw.isHwConnected())
    {

      if (!brildaqPublisherP_.get())
        {
          swVersion_ = cpmController_.getHwT1().readSwVersionRaw();
          fwVersion_ = cpmController_.getHwT1().readFwVersionRaw();
          fedId_ = cpmController_.getHwT1().readFEDId();
          std::string const busName =
            cpmController_.getConfigurationInfoSpaceHandler().getString("busName");
          std::string const topicName =
            cpmController_.getConfigurationInfoSpaceHandler().getString("topicName");
          brildaqPublisherP_ =
            std::auto_ptr<tcds::cpm::BRILDAQPublisher>(new tcds::cpm::BRILDAQPublisher(cpmController_, busName, topicName));
        }

      // Read the (hopefully) new packet.
      double const timestamp = toolbox::TimeVal::gettimeofday();
      try
        {
          brildaqPacket = std::auto_ptr<BRILDAQPacket>(new BRILDAQPacket(hw.readBRILDAQPacket(),
                                                                         timestamp));
          // If we get here, let's revoke any existing alarm...
          cpmController_.revokeAlarm(nibbleDAQAlarmName_);
        }
      catch (tcds::exception::Exception const& err)
        {
          // Raise the alarm.
          std::string const msg =
            toolbox::toString("At least one CPM nibbleDAQ update poll failed: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::NibbleDAQFailureAlarm, e, msg);
          cpmController_.raiseAlarm(nibbleDAQAlarmName_, "fatal", e);
        }

      //----------

      bool havePacket = false;
      bool haveNewSection = false;
      if ((brildaqPacket.get() != 0) &&
          !brildaqPacket->isEmpty())
        {
          // Kinda course locking, but okay.
          // NOTE: Just make sure the BRILDAQ publishing is not
          // included inside the lock. If that times out, it will slow
          // down everything else as well.
          tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

          bool readSomethingTwice = true;
          bool missedSomething = true;
          uint32_t numMissed = 0;

          uint32_t currSection = 0;
          uint32_t currNibble = 0;
          uint32_t prevSection = 0;
          uint32_t prevNibble = 0;
          uint32_t prevNumNibblesPerSection = 0;
          uint32_t prevRun = 0;

          if (!brildaqPacket->isCorrupted() &&
              brildaqPacket->isValid())
            {
              // See if we have missed any nibbles, or something like
              // that.
              if (nibbles_.size() < 1)
                {
                  readSomethingTwice = false;
                  missedSomething = false;
                }
              else
                {
                  LumiNibble const& latestNibble = nibbles_.back();
                  currSection = brildaqPacket->lumiSectionNumber();
                  currNibble = brildaqPacket->lumiNibbleNumber();
                  prevSection = latestNibble.lumiSectionNumber();
                  prevNibble = latestNibble.lumiNibbleNumber();
                  prevNumNibblesPerSection = brildaqPacket->numNibblesPerSection();
                  prevRun = brildaqPacket->runNumber();

                  if ((currSection == prevSection) && (currNibble == prevNibble))
                    {
                      readSomethingTwice = true;
                      missedSomething = false;
                    }
                  else
                    {
                      readSomethingTwice = false;
                      // NOTE: Special case for when a new run has
                      // started. Recognized from the fact that the
                      // current nibble is section 1, nibble 1.
                      if ((currSection == 1) && (currNibble == 1))
                        {
                          numMissed = 0;
                        }
                      else if (currSection == prevSection)
                        {
                          numMissed = currNibble - prevNibble - 1;
                        }
                      else
                        {
                          uint32_t const numNibblesPerSection = brildaqPacket->numNibblesPerSection();
                          numMissed =
                            (currSection - prevSection - 1) * numNibblesPerSection +
                            numNibblesPerSection - prevNibble +
                            currNibble - 1;
                        }
                      missedSomething = (numMissed != 0);
                    }
                }
            }

          //----------

          bool dropThisPacket =
            !brildaqPacket->isValid() ||
            brildaqPacket->isCorrupted() ||
            readSomethingTwice;
          havePacket = !dropThisPacket;

          if (!dropThisPacket)
            {
              nibbles_.push_back(LumiNibble(*brildaqPacket));
              size_t const maxNibbleHistoryLength = brildaqPacket->numNibblesPerSection();
              // Limit the nibble history to the number of nibbles in
              // a section, as determined from the latest nibble
              // received.
              while (nibbles_.size() > maxNibbleHistoryLength)
                {
                  nibbles_.pop_front();
                }
              if (sections_.size() > 0)
                {
                  if (brildaqPacket->lumiSectionNumber() == sections_.back().lumiSectionNumber())
                    {
                      sections_.back().addNibble(*brildaqPacket);
                    }
                  else
                    {
                      sections_.push_back(LumiSection(*brildaqPacket));
                      haveNewSection = true;
                    }
                }
              else
                {
                  sections_.push_back(LumiSection(*brildaqPacket));
                  // NOTE: In this case we leave haveNewSection =
                  // false, since there was not even an 'old' lumi
                  // section.
                }

              // Limit the section history to some arbitrary (but
              // defined) number.
              while (sections_.size() > kMaxSectionHistoryLength)
                {
                  sections_.pop_front();
                }
            }

          //----------

          // Send an error in case we encountered any kind of problem.
          std::string const msgBase = "NibbleDAQ problem encountered";
          std::string msg;
          if (!brildaqPacket->isValid())
            {
              numNibblesInvalid_ += 1;
              msg = toolbox::toString("%s: read an invalid nibble packet. "
                                      "Section run %d, %d, nibble %d.",
                                      msgBase.c_str(),
                                      brildaqPacket->runNumber(),
                                      brildaqPacket->lumiSectionNumber(true),
                                      brildaqPacket->lumiNibbleNumber(true));
            }
          else if (brildaqPacket->isCorrupted())
            {
              numNibblesCorrupted_ += 1;
              msg = toolbox::toString("%s: read a corrupted nibble packet. "
                                      "Header: run %d, section %d, nibble %d, "
                                      "footer: run %d, section %d, nibble %d.",
                                      msgBase.c_str(),
                                      brildaqPacket->runNumber(),
                                      brildaqPacket->lumiSectionNumber(true),
                                      brildaqPacket->lumiNibbleNumber(true),
                                      brildaqPacket->runNumber(),
                                      brildaqPacket->lumiSectionNumber(false),
                                      brildaqPacket->lumiNibbleNumber(false));
            }
          else if (brildaqPacket->isAlmostCorrupted())
            {
              numNibblesAlmostCorrupted_ += 1;
              // No error in this case.
              msg = "";
            }
          else if (missedSomething)
            {
              numNibblesMissed_ += numMissed;
              msg = toolbox::toString("%s: missed at least one nibble packet. "
                                      "Last packet received was "
                                      "run %d, section %d, nibble %d, "
                                      "expecting %d nibbles per section. "
                                      "Current packet is section %d, nibble %d.",
                                      msgBase.c_str(),
                                      prevRun,
                                      prevSection,
                                      prevNibble,
                                      prevNumNibblesPerSection,
                                      currSection,
                                      currNibble);
            }
          else if (readSomethingTwice)
            {
              numNibblesReadTwice_ += 1;
              // No error in this case.
              msg = "";
            }
          if (!msg.empty())
            {
              XCEPT_DECLARE(tcds::exception::NibbleDAQProblem, e, msg);
              cpmController_.notifyQualified("fatal", e);
            }
        }

      //----------

      // If the packet is worth keeping, it's worth publishing to the
      // BRILDAQ as well.
      if (havePacket)
        {
          try
            {
              brildaqPublisherP_->publishBRILDAQTable(swVersion_,
                                                      fwVersion_,
                                                      fedId_,
                                                      LumiNibble(*brildaqPacket));
              // If we get here, let's revoke any existing
              // alarm...
              cpmController_.revokeAlarm(brilDAQAlarmName_);
            }
          catch (tcds::exception::Exception const& err)
            {
              // Raise the alarm.
              std::string const msg =
                toolbox::toString("At least one CPM BRILDAQ publication update failed: '%s'.", err.what());
              XCEPT_DECLARE(tcds::exception::BRILDAQFailureAlarm, e, msg);
              cpmController_.raiseAlarm(brilDAQAlarmName_, "fatal", e);
            }
        }

      //----------

      // If we have a new section, let's push the old one into the
      // 'storage' InfoSpace on its way to the database.
      if (haveNewSection)
        {
          // ASSERT ASSERT ASSERT
          assert (sections_.size() > 1);
          // ASSERT ASSERT ASSERT end

          LumiSection const& sectionToStore = sections_.at(sections_.size() - 2);

          //----------

          // sectionStorage_.update(sectionToStore);

          //----------

          // The book keeping information.
          brildaqStorage_.setUInt32("fill_number", sectionToStore.fillNumber());
          brildaqStorage_.setUInt32("run_number", sectionToStore.runNumber());
          brildaqStorage_.setUInt32("section_number", sectionToStore.lumiSectionNumber());
          brildaqStorage_.setUInt32("num_nibbles", sectionToStore.numNibbles());

          //----------

          std::vector<std::string> types;
          types.push_back("");
          types.push_back("_beamactive");

          //----------

          // The trigger counts.
          for (std::vector<std::string>::const_iterator it = types.begin();
               it != types.end();
               ++it)
            {
              bool const gated = (*it == "_beamactive");
              brildaqStorage_.setUInt32(toolbox::toString("trigger_count%s_total", it->c_str()),
                                        sectionToStore.triggerCountTotal(gated));
              for (int trigType = tcds::definitions::kTrigTypeMin;
                   trigType <= tcds::definitions::kTrigTypeMax;
                   ++trigType)
                {
                  brildaqStorage_.setUInt32(toolbox::toString("trigger_count%s_trigtype%d", it->c_str(), trigType),
                                            sectionToStore.triggerCount(trigType, gated));
                }
            }

          //----------

          // The missed-trigger counts.
          for (std::vector<std::string>::const_iterator it = types.begin();
               it != types.end();
               ++it)
            {
              bool const gated = (*it == "_beamactive");
              brildaqStorage_.setUInt32(toolbox::toString("suppressed_trigger_count%s_total", it->c_str()),
                                        sectionToStore.suppressedTriggerCountTotal(gated));
              for (int trigType = tcds::definitions::kTrigTypeMin;
                   trigType <= tcds::definitions::kTrigTypeMax;
                   ++trigType)
                {
                  brildaqStorage_.setUInt32(toolbox::toString("suppressed_trigger_count%s_trigtype%d", it->c_str(), trigType),
                                            sectionToStore.suppressedTriggerCount(trigType, gated));
                }
            }

          //----------

          // The trigger rates.
          for (std::vector<std::string>::const_iterator it = types.begin();
               it != types.end();
               ++it)
            {
              bool const gated = (*it == "_beamactive");
              brildaqStorage_.setFloat(toolbox::toString("trigger_rate%s_total", it->c_str()),
                                       sectionToStore.triggerRateTotal(gated));
              for (int trigType = tcds::definitions::kTrigTypeMin;
                   trigType <= tcds::definitions::kTrigTypeMax;
                   ++trigType)
                {
                  brildaqStorage_.setFloat(toolbox::toString("trigger_rate%s_trigtype%d", it->c_str(), trigType),
                                           sectionToStore.triggerRate(trigType, gated));
                }
            }

          //----------

          // The missed-trigger rates.
          for (std::vector<std::string>::const_iterator it = types.begin();
               it != types.end();
               ++it)
            {
              bool const gated = (*it == "_beamactive");
              brildaqStorage_.setFloat(toolbox::toString("suppressed_trigger_rate%s_total", it->c_str()),
                                       sectionToStore.suppressedTriggerRateTotal(gated));
              for (int trigType = tcds::definitions::kTrigTypeMin;
                   trigType <= tcds::definitions::kTrigTypeMax;
                   ++trigType)
                {
                  brildaqStorage_.setFloat(toolbox::toString("suppressed_trigger_rate%s_trigtype%d", it->c_str(), trigType),
                                           sectionToStore.suppressedTriggerRate(trigType, gated));
                }
            }

          //----------

          // Deadtimes.
          for (std::vector<std::string>::const_iterator it = types.begin();
               it != types.end();
               ++it)
            {
              bool const gated = (*it == "_beamactive");
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_total", it->c_str()),
                                       sectionToStore.deadtimeFractionTotal(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_tts", it->c_str()),
                                       sectionToStore.deadtimeFractionFromTTSAllPartitions(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_trigger_rules", it->c_str()),
                                       sectionToStore.deadtimeFractionFromTriggerRules(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_bunch_mask_veto", it->c_str()),
                                       sectionToStore.deadtimeFractionFromBunchMaskVeto(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_retri", it->c_str()),
                                       sectionToStore.deadtimeFractionFromRetri(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_apve", it->c_str()),
                                       sectionToStore.deadtimeFractionFromAPVE(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_daq_backpressure", it->c_str()),
                                       sectionToStore.deadtimeFractionFromDAQBackpressure(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_calibration", it->c_str()),
                                       sectionToStore.deadtimeFractionFromCalibration(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_sw_pause", it->c_str()),
                                       sectionToStore.deadtimeFractionFromSWPause(gated));
              brildaqStorage_.setFloat(toolbox::toString("deadtime%s_fw_pause", it->c_str()),
                                       sectionToStore.deadtimeFractionFromFWPause(gated));
            }

          // And the push into the InfoSpace.
          brildaqStorage_.writeInfoSpace();
        }

    }

  //----------

  // Wait a little in order not to completely hog the CPU.
  ::usleep(kLoopRelaxTime);

  // Return true in order to schedule the next iteration.
  return true;
}

tcds::cpm::BRILDAQSnapshot
tcds::cpm::BRILDAQLoop::snapshot() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  BRILDAQSnapshot snapshot(numNibblesMissed_,
                           numNibblesReadTwice_,
                           numNibblesInvalid_,
                           numNibblesCorrupted_,
                           numNibblesAlmostCorrupted_,
                           nibbles_,
                           sections_);

  return snapshot;
}
