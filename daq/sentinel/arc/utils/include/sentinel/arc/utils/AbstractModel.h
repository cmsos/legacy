// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_AbstractModel_h_
#define _sentinel_arc_utils_AbstractModel_h_

#include "xcept/Exception.h"

#include <xercesc/dom/DOM.hpp>

#include <string>

#include "toolbox/Properties.h"

XERCES_CPP_NAMESPACE_USE

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{

			class Node;

			static const std::string EXCEPTION_UUID_ATTR = "exception";
			static const std::string EVENT_UUID_ATTR = "uniqueid";
			static const std::string SEVERITY_ATTR = "severity";

			class AbstractModel
			{
				public:

					virtual void lock() = 0;
					virtual void unlock() = 0;

					virtual void repack () = 0;

					virtual void repack (toolbox::Properties & e) = 0;

					virtual void fire (toolbox::Properties & e) = 0;

					virtual void revoke (toolbox::Properties & e) = 0;

					virtual void shelve (toolbox::Properties & e) = 0;

					virtual void accept (toolbox::Properties & e) = 0;

					virtual size_t getUniqueID () = 0;

					virtual size_t getSeverityLevel (std::string severity) = 0;

					virtual void addGuard (Node * node) = 0;
					virtual void indexNode (Node * node) = 0;

					virtual sentinel::arc::utils::Node* getOtherNode () = 0;
					//virtual void setOtherNode (Node* node) = 0;

					typedef enum exception_status
					{
						RAISED_UNACCEPTED = 0,
						RAISED_ACCEPTED = 1,
						SHELVED_UNACCEPTED = 2,
						SHELVED_ACCEPTED = 3,
						CLEARED_UNACCEPTED = 4,
						CLEARED_ACCEPTED = 5
					} ExceptionStatus;

					static std::string exceptionStatusToString (ExceptionStatus ex)
					{
						switch (ex)
						{
							case 0:
							{
								return "Raised Unaccepted";
							}
							case 1:
							{
								return "Raised Accepted";
							}
							case 2:
							{
								return "Shelved Unaccepted";
							}
							case 3:
							{
								return "Shelved Accepted";
							}
							case 4:
							{
								return "Cleared Unaccepted";
							}
							case 5:
							{
								return "Cleared Accepted";
							}
							default:
							{
								return "Unknown";
							}
						}
					}

					static std::string exceptionStatusToClass (ExceptionStatus ex)
					{
						switch (ex)
						{
							case 0:
							{
								return "exception-raised-unaccepted";
							}
							case 1:
							{
								return "exception-raised-accepted";
							}
							case 2:
							{
								return "exception-shelved-unaccepted";
							}
							case 3:
							{
								return "exception-shelved-accepted";
							}
							case 4:
							{
								return "exception-cleared-unaccepted";
							}
							case 5:
							{
								return "exception-cleared-accepted";
							}
							default:
							{
								return "exception-unknown";
							}
						}
					}

			};
		}
	}
}
#endif
