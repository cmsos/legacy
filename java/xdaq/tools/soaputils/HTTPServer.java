package xdaq.tools.soaputils;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.event.*;

import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import javax.xml.soap.*;
import javax.xml.parsers.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;
import java.lang.*;

import xdaq.tools.soaputils.httpInputStream;

public class HTTPServer extends Thread
{
    ServerSocket serverSocket_;
    HashMap map_;

    public HTTPServer (int port)
	{
	    try 
	    {
		serverSocket_ = new ServerSocket (port);
//		System.out.println("HttpServer running on port " + port + ".");
	    } catch (Exception ex) {
		System.out.println (ex.toString());
	    }

	    map_ = new HashMap();
	    this.start();
	}

    public void run()
	{
	    Socket             clientSocket;

	    try 
	    {
		try 
		{
		    while (true) 
		    {
			// Accept any client connection.
			clientSocket = serverSocket_.accept();
			processHTTPRequest ( clientSocket );
		    }
		}
		finally {
		    serverSocket_.close();
		}
	    } catch (java.io.IOException ex)
	    {
		System.out.println ("Cannot close server socket.");
	    }
	}
    
    public void processHTTPRequest (Socket clientSocket)
	{
	    OutputStream    out = null;
	    httpInputStream  in;
	    String          line;
	    StringTokenizer tokenizer;
	    String          method;
	    String          url;
	    String          httpVersion;
	    
	    MessageFactory mf = null;
	    
	    try 
	    {
		// Get references to the input and output streams.
		
		
		out = clientSocket.getOutputStream();
		in = new httpInputStream(clientSocket.getInputStream(),false);
		in.readHeader();
		method = in.getMethod();
		
		// If the method is "get", send the response.
		//
		if (method.equalsIgnoreCase("get")) 
		{
		    //sendResponse(out, in.getURL());
		    System.out.println ("HTTP GET not yet implemented.");
		}
		
		// In case of a POST, assume a SOAP message,
		// extract the SOAP function, look it up and call it back.
		//
		if (method.equalsIgnoreCase("post")) 
		{
		    mf = MessageFactory.newInstance();
		    MimeHeaders mimeHeader = new MimeHeaders();
		    mimeHeader.setHeader("Content-Type", "text/xml");
		    SOAPMessage msg = mf.createMessage(mimeHeader, in);
		    SOAPBody body = msg.getSOAPPart().getEnvelope().getBody();
		    
		    // Dump the SOAP message to screen
		    // msg.writeTo(System.out);
		    if (body.hasFault())
		    {
			System.out.println("Fault in SOAP message: "+body.getFault().getFaultString());
			SOAPMessage reply = mf.createMessage();
			SOAPBody b = reply.getSOAPPart().getEnvelope().getBody();
			SOAPFault fault = b.addFault();
			fault.setFaultCode("Client");
			fault.setFaultString("SOAP message contained a Fault element");
			sendResponse (out, reply);
		    } else
		    {
			Iterator i = body.getChildElements();
			SOAPElement e = (SOAPElement) i.next();
			javax.xml.soap.Name bodyName = e.getElementName();
			String methodName = bodyName.getLocalName();
			String objectName = new String("");
			SOAPMessage reply = processPostRequest (objectName, methodName, msg);
			sendResponse (out, reply);
		    }
		    
		}
	    } catch (Exception ex) 
	    {
		System.out.println("HTTPServer exception: " + ex.toString());
		try {
			SOAPMessage reply = mf.createMessage();
			SOAPBody b = reply.getSOAPPart().getEnvelope().getBody();
			SOAPFault fault = b.addFault();
			fault.setFaultCode("Server");
			fault.setFaultString( ex.toString() );
			sendResponse (out, reply);
		} catch (javax.xml.soap.SOAPException sex)
		{
			System.out.println ("Cannot create SOAP reply:" + sex.toString());
		}
			
	    }
	    
	    finally 
	    {
		try
		{
			clientSocket.close();
	   	}
		catch(IOException ex)
		{
		    System.out.println("HTTPServer exception, unable to close socket: "+ ex.toString());
		}
	    }
	}
    
    
    public SOAPMessage processPostRequest (String objectName, String methodName, SOAPMessage msg)
	{
	    ReqRespListener servlet = (ReqRespListener) map_.get (methodName);
	    if (map_.containsKey(methodName))
	    {
		return servlet.onMessage (msg);
	    } else {
	    	try {
	    		MessageFactory mf = MessageFactory.newInstance();
			SOAPMessage reply = mf.createMessage();
			SOAPBody b = reply.getSOAPPart().getEnvelope().getBody();
			SOAPFault fault = b.addFault();
			fault.setFaultCode("Server");
			fault.setFaultString( "SOAP Method not found: " + methodName );
	    		return reply;
		} catch (javax.xml.soap.SOAPException ex)
	     	{
			return null;
		}
	    }
	}

    public void registerListener (String objectName, String methodName, ReqRespListener listener)
	{
	    map_.put (methodName, listener);
	}
    
    
      
    // Sends the response to the client.
    //
    public void sendResponse(OutputStream out, SOAPMessage reply) 
	{
         byte[]            data = new byte[1024];
         int               inCount;
         String            temp;
         long              contentLength;
         String            status = "200 OK";

	 try
	 {
	     out.write(("HTTP/1.1 " + status + "\r\n").getBytes());
	     out.write(("Server: Httpd\r\n").getBytes());
	     out.write(("Date: " + new Date() + "\r\n").getBytes());
	     out.write(("Content-Type: text/xml\r\n").getBytes());
	     out.write(("Content-Length: nnnn\r\n").getBytes());
	     out.write(("\r\n").getBytes());
	     try {
	     	reply.writeTo (out); 
	     } catch (javax.xml.soap.SOAPException ex)
	     {
	     	System.out.println ("Writing SOAP reply to socket failed.");
	     }
	 }
	 catch(IOException ex)
	 {
	     System.out.println("HTTPServer exception, unable to send response: "+ ex.toString());
	 }
      }
};
