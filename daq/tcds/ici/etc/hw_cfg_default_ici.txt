#----------------------------------------------------------------------
# Default iCI configuration.
#----------------------------------------------------------------------

#----------------------------------------
# Disable all inputs.
#----------------------------------------
main.inselect.combined_cyclic_bgo_enable                                 0x00000000
main.inselect.combined_cyclic_trigger_enable                             0x00000000

main.inselect.combined_software_bgo_enable                               0x00000000
main.inselect.combined_software_trigger_enable                           0x00000000

main.inselect.combined_achannel_trigger_enable                           0x00000000
main.inselect.combined_bchannel_trigger_enable                           0x00000000

main.inselect.combined_bunch_trigger_generator_trigger_enable            0x00000000
main.inselect.combined_trigger_generator_trigger_enable                  0x00000000
main.inselect.sequence_trigger_generator_enable                          0x00000000

main.inselect.combined_l1a_in0_trigger_enable                            0x00000000
main.inselect.combined_l1a_in1_trigger_enable                            0x00000000

main.inselect.exclusive_cpm_bgo_enable                                   0x00000000
main.inselect.exclusive_cpm_trigger_enable                               0x00000000
main.inselect.cpm_orbit_select                                           0x00000000

main.inselect.exclusive_lpm_bgo_enable                                   0x00000000
main.inselect.exclusive_lpm_trigger_enable                               0x00000000
main.inselect.lpm_orbit_select                                           0x00000000

main.inselect.exclusive_lpm2_bgo_enable                                  0x00000000
main.inselect.exclusive_lpm2_trigger_enable                              0x00000000
main.inselect.lpm2_orbit_select                                          0x00000000

main.inselect.lemo_orbit_select                                          0x00000000

#----------------------------------------
# Switch off and zero out all cyclic generators.
#----------------------------------------
cyclic_generator0.configuration.enabled                                 0x00000000
cyclic_generator0.configuration.initial_prescale                        0x00000000
cyclic_generator0.configuration.pause                                   0x00000000
cyclic_generator0.configuration.permanent                               0x00000000
cyclic_generator0.configuration.postscale                               0x00000000
cyclic_generator0.configuration.prescale                                0x00000000
cyclic_generator0.configuration.repeat_cycle                            0x00000000
cyclic_generator0.configuration.start_bx                                0x00000000
cyclic_generator0.configuration.init_on_bgo                             0x00000000
cyclic_generator0.configuration.init_bgo_id                             0x00000000
cyclic_generator0.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator0.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator0.configuration.trigger_word.id                         0x00000000
cyclic_generator0.configuration.trigger_word.l1a                        0x00000000

cyclic_generator1.configuration.enabled                                 0x00000000
cyclic_generator1.configuration.initial_prescale                        0x00000000
cyclic_generator1.configuration.pause                                   0x00000000
cyclic_generator1.configuration.permanent                               0x00000000
cyclic_generator1.configuration.postscale                               0x00000000
cyclic_generator1.configuration.prescale                                0x00000000
cyclic_generator1.configuration.repeat_cycle                            0x00000000
cyclic_generator1.configuration.start_bx                                0x00000000
cyclic_generator1.configuration.init_on_bgo                             0x00000000
cyclic_generator1.configuration.init_bgo_id                             0x00000000
cyclic_generator1.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator1.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator1.configuration.trigger_word.id                         0x00000000
cyclic_generator1.configuration.trigger_word.l1a                        0x00000000

cyclic_generator2.configuration.enabled                                 0x00000000
cyclic_generator2.configuration.initial_prescale                        0x00000000
cyclic_generator2.configuration.pause                                   0x00000000
cyclic_generator2.configuration.permanent                               0x00000000
cyclic_generator2.configuration.postscale                               0x00000000
cyclic_generator2.configuration.prescale                                0x00000000
cyclic_generator2.configuration.repeat_cycle                            0x00000000
cyclic_generator2.configuration.start_bx                                0x00000000
cyclic_generator2.configuration.init_on_bgo                             0x00000000
cyclic_generator2.configuration.init_bgo_id                             0x00000000
cyclic_generator2.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator2.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator2.configuration.trigger_word.id                         0x00000000
cyclic_generator2.configuration.trigger_word.l1a                        0x00000000

cyclic_generator3.configuration.enabled                                 0x00000000
cyclic_generator3.configuration.initial_prescale                        0x00000000
cyclic_generator3.configuration.pause                                   0x00000000
cyclic_generator3.configuration.permanent                               0x00000000
cyclic_generator3.configuration.postscale                               0x00000000
cyclic_generator3.configuration.prescale                                0x00000000
cyclic_generator3.configuration.repeat_cycle                            0x00000000
cyclic_generator3.configuration.start_bx                                0x00000000
cyclic_generator3.configuration.init_on_bgo                             0x00000000
cyclic_generator3.configuration.init_bgo_id                             0x00000000
cyclic_generator3.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator3.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator3.configuration.trigger_word.id                         0x00000000
cyclic_generator3.configuration.trigger_word.l1a                        0x00000000

cyclic_generator4.configuration.enabled                                 0x00000000
cyclic_generator4.configuration.initial_prescale                        0x00000000
cyclic_generator4.configuration.pause                                   0x00000000
cyclic_generator4.configuration.permanent                               0x00000000
cyclic_generator4.configuration.postscale                               0x00000000
cyclic_generator4.configuration.prescale                                0x00000000
cyclic_generator4.configuration.repeat_cycle                            0x00000000
cyclic_generator4.configuration.start_bx                                0x00000000
cyclic_generator4.configuration.init_on_bgo                             0x00000000
cyclic_generator4.configuration.init_bgo_id                             0x00000000
cyclic_generator4.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator4.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator4.configuration.trigger_word.id                         0x00000000
cyclic_generator4.configuration.trigger_word.l1a                        0x00000000

cyclic_generator5.configuration.enabled                                 0x00000000
cyclic_generator5.configuration.initial_prescale                        0x00000000
cyclic_generator5.configuration.pause                                   0x00000000
cyclic_generator5.configuration.permanent                               0x00000000
cyclic_generator5.configuration.postscale                               0x00000000
cyclic_generator5.configuration.prescale                                0x00000000
cyclic_generator5.configuration.repeat_cycle                            0x00000000
cyclic_generator5.configuration.start_bx                                0x00000000
cyclic_generator5.configuration.init_on_bgo                             0x00000000
cyclic_generator5.configuration.init_bgo_id                             0x00000000
cyclic_generator5.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator5.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator5.configuration.trigger_word.id                         0x00000000
cyclic_generator5.configuration.trigger_word.l1a                        0x00000000

cyclic_generator6.configuration.enabled                                 0x00000000
cyclic_generator6.configuration.initial_prescale                        0x00000000
cyclic_generator6.configuration.pause                                   0x00000000
cyclic_generator6.configuration.permanent                               0x00000000
cyclic_generator6.configuration.postscale                               0x00000000
cyclic_generator6.configuration.prescale                                0x00000000
cyclic_generator6.configuration.repeat_cycle                            0x00000000
cyclic_generator6.configuration.start_bx                                0x00000000
cyclic_generator6.configuration.init_on_bgo                             0x00000000
cyclic_generator6.configuration.init_bgo_id                             0x00000000
cyclic_generator6.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator6.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator6.configuration.trigger_word.id                         0x00000000
cyclic_generator6.configuration.trigger_word.l1a                        0x00000000

cyclic_generator7.configuration.enabled                                 0x00000000
cyclic_generator7.configuration.initial_prescale                        0x00000000
cyclic_generator7.configuration.pause                                   0x00000000
cyclic_generator7.configuration.permanent                               0x00000000
cyclic_generator7.configuration.postscale                               0x00000000
cyclic_generator7.configuration.prescale                                0x00000000
cyclic_generator7.configuration.repeat_cycle                            0x00000000
cyclic_generator7.configuration.start_bx                                0x00000000
cyclic_generator7.configuration.init_on_bgo                             0x00000000
cyclic_generator7.configuration.init_bgo_id                             0x00000000
cyclic_generator7.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator7.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator7.configuration.trigger_word.id                         0x00000000
cyclic_generator7.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Disable all B-channels.
#----------------------------------------

# B-channel 0: LumiNibble.
bchannels.bchannel0.configuration.single                                 0x00000000
bchannels.bchannel0.configuration.double                                 0x00000000
bchannels.bchannel0.configuration.block                                  0x00000000
bchannels.bchannel0.configuration.bx_or_delay                            0x00000000
bchannels.bchannel0.configuration.bx_sync                                0x00000000
bchannels.bchannel0.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel0.configuration.repeat_cycle                           0x00000000
bchannels.bchannel0.configuration.prescale                               0x00000000
bchannels.bchannel0.configuration.initial_prescale                       0x00000000
bchannels.bchannel0.configuration.postscale                              0x00000000
bchannels.bchannel0.ram                                                  0x00000000 0x00000006

# B-channel 1: BC0.
bchannels.bchannel1.configuration.single                                 0x00000000
bchannels.bchannel1.configuration.double                                 0x00000000
bchannels.bchannel1.configuration.block                                  0x00000000
bchannels.bchannel1.configuration.bx_or_delay                            0x00000000
bchannels.bchannel1.configuration.bx_sync                                0x00000000
bchannels.bchannel1.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel1.configuration.repeat_cycle                           0x00000000
bchannels.bchannel1.configuration.prescale                               0x00000000
bchannels.bchannel1.configuration.initial_prescale                       0x00000000
bchannels.bchannel1.configuration.postscale                              0x00000000
bchannels.bchannel1.ram                                                  0x00000000 0x00000006

# B-channel 2: TestEnable.
bchannels.bchannel2.configuration.single                                 0x00000000
bchannels.bchannel2.configuration.double                                 0x00000000
bchannels.bchannel2.configuration.block                                  0x00000000
bchannels.bchannel2.configuration.bx_or_delay                            0x00000000
bchannels.bchannel2.configuration.bx_sync                                0x00000000
bchannels.bchannel2.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel2.configuration.repeat_cycle                           0x00000000
bchannels.bchannel2.configuration.prescale                               0x00000000
bchannels.bchannel2.configuration.initial_prescale                       0x00000000
bchannels.bchannel2.configuration.postscale                              0x00000000
bchannels.bchannel2.ram                                                  0x00000000 0x00000006

# B-channel 3: PrivateGap.
bchannels.bchannel3.configuration.single                                 0x00000000
bchannels.bchannel3.configuration.double                                 0x00000000
bchannels.bchannel3.configuration.block                                  0x00000000
bchannels.bchannel3.configuration.bx_or_delay                            0x00000000
bchannels.bchannel3.configuration.bx_sync                                0x00000000
bchannels.bchannel3.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel3.configuration.repeat_cycle                           0x00000000
bchannels.bchannel3.configuration.prescale                               0x00000000
bchannels.bchannel3.configuration.initial_prescale                       0x00000000
bchannels.bchannel3.configuration.postscale                              0x00000000
bchannels.bchannel3.ram                                                  0x00000000 0x00000006

# B-channel 4: PrivateOrbit.
bchannels.bchannel4.configuration.single                                 0x00000000
bchannels.bchannel4.configuration.double                                 0x00000000
bchannels.bchannel4.configuration.block                                  0x00000000
bchannels.bchannel4.configuration.bx_or_delay                            0x00000000
bchannels.bchannel4.configuration.bx_sync                                0x00000000
bchannels.bchannel4.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel4.configuration.repeat_cycle                           0x00000000
bchannels.bchannel4.configuration.prescale                               0x00000000
bchannels.bchannel4.configuration.initial_prescale                       0x00000000
bchannels.bchannel4.configuration.postscale                              0x00000000
bchannels.bchannel4.ram                                                  0x00000000 0x00000006

# B-channel 5: Resync.
bchannels.bchannel5.configuration.single                                 0x00000000
bchannels.bchannel5.configuration.double                                 0x00000000
bchannels.bchannel5.configuration.block                                  0x00000000
bchannels.bchannel5.configuration.bx_or_delay                            0x00000000
bchannels.bchannel5.configuration.bx_sync                                0x00000000
bchannels.bchannel5.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel5.configuration.repeat_cycle                           0x00000000
bchannels.bchannel5.configuration.prescale                               0x00000000
bchannels.bchannel5.configuration.initial_prescale                       0x00000000
bchannels.bchannel5.configuration.postscale                              0x00000000
bchannels.bchannel5.ram                                                  0x00000000 0x00000006

# B-channel 6: HardReset.
bchannels.bchannel6.configuration.single                                 0x00000000
bchannels.bchannel6.configuration.double                                 0x00000000
bchannels.bchannel6.configuration.block                                  0x00000000
bchannels.bchannel6.configuration.bx_or_delay                            0x00000000
bchannels.bchannel6.configuration.bx_sync                                0x00000000
bchannels.bchannel6.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel6.configuration.repeat_cycle                           0x00000000
bchannels.bchannel6.configuration.prescale                               0x00000000
bchannels.bchannel6.configuration.initial_prescale                       0x00000000
bchannels.bchannel6.configuration.postscale                              0x00000000
bchannels.bchannel6.ram                                                  0x00000000 0x00000006

# B-channel 7: EC0.
bchannels.bchannel7.configuration.single                                 0x00000000
bchannels.bchannel7.configuration.double                                 0x00000000
bchannels.bchannel7.configuration.block                                  0x00000000
bchannels.bchannel7.configuration.bx_or_delay                            0x00000000
bchannels.bchannel7.configuration.bx_sync                                0x00000000
bchannels.bchannel7.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel7.configuration.repeat_cycle                           0x00000000
bchannels.bchannel7.configuration.prescale                               0x00000000
bchannels.bchannel7.configuration.initial_prescale                       0x00000000
bchannels.bchannel7.configuration.postscale                              0x00000000
bchannels.bchannel7.ram                                                  0x00000000 0x00000006

# B-channel 8: OC0.
bchannels.bchannel8.configuration.single                                 0x00000000
bchannels.bchannel8.configuration.double                                 0x00000000
bchannels.bchannel8.configuration.block                                  0x00000000
bchannels.bchannel8.configuration.bx_or_delay                            0x00000000
bchannels.bchannel8.configuration.bx_sync                                0x00000000
bchannels.bchannel8.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel8.configuration.repeat_cycle                           0x00000000
bchannels.bchannel8.configuration.prescale                               0x00000000
bchannels.bchannel8.configuration.initial_prescale                       0x00000000
bchannels.bchannel8.configuration.postscale                              0x00000000
bchannels.bchannel8.ram                                                  0x00000000 0x00000006

# B-channel 9: Start.
bchannels.bchannel9.configuration.single                                 0x00000000
bchannels.bchannel9.configuration.double                                 0x00000000
bchannels.bchannel9.configuration.block                                  0x00000000
bchannels.bchannel9.configuration.bx_or_delay                            0x00000000
bchannels.bchannel9.configuration.bx_sync                                0x00000000
bchannels.bchannel9.configuration.double_bcommand_delay                  0x00000000
bchannels.bchannel9.configuration.repeat_cycle                           0x00000000
bchannels.bchannel9.configuration.prescale                               0x00000000
bchannels.bchannel9.configuration.initial_prescale                       0x00000000
bchannels.bchannel9.configuration.postscale                              0x00000000
bchannels.bchannel9.ram                                                  0x00000000 0x00000006

# B-channel 10: Stop.
bchannels.bchannel10.configuration.single                                0x00000000
bchannels.bchannel10.configuration.double                                0x00000000
bchannels.bchannel10.configuration.block                                 0x00000000
bchannels.bchannel10.configuration.bx_or_delay                           0x00000000
bchannels.bchannel10.configuration.bx_sync                               0x00000000
bchannels.bchannel10.configuration.double_bcommand_delay                 0x00000000
bchannels.bchannel10.configuration.repeat_cycle                          0x00000000
bchannels.bchannel10.configuration.prescale                              0x00000000
bchannels.bchannel10.configuration.initial_prescale                      0x00000000
bchannels.bchannel10.configuration.postscale                             0x00000000
bchannels.bchannel10.ram                                                 0x00000000 0x00000006

# B-channel 11: Stop.
bchannels.bchannel11.configuration.single                                0x00000000
bchannels.bchannel11.configuration.double                                0x00000000
bchannels.bchannel11.configuration.block                                 0x00000000
bchannels.bchannel11.configuration.bx_or_delay                           0x00000000
bchannels.bchannel11.configuration.bx_sync                               0x00000000
bchannels.bchannel11.configuration.double_bcommand_delay                 0x00000000
bchannels.bchannel11.configuration.repeat_cycle                          0x00000000
bchannels.bchannel11.configuration.prescale                              0x00000000
bchannels.bchannel11.configuration.initial_prescale                      0x00000000
bchannels.bchannel11.configuration.postscale                             0x00000000
bchannels.bchannel11.ram                                                 0x00000000 0x00000006

# B-channel 12: Stop.
bchannels.bchannel12.configuration.single                                0x00000000
bchannels.bchannel12.configuration.double                                0x00000000
bchannels.bchannel12.configuration.block                                 0x00000000
bchannels.bchannel12.configuration.bx_or_delay                           0x00000000
bchannels.bchannel12.configuration.bx_sync                               0x00000000
bchannels.bchannel12.configuration.double_bcommand_delay                 0x00000000
bchannels.bchannel12.configuration.repeat_cycle                          0x00000000
bchannels.bchannel12.configuration.prescale                              0x00000000
bchannels.bchannel12.configuration.initial_prescale                      0x00000000
bchannels.bchannel12.configuration.postscale                             0x00000000
bchannels.bchannel12.ram                                                 0x00000000 0x00000006

# B-channel 13: Stop.
bchannels.bchannel13.configuration.single                                0x00000000
bchannels.bchannel13.configuration.double                                0x00000000
bchannels.bchannel13.configuration.block                                 0x00000000
bchannels.bchannel13.configuration.bx_or_delay                           0x00000000
bchannels.bchannel13.configuration.bx_sync                               0x00000000
bchannels.bchannel13.configuration.double_bcommand_delay                 0x00000000
bchannels.bchannel13.configuration.repeat_cycle                          0x00000000
bchannels.bchannel13.configuration.prescale                              0x00000000
bchannels.bchannel13.configuration.initial_prescale                      0x00000000
bchannels.bchannel13.configuration.postscale                             0x00000000
bchannels.bchannel13.ram                                                 0x00000000 0x00000006

# B-channel 14: Stop.
bchannels.bchannel14.configuration.single                                0x00000000
bchannels.bchannel14.configuration.double                                0x00000000
bchannels.bchannel14.configuration.block                                 0x00000000
bchannels.bchannel14.configuration.bx_or_delay                           0x00000000
bchannels.bchannel14.configuration.bx_sync                               0x00000000
bchannels.bchannel14.configuration.double_bcommand_delay                 0x00000000
bchannels.bchannel14.configuration.repeat_cycle                          0x00000000
bchannels.bchannel14.configuration.prescale                              0x00000000
bchannels.bchannel14.configuration.initial_prescale                      0x00000000
bchannels.bchannel14.configuration.postscale                             0x00000000
bchannels.bchannel14.ram                                                 0x00000000 0x00000006

# B-channel 15: Stop.
bchannels.bchannel15.configuration.single                                0x00000000
bchannels.bchannel15.configuration.double                                0x00000000
bchannels.bchannel15.configuration.block                                 0x00000000
bchannels.bchannel15.configuration.bx_or_delay                           0x00000000
bchannels.bchannel15.configuration.bx_sync                               0x00000000
bchannels.bchannel15.configuration.double_bcommand_delay                 0x00000000
bchannels.bchannel15.configuration.repeat_cycle                          0x00000000
bchannels.bchannel15.configuration.prescale                              0x00000000
bchannels.bchannel15.configuration.initial_prescale                      0x00000000
bchannels.bchannel15.configuration.postscale                             0x00000000
bchannels.bchannel15.ram                                                 0x00000000 0x00000006

#----------------------------------------
# Disable B-data transmission.
#----------------------------------------
bchannels.main.bdata_config.bdata_enable                                 0x00000000

#----------------------------------------------------------------------
