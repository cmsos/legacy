#!/bin/sh
# usage: raise url identifier severity message tag
# e.g. ./raise.sh http://srv-c2d06-17.cms:1972/urn:xdaq-application:lid=1000  myAlarm error "my message" myTag
curl  $1/raise?identifier=$2&severity=$3&message=$4&tag=$5
echo ""
