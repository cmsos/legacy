#!/usr/bin/env python

###############################################################################
## Check BST timestamps from the CPM BRILDAQ packets.
## NOTE: For testing purposes only.
###############################################################################

import datetime
import sys
import time

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_ITERATIONS = 10
SLEEP_TIME = .4

###############################################################################

class Checker(CmdLineBase):

    def handle_args(self):
        # One and only one argument is expected. It has to sound like
        # an IP address.
        if len(self.args) != 1:
            msg = "One arguments is required: " \
                  "the IP address of the device."
            self.error(msg)
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like a valid network address."
                msg = msg.format(ip_address_arg)
                self.error(msg)
            else:
                self.ip_address = ip_address
        # End of handle_args().

    def main(self):
        cpm = get_cpm_hw(self.ip_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        PACKET_SIZE = 448
        BST_OFFSET = 40
        for i in xrange(NUM_ITERATIONS):
            raw_data = cpm.read_block("cpmt1.brildaq", 2 * PACKET_SIZE)
            # Ungated BRILDAQ packet.
            n_microsec = raw_data[BST_OFFSET]
            n_sec = raw_data[BST_OFFSET + 1]
            timestamp_plain = datetime.datetime.utcfromtimestamp(n_sec) + \
                              datetime.timedelta(microseconds=n_microsec)
            # Gated BRILDAQ packet.
            n_microsec = raw_data[PACKET_SIZE + BST_OFFSET]
            n_sec = raw_data[PACKET_SIZE + BST_OFFSET + 1]
            timestamp_gated = datetime.datetime.utcfromtimestamp(n_sec) + \
                              datetime.timedelta(microseconds=n_microsec)
            # Time difference.
            delta = timestamp_gated - timestamp_plain
            delta
            print "----------"
            print "  plain: {0:s}".format(timestamp_plain.isoformat())
            print "  gated: {0:s}".format(timestamp_gated.isoformat())
            print "    delta = {0:s} s".format(delta)
            time.sleep(SLEEP_TIME)

        #----------

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    description = "Check BST timestamps from the CPM BRILDAQ packets.."
    usage = "usage: %prog [options] IP"
    res = Checker(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
