/*
 * XPathAPI
 *
 * $Id: XPathAPI.java,v 4.1 2007/03/26 14:40:24 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

// XPath
import javax.xml.xpath.*;
import javax.xml.namespace.NamespaceContext;

// DOM
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Wrapper to mimic the old Xalan XPathAPI
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XPathAPI
{
	private static XPath xpath;
	private static NamespaceContext nullNamespace;

	static {
		XPathFactory factory = XPathFactory.newInstance();
		xpath = factory.newXPath();

		nullNamespace = new DefaultNamespaceContext();
	}

	public static Node selectSingleNode(Node context, String expression)
			throws XPathAPIException
	{
		NodeList list = selectNodeList(context, expression);

		return (list.getLength() > 0) ? list.item(0) : null;
	}

	public static Node selectSingleNode(
			Node context, String expression, NamespaceContext namespace)
			throws XPathAPIException
	{
		NodeList list = selectNodeList(context, expression, namespace);

		return (list.getLength() > 0) ? list.item(0) : null;
	}

	public static NodeList selectNodeList(Node context, String expression)
			throws XPathAPIException
	{
		if (context == null) {
			throw new IllegalArgumentException("Context node is null.");
		}
		if (expression == null) {
			throw new IllegalArgumentException("XPath expression is null.");
		}

		NodeList list;

		synchronized(xpath) {
			xpath.reset(); // clear NamespaceContext
			xpath.setNamespaceContext(nullNamespace);

			try {
				list = (NodeList)xpath.evaluate(
						expression, context, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				throw new XPathAPIException(
						"Failed expression: " + expression, e);
			}
		}

		return list;
	}

	public static NodeList selectNodeList(
			Node context, String expression, NamespaceContext namespace)
			throws XPathAPIException
	{
		if (context == null) {
			throw new IllegalArgumentException("Context node is null.");
		}
		if (expression == null) {
			throw new IllegalArgumentException("XPath expression is null.");
		}
		if (namespace == null) {
			throw new IllegalArgumentException("Namespace context is null.");
		}

		NodeList list;

		synchronized(xpath) {
			xpath.setNamespaceContext(namespace);

			try {
				list = (NodeList)xpath.evaluate(
						expression, context, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				throw new XPathAPIException(
						"Failed expression: " + expression, e);
			}
		}

		return list;
	}
}

// End of file
// vim: set ts=4 sw=4:
