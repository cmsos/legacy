#include "bril/CommonDataFormat.h"
#include "bril/BCM1FTopics.hh"
#include "bril/LUMITopics.hh"
#include "bril/TCDSTopics.hh"
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <string>
#include <string.h>
#include <vector>
/**
 * test different topic data types
 */
using namespace interface::bril;
int main(int argc, char** argv){
  std::cout<<"  === data version: "<<DATA_VERSION<<std::endl;
  std::cout<<"  === testing compound data === "<<std::endl;

  std::cout<<"headsize "<<sizeof(DatumHead)<<std::endl;
  size_t totsize = bestlumiT::maxsize();
  char *buffer=(char*)malloc(totsize); 
  if (buffer==NULL) exit (1);
  ((DatumHead*)buffer)->setTime(2312,123450,1,1,1398344937,0);
  ((DatumHead*)buffer)->setResource(DataSource::LUMI,0,0,StorageType::COMPOUND);
  ((DatumHead*)buffer)->setTotalsize(totsize);
  ((DatumHead*)buffer)->setFrequency(4);
  std::cout<<bestlumiT::topicname()<<" datasize "<<((DatumHead*)buffer)->totalsize()<<" payloadsize "<<((DatumHead*)buffer)->payloadsize()<<std::endl;
  std::cout<<bestlumiT::topicname()<<" maxsize "<<totsize<<" n "<<bestlumiT::n()<<std::endl;
  free(buffer);

  std::cout<<"  === testing simple data === "<<std::endl;
  totsize = bcm1fhistT::maxsize();
  buffer=(char*)malloc(totsize); 
  if (buffer==NULL) exit (1);
  ((DatumHead*)buffer)->setTime(2312,123450,1,1,1398344937,0);
  ((DatumHead*)buffer)->setResource(DataSource::BCM1F,3,0,StorageType::UINT16);
  ((DatumHead*)buffer)->setTotalsize(totsize);
  ((DatumHead*)buffer)->setFrequency(4);
  std::cout<<bcm1fhistT::topicname()<<" datasize "<<((DatumHead*)buffer)->totalsize()<<" payloadsize "<<((DatumHead*)buffer)->payloadsize()<<std::endl;
  std::cout<<bcm1fhistT::topicname()<<" maxsize "<<totsize<<" n "<<bcm1fhistT::n()<<std::endl;
  std::cout<<"algo id "<<((DatumHead*)buffer)->getAlgoID()<<std::endl;
  free(buffer);

  std::cout<<"  === testing signal data === "<<std::endl;
  totsize = NB1T::maxsize();
  buffer=(char*)malloc(totsize); 
  if (buffer==NULL) exit (1);
  ((DatumHead*)buffer)->setTime(2312,123450,1,1,1398344937,0);
  ((DatumHead*)buffer)->setResource(DataSource::TCDS,0,0,0);
  ((DatumHead*)buffer)->setTotalsize(totsize);
  ((DatumHead*)buffer)->setFrequency(4);
  std::cout<<NB1T::topicname()<<" datasize "<<((DatumHead*)buffer)->totalsize()<<" payloadsize "<<((DatumHead*)buffer)->payloadsize()<<std::endl;
  std::cout<<NB1T::topicname()<<" maxsize "<<totsize<<" n "<<NB1T::n()<<std::endl;
  std::cout<<"NB1 maxsize "<<NB1T::maxsize()<<std::endl;
  std::cout<<"NB1 nitems "<<((DatumHead*)buffer)->nitems()<<std::endl;
  std::cout<<"NB1 n "<<NB1T::n()<<std::endl;
  free(buffer);

}
