# $Id: Makefile,v 1.6 2009/03/04 10:44:22 lorsini Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# This is the TriDAS/extern/dim Makefile
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
Package=extern/caen/caenvmelib

PackageName=caenvmelib

PACKAGE_VER_MAJOR=2
PACKAGE_VER_MINOR=41
PACKAGE_VER_PATCH=0

Summary=Interface library for CAEN VME Bridges

Description= CAENVMELib is a set of ANSI C functions which permits an user program the use and the configuration of the CAEN Bridges:\
 - Mod. V1718 / VX1718 - VME-USB2.0 Bridge\
 - Mod. V2718 / VX2718 - VME-PCI Optical Link Bridge\
 - Mod. A2818 - PCI CONET Controller\
CAENVMELib is logically located between an application like the samples provided and the lower layer software libraries

Link=http://www.caen.ch

UNPACKDIR=CAENVMELib-2.41
TARFILE=CAENVMELib-2.41.tgz
CFLAGS:=$(CFlags)

export CFLAGS
export CC
export CXX
LD:=$(LDD)
export LD

_all: all

default: all

$(UNPACKDIR)/CAEN_License_Agreement.txt:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(UNPACKDIR) 
	tar -zxvf $(TARFILE) 
        
all: $(UNPACKDIR)/CAEN_License_Agreement.txt 
	mkdir -p $(XDAQ_PLATFORM)/include $(XDAQ_PLATFORM)/lib $(XDAQ_PLATFORM)/bin
	install -m 655 $(BUILD_HOME)/$(Project)/$(Package)/$(UNPACKDIR)/include/*.h $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/include
ifeq ($(XDAQ_PLATFORM),x86_slc5)
	install -m 655 $(BUILD_HOME)/$(Project)/$(Package)/$(UNPACKDIR)/lib/x86/*.so.* $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/lib
else ifeq ($(XDAQ_PLATFORM),x86_slc6)
	install -m 655 $(BUILD_HOME)/$(Project)/$(Package)/$(UNPACKDIR)/lib/x86/*.so.* $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/lib
else
	install -m 655 $(BUILD_HOME)/$(Project)/$(Package)/$(UNPACKDIR)/lib/x64/*.so.* $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/lib
endif
	cd $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/lib; ln -sf  libCAENVME.so.2.41 libCAENVME.so
	cp $(BUILD_HOME)/$(Project)/$(Package)/spec.$(XDAQ_PLATFORM).template $(BUILD_HOME)/$(Project)/$(Package)/spec.template


_installall: install

install:
	mkdir -p $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/include $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
	install -m 655 $(BUILD_HOME)/$(Project)/$(Package)/$(UNPACKDIR)/*.h $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/include
ifeq ($(XDAQ_PLATFORM),x86_slc5)
	install -m 655 $(BUILD_HOME)/$(Project)/$(Package)/$(UNPACKDIR)/lib/x86/*.so.* $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib.
else
	install -m 655 $(BUILD_HOME)/$(Project)/$(Package)/$(UNPACKDIR)/lib/x64/*.so.* $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib
endif
	cd $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib; ln -sf libCAENVME.so.2.41 libCAENVME.so

_cleanall: clean

clean:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf $(UNPACKDIR) 
	-rm -rf $(BUILD_HOME)/$(Project)/$(Package)/spec.template

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules
