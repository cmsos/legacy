/*
 * GlobalGeneralConnectionPool.h
 *
 *  Created on: Jul 15, 2010
 *      Author: janulis
 */

#ifndef GLOBALGENERALCONNECTIONPOOL_H_
#define GLOBALGENERALCONNECTIONPOOL_H_

#include <occiControl.h>
#include "tstore/exception/Exception.h"
#include <string>

namespace tstore {

class GlobalGeneralConnectionPool {
protected:
	oracle::occi::Environment* env;
	unsigned int minConn;
	unsigned int maxConn;
	unsigned int incrConn;

	GlobalGeneralConnectionPool(
		unsigned int minConn,
		unsigned int maxConn,
		unsigned int incrConn)
	{
		this->minConn = minConn;
		this->maxConn = maxConn;
		this->incrConn = incrConn;

		env  = oracle::occi::Environment::createEnvironment(oracle::occi::Environment::OBJECT);
	}

public:
	virtual ~GlobalGeneralConnectionPool() {
		if (env) oracle::occi::Environment::terminateEnvironment(env);
	}

	virtual oracle::occi::Environment* getEnvironment() const { return env; }
	virtual oracle::occi::Connection* getConnection(const std::string& user, const std::string& pass, const std::string& db, const std::string &tag="")  = 0;
	virtual void releaseConnection(oracle::occi::Connection* conn, const std::string& db, const std::string &tag="")  = 0;
	virtual void terminateConnection(oracle::occi::Connection* conn, const std::string& db)  = 0;

	virtual unsigned int getBusyConnections(const std::string& db) = 0;
	virtual unsigned int getOpenConnections(const std::string& db) = 0;
	virtual unsigned int getMaxConnections() { return maxConn; }

	virtual void printStats()  {};
};

}

#endif /* GLOBALGENERALCONNECTIONPOOL_H_ */
