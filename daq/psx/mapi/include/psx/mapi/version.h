// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_mapi_version_h_
#define _psx_mapi_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define PSXMAPI_VERSION_MAJOR 1
#define PSXMAPI_VERSION_MINOR 9
#define PSXMAPI_VERSION_PATCH 2
// If any previous versions available E.g. #define PSXMAPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef PSXMAPI_PREVIOUS_VERSIONS


//
// Template macros
//
#define PSXMAPI_VERSION_CODE PACKAGE_VERSION_CODE(PSXMAPI_VERSION_MAJOR,PSXMAPI_VERSION_MINOR,PSXMAPI_VERSION_PATCH)
#ifndef PSXMAPI_PREVIOUS_VERSIONS
#define PSXMAPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(PSXMAPI_VERSION_MAJOR,PSXMAPI_VERSION_MINOR,PSXMAPI_VERSION_PATCH)
#else 
#define PSXMAPI_FULL_VERSION_LIST  PSXMAPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(PSXMAPI_VERSION_MAJOR,PSXMAPI_VERSION_MINOR,PSXMAPI_VERSION_PATCH)
#endif 

namespace psxmapi
{
	const std::string package  =  "psxmapi";
	const std::string versions =  PSXMAPI_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "SMI state machine interface service for PSX";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();

}

#endif
