<!DOCTYPE html>
<html lang="en">
<head>
<title>Speedial5</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- jQuery -->
<script src="../utils/js/jquery-3.2.1.min.js"></script>

<!-- Bootstrap -->
<link rel="stylesheet" href="../utils/bootstrap-3.3.7-dist/css/bootstrap.min.css">
<script src="../utils/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

<!-- DataTables -->
<link rel="stylesheet" href="../utils/DataTables-1.10.15/media/css/jquery.dataTables.min.css">
<script src="../utils/DataTables-1.10.15/media/js/jquery.dataTables.min.js"></script>

<!-- Fancytree -->
<link rel="stylesheet" href="../utils/fancytree-2.24.0/dist/skin-bootstrap-n/ui.fancytree.min.css">
<script src="../utils/fancytree-2.24.0/dist/jquery.fancytree-all-deps.min.js"></script>
<script src="../utils/fancytree-2.24.0/dist/src/jquery.fancytree.table.js"></script>

<link href="../utils/css/pulse.css" rel="stylesheet" type="text/css">
<style>
.navbar{
	margin-bottom:2px;
}
.navbar-nav > li > a, .navbar-brand {
	padding-top:5px !important; padding-bottom:0px !important;
	height: 30px;
}
.navbar {min-height:30px !important;}

/* Define custom width and alignment of table columns */
#treetable {
	table-layout: fixed;
}

/* Set gray background color and 100% height */
.sidenav {
	height: 95%;
	background-color: SlateGray ;
	overflow-y: scroll;
	z-index: 1; /* Stay on top */
	top: 0;
	left: 0;
	overflow-x: hidden; /* Disable horizontal scroll */
	padding-top: 4px; /* Place content 60px from the top */
	transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

#treetable tr td:nth-of-type(1) {
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
}

#treetable tr td:nth-of-type(2) {
	text-align: right;
}

.rightnav {
	
	overflow-y: hidden;
	overflow-x: hidden; /* Disable horizontal scroll */

}


.fullview {
	height: 100%;
}

html, body {
	height: 100%;
}

.container-fluid {
	height: 100%;
	overflow-y: scroll; /* don't show content that exceeds my height */
}

/* Set black background color, white text and some padding */
/*footer {
	background-color: #555;
	color: white;
	padding: 5px;
	width: 100%;
}*/

#buttonbar {
	background-color: #555;
	color: white;
	padding: 15px;
}


.table-borderless > tbody > tr > td,
.table-borderless > tbody > tr > th,
.table-borderless > tfoot > tr > td,
.table-borderless > tfoot > tr > th,
.table-borderless > thead > tr > td,
.table-borderless > thead > tr > th {
	border: none!important;
}

td.highlight {
	font-weight: bold;
}

.floatRight{
	float:right;
}

#autorefresh {
	margin-top: 5px;
	margin-bottom: 5px;
}
</style>
<script>

var zone = "<?php echo $_GET['zone']?>";
var heartbeaturl = "<?php echo $_GET['heartbeaturl']?>";

function doNodeSelection(classname) {
	var serviceTable = $("#servicetable").dataTable().api();
	serviceTable.ajax.url('getservices.php?heartbeaturl=' + heartbeaturl + '&classname=' + classname ).load( null, true );
}

function importTree(modelTree, tree) {
	for (var key in modelTree) {
		//update node
		var node = tree.getNodeByKey(modelTree[key].key);
		node.data.tags[0] = modelTree[key].tags[0];
		node.data.lastupdate = modelTree[key].lastupdate;
		node.data.conspicuity = modelTree[key].conspicuity;
		importTree(modelTree[key].children, tree);
	}
}

function refreshModelTree(data) {
	modelTree = data;

	var tree = $('#treetable').fancytree('getTree');
	var node = tree.getActiveNode();
	//var lastUpdate = node.data.lastupdate;
	
	importTree(modelTree, tree);

	//Render the tree
	tree.render(true, true);

	//Re-select the active node
	//if (lastUpdate != node.data.lastupdate) {
		doNodeSelection(node.title);
	//}
}

var autorefreshId = -1;

function dataRefresh() {
	$.getJSON("getclasses.php?heartbeaturl=" + heartbeaturl, refreshModelTree);
}

function autorefreshOnOff() {
	var button = $("#autorefresh");
	if ( button.html() == 'Off' ) {
		button.removeClass('btn-danger').addClass('btn-success').addClass('pulse-success').html("On");
		autorefreshId = window.setInterval(dataRefresh, 2000);
	} else {
		window.clearInterval(autorefreshId);
		button.removeClass('btn-success').addClass('btn-danger').removeClass('pulse-success').html("Off");
	}
}

//Page load
$(function() {
	$("#zonename").html(zone);

	//------------------- treetable ---------------------
	var tree = $('#treetable').fancytree({
		autoScroll: true,
		extensions: ["table"],
		selectMode: 1,
		minExpandLevel: 2,
		table: {
			checkboxColumnIdx: 0,   // render the checkboxes into the this column index (default: nodeColumnIdx)
			nodeColumnIdx: 0        // render node expander, icon, and title to this column (default: #0)
		},
		renderColumns: function(event, data) {
			var node = data.node;
			$tdList = $(node.tr).find(">td");
			var conspicuity =  node.data.conspicuity;
			$tdList.eq(1).html('<span class="badge conspicuity-' + conspicuity + '">' + node.data.tags[0] + '</span>');
		},
		renderNode: function(event, data) {
			// Optionally tweak data.node.span
			var node = data.node;
			if (node.data.usericon) {
				var $span = $(node.span);
				$span.find("> span.fancytree-icon").css({
					//border: "1px solid green",
					backgroundImage: 'url(' + node.data.usericon + ')',
					backgroundPosition: "0 0",
					width: "24px",
					height:"24px", 
					'background-size': 'contain'
				});
			}
		},
		activate: function(event, data) {
			var node = data.node;
			doNodeSelection(node.title);
		},
		init: function(event, data, flag) {
			var node = data.tree.getRootNode();
			node.getFirstChild().setActive();
		},
		source : {url: "getclasses.php?heartbeaturl=" + heartbeaturl}
	});

	//------------------- servicetable ---------------------
	var pageScrollPos = 0;
	var exceptiontable = $('#servicetable').DataTable( {
		dom: 'frtp',
		select: true,
		//scrollY: 300,
		scrollY:'89vh',
		scrollCollapse: true,
		paging: false,
		order: [[ 1, "desc" ]], // by default all services are ordered by context
		ajax: {
			url: 'getservices.php?heartbeaturl=' + heartbeaturl,
			dataSrc: ""
		},
		preDrawCallback: function (settings) {
			pageScrollPos = $('div.dataTables_scrollBody').scrollTop();
		},
		drawCallback: function (settings) {
			$('div.dataTables_scrollBody').scrollTop(pageScrollPos);
		},
		columnDefs: [
			{ targets: 4, orderData: 7},
			{ targets: 5, visible: false}, // column containing lid
			{ targets: 6, visible: false}, // column containing uuid
			{ targets: 7, visible: false}  // column containing age
		],
		columns: [
			{ data: 'class',
				render: function ( data, type, row ) {
					return '<a href="' + row["context"] + '/urn:xdaq-application:lid=' + row["id"] + '">' + data + '</a>';
				}
			},
			{ data: 'context',
				render: function ( data, type, row ) {
					return '<a href="' + data + '">' + data + '</a>';
				}
			},
			{ data: 'service' },
			{ data: 'group' },
			{ data: 'progress',
				render: function ( data, type, row ) {
					if ( data < 0.5 ) {
						conspiquity = "progress-bar-success";
					} else if ( data < 1.0 ) {
						conspiquity = "progress-bar-warning"
					} else {
						conspiquity = "progress-bar-danger";
					}
					return '<span><div class="progress" style="height: 10px; margin: 0px 0px 0px 0px;"><div class="progress-bar ' + conspiquity + '" style="width:' + data * 100 + '%"/></div></span>';
				}
			},
			{ data: 'id' },
			{ data: 'uuid' },
			{ data: 'age'}
		],
		rowId: 'uuid'
	});

	//Reload the tree every 2 seconds
	//autorefreshId = window.setInterval(dataRefresh, 2000);
});

</script>

</head>
<body>


	<div class="container-fluid fullview">

		<nav class="navbar navbar-default navbar-xs">
				<div class="navbar-header" align="left">
					<a class="navbar-brand" href="#">
						<div align="bottom">
							<img alt="Speedial5" src="images/telephone24.png"> <img alt="Speedial5">
						</div>
					</a>
				</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">				
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" id="zonename" style="color:green;"></a></li>
				</ul>
				<button data-toggle="tooltip" title="Enable/disable autorefresh" id="autorefresh" onclick="autorefreshOnOff()" type="button" class="btn btn-danger navbar-btn navbar-right btn-xs">Off</button>
			</div>
		</nav>
		<div class="col-sm-3 col-md-3 col-lg-3 sidenav">
			<div class="panel panel-default">
				<table id="treetable" class="table table-hover fancytree-fade-expander table-borderless">
					<colgroup>
						<col width="80px"></col>
						<col width="10px"></col>
					</colgroup>
					<tbody>
						<tr>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-sm-9 col-md-9 col-lg-9" id="rightnav">
			<div class="row">
				<div>
					<table id="servicetable" class="display compact nowrap"
						cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>Class</th>
								<th>Context</th>
								<th>Service</th>
								<th>Group</th>
								<th>Age</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>

 <!-- Modal -->
  <div class="modal fade" id="invalidselection" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invalid Selection</h4>
        </div>
        <div class="modal-body">
          <p>Select one or more exception to delete</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
	<!--  footer class="container-fluid">
		<p>Footer Text</p>
	</footer -->

</body>
</html>
