package net.hep.cms.xdaqctl.xdata;

/**
 * Simple XData items which can be mapped to Java objects.
 * <p/>
 * Java <tt>xdaqctl.xdata.</tt>class names are same as corresponding
 * C++ xdata:: class names.
 * Java object classes used in <tt>getValue()/setValue()</tt> methods
 * are listed in the table below.
 * <p/>
 * <table>
 * <tr><th>XData/xdaqctl.xdata</th><th>Java</th></tr>
 * <tr><td>Boolean</td>           <td>Boolean</td></tr>
 * <tr><td>Integer</td>           <td>Integer</td></tr>
 * <tr><td>Integer32</td>         <td>Integer</td></tr>
 * <tr><td>Integer64</td>         <td>Long</td></tr>
 * <tr><td>UnsignedInteger</td>   <td>Long</td></tr>
 * <tr><td>UnsignedInteger32</td> <td>Long</td></tr>
 * <tr><td>UnsignedInteger64</td> <td>Long</td></tr>
 * <tr><td>UnsignedShort</td>     <td>Integer</td></tr>
 * <tr><td>UnsignedLong</td>      <td>Long</td></tr>
 * <tr><td>Float</td>             <td>Float</td></tr>
 * <tr><td>Double</td>            <td>Double</td></tr>
 * <tr><td>String</td>            <td>String</td></tr>
 * <tr><td>TimeVal</td>           <td>Date</td></tr>
 * </table>
 * <p/>
 * <small>(Note: All Java classes are in <tt>java.lang</tt> package,
 * except <tt>java.util.Date</tt>.)</small>
 *
 * @author A. Oh &lt;Alexander.Oh@cern.ch&gt; and I. Suzuki &lt;ichiro@fnal.gov&gt;
 * @version $Name:  $
 */

public abstract class SimpleItem extends XDataItem {
	/**
	 * @return internal Java object, as listed in the table above.
	 */
	public abstract Object getValue();

	/**
	 * @param value Java object/literal to set the value.
	 * @throws ClassCastException
	 *     When <tt>value</tt> type can not be converted to
	 *     the internal Java representation.
	 */
	public abstract void setValue(Object value);
}

// End of file
// vim: set sw=4 ts=4:
