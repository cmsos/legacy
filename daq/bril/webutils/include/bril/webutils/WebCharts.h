/**
 * @brief Webpage utility functions and highcharts based monitoring.
 *
 * @author N. Tosi \<nicolo.tosi@cern.ch\>
 *
 * This package includes various utility functions to create tables and plots in xdaq webpages.
 */

#ifndef _bril_webutils_WebCharts_h_
#define _bril_webutils_WebCharts_h_

#include <vector>
#include "bril/webutils/WebHelpers.h"
#include "cgicc/Cgicc.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Output.h"
#include "bril/webutils/exception/Exception.h"

namespace bril
 {

  namespace webutils
   {

    class WebChart
     {

      public:
      struct series_properties
       {
        std::string color;
        std::string marker;
        series_properties( std::string c = "", std::string m = "" ) : color( c ), marker( m ) {}
       };

      typedef std::map<std::string, series_properties> property_map;

      typedef std::map<std::string, std::string> type_map;

      public:
      WebChart( std::string, std::string );

      WebChart( std::string, std::string, std::string, std::string, const property_map& );

      void writeChart( xgi::Output*, const std::string& = "height:400px; width:100%;" ) const;

      template <typename Data>
      void writeDataForQuery( const cgicc::Cgicc&, xgi::Output*, const Data& );

      template <typename Iter>
      void writeDataForQuery( const cgicc::Cgicc&, xgi::Output*, Iter, Iter );

      void newDataReady();

      WebChart& setTitle( const std::string& );

      WebChart& setType( const std::string& );

      WebChart& setAnimation( bool );

      WebChart& setLogScale( bool );

      WebChart& setDatetime( bool );

      WebChart& setPlotOptions( const std::string& );

      WebChart& setSeries( const property_map& pm )
       {
        m_series = pm;
        return *this;
       }

      WebChart& setTypes( const type_map& tm )
       {
        m_types = tm;
        return *this;
       }

      static void pageHeader( xgi::Output*, bool = true );

      static void dataHeader( xgi::Output* );

      private:
      std::string makeQueryString( bool = false ) const;

      private:
      std::string m_id;
      std::string m_callback;
      std::string m_chartoptions;
      std::string m_extraoptions;
      property_map m_series;
      type_map m_types;
      bool m_has_fresh_data;

     };

/**
 * If @p query matches this chart, writes to @p out the data map @data.
 * @param data can be any std::map<std::string, Iterable>, as long as the keys match those declared in the constructor.
 */
    template <typename Data>
    void WebChart::writeDataForQuery( const cgicc::Cgicc& query, xgi::Output* out, const Data& data )
     {
      if ( query( "chart" ) != m_id ) //not this chart
        return;
      if ( query( "fnd" ) != "true" && !m_has_fresh_data )
       {
        *out << "{ \"updated\": false }";
        return; //no new data
       }
      *out << "{ \"updated\": true, \"series\" : [";
      for ( auto d = data.begin(); d != data.end(); /*++d is below*/ )
       {
        auto s = m_series.find( d->first );
        if ( s == m_series.end() )
          XCEPT_RAISE( exception::DataMismatchError, "Series name mismatch, unknown series " + d->first );
        *out << "{ \"name\": \"" << d->first << "\"";
        if ( !s->second.color.empty() )
          *out << ", \"color\": \"" << s->second.color << "\"";
        if ( !s->second.marker.empty() )
          *out << ", \"marker\": \"" << s->second.marker << "\"";
        *out << ", \"animation\": false",
        *out << ", \"data\": [ ";
        *out << to_string( d->second.begin(), d->second.end() ); //to be replaced with std::begin and std::end
        *out << " ] }";
        if ( ++d != data.end() )
          *out << ", ";
       }
      *out << "] }";
      m_has_fresh_data = false;
     }
/**
 * If @p query matches this chart, writes to @p out the data sequence defined by [@p first, @p last).
 * It there are more than one series in this chart, an exception is thrown
 */
    template <typename Iter>
    void WebChart::writeDataForQuery( const cgicc::Cgicc& query, xgi::Output* out, Iter first, Iter last )
     {
      if ( query( "chart" ) != m_id ) //not this chart
        return;
      if ( query( "fnd" ) != "true" && !m_has_fresh_data )
       {
        *out << "{ \"updated\": false }";
        return; //no new data
       }
      if ( m_series.size() != 1 )
        XCEPT_RAISE( exception::DataMismatchError, "Expected " + to_string( m_series.size() )  + " data series." );
      *out << "{ \"updated\": true, \"series\" : [";
      auto s = *m_series.begin();
      *out << "{ \"name\": \"" << s.first << "\"";
      if ( !s.second.color.empty() )
        *out << ", \"color\": \"" << s.second.color << "\"";
      if ( !s.second.marker.empty() )
        *out << ", \"marker\": \"" << s.second.marker << "\"";
      *out << ", \"animation\": false",
      *out << ", \"data\": [ ";
      *out << to_string( first, last );
      *out << " ] } ] }";
      m_has_fresh_data = false;
     }

   }

 }

#endif
