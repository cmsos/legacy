#ifndef _tcds_hwlayertca_version_h_
#define _tcds_hwlayertca_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSHWLAYERTCA_VERSION_MAJOR 3
#define TCDSHWLAYERTCA_VERSION_MINOR 13
#define TCDSHWLAYERTCA_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSHWLAYERTCA_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSHWLAYERTCA_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSHWLAYERTCA_VERSION_CODE PACKAGE_VERSION_CODE(TCDSHWLAYERTCA_VERSION_MAJOR,TCDSHWLAYERTCA_VERSION_MINOR,TCDSHWLAYERTCA_VERSION_PATCH)
#ifndef TCDSHWLAYERTCA_PREVIOUS_VERSIONS
#define TCDSHWLAYERTCA_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSHWLAYERTCA_VERSION_MAJOR,TCDSHWLAYERTCA_VERSION_MINOR,TCDSHWLAYERTCA_VERSION_PATCH)
#else
#define TCDSHWLAYERTCA_FULL_VERSION_LIST TCDSHWLAYERTCA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSHWLAYERTCA_VERSION_MAJOR,TCDSHWLAYERTCA_VERSION_MINOR,TCDSHWLAYERTCA_VERSION_PATCH)
#endif

namespace tcdshwlayertca {

  const std::string package = "tcdshwlayertca";
  const std::string versions = TCDSHWLAYERTCA_FULL_VERSION_LIST;
  const std::string description = "CMS TCDS helper software.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "Part of the CMS TCDS software.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdshwlayertca

#endif
