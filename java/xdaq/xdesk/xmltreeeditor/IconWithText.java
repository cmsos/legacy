package xmltreeeditor;

import javax.swing.*;

public class IconWithText {
  private ImageIcon icon = null;
  private String text = "";
  public IconWithText(ImageIcon n_icon, String n_text) {
    icon = n_icon;
    text = n_text;
  }

  public ImageIcon getIcon() {
    return icon;
  }

  public String toString() {
    return text;
  }
}
