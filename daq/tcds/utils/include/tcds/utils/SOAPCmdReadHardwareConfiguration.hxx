#include "tcds/utils/SOAPUtils.h"

#include <string>

template <class T>
tcds::utils::SOAPCmdReadHardwareConfiguration<T>::SOAPCmdReadHardwareConfiguration(T& controller) :
  SOAPCmdBase<T>(controller, "ReadHardwareConfiguration", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdReadHardwareConfiguration<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Do what we were asked to do.
  std::string res = this->controller_.readHardwareConfiguration();

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  tcds::utils::soap::addSOAPReplyParameter(reply, "hardwareConfigurationString", res);
  return reply;
}
