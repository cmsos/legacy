<?php

    require 'vendor/autoload.php';
    
    function allEntries($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . '}';
        //echo $query;
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search?size=". $maxSources)->sendsJson()->body($query)->send();
            
        $json = json_decode($response, true);
        
        return $json;
    }
        
    function topHits($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . ',"size":0,"aggs":{"group_by_context":{"terms":{"field":"hb-key","size": ' . $maxSources . '},"aggs":{"top_metrics":{"top_hits":{ "sort" : [{"_timestamp" : { "order" : "desc"}}], "size":1}}}}}}';
        
        // echo $query;
        
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search")->sendsJson()->body($query)->send();
        
        //echo $response;
        $json = json_decode($response, true);
        
        return $json;
        
    }
    
    function topHitsRawData($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . ',"size":0,"aggs":{"group_by_context":{"terms":{"field":"hb-key","size": ' . $maxSources . '},"aggs":{"top_metrics":{"top_hits":{ "sort" : [{"_timestamp" : { "order" : "desc"}}], "size":1}}}}}}';
        
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search")->sendsJson()->body($query)->send();
        
        return $response;
        
    }
    
    function buildFilter($filter) {
        
        $query = '"query": {"filtered": {"query": {"match_all": {}}';
        
        $arrayobject = new ArrayObject($filter);
        $iterator = $arrayobject->getIterator();
        
        if ($arrayobject->count() > 0)
        {
            
            $query .=  ',"filter": {"and": [';
            
            
            while($iterator->valid()) {
                
                $val = $iterator->current();
                
                $query .='{"regexp": { "' . $iterator->key() .'": "'. $val .'"}}';
                
                $iterator->next();
                
                if ( $iterator->valid() )
                {
                    $query .= ',';
                }
                
            }
            
            $query .= ']}';
        }
        $query .= '}}';
        return $query;
        
    }
    
    function allHits($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . '}';
        //echo $query;
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search?size=". $maxSources)->sendsJson()->body($query)->send();
        
        $json = json_decode($response, true);
        
        return $json;
    }
    
    function allHitsRawData($host, $port, $index, $flash, $maxSources, $filter)
    {
        $query = '{' . buildFilter($filter) . '}';
        //echo $query;
        $response = \Httpful\Request::post( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_search?size=". $maxSources)->sendsJson()->body($query)->send();
        
        return $response;
    }
    
    function deleteMapping($host, $port, $index, $flash)
    {
        $response = \Httpful\Request::delete( "http://" . $host . ":" . $port . "/" . $index . "/" . $flash . "/_mapping")->send();
    }
    
    
    function indent($json) {
        
        $result      = '';
        $pos         = 0;
        $strLen      = strlen($json);
        $indentStr   = '  ';
        $newLine     = "\n";
        $prevChar    = '';
        $outOfQuotes = true;
        
        for ($i=0; $i<=$strLen; $i++) {
            
            // Grab the next character in the string.
            $char = substr($json, $i, 1);
            
            // Are we inside a quoted string?
            if ($char == '"' && $prevChar != '\\') {
                $outOfQuotes = !$outOfQuotes;
                
                // If this character is the end of an element,
                // output a new line and indent the next line.
            } else if(($char == '}' || $char == ']') && $outOfQuotes) {
                $result .= $newLine;
                $pos --;
                for ($j=0; $j<$pos; $j++) {
                    $result .= $indentStr;
                }
            }
            
            // Add the character to the result string.
            $result .= $char;
            
            // If the last character was the beginning of an element,
            // output a new line and indent the next line.
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                $result .= $newLine;
                if ($char == '{' || $char == '[') {
                    $pos ++;
                }
                
                for ($j = 0; $j < $pos; $j++) {
                    $result .= $indentStr;
                }
            }
            
            $prevChar = $char;
        }
        
        return $result;
    }
    
?>
