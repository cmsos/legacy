<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
<script type="text/javascript" src="js/toggle.js"></script>
</head>

<body>

<?php
    include_once ('config.php');
    require 'vendor/autoload.php';
    use Httpful\Exception;
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    
    $url_ =  $config['elasticsearchurl'] ."/_nodes";
    $response = \Httpful\Request::get($url_)->send();
    $json = json_decode($response, true);
    $first = reset($json['nodes']);

    // var_dump($first);


    //$hosts = array("pc-c2e11-34-01.cms", "pc-c2e11-35-01.cms", "pc-c2e11-36-01.cms", "pc-c2e11-37-01.cms", "pc-c2e11-38-01.cms"); 
    // get as configured in yml file through the first node extracted

    $hosts = $first['settings']['discovery']['zen']['ping']['unicast']['hosts']; 
   
    // base url for proxy E
    
    foreach($hosts as $host)
    {
    
    // build url

    $url_ =  "http://" . $host . ":9200/_nodes";


    echo $host;
    
    // perform request to ES
    $response = \Httpful\Request::get($url_)->send();
    
    // parse response into object code
    $json = json_decode($response, true);
    
    echo '<br />';
    
    $header_fields = array("host", "ip","name");
       
    // Output table in html
    echo '<table class="xdaq-table">';
    
    echo '<thead>';
    foreach($header_fields as $name)
    {
        
        echo '<th>';
        echo $name;
        echo '</th>';
    }
    echo '</thead>';
    
    echo '<tbody>';
    
    foreach ($json['nodes'] as $key => $val) {
        echo "<tr>";
        //foreach ($val ['_source'] as $key => $field) {
        foreach($header_fields as $name)
        {
            $field = $val[$name];
            echo '<td>';
            echo $field;
          
            
            echo '</td>';
            
        }
        echo "</tr>";
    }
    
    echo '</tbody>';
    echo '</table>';
    }
    
    ?>

<br>

        <form action="index.php">
                <input type="submit" value="Go back" method="get"/>
        </form>

</body>
</body>
<footer>Copyright © XDAQ 2016</footer>
</html>
