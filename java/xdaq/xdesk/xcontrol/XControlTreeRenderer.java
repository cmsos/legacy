package xdaq.xdesk.xcontrol;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import java.util.*;
import org.w3c.dom.*;
import java.net.*;

public class XControlTreeRenderer extends DefaultTreeCellRenderer
{
	public XControlTreeRenderer ()
	{
	
	}
	
	public Component getTreeCellRendererComponent (
		JTree tree,
		Object value,
		boolean sel,
		boolean expanded,
		boolean leaf,
		int row,
		boolean hasFocus )
	{
		super.getTreeCellRendererComponent (tree, value, sel, expanded, leaf, row, hasFocus);
			
		XControlTreeAdapterNode node = (XControlTreeAdapterNode)(((DefaultMutableTreeNode) value).getUserObject());
		
		String nodeName = node.domNode.getNodeName();
		
		String iconUrl = node.getAttribute ("iconUrl");
		
		if (nodeName.startsWith("Partition")) 
		{
			setIcon ( new ImageIcon ("./icons/Partition.gif"));
			//this.setText ( node.getAttribute ("url"));
		}
		if (nodeName.startsWith("Host")) 
		{
			setIcon ( new ImageIcon ("./icons/Host.gif"));
			this.setText ( node.getAttribute ("url"));
		}
		if (nodeName.startsWith("Script")) 
		{
			setIcon ( new ImageIcon ("./icons/Script.gif"));
			this.setText ( node.getAttribute ("name"));
		}
		if (nodeName.startsWith("Transport"))
		{
			setIcon ( new ImageIcon ("./icons/Transport.gif"));
			this.setText ( node.getAttribute ("class") + "(" +
					node.getAttribute ("instance") +  "," + node.getAttribute ("targetAddr")+ ")" );
		}
		if (nodeName.startsWith("Application"))
		{
			setIcon ( new ImageIcon ("./icons/Application.gif"));
			this.setText ( node.getAttribute ("class") + "(" +
					node.getAttribute ("instance") +  "," + node.getAttribute ("targetAddr")+ ")" );
		}
		if (nodeName.startsWith("Parameter"))
		{
			setIcon ( new ImageIcon ("./icons/Parameter.gif"));
			this.setText ( node.getAttribute ("name") + "(" +
					node.domNode.getFirstChild().getNodeValue() + ")" );
		}
		if (nodeName.startsWith("ClassDef"))
		{
			this.setText ( nodeName + ": " +
					node.domNode.getFirstChild().getNodeValue() + " (" +
					node.getAttribute ("id")+ ")" );
			setIcon ( new ImageIcon ("./icons/ClassDef.gif"));
		}
		if (nodeName.startsWith("TransportDef"))
		{
			this.setText ( nodeName + ": " +
					node.domNode.getFirstChild().getNodeValue() + " (" +
					node.getAttribute ("id")+ ")" );
					
			setIcon ( new ImageIcon ("./icons/TransportDef.gif"));
		}
		if (nodeName.startsWith("urlApplication"))
		{		
			setIcon ( new ImageIcon ("./icons/urlApplication.gif"));
			this.setText ( node.domNode.getFirstChild().getNodeValue() );
		}
		if (nodeName.startsWith("urlTransport"))
		{
			
			setIcon ( new ImageIcon ("./icons/urlTransport.gif"));
			this.setText ( node.domNode.getFirstChild().getNodeValue() );
		}
		if (nodeName.startsWith("Address"))
		{
			
					
			setIcon ( new ImageIcon ("./icons/Address.gif"));
			this.setText ( node.getAttribute ("type") );
		}
		// Override icon if provided by user
		//
		if ( !iconUrl.equals("") )
		{
			try {
				ImageIcon icon = new ImageIcon ( new URL ( iconUrl ) );
				setIcon ( icon );
			} catch ( MalformedURLException e )
			{
				System.out.println ("Cannot load icon: " + iconUrl);
			}
		}
		
		//tree.setRowHeight ( 50 );
		
		return this;	
	}
}
