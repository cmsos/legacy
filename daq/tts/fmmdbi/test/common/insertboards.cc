#include "FMMDBI.hh"

#include <iostream>
#include <sstream>
#include <iomanip>


int main() {

  tts::FMMDBI dbi("CMS_DAQ_HSAKULIN", "a3a3a3a3", "devdb10");

  for (int i=14;i<=60;i++) {
    std::stringstream ss;
    ss << "31001020100402000" << std::setw(2) << std::setfill('0') << i;
    std::cout << "SN=" << ss.str() << std::endl;
    dbi.insertBoard(ss.str(), "FMM");
  }
}
