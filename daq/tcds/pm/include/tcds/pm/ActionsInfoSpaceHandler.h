#ifndef _tcds_pm_ActionsInfoSpaceHandler_h_
#define _tcds_pm_ActionsInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class ActionsInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      ActionsInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::utils::InfoSpaceUpdater* updater,
                              bool const cpmMode);
      virtual ~ActionsInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      // A flag distinguishing between CPM mode (spanning 12 LPMs) and
      // LPM mode (spanning a single LPM, obviously).
      bool isCPMMode_;
      unsigned int numLPMs_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_ActionsInfoSpaceHandler_h_
