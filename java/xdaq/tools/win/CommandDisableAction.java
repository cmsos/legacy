package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import xdaq.tools.win.xdaqWin;

public class CommandDisableAction extends AbstractAction
{
	xdaqWin win_;

	public CommandDisableAction (xdaqWin win)
	{
		super ("Disable");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {
			win_.executeCommandOnSelection ("Disable", null, win_.getPaths());
		}
		catch(xdaqWinException ex )
		{
		
		}

	}
}
