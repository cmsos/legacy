// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, A. Petrucci, P. Roberts			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>
#include <string>
#include <iostream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"
#include "toolbox/net/URN.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "es/xtreme/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "es/xtreme/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xmas/exception/Exception.h"

#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/InputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/Integer32.h"
#include "xdata/Integer64.h"
#include "xdata/TimeVal.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdata/TableIterator.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/Guard.h"

#include "xoap/DOMParserFactory.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

#include <stdio.h>
#include <curl/curl.h>

#include "es/api/Cluster.h"
#include "es/api/Stream.h"

XDAQ_INSTANTIATOR_IMPL (es::xtreme::Application);

XERCES_CPP_NAMESPACE_USE

es::xtreme::Application::FlashListDefinition::FlashListDefinition(DOMDocument* doc, const std::string & href): xmas::FlashListDefinition(0)
{
	this->setProperty("location", href); // must be url#qname
	this->setProperty ("local", "false");
	this->parse(doc, href);
}

es::xtreme::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL, false), adispatcher_("xtreme-dispatcher", "waiting", 0.8, 16)
{

	b2in::nub::bind(this, &es::xtreme::Application::onMessage);
	adispatcher_.addActionListener(this);
	toolbox::task::getWorkLoopFactory()->getWorkLoop("xtreme-dispatcher", "waiting")->activate();

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/es/xtreme/images/elasticsearch.png");

	sampleTime_ = "PT5S";
	getApplicationInfoSpace()->fireItemAvailable("sampleTime",&sampleTime_);

	enableESCloud_ = true;
	this->getApplicationInfoSpace()->fireItemAvailable("enableESCloud", &enableESCloud_);

	enableESIndexOperation_ = true;
	this->getApplicationInfoSpace()->fireItemAvailable("enableESIndexOperation", &enableESIndexOperation_);

	// optional broker properties
	autoConfSearchPath_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("autoConfSearchPath", &autoConfSearchPath_);

	elasticsearchClusterUrl_ = "unknown";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchClusterUrl", &elasticsearchClusterUrl_);

	elasticsearchFlashIndexName_ = "flashlist";
	elasticsearchShelfIndexName_ = "shelflist";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchFlashIndexName", &elasticsearchFlashIndexName_);
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchShelfIndexName", &elasticsearchShelfIndexName_);

	elasticsearchFlashIndexStoreType_ = "memory";
	elasticsearchShelfIndexStoreType_ = "niofs";
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchFlashIndexStoreType", &elasticsearchFlashIndexStoreType_);
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchShelfIndexStoreType", &elasticsearchShelfIndexStoreType_);

	elasticsearchConnectionForbidReuse_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("elasticsearchConnectionForbidReuse", &elasticsearchConnectionForbidReuse_);

	httpVerbose_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("httpVerbose", &httpVerbose_);

	tcpNoDelay_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("tcpNoDelay", &tcpNoDelay_);

	numberOfChannels_ = "2";
	this->getApplicationInfoSpace()->fireItemAvailable("numberOfChannels", &numberOfChannels_);

	ttl_ = "60s";
	this->getApplicationInfoSpace()->fireItemAvailable("ttl", &ttl_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &es::xtreme::Application::Default, "Default");

	xgi::bind(this,  &es::xtreme::Application::displayFlashlist, "displayFlashlist");
	xgi::bind(this,  &es::xtreme::Application::displayFlashlistMapping, "displayFlashlistMapping");
	//xgi::bind(this,  &es::xtreme::Application::displayLatestData, "displayLatestData");

	// easy curl opt
	xgi::bind(this,  &es::xtreme::Application::enableESCloud, "enableESCloud");
	xgi::bind(this,  &es::xtreme::Application::disableESCloud, "disableESCloud");
	xgi::bind(this,  &es::xtreme::Application::enableESIndex, "enableESIndex");
	xgi::bind(this,  &es::xtreme::Application::disableESIndex, "disableESIndex");


	//SOAP Binding
	xoap::bind(this, &es::xtreme::Application::report, "report",  xmas::NamespaceUri );

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);
}

es::xtreme::Application::~Application ()
{

}

void es::xtreme::Application::actionPerformed (xdata::Event& event)
{
	counter_ = 0;
	rate_ = 0.0;
	totalIndexOperationsCounter_ = 0;
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		indexCreated_ = false;
		toolbox::Properties properties;
		if ( httpVerbose_ )
		{
			properties.setProperty("urn:es-api-stream:CURLOPT_VERBOSE","true");
		}
		if ( tcpNoDelay_ )
		{
			properties.setProperty("urn:es-api-stream:CURLOPT_TCP_NODELAY","true");
		}
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Attaching to elastic search...");
		if ((bool)(elasticsearchConnectionForbidReuse_))
		{
			properties.setProperty("urn:es-api-stream:CURLOPT_FORBID_REUSE", "true");
		}

		properties.setProperty("urn:es-api-cluster:number-of-channels", numberOfChannels_.toString() );

		member_ = new es::api::Member(this,properties);
		if ( enableESCloud_ )
		{
		 		this->applyCollectorSettings();
		}

	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(es::xtreme::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void es::xtreme::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string timerTaskName = e.getTimerTask()->name;
	if (timerTaskName == "urn:es:CheckRateTimer-task")
	{
		toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
		double delta = (double)now - (double)lastTime_;
		rate_ = (double)(counter_ / delta);
		totalIndexOperationsCounter_ = totalIndexOperationsCounter_ + counter_;
		counter_ = 0;
		lastTime_ =  now;

		//es::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
		//cluster.showStats();
	}
}

void es::xtreme::Application::onMessage (toolbox::mem::Reference * ref, xdata::Properties & plist) 
{
	es::xtreme::Event * ep = new es::xtreme::Event(plist, ref);
	toolbox::task::EventReference e(ep);

	std::string qname = plist.getProperty("urn:xmas-flashlist:name");
	try
	{
		// deliver to samplers
		if ( qname != "" )
			adispatcher_.fireEvent(e);
		else
		{
			if ( ref != 0 )
				ref->release();
		}
	}
	catch(toolbox::task::exception::Overflow & e)
	{
		if ( ref != 0 )
			ref->release();
		lossQueueFullCounters_[qname]++;
		std::stringstream  msg;
		msg << "Failed to dispatch event report for flashlist '" << qname << "'";
		XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, msg.str(), e);
		this->notifyQualified("warning", q);
	}
	catch(toolbox::task::exception::OverThreshold & e)
	{
		if ( ref != 0 )
			ref->release();
		lossQueueFullCounters_[qname]++;
		// report is lost, keep counting
	}
	catch(toolbox::task::exception::InternalError & e)
	{
		if ( ref != 0 )
			ref->release();
		lossQueueFullCounters_[qname]++;
		std::stringstream  msg;
		msg << "Failed to dispatch event report for flashlist '" << qname << "'";
		XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
	}
}

void es::xtreme::Application::processEventData (toolbox::mem::Reference * msg, xdata::Properties & plist) 
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	if ( ! enableESCloud_ )
	{
		if (msg != 0 )
			msg->release();
		return;
	}

	if (msg == 0)
	{
		// these messages with properties only do not belong to this application ! Let's ignore them


		for (std::map<std::string, std::string, std::less<std::string> >::iterator i = plist.begin(); i != plist.end(); i++ )
		{
			//std::cout << (*i).first << " - " << (*i).second <<std::endl;
		}
		//XCEPT_DECLARE(es::xtreme::exception::Exception, q, "no data no valid monitoring work to be done, ignore it");
		//this->notifyQualified("warning",q);
		return;
	}

	// report data either direct receive from xmas::probe or through a notification  from b2in-eventing
	if ((plist.getProperty("urn:b2in-protocol:action") == "flashlist") || (plist.hasProperty("urn:xmas-flashlist:name") && (plist.getProperty("urn:b2in-eventing:action") == "notify")  ) )
	{
		std::string qname = plist.getProperty("urn:xmas-flashlist:name");
		toolbox::net::URN flashlistURN(qname);
		std::string fname = flashlistURN.getNSS();

		if ( blackFlashList_.find(qname)  != blackFlashList_.end())
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Flashlist " << qname << "is blacklisted");
			if ( msg != 0 )
				msg->release();
			return;
		}

		if (! indexAvailable_ )
		{

			json_t * flashPayload = json_pack("{s:{s:s}}","settings", "index.store.type", elasticsearchFlashIndexStoreType_.toString().c_str());

			try
			{
				this->createIndex(elasticsearchFlashIndexName_.toString(), flashPayload);
				json_decref(flashPayload);
			}
			catch(es::xtreme::exception::Exception & e)
			{
				json_decref(flashPayload);
				std::stringstream info;
				info << "Cannot validate index '" << elasticsearchFlashIndexName_.toString() << "' in ES cluster";
				XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, info.str(), e);
				this->notifyQualified("error",q);

				if ( msg != 0 )
					msg->release();
				lossCounters_[qname]++;
				this->resetActiveFlashlists();
				return;

#warning This would keep flooding  with exceptions for each received message, so we might ignore

			}

			json_t * shelfPayload = json_pack("{s:{s:s}}","settings", "index.store.type", elasticsearchShelfIndexStoreType_.toString().c_str());

			try
			{
				this->createIndex(elasticsearchShelfIndexName_.toString(), shelfPayload);
				json_decref(shelfPayload);

			}
			catch(es::xtreme::exception::Exception & e)
			{
				json_decref(shelfPayload);
				std::stringstream info;
				info << "Cannot validate index '" << elasticsearchShelfIndexName_.toString() << "' in ES cluster";
				XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, info.str(), e);
				this->notifyQualified("error",q);

				if ( msg != 0 )
					msg->release();
				lossCounters_[qname]++;
				this->resetActiveFlashlists();
				return;

#warning This would keep flooding  with exceptions for each received message, so we might ignore

			}

			indexAvailable_ = true;
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Index has been validated");
			//std::cout << "Index has been validated" << std::endl;
		}

		// check if flashlist has already been mapped
		if ( activeFlashList_.find(qname)  != activeFlashList_.end())
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "check mapping already enabled  " << activeFlashList_[qname] << " for " << qname);
			if (!activeFlashList_[qname])
			{
				try
				{

					std::string timeToLive = ttl_.toString();
					if ( ttls_.find(qname) != ttls_.end() )
					{
						timeToLive = ttls_[qname];
					}

					// alway create flashlist entry with default ttl time or with ttl time provided in xml
					this->createMapping(elasticsearchFlashIndexName_.toString(), qname, timeToLive);

					// if entry found means create the mapping also in the shelflist for persistence
					// either infinite time if ttl empty string or the ttl provide in XML
					if (shelflist_.find(qname) != shelflist_.end())
					{
						this->createMapping(elasticsearchShelfIndexName_.toString(), qname, shelflist_[qname]);
					}
				}
				catch(es::xtreme::exception::Exception & e)
				{
					std::stringstream info;
					info << "Cannot validate mapping for  '" << qname << "' in ES cluster";
					XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, info.str(), e);
					this->notifyQualified("error",q);

					if ( msg != 0 )
						msg->release();
					lossCounters_[qname]++;
					indexAvailable_ = false; // trigger reset of all actions, activeFlashList_ is also false
					this->resetActiveFlashlists();
					return;
#warning This would keep flooding  with exceptions for each received message, so we might ignore
				}
				//needs to be corrected

				activeFlashList_[qname] = true;
				LOG4CPLUS_INFO(this->getApplicationLogger(), "Mapping has been validated for " << qname);
			}

		}
		else
		{
			std::stringstream info;
			info << "Cannot validate mapping for '" << qname << "' on index '"  << elasticsearchFlashIndexName_.toString() << "', flashlist is not configure for collection (this flashlist is blacklisted)";
			XCEPT_DECLARE(es::xtreme::exception::Exception, q, info.str());
			this->notifyQualified("warning",q);
			// blacklist this name
			if ( msg != 0 )
				msg->release();
			blackFlashList_[qname] = true;
			return;

		}

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Going to publish  " << activeFlashList_[qname] << " for " << qname);

		try
		{
			//this->publishReport(msg, plist, elasticsearchFlashIndexName_.toString());
			this->publishReport((char*) msg->getDataLocation(), msg->getDataSize(), qname, elasticsearchFlashIndexName_.toString());

		}
		catch(es::xtreme::exception::Exception & e)
		{
			std::stringstream info;
			info << "Cannot publish data for  '" << qname << "' in ES cluster (flashlist)";
			XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, info.str(), e);
			this->notifyQualified("error",q);

			indexAvailable_ = false; // trigger reset of all actions
			this->resetActiveFlashlists();
			//activeFlashList_[qname] = false;

			if ( msg != 0 )
				msg->release();
			lossCounters_[qname]++;
			return;
#warning This would keep flooding  with exceptions for each received message, so we might ignore

		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Flashlist data has been indexed for " << qname);

		if (shelflist_.find(qname) != shelflist_.end())
		{
			try
			{
				//this->publishReport(msg, plist, elasticsearchFlashIndexName_.toString());
				this->publishReport((char*) msg->getDataLocation(), msg->getDataSize(), qname, elasticsearchShelfIndexName_.toString());

				successCounters_[qname]++;
			}
			catch(es::xtreme::exception::Exception & e)
			{
				std::stringstream info;
				info << "Cannot publish data for  '" << qname << "' in ES cluster (shelflist)";
				XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, info.str(), e);
				this->notifyQualified("error",q);

				indexAvailable_ = false; // trigger reset of all actions
				this->resetActiveFlashlists();
				//activeFlashList_[qname] = false;

				if ( msg != 0 )
					msg->release();
				lossCounters_[qname]++;
				return;
#warning This would keep flooding  with exceptions for each received message, so we might ignore

			}
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Shelflist data has been indexed for " << qname);
		}
	} // valid message

	if ( msg != 0 )
		msg->release();
}

void es::xtreme::Application::createIndex (const std::string & iname, json_t * args) 
{
	es::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joined cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		/*
				XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, ne, msg.str(), e);
				this->notifyQualified("fatal", ne);
				return;
		 */

		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(),e);
	}

	// check if index exists otherwise it creates

	try
	{
		if ( !cluster.exists(iname, ""))
		{
			try
			{
				//expected {"acknowledged":true}
				json_t * result = cluster.createIndexData(iname, args);
				json_t * acknowledged =json_object_get(result, "acknowledged");
				if (! json_boolean_value(acknowledged))
				{
					std::stringstream msg;
					msg << "failed to create index: '" << json_dumps(result, 0) << "'";
					json_decref(result);
					XCEPT_RAISE(es::xtreme::exception::Exception, msg.str());
				}

				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Created index : " << json_dumps(result, 0));
				json_decref(result);

			}
			catch(es::api::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "failed to create index with : '" << iname;
				XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str() ,e);
			}

		}
	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not check index:" << iname<< " exists on url: '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
	}

}

void es::xtreme::Application::createMapping(const std::string & indexName, const std::string & qname, const std::string & ttl) 
{
	toolbox::net::URN flashlistURN(qname);
	std::string fname = flashlistURN.getNSS();

	xmas::FlashListDefinition * flashlist = flashlists_[qname];

	es::api::Cluster& cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joined cluster :  " << cname);
	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url (flashlist process aborted): '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
	}

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Converting flashlist : " << qname << " to JSON mapping.");
	json_t * mapping = 0;
	try
	{
		mapping = this->flashToJSON(flashlist, ttl);
	}
	catch(es::xtreme::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not create mapping object for: " << indexName << "and flashlist: " << qname;
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
	}
	//{"error": "MergeMappingException[Merge failed with failures {[mapper [stateName] of different type, current_type [string], merged_type [integer]]}]", "status": 400}
	try
	{
		json_t * result = cluster.createMapping(indexName, fname, mapping);
		json_t * acknowledged =json_object_get(result, "acknowledged");
		if (! json_boolean_value(acknowledged))
		{

			json_t * status =json_object_get(result, "status");

			json_t * error =json_object_get(result, "error");

			std::stringstream msg;
			msg << "Could not create mapping for flashlist : '" << qname  << "' with error '" << json_dumps(error,0) << "' status result : " << json_real_value(status) << " mapping : '" << json_dumps(mapping, 0)  << "'";
			json_decref(result);
			json_decref(mapping);
			XCEPT_RAISE(es::xtreme::exception::Exception, msg.str());

		}

		/* DEBUG
				json_t * completemap = cluster.getMapping(indexName, fname);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Complete Mapping : " << json_dumps(completemap, 0));
				json_decref(completemap);
		 */
		json_decref(result);
		json_decref(mapping);

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Mapping has been actually created for " << qname);

	}
	catch(es::api::exception::Exception& nae)
	{
		std::stringstream msg;
		msg << "could not create mapping for flashlist : '" << qname  << "'";
		json_decref(mapping);
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(),nae);
	}
}

void es::xtreme::Application::publishReport (char * buffer, size_t size, const std::string & qname, const std::string & indexName) 

//void es::xtreme::Application::publishReport (toolbox::mem::Reference * msg, xdata::Properties & plist, const std::string & indexName) 
{

	//std::string name = plist.getProperty("urn:xmas-flashlist:name");
	toolbox::net::URN flashlistURN(qname);
	std::string fname = flashlistURN.getNSS();
	/*
	 * $ curl -XPUT 'http://localhost:9200/twitter/tweet/1' -d '{
	 *   "user" : "kimchy",
	 *   "post_date" : "2009-11-15T14:12:12",
	 *   "message" : "trying out Elasticsearch"
	 * }'
	 */
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "joining cluster ... ");
	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());

	/*try
	{
		std::string cname = cluster.getClusterName();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Joining cluster :  " << cname);

	}
	catch (es::api::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not join cluster on url : '" << elasticsearchClusterUrl_.toString();
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
	}*/

	//std::string indexName = elasticsearchFlashIndexName_.toString();

	xdata::Table * t = 0;
	try
	{
		t = this->getDataTable(buffer,size);
	}
	catch (es::xtreme::exception::Exception & e)
	{
		std::stringstream msg;
		msg << "could not extract data table from message for " << indexName << " with type : " << fname;
		XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
	}

//if bigger than one bulk
	//std::cout << "bulk is:" << t->getRowCount() << std::endl;

	size_t bulkSize = t->getRowCount();
	if ( bulkSize > 1 )
	{
		//do bulk
		std::stringstream bulk;

		std::vector<std::string> columns = t->getColumns();
		for (xdata::Table::iterator ti = t->begin(); ti != t->end(); ti++)
		{
			json_t * jsondata = 0;

			try
			{
				jsondata = this->tableRowToJSON(ti, columns, qname);//full qualified flashlist name
			}
			catch(es::xtreme::exception::Exception & e)
			{
				delete t;
				std::stringstream msg;
				msg << "could not create data entry for bulk: " << indexName << " with type : " << fname;
				XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
			}

			char * dataEntry = json_dumps(jsondata, 0);
			bulk << "{\"index\" : { \"_type\" : \"" << fname.c_str() << "\"}}\n";
			bulk << dataEntry << "\n";
			//add metadata
			json_decref(jsondata);
			::free(dataEntry);

		}
		//bulk the lot after
		try
		{
			if ( enableESIndexOperation_ )
			{
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Bulk Indexing data : " << bulk.str());
				json_t * result =cluster.bulkIndex(indexName, "", bulk);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Bulk Indexing data result:  " << json_dumps(result, 0));
				json_decref(result);

				bulkCounters_[qname]++;
				lastBulkSizes_[qname] = bulkSize;
				if (bulkSize > maxBulkSizes_[qname])
				{
					maxBulkSizes_[qname] = bulkSize;
				}

			}

			counter_ = counter_ + 1;
		}
		catch (es::api::exception::Exception & e)
		{
			delete t;
			std::stringstream msg;
			msg << "could not bulk index data for : " << indexName << " and type : " << fname;
			XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
		}

	}
//else carry on as usual
	else
	{
		std::vector<std::string> columns = t->getColumns();
		//ti.operator=(t.begin());
		for (xdata::Table::iterator ti = t->begin(); ti != t->end(); ti++)
		{
			//xdata::UnsignedLong * number = dynamic_cast<xdata::UnsignedLong *>((*ti).getField("Number"));
			json_t * jsondata = 0;

			try
			{
				jsondata = this->tableRowToJSON(ti, columns, qname);//full qualified flashlist name
			}
			catch(es::xtreme::exception::Exception & e)
			{
				delete t;
				std::stringstream msg;
				msg << "could not create data entry for : " << indexName << " with type : " << fname;
				XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
			}

			// inject into elasticsearch here ...

			//std::cout << "inserting data for flashlist name : " << fname << std::endl;
			//std::cout << json_dumps(jsondata, 0) << std::endl;

			//carry on as normal
			try
			{
				if ( enableESIndexOperation_ )
				{
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data : " << json_dumps(jsondata, 0));
					json_t * result =cluster.index(indexName,fname,"", jsondata);
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Indexing data result:  " << json_dumps(result, 0));
//NEEDED?
					json_t * success =json_object_get(result, "created");
					if (json_boolean_value(success))
					{
						LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Successfully indexed data :  " << json_dumps(result, 0));
					}

					json_decref(result);

					successCounters_[qname]++;
				}

				counter_ = counter_ + 1;
			}
			catch (es::api::exception::Exception & e)
			{
				delete t;
				json_decref(jsondata);
				std::stringstream msg;
				msg << "could not index data for : " << indexName << " and type : " << fname;
				XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
			}
			json_decref(jsondata);
		}
	}

	delete t;
	return;

}

json_t * es::xtreme::Application::flashToJSON (xmas::FlashListDefinition * flashlist, const std::string & timeToLive) 
{
	std::string name = flashlist->getProperty("name");
	std::string version = flashlist->getProperty("version");
	std::string key = flashlist->getProperty("key");
	toolbox::net::URN flashlistURN(name);
	std::string fname = flashlistURN.getNSS();

	json_t * jsonflash = json_object();
	json_t * jsonproperties = json_object();
	json_t * jsonitems = json_object();

	json_object_set_new( jsonflash, fname.c_str() , jsonproperties );

	json_object_set_new( jsonproperties, "properties",jsonitems);

	// { "<flashlistname>" : {
	//        "properties": {

	std::vector<xmas::ItemDefinition*> items = flashlist->getItems();

	//std::cout << " found num items in flashlist:" << items.size() << std::endl;

	for (std::vector<xmas::ItemDefinition*>::iterator i =  items.begin(); i != items.end(); i++)
	{
		//std::cout << " processing item: " <<  (*i)->getProperty("name") << std::endl;
		std::string iname = (*i)->getProperty("name");
		json_t * jsondef = 0;
		try
		{
			jsondef = this->itemToJSON(*i,name); // recursive
		}
		catch(es::xtreme::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "could not convert flashlist : " << name;
			json_decref(jsonflash);
			XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
		}
		json_object_set_new( jsonitems, iname.c_str(),jsondef);

	}

	// add meta data
	json_t * jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "string" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsonitems, "meta_unique_key",jsondef);
	jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "string" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsonitems, "meta_hash_key", jsondef);

	//custom copy_to for hash key used for quick aggregation
	jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "string" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsonitems, "flash_key", jsondef);

	// add createtime_ for replacement of deprecated _timestamp
	jsondef = json_object();
	json_object_set_new( jsondef, "type", json_string( "date" ) );
	json_object_set_new( jsondef, "store", json_boolean(true) );
	json_object_set_new( jsondef, "index", json_string( "not_analyzed" ) );
	json_object_set_new( jsondef, "format", json_string( "epoch_millis" ) );
	json_object_set_new( jsonitems, "createtime_", jsondef);
	//add _ttl

	if (timeToLive != "")
        { 
		json_t * ttl = json_object();
                json_object_set_new( ttl, "enabled" , json_boolean(true));
                json_object_set_new( ttl, "default" , json_string(timeToLive.c_str()));
                json_object_set_new( jsonproperties, "_ttl" , ttl );

	}
	// add _meta with original flashlist xdaq definition
	json_t * meta = json_object();
	json_object_set_new( meta, "id" , json_string( name.c_str() ));
	json_object_set_new( meta, "version" , json_string( version.c_str() ));
	json_object_set_new( meta, "key" , json_string( key.c_str() ));
	// build meta flashlist
	//json_t * definition = json_array();
	json_t * definition = json_object();
	for (std::vector<xmas::ItemDefinition*>::iterator i =  items.begin(); i != items.end(); i++)
		{
			//std::cout << " processing item: " <<  (*i)->getProperty("name") << std::endl;
			std::string iname = (*i)->getProperty("name");
			json_t * metadef = 0;
			try
			{
				metadef = this->itemToMETAJSON(*i,name); // recursive
			}
			catch(es::xtreme::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "could not convert meta flashlist : " << name;
				json_decref(jsonflash);
				XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
			}
			//json_array_append_new(definition, metadef);
			json_object_set_new( definition, iname.c_str(),metadef);

		}
	//
	json_object_set_new( meta, "definition" , definition);
	json_object_set_new( jsonproperties, "_meta" , meta );

/*
	if (ttl != "")
	{
		std::string timeToLive = ttl_.toString();
		if ( ttls_.find(name) != ttls_.end() )
		{
			timeToLive = ttls_[name];
		}

		json_t * ttl = json_object();
		json_object_set_new( ttl, "enabled" , json_boolean(true));
		json_object_set_new( ttl, "default" , json_string(timeToLive.c_str()));
		json_object_set_new( jsonproperties, "_ttl" , ttl );
	}
*/
	//

	/*add _timestamp*/
	 json_t * timestamp = json_object();
	 json_object_set_new( timestamp, "enabled" , json_boolean(true));
	 json_object_set_new( jsonproperties, "_timestamp" , timestamp);

	// }

	return jsonflash;
}

json_t * es::xtreme::Application::itemToJSON (xmas::ItemDefinition * itemdef, const std::string & fname) 
{
	json_t * jsondef = json_object();

	std::string name = itemdef->getProperty("name");
	std::string primitivetype = itemdef->getProperty("type");
	std::string index = "not_analyzed";
	if ( itemdef->hasProperty("es:index"))
	{
		index = itemdef->getProperty("es:index");
	}
	bool store = true;
	if ( itemdef->hasProperty("es:store"))
	{
		if (itemdef->getProperty("es:store") == "false")
		{
			store = false;
		}
	}
	
	std::vector<xmas::ItemDefinition*> defs = itemdef->getItems();

	// sub properties
	if (defs.size() > 0 )
	{
		json_t * jsonitems = json_object();
		// { "<name>" : {
		//        "properties": {
		for (std::vector<xmas::ItemDefinition*>::iterator i =  defs.begin(); i != defs.end(); i++)
		{
				std::string iname = (*i)->getProperty("name");
				json_t * jsonitem = this->itemToJSON(*i,""); // recursive
				json_object_set_new( jsonitems, iname.c_str(),jsonitem);
		}
		// }
		json_object_set_new( jsondef, "properties",jsonitems);
	}
	else
	{
		try
		{
			std::string elasticsearchType = this->xdaqToElasticsearchType(primitivetype.c_str() );
			// { "<name>" : { "type" : "primitivetype", ... }
			json_object_set_new( jsondef, "type", json_string( elasticsearchType.c_str() ) );
			json_object_set_new( jsondef, "store", json_boolean(store) );
			json_object_set_new( jsondef, "index", json_string( index.c_str() ) );


			std::list<std::string> copy_to_list;
			if ( unique_keys_.find(fname) != unique_keys_.end() )
			{
				std::set<std::string> & key = unique_keys_[fname];
				if (key.find(name)  != key.end()) // valid only for first level flashlist items
				{
					copy_to_list.push_back("meta_unique_key");
				}
			}

			if ( hash_keys_.find(fname) != hash_keys_.end() )
			{
				std::set<std::string> & key = hash_keys_[fname];
				if (key.find(name)  != key.end()) // valid only for first level flashlist items
				{
					copy_to_list.push_back("meta_hash_key");
				}
			}
			if (copy_to_list.size() > 0  )
			{
				json_t * jsonvector = json_array();
				for (std::list<std::string>::iterator i = copy_to_list.begin(); i != copy_to_list.end(); i++)
				{
					//std::cout << "adding copy_to for " << name <<  "to " <<  (*i) << std::endl;
					json_array_append_new(jsonvector, json_string((*i).c_str()));
				}
				json_object_set_new( jsondef, "copy_to", jsonvector );
			}

			if (elasticsearchType == "date")
			{
				json_object_set_new( jsondef, "format", json_string("yyyy-MM-dd'T'HH:mm:ss'Z'") );
			}

		}
		catch (es::xtreme::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "failed to build item : "<< name << " and type : " << primitivetype;
			json_decref(jsondef);
			XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
		}
	}

	return jsondef;
}

json_t * es::xtreme::Application::itemToMETAJSON (xmas::ItemDefinition * itemdef, const std::string & fname) 
{
	json_t * jsondef = json_object();

	std::string name = itemdef->getProperty("name");
	std::string primitivetype = itemdef->getProperty("type");


	std::vector<xmas::ItemDefinition*> defs = itemdef->getItems();

	// sub properties
	if (defs.size() > 0 )
	{
		json_t * jsonitems = json_object();
		// { "name" : {
		//        "definition": [
		for (std::vector<xmas::ItemDefinition*>::iterator i =  defs.begin(); i != defs.end(); i++)
		{
				std::string iname = (*i)->getProperty("name");
				json_t * jsonitem = this->itemToMETAJSON(*i,""); // recursive
				json_object_set_new( jsonitems, iname.c_str(),jsonitem);
		}
		// ]
		json_object_set_new( jsondef, "definition",jsonitems);

	}

	std::vector<std::string> plist = itemdef->propertyNames();
	for (std::vector<std::string>::iterator i =  plist.begin(); i != plist.end(); i++)
	{
		std::string pvalue = itemdef->getProperty(*i);
		try
		{
			// { "name" :  "type"  }
			json_object_set_new( jsondef, (*i).c_str(), json_string( pvalue.c_str()  ) );

		}
		catch (es::xtreme::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "failed to build META item : "<< name << " and type : " << primitivetype;
			json_decref(jsondef);
			XCEPT_RETHROW(es::xtreme::exception::Exception, msg.str(), e);
		}
	}


	return jsondef;
}

json_t * es::xtreme::Application::tableRowToJSON(xdata::Table::iterator & ti, std::vector<std::string> & columns, const std::string & name) 
{
	json_t * document = json_object();

	for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
	{

		//std::string localName = columns[k].substr(columns[k].rfind(":")+1);
		std::string localName = columns[k];

		//std::cout << "Printing the column : " << columns[k] << std::endl;
		//xdata::Serializable * s = t->getValueAt(j, columns[k]);

		xdata::Serializable * s = (*ti).getField(columns[k]);

		if (s->type() == "string")
		{
			std::string value = s->toString();
			json_object_set_new( document, localName.c_str(), json_string( value.c_str()) );
		}
		else if (s->type() == "bool")
		{
			xdata::Boolean* b = dynamic_cast<xdata::Boolean*>(s);
			json_object_set_new( document, localName.c_str(), json_boolean((xdata::BooleanT)*b) );
		}
		else if ( s->type() == "int" )
		{
			xdata::Integer* i = dynamic_cast<xdata::Integer*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::IntegerT)*i) );
		}
		else if ( s->type() == "int 32" )//REMI
		{
			xdata::Integer32* i = dynamic_cast<xdata::Integer32*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::Integer32T)*i) );
		}
		else if ( s->type() == "unsigned int" )
		{
			xdata::UnsignedInteger* i = dynamic_cast<xdata::UnsignedInteger*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedIntegerT)*i) );
		}
		else if ( s->type() == "unsigned int 32" )
		{
			xdata::UnsignedInteger32* i = dynamic_cast<xdata::UnsignedInteger32*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i) );
		}
		else if ( s->type() == "unsigned int 64" )
		{
			xdata::UnsignedInteger64* i = dynamic_cast<xdata::UnsignedInteger64*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i) );
		}
		else if ( s->type() == "unsigned long" )
		{
			xdata::UnsignedLong* i = dynamic_cast<xdata::UnsignedLong*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedLongT)*i) );
		}
		else if ( s->type() == "unsigned short" )
		{
			xdata::UnsignedShort* i = dynamic_cast<xdata::UnsignedShort*>(s);
			json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedShortT)*i) );
		}
		//json_integer needs to change!
		else if ( s->type() == "time" )
		{
			xdata::TimeVal* i = dynamic_cast<xdata::TimeVal*>(s);
			std::string formattedTime = i->value_.toString("%FT%H:%M:%SZ", toolbox::TimeVal::gmt);
			json_object_set_new( document, localName.c_str(), json_string(formattedTime.c_str()) );
		}
		else if ( s->type() == "double" )
		{
			xdata::Double* i = dynamic_cast<xdata::Double*>(s);
			json_object_set_new( document, localName.c_str(), json_real((xdata::DoubleT)*i) );
		}
		else if ( s->type() == "float" )
		{
			xdata::Float* i = dynamic_cast<xdata::Float*>(s);
			json_object_set_new( document, localName.c_str(), json_real((xdata::FloatT)*i) );
		}
		else if (s->type().find("vector") != std::string::npos )
		{
			xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(s);

			if ( v->getElementType() == "unsigned int 32" )
			{
				xdata::Vector<xdata::UnsignedInteger32> * v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(s);

				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::UnsignedInteger32>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger32T)*i) );
					json_t * val = json_integer(((xdata::UnsignedInteger32T)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "int 32" )
			{
				xdata::Vector<xdata::Integer32> * v = dynamic_cast<xdata::Vector<xdata::Integer32>*>(s);

				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::Integer32>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_integer((xdata::Integer32T)*i) );
					json_t * val = json_integer(((xdata::Integer32T)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "int" )
			{
				xdata::Vector<xdata::Integer> * v = dynamic_cast<xdata::Vector<xdata::Integer>*>(s);

				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::Integer>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_integer((xdata::IntegerT)*i) );
					json_t * val = json_integer(((xdata::IntegerT)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else if ( v->getElementType() == "unsigned int" )
			{
				xdata::Vector<xdata::UnsignedInteger> * v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger>*>(s);

				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::UnsignedInteger>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedIntegerT)*i) );
					json_t * val = json_integer(((xdata::UnsignedIntegerT)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(), jsonvector);
				//vector treated as primitive type
			}
			else if (  v->getElementType()  == "unsigned int 64" )
			{
				xdata::Vector<xdata::UnsignedInteger64> * v = dynamic_cast<xdata::Vector<xdata::UnsignedInteger64>*>(s);
				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::UnsignedInteger64>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_integer((xdata::UnsignedInteger64T)*i) );
					json_t * val = json_integer(((xdata::UnsignedInteger64T)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else if (  v->getElementType()  == "float" )
			{
				xdata::Vector<xdata::Float> * v = dynamic_cast<xdata::Vector<xdata::Float>*>(s);
				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::Float>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_real((xdata::FloatT)*i) );
					json_t * val = json_real(((xdata::FloatT)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else if (  v->getElementType()  == "bool" )
			{
				xdata::Vector<xdata::Boolean> * v = dynamic_cast<xdata::Vector<xdata::Boolean>*>(s);
				json_t * jsonvector = json_array();
				for ( xdata::Vector<xdata::Boolean>::iterator i = v->begin(); i != v->end(); i++ )
				{
					json_object_set_new( document, localName.c_str(), json_boolean((xdata::BooleanT)*i) );
					json_t * val = json_boolean(((xdata::BooleanT)*i));
					json_array_append_new(jsonvector, val);
				}
				json_object_set_new(document, localName.c_str(),jsonvector);
				//vector treated as primitive type
			}
			else
			{
				XCEPT_RAISE (es::xtreme::exception::Exception, "Failed to convert data entry,  unsupported vector type as "+ v->getElementType()  );
			}
		}
		else if (s->type() == "table")
		{
			xdata::Table * st = dynamic_cast<xdata::Table*>(s);
			std::vector<std::string> cols = st->getColumns();
			json_t * jsontable = json_array();
			for (xdata::Table::iterator sti = st->begin(); sti != st->end(); sti++)
			{
				try
				{
					json_t * jsonrow  = this->tableRowToJSON( sti, cols, "");
					//tablename

					json_array_append_new(jsontable, jsonrow);
				}
				catch(es::xtreme::exception::Exception & e)
				{
					json_decref(document);
					XCEPT_RETHROW (es::xtreme::exception::Exception, "Failed to convert inner data table,  xdata to es type as "+s->type(), e );
				}

			}
			json_object_set_new(document, localName.c_str(),jsontable);
		}
		else
		{
			json_decref(document);
			XCEPT_RAISE (es::xtreme::exception::Exception, "Failed to convert data entry,  xdata to es type as "+s->type() );
		}
	}

	//adding the flash key
	if ( hash_keys_.find(name) != hash_keys_.end() )
	{
		std::string flashkeyValue = "@";
		std::set<std::string> & key = hash_keys_[name];
		for (std::set<std::string>::iterator i = key.begin(); i != key.end(); i++) // valid only for first level flashlist items
		{
			xdata::Serializable * s = (*ti).getField(*i);
			flashkeyValue += s->toString() + "-";
		}
		json_object_set_new( document,"flash_key", json_string( flashkeyValue.c_str()) );

	}

	// insert createtime_
	toolbox::TimeVal createtime = toolbox::TimeVal::gettimeofday();
	uint64_t millis = ((uint64_t)(createtime.sec()) * (uint64_t)1000) + createtime.millisec();
	json_object_set_new( document,"createtime_", json_integer(millis) );


	return document;
}
//*out << cgicc::td(s->toString()).set("style","vertical-align: top;");
//xdata::Mime* m = dynamic_cast<xdata::Mime*>(s);

xdata::Table* es::xtreme::Application::getDataTable(char * buffer, size_t size) 

//xdata::Table* es::xtreme::Application::getDataTable(toolbox::mem::Reference * msg) 
{
	//xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buffer, size);

	xdata::Table * t = new xdata::Table();

	try
	{
		serializer_.import(t, &inBuffer);
		return t;
	}
	catch (xdata::exception::Exception& e)
	{
		delete t;
		XCEPT_RETHROW (es::xtreme::exception::Exception, "Failed to deserialize flashlist table", e);
	}

}

std::string es::xtreme::Application::xdaqToElasticsearchType(const std::string & type) 
{
	if ( type == "string")
		return "string";
	else if ( type == "bool" )
	{
		return "boolean";
	}
	else if ( type == "int" )
	{
		return "integer";
	}
	else if ( type == "int 32" )//remi
	{
		return "integer";
	}
	else if ( type == "unsigned int" )
	{
		return "long";
	}
	else if ( type == "unsigned int 32" )
	{
		return "long";
	}
	else if ( type == "unsigned int 64" )
	{
		return "long";
	}
	else if ( type == "unsigned long" )
	{
		return "long";
	}
	else if ( type == "unsigned short" )
	{
		return "integer";
	}
	else if ( type == "time" )
	{
		return "date";
	}
	/*this is resolved by list of sub items directly
	 * else if ( type == "table" )
	{
		return "string";
	}*/
	else if ( type == "float" )
	{
			return "float";
	}
	else if ( type == "double" )
	{
		return "double";
	}
	else if ( type == "vector unsigned int 32" )
	{
		return "long";
	}
	else if ( type == "vector unsigned int" )
	{
		return "long";
	}
	else if ( type == "vector int 32" )
	{
		return "integer";
	}
	else if ( type == "vector int" )
	{
		return "integer";
	}
	else if ( type == "vector unsigned int 64" )
	{
		return "long";
	}
	else if ( type == "vector float" )
	{
		return "float";
	}
	else if ( type == "vector bool" )
	{
		return "bool";
	}
	else
	{
		XCEPT_RAISE (es::xtreme::exception::Exception, "Failed to convert type xdata to es type as "+ type );

	}

}

void es::xtreme::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "xdaq::EndpointAvailableEvent")
	{

	}
	else if (event.type() == "urn:es-xtreme:Event")
	{
		// event contains flashlist and data
		es::xtreme::Event& me = dynamic_cast<es::xtreme::Event&>(event);
		this->processEventData(me.ref_, me.plist_);

	}
}

void es::xtreme::Application::resetActiveFlashlists()
{
	for (std::map<std::string, bool>::iterator i =  activeFlashList_.begin(); i != activeFlashList_.end(); i++)
	{
		(*i).second = false;
	}
}

void es::xtreme::Application::loadFlashlistDefinition ( const std::string & href, const std::string & qname)
{

	DOMDocument* doc = 0;
	try
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "load flashlist definition from file  '" << href << "'");
		//std::string filename = "file://" + href;
		//std::cout << "Load XML " << filename << std::endl;
		doc = xoap::getDOMParserFactory()->get("configure")->loadXML(href);
		if ( doc == 0 )
		{
			std::stringstream msg;
			msg << "Failed to load document ( null document) : '" << href << "'";
			XCEPT_DECLARE(es::xtreme::exception::Exception, q, msg.str());
			this->notifyQualified("error", q);
			return;

		}
		/* debug
		else
		{
			std::cout << "Dump tree" << std::endl;
			std::string dump;
			xoap::dumpTree(doc->getDocumentElement(),dump);
			std::cout << "--" << dump  << "--" << std::endl;
		}
		*/


		xmas::FlashListDefinition * flashlist = new es::xtreme::Application::FlashListDefinition(doc, href + "#" + qname );

		std::string qname = flashlist->getProperty("name");
		std::set<std::string> key = toolbox::parseTokenSet(flashlist->getProperty("key"), ",");

		/*debug
				//std::cout << "found meta_unique_key ";
				for (std::set<std::string>::iterator w = key.begin(); w  != key.end(); w++)
				{
					std::cout << (*w) << ","	;
				}
				std::cout << std::endl;*/
		//debug

		if ( flashlists_.find(qname) == flashlists_.end())
		{
			successCounters_[qname] = 0;
			lossCounters_[qname] = 0;
			lossQueueFullCounters_[qname] = 0;
			bulkCounters_[qname] = 0;
			lastBulkSizes_[qname] = 0;
			maxBulkSizes_[qname] = 0;

			LOG4CPLUS_INFO(this->getApplicationLogger(), "Installed flashlist '" << qname << "'");
			flashlists_[qname] = flashlist;
			activeFlashList_[qname] = false;
			unique_keys_[qname] = key;
		}
		else
		{
			std::stringstream msg;
			msg << "Failed to create flashlist entry ( already existing) : '" << qname << "'";
			XCEPT_DECLARE(es::xtreme::exception::Exception, q, msg.str());
			this->notifyQualified("error", q);
		}
		doc->release();
		doc = 0;
	}
	catch (xoap::exception::Exception& e)
	{
		if (doc != 0) doc->release();
		std::stringstream msg;
		msg << "Failed to load flashlist file from '" << href << "'";
		XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, msg.str(), e);
		//std::cout << xcept::stdformat_exception_history(q) << std::endl;;
		this->notifyQualified("error", q);
	}
	catch (xmas::exception::Exception& e)
	{
		// this is a parse error
		if (doc != 0) doc->release();
		std::stringstream msg;
		msg << "Failed to parse configuration from '" << href << "'";
		XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, msg.str(), e);
		//std::cout << xcept::stdformat_exception_history(q) << std::endl;
		this->notifyQualified("error", q);
	}

}

void es::xtreme::Application::applyStoreSettings (const std::string & href)
{
	DOMDocument* doc = 0;
	try
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "apply store setting for file  '" << href << "'");
		doc = xoap::getDOMParserFactory()->get("configure")->loadXML(href);
		DOMNodeList* flashlistList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flash"));
		for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
		{
			DOMNode * flashlistNode = flashlistList->item(i);

			if (xoap::getNodeAttribute(flashlistNode, "xlink:type") == "locator")
			{
				std::string fhref = xoap::getNodeAttribute(flashlistNode, "xlink:href");

				std::size_t pos = fhref.find ("#urn:");


				if ( pos != std::string::npos)
				{
					std::string qname = fhref.substr(pos+1);
					toolbox::net::URN flashlistURN(qname);
					std::string fname = flashlistURN.getNSS();
					shelflist_[qname] = "";

					if ( ((DOMElement*)flashlistNode)->hasAttributeNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2015/escaped"),xoap::XStr("ttl")))
					{

						shelflist_[qname] = xoap::XMLCh2String(((DOMElement*)flashlistNode)->getAttributeNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2015/escaped"), xoap::XStr("ttl")));
						LOG4CPLUS_INFO(this->getApplicationLogger(), "set shelflist TTL '" << shelflist_[qname] << "' for '" << qname << "'");
					}
				}
				else
				{
					if (doc != 0) doc->release();
					std::stringstream msg;
					msg << "Failed to extract flash name from href '" << href << "'";
					XCEPT_DECLARE(es::xtreme::exception::Exception, q, msg.str());
					this->notifyQualified("error", q);
					return;
				}

			}
		}
		doc->release();
		doc = 0;
	}
	catch (xoap::exception::Exception& e)
	{
		if (doc != 0) doc->release();
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Did not find any persistency settings for  '" << href << "', ignore it" );
		return;
	}

}

void es::xtreme::Application::applyCollectorSettings ()
{
	std::string searchPath = autoConfSearchPath_.toString();

	std::cout << "autoConfSearchPath " << searchPath << std::endl;
	std::vector < std::string > paths;
	try
	{
		paths = toolbox::getRuntime()->expandPathName(searchPath);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand path name '" << searchPath << "'";
		XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
		return;

	}
	if ( paths.size() != 1 )
	{
		std::stringstream msg;
		msg << "Ambiguous path name '" << searchPath << "' resolve to multiple files";
		XCEPT_DECLARE(es::xtreme::exception::Exception, q, msg.str());
		this->notifyQualified("fatal", q);
		return;
	}
	std::cout << "paths[0] " << paths[0] << std::endl;
	std::string base = paths[0];
	std::vector < std::string > files;

	// add files to be processed
	files.push_back("broker/xmas/collector.settings");

	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			std::string pathname = base + "/" +  (*j);
			LOG4CPLUS_INFO(this->getApplicationLogger(), "apply collector setting for file  '" << pathname << "'");
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(pathname);
			DOMNodeList* flashlistList = doc->getElementsByTagNameNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"), xoap::XStr("flashlist"));
			for (XMLSize_t i = 0; i < flashlistList->getLength(); i++)
			{
				DOMNode * flashlistNode = flashlistList->item(i);
				std::string qname = xoap::getNodeAttribute (flashlistNode, "name");

				// Load flash list definition
				toolbox::net::URN flashlistURN(qname);
				std::string fname = flashlistURN.getNSS();
				this->loadFlashlistDefinition(base + "/flash/" + fname + ".flash", qname );
				// apply persistency iof any
				this->applyStoreSettings (base + "/store/" + fname + ".store");
				//
				DOMNodeList* collectorList = flashlistNode->getChildNodes();
				for (XMLSize_t j = 0 ; j < collectorList->getLength() ; j++)
				{
					DOMNode * collectorNode = collectorList->item(j);
					std::string nodeName = xoap::XMLCh2String(collectorNode->getLocalName());
					if(nodeName == "collector")
					{

						if ( ((DOMElement*)collectorNode)->hasAttribute(xoap::XStr("hashkey")))
						{
							std::set<std::string> key = toolbox::parseTokenSet(xoap::getNodeAttribute (collectorNode, "hashkey"), ",");
							hash_keys_[qname] = key;
						}

						if ( ((DOMElement*)collectorNode)->hasAttributeNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2015/escaped"),xoap::XStr("ttl")))
						{

							ttls_[qname] = xoap::XMLCh2String(((DOMElement*)collectorNode)->getAttributeNS (xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2015/escaped"), xoap::XStr("ttl")));
						}
					}
				}
			}

			doc->release();
			doc = 0;
		}
		catch (xoap::exception::Exception& e)
		{
			// WE THINK, if directory services are not running, this is where it will fail. not fatal
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to load collector file from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			return;
		}
		catch (xmas::exception::Exception& e)
		{
			// this is a parse error
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to parse collector from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(es::xtreme::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			continue;
		}
	}

}

void es::xtreme::Application::clearCollectorSettings()
{
	for (std::map<std::string, xmas::FlashListDefinition*>::iterator i = flashlists_.begin(); i !=  flashlists_.end(); i++ )
	{
		if ( (*i).second != 0 )
		{
			delete (*i).second;
		} 
	}

	flashlists_.clear();

	unique_keys_.clear();
	hash_keys_.clear();
	blackFlashList_.clear();
	shelflist_.clear();

}
/*
std::vector<std::string> es::xtreme::Application::getFileListing (const std::string& directoryURL, const std::string & extension ) 
{
	std::vector < std::string > list;
	try
	{
		XMLURL xmlUrl(directoryURL.c_str());
		//BinInputStream* s = accessor->makeNew (xmlUrl);
		BinInputStream* s = xmlUrl.makeNewStream();

		if (s == 0)
		{
			std::string msg = "Failed to access ";
			msg += directoryURL;
			XCEPT_RAISE(xoap::exception::Exception, msg);
		}

		int r = 0;
		int curPos = 0;

		int bufferSize = 2048;
		XMLByte * buffer = new XMLByte[bufferSize];
		std::stringstream ss;
		while ((r = s->readBytes(buffer, bufferSize)) > 0)
		{
			int read = s->curPos() - curPos;
			curPos = s->curPos();
			ss.write((const char*) buffer, read);
		}

		delete s;
		delete[] buffer;

		while (!ss.eof())
		{
			std::string fileName;
			std::getline(ss, fileName);
			// apply blob
			// if "filename.sensorsomehtingelse" exists, we dont care extension is .something
		
			if (fileName.find(extension) != std::string::npos)
			{
				list.push_back(fileName);
			}
		}

		return list;
	}
	catch (NetAccessorException& nae)
	{
		XCEPT_RAISE(es::xtreme::exception::Exception, xoap::XMLCh2String(nae.getMessage()));
	}
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE(es::xtreme::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}
	catch (std::exception& se)
	{
		XCEPT_RAISE(es::xtreme::exception::Exception, se.what());
	}
	catch (...)
	{
		std::string msg = "Caught unknown exception during 'get' operation on url ";
		msg += directoryURL;
		XCEPT_RAISE(es::xtreme::exception::Exception, msg);
	}
}
*/
void es::xtreme::Application::displayFlashlist(xgi::Input * in, xgi::Output * out ) 
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = elasticsearchFlashIndexName_.toString();

	//LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Searching for Flashlist : " << json_dumps(completemap, 0));

	try
	{
		json_t * flashlistData = cluster.search(indexName, flashlistName, "", 0);
		*out << json_dumps(flashlistData, 0) << std::endl;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Flashlist for " << flashlistName << " has been found. ");
		json_decref(flashlistData);

	}
	catch(es::xtreme::exception::Exception & e)
	{
		XCEPT_RETHROW(es::xtreme::exception::Exception, "failed to retrieve flashlist", e);
	}

}

void es::xtreme::Application::displayFlashlistMapping(xgi::Input * in, xgi::Output * out ) 
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = elasticsearchFlashIndexName_.toString();

	try
	{
		json_t * mappingData = cluster.getMapping(indexName, flashlistName);
		*out << json_dumps(mappingData, 0) << std::endl;
		json_decref(mappingData);
	}
	catch(es::xtreme::exception::Exception & e)
	{
		XCEPT_RETHROW(es::xtreme::exception::Exception, "failed to retrieve flashlist mapping", e);
	}

}

xoap::MessageReference es::xtreme::Application::report (xoap::MessageReference msg) 
{
	// DEBUG
	//	msg->writeTo(std::cout);
	//
	DOMNodeList* bodyList = msg->getSOAPPart().getEnvelope().getBody().getDOMNode()->getChildNodes();
	std::string namespaceURI = "";
	std::string namespacePrefix = "";
	std::string commandName = "";
	for (XMLSize_t i = 0; i < bodyList->getLength(); i++)
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			namespaceURI = xoap::XMLCh2String (command->getNamespaceURI());
			namespacePrefix = xoap::XMLCh2String (command->getPrefix());
			commandName = xoap::XMLCh2String (command->getLocalName());
			break;
		}
	}

	DOMDocument * document = msg->getSOAPPart().getEnvelope().getDOM()->getOwnerDocument();
	DOMNodeList * list =  document->getElementsByTagNameNS(xoap::XStr(xmas::NamespaceUri), xoap::XStr("sample"));
	if ( list->getLength() == 0)
	{
		XCEPT_RAISE(xoap::exception::Exception, "Could not find sample element");
	}

	std::string flashListName = xoap::getNodeAttribute(list->item(0),"flashlist");
	std::string originator = xoap::getNodeAttribute(list->item(0),"originator");
	std::string tagName = xoap::getNodeAttribute(list->item(0),"tag");
	std::string version = xoap::getNodeAttribute(list->item(0),"version");

	// extract attachment with flashlist data and serialize into a table
	std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();
	std::list<xoap::AttachmentPart*>::iterator j;
	for ( j = attachments.begin(); j != attachments.end(); j++ )
	{
		if ( (*j)->getSize() == 0 )
		{
			XCEPT_RAISE(xoap::exception::Exception, "empty attachment, cannot forward flashlist");
		}
		//this->publishReport((*j)->getContent(),(*j)->getSize(), flashListName, version, tagName, originator);
		this->publishReport((char*) (*j)->getContent(), (*j)->getSize(), flashListName, elasticsearchFlashIndexName_.toString());

	}

	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName(commandName + "Response", namespacePrefix, namespaceURI);
	b.addBodyElement ( responseName );
	return reply;
}

/*
void es::xtreme::Application::displayLatestData(xgi::Input * in, xgi::Output * out ) 
{
	cgicc::Cgicc cgi(in);
	std::string flashlistName = cgi["name"]->getValue();

	es::api::Cluster & cluster = member_->joinCluster(elasticsearchClusterUrl_.toString());
	std::string indexName = elasticsearchIndexName_.toString();

	try
	{
	//WEB -> http://localhost:9500/flashlist/jobcontrol/_search?sort=timestamp:desc&size=1

		std::string properties = "sort=timestamp:desc&size=1";
		json_t * result = cluster.search(indexName, flashlistName, properties, 0);
		*out << json_dumps(result, 0) << std::endl;
		std::cout << "result  = : " << json_dumps(result, 0) << std::endl;
		json_decref(result);
	}
	catch(es::xtreme::exception::Exception & e)
	{
		XCEPT_RETHROW(es::xtreme::exception::Exception, "failed to retrieve latest document", e);
	}
}
*/

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void es::xtreme::Application::TabPanel (xgi::Output * out)
{
	*out << "<script type=\"text/javascript\" src=\"/es/xtreme/html/js/es-xtreme.js\"></script> " << std::endl;
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Flashlists\" id=\"tabPage3\">"<< std::endl;

	this->FlashlistsTabPage(out);
	*out << "</div> "<< std::endl;

	//panel-end-div
	*out << "</div>";
}

void es::xtreme::Application::StatisticsTabPage (xgi::Output * out)
{
	//Dialup
	 *out << cgicc::table().set("class","xdaq-table");
	 *out << cgicc::caption("Total Rate");

        *out << cgicc::thead();
        *out << cgicc::tr();
        *out << cgicc::th("Rate (es indexing/s)").set("class","xdaq-case").set("style", "min-width: 100px;");
        *out << cgicc::th("Total index operations").set("class","xdaq-case").set("style", "min-width: 100px;");
  	*out << cgicc::tr() << std::endl;
        *out << cgicc::thead();
        *out << cgicc::tbody();
  	*out << cgicc::tr() << std::endl;
        *out << cgicc::td(rate_.toString());
        *out << cgicc::td(totalIndexOperationsCounter_.toString());
  	*out << cgicc::tr() << std::endl;
	 *out << cgicc::tbody();
        *out << cgicc::table();

	// Per flashlist loss of reports
	*out << cgicc::table().set("class","xdaq-table");
	*out << cgicc::caption("Flashlist");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class","xdaq-case").set("style", "min-width: 100px;");
	*out << cgicc::th("Active");
	*out << cgicc::th("Index");
	*out << cgicc::th("Bulk");
	*out << cgicc::th("Latest Bulk Size");
	*out << cgicc::th("Max Bulk Size");
	*out << cgicc::th("Loss");
	*out << cgicc::th("Input Loss (queue full)");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	*out << cgicc::tbody();

	for (std::map<std::string, xmas::FlashListDefinition*>::iterator i = flashlists_.begin(); i != flashlists_.end(); i++ )
	{

			xmas::FlashListDefinition * flashlist = (*i).second;
			std::string name = flashlist->getProperty("name");
			toolbox::net::URN flashlistURN(name);
			std::string fname = flashlistURN.getNSS();

			*out << cgicc::tr() << std::endl;
			*out << cgicc::td(fname);

			*out << cgicc::td((activeFlashList_[name]) ?"true":"false");

			*out << cgicc::td(toolbox::toString("%d",successCounters_[name]));
			*out << cgicc::td(toolbox::toString("%d",bulkCounters_[name]));
			*out << cgicc::td(toolbox::toString("%d",lastBulkSizes_[name]));
			*out << cgicc::td(toolbox::toString("%d",maxBulkSizes_[name]));
			*out << cgicc::td(toolbox::toString("%d",lossCounters_[name]));
			*out << cgicc::td(toolbox::toString("%d",lossQueueFullCounters_[name]));

			*out << cgicc::tr() << std::endl;

	}

	*out << cgicc::tbody();
	*out << cgicc::table();

}

void es::xtreme::Application::FlashlistsTabPage (xgi::Output * out){

	std::stringstream baseurl;
	baseurl << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

	*out << "<div id=\"flashlist-selection\">"<< std::endl;

	*out << "<button id=\"flashlist-refresh\" data-url=\"" << baseurl.str() << "\" >Reload Flashlist</button>" << std::endl;

	*out << " <select id=\"flashlist-combo\" style=\"width:350px\"> " << std::endl;
	*out << " <option  selected=\"selected\">Select a Flashlist ... </option>" << std::endl;
	for (std::map<std::string, xmas::FlashListDefinition*>::iterator i = flashlists_.begin(); i != flashlists_.end(); i++ )
	{
		xmas::FlashListDefinition * flashlist = (*i).second;
		std::string name = flashlist->getProperty("name");
		toolbox::net::URN flashlistURN(name);
		std::string fname = flashlistURN.getNSS();
		*out << "<option>" << fname << "</option>" << std::endl;
	}
	*out << " </select>" << std::endl;

	*out << " <select id=\"display-combo\" style=\"width:350px\"> " << std::endl;
	*out << " <option selected=\"selected\">Summary</option>" << std::endl;
	*out << " <option>Mapping</option>" << std::endl;
	*out << " <option>Table Data</option>" << std::endl;
	*out << " <option>JSON Data</option>" << std::endl;
	*out << " <option>Latest Data</option>" << std::endl;
	*out << " </select>" << std::endl;

	*out << "</div> "<< std::endl;

	*out << "<div id=\"flashlist-data\">"<< std::endl;
	*out << "</div> "<< std::endl;
}

void es::xtreme::Application::Default (xgi::Input * in, xgi::Output * out) 
{

	this->TabPanel(out);

}

void es::xtreme::Application::enableESCloud (xgi::Input * in, xgi::Output * out) 
{
 	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

 	if ( ! enableESCloud_ )
 	{
 		enableESCloud_ = true;
 		this->applyCollectorSettings();
 	}
}
// allow to clear ES cluster ( mapping deletion) NB. when all clients are disabled
void es::xtreme::Application::disableESCloud (xgi::Input * in, xgi::Output * out) 
{
 	toolbox::task::Guard < toolbox::BSem > guard(mutex_);
 	if (enableESCloud_ )
 	{
 		enableESCloud_ = false;
 		this->resetActiveFlashlists();
 		this->clearCollectorSettings();
 	}
}
// test only operation , should not clear or modify current ES cluster
void es::xtreme::Application::enableESIndex (xgi::Input * in, xgi::Output * out) 
{
	enableESIndexOperation_ = true;
}
void es::xtreme::Application::disableESIndex (xgi::Input * in, xgi::Output * out) 
{
	enableESIndexOperation_ = false;
}


