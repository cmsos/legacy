$Id: README,v 4.5 2007/12/14 13:13:49 ichiro Exp $

== 0. Distribution

    RPM files are available in http://home.fnal.gov/~ichiro/xdaqctl/ .

xdaqctl-<version>-0i.noarch.rpm: Binary distribution
xdaqctl-javadoc-<version>-0i.noarch.rpm: API Javadoc
xdaqctl-<version>-0i.src.rpm: Source RPM to rebuild binary RPMs

    If you need, you can get only 'xdaqctl.jar' from the RPM.
foo> rpm2cpio xdaqctl-<version>-0i.noarch.rpm | cpio -id
foo> mv usr/share/java/xdaqctl.jar .
foo> rm -r usr

== 1. How to get the source code tree

= 1.a From the CVS

    The development CVS repository is accessible for public.

foo> cd ~/work
foo> export CVSROOT=:pserver:anonymous@isscvs.cern.ch:/local/reps/tridas
foo> cvs login
Logging in to :pserver:anonymous@isscvs.cern.ch:2401/local/reps/tridas
CVS password: 98passwd
foo> cvs co -r Release-3_3_2 -d xdaqctl TriDAS/java/xdaqctl

= 1.b From the source RPM

    You can retrieve source tree tar ball from the source RPM file.

foo> rpm -qpl xdaqctl-<version>-0i.src.rpm  # find out name of the tar file
xdaqctl-<version>.tar.gz
xdaqctl.spec
foo> rpm2cpio xdaqctl-<version>-0i.src.rpm | cpio -i xdaqctl-<version>.tar.gz
foo> tar xzf xdaqctl-<version>.tar.gz

    Here, <version> is in a format of either 1.2.3 or 1_2_3 .

== 2. Files in the source directory

README         this file
*.java         Source files of the library.
build.xml      Ant build file for xdaqctl.
xdaqctl.spec   RPM spec file
test/          Unit test code
ext/           Direcotry for external Java libraries
xctl           Sample run script for control applications
sample.java    Sample XDAQ control application
sample.xml     XDAQ configuration file used by sample.java

== 3. Library dependence

    You need to have Java SDK 1.5, Jakarta Ant and Java XML libraries.
    You first need to create symbolic links to your jar files
  in ext/ directory.  Change directory to 'ext/' and run 'create_links'
  script with an appropriate argument.  Valid arguments are 'jpackage'(default)
  'jwsdp{20,16,15,14}', 'summer02' or 'rcms30' depending
  on which XML library distribution you use.  (You may need to adjust
  'jar_dir' in the script.)

    To build and test the library, ant and junit are necessary.

== 4. Test status

= 4.a Test environment

  Linux kernel 2.6
  gcc 3.4.3
  xerces-c-2.6.0
  Log4cplus came with XDAQ V3.9
  Sun's J2SDK 1.5.0-09
  jpackage 1.6, including ant 1.6.2, junit 3.8.2 and axis 1.4
  XDAQ release-4

= 4.b Unit test
    All tests in the test classes, test/*Test.java, are passed
  using a dummy XDAQ application, text/xdaq/Test*.

= 4.c System test
    System test is performed using CMS HCAL test beam run control.

== 5. Sample application
    cd test/xdaq
    make
    (edit sample.xml to fit with your directory structure.)
    $XDAQ_ROOT/bin/linux/x86/xdaq.exe -h localhost
    javac -classpath xdaqctl.jar -d . sample.java
    ./xctl sample

== 6. Known bugs

= 6.a Limited functionality is available for container type
    parameters (bag, vector).

= 6.b No test setup to check multi-thread safety

== 7. Contact

  Alexander Oh, alexander.oh@cern.ch
  Ichiro Suzuki, ichiro@fnal.gov

