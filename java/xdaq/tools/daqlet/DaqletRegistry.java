package xdaq.tools.daqlet;

import xdaq.tools.daqlet.Daqlet;
import xdaq.tools.daqlet.DaqletClassInfo;
import xdaq.tools.daqlet.JarClassLoader;
import xdaq.tools.daqlet.DaqletList;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URLClassLoader;
import java.net.URL;
import java.lang.*;
import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;

public class DaqletRegistry
{
	DaqletInstanceInfo daqletInstances_ = null;
	JarClassLoader loader_ = null;
	DaqletClassInfo daqletClasses_ = null;
	HashMap daqletList_ = null;
	JSplitPane daqletListPane_;
	Framework framework_;
	
        // lo begin - applet compatibility
        DaqletContext daqletContext_ = null;
        // lo end
	
	public DaqletRegistry(JSplitPane daqletListPane, Framework framework)
	{
		framework_ = framework;
		daqletListPane_ = daqletListPane;
		daqletClasses_ = new DaqletClassInfo();
		daqletInstances_ = new DaqletInstanceInfo();
		daqletList_ = new HashMap();
                daqletContext_ = new DaqletContext(framework_);
	}
	
	// <Daqlet association="RU" href="http://xxx/xxx.jar" class="xdaq.pippo.ciccio" />
	
	public void loadDaqlet (Properties daqletProperty)
	{
		String archive = daqletProperty.getProperty("archive");
		String codebase = daqletProperty.getProperty("codebase");
		String code = daqletProperty.getProperty("code");
		String association = daqletProperty.getProperty("association");
		String shared = daqletProperty.getProperty("shared");
		String activation = daqletProperty.getProperty("activation");
		
		String classPath = "";
		if (archive.equals(""))
		{
			classPath = codebase+"/"+code;
		} else
		{
			classPath = codebase+"/"+archive;
		}
	
		try 
		{			
			if (loader_ == null) 
			{			
				loader_ = new JarClassLoader ( classPath );
			}
			else
			{
				loader_.addJarURL ( classPath );
			}
			
			daqletClasses_.put (daqletProperty);
			
			// iff onLoad and it is shared then initialize daqlet
			if ( activation.equals("onLoad") && shared.equals("true") ) {
				Integer daqletClassNumber = new Integer(daqletProperty.getProperty("daqletClassNumber"));
				this.initDaqlet(association, "", daqletClassNumber.intValue(), daqletProperty);
			}
			
	        } catch (java.net.MalformedURLException e)
		{
			System.out.println ("Malformed URL: " + classPath);
		}
	}

	
	// Instantiate a Daqlets for a given association. The parameter
	// instance may be "". This function will not check, if a
	// Daqlet is per instance or not. It instantiates blindly,
	// according to the parameters passed.
        protected void initDaqlet (String association, String instance, int daqletNum, Properties p)
	{
            try
            {
                 String code = p.getProperty("code");
                if (!daqletInstances_.exists (association, instance, code, daqletNum))
                {
                                    
                        Class c = loader_.loadClass (code);
				     
                        // !!! creation of Daqlet is done here !!!
                                     
                        Daqlet d = (Daqlet) c.newInstance();
                                     
                        // LO begin - compatibility with Applets
                        //System.out.println("create daqlet for "+daqletNum+"/"+association+"/"+instance+"sharing "+p.getProperty("shared"));
                        DaqletStub daqletStub = new DaqletStub( p , daqletContext_);
                        d.setStub(daqletStub);
                                     
                        // LO end 
                                     
                        daqletInstances_.put (association, instance, daqletNum, d);
                        d.setIdentifier (daqletNum+"/"+association+"/"+instance);
				     
                        d.setFramework (framework_);
				     
                        // Add framework as listener to Daqlet.
                        d.addActionListener (framework_);
                        d.init();
				     
                } 
            } 
            catch (java.lang.ClassNotFoundException e)
            {
				System.out.println ("Class not found: " + e.toString());
            }
            catch (java.lang.InstantiationException e)
            {
				System.out.println ("Instantiation for plugin failed: " + e.toString());
            } 
            catch (java.lang.IllegalAccessException e)
            {
				System.out.println ("Cannot access class for association: " + e.toString());
            }	
        }
        
        // Return a JTable with all Daqlet descriptions. The table also
	// contains pointers to the Daqlets. These are created on demand,
	// i.e. as a side-effect of retrieving this table.
	//
	public DaqletList getDaqletTable (String association, String instance)
	{
                // System.out.println("get table for association "+association+ "instance "+instance);

		// Check, if for this association/instance combination 
		// a DaqletList table has already been generated.
		DaqletList l = (DaqletList) daqletList_.get (association+instance);
		if (l != null) 
		{
                	// System.out.println("Table already existing "+association+ "instance "+instance);
			return l;
		}
		
		// Figure out Daqlet properties for this association class and check if multiple is set.
		// If not multiple, set instance to "" for retrieving.
		Vector properties = daqletClasses_.get(association);
		if (properties == null)
		{
			return null;
		}

               	// System.out.println("Table create "+association+ "instance "+instance);
		// Now create a new table to hold he Daqlet descriptions to be displayed
		DaqletList daqletListEntry = new DaqletList( daqletListPane_ );
		
		// For all Daqlet classes, retrieve the Daqlet pointers.
		for (int i = 0; i < properties.size(); i++)
		{
                
			Properties p = (Properties) properties.elementAt(i);
			String lookForInstance = new String(instance);
                        //System.out.println("daqlet properties - shared"+ p.getProperty("shared") + " instance "+instance+" daqletClassNumber "+
			//p.getProperty("daqletClassNumber"));
			
			Integer daqletClassNumber = new Integer(p.getProperty("daqletClassNumber"));
			if (p.getProperty("shared").equals("true") ) lookForInstance = "";

			// check if a daqlet has already been instantiated for this association
			Vector v = daqletInstances_.get (association, lookForInstance, daqletClassNumber.intValue());
			if (v == null) // create all of them
			{
				initDaqlet (association, lookForInstance, daqletClassNumber.intValue(), p);
				v = daqletInstances_.get (association, lookForInstance,daqletClassNumber.intValue());
			} 
			
			// Create a Table and add the Daqlets to it
			for (int j = 0; j < v.size(); j++)
			{
				Daqlet d =  (Daqlet) v.elementAt(j);
				
				// Create tree model and add data;
				daqletListEntry.add (d, p);
			}
		}
		
		// Create table, store according to association and instance, then return it.
		daqletList_.put (association + instance, daqletListEntry);
		return daqletListEntry;
	}
}
