// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "toolbox/stl.h"

size_t toolbox::stl::cifind( const std::string & str1, const std::string & str2, const std::locale& loc )
{
                        std::string::const_iterator it = std::search( str1.begin(), str1.end(),
                        str2.begin(), str2.end(), ciequal<std::string::value_type>(std::locale()) );
                        if ( it != str1.end() ) return it - str1.begin();
                        else return std::string::npos; // not found
}

