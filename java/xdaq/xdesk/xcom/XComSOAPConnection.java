package xdaq.xdesk.xcom;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;

import javax.xml.parsers.*;

import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import org.apache.log4j.*;

public class XComSOAPConnection
{
	Logger logger = Logger.getLogger (XComSOAPConnection.class);
	
	SOAPConnection con;

	public XComSOAPConnection() throws XComSOAPException
	{
		try 
		{
			SOAPConnectionFactory scf =
				SOAPConnectionFactory.newInstance();
			con = scf.createConnection();
		} catch (Exception e) 
		{
			throw new XComSOAPException ("Cannot create SOAP connection: " + e.toString());
		}
	}
	
	public SOAPMessage call (SOAPMessage msg, URL url) throws XComSOAPException
	{
		SOAPMessage reply = null;
		
		try {
			// OLD JAXM, deprecated
			URLEndpoint urlEndpoint = new URLEndpoint (url.toString());
			
			//System.out.println ("Message to send:");
			//msg.getSOAPMessage().writeTo (System.out);
			
			reply = con.call ( msg , urlEndpoint );
			
			if (reply == null ) 
			{
				throw new XComSOAPException ("Empty reply");
			}
		} catch (SOAPException e) 
		{
			throw new XComSOAPException ( e.getMessage() );
		} catch (Throwable e) 
		{
			throw new XComSOAPException ("Send to " + url + " failed: " + e.toString());
		} 
		return reply;
	}
}
