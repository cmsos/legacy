#ifndef __EventGenerator
#define __EventGenerator

#include <vector>
#include <sstream>
#include "log4cplus/logger.h"
#include "xdaq/Application.h"
#include "hal/HardwareDeviceInterface.hh"

/************************************************************************
 *
 *
 *     @short Base class for utils::EventGenerators
 *            
 *            This  class generates  descriptors  for event  generators 
 *            implemented  in the  FEROL40, and 
 *            SLINK firmware.  The parts specific to  the various event 
 *            generators are implemented in derived classes.
 *
 *       @see 
 *    @author $Author: fulcher $
 *   @version $Revision: 1.6 $
 *      @date $Date: 2018/02/01 17:07:51 $
 *
 *      Modif 
 *
 *///////////////////////////////////////////////////////////////////////

namespace ferol40 
{
    class utils::EventGenerator {
        
    public:
        utils::EventGenerator( Logger logger );
        virtual ~EventGenerator() {};

        void setupEventGenerator( uint32_t meansize,
                                  uint32_t maxsize,
                                  uint32_t stdevsize,
                                  uint32_t meandelay,
                                  uint32_t stdevdelay, 
                                  uint32_t nevt,
                                  uint32_t seed,
                                  uint32_t streamNo,
                                  uint32_t fedId );

        double getMeanSize() { return meanSize_; };
        double getStdevSize() { return stdevSize_; };
        
    protected:

        /**
         *
         *     @short This function checks and  adjusts the number of events to
         *            be generated and the stream number.
         *            
         *            The number of  events is checked for its  validity and it 
         *            is  adjusted to  match the  requirements of  the specific 
         *            event generator. For example  the numbr is limited to the
         *            maximum  number  of  events  which  the  memory  for  the 
         *            descriptors  can  gold.   Some  generators  require  this 
         *            number to  be a power  of 2 in  which case the  number is 
         *            adjusted. 
         *
         *     @param nevt: the  number of event descriptors  to generate. This 
         *            number might be changed by the function. 
         *            streamNo: the  stream number for  which event descriptors 
         *            should be generated. (Either 0 or 1)
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        virtual void checkParams( uint32_t &nevt, uint32_t streamNo ) = 0;
 
        /**
         *
         *     @short Function to transfer the event descriptors to the hardware.
         *
         *            Depending on  the specific event  generator this function 
         *            contains  the  code to  write  the  descriptors into  the 
         *            correct memory in the required format.
         *            
         *     @param streamNo  is the stream  number (0  or 1)  It is  used to 
         *            form the  HAL Addresstable items  for the filling  of the 
         *            descriptor table.
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        virtual void writeDescriptors( uint32_t streamNo ) = 0;
        
    protected: 
        struct descriptor {
            uint32_t size;
            uint32_t delay;
            uint32_t bx;
            uint32_t fedId;
            uint32_t seed;
        };

        Logger logger_;

        std::vector< descriptor > descriptors_;

        double meanSize_;
        double stdevSize_;
    };
};
#endif /* __EventGenerator */

