#ifndef _tcds_cpm_LumiSection_h_
#define _tcds_cpm_LumiSection_h_

#include <vector>

#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiNibbleGroupBase.h"

namespace tcds {
  namespace cpm {

    class LumiSection : public tcds::cpm::LumiNibbleGroupBase
    {

    public:
      LumiSection(tcds::cpm::LumiNibble const& nibble);
      LumiSection(std::vector<tcds::cpm::LumiNibble> const& nibbles);
      virtual ~LumiSection();

      void addNibble(tcds::cpm::LumiNibble const& nibble);

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_LumiSection_h_
