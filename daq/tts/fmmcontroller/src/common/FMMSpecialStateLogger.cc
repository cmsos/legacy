/**
 *      @file FMMSpecialStateLogger.cc
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.6 $
 *     $Date: 2007/03/27 07:53:31 $
 *
 *
 **/
#include "tts/fmmcontroller/FMMSpecialStateLogger.hh"
#include "tts/fmmcontroller/FMMHistoryConsumer.hh"

#include "toolbox/string.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <unistd.h>

#include "stdlib.h"

tts::FMMSpecialStateLogger::FMMSpecialStateLogger( std::set<tts::TTSState> statesToLog,
						   std::string const& logFileBaseName,
						   uint32_t logFileSizeLimitBytes,
						   log4cplus::Logger* logger)
  : _statesToLog(statesToLog),
    _logFileBaseName(logFileBaseName), 
    _logFileSizeLimitBytes(logFileSizeLimitBytes),
    _logger(logger),
    _previous_hi(),
    _justStarted(true) {
};

tts::FMMSpecialStateLogger::~FMMSpecialStateLogger() {

};


void tts::FMMSpecialStateLogger::setRunNumber( uint32_t runNumber ) {
  _runNumber = runNumber;
}

void tts::FMMSpecialStateLogger::setInputEnableMask( uint32_t inputEnableMask ) {
  _inputEnableMask = inputEnableMask;
}

void tts::FMMSpecialStateLogger::beforeMonitorStart() {

  std::string fn = makeFileName();
  _of.open(fn.c_str(), std::ofstream::app);

  checkFileSize();

  time_t now;
  now = time(NULL);
  _of << "== Run number " << std::dec << _runNumber 
      << " inputEnableMask = " << toolbox::toString("0x%05lx",  _inputEnableMask)
      << " started at " << ctime(&now) << std::flush;

  _justStarted = true;
};

void tts::FMMSpecialStateLogger::afterMonitorStop( uint64_t extendedTimeStamp ) {
  _of.close();
};


void tts::FMMSpecialStateLogger::processItem( tts::FMMHistoryItem const& hi, uint32_t address ) {

  std::vector<tts::TTSState> current_states = hi.getInputStates();
  std::vector<tts::TTSState> previous_states = _previous_hi.getInputStates();

  for (std::vector<tts::TTSState>::size_type i=0; i<current_states.size(); ++i) {


    bool doLog = false;
    if (_justStarted)
      doLog = _statesToLog.count( current_states[i] );
    else
      doLog = current_states[i] != previous_states[i] &&
   	      ( _statesToLog.count( current_states[i] ) || 
	        _statesToLog.count( previous_states[i] )   );

    if ( doLog ) {

      _of << "addr = " << std::setw(6) << std::hex << address
	  << " t=" << std::setw(15) << hi.getTimestampString()
	  << " io=" << std::setw(2) << std::dec << i 
	  << "  " << (_justStarted?std::string("unknown"):previous_states[i].getShortName()) << " => " << current_states[i].getShortName();
      _of << std::endl << std::flush;
    }
 
  }

  _justStarted = false;
  _previous_hi = hi;
} 

std::string tts::FMMSpecialStateLogger::makeFileName() {
  
  return _logFileBaseName;
}


void tts::FMMSpecialStateLogger::checkFileSize() {

  if ( _of.tellp() > (std::streampos) _logFileSizeLimitBytes ) {

    LOG4CPLUS_WARN ( (*_logger), 
		     "tts::FMMSpecialStateLogger::checkFileSize(): special state log file '" 
		     << _logFileBaseName
		     << "' has exceeded its limit of " 
		     << std::dec << _logFileSizeLimitBytes << " bytes. Continuing to append to file." );
  }
}



