// $Id$
#ifndef _bril_radmonprocessor_version_h_
#define _bril_radmonrocessor_version_h_
#include "config/PackageInfo.h"
#define BRILRADMONPROCESSOR_VERSION_MAJOR 1
#define BRILRADMONPROCESSOR_VERSION_MINOR 4
#define BRILRADMONPROCESSOR_VERSION_PATCH 1
#define BRILRADMONPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRILRADMONPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILRADMONPROCESSOR_VERSION_MAJOR,BRILRADMONPROCESSOR_VERSION_MINOR,BRILRADMONPROCESSOR_VERSION_PATCH)
#ifndef BRILRADMONPROCESSOR_PREVIOUS_VERSIONS
#define BRILRADMONPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILRADMONPROCESSOR_VERSION_MAJOR,BRILRADMONPROCESSOR_VERSION_MINOR,BRILRADMONPROCESSOR_VERSION_PATCH)
#else
#define BRILRADMONPROCESSOR_FULL_VERSION_LIST  BRILRADMONPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILRADMONPROCESSOR_VERSION_MAJOR,BRILRADMONPROCESSOR_VERSION_MINOR,BRILRADMONPROCESSOR_VERSION_PATCH)
#endif

namespace brilradmonprocessor
{
  const std::string package     = "brilradmonprocessor";
  const std::string versions    = BRILRADMONPROCESSOR_FULL_VERSION_LIST;
  const std::string summary     = "BRIL DAQ radmonprocessor";
  const std::string description = "collect and process RADMON data from a DIM server";
  const std::string authors     = "Arkday Lokhovitskiy";
  const std::string link        = "";

  config::PackageInfo getPackageInfo ();
  
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);

  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
