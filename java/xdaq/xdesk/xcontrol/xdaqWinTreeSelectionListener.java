package xdaq.tools.win;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWin;

public class xdaqWinTreeSelectionListener implements TreeSelectionListener
{
	xdaqWin win_;

	xdaqWinTreeSelectionListener(xdaqWin win)
	{
		win_ = win;
	}
	
	public void valueChanged (TreeSelectionEvent e)
	{
		xdaqWinTree tree = (xdaqWinTree) e.getSource();
		TreePath[] paths = tree.getSelectionModel().getSelectionPaths();
		
		// Protect against empty selection
		if (paths == null) return;
		
		String association = "";
		String instance = "";
		
		// consistency check
		// remember the last item of the first selection.
		// all other selected items must be the same
		
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
		xdaqWinTreeAdapterNode adapterNode = (xdaqWinTreeAdapterNode) node.getUserObject();
		String lastElement = node.toString();
		
		//System.out.println ("Last Element selected: " + lastElement);
		
		for (int i = 1; i < paths.length; i++) 
		{
			node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			String currentElement =  node.toString();
			
			// Transport and Application are equivalent
			if (  currentElement.equals("Transport") ) currentElement = "Application";
			
			if (!lastElement.equals ( currentElement ))
			{
					JOptionPane.showMessageDialog (null,
				 		"Must select nodes of the same type",
                                       		 "Selection failed", JOptionPane.ERROR_MESSAGE);

					tree.getSelectionModel().removeSelectionPath (paths[i]);	
			}
		}
		
		if (lastElement.equals("Host"))
		{
			win_.getMenu().setHostMenu();
			
			association = "Host";
			instance = adapterNode.getAttribute ("id");
			
		}
		else if (lastElement.equals("Application") || lastElement.equals("Transport"))
		{
			// Check for plugin
			association = adapterNode.getAttribute ("class");
			instance = adapterNode.getAttribute ("instance");
			
			win_.getMenu().setApplicationMenu();
			
		} else if (lastElement.equals("Partition")) 
		{
			win_.getMenu().setPartitionMenu();
			association = "Partition";
			instance = "";
		
		} else if (lastElement.equals("Script")) 
		{
			//win_.getMenu().setScriptMenu();
			win_.getMenu().setScriptMenu();
			
			association = "Script";
			instance = "";
		} else 
		{
			association = adapterNode.getNodeName();
			instance = "";
			win_.getMenu().setEmptyMenu();
		}
		
		// Display the list of Daqlets for this specific association instance
		//
		win_.displayDaqlet ( association, instance );
		
	}
}
