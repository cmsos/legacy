#include "ferol/FerolInfoSpaceHandler.hh"
#include "ferol/loggerMacros.h"

ferol::FerolInfoSpaceHandler::FerolInfoSpaceHandler( xdaq::Application* xdaq, 
                                                     utils::InfoSpaceUpdater *updater )
    : utils::InfoSpaceHandler( xdaq, "Ferol_IS", updater )
{
    createuint32( "FerolFwVersion", 0, "hex", NOUPDATE, "The version number of the firmware. Each compilation of the firmware increments this number. The software checks for a minimal value required to run the software." );
    createuint32( "FerolFwType", 0, "hex", NOUPDATE, "The Firmware Type determines the functionality of the card. E.g. there is a dedicated firmware for the Fedkit." );
    createuint32( "FerolHwRevision", 0, "hex", NOUPDATE, "The HwRevision refers to the version of the Hardware (PCB, FPGAs, ...)" );
    createuint32( "slotNumber", 0, "", NOUPDATE, "The slot number is harwired on the backplane and mapped in the PCI Configuration Space, from where it is read." );
    createuint64( "DestinationMACAddress", 0, "mac", NOUPDATE, "This register is filled after a successful ARP request." );
    createuint64( "Source_MAC_EEPROM", 0, "mac", NOUPDATE, "This XDAQ variable is filled with the MAC address which is read from the Hardware during the creation of the hardware device object (at the early stage of \"Configure\")." );
    createuint64( "FerolSnLow", 0, "hex", NOUPDATE, "The serial number (96 bits) is read from an EPROM when the hardware device object is created at the early stage of \"Configure\". These are the lower 64 bits of the SN." );
    createuint32( "FerolSnHi", 0, "hex", NOUPDATE, "The serial number (96 bits) is read from an EPROM when the hardware device object is created at the early stage of \"Configure\". These are the higher 32 btis of the SN." );

    createuint32( "ARP_MAC_CONFLICT", 0, "", HW32, "Should be '0'. A counter which is incremented every time an ARP reply is received which contaisn a source MAC address equal to ours. If this counter is larger than 0, we have a device on the network which has the same MAC address as this Ferol. This is a deasaster!.");
    createuint32( "ARP_IP_CONFLICT", 0, "", HW32, "Should be '0'. A counter which is incremented every time an ARP reply is received which contaisn a source IP address equal to ours. If this counter is larger than 0, we have a device on the network which has the same IP address as this Ferol. This is a deasaster!. At configure time the Ferol sends a probe ARP-request to search for devices with our IP address in order to detect such a conflict.");
    createuint64( "ARP_MAC_WITH_IP_CONFLICT_LO", 0, "mac", HW64, "Should be '00:00:00:00:00:00'. If an IP conflict has been detected on the network this register contains the MAC address of the device which has the same IP address as this Ferol." ); 

    createuint32( "SERDES_STATUS", 0, "", HW32, "Indicates (if '1') that the SERDES of the FPGA which is connected to the XAUI link to the Vitesse chip is up." );
    createuint32( "TCP_ARP_REPLY_OK", 0, "", HW32, "A '1' indicates that the ARP request for the destination IP has been answered successfully."  );
    createuint32( "TCP_STATE_FEDS", 0, "hex", HW32, "Bitfield 0: connection stream0 reset; 1: sync request stream0 pending; 2: connection stream0 established; bits 16,17,18 the same for stream 1; bit 31: ARP reply received.");

    createuint32( "PAUSE_FRAMES_COUNTER", 0, "", HW32, "The number of Pause Frames received by the Ferol" );

    createuint64( "PACKETS_RECEIVED_LO", 0, "", HW64, "Total number of packets received from the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, TCP/IP related packets, ...)" );
    createuint64( "PACKETS_RECEIVED_BAD_LO", 0, "", HW64, "Total number of errorneous packets received. These are packets with a CRC error." );
    createuint64( "PACKETS_SENT_LO", 0, "", HW64, "Total number of packets which have been sent out of the 10Gb DAQ-link. Here all packets are counted (ARP, PING, DHCP, sent and retransmitted TCP/IP packets, ...)");



    //////////////////////////// The following are per stream /////////////////////////////


    createuint32( "Link5gb_SFP_status_L0", 0, "field%RX-loss%in%ena%TX-fault%", HW32, "4 status bits for the low speed input link. Active bits are shown in red. RX-loss: no valid input signal detected; in: SFP detected; ena: Link enabled successfully; TX-loss: major failure of Laser." );
    createuint32( "Link5gb_SFP_status_L1", 0, "field%RX-loss%in%ena%TX-fault%", HW32, "4 status bits for the low speed input link. Active bits are shown in red. RX-loss: no valid input signal detected; in: SFP detected; ena: Link enabled successfully; TX-loss: major failure of Laser." );

    createuint32( "L5gb_OLstatus_SlX0", 0, "hex", HW32, "Additional expert debugging info for the input link 0. The meaning of the bits might change.");
    createuint32( "L5gb_OLstatus_SlX1", 0, "hex", HW32, "Additional expert debugging info for the input link 1. The meaning of the bits might change.");

    createuint32( "GEN_EVENT_NUMBER_FED0", 0, "", HW32, "The number of generated events in the ferol event generator.");
    createuint32( "GEN_EVENT_NUMBER_FED1", 0, "", HW32, "The number of generated events in the ferol event generator.");
    createuint32( "GEN_TRIGGER_CONTROL_FED0", 0x0, "", HW32, "Bit 0 : writing a '1' will generate one software trigger. <br>Bit 1 : loop mode (to be set before or at the same time as bit 4)<br>Bit 2 : stop the triggers in loop mode. <br>Bit 3 : If 0: only loop mode can be used and the event length must be specified in the descriptors written to the descriptor-fifo. If 1: The event length is taken from the register \"GEN_EVENT_LEN_FED0\" (in loop mode and for software triggers)." );
    createuint32( "GEN_TRIGGER_CONTROL_FED1", 0x0, "", HW32, "Bit 0 : writing a '1' will generate one software trigger. <br>Bit 1 : loop mode (to be set before or at the same time as bit 4)<br>Bit 2 : stop the triggers in loop mode. <br>Bit 3 : If 0: only loop mode can be used and the event length must be specified in the descriptors written to the descriptor-fifo. If 1: The event length is taken from the register \"GEN_EVENT_LEN_FED1\" (in loop mode and for software triggers)."  );

    createuint32( "L5gb_sync_lost_draining_SlX0", 0, "", HW32, "'1' indicates that a wrong event number has been received in this channel (sync-lost-draining)." ); 
    createuint32( "L5gb_sync_lost_draining_SlX1", 0, "", HW32, "'1' indicates that a wrong event number has been received in this channel (sync-lost-draining)." );  
    createuint32( "L5gb_trg_Bad_rcved_SlX0", 0, "", HW32, "Bad trigger number of first sync-lost draining in this run." );
    createuint32( "L5gb_trg_Bad_rcved_SlX1", 0, "", HW32, "Bad trigger number of first sync-lost draining in this run." );

    createuint32( "L5gb_bad_pack_received_SlX0", 0, "", HW32, "Counts the number of packets with a low-level CRC error. These packets are re-sent. This CRC is not the SLINK or FED - CRC but is implemented at a lower level. Actually an SLINK CRC error should never be seen on the SLINKexpress due to the resend-feature. However the SLINK CRC has been implemented for compatiblity with the electrical SLINK." ); 
    createuint32( "L5gb_bad_pack_received_SlX1", 0, "", HW32, "Counts the number of packets with a low-level CRC error. These packets are re-sent. This CRC is not the SLINK or FED - CRC but is implemented at a lower level. Actually an SLINK CRC error should never be seen on the SLINKexpress due to the resend-feature. However the SLINK CRC has been implemented for compatiblity with the electrical SLINK." ); 

    createuint64( "L5gb_FED_CRC_ERR_SlX0_LO", 0, "", HW64, "Counts the number of FED CRC errors received over the optical slink.");
    createuint64( "L5gb_FED_CRC_ERR_SlX1_LO", 0, "", HW64, "Counts the number of FED CRC errors received over the optical slink.");

    createuint64( "L5gb_SLINK_CRC_ERR_SlX0_LO", 0, "", HW64, "Counts the number of SLINK CRC errors received over the optical slink. If this number is differnt from 0 there is a problem in the firmware, since packets with SLINK CRC errors are re-sent over the optical link.");
    createuint64( "L5gb_SLINK_CRC_ERR_SlX1_LO", 0, "", HW64, "Counts the number of SLINK CRC errors received over the optical slink. If this number is differnt from 0 there is a problem in the firmware, since packets with SLINK CRC errors are re-sent over the optical link.");


    // the SFP related variables are of type NOUPDATE since they are updated
    // and pushed through to the infospace in a call to one function.
    createstring( "SFP_DATE_0", "", "", NOUPDATE );
    createstring( "SFP_DATE_1", "", "", NOUPDATE );
    createstring( "SFP_PARTNO_0", "", "", NOUPDATE );
    createstring( "SFP_PARTNO_1", "", "", NOUPDATE );
    createstring( "SFP_VENDOR_0", "", "", NOUPDATE );
    createstring( "SFP_VENDOR_1", "", "", NOUPDATE );
    createstring( "SFP_SN_0", "", "", NOUPDATE );
    createstring( "SFP_SN_1", "", "", NOUPDATE );
    createdouble( "SFP_VCC_0",     0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_1",     0.0, "V", NOUPDATE );
    createdouble( "SFP_TEMP_0",    0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_1",    0.0, "C", NOUPDATE );
    createdouble( "SFP_BIAS_0",    0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_1",    0.0, "mA", NOUPDATE );
    createdouble( "SFP_TXPWR_0",   0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_1",   0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_0",   0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_1",   0.0, "uW", NOUPDATE );

    createdouble( "SFP_VCC_L_ALARM_0",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_ALARM_0",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_L_ALARM_1",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_ALARM_1",  0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_L_WARN_0",   0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_WARN_0",   0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_L_WARN_1",   0.0, "V", NOUPDATE );
    createdouble( "SFP_VCC_H_WARN_1",   0.0, "V", NOUPDATE );
    createdouble( "SFP_TEMP_L_ALARM_0",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_ALARM_0",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_L_ALARM_1",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_ALARM_1",  0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_L_WARN_0",   0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_WARN_0",   0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_L_WARN_1",   0.0, "C", NOUPDATE );
    createdouble( "SFP_TEMP_H_WARN_1",   0.0, "C", NOUPDATE );
    createdouble( "SFP_BIAS_L_ALARM_0",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_ALARM_0",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_L_ALARM_1",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_ALARM_1",  0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_L_WARN_0",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_WARN_0",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_L_WARN_1",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_BIAS_H_WARN_1",   0.0, "mA", NOUPDATE );
    createdouble( "SFP_TXPWR_L_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_L_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_L_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_L_WARN_1",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_TXPWR_H_WARN_1",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_ALARM_0", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_ALARM_1", 0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_WARN_0",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_L_WARN_1",  0.0, "uW", NOUPDATE );
    createdouble( "SFP_RXPWR_H_WARN_1",  0.0, "uW", NOUPDATE );

    // calculated rates
    createdouble( "PacketRate", 0 , "rate", TRACKER, "Total rate of packets leaving the DAQ link in this moment." );
    createdouble( "PauseFrameRate", 0, "rate", TRACKER, "The total rate of pause frames received in this moment." );
    createdouble( "FerolEventGenRateFed0", 0, "rate", TRACKER, "Event counter for the internal event generator." );    
    createdouble( "FerolEventGenRateFed1", 0, "rate", TRACKER, "Event counter for the internal event generator." );    
}

void
ferol::FerolInfoSpaceHandler::registerTrackerItems( utils::DataTracker &tracker )
{
    tracker.registerRateTracker( "PacketRate", "PACKETS_SENT_LO", utils::DataTracker::HW64 );
    tracker.registerRateTracker( "PauseFrameRate", "PAUSE_FRAMES_COUNTER", utils::DataTracker::HW32 );
    tracker.registerRateTracker( "FerolEventGenRateFed0", "GEN_EVENT_NUMBER_FED0", utils::DataTracker::HW32 );
    tracker.registerRateTracker( "FerolEventGenRateFed1", "GEN_EVENT_NUMBER_FED1", utils::DataTracker::HW32 );
}
