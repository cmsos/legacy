#include "tcds/ttcspy/SpyMemEntry.h"

#include <algorithm>
#include <climits>
#include <vector>

// NOTE: This stores the bits in the 'reverse' order from what is
// normally expected. This allows much simpler use of iterators
// (instead of reverse iterators) though.
template <typename T>
std::vector<bool>
tcds::ttcspy::SpyMemEntry::intToVecBool(T const input) const
{
  std::vector<bool> res;
  T tmp = input;
  for (size_t i = 0; i < sizeof(T) * CHAR_BIT; ++i)
    {
      res.push_back(tmp & T(1));
      tmp >>= 1;
    }
  return res;
}

// NOTE: See the note at the top of intToVecBool().
template <typename T>
T
tcds::ttcspy::SpyMemEntry::vecBoolToUInt(std::vector<bool> const input) const
{
  uint32_t res = 0;
  uint32_t mask = 1;
  for (std::vector<bool>::const_iterator it = input.begin();
       it != input.end();
       ++it)
    {
      if (*it == true)
        {
          res |= mask;
        }
      mask <<= 1;
    }
  return res;
}
