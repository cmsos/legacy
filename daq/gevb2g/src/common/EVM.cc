#include <sstream>
#include "gevb2g/EVM.h"
#include "i2o/utils/AddressMap.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xcept/tools.h"
#include "toolbox/hexdump.h"
#include "toolbox/mem/HeapAllocator.h"

// Log4CPLUS
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL (gevb2g::EVM)

gevb2g::EVM::EVM (xdaq::ApplicationStub* c) 
	: xdaq::Application(c), xgi::framework::UIManager(this)
{
	i2o::bind(this, &gevb2g::EVM::Available, I2O_AVAILABLE, XDAQ_ORGANIZATION_ID);
	i2o::bind(this, &gevb2g::EVM::Trigger, I2O_TRIGGER, XDAQ_ORGANIZATION_ID);

	//i2o::bind (this, &gevb2g::EVM::TriggerAccept, I2O_TRIGGER_ACCEPT ,XDAQ_ORGANIZATION_ID);

	xoap::bind(this, &gevb2g::EVM::Configure, "Configure", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::EVM::Enable, "Enable", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::EVM::Halt, "Halt", XDAQ_NS_URI);

	triggerQueue_ = toolbox::rlist<unsigned long>::create("EVM/triggerQueue_");
        resourceQueue_ = toolbox::rlist<BUResourceDescriptor>::create("EVM/resourceQueue_");

	bxCounter_ = 0;
	shipFrameSize_ = 0;
	resourceQueueSize_ = 0;
	triggerQueueSize_ = 0;
	triggerDisable_ = 1;
	triggerGather_ = 0;
	consumedTriggers_ = 0;
	triggerTid_ = 0;
	triggerClassInstance_ = 0;
	triggerClassName_ = "unknown";

	createPool_ = true;
	poolName_ = "evmPool";

	getApplicationInfoSpace()->fireItemAvailable("shipFrameSize", &shipFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("resourceQueueSize", &resourceQueueSize_);
	getApplicationInfoSpace()->fireItemAvailable("triggerQueueSize", &triggerQueueSize_);
	getApplicationInfoSpace()->fireItemAvailable("triggerDisable", &triggerDisable_);
	getApplicationInfoSpace()->fireItemAvailable("triggerGather", &triggerGather_);
	getApplicationInfoSpace()->fireItemAvailable("triggerClassInstance", &triggerClassInstance_);
	getApplicationInfoSpace()->fireItemAvailable("triggerClassName", &triggerClassName_);

	getApplicationInfoSpace()->fireItemAvailable("createPool", &createPool_);
	getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);

	requestMutex_ = new toolbox::BSem(toolbox::BSem::FULL);

	// FUs
	std::set<const xdaq::ApplicationDescriptor*> rus;

	try
	{
		rus = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("gevb2g::RU");
		size_t size = rus.size();
		ruTid_.resize(size);
		rus_.resize(size);
	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW(xcept::Exception, "No FU application instance found. Client cannot be configured.", e);
	}

	std::set<const xdaq::ApplicationDescriptor*>::iterator iter;

	for (iter = rus.begin(); iter != rus.end(); iter++)
	{
		size_t instance = (*iter)->getInstance();
		ruTid_[instance] = i2o::utils::getAddressMap()->getTid(*iter);
		rus_[instance] = *iter;
		LOG4CPLUS_DEBUG(getApplicationLogger(), "get tid for RU " << (*iter)->getInstance() << "(" << ruTid_[instance] << ")");
	}

	tid_ = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

void gevb2g::EVM::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
		{
			if (createPool_)
			{
				toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			}
			else
			{
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
			}
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
			return;
		}

	}
}

xoap::MessageReference gevb2g::EVM::Configure (xoap::MessageReference message) 
{
	// remove any surious elements if any from previous runs
	this->clear();

	// reset of value 
	consumedTriggers_ = 0;

	triggerQueue_->resize(triggerQueueSize_);
	resourceQueue_->resize(resourceQueueSize_);

	maxGatherRequests_ = (shipFrameSize_ - (sizeof(I2O_SHIP_FRAGMENTS_MESSAGE_FRAME) + sizeof(ShipRequest))) / sizeof(ShipRequest);

	if (triggerDisable_)
	{
		triggerTid_ = 0;
		// fill with dummy bx			
		for (size_t i = 0; i < triggerQueueSize_; i++)
		{
			triggerQueue_->push_back(bxCounter_);
			bxCounter_++;
		}
	}
	else
	{ //get tid of <triggerClassName ,triggerClassInstance>

		try
		{
			triggerDescriptor_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptor(triggerClassName_, triggerClassInstance_);
		}
		catch (xdaq::exception::Exception& e)
		{
			XCEPT_RETHROW(xcept::Exception, "No Trigger application instance found. Client cannot be configured.", e);
		}

		//triggerTid_ = xdaq::getTid(triggerClassName_,triggerClassInstance_);
		triggerTid_ = i2o::utils::getAddressMap()->getTid(triggerDescriptor_);
		LOG4CPLUS_INFO(this->getApplicationLogger(), "found tid " << (size_t) triggerTid_ << " for class " << triggerClassName_.toString() << " instance " << (size_t) triggerClassInstance_);

	}

	this->logConfiguration();

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Configured");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("ConfigureResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;
}

void gevb2g::EVM::Available (toolbox::mem::Reference * ref)
{
	if (stopped_)
	{
		ref->release();
		return;
	}

	PI2O_AVAILABLE_MESSAGE_FRAME frame = (PI2O_AVAILABLE_MESSAGE_FRAME) ref->getDataLocation();

	U32 resources = frame->resources;
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "received available resources " << frame->resources);
	//DEBUG
	//		std::cout << "---begin--- size(WC) " << frame->PvtMessageFrame.StdMessageFrame.MessageSize << " function " << std::hex << frame->PvtMessageFrame.StdMessageFrame.Function  <<  " XFUNCTION code" << frame->PvtMessageFrame.XFunctionCode << std::dec << std::endl;
//		toolbox::hexdump(ref->getDataLocation(),frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2 );
//		std::cout << "---end-----" << std::endl;

	//DEBUG	
	for (size_t i = 0; i < resources; i++)
	{
		BUResourceDescriptor descriptor;
		descriptor.resourceId = frame->resource[i].resourceId;
		descriptor.destination = frame->destination;
		descriptor.context = frame->resource[i].context;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "received  with context " << std::hex << frame->resource[i].context << " resource id " << std::dec << frame->resource[i].resourceId);

		try
		{
			resourceQueue_->push_back(descriptor);
		}
		catch (xdaq::exception::Exception e)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), e.what());
			LOG4CPLUS_FATAL(this->getApplicationLogger(), "EVM resource queue full. Size " << resourceQueueSize_ << ", resources to push " << resources << ", pushed " << i);
			ref->release();
			return;
		}

	}

	if (triggerDisable_)
	{
		ref->release();
	}
	else
	{
		// reuse frame to inform trigger supervisor about token availables
		PI2O_TRIGGER_ACCEPT_MESSAGE_FRAME acceptFrame = (PI2O_TRIGGER_ACCEPT_MESSAGE_FRAME) ref->getDataLocation();
		acceptFrame->PvtMessageFrame.StdMessageFrame.MessageSize = sizeof(I2O_TRIGGER_ACCEPT_MESSAGE_FRAME) >> 2;
		acceptFrame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		acceptFrame->PvtMessageFrame.XFunctionCode = I2O_TRIGGER_ACCEPT;
		acceptFrame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
		acceptFrame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
		acceptFrame->PvtMessageFrame.StdMessageFrame.TargetAddress = triggerTid_;
		acceptFrame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
		acceptFrame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
		acceptFrame->tokens = resources;

		//xdaq::frameSend(ref);
		ref->setDataSize(sizeof(I2O_TRIGGER_ACCEPT_MESSAGE_FRAME));
		this->getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), triggerDescriptor_);

	}

	//this->checkBackPressure();

	this->matchRequests();
}

void gevb2g::EVM::logConfiguration ()
{
	std::stringstream msg;
	msg << "Configuration parameter settings:" << std::endl;
	msg << "shipFrameSize " << shipFrameSize_ << std::endl;
	msg << "resourceQueueSize " << resourceQueueSize_ << std::endl;
	msg << "triggerQueueSize " << triggerQueueSize_ << std::endl;
	msg << "triggerDisable " << triggerDisable_ << std::endl;
	msg << "triggerGather " << triggerGather_ << std::endl;
	msg << "triggerTid " << triggerTid_ << std::endl;
	msg << "max gathering " << maxGatherRequests_ << " requests per (ship) frame";

	LOG4CPLUS_INFO(this->getApplicationLogger(), msg.str());
}

//
// Message incoming from FLT with new triggers
//
void gevb2g::EVM::Trigger (toolbox::mem::Reference* ref)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "received trigger");
	if (stopped_)
	{
		ref->release();
		return;
	}

	PI2O_TRIGGER_MESSAGE_FRAME frame = (PI2O_TRIGGER_MESSAGE_FRAME) ref->getDataLocation();

	for (size_t i = 0; i < frame->triggers; i++)
	{
		LOG4CPLUS_DEBUG(getApplicationLogger(), "PUSHTRIGGER for bx" << bxCounter_);
		bxCounter_++; // dummy
		triggerQueue_->push_back(bxCounter_);
	}

	ref->release();

	this->checkBackPressure();
	this->matchRequests();
}

void gevb2g::EVM::checkBackPressure ()
{
	// not needed now		
}

void gevb2g::EVM::recycleTrigger ()
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "recycle trigger");
	consumedTriggers_++;

	if (consumedTriggers_ == triggerGather_)
	{
		//toolbox::mem::Reference* ref = xdaq::frameAlloc(sizeof(I2O_TRIGGER_MESSAGE_FRAME));

		toolbox::mem::Reference* ref = 0;
		try
		{
			// allocate frame
			ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, sizeof(I2O_TRIGGER_MESSAGE_FRAME));
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
		}

		PI2O_TRIGGER_MESSAGE_FRAME frame = (PI2O_TRIGGER_MESSAGE_FRAME) ref->getDataLocation();
		frame->PvtMessageFrame.StdMessageFrame.MessageSize = sizeof(I2O_TRIGGER_MESSAGE_FRAME) >> 2;
		frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		frame->PvtMessageFrame.XFunctionCode = I2O_TRIGGER;
		frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
		frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
		frame->PvtMessageFrame.StdMessageFrame.TargetAddress = tid_; // myself
		frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
		frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
		frame->triggers = consumedTriggers_;
		consumedTriggers_ = 0;

		//xdaq::frameSend (ref);
		ref->setDataSize(sizeof(I2O_TRIGGER_MESSAGE_FRAME));
		LOG4CPLUS_DEBUG(getApplicationLogger(), "post trigger");
		this->getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), this->getApplicationDescriptor());

	}
}

void gevb2g::EVM::matchRequests ()
{
	requestMutex_->take();
	LOG4CPLUS_DEBUG(getApplicationLogger(), "match");

	//cout << "(" << who << ")Triggers:" << triggerQueue_->size() << "Resources:" << resourceQueue_->size() << endl;

	while ((!triggerQueue_->empty()) && (!resourceQueue_->empty()))
	{
		//toolbox::mem::Reference * ref = xdaq::frameAlloc(shipFrameSize_);
		toolbox::mem::Reference * ref = 0;
		try
		{
			// allocate frame
			ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, shipFrameSize_);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
		}

		PI2O_SHIP_FRAGMENTS_MESSAGE_FRAME framePtr = (PI2O_SHIP_FRAGMENTS_MESSAGE_FRAME) ref->getDataLocation();
		framePtr->fragments = 0;

		while (framePtr->fragments < maxGatherRequests_)
		{
			if ((!triggerQueue_->empty()) && (!resourceQueue_->empty()))
			{
				// add to frame
				framePtr->request[framePtr->fragments].bx = triggerQueue_->front();
				triggerQueue_->pop_front();

				if (triggerDisable_)
				{
					recycleTrigger();
				}

				BUResourceDescriptor descriptor = resourceQueue_->front();
				resourceQueue_->pop_front();

				framePtr->request[framePtr->fragments].destination = descriptor.destination;
				framePtr->request[framePtr->fragments].resourceId = descriptor.resourceId;
				framePtr->request[framePtr->fragments].context = descriptor.context;

				framePtr->fragments++;
			}
			else
			{
				break;
			}
		}

		this->multiCast(ref);
	}

	requestMutex_->give();
}

void gevb2g::EVM::multiCast (toolbox::mem::Reference * ref)
{
	for (size_t i = 0; i < ruTid_.size(); i++)
	{
		// allocate a new frame for shipping
		//toolbox::mem::Reference * copyRef = xdaq::frameAlloc(shipFrameSize_);
		toolbox::mem::Reference* copyRef = 0;
		try
		{
			// allocate frame
			copyRef = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, shipFrameSize_);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
		}

		// copy payload
		memcpy(copyRef->getDataLocation(), ref->getDataLocation(), shipFrameSize_);

		PI2O_SHIP_FRAGMENTS_MESSAGE_FRAME framePtr = (PI2O_SHIP_FRAGMENTS_MESSAGE_FRAME) copyRef->getDataLocation();

		framePtr->PvtMessageFrame.StdMessageFrame.MessageSize = shipFrameSize_ >> 2;
		framePtr->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		framePtr->PvtMessageFrame.XFunctionCode = I2O_SHIP_FRAGMENTS;
		framePtr->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
		framePtr->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
		framePtr->PvtMessageFrame.StdMessageFrame.TargetAddress = ruTid_[i];
		framePtr->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
		framePtr->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;

		//	xdaq::frameSend(copyRef);
		ref->setDataSize(shipFrameSize_);
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), toolbox::toString("send  to RU tid: %d requests: %d", ruTid_[i], framePtr->fragments));
		try
		{
			this->getApplicationContext()->postFrame(copyRef, this->getApplicationDescriptor(), rus_[i]);
		}
		catch (xdaq::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

	}

	ref->release();
}

xoap::MessageReference gevb2g::EVM::Enable (xoap::MessageReference message) 
{
	stopped_ = false;
	LOG4CPLUS_INFO(this->getApplicationLogger(), "Enabling now");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("EnableResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

xoap::MessageReference gevb2g::EVM::Halt (xoap::MessageReference message) 
{
	// inhibit external interfaces
	stopped_ = true;

	//!!! it cannot be done here due to race conditions
	// this->clear();

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Halting now");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("HaltResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

void gevb2g::EVM::clear ()
{
	// remove any spurious elements
	while (!triggerQueue_->empty())
		triggerQueue_->pop_front();
	while (!resourceQueue_->empty())
		resourceQueue_->pop_front();

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Trigger and resource queues have been flushed");
}

