// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/sapi/DpDisconnectRequest.h"
#include "psx/sapi/ApplicationService.h"
     

psx::sapi::DpDisconnectRequest::DpDisconnectRequest (psx::sapi::ApplicationService * as) : psx::sapi::Request("dpDisconnect",as)
{
	url_ = "";
	context_ = "";
	id_ = "";	
}

void psx::sapi::DpDisconnectRequest::setTransactionId(const std::string& id)
{
	id_ = id;
}

std::string& psx::sapi::DpDisconnectRequest::getTransactionId()
{
	return id_;
}

void psx::sapi::DpDisconnectRequest::setURL(const std::string & url )
{
	url_ = url;
}

			
void psx::sapi::DpDisconnectRequest::setContext(const std::string & context)
{
	context_ = context;
}

std::string psx::sapi::DpDisconnectRequest::getURL()
{
	return url_;
}

			
std::string psx::sapi::DpDisconnectRequest::getContext()
{
	return  context_;

}



