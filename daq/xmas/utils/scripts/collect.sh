#!/bin/sh

curl -D headers.txt --trace trace.dat -o response.dat  \
-H "SOAPAction: urn:xdaq-application:service=sensor" \
-d "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" 
  xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" 
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
	<xmas:collect xmlns:xmas=\"http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10\">
		<xmas:sample flashlist=\"urn:xdaq-flashlist:test\" tag=\"$2\"/>	
        </xmas:collect>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>" $1

echo ""

