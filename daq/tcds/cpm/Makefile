BUILD_HOME:=$(shell pwd)/../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
export BUILD_SUPPORT
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
export PROJECT_NAME
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

ifndef MFDEFS_SUPPORT
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.coretools
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.extern_coretools
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.general_worksuite
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.hardware_worksuite
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.powerpack
else
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)
endif

# As a transition measure between XDAQ14 (gcc 4.8, C++98) and XDAQ15
# (gcc 7.2, C++14), let's build in some wiggle room.
# Useful choices should be one of:
# - XDAQ_TARGET_XDAQ14
# - XDAQ_TARGET_XDAQ15
ifndef XDAQ_TARGET
  export XDAQ_TARGET=XDAQ_TARGET_XDAQ14
endif

Project = $(PROJECT_NAME)
Package = tcds/cpm

Sources = \
    version.cc \
    BRILDAQPublisher.cc \
    BRILDAQTable.cc \
    ConfigurationInfoSpaceHandler.cc \
    CPMController.cc \
    CPMInputsInfoSpaceHandler.cc \
    CPMInputsInfoSpaceUpdater.cc \
    CPMBRILDAQInfoSpaceHandler.cc \
    CPMBRILDAQInfoSpaceUpdater.cc \
    CPMRatesInfoSpaceHandler.cc \
    CPMRatesInfoSpaceUpdater.cc \
    CPMTTSLinksInfoSpaceHandler.cc \
    CPMTTSLinksInfoSpaceUpdater.cc \
    HwIDInfoSpaceHandlerTCA.cc \
    HwIDInfoSpaceUpdaterTCA.cc \
    HwStatusInfoSpaceHandler.cc \
    HwStatusInfoSpaceUpdater.cc \
    BRILDAQLoop.cc \
    BRILDAQPacket.cc \
    BRILDAQSnapshot.cc \
    BRILDAQStorageInfoSpaceHandler.cc \
    LumiNibble.cc \
    LumiNibbleGroup.cc \
    LumiNibbleGroupBase.cc \
    LumiSection.cc \
    NibbleHistory.cc \
    SectionHistory.cc \
    TCADeviceCPMT1.cc \
    TCADeviceCPMT2.cc \
    WebTableNibbleHistory.cc \
    WebTableSectionHistory.cc \
    WebTableSuppressedTriggers.cc

TCDS_EXCEPTION_PREFIX = $(BUILD_HOME)/$(Project)/$(Package)/../exception
TCDS_EXCEPTION_INCLUDE_PREFIX = $(TCDS_EXCEPTION_PREFIX)/include
TCDS_EXCEPTION_LIB_PREFIX = $(TCDS_EXCEPTION_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

TCDS_HWLAYER_PREFIX = $(BUILD_HOME)/$(Project)/$(Package)/../hwlayer
TCDS_HWLAYER_INCLUDE_PREFIX = $(TCDS_HWLAYER_PREFIX)/include
TCDS_HWLAYER_LIB_PREFIX = $(TCDS_HWLAYER_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

TCDS_HWLAYERTCA_PREFIX = $(BUILD_HOME)/$(Project)/$(Package)/../hwlayertca
TCDS_HWLAYERTCA_INCLUDE_PREFIX = $(TCDS_HWLAYERTCA_PREFIX)/include
TCDS_HWLAYERTCA_LIB_PREFIX = $(TCDS_HWLAYERTCA_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

TCDS_HWUTILSTCA_PREFIX = $(BUILD_HOME)/$(Project)/$(Package)/../hwutilstca
TCDS_HWUTILSTCA_INCLUDE_PREFIX = $(TCDS_HWUTILSTCA_PREFIX)/include
TCDS_HWUTILSTCA_LIB_PREFIX = $(TCDS_HWUTILSTCA_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

TCDS_PM_PREFIX = $(BUILD_HOME)/$(Project)/$(Package)/../pm
TCDS_PM_INCLUDE_PREFIX = $(TCDS_PM_PREFIX)/include
TCDS_PM_LIB_PREFIX = $(TCDS_PM_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

TCDS_UTILS_PREFIX = $(BUILD_HOME)/$(Project)/$(Package)/../utils
TCDS_UTILS_INCLUDE_PREFIX = $(TCDS_UTILS_PREFIX)/include
TCDS_UTILS_LIB_PREFIX = $(TCDS_UTILS_PREFIX)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)

IncludeDirs = \
    $(BUILD_HOME)/$(Project)/$(Package)/include \
    $(CGICC_INCLUDE_PREFIX) \
    $(CONFIG_INCLUDE_PREFIX) \
    $(EVENTING_API_INCLUDE_PREFIX) \
    $(HYPERDAQ_INCLUDE_PREFIX) \
    $(LOG4CPLUS_INCLUDE_PREFIX) \
    $(PT_INCLUDE_PREFIX) \
    $(TCDS_EXCEPTION_INCLUDE_PREFIX) \
    $(TCDS_HWLAYER_INCLUDE_PREFIX) \
    $(TCDS_HWLAYERTCA_INCLUDE_PREFIX) \
    $(TCDS_HWUTILSTCA_INCLUDE_PREFIX) \
    $(TCDS_PM_INCLUDE_PREFIX) \
    $(TCDS_UTILS_INCLUDE_PREFIX) \
    $(TOOLBOX_INCLUDE_PREFIX) \
    $(XCEPT_INCLUDE_PREFIX) \
    $(XDAQ_INCLUDE_PREFIX) \
    $(XDAQ2RC_INCLUDE_PREFIX) \
    $(XDATA_INCLUDE_PREFIX) \
    $(XERCES_INCLUDE_PREFIX) \
    $(XGI_INCLUDE_PREFIX) \
    $(XOAP_INCLUDE_PREFIX)

DependentLibraryDirs =

DependentLibraries =

UserCFlags =
UserCCFlags = -Werror -Wno-deprecated -D__STDC_FORMAT_MACROS
UserCCFlags += -D$(XDAQ_TARGET)
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =

ExternalObjects =

DynamicLibrary = tcdscpm
StaticLibrary =
Executables =

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
