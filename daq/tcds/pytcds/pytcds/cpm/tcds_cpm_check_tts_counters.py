#!/usr/bin/env python

###############################################################################
## Periodically check a certain TTS 'time spent' counter in the CPM.
## NOTE: For testing purposes only.
###############################################################################

import datetime
import sys
import time

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_cpm_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        reg_name = "cpmt1.ipm.tts_time_counters.lpm1.ici1.ready"
        # reg_name = "cpmt1.ipm.tts_time_counters.trigger_rules.ready"
        print "Reading register '{0:s}'".format(reg_name)

        cpm.write("cpmt1.ipm.main.resets.tts_counters_reset", 0x1)
        cpm.write("cpmt1.ipm.main.resets.tts_counters_reset", 0x0)

        while (True):
            try:
                cpm.write("cpmt1.ipm.main.resets.tts_counters_latch", 0x1)
                val = cpm.read(reg_name)
                mantissa = val >> 28
                leftover = val & 0x0fffffff
                decoded = leftover << mantissa
                tmp0 = decoded & 0xffffffff
                tmp1 = 100. * tmp0 / 0xffffffff
                duration = 1. * decoded / 3564 / 11246
                print "{0:s} 0x{1:08x} 0x{2:x} 0x{3:08x} {4:14d} {5:6.1f} ({6:14d}, {7:.2f})" \
                    .format(datetime.datetime.utcnow().isoformat(),
                            val,
                            mantissa,
                            leftover,
                            decoded,
                            duration,
                            tmp0,
                            tmp1)
                time.sleep(1)
            except KeyBoardInterrupt:
                break

        #----------

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    description = "Periodically check a certain TTS 'time spent' counter in the CPM."

    res = Checker(description).run()

    print "Done"
    sys.exit(res)

###############################################################################
