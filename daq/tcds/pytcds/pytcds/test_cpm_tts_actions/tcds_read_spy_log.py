#!/usr/bin/env python

###############################################################################
## A little script to read the TTCSpy-in-the-PI contents.
###############################################################################

import ast
import ConfigParser
import os
import urllib2
import simplejson
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class UpdateGetter(CmdLineBase):

    def pre_hook(self):
        # The target comes from the command line.
        self.target = None
        # The following are read from the config file.
        self.targets = {}
        # End of pre_hook().

    def setup_parser(self):
        # Start with the basic parser configuration.
        super(UpdateGetter, self).setup_parser()
        # Add the option to choose a custom cfg file.
        help_str = "Configuration file to use [default: %default]"
        self.parser.add_option("--cfg-file",
                               action="store",
                               default="tcds_setup.cfg",
                               help=help_str)
        # End of setup_parser().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).

        # On the command line we require one target application.
        if len(self.args) < 1:
            self.error("One arguments is required: " \
                       "the target application.")
        else:
            self.target = self.args[0]

        self.cfg_file_name = self.opts.cfg_file
        # End of handle_args().

    def main(self):

        # Host parameters etc. come from the configuration file.
        cfg_parser = ConfigParser.SafeConfigParser()
        config_file_name = self.cfg_file_name
        if self.verbose:
            print "Reading configuration from '{0:s}'".format(config_file_name)
        if not os.path.exists(config_file_name):
            self.error("Config file '{0:s}' does not exist.".format(config_file_name))
        cfg_parser.read(config_file_name)

        # Find all possible target applications.
        targets = cfg_parser.items("targets")
        for (target_name, target_dict) in targets:
            self.targets[target_name] = ast.literal_eval(target_dict)
        if self.verbose:
            if len(self.targets):
                print "Found the following target applications: "
                target_names = self.targets.keys()
                target_names.sort()
                for target_name in target_names:
                    print "  {0:s}".format(target_name)
            else:
                print "No target applications found"

        #----------

        # Check if the target name is valid.
        if not self.target in self.targets.keys():
            msg = "'{0:s}' is not a valid target application. " \
                  "Valid targets are read from the config file '{1:s}' " \
                  "and are '{2:s}'."
            self.error(msg.format(self.target,
                                  self.cfg_file_name,
                                  "', '".join(sorted(self.targets.keys()))))

        #----------

        # Now actually do what we have been asked to do.
        target_details = self.targets[self.target]
        host_name = target_details["host"]
        port_number = target_details["port"]
        lid_number = target_details["lid"]

        tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/update"
        url = tmp.format(host_name, port_number, lid_number)

        req = urllib2.Request(url)
        opener = urllib2.build_opener()
        f = opener.open(req)
        contents = simplejson.load(f)
        spy_log_contents =  contents["itemset-ttcspylog"]["TTCSpy log contents"]
        for entry in spy_log_contents:
            msg = "Entry {0:s}: {1:s} with data {2:s} to {3:s} in BX {4:s}"
            print msg.format(entry["Entry"],
                             entry["Type"],
                             entry["Data"],
                             entry["Address"],
                             entry["BX"])

        # End of main().

    # End of class UpdateGetter.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to read the TTCSpy-in-the-PI contents."
    usage_str = "usage: %prog device"
    epilog_str_tmp = ""
    epilog_str = epilog_str_tmp.format(os.path.basename(sys.argv[0]))

    UpdateGetter(desc_str, usage_str, epilog_str).run()

    print "Done"

###############################################################################
