package xdaq.tools.win;

public interface CanEnable 
{  
  public void setEnabled(boolean isEnable);
  public boolean isEnabled();
}
