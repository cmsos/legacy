<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
 <xp:Application heartbeat="false" class="executive::Application" id="0" group="profile" service="executive" network="local" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
   <logUrl xsi:type="xsd:string">console</logUrl>
   <logLevel xsi:type="xsd:string">INFO</logLevel>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>
 <xp:Application class="pt::utcp::Application" id="20" instance="0" network="local" heartbeat="false" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:pt::utcp::Application" xsi:type="soapenc:Struct">
   <maxBlockSize xsi:type="xsd:unsignedInt">65536</maxBlockSize>
   <committedPoolSize xsi:type="xsd:double">0x17580000</committedPoolSize>
   <ioQueueSize xsi:type="xsd:unsignedInt">8192</ioQueueSize>
   <eventQueueSize xsi:type="xsd:unsignedInt">8192</eventQueueSize>
   <maxReceiveBuffers xsi:type="xsd:unsignedInt">4</maxReceiveBuffers>
   <autoConnect xsi:type="xsd:boolean">false</autoConnect>
   <protocol xsi:type="xsd:string">utcp</protocol>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libtcpla.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libptutcp.so</xp:Module>
 <xp:Application heartbeat="false" class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
   <documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
   <aliasName xsi:type="xsd:string">/directory</aliasName>
   <aliasPath xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}</aliasPath>
   <httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[3]">
    <item xsi:type="soapenc:Struct" soapenc:position="[0]">
        <name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
        <value xsi:type="xsd:string">*</value>
    </item>
    <item xsi:type="soapenc:Struct" soapenc:position="[1]">
        <name xsi:type="xsd:string">Access-Control-Allow-Methods</name>
        <value xsi:type="xsd:string">POST, GET, OPTIONS</value>
    </item>
    <item xsi:type="soapenc:Struct" soapenc:position="[2]">
        <name xsi:type="xsd:string">Access-Control-Allow-Headers</name>
        <value xsi:type="xsd:string">x-requested-with</value>
    </item>
   </httpHeaderFields>
   <expiresByType xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[5]">
   <item xsi:type="soapenc:Struct" soapenc:position="[0]">
    <name xsi:type="xsd:string">image/png</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[1]">
    <name xsi:type="xsd:string">image/jpg</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[2]">
    <name xsi:type="xsd:string">image/gif</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[3]">
    <name xsi:type="xsd:string">application/x-shockwave-flash</name>
    <value xsi:type="xsd:string">PT120H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[4]">
    <name xsi:type="xsd:string">application/font-woff</name>
    <value xsi:type="xsd:string">PT8600H</value>
   </item>
   </expiresByType>
         </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>
 <xp:Application heartbeat="false" class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" logpolicy="inherit" />
 <xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
 <!-- HyperDAQ -->
 <xp:Application heartbeat="false" class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local" logpolicy="inherit"/>
 <xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>
 <!-- XMem probe-->
 <xp:Application class="xmem::probe::Application" id="7" network="local" logpolicy="inherit"/>
 <xp:Module>${XDAQ_ROOT}/lib/libxmemprobe.so</xp:Module>
 <xp:Endpoint protocol="utcp" service="b2in" hostname="localhost" port="1911" maxport="1970" autoscan="true" network="localnet" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000"/>
 <!-- Heartbeat Probe -->
 <!-- xp:Application heartbeat="true" class="xmas::heartbeat::probe::Application" id="4" network="localnet" group="sentinel" service="heartbeatprobe" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:xmas::heartbeat::probe::Application" xsi:type="soapenc:Struct">
   <heartbeatdURL xsi:type="xsd:string">utcp://localhost:4837</heartbeatdURL>
   <heartbeatWatchdog xsi:type="xsd:string">PT10S</heartbeatWatchdog>
  </properties>
 </xp:Application -->
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasheartbeatprobe.so</xp:Module>
 <!-- XMAS Probe -->
 <!-- xp:Application heartbeat="true" class="xmas::probe::Application" id="5" network="localnet" group="xmas" service="xmasprobe" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:xmas::probe::Application" xsi:type="soapenc:Struct">
   <autoConfigure xsi:type="xsd:boolean">true</autoConfigure>
   <autoConfSearchPath xsi:type="xsd:string">${XDAQ_ROOT}/share/${XDAQ_ZONE}/sensor</autoConfSearchPath>
   <sensordURL xsi:type="xsd:string">utcp://localhost:4837</sensordURL>
   <maxReportMessageSize xsi:type="xsd:unsignedInt" >0x100000</maxReportMessageSize>
  </properties>
 </xp:Application -->
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasprobe.so</xp:Module>
 <!-- Sentinel Probe -->
 <!-- xp:Application heartbeat="true" class="sentinel::probe::Application" id="6" network="localnet" group="sentinel" service="sentinelprobe" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:sentinel::probe::Application" xsi:type="soapenc:Struct">
   <sentineldURL xsi:type="xsd:string">utcp://localhost:4837</sentineldURL>
  </properties>
 </xp:Application -->
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libsentinelutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libsentinelprobe.so</xp:Module>
<!-- include "module/access.module" -->
 <xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" port="8181" network="slimnet" publish="true" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000"/>
 
 <xp:Application heartbeat="true" class="b2in::eventing::Application"  id="42" instance="0"  network="slimnet" group="sentinel,xmas" service="b2in-eventing" publish="true" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:b2in::eventing::Application" xsi:type="soapenc:Struct">
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libb2ineventing.so</xp:Module>
 
 <xp:Application class="eventing::core::Subscriber" id="16" instance="0" network="slimnet" bus="duple" service="eventing-subscriber">
            		<properties xmlns="urn:xdaq-application:eventing::core::Subscriber" xsi:type="soapenc:Struct">
                		<eventings xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[2]">
                			<item xsi:type="xsd:string" soapenc:position="[0]">utcp://srv-s2d16-18-01.cms:8181</item>
                			<item xsi:type="xsd:string" soapenc:position="[1]">utcp://srv-s2d16-27-01.cms:9191</item>
                		</eventings>
            		</properties>
        	</xp:Application>
        	
        	
        	
  <xp:Application heartbeat="true" class="eventing::duple::Application" id="43" network="slimnet" group="central,mirror" service="duple" publish="true" logpolicy="inherit">
   <properties xmlns="urn:xdaq-eventing::duple::Application" xsi:type="soapenc:Struct">
   		<eventingBusName xsi:type="xsd:string">duple</eventingBusName>
   		<mirrorTopics xsi:type="xsd:string">bestlumi,bcm1fagghist,tcds_brildaq_data,beam</mirrorTopics>
  </properties>
 </xp:Application>
 <xp:Module>/nfshome0/lorsini/devel/baseline12/trunk/daq/eventing/duple/lib/linux/x86_64_slc6/libeventingduple.so</xp:Module>
 
 
                
 	<xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
  	<xp:Module>${XDAQ_ROOT}/lib/libeventingapi.so</xp:Module>
  	<!-- xp:Module>${XDAQ_ROOT}/lib/libeventingcore.so</xp:Module -->
 
  <xp:Module>/nfshome0/lorsini/devel/baseline12/trunk/daq/eventing/core/lib/linux/x86_64_slc6/libeventingcore.so</xp:Module>
 
 <!-- xp:Module>/nfshome0/lorsini/devel/baseline12/trunk/daq/eventing/api/lib/linux/x86_64_slc6/libeventingapi.so</xp:Module -->
</xp:Profile>
