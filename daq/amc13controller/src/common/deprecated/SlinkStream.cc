#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <iomanip>
#include "amc13/SlinkStream.hh"
#include "amc13/loggerMacros.h"
#include "amc13/EventGenerator.hh"
#include "toolbox/math/random.h"
#include <cmath>

amc13controller::SlinkStream::SlinkStream( Logger logger,
                                 amc13controller::ApplicationInfoSpaceHandler &appIS,
                                 amc13controller::FrlInfoSpaceHandler &frlIS,
                                 HAL::PCIDevice *frlDevice,
                                 HAL::PCIDevice *bridgeDevice,
                                 uint32_t linkNumber )

    : logger_( logger ),
      appIS_( appIS ),
      frlIS_( frlIS ),
      frlDevice_P( frlDevice ),
      bridgeDevice_P( bridgeDevice ),
      linkNumber_(linkNumber)
{
    std::stringstream tmp;
    tmp << "expectedFedId_" << linkNumber_;
    expectedFedsrcid_ = appIS_.getuint32( tmp.str() );
    tmp.str("");
    tmp << "expsrcId_" << linkNumber_;
    expectedFedsrcIdItem_ = tmp.str();
    tmp.str("");
    tmp << "enableStream" << linkNumber_;
    enableItem_ = tmp.str();
    tmp.str("");
    tmp << "CMCFwVersion_L" << linkNumber_;
    cmcFwItem_ = tmp.str();
    tmp.str("");
    tmp << "gen_pending_trg_L" << linkNumber_;
    pendingTriggerItem_ = tmp.str();
    cmcFwVersion_ = 0;
    DCBalanceActive_ = 0;
}


amc13controller::SlinkStream::~SlinkStream()
{

}

void amc13controller::SlinkStream::enable()
    throw( utils::exception::HardwareAccessFailed )
{
    if ( ! isActive() ) return;
    try
        {
            frlDevice_P->setBit( enableItem_ );
        }
    catch (HAL::HardwareAccessException& e)
	{
            std::stringstream msg;
            msg << "Failed to enable slink stream " << linkNumber_ << ".";
            FATAL( msg.str() );
            XCEPT_DECLARE_NESTED( utils::exception::HardwareAccessFailed, top, msg.str(), e);
            std::stringstream tag;
            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
            top.setProperty( "tag", tag.str() );
            throw( top );
        }
}

void amc13controller::SlinkStream::setExpectedSourceId()
    throw( utils::exception::HardwareAccessFailed )
{
    if ( ! isActive() ) return;

    try
        {
            frlDevice_P->write( expectedFedsrcIdItem_, expectedFedsrcid_);
        }
    catch (HAL::HardwareAccessException& e)
	{
            std::string msg = "Failed to set expected source id in FPGA";
            XCEPT_DECLARE_NESTED( utils::exception::HardwareAccessFailed, top, msg, e);
            FATAL( msg );
            std::stringstream tag;
            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
            top.setProperty( "tag", tag.str() );
            throw( top );
	}
}

//void
//amc13controller::SlinkStream::setupFedEventGen( uint32_t meansize, 
//                                      uint32_t maxsize, 
//                                      uint32_t stdevsize, 
//                                      uint32_t meandelay, 
//                                      uint32_t stdevdelay, 
//                                      uint32_t n, 
//                                      uint32_t seed ) 
//    throw ( utils::exception::HardwareAccessFailed )
//
//{
//
//    uint32_t nevent = 1;
//    while ( n > 1 ) 
//        { 
//            n /= 2;
//            nevent *= 2;
//        }
//
//    if ( nevent > 0x10000 ) nevent = 0x10000;
//
//    INFO( "Generating " << nevent << " events for stream " << linkNumber_ << "." ); 
//
//    if ( ! isActive() ) return;
//
//    uint32_t bcCount = 1;
//
//    meansize   = meansize  >> 3;       // 8 bytes granularity, in 64 bit word count
//    maxsize    = maxsize   >> 3;       // 8 bytes granularity, in 64 bit word count
//    stdevsize  = stdevsize >> 3;       // 8 bytes granularity, in 64 bit word count
//
//    if ( meansize < 3 ) meansize = 3; // at least header and trailer and one dataword
//
//    toolbox::math::LogNormalGen lognormsize( seed,
//                                             (double) meansize,
//                                             (double) stdevsize,
//                                             (uint32_t) 3, 
//                                             (uint32_t) maxsize );
//
//    toolbox::math::LogNormalGen lognormdelay( seed,
//                                              (double) meandelay,
//                                              (double) stdevdelay,
//                                              (uint32_t) 0,
//                                              (uint32_t) (100 * 0xfffff));
//    try
//        {
//            uint64_t size, delay;
//            uint32_t desc[ 4 * nevent ];
//            double sizesum = 0.0;
//            uint32_t k = 1;
//            double mk = 0;
//            double qk = 0;
//            // for checking if our random generator works:
//            INFO( "Generation of descriptors in the FRL" );
//            INFO( "All numbers below are in units of bytes!");
//            INFO( "This is the output for stream " << linkNumber_ );
//            for ( uint32_t i = 0; i < 4*nevent; i+=4 )
//                {
//                    if ( stdevsize == 0 ) size = meansize;
//                    else size = lognormsize.getRandomSize();
//                    INFO( size );
//                    if ( stdevdelay == 0 ) delay = meandelay;
//                    else delay = lognormdelay.getRandomSize();
//                    
//                    size = size << 3;
//
//                    delay = delay / 100; // 100 ns units in the Frl
//                    
//                    // The descriptors are prepared in the computer to limit the number
//                    // of hardware accesses to write them.
//                    desc[ i ] = 0x00ffffff & size;
//                    
//                    desc[ i+1 ] = ( (0x00ff & seed) << 16 ) + ( 0x0fff & expectedFedsrcid_ );
//
//                    desc[ i+2 ] =  (( 0xfffff & delay ) << 12) + (0x0fff & bcCount);
//
//                    desc[ i+3 ] = 0;
//                                        
//                    //INFO( i/4 << " size,  desc0 : " << std::dec << size << ", " << std::hex << desc[i] << " " << desc[i+1] << " " << desc[i+2] << " " << desc[i+3] );
//
//                    sizesum += size;
//                    if ( k == 1 ) {
//                        mk = size;
//                        qk = 0;
//                    } else {
//                        double mkn = mk + (size - mk)/k;
//                        qk = qk + (k-1)*(size - mk)*(size-mk)/k;
//                        mk = mkn;
//                    }
//                    bcCount++;
//                    seed++;
//                    k++;
//                }
//            k--;
//            double mean = sizesum / k;
//            double stdev = sqrt( qk / (k-1) );
//
//            INFO( "Calculated mean is  : " << mean );
//            INFO( "Calculated stdev is : " << stdev );
//            
//            std::stringstream snw, smean, sstdev;
//            snw << "Generator_NWords_L" << linkNumber_;
//            smean << "Generator_CalcWordSizeMean_L" << linkNumber_;
//            sstdev << "Generator_CalcWordSizeStdev_L" << linkNumber_;
//
//            frlIS_.setuint32( snw.str(), k, true );
//            frlIS_.setdouble( smean.str(), mean, true );
//            frlIS_.setdouble( sstdev.str(), stdev, true );
//
//            std::stringstream tmp;
//            tmp << "stream" << linkNumber_ << "_descriptor0";
//            
//            //INFO( "write the events to hardware. block length " << nevent*16 << "  in item " << tmp.str());
//            //bridgeDevice_P->writeBlock( tmp.str(), (nevent*16), (char*)desc, HAL::HAL_DO_VERIFY );
//            //INFO( "BTF done");
//
//            for ( uint32_t ic=0; ic<nevent*4; ic++)
//                {
//                    //std::cout << "write " << std::hex << desc[ic] << "  offset " << (ic*4) << std::endl;
//                    bridgeDevice_P->write( tmp.str(), desc[ic], HAL::HAL_DO_VERIFY, (ic*4));
//                }
//            //
//            //INFO( nevent << " event descriptors for dummy events in the Frl have been generated:");
//            //INFO( "    mean size   : " << meansize   << " 64bit words");
//            //INFO( "    stdev       : " << stdevsize  << " 64bit words");
//            //INFO( "    mean delay  : " << meandelay  << " ns");
//            //INFO( "    stdev delay : " << stdevdelay << " ns");            
//            
//        } 
//    catch( HAL::HardwareAccessException &e )
//        {
//            std::string msg = "Failed to write Event Desccriptor for emulator events.";
//            XCEPT_DECLARE_NESTED( utils::exception::HardwareAccessFailed, top, msg, e);
//            FATAL( msg );
//            std::stringstream tag;
//            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
//            top.setProperty( "tag", tag.str() );
//            throw( top );
//
//        }
//}


void
amc13controller::SlinkStream::testSlink( uint32_t mseconds ) 
    throw (utils::exception::SlinkTestFailed) 
{

    if ( ! isActive() ) return;
   
    uint32_t linkcnt1   = 0;
    uint32_t linkcnt2   = 0;
    uint32_t lowbit     = 0;
    uint32_t highbit    = 0;
    uint32_t uctrl_err  = 0;
    uint32_t autotest   = 99;  // test not yet performed

    try 
	{
	
	    frlDevice_P->resetBit( "DAQ_mode" );    // sets the slink in command mode
            ::usleep( 2 ); // the previous command 1.6us to send the command over the slink to both sender cards.
            frlDevice_P->write(  "TestLink" , 0 ); 
            frlDevice_P->setBit("globalFifoReset"); // This takes 80ns. In principle no delay necessary
            frlDevice_P->write( "SlinkAddress" , linkNumber_);
            frlDevice_P->write( "TestLink"    , 1 );

            // use the data counter to see if data is flowing:
            frlDevice_P->read( "LinkTestWordCounter" , &linkcnt1 ); 
            DEBUG( "Testing for " << mseconds << "ms. Initial read : " << linkcnt1 );
            ::usleep( mseconds*1000 );
            frlDevice_P->read( "LinkTestWordCounter" , &linkcnt2 ); 
            DEBUG( "Final read " << linkcnt2 );
            if ( linkcnt1 != linkcnt2 ) 
		{

                    autotest = 0;   // test has been performed

                    frlDevice_P->read( "CMC_ver", &cmcFwVersion_ );
                    frlIS_.setuint32( cmcFwItem_, cmcFwVersion_, true ); // push it al the way through! it is a NOUPDATE item

                    frlDevice_P->setBit( "SelHighBerr" ); 
                    // read data error highsignbits
                    frlDevice_P->read( "TL_Berr" , &highbit);

                    frlDevice_P->resetBit( "SelHighBerr" ); 
                    // read data error lowsignbits
                    frlDevice_P->read( "TL_Berr" , &lowbit);

                    // read error on uctrl
                    frlDevice_P->read( "UCTRL_err", &uctrl_err );

                    if ( (highbit==0) && (lowbit==0) && (uctrl_err==0) ) 
                        {
                            autotest = 1;
                        }
		}

            // end the test
            frlDevice_P->write(  "TestLink" , 0 );
            // There is the suspicion of a race condition which leads to the situation where
            // one fifo still has a data word whereas the other is reset. This then causes slink
            // crc errors in all events (rarely observed in the tracker). The suspicion is it 
            // comes from the fiforeset being issued too early (i.e. the TestLink command not being
            // completely executed yet. Therefore a ms delay here.
            ::usleep( 1000 );
            frlDevice_P->setBit( "globalFifoReset" );
	}
    catch (HAL::HardwareAccessException& e)
	{
            XCEPT_DECLARE_NESTED( utils::exception::SlinkTestFailed, top, "Failed to access FRL hardware during Slink test.", e);
            FATAL("Failed to access FRL hardware during Slink test.");
            std::stringstream tag;
            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
            top.setProperty( "tag", tag.str() );
            throw( top );
	}
	
		
    // A hack to have an error every now and then to test the software:
    //
    // double wanterr = (double)random() / (double)RAND_MAX;
    // if ( ((xdata::UnsignedInteger32T)autotest_ != 1) || (wanterr > 0.99)) 

    if ( autotest != 1 ) 
	{
            std::stringstream msg;
            msg << "Failed to test link " << linkNumber_ << " for FRL in slot "
                << appIS_.getuint32("slotNumber")
                << ". ";
            if ( autotest == 99 ) 
                {
                    msg << "No data received during test. SLINK Cable not plugged in?";
                }
            else 
                {
                    msg << "Bit errors received. Errors occured in the following bits (bit error mask): " << std::hex << std::setw(8) << std::setfill('0') << highbit << " " << std::setw(8) << lowbit;
                    if ( uctrl_err != 0 )
                        {
                            msg << " and errors (I do not know how many) in the UCTRL bit received";
                        }
                    msg << ". Altogether " << std::dec << (linkcnt2 - linkcnt1) 
                        << " 65bit words have been transfered (including utrl bit) in this test.";
                }
            FATAL( msg.str() );
            XCEPT_DECLARE( utils::exception::SlinkTestFailed, top, msg.str());
            std::stringstream tag;
            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
	    top.setProperty( "tag", tag.str() );
            throw( top );
	}
}


void 
amc13controller::SlinkStream::deskewLink() 
    throw (utils::exception::HardwareAccessFailed) 
{

    if ( ! isActive() ) return;

    if ( ! appIS_.getbool( "enableDCBalance" ) ) {
        std::string msg = "You cannot execute an deskew of the SLINK without having put the LVDS chip in DCBalance mode before. This is a configuration error.";
        ERROR( msg );
        XCEPT_RAISE( utils::exception::HardwareAccessFailed, msg );
    }

    uint32_t deskewPoll;

    try
	{	
	    frlDevice_P->resetBit( "DAQ_mode" );    // sets the slink in command mode
            ::usleep( 2 ); // the previous command takes 6*4 clocks (20MHz -> 1.2us )
            frlDevice_P->write   ( "SlinkAddress", linkNumber_ );
            frlDevice_P->setBit  ( "enableDeskew" );
            frlDevice_P->setBit  ( "execSlinkDeskew" );
            frlDevice_P->pollItem( "execSlinkDeskew" , 0, 5000, &deskewPoll );
	}
    catch( HAL::HardwareAccessException& e ) 
	{
            std::stringstream msg;
            msg <<  "Failed to deskew for SLINK (link number " << linkNumber_ << ").";
            XCEPT_DECLARE_NESTED( utils::exception::HardwareAccessFailed, top, msg.str(), e);
            std::stringstream tag;
            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
	    top.setProperty( "tag", tag.str() );

          throw( top );
	}
}


void 
amc13controller::SlinkStream::setDCBalance( bool dcflag )  
    throw (utils::exception::HardwareAccessFailed) 
{

    if ( ! isActive() ) return;

    try
	{
 	    frlDevice_P->resetBit( "DAQ_mode" );    // sets the slink in command mode
            ::usleep( 2 ); // the previous command takes 6*4 clocks (20MHz -> 1.2us )
            uint32_t dcvalue=0;
            if( dcflag ) dcvalue = 1;

            frlDevice_P->write( "SlinkAddress" , linkNumber_ );
            frlDevice_P->write( "DC_balance" , dcvalue );
            ::usleep(1000);
	}
    catch( HAL::HardwareAccessException& e) 
	{
            std::stringstream msg;
            msg << "Failed to set DC balance for SLINK (link number " << linkNumber_ << ").";
            XCEPT_DECLARE_NESTED( utils::exception::HardwareAccessFailed, top, msg.str(), e ); 

            std::stringstream tag;
            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
	    top.setProperty( "tag", tag.str() );
            
            throw( top );
	}
}

uint32_t
amc13controller::SlinkStream::pendingTriggers() 
    throw (utils::exception::HardwareAccessFailed)
{

    if ( ! this->isActive() ) return 0;

    try
        {
            uint32_t res;
            frlDevice_P->read( pendingTriggerItem_ , &res ); 
            return ( res );
        }
    catch( HAL::HardwareAccessException& e) 
	{
            std::stringstream msg;
            msg << "Failed to read the pending triggers (link number " << linkNumber_ << ").";
            XCEPT_DECLARE_NESTED( utils::exception::HardwareAccessFailed, top, msg.str(), e ); 

            std::stringstream tag;
            tag << "channel_" << linkNumber_ << ",fedsrcid_" << expectedFedsrcid_;
	    top.setProperty( "tag", tag.str() );
            
            throw( top );
	}
}

bool
amc13controller::SlinkStream::isActive()
{
    return appIS_.getbool( enableItem_ );
} 
