#ifndef _bril_vdmmonitor_ShapeData_h_
#define _bril_vdmmonitor_ShapeData_h_
#include <string>
#include <vector>
#include <sstream>

namespace bril{
  namespace vdmmonitor{
    class ShapeData{
    public:
    ShapeData():plane(0),fillnum(0),runnum(0),timestampsec_begin(0),timestampsec_end(0),avgwidth(0),avgwidth_err(0),avgpeak(0),avgpeak_err(0),avgmean(0),avgmean_err(0){        
}
      int plane; //1: X, 2:Y
      unsigned int fillnum;
      unsigned int runnum;
      unsigned int timestampsec_begin;
      unsigned int timestampsec_end;
      int crossing_angle;
      int beta_star;
      float avgwidth;
      float avgwidth_err;
      float avgpeak;
      float avgpeak_err;
      float avgmean;
      float avgmean_err;
      std::vector<float> bxproductb1b2;
      std::vector<float> bxwidth;
      std::vector<float> bxwidth_err;
      std::vector<float> bxpeak;
      std::vector<float> bxpeak_err;
      std::vector<float> bxmean;
      std::vector<float> bxmean_err;
      std::string toString() const{
	std::stringstream ss;
	ss<<"plane "<<plane<<" fill "<<fillnum<<" run "<<runnum<<" first_step_time "<<timestampsec_begin<<" last_step_time "<<timestampsec_end<<" avgwidth "<<avgwidth<< " avgwidth_err "<<avgwidth_err<< " avgpeak "<<avgpeak<< " avgpeak_err "<<avgpeak_err << " avgmean "<<avgmean<< " avgmean_err "<<avgmean_err<< " bxwidth size "<<bxwidth.size()<<" bxpeak size "<<bxpeak.size();
	return ss.str();
      }
    };
    
  }//end ns vdmmonitor
}//end ns bril
#endif
