#include "tcds/ttcspy/TTCSpy.h"

#include <memory>
#include <stdint.h>
#include <string>

#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/ttcspy/ConfigurationInfoSpaceHandler.h"
#include "tcds/ttcspy/HwStatusInfoSpaceHandler.h"
#include "tcds/ttcspy/HwStatusInfoSpaceUpdater.h"
#include "tcds/ttcspy/TCADeviceTTCSpy.h"
#include "tcds/ttcspy/TTCSpyInfoSpaceHandler.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/Monitor.h"

XDAQ_INSTANTIATOR_IMPL(tcds::ttcspy::TTCSpy);

tcds::ttcspy::TTCSpy::TTCSpy(xdaq::ApplicationStub* const stub)
try
  :
  tcds::utils::XDAQAppWithFSMBasic(stub,
                                   std::auto_ptr<tcds::hwlayer::DeviceBase>(new TCADeviceTTCSpy())),
    hwIDInfoSpaceUpdaterP_(0),
    hwIDInfoSpaceP_(0),
    hwStatusInfoSpaceUpdaterP_(0),
    hwStatusInfoSpaceP_(0),
    ttcspyInfoSpaceP_(0)
  {
    // Create the InfoSpace holding all configuration items.
    cfgInfoSpaceP_ =
      std::auto_ptr<ConfigurationInfoSpaceHandler>(new ConfigurationInfoSpaceHandler(*this));

    // Register all InfoSpaceItems with the Monitor.
    cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
    appStateInfoSpace_.registerItemSets(monitor_, webServer_);

    // Instantiate all hardware-dependent InfoSpaceHandlers and InfoSpaceUpdaters.
    hwIDInfoSpaceUpdaterP_ =
      std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
    hwIDInfoSpaceP_ =
      std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
    hwStatusInfoSpaceUpdaterP_ =
      std::auto_ptr<tcds::ttcspy::HwStatusInfoSpaceUpdater>(new tcds::ttcspy::HwStatusInfoSpaceUpdater(*this, getHw()));
    hwStatusInfoSpaceP_ =
      std::auto_ptr<tcds::ttcspy::HwStatusInfoSpaceHandler>(new tcds::ttcspy::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
    ttcspyInfoSpaceP_ = std::auto_ptr<TTCSpyInfoSpaceHandler>(new TTCSpyInfoSpaceHandler(*this, 0));
    // BUG BUG BUG
    // Needs fixing.
    // ttcspyInfoSpaceUpdaterP_ = std::auto_ptr<TTCSpyInfoSpaceUpdater>(new TTCSpyInfoSpaceUpdater(getHw()));
    // BUG BUG BUG end
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the TTCSpy application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::ttcspy::TTCSpy::~TTCSpy()
{
  hwRelease();
}

tcds::ttcspy::TCADeviceTTCSpy&
tcds::ttcspy::TTCSpy::getHw() const
{
  return static_cast<TCADeviceTTCSpy&>(*hwP_.get());
}

void
tcds::ttcspy::TTCSpy::hwConnectImpl()
{
  // BUG BUG BUG
  // std::string const deviceAddress = cfgInfoSpaceP_->getString("deviceAddress");
  // std::string const controlhubAddress = cfgInfoSpaceP_->getString("controlhubAddress");
  // uint32_t const devicePort = cfgInfoSpaceP_->getUInt32("devicePort");
  // uint32_t const controlhubPort = cfgInfoSpaceP_->getUInt32("controlhubPort");

  // getHw().hwConnect(deviceAddress, controlhubAddress, devicePort, controlhubPort);
  // BUG BUG BUG end
}

void
tcds::ttcspy::TTCSpy::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::ttcspy::TTCSpy::hwCfgInitializeImpl()
{
  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).
  if (!getHw().isTTCClockUp())
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }
}
