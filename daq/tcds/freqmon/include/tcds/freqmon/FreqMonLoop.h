#ifndef _tcds_freqmon_FreqMonLoop_h_
#define _tcds_freqmon_FreqMonLoop_h_

#include <memory>
#include <stdint.h>
#include <string>
#include <sys/types.h>

#include "toolbox/lang/Class.h"
#include "toolbox/task/WorkLoop.h"

#include "tcds/freqmon/FreqCounterReader.h"
#include "tcds/utils/Lock.h"

namespace toolbox {
  namespace task {
    class WorkLoop;
  }
}

namespace tcds {
  namespace freqmon {
    class FreqMon;
  }
}

namespace tcds {
  namespace freqmon {

    class FreqMonLoop : public toolbox::lang::Class
    {

    public:
      FreqMonLoop(tcds::freqmon::FreqMon& controller);
      virtual ~FreqMonLoop();

      void start();
      void stop();

      bool isAllOk() const;

      bool update(toolbox::task::WorkLoop* workLoop);

      double snapshot() const;

    private:
      static const useconds_t kLoopRelaxTime = 500000;

      std::string const alarmName_;
      tcds::freqmon::FreqMon& controller_;
      std::auto_ptr<toolbox::task::WorkLoop> workLoopP_;
      std::string workLoopName_;
      bool allOk_;
      std::auto_ptr<tcds::freqmon::FreqCounterReader> freqCounterReaderP_;

      mutable tcds::utils::Lock lock_;

      uint32_t freqVal_;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMonLoop_h_
