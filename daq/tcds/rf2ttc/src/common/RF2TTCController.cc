#include "tcds/rf2ttc/RF2TTCController.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/hwutilsvme/ConfigurationInfoSpaceHandlerVME.h"
#include "tcds/hwutilsvme/HwIDInfoSpaceHandlerVME.h"
#include "tcds/hwutilsvme/HwIDInfoSpaceUpdaterVME.h"
#include "tcds/hwutilsvme/Utils.h"
#include "tcds/rf2ttc/AutoModeInfoSpaceHandler.h"
#include "tcds/rf2ttc/AutoModeInfoSpaceUpdater.h"
#include "tcds/rf2ttc/BunchClockInfoSpaceHandler.h"
#include "tcds/rf2ttc/BunchClockInfoSpaceUpdater.h"
#include "tcds/rf2ttc/Definitions.h"
#include "tcds/rf2ttc/HwStatusInfoSpaceHandler.h"
#include "tcds/rf2ttc/HwStatusInfoSpaceUpdater.h"
#include "tcds/rf2ttc/OrbitInfoSpaceHandler.h"
#include "tcds/rf2ttc/OrbitInfoSpaceUpdater.h"
#include "tcds/rf2ttc/VMEDeviceRF2TTC.h"

XDAQ_INSTANTIATOR_IMPL(tcds::rf2ttc::RF2TTCController);

tcds::rf2ttc::RF2TTCController::RF2TTCController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMBasic(stub,
                                   std::auto_ptr<tcds::hwlayer::DeviceBase>(new tcds::rf2ttc::VMEDeviceRF2TTC())),
    autoModeInfoSpaceUpdaterP_(0),
    autoModeInfoSpaceP_(0),
    bunchClockInfoSpaceUpdaterP_(0),
    bunchClockInfoSpaceP_(0),
    hwIDInfoSpaceUpdaterP_(0),
    hwIDInfoSpaceP_(0),
    hwStatusInfoSpaceUpdaterP_(0),
    hwStatusInfoSpaceP_(0),
    orbitInfoSpaceUpdaterP_(0),
    orbitInfoSpaceP_(0),
    soapCmdDumpHardwareState_(*this)
  {
    // Create the InfoSpace holding all configuration information.
    cfgInfoSpaceP_ =
      std::auto_ptr<tcds::utils::ConfigurationInfoSpaceHandler>(new tcds::hwutilsvme::ConfigurationInfoSpaceHandlerVME(*this));

    // Make sure the correct default hardware configuration file is found.
    cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                              "${XDAQ_ROOT}/etc/tcds/rf2ttc/hw_cfg_default_rf2ttc.txt");
  }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the RF2TTCController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::rf2ttc::RF2TTCController::~RF2TTCController()
{
  hwRelease();
}

void
tcds::rf2ttc::RF2TTCController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-related InfoSpaceHandlers and InfoSpaceUpdaters.
  autoModeInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::rf2ttc::AutoModeInfoSpaceUpdater>(new tcds::rf2ttc::AutoModeInfoSpaceUpdater(*this, getHw()));
  autoModeInfoSpaceP_ =
    std::auto_ptr<tcds::rf2ttc::AutoModeInfoSpaceHandler>(new tcds::rf2ttc::AutoModeInfoSpaceHandler(*this, autoModeInfoSpaceUpdaterP_.get()));
  bunchClockInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::rf2ttc::BunchClockInfoSpaceUpdater>(new tcds::rf2ttc::BunchClockInfoSpaceUpdater(*this, getHw()));
  bunchClockInfoSpaceP_ =
    std::auto_ptr<tcds::rf2ttc::BunchClockInfoSpaceHandler>(new tcds::rf2ttc::BunchClockInfoSpaceHandler(*this, bunchClockInfoSpaceUpdaterP_.get()));
  hwIDInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME>(new tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::auto_ptr<tcds::hwutilsvme::HwIDInfoSpaceHandlerVME>(new tcds::hwutilsvme::HwIDInfoSpaceHandlerVME(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::rf2ttc::HwStatusInfoSpaceUpdater>(new tcds::rf2ttc::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::auto_ptr<tcds::rf2ttc::HwStatusInfoSpaceHandler>(new tcds::rf2ttc::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
  orbitInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::rf2ttc::OrbitInfoSpaceUpdater>(new tcds::rf2ttc::OrbitInfoSpaceUpdater(*this, getHw()));
  orbitInfoSpaceP_ =
    std::auto_ptr<tcds::rf2ttc::OrbitInfoSpaceHandler>(new tcds::rf2ttc::OrbitInfoSpaceHandler(*this, orbitInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor and the WebServer.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  bunchClockInfoSpaceP_->registerItemSets(monitor_, webServer_);
  orbitInfoSpaceP_->registerItemSets(monitor_, webServer_);
  autoModeInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

tcds::rf2ttc::VMEDeviceRF2TTC&
tcds::rf2ttc::RF2TTCController::getHw() const
{
  return static_cast<tcds::rf2ttc::VMEDeviceRF2TTC&>(*hwP_.get());
}

void
tcds::rf2ttc::RF2TTCController::hwConnectImpl()
{
  tcds::hwutilsvme::vmeDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::rf2ttc::RF2TTCController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::rf2ttc::RF2TTCController::hwCfgInitializeImpl()
{
  // Configure the TTCrx chip to enable its Dout bus carrying the LHC
  // machine from from the BST.
  getHw().enableBST();
}

void
tcds::rf2ttc::RF2TTCController::hwCfgFinalizeImpl()
{
  VMEDeviceRF2TTC const& hw = getHw();

  //----------

  // The Delay25 GCR registers should not be messed with. In principle
  // these should be the default values, but it does not hurt to make
  // sure.
  // - The clock selection: 40 MHz.
  hw.writeRegister("bc_delay25_gcr", 0x0);
  hw.writeRegister("orbin_delay25_gcr", 0x0);
  hw.writeRegister("orbout_delay25_gcr", 0x0);

  //----------

  // If the QPLLs are not locked: reset them. If it helps: great. If
  // not: nothing is lost.
  for (int bcSignal = tcds::definitions::kBCSignalMin;
       bcSignal <= tcds::definitions::kBCSignalMax;
       ++bcSignal)
    {
      tcds::definitions::BC_SIGNAL tmp = static_cast<tcds::definitions::BC_SIGNAL>(bcSignal);
      if (!hw.isQPLLLocked(tmp))
        {
          hw.resetQPLL(tmp);
        }
    }
}

void
tcds::rf2ttc::RF2TTCController::coldResetActionImpl(toolbox::Event::Reference event)
{
  // Connect to the hardware.
  hwConnect();

  // Reset the board.
  getHw().resetBoard();

  // Release the hardware.
  hwRelease();

  // Reconnect to the hardware. This method will poll while trying to
  // reconnect, so this is a nice way to wait till the board is back
  // online.
  hwConnect();
  // Finally release to clean up.
  hwRelease();
}

void
tcds::rf2ttc::RF2TTCController::zeroActionImpl(toolbox::Event::Reference event)
{
  bunchClockInfoSpaceUpdaterP_->resetCounters();
  orbitInfoSpaceUpdaterP_->resetCounters();
}

bool
tcds::rf2ttc::RF2TTCController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  // NOTE: This only checks for writability.
  bool res = tcds::utils::XDAQAppBase::isRegisterAllowed(regInfo);

  // Only the 'xx_delay25_yyy_val' registers are allowed. The rest is handled
  // in-house.
  // NOTE: This is very special, since these registers are
  // strictly-speaking read-only.
  if (regInfo.name().find("delay25") != std::string::npos)
    {
      res = toolbox::endsWith(regInfo.name(), "_val");
    }

  // The Delay25 GCR registers should not be messed with.
  res = res && (regInfo.name().find("gcr") == std::string::npos);

  return res;
}
