/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: frl-types.h,v 1.1 2004/07/05 09:35:08 cschwick Exp $
*/
/** @file fedkit-types.h 
 * Headers used to handle the file-types pain.
 * Documentation can be found at http://cern.ch/cano/fedkit
 */

#ifndef _FRL_TYPES_H_ 
#define _FRL_TYPES_H_
/* We try to get away with the i2o headers. Everybody uses them, so
   using something else with the same names would lead to chaos */
#include <i2o/i2o.h>

#ifdef __KERNEL__
	#include <linux/kernel.h>
#else
	#include <stdio.h>
#endif

/* Various versions of printf (eprintf for errors, iprintf for information) */

#ifdef __GNUC__
	#ifdef __KERNEL__
		#define eprintf(format, args...) printk (KERN_ERR format , ## args)
		#define iprintf(format, args...) printk (KERN_INFO format , ## args)
	#else /* defined __KERNEL__ */
		#define eprintf(format, args...) fprintf (stderr, format , ## args)
		#define iprintf(format, args...) printf (format , ## args)
	#endif /* defined __KERNEL__ */
#else /*defined __GNUC__ */ /* if it's not gnu C, let's try the C standard way */
	#ifdef __KERNEL__
		#define eprintf(...) printk (KERN_ERR __VA_ARGS__)
		#define iprintf(...) printk (KERN_INFO __VA_ARGS__)
	#else /* defined __KERNEL__ */
		#define eprintf(...) fprintf (stderr, __VA_ARGS__)
		#define iprintf(...) printf (__VA_ARGS__)
	#endif /* defined __KERNEL__ */
#endif /*defined __GNUC__ */

#ifdef I2ODEBUG
	#ifdef __GNUC__
		#ifdef __KERNEL__
			#define idprintf(format, args...)\
				printk (KERN_INFO __FILE__ " :%d:" format , __LINE__ , ## args)
		#else /* defined __KERNEL__ */
			#define idprintf(format, args...)\
				printf (__FILE__ " :%d:" format , __LINE__ , ## args)
		#endif /* defined __KERNEL__ */
	#else /*defined __GNUC__ */ /* if it's not gnu C, let's try the C standard way */
		#ifdef __KERNEL__
			#define idprintf(...) printk (KERN_INFO __VA_ARGS__)
		#else /* defined __KERNEL__ */
			#define idprintf(...) printf (__VA_ARGS__)
		#endif /* defined __KERNEL__ */
	#endif /*defined __GNUC__ */
#else /* def I2ODEBUG */
	#ifdef __GNUC__
		#define idprintf(args...) {}
	#else /*defined __GNUC__ */ /* if it's not gnu C, let's try the C standard way */
		#define idprintf(...) {}
	#endif /*defined __GNUC__ */
#endif /* def I2ODEBUG */

#endif /* ndef _FRL_TYPES_H_ */
