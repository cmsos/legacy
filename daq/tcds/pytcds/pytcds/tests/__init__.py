###############################################################################

import ConfigParser
import functools
import nose
import StringIO

###############################################################################

# The name of the file to read to figure out the test setup
# configuration.
CFG_FILE_NAME = "test_settings.cfg"

# Some default values for the contents of the above file (for the
# variables for which this makes sense).
DEFAULT_SETTINGS = StringIO.StringIO("""\
[tests]
quick_test = False
ping_deadline = 1
bxs_to_test = [0, 1, 42, 3563]

[controlhub]
address = 127.0.0.1
port = 10203
ici_connected_to_ttcspy = 1

[glib]
address = 192.168.0.161
port = 50001

[cpm]
address = 192.168.0.161
port = 50001

[lpm]
address = 192.168.0.161
port = 50001
num_icis = 2

[ipm]
num_cyclic_generators = 16

[ici]
num_cyclic_generators = 8

[ici]
address = 192.168.0.172
port = 50001

[ttcspy]
address = 192.168.0.172
port = 50001
""")

###############################################################################

def attr(*args, **kwargs):
    """Decorator that adds attributes to objects.

    For use with the nose attribute (-a) plugin.

    """
    def wrap(func):
        for name in args:
            # These are just flags:
            setattr(func, name, True)
        func.__dict__.update(kwargs)
        return func
    # End of attr().
    return wrap

###############################################################################

def expected_failure(test):
    """Decorator for nose tests that are expected to fail."""
    @functools.wraps(test)
    def inner(*args, **kwargs):
        try:
            test(*args, **kwargs)
        except Exception, err:
            raise nose.SkipTest("Test expected to fail.")
        else:
            raise AssertionError("Test expected to fail, but succeeded.")
    # End of expected_failure().
    return inner

###############################################################################

class Bunch:
    """Simple place-holder to group variables."""
    def __init__(self, **kwds):
        self.__dict__.update(kwds)
    # End of class Bunch.

###############################################################################

class Settings(object):

    def __init__(self):
        parser = ConfigParser.SafeConfigParser()
        parser.readfp(DEFAULT_SETTINGS)
        files_read = parser.read(CFG_FILE_NAME)
        if not files_read:
            msg = "Configuration file '{0:s}' not found. " \
                  "Using default configuration."
            print msg.format(CFG_FILE_NAME)
        for section in parser.sections():
            for (item, value) in parser.items(section):
                # Figure out the type of the parameter. A bit clunky,
                # but okay.
                value_typed = None
                if value.find("[") > -1:
                    tmp = value.replace("[", "").replace("]", "")
                    tmp = tmp.split(",")
                    value_typed = [self.type_parameter(i) for i in tmp]
                else:
                    value_typed = self.type_parameter(value)
                # Now store the parameter in the settings.
                tmp = None
                try:
                    tmp = getattr(self, section)
                except AttributeError:
                    setattr(self, section, Bunch())
                    tmp = getattr(self, section)
                setattr(tmp, item, value_typed)
        # End of __init__().

    def type_parameter(self, value):
        if value == "":
            value_typed = None
        elif value.lower() in ["yes", "true"]:
            value_typed = True
        elif value.lower() in ["no", "false"]:
            value_typed = False
        else:
            value_typed = value
            try:
                value_typed = float(value)
                # NOTE: If it can be represented as an integer, store
                # it as an integer. Otherwise store it as a float.
                if float(value) == int(value):
                    value_typed = int(value)
            except ValueError:
                pass
        # End of type_parameter().
        return value_typed

    # End of class Settings.

###############################################################################

settings = Settings()

###############################################################################
