BUILD_HOME:=$(shell pwd)/../../../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

ifndef XAAS_ROOT
XAAS_ROOT=$(BUILD_HOME)/$(PROJECT_NAME)/xaas
endif
export XAAS_ROOT

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
PackageName=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF-1]}')
PackageType=$(shell pwd | awk -F"/" '{split($$0,a,"/");  print a[NF-2]}')
Package=xaas/$(PackageType)/$(PackageName)

PACKAGE_VER_MAJOR=5
PACKAGE_VER_MINOR=1
PACKAGE_VER_PATCH=0

ifndef BUILD_VERSION
BUILD_VERSION=1
endif

# As a transition measure between XDAQ14 (gcc 4.8, C++98) and XDAQ15
# (gcc 7.2, C++14), let's build in some wiggle room.
# Useful choices should be one of:
# - XDAQ_TARGET_XDAQ14
# - XDAQ_TARGET_XDAQ15
ifndef XDAQ_TARGET
  export XDAQ_TARGET=XDAQ_TARGET_XDAQ14
endif

UserCPPFlags += -D$(XDAQ_TARGET)

TEMPLATEDIR=$(XAAS_ROOT)/template/zone

build: _buildall

_buildall: all

_all: all

default: all

all:
	make -f $(TEMPLATEDIR)/zone.makefile UserCPPFlags=$(UserCPPFlags) ZONE_NAME=$(PackageName) PACKAGE_TYPE=$(PackageType)
_installall: install

install:

_cleanall: clean

clean:
	make -f $(TEMPLATEDIR)/zone.makefile UserCPPFlags=$(UserCPPFlags) ZONE_NAME=$(PackageName) PACKAGE_TYPE=$(PackageType) clean

_rpmall: rpm

rpm:
	make -f $(TEMPLATEDIR)/zone.makefile UserCPPFlags=$(UserCPPFlags) ZONE_NAME=$(PackageName) PACKAGE_TYPE=$(PackageType) BUILD_VERSION=$(BUILD_VERSION) PACKAGE_VER_MAJOR=$(PACKAGE_VER_MAJOR) PACKAGE_VER_MINOR=$(PACKAGE_VER_MINOR) PACKAGE_VER_PATCH=$(PACKAGE_VER_PATCH) rpm

_installrpmall: installrpm

installrpm:
	make -f $(TEMPLATEDIR)/zone.makefile UserCPPFlags=$(UserCPPFlags) ZONE_NAME=$(PackageName) PACKAGE_TYPE=$(PackageType) installrpm

_cleanrpmall: cleanrpm

cleanrpm:
	make -f $(TEMPLATEDIR)/zone.makefile UserCPPFlags=$(UserCPPFlags) ZONE_NAME=$(PackageName) PACKAGE_TYPE=$(PackageType) cleanrpm
