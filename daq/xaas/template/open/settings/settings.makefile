# $Id: Makefile,v 1.110 2009/05/29 13:15:07 rmoser Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# 
# 
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../../../..

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

PackageName=xaas-$(ZONE_NAME)-settings
Project=$(PROJECT_NAME)
Package=xaas/open/$(ZONE_NAME)/settings



Summary=Setting for $(ZONE_NAME) setup 

Description=This is a configuration (flashlist and other settings) for $(ZONE_NAME) 

Link=http://xdaq.web.cern.ch
#
# Template instantiate value, as per zone
#
TEMPLATEDIR=$(XAAS_ROOT)/template/open/settings

_all: all

default: all

all: clean
	cp $(TEMPLATEDIR)/spec.template .
	perl -p -i -e 's#__zonename__#$(ZONE_NAME)#' spec.template
	if [ -e $(BUILD_HOME)/$(Project)/$(Package)/etc/tracerfilter.xml ]; then \
		echo "Using user tracer filter"; \
	else \
		cp $(TEMPLATEDIR)/etc/tracerfilter.xml $(BUILD_HOME)/$(Project)/$(Package)/etc/; \
	fi

_cleanall: clean

clean:
	-rm -f spec.template

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfSetupRPM.rules
