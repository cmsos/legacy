/*
 * ComplexParamTest
 *
 * $Id: ComplexParamTest.java,v 4.1 2007/03/26 14:40:33 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import java.util.List;
import junit.framework.*;
import junit.extensions.TestSetup;

// DOM
import org.w3c.dom.*;

/**
 * ComplexParamTest
 */
public class ComplexParamTest extends TestCase
{
	private static final long WAIT = 1000; // [ms]

	final String PARAMETER_HEADER = 
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<p:properties " +
				"xmlns:p=\"urn:xdaq-application:TestParam\" " +
				"xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding\" " +
				"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
				"xsi:type=\"soapenc:Struct\">";

	final String PARAMETER_TRAILER = 
				"</p:properties>\n";

	public ComplexParamTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(ComplexParamTest.class);
	}

	public void testConstructors() throws Exception
	{
		XDAQParameter parameter;
		
		// (String endpoint, String applicationClass, int instance)
		parameter = new XDAQParameter("http://localhost:40000", "TestParam", 0);
		assertNotNull("Failed to create a XDAQParameter", parameter);
		assertNotNull("XDAQParameter is empty", parameter.getDOMParameter());

		System.out.println("==== string dump of a complex parameter structure");
		System.out.println(TestUtil.domToString(parameter.getDOMParameter()));
	}

	public void testSelect() throws Exception
	{
		XDAQParameter parameter;
		String good_result, result;

		parameter = new XDAQParameter("http://localhost:40000", "TestParam", 0);
		parameter.select("vector");

		good_result = PARAMETER_HEADER +
				"<p:vector soapenc:arrayType='xsd:ur-type[3]' xsi:type='soapenc:Array'>" +
				"<p:item soapenc:position='[0]' xsi:type='xsd:integer'>1</p:item>" +
				"<p:item soapenc:position='[1]' xsi:type='xsd:integer'>2</p:item>" +
				"<p:item soapenc:position='[2]' xsi:type='xsd:integer'>3</p:item>" +
				"</p:vector>" + 
				PARAMETER_TRAILER;

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		parameter = new XDAQParameter("http://localhost:40000", "TestParam", 0);
		parameter.select("d");

		good_result = PARAMETER_HEADER +
				"<p:bag xsi:type='soapenc:Struct'>" +
				"<p:d xsi:type='xsd:double'>1.000000e-01</p:d>" +
				"</p:bag>" + 
				PARAMETER_TRAILER;

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testSelectMany() throws Exception
	{
		XDAQParameter parameter;
		String good_result, result;

		parameter = new XDAQParameter("http://localhost:40000", "TestParam", 0);
		parameter.select(new String[] { "string", "d" });

		good_result = PARAMETER_HEADER +
				"<p:bag xsi:type='soapenc:Struct'>" +
				"<p:d xsi:type='xsd:double'>1.000000e-01</p:d>" +
				"</p:bag>" + 
				"<p:string xsi:type='xsd:string'>String</p:string>" +
				PARAMETER_TRAILER;

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testRemove() throws Exception
	{
		XDAQParameter parameter;
		String good_result, result;

		parameter = new XDAQParameter("http://localhost:40000", "TestParam", 0);
		parameter.remove("vector");
		parameter.remove("v");

		good_result = PARAMETER_HEADER +
				"<p:bag xsi:type='soapenc:Struct'>" +
				"<p:d xsi:type='xsd:double'>1.000000e-01</p:d>" +
				"</p:bag>" + 
				"<p:bool xsi:type='xsd:boolean'>true</p:bool>" +
				"<p:double xsi:type='xsd:double'>1.100000e+24</p:double>" +
				"<p:float xsi:type='xsd:float'>0.001200</p:float>" +
				"<p:int xsi:type='xsd:integer'>-255</p:int>" +
				"<p:string xsi:type='xsd:string'>String</p:string>" +
				"<p:ulong xsi:type='xsd:unsignedLong'>2779096485</p:ulong>" +
				PARAMETER_TRAILER;

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testVector() throws Exception
	{
		XDAQParameter parameter;
		parameter = new XDAQParameter("http://localhost:40000", "TestParam", 0);
		parameter.select("vector");

		String[] array;

		// check the contents
		array = parameter.getVector("vector");
		assertEquals(3, array.length);
		assertEquals("2", array[1]);

		array[1] = "-1";
		parameter.setVector("vector", array);
		array[1] = "-9";

		// check the contents before send()
		array = parameter.getVector("vector");
		assertEquals("-1", array[1]);

		// send, confirming the current values are overwritten by get()
		parameter.send();
		array[1] = "-9";
		parameter.setVector("vector", array);
		parameter.get();

		// check the contents after send()
		array = parameter.getVector("vector");
		assertEquals("-1", array[1]);

		// resize - lengthen
		array = new String[] { "0", "1", "2", "3", "4" };
		parameter.setVector("vector", array);
		array = parameter.getVector("vector");
		assertEquals(5, array.length);

		parameter.send();
		parameter.get();
		array = parameter.getVector("vector");
		assertEquals(5, array.length);
		assertEquals("0", array[0]);

		// resize - shorten
		array = new String[] { "-1", "0" };
		parameter.setVector("vector", array);
		array = parameter.getVector("vector");
		assertEquals(2, array.length);

		parameter.send();
		parameter.get();
		array = parameter.getVector("vector");
		assertEquals(2, array.length);
		assertEquals("-1", array[0]);

		// zero-length vector
		array = new String[0];
		parameter.setVector("vector", array);

		String good_result = PARAMETER_HEADER +
				"<p:vector soapenc:arrayType='xsd:ur-type[0]' xsi:type='soapenc:Array'>" +
				"</p:vector>" + 
				PARAMETER_TRAILER;

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		array = parameter.getVector("vector");
		assertEquals(0, array.length);

		parameter.send();
		parameter.get();
		array = parameter.getVector("vector");
		assertEquals(0, array.length);

		// non-vector
		assertNull(parameter.getVector("string"));
	}

	public void testGetNames() throws Exception
	{
		XDAQParameter parameter;
		parameter = new XDAQParameter("http://localhost:40000", "TestParam", 0);

		List<String> names = parameter.getNames();

		assertNotNull(names);
		assertEquals(6, names.size());
		assertFalse(names.contains("bag"));
		assertFalse(names.contains("vector"));
	}


	private static void oneTimeSetUp() throws Exception
	{
		Runtime.getRuntime().exec("./test/xdaq/bootxdaq start").waitFor();
		Thread.sleep(WAIT);

		XDAQMessage message = new XDAQMessage(
				"Configure", TestUtil.fileToDOM("test/xdaq/param.xml"));
		message.send("http://localhost:40000", 0);
	}

	private static void oneTimeTearDown() throws Exception
	{
		Runtime.getRuntime().exec("./test/xdaq/bootxdaq stop").waitFor();
		Thread.sleep(WAIT);
	}

	public static Test suite()
	{
		TestSuite suite = new TestSuite(ComplexParamTest.class);

		TestSetup wrapper = new TestSetup(suite) {
			protected void setUp() throws Exception { oneTimeSetUp() ; }
			protected void tearDown() throws Exception { oneTimeTearDown(); }
		};

		return wrapper;
	}
}

// End of file
// vim: set ts=4 sw=4:
