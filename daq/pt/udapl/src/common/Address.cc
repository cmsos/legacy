// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <sstream>

#include "pt/udapl/Address.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"

// only for debugging inet_ntoa

pt::udapl::Address::Address (const std::string & url, const std::string& service)
	: url_(url)
{
	if (service_ != "i2o")
	{
		// std::string msg = "Cannot create pt::udapl::Address from url, unsupported service ";
		// msg += service;
		// XCEPT_RAISE (pt::exception::Exception, msg);
	}
	service_ = service;
}

pt::udapl::Address::~Address ()
{
}

std::string pt::udapl::Address::getService ()
{
	return service_;
}

std::string pt::udapl::Address::getProtocol ()
{
	return url_.getProtocol();
}

std::string pt::udapl::Address::toString ()
{
	return url_.toString();
}

std::string pt::udapl::Address::getURL ()
{
	return url_.toString();
}

std::string pt::udapl::Address::getHost ()
{
	return url_.getHost();
}

std::string pt::udapl::Address::getPort ()
{
	std::ostringstream o;
	if (url_.getPort() > 0) o << url_.getPort();
	return o.str();
}

unsigned short pt::udapl::Address::getPortNum ()
{
	return (url_.getPort());
}

std::string pt::udapl::Address::getPath ()
{
	std::string path = url_.getPath();
	if (path.empty())
	{
		return "/";
	}
	else
	{
		if (path[0] == '/')
		{
			return path;
		}
		else
		{
			path.insert(0, "/");
			return path;
		}
	}
}

std::string pt::udapl::Address::getServiceParameters ()
{
	return url_.getPath();
}

bool pt::udapl::Address::equals (pt::Address::Reference address)
{
	return ((this->toString() == address->toString()) && (this->getService() == address->getService()));
}
