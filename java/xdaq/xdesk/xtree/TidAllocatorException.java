package xdaq.xdesk.xtree;

public class TidAllocatorException extends Exception
{
	TidAllocatorException () { super(); }

	TidAllocatorException ( String msg )
	{
		super (msg);
	}
}
