#ifndef _bril_bhmsource_WebUtils_h_
#define _bril_bhmsource_WebUtils_h_

#include <string>
#include <vector>
#include <tuple>
#include "xdata/Serializable.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Output.h"

namespace bril
 {

  namespace bhmsource
   {

    namespace WebUtils
     {

      inline std::string to_string( int x )
       {
        return std::to_string( (long long)x );
       }

      inline std::string to_string( unsigned int x )
       {
        return std::to_string( (unsigned long long)x );
       }

      inline std::string to_string( double x )
       {
        return std::to_string( (long double)x );
       }

      inline std::string to_string( std::string x )
       {
        return x;
       }

      inline std::string to_string( const xdata::Serializable* x )
       {
//        return const_cast<xdata::Serializable*>( x )->toString(); //correct constness is fixed in newer version of xdata
        return x->toString();
       }

      template <typename T>
      inline std::string to_string( const std::vector<T>& vec, std::size_t start = 0 )
       {
        if ( vec.size() <= start )
          return std::string();
        if ( vec.size() == start + 1 )
          return to_string( vec[start] );
        return to_string( vec[start] ) + ", " + to_string( vec, start + 1 );
       }

      namespace
       {

        using namespace cgicc;

        template <typename IterTuple, std::size_t N>
        struct TuplePrinter
         {
          static void print( xgi::Output* out, IterTuple& t )
           {
            TuplePrinter<IterTuple, N - 1>::print( out, t );
            *out << td( to_string( *std::get<N - 1>( t ) ) );
            std::get<N - 1>( t )++;
           }
         };

        template <typename IterTuple>
        struct TuplePrinter<IterTuple, 1>
         {
          static void print( xgi::Output* out, IterTuple& t )
           {
            *out << td( to_string( *std::get<0>( t ) ) );
            std::get<0>( t )++;
           }
         };

        template <typename HeadIter, typename Container>
        void printLine( xgi::Output* out, bool dohdr, HeadIter hdr, const Container& c )
         {
          *out << tr();
          if ( dohdr )
            *out << th( *hdr );
          for ( auto it = c.begin(); it != c.end(); ++it )
            *out << td( to_string( *it ) );
          *out << tr() << std::endl;
         }

        template <typename HeadIter, typename Container, typename... OtherLines>
        void printLine( xgi::Output* out, bool dohdr, HeadIter hdr, const Container& c, OtherLines... others )
         {
          printLine( out, dohdr, hdr, c );
          if ( dohdr )
            ++hdr;
          printLine( out, dohdr, hdr, others... );
         }

       }

    /**
     * Outputs to @param out an html table from the specified @param head and @param others columns (which must all have the same length).
     * The optional horizontal header is specified in @param titles.
     * It is only used if it has as many string as there would be columns in the table (sizeof...(others) + 1), otherwise no header row is generated.
     * @param head can be any stl-like container of strings (i.e. something with begin() and end())
     * @param others can be one or more stl-like container of anything that can be converted to string
     */
      template <typename Titles, typename HeaderT, typename... OtherColumns>
      void cgiVerticalTable( xgi::Output* out, Titles titles, HeaderT head, OtherColumns... others )
       {
        *out << cgicc::table().set( "class", "xdaq-table-vertical" );
        if ( titles.size() == sizeof...( others ) + 1 )
         {
          *out << cgicc::thead() << cgicc::tr();
          for ( auto hit = titles.begin(); hit != titles.end(); ++hit )
            *out << cgicc::th( *hit );
          *out << cgicc::tr() << cgicc::thead() << std::endl;
         }
        *out << cgicc::tbody() << std::endl;
        auto iterators = std::make_tuple( others.begin()... );
        for ( auto hit = head.begin(); hit != head.end(); ++hit )
         {
          *out << cgicc::tr() << cgicc::th( *hit );
          TuplePrinter<decltype( iterators ), sizeof...( others )>::print( out, iterators );
          *out << cgicc::tr() << std::endl;
         }
        *out << cgicc::tbody() << cgicc::table() << std::endl;
       }

    /**
     * Outputs to @param out an html table from the specified @param head and @param others rows (which must all have the same length).
     * The optional vertical header is specified in @param titles.
     * It is only used if it has as many string as there would be rows in the table (sizeof...(others) + 1), otherwise no header column is generated.
     * @param head can be any stl-like container of strings (i.e. something with begin() and end())
     * @param others can be one or more stl-like container of anything that can be converted to string
     */
      template <typename Titles, typename HeaderT, typename... OtherColumns>
      void cgiHorizontalTable( xgi::Output* out, Titles titles, HeaderT head, OtherColumns... others )
       {
        *out << cgicc::table().set( "class", "xdaq-table" );
        *out << cgicc::thead() << cgicc::tr();
        auto tb = titles.begin();
        if ( titles.size() == ( sizeof...( others ) + 1 ) )
          *out << cgicc::th( *tb++ );
        for ( auto hit = head.begin(); hit != head.end(); ++hit )
          *out << cgicc::th( *hit );
        *out << cgicc::tr() << cgicc::thead() << std::endl << cgicc::tbody() << std::endl;
        printLine( out, ( titles.size() == ( sizeof...( others ) + 1 ) ), tb, others... );
        *out << cgicc::tbody() << cgicc::table() << std::endl;
       }

     }

   }

 }

#endif
