// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/blit/PeerTransport.h"
#include "tcpla/PublicServicePoint.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"

#include "pt/blit/EventHandler.h"



#include "tcpla/Utils.h"


pt::blit::EventHandler::EventHandler ( xdaq::Application * owner, size_t maxClients , size_t maxPSPs) : xdaq::Object(owner)
{
	dispatcher_.resize(maxClients*maxPSPs);

	for ( std::vector<pt::blit::Dispatcher*>::iterator j = dispatcher_.begin(); j != dispatcher_.end(); j++ )
	{
			(*j) = 0;
	}

	for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++ )
	{
				eventCounter_[i] = 0;
	}

}

pt::blit::EventHandler::~EventHandler ()
{


}

void pt::blit::EventHandler::setDispatcher ( int index, pt::blit::Dispatcher * d  )
{
	dispatcher_[index] = d;
}

pt::blit::Dispatcher * pt::blit::EventHandler::getDispatcher ( int index  )
{
	return dispatcher_[index];
}


// this callback can be invoked by several threads
void pt::blit::EventHandler::handleEvent ( tcpla::Event & event )
{
	// All event handling goes here

	eventCounter_[event.event_number]++;

	switch ( event.event_number )
	{
		case tcpla::TCPLA_DTO_RCV_COMPLETION_EVENT:
		{
			//int fd = event.event_data.dto_completion_event_data.psp->getFD(event.event_data.dto_completion_event_data.ep_handle);
			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_cookie.as_ptr;
			//int sid = event.event_data.dto_completion_event_data.psp->indexId_ + event.event_data.dto_completion_event_data.ep_handle.pollfdsKey_;
			int sid = event.event_data.dto_completion_event_data.ep_handle.pollfdsKey_;
			int index = event.event_data.dto_completion_event_data.psp->indexId_;
			//tcpla::EndPoint  ep_handle = event.event_data.dto_completion_event_data.ep_handle;
			size_t len = event.event_data.dto_completion_event_data.transfered_length;

			try
			{
				dispatcher_[index]->processIncomingMessage(ref, len, sid);

				//dispatcher_[index]->processIncomingMessage(ref, index, sid, ep_handle, len);
			}
			catch(pt::blit::exception::Exception & ex)
			{
				ref->release();

				std::stringstream msg;
				msg << "Failed to process incoming data";
				XCEPT_DECLARE_NESTED(pt::blit::exception::Exception, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}


		}
		break;

		case tcpla::TCPLA_CONNECTION_REQUEST_EVENT: // server only
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection request");

			int index = event.event_data.cr_event_data.psp->indexId_;

			try
			{
				dispatcher_[index]->connectionRequest(event.event_data.cr_event_data.psp->ia_, event.event_data.cr_event_data.psp, event.event_data.cr_event_data.cr_handle);
			}
			catch(pt::blit::exception::Exception & ex)
			{
					std::stringstream msg;
					msg << "Failed connection acceptance";
					XCEPT_DECLARE_NESTED(pt::blit::exception::Exception, e, msg.str(), ex);
					this->getOwnerApplication()->notifyQualified("fatal", e);
			}


		}
		break;


		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPTED:
		{

			tcpla::EndPoint ep_handle = event.event_data.connect_event_data.ep_handle;
			//int sid = ep_handle.psp_->indexId_ + event.event_data.connect_event_data.ep_handle.pollfdsKey_;
			int sid = event.event_data.connect_event_data.ep_handle.pollfdsKey_;

			int fd = ep_handle.psp_->getFD(event.event_data.connect_event_data.ep_handle);
			int index = ep_handle.psp_->indexId_;

			try
			{
				dispatcher_[index]->connectionAccepted(index, sid, fd, ep_handle);
			}
			catch(pt::blit::exception::Exception & ex)
			{
				event.event_data.connect_event_data.ep_handle.psp_->ia_->destroyEndPoint(ep_handle);

				std::stringstream msg;
				msg << "Failed connection acceptance";
				XCEPT_DECLARE_NESTED(pt::blit::exception::Exception, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);
			}

		}
		break;


		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
		break;

		case tcpla::TCPLA_CONNECTION_CLOSED_BY_PEER:
		{
			tcpla::EndPoint ep_handle = event.event_data.connection_broken_event_data.ep_handle;
			//int sid = ep_handle.psp_->indexId_ + event.event_data.connect_event_data.ep_handle.pollfdsKey_;
			int sid = ep_handle.pollfdsKey_;
			int fd = ep_handle.psp_->getFD(ep_handle);
			int index = ep_handle.psp_->indexId_;

			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
				//tolerant behaviour
				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				dispatcher_[index]->connectionClosedByPeer(index, sid, fd, ep_handle);
			}
			catch(pt::blit::exception::Exception & ex)
			{
				event.event_data.connect_event_data.ep_handle.psp_->ia_->destroyEndPoint(ep_handle);

				std::stringstream msg;
				msg << "Failed notifying connection closed by peer";
				XCEPT_DECLARE_NESTED(pt::blit::exception::Exception, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);
			}

		}
		break;

		case tcpla::TCPLA_CONNECTION_EVENT_BROKEN: // server only
		{
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));

				delete event.event_data.connection_broken_event_data.exception;
			}

			//pt::blit::PipeConnectionBroken broken;
			//pipeServiceListener_->pipeServiceEvent(pt::blit::PipeConnectionBroken);

		}
		break;

		case tcpla::TCPLA_CONNECTION_RESET_BY_PEER: //server only
		{
			tcpla::EndPoint ep_handle = event.event_data.connection_broken_event_data.ep_handle;

			int sid = ep_handle.pollfdsKey_;
			int fd = ep_handle.psp_->getFD(ep_handle);
			int index = ep_handle.psp_->indexId_;

			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));

				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
					dispatcher_[index]->connectionResetByPeer(index, sid, fd, ep_handle);
			}
			catch(pt::blit::exception::Exception & ex)
			{
					event.event_data.connect_event_data.ep_handle.psp_->ia_->destroyEndPoint(ep_handle);

					std::stringstream msg;
					msg << "Failed notifying connection closed by peer";
					XCEPT_DECLARE_NESTED(pt::blit::exception::Exception, e, msg.str(), ex);
					this->getOwnerApplication()->notifyQualified("fatal", e);
			}


		}
		break;

		case tcpla::TCPLA_CONNECTION_EVENT_TIMED_OUT:

		break;

		case tcpla::TCPLA_CONNECTION_EVENT_UNREACHABLE:
		{
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));

				delete event.event_data.connection_broken_event_data.exception;
			}

		}
		break;

		case tcpla::TCPLA_ASYNC_ERROR_EVD_OVERFLOW:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
		break;

		case tcpla::TCPLA_ASYNC_ERROR_IA_CATASTROPHIC:
		{


			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
		break;

		case tcpla::TCPLA_ASYNC_ERROR_EP_BROKEN:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
		break;

		case tcpla::TCPLA_ASYNC_ERROR_TIMED_OUT:

		break;

		case tcpla::TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}

			//std::stringstream ss;

			//ss << "Internal error event reason: " << event.event_data.asynch_error_event_data.reason;

			//LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), ss.str());
		}
		break;

		case tcpla::TCPLA_SOFTWARE_EVENT:

		break;

		case tcpla::TCPLA_POST_BLOCK_FAILED:
		case tcpla::TCPLA_DTO_SND_UNDELIVERABLE_BLOCK:
		{
			std::stringstream ss;
			ss << "Event: " << tcpla::eventToString(event.event_number);

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());
			if ( event.event_data.tcpla_dto_block_event_data.handler != 0 )
			{
				XCEPT_DECLARE(pt::blit::exception::Exception, e, "Undeliverable message");
				event.event_data.tcpla_dto_block_event_data.handler->invoke(e, event.event_data.tcpla_dto_block_event_data.context);
			}

			if ( event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr != 0 )
			{
				toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr;

				ref->release();
			}
		}
		break;

		case tcpla::TCPLA_DTO_RCV_UNUSED_BLOCK:
		{

			/*pt::blit::Event event;

			event.event_number = TCPLA_DTO_RCV_UNUSED_BLOCK;
			event.event_data.tcpla_dto_block_event_data.user_cookie = d.cookie;
			event.event_data.tcpla_dto_block_event_data.block_length = d.size;
			event.event_data.tcpla_dto_block_event_data.buffer = d.local_iov;*/

			//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Recovering unused receive block");

			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_cookie.as_ptr;

			ref->release();
		}
		break;



		default:
		{
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), "Unknown event");
		}
		break;
	}
}
// pipe support





std::string pt::blit::EventHandler::toHTML ()
{
	std::stringstream ss;

	ss << cgicc::table().set("class", "xdaq-table") << std::endl;
	ss << cgicc::caption("FRL Events") << std::endl;
	ss << "				<thead>" << std::endl;
	ss << "				<tr>" << std::endl;
	ss << "				<th>Name</th>" << std::endl;
	ss << "				<th>Count</th>" << std::endl;
	ss << "				</tr>" << std::endl;
	ss << "				</thead>" << std::endl;

	ss << cgicc::tbody() << std::endl;
	for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++ )
	{
		ss << cgicc::tr();
		ss << cgicc::td(tcpla::eventToString(i)).set("style", "text-align: right;");
		ss << cgicc::td(toolbox::toString("%d", eventCounter_[i])).set("style", "min-width: 100px;");
		ss << cgicc::tr() << std::endl;
	}

	ss << cgicc::tbody() << std::endl;
	ss << cgicc::table();

	return ss.str();
}
