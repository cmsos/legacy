// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _elastic_api_exception_NotFound_h_
#define _elastic_api_exception_NotFound_h_

#include "elastic/api/exception/Exception.h"

namespace elastic
{
	namespace api
	{
		namespace exception
		{

			class NotFound : public elastic::api::exception::Exception
			{
				public:
					NotFound (std::string name, std::string message, std::string module, int line, std::string function)
						: elastic::api::exception::Exception::Exception(name, message, module, line, function)
					{
					}

					NotFound (std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e)
						: elastic::api::exception::Exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
