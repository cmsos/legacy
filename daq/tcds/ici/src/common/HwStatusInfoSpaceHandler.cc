#include "tcds/ici/HwStatusInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::ici::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                              tcds::utils::InfoSpaceUpdater* updater) :
  HwStatusInfoSpaceHandlerTCA(xdaqApp, "tcds-hw-status-ici", updater)
{
}

tcds::ici::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}
