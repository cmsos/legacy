package xdaq.xdesk.xcontrol;

public interface CanEnable 
{  
  public void setEnabled(boolean isEnable);
  public boolean isEnabled();
}
