#include "ttc/utils/BGOChannel.hh"

#include "ttc/utils/Utils.hh"

#include <sstream>
#include <stdexcept>


using namespace std;


ttc::BGOChannel::BGOChannel(const unsigned int ChannelID)
:
    channel(ChannelID),
    delay_(0),
    inhibitDelay(0x80000000 | (0x1 << BGO_REPETITIVE)),
    prescale(0),
    postscale(0)
{
}


void ttc::BGOChannel::Set(const unsigned int ChannelID)
{
  channel = ChannelID;
  delay_ = 0;
}


void ttc::BGOChannel::Reset()
{
  inhibitDelay = 0x80000000 | (0x1 << BGO_REPETITIVE);
  prescale = 0;
  postscale = 0;
}


bool ttc::BGOChannel::IsOK() const
{
  return true;
}


void ttc::BGOChannel::Check() const
{
  if (!IsOK())
  {
    stringstream myerr;
    myerr << "ERROR: BGOChannel::Check(): SOMETHING'S WRONG with channel #" << channel;
    throw std::out_of_range(myerr.str());
  }
}


void ttc::BGOChannel::SetName(const string &Name)
{
  name = Name;
}


string ttc::BGOChannel::GetName() const
{
  return name;
}


bool ttc::BGOChannel::MatchesNameOrAlternative(string name, bool case_sensitive)
{
  if (case_sensitive)
  {
    if (name == this->name)
      return true;

    return alternative_names.find(name) != alternative_names.end();
  }

  // Special treatment if we do not insist on the correct case.
  name = to_lower(name);

  if (name == to_lower(this->name))
    return true;

  // Search the map by hand...
  for (set<string>::const_iterator
      it = alternative_names.begin();
      it != alternative_names.end(); ++it)
  {
    if (name == to_lower(*it))
      return true;
  }

  // Not found.
  return false;
}


void ttc::BGOChannel::AddAlternativeName(const string &name)
{
  alternative_names.insert(name);
}


void ttc::BGOChannel::AddAlternativeNames(const vector<string> &names)
{
  std::copy(names.begin(), names.end(), inserter(alternative_names, alternative_names.begin()));
}


ttc::BGODataLength ttc::BGOChannel::GetDataLength() const
{
  if (((inhibitDelay >> BGO_SINGLEBIT) & 0x1) == 1)
    return SINGLE;
  else if (((inhibitDelay >> BGO_DOUBLEBIT) & 0x1) == 1)
    return DOUBLE;
  else if (((inhibitDelay >> BGO_BLOCKBIT) & 0x1) == 1)
    return BLOCK;
  return UNSET;
}


uint32_t ttc::BGOChannel::GetPrescale() const
{
  return (prescale & 0xffff);
}


uint32_t ttc::BGOChannel::GetInitialPrescale() const
{
  return ((prescale >> BGO_PRESCALESHIFT) & 0xffff);
}


uint32_t ttc::BGOChannel::GetPostscale() const
{
  return (postscale & 0xffff);
}


uint32_t ttc::BGOChannel::GetDelayTime1() const
{
  return ((inhibitDelay >> BGO_TIME1SHIFT) & 0xfff);
}


uint32_t ttc::BGOChannel::GetDelayTime2() const
{
  return ((inhibitDelay) & 0xfff);
}


bool ttc::BGOChannel::IsSingleCommand() const
{
  return GetDataLength() == SINGLE;
}


bool ttc::BGOChannel::IsDoubleCommand() const
{
  return GetDataLength() == DOUBLE;
}


bool ttc::BGOChannel::IsBlockCommand() const
{
  return GetDataLength() == BLOCK;
}


void ttc::BGOChannel::SetPreAndInitPrescale(const uint32_t Prescale, const uint32_t InitPrescale)
{
  prescale = ((Prescale & 0xFFFF) | ((InitPrescale & 0xFFFF) << BGO_PRESCALESHIFT));
}


void ttc::BGOChannel::SetPostscale(const uint32_t val)
{
  postscale = val & 0xffff;
}


uint32_t ttc::BGOChannel::InhibitDelayWord() const
{
  return inhibitDelay;
}


uint32_t ttc::BGOChannel::PrescaleWord() const
{
  return prescale;
}


uint32_t ttc::BGOChannel::PostscaleWord() const
{
  return postscale;
}


void ttc::BGOChannel::SetInhibitDelayWord(const uint32_t wd)
{
  inhibitDelay = wd;
  Check();
}


void ttc::BGOChannel::SetPrescaleWord(const uint32_t wd)
{
  prescale = wd;
}


void ttc::BGOChannel::SetPostscaleWord(const uint32_t wd)
{
  postscale = wd;
}
