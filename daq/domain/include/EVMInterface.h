//
//  File Name:
//
//      EVMInterface.h
//
//  File Type:
//
//      C++ Header file
//
//  Creative Authors:
//
//      Luciano B. ORSINI
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71615 
//      E-mail:Luciano.Orsini@cern.ch
//      
//      
//      Johannes Gutleber
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71536
//	E-mail:Johannes.Gutleber@cern.ch
//
//  Change History:
//
//      $Log: EVMInterface.h,v $
//      Revision 1.1  1999/07/30 16:12:24  lorsini
//      Reference: LORSINI
//      Software Change:
//      	Source Created.
//

//
//
//
//  Version Number
//
//      @(#)$Id: EVMInterface.h,v 1.1 1999/07/30 16:12:24 lorsini Exp $
//
//

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// The following pre-processor directives ensure that spurious errors will not
// be generated in the EventIdentifier that this header file is included several times.
 
#ifndef __EVMInterface_h__
#define __EVMInterface_h__

//  ------------------------------------------------------------------------ 
//  ------------------------  Include Header Files  ------------------------
//  ------------------------------------------------------------------------
 
// Include the following system header files:
 

  
// Include the following vxRU  header files:
 
#include "DAQBinding.h" 
 

//  ------------------------------------------------------------------------ 
//  ----------------------  Public Class Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//

//
//  Class:
//
//     EVMInterface 
//
//  Superclasses:
//
//     NONE 
//
//
// Class Arguments
//
//
//  Description:
//
//	The interface for the EventIdentifier Manager. From this file the stubs and 
//	skeletons have to be derived using an agreed protocol.
//      The EventIdentifier Manager is responsible for delivering valid EventIdentifier 
//      identifiers to the Filter Nodes. It also takes care  of disposing 
//      the EventIdentifiers as soon as all readers of a single  EventIdentifier have closed it. 
//      In case the FUI cannot communicate directly to the EVMInterface it also takes 
//      care of forwarding fragment requests.
// 


class EVMInterface
{

//
//  Member Functions:
//

//
//      This class defines the following public member functions:
//
//	
public:
  
  //
  // A request from the FUI for an EventIdentifier matching the passed EventProfile.
  // The DestinationCookie for sending back the id using the "confirm" message,
  // or via the "send" from the EVMInterface is passed as second
  // parameter. The cookie is an opaque parameter.
  //
  virtual void allocate (EventProfile* eprofile, DestinationCookie* dcookie) = 0;
  
  virtual void allocateRetrieve (FragmentSet* fset, EventProfile* eprofile, DestinationCookie* dcookie) = 0;
  
  //
  // Called by the FUI to ask the EVMInterface to send EventIdentifier fragments from the 
  // given RUs in FragmentSet. The return DestinationCookie is given in fui.
  //
  virtual void retrieve (EventIdentifier* eid, FragmentSet* fset, DestinationCookie* dcookie) = 0;
  
  //
  // A request from the FUI to release the associations held with the EventIdentifier.
  //
  virtual void clear (EventIdentifier* eid) = 0;
};


//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// Terminate the #if ... #endif pre-processor control directive located at the
// start of this header file.
 
#endif /* ifndef  __EVMInterface_h__ */


