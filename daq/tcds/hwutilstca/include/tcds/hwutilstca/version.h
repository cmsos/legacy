#ifndef _tcds_hwutilstca_version_h_
#define _tcds_hwutilstca_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSHWUTILSTCA_VERSION_MAJOR 3
#define TCDSHWUTILSTCA_VERSION_MINOR 13
#define TCDSHWUTILSTCA_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSHWUTILSTCA_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSHWUTILSTCA_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSHWUTILSTCA_VERSION_CODE PACKAGE_VERSION_CODE(TCDSHWUTILSTCA_VERSION_MAJOR,TCDSHWUTILSTCA_VERSION_MINOR,TCDSHWUTILSTCA_VERSION_PATCH)
#ifndef TCDSHWUTILSTCA_PREVIOUS_VERSIONS
#define TCDSHWUTILSTCA_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSHWUTILSTCA_VERSION_MAJOR,TCDSHWUTILSTCA_VERSION_MINOR,TCDSHWUTILSTCA_VERSION_PATCH)
#else
#define TCDSHWUTILSTCA_FULL_VERSION_LIST TCDSHWUTILSTCA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSHWUTILSTCA_VERSION_MAJOR,TCDSHWUTILSTCA_VERSION_MINOR,TCDSHWUTILSTCA_VERSION_PATCH)
#endif

namespace tcdshwutilstca {

  const std::string package = "tcdshwutilstca";
  const std::string versions = TCDSHWUTILSTCA_FULL_VERSION_LIST;
  const std::string description = "CMS TCDS helper software.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "Part of the CMS TCDS software.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdshwutilstca

#endif
