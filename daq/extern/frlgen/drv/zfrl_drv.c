/*
 * zfrl_drv.c 
 *
 * source for the linux zfrl device driver module
 * for linux 2.4.x
 *
 * $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/drv/zfrl_drv.c,v 1.7 2007/02/04 14:48:08 frans Exp $
 */

/*
 *  pci bus
 *  tuple bus:slot.function
 *    function=0  FRL bridge-FPGA
 *    function=1  FRL main-FPGA
 *    function=2  myrinet NIC
 *
 * to do:
 *  - return system reosurces on errors
 *
 * possible improvements:
 * - check pci-vendor device-is while probing for func=1,2
 * - single-user device by using semaphore in open()/close()
 * - factorize (re)read config space of fpga-main in probe_frl () and ioctl - load_fpga  
 * - defines for frl pci config offsets 
 */


/*************************************************************************
 * 
 * system headers 
 *
 *************************************************************************/

#include <linux/config.h>
#ifdef CONFIG_SMP
#  define __SMP__
#endif
#include <linux/version.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
//	#define MODULE

	#ifdef CONFIG_MODVERSIONS
        	#define MODVERSIONS
        	#include <linux/modversions.h>
	#endif
#else
//	#include <linux/interrupt.h>
//	#include <linux/pagemap.h>
#endif

#include <linux/module.h>
//#include <linux/version.h>
//#include <linux/fs.h>
#include <linux/pci.h>
//#include <linux/wrapper.h>
#include <asm/uaccess.h>
//#include <linux/proc_fs.h>

//include <linux/types.h>
//#include <linux/errno.h>
//#include <linux/vmalloc.h>
//#include <asm/io.h>
//#include <stdio.h>



/*************************************************************************
 * 
 * flags 
 *
 *************************************************************************/


/*************************************************************************
 * 
 * private headers 
 *
 *************************************************************************/

#include "zfrl.h"
#include "zfrl_pci.h"

/*************************************************************************
 * 
 * local definitions 
 *
 *************************************************************************/


#undef PRINT_LEVEL
#define PRINT_LEVEL 0


/* name used for (un)registering device and directory name in /proc/driver/ */
#define MODULE_NAME "zfrl"


/* try to get this from PCI conf space */


/*
 * define zfrl device to store info across driver entry points
 */

#define ZFRL_MAX_DEVS 16

/* statically allocate array of zfrl devices
 * index to this array is unit number
 * the unit number also equals the device minor number 
 */

static zfrl_dev_t zfrl_devs [ZFRL_MAX_DEVS] ;
static struct zfrl_drv_info zfrl_drv_info_arr [ZFRL_MAX_DEVS];


/*************************************************************************
 * 
 * prototypes for public functions 
 *
 *************************************************************************/

//int init_module (void);
//void cleanup_module (void);

/*************************************************************************
 * 
 * prototypes for private functions 
 *
 *************************************************************************/


static int zfrl_ioctl (struct inode *, struct file *, unsigned int cmd, unsigned long arg);
static int zfrl_mmap (struct file *, struct vm_area_struct *);
static int zfrl_open (struct inode *, struct file *);
static int zfrl_release (struct inode *, struct file *);


/*************************************************************************
 * 
 * file operations for char device 
 *
 *************************************************************************/


static struct file_operations zfrl_fops =
{
  .owner =   THIS_MODULE,
  .ioctl =   zfrl_ioctl,	     
  .mmap =    zfrl_mmap,	     
  .open =    zfrl_open,	     
  .release = zfrl_release,    
};



/*************************************************************************
 * 
 *  
 *
 *************************************************************************/


void wait_us (int nus) {
  set_current_state (TASK_UNINTERRUPTIBLE);
  schedule_timeout (((nus * HZ) / 1000000) + 1);
}

/*************************************************************************
 * 
 *  
 *
 *************************************************************************/

// pci base address register

#define PCI_BAR_0  ((int)0)

static 
int pci_probe_frl(void)
{
  struct pci_dev *pcidev_p;  
  struct pci_dev *pcidev_loop_p;  
  struct pci_dev *pcidev_bridge_p;  
  struct pci_dev *pcidev_main_p;  
  struct pci_dev *pcidev_nic_p;  
  int index ; // index of FRL-bridge device

  int vendorid_bridge, deviceid_bridge ;
  // addresses must be unsigned long (4/8 bytes on 32/64b)
  unsigned long iobase_frl_bridge_pci_adr ;
  unsigned long iobase_frl_main_pci_adr ;
  unsigned long iobase_frl_main_remap_adr ;
  unsigned long iobase_nic_pci_adr ;
  //  int busnmb ;
  int pcibus_bridge, pcislot_bridge, pcifunc_bridge ;
  //  unsigned int frl_dev_config[16];
  u8 u8_data ;
  int frl_geoslot ;
  u32 fpga_version_bridge ;
  u32 fpga_version_main ;
  int devfn ;
  int board_size ;
  //
  uint32_t pci_rsrc0_size_fpga_main ; 
  
  zfrl_dev_t *dev_p ;

  // find all frl-bridge devices
  
  for (pcidev_loop_p=NULL, index=0 ; ; index ++) {
    
    /* probe PCI for FRL bridge  device */
    
    vendorid_bridge = ZFRL_BRIDGE_PCI_VENDORID ; 
    deviceid_bridge = ZFRL_BRIDGE_PCI_DEVICEID ;
    
    pcidev_loop_p = pci_find_device (vendorid_bridge, deviceid_bridge, pcidev_loop_p) ;
    if (pcidev_loop_p == NULL) 
      break ;
    
    pcidev_bridge_p = pcidev_loop_p ; // found it 
  
    //    iobase_frl_bridge_pci_adr = pcidev_bridge_p->resource[0].start ;
    iobase_frl_bridge_pci_adr = pci_resource_start (pcidev_bridge_p, PCI_BAR_0) ;
    

    pcibus_bridge = pcidev_bridge_p->bus->number ; 
    pcislot_bridge = PCI_SLOT (pcidev_bridge_p->devfn); 
    pcifunc_bridge = PCI_FUNC (pcidev_bridge_p->devfn) ;
    // frl-bridge should have pcifunc==0
    
    // info from configuration space
    pci_read_config_byte (pcidev_bridge_p, 0x50, &u8_data) ;
    frl_geoslot = u8_data ;
    pci_read_config_dword (pcidev_bridge_p, 0x48, &fpga_version_bridge) ;

    PRINTF(0) ("zfrl[%d]: FRL-bridge vendor x%X deviceid x%X bus=x%X slot=x%X func=x%X baseadr x%08X geoslot=%d fpga=x%08X \n"
	       ,index, vendorid_bridge, deviceid_bridge
	       ,pcibus_bridge, pcislot_bridge, pcifunc_bridge
	       ,iobase_frl_bridge_pci_adr
	       ,frl_geoslot, fpga_version_bridge);
    
    // FRL-main at pci-func=1

    // search corresponding device with function=1 ie main
    devfn = PCI_DEVFN(pcislot_bridge,0x1) ;
    pcidev_p = pci_find_slot (pcibus_bridge, devfn) ;
    if (pcidev_p == NULL) {
      PRINTF(0) ("zfrl[%d]: no device with func=1 \n", index) ;
      return -ENXIO ;
    }
    // could verify correct vendor/device

    pcidev_main_p = pcidev_p ; // found it 
    //    iobase_frl_main_pci_adr = pcidev_main_p->resource[0].start ;
    iobase_frl_main_pci_adr = pci_resource_start (pcidev_main_p, PCI_BAR_0) ;

    // info from configuration space
    pci_read_config_dword (pcidev_main_p, 0x48, &fpga_version_main) ;

    PRINTF(0) ("zfrl[%d]: FRL-main bus=x%X slot=x%X baseadr x%08X fpga=x%08X \n"
	       ,index
	       ,pcibus_bridge, pcislot_bridge
	       ,iobase_frl_main_pci_adr
	       ,fpga_version_main);

    // NIC  at pci-func=2

    // search corresponding device with function=2 ie l2xp nic
    devfn = PCI_DEVFN(pcislot_bridge,0x2) ;
    pcidev_p = pci_find_slot (pcibus_bridge, devfn) ;
    if (pcidev_p == NULL) {
      PRINTF(0) ("zfrl[%d]: no device with func=2 \n", index) ;
      return -ENXIO ;
    }
    // could verify correct vendor/device

    pcidev_nic_p = pcidev_p ; // found it 
    //    iobase_frl_main_pci_adr = pcidev_main_p->resource[0].start ;
    iobase_nic_pci_adr = pci_resource_start (pcidev_nic_p, PCI_BAR_0) ;


    PRINTF(0) ("zfrl[%d]: NIC bus=x%X slot=x%X baseadr x%08X \n"
	       ,index
	       ,pcibus_bridge, pcislot_bridge
	       ,iobase_nic_pci_adr );




    //#ifdef OUT
    /*
     * high kernel addresses need to be remapped to lower ones.
     */


    // note _end is last usable address
    //
    pci_rsrc0_size_fpga_main = pci_resource_end (pcidev_main_p, PCI_BAR_0) - pci_resource_start (pcidev_main_p, PCI_BAR_0) + 1 ;
    board_size = pci_rsrc0_size_fpga_main ;

    //
    // TODO: MUST CLEANUP MAPPED SPACE ON ERROR BRANCHES OF SAME AND DIFFERENT DEVICES
    //

    // ioremap returns (void *), both args are (unsigned long)
    iobase_frl_main_remap_adr = (unsigned long) ioremap(iobase_frl_main_pci_adr, (unsigned long) board_size) ;

    if (iobase_frl_main_remap_adr == 0) {
      PRINTF(0) ("zfrl: ioremap() failed! \n") ;
      return -ENXIO ;
    }

    PRINTF(0) ("zfrl[%d]: pci_base x%08X remapped to x%08X size x%08X \n"
	     ,index,iobase_frl_main_pci_adr, iobase_frl_main_remap_adr, board_size) ;
    //#endif


    /* store info in dev struct */

    dev_p = &zfrl_devs [index] ;

    dev_p->iobase_main_pci = iobase_frl_main_pci_adr ;
    dev_p->iobase_main_remap = iobase_frl_main_remap_adr ;
    dev_p->iobase_bridge_pci = iobase_frl_bridge_pci_adr ;
    dev_p->pci_rsrc0_size_fpga_main = pci_rsrc0_size_fpga_main ;
    dev_p->frl_geoslot = frl_geoslot ;
    dev_p->fpga_version_bridge = fpga_version_bridge ;
    // [note: can be updated by ioctl-load-fpga]
    dev_p->fpga_version_main = fpga_version_main ;

    dev_p->pcidev_frl_bridge_p = pcidev_bridge_p;  
    dev_p->pcidev_frl_main_p = pcidev_main_p;  
    dev_p->pcidev_nic_p = pcidev_nic_p;  

    //    dev_p->busnmb = busnmb ;
    //    dev_p->frldev_pb = pb ;
    //    dev_p->frldev_dev_num = dev_num ;
    //    dev_p->frlbridge_pb = pb_bridge ;
    //    dev_p->frlbridge_dev_num = dev_num_bridge ;

  }


  return 0;  /* happy */
}




/*************************************************************************
 * 
 * init is called when driver is installed [/sbin/insmod] 
 *
 *************************************************************************/

static int __init zfrl_init(void)
{
  int rt ;
  int u ;
  int boards_found ;

  PRINTF(0) ("zfrl: init_ called\n");

  /* init devices array */

  for (u=0 ; u < ZFRL_MAX_DEVS ; u++) {
    zfrl_devs[u].unit = u ;
    zfrl_devs[u].iobase_main_pci = 0 ;
    //    zfrl_devs[u].dma_block = 0 ;
    //    zfrl_devs [u].dma_block_size = 0 ; 
  } 

  for (u=0 ; u < ZFRL_MAX_DEVS ; u++) {
    zfrl_drv_info_arr[u].stamp1 = 1234 ;
    zfrl_drv_info_arr[u].nmb_interrupts = 0 ;
    zfrl_drv_info_arr[u].nmb_spurious = 0 ;
    zfrl_drv_info_arr[u].stamp2 = 4567 ;
  }   

  /* register char device */

  rt = register_chrdev (ZFRL_MAJOR,MODULE_NAME,&zfrl_fops) ;
  if (rt) {
    PRINTF(0) ("zfrl:register_chrdev() failed. major_device_num = %d \n",
	       ZFRL_MAJOR);
    return -EIO;
  }

  /* probe PCI for FRL boards and initialise them */

  pci_probe_frl() ;

  /* how many boards found ? */

  for (boards_found=0, u=0 ; u < ZFRL_MAX_DEVS ; u++) {
    if (zfrl_devs [u].iobase_main_pci) {
      boards_found ++ ;
    } 
  }
  if (boards_found == 0) {
    PRINTF(0) ("zfrl: no frl boards found and initialised! \n");
    goto error ;
  }

  return 0; /* happy */

 error:

  unregister_chrdev (ZFRL_MAJOR,MODULE_NAME);
  return -EIO;
}


/*************************************************************************
 * 
 * cleanup is called when driver is removed  [/sbin/rmmod] 
 *
 *************************************************************************/



static void __exit zfrl_cleanup(void)
{
  int u ;
  zfrl_dev_t *dev_p ;

  PRINTF(0) ("zfrl: cleanup_ called\n");

  /* free all resources */

  for (u=0 ; u < ZFRL_MAX_DEVS ; u++) {
    dev_p = &zfrl_devs [u];

    if (dev_p->iobase_main_remap)
      iounmap ((void *) dev_p->iobase_main_remap);
  } 

  unregister_chrdev (ZFRL_MAJOR,MODULE_NAME);
}


/*************************************************************************
 * 
 * _open()  
 *
 *************************************************************************/


static int zfrl_open (struct inode *inode_p, struct file *file_p)
{
  int minor = MINOR(inode_p->i_rdev);
  int major = MAJOR(inode_p->i_rdev);
  int unit = minor ;
  zfrl_dev_t *dev_p ;

  PRINTF(5)("zfrl_open(%p, %p)\n", inode_p,  file_p);
  PRINTF(5)("zfrl_open:  major = %d  minor = %d\n", major, minor);

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
  MOD_INC_USE_COUNT;
#endif

  /* check device present */
  
  dev_p = &zfrl_devs [unit] ;
  if (dev_p->iobase_main_pci == 0) {
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
    MOD_DEC_USE_COUNT;
#endif
    return -ENODEV ;
  }
 
  file_p->private_data = dev_p ;

  return (0); /* happy */
}

/*************************************************************************
 * 
 * _release()  
 *
 *************************************************************************/


static int zfrl_release (struct inode *inode_p, struct file *file_p)
{

  int minor = MINOR(inode_p->i_rdev);
  int major = MAJOR(inode_p->i_rdev);

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,5,0)
  MOD_DEC_USE_COUNT;
#endif

  PRINTF(5)("zfrl_release(%p, %p)\n", inode_p, file_p);
  PRINTF(5)("zfrl_release:  major = %d  minor = %d\n", major, minor);

  return (0); /* happy */
} 

/*************************************************************************
 * 
 * _ioctl()  
 *
 *************************************************************************/

#define PCI_CONFIG_LEN 64

int
zfrl_ioctl (struct inode *inode_p, struct file *file_p,
	      unsigned int cmd, unsigned long arg)
{
  int minor = MINOR(inode_p->i_rdev);
  int unit = minor ;
  zfrl_dev_t *dev_p ;

  struct zfrl_board_info bi_copy;  /* holder for board_info */
  struct zfrl_drv_info di_copy;  /* holder for drv_info */

  uint8_t config_space_frl_main[PCI_CONFIG_LEN] ;
  uint8_t config_space_nic[PCI_CONFIG_LEN] ;

  /* check device present */

  dev_p = &zfrl_devs [unit] ;
  if (dev_p->iobase_main_pci == 0)
    return -ENODEV ;


  /* switch according to cmd code */

  switch(cmd) {

  case ZFRL_GET_BOARD_INFO :

    PRINTF(0) ("zfrl[%d]: get_board_info: \n",unit);

    //    bi_copy.revision      = dev_p->revision ;
    //    bi_copy.pci_busnmb      = dev_p->busnmb ;
    //    bi_copy.dma_block_bus = dev_p->dma_block_bus ;
    bi_copy.iobase_main_pci = dev_p->iobase_main_pci ;
    bi_copy.pci_rsrc0_size_fpga_main = dev_p->pci_rsrc0_size_fpga_main ;
    bi_copy.frl_geoslot = dev_p->frl_geoslot ;

    bi_copy.fpga_version_bridge = dev_p->fpga_version_bridge ; 
    bi_copy.fpga_version_main = dev_p->fpga_version_main ; 


    /* fill struct in user address space */ 

    if (!access_ok (VERIFY_WRITE, (void *) arg, sizeof(struct zfrl_board_info))) {
      return -EFAULT ;
    }
    __copy_to_user((void *) arg, &bi_copy, sizeof(struct zfrl_board_info));

    break;

  case ZFRL_GET_DRV_INFO :

    di_copy = zfrl_drv_info_arr[unit] ;

   /* fill struct in user address space */ 

    if (!access_ok (VERIFY_WRITE, (void *) arg, sizeof(struct zfrl_drv_info))) {
      return -EFAULT ;
    }
    __copy_to_user((void *) arg, &di_copy, sizeof(struct zfrl_drv_info));

    break;

  case ZFRL_LOAD_FPGA :

    {
      struct zfrl_load_fpga ioctl_load_fpga ;
      int blockno ;
      unsigned int data ;
      u32 fpga_version_main ;
      
      if (!access_ok (VERIFY_READ, (void *) arg, sizeof(struct zfrl_load_fpga))) {
	return -EFAULT ;
      }
      
      __copy_from_user(&ioctl_load_fpga, (void *) arg, sizeof(struct zfrl_load_fpga)) ;

      
      blockno = ioctl_load_fpga.blockno ;

      PRINTF(0) ("zfrl[%d]: load_fpga: blockno %d \n",unit,blockno);

      // read configuration space of FRL device and store it
      {
	int n ;
	for (n=0 ; n < PCI_CONFIG_LEN ; n++) {
	  pci_read_config_byte (dev_p->pcidev_frl_main_p, n, &config_space_frl_main[n]) ;
	}
      }
      
      // read configuration space of NIC device and store it
      {
	int n ;
	for (n=0 ; n < PCI_CONFIG_LEN ; n++) {
	  pci_read_config_byte (dev_p->pcidev_nic_p, n, &config_space_nic[n]) ;
	}
      }
      
      // on the bridge device ..
      // enable jtag chain by writing x1 at adr 0x40
      
      // hannes writes only x4 (no jtag gymnastics)

      data = 0x1 ;
      pci_write_config_dword (dev_p->pcidev_frl_bridge_p, 0x40, data) ;
      
      // initiate FPGA load
      data = ((blockno<<16) | 0x5) ;
      //      data = ((blockno<<16) | 0x4) ;
      pci_write_config_dword (dev_p->pcidev_frl_bridge_p, 0x40, data) ;
      
      
      //	PRINTF(0) ("zfrl[%d]: load_fpga did step %d. \n",unit,step);
      
      // sleep 100 ms
      wait_us(100000) ;
      
      // on the bridge device ..
      // close jtag chain by writing x0 at adr 0x40
      
      data = 0x0 ;
      pci_write_config_dword (dev_p->pcidev_frl_bridge_p, 0x40, data) ;
      
      // sleep 100 ms
      wait_us(100000) ;

      // info from configuration space
      pci_read_config_dword (dev_p->pcidev_frl_main_p, 0x48, &fpga_version_main) ;
      
      PRINTF(0) ("zfrl[%d]: FRL-main fpga=x%08X \n"
		 ,unit
		 ,fpga_version_main);
      

      // 1 sec
      wait_us(1000000) ;
      

      // restore configuration space of FRL device 
      {
	int n ;
	for (n=0 ; n < PCI_CONFIG_LEN ; n++) {
	  pci_write_config_byte (dev_p->pcidev_frl_main_p, n, config_space_frl_main[n]) ;
	}
      }
      
      // restore configuration space of NIC device 
      {
	int n ;
	for (n=0 ; n < PCI_CONFIG_LEN ; n++) {
	  pci_write_config_byte (dev_p->pcidev_nic_p, n, config_space_nic[n]) ;
	  }
      }
      
      // should read config space (beyond 64 bytes) again to get updated values in static storage of driver 
      
      
      // info from configuration space
      pci_read_config_dword (dev_p->pcidev_frl_main_p, 0x48, &fpga_version_main) ;
      
      PRINTF(0) ("zfrl[%d]: FRL-main fpga=x%08X \n"
		 ,unit
		 ,fpga_version_main);
      
      /* store info in dev struct */
      
      dev_p->fpga_version_main = fpga_version_main ;
      
      PRINTF(0) ("zfrl[%d]: load_fpga: blockno %d done \n",unit,blockno);
    }
    

    break;
    
  default:

   PRINTF(0) ("ioctl cmd %X \n",cmd) ;

    return -EINVAL;
  }

  return 0;  /* happy */
}



/*************************************************************************
 * 
 * _mmap()  
 *
 * gives user access to board map (and maybe later copy_block space)
 *  
 *
 *************************************************************************/


int
zfrl_mmap (struct file *file_p, struct vm_area_struct *vma)
{
  int rt ;
  zfrl_dev_t *dev_p ;
  unsigned long totlen ;
  int unit ;
  unsigned long offset;
  //  unsigned long start ;
  unsigned long boardsize ;
  unsigned long dmablocksize ;
  unsigned long len ;
  unsigned long vma_start, vma_end, vma_offset, vma_len ;
  pgprot_t vma_page_prot ;

  PRINTF(5) ("zfrl_mmap(%p, %p)\n",file_p, vma);
  
  /* [note that unlike most driver entry points inode* is not in the 
   *  argument list. have to obtain unit number another way] 
   */
 
  dev_p = (zfrl_dev_t *) file_p->private_data;
  unit = dev_p->unit ;

  /* check if device present */
  dev_p = &zfrl_devs[unit] ;
  if (dev_p->iobase_main_pci == 0)
    return -ENODEV ;

  vma_offset = vma->vm_pgoff << PAGE_SHIFT ;
  vma_start = vma->vm_start ;
  vma_end = vma->vm_end ;
  vma_page_prot = vma->vm_page_prot ;

  vma_len = vma_end - vma_start ;

  PRINTF(0) ("zfrl_mmap[%d]: vma offset=x%lx  start=x%lx  end=x%lx so len=x%lx \n",
	     unit, vma_offset, vma_start, vma_end, vma_len) ;

  /* 
   * just map the whole board to user space 
   */

  boardsize = dev_p->pci_rsrc0_size_fpga_main ;
  //  dmablocksize = dev_p->dma_block_size ;
  dmablocksize = 0 ;
  totlen = boardsize + dmablocksize ;

  if (totlen > vma_len) {
    PRINTF(0) ("zfrl_mmap[%d]: FATL vma_len too small; vma_len=x%X len=x%X  \n",unit, vma_len, totlen);
    return -EAGAIN;
  }
      
  offset = (unsigned long) dev_p->iobase_main_pci  ;
  len =  boardsize ;


#ifdef OUT
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,19)
  rt = remap_page_range (vma_start, offset, len, vma_page_prot) ;
#endif
#endif

  rt = remap_page_range (vma,vma_start, offset, len, vma_page_prot) ;
  if (rt) {
    return -EAGAIN;
  }

  PRINTF(0) ("zfrl_mmap[%d]: mapped frl %08X to %08X len %08X \n",unit,offset,vma_start,len);

 
#ifdef OUT
  /* dma block */

  if (dev_p->dma_block) {

    start = vma_start + boardsize ;
    offset = dev_p->dma_block_phys ;
    len = dmablocksize ;
    
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,19)
  rt = remap_page_range (vma,start, offset, len, vma_page_prot) ;
#else
  rt = remap_page_range (start, offset, len, vma_page_prot) ;
#endif
  if (rt) {
    return -EAGAIN;
  }
    
    PRINTF(0) ("zfrl_mmap[%d]: mapped dma_block %08X to %08X len %08X \n",unit, offset,start,len);
    
  }
#endif
  
  return (0);   /* happy */
}



/*************************************************************************
 * 
 * auxilary functions  
 *
 *************************************************************************/




/*************************************************************************
 * 
 *   
 *
 *************************************************************************/


module_init(zfrl_init);
module_exit(zfrl_cleanup);

MODULE_LICENSE ("GPL");
MODULE_AUTHOR ("FM");
MODULE_DESCRIPTION ("Driver for access to FRL");


