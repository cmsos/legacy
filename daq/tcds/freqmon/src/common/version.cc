#include "tcds/freqmon/version.h"

#include "b2in/nub/version.h"
#include "config/version.h"
#include "eventing/api/version.h"
#include "hyperdaq/version.h"
#include "pt/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xgi/version.h"
#include "xoap/version.h"

#include "tcds/exception/version.h"
#include "tcds/hwlayer/version.h"
#include "tcds/hwlayertca/version.h"
#include "tcds/hwutilstca/version.h"
#include "tcds/utils/version.h"

GETPACKAGEINFO(tcdsfreqmon)

void
tcdsfreqmon::checkPackageDependencies()
{
  CHECKDEPENDENCY(b2innub);
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(eventingapi);
  CHECKDEPENDENCY(hyperdaq);
  CHECKDEPENDENCY(pt);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);
  CHECKDEPENDENCY(xoap);

  CHECKDEPENDENCY(tcdsexception);
  CHECKDEPENDENCY(tcdshwlayer);
  CHECKDEPENDENCY(tcdshwlayertca);
  CHECKDEPENDENCY(tcdshwutilstca);
  CHECKDEPENDENCY(tcdsutils);
}

std::set<std::string, std::less<std::string> >
tcdsfreqmon::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, b2innub);
  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, eventingapi);
  ADDDEPENDENCY(dependencies, hyperdaq);
  ADDDEPENDENCY(dependencies, pt);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);
  ADDDEPENDENCY(dependencies, xdaq);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);
  ADDDEPENDENCY(dependencies, xoap);

  ADDDEPENDENCY(dependencies, tcdsexception);
  ADDDEPENDENCY(dependencies, tcdshwlayer);
  ADDDEPENDENCY(dependencies, tcdshwlayertca);
  ADDDEPENDENCY(dependencies, tcdshwutilstca);
  ADDDEPENDENCY(dependencies, tcdsutils);

  return dependencies;
}
