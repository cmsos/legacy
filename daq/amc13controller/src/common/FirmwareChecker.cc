#include "amc13controller/FirmwareChecker.hh"
#include "d2s/utils/loggerMacros.h"
#include <sstream>
#include <iomanip>
//#include "amc13controller/AMC13.hh"

amc13controller::FirmwareChecker::FirmwareChecker(amc13controller::AMC13 *amc13Device, bool dontFail)
{
    //Read on construction only to minimise hardware access (assuming that any firmware
    //checker will check the firmware versions at some point in its lifetime).
    amc13T1FwVersion_  = amc13Device->readRegister(amc13controller::AMC13::T1, "STATUS.FIRMWARE_VERS");
    amc13T2FwVersion_  = amc13Device->readRegister(amc13controller::AMC13::T2, "STATUS.FIRMWARE_VERS");

    fpgaType_ = A13;
    dontFail_ = dontFail;
}


bool amc13controller::FirmwareChecker::checkFirmware(std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw)
{
    bool result = true;
    if (fpgaType_ == A13)
    {
        result = checkAMC13Firmware(mode, dataSource, errorstr);
    }
    else
    {
        result = false;
        errorstr = "Illegal fpgaType encountered (probably a software bug)";
    }

    if (dontFail_)
        result = true;

    return result;
}


bool amc13controller::FirmwareChecker::checkAMC13Firmware(std::string mode, std::string dataSource, std::string &errorstr)
{
    bool ret = true;
    std::stringstream error;

    // There are two generations of FRLs which are used.
    // The newer type has bit 0 of the type set to 1
    // whereas the older has this bit set to 0. We accept
    // both versions since they are functionally compatible.

    // check the AMC13 type:
    uint32_t minFirmwareT1 = 0;
    uint32_t minFirmwareT2 = 0;

    minFirmwareT1 = MIN_T1_FIRMWARE;
    minFirmwareT2 = MIN_T2_FIRMWARE;

    if (amc13T1FwVersion_ < minFirmwareT1)
    {
        ret = false;
        error << "Error verifying AMC13 firmware type - expected "
              << std::hex << minFirmwareT1 << " or higher, read "
              << amc13T1FwVersion_;
    }

    // check the amc13 fw version
    if (amc13T2FwVersion_ < minFirmwareT2)
    {
        ret = false;
        error << "Error verifying AMC13 firmware version - expected >= "
              << std::hex << minFirmwareT2 << " read "
              << amc13T2FwVersion_;
    }
    errorstr = error.str();

    return ret;
}


uint32_t
amc13controller::FirmwareChecker::getAMC13T1FirmwareVersion() const
{
    return amc13T1FwVersion_;
}

uint32_t
amc13controller::FirmwareChecker::getAMC13T2FirmwareVersion() const
{
    return amc13T2FwVersion_;
}

/*
uint32_t
amc13controller::FirmwareChecker::getAMC13FirmwareType() const
{
    return amc13FwType_;
}

uint32_t
amc13controller::FirmwareChecker::getAMC13HardwareRevision() const
{
    return amc13HwRevision_;
}
*/

//void amc13controller::FrlFirmwareChecker::reloadFrlFirmware( Frl *frl,
//                                                   AMC13 *amc13,
//                                                   uint32_t firmwareId )
//{
//    return;
//
//    // this is not so easy: first the monitoring loops have to be shut down.
//    // No access during this gymnastics! Also the AMC13 must not be touched
//    // since during the procedure the hardware issues a PCI reset to the amc13
//    // so that also the amc13 looses the PCI configuration and all other
//    // configuration. Therefore this reload has to be handled differently...
//
//    HAL::PCIDevice *frlDevice = frl->frlDevice_P;
//    this->saveFrlConfigSpace( frlDevice );
//    this->saveAMC13ConfigSpace( amc13Device );
//    uint32_t ctrl = (firmwareId << 16) + 0x04; // firmware Id to bit 16..18, bit 2 set to trigger reload
//    bridgeDevice->write( "firmwareControl", ctrl );
//    ::sleep(4);
//    this->writeBackFrlConfigSpace( frlDevice );
//    this->writeBackFrlConfigSpace( amc13Device );
//    std::cout << "all done" << std::endl;
//}
