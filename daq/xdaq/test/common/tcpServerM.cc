#include "sdruM.h"
#include "sdevmM.h"
#include "TcpDispatcherAcceptorTaskM.h"


extern "C" int Main(char * host, char * ua)
{

	//test
	
	
	//test
	TcpDispatcherAcceptorTaskM * dm;

	SDRU * sdru ;          
	SDEVM * sdevm ;        
	vector<FUInterface*> fuv;
	UAddress uaddr;
	sscanf(ua,"%d",&uaddr);
	Try
	{
		//Test Select
		vector<Addr*>  addrv;
		addrv.resize(2);
		
		
		addrv[0] = new tcpAddr();
		strcpy(((tcpAddr*)addrv[0])->host,"137.138.82.59");
		((tcpAddr*)addrv[0])->port = 20300;
		
		addrv[1] = new tcpAddr();
		strcpy(((tcpAddr*)addrv[1])->host,"137.138.82.28");	
		((tcpAddr*)addrv[1])->port = 20300;
			
		dm = new TcpDispatcherAcceptorTaskM(host,20300, &addrv);
		dm->activate();
		
		//dm->sync();
		
	       
		fuv.resize(2);
		fuv[0] = new XFU(&((*dm->getXIO())[0]));
		fuv[1] = new XFU(&((*dm->getXIO())[1]));
		sdru = new SDRU(&fuv, 100, uaddr);
		sdevm = new SDEVM(&fuv, sdru, 100); //local
  		TaskGroup g;
  		dm->initTaskGroup(&g);
		sdevm->initTaskGroup(&g);
  		dm->addListener(sdru);
		dm->addListener(sdevm); //local
    
		sdevm->activate();
		
 		g.join();
	}
	Catch(vxException e)
	{
		cout << e;
		
	};
	delete dm;
	delete fuv[0];
	delete fuv[1];
	delete sdru;
	return(0);	
}
#ifndef vxworks
int main(int argc, char* argv[])
{

	return Main(argv[1],argv[2]);
}
#endif
