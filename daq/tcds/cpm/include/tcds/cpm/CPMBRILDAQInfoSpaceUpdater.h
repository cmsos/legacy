#ifndef _tcds_cpm_CPMBRILDAQInfoSpaceUpdater_h_
#define _tcds_cpm_CPMBRILDAQInfoSpaceUpdater_h_

#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cpm {

    class BRILDAQLoop;

    class CPMBRILDAQInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      CPMBRILDAQInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                 tcds::cpm::BRILDAQLoop const& brildaqLoop);
      virtual ~CPMBRILDAQInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::cpm::BRILDAQLoop const& brildaqLoop_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_CPMBRILDAQInfoSpaceUpdater_h_
