package xdaq.tools.cluster;

import javax.swing.*;
import java.awt.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.util.*;
import java.net.*;
import java.io.*;

import xdaq.tools.cluster.ClusterTable;
import xdaq.tools.cluster.ClusterLogger;
import xdaq.tools.cluster.CommandHistoryFile;

public class Identification extends JPanel implements ActionListener
{
	private JTextField userText_;
	private JTextField passwdText_;
	private JTextField historyField_;
	private JLabel userLabel_;
	private JLabel passwdLabel_;
	private JButton clearButton_;
	private CommandHistoryFile commandHistoryFile_;
	private JCheckBox soundToggle_;
	
	public JCheckBox getSoundToggle()
	{
		return soundToggle_;
	}
	
	public Identification()
	{
		// three rows, one column
		this.setLayout ( new GridLayout (3,1));
		
		Box idBox = new Box (BoxLayout.X_AXIS);
		
		Box labelBox = new Box (BoxLayout.Y_AXIS);
		Box fieldBox = new Box (BoxLayout.Y_AXIS);
	
		userText_ = new JTextField (10);
		userLabel_ = new JLabel ("Username: ", JLabel.LEFT);
		
		passwdText_ = new JPasswordField (10);
		passwdLabel_ = new JLabel ("Password: ", JLabel.LEFT);
		
		
		labelBox.add (userLabel_);
		labelBox.add (Box.createVerticalStrut(10));
		labelBox.add (passwdLabel_);
		
		fieldBox.add (userText_);
		fieldBox.add (Box.createVerticalStrut(10));
		fieldBox.add (passwdText_);
		
		Box historyBox = new Box (BoxLayout.X_AXIS);
		historyBox.add ( new JLabel ("URL history file: ", JLabel.LEFT) );
		historyField_ = new JTextField (new String("history.txt"), 20);
		historyField_.setEditable(false);
		historyBox.add (historyField_);
		clearButton_ = new JButton ("Clear");
		clearButton_.addActionListener (this);
		historyBox.add (clearButton_);
		//historyBox.add (Box.createVerticalStrut(20) );
		
		idBox.add (labelBox);
		idBox.add (fieldBox);
		idBox.add (Box.createHorizontalStrut (400));
		
		Box contentBox = new Box (BoxLayout.Y_AXIS);
		contentBox.add (Box.createVerticalStrut(10));
		contentBox.add (idBox);
		contentBox.add (Box.createVerticalStrut(20) );
		contentBox.add (historyBox);
		contentBox.add (Box.createVerticalStrut(20) );
		this.add (contentBox);
		
		soundToggle_ = new JCheckBox ("Play sound", true);
		this.add (soundToggle_);
		
		commandHistoryFile_ = new CommandHistoryFile ( historyField_.getText() );
	}
	
	public boolean sound()
	{
		return soundToggle_.isSelected();
	}
	
	public String[] retrieveCommandHistory()
	{
		return commandHistoryFile_.retrieve();
	}
	
	public void storeCommandHistory(String url)
	{
		commandHistoryFile_.store ( url );
	}
	
	public String historyFile()
	{
		return historyField_.getText();
	}
	
	public String username()
	{
		return userText_.getText();
	}
	
	public String password()
	{
		return passwdText_.getText();
	}
	
	public void actionPerformed (ActionEvent evt) {
		Object source = evt.getSource();
		
		if (source == clearButton_) {
			System.out.println ("Clearing history file...");
			commandHistoryFile_.close();
			commandHistoryFile_.clear();
			
		}
	}
	
	public void close()
	{
		commandHistoryFile_.close();
	}
}
