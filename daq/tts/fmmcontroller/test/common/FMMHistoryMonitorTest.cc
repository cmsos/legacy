/**
*      @file FMMHistoryMonitorTest.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.5 $
*     $Date: 2007/03/27 08:06:59 $
*
*
**/
#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMCard.hh"
#include "tts/ttsbase/TTSState.hh"
#include "tts/fmm/FMMPoissonTransitionGenerator.hh"

#include "tts/fmmcontroller/FMMHistoryMonitor.hh"
#include "tts/fmmcontroller/FMMHistoryLogger.hh"

#include "hal/PCIDevice.hh"

#include <iostream>
#include <unistd.h>
#include <stdint.h>

int main() {


  try {

    std::cout << "==== FMMTester started ... " << std::endl << std::endl;
    tts::FMMCrate crate;
  
    std::cout << std::endl << "==== detected " << crate.numFMMs() << " FMMs in the crate." << std::endl;

    tts::FMMCard& card = crate.getFMM(0);

    std::cout << "current output state is: " << card.readResultA().getLongName() << std::endl;

    //--------------------------------------------------------------------------------
    
    card.disableLocking();
    card.getWriteLock();
    card.toggleDualMode(false);
    card.toggleSimuMode(true);
    card.toggleDMAMode(false);
    card.resetAll();
    
    std::vector<tts::TTSState> simulated_states(20, tts::TTSState::READY);
    card.setSimulatedInputStates(simulated_states);

    tts::FMMPoissonTransitionGenerator tg(20);

    double cacheDepthSeconds = 1.;
    bool autoUpdate = true;
    uint32_t logFileSizeLimitBytes = 2000000;
    tts::FMMHistoryMonitor mon( card, cacheDepthSeconds, autoUpdate);
    bool enableRotation = false;
    tts::FMMHistoryLogger logger("/tmp/fmmhistory_test.log", logFileSizeLimitBytes, enableRotation);
    mon.registerConsumer( &logger );

    const bool resetHW = true;
    uint32_t runNumber = 99;
    logger.setRunNumber( runNumber );
    mon.start( resetHW);
  
    uint64_t rep = 0;

    while ( true ) {
          
      tg.getNewStates( simulated_states );
      card.setSimulatedInputStates( simulated_states );
      uint64_t tt = card.readTimeTag();
      if ( (++rep % 10000) == 0)
	std::cout << "rep = " << std::dec << rep 
		  << "  tt=0x" << std::hex << tt
		  << std::endl << std::flush;
      
//       std::vector<FMMHistoryItem> hi;
//       mon.retrieveCachedHistory(hi);

//       std::vector<FMMHistoryItem>::iterator it = hi.begin();
//       for (;it != hi.end(); it++) { 

// 	cout << "HistoryItem: time=" << std::dec << std::setw(10) << setprecision(2)  
// 	     << (float) (*it).getTimestamp() / 40e6;

// 	vector<TTSState> states = (*it).getInputStates();

// 	vector<TTSState>::iterator it1 = states.begin();
// 	for (; it1!=states.end(); it1++)
// 	  std::cout << " " << (*it1).getShortName();

// 	cout << std::endl;
//       }

//      sleep(3);
    }
  

    mon.stop();


    



    std::cout << std::endl << "==== FMMHistoryMonitorTest done ... " << std::endl;

  }  catch (std::exception &e) {

    std::cout << "*** Exception occurred : " << std::endl;
    std::cout << e.what() << std::endl;

  }

return 0;
}
