-- This SQL script creates the required tables by org.apache.log4j.db.DBAppender and 
-- org.apache.log4j.db.DBReceiver.
--
-- It is intended for Oracle databases.


-- The following lines are useful in cleaning any previous tables 

drop TRIGGER logging_event_id_seq_trig; 
drop TRIGGER logging_session_id_seq_trig; 
drop SEQUENCE logging_event_id_seq; 
drop SEQUENCE logging_session_id_seq; 
drop table logging_event_property; 
drop table logging_event_exception; 
drop table logging_event; 
drop table logging_session; 
