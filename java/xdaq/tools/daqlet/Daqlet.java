package xdaq.tools.daqlet;

import java.awt.event.*;
import javax.swing.JApplet;
import java.util.*;
import java.awt.*;
import javax.swing.event.*;

import xdaq.tools.daqlet.Framework;

public class Daqlet extends JApplet
{
	protected Framework framework_;
	protected String id_;
	protected EventListenerList listenerList = new EventListenerList();

	public void setFramework (Framework framework)
	{
		framework_ = framework;
	}
	
	public void setIdentifier (String id)
	{
		id_ = id;
	}
	
	public String getIdentifier ()
	{
		return id_;
	}
		
	  /**
     * Adds an actionListener to the Timer
     */
    public void addActionListener(ActionListener listener) {
        listenerList.add(ActionListener.class, listener);
    }


    /**
     * Removes an ActionListener from the Timer.
     */
    public void removeActionListener(ActionListener listener) {
        listenerList.remove(ActionListener.class, listener);
    }
	
   /**
     * Notify all listeners that have registered interest for
     * notification on this event type.  The event instance 
     * is lazily created using the parameters passed into 
     * the fire method.
     * @see EventListenerList
     */
    public void fireActionPerformed(ActionEvent e) 
    {
    	//System.out.println ("Daqlet fireActionPerformed, e:" + e.getActionCommand());
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();

        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i=listeners.length-2; i>=0; i-=2) {
            if (listeners[i]==ActionListener.class) {
                ((ActionListener)listeners[i+1]).actionPerformed(e);
            }          
        }
    }
    
    
}
