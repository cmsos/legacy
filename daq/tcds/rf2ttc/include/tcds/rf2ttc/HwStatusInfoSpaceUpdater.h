#ifndef _tcds_rf2ttc_HwStatusInfoSpaceUpdater_h_
#define _tcds_rf2ttc_HwStatusInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace rf2ttc {

    class VMEDeviceRF2TTC;

    class HwStatusInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::rf2ttc::VMEDeviceRF2TTC const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::rf2ttc::VMEDeviceRF2TTC const& getHw() const;

    };

  } // namespace rf2ttc
} // namespace tcds

#endif // _tcds_rf2ttc_HwStatusInfoSpaceUpdater_h_
