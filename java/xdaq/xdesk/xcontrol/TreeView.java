package xdaq.tools.win;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import javax.swing.JSplitPane;

import org.w3c.dom.*;

//import  cms.tridas.xdaq.soap.*;
//import  cms.tridas.xdaq.utility.*;


/**
 * A class to get view and act with a gui on the XML configuration document.
 * This class is written as a universal plugin. It should be usable for any
 * gui application which wants to visualize the Configuration document in 
 * a structured view.
 * 
 * @author Lucaino Orsini, Johannes Gutleber
 */
public class TreeView extends JSplitPane {
    static final int leftWidth = 300;
    private JTree tree;
    private HostTreeRenderer treeRenderer = null;

    public JTree getJtree() { return tree; }

	public TreeView() {		
		// the tree window
		tree = new JTree(new DefaultMutableTreeNode("Empty"));
		tree.getSelectionModel().setSelectionMode 
			(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		tree.setEditable(false);
		treeRenderer	=  new HostTreeRenderer();
      	tree.putClientProperty("JTree.lineStyle", "Angled");	
		JScrollPane treeView = new JScrollPane(tree);
		CommandPopup commandPopup = new CommandPopup(tree);
		tree.add(commandPopup);
		tree.addMouseListener(new PopupListener (commandPopup) );

		// the realted text window
		JEditorPane htmlPane = new JEditorPane("text/html","");
		htmlPane.setEditable(false);
		JScrollPane htmlView = new JScrollPane(htmlPane);
	
		// Build split-pane view for the two items above
		setLeftComponent( treeView );
		setRightComponent( htmlView );
	    setOrientation( JSplitPane.HORIZONTAL_SPLIT );
		setContinuousLayout( true );
		setDividerLocation( leftWidth );
	   
		//
		//		// Add Event listener to button
		//
		DestinationSelectionListener selectionListener = 
			new DestinationSelectionListener(htmlPane,tree) ;
		tree.addTreeSelectionListener(selectionListener);
	}
	
	/**
	 * The routine to build the tree from a root node.
	 * This function can be called at any time and also more than once.
	 *
	 * @param rootNode is the Node from which the tree should start. All 
	 *   children of rootNode are considered during construction of the tree.
	 *
	 */
	public void setUp( AdapterNode rootNode ) {
		DefaultMutableTreeNode mRootNode = 
			new DefaultMutableTreeNode( rootNode );
		DefaultTreeModel treeModel = new DefaultTreeModel(mRootNode);
		tree.setModel(treeModel);
		tree.setCellRenderer ( treeRenderer ); 
		
		//Build the tree
		DefaultMutableTreeNode top = createTreeNode(rootNode);
		treeModel.setRoot(top);
		tree.treeDidChange();
	}

	/** 
	 * This recursive funtion iterates through all the nodes below 
	 * the Argument node of the tree in order to construct the tree.
	 *
	 */
    private DefaultMutableTreeNode createTreeNode(AdapterNode adapternode) {
		DefaultMutableTreeNode node = new DefaultMutableTreeNode(adapternode);	  
	  
		if ( adapternode.isLeaf() ) {
			return node;
		}
		else {
			NodeList children = adapternode.domNode.getChildNodes();
			for (int k=0; k< adapternode.childCount(); k++ ) {
				node.add(createTreeNode(adapternode.child(k)));
			}
			return node;
		}
    }

    public void editSelection(String selection ) {	
		// create main path		 
		DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
		TreePath mainPath = new TreePath(root);

		tree.clearSelection();
		if ( selection == "Select all Hosts" ) {
			for (int i = 0; i < model.getChildCount(root); i++ ) {
				DefaultMutableTreeNode child = 
					(DefaultMutableTreeNode)model.getChild(root,i);
				String name = ((AdapterNode)(child.getUserObject())).toString();
				//System.out.println("Extracted Host Path:"+name+"<");

				if ( name.equals("Host") ) {
					//System.out.println("Found Host select it!");
					TreePath path = mainPath.pathByAddingChild(child);	
					tree.addSelectionPath(path);
				}	
			}
		} else {
			for (int i = 0; i < model.getChildCount(root); i++ ) {
				DefaultMutableTreeNode child = 
					(DefaultMutableTreeNode)model.getChild(root,i);
				String name = ((AdapterNode)(child.getUserObject())).toString();
				//System.out.println("Extracted Host Path:"+name+"<");

				if ( name.equals("Host") ) {
					//System.out.println("Found Host select it!");
					TreePath path = mainPath.pathByAddingChild(child);	
					for (int j = 0; j < model.getChildCount(child); j++ ) {
						DefaultMutableTreeNode dev = 
							(DefaultMutableTreeNode)model.getChild(child,j); 
						String application = ((AdapterNode)(dev.getUserObject())).toString();
						//System.out.println("Extracted Application Path:"+application+"<");
						
						if ( application.equals("Application") || application.equals("Transport")) {
							AdapterNode node = (AdapterNode)dev.getUserObject();
							if ( node.getAttribute("class").equals(selection) ) {
								TreePath path2 = path.pathByAddingChild(dev);	
								tree.addSelectionPath(path2);
							}
						}	
					}
				}	
			}
		}
    }
}	

	
