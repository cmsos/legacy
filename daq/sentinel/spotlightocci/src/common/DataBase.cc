// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <sstream>
#include <iomanip>

#include "toolbox/TimeVal.h"
#include "toolbox/net/URL.h"
#include "toolbox/Runtime.h"
#include "toolbox/string.h"
#include "toolbox/hexdump.h"

#include "sentinel/spotlightocci/Repository.h"
#include "sentinel/spotlightocci/version.h"

#include "xcept/tools.h"

// Access to this function must be protected to the repositoryLock over the whole catalog query
//
static bool hasInsertedfirstRow_ = false;

void  sentinel::spotlightocci::DataBase::hasInsertedfirstRow( bool b)
{
	hasInsertedfirstRow_ = b;
}

toolbox::TimeVal sentinel::spotlightocci::DataBase::outputCatalogJSONExtended(xgi::Output* out, oracle::occi::ResultSet* occiResult)
        
{
        double maxStoreTime = 0.0;
        try
        {
                bool firstRow = true;

                size_t rowCounter = 0;
                while (occiResult->next())
                {
                        std::vector<oracle::occi::MetaData> metaData = occiResult->getColumnListMetaData();
                        int columnCount = metaData.size();

                        rowCounter++;

                        // If this is not the first row streamed out, prepend a comma
                        if (firstRow)
                                *out << "{";
                        else
                                *out << ",{";

                        for (int column = 1; column <= columnCount; column++)
                        {
                                *out << "\"";
                                std::string columnName = toolbox::tolower(metaData[column - 1].getString(oracle::occi::MetaData::ATTR_NAME));
                                if ( columnName == "datetime" )
                                        columnName = "dateTime";
                                else if ( columnName == "storetime" )
                                        columnName = "storeTime";
                                else if ( columnName == "class" )
                                        columnName = "class";
                                else if ( columnName == "function" )
                                        columnName = "func";
                                *out << columnName;
                                *out << "\":\"";
				if  ( columnName == "storeTime" )
				{
                                	*out <<    std::fixed << std::setprecision(6) << occiResult->getDouble(column);
				}
				else
				{
                                	*out << toolbox::jsonquote(occiResult->getString(column));
				}
                                *out << "\"";

                                // add a comma if there are one or more columns yet to come
                                if (column < columnCount)
                                        *out << ",";

                                // calculate max storeTime

                                if ( columnName == "storeTime" )
                                {
                                        double storeTime = occiResult->getDouble(column);

                                        if ( storeTime > maxStoreTime ) maxStoreTime = storeTime;
                                        //std::cout << " outputCatalogJSON:" <<   std::fixed << std::setprecision(6) << storeTime << " maxStoreTime" <<   std::fixed << std::setprecision(6) << maxStoreTime <<  std::endl;
                                }

                                // end calculate 
                        }

                        *out << "}";
                        firstRow = false;

                        // Disable and use row numer in sql query for optimization
                        //if ( rowCounter >= maxRowCounter )
                                //break;
                }

        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to extract column data. ") + e.what());
        }

        try
        {
                toolbox::TimeVal t(maxStoreTime);
                return t;
        }
        catch (toolbox::exception::Exception& e)
        {
                // invalid time if conversion fails
                std::stringstream msg;
                msg << "Failed to convert maxStoreTime:" << maxStoreTime;
                XCEPT_RETHROW(sentinel::spotlightocci::exception::Exception, msg.str(),e);
        }


}


toolbox::TimeVal sentinel::spotlightocci::DataBase::outputCatalogJSON (xgi::Output* out, oracle::occi::ResultSet* occiResult) 
{
	double maxStoreTime = 0.0;
	try
	{
		bool firstRow = true;

		size_t rowCounter = 0;
		while (occiResult->next())
		{
			std::vector < oracle::occi::MetaData > metaData = occiResult->getColumnListMetaData();
			int columnCount = metaData.size();

			rowCounter++;

			std::stringstream row;

			// If this is not the first row streamed out, prepend a comma
			if (firstRow)
				row << "{";
			else
				row << ",{";

			bool includeInOutput = true;

			for (int column = 1; column <= columnCount; column++)
			{
				std::string columnName = toolbox::tolower(metaData[column - 1].getString(oracle::occi::MetaData::ATTR_NAME));

				if (columnName == "type")
				{
					std::string type = occiResult->getString(column);
					if (type != "fire" && type != "revoke" && type != "clear")
					{
						includeInOutput = false;
						break;
					}
				}
				row << "\"";

				if (columnName == "datetime")
				{
					columnName = "dateTime";
				}
				else if (columnName == "storetime")
				{
					columnName = "storeTime";
				}
				else if (columnName == "class")
				{
					columnName = "class";
				}
				else if (columnName == "function")
				{
					columnName = "func";
				}
				row << columnName;
				row << "\":\"";
				if (columnName == "storeTime")
				{
					row << std::fixed << std::setprecision(6) << occiResult->getDouble(column);
				}
				else if (columnName == "type")
				{
					// for hotspot compatibility, rearm becomes clear
					std::string type = occiResult->getString(column);
					if (type == "clear")
					{
						row << toolbox::jsonquote("rearm");
					}
					else
					{
						row << toolbox::jsonquote(occiResult->getString(column));
					}
				}
				else
				{
					row << toolbox::jsonquote(occiResult->getString(column));
				}

				row << "\"";

				// add a comma if there are one or more columns yet to come
				if (column < columnCount) row << ",";

				// calculate max storeTime
				if (columnName == "storeTime")
				{
					double storeTime = occiResult->getDouble(column);

					if (storeTime > maxStoreTime) maxStoreTime = storeTime;
					//std::cout << " outputCatalogJSON:" <<   std::fixed << std::setprecision(6) << storeTime << " maxStoreTime" <<   std::fixed << std::setprecision(6) << maxStoreTime <<  std::endl;
				}

				// end calculate 
			}

			row << "}";
			firstRow = false;

			if (includeInOutput)
			{
				*out << row.str();
			}
			// Disable and use row numer in sql query for optimization
			//if ( rowCounter >= maxRowCounter )
			//break;
		}

	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to extract column data. ") + e.what());
	}

	try
	{
		toolbox::TimeVal t(maxStoreTime);
		return t;
	}
	catch (toolbox::exception::Exception& e)
	{
		// invalid time if conversion fails
		std::stringstream msg;
		msg << "Failed to convert maxStoreTime:" << maxStoreTime;
		XCEPT_RETHROW(sentinel::spotlightocci::exception::Exception, msg.str(), e);
	}

}

// -- End of SQLite C callback definition

sentinel::spotlightocci::DataBase::DataBase( const std::string& username, const std::string& password, const std::string& tnsname, bool readOnly, unsigned int scatterReadNum )	

:lock_(toolbox::BSem::FULL)
{
	averageTimeToStore_ = 0.0;
	averageTimeToRetrieveCatalog_ = 0.0;
	averageTimeToRetrieveException_ = 0.0;

	scatterReadNum_ = scatterReadNum;

	//maxRowCounter_ = 100;

	// Used for loading entries from Oracle to Cache, incremented when some entries found
	maintenanceSinceTime_ = toolbox::TimeVal::gettimeofday();

	// Initialise Oracle environment
	try
	{
		environment_ = oracle::occi::Environment::createEnvironment(oracle::occi::Environment::DEFAULT);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Error creating environment
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Could not create environment. ") + e.what());
	}

	// Connect to the database
	try
	{
		connection_ = environment_->createConnection(username.c_str(), password.c_str(), tnsname.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToOpen, std::string("Could not connect to database. ") + e.what());
	}

	// Prepare the Oracle db if necessary
	
	// 2) Prepare tables for write and pre-compile SQL statements for read and write
	// parameter 'true' means read only, 'false' means read and write
	//
	try
	{
		this->prepareDatabase(false);
	}
	catch (sentinel::spotlightocci::exception::Exception &e)
	{
		// Error creating tables in Oracle
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToOpen, std::string("Could not prepare database. ") + e.what());
	}

	try 
	{
		lastExceptionTime_ = this->getLatestExceptionTime();
	}
	catch (sentinel::spotlightocci::exception::Exception & e)
        {
                // Error connecting
                std::stringstream msg;
                msg << "Failed to retrieve latest exception time";
                XCEPT_RETHROW(sentinel::spotlightocci::exception::FailedToOpen, msg.str(),e);
        }
}

sentinel::spotlightocci::DataBase::~DataBase()
{
	connection_->terminateStatement(insertCatalogStmt_);
	connection_->terminateStatement(insertEventStmt_);
	connection_->terminateStatement(insertExceptionStmt_);
	connection_->terminateStatement(insertApplicationStmt_);
	environment_->terminateConnection(connection_);
}

void sentinel::spotlightocci::DataBase::maintenance()

{

	try
        {
                lastExceptionTime_ = this->getLatestExceptionTime();
		if ( lastExceptionTime_ != toolbox::TimeVal::zero() )
		{
			maintenanceSinceTime_ = lastExceptionTime_;
		}
        }
        catch (sentinel::spotlightocci::exception::Exception & e)
        {
                // Error connecting
                std::stringstream msg;
                msg << "Failed to retrieve latest exception time";
                XCEPT_RETHROW(sentinel::spotlightocci::exception::FailedToOpen, msg.str(),e);
        }

}


/* Macro function to prepare the tables */
void sentinel::spotlightocci::DataBase::prepareDatabase (bool readOnly)

{
	oracle::occi::Statement* occiStmt = 0;

	std::stringstream estatement;	
	estatement << "CREATE TABLE event(uniqueid varchar(37), storeTime double precision, exception varchar(37), source varchar(1024), type varchar(2000), creationTime double precision, PRIMARY KEY (uniqueid))";

	try
	{
		occiStmt = connection_->createStatement(estatement.str().c_str());
        	occiStmt->executeUpdate();
        	connection_->terminateStatement(occiStmt);
	}
        catch (oracle::occi::SQLException &e)
        {
		// Table already exists
        }

	std::stringstream statement;	
	statement << "CREATE TABLE catalog(uniqueid varchar(37), dateTime double precision, identifier varchar(80), occurrences varchar(10), notifier varchar(1024), severity varchar(80), message varchar(4000), schema varchar(1024), sessionid varchar(10), tag varchar(512), version varchar(10), module varchar(1024), line varchar(10), func varchar(1024), expiry double precision, args varchar(256), PRIMARY KEY (uniqueid))";
	
	try
	{
		occiStmt = connection_->createStatement(statement.str().c_str());
        	occiStmt->executeUpdate();
        	connection_->terminateStatement(occiStmt);
	}
        catch (oracle::occi::SQLException &e)
        {
		// Table already exists
        }

	// Specialized property tables
	//
	std::stringstream xstatement;	
	xstatement << "CREATE TABLE xdaq_application(uniqueid varchar(37), class varchar(1024), instance varchar(10), lid varchar(10), context varchar(1024),"
	<< "groups varchar(1024), service varchar(80), zone varchar(80), uuid varchar(37), PRIMARY KEY (uniqueid))";

	try
	{
		occiStmt = connection_->createStatement(xstatement.str().c_str());
       		occiStmt->executeUpdate();
        	connection_->terminateStatement(occiStmt);
	}
        catch (oracle::occi::SQLException &e)
        {
		// Table already exists
        }

	// end of specialized property tables
	
	std::stringstream statement2;
	statement2 << "CREATE TABLE exceptions(uniqueid varchar(37), exception blob, storeTimestamp timestamp default SYSTIMESTAMP NOT NULL, PRIMARY KEY (uniqueid))";
	
	try
	{
		occiStmt = connection_->createStatement(statement2.str().c_str());
        	occiStmt->executeUpdate();
        	connection_->terminateStatement(occiStmt);
	}
        catch (oracle::occi::SQLException &e)
        {
		// Table already exists
        }

	// Create index on dateTime for catalog tab
	std::stringstream createIndexStatement;
	createIndexStatement << "CREATE INDEX dateTimeIndex ON catalog(dateTime DESC)";

	try
	{
		occiStmt = connection_->createStatement(createIndexStatement.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Index already exists
	}

	// Create index on exception id for catalog table
	std::stringstream createIndexStatementException;
	createIndexStatementException << "CREATE INDEX exceptionIDIndex ON event(exception DESC)";

	try
	{
		occiStmt = connection_->createStatement(createIndexStatementException.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Index already exists
	}

	std::string insertCatalogStmtStr("insert into catalog (uniqueid,dateTime,identifier,occurrences,notifier,severity,message,schema,sessionid,tag,version,module,line,func,expiry,args) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16)");
	
	try
	{
		insertCatalogStmt_ = connection_->createStatement(insertCatalogStmtStr.c_str());
		insertCatalogStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to prepare insert statement for catalog table. ") + e.what());
        }

	std::string insertEventStmtStr("insert into event (uniqueid,storeTime,exception,source,type,creationTime) values(:1,:2,:3,:4,:5,:6)");

	try
	{
		insertEventStmt_ = connection_->createStatement(insertEventStmtStr.c_str());
		insertEventStmt_->setAutoCommit(true);
	}
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to prepare insert statement for event table. ") + e.what());
        }

	std::string insertApplicationStmtStr("insert into xdaq_application (uniqueid,class,instance,lid,context,groups,service,zone,uuid) values(:1,:2,:3,:4,:5,:6,:7,:8,:9)");

	try
	{
		insertApplicationStmt_ = connection_->createStatement(insertApplicationStmtStr.c_str());
		insertApplicationStmt_->setAutoCommit(true);
	}        
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to prepare insert statement for xdaq_application table. ") + e.what());
        }

	std::string insertExceptionsStr("insert into exceptions(uniqueid, exception) values(:1, :2)");

	try
	{
		insertExceptionStmt_ = connection_->createStatement(insertExceptionsStr.c_str());
		insertExceptionStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to prepare insert statement for exceptions table. ") + e.what());
	}

	std::stringstream updateEventStr;
	updateEventStr << "update event SET storetime=:1 WHERE uniqueid=:2";

	try
	{
		updateEventStmt_ = connection_->createStatement(updateEventStr.str().c_str());
		updateEventStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to prepare update statement for events table. ") + e.what());
	}
}


/*
 Storage of an exception into the catalog and schema specific paramaters
 */
void sentinel::spotlightocci::DataBase::store (toolbox::Properties& properties, const std::string & blob)

{
	// Event insert preparation
	// { uniqueid , storeTime , exception , source, type}
		
	//LOMB-BEGIN
	std::string uniqueid = properties.getProperty("uniqueid");	

	
	// create UUID on the fly
	toolbox::net::UUID euid;
	std::string event_uuid =  euid.toString();

	try
	{
		insertEventStmt_->setString(1, event_uuid.c_str());
	}        
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind euid value to insert event statement. ") + e.what());
        }


        toolbox::TimeVal storeTimeT;
        storeTimeT.fromString(properties.getProperty("storeTime"), "", toolbox::TimeVal::gmt);
        double lastExceptionTime = (double) storeTimeT;

	try
	{
		insertEventStmt_->setDouble(2, lastExceptionTime);
	}
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
        }

	std::string exception_uuid = properties.getProperty("uniqueid");	// from exception

	try
	{
		insertEventStmt_->setString(3, exception_uuid.c_str());
	}
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind exception(uuid) value to insert event statement. ") + e.what());
        }

	std::string source = properties.getProperty("source");	// event source

	try
	{
		insertEventStmt_->setString(4, source.c_str());
	}
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind source value to insert event statement. ") + e.what());
        }

	try
	{
		insertEventStmt_->setString(5, "fire");
	}
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind type value 'fire' to insert event statement. ") + e.what());
        }

	try
	{
		insertEventStmt_->setDouble(6, lastExceptionTime);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
	}

	try
	{
		insertEventStmt_->executeUpdate();
	}
        catch (oracle::occi::SQLException &e)
        {
		std::stringstream msg;
		msg << "Failed to execute insert event statement - " << e.what();
		msg << "properties: ";
                msg << ", event_uuid: " << event_uuid;
                msg << ", storeTime: " << properties.getProperty("storeTime");
                msg << ", exception(uniqueid): " << exception_uuid;
                msg << ", source: " << properties.getProperty("source");
                msg << ", type: " << "fire";

                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, msg.str());
        }
        /*
	try
	{
		if ( this->exists(uniqueid) )
		{
			// ignore it, already inserted , might be an alarm
			return;
		}
	}
	catch (sentinel::spotlightocci::exception::Exception &e)
        {
		XCEPT_RETHROW(sentinel::spotlightocci::exception::FailedToStore, "Failed in checking uniqueid exist.", e);
	}
*/

	// 
	//
	// Full exception prperties into blob
	try
	{
		this->writeBlob( uniqueid, blob );
	}
	catch (sentinel::spotlightocci::exception::FailedToStore& e)
	{
		std::stringstream msg;
		msg << "Failed to store blob into archive";
		XCEPT_RETHROW (sentinel::spotlightocci::exception::FailedToStore, msg.str(), e);
	}	
	//LOMB-END
	
	// Catalog insert preparation
	// { uniqueid,dateTime,identifier,occurrences,notifier,severity,message,schema,sessionid,tag,version }
	//

	try
	{
		insertCatalogStmt_->setString(1, uniqueid.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind uniqueid value to insert catalog statement. ") + e.what());
	}

	toolbox::TimeVal dateTimeT;
	try
	{
		dateTimeT.fromString(properties.getProperty("dateTime"), "", toolbox::TimeVal::gmt);
	}
	catch (toolbox::exception::Exception & e)
	{
	        std::stringstream msg;
                msg << "Failed to parse dateTime '" << properties.getProperty("dateTime") << "' originated by '" <<  properties.getProperty("notifier") << "' with message '" <<  this->escape(properties.getProperty("message")) << "'";
                XCEPT_RETHROW (sentinel::spotlightocci::exception::FailedToStore, msg.str(), e);
	}

	double dateTime = (double) dateTimeT;

	try
        {
                insertCatalogStmt_->setDouble(2, dateTime);
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind timestamp value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(3, properties.getProperty("identifier").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind identifier value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(4, properties.getProperty("occurrences").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind occurences value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(5, properties.getProperty("notifier").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind notifier value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(6, properties.getProperty("severity").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind severity value to insert catalog statement. ") + e.what());
        }

	std::string encodedMessage = this->escape(properties.getProperty("message"));

	if (encodedMessage.size() > 4000 )
	{
	/*	 debug
	
		std::cout << "Dump message isize: " << encodedMessage.size() << " dump:";
 		toolbox::hexdump((void*)encodedMessage.c_str(),encodedMessage.size());
		std::cout << " <- end of dump" << std::endl;;
	*/
		encodedMessage.resize(4000);	
		std::cerr << "warning: message size too large for database catalog (full message stored in BLOB )" << std::endl;
	} 


	try
        {
                insertCatalogStmt_->setString(7, encodedMessage.c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind message value to insert catalog statement. ") + e.what());
        }

	std::string propschema = properties.getProperty("qualifiedErrorSchemaURI");
	if( propschema == "" )
	{
		propschema = properties.getProperty("schema");
	}
 
	try
        {
                insertCatalogStmt_->setString(8, propschema.c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind schema value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(9, properties.getProperty("sessionID").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind sessionid value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(10, properties.getProperty("tag").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind tag value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(11, properties.getProperty("version").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind version value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(12, properties.getProperty("module").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind module value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(13, properties.getProperty("line").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind line value to insert catalog statement. ") + e.what());
        }

	try
        {
                insertCatalogStmt_->setString(14, properties.getProperty("function").c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind func value to insert catalog statement. ") + e.what());
        }

	try
		{
			if (properties.getProperty("expiry") == "")
			{
				insertCatalogStmt_->setDouble(15, 0);
			}
			else
			{
				toolbox::TimeVal expiryT;
				expiryT.fromString(properties.getProperty("expiry"), "", toolbox::TimeVal::gmt);
				double expiry = (double) expiryT;

				insertCatalogStmt_->setDouble(15, expiry);
			}
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind func value to insert catalog statement. ") + e.what());
		}

		try
		{
			insertCatalogStmt_->setString(16, properties.getProperty("args").c_str());
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind func value to insert catalog statement. ") + e.what());
		}
	try
	{
		insertCatalogStmt_->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		std::stringstream msg;
		msg << "Failed to execute insert caalog statement - " << e.what();
		msg << "properties are:";
                msg << "dateTime: " << properties.getProperty("dateTime");
                msg << ", notifier: " << properties.getProperty("notifier");
                msg << ", uniqueid: " << uniqueid;
                msg << ", identfier: " << properties.getProperty("identifier");
                msg << ", severity: " << properties.getProperty("severity");
                msg << ", message: " << properties.getProperty("message");
                msg << ", sessionid: " << properties.getProperty("sessionID");
                msg << ", schema: " << properties.getProperty("qualifiedErrorSchemaURI");
                msg << ", tag: " << properties.getProperty("tag");
                msg << ", module: " << properties.getProperty("module");
                msg << ", line: " << properties.getProperty("line");
                msg << ", func: " << properties.getProperty("function");
                msg << ", version: " << properties.getProperty("version");

                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, msg.str());
	}

	//std::cout << "QualifiedErrorSchemaURI: " << properties.getProperty("qualifiedErrorSchemaURI") << std::endl;
  	
	// This exception is qualified as a XDAQ application exception, therefore add specific schema property into xdaq-application table
	if ( propschema == "http://xdaq.web.cern.ch/xdaq/xsd/2005/QualifiedSoftwareErrorRecord-10.xsd" ) 
	{
		// xdaq-application insert preparation
		// { uniqueid,class,instance,lid,context,groups,service,zone,uuid(of application) }
		//
	
		std::string propclass = properties.getProperty("urn:xdaq-application:class");
		if( propclass == "" )
		{
			propclass = properties.getProperty("class");
		}

		std::string propinstance = properties.getProperty("urn:xdaq-application:instance");
		if( propinstance == "" )
		{
			propinstance = properties.getProperty("instance");
		}

		std::string propid = properties.getProperty("urn:xdaq-application:id");
		if( propid == "" )
		{
			propid = properties.getProperty("lid");
		}

		std::string propcontext = properties.getProperty("urn:xdaq-application:context");
		if( propcontext == "" )
		{
			propcontext = properties.getProperty("context");
		}

		std::string propgroup = properties.getProperty("urn:xdaq-application:group");
		if( propgroup == "" )
		{
			propgroup = properties.getProperty("groups");
		}

		std::string propservice = properties.getProperty("urn:xdaq-application:service");
		if( propservice == "" )
		{
			propservice = properties.getProperty("service");
		}

		std::string propzone = properties.getProperty("urn:xdaq-application:zone");
		if( propzone == "" )
		{
			propzone = properties.getProperty("zone");
		}

		std::string propuuid = properties.getProperty("urn:xdaq-application:uuid");
		if( propuuid == "" )
		{
			propuuid = properties.getProperty("uuid");
		}

		try
        	{
        	        insertApplicationStmt_->setString(1, uniqueid.c_str());
       		}
        	catch (oracle::occi::SQLException &e)
        	{
        	        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind uniqueid value to insert xdaq_application statement. ") + e.what());
        	}


		try
                {
                        insertApplicationStmt_->setString(2, propclass.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
				std::string("Failed to bind urn:xdaq-application:class value to insert xdaq_application statement. ") + e.what());
                }


		try
                {
                        insertApplicationStmt_->setString(3, propinstance.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                                std::string("Failed to bind urn:xdaq-application:instance value to insert xdaq_application statement. ") + e.what());
                }


		try
                {
                        insertApplicationStmt_->setString(4, propid.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                                std::string("Failed to bind urn:xdaq-application:id value to insert xdaq_application statement. ") + e.what());
                }
		

		try
                {
                        insertApplicationStmt_->setString(5, propcontext.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                                std::string("Failed to bind urn:xdaq-application:context value to insert xdaq_application statement. ") + e.what());
                }


		try
                {
                        insertApplicationStmt_->setString(6, propgroup.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                                std::string("Failed to bind urn:xdaq-application:group value to insert xdaq_application statement. ") + e.what());
                }


		try
                {
                        insertApplicationStmt_->setString(7, propservice.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                                std::string("Failed to bind urn:xdaq-application:service value to insert xdaq_application statement. ") + e.what());
                }


		try
                {
                        insertApplicationStmt_->setString(8, propzone.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                                std::string("Failed to bind urn:xdaq-application:zone value to insert xdaq_application statement. ") + e.what());
                }


		try
                {
                        insertApplicationStmt_->setString(9, propuuid.c_str());
                }
                catch (oracle::occi::SQLException &e)
                {
                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                                std::string("Failed to bind urn:xdaq-application:uuid value to insert xdaq_application statement. ") + e.what());
                }

		try
		{
			insertApplicationStmt_->executeUpdate();
		}
		catch (oracle::occi::SQLException &e)
		{
			std::stringstream msg;
			msg << "Failed to execute insert application statement - " << e.what();
			msg << ", uniqueid: " << uniqueid;
                        msg << ", class: " << propclass;
                        msg << ", instance: " << propinstance;
                        msg << ", id: " << propid;
                        msg << ", context: " << propcontext;
                        msg << ", group: " << propgroup;
                        msg << ", service: " << propservice;
                        msg << ", zone: " << propzone;
                        msg << ", uuid(xdaq application): " << propuuid;

                        XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, msg.str());
		}
	}
	
	//AP add auto commit
	//connection_->commit();
}


void sentinel::spotlightocci::DataBase::event (const std::string & type, toolbox::Properties& properties)

{
	// Event insert preparation
	// { uniqueid , storeTime , exception , source, type = "revoke"}
	
	// create UUID on the fly
	toolbox::net::UUID euid;
	std::string event_uuid =  euid.toString();

	try
        {
                insertEventStmt_->setString(1, event_uuid.c_str());
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                	std::string("Failed to bind euid value to insert event statement. ") + e.what());
        }

	 std::stringstream storeTimeStream;
	 storeTimeStream << properties.getProperty("storeTime");
	 double lastExceptionTime;
	 storeTimeStream >> lastExceptionTime;
	 

	try
        {                
                insertEventStmt_->setDouble(2, lastExceptionTime);
        }
        catch (oracle::occi::SQLException &e)
        {       
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                        std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
        }

	 std::string exception_uuid = properties.getProperty("uniqueid");	// from exception


	try
        {                
                insertEventStmt_->setString(3, exception_uuid.c_str());
        }
        catch (oracle::occi::SQLException &e)
        {       
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                        std::string("Failed to bind euid(uuid) value to insert event statement. ") + e.what());
        }

	 std::string source = properties.getProperty("source");	// event source


	try
        {                
                insertEventStmt_->setString(4, source.c_str());
        }
        catch (oracle::occi::SQLException &e)
        {       
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                        std::string("Failed to bind source value to insert event statement. ") + e.what());
        }


	try
        {                
                insertEventStmt_->setString(5, type.c_str());
        }
        catch (oracle::occi::SQLException &e)
        {       
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore,
                        std::string("Failed to bind type value to insert event statement. ") + e.what());
        }

	try
	{
		insertEventStmt_->setDouble(6, lastExceptionTime);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
	}

	try
	{
		insertEventStmt_->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		std::stringstream msg;
		msg << "Failed to execute insert event statement - " << e.what();
		msg << ", event_uuid: " << event_uuid;
                msg << ", storeTime: " << properties.getProperty("storeTime");
                msg << ", exception(uniqueid): " << exception_uuid;
                msg << ", source: " << properties.getProperty("source");
                msg << ", type: " << type;

                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, msg.str());
	}
}

void sentinel::spotlightocci::DataBase::rearm (std::string const & exception, std::string const & source)

{
	//std::cout << "rearm " << exception << " source " << source  << std::endl;
	// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	
	toolbox::Properties properties;

	std::stringstream storeTimeStream;
	storeTimeStream <<  std::fixed << std::setprecision(6) << (double)lastExceptionTime_;
	properties.setProperty("storeTime", storeTimeStream.str());

	// for fire event this identify the originator
	properties.setProperty("source", source );
	properties.setProperty("uniqueid",exception);
		
	this->event("clear", properties);
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double)(stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double)(stop - lastExceptionTime_))/2;
	}
}


void sentinel::spotlightocci::DataBase::revoke (xcept::Exception& ex)

{
	// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	
	// convert to double
	toolbox::TimeVal timestamp;
	
	try
	{
		timestamp.fromString(ex.getProperty("dateTime"),"",toolbox::TimeVal::gmt);
        }
        catch(toolbox::exception::Exception & e )
        {
                std::stringstream msg;
                msg << "Failed to parse datetime '" << ex.getProperty("dateTime") << "'";
                XCEPT_RETHROW (sentinel::spotlightocci::exception::FailedToStore, msg.str(), e);

        }

	std::stringstream dateTimeStream;
	dateTimeStream <<  std::fixed << std::setprecision(6) << (double)timestamp;
	
	ex.setProperty("dateTime",dateTimeStream.str());
	
	// FORMAT					
	//"error":{"field1":"value1", "field2":"value2", ... ,"error":{...}}
	//
	toolbox::Properties properties;

	std::stringstream storeTimeStream;
	storeTimeStream <<  std::fixed << std::setprecision(6) << (double)lastExceptionTime_;
    properties.setProperty("storeTime", storeTimeStream.str());

    // for fire event this identify the originator
	properties.setProperty("source", ex.getProperty("notifier") );

	std::vector<xcept::ExceptionInformation> & history = ex.getHistory();
	std::vector<xcept::ExceptionInformation>::reverse_iterator ri = history.rbegin();
	std::stringstream blob;
	while ( ri != history.rend() )
	{
		std::map<std::string, std::string, std::less<std::string> >& p = (*ri).getProperties();			
		std::map<std::string, std::string, std::less<std::string> >::iterator mi;		
		mi = p.begin();
		while (mi != p.end())
		{
			// if leading exception then keep properties 
			if ( ri ==  history.rbegin())
			{
				properties.setProperty((*mi).first,(*mi).second);
			}
			++mi;
		}		
		++ri;
	}
	
	this->event("revoke",properties);
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double)(stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double)(stop - lastExceptionTime_))/2;
	}
}

std::pair<std::string, std::string> sentinel::spotlightocci::DataBase::getLastEvent (const std::string& uuid, const std::string& time) 
{
	std::stringstream zSQL;

	zSQL << "SELECT uniqueid,type FROM (SELECT * FROM event where (exception='" << uuid << "' AND creationtime<=" << time << " AND (type='clear' OR type='fire')) ORDER BY creationtime DESC) WHERE ROWNUM <= 1";

	std::pair<std::string, std::string> result;
	result.first = "";
	result.second = "";

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;
	try
	{

		occiStmt = connection_->createStatement(zSQL.str());
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			result.first = occiResult->getString(1);
			result.second = occiResult->getString(2);
		}

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to get last event from event table. ") + e.what());
	}

	return result;
}

void sentinel::spotlightocci::DataBase::fire (xcept::Exception& ex) 
{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();

//LOMB-BEGIN
	std::string uniqueid = ex.getProperty("uniqueid");

	std::stringstream timeStream;
	timeStream << std::fixed << std::setprecision(6) << (double) lastExceptionTime_;
	std::string timeStr = timeStream.str();

	std::pair<std::string, std::string> lastEvent = this->getLastEvent(uniqueid, timeStr);

	if (lastEvent.second == "fire")
	{
		try
		{

			toolbox::net::UUID eventUUID;

			//std::cout << "found in catalog, updating" << std::endl;
			//std::cout << "ALARM STORE updating storeTime to " << (double) lastExceptionTime_ << std::endl;

			// 1: storetime, 2: exceptionid, 3: type

			try
			{
				// current time (used as creationtime and store time everywhere in query)
				updateEventStmt_->setDouble(1, (double) lastExceptionTime_);
			}
			catch (oracle::occi::SQLException &e)
			{
				XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
			}

			try
			{
				// event id
				updateEventStmt_->setString(2, lastEvent.first.c_str());
			}
			catch (oracle::occi::SQLException &e)
			{
				XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind euid value to insert event statement. ") + e.what());
			}

			//std::cout << updateEventStr.str() << std::endl;

			try
			{
				updateEventStmt_->executeUpdate();
			}
			catch (oracle::occi::SQLException &e)
			{
				std::stringstream msg;
				msg << "Failed to execute update event statement - " << e.what();

				XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, msg.str());
			}

		}
		catch (sentinel::spotlightocci::exception::Exception &e)
		{
			XCEPT_RETHROW(sentinel::spotlightocci::exception::FailedToStore, "Failed to fire exception", e);
		}
	}
	else if (lastEvent.second == "clear")
	{
		// fire event only
		toolbox::Properties properties;

		properties.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));
		properties.setProperty("uniqueid", uniqueid);
		properties.setProperty("source", ex.getProperty("notifier"));

		toolbox::net::UUID euid;
		std::string event_uuid = euid.toString();

		try
		{
			insertEventStmt_->setString(1, event_uuid.c_str());
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind euid value to insert event statement. ") + e.what());
		}

		/*
		 std::stringstream storeTimeStream;
		 storeTimeStream << properties.getProperty("storeTime");
		 double lastExceptionTime;
		 storeTimeStream >> lastExceptionTime;
		 */

		toolbox::TimeVal storeTimeT;
		storeTimeT.fromString(properties.getProperty("storeTime"), "", toolbox::TimeVal::gmt);
		double lastExceptionTime = (double) storeTimeT;

		try
		{
			insertEventStmt_->setDouble(2, lastExceptionTime);
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
		}

		std::string exception_uuid = properties.getProperty("uniqueid");	// from exception

		try
		{
			insertEventStmt_->setString(3, exception_uuid.c_str());
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind euid(uuid) value to insert event statement. ") + e.what());
		}

		std::string source = properties.getProperty("source");	// event source

		try
		{
			insertEventStmt_->setString(4, source.c_str());
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind source value to insert event statement. ") + e.what());
		}

		try
		{
			insertEventStmt_->setString(5, "fire");
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to bind type value to insert event statement. ") + e.what());
		}

		try
		{
			insertEventStmt_->setDouble(6, lastExceptionTime);
		}
		catch (oracle::occi::SQLException &e)
		{
			XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
		}

		try
		{
			insertEventStmt_->executeUpdate();
		}
		catch (oracle::occi::SQLException &e)
		{
			std::stringstream msg;
			msg << "Failed to execute insert event statement - " << e.what();
			msg << ", event_uuid: " << event_uuid;
			msg << ", storeTime: " << properties.getProperty("storeTime");
			msg << ", exception(uniqueid): " << exception_uuid;
			msg << ", source: " << properties.getProperty("source");
			msg << ", type: fire";// << type;

			XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, msg.str());
		}
	}
	else
	{
		try
		{

			toolbox::Properties properties;

			std::stringstream version;
			version <<  SENTINELSPOTLIGHTOCCI_VERSION_MAJOR << "." << SENTINELSPOTLIGHTOCCI_VERSION_MINOR << "." << SENTINELSPOTLIGHTOCCI_VERSION_PATCH;
			properties.setProperty("version", version.str());

			properties.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));

			//std::cout << "generate: " << properties.getProperty("storeTime") << std::endl;

			// for fire event this identify the originator
			properties.setProperty("source", ex.getProperty("notifier"));

			std::vector < xcept::ExceptionInformation > &history = ex.getHistory();
			std::vector<xcept::ExceptionInformation>::reverse_iterator ri = history.rbegin();
			std::stringstream blob;
			blob << "{";
			while (ri != history.rend())
			{
				std::map<std::string, std::string, std::less<std::string> >& p = (*ri).getProperties();
				blob << "\"label\":\"" << p["identifier"] << "\",";
				blob << "\"properties\":[";
				std::map<std::string, std::string, std::less<std::string> >::iterator mi;
				mi = p.begin();
				while (mi != p.end())
				{
					// if leading exception then keep properties
					if (ri == history.rbegin())
					{
						properties.setProperty((*mi).first, (*mi).second);

					}
					//
					if ((*mi).first == "message")
					{
						blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" << this->escape((*mi).second) << "\"}";
					}
					else
					{
						blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" << toolbox::jsonquote((*mi).second) << "\"}";
					}

					++mi;
					if (mi != p.end())
					{
						blob << ",";
					}
				}
				++ri;
				blob << "]";
				if (ri != history.rend())
				{
					blob << ",\"children\":[{";
				}
			}

			for (size_t s = 0; s < (history.size() - 1); ++s)
			{
				blob << "}]";
			}
			blob << "}";

			this->store(properties, blob.str());
			//////////

		}
		catch (sentinel::spotlightocci::exception::Exception &e)
		{
			XCEPT_RETHROW(sentinel::spotlightocci::exception::FailedToStore, "Failed to fire exception", e);
		}
	}

	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double) (stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double) (stop - lastExceptionTime_)) / 2;
	}
}

/*
 */
/*
void sentinel::spotlightocci::DataBase::store (xcept::Exception& ex)

{
	// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	
	
	// convert to double
	toolbox::TimeVal timestamp;
	
	
	try
	{
		timestamp.fromString(ex.getProperty("dateTime"),"",toolbox::TimeVal::gmt);
	}
        catch(toolbox::exception::Exception & e )
        {
                std::stringstream msg;
                msg << "Failed to parse datetime '" << ex.getProperty("dateTime") << "'";
                XCEPT_RETHROW (sentinel::spotlightocci::exception::FailedToStore, msg.str(), e);

        }

	std::stringstream dateTimeStream;
	dateTimeStream <<  std::fixed << std::setprecision(6) << (double)timestamp;
	
	//std::cout << "{" <<ex.getProperty("dateTime") << "," << dateTimeStream.str() << "}" << std::endl;
	ex.setProperty("dateTime",dateTimeStream.str());
	
	// FORMAT					
	//"error":{"field1":"value1", "field2":"value2", ... ,"error":{...}}
	//
	toolbox::Properties properties;

	std::stringstream version;
	version <<  SENTINELSPOTLIGHTOCCI_VERSION_MAJOR << "." << SENTINELSPOTLIGHTOCCI_VERSION_MINOR << "." << SENTINELSPOTLIGHTOCCI_VERSION_PATCH;
	properties.setProperty("version", version.str());


	std::stringstream storeTimeStream;
	storeTimeStream <<  std::fixed << std::setprecision(6) << (double)lastExceptionTime_;
	properties.setProperty("storeTime", storeTimeStream.str());

	std::cout << "generate: " << storeTimeStream.str() << std::endl;

    // for fire event this identify the originator
	properties.setProperty("source", ex.getProperty("notifier") );

	std::vector<xcept::ExceptionInformation> & history = ex.getHistory();
	std::vector<xcept::ExceptionInformation>::reverse_iterator ri = history.rbegin();
	std::stringstream blob;
	blob << "{";
	while ( ri != history.rend() )
	{
		std::map<std::string, std::string, std::less<std::string> >& p = (*ri).getProperties();			
		blob << "\"label\":\"" << p["identifier"] << "\",";		
		blob << "\"properties\":["; 
		std::map<std::string, std::string, std::less<std::string> >::iterator mi;		
		mi = p.begin();
		while (mi != p.end())
		{
			// if leading exception then keep properties 
			if ( ri ==  history.rbegin())
			{
				properties.setProperty((*mi).first,(*mi).second);

			}
			//
			if ((*mi).first == "message")
			{
				blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" << this->escape((*mi).second) << "\"}";		
			}
			else
			{
				blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" <<  toolbox::jsonquote((*mi).second) << "\"}";			
			}
			
			++mi;
			if (mi != p.end())
			{
				blob << ",";
			}
		}		
		++ri;
		blob << "]";
		if (ri != history.rend())
		{
			blob << ",\"children\":[{";
		}
	}
	
	for (size_t s = 0; s < (history.size()-1); ++s)
	{
		blob << "}]";
	}
	blob << "}";	


	this->store(properties, blob.str());
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double)(stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double)(stop - lastExceptionTime_))/2;
	}
}
*/
bool sentinel::spotlightocci::DataBase::hasException (const std::string& uuid) 
{
 	std::stringstream zSQL;

	try
	{
		zSQL << "select count(*) from exceptions where uniqueid='" << uuid << "'";

		oracle::occi::Statement* occiStmt = 0;

		occiStmt = connection_->createStatement(zSQL.str().c_str());
		oracle::occi::ResultSet* result = occiStmt->executeQuery();

		int count = 0;

		if (result->next())
		{
			count = result->getInt(1);
		}

		occiStmt->closeResultSet(result);
		connection_->terminateStatement(occiStmt);

		if (count > 0)
			return true;
	}
	catch (oracle::occi::SQLException &e)
	{
		// not found ignore and let return false
	}

	return false;
}

void sentinel::spotlightocci::DataBase::retrieve (const std::string& uuid, const std::string& format, xgi::Output* out) 

{
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	
	try
	{
		std::string jsonException;
		this->readBlob (uuid, jsonException);		
		*out << jsonException;
	}
	catch (sentinel::spotlightocci::exception::FailedToRead& e)
	{
		std::stringstream msg;
		msg << "Failed to read exception '" << uuid << "'";
		XCEPT_RETHROW (sentinel::spotlightocci::exception::NotFound, msg.str(), e);
	}
	
	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveException_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveException_ = (double)(stop - start);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveException_ = (averageTimeToRetrieveException_ + (double)(stop - start))/2;
	}
}

void sentinel::spotlightocci::DataBase::catalog
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal & end,
 const std::string& format
)

{
	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();
	
	std::stringstream selectCatalogStmt;
	// Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	// make a full precision time comparison afterwards
	
	selectCatalogStmt << "select * from catalog where dateTime<=" << std::fixed << std::setprecision(6) << (double)end;
	selectCatalogStmt << " and dateTime>=" << (double)start;

        oracle::occi::Statement* occiStmt = 0;
	try
	{
		occiStmt = connection_->createStatement(selectCatalogStmt.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	}


	oracle::occi::ResultSet* result = 0;

	try
	{
		result = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	}

	this->outputCatalogJSON(out, result); // return a maximum of 1000 exceptions

        //ttStmt_.Close(ttstatus)
	occiStmt->closeResultSet(result);
	connection_->terminateStatement(occiStmt);

	// Assume synchronous processing of the callback
	//
	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double)(stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double)(stopChrono - startChrono))/2;
	}
}

//


//
toolbox::TimeVal  sentinel::spotlightocci::DataBase::lastStoredEvents
(
 xgi::Output* out, 
 toolbox::TimeVal & since, 
 const std::string& format
)

{
	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();
	toolbox::TimeVal t;

	
	std::stringstream selectCatalogStmt;
	// Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	// make a full precision time comparison afterwards
	
	selectCatalogStmt << "select * from (select event.*,dateTime,identifier,occurrences,notifier,severity,message"
	<< ",schema,sessionid,tag,version,class,instance,lid,context,groups,service,zone,uuid"
	<< " from event left join catalog on (event.exception=catalog.uniqueid)"
	<< " left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid)"
	<< " where storeTime>"  << std::fixed << std::setprecision(6) << (double)since << " ORDER BY storeTime) where rownum <= " << scatterReadNum_;

	//std::cout << selectCatalogStmt.str() << std::endl;
	
	// Assume synchronous processing of the callback
	//
	oracle::occi::Statement* occiStmt = 0;

	try
	{
		occiStmt = connection_->createStatement(selectCatalogStmt.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	}


	oracle::occi::ResultSet* occiResult = 0;
	try
	{
		occiResult = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	}

	t = this->outputCatalogJSON(out, occiResult );

	if ( t  == toolbox::TimeVal::zero()  )
	{
		t = since;
	}

	occiStmt->closeResultSet(occiResult);
	connection_->terminateStatement(occiStmt);

	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double)(stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double)(stopChrono - startChrono))/2;
	}

	return t;
}

void sentinel::spotlightocci::DataBase::query
(
 xgi::Output* out,
 const std::string& query
)

{
	//std::cout << "Database::query: " << query << std::endl;
        toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();

        oracle::occi::Statement* occiStmt = 0;
        try
        {
                occiStmt = connection_->createStatement(query);
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
        }

        oracle::occi::ResultSet* occiResult = 0;

        try
        {
                occiResult = occiStmt->executeQuery();
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
        }

        this->outputCatalogJSONExtended(out, occiResult); // max number of returned exceptions is 1000

        occiStmt->closeResultSet(occiResult);
        connection_->terminateStatement(occiStmt);

        toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();

        if (averageTimeToRetrieveCatalog_ == 0.0)
        {
                // Initialize running average
                averageTimeToRetrieveCatalog_ = (double)(stopChrono - startChrono);
        }
        else
        {
                // Calculate running average
                averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double)(stopChrono - startChrono))/2;
        }
}

void sentinel::spotlightocci::DataBase::events
(
 xgi::Output* out, 
 toolbox::TimeVal & start, 
 toolbox::TimeVal & end,
 const std::string& format
)

{
	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();

	std::stringstream selectCatalogStmt;
	// Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	// make a full precision time comparison afterwards
		
	selectCatalogStmt << "select event.*,dateTime,identifier,occurrences,notifier,severity,message"
	<< ",schema,sessionid,tag,version,class,instance,lid,context,groups,service,zone,uuid"
	<< " from event left join catalog on (event.exception=catalog.uniqueid)"
	<< " left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid)"
	<< " where catalog.dateTime<=" << std::fixed << std::setprecision(6) << (double)end
	<< " and catalog.dateTime>=" << (double)start;

	//std::cout << selectCatalogStmt.str() << std::endl;

	oracle::occi::Statement* occiStmt = 0;
	try
	{
		occiStmt = connection_->createStatement(selectCatalogStmt.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	}

	oracle::occi::ResultSet* occiResult = 0;

	try
	{
		occiResult = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	}

	this->outputCatalogJSON(out, occiResult); // max number of returned exceptions is 1000

	occiStmt->closeResultSet(occiResult);
	connection_->terminateStatement(occiStmt);

	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();
	
	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double)(stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double)(stopChrono - startChrono))/2;
	}
}


/*
 ** Store a blob in database db. Return an SQLite error code.
 **
 ** This function inserts a new row into the blobs table. The 'key' column
 ** of the new row is set to the string pointed to by parameter zKey. The
 ** blob pointed to by zBlob,is stored in the 'value' 
 ** column of the new row.
 */ 
void sentinel::spotlightocci::DataBase::writeBlob(
											  const std::string & zKey,              /* Null-terminated key string */
											  const std::string & zBlob				/*  blob of data */
)

{
    /* Bind the key and value data for the new table entry to SQL variables
	 ** (the ? characters in the sql statement) in the compiled INSERT 
	 ** statement. 
	 **
	 ** NOTE: variables are numbered from left to right from 1 upwards.
	 ** Passing 0 as the second parameter of an sqlite3_bind_XXX() function 
	 ** is an error.
	 */


	try
	{
		insertExceptionStmt_->setString(1, zKey.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to set zKey parameter for insert exception statement. ") + e.what());
	}

	try
	{
		// Create an empty BLOB here, fill it later
		oracle::occi::Blob blob = oracle::occi::Blob(connection_);
		blob.setEmpty();

		insertExceptionStmt_->setBlob(2, blob);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to set zBlob parameter for insert exception statement. ") + e.what());
	}

	
	try
	{
		insertExceptionStmt_->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to insert exception statement. ") + e.what());
	}

	// Create BLOB to be inserted in the new record
	try
	{
		// Lock the blob column so it can be updated
		std::string qry = "SELECT exception FROM exceptions WHERE uniqueid = '" + zKey + "' FOR UPDATE";
		//std::cout << qry << std::endl;

		oracle::occi::Statement *blobStmt = connection_->createStatement(qry);
		oracle::occi::ResultSet *rset = blobStmt->executeQuery();
		rset->next();

		// Retrieve the existing empty blob to edit it
		oracle::occi::Blob blob = rset->getBlob(1);
		blob.open(oracle::occi::OCCI_LOB_READWRITE);
		blob.write(zBlob.length(), (unsigned char*)zBlob.c_str(), zBlob.length());
		blob.close();

		// Clean up
		blobStmt->closeResultSet(rset);
		connection_->terminateStatement(blobStmt);
		connection_->commit();
	}
	catch (oracle::occi::SQLException &e)
	{
		// Failed writing the blob
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToStore, std::string("Failed to insert exception BLOB into exceptions table. ") + e.what());
	}
}

/*
 ** Read a blob from database db. Return an SQLite error code.
 */ 
void sentinel::spotlightocci::DataBase::readBlob(
											 const std::string& zKey,          /* Null-terminated key to retrieve blob for */
											 std::string& pzBlob    /* Set *pzBlob to point to the retrieved blob */
)

{
	std::stringstream zSQL;
	
	
	zSQL << "select exception from exceptions where uniqueid='" << zKey << "'";

	oracle::occi::Statement* occiStmt = 0;

	try
	{
		occiStmt = connection_->createStatement(zSQL.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToRead, std::string("Failed to prepare statement for select exceptions. ") + e.what());
	}


	oracle::occi::ResultSet* occiResult = 0;
	try
	{
		occiResult = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToRead, std::string("Failed to select from exceptions. ") + e.what());
	}

	try
	{
		occiResult->next();

		unsigned char* blobPtr;
		unsigned int size;

		// Get the blob and read it into a char array
		oracle::occi::Blob blob = occiResult->getBlob(1);
		blob.open(oracle::occi::OCCI_LOB_READONLY);
		size = blob.length();

		blobPtr = new unsigned char[size];
		blob.read(size, blobPtr, size);

		// Load char array into pzBlob string
		pzBlob.assign((char*)blobPtr, size);
		delete[] blobPtr;
		blob.close();
	}
	catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToRead, std::string("Failed to read blob from exceptions. ") + e.what());
        }

	occiStmt->closeResultSet(occiResult);
	connection_->terminateStatement(occiStmt);
}

// check if exception alreadi exists
bool sentinel::spotlightocci::DataBase::exists(const std::string& zKey) 
{
	std::stringstream zSQL;


        zSQL << "select count(*) from catalog where uniqueid='" << zKey << "'";

        int count = 0;

        oracle::occi::Statement* occiStmt = 0;
        oracle::occi::ResultSet* occiResult = 0;
        try
        {

                occiStmt = connection_->createStatement(zSQL.str());
                occiResult = occiStmt->executeQuery();

                if (occiResult->next())
                {
                        count = occiResult->getInt(1);
                }

                occiStmt->closeResultSet(occiResult);
                connection_->terminateStatement(occiStmt);
        }
        catch (oracle::occi::SQLException &e)
        {
                XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to count from catalog table. ") + e.what());
        }


	return count > 0;
}


/*! Retrieve the number of exceptions stored */
std::string sentinel::spotlightocci::DataBase::getNumberOfExceptions() 
{
	int count = 0;

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;
	try
	{

		occiStmt = connection_->createStatement("select count(*) from catalog");
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			count = occiResult->getInt(1);
		}

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to count from catalog table. ") + e.what());
	}

	std::stringstream strcount;
	strcount << count;

	return strcount.str();
}

size_t sentinel::spotlightocci::DataBase::getSize()
{
	return 0;
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::spotlightocci::DataBase::getLatestStoreTime()
{
	return lastExceptionTime_;		
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::spotlightocci::DataBase::getLatestEventTime() 
{
	toolbox::TimeVal latestExceptionTime = toolbox::TimeVal::zero(); // invalid time


	oracle::occi::Statement* occiStmt = 0;
        oracle::occi::ResultSet* occiResult = 0;

    	try
        {
		
		occiStmt = connection_->createStatement("select count(storeTime), max(storeTime) from event");
		occiResult = occiStmt->executeQuery();
                
		if (occiResult->next())
		{
                        int count = 0;
                        count = occiResult->getInt(1);

			if (count != 0)
                        {
                                double maxStoreTime = 0.0;

				try
                                {
                                        maxStoreTime = occiResult->getDouble(2);
					toolbox::TimeVal t(maxStoreTime);
                                        latestExceptionTime = t;
                                }
                                catch (toolbox::exception::Exception& e)
                                {
                                        occiStmt->closeResultSet(occiResult);
					connection_->terminateStatement(occiStmt);

					// invalid time if conversion fails
					std::stringstream msg;
                			msg << "Failed to convert latestEventTime maxStoreTime:" << maxStoreTime;
                			XCEPT_RETHROW(sentinel::spotlightocci::exception::Exception, msg.str(),e);
                                }
                        }
                }
        }
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to count storeTime in event. ") + e.what());
	}

	occiStmt->closeResultSet(occiResult);
	connection_->terminateStatement(occiStmt);

	return latestExceptionTime;

// - LO
}
/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::spotlightocci::DataBase::getLatestExceptionTime() 
{
	toolbox::TimeVal latestExceptionTime = toolbox::TimeVal::zero(); // invalid time


	
	oracle::occi::Statement* occiStmt = 0;
        oracle::occi::ResultSet* occiResult = 0;

    	try
        {
		
		occiStmt = connection_->createStatement("select count(storeTimestamp), max(storeTimestamp) from exceptions");
		occiResult = occiStmt->executeQuery();

                if (occiResult->next())
		{
                        int count = 0;

                        count = occiResult->getInt(1);

			if (count != 0)
                        {
				std::string timestring;

				oracle::occi::Timestamp maxStoreTime;

                                try
                                {
					maxStoreTime = occiResult->getTimestamp(2);
					timestring = maxStoreTime.toText("yyyy/mm/dd hh:mi:ss", 0);

					toolbox::TimeVal t;
					t.fromString(timestring,"%Y-%m-%d %H:%M:%S", toolbox::TimeVal::loc);

                                        latestExceptionTime = t;
                                }
                                catch (toolbox::exception::Exception& e)
                                {
					occiStmt->closeResultSet(occiResult);
					connection_->terminateStatement(occiStmt);

                                        // invalid time if conversion fails
					std::stringstream msg;
                			//msg << "Failed to convert latestExceptionTime maxStoreTime:" << maxStoreTime;
                			XCEPT_RETHROW(sentinel::spotlightocci::exception::Exception, msg.str(),e);
                                }
                        }
                }

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
        }
	catch (oracle::occi::SQLException &e)
	{
		connection_->terminateStatement(occiStmt);
		XCEPT_RAISE(sentinel::spotlightocci::exception::Exception, std::string("Failed to count storeTime in exceptions. ") + e.what());
	}

	return latestExceptionTime;
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::spotlightocci::DataBase::getOldestExceptionTime()
{
	toolbox::TimeVal oldestExceptionTime = toolbox::TimeVal::zero(); // invalid time

	std::stringstream query;
        query << "select count(dateTime), min (dateTime) from catalog";

	oracle::occi::Statement* occiStmt = 0;
        oracle::occi::ResultSet* occiResult = 0;

	try
	{
		occiStmt = connection_->createStatement(query.str().c_str());
		occiResult = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
        {
		return oldestExceptionTime;
	}

	if (occiResult->next())
	{
		int count = occiResult->getInt(1);

		if (count != 0)
		{
			try
			{
				double datec = occiResult->getDouble(2);
				toolbox::TimeVal t(datec);
				oldestExceptionTime = t;
			}
			catch (toolbox::exception::Exception &e)
			{
				// invalid time if conversion fails
			}
		}
	}

	return oldestExceptionTime;
}




std::string sentinel::spotlightocci::DataBase::escape(const std::string& s)
{
	std::stringstream ss;
	size_t len = s.length();
	for(size_t i=0 ; i<len ; i++)
	{
		char c = s[i];
		switch(c)
		{
			case 'a' ... 'z':
			case 'A' ... 'Z':
			case '0' ... '9':
			case ';':
			case '/':
			case '?':
			case ':':
			case '@':
			case '&':
			case '=':
			case '+':
			case '$':
			case ',':
			case '-':
			case '_':
			case '.':
			case '!':
			case '~':
			case '*':
			case '\'':
			case '(':
			case ')':
				ss << c;
				break;
			default:
				ss << '%' << std::setbase(16) << std::setfill('0') << std::setw(2) << (int)(c);
				break;
		}
	}
	return ss.str();
}

void sentinel::spotlightocci::DataBase::lock()
{
	lock_.take();
}

void sentinel::spotlightocci::DataBase::unlock()
{
	lock_.give();
}

/*! Average time to store a single exception */
double sentinel::spotlightocci::DataBase::getAverageTimeToStore()
{
	return averageTimeToStore_;
}

/*! Average time to retrieve a catalog */
double sentinel::spotlightocci::DataBase::getAverageTimeToRetrieveCatalog()
{
	return averageTimeToRetrieveCatalog_;
}

/*! Average time to retrieve a single exception by uuid */
double sentinel::spotlightocci::DataBase::getAverageTimeToRetrieveException()
{
	return averageTimeToRetrieveException_;
}

void sentinel::spotlightocci::DataBase::remove(toolbox::TimeVal & age)

{
	// clear aged exceptions
	std::stringstream removeCatalogStmt;
	removeCatalogStmt << "delete from catalog where dateTime<" << std::fixed << std::setprecision(6) << (double)age;
	
	oracle::occi::Statement* occiStmt = 0;

	try
	{
		occiStmt = connection_->createStatement(removeCatalogStmt.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToRemove, std::string("Failed to prepare statement to remove from catalog statement. ") + e.what());
	}

	try
	{
		occiStmt->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		connection_->terminateStatement(occiStmt);
		XCEPT_RAISE(sentinel::spotlightocci::exception::FailedToRemove, std::string("Failed to execute remove from catalog statement. ") + e.what());
	}

	connection_->terminateStatement(occiStmt);
}	

