function pdebug( text )
{
  document.getElementById("debug").innerHTML = text;
};

function sendrequest( jsonurl )
{
var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      var res = eval( "(" + xmlhttp.responseText + ")" );
        for ( var set in res ) {
          var arr = res[set];
          for( var i=0; i<arr.length; i++ )
            {
              document.getElementById( arr[i].name ).innerHTML = arr[i].value;
            }
        }
    }
  };
xmlhttp.open("GET", jsonurl, true);
xmlhttp.send();
};

function startUpdate( jsonurl )
{
  var interval;
  interval = setInterval( "sendrequest( \"" + jsonurl + "\" )" , 1000 );

};

function amc13WebCommand( command, para )
{
    $("input#command").val( command );
    $("input#parameter").val( para );
    pdebug( "setting command to " + command + " and commandpara to " + para );
    $("form#debugForm").submit();
    //return -1;
};

function updateQueryString( uri, key, value )
{
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (!uri.includes("UpdateLevel") && !uri.includes("alternate")) 
    {   
        if (uri.slice(-1) != "/") uri += "/"; 
        uri += "UpdateLevel";
    }
    if (uri.match(re)) 
    {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else
    {
        return uri + separator + key + "=" + value;
    }  
}

function removeQuery( uri, key )
{
    var replaceString = new RegExp(key + "=.*?(&|$)");
    uri = uri.replace(replaceString, "");
    uri = uri.replace(/&$/,""); //if last char is &, remove it.
    return uri;
}

function levelChange( e )
{
    //prepend an 'l' for local, as this level is local to the table.
    window.location.href = updateQueryString(window.location.href, 'l' + e.name, e.value);
}

function globalLevelChange( e )
{
    //prepend a g for 'global', as this level is for all tables in the tab.
    window.location.href = updateQueryString(window.location.href, "g" + e.name, e.value);
}

function levelToggle( e )
{
    var uri = window.location.href;
    var queryValue;
    if (e.checked)
    {
        queryValue = "local";
        uri = removeQuery(uri, "g" + e.name);
    }
    else
    {
        queryValue = "global";

        //Get names of all the tables and remove their corresponding queries
        //do this by getting names of all select items on the page.
        var selects = document.getElementsByTagName("select");
        for(var i = 0; i < selects.length; i++)
        {
            var selectTab = selects[i].parentNode.parentNode.parentNode.parentNode.parentNode.id;
            if (selectTab == e.name)
               uri = removeQuery(uri,"l" + selects[i].name); 
        }
    }
    //prepend an m for 'mode', as this indicates what mode of adjusting 
    //levels is in use.
    window.location.href = updateQueryString(uri, "m" + e.name, queryValue);
}
