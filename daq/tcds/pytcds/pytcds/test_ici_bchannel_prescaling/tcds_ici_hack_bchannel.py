#!/usr/bin/env python

###############################################################################

import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class ICIHack(CmdLineBase):

    def main(self):
        lpm = get_lpm_hw(self.ip_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        for i in xrange(1, 3564, 10):
            print "DEBUG JGH {0:d}".format(i)
            # lpm.write("ici1.cyclic_generator2.configuration.start_bx", i)
            lpm.write("ici1.bchannels.bchannel11.configuration.bx_or_delay", i)
            time.sleep(1)

        # End of main().

    # End of class ICIHack.

###############################################################################

if __name__ == "__main__":

    ICIHack().run()
    print "Done"

###############################################################################
