#!/usr/bin/env python

###############################################################################
## Clears the PROM contents on an AMC13/CPM.
###############################################################################

import sys

from pytcds.utils.tcds_amc13_flasher import AMC13Flasher
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_hw_connect import get_amc13t2_hw
from pytcds.utils.tcds_utils_misc import chunkify
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PROMWiper(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        print "Wiping the full PROM contents on '{0:s}'.".format(self.target)
        amc13_t2 = get_amc13t2_hw(network_address, controlhub_address, verbose=self.verbose)

        flasher = AMC13Flasher(amc13_t2)
        data = flasher.erase_prom()

        # End of main().

    # End of class PROMWiper.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to erase the PROM contents on an AMC13/CPM."

    res = PROMWiper(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
