package xdaq.tools.dstorebrowser;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import xdaq.tools.dstorebrowser.NodeEditorPanel;
import xdaq.tools.dstorebrowser.dstoreTree;
import java.awt.*;
import xdaq.tools.win.xdaqWinTreeAdapterNode;

public class NodeEditorPopup extends MouseAdapter
{
    dstorebrowser mainWindow_;

    public NodeEditorPopup ( dstorebrowser mainWindow)
	{
	    mainWindow_ = mainWindow;
	}

    public void mouseClicked ( MouseEvent e )
	{
	    if (e.getClickCount() > 1)
	    {
		JTree tree_ = mainWindow_.tree_;
		int x = e.getX();
		int y = e.getY();
		TreePath path = tree_.getPathForLocation (x,y);
		Rectangle textPosition = mainWindow_.tree_.getRowBounds(tree_.getRowForLocation(x,y));
		
		if (path != null)
		{	
		    DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
		    if (node.isLeaf())
		    {
			xdaqWinTreeAdapterNode xdaqNode = (xdaqWinTreeAdapterNode) node.getUserObject();
			if (! xdaqNode.getAttribute("_type").startsWith("Module"))
			{
			    NodeEditorPanel ed = new NodeEditorPanel ( node, mainWindow_ );
			    JFrame parent = getFrame (tree_);
			    
			    ed.setLocation (textPosition.x+tree_.getX()+tree_.getParent().getParent().getX(),
					    textPosition.y+tree_.getY()+tree_.getParent().getParent().getY());
			    ed.setSize ( ed.getPreferredSize() );
			    
			    JLayeredPane rootPane = (JLayeredPane) parent.getLayeredPane();
			    if(rootPane.getComponentCountInLayer(JLayeredPane.POPUP_LAYER.intValue()) > 0) return;
			    rootPane.add(ed, JLayeredPane.POPUP_LAYER);
			    ed.setVisible(true);
			}
		    }
		}
	    }
	}

    private static JFrame getFrame(Component c) {
        Component w = c;

        while(!(w instanceof JFrame) && (w!=null)) {
            w = w.getParent();
        }
        return (JFrame)w;
    }
}
