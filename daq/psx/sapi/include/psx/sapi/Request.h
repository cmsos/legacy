// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_Request_h_
#define _psx_sapi_Request_h_

#include <string>
#include <set>
#include "toolbox/net/UUID.h"
     
namespace psx {
	namespace sapi {
	
		class ApplicationService;
		
		class Request 
		{
      			public:
			
			Request(const std::string & type, psx::sapi::ApplicationService * as) ;
			virtual ~Request();
		
			virtual std::string type();
			
			virtual psx::sapi::ApplicationService * getApplicationService();
			
			//! set \param ack to true if a SOAP reply must be created (default)
			//
			virtual void setAck(bool ack);
			
			//! Used to determine if a SOAP reply must be created, by default true
			//
			virtual bool getAck();


			virtual toolbox::net::UUID  getUUID();
			
			private:
			
			std::string type_;
			psx::sapi::ApplicationService * as_;
			bool ack_;
			toolbox::net::UUID uuid_;
		};
	}
}

#endif 

