// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_Sync_h_
#define _tstore_api_admin_Sync_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/admin/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
namespace admin
{
	class Sync: public tstore::api::Request
	{
		public:
		
		Sync(const std::string & connectionId, const std::string & mode, const std::string & pattern);
		
		Sync(xoap::MessageReference& msg) ;
        
		virtual ~Sync();
				
		xoap::MessageReference toSOAP();

		std::string getConnectionId();

		std::string getMode();

		std::string getPattern();

		private:

		std::string connectionId_;
		std::string mode_;
		std::string pattern_;
	};
}}}

#endif
