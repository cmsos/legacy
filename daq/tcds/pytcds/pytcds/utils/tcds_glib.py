###############################################################################
## Utilities related to the GLIB FMC-carrier board.
###############################################################################

import struct
import time

from pytcds.utils.tcds_carrier import Carrier
from pytcds.utils.tcds_constants import BOARD_TYPE_GLIB
from pytcds.utils.tcds_glib_flash_mem import FLASH_LIMITS
from pytcds.utils.tcds_glib_flash_mem import GlibFlashMem
from pytcds.utils.tcds_utils_mcs import MCSFile

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The time to wait after a firmware (re)load, in seconds.
# NOTE: When connecting directly to the board, one second is enough
# for the board to load the firmware and become responsive again. (Any
# attempt to communicate with the board before that time will lock it
# up completely.) When connecting through the controlhub there is a
# cache with a lifetime of 15 seconds. Only after 15 seconds of
# silence will the cache be destroyed and the next connection
# successfull. Another possibility is to try a dummy read/write and
# let that time-out. The side-effect will be the destruction of the
# cache associated with this connection.
TIME_WAIT_FIRMWARE_LOAD = 1

# The GLIB implements the XILINX ICAP business to load firmware from
# two difference images stored in flash memory:
# - page 0: The 'golden' image, supposed to always work. Loaded at power-on.
# - page 2: The 'user' image. Can be loaded via a command.
FLASH_PAGE_NUM_GOLDEN = 0
FLASH_PAGE_NUM_USER = 2

# Some magic constants related to cross-point switches and clock routing.
GLIB_XPOINT_AND_MASK = 0xff000fff

# These are the different sources of the clock, basically.
# - Select fmc1_clk0 (the clock coming from FMC1).
FMC1_CLK0_OR_MASK = 0x00000000
# - Select the GLIB on-board default clock (xtal/sma).
#   NOTE: The actual choice between the front-panel SMA input and the
#   on-board crystal is done with a jumper.
SMA_OR_XTAL_OR_MASK = 0x00aa0000
# - Select FCLKA (the clock coming from the backplane).
FCLKA_OR_MASK = 0x00ff0000

##########

# The I2C address of the EEPROM with the EUI-48.
# NOTE: Should be something like a 24AA025E48.
I2C_ADDRESS_EUI_EEPROM = 0x56

# This is the I2C address of the EEPROM storing the raw MAC and IP
# addresses.
I2C_ADDRESS_MAC_AND_IP_EEPROM = 0x56

# The address of the I2C multiplexer switching between the different
# SFP status types on the TCDS FMCs and the FMC module type register.
# NOTE: Adresses depend on the FMC number.
I2C_ADDRESS_MUX_SFP_STATUS = {
    "1" : 0x74,
    "2" : 0x77
}

# The address of the I2C multiplexer switching between the different
# SFPs on the TCDS FMCs.
# NOTE: Adresses depend on the FMC number.
I2C_ADDRESS_MUX_SFP_INFO_REGISTERS = {
    "1" : 0x70,
    "2" : 0x73
}

FMC_NAMES = ["1", "2"]

FMC_I2C_BUS_SELECTORS = {
    "1" : 0,
    "2" : 1
}

###############################################################################

class Glib(Carrier):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_GLIB
        # ASSERT ASSERT ASSERT end
        super(Glib, self).__init__(carrier_type, board_type, hw_interface, verbose)
        self.user_fw_base_name = "glib"
        # End of __init__().

    def choose_clock_source(self, source):
        # Configure the cross-point switch defining the clocking
        # scheme.
        reg_name = "glib.ctrl"
        ctrl = self.read(reg_name)

        or_mask = None
        tmp_source = source.lower()
        if tmp_source == "fclka":
            or_mask = FCLKA_OR_MASK
        elif tmp_source == "front_input":
            or_mask = SMA_OR_XTAL_OR_MASK
        elif tmp_source == "xtal":
            or_mask = SMA_OR_XTAL_OR_MASK
        elif tmp_source == "fmc1_clk0":
            or_mask = FMC1_CLK0_OR_MASK
        else:
            # ASSERT ASSERT ASSERT
            assert False, "Clock source '{0:s}' not understood.".format(source)
            # ASSERT ASSERT ASSERT end

        ctrl = (ctrl & GLIB_XPOINT_AND_MASK) | or_mask
        self.write(reg_name, ctrl)
        # End of choose_clock_source().

    def get_fmc_locations(self):
        # End of get_fmc_locations().
        return FMC_NAMES

    def get_fmc_i2c_bus_select(self, fmc):
        bus_select = FMC_I2C_BUS_SELECTORS[fmc]
        # End of get_fmc_i2c_bus_select().
        return bus_select

    def get_fmc_i2c_address_mux_sfp_status(self, fmc):
        i2c_address = I2C_ADDRESS_MUX_SFP_STATUS[fmc]
        # End of get_fmc_i2c_address_mux_sfp_status().
        return i2c_address

    def get_fmc_i2c_address_mux_sfp_info_registers(self, fmc):
        i2c_address = I2C_ADDRESS_MUX_SFP_INFO_REGISTERS[fmc]
        # End of get_fmc_i2c_address_mux_sfp_info_registers().
        return i2c_address

    def is_fmc_powered(self, fmc):
        # The FMCs on the GLIB are always powered.
        # End of is_fmc_powered().
        return True

    def write_firmware(self, image_name, image_file_name, verbose=None):
        """Programs a firmware image from a .mcs (or a .mcs.gz) file."""

        if not image_name in ["golden", "user"]:
            msg = "Image name '{0:s}' is not a valid GLIB firmware image name. " \
                  "Try 'golden' or 'user'.".format(image_name)
            raise ValueError(msg)

        if not image_file_name.endswith(".mcs") and \
           not image_file_name.endswith(".mcs.gz"):
            msg = "GLIB firmware needs to be in the format of a .mcs(.gz) file. " \
                  "File '{0:s}' is not a .mcs file.".format(image_file_name)
            raise ValueError(msg)

        if verbose:
            msg = "Reading firmware image from file '{0:s}'."
            print msg.format(image_file_name)
        mcs_parser = MCSFile(image_file_name)
        mcs_parser.parse_file()
        image_data = mcs_parser.bitstream()
        self._write_firmware_image(image_name, image_data)
        # End of write_firmware().

    def _write_firmware_image(self, image_name, image_data, verbose=None):
        (block_num_lo, block_num_hi) = FLASH_LIMITS[image_name]
        num_blocks = block_num_hi - block_num_lo + 1
        num_blocks_done = 0.
        # NOTE: The bitstream data works in 8-bit bytes, so we have
        # to convert to 16-bit flash entries.
        num_bytes = len(image_data)
        num_words = num_bytes / 2
        # ASSERT ASSERT ASSERT
        assert num_words * 2 == num_bytes
        # ASSERT ASSERT ASSERT end
        image_data = list(struct.unpack("{0:d}H".format(num_words), image_data))
        num_words_done = 0
        with GlibFlashMem(self) as flash_mem:
            for block_num in xrange(block_num_hi, block_num_lo - 1, -1):
                block_size = flash_mem.get_block_size(block_num)
                ind_lo = num_words_done
                ind_hi = ind_lo + block_size
                bitstream_block = image_data[ind_lo:ind_hi]
                flash_mem.unlock_block(block_num)
                flash_mem.erase_block(block_num)
                if len(bitstream_block) > 0:
                    flash_mem.write_block(block_num, bitstream_block)
                flash_mem.lock_block(block_num)
                perc = 100. * num_blocks_done / num_blocks
                print "{0:5.1f}% written".format(perc)
                num_blocks_done += 1
                num_words_done += len(bitstream_block)
        perc = 100. * num_blocks_done / num_blocks
        print "{0:5.1f}% written".format(perc)
        # End of _write_firmware_image().

    def load_firmware(self, image_name):
        # NOTE: After reloading the firmware the IPbus connection will
        # be completely mucked up.

        # Tell the hardware which firmware page to jump to.
        if image_name == "golden":
            image_number = FLASH_PAGE_NUM_GOLDEN
        elif image_name == "user":
            image_number = FLASH_PAGE_NUM_USER
        else:
            # ASSERT ASSERT ASSERT
            msg = "ERROR Unknown firmware image: '{0:s}'.".format(image_name)
            assert False, msg
            # ASSERT ASSERT ASSERT end
        self.write("glib.ctrl2.flash_firmware_page", image_number)

        # Perform the jump/load the firmware.
        self.write("glib.ctrl2.load_flash_firmware", 0x1)

        # Give the board a little bit of time to recover.
        time.sleep(TIME_WAIT_FIRMWARE_LOAD)
        # End of load_firmware().

    # End of class Glib.

###############################################################################

class TCDSGlib(Glib):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        # ASSERT ASSERT ASSERT
        assert carrier_type == BOARD_TYPE_GLIB
        # ASSERT ASSERT ASSERT end
        super(TCDSGlib, self).__init__(carrier_type, board_type, hw_interface, verbose)
        # End of __init__().

    # def is_ttc_clock_up(self):
    #     res = self.read("fmc_main.ttc_clock_up")
    #     # End of is_ttc_clock_up().
    #     return res

    # End of class TCDSGlib.

###############################################################################
