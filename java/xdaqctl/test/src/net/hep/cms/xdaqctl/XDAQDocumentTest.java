/*
 * XDAQDocumentTest
 *
 * $Id: XDAQDocumentTest.java,v 4.1 2007/03/26 14:40:33 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import junit.framework.*;
import junit.extensions.TestSetup;

import java.io.*;
import java.util.Vector;

// DOM
import org.w3c.dom.*;

/**
 * XDAQDocumentTest
 */
public class XDAQDocumentTest extends TestCase
{
	private static final long WAIT = 1000; // [ms]
	private boolean configured = false;

	final String PARAMETER_HEADER = 
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
			"<p:properties " +
			"xmlns:p=\"urn:xdaq-application:TestXctl\" " +
			"xmlns:soapenc=\"http://schemas.xmlsoap.org/soap/encoding\" " +
			"xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
			"xsi:type=\"soapenc:Struct\">";
	final String PARAMETER_TRAILER = 
			"</p:properties>\n";

	public XDAQDocumentTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(XDAQDocumentTest.class);
	}

	public void testXDAQDocument() throws Exception
	{
		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		assertNotNull(document);

		String result = TestUtil.compareDOMs(
				TestUtil.fileToDOM("test/xdaq/config.xml"),
				document.getDOMDocument());

		assertNull(result, result);
	}

	public void testXDAQDocumentFromString() throws Exception
	{
		File file = new File("test/xdaq/config.xml");
		FileReader reader = new FileReader(file);
		char[] buffer = new char[(int)file.length()];
		reader.read(buffer);
		reader.close();

		String string = new String(buffer);

		XDAQDocument document = new XDAQDocument(new StringReader(string));
		assertNotNull(document);

		String result = TestUtil.compareDOMs(
				TestUtil.fileToDOM("test/xdaq/config.xml"),
				document.getDOMDocument());

		assertNull(result, result);
	}

	public void testAssignLocalID() throws Exception
	{
		// auto0 becomes same as auto1 after the ID assignment
		XDAQDocument auto0 = new XDAQDocument("test/xdaq/auto0.xml");
		auto0.assignLocalID();

		XDAQDocument auto1 = new XDAQDocument("test/xdaq/auto1.xml");

		String result = TestUtil.compareDOMs(
				auto1.getDOMDocument(), auto0.getDOMDocument());
		assertNull(result, result);
	}

	public void testXDAQDocumentCopy() throws Exception
	{
		String result;

		// Original document
		XDAQDocument original = new XDAQDocument("test/xdaq/config.xml");

		// Make a copy
		XDAQDocument copy = new XDAQDocument(original);

		// Compare
		result = TestUtil.compareDOMs(
				original.getDOMDocument(), copy.getDOMDocument());
		assertNull(result, result);

		// Change copy
		Document d = copy.getDOMDocument();
		d.getDocumentElement().appendChild(d.createElement("Foo"));

		// Not same any more
		result = TestUtil.compareDOMs(
				original.getDOMDocument(), copy.getDOMDocument(), false);
		assertNotNull("Modified copy is still the same.", result);
	}

	public void testConfigureHost() throws Exception
	{
		String good_result, result;

		// Clean up the executive
		restartXDAQ();

		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		
		// First, it should work.
		good_result = "ConfigureHost: http://localhost:40000: Success\n";
		result = document.configureHost();
		configured = true;

		assertEquals(good_result, result);

		// Then, it should fail.
		result = document.configureHost();

		assertTrue(result,
				result.contains("ConfigureHost: http://localhost:40000:"));
		assertTrue(result, result.contains("urn:xdaq-soap:3.0:Configure"));
		assertTrue(result, result.contains("raised at processIncomingMessage"));

		// Clean up the executive
		restartXDAQ();
	}

	public void testGetResponseString() throws Exception
	{
		String good_result, result;
		final String LEAD = "Command: http://localhost:40000: ";

		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		if (!configured) {
			document.configureHost();
			configured = true;
		}
		
		// First, to all the applications
		good_result = LEAD + "TestXctl: 0: Success\n" +
				LEAD + "TestXctl: 1: Success\n";
		result = document.getResponseString("Command");

		assertEquals(good_result, result);

		// Second, to a specific class
		good_result = LEAD + "TestXctl: 0: Success\n" +
				LEAD + "TestXctl: 1: Success\n";
		result = document.getResponseString("Command", "TestXctl");

		assertEquals(good_result, result);

		// Third, to a specific class and instance
		good_result = LEAD + "TestXctl: 1: Success\n";
		result = document.getResponseString("Command", "TestXctl", 1);

		assertEquals(good_result, result);
	}

	public void testGetResponseDOM() throws Exception
	{
		String good_result, result;

		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		if (!configured) {
			document.configureHost();
			configured = true;
		}

		good_result =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<xdaq:CommandResponse xmlns:xdaq=\"urn:xdaq-soap:3.0\"/>\n";

		result = TestUtil.domToString(
				document.getResponseDOM("Command", "TestXctl", 1));

		assertEquals(good_result, result);
	}

	public void testGetResponseStringWithDOMCommand() throws Exception
	{
		String good_result, result;

		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		if (!configured) {
			document.configureHost();
			configured = true;
		}
		
		good_result = "Structured command: http://localhost:40000: " +
				"TestXctl: 1: Success\n";

		File file = new File("test/xdaq/command.xml");
		FileReader reader = new FileReader(file);
		char [] buffer = new char[(int)file.length()];
		reader.read(buffer);
		reader.close();
		String domString = new String(buffer);

		result = document.getResponseString(domString, "TestXctl", 1);

		assertEquals(good_result, result);
	}

	public void testGetContextList() throws Exception
	{
		XDAQDocument document = new XDAQDocument("test/xdaq/auto1.xml");

		Vector list = document.getContextList();

		assertNotNull(list);
		assertEquals(2, list.size());
		assertEquals("http://localhost:40000", list.get(0));
		assertEquals("http://localhost:40001", list.get(1));
	}

	public void testGetApplicationList() throws Exception
	{
		XDAQDocument document = new XDAQDocument("test/xdaq/auto1.xml");

		Vector list;
		
		list = document.getApplicationList();

		assertNotNull(list);
		assertEquals(1, list.size());
		assertEquals("TestXctl", list.get(0));

		list = document.getApplicationList("http://localhost:40001");

		assertNotNull(list);
		assertEquals(1, list.size());
		assertEquals("TestXctl", list.get(0));
	}

	public void testGetInstanceList() throws Exception
	{
		XDAQDocument document = new XDAQDocument("test/xdaq/auto1.xml");

		Vector list;
		
		list = document.getInstanceList("TestXctl");

		assertNotNull(list);
		assertEquals(4, list.size());
		assertEquals(0, list.get(0));
		assertEquals(3, list.get(3));

		list = document.getInstanceList("http://localhost:40001", "TestXctl");

		assertNotNull(list);
		assertEquals(2, list.size());
		assertEquals(2, list.get(0));
		assertEquals(3, list.get(1));

		list = document.getInstanceList("Foo");

		assertNotNull(list);
		assertEquals(0, list.size());
	}

	public void testGetParameter() throws Exception
	{
		String good_result, result;
		XDAQParameter parameter;

		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		if (!configured) {
			document.configureHost();
			configured = true;
		}
		
		// Get parameter and check it
		good_result =
				PARAMETER_HEADER +
				"<p:line xsi:type=\"xsd:string\">foo</p:line>" +
				"<p:nullstr xsi:type=\"xsd:string\"></p:nullstr>" +
				"<p:number xsi:type=\"xsd:unsignedLong\">99</p:number>" +
				PARAMETER_TRAILER;

		parameter = document.getParameter("TestXctl", 1);
		assertNotNull(parameter);

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);

		parameter = document.getParameter("TestXctl", 9);
		assertNull(parameter);
	}

	public void testGetParameterWithString() throws Exception
	{
		String good_result, result;
		XDAQParameter parameter;

		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		
		// Get parameter and check it
		good_result =
				PARAMETER_HEADER +
				"<p:line xsi:type=\"xsd:string\">foo</p:line>" +
				"<p:number xsi:type=\"xsd:unsignedLong\">99</p:number>" +
				PARAMETER_TRAILER;

		parameter = document.getParameter("TestXctl", 1, good_result);
		assertNotNull(parameter);

		result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), parameter.getDOMParameter());
		assertNull(result, result);
	}

	public void testTimeout() throws Exception
	{
		XDAQDocument document = new XDAQDocument("test/xdaq/config.xml");
		if (!configured) {
			document.configureHost();
			configured = true;
		}

		// 'Freeze' the application
		document.getResponseDOM("Freeze", "TestXctl", 1);

		document.setTimeout(1000);
		assertEquals(1000, document.getTimeout());

		Document reply = null;

		long before, after;
		before = System.currentTimeMillis();
		try {
			reply = document.getResponseDOM("Command", "TestXctl", 1);
		} catch (XDAQTimeoutException ignored) {}
		after = System.currentTimeMillis();
		assertEquals("expected 1000[ms] timeout, but was " +
				(after - before) + "[ms].",
				5, (after - before) / 200); // 1000..1199[ms]

		assertNull(reply);

		// Clean up the executive
		restartXDAQ();
	}

	private void restartXDAQ() throws Exception
	{
		XDAQDocumentTest.oneTimeTearDown();
		XDAQDocumentTest.oneTimeSetUp();
		configured = false;
	}

	private static void oneTimeSetUp() throws Exception
	{
		Process boot = Runtime.getRuntime().exec("./test/xdaq/bootxdaq start");
		boot.waitFor();
		String line = new BufferedReader(
				new InputStreamReader(boot.getInputStream())).readLine();
		assertNull("Failed to boot a XDAQ executive", line);

		Thread.sleep(WAIT);
	}

	private static void oneTimeTearDown() throws Exception
	{
		Runtime.getRuntime().exec("./test/xdaq/bootxdaq stop").waitFor();
		Thread.sleep(WAIT);
	}

	public static Test suite()
	{
		TestSuite suite = new TestSuite(XDAQDocumentTest.class);

		TestSetup wrapper = new TestSetup(suite) {
			protected void setUp() throws Exception { oneTimeSetUp() ; }
			protected void tearDown() throws Exception { oneTimeTearDown(); }
		};

		return wrapper;
	}
}

// End of file
// vim: set ts=4 sw=4:
