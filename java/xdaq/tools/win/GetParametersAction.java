package xdaq.tools.win;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;
import org.w3c.dom.*;
import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.ParameterTableModel;

public class GetParametersAction implements ActionListener
{
	xdaqWin win_;
	ParameterTableModel model_;
	PropertiesWindow propertiesWin_;
	String what_ = "";

	public GetParametersAction (xdaqWin win, PropertiesWindow propertiesWin)
	{
		win_ = win;
		propertiesWin_ = propertiesWin;
	}
	
	public SOAPMessage sendCommand (String targetAddr, String url, String className) 
	{
		try 
		{
			xdaqSOAPMessage msg =  new xdaqSOAPMessage ( "ParameterGet", targetAddr );

			// Deep export -> export of vector elements
			org.w3c.dom.Node root = model_.exportSelection (true,"urn:xdaq-application:"+ className, className );
			
			if ( root.hasChildNodes() )
			{					
				msg.setParameters (root.getFirstChild() );
				xdaqSOAPConnection con = new xdaqSOAPConnection();

				//statusPane_.advanceProgressBar (1);
				return con.call ( msg, url );
			} else 
			{
				what_ = "no parameters were selected";
			}
		} catch (xdaqSOAPException e) 
		{
			System.out.println (e);
		} 

		return null;
	}
	
	public void actionPerformed (ActionEvent e)
	{	
		what_ = "";	
		model_ = propertiesWin_.getModel();
		
		String errorString = "";
		TreePath[] paths = propertiesWin_.getSelection();
		
		for (int i = 0; i < paths.length; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();	
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();
			if (node.toString().equals("Context") || node.toString().equals ("Application") || node.toString().equals ("Transport"))
			{
				String targetAddress;
				String url;
			 	String className;	

				if (node.toString().equals ("Application") )
				{
				  	xdaqWinTreeAdapterNode father = ((xdaqWinTreeAdapterNode) node.getUserObject()).getParent();
				  	targetAddress = aNode.getAttribute ("id");
				  	url = father.getAttribute ("url");
					className = aNode.getAttribute("class");
				} else {
					targetAddress = new String ("0"); // executive tid
					url = aNode.getAttribute ("url");
					className = "Executive";
				}
				
				SOAPMessage reply = sendCommand ( targetAddress, url, className );

				if (reply != null)
				{	
					xdaqSOAPMessage replyMsg = new xdaqSOAPMessage(reply);
					Document document = replyMsg.getDOM();
					//NodeList faultList = document.getElementsByTagName ("faultstring");
					// match of faultstring tag in all namespaces
					NodeList faultList = document.getElementsByTagNameNS ("*", "faultstring");
					if (faultList.getLength() == 1)
					{
					  	errorString += url + " (";
						errorString += faultList.item(0).getFirstChild().getNodeValue();
						errorString += ")\n";					
					} else {
						NodeList l = document.getElementsByTagNameNS ("*","ParameterGetResponse");
						if (l.getLength() == 1)
						{
							// find bag
							NodeList parameters = l.item(0).getChildNodes();
							for (int j=0; i < parameters.getLength(); j++ ) {
								System.out.println("found parameters:"+parameters.item(j).getNodeName());
							
								if ( parameters.item(j).getLocalName().equals("properties") ) {
									model_.update(parameters.item(j));
									break;
								}
							}
						} else {
							errorString += url + " (";
							errorString += "ParameterGet did not contain a response";
							errorString += ")\n";
						}
					}
				} else { // The action failed...
					errorString += url;
					errorString += what_;
					errorString += '\n';
				}
			} else {
				return; // don't update information
			}
		} // end for
		if (errorString != "") 
		{
			JOptionPane.showMessageDialog ( null, errorString, "ParameterGet Error", JOptionPane.ERROR_MESSAGE );
		} 	
	}		
}
