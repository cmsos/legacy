
// Basic GUI components
import javax.swing.tree.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.JEditorPane;
import javax.swing.*;
import java.lang.*;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// GUI support classes
import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;

// For creating borders
import javax.swing.border.EmptyBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
// For creating a TreeModel
import java.util.*;
import java.net.*;
import java.io.*;
import XMLLoader.*;

class ControlTable extends AbstractTableModel {

        String [] columnsNames = {  "Class",  "Instance", "Id", "Context" ,"Selection" };
        Vector data = new Vector();
/*
        public void load (String urlStr) {
                XMLLoader loader = new XMLLoader();
                Document document = loader.load(urlStr);

                NodeList elements = document.getElementsByTagName("SearchKey");
                //tableSize = elements.getLength();
                //expressionsTable = new String[tableSize][2];

                for (int i=0; i< elements.getLength(); i++ ) {
                         NamedNodeMap expAttributes = elements.item(i).getAttributes();
                         String pattern = expAttributes.getNamedItem("pattern").getNodeValue();
                         //String regexp = expAttributes.getNamedItem("regexp").getNodeValue();
                         String engine = expAttributes.getNamedItem("engine").getNodeValue();
                         String maxpages = expAttributes.getNamedItem("maxPages").getNodeValue();
                         
                         this.addRow(pattern,maxpages,engine,Boolean.TRUE);
                         
                }

        }
	
*/	
        public void addRow( String className, String instance, String id, String context, Boolean selection) {

                Vector row = new Vector();
                row.add(className);
                row.add(instance);
                row.add(id);
                row.add(context);
		row.add(selection);
                data.add(row);

        }

        public SearchEngineTable () {

        }

        public int getRowCount() {
                return data.size();
        }

        public int getColumnCount() {
                return columnsNames.length;
        }

        public Object getValueAt(int r, int c ) {
                //Vector v = (Vector)data.elementAt(r);
                return ((Vector)data.elementAt(r)).elementAt(c);

        }

        public String getColumnName(int c) {

                return columnsNames[c];
        }

        public Class getColumnClass(int c) {

                return getValueAt(0,c).getClass();
        }
        public boolean isCellEditable( int r, int c ) {
                if ( (columnsNames[c] == "Key") || (columnsNames[c] == "Selection") || (columnsNames[c] == "MaxPages")||
                (columnsNames[c] == "Engine")) {
                        return true;
                }
                return false;

        }

        public void setValueAt(Object value, int row, int col) {
                Vector v = (Vector)data.elementAt(row);
                v.setElementAt(value,col);
                fireTableCellUpdated(row,col);
        }

        public boolean getSelection(int row) {
                return ((Boolean)getValueAt(row, 3)).booleanValue();
        }

        public String getPattern(int row) {
                return (String)getValueAt(row, 0 );
        }
        public String getEngine(int row) {
                return (String)getValueAt(row, 2 );
        }

        public int getMaxPages(int row) {
                String str =  (String)getValueAt(row, 1 );
                Integer val = new Integer(str);
                return val.intValue();
        }
        /*
        public String getIndex(int row) {
                return (String)getValueAt(row, 1 );
        }


        public void replaceRow(String name, String value, String type) {
                for ( int i = 0; i < data.size(); i++ ) {
                        if ( name.equals(getName(i)) ) {
                                setValueAt(value,i,1);

                        }

                }


        }
        */
}
