// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci	 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef xoap_SOAPConstants_h
#define xoap_SOAPConstants_h

#include <string>

namespace xoap
{

	namespace SOAPConstants
	{

		const std::string	DEFAULT_SOAP_PROTOCOL = "SOAP 1.1 Protocol";
		const std::string	DYNAMIC_SOAP_PROTOCOL = "Dynamic Protocol";
		const std::string	SOAP_1_1_CONTENT_TYPE = "text/xml";
		const std::string	SOAP_1_1_PROTOCOL     =	"SOAP 1.1 Protocol";
		const std::string	SOAP_1_2_CONTENT_TYPE =	"application/soap+xml";
		const std::string	SOAP_1_2_PROTOCOL     =	"SOAP 1.2 Protocol";
		const std::string	SOAP_ENV_PREFIX	      = "env";
		const std::string	URI_NS_SOAP_1_1_ENVELOPE   =	"http://schemas.xmlsoap.org/soap/envelope/";
		const std::string	URI_NS_SOAP_1_2_ENCODING   =	"http://www.w3.org/2003/05/soap-encoding";
		const std::string	URI_NS_SOAP_1_2_ENVELOPE   =	"http://www.w3.org/2003/05/soap-envelope";
		const std::string	URI_NS_SOAP_ENCODING   =	"http://schemas.xmlsoap.org/soap/encoding/";
		const std::string	URI_NS_SOAP_ENVELOPE   =	"http://schemas.xmlsoap.org/soap/envelope/";
		const std::string	URI_SOAP_1_2_ROLE_NEXT   =	"http://www.w3.org/2003/05/soap-envelope/role/next";
		const std::string	URI_SOAP_1_2_ROLE_NONE   =	"http://www.w3.org/2003/05/soap-envelope/role/none";
		const std::string	URI_SOAP_1_2_ROLE_ULTIMATE_RECEIVER   =	"http://www.w3.org/2003/05/soap-envelope/role/ultimateReceiver";
		const std::string	URI_SOAP_ACTOR_NEXT   =	"http://schemas.xmlsoap.org/soap/actor/next";
	};
}

#endif

