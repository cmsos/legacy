function HelpHover()
{
	this._mousePosX = 0;
	this._mousePosY = 0;
	this._hoverItem = null;
	this._hoverContents = null;
}

HelpHover.prototype.init = function()
{
	var hh = this;
	var helpItems = document.getElementsByClassName('hasHelp');
	for (var i=0; i<helpItems.length; i++)
	{
		helpItems[i].onmousemove = function(e)
		{
			if (!e) var e = window.event;
			
			if (e.pageX || e.pageY)
			{
				hh.mousePosX = e.pageX;
				hh.mousePosY = e.pageY;
			}
			else if (e.clientX || e.clientY)
			{
				hh.mousePosX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
				hh.mousePosY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
			}
			
			hh._hoverItem = this;
			hh._hoverContents = document.getElementById(this.id+'Help');
			hh.move();
		}
		helpItems[i].onmouseout = function (e)
		{
			hh.out();
		}
	}
}

HelpHover.prototype.out = function()
{
	this._hoverContents.style.top = -10000+'px';
	this._hoverContents.style.left = -10000+'px';
	this._hoverItem = null;
	this._hoverContents = null;
}

HelpHover.prototype.move = function()
{
	this._hoverContents.style.top = this.mousePosY+10+'px';
	this._hoverContents.style.left = this.mousePosX+10+'px';
}

addEvent(window, 'load', function()
{
	var hh = new HelpHover();
	hh.init();
});

