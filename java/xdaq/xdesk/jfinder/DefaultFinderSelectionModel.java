package xdaq.xdesk.jfinder;

import java.util.*;
import javax.swing.JLabel;
import javax.swing.event.*;

public class DefaultFinderSelectionModel
implements FinderSelectionModel {
	int selectionMode_;
	Vector cells_; // selected cells
	EventListenerList listenerList_ = new EventListenerList();
	
	public DefaultFinderSelectionModel() {
		selectionMode_ = SINGLE_SELECTION;
		cells_ = new Vector();
	}
	
	public void addFinderSelectionListener(FinderSelectionListener l) {
		listenerList_.add(FinderSelectionListener.class, l);
	}
	
	public void removeFinderSelectionListener(FinderSelectionListener l) {
		listenerList_.remove(FinderSelectionListener.class, l);
	}
	
	public void clearSelection() {
		System.out.println("Clear Selection.");
		JLabel[] oldSelection = this.getSelectionCells();
		cells_.removeAllElements();
		this.fireTreeNodesChanged(this, oldSelection, this.getSelectionCells());
	}
	
	public int getSelectionMode() {
		return selectionMode_;
	}
	
	public void setSelectionMode(int mode) {
		selectionMode_ = mode;
	}
	
	public void setSelectionCell(JLabel n) {
		JLabel[] oldSelection = this.getSelectionCells();
		if (oldSelection.length != 0) {
			cells_.removeAllElements();
		}
		cells_.add(n);
		this.fireTreeNodesChanged(this, oldSelection, this.getSelectionCells());
	}
	
	public void addSelectionCell(JLabel n) {
		JLabel[] oldSelection = this.getSelectionCells();
		cells_.add(n);
		this.fireTreeNodesChanged(this, oldSelection, this.getSelectionCells());
	}
	
	public void removeSelectionCell(JLabel n) {
		JLabel[] oldSelection = this.getSelectionCells();
		cells_.remove(n);
		this.fireTreeNodesChanged(this, oldSelection, this.getSelectionCells());
	}
	
	public JLabel[] getSelectionCells() {
		return (JLabel[]) cells_.toArray(new JLabel[] {});
	}
	
	public JLabel getSelectionCell() {
		return (JLabel) cells_.firstElement();
	}
	
	public int getSelectionCount() {
		return cells_.size();
	}
	
	public boolean isNodeSelected(JLabel n) {
		return cells_.contains(n);
	}
	
	public boolean isSelectionEmpty() {
		return cells_.isEmpty();
	}
	
	// ----------------------------------------------------------------
	
	protected void fireTreeNodesChanged(Object source, JLabel[] oldSelection,
										JLabel[] newSelection) {
		Object[] listeners = listenerList_.getListenerList();
		FinderSelectionEvent e = null;
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == FinderSelectionListener.class) {
				if (e == null) {
					e = new FinderSelectionEvent(source, oldSelection, newSelection);
				}
				( (FinderSelectionListener) listeners[i + 1]).valueChanged(e);
			}
		}
	}
	
}
