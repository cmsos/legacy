// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/




//#include "xoap/HTTPURLConnection.h"
#include "xoap/memSearch.h"
#include "xoap/exception/HTTPException.h"


#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <string.h>
#include <string>
#include <sstream>
#include <unistd.h>
#include <cstdio>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>     
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <netdb.h>

#include "es/api/HTTPURLConnection.h"

const size_t ReadSegmentSize = 8192;

es::api::HTTPURLConnection::HTTPURLConnection() 
{
	socket_ = -1;
}


void es::api::HTTPURLConnection::open() 
{
	socket_ = socket(AF_INET, SOCK_STREAM, 0);
   	if (socket_ == -1)
  	{
		XCEPT_RAISE(es::api::exception::Exception,  strerror(errno)  );
		
	}
	int optval = 1;

   	if (setsockopt(socket_, SOL_SOCKET, SO_REUSEADDR, (char *)&optval, sizeof(optval)) < 0)
  	{
		XCEPT_RAISE(es::api::exception::Exception,  strerror(errno)  );
  	}
}

es::api::HTTPURLConnection::~HTTPURLConnection()
{
	this->close();
}

void es::api::HTTPURLConnection::connect(const std::string& hostname, unsigned int port) 
{	
	// test if socket_ is valid. if not, recreate
	if (socket_ == -1)
	{
		this->open();
	}
	
	unsigned char ipnumber[4];

	struct hostent* hP = gethostbyname(hostname.c_str());
	if ( hP != (struct hostent*)0 ) 
	{
		// it could resolve the name to a IP number
		char* inAddr = hP->h_addr_list[0];
		ipnumber[0] = inAddr[0];
		ipnumber[1] = inAddr[1];
		ipnumber[2] = inAddr[2];
		ipnumber[3] = inAddr[3];
	} else 
	{ // it assumes it is a valid ip number
		in_addr_t inAddr = inet_addr(hostname.c_str());

		ipnumber[0] = (unsigned char)(((char*)&inAddr)[0]);
		ipnumber[1] = (unsigned char)(((char*)&inAddr)[1]);
		ipnumber[2] = (unsigned char)(((char*)&inAddr)[2]);
		ipnumber[3] = (unsigned char)(((char*)&inAddr)[3]);	
	}


	char host[256];
	 
	std::sprintf (host, "%d.%d.%d.%d", ipnumber[0], ipnumber[1], ipnumber[2], ipnumber[3]);

	in_addr_t inetaddr;
	//  Set connection info
	if ((inetaddr = inet_addr(host)) == (in_addr_t)-1)
	{
		::close(socket_);
		XCEPT_RAISE(es::api::exception::Exception,  strerror(errno)  );
	}
	
	struct sockaddr_in  writeAddr;
	unsigned int writeAddrLen = sizeof(writeAddr);
	bzero((char *)&writeAddr, writeAddrLen);

	writeAddr.sin_family = AF_INET;
	writeAddr.sin_port = htons(port);
	writeAddr.sin_addr.s_addr = inetaddr;

	if (::connect(socket_, (struct sockaddr *)&writeAddr, writeAddrLen) == -1)
	{
		::close(socket_);
		XCEPT_RAISE(es::api::exception::Exception,  strerror(errno)  );
	}
}

void es::api::HTTPURLConnection::close()
{
	::close(socket_);
	socket_ = -1;
}


ssize_t es::api::HTTPURLConnection::receive(char * buf, size_t len ) 
{
	ssize_t r = recv (socket_, buf, len, 0);
	if (r < 0)
	{
		XCEPT_RAISE (es::api::exception::Exception, strerror(errno));
	}
	return r;
}

void es::api::HTTPURLConnection::send(const char * buf, size_t len) 
{
	//std::cout << "buffer sent: [" << buf << "]" << std::endl;
	ssize_t toWrite = len;
        ssize_t nBytes = 0;

        while (toWrite > 0) 
	{
		nBytes = write(socket_,(char*)&buf[nBytes],toWrite);
		if (nBytes < 0)
		{
			XCEPT_RAISE(es::api::exception::Exception,  strerror(errno)  );
		}
		toWrite -= nBytes;
        }
}

std::string es::api::HTTPURLConnection::receiveFrom() 
{
	char* buffer = 0;
	ssize_t dataOffset = 0;
	ssize_t nBytes = 0;

	buffer = new char [ReadSegmentSize];
	ssize_t available = ReadSegmentSize;

	// Read lines (up to a CRLF) in 255 bytes quantities
	// up to an empty line => \n\n
	ssize_t start = 0;
	ssize_t cursor = start;
	unsigned int foundCR = 0; // number of '\n'
	unsigned int foundLN = 0;

	
	do {
		//
		nBytes =  this->receive(&buffer[cursor], 1);

		if ( nBytes > 0 ) 
		{	
			available -= nBytes;

			if (available == 0) 
			{
				delete buffer;
				std::string msg = "Minimum size for reading HTTP header too small (redimension maxMtuSize in HTTPReceiveEntry";
				XCEPT_RAISE (es::api::exception::Exception, msg);
			}

			if ( buffer[cursor] == '\n'  )  
			{
				foundLN++;
			}
			else if ( buffer[cursor] == '\r' ) 
			{
				foundCR++;
			}
			else 
			{
				foundLN = 0;
				foundCR = 0;
			}
			if ( foundLN == 2 ) 
			{
				cursor += nBytes;
				dataOffset = cursor;
				break;
			}
		}
		cursor += nBytes;
	} while (nBytes > 0);
	
	if ( foundLN != 2 )  
	{
		delete buffer;
		std::string msg = "connection was closed by peer (wrong HTTP header, empty line (CRLFCRLF) missing)";
		XCEPT_RAISE(es::api::exception::Exception, msg  );
	}	

	ssize_t len = 0;

	char keyword[15];
	strcpy(keyword,"Content-Length");
	char*  offset = (char*) xoap::memSearch(&buffer[start], keyword, cursor, strlen(keyword));
	if ( offset == 0 ) // try also with capital L
	{
		strcpy(keyword,"Content-length");	
		offset = (char*)xoap::memSearch(&buffer[start], keyword, cursor, strlen(keyword) );
	}
	ssize_t toRead;
	// reset cursor when start reading the payload, restart from the buffer[0]
	cursor = 0; 
	available += dataOffset;
	if  (offset != 0) 
	{
		char scanform[80];
		strcpy(scanform,keyword);
		strcat(scanform,": %d");
		if ( sscanf (offset, scanform,&len) > 0 ) 
		{
			toRead = len;

			// Allocate more memory, if needed
			if (toRead > available) 
			{
				char * newBuffer = new char [toRead + cursor + 1];
				memcpy (newBuffer, buffer, cursor);
				delete buffer;
				buffer = newBuffer;
				available = toRead;
			}

			do 
			{
				nBytes = this->receive(&buffer[cursor], toRead);
				available -= nBytes;

				if ( nBytes == 0 ) 
				{
					delete buffer;
					std::string msg = "Connection closed by peer (request not completed)";
					XCEPT_RAISE(es::api::exception::Exception, msg  );
				}
				toRead -= nBytes;
				cursor += nBytes;
			} while (toRead > 0);
			
		}
	}
	
	if ( len == 0 ) 
	{ // no length specified then read till close connection
		len = available;

		do {
			nBytes = this->receive(&buffer[cursor], available);
			cursor += nBytes;
			available -= nBytes;

			if (available == 0) 
			{
				// Allocate more buffer memory if needed
				// Add one mtuSize_ each time.

				ssize_t toAllocate = ReadSegmentSize + cursor + 1;
				char * newBuffer = new char [toAllocate];
				memcpy (newBuffer, buffer, cursor);
				delete buffer;
				buffer = newBuffer;
				available = ReadSegmentSize;
			}
		} while ( nBytes > 0 );
	}

	std::string receivedData;
	receivedData.append(buffer,cursor);
	delete buffer;
	return receivedData;	
}


void es::api::HTTPURLConnection::sendTo( char * path, char * host, unsigned int port, const char * buf, size_t len)
	
{
	//std::string boundary = extractMIMEBoundary(buf, len);

	//if ( boundary == "" )
	//{
		this->writeHttpPostHeader(path, host, port, len, "");
	//} else
	//{
	//	this->writeHttpPostMIMEHeader( path, host, port,  boundary, len, "");
	//}
	// send payload
	this->send(buf,len);
}

void es::api::HTTPURLConnection::sendTo
	(
		char * path, 
		char * host, 
		unsigned int port, 
		const char * buf, 
		size_t len,
		const char* soapAction
	) 
	
{
	std::string boundary = extractMIMEBoundary(buf, len);

	if ( boundary == "" ) 
	{
		this->writeHttpPostHeader(path, host, port, len, soapAction);
	} else 
	{
		this->writeHttpPostMIMEHeader( path, host, port,  boundary, len, soapAction);
	}
	// send payload
	this->send(buf,len);
}


std::string es::api::HTTPURLConnection::extractMIMEBoundary(const char * buf , size_t size)
	
{
	char* boundary = 0;
	if ((buf[0] == '-') && (buf[1] == '-'))
        {
		boundary = (char*)xoap::memSearch(buf, "--", size, 2);
	}
	
	if (boundary != 0)
	{
		// move to start of boundary string
		boundary= &boundary[2]; 
		//
		// Find name of boundary string
		// Boundary string can be ended by "\n" or "\r\n"
		//
		char* boundaryStringEnd = (char*)xoap::memSearch(boundary, "\r\n", size, 2);
		if (boundaryStringEnd == 0)
		{
			boundaryStringEnd = (char*)xoap::memSearch(boundary, "\n",size, 1);
		}
		
		if (boundaryStringEnd == 0)
		{
			std::string msg = "Cannot find MIME boundary in SOAP message";
			XCEPT_RAISE(es::api::exception::Exception,  msg  );
        	}
		
		size_t boundaryStringSize = boundaryStringEnd - boundary;
		std::string boundaryString = std::string(boundary, boundaryStringSize);
		return boundaryString;
	}	
	return "";
}

void es::api::HTTPURLConnection::writeHttpPostMIMEHeader
	(
		char * path, 
		char * host, 
		unsigned int port, 
		std::string & boundaryStr, 
		size_t len,
		const char* soapAction
	) 
	
{
	std::stringstream header;

        // MIME content
	//
        header << "POST ";
        header << path; 
        header << " HTTP/1.1\r\n";
        header << "Host: ";
        header << host;
        header << ":";
	header << port;
        header << "\r\n";
        header << "Connection: keep-alive\r\n";
        header << "Content-type: multipart/related; type=\"text/xml\"; boundary=";
        header << boundaryStr;
        header << "\r\n";
        header << "Content-Length: ";
        header << len;
        header << "\r\nSOAPAction: \"" << soapAction << "\"\r\n";
        header << "Accept: text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2\r\n";
        header << "Content-Description: XDAQ SOAP with attachments.\r\n\r\n";

	this->send((char*)header.str().c_str(),header.str().size());                        
}

void es::api::HTTPURLConnection::writeHttpPostHeader
	(
		char * path, 
		char * host, 
		unsigned int port, 
		size_t len,
		const char* soapAction
	) 
	
{
	//  
	// HTTP without MIME
	//
	std::stringstream header;
	header << "POST ";
	header << path;
	header << " HTTP/1.1\r\nHost: ";
	header << host;
	header << ":";
	header << port;
	//header << "\r\nConnection: keep-alive\r\nContent-type: text/xml; charset=utf-8\r\nContent-length: ";
	header << "\r\nContent-type: text/xml; charset=utf-8\r\nContent-length: ";
	header << len;
	//header << "\r\nSOAPAction: \"" << soapAction << "\"\r\n";
	header << "\r\n\r\n";
	this->send((char*)header.str().c_str(),header.str().size());
}
