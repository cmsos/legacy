###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
ICIS = ["ici-11"]
APVES = ["apve-11"]
PIS = ["pi-11"]

# The FED id of the CPM, so we can disable the readout.
FED_ID_CPM = 1024

# Some misc. FED ids, so we can enable/disable 'random' inputs on the
# PI.
FED_IDS_MISC = [[10, 2]]

###############################################################################
