#ifndef _tcds_ici_Definitions_h_
#define _tcds_ici_Definitions_h_

#include <stdint.h>

namespace tcds {
  namespace definitions {

    // B-channel timing modes: delayed or BX-synchronous.
    enum BCHANNEL_TIMING_MODE {
      BCHANNEL_TIMING_DELAYED=0,
      BCHANNEL_TIMING_BX_SYNC=1
    };

    // The number of cyclic generators in the iCI.
    unsigned int const kICICyclicGenNumMin = 0;
    unsigned int const kICICyclicGenNumMax = 7;
    unsigned int const kNumCyclicGensPerICI = kICICyclicGenNumMax - kICICyclicGenNumMin + 1;

  } // namespace definitions
} // namespace tdcs

#endif // _tcds_ici_Definitions_h_
