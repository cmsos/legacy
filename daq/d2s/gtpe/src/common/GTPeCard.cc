/**
*      @file GTPeCard.cc
*
*       @see ---
*    @author Hannes Sakulin
*
* $Revision: 1.20 $
*     $Date: 2008/05/26 09:43:47 $
*
*
**/
#include "d2s/gtpe/GTPeCard.hh"
#include "tts/ttsbase/TTSState.hh"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>

static const double randm_cycle = 4194303.;
static const double n_all  = 3564;
static const double n_full = 2835; // 2808;
static const double f_LHC = 40.e6;

static const char* key_file = "/dev/xpci";
static const char project_id = 'g';
static const uint32_t n_semaphores = 17; // one global, 8 for DAQ partitions, 8 for Det partitions

static const char* trigger_rule_regnames[] = { "PIX", "TRA", "PRE", "EBX", "HCA", "CSC", "EBX", "EBX" };
static const char* trigger_rule_names[] = { "Pixel(2ebx+1eo/s)", "Tracker(2ebx)", "Preshower(1ebx+14/5.4us)", "ECAL(1ebx)", "HCAL(1ebx+22/o)", "CSC(2ebx)", "RPC(1ebx)", "DT(1ebx)" };



d2s::GTPeCard::GTPeCard( HAL::PCIDevice* gtpe )
  throw (HAL::HardwareAccessException, 
	 ipcutils::exception::Exception,
	 d2s::exception::IncompatibleFirmware) 
  : _gtpe (gtpe), 
    _latch_semaphore(toolbox::BSem::FULL),
    _semaphorearray_sema(toolbox::BSem::FULL),
    _sa(key_file, project_id, true, n_semaphores),
    _haveMasterLock(false) {

  latchCounters();
  std::string required_version("080402_00");

  if (readFirmwareRevisison() < required_version )
    XCEPT_RAISE (d2s::exception::IncompatibleFirmware, std::string("GTPe Firmware revision too old. Required version is ") 
		 + required_version + std::string(" or newer.") );

};

d2s::GTPeCard::~GTPeCard() {
  delete _gtpe;
};


std::string d2s::GTPeCard::readFirmwareRevisison()
   {

  uint32_t year;
  uint32_t month;
  uint32_t day;
  uint32_t rev_in_day;

  _gtpe->read("Version_year", &year);
  _gtpe->read("Version_month", &month);
  _gtpe->read("Version_day", &day);
  _gtpe->read("Version_rev_in_day", &rev_in_day);

  std::string rev =  
    BCDtoString( year ) +
    BCDtoString( month ) +
    BCDtoString( day ) +
    "_" +
    BCDtoString( rev_in_day );

  return rev;
  

}

std::string d2s::GTPeCard::BCDtoString(uint32_t num, uint32_t ndigit) {

  std::stringstream txt;
  for (int32_t i = (int32_t) ndigit-1; i>=0; i--)
    txt << std::setw(1) << std::hex << ( ( num & ( 0xf << (4*i) ) ) >> (4*i) );

  return txt.str();
}


void d2s::GTPeCard::globalReset() 
   {

  _gtpe->write("GlobalReset", 0x1);
  _gtpe->write("GlobalReset", 0x0);
};

void d2s::GTPeCard::resetCounters() 
   {

  _gtpe->write("ResetGTPCounters", 0x1 );
  _gtpe->write("ResetGTPCounters", 0x0 );
};

void d2s::GTPeCard::setClockPeriod(uint32_t n_25ns) 
   {
  
  _gtpe->write("TriggerClockPeriod", n_25ns); 
  
}

uint32_t d2s::GTPeCard::readbackClockPeriod() 
   {
  
  uint32_t n_25ns;

  _gtpe->read("TriggerClockPeriod", &n_25ns); 
  
  return (uint32_t) n_25ns;
}

void d2s::GTPeCard::toggleSLINKRequired(bool requ) 
   {

  _gtpe->write("ignoreSLINK", requ ? 0x0 : 0x1);
};

bool d2s::GTPeCard::isSLINKRequired() 
   {

  uint32_t val;
  _gtpe->read("ignoreSLINK", &val);

  return val == 0x0;
};


void d2s::GTPeCard::startSLINK() 
   {

  _gtpe->write("startSLINK", 0x1);
};

void d2s::GTPeCard::stopSLINK() 
   {

  _gtpe->write("startSLINK", 0x0);
};


void d2s::GTPeCard::toggleClockedMode(bool clocked) 
   {

  _gtpe->write("ClockedMode", clocked ? 0x1 : 0x0);
}


bool d2s::GTPeCard::isClockedMode() 
   {

  uint32_t val;
  _gtpe->read("ClockedMode", &val);

  return val == 0x1;
}


void d2s::GTPeCard::enableBCResetLEMO(bool enable)
   {

  _gtpe->write("BCResetLEMOEnable", enable ? 0x1 : 0x0);

}


void d2s::GTPeCard::enableBunchStructureLEMO(bool enable)
   {

  _gtpe->write("BunchStrucLEMOEnable", enable ? 0x1 : 0x0);

}


bool d2s::GTPeCard::isBCResetLEMOEnabled()
   {

  uint32_t val;
  _gtpe->read("BCResetLEMOEnable", &val);

  return val == 0x1;
}


bool d2s::GTPeCard::isBunchStructureLEMOEnabled()
   {

  uint32_t val;
  _gtpe->read("BunchStrucLEMOEnable", &val);

  return val == 0x1;
}



void d2s::GTPeCard::startTriggers() 
   {

  _gtpe->write("GTPePause", 0x0);
  _gtpe->write("GTPeStart", 0x1);
};

void d2s::GTPeCard::pauseTriggers() 
   {

  _gtpe->write("GTPePause", 0x1);
}

void d2s::GTPeCard::resumeTriggers() 
   {

  _gtpe->write("GTPePause", 0x0);
}


void d2s::GTPeCard::stopTriggers() 
   {

  _gtpe->write("GTPeStart", 0x0);
  _gtpe->write("GTPePause", 0x0);
};

bool d2s::GTPeCard::isRunning() 
   {

  uint32_t val;
  _gtpe->read("GTPeStart", &val);

  return val == 0x1;
}


bool d2s::GTPeCard::isPaused() 
   {

  uint32_t val;
  _gtpe->read("GTPePause", &val);

  return val == 0x1;
}


double d2s::GTPeCard::setPartitionDef(uint32_t part_group_idx, 
				      uint32_t selected_partitions,  
				      double triggerrate) 
   {

  if (part_group_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "partition group index out of range");

  uint32_t part_def_word = (selected_partitions & 0xff) << 16;

  uint32_t rate_bits = (uint32_t) (triggerrate / f_LHC * n_all / n_full * randm_cycle);
  if (rate_bits > 0xffff) rate_bits = 0xffff;

  part_def_word |= rate_bits;

  std::stringstream part_item;
  part_item << "Partition" << part_group_idx << "Def";

  _gtpe->write(part_item.str(), part_def_word);

  // return the actually set frequency
  return rate_bits / randm_cycle * n_full/n_all * f_LHC;
};

double d2s::GTPeCard::setPartitionDef(uint32_t part_group_idx, 
				      uint32_t selected_partitions,  
				      uint32_t rate_bits) 
   {

  if (part_group_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "partition group index out of range");

  uint32_t part_def_word = (selected_partitions & 0xff) << 16;

  if (rate_bits > 0xffff) rate_bits = 0xffff;

  part_def_word |= rate_bits;

  std::stringstream part_item;
  part_item << "Partition" << part_group_idx << "Def";

  _gtpe->write(part_item.str(), part_def_word);

  // return the actually set frequency
  return rate_bits / randm_cycle * n_full/n_all * f_LHC;
};

uint32_t d2s::GTPeCard::readbackPartitionDef(uint32_t part_group_idx)
   {

  if (part_group_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "partition group index out of range");

  uint32_t pdef;
  std::stringstream part_item;
  part_item << "Partition" << part_group_idx << "Def";

  _gtpe->read(part_item.str(), &pdef);
  
  return pdef;
}


uint32_t d2s::GTPeCard::readbackRate(uint32_t part_group_idx)
   {
  
  return readbackPartitionDef(part_group_idx) & 0xffff;
}

double d2s::GTPeCard::readbackRateHz(uint32_t part_group_idx)
   {
  
  uint32_t rate = readbackRate(part_group_idx);

  return (double) rate * f_LHC / n_all * n_full / randm_cycle;
}


uint32_t d2s::GTPeCard::readbackSelectedPartitions(uint32_t part_group_idx)
   {

  return ( readbackPartitionDef(part_group_idx) & (0xff << 16) ) >> 16;
}


double d2s::GTPeCard::readbackCurrentTotalRate() 
   {

  if ( isClockedMode() ) {

    uint32_t n_25ns = readbackClockPeriod();
    return 40000000. / (double) n_25ns;

  } else {

    // sum up rates of active partitions

    double rate = 0.;
    for (uint32_t i=0; i<8; i++) {
      uint32_t pdef = readbackPartitionDef(i);
      double frequ = (double) (pdef&0xffff) * f_LHC / n_all * n_full / randm_cycle;

      // FIXME: put back in when firmware changed      if ( (pdef&0xff0000) != 0)      
        rate += frequ;
    }

    return rate;
  }

}


void d2s::GTPeCard::latchCounters() 
   {
  
  _gtpe->writePulse("latchCounters");

}

uint32_t d2s::GTPeCard::readPartitionStatus() 
   {
  
  uint32_t countval;
  _gtpe->read("PARTSTAT", &countval);

  return countval;
};


uint64_t d2s::GTPeCard::readCounter64bit(std::string item) 
   {

  uint32_t lowval;
  uint32_t highval;
  _gtpe->read(item + "L", &lowval);
  _gtpe->read(item + "U", &highval);


  return (  ( (uint64_t) highval) << 32  ) | ( (uint64_t) lowval );
}

uint64_t d2s::GTPeCard::readGlobalTimer() 
   {
  return readCounter64bit("GlobalTimer");
}

uint64_t d2s::GTPeCard::readCounterAcceptedTriggers() 
   {
  return readCounter64bit("AcceptedTriggers");
}

uint64_t d2s::GTPeCard::readCounterGeneratedTriggers() 
   {
  return readCounter64bit("GeneratedTriggers");
}

uint64_t d2s::GTPeCard::readCounterPartitionAcceptedTriggers(uint32_t part_idx) 
   {

  if (part_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "partition group index out of range");

  std::stringstream regname;
  regname << "GeneratedPart" << part_idx;
  return readCounter64bit( regname.str() );
}

uint64_t d2s::GTPeCard::readLostEventsATTS(uint32_t part_idx) 
   {

  if (part_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "partition group index out of range");

  std::stringstream regname;
  regname << "aTTSLost" << part_idx;
  return readCounter64bit( regname.str() );
}

uint64_t d2s::GTPeCard::readLostEventsSTTS(uint32_t det_idx) 
   {

  if (det_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "detector partition index out of range");

  std::stringstream regname;
  regname << "sTTSLost" << det_idx;
  return readCounter64bit( regname.str() );
}

std::string d2s::GTPeCard::getTriggerRuleName(uint32_t det_idx) 
   {

  if (det_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "detector partition index out of range");

  return std::string(trigger_rule_names[det_idx]);
}

uint64_t d2s::GTPeCard::readLostEventsTriggerRule(uint32_t det_idx) 
   {

  if ( trigger_rule_regnames[det_idx] == NULL ) 
    return 0;

  std::stringstream regname;
  regname << "TRul" << trigger_rule_regnames[det_idx];
  return readCounter64bit( regname.str() );
}


uint64_t d2s::GTPeCard::readLostEventsSLINK() 
   {
  return readCounter64bit("EVMLost");
}

uint64_t d2s::GTPeCard::readLostEventsIntBuf() 
   {
  return readCounter64bit("BUFLost");
}

d2s::GTPeStatus d2s::GTPeCard::readStatusSnapshot() 
   {
  
  _latch_semaphore.take();
  latchCounters();
  
  uint32_t reg_tts_status;
  _gtpe->read("REG_TTS_STATUS", &reg_tts_status);
  d2s::GTPeStatus status( reg_tts_status, readPartitionStatus());

  _latch_semaphore.give();

  return status;
}

void d2s::GTPeCard::forceATTSOutput (uint32_t part_group_idx,
				     bool force,
				     tts::TTSState const& state) 
   {

  if (part_group_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "partition group index out of range");

  if ( force ) {

    uint32_t outstate=0;
    switch (state) {
    case tts::TTSState::IDLE   : outstate=0x3; break;
    case tts::TTSState::WARNING: outstate=0x2; break;
    case tts::TTSState::BUSY   : outstate=0x1; break;
    case tts::TTSState::READY  : outstate=0x0; break;
    default: 
      XCEPT_RAISE(d2s::exception::OutOfRange, "state out of range, valid states for GTPe are IDLE, WARNING, BUSY, READY");
    }

    uint32_t bpword;
    _gtpe->read("TestBackpressure", &bpword);
    bpword &= ~( 0x3 << (2*part_group_idx));
    bpword |= outstate << (2*part_group_idx);
    _gtpe->write("TestBackpressure", bpword);
    
    // switch to simu mode
    uint32_t atts_enable;
    _gtpe->read("aTTS_Backpr", &atts_enable);
    // clear bit
    atts_enable &= ~( 0x1 << part_group_idx);
    _gtpe->write("aTTS_Backpr", atts_enable);

  }
  else {

    // switch to normal mode
    uint32_t atts_enable;
    _gtpe->read("aTTS_Backpr", &atts_enable);
    // set bit
    atts_enable |= 0x1 << part_group_idx;
    _gtpe->write("aTTS_Backpr", atts_enable);

  }    

};
  
tts::TTSState d2s::GTPeCard::readbackATTSTestOutputState(uint32_t part_group_idx)
   {

  if (part_group_idx>7) 
    XCEPT_RAISE( d2s::exception::OutOfRange, "partition group index out of range");

  uint32_t bpword;
  _gtpe->read("TestBackpressure", &bpword);
  uint32_t bp = ( bpword & ( 0x3 << (2*part_group_idx) ) ) >> (2*part_group_idx);

  switch (bp) {
  case 0x0: return tts::TTSState::READY;
  case 0x1: return tts::TTSState::BUSY;
  case 0x2: return tts::TTSState::WARNING;
  case 0x3: return tts::TTSState::IDLE;
  default: return 16;
  }

}

uint32_t d2s::GTPeCard::readbackATTSOutputEnables() 
   {

  uint32_t atts_enable;
  _gtpe->read("aTTS_Backpr", &atts_enable);

  return atts_enable & 0xff;
}

void d2s::GTPeCard::setSTTSInputMask (uint32_t mask)
   {

  _gtpe->write("Mask_sTTS", mask);

}

void d2s::GTPeCard::setATTSInputMask (uint32_t mask)
   {

  _gtpe->write("Mask_aTTS", mask);

}

uint32_t d2s::GTPeCard::readbackSTTSInputMask ()
   {

  uint32_t mask;
  _gtpe->read("Mask_sTTS", &mask);

  return mask;
}

uint32_t d2s::GTPeCard::readbackATTSInputMask ()
   {

  uint32_t mask;
  _gtpe->read("Mask_aTTS", &mask);

  return mask;
}



void d2s::GTPeCard::lock()
  throw (ipcutils::exception::Exception,
	 d2s::exception::Timeout) {

  _semaphorearray_sema.take();                  // first synchronize with other threads
  uint32_t timeout_sec = 2;
  bool success = _sa.takeTimed(0, timeout_sec); // then take the system-wide semaphore

  if (!success) {
      std::stringstream msg;
      msg << "Timeout occured obtaining the master lock for the GTPe. Could not obtain lock after " << std::dec << timeout_sec << " seconds.";
      XCEPT_RAISE(d2s::exception::Timeout, msg.str() );      
  }

  _haveMasterLock = true;
}


void d2s::GTPeCard::unlock()
   {

  if ( _haveMasterLock ) {
    _sa.give(0);                 // relase system wide semaphore
    _semaphorearray_sema.give();
  } 
  _haveMasterLock = false;
}



bool d2s::GTPeCard::lockDAQPartition( uint32_t i_part )
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  return _sa.takeIfUnlocked(1 + i_part);
}



void d2s::GTPeCard::unlockDAQPartition ( uint32_t i_part )
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  _sa.give(1 + i_part);
}



bool d2s::GTPeCard::isDAQPartitionLocked( uint32_t i_part )
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  return _sa.isLocked(1 + i_part);
}



uint32_t d2s::GTPeCard::getDAQPartitionLockOwnerPID( uint32_t i_part)
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  return _sa.getPID(1 + i_part);
}



bool d2s::GTPeCard::lockDetPartition( uint32_t i_det )
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  return _sa.takeIfUnlocked(9 + i_det);
}



void d2s::GTPeCard::unlockDetPartition ( uint32_t i_det )
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  return _sa.give(9 + i_det);
}


bool d2s::GTPeCard::isDetPartitionLocked( uint32_t i_det )
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  return _sa.isLocked(9 + i_det);
}



uint32_t d2s::GTPeCard::getDetPartitionLockOwnerPID( uint32_t i_det)
  throw (ipcutils::exception::Exception,
	 ipcutils::exception::OutOfRange) {

  return _sa.getPID(9 + i_det);
}

void d2s::GTPeCard::setPartitionRunNumber(uint32_t i_part, uint32_t runNumber)
  throw (HAL::HardwareAccessException,
	 d2s::exception::OutOfRange) {

  if (i_part > 7) 
    XCEPT_RAISE(d2s::exception::OutOfRange, "partition index out of range.");

  std::stringstream regName;
  regName << "PART_RUN" << i_part;

  _gtpe->write(regName.str(), runNumber);

}

uint32_t d2s::GTPeCard::readbackPartitionRunNumber(uint32_t i_part)
  throw (HAL::HardwareAccessException,
	 d2s::exception::OutOfRange) {

  if (i_part > 7) 
    XCEPT_RAISE(d2s::exception::OutOfRange, "partition index out of range.");

  std::stringstream regName;
  regName << "runNumberPartition" << i_part;

  uint32_t runNumber;
  _gtpe->read(regName.str(), &runNumber);

  return runNumber;
}


void d2s::GTPeCard::resyncDetPartitions (uint32_t detPartitionMask)
   {

  _gtpe->write("resyncDetParts", detPartitionMask);
  _gtpe->write("resyncDetParts", 0x0);

}
