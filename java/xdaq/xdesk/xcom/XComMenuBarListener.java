package xdaq.xdesk.xcom;



// extends Standard menu bar listener with Desklet specific actions
// in Combination with Desklet specific menubar
public interface XComMenuBarListener extends xdaq.xdesk.tools.MenuBarListener
{
	public void closeAction();
	
}
