#include "sdru.h"

extern "C" int Main(char * host, char * portstr)
{
  	int port;
	sscanf(portstr,"%d",&port);
  	TcpDispatcherAcceptorTaskS * d1 = new TcpDispatcherAcceptorTaskS(host,20300);
	TcpDispatcherAcceptorTaskS * d2 = new TcpDispatcherAcceptorTaskS(host,20301);
	XFU * fu = new XFU(d1->getXIO());
	SDRU * sdru = new SDRU(fu,100);
	//SDEVM * sdevm = new SDEVM(fu,sdru, 100); //local

  	TaskGroup g;
  	d1->initTaskGroup(&g);
  	d1->addListener(sdru);
	//d1->addListener(sdevm); //local
	d2->addListener(sdru);
  
  
  	d1->activate();
	d2->activate();
	//
	//Trigger loop local
	//
	//for(;;) {
	//	sdevm->trigger();
	//}
 	g.join();
	delete d1;
	delete d2;
	delete fu;
	delete sdru;
	return(0);	
}
#ifndef vxworks
int main(int argc, char* argv[])
{

	return Main(argv[1], argv[2]);
}
#endif
