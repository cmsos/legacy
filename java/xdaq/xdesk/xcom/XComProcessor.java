package xdaq.xdesk.xcom;

import java.applet.*;
import java.awt.*;
import java.beans.*;
import java.util.*;
import javax.infobus.*;
import javax.swing.*;
import java.awt.Graphics;
import javax.swing.table.*;

import org.apache.log4j.*;

import xdaq.xdesk.tools.*;

public class XComProcessor implements  InfoBusSubscriberListener, Runnable
{
	Logger logger = Logger.getLogger (XComProcessor.class);

	ObjectFIFO fifo_;
	XCom xcom_;
	// Infobus stuff
	InfoBusPublisher commandPublisher_;
	InfoBusSubscriber replySubscriber_;
	
	
	class Stop {
	}
	
	public synchronized void  update (Object data, Object source)
	{
			try 
			{
				synchronized (fifo_) 
				{
					fifo_.add ( data );
				}
			} catch (java.lang.InterruptedException e)
			{
				logger.warn ("Interrupted fifo: " + e.toString());
			}
  	}
	
	XComProcessor (XCom xcom)
	{
		xcom_ = xcom;
		fifo_ = new ObjectFIFO(10); // 10 elements in fifo allowed.
		
		// --------------------------------------------------
		// InfoBus attachment
		commandPublisher_ = new InfoBusPublisher ("xcontrolbus");
		replySubscriber_ = new InfoBusSubscriber ("xcontrolbus");
		commandPublisher_.publishItem ("xreply");
		replySubscriber_.subscribeItem ("xcommand", this );
		// --------------------------------------------------
		
		Thread t = new Thread (this);
		t.start();
	}
		
	// Take record from queue, send messages and put result onto infobus
	//
	public void run ()
	{
		logger.info("XCom processor activated...");
		while (true) 
		{
			try {
				synchronized (fifo_) 
				{
					Object command = fifo_.remove();
					if (command instanceof MessageSet)
					{
						MessageSet reply = xcom_.xcommand ( (MessageSet)command );
						//debugXCommand ( (DefaultTableModel) reply );
						commandPublisher_.fireItemValueChanged ( "xreply", reply);
					} else if ( command instanceof Stop)
					{
						logger.info("XCom processor exiting...");
						commandPublisher_.shutdown();
						replySubscriber_.shutdown();
						return;
					}
					else
					{
						logger.warn ("Received wrong 'xreply' datatype: " + command.toString());
					}
				}			
			} catch (java.lang.InterruptedException e)
			{
				logger.warn ("interrupted fifo: " + e.toString());
				return;
			}
		}
	}
	
	void shutdown()
	{
		this.update(new Stop(), this);
		
	}	
}
