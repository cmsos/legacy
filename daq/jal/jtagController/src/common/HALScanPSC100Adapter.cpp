// Package include files
#include "jal/jtagController/HALScanPSC100Adapter.h"

#include "hal/HardwareDeviceInterface.hh"
#include "hal/HardwareAccessException.hh"

// System include files
#include <iostream>
#include <stdint.h>

using namespace std;
using namespace HAL;

jal::HALScanPSC100Adapter::HALScanPSC100Adapter(HardwareDeviceInterface& device, const char* pscbase_name, uint8_t address_shift) :
  _device(device) {
  _pscbase_name  = pscbase_name;
  _address_shift = address_shift;
}

jal::HALScanPSC100Adapter::~HALScanPSC100Adapter() {
}

void jal::HALScanPSC100Adapter::writePSC(uint8_t offset, uint8_t data) 
  throw (jal::OutOfRangeException,
	 jal::HardwareException) {

  if (offset > 7) 
    XCEPT_RAISE(jal::OutOfRangeException, "PSC offset out of range" );

  try
   {
     _device.unmaskedWrite(_pscbase_name, (uint32_t) data, HAL_NO_VERIFY, ((uint32_t) offset) << _address_shift);
   }
   catch(HardwareAccessException &e)
   {
     XCEPT_RETHROW(jal::HardwareException, "error during write", e); 
   }
}

void jal::HALScanPSC100Adapter::writeBlockPSC(uint8_t offset, uint32_t num_bytes, const uint8_t *data) 
  throw (jal::OutOfRangeException,
	 jal::HardwareException) {

  if (offset > 7) 
    XCEPT_RAISE(jal::OutOfRangeException, "PSC offset out of range" );


   try
   {
      for (uint32_t loop_index = 0; loop_index < (uint32_t) num_bytes; loop_index++)  {

	uint32_t d = data[loop_index];
	_device.unmaskedWrite(_pscbase_name, d, HAL_NO_VERIFY, ((uint32_t) offset) << _address_shift);
      }

      //      _device.writeBlock(_pscbase_name, num_bytes, (char*) data, HAL_NO_VERIFY, HAL_NO_INCREMENT, ((uint32_t) offset) << _address_shift);

   }
   catch(HardwareAccessException &e)
   {
     XCEPT_RETHROW(jal::HardwareException, "error during block write", e); 
   }
}


uint8_t jal::HALScanPSC100Adapter::readPSC(uint8_t offset)
  throw (jal::OutOfRangeException,
	 jal::HardwareException) {

  if (offset > 7) 
    XCEPT_RAISE(jal::OutOfRangeException, "PSC offset out of range" );

   uint32_t content = 0;

   try   {
     _device.unmaskedRead(_pscbase_name, &content, ((uint32_t) offset) << _address_shift);
   }
   catch(HardwareAccessException &e)
   {
     XCEPT_RETHROW(jal::HardwareException, "error during read", e); 
   }

   return (uint8_t) content;
}

