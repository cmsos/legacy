###############################################################################
## The BaseBoard class handles everything that the AMC13 (in its guise
## as CPM) has in common with the GLIB/FC7. This is not a lot, but it
## provides a uniform interface for firmware programming and
## reloading, etc.
###############################################################################

import uhal

from pytcds.utils.tcds_hwinterfacewrapper import HwInterfaceWrapper
from pytcds.utils.tcds_utils_misc import uint32_to_string

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class BaseBoard(object):

    def __init__(self, carrier_type, board_type, hw_interface, verbose=False):
        self.hw = HwInterfaceWrapper(carrier_type, board_type, hw_interface)
        self.verbose = verbose
        # This one should be filled in by descendant classes.
        self.user_fw_base_name = None
        # End of __init__().

    #----------

    # Expose access to the hardware.
    # NOTE: Some timing-critical things (like programming the GLIB
    # FLASH memory) need this level of control.
    def get_client(self, *args, **kwargs):
        return self.hw.get_client(*args, **kwargs)

    def get_nodes(self, *args, **kwargs):
        return self.hw.get_nodes(*args, **kwargs)

    def get_node(self, *args, **kwargs):
        return self.hw.get_node(*args, **kwargs)

    def get_node_address(self, *args, **kwargs):
        return self.get_node(*args, **kwargs).getAddress()

    def read(self, *args, **kwargs):
        return self.hw.read(*args, **kwargs)

    def read_block(self, *args, **kwargs):
        return self.hw.read_block(*args, **kwargs)

    def write(self, *args, **kwargs):
        return self.hw.write(*args, **kwargs)

    def write_block(self, *args, **kwargs):
        return self.hw.write_block(*args, **kwargs)

    def dispatch(self, *args, **kwargs):
        return self.hw.dispatch(*args, **kwargs)

    #----------

    def get_board_id(self):
        res = uint32_to_string(self.read("system.board_id"))
        # End of get_board_id().
        return res

    def get_system_id(self, which="system"):
        res = uint32_to_string(self.read("{0:s}.system_id".format(which)))
        # End of get_system_id().
        return res

    def get_user_system_id(self):
        res = self.get_system_id("system")
        # End of get_user_system_id().
        return res

    #----------

    def get_firmware_version(self, which="system"):
        raise NotImplementedError("BaseBoard.get_firmware_version() is not implemented.")
        # End of get_firmware_version().
        return None

    def get_firmware_date(self, which="system"):
        raise NotImplementedError("BaseBoard.get_firmware_date() is not implemented.")
        # End of get_firmware_date().
        return None

    #----------

    def is_user_firmware_loaded(self):
        raise NotImplementedError("BaseBoard.is_user_firmware_loaded() is not implemented.")
        # End of is_user_firmware_loaded().
        return None

    #----------

    def choose_clock_source(self, source):
        """Configure clock routing."""
        raise NotImplementedError("BaseBoard.choose_clock_source() is not implemented.")
        # End of choose_clock_source().

    def reset(self):
        """Reset the device to a well-known configuration."""
        raise NotImplementedError("BaseBoard.reset() is not implemented.")
        # End of reset().

    #----------

    # End of class BaseBoard.

###############################################################################
