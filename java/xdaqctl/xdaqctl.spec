%define name		xdaqctl
%define version		0.0.0
%define archivever	0_0_0
%define release		0i
%define section		free
%define xmlprovider	jpackage
%define prefix		/opt

Name:		%{name}
Version:	%{version}
Release:	%{release}
Epoch:		0
Summary:	The XDAQ control library in Java
License:	BSD
URL:		http://home.fnal.gov/~ichiro/slicetest/xctl/
Group:		Development/Libraries/Java
Vendor:		Fermilab
Distribution:	Fermilab
Source0:	%{name}-%{archivever}.tar.gz
BuildArch:	noarch
BuildRequires:	jpackage-utils >= 0:1.5
BuildRequires:	java-devel
BuildRequires:	ant
BuildRequires:	axis >= 0:1.4
Requires:	java >= 0:1.5.0
Requires:	axis >= 0:1.4
Requires:	jakarta-commons-discovery
Requires:	jakarta-commons-logging
Requires:	wsdl4j
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-buildroot
Provides:	xdaqctl
Prefix:		%{prefix}

%description
Java library to control XDAQ applications.
For XDAQ itself, see http://xdaqwiki.cern.ch .

%package javadoc
Summary:	Javadoc for %{name}
Group:		Development/Libraries/Java

%description javadoc
Javadoc for %{name}.

%prep
rm -rf $RPM_BUILD_ROOT
%setup -q -n %{name}

# Remove provided binaries/documents
find . -name "*.jar" -exec rm -f {} \;
find . -name "*.class" -exec rm -f {} \;
rm -rf doc/*

%build

pushd ext
./create_links %{xmlprovider}
popd
ant jar
ant javadoc

%install

### Jar file

install -d -m 755 $RPM_BUILD_ROOT%{prefix}/%{name}-%{version}
install -m 644 %{name}.jar $RPM_BUILD_ROOT%{prefix}/%{name}-%{version}/%{name}-%{version}.jar
install -m 644 README $RPM_BUILD_ROOT%{prefix}/%{name}-%{version}

# symbolic links
# xdaqctl-X.Y.Z/xdaqctl.jar -> xdaqctl-X.Y.Z.jar
ln -s %{name}-%{version}.jar $RPM_BUILD_ROOT%{prefix}/%{name}-%{version}/%{name}.jar
# xdaqctl -> xdaqctl-X.Y.Z
ln -s %{name}-%{version} $RPM_BUILD_ROOT%{prefix}/%{name}
# share/java/xdaqctl.jar -> xdaqctl/xdaqctl.jar
install -d -m 755 $RPM_BUILD_ROOT%{prefix}/share/java
ln -s ../../%{name}/%{name}.jar $RPM_BUILD_ROOT%{prefix}/share/java

### Javadoc

install -d -m 755 $RPM_BUILD_ROOT%{prefix}/%{name}-%{version}
cp -pr doc $RPM_BUILD_ROOT%{prefix}/%{name}-%{version}/javadoc
install -d -m 755 $RPM_BUILD_ROOT%{prefix}/share/javadoc
ln -s ../../%{name}-%{version}/javadoc $RPM_BUILD_ROOT%{prefix}/share/javadoc/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(0644,root,root,0755)
%dir %{prefix}/%{name}-%{version}
%doc %{prefix}/%{name}-%{version}/README
%{prefix}/%{name}-%{version}/*.jar
%{prefix}/%{name}
%{prefix}/share/java/*.jar

%files javadoc
%defattr(0644,root,root,0755)
%dir %{prefix}/%{name}-%{version}
%{prefix}/%{name}-%{version}/javadoc
%{prefix}/share/javadoc/%{name}

%changelog

* Wed Dec 13 2006 Ichiro Suzuki <ichiro@fnal.gov> 3.1.3
- installed under /opt and made relocatable

* Thu Nov 30 2006 Ichiro Suzuki <ichiro@fnal.gov> 3.1.0-0i
- initial version

