// $Id$

/*************************************************************************
 * XDAQ Application Template						                     *
 * Copyright (C) 2000-2009, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest, P. Roberts													 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _xmas_smarthub_Application_h_
#define _xmas_smarthub_Application_h_

#include <string>
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

//#include "xgi/Output.h"
//#include "xgi/exception/Exception.h"

namespace xmas{

namespace smarthub
{
	class Application : public xdaq::Application, public xgi::framework::UIManager
	{
		public:

			XDAQ_INSTANTIATOR();

			Application (xdaq::ApplicationStub* s) ;
			void Default(xgi::Input * in, xgi::Output * out) ;
			void flexDisplay(xgi::Input * in, xgi::Output * out) ;

		
			~Application ();
			
	
		protected:

	};
}
}
#endif
