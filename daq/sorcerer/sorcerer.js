//$Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini                                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/


const addon = require('../alchemy/build/Release/alchemy');
const sorcerer = require('./build/Release/sorcerer');

var http = require("http");
var dns = require('dns');
var url = require('url');
const path = require('path');
const fs = require('fs');
var xml2js = require('xml2js');
var util = require('util');


var express = require('express');
var Listener = require('../interact/listener');
var tools = require('../interact/tools');
var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;



// Create ApplicationContext
//
//const applications = [
//{ context: 'http://cmsos-xdaq-future.cern.ch:1973',
//icon: '/executive/images/executive-icon.png',
//    urn: 'urn:xdaq-application:lid=0',
//       class: 'executive::Application',
//       service: 'executive' }
//			 ];
//


var querystring = require('querystring');
var bodyParser = require('body-parser');


function extractLID(req, res, next) {
	var script_name = 'urn:xdaq-application:lid=' + req.params.lid;
	var path_translated =  '/' +script_name + '/' + req.params.resource;
	req.environment["PATH_TRANSLATED"] = path_translated;
	req.environment["SCRIPT_NAME"] = script_name ;
	req.environment["PATH_INFO"] = req.params.resource ;

	next();
} 
/*
var result = '';
var calls = [];
calls.push(function(callback) {
    // First call
    http.get('http://cmsoshub.cms', function (resource) {
        var body = '';
         resource.setEncoding('utf8');
         resource.on('data', function (data) {
             //console.log('first received', data);
        	 console.log('doing request');
		      body += data; 

         });

		 resource.on('end', function () {
			 console.log('end of request');
		      result = body;
		 });
    });
});

*/
function backtothefuture(path) {

  console.log(path);
	

}
//This should work in node.js and other ES5 compliant implementations.
function isEmptyObject(obj) {
  return !Object.keys(obj).length;
}


function nameToUpperCase(name){
    //return name.toUpperCase();
    return name;
}

function isArray(a) {
    return (!!a) && (a.constructor === Array);
};

function traverseRecursiveSync(node) {
	var fancynode = [];
	var expand = true;
	Object.keys(node).forEach(function(key){
		console.log("going to parse ->" + key + "->" + node[key]['$']['xsi:type'] )

		if ( (node[key]['$']['xsi:type'] ==  'soapenc:Struct') || (node[key]['$']['xsi:type'] ==  'soapenc:Array') )
		{
			if ( key == 'properties' )
			{
				expand = false;
			}

			if ( key != 'stub' )
			{
				console.log(util.inspect(node[key]['$$'], false, null))

				console.log('has property:' + node[key].hasOwnProperty("$$") );
				console.log('is array:' + isArray(fancynode) );

				if (! node[key].hasOwnProperty("$$"))
				{
					fancynode.push({ title: key, type: node[key]['$']['xsi:type'] ,  folder: true, expanded: expand, children: []});

				} 
				else
				{
					fancynode.push({ title: key, type: node[key]['$']['xsi:type'] ,  folder: true, expanded: expand, children: traverseRecursiveSync(node[key]['$$']) });
				}
			}
		}
		else
		{
			fancynode.push({ title: key, type: node[key]['$']['xsi:type'] , value: node[key]['_'] });
		}
	});
	return fancynode;
}



class Sorcerer {
	   constructor(parameters, descriptor) {
		console.log("constructing Sorcerer");
		console.log(descriptor);
		
		this._context = addon.getApplicationContext();

		
	    this._parameters = parameters;
	    this._descriptor = descriptor;
	    this._descriptor.icon = "/sorcerer/images/sorcerer.png";
		//this._listener = new Listener();
		//console.log(this._listener);
	    
	    this._router = express.Router();
	    this._router .get("/yoursecurity",  ensureLoggedIn('/login'), this.groups.bind(this)); 

		//this._router.get('/applications/myapplication/whoami', this.whoami);
		
		//this._router.get('/urn:xdaq-application:service=myservice', this.home);
		//this._router.get('/urn:xdaq-application:lid=104', function(req, res, next) {
	 	//		res.redirect('/urn:xdaq-application:service=hyperdaq');
		//});
		
		
		
                   // A GET to the root of a resource returns a list of that resource
                //this._router.get('/application/:lid/:resource', this.transmute);	
		this._router.get('/urn:xdaq-application:lid=' + descriptor.id + '/',  this.home.bind(this));
		this._router.get('/urn:xdaq-application:service=sorcerer', this.home.bind(this));
		this._router.get('/urn:xdaq-application:service=sorcerer/groups', this.groups.bind(this));
		this._router.get('/urn:xdaq-application:service=sorcerer/applications', this.applications.bind(this));
		this._router.get('/login', this.login.bind(this));
		//this._router.get('/urn:xdaq-application:service=sorcerer/profile',  ensureLoggedIn('/login'), this.profile.bind(this));
		
		
		this._router.get('/urn:xdaq-application:service=sorcerer/propertieseditor', ensureLoggedIn('/login'), this.propertieseditor.bind(this));
		//this._router.get('/urn:xdaq-application:service=sorcerer/propertieseditor',  this.propertieseditor.bind(this));

		this._router.get('/urn:xdaq-application:service=sorcerer/parameters2xml', this.parameters2xml.bind(this));
		this._router.get('/urn:xdaq-application:service=sorcerer/endpoints', this.endpoints.bind(this));
		this._router.get('/urn:xdaq-application:service=sorcerer/modules', this.modules.bind(this));
		
	



		
		
		//this._router.get('/forward/:lid' , this.transmute);
		var listener = new Listener();
                //   listener.processIncoming (req,res);
		var options = {
  			inflate: true,
  			limit: '100kb',
  			type: '*/*'
		};

  		//this._router.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies
  		//this._router.use(bodyParser.raw({type: 'application/x-www-form-urlencoded'})); // support encoded bodies

		this._router.get('/urn:xdaq-application:lid=:id/:resource' ,tools.processEnvironment({}),  listener.processIncoming,  this.transmute.bind(this));
        this._router.get('/urn:xdaq-application:lid=:id' ,  tools.processEnvironment({}),listener.processIncoming, this.transmute.bind(this));
        this._router.post('/urn:xdaq-application:lid=:id/:resource' , tools.processEnvironment({}), listener.processIncoming, this.transmute.bind(this));
        this._router.post('/urn:xdaq-application:lid=:id' , tools.processEnvironment({}), listener.processIncoming, this.transmute.bind(this)); 

        
		//var test = express.Router();
        //this._router.get("/pippo",  ensureLoggedIn('/login'),  function (req, res, next) {
		//	res.redirect(redirect)
		//	next
		//});
        
        
        //this._router.get('/myprotected',  ensureLoggedIn('/login'), this.home.bind(this));
        //this._router.get('/mypropertieseditor', ensureLoggedIn('/login'), this.propertieseditor.bind(this));
		//this._app .use('/', test);
		
		sorcerer.setLayout( backtothefuture);
		
		
	   }

	 
	
	   home(req,res) {
		var descriptors = this._context.operation({ action: 'descriptors'});
		console.log(descriptors);
	   	res.render('home', { user: req.user, applications: descriptors.data, zone: 'default' })
	   }
	   
	   groups(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
			var groups = this._context.operation({ action: 'groups'});
			//console.log(groups);
			res.locals.user = req.user;
		   	res.render('groups', {  user: req.user, groups: groups.data,  applications: descriptors.data , zone: 'default' })
	   }
	   
	   endpoints(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
			var endpoints = this._context.operation({ action: 'endpoints'});
			console.log(endpoints);
		   	res.render('endpoints', { user: req.user, endpoints: endpoints.data,  applications: descriptors.data , zone: 'default' })
	   }
	   
	   modules(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
			var modules = this._context.operation({ action: 'modules'});
			//console.log(modules);
		   	res.render('modules', { user: req.user, modules: modules.data,  applications: descriptors.data , zone: 'default' })
	   }
	   
	   cpupolicies(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
			var cpupolicies = this._context.operation({ action: 'cpupolicies'});
			//console.log(cpupolicies);
		   	res.render('cpupolicies', {  user: req.user, cpupolicies: cpupolicies.data,  applications: descriptors.data , zone: 'default' })
	   }

	   
	   applications(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
		   	res.render('applications', { user: req.user, applications: descriptors.data , zone: 'default' })
	   }
	   
	   login(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
		   	res.render('login', {  user: req.user, message: 'TBD flash  message', applications: descriptors.data , zone: 'default' })
	   }
	   
	   profile(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
			console.log('the user is ' + req.user);
			console.log(util.inspect(req.user, true, null));
		   	res.render('profile', { user: req.user, message: 'TBD flash  message', applications: descriptors.data , zone: 'default' })
	   }
	   
	   
	   
	   
	  display()
	  {
		  
	  }
	

	  
	  propertieseditor(req,res) {
			var descriptors = this._context.operation({ action: 'descriptors'});
			var pxml = this._context.operation({ action: 'parameters', options: req.query.lid});
			console.log(pxml.data);
			var parser = new xml2js.Parser({ explicitArray : false, explicitChildren: true, trim: true,   tagNameProcessors: [nameToUpperCase]});
			
			parser.parseString(pxml.data,  function (err, result) {
				//console.log(util.inspect(result, true, null))
				var fancynode = traverseRecursiveSync(result['Properties']['$$']);
				//console.log(util.inspect(fancynode, false, null));
				var urn = 'urn:xdaq-application:lid=' + req.query.lid;
				console.log('before rendering properties');
			   	res.render('properties', {  user: req.user, source: fancynode, urn:  urn, applications: descriptors.data, zone: 'default' })
			});
	   }
	   
	  parameters2xml(req,res) {		   
		   var pxml = this._context.operation({ action: 'parameters', options: req.query.lid});
		   //console.log(pxml);
		   res.set('Content-Type', 'text/xml');
		   res.send(pxml.data);
	   }
	   
	   
	   transmute(req,res) {
			
			 //console.log("transmute");
			 //console.log(res._headers);
			var server = res._headers.server;
			//console.log(server);
			
			if ( res._headers.server == 'sorcerer/layout')
			{
			 //console.log("with layout");
  				var descriptors = this._context.operation({ action: 'descriptors'});
				res.render('server', {  user: req.user, applications: descriptors.data, zone: 'default', html: res.xgiout.data })
			}
			else
			{
			 //console.log("withoput layout");
			 //console.log(res.xgiout.data); 
				res.send(res.xgiout.data);
			}
			
	   }
	   
/*
	   transmute(req,res) {
		   
		   
		  var query = req.query;
		
		  console.log('+++++++++++++++');
		  var script_name = '/api/application/'+req.params.lid;
		  if ( req.params.resource != undefined )
		  { 
			script_name = script_name + '/'+ req.params.resource;
		  }

		   console.log(req.params.lid);
		   console.log(req.params.resource);
		   console.log(script_name);
		   
		   var descriptors = context.get({ action: 'descriptors'});
		   console.log(descriptors);
		   var url = 'http://cmsos-xdaq-future.cern.ch:1973' + script_name;
		   
		   if (! isEmptyObject(query)) {
			   url= url + '?' + querystring.stringify(query);
			}
		   console.log ('the query was:' + url)
		    http.get(url, function (resource) {
		    	     console.log ('final response from XDAQ process application:');
		    	     console.log (resource.headers);
		    		var server = resource.headers['server'];
		        var body = '';
		         resource.setEncoding('utf8');
		         resource.on('data', function (data) {
		             //console.log('first received', data);
		        	 		//console.log('doing request');
				      body += data; 

		         });

				 resource.on('end', function () {
					 //console.log('end of request');
					if ( server == 'sorcerer/layout')
					{
						res.render('server', { applications: descriptors.data, zone: 'default', html: body })
					}
					else
					{
						res.send(body);
					}
				 });
		    });
		   
			
	   }
*/	   
	   
	   whoami(req,res) {
			console.log("hello world");
    			res.send('I am Sorcerer');
	   }
	
	   
} // end of class

module.exports = class Instantiator {
	constructor() {
	}
	
	create (args, descriptor) {
		return new Sorcerer(args,descriptor)
	}
	
	type() {
		return "Sorcerer";
	}
	
	
}
/*
exports.whoami = function(req, res) {
    res.send('I am MyApplication');
};

router.get('/myapplication/whoami', exports.whoami);

*/
