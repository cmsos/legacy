package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import xdaq.tools.win.*;


public class TclInterpreterAction extends AbstractAction
{
	String script_;
	xdaqWin win_;

	public TclInterpreterAction (String name, String script, xdaqWin win)
	{
		super (name);
		script_ = script;
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		TclInterpreter interp = new TclInterpreter (win_);
		String status = interp.evalScript ( script_ );
		if (! status.equals(""))
		{
			JOptionPane.showMessageDialog (null, status, "Script error, execution stopped", JOptionPane.WARNING_MESSAGE);
		}	
		interp.dispose();
	}
}
