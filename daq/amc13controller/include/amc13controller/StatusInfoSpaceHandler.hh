#ifndef __StatusInfoSpaceHandler
#define __StatusInfoSpaceHandler

#include "xdaq/Application.h"
#include "amc13controller/AMC13BaseInfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"

namespace amc13controller {

    class StatusInfoSpaceHandler : public AMC13BaseInfoSpaceHandler 
    {
    public:
        StatusInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater = NULL);
    };
}

#endif /* __StatusInfoSpaceHandler */
