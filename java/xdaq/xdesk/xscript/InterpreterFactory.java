package xdaq.xdesk.xscript;

import java.awt.datatransfer.*;

public class  InterpreterFactory {

	Interpreter createInterpreter(DataFlavor flavor)
	{
	
		if (flavor.getSubType().equals("tcl"))
		{
			return new TclInterpreter();
		}
		else 
		{
		
			return null;
		}
	
	
	}

}


