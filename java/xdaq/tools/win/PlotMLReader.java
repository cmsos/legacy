package xdaq.tools.win;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.text.html.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

import java.util.*;
import jas.util.xml.XMLNodeTraverser.*;
import jas.hist.*;
import jas.plot.PrintHelper;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.w3c.dom.*;

public class PlotMLReader implements ActionListener
{
	private JASHist histo_;
	private JFrame frame_;
	private JButton saveButton_;
	private JButton printButton_;
	String title_;
	
	public PlotMLReader ( org.w3c.dom.Document doc, String title )
	{
		System.out.println ("Plot title: " + title);
		title_ = title;
		frame_ = null;
		if (doc != null) setPlot ( doc );
	}
	
	public void refresh ( org.w3c.dom.Document doc )
	{
		try {
			XMLHistBuilder xmh = new XMLHistBuilder ( doc );
			xmh.modifyPlot (histo_);
		} catch (BadXMLException e) {
			System.out.println (e.toString());
		}
	}
	
	/*
	public PlotMLReader ( String url )
	{
		org.w3c.dom.Document doc = load (url);
		if (doc != null) 
		{
			setPlot ( doc );
			show();
		}
	}
	*/
	
	public void setPlot ( org.w3c.dom.Document doc )
	{
		try {
			histo_ = new JASHist();	
			histo_.setAllowUserInteraction (true);			
			XMLHistBuilder xmh = new XMLHistBuilder ( doc );
			xmh.modifyPlot (histo_);
		} catch (BadXMLException e) {
			System.out.println (e.toString());
		}	
	}
	
	//! If frame is not visible, show it
	public void show()
	{
		if (frame_ == null)
		{
			frame_ = new JFrame(title_);
			JPanel panel = new JPanel ( new BorderLayout (0,0) );
			panel.add (histo_);
			frame_.getContentPane().add (panel);
			
			JPanel buttonPanel = new JPanel();
			
			saveButton_ = new JButton ("Save as");
			saveButton_.addActionListener ( this );
			buttonPanel.add (saveButton_);
			
			printButton_ = new JButton ("Print");
			printButton_.addActionListener ( this );
			buttonPanel.add (printButton_);
			
			frame_.getContentPane().add (buttonPanel, BorderLayout.SOUTH);
			
			frame_.show();
		} else {
			frame_.show();
		}
	}
	
	private org.w3c.dom.Document load (String url)
	{
		org.w3c.dom.Document doc = null;
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		try {
			doc = fac.newDocumentBuilder().parse (url);
		} catch (SAXParseException spe) {
			System.out.println ( "Parse error line " + spe.getLineNumber());
			return null;
		} catch (SAXException sxe) {
			Exception x = sxe;
			System.out.println ("Init error " + x.toString() );
			return null;
		} catch (ParserConfigurationException pce) {
			System.out.println ( pce.toString() );
			return null;
		} catch (IOException ioe) {
			System.out.println ("Read error " + ioe.toString() );
		}
		return doc;
	}
	
	//! Called when Save button is pushed
	public void actionPerformed (ActionEvent e)
	{
		if (e.getSource() == saveButton_)
		{
			try {
			histo_.saveAs();
			} catch (java.io.IOException ioe)
			{
				JOptionPane.showMessageDialog ( null, ioe.toString(), "Save error", JOptionPane.ERROR_MESSAGE );
			}
		} else if (e.getSource() == printButton_) 
		{
			try {
				PrintHelper ph = PrintHelper.instance();
				ph.printTarget ( histo_ );
			} catch (Exception x) {
				JOptionPane.showMessageDialog ( null, x.toString(), "Print error", JOptionPane.ERROR_MESSAGE );
			}
		}
	}
}
