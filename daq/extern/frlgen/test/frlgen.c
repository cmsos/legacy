/* 
 * frlgen.c
 *
 *
 * simple test program for FRLs generating events (with firmware-1)
 * run this on a PC hosting an FRL crate
 * - FRL firmware-1 has to be loaded beforehand by another utility
 * - start the FRL before the FBI
 * - participating FRLs are specified with the mask cmd-line argument
 *    bit=0 corresponds to geoslot=1, etc
 * - execution is controlled by commands read from command fifo (named pipe)
 * - status info is updated in a a file
 *   $HOME/status/frlgen.<hostname>
 *
 * $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/test/frlgen.c,v 1.15 2006/12/22 07:31:14 frans Exp $ 
 *
 * to do:
 * - 
 *
 */

/*
 * status file is eg /users/<username>/status/frlgen.frlpc0.tdrdemo
 *
 * command fifo is /tmp/frlgencmd
 *
 * commands are
 *   go                              ! trigger unlimited
 *   go <n>                          ! generate n triggers
 *   pause
 *   sizes <mean> <sigma>            ! all devices
 *   size  <idev> <mean> <sigma>     ! per device
 *   
 */


/*************************************************************************
 *
 * system headers 
 *
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdarg.h>
#include <sys/resource.h>
#include <sched.h>
#include <netinet/in.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>

#include <math.h>  // for sqrt

/*************************************************************************
 *
 * flags and parameters 
 *
 *************************************************************************/

//#define DEBUG

/* time between intervals for statistics */
#define INT_TIME 1

#define PATH_CMD_FIFO "/tmp/frlgencmd"

/*************************************************************************
 *
 * local headers 
 *
 *************************************************************************/

#include "frl_hw.h"
#include "frl_api.h"
#include "zfrl.h"

#include "rngs.h"
#include "rvgs.h"


/*************************************************************************
 *
 * definitions  
 *
 *************************************************************************/

#define SIGINT 2
#define SIGTERM  15

#define MAX_UNITS 16
#define MAX_DEVICES 16


// 32 bytes for myrinet header (myrfb_pkt_head_t in mcp/pkt.h) and 8 bytes for tail with crc's
#define MYRFB_PACKET_RESERVED (32+8)
// size of frl-header (frlh_t)  
#define FRL_HEADER_WORDS32 (6)

//#define FBI_BLOCK_SIZE 2048
#define FBI_BLOCK_SIZE 4096

#define FRL_PAYLOAD_SIZE (FBI_BLOCK_SIZE-4*FRL_HEADER_WORDS32-MYRFB_PACKET_RESERVED)

#define FED_HEADER_W64 1
#define FED_TRAILER_W64 1

/* structure to store statistics for time slice */
typedef struct stats_bin_struct {
  int nmb ;
  int min_size ;
  int max_size ;
  long long size_sum ;
  long long size2_sum ;
} stats_bin_t ;


/*************************************************************************
 *
 * global vars  
 *
 *************************************************************************/

/* default values for command line args */

int arg_verb = 0;  // verbose
int arg_quit = 0;

int arg_mask = 0 ;  // mask in terms of devices participating (ie FBI mask)

int arg_maxlen = 0 ; // maximum fragment len in bytes (0 = no max)

int arg_resync_evtno = 0 ;  // if different from zero, the evtno to resync on

//int arg_blocksize = (4*1024) ;

//int arg_new_triggerfifo = 0 ;

/* other global vars */

static FILE *fp_cmdfifo = NULL ;  // file pointer for command fifo
static FILE *fp_status = NULL ;   // file pointer for status file

static int quit_flag = 0 ;

/* array to store hostname */
#define LEN_HOSTNAME 64
char hostname[LEN_HOSTNAME] ;

//char userprefix[512] ;

struct zfrl_board_info devs_binfo [MAX_DEVICES] ;

// number of triggers allowed to generate ,-1 means unlimited
int trig_quota = 0 ;  

static int trigger_trigno = 0 ;  /* current trigger number generated */

/* statistics */

static int gen_nmb_trigs = 0 ;

// time slice statistics
stats_bin_t tslice_stats_blocks_devs [MAX_DEVICES] ;

// generator configs

int gen_size_mean [MAX_DEVICES] ; 
int gen_size_sigma [MAX_DEVICES] ; 
double gen_size_lognorm_a [MAX_DEVICES] ;
double gen_size_lognorm_b [MAX_DEVICES] ;

/* setup configs */

static int setup_unitno [MAX_DEVICES] ;

/* some forward function declarations */

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

// WARNING 
// hard coded configuration information
//

int fill_setup ()
{
  printf("fill_setup() for %s \n",hostname) ;
  fflush(stdout) ;


  //
  // lab40
  //  if (strcmp(hostname,"pcepcmd24.cern.ch") == 0 || strcmp(hostname,"pcepcmd24") { 
  //  if (strcmp(hostname,"PCEPCMD22") == 0) {
  //
  //  if (strcmp(hostname,"frlpc0.tdrdemo")==0) {
  // Cessy GB 
  //  if (strcmp(hostname,"frlpc0.cmsdaqpreseries")==0 ||
  // Cessy USC
  // if (strcasecmp(hostname,"frlpcs1d06-02.cms")==0 ||

  return (0) ;
}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
handler_sig (int s)
{
  //  int dt ;

  printf("trapped signal %d ..\n",s) ;
  fflush(stdout) ;

  switch (s) {

    //  case SIGQUIT:
  case SIGINT:
    printf("attempting orderly shutdown ..\n") ;
    fflush(stdout) ;
    quit_flag = 1 ;
    break ;
  default:
    printf("exiting .. \n") ;
    fflush(stdout) ;
    exit(1);
  }
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/


static
void subtractTime(struct timeval *t, struct timeval *sub)
{
  signed long sec, usec;

  sec = t->tv_sec - sub->tv_sec;
  usec = t->tv_usec - sub->tv_usec;
  if (usec < 0) {
    sec--;
    usec += 1000000;
  }
  if (sec < 0) {
    t->tv_sec = 0;
    t->tv_usec = 0;
  }
  else {
    t->tv_sec = (unsigned long) sec;
    t->tv_usec = (unsigned long) usec;
  }
}

//
// TODO: change to something which actually sleeps instead of busy wait
//   

static void udelay(int n_us)
{
  struct timeval time_start;
  struct timeval time_stop;
  int nbsecs,nbusecs;
  
  nbsecs = 0;
  nbusecs = 0;
  gettimeofday (&time_start, NULL);
  
  while ( (nbsecs==0) && (nbusecs<n_us)) {
    gettimeofday (&time_stop, NULL);
    subtractTime (&time_stop, &time_start);
    nbsecs = time_stop.tv_sec;
    nbusecs = time_stop.tv_usec;
  }
  
  //  printf(" start %d %d stop %d %d \n",time_start.tv_sec,time_start.tv_usec,time_stop.tv_sec,time_stop.tv_usec) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
die (char *fmt,...)
{
  va_list ap;
  va_start(ap, fmt);
  fprintf(stderr, "options:\n");
  fprintf(stderr, "-v \n");
  fprintf(stderr, "-quit \n");
  fprintf(stderr, "-mask <mask in hex> \n");
  fprintf(stderr, "-maxlen <maxlen (bytes) in dec> \n");
  fprintf(stderr, "-resync <evtno in dec> \n");
  //fprintf(stderr, "last words:\n");
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  va_end(ap);
  exit(1);
}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
command_line (int argc, char **argv)
{
  int i;

  for (i = 1 ; i < argc ; i++) {

    if (!strcmp(argv[i], "-v")) {
      arg_verb = 1 ;
    }
    else if (!strcmp(argv[i], "-quit")) {
      arg_quit = 1 ;
    }
    //    else if (!strcmp(argv[i], "-newtf")) {
    //      arg_new_triggerfifo = 1 ;
    //    }
    else if (!strcmp(argv[i], "-mask")) {
      if (++i == argc)
	die("no value");
      if (1 != sscanf(argv[i], "%x", &arg_mask))
	die("couldn't convert %s into (hex)");
    }
    else if (!strcmp(argv[i], "-maxlen")) {
      if (++i == argc)
	die("no value");
      if (1 != sscanf(argv[i], "%d", &arg_maxlen))
	die("couldn't convert %s into (dec)");
    }
    else if (!strcmp(argv[i], "-resync")) {
      if (++i == argc)
	die("no value");
      if (1 != sscanf(argv[i], "%d", &arg_resync_evtno))
	die("couldn't convert %s into (dec)");
    }

    else
      die("couldn't figure out what %s means", argv[i]);
  }
}


/*************************************************************************
 *
 *  generate fraglen according to device
 *  returns legnth in bytes
 *
 *************************************************************************/

inline int get_fraglen(int idev)
{
  int l ; 
  double a,b,x ;

  if (gen_size_sigma [idev] == 0) {  // fixed size
    l = gen_size_mean[idev] ;
  }
  else {  // log-normal
    a = gen_size_lognorm_a[idev] ;
    b = gen_size_lognorm_b[idev] ;
    x = Lognormal (a, b) ;
    l = x ;
    //    printf("a=%f b=%f x=%f l=%d \n",a,b,x,l) ;
  }

  // obey minimum size
  if (l < 16) l = 16 ;
  // truncate to make it multiple of 64bits ie 8 bytes
  l &= ~0x7 ;

  //  l &= FRL_GEN_FRAGLEN_MASK  ;
  // cutoff for now
  //  if (l > 4000) l = 4000 ;
  // trigger-word has 16 bits for length in 64bit words
  //  if (l > 0x7FFFF) l = 0x7FFFF ;

  // obey maximum size
  if (arg_maxlen != 0 && l > arg_maxlen) l = arg_maxlen ;
  if (l > 0x7FFFF) l = 0x7FFFF ; 

  return (l) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/


int init_frl () 
{
  int num_units;
  int rt ;
  struct zfrl_board_info *p_bi ;
  //  int iunit ;
  int idev ;
  int unitno ;
  int geoslot ;
  uint32_t fpga_version_main ;

  /*
   * open the FRL devices, 
   * there is a single call for any number of NICs 
   */
  rt = frl_zfrl_open (&num_units);
  printf("frl_zfrl_open() rt=%d=x%08X, num_units=%d \n",rt,rt,num_units);
  if(num_units <= 0) {
    printf("frl_zfrl_open() rt=%d=x%08X, num_units=%d \n",rt,rt,num_units);
    printf("no FRL board found \n");
    fflush(stdout) ;
    goto error ;
  }


  // initialise unitno in table with invalid value
  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {
    setup_unitno[idev] = -1 ;
  }

  // loop over all unitno's to find their geoslot's
  for (unitno=0 ; unitno < MAX_UNITS ; unitno ++) {
    
    /* get board info and set pointers for memory mapped access */
    
    p_bi = &devs_binfo[unitno] ;
    rt = zfrl_get_board_info (unitno,p_bi);
    if (rt)
      break ;
    
    printf("unit %d zfrl_get_board_info() rt=%d iobase pci x%08X user x%08X geoslot=%d  fpga_ bridge=x%08X fpga_main=x%08X \n"
	   ,unitno,rt,p_bi->iobase_main_pci,p_bi->iobase_main_user,p_bi->frl_geoslot,p_bi->fpga_version_bridge,p_bi->fpga_version_main);
    geoslot = p_bi->frl_geoslot ;
    
    // geoslot runs 1-16, inclusive
    if (geoslot < 1 || geoslot > 16) 
      goto error ;
    idev = geoslot - 1 ;
    setup_unitno[idev] = unitno ;
    
    //  p_frlc = (frl_control_t * ) p_bi->iobase_user ;
  }
  
  if (arg_mask == 0) {
     printf("fatal mask=x%X empty \n",arg_mask) ;
      goto error ;
    }

  // check devices selected in the mask are present
  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {
    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask
    unitno =setup_unitno[idev] ; 
    if (unitno == -1) {
      printf("fatal mask=x%X idev=%d not present \n",arg_mask,idev) ;
      goto error ;
    }

    // check correct fpga-main
    p_bi = &devs_binfo[unitno] ;
    fpga_version_main = p_bi->fpga_version_main ;  
    if ( (fpga_version_main & 0xFFF00000) != 0xF1200000) {
      printf("fatal idev=%d fpga_version_main=x%08X \n",idev,fpga_version_main) ;
      goto error ;
    }

  }
  
  fflush(stdout) ;  
  
  return 0 ;
 error:
  fflush(stdout) ;  
  return -1;

}


/*************************************************************************
 *
 * this routine is woken up at regular intervals 
 * - it calculates statistics
 * - writes status to filr
 *
 *************************************************************************/


static struct timeval time_alrm;

struct timeval time_start;
struct timeval time_stop;


void 
handler_sigalrm()
{
  static int nmb_sig ;
  struct timeval time_alrm_last ;
  struct timeval time_alrm_temp ;
  int dt ;
  int dtus ;
  static int gen_nmb_trigs_last = 0 ;
  int nmb_trigs ;
  int idev ;
  int unitno ;
  frl_control_t *p_frlc ;
  struct zfrl_board_info *p_bi ;

  nmb_sig++ ;

  // rewind status file
  rewind (fp_status) ;  

  time_alrm_last = time_alrm ;
  
  gettimeofday (&time_alrm, NULL);

  time_alrm_temp = time_alrm ;
  
  subtractTime (&time_alrm_temp, &time_alrm_last);
  dt = time_alrm_temp.tv_sec * 1000000 + time_alrm_temp.tv_usec;
  dtus = time_alrm_temp.tv_sec * 1000000 + time_alrm_temp.tv_usec;

  {
    double avgtime ;
    double rate ;

    nmb_trigs = gen_nmb_trigs - gen_nmb_trigs_last ; 

    if (nmb_trigs > 0) {
      avgtime = (double) dt / (double) nmb_trigs ;
      rate = 1000000. / avgtime ;
    }
    else {
      avgtime = 0. ;
      rate = 0. ;
    }

    fprintf(fp_status,"dt %7d us generated: total trigs %d  last-evtno %6d=x%06X  avg-time  %6.3f us rate %8.0f Hz \n"
	    ,dt, gen_nmb_trigs, trigger_trigno,trigger_trigno,avgtime,rate) ;
    
    gen_nmb_trigs_last = gen_nmb_trigs ;
  }

  // header lines for all devices
  
  //                 1234 1234 
  fprintf(fp_status,"idev unit") ;
  //                  12345678901 12345678901 12345678 12345678 12345678
  fprintf(fp_status," | total ............... | current ................") ;
  //                  12345678 12345678 12345678 12345678 12345678 1234567890 1234567890
  fprintf(fp_status," | time slice ...........................................") ;
  fprintf(fp_status,"\n") ;

  //                 1234 1234 
  fprintf(fp_status,"         ") ; 
  //                  12345678901 12345678901 12345678 12345678 12345678
  fprintf(fp_status,"      blocks      events    evtno avail-bk trig-pnd") ;
  //                  12345678 12345678 12345678 12345678 12345678 1234567890
  fprintf(fp_status,"   blocks size-min size-max size-avg size-rms thru(MB/s)") ;
  fprintf(fp_status,"\n") ;


  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {

    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask
    
    unitno = setup_unitno[idev] ;
    /* get board info and set pointers for memory mapped access */
    
    p_bi = &devs_binfo[unitno] ;
    p_frlc = (frl_control_t * ) p_bi->iobase_main_user ;
    
    fprintf(fp_status,"%4d %4d",idev,unitno) ;
    
    {
      int nmb_segments ;
      int nmb_triggers ;
      int nmb_triggers_pending ;
      int nmb_free_blocks ;
      int current_trigno ;
  
      nmb_segments = getPCI32noswap(&p_frlc->nmb_segments) ;
      nmb_triggers = getPCI32noswap(&p_frlc->nmb_triggers) ;
      current_trigno = getPCI32noswap(&p_frlc->current_trigno) ;
      nmb_triggers_pending = getPCI32noswap(&p_frlc->nmb_triggers_pending) ;
      nmb_free_blocks = getPCI32noswap(&p_frlc->nmb_free_blocks) ;
      
      //  fprintf(fp,"idev %d FRL status: cnt: segments %10d triggers %10d current_trigno %8d nmb_triggers_pending %3d nmb_blocks_pending %3d\n",
      //	 idev,nmb_segments, nmb_triggers, current_trigno, nmb_triggers_pending, nmb_blocks_pending) ;
      fprintf(fp_status," %11d %11d %8d %8d %8d",nmb_segments, nmb_triggers, current_trigno, nmb_free_blocks,  nmb_triggers_pending) ;
    }

    {
      stats_bin_t *psb = &tslice_stats_blocks_devs[idev] ; 
      int nmb = psb->nmb ;
      int avg, rms ;
      double recv_msg_time ;
      double recv_msg_thru ;
      
      if (nmb == 0) {
	avg = 0 ; 
	rms = 0 ;
	recv_msg_time = 0 ; 
	recv_msg_thru = 0 ; 
      }
      else {
	avg = psb->size_sum / nmb ;
	rms = sqrt( (psb->size2_sum/nmb) - (avg*avg)) ;
	recv_msg_time = (double) dtus / (double) nmb ; 
	recv_msg_thru = (double) psb->size_sum  / (double) dtus ; 
      }      
      
      //      fprintf(fp_status,"slice blocks nmb %10d sizes min %d max %d avg %d rms %d time %6.3f us thru %6.2f MB/s \n",
      //	      psb->nmb, psb->min_size, psb->max_size, avg, rms, recv_msg_time, recv_msg_thru) ;

      fprintf(fp_status," %8d %8d %8d %8d %8d %10.2f"
	      ,psb->nmb, psb->min_size, psb->max_size, avg, rms, recv_msg_thru) ;
      
      memset((void*)psb,0,sizeof(*psb)) ;   
    }

    fprintf(fp_status,"\n") ;
    
  } // devices
  
  {
    // truncate status file to get rid off potential old stuff
    int offset ;
    int fd = fileno(fp_status) ;
    offset = ftell(fp_status) ;
    ftruncate (fd, offset) ;
    fflush(fp_status) ;
  }
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void print_frl_status (FILE *fp, int idev, frl_control_t *p_frlc)
{
  int nmb_segments ;
  int nmb_triggers ;
  int current_trigno ;
  int nmb_triggers_pending ;
  int nmb_blocks_pending ;
  
  nmb_segments = getPCI32noswap(&p_frlc->nmb_segments) ;
  nmb_triggers = getPCI32noswap(&p_frlc->nmb_triggers) ;
  current_trigno = getPCI32noswap(&p_frlc->current_trigno) ;
  nmb_triggers_pending = getPCI32noswap(&p_frlc->nmb_triggers_pending) ;
  nmb_blocks_pending = getPCI32noswap(&p_frlc->nmb_free_blocks) ;
  
  fprintf(fp,"idev %d FRL status: cnt: segments %10d triggers %10d current_trigno %8d nmb_triggers_pending %3d nmb_blocks_pending %3d\n",
	 idev,nmb_segments, nmb_triggers, current_trigno, nmb_triggers_pending, nmb_blocks_pending) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void generate_next_trigger (int idev, frl_control_t *p_frlc, int trigger_trigno)
{
  int trigger_source, trigger_fraglen, trigger_fraglen_w64;

  unsigned long trigger_seed = 0xFA ;
  unsigned long trigger_entry_1, trigger_entry_2 ;


  trigger_source = idev ;

  trigger_source &= FRL_GEN_SOURCE_MASK ;
  trigger_fraglen = get_fraglen (idev) ;  
  trigger_fraglen_w64 = trigger_fraglen >> 3 ;  /* byes to 8-byte words */


  trigger_entry_1 = (trigger_source<<24) | trigger_fraglen_w64 ;
  trigger_entry_2 = (trigger_seed<<24) | (trigger_trigno) ;
  
  setPCI32noswap(&p_frlc->fifo_trigger_entry,trigger_entry_1) ;
  setPCI32noswap(&p_frlc->fifo_trigger_entry,trigger_entry_2) ;
  
#ifdef DEBUG
  printf("idev %d about to push trigger x%08X x%08X  \n",idev
	 ,trigger_entry_1 ,trigger_entry_2) ;
#endif
  
  { 
    // bump statistics for time slice

    stats_bin_t *psb = &tslice_stats_blocks_devs [idev] ;
    int len = trigger_fraglen ;

    if (psb->nmb == 0) psb->min_size = len ;
    if (len < psb->min_size) psb->min_size = len ;
    if (len > psb->max_size) psb->max_size = len ;
    psb->nmb ++ ;
    psb->size_sum += len ;
    psb->size2_sum += len*len ;
  }	

}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

#define MAX_NMB_RESYNC_IN_ROW (1)

static int nmb_resyncs_in_row = 0 ;


void generate_next_trigger_devs_in_mask ()
{
  int idev ;
  int unitno ;
  struct zfrl_board_info *p_bi ;
  frl_control_t *p_frlc ;

  trigger_trigno ++ ;

  // optionally resync at certain evtno
  if (arg_resync_evtno && trigger_trigno == arg_resync_evtno) {
    trigger_trigno = 1 ;
    nmb_resyncs_in_row ++ ;
  }
  else {
    if (nmb_resyncs_in_row > 0 &&
	nmb_resyncs_in_row < MAX_NMB_RESYNC_IN_ROW &&
	trigger_trigno == 2 
	) {
      trigger_trigno = 1 ;
      nmb_resyncs_in_row ++ ;
      if (nmb_resyncs_in_row == MAX_NMB_RESYNC_IN_ROW)
	nmb_resyncs_in_row = 0;	
    }
  }

  trigger_trigno &= 0x00FFFFFF ; // wraps at 24 bits
  
  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {

    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask
    
    unitno = setup_unitno[idev] ;
    /* get board info and set pointers for memory mapped access */
      
    p_bi = &devs_binfo[unitno] ;
    p_frlc = (frl_control_t * ) p_bi->iobase_main_user ;

    generate_next_trigger (idev, p_frlc,trigger_trigno) ;
  }

  gen_nmb_trigs ++ ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void frl_configure (frl_control_t *p_frlc)
{
  int data ;
  
  /* configure FRL */
  
  data = FRL_CONTROL_SOFTRESET ;
  setPCI32noswap(&p_frlc->control,data) ;
  /* looks like after softreset ha sto settle a bit 
   * so do read of status register */
  data = getPCI32noswap(&p_frlc->control) ;

  //  printf("FRL control x%08x \n",data) ;

  data = FRL_CONTROL_GENERATOR ;
  setPCI32noswap(&p_frlc->control,data) ;
 
  setPCI32noswap(&p_frlc->block_size_bytes,FRL_PAYLOAD_SIZE) ;
  setPCI32noswap(&p_frlc->frl_header_size_32,FRL_HEADER_WORDS32) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void frl_configure_all_devs ()
{
  int idev ;
  int unitno ;
  struct zfrl_board_info *p_bi ;
  frl_control_t *p_frlc ;

  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {

    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask

    unitno = setup_unitno[idev] ;
    /* get board info and set pointers for memory mapped access */
    
    p_bi = &devs_binfo[unitno] ;
    p_frlc = (frl_control_t * ) p_bi->iobase_main_user ;

    frl_configure (p_frlc) ;
  }
}



/*************************************************************************
 *
 * 
 *
 *************************************************************************/

#ifdef NOTUSED

void frl_clear_fifo_free_block (frl_control_t *p_frlc)
{
  unsigned int data ;

  printf("frl_clear_fifo_free_block() .. \n") ;
  fflush(stdout) ;

  data = FRL_CONTROL_CLEAR_FIFO_FREE_BLOCK ;

  // TO CHECK : STILL IN FIRST WORD ??
  setPCI32noswap(&p_frlc->control,data) ;
  /* maybe has to settle a bit 
   * so do read of status register */
  data = getPCI32noswap(&p_frlc->control) ;

  printf("FRL control x%08x \n",data) ;
  fflush(stdout) ;
}

#endif

/*************************************************************************
 *
 * 
 *
 *************************************************************************/


//
// calculate the 2 log-normal parameters a and b from the
//   wanted mean and sigma 
//

void set_lognormal (int mean, int sigma, double *pa, double *pb) 
{
  double m = mean ;
  double s = sigma ;
  double m2 = m * m ;
  double s2 = s * s ;
  double sm2 = s2 + m2 ;
  double a, b ;

  a = log ( m2 / sqrt (sm2) ) ;
  b = sqrt (log(sm2/m2)) ;

  *pa = a ;
  *pb = b ;

  //  printf("set_lognormal() idev %d  m=%f s=%f a=%f b=%f \n",idev,m,s,a,b) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/


void cmd_go(int ntrig) 
{
  int trig_quota_before ;

  //  printf("cmd_go() ntrig %d \n",ntrig) ;
  //  fflush(stdout) ;

  trig_quota_before = trig_quota ;

  if (ntrig == -1) 
    trig_quota = -1 ;
  else
    trig_quota = ntrig ;

  printf("cmd_go() changed trg_quota from %d to %d \n",trig_quota_before,trig_quota) ;
  fflush(stdout) ;    
}


void cmd_pause() 
{
  int trig_quota_before ;

  //  printf("cmd_pause()\n") ;

  trig_quota_before = trig_quota ; 
  trig_quota = 0 ;

  printf("cmd_pause() changed trg_quota from %d to %d \n",trig_quota_before,trig_quota) ;
  fflush(stdout) ;    
}


void cmd_size_dev (int idev, int mean, int sigma)
{
  int mean_before, sigma_before ;
  double a, b ;

  mean_before = gen_size_mean[idev] ;
  sigma_before = gen_size_sigma[idev] ;

  gen_size_mean[idev] = mean ;
  gen_size_sigma[idev] = sigma ;

  set_lognormal (mean, sigma, &a, &b) ;

  gen_size_lognorm_a [idev] = a ;
  gen_size_lognorm_b [idev] = b ;

  printf("cmd_size(s)() idev %d changed mean %d to %d, sigma %d to %d \n"
	 , idev, mean_before, mean, sigma_before, sigma) ;
  fflush(stdout) ;
}


void cmd_size (int idev, int mean, int sigma)
{
  if (idev < 0)
    return ;

  if ((arg_mask & (1<<idev)) == 0) 
    return ;   // ignore if not in the mask
  
  cmd_size_dev (idev, mean, sigma) ;
}
 

void cmd_sizes (int mean, int sigma)
{
  int idev ;
 
  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {

    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask

    cmd_size_dev (idev, mean, sigma) ;
  }
}


void handle_cmd (char *line) 
{
  int ntrig ;
  int size_mean, size_sigma ;
  int idev ;

  // note: nmatched counts successful conversions - matching literals are not included

  if ((2-1) == sscanf (line,"go %d",&ntrig) ) {
    cmd_go (ntrig) ;
  }
  else if (strcmp(line,"go\n")==0) {
    cmd_go (-1) ;
  }
  else if (strcmp(line,"pause\n")==0) {
    cmd_pause () ;
  }
  else if ((3-1) == sscanf (line,"sizes %d %d",&size_mean,&size_sigma) ) {
    cmd_sizes (size_mean,size_sigma) ;
  }
  else if ((2-1) == sscanf (line,"sizes %d",&size_mean) ) {
    cmd_sizes (size_mean,0) ;
  }
  else if ((4-1) == sscanf (line,"size %d %d %d",&idev,&size_mean,&size_sigma) ) {
    cmd_size (idev,size_mean,size_sigma) ;
  }
  else if ((3-1) == sscanf (line,"size %d %d",&idev,&size_mean) ) {
    cmd_size (idev,size_mean,0) ;
  }
  else {
    printf("illegal command: %s \n",line) ;
  }
  fflush(stdout) ;
}


void poll_cmd() 
{
  char buf[4096] ;
  char *line ;
  int size = 4096 ;

  line = fgets (buf, size, fp_cmdfifo) ;
  if (line == NULL) 
    return ;

  handle_cmd (line) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/



int test()
{
  //  int rt ;
  int idev ;
  int unitno ;
  struct zfrl_board_info *p_bi ;
  frl_control_t *p_frlc ;

  int nmb_triggers_pending ;
  int nmb_gen ;
  int n ;
  int nfree ;
  int nfree_min ;

  /* .. */

  for (;;) { /* endless loop */

    if (quit_flag) 
      break ;
    
    udelay (50) ;   /* wait for XX usec */
    
    poll_cmd() ;  // poll if command arrived and if so execute it

    // determine how many free entries available in trigger-fifo
    // take minimum across all participating FRLs

    nfree_min = FRL_GEN_SIZE_TRIGGER_FIFO ;

  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {

    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask

      unitno = setup_unitno[idev] ;
      /* get board info and set pointers for memory mapped access */
      
      p_bi = &devs_binfo[unitno] ;
      p_frlc = (frl_control_t * ) p_bi->iobase_main_user ;
      
      nmb_triggers_pending = getPCI32noswap(&p_frlc->nmb_triggers_pending) ;
      nfree = FRL_GEN_SIZE_TRIGGER_FIFO - nmb_triggers_pending ;
      if (nfree < nfree_min) nfree_min = nfree ;
      
      //      printf("idev %d nfree %d nfree_min %d \n",idev,nfree,nfree_min) ;
    }

#ifdef OBSOLETE    
    //    printf(" nmb_triggers_pending %d \n", nmb_triggers_pending) ;
    /* print a warning message if number of triggers in FIFO getting low
     *  can then decrease the delay time */
    if (arg_max_nmb_trigs == -1 && nmb_triggers_pending < 10) {
      //      printf(" nmb_triggers_pending %d \n", nmb_triggers_pending) ;
    }
#endif 

    // safety of 4 entries
    nfree_min -= 4 ;

    //
    // if sufficient amount of free entries
    // generate a batch of triggers

    if (nfree_min > 100) {
      
      if (trig_quota == -1) {
	nmb_gen = nfree_min ;
      }
      else {
	if (trig_quota > nfree_min) {
	  nmb_gen = nfree_min ;
	}
	else {
	  nmb_gen = trig_quota ;
	}
	trig_quota -= nmb_gen ;
      }

      //      printf("nmb_gen %d \n",nmb_gen) ;
      
      // generate triggers in all participating FRLs
      for (n=0 ; n < nmb_gen ; n++) {
	generate_next_trigger_devs_in_mask ();
      }
    }

  } // endless loop

  // come here when quitting
  // wait till all pending triggers flushed
  
  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {

    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask
    
    unitno = setup_unitno[idev] ;
    /* get board info and set pointers for memory mapped access */
    p_bi = &devs_binfo[unitno] ;
    p_frlc = (frl_control_t * ) p_bi->iobase_main_user ;
    
    printf("quit unit=%d \n",unitno) ;
    fflush(stdout) ;

    {
      unsigned int data ;
      data = FRL_CONTROL_SOFTRESET ;
      setPCI32noswap(&p_frlc->control,data) ;
      /* looks like after softreset ha sto settle a bit 
       * so do read of status register */
      data = getPCI32noswap(&p_frlc->control) ;
    }

    for (;;) {

      udelay (200) ;   /* wait for 200 usec */
      poll_cmd() ;  // poll if command arrived and if so execute it
    
      nmb_triggers_pending = getPCI32noswap(&p_frlc->nmb_triggers_pending) ;
      if (nmb_triggers_pending == 0) 
	break ;
    }
  }

  printf("trigger fifo(s) flushed ..\n") ;
 
  fflush(stdout) ;
 
  //  for (;;) { sleep(1) ; }
  
  return 0 ;
  goto error ;
 error:
  return -1;
}



/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void init_rngs() 
{
  SelectStream(0) ;
  PutSeed(1) ;
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/


int main(argc,argv)
     int    argc;
     char **argv;
{
  int rt ;
  struct itimerval itim, oitim ;
  //  int busnmb ;
  char loginname[512] ;
  char filename[512] ;
  char homename[512] ;
  char *p ;
  int fd_cmdinfo ;
  struct passwd * pwd_ent;

  gethostname (hostname,LEN_HOSTNAME);
/*  getlogin_r (loginname,512) ;*/
  if ((pwd_ent = getpwuid(geteuid()))) {
          strncpy (loginname, pwd_ent->pw_name, 512);
          printf("login name=%s \n",loginname);
  } else {
          die ("Don't know who I am...");
  }

  // find out home directory 
  // in order to put status file later
  strncpy (homename,"",512) ;
  p = getenv("HOME") ;
  if (p) {
    strncpy (homename,p,512) ;
  }
  printf("home name=%s \n",homename);    

  fflush(stdout) ;


  /* get cmd line args */

  command_line (argc, argv) ;

  /* sanity check of args */

  if (arg_mask == 0) 
    die ("bad mask ..") ; 

  // fill configuration of setup from hardcoded info

  rt = fill_setup() ;
  if (rt) {
    printf("fill_setup() error \n") ;
    exit(1);
  }

  // initialise random number generator

  init_rngs() ;

  // open command fifo 

  strcpy (filename,PATH_CMD_FIFO) ;

  rt = mkfifo (filename,(S_IREAD|S_IWRITE)) ;
  if (rt && errno != EEXIST) {
    printf("mkfifo() %s failed errno %d \n",filename,errno) ;
    exit (errno) ;
  }  
  fd_cmdinfo = open (filename,(O_RDONLY|O_NONBLOCK)) ;
  if (fd_cmdinfo == -1) {
    printf("open() %s failed errno %d \n",filename,errno) ;
    exit (errno) ;
  }
  fp_cmdfifo = fdopen (fd_cmdinfo, "r");
  if (fp_cmdfifo == NULL) {
    printf("fdopen() %s failed errno %d \n",filename,errno) ;
    exit (errno) ;
  }
   
  // open status file 

  //  strcpy (filename,userprefix) ; strcat (filename,loginname) ;
  strcpy (filename,homename) ;
  strcat (filename,"/status/frlgen.") ;
  strcat (filename,hostname) ;

  fp_status = fopen (filename,"w") ;
  if (fp_status == NULL) {
    printf("fopen() %s failed errno %d \n",filename,errno) ;
    perror (filename);
    exit (errno) ;
  }

  printf("fopen() %s \n",filename) ;
  fflush(stdout) ;

  /* install signal handler */

  signal (SIGINT,handler_sig);
  signal (SIGTERM,handler_sig);

  if (arg_quit) {
    printf("exiting .. \n") ;
    exit (0) ;
  }

  /* initialise frl driver and unit ufrl */
 
  rt = init_frl() ;
  if (rt) {
    printf("init_frl_on_bus() error \n") ;
    exit(1);
  }

  /* install signal handlers */

  signal (SIGALRM,handler_sigalrm) ;

  /* start interval timer alarm */

  gettimeofday (&time_alrm, NULL);

  itim.it_interval.tv_sec = INT_TIME ;
  itim.it_interval.tv_usec = 0 ;
  itim.it_value = itim.it_interval ;

  setitimer (ITIMER_REAL, &itim, &oitim) ;

  gettimeofday (&time_start, NULL);


#ifdef OUT
  for (;;) {  printf("sleeping ..\n") ; sleep(4);  }
#endif


  /* reset configure */

  frl_configure_all_devs() ;
  
  //    print_frl_status (p_frlc) ;

  /* perform test */

  test();

  // error:
  printf("exiting ..") ;
  printf("\n") ;
  fflush(stdout) ;

  //  for (;;) { sleep(1);}

  exit (0);
}



/* eof */
