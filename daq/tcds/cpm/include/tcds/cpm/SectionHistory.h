#ifndef _tcds_cpm_SectionHistory_h_
#define _tcds_cpm_SectionHistory_h_

#include <string>
#include <vector>

#include "tcds/cpm/LumiSection.h"

namespace tcds {
  namespace cpm {

    class SectionHistory
    {

    public:
      SectionHistory(std::vector<tcds::cpm::LumiSection> const& dataIn);
      ~SectionHistory();

      std::string getJSONString() const;

    private:
      // This constructor is not supposed to be used.
      SectionHistory();

      // These are the individual entries.
      std::vector<tcds::cpm::LumiSection> const sections_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_SectionHistory_h_
