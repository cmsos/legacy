#----------------------------------------------------------------------
# iCI configuration file for iCI part of tests.
#----------------------------------------------------------------------

# Enabled inputs.
main.inselect.combined_cyclic_bgo_enable                                 0x00000001

#----------------------------------------
# Cyclic generator 1: BC0 B-go.
#----------------------------------------
cyclic_generator1.configuration.enabled                                 0x00000001
cyclic_generator1.configuration.initial_prescale                        0x00000000
cyclic_generator1.configuration.pause                                   0x00000000
cyclic_generator1.configuration.permanent                               0x00000001
cyclic_generator1.configuration.postscale                               0x00000000
cyclic_generator1.configuration.prescale                                0x00000000
cyclic_generator1.configuration.repeat_cycle                            0x00000001
cyclic_generator1.configuration.start_bx                                0x00000dd4
cyclic_generator1.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator1.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator1.configuration.trigger_word.id                         0x00000001
cyclic_generator1.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 2: StartOfGap B-go.
#----------------------------------------
cyclic_generator2.configuration.enabled                                 0x00000001
cyclic_generator2.configuration.initial_prescale                        0x00000000
cyclic_generator2.configuration.pause                                   0x00000000
cyclic_generator2.configuration.permanent                               0x00000001
cyclic_generator2.configuration.postscale                               0x00000000
cyclic_generator2.configuration.prescale                                0x00000000
cyclic_generator2.configuration.repeat_cycle                            0x00000001
cyclic_generator2.configuration.start_bx                                0x00000d40
cyclic_generator2.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator2.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator2.configuration.trigger_word.id                         0x0000000b
cyclic_generator2.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# B-channel 1: BC0.
#----------------------------------------
bchannels.bchannel1.configuration.single                                 0x00000001
bchannels.bchannel1.configuration.double                                 0x00000000
bchannels.bchannel1.configuration.bx_sync                                0x00000000
bchannels.bchannel1.configuration.bx_or_delay                            0x00000000
bchannels.bchannel1.configuration.initial_prescale                       0x00000000
bchannels.bchannel1.configuration.prescale                               0x00000000
bchannels.bchannel1.configuration.postscale                              0x00000000
bchannels.bchannel1.configuration.repeat_cycle                           0x00000001
bchannels.bchannel1.ram                                                  0x00000001 0x00000000
bchannels.bchannel1.ram                                                  0x00000000 0x00000006

#----------------------------------------
# B-channel 11: StartOfGap.
#----------------------------------------
bchannels.bchannel11.configuration.single                                0x00000001
bchannels.bchannel11.configuration.double                                0x00000000
bchannels.bchannel11.configuration.bx_sync                               0x00000000

# These two lines make the difference.
# - This one works.
bchannels.bchannel11.configuration.bx_or_delay                           0x00000060
# - This one does not work.
# bchannels.bchannel11.configuration.bx_or_delay                           0x00000848

bchannels.bchannel11.configuration.initial_prescale                      0x00000000
bchannels.bchannel11.configuration.prescale                              0x00000003
bchannels.bchannel11.configuration.postscale                             0x00000000
bchannels.bchannel11.configuration.repeat_cycle                          0x00000001
bchannels.bchannel11.ram                                                 0x0000000b 0x00000000
bchannels.bchannel11.ram                                                 0x00000000 0x00000006

#----------------------------------------------------------------------
