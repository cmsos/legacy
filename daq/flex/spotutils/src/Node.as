package
{
	import flash.utils.Dictionary;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.collections.Sort;
	import mx.controls.Alert;
	import mx.controls.Image;
	import mx.graphics.BitmapFill;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import spark.collections.SortField;
	import flash.net.URLRequest;
	import flash.events.*;
	import flash.display.Loader;
	import flash.display.LoaderInfo;

	
	public class Node
	{
		private var _label:String;
		private var _parent:Node;
		private var _image:String;
		//icon: defaultIcon,
		private var exceptionCounters:Dictionary; 
		private var exceptionAcknowledgeCounters:Dictionary;
		private var exceptionAcknowledgeLists:Dictionary;
		private var exceptionLists:Dictionary;
		/*
		warningAckCounter:0,
		errorAckCounter:0,
		fatalAckCounter:0,
		warningAckList: new Array(),
		errorAckList: new Array(),
		fatalAckList: new Array(),
		warningCounter:0,
		errorCounter:0,
		fatalCounter:0,
		warningList: new Array(),
		errorList: new Array(),
		fatalList: new Array(),
		*/
		private var _pathId:Number = 0;
		private var _children:ArrayCollection;
		private var _self:ArrayCollection;
		private var _exp:Dictionary;
		private var _properties:ArrayList;
		private var _severityLevels:Dictionary;
		private var _icon:Bitmap;
		
		public function Node(name:String,father:Node, imagePath:String, id:Number, severityLevels:Dictionary)
		{
				/*
				var loader:Loader = new Loader();
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, completeHandler);
				loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
				*/
			
				this._severityLevels = severityLevels;
				this.label = name;
				this.parent = father;
				this.image = imagePath;
				/*
				loader.load( new URLRequest(imagePath) );
				*/
				
				//this.icon.cacheAsBitmap = true;
				this.pathId = id;
				exceptionCounters = new Dictionary();
				exceptionAcknowledgeCounters = new Dictionary();
				exceptionAcknowledgeLists = new Dictionary();
				exceptionLists = new Dictionary();

				for (var severity:Object in severityLevels)
				{
					exceptionCounters[severity] = 0;
					exceptionAcknowledgeCounters[severity] = 0;
					exceptionAcknowledgeLists[severity] = new Dictionary();
					exceptionLists[severity] = new Dictionary(); 
				}
				this.children = null;
				this.exp =  new Dictionary();
				this.properties = new ArrayList();
				
				this.self = new ArrayCollection();
				this.self.addItem(this);
		}
		/*
		private function completeHandler(event:Event):void {
			this.icon = (( event.target as LoaderInfo ).content as Bitmap);

		}
		
		private function ioErrorHandler(event:IOErrorEvent):void {
			Alert.show("Unable to load image: " + this.image);
		}
		
		public function get icon():Bitmap
		{
			return _icon;
		}

		public function set icon(value:Bitmap):void
		{
			_icon = value;
		}
		*/
		
		public function get self():ArrayCollection
		{
			return _self;
		}

		public function set self(value:ArrayCollection):void
		{
			_self = value;
		}

		public function isLeaf(): Boolean
		{
			return children == null;	
		}
		public function get hasExceptions():Boolean
		{
			return this.totalCount > 0 || this.totalAckCount > 0 ;
		}
		public function  count(severity:String):Number
		{
						
			return 	exceptionCounters[severity];
					
		}
		public function  ackCount(severity:String):Number
		{
			
			return 	exceptionAcknowledgeCounters[severity];
			
		}
		public function get totalCount():Number
		{
			var counters:Number = 0;
			
			for  (var severity:Object in this._severityLevels)
			{
				counters += exceptionCounters[severity];
				
			}	
			return counters;
		}

		public function get totalAckCount():Number
		{
			var counters:Number = 0;
			
			for (var severity:Object in this._severityLevels)
			{
				counters += exceptionAcknowledgeCounters[severity];
				
			}	
			return counters;
		}

		public function get counters():ArrayList
		{
			var counters:ArrayList = new ArrayList();
			
			for  (var severity:Object in this._severityLevels)
			{
				counters.addItem({name: severity , value: exceptionCounters[severity] });
				counters.addItem({name: severity + "-acknowledged" , value: exceptionAcknowledgeCounters[severity] });
				
			}	
			return counters;
		}

		public function match(exception:Object): Boolean
		{
		
			var matches:Number = 0;
			for (var uname:String in this._exp)
			{
				matches++;
				if ( exception.hasOwnProperty(uname) )
				{
					if (! this._exp[uname].test(exception[uname]) )
					{
							return false;	
					}
				}
				else
				{
					trace("not matching attribute '" + uname + "' for view in model" );
				}
			}	
			
			if ( matches > 0 ) return true
			else return false;
						
		}
		
		public function fire( exception:Object): Boolean
		{
			//this.addToScroll(exception,cursor);
			var uuid:String = exception.exception;
			for  (var severity:Object in this._severityLevels)
			{
				if (exception.severity.toLowerCase() == severity )
				{
				
					if ( (this.exceptionLists[severity])[uuid] != undefined )
					{
						return false;
					}
					else if ((this.exceptionAcknowledgeLists[severity])[uuid] != undefined ) 
					{
						// move from ack to active list
						this.exceptionAcknowledgeCounters[severity]--;
						delete (this.exceptionAcknowledgeLists[severity])[uuid];
					}
					
					this.exceptionCounters[severity]++;
					(this.exceptionLists[severity])[uuid] = exception;
					return true;
				
				}
				
			}
			
			return false;
		}
		
		// clear same algorithm than revoke, but invoked by GC
		
		public function clear( exception:Object): Boolean
		{
			var uuid:String = exception.exception;
			for (var severity:Object in this._severityLevels)
			{
				if (exception.severity.toLowerCase() == severity )
				{
					
					if ( (this.exceptionLists[severity])[uuid] != undefined ) // found
					{
						delete (this.exceptionLists[severity])[uuid];
						this.exceptionCounters[severity]--;
						return true;
					}
					else if ((this.exceptionAcknowledgeLists[severity])[uuid] != undefined ) 
					{
						this.exceptionAcknowledgeCounters[severity]--;
						delete (this.exceptionAcknowledgeLists[severity])[uuid];
						return true;
					}
					
					return false;
				}	
			}
			return false;
		}

		public function revoke( exception:Object): Boolean
		{
			var uuid:String = exception.exception;
			for (var severity:Object in this._severityLevels)
			{
				if (exception.severity.toLowerCase() == severity )
				{
					
						if ( (this.exceptionLists[severity])[uuid] != undefined ) // found
						{
							delete (this.exceptionLists[severity])[uuid];
							this.exceptionCounters[severity]--;
							return true;
						}
						else if ((this.exceptionAcknowledgeLists[severity])[uuid] != undefined ) 
						{
							this.exceptionAcknowledgeCounters[severity]--;
							delete (this.exceptionAcknowledgeLists[severity])[uuid];
							return true;
						}
						
						return false;
				}	
			}
			return false;
		}
		
		public function rearm( exception:Object): Boolean
		{	var uuid:String = exception.exception;
			for  (var severity:Object in this._severityLevels)
			{
				if (exception.severity.toLowerCase() == severity )
				{
					if ((this.exceptionAcknowledgeLists[severity])[uuid] != undefined ) 
					{
						return false;
					}
					else if ( (this.exceptionLists[severity])[uuid] != undefined )
					{
						this.exceptionCounters[severity]--;
						delete (this.exceptionLists[severity])[uuid];
						this.exceptionAcknowledgeCounters[severity]++;
						(this.exceptionAcknowledgeLists[severity])[uuid] = exception;
						return true;
					}
					return false;
				
				}
			}
			return false;
			
		}
		
		public function  getExceptionIds ():Array
		{
			var l:Array = new Array();
			
			for  (var severity:Object in this._severityLevels)
			{
				for (var e:String in (this.exceptionLists[severity]) )
				{
					l.push(e);
						
				}
			
			}               
			
			return l;
			
		}
		// This function is used every time the info browser is used and all exception are therefore initialized with the proper ack field

		
		public function  getExceptionsBySeverity (severity:String):ArrayCollection
		{
			var l:ArrayCollection = new ArrayCollection();
			
			for each (var e:Object in (this.exceptionLists[severity]) )
			{
					e.acknowledged = false;
					l.addItem(e);
					
			}           
			for each (var a:Object in (this.exceptionAcknowledgeLists[severity]) )
			{
				a.acknowledged = true;
				l.addItem(a);
				
			}
			return l;
			
		}
		
		public function  getExceptions ():ArrayCollection
		{
			var l:ArrayCollection = new ArrayCollection();
			
			for  (var severity:Object in this._severityLevels)
			{
				for each (var e:Object in (this.exceptionLists[severity]) )
				{
					e.acknowledged = false;
					l.addItem(e);
					
				}
				
			}               
			
			return l;
			
		}
		
		
		public function  getAllExceptions ():ArrayCollection
		{
			var l:ArrayCollection = new ArrayCollection();
			var sort:Sort = new Sort();
			sort.fields = [new SortField("dateTime",true)];
			
			for  (var severity:Object in this._severityLevels)
			{
				for each (var e:Object in (this.exceptionLists[severity]) )
				{
					e.acknowledged = false;
					l.addItem(e);
					
				}
				for each (var a:Object in (this.exceptionAcknowledgeLists[severity]) )
				{
					a.acknowledged = true;
					l.addItem(a);
					
				}
				
			}               
			l.sort = sort;
			l.refresh();
			return l;
			
		}
		
		
		public function  getHighestSeverity ():Number
		{
			var level:Number = 0;
			for  (var severity:Object in this._severityLevels) 
			{
				if ( this.exceptionCounters[severity] > 0 )
				{
					if ( this._severityLevels[severity] > level )
						level = this._severityLevels[severity];
				}
			}
			return level;
		}
		[Bindable]
		public function get properties():ArrayList
		{
			return _properties;
		}

		public function set properties(value:ArrayList):void
		{
			_properties = value;
		}
		[Bindable]
		public function get exp():Dictionary
		{
			return _exp;
		}

		public function set exp(value:Dictionary):void
		{
			_exp = value;
		}
		[Bindable]
		public function get children():ArrayCollection
		{
			return _children;
		}

		public function set children(value:ArrayCollection):void
		{
			_children = value;
		}
		[Bindable]
		public function get pathId():Number
		{
			return _pathId;
		}

		public function set pathId(value:Number):void
		{
			_pathId = value;
		}
		[Bindable]
		public function get image():String
		{
			return _image;
		}

		public function set image(value:String):void
		{
			_image = value;
		}
		[Bindable]
		public function get parent():Node
		{
			return _parent;
		}

		public function set parent(value:Node):void
		{
			_parent = value;
		}

		[Bindable]
		public function get label():String
		{
			return _label;
		}

		public function set label(value:String):void
		{
			_label = value;
		}
		
		
		
		
		
		public function get color():Number {       
			
			var level:Number = this.getHighestSeverity();
			
			
			if ( level >= 20 )
			{
				
				return 0xFF0000;
			
			}
			else if ( level >= 10) 
			{
				
				return 0xFF7F00;
			}
			else if ( level >=  1 )
			{
				return 0xFFD700;
			}
			else
			{
				return 0xffffff;
				
			}

			
		}

	}
}