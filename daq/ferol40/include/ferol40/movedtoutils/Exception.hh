#ifndef __ferol40_Exception
#define __ferol40_Exception

#include "xcept/Exception.h"

/* This definition is now in XDAQ:
#define XCEPT_DEFINE_EXCEPTION(NAMESPACE1 , EXCEPTION_NAME) \
namespace NAMESPACE1 { \
namespace exception { \
class EXCEPTION_NAME: public xcept::Exception \
{\
	public: \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
		xcept::Exception(name, message, module, line, function) \
	{}; \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception e ): \
		xcept::Exception(name, message, module, line, function, e) \
	{}; \
}; \
} \
}
#define XCEPT_DECLARE_NESTED( EXCEPTION, VAR, MSG, PREVIOUS ) \
EXCEPTION VAR( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__, PREVIOUS)
*/


/************************************************************************
*
*
*     @short Baseclass of all ferol40 exceptions.
*            
*            It  is  useful  to  define  a  baseclass  for  all  ferol40 
*            exceptions since then you  can catch on this baseclass in
*            order to catch all possible subclasses with one command.
*
*       @see 
*    @author $Author: schwick $
*   @version $Revision: 1.6 $
*      @date $Date: 2001/03/06 17:07:51 $
*
*
**//////////////////////////////////////////////////////////////////////
namespace ferol40 {
    namespace exception {
        class utils::Exception : public xcept::Exception {
        public:
            utils::Exception( std::string name, std::string message, std::string module, int line, std::string function )
                : xcept::Exception(name, message, module, line, function)
            {};
            utils::Exception( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception e )
                : xcept::Exception(name, message, module, line, function, e)
            {};
        };
    }
}


#define FEROL40_DEFINE_EXCEPTION( EXCEPTION_NAME )  \
namespace ferol40 { \
    namespace exception {                                               \
        class EXCEPTION_NAME : public utils::exception::Exception        \
        {                                                               \
	public:                                                         \
            EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
		utils::exception::Exception(name, message, module, line, function) \
            {};                                                         \
            EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception e ): \
		utils::exception::Exception(name, message, module, line, function, e) \
            {};                                                         \
        };  \
    }       \
}


// Here are the Ferol40 utils::Exceptions which we use in the ferol40 applicaton
FEROL40_DEFINE_EXCEPTION( ConfigurationProblem )
FEROL40_DEFINE_EXCEPTION( SoftwareProblem )
FEROL40_DEFINE_EXCEPTION( HardwareAccessFailed )
FEROL40_DEFINE_EXCEPTION( HardwareNotAvailable )
FEROL40_DEFINE_EXCEPTION( IllegalStateTransition )
FEROL40_DEFINE_EXCEPTION( SOAPTransitionProblem )

//FEROL40_DEFINE_EXCEPTION( FrlException )
FEROL40_DEFINE_EXCEPTION( Ferol40Exception )
FEROL40_DEFINE_EXCEPTION( SlinkTestFailed )

FEROL40_DEFINE_EXCEPTION( RCMSNotificationError )
FEROL40_DEFINE_EXCEPTION( DataCorruptionDetected )

#endif /* __ferol40_Exception */

