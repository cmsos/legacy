#ifndef bril_supervisor_SinceData_h
#define bril_supervisor_SinceData_h
#include "soci/soci.h"
#include <string>

namespace bril {namespace supervisor{
    struct SinceData{
    SinceData():since(0){}
      long long since; //long long maps to NUMBER(10)
      std::string func;
      std::string payload;
    };
    typedef std::map< unsigned int, SinceData > Sincemap;
  }}

namespace soci{
  template<>
    struct type_conversion<bril::supervisor::SinceData>{
    typedef values base_type;
    
    static void from_base(const values& v, indicator ind, bril::supervisor::SinceData& p)
    {
      p.since = v.get<long long>("SINCE");
      p.payload = v.get<std::string>("PAYLOAD");
      p.func = v.get<std::string>("FUNC");

      // p.func will be set to the default value "unknown"
      // when the column is null:
      p.since = v.get<long long>("SINCE",0);
      p.payload = v.get<std::string>("PAYLOAD", "unknown");
      p.func = v.get<std::string>("FUNC", "unknown");
    }
    
    static void to_base(const bril::supervisor::SinceData & p, values & v, indicator & ind)
    {
      v.set("SINCE", p.since);
      v.set("PAYLOAD", p.payload);
      v.set("FUNC", p.func);
      ind = i_ok;
    }
  };
}
#endif
