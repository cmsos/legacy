{
  "targets": [
    {
      "target_name": "alchemy",
      "sources": [
        "src/common/addon.cc",
        "src/common/myobject.cc",
        "src/common/ApplicationDescriptor.cc",
        "src/common/ApplicationContext.cc",
        "src/common/PeerTransportAgent.cc",
        "src/common/Listener.cc",
        "src/common/Address.cc",
        "src/common/SOAPMessenger.cc",
        "src/common/SOAPLoopbackMessenger.cc",
		"src/common/HTTPSender.cc",
		"src/common/Facade.cc"
      ],
       "cflags_cc!": [ "-fno-rtti",  "-fno-exceptions", "-no-fpermissive","-O3"  ],
       "cflags_cc+": ["-DOS_VERSION_CODE=132640", "-Dx86_64_slc6", "-Dlinux", "-DLITTLE_ENDIAN__"],
       "cflags!": [ "-fno-rtti", "-fno-exceptions",  "-no-fpermissive", "-O3" ],
      "include_dirs": [
      	"./include/alchemy",
  		"/opt/xdaq/include",
  		"/opt/xdaq/include/linux",
	  ],
	  'link_settings': {
      "libraries": [
        "-L/opt/xdaq/lib",
        "-lxdaq",
  		"-lcrypt", 
  		"-lconfig",
  		"-lpeer",
  		"-ltoolbox",
  		"-lasyncresolv",
  		"-llog4cplus",
  		"-lxerces-c",
  		"-lcgicc",
  		"-lxcept",
  		"-lxoap",
  		"-lxdata",
  		"-lxgi",
  		"-llogudpappender",
  		"-llogxmlappender",
  		"-lmimetic",
  		"-luuid"
		]
		}
    }
  ] 
}
