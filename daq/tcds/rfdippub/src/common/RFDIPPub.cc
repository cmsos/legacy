#include "tcds/rfdippub/RFDIPPub.h"

// #include <memory>
#include <string>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/rfdippub/ConfigurationInfoSpaceHandler.h"
#include "tcds/rfdippub/RF2TTCDataInfoSpaceHandler.h"
#include "tcds/rfdippub/RF2TTCDataInfoSpaceUpdater.h"
#include "tcds/rfdippub/RFRXDDataInfoSpaceHandler.h"
#include "tcds/rfdippub/RFRXDDataInfoSpaceUpdater.h"
#include "tcds/exception/Exception.h"
#include "tcds/utils/LogMacros.h"

XDAQ_INSTANTIATOR_IMPL(tcds::rfdippub::RFDIPPub);

tcds::rfdippub::RFDIPPub::RFDIPPub(xdaq::ApplicationStub* const stub)
try
  :
  tcds::utils::XDAQAppBase(stub, std::auto_ptr<tcds::hwlayer::DeviceBase>(0)),
    rf2ttcInfoSpaceUpdaterP_(0),
    rf2ttcInfoSpaceP_(0),
    rfrxdInfoSpaceUpdaterP_(0),
    rfrxdInfoSpaceP_(0)
      {
        // Create the InfoSpace holding all configuration information.
        cfgInfoSpaceP_ =
          std::auto_ptr<ConfigurationInfoSpaceHandler>(new ConfigurationInfoSpaceHandler(*this));
      }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the RFDIPPub application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::rfdippub::RFDIPPub::~RFDIPPub()
{
}

void
tcds::rfdippub::RFDIPPub::setupInfoSpaces()
{
  std::cout << "DEBUG JGH 2018 RFDIPPub::setupInfoSpaces()" << std::endl;
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  rf2ttcInfoSpaceUpdaterP_ =
    std::auto_ptr<RF2TTCDataInfoSpaceUpdater>(new RF2TTCDataInfoSpaceUpdater(*this));
  rf2ttcInfoSpaceP_ =
    std::auto_ptr<RF2TTCDataInfoSpaceHandler>(new RF2TTCDataInfoSpaceHandler(*this, rf2ttcInfoSpaceUpdaterP_.get()));
  rfrxdInfoSpaceUpdaterP_ =
    std::auto_ptr<RFRXDDataInfoSpaceUpdater>(new RFRXDDataInfoSpaceUpdater(*this));
  rfrxdInfoSpaceP_ =
    std::auto_ptr<RFRXDDataInfoSpaceHandler>(new RFRXDDataInfoSpaceHandler(*this, rfrxdInfoSpaceUpdaterP_.get()));

  // We don't have an FSM, so no state either.
  appStateInfoSpace_.setString("stateName", "n/a");
  // Similar for the hardware lease.
  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  rf2ttcInfoSpaceP_->registerItemSets(monitor_, webServer_);
  rfrxdInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

void
tcds::rfdippub::RFDIPPub::hwConnectImpl()
{
}

void
tcds::rfdippub::RFDIPPub::hwReleaseImpl()
{
}

void
tcds::rfdippub::RFDIPPub::hwConfigureImpl()
{
}
