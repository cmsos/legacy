#ifndef _tcds_cpm_NibbleHistory_h_
#define _tcds_cpm_NibbleHistory_h_

#include <string>
#include <vector>

#include "tcds/cpm/LumiNibble.h"

namespace tcds {
  namespace cpm {

    class NibbleHistory
    {

    public:
      NibbleHistory(std::vector<tcds::cpm::LumiNibble> const& dataIn);
      ~NibbleHistory();

      std::string getJSONString() const;

    private:
      // This constructor is not supposed to be used.
      NibbleHistory();

      // These are the raw entries.
      std::vector<tcds::cpm::LumiNibble> const nibbles_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_NibbleHistory_h_
