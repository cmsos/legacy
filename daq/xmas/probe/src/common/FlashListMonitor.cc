// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xcept/tools.h"
#include "xmas/probe/FlashListMonitor.h"
#include "toolbox/regex.h"
#include "xdata/ItemGroupEvent.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Properties.h"
#include "xmas/probe/MonitorReportEvent.h"
#include "xmas/MonitorSettings.h"
#include "xmas/probe/InstantSampler.h"
#include "xmas/probe/HistorySampler.h"
#include "xmas/probe/HashSampler.h"
#include "xmas/probe/exception/FlashListMonitorCreationFailed.h"
#include "xmas/probe/exception/AmbiguousFlashListDefinition.h"
#include "xmas/probe/exception/MissingInfoSpaceDeclaration.h"
#include "xmas/probe/exception/EvaluationFailed.h"
#include "toolbox/Runtime.h"
#include "xmas/probe/exception/Exception.h"
#include "toolbox/net/Utils.h"

xmas::probe::FlashListMonitor::FlashListMonitor
(
	xmas::MonitorSettings * settings, 
	MetricsScheduler* scheduler,
	xdaq::Application * owner
	
)
	
	
	:xdaq::Object(owner), settings_(settings),scheduler_(scheduler)
{
	monitorFireCounter_              = 0;
	monitorPulseCounter_             = 0;
	monitorInternalLossCounter_      = 0;
	monitorCommunicationLossCounter_ = 0;
	monitorMemoryLossCounter_        = 0;
	monitorUnassignedLossCounter_    = 0;
	
	xdata::getInfoSpaceFactory()->lock();
	
	// Attach to all publishers
	// Before creating the FlashListMonitor, see if the requested consumers
	// are available and add them to the dispatcher. If this is not possible
	// do not even create a FlashListMonitor
	std::string mode = settings->getProperty("mode");
	std::string remote = settings->getProperty("remote");
	std::vector<xmas::SamplerSettings*> samplerSettings = settings->getSamplerSettings();
		
	std::vector<xmas::SamplerSettings*>::iterator i;
	for (i = samplerSettings.begin(); i != samplerSettings.end(); ++i)
	{
		std::string type = (*i)->getProperty("type");
		xmas::probe::Sampler* sampler = 0;
				
		if (type == "urn:xmas-sampler:instant")
		{
			sampler = new xmas::probe::InstantSampler( (*i), settings->getFlashListDefinition() );	
		}
		else if (type == "urn:xmas-sampler:history")
		{
			sampler = new xmas::probe::HistorySampler( (*i), settings->getFlashListDefinition() );	
		}
		else if (type == "urn:xmas-sampler:hash")
		{
			sampler = new xmas::probe::HashSampler( (*i), settings->getFlashListDefinition(), this->getOwnerApplication() );	
		}
		else
		{
			std::stringstream msg;
			msg << "Invalid sampler type '" << type << "'";
			XCEPT_RAISE (xmas::probe::exception::FlashListMonitorCreationFailed, msg.str());
		}
		
		this->addActionListener(sampler);
		
	}
	
	//
	// For all infoscapes indicated attach listeners
	// 
	std::set<std::string> isst = settings->getFlashListDefinition()->getInfospacesRegexSet();
	std::set<std::string>::iterator si;
	for ( si = isst.begin(); si != isst.end(); si++ )
	{
		//
		// search all instances (if was a prefix ) for this infospace name and attach listener
		//	
	        std::map<std::string, xdata::Serializable *, std::less<std::string> > iss = xdata::getInfoSpaceFactory()->match(*si); 
 		 for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator j = iss.begin(); j != iss.end(); ++j) 	
                {
			xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*j).second);
			is->addGroupChangedListener(this);						
		}	
	}
	
	xdata::getInfoSpaceFactory()->unlock();					
}

std::list<xmas::probe::Sampler*> xmas::probe::FlashListMonitor::getSamplers()
{
	std::list<xmas::probe::Sampler*> samplers;
	std::list<toolbox::ActionListener*> listeners = this->getActionListeners();
	std::list<toolbox::ActionListener*>::iterator ai;
		
	for (ai = listeners.begin(); ai != listeners.end(); ++ai)
	{
		samplers.push_back( dynamic_cast<xmas::probe::Sampler*>(*ai) );
	}
	return samplers;
}

xmas::probe::FlashListMonitor::~FlashListMonitor()
{
	xdata::getInfoSpaceFactory()->lock();

	// Detach and delete samplers
	std::list<toolbox::ActionListener*> samplers = this->getActionListeners();
	std::list<toolbox::ActionListener*>::iterator ai;
	for (ai = samplers.begin(); ai != samplers.end(); ++ai)
	{
		this->removeActionListener( *ai );
		delete (*ai);
	}
	
	std::set<std::string> isst = settings_->getFlashListDefinition()->getInfospacesRegexSet();
	std::set<std::string>::iterator i;
	for ( i = isst.begin(); i != isst.end(); i++ )
	{
		//
		// search all instances (if was a prefix ) for this infospace name and detach listener
		//
		std::map<std::string, xdata::Serializable *, std::less<std::string> > iss = xdata::getInfoSpaceFactory()->match(*i);
		for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator j = iss.begin(); j != iss.end(); ++j)	
                {
			try
			{
				xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*j).second);
				is->lock();
				is->removeGroupChangedListener(this);
				is->unlock();
			}
			catch(xdata::exception::Exception & e)
			{
				// ignore
			}
		}	
	}
	
	delete settings_;
	
	xdata::getInfoSpaceFactory()->unlock();
}

void xmas::probe::FlashListMonitor::attachToInfospace(xdata::InfoSpace* infospace)
{
	// Check if the infospace match any in the infospace set for this flashList
	// and add listsner to it, otherwise ignore
	//
	xdata::getInfoSpaceFactory()->lock();
	if (this->belongsTo(infospace->name()))
	{
		infospace->lock();
		infospace->addGroupChangedListener(this);
		infospace->unlock();
	}
	xdata::getInfoSpaceFactory()->unlock();
}

bool xmas::probe::FlashListMonitor::belongsTo(const std::string& infospace)
{
	// Check if the infospace match any in the infospace set for this flashList
	// and add listsner to it, otherwise ignore
	//
	std::set<std::string> isst = settings_->getFlashListDefinition()->getInfospacesRegexSet();
	std::set<std::string>::iterator i;
	for ( i = isst.begin(); i != isst.end(); i++ )
	{
		if ( toolbox::regx_match(infospace, (*i)) )
		{
			return true;
		}
	}
	return false;
}

bool xmas::probe::FlashListMonitor::resolveMultipleMetrics( 
			    std::map<std::string, std::vector<xmas::ItemDefinition*> > & items,
			    std::map<std::string, std::vector<xdata::InfoSpace*> >  & infospaces )
	
{

	///////////////////////////////////////
	xdata::getInfoSpaceFactory()->lock();
	///////////////////////////////////////

	std::set<std::string> isst = settings_->getFlashListDefinition()->getInfospacesRegexSet();
	
	std::set<std::string>::iterator i;
	for ( i = isst.begin(); i != isst.end(); i++ )
	{
		std::stringstream expression;
		expression << (*i); /* << ".*"; */ 
		//std::cout << "--->>> search infospaces [" << expression.str() << "]"  << std::endl;
		std::map<std::string, xdata::Serializable *, std::less<std::string> > spaces = xdata::getInfoSpaceFactory()->match(expression.str()); 
 	        for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator it = spaces.begin(); it != spaces.end(); ++it) 
 	        { 
 	             infospaces[*i].push_back(dynamic_cast<xdata::InfoSpace *> ((*it).second)); 
 	        } 
 	 
 	        std::vector<xdata::InfoSpace*> & ir = infospaces[*i]; 
		//std::cout << "---->>>> found infospaces [" << ir.size() << "]"  << std::endl;
		if ( ir.size() > 0 )
		{
			for (std::vector<xdata::InfoSpace*>::iterator j = ir.begin(); j != ir.end(); j++ )
			{
				(*j)->lock();
				//std::cout << "---- >>> check infospaces [" << (*j)->name() << "]"  << std::endl;

				//items[*i] = settings_->getFlashListDefinition()->matchInfoSpaceItemProperties(*i);
				items[*i] = settings_->getFlashListDefinition()->matchInfoSpaceItemProperties((*j)->name());

				std::vector<xmas::ItemDefinition*> & pr = items[*i];
				std::vector<xmas::ItemDefinition*>::iterator k;
				
				//std::cout << "Size of vector: " << pr.size() << std::endl;
				
				for ( k = pr.begin(); k != pr.end(); k++)
				{				
					// Check if flashlist item has an infospace property
					if ((*k)->hasProperty("infospace") ) // look in flashlist, only infosapce item types, otherwise skipt it
					{
						std::string source = (*k)->getProperty("source");
						
						//std::cout << "Item to be resolved: " << source << std::endl;
						
						if ( source.find("$") == 0 )
						{
							try
							{
								xdata::Properties * p = dynamic_cast<xdata::Properties*>((*j)->find("_descriptor"));
								if (! p->hasProperty(source.substr(1)) )
								{
									(*j)->unlock();
									///////////////////////////////////////
									xdata::getInfoSpaceFactory()->unlock();
									///////////////////////////////////////
									//std::cout << "could not resolve variable" << source << " in descriptor" << std::endl;
									return false;	
								}

							}
							catch(xdata::exception::Exception & e)
							{
								(*j)->unlock();
								///////////////////////////////////////
								xdata::getInfoSpaceFactory()->unlock();
								///////////////////////////////////////
								//std::cout << "could not resolve variable" << source << " in InfoSpace" << std::endl;
								return false;
							}
						}
						else if ( ! (*j)->hasItem( source ) )
						{ 
							(*j)->unlock();
							///////////////////////////////////////
							xdata::getInfoSpaceFactory()->unlock();
							///////////////////////////////////////
							//std::cout << "could not find item" << (*k)->getProperty("name") << " in InfoSpace" << std::endl;
							return false;
						}
					}
				}
				(*j)->unlock();		
			}
		}
		else
		{
			//std::cout << "could not find infospaces for " << (*i) << std::endl;
			///////////////////////////////////////
			xdata::getInfoSpaceFactory()->unlock();
			///////////////////////////////////////
			return false;
		}
	}
	//std::cout << "resolved infospace for this flash" << std::endl;
	///////////////////////////////////////
	xdata::getInfoSpaceFactory()->unlock();
	///////////////////////////////////////
	return true;
}

bool xmas::probe::FlashListMonitor::resolveSingleMetrics( 
			    std::vector<xmas::ItemDefinition*>& items,
			    xdata::InfoSpace* infospace )
{
	infospace->lock();
	

	items = settings_->getFlashListDefinition()->getInfoSpaceItemProperties();
	

	std::vector<xmas::ItemDefinition*>::iterator i;
	for ( i = items.begin(); i != items.end(); i++)
	{	
		std::string source = (*i)->getProperty("source");

		if ( source.find("$") == 0 ) 
		{
			try
			{
				xdata::Properties * p = dynamic_cast<xdata::Properties*>(infospace->find("_descriptor"));
				if (! p->hasProperty(source.substr(1)) )
				{
					infospace->unlock();
					return false;	
				}

			}
			catch(xdata::exception::Exception & e)
			{
				infospace->unlock();
				return false;
			}
		}
		else if ( ! infospace->hasItem( source ) )
		{ 
			infospace->unlock();
			return false;

		}
		
	}
	infospace->unlock();		
	return true;
}


void xmas::probe::FlashListMonitor::addFunctionMetrics(xdata::Table::Reference & table)
	
{
	std::vector<xmas::ItemDefinition*>::iterator j;
	std::vector<xmas::ItemDefinition*> functions = settings_->getFlashListDefinition()->getFunctionItemProperties();
	
	// Add all the function metrics columns
	for (j = functions.begin(); j != functions.end(); ++j)
	{
		std::stringstream id;
		id << (*j)->getProperty("name");
		try 
		{	
			table->addColumn(id.str(), (*j)->getProperty("type"));
		}	
		catch (xdata::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to add column name '" << id.str() << "' (invalid or ambiguous)";
			XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
		}
		
		xdata::Serializable * value = 0;
		try 
		{		
			value = this->eval( (*j)->getProperty("function"), (*j)->getProperty("type") );
		}
		catch (xmas::probe::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to evaluate function metrics '";
			msg << (*j)->getProperty("function") << "' for column '" << id.str() << "'";
			XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
		}	
		
		try
		{
			// Loop over all rows and add the function values
			for (size_t r = 0; r < table->getRowCount(); ++r)
			{
				table->setValueAt (r, id.str(), *value );
			}
			delete value;
		}
		catch (xdata::exception::Exception & e)
		{
			delete value;
			std::stringstream msg;
			msg << "Failed to set value for column name '" << id.str() << "' (invalid or ambiguous)";
			XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
		}	
	}
}

xdata::Table::Reference xmas::probe::FlashListMonitor::retrieveSingleMetrics(
			std::vector<xmas::ItemDefinition*> & items,
			xdata::InfoSpace* infospace)
	
{
	xdata::Table::Reference t(new xdata::Table());
	std::list<std::string> names;
	std::vector<xmas::ItemDefinition*>::iterator i;
	
	infospace->lock();
	
	try
	{
		//std::cout << "Prepare table with " << items.size() << " columns" << std::endl;
		
		for (i = items.begin(); i != items.end(); ++i)
		{
			//std::cout << "Preparing table column name '" << (*i)->getProperty("name") << "'" << std::endl;
			if ((*i)->hasProperty("infospace") ) // only infospace items
			{
				std::string source = (*i)->getProperty("source");
		
				if ( source.find("$") != 0 ) // fires user items only
				{
					names.push_back(source);
				}
			}	
		}
		
		// Notify retrieval of items to interested parties
		infospace->fireItemGroupRetrieve(names, this);
	}
	catch (xdata::exception::Exception & ex)
	{
		std::stringstream msg;
		msg << "failed to fire group retrieve event to infospace '" << infospace->name() << "'";
		infospace->unlock();
		XCEPT_RETHROW(	xmas::probe::exception::Exception, msg.str(), ex);
	}
	
	for (i = items.begin(); i != items.end(); ++i)
	{	
		if ( (*i)->hasProperty("infospace") ) // only infospace items
		{		
			//std::stringstream id;
			//id <<  (*i)->getProperty("infospace") << ":" << (*i)->getProperty("name");

			//std::cout << "Add column '" << id.str() << "'" << std::endl;
			std::string name = (*i)->getProperty("name");
			try 
			{	
				std::string source = (*i)->getProperty("source");
				
				// Get header
				t->addColumn(name, (*i)->getProperty("type"));
				xdata::Serializable * s;
				
				if ( source.find("$") == 0 )
				{
					xdata::String value;
					
					xdata::Properties * p = dynamic_cast<xdata::Properties*>(infospace->find("_descriptor"));
					if (! p->hasProperty(source.substr(1)) )
					{
						std::stringstream msg;
						msg << "Could not resolve variable '" << source << "' in application descriptor";
						infospace->unlock();
						XCEPT_RAISE ( xmas::probe::exception::Exception, msg.str() );
					}
					else
					{
						value = p->getProperty(source.substr(1));
						t->setValueAt(0, name , value);
					}
				}
				else
				{
					// Get value
				 	s = infospace->find( source );
					/* DEBUG 
					if ( s->type() == "table" )
					{

						std::cout << "Table size:" <<  dynamic_cast<xdata::Table*>(s)->getRowCount() << std::endl;
					}
					*/
					if ( (*i)->getProperty("type") == "string")
					{
						std::string maxsize = (*i)->getProperty("maxsize");
						if(maxsize == "")
						{
							maxsize = "80";
						}

						// Limit maximum string length in flashlist
						try
						{
							size_t len = toolbox::toUnsignedLong(maxsize);
							std::string v = (*s).toString().substr(0, len);
							xdata::String safeString(v);
							t->setValueAt(0, name, safeString);
						}
						catch(toolbox::exception::Exception & e)
						{
							std::stringstream msg;
							msg << "Invalid maxsize for string '" << name << "'(maxsize='" << maxsize << "')";
							infospace->unlock();
							XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
						}
					}
					else
					{
						t->setValueAt(0, name, *s);
					}
					//std::cout << "Add value " << s->toString() << std::endl;
				}
			}
			catch (xdata::exception::Exception & e)
			{
				std::stringstream msg;
				msg << "cannot add column name '" << name << "' (invalid or ambiguous)";
				infospace->unlock();
				XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
			}
		}	
	}
	infospace->unlock();
	
	return t;
} // end of retrieve



xdata::Table::Reference xmas::probe::FlashListMonitor::retrieveMultipleMetrics(
			std::map<std::string, std::vector<xmas::ItemDefinition*> > & items,
			std::map<std::string, std::vector<xdata::InfoSpace*> >  & infospaces)
	
{
	xdata::Table::Reference cartesian;
	std::map<std::string, std::vector<xmas::ItemDefinition*> >::iterator i;
	for ( i = items.begin(); i != items.end(); i++ ) // main loop over all infosapces prefixes
	{
		//
		// preapring/extending Table header for fields
		//
		xdata::Table::Reference t(new xdata::Table());
		std::list<std::string> names;
		std::vector<xmas::ItemDefinition*>::iterator j; 
		std::vector<xmas::ItemDefinition*> & p = (*i).second;
		for (j = p.begin(); j != p.end(); j++)
		{	
			if ( (*j)->hasProperty("infospace") ) // only infospace items
			{
				std::string source =  (*j)->getProperty("source");			
				
				if ( source.find("$") != 0 )
				{
					names.push_back(source);
				}
				
				try 
				{	
					t->addColumn((*j)->getProperty("name"), (*j)->getProperty("type"));
				}
				catch (xdata::exception::Exception & e)
				{
					std::stringstream msg;
					msg << "cannot add column name '" << (*j)->getProperty("name") << "' (invalid or ambiguous)";
					XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
				}
			}		

		}
		//
		// search all instances (rows) for this infospace name and fill temporary table
		//
		std::vector<xdata::InfoSpace*> &  kr = infospaces[(*i).first];
		std::vector<xdata::InfoSpace*>::iterator k;
		size_t row = 0;
		for (k = kr.begin(); k != kr.end(); k++, row++)
		{
			//
			// Fire user about the retrieving of these items
			//
			(*k)->lock();
			try 
			{
				(*k)->fireItemGroupRetrieve(names, this);
			}
			catch (xdata::exception::Exception & ex)
			{
				(*k)->unlock();
				std::stringstream msg;
				msg << "failed to fire group retrieve event to infospace '" << (*i).first << "'";
				XCEPT_RETHROW(	xmas::probe::exception::Exception, msg.str(), ex);
			}

			//
			// retrieve data items and fill table
			//
			for (std::vector<toolbox::Properties*>::size_type w = 0; w < p.size(); w++)
			{	
			
				if ( p[w]->hasProperty("infospace") ) // only infospace items
				{					
					//std::stringstream id;
					//id <<  p[w]->getProperty("infospace") << ":" << p[w]->getProperty("name");
					std::string name =  p[w]->getProperty("name");
					std::string source = p[w]->getProperty("source");
					try 
					{
						if (source.find("$") == 0 )
						{
							xdata::String value;
							
							xdata::Properties * props = dynamic_cast<xdata::Properties*>((*k)->find("_descriptor"));
							if (! props->hasProperty(source.substr(1)) )
							{
								(*k)->unlock();
								std::stringstream msg;
								msg << "Could not resolve variable '" << source << "' in application descriptor";
								XCEPT_RAISE ( xmas::probe::exception::Exception, msg.str() );
							}
							else
							{
								value = props->getProperty(source.substr(1));
							}
							
							t->setValueAt(row, name, value); 
						}
						else
						{
							xdata::Serializable * s = (*k)->find(source);
							if ( p[w]->getProperty("type") == "string")
							{
								std::string maxsize = p[w]->getProperty("maxsize");
								if(maxsize == "")
								{
									maxsize = "80";
								}

								// Limit maximum string length in flashlist
								try
								{
									size_t len = toolbox::toUnsignedLong(maxsize);
									std::string v = (*s).toString().substr(0, len);
									xdata::String safeString(v);
									t->setValueAt(row, name, safeString);
								}
								catch(toolbox::exception::Exception & e)
								{
									(*k)->unlock();
									std::stringstream msg;
									msg << "Invalid maxsize for string '" << name << "'(maxsize='" << maxsize << "')";
									XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
								}
							}
							else
							{
								t->setValueAt(row, name , *s); 
							}
						}
					}
					catch (xdata::exception::Exception & e )
					{
						(*k)->unlock();

						std::stringstream msg;
						msg << "possible ambigous infospace pattern '" << (*i).first << "' leads to '" << (*k)->name() << "'";
						XCEPT_RETHROW(	xmas::probe::exception::Exception, msg.str() , e);						
					}
				}	
			}
			(*k)->unlock();
		}
		//
		// build cartesian product if flashList spans more than one infosapce
		//
		if ( cartesian.isNull() )
		{
			cartesian = t;
		}			
		else
		{
			try
			{
				xdata::Table * product = xdata::Table::cartesianJoin(&(*cartesian), &(*t)); 
				cartesian = product;
			}
			catch(xdata::exception::Exception & e)
			{
				std::stringstream msg;
                                msg << "failed cartesian join when retrieving flashlist";
                                XCEPT_RETHROW(  xmas::probe::exception::Exception, msg.str() , e);
				
			}
		}
	}

	return cartesian;
} // end of retrieve


// The execution of this method is under the locking of the infospace doing the change
//
void xmas::probe::FlashListMonitor::actionPerformed( xdata::Event& event) 
{	
	
	//
	// !! This call is a synchrnous call from the infospace, therefore the factory does not need to be locked
	// here. It is responsibility of the user to guaranteed that this info space exists.
	//

	//std::cout << "received event " << event.type() << std::endl;

	if (event.type() != "urn:xdata-event:ItemGroupChangedEvent")
	{
		return;
	}

	this->incrementFireCounter();

	// process event
	//
	std::set<std::string> isst = settings_->getFlashListDefinition()->getInfospacesRegexSet();
	if (( isst.size() != 1 ) )
	{
		std::stringstream  msg;
		msg << "Ambiguous flash list definition '" << settings_->getFlashListDefinition()->getProperty("name") 
			<< "', does not match infospace";

		XCEPT_DECLARE(xmas::probe::exception::AmbiguousFlashListDefinition, e, msg.str());
		this->getOwnerApplication()->notifyQualified("fatal",e);
		return;	
	}


	xdata::ItemGroupChangedEvent & e = dynamic_cast<xdata::ItemGroupChangedEvent&>(event);
	xdata::InfoSpace* infospace = e.infoSpace(); 

	// resolve flash list
	//
	std::vector<xmas::ItemDefinition*> items;

	
	std::string pattern = *(isst.begin());

	
	if ( this->resolveSingleMetrics(items, infospace ) )
	{
		xdata::Table::Reference t;
		
		
		try
		{
			t = this->retrieveSingleMetrics(items, infospace);
		}
		catch(xmas::probe::exception::Exception & e)
		{
			
			std::stringstream  msg;
			msg << "Failed to process user infospace change event for flashlist '" <<
			settings_->getFlashListDefinition()->getProperty("name") << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q , msg.str() , e);
			this->getOwnerApplication()->notifyQualified("error",q);			
			return;		
		}
		
		if ( t.isNull() )
		{

			std::stringstream  msg;
			msg << "At least one infospace required for flashlist '" <<
			settings_->getFlashListDefinition()->getProperty("name") << "'";

			XCEPT_DECLARE(xmas::probe::exception::MissingInfoSpaceDeclaration, e , msg.str());
			this->getOwnerApplication()->notifyQualified("fatal",e);
			return;
		}
		
		try
		{	
			// Add the values of the functions according the flashlist definition
			this->addFunctionMetrics(t);
		}
		catch(xmas::probe::exception::Exception & e)
		{

			std::stringstream  msg;
			msg << "Cannot evaluate functions for flashlist '" <<
			settings_->getFlashListDefinition()->getProperty("name") << "'";

			XCEPT_DECLARE_NESTED(xmas::probe::exception::EvaluationFailed, ex , msg.str(),e);
			this->getOwnerApplication()->notifyQualified("fatal",ex);
			return;
		}
		
		
		xmas::probe::MonitorReportEvent * ep = new xmas::probe::MonitorReportEvent(this->getFlashListDefinition());
		toolbox::task::EventReference e (ep);
		
		// Add as originator the source of the InfoSpace creator (not reliable as source, but better
		// than using the probe as the originator
		if (infospace->hasItem("_descriptor"))
		{
			xdata::Properties * p = dynamic_cast<xdata::Properties*>(infospace->find("_descriptor"));
			std::string originator = p->getProperty("context");
			originator += "/urn:xdaq-application:uuid=";
			originator += p->getProperty("uuid");
			ep->setProperty("originator", originator);
		}

		ep->addMetrics (toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt), t);

		try
		{
			scheduler_->fireEvent (e);
		}
		catch(toolbox::task::exception::Overflow & e )
		{
			this->incrementInternalLossCounter();
			
			std::stringstream  msg;
			msg << "Failed to dispatch monitor report for flashlist '" <<
			settings_->getFlashListDefinition()->getProperty("name") << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q , msg.str() , e);
			this->getOwnerApplication()->notifyQualified("warning",q);
		}
		catch(toolbox::task::exception::OverThreshold & e )
		{
			// report is lost, keep counting
			this->incrementInternalLossCounter();
		}
		catch(toolbox::task::exception::InternalError & e )
		{
			// dead band ignore
			this->incrementInternalLossCounter();
		
			std::stringstream  msg;
			msg << "Failed to dispatch monitor report for flashlist '" <<
			settings_->getFlashListDefinition()->getProperty("name") << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q , msg.str() , e);
			this->getOwnerApplication()->notifyQualified("fatal",q);		
		}			
	}
	
			
}

void xmas::probe::FlashListMonitor::incrementPulseCounter()
{
	monitorPulseCounter_++;
}

void xmas::probe::FlashListMonitor::incrementFireCounter()
{
	monitorFireCounter_++;
}

void xmas::probe::FlashListMonitor::incrementInternalLossCounter()
{
	monitorInternalLossCounter_++;
}

void xmas::probe::FlashListMonitor::incrementCommunicationLossCounter()
{
	monitorCommunicationLossCounter_++;
}

void xmas::probe::FlashListMonitor::incrementMemoryLossCounter()
{
	monitorMemoryLossCounter_++;
}

void xmas::probe::FlashListMonitor::incrementUnassignedLossCounter()
{
	monitorUnassignedLossCounter_++;
}

xdata::UnsignedInteger64T xmas::probe::FlashListMonitor::getFireCounter()
{
	return monitorFireCounter_;
}

xdata::UnsignedInteger64T xmas::probe::FlashListMonitor::getPulseCounter()
{
	return monitorPulseCounter_;
}

xdata::UnsignedInteger64T xmas::probe::FlashListMonitor::getInternalLossCounter()
{
	return monitorInternalLossCounter_;
}

xdata::UnsignedInteger64T xmas::probe::FlashListMonitor::getCommunicationLossCounter()
{
	return monitorCommunicationLossCounter_;
}

xdata::UnsignedInteger64T xmas::probe::FlashListMonitor::getMemoryLossCounter()
{
	return monitorMemoryLossCounter_;
}

xdata::UnsignedInteger64T xmas::probe::FlashListMonitor::getUnassignedLossCounter()
{
	return monitorUnassignedLossCounter_;
}

std::string xmas::probe::FlashListMonitor::getFlashListName()
{
	return settings_->getFlashListDefinition()->getProperty("name");
}
			
xmas::FlashListDefinition * xmas::probe::FlashListMonitor::getFlashListDefinition()
{
	return settings_->getFlashListDefinition();
}

xdata::Serializable * xmas::probe::FlashListMonitor::eval (const std::string& functionName, const std::string& type)
	
{

	if ( functionName.find("$") == 0 )
	{
		std::vector<std::string> expandedFile;
		try
		{
			expandedFile = toolbox::getRuntime()->expandPathName(functionName);
		} 
		catch (toolbox::exception::Exception& tbe)
		{
			return new xdata::String("");
		}

		if (expandedFile.size() != 1)
		{
			return new xdata::String("");
		}
		
		return new xdata::String(expandedFile[0]);
	}
	else if (functionName == "systime()")
	{
		return new xdata::TimeVal(toolbox::TimeVal::gettimeofday());
	}
	else if (functionName == "context()")
	{
		return new xdata::String(this->getOwnerApplication()->getApplicationDescriptor()->getContextDescriptor()->getURL());
	}
	else if (functionName == "uuid()")
	{
		toolbox::net::UUID id;
		return new xdata::String(id.toString());
	}
	else if (functionName == "session()")
	{
		return new xdata::String( this->getOwnerApplication()->getApplicationContext()->getSessionId());
	}
	else if (functionName == "zone()")
	{
		std::set<std::string> zones = this->getOwnerApplication()->getApplicationContext()->getZoneNames();
		return new xdata::String( toolbox::printTokenSet(zones,"," ));
	}
	else if(functionName == "hostname()")
	{
		return new xdata::String( toolbox::net::getHostName() );
	}
	else
	{
		std::stringstream msg;
		msg << "Function '" << functionName << "' unknown";
		XCEPT_RAISE (xmas::probe::exception::Exception, msg.str());
	}
}

/*
std::vector<xmas::probe::Publisher*> xmas::probe::FlashListMonitor::getPublishers
	(xmas::probe::Publisher::Type & type, const std::string& group)
{	
	std::vector<xmas::probe::Publisher*> v;
	std::list<toolbox::ActionListener*> samplers = this->getActionListeners();
	std::list<toolbox::ActionListener*>::iterator ai;
	for (ai = samplers.begin(); ai != samplers.end(); ++ai)
	{
		xmas::probe::Sampler* sampler = dynamic_cast<xmas::probe::Sampler*>(*ai);
		if (sampler->belongsToGroup(group))
		{
			v += sampler->getPublishers(type);
		}
	}
	return v;
}
*/
//std::cout << "Fire event with metrics..." << std::endl;
					/*
					std::cout << "--------------------- TABLE BEGIN ----------------" << std::endl;
					t->writeTo(std::cout);
					xdata::Serializable * inner = t->getValueAt(0,"jobTable");
					xdata::Table * innerT = dynamic_cast<xdata::Table *>(inner);
					if (innerT->getRowCount() > 0 )
					{
							innerT->writeTo(std::cout);
							
							
					}
					else
					{
						std::cout << "----- Inner table is empty ---" << std::endl;
					}
					std::cout << " denormilized" << std::endl;
					try
					{
						xdata::Table * dt = xdata::Table::denormalize(&(*t));	
						if ( dt != 0 )
						{				
							dt->writeTo(std::cout);
						}
						else
						{
							std::cout << " denormilized is EMPTY !!!! " << std::endl;	
						}
					}
					catch (xdata::exception::Exception & e)
					{
						std::cout << "error in deno:" << xcept::stdformat_exception_history(e) << std::endl;
					}	
					std::cout << "--------------------- TABLE END ----------------" << std::endl;
					*/




