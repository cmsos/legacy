<?xml version="1.0" encoding="UTF-8"?>

<node id="apve">

  <node id="instance_id" address="0x00000000">
    <node id="type" mask="0xffffff00" permission="r"
          description="Should be 'A' for 'APVE' (but the APVE design was made for VME, which has only 16 data bits)." />
    <node id="number" mask="0x000000ff" permission="r"
          description="Instance number, i.e. 1 to 4." />
  </node>

  <node id="config" address="0x00000000"
        description="APVE configuration registers.">
    <node id="apv_latency" address="0x00000003" mask="0x000000ff" permission="rw"
          description="The APV L1A latency in units of clock cycles." />
    <node id="apv_read_mode" address="0x00000003" mask="0x00000100" permission="rw"
          description="APV read-mode. 0: deconvolution mode, 1: peak mode." />
    <node id="apv_trigger_mode" address="0x00000003" mask="0x00000200" permission="rw"
          description="APV trigger mode. 0: deconvolution/peak-multi mode (3 samples per trigger). 1: peak-single mode (1 sample per trigger)." />

    <node id="apv_threshold_busy" address="0x00000004" mask="0x0000001f" permission="rw"
          description="APV buffer threshold (number of free buffers), measured in L1As, at which 'BUSY' is asserted (e.g., if set to 0 all APV buffers used, if set to 9 and in DECON mode then BUSY is asserted after single L1A)." />
    <node id="apv_threshold_warning" address="0x00000004" mask="0x000003e0" permission="rw"
          description="APV buffer threshold (number of free buffers), measured in L1As, at which 'WARNING' is asserted (e.g., if set to 0 all APV buffers used, if set to 9 and in DECON mode then WARNING is asserted after single L1A)." />

    <node id="apv_sim_select" address="0x00000004" mask="0x00000400" permission="rw"
          description="Source of available APV buffer space calculation. Keep set to 1 to use APV simulation, since no real APV exists in the TCDS." />

    <node id="apv_enable" address="0x00000008" mask="0x00000001" permission="rw"
          description="Overall APV enable flag. If set to 1, the APV simulation contributes to the TTS output state. If set to 0, the APV simulation is forced to 'READY'." />
    <node id="fmm_enable" address="0x00000008" mask="0x00000002" permission="rw"
          description="Overall FMM-input enable flag. If set to 0, the FMM input is forced to 'READY'. Keep set to 0, since the TCDS APVE does not have an FMM input." />

    <node id="l1a_offset" address="0x0000000f" mask="0x000000ff" permission="rw"
          description="?" />

    <node id="apsp_offset" address="0x00000010" mask="0x000000ff" permission="rw"
          description="?" />
  </node>

  <node id="debug_tts" address="0x00000008">
    <node id="force_tts_error" mask="0x00000004" permission="rw"
          description="Force flag for the simulated TCDS 'ERROR' state. Keep set to 0." />
    <node id="force_tts_oos" mask="0x00000008" permission="rw"
          description="Force flag for the simulated TCDS 'OOS' state. Keep set to 0." />
    <node id="force_tts_busy" mask="0x00000010" permission="rw"
          description="Force flag for the simulated TCDS 'BUSY' state. Keep set to 0." />
    <node id="force_tts_warning" mask="0x00000020" permission="rw"
          description="Force flag for the simulated TCDS 'WARNING' state. Keep set to 0." />
  </node>

  <node id="pipeline_offsets" address="0x0000000c">
    <node id="l1a" address="0x00000000" mask="0x0000ff00" permission="rw"
          description="APVE pipeline offset for L1As." />
    <node id="resync" address="0x00000000" mask="0x000000ff" permission="rw"
          description="APVE pipeline offset for Resync B-gos." />
    <node id="tcs_error" address="0x00000001" mask="0x000000ff" permission="rw"
          description="APVE pipeline offset for TCS errors." />
    <node id="bc0" address="0x00000001" mask="0x0000ff00" permission="rw"
          description="APVE pipeline offset for BC0 B-gos." />
    <node id="oc0" address="0x00000002" mask="0x000000ff" permission="rw"
          description="APVE pipeline offset for OC0 B-gos." />
    <node id="ec0" address="0x00000002" mask="0x0000ff00" permission="rw"
          description="APVE pipeline offset for EC0 B-gos." />
  </node>

  <node id="diagnostics" address="0x00000000">
    <node id="apv_oos_reason" address="0x00000006" permission="r"
          description="If the APV asserts 'OOS' this register explains why.">
      <node id="error_latch" mask="0x00000001" permission="r" />
      <node id="bc0_sync_loss" mask="0x00000002" permission="r" />
      <node id="software" mask="0x00000004" permission="r" />
      <node id="apv_i2c_configuration_change" mask="0x00000008" permission="r" />
      <node id="apv_configuration_change" mask="0x00000010" permission="r" />
      <node id="apv_buffer_overflow" mask="0x00000020" permission="r" />
      <node id="apv_reset_in_progress" mask="0x00000040" permission="r" />
      <node id="real_apv_error_bit" mask="0x00000080" permission="r"
            description="No real APV exists in the TCDS setup so this should be zero." />
      <node id="real_apv_tick_error" mask="0x00000100" permission="r"
            description="No real APV exists in the TCDS setup so this should be zero." />
    </node>
    <node id="apv_error_reason" address="0x00000007"
          description="If the APV asserts 'ERROR' this register explains why.">
      <node id="software" mask="0x00000001" permission="r" />
      <node id="apv_latency" mask="0x00000002" permission="r" />
      <node id="apv_busy_threshold_set_incorrectly" mask="0x00000004" permission="r" />
      <node id="apv_error_threshold_set_incorrectly" mask="0x00000008" permission="r" />
      <node id="no_tcs_source_enabled" mask="0x00000010" permission="r" />
      <node id="tcs_control_error" mask="0x00000020" permission="r" />
      <node id="tcs_clock_error" mask="0x00000040" permission="r" />
    </node>
  </node>

  <!-- TTS and status history. -->
  <node id="status_history" address="0x00000040"
        description="History of TTS and status changes. Each history entry contains 128 bits, split across 8 words with 16 bits each (in the 16 lower bits of the 32-bit IPbus word).">
    <node id="reset_read_pointer" address="0x00000000" mask="0xffffffff" permission="w"
          description="Any write to this register will reset the read pointer to the beginning." />
    <node id="data" address="0x00000001" mode="non-incremental" size="1024" permission="r"
          description="" />
    <node id="locked" address="0x00000002" mask="0x00000001" permission="rw"
          description="If asserted: no further entries are written to the history." />
  </node>

  <!-- Simulated pipeline history. -->
  <node id="apv_sim_history" address="0x000000c0"
        description="History of event numbers and the corresponding simulated APV pipeline addresses.">
    <node id="reset_read_pointer" address="0x00000000" mask="0xffffffff" permission="w"
          description="Any write to this register will reset the read pointer to the beginning." />
    <node id="data" address="0x00000001" mode="non-incremental" size="1024" permission="r"
          description="Bits [7:0]: APV pipeline address. bits [31:8]: event number." />
    <node id="locked" address="0x00000002" mask="0x00000001" permission="rw"
          description="If asserted: no further entries are written to the history." />
  </node>

  <!-- TTC statistics accounting. -->
  <node id="ttc_stats" address="0x00000080"
        description="TTC signal statistics. NOTE: These counters are all 64-bits. Each requires four successive reads to obtain the full data.">
    <node id="reset" address="0x00000000" mask="0x0000ffff" />
    <node id="latch" address="0x00000001" mask="0x0000ffff" />
    <node id="clk_count" address="0x00000000" mask="0x0000ffff" />
    <node id="bc0_count" address="0x00000001" mask="0x0000ffff" />
    <node id="l1a_count" address="0x00000002" mask="0x0000ffff" />
    <node id="resync_count" address="0x00000003" mask="0x0000ffff" />
    <node id="tcs_warn_count" address="0x00000006" mask="0x0000ffff" />
    <node id="tcs_busy_count" address="0x00000007" mask="0x0000ffff" />
    <node id="fmm_warn_count" address="0x00000008" mask="0x0000ffff" />
    <node id="fmm_busy_count" address="0x00000009" mask="0x0000ffff" />
    <node id="apv_warn_count" address="0x0000000a" mask="0x0000ffff" />
    <node id="apv_busy_count" address="0x0000000b" mask="0x0000ffff" />
  </node>

  <!-- High-rate noise trigger veto. -->
  <node id="trigger_veto_shift_register" address="0x000000011" permission="rw"
        description="Trigger-veto shift register (to avoid high-rate noise in the APVs). Write a 0x4 in here and then just use the trigger_veto_load_buffer for access." />
  <node id="trigger_veto_mask" address="0x000000200"
        description="Parallel-loading register for the trigger-veto shift register.">
    <node id="word0" address="0x00000000" mask="0x0000ffff" permission="rw" />
    <node id="word1" address="0x00000001" mask="0x0000ffff" permission="rw" />
    <node id="word2" address="0x00000002" mask="0x0000ffff" permission="rw" />
    <node id="word3" address="0x00000003" mask="0x0000ffff" permission="rw" />
    <node id="word4" address="0x00000004" mask="0x0000ffff" permission="rw" />
    <node id="word5" address="0x00000005" mask="0x0000ffff" permission="rw" />
    <node id="word6" address="0x00000006" mask="0x0000ffff" permission="rw" />
    <node id="word7" address="0x00000007" mask="0x0000ffff" permission="rw" />
    <node id="word8" address="0x00000008" mask="0x0000ffff" permission="rw" />
    <node id="word9" address="0x00000009" mask="0x0000ffff" permission="rw" />
    <node id="word10" address="0x0000000a" mask="0x0000ffff" permission="rw" />
    <node id="word11" address="0x0000000b" mask="0x0000ffff" permission="rw" />
    <node id="word12" address="0x0000000c" mask="0x0000ffff" permission="rw" />
    <node id="word13" address="0x0000000d" mask="0x0000ffff" permission="rw" />
    <node id="word14" address="0x0000000e" mask="0x0000ffff" permission="rw" />
    <node id="word15" address="0x0000000f" mask="0x0000ffff" permission="rw" />
    <node id="word16" address="0x00000010" mask="0x0000ffff" permission="rw" />
    <node id="word17" address="0x00000011" mask="0x00000ff" permission="rw" />
  </node>

</node>
