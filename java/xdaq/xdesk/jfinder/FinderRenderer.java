package xdaq.xdesk.jfinder;

import java.awt.Component;
import javax.swing.*;

public interface FinderRenderer
{
	public Component getFinderRendererComponent (JFinder finder,
												 Object value,
												 boolean isSelected,
												 boolean hasFocus,
												 JLabel where);
}
