#include "xdaq2rc/RcmsNotifierInstance.h"


void xdaq2rc::RcmsNotifierInstance::registerFields
(
  xdata::Bag<RcmsNotifierInstance> *bag
)
{
  bag->addField("classname", &classname);
  bag->addField("instance",  &instance);
  bag->addField("hltsgURI",  &hltsgURI);
}
