#ifndef __MdioInterface
#define __MdioInterface

#include "toolbox/BSem.h"
#include "hal/PCIDevice.hh"
#include "log4cplus/loggingmacros.h"


namespace ferol40
{
    class MdioInterface {
    public:
        MdioInterface( HAL::PCIDevice *ferol40Device, 
                       log4cplus::Logger logger );
        
        void mdiolock();
        void mdiounlock();
        
        void mdioReadBlock( uint32_t vitesseNo, uint32_t reg, char data[], 
                            uint32_t length, uint32_t device = 0x1E );
        
        void mdioRead( uint32_t vitesseNo, uint32_t reg, uint32_t *value_ptr, 
                       uint32_t device = 0x1E );
        void mdioWrite( uint32_t vitesseNo, uint32_t reg, uint32_t value, 
                        uint32_t device = 0x1E );
        
    private:
        toolbox::BSem mdioLock_;
        HAL::PCIDevice *ferol40Device_P; 
        log4cplus::Logger logger_;
    };
};

#endif /* __MdioInterface */
