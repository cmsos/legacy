// $Id

/*************************************************************************
 * XDAQApplication Template                                              *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

// This is a first attempt at rewriting the PLTSlinkSource to deal with the new data
// flow directly from the FEDStreamReader. In particular, it now does the job of decoding
// the raw data stream and parsing it into hit locations. However several more things need
// to be done before this is ready for production:
// 1) The data is not synchronized to the nibble clock. Rather, we accumulate hits until
// we get 500 (the current size of the data structure) and then send them out.
// 2) Error data is completely ignored. This needs to be published into the BRILDAQ data
// stream so it can be at least monitored.
// 3) The header/trailer data (timestamp, event number, BX) is also ignored. It would also
// be useful to have this available in the datastream.
//
// -- Paul Lujan, 4 April 2017

#include <sstream>
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/Method.h"

#include "bril/pltslinksource/Application.h"
#include "bril/pltslinksource/WebUtils.h"
#include "bril/pltslinksource/exception/Exception.h"
#include "interface/bril/PLTSlinkTopics.hh"

#include "xdata/Properties.h"

#include "b2in/nub/Method.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/TimeVal.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/task/TimerFactory.h"

#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"

#include <unistd.h>
#include "bril/pltslinksource/zmq.hpp"

XDAQ_INSTANTIATOR_IMPL (bril::pltslinksource::Application)

using namespace interface::bril;

bril::pltslinksource::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception)
: xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL)
{
  xgi::framework::deferredbind(this,this,&bril::pltslinksource::Application::Default, "Default");
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  m_appDescriptor= getApplicationDescriptor();
  m_classname    = m_appDescriptor->getClassName();
  m_instanceid   = 0;

  if( m_appDescriptor->hasInstanceNumber() ){
    m_instanceid = m_appDescriptor->getInstance();        
  }     
  //	std::string memPoolName = m_classname + m_instance.toString() + std::string("_memPool");

  toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
  toolbox::net::URN urn("toolbox-mem-pool",m_classname.toString()+"_"+m_instanceid.toString()+"_memPool");
  try{
    LOG4CPLUS_INFO(getApplicationLogger(), "Creating Memory Pool");
     m_memPool = m_poolFactory->createPool(urn,allocator);
  }catch(xcept::Exception & e){
     XCEPT_RETHROW(xdaq::exception::Exception, "Could not create Memory Pool", e);
  }
  try{
    // Listen to app infospace
    getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
    getApplicationInfoSpace()->fireItemAvailable("topics",&m_topics);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    m_appURN.fromString(m_appDescriptor->getURN());
  }catch( xcept::Exception & e){
    XCEPT_RETHROW(xdaq::exception::Exception, "Failed to put parameters into info spaces", e);
  }

  try{
    
    std::string nid("Pltslinksource_mon");
    
    std::string monurnPull = createQualifiedInfoSpace(nid).toString();
    m_monInfoSpacePull = dynamic_cast<xdata::InfoSpace* >(xdata::getInfoSpaceFactory()->find(monurnPull));
    m_monInfoSpacePull->fireItemAvailable("className", &m_classname);
    m_monItemListPull.push_back("className");
    m_monInfoSpacePull->fireItemAvailable("appURN", &m_appURN);
    m_monItemListPull.push_back("appURN");
	  
    m_monInfoSpacePull->addGroupRetrieveListener(this);
	  
	  
  }catch(xdata::exception::Exception& e){
    std::stringstream msg;
    msg<<"Failed to fire monitoring items";
    LOG4CPLUS_ERROR(getApplicationLogger(), stdformat_exception_history(e));
    XCEPT_RETHROW(bril::pltslinksource::exception::Exception,msg.str(),e);
  }catch(xdaq::exception::Exception& e){
    std::stringstream msg;
    msg<<"Failed to create infospace";
    LOG4CPLUS_ERROR(getApplicationLogger(), msg.str());
    XCEPT_RETHROW(bril::pltslinksource::exception::Exception, stdformat_exception_history(e),e);
  }catch(std::exception &e){
    LOG4CPLUS_ERROR(getApplicationLogger(), "std error "+std::string(e.what()));
    XCEPT_RAISE(bril::pltslinksource::exception::Exception, e.what());
  }
  
}

bril::pltslinksource::Application::~Application ()
{  
}

void bril::pltslinksource::Application::actionPerformed(xdata::Event& e){
  if( e.type() == "urn:xdaq-event:setDefaultValues"){           
    //count_=0;

    std::string timername(m_appURN.toString()+"_zmqstarter");
    try{
      toolbox::TimeVal zmqStart(2,0);
      toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername);
      //      toolbox::TimeVal start =toolbox::TimeVal::gettimeofday();
      timer->schedule(this, zmqStart, 0, timername);
    }catch(toolbox::exception::Exception& e){
      std::stringstream msg;
      msg << "failed to start timer" << timername;
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::pltslinksource::exception::Exception,msg.str(),e);
    }
    
    
    if(!m_topics.value_.empty()){
      m_topiclist = toolbox::parseTokenSet(m_topics.value_,",");
    }
  }
}

void bril::pltslinksource::Application::actionPerformed(toolbox::Event& e){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());
  if ( e.type() == "eventing::api::BusReadyToPublish" ){
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    std::stringstream msg;
    msg<< "Eventing bus '" << busname << "' is ready to publish";
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
  }
}

/*
 * This is the default web page for this XDAQ application.
 */
void bril::pltslinksource::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception)
{
  *out << "XDAQ PLT Slink source" << std::endl;
}

void bril::pltslinksource::Application::do_zmqclient(){
  int nhits = 0;

  zmq::context_t zmq_context(1);
  zmq::socket_t zmq_socket(zmq_context, ZMQ_SUB);

  int zmq_port = 7776;

  char address[1000];
  sprintf(address, "tcp://pltslink.cms:%d", zmq_port);

  LOG4CPLUS_INFO(getApplicationLogger(), "Connecting to zmq publisher at " << address);
  zmq_socket.connect(address);
  zmq_socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);

  //  do_stoptimer();

  const int inputBufferSize = 1024;
  uint32_t inputBuffer[inputBufferSize];
  const int outputBufferCols = 5;   // pieces of data for each hit
  const int outputBufferRows = 500; // number of hits stored in the buffer
  uint32_t outputBuffer[outputBufferCols*outputBufferRows+1] = {0};

  int nevents = 0;

  while (1) {
    // Receive message.
    int eventSize = zmq_socket.recv((void*)inputBuffer, sizeof(inputBuffer)*sizeof(uint32_t), 0);
    int nwords = eventSize/sizeof(uint32_t);
    LOG4CPLUS_TRACE(getApplicationLogger(), "Received a zmq message with event size " << nwords  << " words");
    
    if (nwords % 2 != 0) {
      LOG4CPLUS_WARN(getApplicationLogger(), "Incoming zmq Message is not of even size (" << nwords << "words)");
    }
    
    bool inEvent = false;
    // Process the message contents.
    for (int i=0; i<nwords; i+=2) {
      
      // Check for a TDC buffer. I don't think I've ever actually seen these in the datastream
      // but might as well be safe.
      if ((inputBuffer[i] == 0x53333333) && (inputBuffer[i+1] == 0x53333333)) {
	i+=2;
	while ((inputBuffer[i] & 0xf0000000) != 0xa0000000 && i<nwords) {
	  i++;
	}
	if (i%2 == 1) i++;
	if (i>=nwords) break;
      }

      // Check for header.
      if (inEvent == false && (inputBuffer[i] & 0xff) == 0 && (inputBuffer[i+1] & 0xff000000) == 0x50000000) {
	if (i!=0) LOG4CPLUS_WARN(getApplicationLogger(), "Found header at unexpected location (" << i << ")");

	inEvent = true;

	// These variables will be useful for later although we're not using them now.
	int eventNumber = inputBuffer[i+1] & 0xffffff;
        int bx = ((inputBuffer[i] & 0xfff00000) >> 20);
        int fedID = ((inputBuffer[i] & 0xfff00) >> 8);
	
	LOG4CPLUS_TRACE(getApplicationLogger(), "Found header with event number " << eventNumber << " FED ID " << fedID << " and BX " << bx);
      }
      // Check for trailer.
      else if ((inputBuffer[i+1] & 0xff000000) == 0xa0000000) {
	inEvent = false;
	
	uint32_t eventTime = inputBuffer[i];
	LOG4CPLUS_TRACE(getApplicationLogger(), "Found trailer with event time " << eventTime);
      }
      // Otherwise this is hit (or error) data.
      else {
	for (int j=0; j<2; ++j) {
	  int word = inputBuffer[i+j];
	  if (word==0) continue;

	  const uint32_t unsigned plsmask = 0xff;
	  const uint32_t unsigned pxlmask = 0xff00;
	  const uint32_t unsigned dclmask = 0x1f0000;
	  const uint32_t unsigned rocmask = 0x3e00000;
	  const uint32_t unsigned chnlmask = 0xfc000000;
	  uint32_t unsigned chan = ((word & chnlmask) >> 26);
	  uint32_t unsigned roc  = ((word & rocmask)  >> 21);
	  uint32_t rawPixel = ((word & pxlmask) >> 8);
	  
	  // Check for embeded special words: roc > 25 is special, not a hit
	  // Some of these (gap/dummy) are unimportant but some are errors that should
	  // be propagated.
	  if (roc > 25) {
	    if ((word & 0xffffffff) == 0xffffffff) {
	      // unknown error
	    } else if (roc == 26) {
	      // gap word -- used to pad to even length, can be discarded
	    } else if (roc == 27) {
	      // dummy word -- used to pad individual FIFOs, can be discarded
	    } else if (roc == 28) {
	      // FIFO near full. Harmless if there's just one, but probably we should do
	      // something if we're getting consistent errors here.
	    } else if (roc == 29) {
	      // Timeout error. This needs more code (not currently included) in order to
	      // actually decode it. Again like the above if we just have a few errors this
	      // is fine, but if we get consistent errors we need to take action.
	      // This *should* be caught by the TBM auto-recovery if it happens, but
	      // more redundancy is good.
	    } else if (roc == 30) {
	      // Trailer error. This may be something harmless (e.g. the ROC reset, which
	      // happens periodically) or something more serious.
	      // The code for decoding this more specifically is in TestBinaryFileReader.cc
	    } else if (roc == 31) {
	      // Event number error. This often indicates desynchronization and should be addressed
	      // quickly.
	    }
	  } else if (chan > 0 && chan < 37) {
	    // This is actually a hit. Decode the hit location.
	    int mycol;
	    if ( ((word & pxlmask) >> 8) % 2) {
	      // Odd
	      mycol = ((word & dclmask) >> 16) * 2 + 1;
	    } else {
	      // Even
	      mycol = ((word & dclmask) >> 16) * 2;
	    }
	    roc -= 1; // The fed gives 123, and we use the convention 012
	    if (roc > 2 || roc < 0) {
	      // Unexpected ROC number. Skip this for now.
	    } else {
	      // Finally we got a good hit! Decode the pixel number into row number
	      int myrow = 0;
	      if (rawPixel < 160) {
		myrow = abs(rawPixel % 2 == 1 ? 80 - (rawPixel - 1) / 2 : (rawPixel) / 2 - 80);
	      }

	      // Store this in the message. Note that the event size is in outputBuffer[0], although it's kind of
	      // superfluous at the moment.
	      LOG4CPLUS_TRACE(getApplicationLogger(), "Hit found with channel " << chan << " ROC " << roc << " col " << mycol << " row " << myrow << " ADC " << (word & plsmask));
	      outputBuffer[nhits + 1] = myrow;
	      outputBuffer[outputBufferRows + nhits + 1] = mycol;
	      outputBuffer[outputBufferRows*2 + nhits + 1] = chan;
	      outputBuffer[outputBufferRows*3 + nhits + 1] = roc;
	      outputBuffer[outputBufferRows*4 + nhits + 1] = word & plsmask;
	      nhits++;

	      // Finally, if our buffer is full, send the message off
	      if (nhits == outputBufferRows) {
		outputBuffer[0] = nhits;
		LOG4CPLUS_DEBUG(getApplicationLogger(), "Publishing " << nhits << " hits");
		publishData(pltslinkhistT::topicname(), 0, 0, 0, 0, 0, 0, 0, &outputBuffer, 2501*sizeof(uint32_t));
		nhits=0;
	      }
	    } // good hit found
	  } else {
	    // Bad channel number. Probably safe just to ignore this word in this case.
	  }
	} // loop over the two hits within each pair
      } // hit/error data found
    } // loop over incoming message contents
    nevents++;
    if (nevents % 10000 == 0) {
      LOG4CPLUS_INFO(getApplicationLogger(), "Processed " << nevents << " events");
    }

  } // get incoming messages
}


void bril::pltslinksource::Application::publishData(const std::string&topic, uint32_t fill,uint32_t run, uint32_t ls,uint32_t nibble,uint32_t timeorbit, uint32_t orbit, uint32_t channel, void* payloadPtr, size_t payloadsize){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Publishing");
  toolbox::mem::Reference* bufRef = 0;
  try{      
    size_t totsize = pltslinkhistT::maxsize();
    bufRef = m_poolFactory->getFrame(m_memPool,totsize);
    bufRef->setDataSize(totsize);
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION", DATA_VERSION);
    DatumHead* header = (DatumHead*)(bufRef->getDataLocation());
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    header->setTime(fill,run,ls,nibble,t.sec(),t.millisec());
    header->setResource(DataSource::PLT,0,channel,StorageType::UINT32);
    header->setTotalsize(totsize);
    header->setFrequency(1);   
    memcpy(header->payloadanchor,payloadPtr,payloadsize);
    this->getEventingBus(m_bus.value_).publish(pltslinkhistT::topicname(), bufRef, plist);
  }catch (eventing::api::exception::Exception & e){
    // we are not doing any error handling in these examples. We just log the error
    LOG4CPLUS_ERROR(this->getApplicationLogger(), "Failed to publish, release message buffer");
    if(bufRef){
      bufRef->release();
      bufRef = 0;
    }
  }

}

 
void bril::pltslinksource::Application::timeExpired(toolbox::task::TimerEvent& e){
  LOG4CPLUS_INFO(getApplicationLogger(), "Beginning zmq listener");
  do_zmqclient();
}



bool bril::pltslinksource::Application::reading(toolbox::task::WorkLoop* wl){
//  std::string topicname=m_classname.toString()+"Topic";
   return false;
}
