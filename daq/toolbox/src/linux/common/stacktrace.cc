// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, J. Hegeman, L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include <toolbox/stacktrace.h>
#include <iostream>
#include <execinfo.h> // Linux header file
#include <cstdlib>
#include <cxxabi.h>
#include <iomanip>
#include <iostream>
//
void toolbox::stacktrace(int depth, std::ostream& stream)
{
  void** dump = 0;
  char** list = 0;

  try
    {
      dump = new void*[depth + 1];

      if(!dump)
        {
          stream << "No memory available for stack trace" << std::endl;
          return;
        }

      int size = ::backtrace(dump, depth);
      list = ::backtrace_symbols(dump, size);

      if(list)
        {
          // Loop over the whole backtrace entry by entry.
          for (int i = 0; i < size; ++i)
            {
              char* begin_name = 0;
              char* begin_offset = 0;
              char* end_offset = 0;

              // The list of backtrace symbols will contain fairly cryptic entries like
              // /lib64/libc.so.6(__libc_start_main+0xf5) [0x7fd11f7fbc05].
              // Let's try to find out where the mangled name is in
              // the entry and demangle that.
              for (char* p = list[i]; *p; ++p)
                {
                  if (*p == '(')
                    {
                      begin_name = p;
                    }
                  else if (*p == '+')
                    {
                      begin_offset = p;
                    }
                  else if ((*p == ')')
                           && (begin_offset || begin_name))
                    {
                      end_offset = p;
                    }
                }

              if (begin_name
                  && end_offset
                  && (begin_name < end_offset))
                {
                  *begin_name++ = '\0';
                  *end_offset++ = '\0';
                  if (begin_offset)
                    {
                      *begin_offset++ = '\0';
                    }

                  int status = 0;
                  size_t funcnamesize = 1024;
                  char funcname[1024];
                  char* ret = abi::__cxa_demangle(begin_name, funcname, &funcnamesize, &status);
                  char* fname = begin_name;
                  if (status == 0)
                    {
                      fname = ret;
                    }

                  if (begin_offset)
                    {
                      stream << std::left
                             << std::setw(30) << list[i]
                             << " (" << fname
                             << " + " << begin_offset
                             << ") "
                             << end_offset
                             << std::endl;
                    } else {
                      stream << std::left
                             << std::setw(30) << list[i]
                             << " (" << fname
                             << ") "
                             << end_offset
                             << std::endl;
                  }
                }
              else
                {
                  // Somehow that did not work... Let's just print the
                  // original line then.
                  stream << list[i] << std::endl;
                }
            }
          free(list);
        }
      else
        {
          stream << "No symbols available in stack trace. Corrupted?" << std::endl;
        }

      delete[] dump;
    }
  catch(...)
    {
      free(list);
      delete[] dump;
      stream << "Stack trace failed" << std::endl;
    }
}
