// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/
#include "toolbox/AllocPolicy.h"

std::string toolbox::AllocPolicy::getType()
{
	return "alloc";
}

toolbox::AllocPolicy::~AllocPolicy()
{

}

void toolbox::AllocPolicy::setDefaultPolicy()
{
}

