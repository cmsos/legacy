#ifndef _tcds_pm_PMInfoSpaceHandler_h_
#define _tcds_pm_PMInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class PMInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      PMInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                         std::string const& name,
                         tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PMInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_PMInfoSpaceHandler_h_
