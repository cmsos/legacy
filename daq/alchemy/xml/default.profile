<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xj="http://xdaq.web.cern.ch/xdaq/xsd/2017/XMLJS10"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application class="executive::Application" instance="0" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>
	

	<xp:Application class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" service="ptfifo"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>

	
	<!-- XRelay -->
	<!-- xp:Application class="xrelay::Application" id="4"  service="xrelay" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libxrelay.so</xp:Module -->
	
	<!-- HyperDAQ -->
	<xp:Application class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	
	
	<!-- xj:Endpoint protocol="http" service="cgi" hostname="xdaqdev1.cms" port="1911"/>
	<xj:Endpoint protocol="http" service="restful" hostname="xdaqdev1.cms" port="1914"/ -->
	
	
	<xj:Application class="Server" id="101" group="profile" service="server">
	 <properties xmlns="urn:js-application:Server" xsi:type="soapenc:Struct">
            	<aliasName xsi:type="xsd:string">api</aliasName>
            	<aliasPath xsi:type="xsd:string">/api</aliasPath>
      </properties>
	</xj:Application>
	
	<xj:Module>/tmp/devel/trunk/daq/alchemy/server.js</xj:Module>
	
	
	
</xp:Profile>
