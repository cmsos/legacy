#include "tcds/hwlayer/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"

#include "tcds/exception/version.h"

GETPACKAGEINFO(tcdshwlayer)

void
tcdshwlayer::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);

  CHECKDEPENDENCY(tcdsexception);
}

std::set<std::string, std::less<std::string> >
tcdshwlayer::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);

  ADDDEPENDENCY(dependencies, tcdsexception);

  return dependencies;
}
