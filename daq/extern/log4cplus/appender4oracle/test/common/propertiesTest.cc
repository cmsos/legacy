
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <appender4oracle/OracleAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>

using namespace std;
using namespace log4cplus;
using namespace log4cplus::helpers;


using namespace log4cplus;

int main(int argc, char**argv)
{
	if (argc < 3)
	{
		cout << argv[0] << " propertiesFile loopCount" << endl;
		return 1;
	}

	std::string properties = argv[1];
	unsigned int loopCount = atoi(argv[2]);

	try 
	{
		helpers::LogLog::getLogLog()->setInternalDebugging(true);
		log4cplus::registerOracleAppenderFactory();
        PropertyConfigurator::doConfigure(properties);
	}
    catch(...) 
	{
        cout << "Configuration error." << endl;
        return 1;
    }

    Logger root = Logger::getRoot();
    Logger test = Logger::getInstance("test");
    Logger subTest = Logger::getInstance("test.subtest");
	getNDC().push("<sid>12345</sid>");

	cout << "Start measuring " << loopCount << " log message insertions." << endl;

	time_t start = time(0);

	for (unsigned long i = 0; i < loopCount; i++)
	{
		LOG4CPLUS_INFO(subTest, "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890");
	}
	
	time_t end = time(0);
	
	cout << "Stopped measuring " << loopCount << " log message insertions." << endl;
	
	unsigned long duration = end-start;
	if (duration < 1)
	{
		cout << "Cannot calculate insertions per second. Test run too short." << endl;
	} 
	else
	{
		cout << "Test time: " << duration << " seconds." << endl;
		cout << "Insertions per second: " << (double) loopCount/ (double) duration << endl;
	}

    return 0;
}


