package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.TclScriptWindow;

public class TclScriptAction extends AbstractAction
{
	xdaqWin win_;

	public TclScriptAction (xdaqWin frame)
	{
		super ("Script...");
		win_ = frame;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		TclScriptWindow w = new TclScriptWindow ( win_ );
	}
}
