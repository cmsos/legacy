###############################################################################

# The addresses of the boards/applications to use in this test.
CPM = "cpm"
LPM = "lpm-4"
ICIS = ['ici-41']
APVES = ['apve-41']
PIS = ['pi-41']

# The FED id of the CPM/LPM, so we can disable the readout.
FED_ID_CPM = 1024
# FED_ID_LPM = 1029
FED_ID_LPM = 0

###############################################################################
