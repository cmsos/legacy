#ifndef _tcds_phasemon_ConfigurationInfoSpaceHandler_h_
#define _tcds_phasemon_ConfigurationInfoSpaceHandler_h_

#include <string>

#include "tcds/hwutilstca/ConfigurationInfoSpaceHandlerTCA.h"
#include "tcds/phasemon/Definitions.h"
#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace phasemon {

    class ConfigurationInfoSpaceHandler :
      public tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA
    {

    public:
      ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp);

      bool isInputEnabled(tcds::definitions::PHASEMON_INPUT const input) const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_ConfigurationInfoSpaceHandler_h_
