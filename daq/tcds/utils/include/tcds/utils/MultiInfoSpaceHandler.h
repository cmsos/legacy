#ifndef _tcds_utils_MultiInfoSpaceHandler_h_
#define _tcds_utils_MultiInfoSpaceHandler_h_

#include <string>
#include <map>

#include "tcds/utils/InfoSpaceHandlerIF.h"
#include "tcds/utils/Uncopyable.h"
#include "tcds/utils/XDAQObject.h"

namespace log4cplus {
  class Logger;
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {

    class InfoSpaceHandler;
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;

    class MultiInfoSpaceHandler :
      public InfoSpaceHandlerIF,
      public XDAQObject,
      private Uncopyable
      {

      public:
        virtual ~MultiInfoSpaceHandler();

        virtual void registerItemSets(Monitor& monitor, WebServer& webServer);

        void writeInfoSpace(bool const force=false);

      protected:
        MultiInfoSpaceHandler(xdaq::Application& xdaqApp,
                              /* std::string const& name, */
                              InfoSpaceUpdater* const updater=0);

        /* virtual void registerItemSetsWithMonitor(Monitor& monitor) = 0; */
        virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
        virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                   tcds::utils::Monitor& monitor,
                                                   std::string const& forceTabName="");

        log4cplus::Logger& logger_;

        // A map to keep track of the multitude of InfoSpaceHandlers.
        std::map<std::string, tcds::utils::InfoSpaceHandler*> infoSpaceHandlers_;

      private:
        /* std::string name_; */
        /* InfoSpaceUpdater* updaterP_; */

      };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_MultiInfoSpaceHandler_h_
