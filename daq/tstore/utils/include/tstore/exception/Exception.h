// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_exception_Exception_h_
#define _tstore_exception_Exception_h_

#include "xcept/Exception.h"

//this macro is temporary, until the one in xcept/Exception.h is fixed
#define DEFINE_EXCEPTION2(NAMESPACE1, EXCEPTION_NAME) \
namespace NAMESPACE1 { \
namespace exception { \
class EXCEPTION_NAME: public xcept::Exception \
{\
	public: \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
		xcept::Exception(name, message, module, line, function) \
	{} \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function, \
		xcept::Exception& e ): \
			xcept::Exception(name, message, module, line, function, e) \
	{} \
}; \
} \
}

DEFINE_EXCEPTION2(tstore, InvalidTimeoutException);
DEFINE_EXCEPTION2(tstore, Exception);

#endif
