package xdaq.tools.dstorebrowser;

import javax.swing.event.*;
import javax.swing.tree.*;
import xdaq.tools.win.*;
import org.w3c.dom.*;

public class dstoreTreeWillExpandListener implements TreeExpansionListener
{
    dstorebrowser mainWindow;

    public dstoreTreeWillExpandListener(dstorebrowser mainWindow_)
	{
	    mainWindow = mainWindow_;
	}

    public void treeExpanded(TreeExpansionEvent tEE) //throws ExpandVetoException 
	{
	    String constraints = "", attrName, separator="";
	    DefaultMutableTreeNode nodeToExpand = (DefaultMutableTreeNode) tEE.getPath().getLastPathComponent();
	    xdaqWinTreeAdapterNode node = (xdaqWinTreeAdapterNode)(nodeToExpand.getUserObject());
	    if (node == null) 
	    {
		System.out.println ("xdaqWinTree adapter node is null in treeWillExpand");
	    } else {
		String type = node.getAttribute("_type");
		if (! type.equals("Expandable")) return;
		NamedNodeMap attributes = node.domNode.getAttributes();
		for (int j = 0; j < attributes.getLength(); j++) {
		    attrName = attributes.item(j).getNodeName();
		    if (! attrName.startsWith("_")) 
		    {
			constraints = constraints + separator + attrName + "=" + attributes.item(j).getNodeValue();
			separator = "&";
		    }
		}
		mainWindow.requestData(nodeToExpand,constraints);
	    }
	}

    public void treeCollapsed(TreeExpansionEvent tEE)
	{
	}
}
