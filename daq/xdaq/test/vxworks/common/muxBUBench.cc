#include "strstream.h"
#include "MuxSingleCasterDispatcher.h"
#include "SensSingleCasterDispatcherTask.h"
#include "XRCAdapter.h"
#include "XRUAdapter.h"
#include "XRC.h"
#include "XIO.h"
#include "vxioslib.h"
#include "vxTiming.h"
#include "MuxXIOChannel.h"
#include "TcpDispatcherAcceptorTaskS.h"
#include "TcpXIOConnectorS.h"
#include "msgQLib.h"

vxChron chrono;	
int build_;

static int loopCounter = 0;

inline void Benchmarks2(int size)
{
	
	
	if ( loopCounter == 1000 )
	{
		chrono.start(0);
	}
	else
	if ( loopCounter == 101000 )
	{
		loopCounter = 0;
		chrono.stop(0);
                printf("Delta Time : %d usecs \n", chrono.dusecs());
		printf("Rate: %f Hz  \n", (float)100000/(float)(chrono.dusecs()/(float)1000000));
		printf("Bandwitdth: %f Mb\n",
		(((float)100000/(float)(chrono.dusecs()/(float)1000000)) * (float)size)/(float)0x100000);
		printf("Time to process one Event: %f usecs\n", (float)(chrono.dusecs()/(float)100000));
	}
	loopCounter++;
}

class Bench: public XRCAdapter, public XRUAdapter {

	public:

	Bench(XRC * xrc,XRU * xru1, XRU * xru2) {
		xrc_ = xrc;
		xru1_ = xru1;
		xru2_ = xru2;
		count_ = 0;
		msgq_ = msgQCreate (1024, sizeof(unsigned long), MSG_Q_FIFO);
		for (unsigned long i=0; i< 1024; i++ ) {
			msgQSend (msgq_, (char*)i, sizeof(unsigned long), WAIT_FOREVER, MSG_PRI_NORMAL);
		}
		build_ = 2;
	}
	
	void readout (EventIdentifier* eid) {
		//::logMsg("start(char * str)\n", 0, 0, 0, 0, 0, 0);
		//sleep(2);
		count_++;
		if ( count_ > 100000 ) {
			::logMsg("Round trip 100000\n", 0, 0, 0, 0, 0, 0);
			count_ = 0;
		}
		//Benchmarks2(1024);
		//msgQSend (msgq_, (char*)eid, sizeof(unsigned long), WAIT_FOREVER, MSG_PRI_NORMAL);
		build_--;
		if ( build_ == 0 ) {
			xru1_->readout(eid);
			xru2_->readout(eid);
			build_ = 2;
		}
	
	}
	
	int svc() {
		EventIdentifier eid;

		for(;;) {
			msgQReceive (msgq_, (char*)&eid, sizeof(unsigned long) , WAIT_FOREVER);	
			xru1_->readout(&eid);
			xru2_->readout(&eid);
		}
		
	
	}
	XRC * xrc_;
	XRU * xru1_;
	XRU * xru2_;
	int count_;
	MSG_Q_ID msgq_;
	
};
extern "C" int Main(char * ifname, char * dest1, char *dest2)
{
	// Preapare Mux Output channel
	MuxXIOChannel muxio1(ifname,0,0x1921,dest1); 
	MuxXIOChannel muxio2(ifname,0,0x1922,dest2); 
	// Prepare interfaces on channel
	XRC xrc(&muxio1);
	XRU xru1(&muxio1);
	XRU xru2(&muxio2);
	
	// Prepare MUX Dispatcher and Listener
	MuxSingleCasterDispatcher t(ifname,0,"dllInput",0x1923 );
	//SensSingleCasterDispatcherTask t(ifname,0,"dllInput",0x1922 );
	Bench bench(&xrc,&xru1,&xru2);
	t.setListener(&bench);
	//t.activate(); // for Sens only
	//Start Point
	char str[256];
	cout << "press ENTER to start" << endl;
	cin >> str;
	EventIdentifier eid = 0x0;
	for (int i=0; i< 2; i++ ) {
		build_ = 2;
		xru1.readout(&eid);
		xru2.readout(&eid);
	}
	//bench.svc();
	
	cout << "press ENTER to stop" << endl;
	cin >> str;
	//pause();
	
}

//Single trip
extern "C" int Main2(char * ifname, char * dest)
{
	// Preapare Mux Output channel
	//MuxXIOChannel muxio(ifname,0,0x1921,dest); 
	// Prepare interfaces on channel
	TcpXIOConnectorS  tcpio("137.138.96.213",20222); 
	tcpio.connect();
	//XRC xrc(&muxio);
	XRC xrc(tcpio.getXIO());

	char str[256];
	int count = 0;
	for (int i=0; i < 1000000; i++) {
		
		if (count++ > 1000000) {
			
			count = 0;
			
			cout << "press ENTER to send" << endl;
			cin >> str;
		}
		
		//
		//cout << "press ENTER to send" << endl;
		//cin >> str;
		//if (strcmp(str,"q") == 0 ) break;
		xrc.stop("BU");	
	}
	
	
	
}


