#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryEnddr.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandEnddr.h"

#include "jal/jtagChain/JTAGState.h"

using namespace std;

jal::JTAGSVFCommand* jal::JTAGSVFCommandFactoryEnddr::create(vector<string> const& args)
  throw (jal::SVFSyntaxException) {

  if (args.size() != 2) 
    XCEPT_RAISE(jal::SVFSyntaxException, "Enddr command needs exactly one parameter.");
  
  jal::JTAGState state( args[1] );

  if ( !state.isStable() )
    XCEPT_RAISE(jal::SVFSyntaxException, "Enddr needs stable state as parameter.");
  
  return (jal::JTAGSVFCommand *) new jal::JTAGSVFCommandEnddr( state );
}


