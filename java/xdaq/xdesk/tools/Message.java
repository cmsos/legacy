package xdaq.xdesk.tools;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.soap.*;

import org.apache.log4j.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class Message {
  Logger logger = Logger.getLogger(Message.class);

  String funcName_; // for debugging reasons only
  URL url_;
  SOAPMessage msg_;
  String status_;
  String transaction_;
  Object source_;
  String nameSpace_ = null;
  String nameSpaceUrl_ = null;
  SOAPBodyElement bodyElement_;

  // Temporarily remember dom document
  // until better JAXP is used.
  Document doc_;

  public Message(String funcName, URL url, Object source) {
    funcName_ = funcName;
    msg_ = null;
    status_ = null;
    transaction_ = null;
    source_ = source;
    url_ = url;

    bodyElement_ = null;
    msg_ = this.createSOAPMessage(funcName, url);
    doc_ = this.getDOM();
  }

  // Get the commands name
  public String getFunction() {
    return funcName_;
  }

  // Get the commands destination URL
  // URL can be a standard URL or XDAQ specific:
  //
  // The thing after the & is the optional originator address to be used
  // only when sending to XDAQ executives.
  //
  // http://hostname:port/classname-instance[&classname-number]#function
  // or
  // http://hostname:port/tid-number[&tid-number]#function
  //
  // This configures xdaq HOST number 2
  // e.g: http://lxplus:40000/xdaq-2#Configure
  //
  // This Enables RU application 3
  // e.g. http://lxplus:40000/ru-3#Enable
  //
  public URL getUrl() {
    return url_;
  }

  public void setSOAP(SOAPMessage msg) {
    msg_ = msg;
    doc_ = this.getDOM();
  }

  // Get SOAPMessage from command
  public SOAPMessage getSOAP() {
    return msg_;
  }

  // Set the status of the command, e.g.: "error" or the exception that was raised
  public void setStatus(String status) {
    status_ = status;
  }

  // Get the Status of the message/reply. Null means o.k.
  public String getStatus() {
    return status_;
  }

  // Provide a transaction identifier that will be returned
  // in the associated reply
  public void setTransaction(String transaction) {
    transaction_ = transaction;
  }

  public String getTransaction() {
    return transaction_;
  }

  public Object getSource() {
    return source_;
  }

  // ---------------------------------------------------------------------
  // Internal functions for creating SOAPMessage with DOM
  // ---------------------------------------------------------------------

  protected SOAPMessage createSOAPMessage(String funcName, URL url) {
    nameSpace_ = new String("");
    nameSpaceUrl_ = new String("");

    SOAPMessage msg = null;

    try {
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // Java 1.4 workaround for missing class loading facility when factory is used
      //
      //JarClassLoader loader = new JarClassLoader ( "file:/project/xdaq/develop/TriDAS/java/extern/saaj-impl.jar" );
      //Class c = loader.loadClass ("com.sun.xml.messaging.saaj.soap.ver1_1.SOAPMessageFactory1_1Impl");
      //ClassLoader classLoader = getClass().getClassLoader();
      //Thread.currentThread().setContextClassLoader(loader);
      //
      // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      javax.xml.soap.MessageFactory mf = javax.xml.soap.MessageFactory.
          newInstance();
      msg = mf.createMessage();

      SOAPPart sp = msg.getSOAPPart();
      SOAPEnvelope envelope = sp.getEnvelope();
      SOAPHeader header = envelope.getHeader();
      SOAPBody body = envelope.getBody();

      javax.xml.soap.Name commandName = envelope.createName(funcName,
          nameSpace_, nameSpaceUrl_);
      bodyElement_ = body.addBodyElement(commandName);

      // Depending on sending the message to a host or an application, xdaq
      // needs still customization here: host -> use 'id' as attribute.
      // application -> use 'targetAddr' as attribute

      // Extract targetAddr from URL: http://host:port/targetAddr-NUMBER#FUNCTION
      //
      String fileStr = url.getFile();
      javax.xml.soap.Name targetAddrName = null;

      String targetAddr = null;

      if (fileStr.startsWith("/xdaq-")) {
        // Temporary cuatom targetAddr and id
        // parameters needed for host configuration
        // messages:
        // id=hostID targetAddr=0
        //

        targetAddrName = envelope.createName("id");

        javax.xml.soap.Name idName = envelope.createName("targetAddr");
        bodyElement_.addAttribute(idName, "0");

      }
      else {
        targetAddrName = envelope.createName("targetAddr");
      }

      // If "targetAddr-" is not found, it should be considered and ordinary, simple
      // SOAP request. We will foresee compatibility later on.
      //
      int indexMinus = fileStr.indexOf("-");

      targetAddr = fileStr.substring(indexMinus + 1);
      bodyElement_.addAttribute(targetAddrName, targetAddr);
    }
    catch (Exception e) {
      logger.error(
          "ETree:Message.java:createSOAPMessage : Cannot create SOAP message: " +
          e.toString());
    }

    return msg;
  }

  protected void addParameter(String name, String type, String value) {
    try {
      SOAPPart sp = msg_.getSOAPPart();
      SOAPEnvelope envelope = sp.getEnvelope();

      SOAPElement element = bodyElement_.addChildElement(
          envelope.createName(
          "Parameter",
          nameSpace_,
          nameSpaceUrl_));

      element.addAttribute(envelope.createName("name"), name);
      element.addAttribute(envelope.createName("type"), type);
      element.addTextNode(value);
    }
    catch (SOAPException e) {
      logger.error("Cannot add attribute to SOAP message: " + e.toString());
    }
  }

  protected void detachElementsByAttribute(String nameString, String value) {
    try {
      SOAPPart sp = msg_.getSOAPPart();
      SOAPEnvelope envelope = sp.getEnvelope();
      javax.xml.soap.Name name = envelope.createName(nameString);
      detachElementsByAttributeRecursion( (SOAPElement) bodyElement_, name,
                                         value);
    }
    catch (javax.xml.soap.SOAPException ex) {
      logger.error("Cannot detach element " + nameString +
                   " from SOAP message: " + ex.toString());
    }
  }

  protected void detachElementsByAttributeRecursion(SOAPElement element,
      javax.xml.soap.Name name, String value) {
    java.util.Iterator i = element.getChildElements();
    Vector v = new Vector();

    while (i.hasNext()) {
      java.lang.Object obj = i.next();
      if (obj instanceof SOAPElement) {
        SOAPElement e = (SOAPElement) obj;
        if (e.getAttributeValue(name) != null &&
            e.getAttributeValue(name).equals(value)) {
          v.add(e);
        }
        else {
          detachElementsByAttributeRecursion(e, name, value);
        }
      }
    }

    for (int j = 0; j < v.size(); j++) {
      ( (SOAPElement) v.elementAt(j)).detachNode();
    }
  }

  public Document getDOM() {
    try {
      ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
      msg_.writeTo(outBuffer);

      ByteArrayInputStream inBuffer = new ByteArrayInputStream(outBuffer.
          toByteArray());

      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();

      return builder.parse(inBuffer);
    }
    catch (SAXException se) {
      logger.error("Cannot retrieve DOM from SOAP message: " + se.toString());
      return null;
    }
    catch (SOAPException e) {
      logger.error("Cannot retrieve DOM from SOAP message: " + e.toString());
      return null;
    }
    catch (IOException e) {
      logger.error("Cannot retrieve DOM from SOAP message: " + e.toString());
      return null;
    }
    catch (ParserConfigurationException e) {
      logger.error("Cannot retrieve DOM from SOAP message: " + e.toString());
      return null;
    }

  }

  public String getFaultString() {

    NodeList faultList = doc_.getElementsByTagName("soap-env:faultstring");

    if (faultList.getLength() == 1) {
      return faultList.item(0).getFirstChild().getNodeValue();
    }
    else {
      return null;
    }
  }

  public String getFaultCode() {
    NodeList faultList = doc_.getElementsByTagName("soap-env:faultcode");

    if (faultList.getLength() == 1) {
      return faultList.item(0).getFirstChild().getNodeValue();
    }
    else {
      return null;
    }
  }

  protected void setNameSpace(String name, String url) {
    nameSpace_ = name;
    nameSpaceUrl_ = url;
  }

  public void setData(org.w3c.dom.Node node) {
    setParameters(node, bodyElement_);
  }

  protected void addAttribute(String name, String value) {
    try {
      SOAPPart sp = msg_.getSOAPPart();
      SOAPEnvelope envelope = sp.getEnvelope();
      javax.xml.soap.Name aName = envelope.createName(name);
      bodyElement_.addAttribute(aName, value);
    }
    catch (SOAPException e) {
      logger.error("Cannot add attribute to SOAP message: " + e.toString());
    }
  }

  protected void setParameters(org.w3c.dom.Node node, SOAPElement current) {
    try {
      if (node.getNodeType() == org.w3c.dom.Node.TEXT_NODE) {
        current.addTextNode(node.getNodeValue());
      }
      else if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
        SOAPEnvelope envelope = msg_.getSOAPPart().getEnvelope();
        SOAPElement element = current.addChildElement(
            envelope.createName(
            node.getNodeName(),
            nameSpace_,
            nameSpaceUrl_));

        NamedNodeMap attributes = node.getAttributes();

        for (int k = 0; k < attributes.getLength(); k++) {
          element.addAttribute(
              envelope.createName(attributes.item(k).getNodeName()),
              attributes.item(k).getNodeValue()
              );
        }

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
          setParameters(children.item(i), element);
        }
      }
    }
    catch (Throwable t) {
      logger.error("Cannot add DOM tree to SOAP message: " + t.toString());
    }
  }

}
