package xdaq.tools.cluster;

import xdaq.tools.cluster.ClusterTable;
import xdaq.tools.cluster.JobControllerFactory;
import xdaq.tools.cluster.JobController;
import java.util.*;
import java.io.*;
import javax.swing.*;
import xdaq.tools.cluster.PlaySoundFile;

class ClusterControlException extends Exception
{
	ClusterControlException () { super(); }
	ClusterControlException ( String s ) { super(s); }
}

class ClusterControl
{
	private Vector 		jobs_ 	 = new Vector();
	private boolean 	running_ = false;
	private ClusterTable 	table_;
	
	public Vector istreams		 = new Vector();
	public Vector ostreams		 = new Vector();
	
	
	private PlaySoundFile horn_;
	private PlaySoundFile halt_;
	
	boolean sound_;
	
	public ClusterControl ( ClusterTable table )
	{
		table_ = table;
		
		horn_ = new PlaySoundFile ("./audio/corne.wav");
		halt_ = new PlaySoundFile ("./audio/morte.wav");
		sound_ = true;
	}
	
	public void setSound (boolean choice)
	{
		sound_ = choice;
	}
	
	public void execute(String username, String password) throws ClusterControlException
	{
		if (running_) {
			throw new ClusterControlException ("System already running. Halt first.");
		} else {
			if (identificationValid (username, password)) {
				String failedNodes = new String();
				//System.out.println("Executing JOB node:"+node);
				if (sound_ == true) horn_.play();
				for (int node = 0; node < table_.getNumNodes(); node++) 
				{
					
					
					if (table_.enabled(node).booleanValue()) 
					{
						String host = table_.getHostname ( node );
						String architecture = table_.getArchitecture (node);
						String protocol = table_.getProtocol (node);
						Vector commands = table_.getCommands (node);
						
						try {
							JobController p = JobControllerFactory.getInstance (protocol);
							p.exec (host, architecture, commands, username, password);
							table_.setState (node, new String("running"));
							jobs_.addElement(p);
							istreams.addElement (p.getInputStream());
							ostreams.addElement (p.getOutputStream());
							running_ = true;
						} catch (JobControllerFactoryException e) {
							String failString = host + " (" + node + "): " + e.getMessage() + '\n';
							table_.enable (node, Boolean.FALSE);
							table_.setState (node, "failed");
							failedNodes += failString;
						} catch (JobControllerException e) {
							String failString = host + " (" + node + "): " + e.getMessage() + '\n';
							table_.enable (node, Boolean.FALSE);
							table_.setState (node, "failed");
							failedNodes += failString;
						}
					}
				}
				
				// Check if all commands were executed correctly
				if (!failedNodes.equals("")) {
					throw new ClusterControlException ("Failed to execute commands:\n"+failedNodes);
				}
			} else {
				throw new ClusterControlException ("Identification failed. Username: "+username+", Password: "+password);
			}
		}
	}
	
	public boolean identificationValid (String username, String password)
	{
		if (username.equals("")) return false;
		if (password.equals("")) return false;
		return true;
	}
	
	public void destroy () throws ClusterControlException
	{
		if (sound_ == true) halt_.play();
		
		String failedNodes = new String();
		for (int i = 0; i < jobs_.size(); i++) {
			try {
				((JobController)(jobs_.elementAt(i))).destroy();
				table_.setState (i, new String("ready"));
			} catch (JobControllerException e) {
				String failString = table_.getHostname (i) + "("+i+"): "+e.getMessage()+'\n';
				failedNodes += failString;
				table_.setState (i, new String("failed"));
			}
		}
		
		jobs_.removeAllElements();
		istreams.removeAllElements();
		ostreams.removeAllElements();
		running_ = false;
		
		if (!failedNodes.equals("")) {
			throw new ClusterControlException ("Halt failed.\n"+failedNodes);
		}	
	}
	
	
}
