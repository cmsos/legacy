<?php
require '../utils/vendor/autoload.php';
include_once ('config.php');

function convertTree($data) {
	$node = array ();
	$node ["title"] = $data ["label"];
	$node ["properties"] = $data ["properties"];
	// Convert message to proper format
	if (isset( $data ["properties"]) ) {
		foreach ( $node ["properties"] as &$property ) {
			if ($property ["name"] == "message") {
				$property ["value"] = urldecode ( $property ["value"] );
			}
		}
	}
	
	if (isset( $data ["children"]) && count ( $data ["children"] ) > 0) {
		$node ["folder"] = true;
		$node ["children"] = array ();
		foreach ( $data ["children"] as $child ) {
			array_push ( $node ["children"], convertTree ( $child ) );
		}
	}
	
	
	
	return $node;
}

$_id = $_GET ['id'];
$_arcurl = $_GET ['arcurl'];

$response = Network::httpgetnocache ( $_arcurl . "/getException?id=" . $_id );

$json = json_decode ( $response->getBody(), true );
$tree = array ();
array_push ( $tree, convertTree ( $json ) );
header ( 'Content-Type: application/json; charset=utf-8' );

echo json_encode ( $tree );
?>
