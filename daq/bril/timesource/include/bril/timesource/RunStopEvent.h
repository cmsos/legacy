#ifndef _bril_timesource_RunStopEvent_h_
#define _bril_timesource_RunStopEvent_h_

#include <string>
#include "toolbox/Event.h"

namespace bril {
  namespace timesource{
    class RunStopEvent : public toolbox::Event{
    public:
      RunStopEvent();
      ~RunStopEvent();
    };
  }
}
#endif
