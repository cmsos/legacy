#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>

#include <errno.h>
#include <string.h>

#include <infiniband/verbs.h>

int main (int argc, const char* argv[])
{
	// Prints each argument on the command line.

	if (argc != 3)
	{
		fprintf(stderr, "Usage : <number of blocks> <block size in bytes>\n");
		return 1;
	}

	struct ibv_context *ctx;
	struct ibv_device **device_list;
	int num_devices;
	int i;
	int rc;

	device_list = ibv_get_device_list(&num_devices);
	if (!device_list)
	{
		fprintf(stderr, "Error, ibv_get_device_list() failed\n");
		//std::cout << "Error, ibv_get_device_list() failed" << std::endl;
	}
	printf("%d IB device(s) found:\n", num_devices);

	if (num_devices > 1)
	{
		printf("Warning : this test was built for a machine with only one attached IB device, this may behave unexpectedly\n");
	}

	for (i = 0; i < num_devices; i++)
	{
		struct ibv_device_attr device_attr;
		int j;

		printf("Opened device %s\n", ibv_get_device_name(device_list[i]));

		ctx = ibv_open_device(device_list[i]);
		if (!ctx)
		{
			fprintf(stderr, "Error, failed to open the device '%s'\n", ibv_get_device_name(device_list[i]));
			//std::cout << "Error, failed to open the device" << std::endl;
			rc = -1;
			//goto out;
		}

		rc = ibv_query_device(ctx, &device_attr);
		if (rc)
		{
			fprintf(stderr, "Error, failed to query the device '%s' attributes\n", ibv_get_device_name(device_list[i]));
			//std::cout << "Error, failed to query the device" << std::endl;
			//goto out_device;
		}

		/*
		 printf("The device '%s' has %d port(s)\n", ibv_get_device_name(ctx->device), device_attr.phys_port_cnt);
		 //std::cout << "the device has ports" << std::endl;

		 for (j = 1; j <= device_attr.phys_port_cnt; ++j)
		 {
		 struct ibv_port_attr port_attr;

		 rc = ibv_query_port(ctx, j, &port_attr);
		 if (rc)
		 {
		 fprintf(stderr, "Error, failed to query port %d attributes in device '%s'\n", j, ibv_get_device_name(ctx->device));
		 //std::cout << "Error, failed to query port" << std::endl;
		 //goto out_device;
		 }

		 printf("RDMA device %s, port %d state %s active\n", ibv_get_device_name(ctx->device), j, (port_attr.state == IBV_PORT_ACTIVE) ? "is" : "isn't");
		 //std::cout << "win" << std::endl;
		 }
		 */

		struct ibv_pd * pd = ibv_alloc_pd(ctx);

		int num = atoi(argv[1]);
		int block_size = atoi(argv[2]);

		void* m[num];
		struct ibv_mr* mr[num];

		size_t align = sysconf(_SC_PAGESIZE);

		errno = 0;

		printf("Starting memory allocations, %d blocks of %d bytes, aligned to %d\n", num, block_size, align);

		// allocate 'num' blocks of 'block_size' bytes and register them
		for (int i = 0; i < num; i++)
		{
			m[i] = memalign(align, block_size);
			struct ibv_mr * r = 0;
			r = ibv_reg_mr(pd, m[i], block_size, IBV_ACCESS_LOCAL_WRITE);
			if (r == 0)
			{
				fprintf(stderr, "Error, failed allocate memory region %d, allocated so far = '%i'\n", i + 1, (block_size * i));
				fprintf(stderr, "errno = %s, terminating program\n", strerror(errno));
				return 1;
			}
			mr[i] = r;
			//fprintf(stderr, "Allocated mr at %p\n", (r->addr));
		}

		// deallocate all blocks

		int ret = 0;
		for (int i = 0; i < num; i++)
		{
			ret = ibv_dereg_mr(mr[i]);
			if (ret != 0)
			{
				fprintf(stderr, "Error, failed deallocate memory region %d\n", i);
				return 1;
			}
			free(m[i]);
		}

		fprintf(stderr, "All memory deallocated\n");

		rc = ibv_close_device(ctx);
		if (rc)
		{
			fprintf(stderr, "Error, failed to close the device '%s'\n", ibv_get_device_name(ctx->device));
//std::cout << "Error, failed to close the device" << std::endl;
			//goto out;
		}

	}
	ibv_free_device_list(device_list);
	return 0;
}
