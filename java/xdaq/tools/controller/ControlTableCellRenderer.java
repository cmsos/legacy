package xdaq.tools.controller;

 // This renderer extends a component. It is used each time a
 // cell must be displayed.
import java.awt.*;
import javax.swing.table.*;
import javax.swing.*;

public class ControlTableCellRenderer extends JLabel implements TableCellRenderer {

// This method is called each time a cell in a column
// using this renderer needs to be rendered.

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int vColIndex) {
		// 'value' is value contained in the cell located at
		// (rowIndex, vColIndex)
		/*if (isSelected) {
			// cell (and perhaps other cells) are selected
			this.setBackground(table.getSelectionBackground());
			this.setForeground(table.getSelectionForeground());
		}
		else
		{
			this.setBackground(table.getBackground());
			this.setForeground(table.getForeground());
		}*/
		this.setOpaque(true);
		// Configure the component with the specified value
		setText(value.toString());
		 if (isSelected) {
		 	 setForeground(table.getSelectionForeground());
			 setBackground(table.getSelectionBackground());
		} else {
			setForeground(table.getForeground());
			setBackground(table.getBackground());
		} 
		if (hasFocus) {
			// this cell is the anchor and the table has the focus
		}
		
		
		
		// Set tool tip if desired
		//setToolTipText((String)value);
		this.setFont(table.getFont());
		this.setHorizontalAlignment(SwingConstants.LEFT);
		
		
		// Since the renderer is a component, return itself
		return this;

	}
}
