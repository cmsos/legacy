package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;

public class CommandConfigureAction extends AbstractAction
{
	xdaqWin win_;

	public CommandConfigureAction (xdaqWin win)
	{
		super ("Configure");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {
			win_.executeCommandOnSelection("Configure", null,win_.getPaths() );
		}
		catch(xdaqWinException ex )
		{
		
		}

	}
}
