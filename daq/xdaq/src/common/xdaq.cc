// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <cstdlib>
#include <pwd.h>
#include <errno.h>

#include "xcept/tools.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/Runtime.h"
#include "xdaq/exception/Exception.h"
#include "toolbox/string.h"
#include "toolbox/utils.h"
#include "toolbox/Groups.h"

#include "xdaq/ApplicationContextImpl.h"

// LOG4CPLUS INCLUDES
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"

// Xerces includes
#include "xercesc/dom/DOM.hpp"
//#include <xqilla/xqilla-dom3.hpp>

#define RUNNING_DIR	"/tmp"
// #define LOCK_FILE	"exampled.lock"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

void daemonize(const std::string & out, const std::string & err)
{
	int i;
	// int lfp;
	// char str[10];

	i=fork();
	if (i<0) exit(1); /* fork error */
	if (i>0) exit(0); /* parent exits */
	
	/* child (daemon) continues */
	setsid(); /* obtain a new process group */

	int fdout = -1;
	try
	{
        	fdout = toolbox::createFile(out, false);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::cerr << "Failed to create file " << out << ": " << e.what() << std::endl;
	}

	int fderr = -1;
	try
	{
        	fderr = toolbox::createFile(err, false);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::cerr << "Failed to create file " << err << ": " << e.what() << std::endl;
	}

	for ( i=0 ; i<=2 ; i++ ) close(i); /* close all standard file descriptors */

	dup2(fdout, 1); /* redirect stdout */
	dup2(fderr, 2); /* redirect stderr */

	chdir(RUNNING_DIR); /* change running directory */
	
	//lfp=open(LOCK_FILE,O_RDWR|O_CREAT,0640);
	// if (lfp<0) exit(1); /* can not open */
	// if (lockf(lfp,F_TLOCK,0)<0) exit(0); /* can not lock */
	/* first instance continues */
	//sprintf(str,"%d\n",getpid());
	// write(lfp,str,strlen(str)); /* record pid to lockfile */
	signal(SIGCHLD,SIG_DFL); /* ignore child */
	signal(SIGTSTP,SIG_IGN); /* ignore tty signals */
	signal(SIGTTOU,SIG_IGN);
	signal(SIGTTIN,SIG_IGN);
	signal(SIGHUP,SIG_IGN); /* catch hangup signal */
	//signal(SIGTERM,signal_handler); /* catch kill signal */
}


int main(int argc, char* argv[])
{
	std::string out = "/dev/null";
	std::string err = "/dev/null";
	bool daemon = false;

	for (int c = 0; c < argc; ++c)
	{
		if (strcmp (argv[c], "--stdout") == 0)
		{
			c++;
			if(c>=argc)
			{
				std::cerr << "Missing stdout parameter" << std::endl;
				exit(1);
			}
			out = argv[c];
		}
		if (strcmp (argv[c], "--stderr") == 0)
		{
			c++;
			if(c>=argc)
			{
				std::cerr << "Missing stderr parameter" << std::endl;
				exit(1);
			}
			err = argv[c];
		}
		else if (strcmp (argv[c], "--daemon") == 0)
		{
			daemon = true;
		}
	}

	//Running XDAQ process as a user provided by the environment variable XDAQ_USER
	if ( const char* envvar = std::getenv("XDAQ_USER") )
	{
		std::string xdaquser(envvar);

		//Get userid from username
		struct passwd * pwd;
		errno = 0;
		pwd = getpwnam(xdaquser.c_str());
		if ( pwd != 0 )
		{
			try
			{
				toolbox::setSupplementaryGroups( pwd );
			}
			catch( xcept::Exception& e )
			{
				std::cerr << "Failed to set the supplementary groups of the process to those of the user " << xdaquser
						<< ": " << xcept::stdformat_exception_history( e ) << std::endl;
				exit(1);
			}

			int groupid = (int) pwd->pw_gid;
			errno = 0;
			if ( setgid(groupid) != 0 )
			{
				std::cerr << "Call to setgid failed for user " << xdaquser << ", group " << groupid << ": "<< strerror(errno) << std::endl;
				exit(1);
			}
			int userid = (int) pwd->pw_uid;
			errno = 0;
			if ( setuid(userid) != 0 )
			{
				std::cerr << "Call to setuid failed for user " << xdaquser << ": "<< strerror(errno) << std::endl;
				exit(1);
			}
		}
		else
		{
			std::cerr << "Call to getpwnam failed for user " << xdaquser << ": "<< strerror(errno) << std::endl;
			exit(1);
		}
	}

	if(daemon)
	{
		std::cout << "daemonizing xdaq process..." << std::endl;
		daemonize(out, err);
	}

	//  Create the Runtime object
	toolbox::getRuntime();

	// Disallow SIGPIPE signals
	// Such, we can handle droped socket connections.
	//
	sigset_t block_pipe;
	sigemptyset (&block_pipe);
	sigaddset (&block_pipe, SIGPIPE);
	sigprocmask(SIG_BLOCK, &block_pipe, NULL);

	XMLPlatformUtils::Initialize();
	//XQillaPlatformUtils::initialize();

	Logger rootLogger = Logger::getRoot();

	try
	{
		xdaq::ApplicationContextImpl context(rootLogger);
		context.init(argc, argv);
		
		for (;;) 
		{
			::pause();
		}
	}
	catch(toolbox::exception::Exception& te) 
	{
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(te));
		return 1;
	}
	catch(xdaq::exception::Exception& xe) 
	{		
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(xe));
		return 2;
	}
	catch(xcept::Exception& ee) 
	{
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(ee));
		return 3;
	}
	catch(std::exception& se) 
	{
		LOG4CPLUS_FATAL(rootLogger,"unhandled std exception " << se.what());
		return 4;
	}
	catch(...) 
	{
		LOG4CPLUS_FATAL(rootLogger,"unhandled unknown exception");
		return 5;
	}
}


