
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_AdvertisementListImpl_h
#define _jxta_AdvertisementListImpl_h

#include <string>
#include <vector>

#include "jxta/exception/Exception.h"
#include "jxta/AdvertisementImpl.h"
#include "jxta/AdvertisementList.h"
#include "jxta.h"
#include "jxta_vector.h"

namespace jxta
{

class AdvertisementListImpl: public AdvertisementList
{
	public:
	
	AdvertisementListImpl(Jxta_vector* advertisements);
	~AdvertisementListImpl();
		
	int getLength();
	jxta::Advertisement::Reference getItem(int i);
	void addItem(jxta::Advertisement::Reference adv);
	void removeItem(int i);

	private:
	Jxta_vector * advertisements_;
};

}

#endif
