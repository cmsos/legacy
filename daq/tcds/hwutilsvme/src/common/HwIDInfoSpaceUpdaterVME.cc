#include "tcds/hwutilsvme/HwIDInfoSpaceUpdaterVME.h"

#include <string>

#include "tcds/hwlayervme/VMEDeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME::HwIDInfoSpaceUpdaterVME(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   tcds::hwlayervme::VMEDeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME::~HwIDInfoSpaceUpdaterVME()
{
}

bool
tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                               tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayervme::VMEDeviceBase const& hw = getHw();
  if (hw.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on
          // the variable name.
          if (name == "board_id")
            {
              std::string const newVal = hw.readBoardID();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
          else if (name == "firmware_version")
            {
              std::string const newVal = hw.readFirmwareVersion();
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayervme::VMEDeviceBase const&
tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME::getHw() const
{
  return dynamic_cast<tcds::hwlayervme::VMEDeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
