#include "strstream.h"
#include "XRCAdapter.h"
#include "XRUAdapter.h"
#include "XRC.h"
#include "TcpDispatcherAcceptorTaskM.h"
#include "MuxSingleCasterDispatcher.h"
#include "MuxSingleCasterDispatcherPollTask.h"
#include "SensSingleCasterDispatcherTask.h"
#include "vxioslib.h"
#include "vxTiming.h"
#include "MuxXIOChannel.h"
#include "TcpDispatcherAcceptorTaskS.h"
#include "msgQLib.h"

vxChron chrono;	
unsigned long loopCounter = 0;
inline void Benchmarks2(int size)
{
	
	
	if ( loopCounter == 1000 )
	{
		chrono.start(0);
	}
	else
	if ( loopCounter == 101000 )
	{
		loopCounter = 0;
		chrono.stop(0);
                printf("Delta Time : %d usecs \n", chrono.dusecs());
		printf("Rate: %f Hz  \n", (float)100000/(float)(chrono.dusecs()/(float)1000000));
		printf("Bandwitdth: %f Mb\n",
		(((float)100000/(float)(chrono.dusecs()/(float)1000000)) * (float)size)/(float)0x100000);
		printf("Time to process one Event: %f usecs\n", (float)(chrono.dusecs()/(float)100000));
	}
	loopCounter++;
}


unsigned long dbg = 0;

class Bench: public XRCAdapter , public XRUAdapter {

	public:
	
	Bench(XRU * xru) {
		xru_ = xru;
		count_ = 0;
		msgq_ = msgQCreate (1024, sizeof(unsigned long), MSG_Q_FIFO);
	}
	
	void stop(char * str) {
		//count_++;
		//if ( count_ > 100000 ) {
			//::logMsg("Single trip 100000\n", 0, 0, 0, 0, 0, 0);
			//count_ = 0;
		//}
		Benchmarks2(1024);
		//::logMsg("Single trip 1\n", 0, 0, 0, 0, 0, 0);
	}
	void readout (EventIdentifier* eid) {
		//::logMsg("start(char * str)\n", 0, 0, 0, 0, 0, 0);
		//msgQSend (msgq_, (char*)eid, sizeof(unsigned long), WAIT_FOREVER, MSG_PRI_NORMAL);
		xru_->readout(eid);

	
	}
	
	int svc() {		
	
		EventIdentifier eid;

		for(;;) {
			msgQReceive (msgq_, (char*)&eid, sizeof(unsigned long) , WAIT_FOREVER);	
			xru_->readout(&eid);
		}
		

	}
	XRU * xru_;
	int count_;
	MSG_Q_ID msgq_;
};


extern "C"  int Main( char * ifname, char * dest, int sap)
{	
	// Preapare Mux Output channel
	MuxXIOChannel muxio(ifname,0,0x1923,dest); 
	//TcpDispatcherAcceptorTaskS tcpd("137.138.96.213",20222);
	// Prepare interfaces on channel
	XRU xru(&muxio);
	//XRC xrc(0);
	// Prepare MUX Dispatcher and Listener
	MuxSingleCasterDispatcher t(ifname,0,"dllInput",sap );
	//SensSingleCasterDispatcherTask t(ifname,0,"dllInput",0x1921 );
	//MuxSingleCasterDispatcherPollTask t(ifname,0,"dllInput",0x1921 );
	Bench bench(&xru);
	//tcpd.addListener(&bench);
	//tcpd.activate();

	t.setListener(&bench);
	//t.activate(); // for Sens only
	char str[256];
	//bench.svc();
	cout << "press ENTER to stop" << endl;
	cin >> str;
	
	
}

