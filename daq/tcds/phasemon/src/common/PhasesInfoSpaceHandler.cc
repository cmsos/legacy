#include "tcds/phasemon/PhasesInfoSpaceHandler.h"

#include "tcds/phasemon/Definitions.h"
#include "tcds/phasemon/PhaseMon.h"
#include "tcds/phasemon/PhaseMonInfoSpaceHandler.h"
#include "tcds/phasemon/Utils.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
// #include "tcds/utils/Definitions.h"
// #include "tcds/utils/InfoSpaceItem.h"
// #include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/Monitor.h"
// #include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::phasemon::PhasesInfoSpaceHandler::PhasesInfoSpaceHandler(tcds::phasemon::PhaseMon& xdaqApp,
                                                               tcds::utils::InfoSpaceUpdater* updater) :
  MultiInfoSpaceHandler(xdaqApp, updater)
{
  // NOTE: The PhaseMon type (i.e., 'CLK' or 'TTC') determines which
  // signals we should monitor.
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    getOwnerApplication().getConfigurationInfoSpaceHandler();
  uint32_t const tmpType = cfgInfoSpace.getUInt32("phasemonType");
  tcds::definitions::PHASEMON_TYPE const phasemonType =
    static_cast<tcds::definitions::PHASEMON_TYPE>(tmpType);
  std::vector<tcds::definitions::PHASEMON_INPUT> inputList;
  // Most signals exist in both PhaseMon versions.
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_FCLKA);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_REFCLK);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP_REFCLK);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP1);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP2);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP3);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP4);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP5);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP6);
  inputList.push_back(tcds::definitions::PHASEMON_INPUT_SFP7);
  // Some signals are only availably in the 'clocks' PhaseMon.
  if (phasemonType == tcds::definitions::PHASEMON_TYPE_CLK)
    {
      inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_TTCORB);
      inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_TR0);
      inputList.push_back(tcds::definitions::PHASEMON_INPUT_ECL_TR1);
    }
  for (std::vector<tcds::definitions::PHASEMON_INPUT>::const_iterator i = inputList.begin();
       i != inputList.end();
       ++i)
    {
      infoSpaceHandlers_["phases-" + inputNumberToRegName(*i)] =
        new tcds::phasemon::PhaseMonInfoSpaceHandler(getOwnerApplication(), updater, *i);
    }
}

tcds::phasemon::PhasesInfoSpaceHandler::~PhasesInfoSpaceHandler()
{
  // Cleanup of InfoSpaceHandlers happens in the MultiInfoSpaceHandler
  // base class.
}

tcds::phasemon::PhaseMon&
tcds::phasemon::PhasesInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::phasemon::PhaseMon&>(tcds::utils::MultiInfoSpaceHandler::getOwnerApplication());
}

// void
// tcds::phasemon::PhasesInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
// {
//   // std::string const itemSetName = "itemset-phasemon-results";

//   for (std::map<std::string, tcds::utils::InfoSpaceHandler*>::iterator
//          i = infoSpaceHandlers_.begin();
//        i != infoSpaceHandlers_.end();
//        ++i)
//     {
//       i->second->registerItemSetsWithMonitor(monitor);
//     }
  // // The overall partition controller state (in the firmware).
  // monitor.addItem("itemset-running-state",
  //                 "phasemon_state",
  //                 "Running state",
  //                 infoSpaceHandlers_.at("misc"));

  //----------

// }

// void
// tcds::phasemon::PhasesInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
//                                                                       tcds::utils::Monitor& monitor,
//                                                                       std::string const& forceTabName)
// {
//   std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

//   for (std::map<std::string, tcds::utils::InfoSpaceHandler*>::iterator
//          i = infoSpaceHandlers_.begin();
//        i != infoSpaceHandlers_.end();
//        ++i)
//     {
//       i->second->registerItemSetsWithWebServer(webServer, monitor, forceTabName);
//     }
// }
