package xdaq.xdesk.xprop;

import javax.swing.*;
import java.util.*;
import javax.infobus.*;
import javax.swing.event.*;


public class XPropDefaultModel  implements XPropModel
{

	EventListenerList listenerList_ = new EventListenerList();
	Hashtable configurations_ = new Hashtable();

	public void addXPropModelListener(XPropModelListener l )
	{
		listenerList_.add(XPropModelListener.class, l );
	
	}
	
	public void removeXPropModelListener(XPropModelListener l )
	{
		listenerList_.remove(XPropModelListener.class, l );
	}
	
	public Enumeration  getConfigurationNames()
	{
		return configurations_.keys();
	
	}
	
	public Object getValue( String name )
	{
		return configurations_.get(name);
	
	}
	
	public void setValue(String name, Object item )
	{
		configurations_.put(name, item);
	}
	
	public Object removeValue(String name)
	{
		return configurations_.remove(name);
	}
	
	
	protected void fireXPropModelDataChanged()
	{
	
		Object[] listeners = listenerList_.getListenerList();
		XPropModelEvent event = null;
		for (int i = listeners.length-2; i>= 0; i-=2)
		{
			System.out.println("Notify listener "+ i);
			if (event == null )
			{
				event = new XPropModelEvent(this);
			}
			((XPropModelListener)listeners[i+1]).xpropChanged(event);
		
		}
	}
	
	
}	
	
/*
	HashMap partitionNameMap_;
*/

/*	
	public XPropDefaultModel()
	{
		partitionNameMap_ = new HashMap();
		listenerList_ = new EventListenerList();
	}
*/	
	// XPropModel listener interface implementation
	

/*
	
*/
/*
	public void setPartitionList(HashMap partitions)
	{
		//System.out.println("BEGIN partition list has changed, I am updating ComboBox" +
		//partitionNameMap_.isEmpty());
	
		
		partitionNameMap_.clear();
		
		// This call has the side effect to fire the combo box has changed, it must stay after the clear
		this.removeAllElements();
				
		//System.out.println("MIDDLE partition list has changed, I am updating ComboBox" +partitionNameMap_.isEmpty());
		
		ArrayList list = new ArrayList(partitions.keySet());
		for (int i = 0; i < list.size(); i++)
		{
			XProp.XPropertiesListener partition = (XProp.XPropertiesListener) partitions.get(list.get(i));
			ImmediateAccess item = (ImmediateAccess) partition.getItem();
			
			System.out.println ("Combo item: " + item.getValueAsString());
			String name = item.getValueAsString();
			
			partitionNameMap_.put (name, item);
			
			this.addElement(name);
		}
		//System.out.println("END partition list has changed, I am updating ComboBox" +
		//partitionNameMap_.isEmpty());
	}
*/	
/*	
	public Object getObject (String name)
	{
		//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$ Getting Item from Map isEmpty?" +
		//partitionNameMap_.isEmpty());
		return partitionNameMap_.get (partitionName);
	}
	
	public boolean containsKey (String partitionName)
	{
		//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$ Getting Item from Map isEmpty?" +
		//partitionNameMap_.isEmpty());
		return partitionNameMap_.containsKey (partitionName);
	}
	
	public boolean isEmpty ()
	{
		//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$ Getting Item from Map isEmpty?" +
		//partitionNameMap_.isEmpty());
		return partitionNameMap_.isEmpty ();
	}
	
	public addObject(Object key, Object item)
	{
		configuration_.add (key, item);
	
	}
	
	public removeObject(Object key )
	{
		configuration_.remove (item);
	
	}
	public Object getObject (Object key)
	{
		//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$ Getting Item from Map isEmpty?" +
		//partitionNameMap_.isEmpty());
		return configuration_.get (partitionName);
	}
	
*/	

