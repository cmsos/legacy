package xdaq.xdesk.xtree;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import java.util.*;
import org.w3c.dom.*;
import java.net.*;

public class XTreeModel extends  DefaultTreeModel
{
        XTreeFilter filter_;
	
	public XTreeModel (DefaultMutableTreeNode root)
	{
            super(root);
            
	}
	
        
        public XTreeModel cloneModelWithFilter(XTreeFilter filter)
        {
            filter_ = filter;
            DefaultMutableTreeNode root =  (DefaultMutableTreeNode)this.getRoot();
            XTreeNode adapterNode = (XTreeNode)root.getUserObject();
            DefaultMutableTreeNode rootNode = this.cloneTreeNode(adapterNode);
            XTreeModel treeModel = new XTreeModel ( rootNode );
            return treeModel;
	}
        
        // Recursivly traverse all DOM nodes and build a tree from DefaultMutableTreeNodes
	//
	public DefaultMutableTreeNode cloneTreeNode ( XTreeNode adapterNode )  // filter all node except the root node
	{
               
		DefaultMutableTreeNode node = new DefaultMutableTreeNode ( adapterNode );
		
		if ( adapterNode.isLeaf() )
		{
			return node;
		}
		else {
			NodeList children = adapterNode.domNode.getChildNodes();
			
			// use adapterNode::childCount instead of the equivalent DOM
			// node function, since the DOM document also contains empty
			// text nodes.
			//
			for (int k = 0; k < adapterNode.childCount(); k++)
			{
                                XTreeNode childNode = adapterNode.child (k) ;
                                String nodeName = childNode.domNode.getNodeName();
                                if ( (filter_ == null) || (! filter_.contains(nodeName)) ) 
                                {
                                       node.add ( this.cloneTreeNode ( childNode ) );     
                                }
                        
			}
			return node;
		}
	}

	
}
