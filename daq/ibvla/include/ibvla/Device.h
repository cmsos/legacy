// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_Device_h
#define _ibvla_Device_h

#include <infiniband/verbs.h>

#include "ibvla/exception/Exception.h"

#include "ibvla/Context.h"

namespace ibvla
{
	class Device
	{

		public:
			Device ()
			{
				device_ = 0;
			}

			Device (ibv_device * d);

			std::string getName ();

			uint64_t getGUID ();

			ibv_device * device_;
	};
}

#endif
