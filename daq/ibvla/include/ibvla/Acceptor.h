// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_Acceptor_h
#define _ibvla_Acceptor_h

#include "toolbox/lang/Class.h"
#include "toolbox/task/WaitingWorkLoop.h"

#include <infiniband/verbs.h>

#include "ibvla/exception/Exception.h"
#include "ibvla/CompletionQueue.h"

#include "toolbox/mem/Pool.h"

namespace ibvla
{

	class AcceptorListener;
	class ConnectionRequest;
	class QueuePair;

	class Acceptor : public virtual toolbox::lang::Class
	{
		public:

			std::string name_;
			AcceptorListener * listener_;

			Acceptor (const std::string & name, ibvla::AcceptorListener * listener, size_t mtu, ibvla::CompletionQueue cqr, toolbox::mem::Pool * rpool, size_t ibPort, size_t ibPath);

			void listen (const std::string & hostname, const std::string & port) ;
			void accept (ConnectionRequest &id, QueuePair &qp) ;
			void reject (ConnectionRequest &id) ;

			bool process (toolbox::task::WorkLoop* wl);

			toolbox::task::WorkLoop* workLoop_;
			toolbox::task::ActionSignature* process_;

			int listenfd_;

			ibv_mtu mtu_;
			ibvla::CompletionQueue cqr_;

			toolbox::mem::Pool * rpool_;
			size_t ibPort_;
			size_t ibPath_;
		private:
	};
}

#endif
