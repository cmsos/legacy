#ifndef _tcds_apve_APVESimHistInfoSpaceUpdater_h_
#define _tcds_apve_APVESimHistInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace apve {

    class TCADeviceAPVE;

    class APVESimHistInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      APVESimHistInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                  TCADeviceAPVE const& hw);
      virtual ~APVESimHistInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      tcds::apve::TCADeviceAPVE const& getHw();

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVESimHistInfoSpaceUpdater_h_
