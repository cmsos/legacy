package test.wse;

public class WseException extends Exception
{
	public WseException()
	{
		super();
	}
	
	public WseException(String msg)
	{
		super(msg);
	}

	public WseException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

	public WseException(Throwable cause)
	{
		super(cause);
	}
}

