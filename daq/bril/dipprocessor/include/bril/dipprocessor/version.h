// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_dipprocessor_version_h_
#define _bril_dipprocessor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILDIPPROCESSOR_VERSION_MAJOR 1
#define BRILDIPPROCESSOR_VERSION_MINOR 10
#define BRILDIPPROCESSOR_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILDIPPROCESSOR_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILDIPPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILDIPPROCESSOR_VERSION_MAJOR,BRILDIPPROCESSOR_VERSION_MINOR,BRILDIPPROCESSOR_VERSION_PATCH)
#ifndef BRILDIPPROCESSOR_PREVIOUS_VERSIONS
#define BRILDIPPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILDIPPROCESSOR_VERSION_MAJOR,BRILDIPPROCESSOR_VERSION_MINOR,BRILDIPPROCESSOR_VERSION_PATCH)
#else
#define BRILDIPPROCESSOR_FULL_VERSION_LIST  BRILDIPPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILDIPPROCESSOR_VERSION_MAJOR,BRILDIPPROCESSOR_VERSION_MINOR,BRILDIPPROCESSOR_VERSION_PATCH)
#endif

namespace brildipprocessor
{
	const std::string package = "brildipprocessor";
	const std::string versions = BRILDIPPROCESSOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ dipprocessor";
	const std::string description = "dip analyzer processor";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
