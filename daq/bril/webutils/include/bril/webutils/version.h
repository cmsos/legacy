// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_webutils_version_h_
#define _bril_webutils_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILWEBUTILS_VERSION_MAJOR 1
#define BRILWEBUTILS_VERSION_MINOR 2
#define BRILWEBUTILS_VERSION_PATCH 4
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILWEBUTILS_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILWEBUTILS_VERSION_CODE PACKAGE_VERSION_CODE(BRILWEBUTILS_VERSION_MAJOR,BRILWEBUTILS_VERSION_MINOR,BRILWEBUTILS_VERSION_PATCH)
#ifndef BRILWEBUTILS_PREVIOUS_VERSIONS
#define BRILWEBUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILWEBUTILS_VERSION_MAJOR,BRILWEBUTILS_VERSION_MINOR,BRILWEBUTILS_VERSION_PATCH)
#else
#define BRILWEBUTILS_FULL_VERSION_LIST  BRILWEBUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILWEBUTILS_VERSION_MAJOR,BRILWEBUTILS_VERSION_MINOR,BRILWEBUTILS_VERSION_PATCH)
#endif

namespace brilwebutils
{
	const std::string package = "brilwebutils";
	const std::string versions = BRILWEBUTILS_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ bhmsource";
	const std::string description = "bril webpage utilities";
	const std::string authors = "N. Tosi";
	const std::string link = "";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
