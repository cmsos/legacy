/*
 * XDAQTimeoutException
 *
 * $Id: XDAQTimeoutException.java,v 4.1 2007/03/26 14:40:24 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

/**
 * Exception caused by messaging timeouts
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XDAQTimeoutException extends XDAQException
{
	public XDAQTimeoutException() {}
	public XDAQTimeoutException(String message) { super(message); }
	public XDAQTimeoutException(Throwable cause) { super(cause); }
	public XDAQTimeoutException(String message, Throwable cause) {
		super(message, cause);
	}
}

// End of file
// vim: set ts=4 sw=4:
