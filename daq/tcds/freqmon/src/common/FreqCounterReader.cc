#include "tcds/freqmon/FreqCounterReader.h"

#include <utility>

#include <arpa/inet.h>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <list>
#include <netdb.h>
#include <sstream>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>
#include <vector>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/Resolver.h"
#include "tcds/utils/Utils.h"

tcds::freqmon::FreqCounterReader::FreqCounterReader(xdaq::Application& xdaqApp,
                                                    std::string const& hostName,
                                                    uint32_t const portNumber) :
  hostName_(hostName),
  portNumber_(portNumber),
  socket_(-1)
{
  connect();
  configure();
}

tcds::freqmon::FreqCounterReader::~FreqCounterReader()
{
  try
    {
      disconnect();
    }
  catch (...)
    {
      // Nothing to do. Just make sure the destructor doesn't throw.
    }
}

uint32_t
tcds::freqmon::FreqCounterReader::readValue()
{
  std::string const data = readRawData();
  std::list<std::string> pieces = toolbox::parseTokenList(data, "\n");
  std::string const lastPiece = pieces.back();
  std::istringstream tmp(lastPiece);
  uint32_t res;
  tmp >> res;
  return res;
}

void
tcds::freqmon::FreqCounterReader::connect() const
{
  // Resolve the host name.
  tcds::utils::Resolver resolver;
  std::vector<struct addrinfo> hostInfos;
  try
    {
      hostInfos = resolver.resolve(hostName_, portNumber_);
    }
  catch (tcds::exception::Exception const& err)
    {
      std::string const msg =
        toolbox::toString("Failed to resolve frequency counter host name '%s'.", hostName_.c_str());
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }
  struct addrinfo const hostInfo = hostInfos.at(0);

  // Create the network socket.
  socket_ = ::socket(hostInfo.ai_family, hostInfo.ai_socktype, hostInfo.ai_protocol);
  if (socket_ < 0)
    {
      std::string const msg =
        toolbox::toString("Failed to create network socket (to connect to the frequency counter): '%s'.",
                          std::strerror(errno));
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  // Build connection object and connect.
  if (::connect(socket_, hostInfo.ai_addr, hostInfo.ai_addrlen) < 0)
    {
      std::string const msg =
        toolbox::toString("Failed to connect to the frequency counter '%s:%d': '%s'.",
                          hostName_.c_str(),
                          portNumber_,
                          std::strerror(errno));

      // Don't forget to clean up the socket!
      ::close(socket_);
      socket_ = -1;

      XCEPT_RAISE(tcds::exception::HardwareProblem, msg.c_str());
    }

  // Set the timeouts of this socket to 2 seconds.
  struct timeval tv;
  tv.tv_sec = 2;
  tv.tv_usec = 0;
  setsockopt(socket_, SOL_SOCKET, SO_SNDTIMEO, (void*)&tv, sizeof(tv));
  setsockopt(socket_, SOL_SOCKET, SO_RCVTIMEO, (void*)&tv, sizeof(tv));
}

void
tcds::freqmon::FreqCounterReader::disconnect() const
{
  if (socket_ >= 0)
    {
      ::close(socket_);
    }
  socket_ = -1;
}

void
tcds::freqmon::FreqCounterReader::configure() const
{
  // Configuration steps:
  // - Stop any possible ongoing loop.
  // - Switch to the external reference clock.
  // - Start a new measurement loop.

  std::vector<std::pair<std::string, bool> > commands;
  commands.push_back(std::make_pair("q", true));
  commands.push_back(std::make_pair("ref external", false));
  commands.push_back(std::make_pair("loop", false));

  for (std::vector<std::pair<std::string, bool> >::const_iterator it = commands.begin();
       it != commands.end();
       ++it)
    {
      std::string const cmd = it->first;
      bool const mayGiveSyntaxError = it->second;
      sendCommand(cmd);
      std::string const tmp = readRawData();
      std::string const res = toolbox::tolower(toolbox::trim(tmp));
      if (res != cmd)
        {
          if (!mayGiveSyntaxError)
            {
              disconnect();
              std::string const msg =
                toolbox::toString("Failed to configure the frequency counter. "
                                  "Received '%s' in response to '%s' command.",
                                  toolbox::trim(tmp).c_str(), cmd.c_str());
              XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
            }
        }
    }
}

void
tcds::freqmon::FreqCounterReader::sendCommand(std::string const& cmd) const
{
  std::string const tmp = cmd + "\r\n";
  send(socket_, tmp.c_str(), tmp.size(), 0);
  ::sleep(1);
}

std::string
tcds::freqmon::FreqCounterReader::readRawData() const
{
  // NOTE: The frequency counter only ever presents a single data
  // value at the output.
  size_t const bufSize = 1024;
  char buf[bufSize];
  int numBytes = recv(socket_, buf, bufSize, 0);
  if (numBytes < 1)
    {
      disconnect();
      std::string msg = "Failed to read from the frequency counter";
      if (errno < 0)
        {
          msg = toolbox::toString("%s: '%s'", msg.c_str(), std::strerror(errno));
        }
      msg += ".";
      XCEPT_RAISE(tcds::exception::RuntimeProblem, msg.c_str());
    }

  std::string const rawData(buf, numBytes);
  return rawData;
}
