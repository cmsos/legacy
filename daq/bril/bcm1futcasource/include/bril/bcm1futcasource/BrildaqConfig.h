// $Id$
#ifndef _BRIL_BCM1FUTCASOURCE_BRILDAQCONFIG_H_
#define _BRIL_BCM1FUTCASOURCE_BRILDAQCONFIG_H_
#include <string>
#include <map>
#include <set>
#include <utility>
#include "xdata/InfoSpace.h"
#include "xdata/Vector.h"
#include "xdata/Properties.h"
#include "xdata/Boolean.h"
#include "xdaq//Application.h"

namespace bril { namespace bcm1futcasource {

class BrildaqConfig{
 public:
    explicit BrildaqConfig(xdaq::Application * application);

    void setDefaultValues();

    std::set<std::string> getListOfPublicationBuses() const;

    std::set<std::string> getListOfPublicationBuses(const std::string & topic) const;

    const std::map<std::string, std::string> & getListOfSubscriptions() const {return _subscriptionTopics; }

    bool isOrbitRequestedEveryNB1() const;
    bool isOrbitRequestedEveryNB4() const;

    bool isCommissioningMode() const { return _commissioningMode; } 

    int  getTimerInterval() const { return _timerInterval; }

 protected:
    //
    // Topics and Eventing buses to subscribe
    std::map<std::string, std::string> _subscriptionTopics;

    // Topics and <Eventing Buses + Publication Rate (NB1,NB4, NB64)> to publish
    std::multimap<std::string, std::pair<std::string, std::string> > _publicationTopics;

    xdaq::Application * _application;

    //  XDAQ format of output topics - unsorted
    //
    xdata::Vector<xdata::Properties> _outputtopics;

    // XDAQ format of input topics - unsorted
    //
    xdata::Vector<xdata::Properties> _inputtopics;

    // If commissioning mode is enabled - disable NO_STORE flag if no beam.
    //
    xdata::Boolean    _commissioningMode;

    // Time interval in sec to look up hardware status and maybe to reset boards
    //
    xdata::UnsignedInteger _timerInterval;
};
}  // namespace bcm1futcasource
}  // namespace bril
#endif
