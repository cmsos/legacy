#ifndef _tcds_ttcspy_TCADeviceTTCSpy_h_
#define _tcds_ttcspy_TCADeviceTTCSpy_h_

#include <stdint.h>
#include <vector>

#include "tcds/hwlayertca/TCADeviceBase.h"

namespace tcds {
  namespace ttcspy {

    /**
     * Implementation of the TTCSpy functionality. The TTCSpy is based
     * on the GLIB FMC carrier board.
     */
    class TCADeviceTTCSpy : public tcds::hwlayertca::TCADeviceBase
    {

    public:

      enum TTC_SOURCE {TTC_SOURCE_FRONTPANEL, TTC_SOURCE_BACKPLANE};
      enum LOGGING_MODE {LOGGING_MODE_ONLY, LOGGING_MODE_EXCEPT};
      enum LOGGING_LOGIC {LOGGING_LOGIC_OR, LOGGING_LOGIC_AND};

      TCADeviceTTCSpy();
      ~TCADeviceTTCSpy();

      /* void hwConfigure(); */
      /* void hwEnable(); */
      /* void hwHalt(); */
      /* void hwPause(); */
      /* void hwResume(); */
      /* void hwStop(); */

      /* void selectSource(uint32_t const src) const; */
      /* void setLoggingMode(uint32_t const src) const; */
      /* void setLoggingLogic(uint32_t const src) const; */

      /* void startLogging() const; */
      /* void stopLogging() const; */
      /* void resetLogging() const; */
      /* void restartLogging() const; */
      /* bool isLoggingEnabled() const; */

      /* bool isSpyMemoryFull() const; */
      /* uint32_t getSpyMemoryMaxEntries() const; */
      /* uint32_t getSpyMemoryEntries() const; */
      /* std::vector<uint32_t> readSpyMemory() const; */
      /* void zeroSpyMemory() const; */

      /* bool isCDRLocked() const; */
      /* bool isSFPConnected() const; */
      /* bool isTTCClockUp() const; */

    /* protected: */
    /*   virtual std::string addressTableFileName() const; */
    /*   virtual std::string ipbusProtocolVersion() const; */

    private:
      // Bit positions for both the filter and the trigger mask.
      // NOTE: The extensions of the broadcast system data masks have
      // not (yet) been implemented.
      /* static size_t const kBitPosMaskCommunicationError = 15; */
      /* static size_t const kBitPosMaskDoubleBitError = 14; */
      /* static size_t const kBitPosMaskSingleBitError = 13; */
      /* static size_t const kBitPosMaskL1A = 12; */
      /* static size_t const kBitPosMaskLongCommandsAll = 11; */
      /* static size_t const kBitPosMaskShortCommandsAll = 10; */
      /* static size_t const kBitPosMaskShortUserDataAll = 9; */
      /* static size_t const kBitPosMaskShortSystemDataAll = 8; */
      /* static size_t const kBitPosMaskShortUserDataHi = 7; */
      /* static size_t const kBitPosMaskShortUserDataLo = 6; */
      /* static size_t const kBitPosMaskShortSystemDataHi = 5; */
      /* static size_t const kBitPosMaskShortSystemDataLo = 2; */
      /* static size_t const kBitPosMaskEvCntRes = 1; */
      /* static size_t const kBitPosMaskBCntRes = 0; */

      /* uint32_t createMask(bool const bitBCntRes, */
      /*                     bool const bitEvCntRes, */
      /*                     uint32_t const bitsShortSystemData, */
      /*                     uint32_t const bitsShortUserData, */
      /*                     bool const bitShortSystemDataAll, */
      /*                     bool const bitShortUserDataAll, */
      /*                     bool const bitShortCommandsAll, */
      /*                     bool const bitLongCommandsAll, */
      /*                     bool const bitL1A, */
      /*                     bool const bitSingleBitError, */
      /*                     bool const bitDoubleBitError, */
      /*                     bool const bitCommunicationError) const; */
      /* uint32_t createFilterMask() const; */
      /* uint32_t createTriggerMask() const; */

    };

  } // namespace ttcspy
} // namespace tcds

#endif // _tcds_ttcspy_TCADeviceTTCSpy_h_
