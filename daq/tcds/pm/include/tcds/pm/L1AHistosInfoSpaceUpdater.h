#ifndef _tcds_pm_L1AHistosInfoSpaceUpdater_h_
#define _tcds_pm_L1AHistosInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class L1AHistosInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      L1AHistosInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                tcds::hwlayer::DeviceBase const& hw);
      virtual ~L1AHistosInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_L1AHistosInfoSpaceUpdater_h_
