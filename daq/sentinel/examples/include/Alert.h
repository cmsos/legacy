// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Alert_h_
#define _Alert_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

class Alert: public xdaq::Application, public xgi::framework::UIManager
{	
	public:
	
	XDAQ_INSTANTIATOR();
	
	Alert(xdaq::ApplicationStub * s);
	~Alert();

	void raise(xgi::Input * in, xgi::Output * out ) ;
	void revoke(xgi::Input * in, xgi::Output * out ) ;

	
	void Default(xgi::Input * in, xgi::Output * out ) ;
	
	
};

#endif
