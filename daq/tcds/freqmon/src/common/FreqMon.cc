#include "tcds/freqmon/FreqMon.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdata/Event.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqmon/ConfigurationInfoSpaceHandler.h"
#include "tcds/freqmon/FreqMonInfoSpaceHandler.h"
#include "tcds/freqmon/FreqMonInfoSpaceUpdater.h"
#include "tcds/utils/LogMacros.h"

XDAQ_INSTANTIATOR_IMPL(tcds::freqmon::FreqMon);

tcds::freqmon::FreqMon::FreqMon(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppBase(stub, std::auto_ptr<tcds::hwlayer::DeviceBase>(0)),
    freqMonInfoSpaceUpdaterP_(0),
    freqMonInfoSpaceP_(0),
    freqMonLoop_(*this)
      {
        // Create the InfoSpace holding all configuration information.
        cfgInfoSpaceP_ =
          std::auto_ptr<tcds::freqmon::ConfigurationInfoSpaceHandler>(new tcds::freqmon::ConfigurationInfoSpaceHandler(*this));
      }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the FreqMon application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::freqmon::FreqMon::~FreqMon()
{
}

void
tcds::freqmon::FreqMon::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // We don't have an FSM, so no state either.
  appStateInfoSpace_.setString("stateName", "n/a");
  appStateInfoSpace_.setString("hwLeaseOwnerId", "n/a");

  freqMonInfoSpaceUpdaterP_ = std::auto_ptr<tcds::freqmon::FreqMonInfoSpaceUpdater>(new tcds::freqmon::FreqMonInfoSpaceUpdater(*this, freqMonLoop_));
  freqMonInfoSpaceP_ =
    std::auto_ptr<tcds::freqmon::FreqMonInfoSpaceHandler>(new tcds::freqmon::FreqMonInfoSpaceHandler(*this, freqMonInfoSpaceUpdaterP_.get()));

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  freqMonInfoSpaceP_->registerItemSets(monitor_, webServer_);
}

void
tcds::freqmon::FreqMon::actionPerformed(xdata::Event& event)
{
  tcds::utils::XDAQAppBase::actionPerformed(event);

  if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
      freqMonLoop_.start();
    }
}

void
tcds::freqmon::FreqMon::hwConnectImpl()
{
}

void
tcds::freqmon::FreqMon::hwReleaseImpl()
{
}

void
tcds::freqmon::FreqMon::hwConfigureImpl()
{
}
