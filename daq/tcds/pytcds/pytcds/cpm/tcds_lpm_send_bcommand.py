#!/usr/bin/env python

###############################################################################
## Send a B-command from an LPM/iCI.
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import IPM
from pytcds.utils.tcds_constants import ICI_NAMES
from pytcds.utils.tcds_constants import ICI_NUMBERS
from pytcds.utils.tcds_utils_misc import parse_as_int
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

# NOTE: Little hackish. Since only iCIs can send B-commands, remove
# the iPM from the list.
del ICI_NUMBERS[ICI_NAMES[IPM]]

###############################################################################

class BCommandSender(CmdLineBase):

    BCOMMAND_TYPES = ["long", "short"]

    def pre_hook(self):
        self.ici_number_string = None
        self.ici_number = None
        self.bcommand_data = None
        self.bcommand_type_string = None
        self.bcommand_is_long = False
        # End of pre_hook().

    def handle_args(self):
        # Three arguments are expected. The first argument has to
        # sound like an IP address.
        if len(self.args) != 4:
            msg = "Four arguments are required: " \
                  "the IP address of the device, " \
                  "the iCI number ({0:s}), " \
                  "the B-command data to send and " \
                  "'long' or 'short'."
            self.error(msg.format(", ".join(sorted(ICI_NUMBERS.keys()))))
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like a valid network address."
                msg = msg.format(ip_address_arg)
                self.error(msg)
            else:
                self.ip_address = ip_address
            # Extract the iCI number.
            self.ici_number_string = self.args[1]
            try:
                self.ici_number = ICI_NUMBERS[self.ici_number_string]
            except KeyError:
                msg = "Failed to interpret '{0:s}' as a valid iCI number. " \
                      "Options are: {1:s}."
                self.error(msg.format(self.ici_number_string,
                                      ", ".join(sorted(ICI_NUMBERS.keys()))))
            # Extract the B-command data.
            bcommand_data_tmp = self.args[2]
            try:
                self.bcommand_data = parse_as_int(bcommand_data_tmp)
            except Exception:
                msg = "Failed to interpret '{0:s}' as valid B-command data. "
                self.error(msg.format(bcommand_data_tmp))
            # Extract the B-command type: long or short.
            self.bcommand_type_string = self.args[3]
            if not self.bcommand_type_string in BCommandSender.BCOMMAND_TYPES:
                msg = "Failed to interpret '{0:s}' as a valid B-command type. " \
                      "Options are: {1:s}."
                self.error(msg.format(self.bcommand_type_string,
                                      ", ".join(BCommandSender.BCOMMAND_TYPES)))
            else:
                if self.bcommand_type_string.lower() == "long":
                    self.bcommand_is_long = True
                else:
                    self.bcommand_is_long = False
        # End of handle_args().

    def main(self):
        lpm = get_lpm_hw(self.ip_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            msg = "Sending B-command with data payload 0x{0:x} through iCI #{1:d}"
            print msg.format(self.bcommand_data, self.ici_number)

        #----------

        lpm.enable_ipbus_requests(self.ici_number)
        lpm.send_ipbus_bcommand(self.ici_number, self.bcommand_data, is_long=self.bcommand_is_long)
        lpm.disable_ipbus_requests(self.ici_number)

        #----------

        # End of main().

    # End of class BCommandSender.

###############################################################################

if __name__ == "__main__":

    description = "Send a B-command from an iPM/iCI."
    usage = "usage: %prog [options] IP ICI_NUMBER BCOMMAND_DATA BCOMMAND_TYPE"
    tmp = "ICI_NUMBER: {0:s}, BCOMMAND_TYPE: {1:s}."
    epilog = tmp.format(", ".join(sorted(ICI_NUMBERS.keys())),
                        ", ".join(sorted(BCommandSender.BCOMMAND_TYPES)))
    res = BCommandSender(description, usage, epilog).run()
    print "Done"
    sys.exit(res)

###############################################################################
