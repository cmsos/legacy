#ifndef _tcds_freqmon_FreqMonInfoSpaceHandler_h_
#define _tcds_freqmon_FreqMonInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqmon {

    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;

    class FreqMonInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      FreqMonInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::utils::InfoSpaceUpdater* updater);
      virtual ~FreqMonInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMonInfoSpaceHandler_h_
