// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "sentinel/arc/utils/Node.h"

#include "toolbox/regex.h"
#include "toolbox/string.h"

//std::map<std::string, std::map<std::string, toolbox::Properties> > standingExceptions_;
//std::map<std::string, std::map<std::string, toolbox::Properties> > shelvedExceptions_;

sentinel::arc::utils::Node::Node (sentinel::arc::utils::AbstractModel * model, DOMNode * n)
	: model_(model)
{
	this->label_ = xoap::getNodeAttribute(n, "label");
	this->nodeID_ = model_->getUniqueID();

	DOMNamedNodeMap* attr = n->getAttributes();
	for (unsigned j = 0; j < attr->getLength(); j++)
	{
		DOMNode* attribute = attr->item(j);
		std::string attributeName = xoap::XMLCh2String(attribute->getNodeName());

		if (attributeName != "label")
		{
			std::string attributeValue = xoap::XMLCh2String(attribute->getNodeValue());

			this->regex_[attributeName] = toolbox::parseTokenSet(attributeValue, ",");
		}
	}

	// if it is a leaf
	if (!(n->hasChildNodes()))
	{
		model_->addGuard(this);
	}
	model_->indexNode(this);

	lastUpdate_ = toolbox::TimeVal::gettimeofday();

	//std::cout << "Created new Node : " << nodeID_ << std::endl;
}

sentinel::arc::utils::Node::Node (sentinel::arc::utils::AbstractModel * model, std::string label)
	: label_(label), model_(model)
{
	this->nodeID_ = model_->getUniqueID();
	this->parent_ = 0;
	model_->indexNode(this);
	lastUpdate_ = toolbox::TimeVal::gettimeofday();

	//std::cout << "Created new Node : " << nodeID_ << std::endl;
}

void sentinel::arc::utils::Node::addChild (Node * child)
{
	children_.push_back(child);
}

std::list<sentinel::arc::utils::Node *>& sentinel::arc::utils::Node::getChildren ()
{
	return children_;
}

void sentinel::arc::utils::Node::setParent (Node * parent)
{
	parent_ = parent;
}

sentinel::arc::utils::Node * sentinel::arc::utils::Node::getParent ()
{
	return parent_;
}

bool sentinel::arc::utils::Node::hasExceptions ()
{
	return raisedUnacceptedExceptions_.size() > 0 || raisedUnacceptedExceptions_.size() > 0 || shelvedUnacceptedExceptions_.size() > 0 || shelvedAcceptedExceptions_.size() > 0 || clearedUnacceptedExceptions_.size() > 0 || clearedAcceptedExceptions_.size() > 0;
}

//////////

size_t sentinel::arc::utils::Node::getRaisedUnacceptedExceptionsCount (std::string severity)
{
	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}
	return raisedUnacceptedExceptions_[severity].size();
}

size_t sentinel::arc::utils::Node::getRaisedAcceptedExceptionsCount (std::string severity)
{
	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}
	return raisedAcceptedExceptions_[severity].size();
}

//////////

size_t sentinel::arc::utils::Node::getShelvedUnacceptedExceptionsCount (std::string severity)
{
	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}
	return shelvedUnacceptedExceptions_[severity].size();
}

size_t sentinel::arc::utils::Node::getShelvedAcceptedExceptionsCount (std::string severity)
{
	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}
	return shelvedAcceptedExceptions_[severity].size();
}

//////////

size_t sentinel::arc::utils::Node::getClearedUnacceptedExceptionsCount (std::string severity)
{
	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}
	return clearedUnacceptedExceptions_[severity].size();
}

size_t sentinel::arc::utils::Node::getClearedAcceptedExceptionsCount (std::string severity)
{
	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}
	return clearedAcceptedExceptions_[severity].size();
}

//////////

size_t sentinel::arc::utils::Node::getTotalCount ()
{
	return getTotalRaisedUnacceptedExceptionsCount() + getTotalRaisedAcceptedExceptionsCount() + getTotalShelvedUnacceptedExceptionsCount() + getTotalShelvedAcceptedExceptionsCount() + getTotalClearedUnacceptedExceptionsCount();
	// + getTotalClearedAcceptedExceptionsCount();
}

//////////

size_t sentinel::arc::utils::Node::getTotalRaisedUnacceptedExceptionsCount ()
{
	size_t count = 0;
	for (ExceptionMap::iterator i = raisedUnacceptedExceptions_.begin(); i != raisedUnacceptedExceptions_.end(); i++)
	{
		count += (*i).second.size();
	}
	return count;
}

size_t sentinel::arc::utils::Node::getTotalRaisedAcceptedExceptionsCount ()
{
	size_t count = 0;
	for (ExceptionMap::iterator i = raisedAcceptedExceptions_.begin(); i != raisedAcceptedExceptions_.end(); i++)
	{
		count += (*i).second.size();
	}
	return count;
}

//////////

size_t sentinel::arc::utils::Node::getTotalShelvedUnacceptedExceptionsCount ()
{
	size_t count = 0;
	for (ExceptionMap::iterator i = shelvedUnacceptedExceptions_.begin(); i != shelvedUnacceptedExceptions_.end(); i++)
	{
		count += (*i).second.size();
	}
	return count;
}

size_t sentinel::arc::utils::Node::getTotalShelvedAcceptedExceptionsCount ()
{
	size_t count = 0;
	for (ExceptionMap::iterator i = shelvedAcceptedExceptions_.begin(); i != shelvedAcceptedExceptions_.end(); i++)
	{
		count += (*i).second.size();
	}
	return count;
}

//////////

size_t sentinel::arc::utils::Node::getTotalClearedUnacceptedExceptionsCount ()
{
	size_t count = 0;
	for (ExceptionMap::iterator i = clearedUnacceptedExceptions_.begin(); i != clearedUnacceptedExceptions_.end(); i++)
	{
		count += (*i).second.size();
	}
	return count;
}

size_t sentinel::arc::utils::Node::getTotalClearedAcceptedExceptionsCount ()
{
	size_t count = 0;
	for (ExceptionMap::iterator i = clearedAcceptedExceptions_.begin(); i != clearedAcceptedExceptions_.end(); i++)
	{
		count += (*i).second.size();
	}
	return count;
}

//////////

size_t sentinel::arc::utils::Node::getHighestSeverityValue ()
{
	size_t highestLevel = 0;
	for (ExceptionMap::iterator i = raisedUnacceptedExceptions_.begin(); i != raisedUnacceptedExceptions_.end(); i++)
	{
		if ((*i).second.size() > 0)
		{
			size_t currentLevel = model_->getSeverityLevel((*i).first);
			if (currentLevel > highestLevel)
			{
				highestLevel = currentLevel;
			}
		}
	}
	for (ExceptionMap::iterator i = raisedAcceptedExceptions_.begin(); i != raisedAcceptedExceptions_.end(); i++)
	{
		if ((*i).second.size() > 0)
		{
			size_t currentLevel = model_->getSeverityLevel((*i).first);
			if (currentLevel > highestLevel)
			{
				highestLevel = currentLevel;
			}
		}
	}
	return highestLevel;
}

size_t sentinel::arc::utils::Node::getHighestRaisedUnacceptedSeverityValue ()
{
	size_t highestLevel = 0;
	for (ExceptionMap::iterator i = raisedUnacceptedExceptions_.begin(); i != raisedUnacceptedExceptions_.end(); i++)
	{
		if ((*i).second.size() > 0)
		{
			size_t currentLevel = model_->getSeverityLevel((*i).first);
			if (currentLevel > highestLevel)
			{
				highestLevel = currentLevel;
			}
		}
	}
	return highestLevel;
}

size_t sentinel::arc::utils::Node::getHighestRaisedAcceptedSeverityValue ()
{
	size_t highestLevel = 0;
	for (ExceptionMap::iterator i = raisedAcceptedExceptions_.begin(); i != raisedAcceptedExceptions_.end(); i++)
	{
		if ((*i).second.size() > 0)
		{
			size_t currentLevel = model_->getSeverityLevel((*i).first);
			if (currentLevel > highestLevel)
			{
				highestLevel = currentLevel;
			}
		}
	}
	return highestLevel;
}

bool sentinel::arc::utils::Node::match (toolbox::Properties & exception)
{
	size_t matches = 0;
	for (std::map<std::string, std::set<std::string> >::iterator i = regex_.begin(); i != regex_.end(); i++)
	{
		if (exception.hasProperty((*i).first))
		{
			std::string value = exception.getProperty((*i).first);

			std::set < std::string > &regset = (*i).second;
			for (std::set<std::string>::iterator j = regset.begin(); j != regset.end(); j++)
			{
				if (!(toolbox::regx_match(value, *j)))
				{
					return false;
				}
			}
			matches++;
		}
	}

	if (matches > 0)
	{
		return true;
	}
	return false;
}

// effectively this is the un-shelve command too
bool sentinel::arc::utils::Node::fire (toolbox::Properties & exception)
{
	std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}

	//std::cout << "Looking for " << uuid << " to fire [" << severity << "]" << std::endl;
	if (raisedUnacceptedExceptions_[severity].find(uuid) != raisedUnacceptedExceptions_[severity].end())
	{
		// already there
		return false;
	}
	else if (raisedAcceptedExceptions_[severity].find(uuid) != raisedAcceptedExceptions_[severity].end())
	{
		// already there
		return false;
	}
	else if (clearedUnacceptedExceptions_[severity].find(uuid) != clearedUnacceptedExceptions_[severity].end())
	{

		clearedUnacceptedExceptions_[severity].erase(uuid);
		raisedUnacceptedExceptions_[severity].insert(uuid);

		//return false;
	}
	else if (clearedAcceptedExceptions_[severity].find(uuid) != clearedAcceptedExceptions_[severity].end())
	{

		clearedAcceptedExceptions_[severity].erase(uuid);
		raisedAcceptedExceptions_[severity].insert(uuid);

		//return false;
	}
	else if (shelvedAcceptedExceptions_[severity].find(uuid) != shelvedAcceptedExceptions_[severity].end())
	{
		shelvedAcceptedExceptions_[severity].erase(uuid);
		raisedAcceptedExceptions_[severity].insert(uuid);
	}
	else
	{
		if (shelvedUnacceptedExceptions_[severity].find(uuid) != shelvedUnacceptedExceptions_[severity].end())
		{
			shelvedUnacceptedExceptions_[severity].erase(uuid);
		}
		raisedUnacceptedExceptions_[severity].insert(uuid);
	}

	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	return true;
}

/*
 bool sentinel::arc::utils::Node::fire (toolbox::Properties & exception)
 {
 std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
 std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

 //std::cout << "Looking for " << uuid << " to fire [" << severity << "]" << std::endl;
 if (standingExceptions_[severity].find(uuid) != standingExceptions_[severity].end())
 {
 // already there
 return false;
 }
 else
 {
 if (shelvedExceptions_[severity].find(uuid) != shelvedExceptions_[severity].end())
 {
 // remove from shelved (will be added to standing)
 shelvedExceptions_[severity].erase(uuid);
 }
 }

 // add to standing exceptions
 standingExceptions_[severity].insert(uuid);
 return true;
 }
 */

// Shelving, both one-shot and temporary shelving supported
bool sentinel::arc::utils::Node::shelve (toolbox::Properties & exception)
{
	std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}

	//std::cout << "Looking for " << uuid << " to fire [" << severity << "]" << std::endl;
	if (shelvedUnacceptedExceptions_[severity].find(uuid) != shelvedUnacceptedExceptions_[severity].end())
	{
		return false;
	}
	else if (shelvedAcceptedExceptions_[severity].find(uuid) != shelvedAcceptedExceptions_[severity].end())
	{
		// already shelved , nothing to be done
		return false;
	}
	else if (raisedAcceptedExceptions_[severity].find(uuid) != raisedAcceptedExceptions_[severity].end())
	{
		raisedAcceptedExceptions_[severity].erase(uuid);
		shelvedAcceptedExceptions_[severity].insert(uuid);
	}
	else if (clearedAcceptedExceptions_[severity].find(uuid) != clearedAcceptedExceptions_[severity].end())
	{
		clearedAcceptedExceptions_[severity].erase(uuid);
		shelvedAcceptedExceptions_[severity].insert(uuid);
	}
	else if (clearedUnacceptedExceptions_[severity].find(uuid) != clearedUnacceptedExceptions_[severity].end())
	{
		clearedUnacceptedExceptions_[severity].erase(uuid);
		shelvedUnacceptedExceptions_[severity].insert(uuid);
	}
	else
	{
		if (raisedUnacceptedExceptions_[severity].find(uuid) != raisedUnacceptedExceptions_[severity].end())
		{
			raisedUnacceptedExceptions_[severity].erase(uuid);
		}
		shelvedUnacceptedExceptions_[severity].insert(uuid);
	}

	//std::cout << "uuid " << uuid << " added to shelf in node " << label_ << std::endl;
	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	return true;
}
// Shelving, both one-shot and temporary shelving supported
bool sentinel::arc::utils::Node::clear (toolbox::Properties & exception)
{
	std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}

	//std::cout << "Looking for " << uuid << " to fire [" << severity << "]" << std::endl;
	if (clearedUnacceptedExceptions_[severity].find(uuid) != clearedUnacceptedExceptions_[severity].end())
	{
		return false;
	}
	else if (clearedAcceptedExceptions_[severity].find(uuid) != clearedAcceptedExceptions_[severity].end())
	{
		// already shelved , nothing to be done
		return false;
	}
	else if (shelvedUnacceptedExceptions_[severity].find(uuid) != shelvedUnacceptedExceptions_[severity].end())
	{
		shelvedUnacceptedExceptions_[severity].erase(uuid);
		clearedUnacceptedExceptions_[severity].insert(uuid);
	}
	else if (shelvedAcceptedExceptions_[severity].find(uuid) != shelvedAcceptedExceptions_[severity].end())
	{
		shelvedAcceptedExceptions_[severity].erase(uuid);
		clearedAcceptedExceptions_[severity].insert(uuid);
	}
	else if (raisedAcceptedExceptions_[severity].find(uuid) != raisedAcceptedExceptions_[severity].end())
	{
		raisedAcceptedExceptions_[severity].erase(uuid);
		clearedAcceptedExceptions_[severity].insert(uuid);
	}
	else
	{
		if (raisedUnacceptedExceptions_[severity].find(uuid) != raisedUnacceptedExceptions_[severity].end())
		{
			raisedUnacceptedExceptions_[severity].erase(uuid);
		}
		clearedUnacceptedExceptions_[severity].insert(uuid);
	}

	//std::cout << "uuid " << uuid << " added to shelf in node " << label_ << std::endl;
	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	return true;
}

/*
 bool sentinel::arc::utils::Node::shelve (toolbox::Properties & exception)
 {
 std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
 std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

 //std::cout << "Looking for " << uuid << " to fire [" << severity << "]" << std::endl;
 if (standingExceptions_[severity].find(uuid) != standingExceptions_[severity].end())
 {
 // remove from standing (will be added to shelved)
 standingExceptions_[severity].erase(uuid);
 }
 else if (shelvedExceptions_[severity].find(uuid) != shelvedExceptions_[severity].end())
 {
 // already shelved , nothing to be done
 return false;
 }

 std::cout << "uuid " << uuid << " added to shelf in node " << label_ << std::endl;
 shelvedExceptions_[severity].insert(uuid);
 return true;
 }
 */

bool sentinel::arc::utils::Node::revoke (toolbox::Properties & exception)
{
	std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}

	//std::cout << "Looking for " << uuid << " to suppress [" << severity << "]" << std::endl;
	if (shelvedUnacceptedExceptions_[severity].find(uuid) != shelvedUnacceptedExceptions_[severity].end())
	{
		shelvedUnacceptedExceptions_[severity].erase(uuid);
		clearedUnacceptedExceptions_[severity].insert(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	else if (shelvedAcceptedExceptions_[severity].find(uuid) != shelvedAcceptedExceptions_[severity].end())
	{
		shelvedAcceptedExceptions_[severity].erase(uuid);
		clearedAcceptedExceptions_[severity].insert(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	else if (raisedAcceptedExceptions_[severity].find(uuid) != raisedAcceptedExceptions_[severity].end())
	{
		raisedAcceptedExceptions_[severity].erase(uuid);
		clearedAcceptedExceptions_[severity].insert(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	else if (raisedUnacceptedExceptions_[severity].find(uuid) != raisedUnacceptedExceptions_[severity].end())
	{
		raisedUnacceptedExceptions_[severity].erase(uuid);
		clearedUnacceptedExceptions_[severity].insert(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}

	return false;
}

/*
 bool sentinel::arc::utils::Node::revoke (toolbox::Properties & exception)
 {
 std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
 std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

 //std::cout << "Looking for " << uuid << " to suppress [" << severity << "]" << std::endl;
 if (standingExceptions_[severity].find(uuid) != standingExceptions_[severity].end())
 {
 standingExceptions_[severity].erase(uuid);
 suppressedExceptions_[severity].insert(uuid);
 return true;
 }
 else if (shelvedExceptions_[severity].find(uuid) != shelvedExceptions_[severity].end())
 {
 shelvedExceptions_[severity].erase(uuid);
 suppressedExceptions_[severity].insert(uuid);
 return true;
 }

 return false;
 }
 */

// Accept not implemented for shelved items
bool sentinel::arc::utils::Node::accept (toolbox::Properties & exception)
{
	std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}

	//std::cout << "Looking for " << uuid << " to fire [" << severity << "]" << std::endl;
	if (raisedUnacceptedExceptions_[severity].find(uuid) != raisedUnacceptedExceptions_[severity].end())
	{
		raisedUnacceptedExceptions_[severity].erase(uuid);
		raisedAcceptedExceptions_[severity].insert(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	else if (clearedUnacceptedExceptions_[severity].find(uuid) != clearedUnacceptedExceptions_[severity].end())
	{
		clearedUnacceptedExceptions_[severity].erase(uuid);
		clearedAcceptedExceptions_[severity].insert(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	else if (shelvedUnacceptedExceptions_[severity].find(uuid) != shelvedUnacceptedExceptions_[severity].end())
	{
		shelvedUnacceptedExceptions_[severity].erase(uuid);
		shelvedAcceptedExceptions_[severity].insert(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	else if (raisedAcceptedExceptions_[severity].find(uuid) != raisedAcceptedExceptions_[severity].end())
	{
		return false;
	}
	else if (clearedAcceptedExceptions_[severity].find(uuid) != clearedAcceptedExceptions_[severity].end())
	{
		return false;
	}
	else if (shelvedAcceptedExceptions_[severity].find(uuid) != shelvedAcceptedExceptions_[severity].end())
	{
		return false;
	}

	raisedAcceptedExceptions_[severity].insert(uuid); // this is allowed for playback compatibility
	lastUpdate_ = toolbox::TimeVal::gettimeofday();
	return true;
}

bool sentinel::arc::utils::Node::repack (toolbox::Properties & exception)
{
	std::string uuid = exception.getProperty(sentinel::arc::utils::EXCEPTION_UUID_ATTR);
	std::string severity = toolbox::tolower(exception.getProperty(sentinel::arc::utils::SEVERITY_ATTR));

	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}

	//std::cout << "Looking for " << uuid << " to clear [" << severity << "]" << std::endl;
	if (clearedAcceptedExceptions_[severity].find(uuid) != clearedAcceptedExceptions_[severity].end())
	{
		clearedAcceptedExceptions_[severity].erase(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	return false;
}

std::set<std::string> sentinel::arc::utils::Node::getShelvedExceptions ()
{
	std::set < std::string > ret;

	for (ExceptionMap::iterator i = shelvedUnacceptedExceptions_.begin(); i != shelvedUnacceptedExceptions_.end(); i++)
	{
		ret.insert((*i).second.begin(), (*i).second.end());
	}
	for (ExceptionMap::iterator i = shelvedAcceptedExceptions_.begin(); i != shelvedAcceptedExceptions_.end(); i++)
	{
		ret.insert((*i).second.begin(), (*i).second.end());
	}

	return ret;
}

std::set<std::string> sentinel::arc::utils::Node::getRaisedExceptions ()
{
	std::set < std::string > ret;

	for (ExceptionMap::iterator i = raisedUnacceptedExceptions_.begin(); i != raisedUnacceptedExceptions_.end(); i++)
	{
		ret.insert((*i).second.begin(), (*i).second.end());
	}
	for (ExceptionMap::iterator i = raisedAcceptedExceptions_.begin(); i != raisedAcceptedExceptions_.end(); i++)
	{
		ret.insert((*i).second.begin(), (*i).second.end());
	}

	return ret;
}

std::map<std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus> sentinel::arc::utils::Node::getExceptions ()
{
	std::map<std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus> exceptions;

	for (ExceptionMap::iterator i = raisedUnacceptedExceptions_.begin(); i != raisedUnacceptedExceptions_.end(); i++)
	{
		for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			exceptions[*j] = sentinel::arc::utils::AbstractModel::RAISED_UNACCEPTED;
		}
	}

	for (ExceptionMap::iterator i = raisedAcceptedExceptions_.begin(); i != raisedAcceptedExceptions_.end(); i++)
	{
		for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			exceptions[*j] = sentinel::arc::utils::AbstractModel::RAISED_ACCEPTED;
		}
	}

	for (ExceptionMap::iterator i = shelvedUnacceptedExceptions_.begin(); i != shelvedUnacceptedExceptions_.end(); i++)
	{
		for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			exceptions[*j] = sentinel::arc::utils::AbstractModel::SHELVED_UNACCEPTED;
		}
	}

	for (ExceptionMap::iterator i = shelvedAcceptedExceptions_.begin(); i != shelvedAcceptedExceptions_.end(); i++)
	{
		for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			exceptions[*j] = sentinel::arc::utils::AbstractModel::SHELVED_ACCEPTED;
		}
	}

	for (ExceptionMap::iterator i = clearedUnacceptedExceptions_.begin(); i != clearedUnacceptedExceptions_.end(); i++)
	{
		for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			exceptions[*j] = sentinel::arc::utils::AbstractModel::CLEARED_UNACCEPTED;
		}
	}

	for (ExceptionMap::iterator i = clearedAcceptedExceptions_.begin(); i != clearedAcceptedExceptions_.end(); i++)
	{
		for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
		{
			exceptions[*j] = sentinel::arc::utils::AbstractModel::CLEARED_ACCEPTED;
		}
	}

	return exceptions;
}

std::map<std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus> sentinel::arc::utils::Node::getExceptions (std::string listName)
{
	std::map<std::string, sentinel::arc::utils::AbstractModel::ExceptionStatus> exceptions;

	if (listName == "raisedUnaccepted")
	{
		for (ExceptionMap::iterator i = raisedUnacceptedExceptions_.begin(); i != raisedUnacceptedExceptions_.end(); i++)
		{
			for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
			{
				exceptions[*j] = sentinel::arc::utils::AbstractModel::RAISED_UNACCEPTED;
			}
		}
	}
	else if (listName == "raisedAccepted")
	{
		for (ExceptionMap::iterator i = raisedAcceptedExceptions_.begin(); i != raisedAcceptedExceptions_.end(); i++)
		{
			for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
			{
				exceptions[*j] = sentinel::arc::utils::AbstractModel::RAISED_ACCEPTED;
			}
		}
	}
	else if (listName == "shelvedUnaccepted")
	{
		for (ExceptionMap::iterator i = shelvedUnacceptedExceptions_.begin(); i != shelvedUnacceptedExceptions_.end(); i++)
		{
			for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
			{
				exceptions[*j] = sentinel::arc::utils::AbstractModel::SHELVED_UNACCEPTED;
			}
		}
	}
	else if (listName == "shelvedAccepted")
	{
		for (ExceptionMap::iterator i = shelvedAcceptedExceptions_.begin(); i != shelvedAcceptedExceptions_.end(); i++)
		{
			for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
			{
				exceptions[*j] = sentinel::arc::utils::AbstractModel::SHELVED_ACCEPTED;
			}
		}
	}
	else if (listName == "clearedUnaccepted")
	{
		for (ExceptionMap::iterator i = clearedUnacceptedExceptions_.begin(); i != clearedUnacceptedExceptions_.end(); i++)
		{
			for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
			{
				exceptions[*j] = sentinel::arc::utils::AbstractModel::CLEARED_UNACCEPTED;
			}
		}

	}
	else if (listName == "clearedAccepted")
	{
		for (ExceptionMap::iterator i = clearedAcceptedExceptions_.begin(); i != clearedAcceptedExceptions_.end(); i++)
		{
			for (std::set<std::string>::iterator j = (*i).second.begin(); j != (*i).second.end(); j++)
			{
				exceptions[*j] = sentinel::arc::utils::AbstractModel::CLEARED_ACCEPTED;
			}
		}
	}

	return exceptions;
}

std::set<std::string> sentinel::arc::utils::Node::getExceptions (std::string listName, std::string severity)
{
	if (model_->getSeverityLevel(severity) == 0)
	{
		severity = "unknown";
	}

	if (listName == "raisedUnaccepted")
	{
		return raisedUnacceptedExceptions_[severity];
	}
	else if (listName == "raisedAccepted")
	{
		return raisedAcceptedExceptions_[severity];
	}
	else if (listName == "shelvedUnaccepted")
	{
		return shelvedUnacceptedExceptions_[severity];
	}
	else if (listName == "shelvedAccepted")
	{
		return shelvedAcceptedExceptions_[severity];
	}
	else if (listName == "clearedUnaccepted")
	{
		return clearedUnacceptedExceptions_[severity];
	}
	else if (listName == "clearedAccepted")
	{
		return clearedAcceptedExceptions_[severity];
	}
	std::set<std::string> emptySet;
	return emptySet;
}

bool sentinel::arc::utils::Node::garbage (const std::string & uuid, const std::string & severity)
{
	if (raisedUnacceptedExceptions_[severity].find(uuid) != raisedUnacceptedExceptions_[severity].end())
	{
		raisedUnacceptedExceptions_[severity].erase(uuid);
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		return true;
	}
	if (raisedAcceptedExceptions_[severity].find(uuid) != raisedAcceptedExceptions_[severity].end())
	{
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		raisedAcceptedExceptions_[severity].erase(uuid);
		return true;
	}
	if (shelvedUnacceptedExceptions_[severity].find(uuid) != shelvedUnacceptedExceptions_[severity].end())
	{
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		shelvedUnacceptedExceptions_[severity].erase(uuid);
		return true;
	}
	if (shelvedAcceptedExceptions_[severity].find(uuid) != shelvedAcceptedExceptions_[severity].end())
	{
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		shelvedAcceptedExceptions_[severity].erase(uuid);
		return true;
	}
	if (clearedUnacceptedExceptions_[severity].find(uuid) != clearedUnacceptedExceptions_[severity].end())
	{
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		clearedUnacceptedExceptions_[severity].erase(uuid);
		return true;
	}
	if (clearedAcceptedExceptions_[severity].find(uuid) != clearedAcceptedExceptions_[severity].end())
	{
		lastUpdate_ = toolbox::TimeVal::gettimeofday();
		clearedAcceptedExceptions_[severity].erase(uuid);
		return true;
	}
	return false;
}
