#ifndef _tcdsfreqmon_version_h_
#define _tcdsfreqmon_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSFREQMON_VERSION_MAJOR 3
#define TCDSFREQMON_VERSION_MINOR 13
#define TCDSFREQMON_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSFREQMON_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSFREQMON_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSFREQMON_VERSION_CODE PACKAGE_VERSION_CODE(TCDSFREQMON_VERSION_MAJOR,TCDSFREQMON_VERSION_MINOR,TCDSFREQMON_VERSION_PATCH)
#ifndef TCDSFREQMON_PREVIOUS_VERSIONS
#define TCDSFREQMON_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSFREQMON_VERSION_MAJOR,TCDSFREQMON_VERSION_MINOR,TCDSFREQMON_VERSION_PATCH)
#else
#define TCDSFREQMON_FULL_VERSION_LIST TCDSFREQMON_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSFREQMON_VERSION_MAJOR,TCDSFREQMON_VERSION_MINOR,TCDSFREQMON_VERSION_PATCH)
#endif

namespace tcdsfreqmon
{
  const std::string package = "tcdsfreqmon";
  const std::string versions = TCDSFREQMON_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS frequency-counter.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS frequency counter.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
