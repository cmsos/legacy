###############################################################################

import nose.tools
import os
import subprocess
import unittest

from nose.tools import assert_equal

from pytcds.tests import expected_failure
from pytcds.tests import settings
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_constants import BOARD_TYPE_GLIB
from pytcds.utils.tcds_glib import get_glib_hw
from pytcds.utils.tcds_utils_hw_id import identify_board
from pytcds.utils.tcds_utils_hw_id import identify_carrier

###############################################################################

def setup():
    TestGLIB().check_setup()
    # End of setup().

###############################################################################

class TestGLIB(object):

    def __init__(self):
        self.carrier_type = BOARD_TYPE_GLIB
        self.board_type = BOARD_TYPE_GLIB

        self.address = settings.glib.address
        self.port = settings.glib.port
        self.controlhub_address = settings.controlhub.address
        self.controlhub_port = settings.controlhub.port

        self.hw = None
        # End of __init__().

    def setup(self):
        # Connect to the board.
        try:
            self.hw = get_glib_hw(self.address, self.controlhub_address)
        except Exception, err:
            controlhub_str = ""
            if self.controlhub_address:
                controlhub_str = " (using controlhub at IP address {0:s})".format(self.controlhub_address)
            msg = "Could not connect to the GLIB at IP address {0:s}{1:s}."
            msg = msg.format(self.address, controlhub_str)
            raise nose.SkipTest(msg)
        # End of setup().

    def check_setup(self):
        # Check if the controlhub can be reached (if used).
        address = settings.controlhub.address
        if address:
            try:
                self.verify_pingable(address)
            except subprocess.CalledProcessError:
                msg = "Could not reach the controlhub (at address '{0:s}')."
                raise nose.SkipTest(msg.format(address))

        # Check if the board itself can be reached.
        try:
            self.verify_pingable()
        except subprocess.CalledProcessError:
            msg = "Could not reach the board (at address '{0:s}')."
            raise nose.SkipTest(msg.format(self.address))

        # Check if the address points to the right type of carrier.
        try:
            self.verify_carrier_identity()
        except RuntimeError, err:
            raise nose.SkipTest(err)

        TestGLIB.setup(self)

        # TODO TODO TODO
        # Waiting for the controlhub connection-recovery saga to end.
        # # Jump to the user firmware image if necessary.
        # # self.hw.ensure_user_firmware_loaded()
        self.hw.load_firmware("user")
        # # TODO TODO TODO end
        # End of check_setup().

    def fail(self, msg=None):
        raise unittest.TestCase.failureException, msg

    def verify_board_identity(self):
        board_type = identify_board(self.address)
        if board_type != self.board_type:
            msg = "The board at address '{0:s}' is not a '{1:s}' but a '{2:s}'."
            raise RuntimeError(msg.format(self.address,
                                          board_names[self.board_type],
                                          board_names[board_type]))
        # End of verify_board_identity().

    def verify_carrier_identity(self):
        carrier_type = identify_carrier(self.address)
        if carrier_type != self.carrier_type:
            msg = "The board carrier at address '{0:s}' is not a '{1:s}' but a '{2:s}'."
            raise RuntimeError(msg.format(self.address,
                                          board_names[self.carrier_type],
                                          board_names[carrier_type]))
        # End of verify_carrier_identity().

    def verify_pingable(self, address=None):
        if address is None:
            address = self.address
        deadline = settings.tests.ping_deadline
        subprocess.check_call(["ping",
                               "-c", "1",
                               "-w", "{0:d}".format(deadline),
                               address],
                              stdout=open(os.devnull, "w"))
        # End of verify_pingable().

    def test_glib_i2c(self):
        try:
            eui = self.hw.get_eui()
        except Exception:
            self.fail("Failed to read EUI. I2C does not work?")
        # End of test_glib_i2c().

    def _test_ipbus_read_write(self, reg_name, value):
        self.hw.write(reg_name, value)
        tmp = self.hw.read(reg_name)
        msg = "Wrote 0x{0:x} to {1:s} but read back 0x{2:x}."
        assert_equal(tmp, value, msg.format(value, reg_name, tmp))
        # End of _test_ipbus_read_write().

    def test_ipbus_read_write_system(self):
        # NOTE: Only a quick check of the system-logic address space.
        values = [0, 42]
        reg_name = "glib.testreg"
        for val in values:
            yield self._test_ipbus_read_write, reg_name, val
        # End of test_ipbus_read_write_system().

    # End of class TestGLIB.

###############################################################################

class TestGlib(TestGLIB):
    pass

###############################################################################
