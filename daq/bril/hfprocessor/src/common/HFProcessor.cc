#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "b2in/nub/Method.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"
#include "bril/hfprocessor/HFProcessor.h" 
#include "bril/hfprocessor/exception/Exception.h"
#include <math.h>
#include <map>
#include <algorithm>
#include <stdio.h>
#include <string.h>

XDAQ_INSTANTIATOR_IMPL (bril::hfprocessor::HFProcessor)

using namespace interface::bril;
 
bril::hfprocessor::HFProcessor::HFProcessor (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::hfprocessor::HFProcessor::Default,"Default");
    b2in::nub::bind(this, &bril::hfprocessor::HFProcessor::onMessage);
    m_appInfoSpace = getApplicationInfoSpace();
    m_appDescriptor= getApplicationDescriptor();
    m_classname    = m_appDescriptor->getClassName();
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    m_collectnb = 4;
    m_nHistsDiscarded = 0;
    m_alignedNB4 = false;
    lumiTotal = 0;
    muTotal = 0;
    computeLumiNow = false;
    saneData = false;
    rejectHistUntilNewLN = false;
    m_vdmflagTopic.fromString("vdmflag");
    m_vdmflag = false;

    lastPubRun=1;
    lastPubLS=1;
    lastPubLN=1;
    lastPub=1;
    hfAfterGlowTotalScale=1;

    thisLumiTopic="";

    m_mon_nbMod4PerBoard.resize(36, -1);
    m_mon_lumiPerBoard.resize(36, 0);
    m_mon_bunchMask.resize(interface::bril::MAX_NBX,0);
    m_mon_nActiveBX = 0;
    m_mon_nBoards   = 0;
    m_mon_nCollected=0;
    BMthresET= 0.001;
    BMthresOC= 8e-6;
    NearNoiseValue = 1e-4;
    AboveNoiseValue = 1e-5;
    BMmin = 1e-2;
    HCALMaskLow = 3481;
    HCALMaskHigh = 3499;
    MAX_NBX_noAG = 3480;
    
    m_beammode = "";
        
    afterglowBufRef=0;
    pedBufRef = 0;
    histBufRef = 0;
    bxBufRef = 0;
  
    debugStatement.str("");
    debugStatement.precision(3);
    
    toolbox::net::URN memurn("toolbox-mem-pool",m_classname+"_mem"); 
    try{
        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool = m_poolFactory->createPool(memurn,allocator);
        m_appInfoSpace->fireItemAvailable("eventinginput",&m_datasources);
        LOG4CPLUS_DEBUG(getApplicationLogger(), "got input eventing topics");
        m_appInfoSpace->fireItemAvailable("eventingoutput",&m_outputtopics);
        LOG4CPLUS_DEBUG(getApplicationLogger(), "got output eventing topics");
        m_appInfoSpace->fireItemAvailable("calibtag",&m_calibtag);
        m_appInfoSpace->addListener(this,"urn:xdaq-event:setDefaultValues");
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_publishing","waiting");
   
        //CP my stuff
        m_appInfoSpace->fireItemAvailable("sigmaVis",                 &sigmaVis);
        m_appInfoSpace->fireItemAvailable("lumiAlgo",                 &lumiAlgo);
        m_appInfoSpace->fireItemAvailable("quadraticCorrection",      &quadraticCorrection);
        m_appInfoSpace->fireItemAvailable("orbits_per_nibble",        &orbits_per_nibble);
        m_appInfoSpace->fireItemAvailable("integration_period_nb",    &integration_period_nb);
        m_appInfoSpace->fireItemAvailable("nibbles_per_section",      &nibbles_per_section);
        m_appInfoSpace->fireItemAvailable("applyAfterglowCorr",       &applyAfterglowCorr);
        m_appInfoSpace->fireItemAvailable("vdmflagTopic",             &m_vdmflagTopic);
        m_appInfoSpace->fireItemAvailable("BMthresET",                &BMthresET);
        m_appInfoSpace->fireItemAvailable("BMthresOC",                &BMthresOC);
        m_appInfoSpace->fireItemAvailable("NearNoiseValue",           &NearNoiseValue);
        m_appInfoSpace->fireItemAvailable("AboveNoiseValue",          &AboveNoiseValue);
        m_appInfoSpace->fireItemAvailable("BMmin",                    &BMmin);

    } catch(xcept::Exception& e){
        std::string msg("Failed to setup memory pool ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        //XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg,e);  
    }

    SetupMonitoring();
}

bril::hfprocessor::HFProcessor::~HFProcessor (){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    delete it->second;
  }
  m_topicoutqueues.clear();
}


// This makes the HyperDAQ page
void bril::hfprocessor::HFProcessor::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){
  //m_applock.take();

  ///////*out << busesToHTML();
  ///////std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();
  ///////*out << "URL: "<< appurl;
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::caption("input/output");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("calibtag");
  ///////*out << cgicc::td( m_calibtag );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("sigmaVis");
  ///////*out << cgicc::td( sigmaVis.toString() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("in_topic");
  ///////*out << cgicc::td( interface::bril::hfCMS1T::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("out_topic");
  ///////*out << cgicc::td( interface::bril::hfOcc1AggT::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("out_topic");
  ///////*out << cgicc::td( interface::bril::hfoclumiT::topicname() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("# hists discarded on LS/run boundary");
  ///////*out << cgicc::td( m_nHistsDiscarded.toString() );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();  
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();

  ///////*out << "Automatic channel masking is ";
  ///////if (m_useAutomaticChannelMasking)
  ///////  {
  ///////    *out << "ON";
  ///////  }
  ///////else
  ///////  {
  ///////    *out << "OFF";
  ///////  }
  ///////*out << cgicc::br();
  ///////*out << cgicc::br();

  ///////*out << "Permanently-masked channels: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-horizontal");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  /////////  *out << cgicc::th("Channels");
  ///////for (xdata::Integer channel = 0; channel < 48; channel++)
  ///////  {
  ///////    if (!m_useChannelForLumi[channel.value_])
  ///////{
  ///////  *out << cgicc::td( channel.toString() );
  ///////}
  ///////  }
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "Temporarily-masked channels: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-horizontal");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  /////////  *out << cgicc::th("Channels");
  ///////for (xdata::Integer channel = 0; channel < 48; channel++)
  ///////  {
  ///////    if (!boardAvailableThisBlock[channel.value_])
  ///////{
  ///////  *out << cgicc::td( channel.toString() );
  ///////}
  ///////  }
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "The current background numbers ARE: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("BG 1");
  ///////*out << cgicc::td( m_BG1_plus.toString() );
  ///////// *out << cgicc::td( &m_BG1_plus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("BG 2");
  ///////*out << cgicc::td( m_BG2_minus.toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("How many times is albedo higher than background?");
  ///////*out << cgicc::td( xdata::Integer(m_countAlbedoHigherThanBackground).toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "And the current lumi average numbers ARE: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("Lumi (calibrated)");
  ///////*out << cgicc::td( lumiTotal.toString() );
  ///////// *out << cgicc::td( &m_BG1_plus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tr();
  ///////*out << cgicc::th("Lumi (raw)");
  ///////*out << cgicc::td( lumiTotal_raw.toString() );
  ///////// *out << cgicc::td( &m_BG2_minus );
  ///////*out << cgicc::tr();

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();


  ///////*out << "First few elements from lumi histogram: ";
  ///////*out << cgicc::br();
  ///////*out << cgicc::table().set("class","xdaq-table-vertical");
  ///////*out << cgicc::tbody();

  ///////for (unsigned int bin = 0; bin < 20; bin++)
  ///////  {
  ///////    *out << cgicc::tr();
  ///////    *out << cgicc::th( xdata::Integer(bin).toString() );
  ///////    *out << cgicc::td( xdata::Float(lumiHistPerBX[bin]).toString() );
  ///////    *out << cgicc::tr();
  ///////  }

  ///////*out << cgicc::tbody();
  ///////*out << cgicc::table();
  ///////*out << cgicc::br();

  //m_applock.give();
}

void bril::hfprocessor::HFProcessor::SetupMonitoring(){
    LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
    std::string monurn = createQualifiedInfoSpace( "hfProcessorMon" ).toString();
    m_mon_infospace = xdata::getInfoSpaceFactory()->get( monurn );
    const int nvars = 11;
    const char* names[nvars] = { "fill", "run", "ls", "nb", "timestamp", "nActiveBX", "nBoards", "statsCollected", "nbMod4PerBoard", "lumiPerBoard", "bunchMask"};
    xdata::Serializable* vars[nvars] = { &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp, &m_mon_nActiveBX, &m_mon_nBoards, &m_mon_nCollected, &m_mon_nbMod4PerBoard, &m_mon_lumiPerBoard, &m_mon_bunchMask };
    for ( int i = 0; i != nvars; ++i ){
        m_mon_varlist.push_back( names[i] );
        m_mon_vars.push_back( vars[i] );
        m_mon_infospace->fireItemAvailable( names[i], vars[i] );
    }
}


// Sets default values of application according to parameters in the 
// xml configuration file.  Listens for xdata::Event and reacts when it's of type
// "urn:xdaq-event:setDefaultValues"
void bril::hfprocessor::HFProcessor::actionPerformed(xdata::Event& e){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    std::stringstream msg;
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
        size_t nsources = m_datasources.elements();
        try{
            for(size_t i=0;i<nsources;++i){
                xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_datasources.elementAt(i));
                xdata::String databus;
                xdata::String topicsStr;
                if(p){
                  databus = p->getProperty("bus");     
                  topicsStr = p->getProperty("topics");
                  std::set<std::string> topics = toolbox::parseTokenSet(topicsStr.value_,",");     
                  m_in_busTotopics.insert(std::make_pair(databus.value_,topics));
                }
            }
            subscribeall();
            size_t ntopics = m_outputtopics.elements();
            for(size_t i=0;i<ntopics;++i){
                xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_outputtopics.elementAt(i));
                if(p){
                  xdata::String topicname = p->getProperty("topic");
                  xdata::String outputbusStr = p->getProperty("buses");
                  std::set<std::string> outputbuses = toolbox::parseTokenSet(outputbusStr.value_,",");
                  for(std::set<std::string>::iterator it=outputbuses.begin(); it!=outputbuses.end(); ++it){
                    m_out_topicTobuses.insert(std::make_pair(topicname.value_,*it));
                  }
                  m_topicoutqueues.insert(std::make_pair(topicname.value_,new toolbox::squeue<toolbox::mem::Reference*>));
                  m_unreadybuses.insert(outputbuses.begin(),outputbuses.end());
                }
            }
            for(std::set<std::string>::iterator it=m_unreadybuses.begin(); it!=m_unreadybuses.end(); ++it){
                debugStatement<<"listening?"<<*it;
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                this->getEventingBus(*it).addActionListener(this);
            }
    
            integration_period_orbits=integration_period_nb.value_ * orbits_per_nibble.value_;
            integration_period_secs=integration_period_orbits / 11245.6;
            
            if (lumiAlgo.value_ == "occupancy") {
                thisLumiTopic.append(interface::bril::hfoclumiT::topicname());
            } else if (lumiAlgo.value_ == "etsum") {
                thisLumiTopic.append(interface::bril::hfetlumiT::topicname());
            }
            InitializeHFSBR(); //need lumiAlgo
    
        }catch(xdata::exception::Exception& e){
            msg<<"Failed to parse application property";
            LOG4CPLUS_ERROR(getApplicationLogger(), msg);
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception, msg.str(), e);
        }      
    }
}

// When buses are ready to publish, start the publishing workloop and 
// timer to check for stale cache. Listens for toolbox::Event
void  bril::hfprocessor::HFProcessor::actionPerformed(toolbox::Event& e){
    if(e.type() == "eventing::api::BusReadyToPublish"){
        std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
        std::stringstream msg;
        msg<< "event Bus '" << busname << "' is ready to publish";
        m_unreadybuses.erase(busname);
        if(m_unreadybuses.size()!=0) return; //wait until all buses are ready
        try{    
            toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this,&bril::hfprocessor::HFProcessor::publishing,"publishing");
            m_publishing->activate();
            m_publishing->submit(as_publishing);
        }catch(toolbox::task::exception::Exception& e){
            msg<<"Failed to start publishing workloop "<<stdformat_exception_history(e);
            LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg.str(),e);  
        }    
        try{
            std::string appuuid = m_appDescriptor->getUUID().toString();
            // check timeExpired every 120 seconds
            // huge time right now because timeExpired is currently doing nothing
            toolbox::TimeInterval checkinterval(120,0);
            std::string timername("stalecachecheck_timer");
            toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername+"_"+appuuid);
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate(start, this, checkinterval, 0, timername);
        }catch(toolbox::exception::Exception& e){
            std::string msg("failed to start timer ");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e) );
//            XCEPT_RETHROW(bril::hfprocessor::exception::Exception,msg,e);
        }
    }
}

// subscribe to all input buses
void bril::hfprocessor::HFProcessor::subscribeall(){
    for(std::map<std::string, std::set<std::string> >::iterator bit=m_in_busTotopics.begin(); bit!=m_in_busTotopics.end(); ++bit ){
        std::string busname = bit->first; 
        for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){       
            LOG4CPLUS_INFO(getApplicationLogger(),"subscribing "+busname+":"+*topicit); 
            try{
                this->getEventingBus(busname).subscribe(*topicit);
            }catch(eventing::api::exception::Exception& e){
                LOG4CPLUS_ERROR(getApplicationLogger(),"failed to subscribe, remove topic "+stdformat_exception_history(e));
                m_in_busTotopics[busname].erase(*topicit);
            }
        }
    }
}

// Listens for reception of data on the eventing.  
// If full statistics collected, call do_process on the already-accumulated nibbles.
void bril::hfprocessor::HFProcessor::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception){
    debugStatement<<"In onMessage";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        curTopic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        std::string v = plist.getProperty("DATA_VERSION");
        if(v.empty()){
            std::string msg("Received data message without version header, do nothing");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            if(ref!=0){
                ref->release();
                ref=0;
            }
            return;
        }
        if(v!=interface::bril::DATA_VERSION){
            std::string msg("Mismatched bril data version in received message, do nothing");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            if(ref!=0){
                ref->release();
                ref=0;
            }
            return;
        }    
    
        debugStatement<<"Received data from "+topic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");

        interface::bril::DatumHead* inheader = (interface::bril::DatumHead*)(ref->getDataLocation());

        if( topic == m_vdmflagTopic.value_ ){
            bool flag = false;
            interface::bril::vdmflagT* me = (interface::bril::vdmflagT*)( ref->getDataLocation() );
            flag = me->payload()[0];

            debugStatement<<"cur flag in prog "<<m_vdmflag<<" flag from topic "<<flag ;
            LOG4CPLUS_DEBUG(getApplicationLogger(),debugStatement.str());
            debugStatement.str("");
            
            if( m_vdmflag==true && flag==false ){
                m_vdmflag = false;
                LOG4CPLUS_INFO(getApplicationLogger(), "TURNING OFF VDMFLAG--BUNCHMASK IS DYNAMIC");
            }else if(  m_vdmflag==false && flag==true ){
                m_vdmflag = true;
                LOG4CPLUS_INFO(getApplicationLogger(), "TURNING ON VDMFLAG--BUNCHMASK IS FIXED");
            }
        }

        if( topic == interface::bril::hfCMS1T::topicname() ){
            if (lumiAlgo == "occupancy") {
                RetrieveSourceHistos(ref, inheader);
            } else {
                LOG4CPLUS_ERROR(getApplicationLogger(),"CMS1 topic found but you aren't running occupancy lumi.");
            }
        } else if( topic == interface::bril::hfCMS_ETT::topicname()) {
            if (lumiAlgo == "etsum") {
                RetrieveSourceHistos(ref, inheader);
            } else {
                LOG4CPLUS_ERROR(getApplicationLogger(),"CMS_ET topic found but you aren't running etsum lumi.");
            }
        } else if( topic == interface::bril::hfCMS_VALIDT::topicname() ) {
            RetrieveSourceHistos(ref, inheader);
        }
    }
    if(ref!=0){
        ref->release();
        ref=0;
    }
}

// When HF data received on eventing, check and store it
void bril::hfprocessor::HFProcessor::RetrieveSourceHistos(toolbox::mem::Reference * ref, interface::bril::DatumHead * inheader) {
    // assume data is guilty until proven innocent
    saneData = false;
    newLumiBlock = false;

    debugStatement<<"START:  NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    unsigned int crate     = inheader->getDataSourceID();
    unsigned int board     = inheader->getChannelID();
    unsigned int boardID   = crate*100+board;
    unsigned int boardIndex = (board - 1) + (crate==32)*24 + (crate==29)*12;
    debugStatement<<"crate, board, boardID "<<crate<<","<<board<<","<<boardID;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    unsigned int runnum = inheader->runnum;
    unsigned int lsnum = inheader->lsnum;
    unsigned int nbnum = inheader->nbnum;
    debugStatement<<"Received data from run "<<runnum<<" ls "<<lsnum<<" nb "<<inheader->nbnum<<" boardID "<<boardID;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    std::string topic;


    // Check if we have collected all channels for all nibbles
    // If so, then process the lot -- starting at last_header
    // (NOT current header!)
   
    debugStatement<< "process last header: run " << prevDataHeader.runnum 
               << " ls " << prevDataHeader.lsnum 
               << " nb " << prevDataHeader.nbnum
               << " (right now in run) " << runnum 
               << " ls " << lsnum 
               << " nb " << nbnum 
               << " Lumi: " << lumiTotal 
               << " Lumi raw: " << muTotal;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");



    // FIXME add consistency checks here

    // CP HACK to make lumi nibble line up.
    // MUST DO BEFORE ANALYZING
    debugStatement<<"making the 4nib line up "<<integration_period_nb.value_;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    if(integration_period_nb.value_>1){
        int modnib=inheader->nbnum % integration_period_nb.value_;
        m_mon_nbMod4PerBoard[boardIndex]=m_mon_nbMod4PerBoard[boardIndex]+modnib; 
        debugStatement<<"nib "<<inheader->nbnum<<" int period "<<integration_period_nb.value_<<" mod "<<modnib;
        if(modnib!=0){
            // always advance
            inheader->nbnum=inheader->nbnum+(integration_period_nb.value_-modnib);
            //democratic
            ////float offsetFrac = (float)modnib / integration_period_nb.value_;
            ////if(offsetFrac>0.5){
            ////    inheader->nbnum=inheader->nbnum+(integration_period_nb.value_-modnib);
            ////} else {
            ////    inheader->nbnum=inheader->nbnum-(modnib);
            ////}
            if(inheader->nbnum==0) {
                inheader->nbnum=64;
                inheader->lsnum-=1;
                debugStatement<<" new LS "<<inheader->lsnum;
            }
            debugStatement<<" new nib "<<inheader->nbnum;
        }
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        runnum = inheader->runnum;
        lsnum = inheader->lsnum;
        nbnum = inheader->nbnum;
    }


    //SanityChecks
    if(runnum==prevDataHeader.runnum&&lsnum==prevDataHeader.lsnum&&nbnum==prevDataHeader.nbnum){
        saneData=true;
    } else if((runnum-prevDataHeader.runnum)>0){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New run");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==1)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New LS");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum>1)) {
        debugStatement<<"New LS - "<<lsnum-prevDataHeader.lsnum<<" LSs later";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        newLumiBlock = true;
        saneData=true;

    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==0)&&(nbnum-prevDataHeader.nbnum==4)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "New block, same run,ls - 4ln later");
        newLumiBlock = true;
        saneData=true;
    } else if((runnum-prevDataHeader.runnum==0)&&(lsnum-prevDataHeader.lsnum==0)&&(nbnum-prevDataHeader.nbnum>4)) {
        LOG4CPLUS_DEBUG(getApplicationLogger(),"New block, same run,ls - more than 4ln later");
        newLumiBlock = true;
        saneData=true;
    } else {
        std::stringstream message;
        message<<"INSANE DATA!!\n";
        message<<"\t PREV run,ls,ln "<<prevDataHeader.runnum<<","<<prevDataHeader.lsnum<<","<<prevDataHeader.nbnum<<"\n";
        message<<"\t THIS run,ls,ln "<<runnum<<","<<lsnum<<","<<nbnum<<"\n";
        message<<"\t THIS boardID "<<boardID;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());
    }

    //if(runnum==lastPubRun&&lsnum==lastPubLS&&nbnum==lastPubLN){
    double thisLN=runnum*1e7+lsnum*1e2+nbnum;
    if(thisLN<=lastPub){
        debugStatement.precision(13);
        debugStatement<<"Last published "<<lastPub<<"  This nibble "<<thisLN
                <<"  Diff "<<lastPub-thisLN<<" dropping this histogram";
        debugStatement<<"  crate, board  "<<crate<<","<<board;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.precision(3);
        debugStatement.str("");
        saneData=false;
    }
    
    timeNow = toolbox::TimeVal::gettimeofday();
    double timeInProcessor=(double)timeNow.sec()+(double)timeNow.millisec()/1000.;
    double timeInHeader=(double)inheader->timestampsec+(double)inheader->timestampmsec/1000.;
    if(timeInProcessor-timeInHeader>0.3){
        debugStatement.precision(4);
        debugStatement<<"Larger than 0.2 s between InProcessor InSource Diff/InBus "<<timeInProcessor<<" "<<timeInHeader<<" "<<timeInHeader-timeInProcessor<<" "<<curTopic;
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.precision(3);
        debugStatement.str("");
    }
    if(!saneData) return;

    if(newLumiBlock&&NBoardsCollected>0) {
        computeLumiNow=true;
    }

    if(newLumiBlock){
        firstDataReceived = timeInProcessor;
        debugStatement<<"newLumiBlock:  NBoardsCollected "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        //rejectHistUntilNewLN = false;
    } 


    if(computeLumiNow){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "found new lumi block-going to send lumi");
        do_process(prevDataHeader);
        computeLumiNow=false;
    }


    // Only proceed if the received topic if it is the raw (occ/etsum) hist or the validity hist
    if( (curTopic == interface::bril::hfCMS1T::topicname() && lumiAlgo == "occupancy") 
     || (curTopic == interface::bril::hfCMS_ETT::topicname() &&lumiAlgo == "etsum") ){
        debugStatement<<"FOUND raw hist topic "<<curTopic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        std::map<unsigned int,toolbox::mem::Reference*>::iterator  boardIter = hfRawHist.find(boardID);
        if(boardIter==hfRawHist.end()) {
            debugStatement<<"don't have the hist. add to list";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize());
            memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
            hfRawHist.insert(std::make_pair(boardID,myref));
            boardAvailableThisBlock[boardIndex]=boardAvailableThisBlock[boardIndex]+1;
            timeHist[boardIndex]=timeInProcessor;
        } else {
            LOG4CPLUS_INFO(getApplicationLogger(), "already got it -- this should not happen -- I should have sent lumi and reset.");
            return;
        }
    }
    
    if( curTopic == interface::bril::hfCMS_VALIDT::topicname() ) {
        debugStatement<<"FOUND hfCMS_VALIDT topic "<<curTopic;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        std::map<unsigned int,toolbox::mem::Reference*>::iterator  boardIter = hfValidHist.find(boardID);
        if(boardIter==hfValidHist.end()) {
            debugStatement<<"don't have the hist. add to list";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize());
            memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
            hfValidHist.insert(std::make_pair(boardID,myref));
            boardAvailableThisBlock[boardIndex]=boardAvailableThisBlock[boardIndex]+2;
            timeValidHist[boardIndex]=timeInProcessor;
        } else {
            LOG4CPLUS_INFO(getApplicationLogger(), "already got it -- this should not happen -- I should have sent lumi and reset.");
            return;
        }
    }
   
    if(boardAvailableThisBlock[boardIndex]==3){
        NBoardsCollected++;
        debugStatement<<"NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        if(NBoardsCollected==36){
            debugStatement<<"all boards collected--going to send lumi";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            do_process(prevDataHeader);
        }    
    }
    
    timeNow = toolbox::TimeVal::gettimeofday();
    lastDataAcquired = timeNow.sec();
    prevDataHeader = *inheader;

    debugStatement<<"END:  NBoardsCollected NHist NValidHist "<<NBoardsCollected<<" "<<hfRawHist.size()<<" "<<hfValidHist.size();
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
}

// Process the accumulated nibbles:
// Calls functions that create and queue for publishing the following products
//  * agghists
//  * luminosity numbers
//  * BX mask
void bril::hfprocessor::HFProcessor::do_process(interface::bril::DatumHead& inheader){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Now entered do_process: VERY BEGINNING");
    
    std::stringstream ss;
    ss.str("");
    ss << "do_process run " << inheader.runnum 
       << " ls " << inheader.lsnum
       << " nb " <<inheader.nbnum
       << " channel " << inheader.getChannelID();
    LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    
    
    debugStatement<<"Total:  "<<NBoardsCollected<<" \n";
    debugStatement<<" run " << prevDataHeader.runnum 
       << " ls " << prevDataHeader.lsnum 
       << " nb " << prevDataHeader.nbnum<<"\n";
    debugStatement<<"Missing ";
    for(int thisInd=0; thisInd<36; thisInd++){
        if(boardAvailableThisBlock[thisInd]!=3){
            debugStatement<<thisInd<<" "<<boardAvailableThisBlock[thisInd]<<" ";
        }
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // 1. first make agghists, using same loop calculate total rates
    LOG4CPLUS_DEBUG(getApplicationLogger(), "About to SumHists");
    debugStatement<<"1) read histograms into local memory\n";
    debugStatement<<"  a) all boards merged \n";
    debugStatement<<"  b) each board \n";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    bool success=SumHists(inheader);
    debugStatement<<"  c) clear cache of hists \n";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    if(!success) {
        ClearCache();
        return;
    }
    
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "2) compute/publish data");
    ComputeLumi(inheader);
    lastPubRun=inheader.runnum;
    lastPubLS=inheader.lsnum;
    lastPubLN=inheader.nbnum;
    lastPub=lastPubRun*1e7+lastPubLS*1e2+lastPubLN;
    
    ClearCache();
}

// Loops  through  uHTR histograms,   makes the agghists for  each c hannel
// and at the same time creates total rates for each channel for monitoring 
// purposes and) making the channel mask. 
//
// Queues the agghists for publishing -->

bool bril::hfprocessor::HFProcessor::SumHists(interface::bril::DatumHead& inheader) {
    // assume failure until success
    bool success=false;
    
    memset(aggHistPerBoardPerBX  ,0,sizeof(aggHistPerBoardPerBX));
    memset(aggValidHistPerBoardPerBX,0,sizeof(aggValidHistPerBoardPerBX));
    memset(muHistPerBoardPerBX   ,0,sizeof(muHistPerBoardPerBX));
    memset(lumiHistPerBoardPerBX ,0,sizeof(lumiHistPerBoardPerBX));
    memset(lumiHistPerBoard      ,0,sizeof(lumiHistPerBoard));
    memset(aggHistPerBX          ,0,sizeof(aggHistPerBX));
    memset(aggValidHistPerBX        ,0,sizeof(aggValidHistPerBX));
    memset(muHistPerBX           ,0,sizeof(muHistPerBX));
    memset(lumiHistPerBX         ,0,sizeof(lumiHistPerBX));
      
    if(!m_vdmflag){
        memset(activeBXMask         ,false,sizeof(activeBXMask));
        NActiveBX=0;
        maxBX=0;
        firstBX=-1;
    }
    
    size_t bxbins = interface::bril::MAX_NBX;

    try{
        debugStatement<<"size of hfRawHist "<<hfRawHist.size();
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        if(hfRawHist.size()==0){
            LOG4CPLUS_INFO(getApplicationLogger(), "no data?!  ugh i'm done (SumHists)");
            return success;
        }
        
        // loop over occupancy histograms; 
        // ensure there is a corresponding valid histogram;
        // extract primitive data; 
        // sum up per BX over all boards
        // save which boards were fully functional
        for(std::map<unsigned int, toolbox::mem::Reference*>::iterator boardIter=hfRawHist.begin(); boardIter!=hfRawHist.end(); ++boardIter){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Accessing this channel of histcache");
            
            unsigned int channelid = boardIter->first;
            unsigned int crate = channelid/100;
            unsigned int board = channelid-crate*100;
            unsigned int boardIndex = (board - 1) + (crate==32)*24 + (crate==29)*12;
    
            if(crate!=22&&crate!=29&&crate!=32){
                debugStatement<<"crate is not 22,29,32 "<<crate;
                LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
            }
    
            std::map<unsigned int, toolbox::mem::Reference*>::iterator validIter=hfValidHist.find(channelid);
            if(validIter==hfValidHist.end()){
                debugStatement<<"crate, board, boardIndex "<<crate<<" "<<board<<" "<<boardIndex;
                debugStatement<<"\nThere is no valid histogram for this occupancy hist... skipping it.";
                LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                continue;
            }
            
            toolbox::mem::Reference* histref = boardIter->second;
            interface::bril::hfCMS1T* histdataptr = (interface::bril::hfCMS1T*)(histref->getDataLocation());
            
            toolbox::mem::Reference* validref = validIter->second;
            interface::bril::hfCMS_VALIDT* validdataptr = (interface::bril::hfCMS_VALIDT*)(validref->getDataLocation());
            
            // extract primitive data; 
            // sum up per BX over all boards
            for(size_t bxid=0; bxid<bxbins; bxid++){
                aggHistPerBoardPerBX[boardIndex][bxid]+=histdataptr->payload()[bxid];
                aggValidHistPerBoardPerBX[boardIndex][bxid]+=validdataptr->payload()[bxid];
                aggHistPerBX[bxid]+=aggHistPerBoardPerBX[boardIndex][bxid];
                aggValidHistPerBX[bxid]+=aggValidHistPerBoardPerBX[boardIndex][bxid];
            }
            
            for(size_t bxid=0; bxid<bxbins; bxid++){
                if(aggValidHistPerBX[bxid]>0){
                    aggNormHistPerBX[bxid]=aggHistPerBX[bxid]/aggValidHistPerBX[bxid];
                } else {
                    aggNormHistPerBX[bxid]=0;
                }
            }
            if(boardAvailableThisBlock[boardIndex]!=3){
                std::stringstream message;
                message<<"I have saved the occ/valid hist for "<<boardIndex<<" but boardAvailableThisBlock is "<<boardAvailableThisBlock[boardIndex]; 
                LOG4CPLUS_INFO(getApplicationLogger(),message);
            }
            success=true;
        }

        //format aggregate hist
        
        if (lumiAlgo.value_ == "occupancy") {
            histBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfOcc1AggT::maxsize());
            histBufRef->setDataSize(interface::bril::hfOcc1AggT::maxsize());        
        } else if (lumiAlgo.value_ == "etsum") {
            histBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfEtSumAggT::maxsize());
            histBufRef->setDataSize(interface::bril::hfEtSumAggT::maxsize());
        }        

        interface::bril::DatumHead* aggHistHeader= (interface::bril::DatumHead*)(histBufRef->getDataLocation());
        aggHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        aggHistHeader->setResource(interface::bril::DataSource::HF,0,0,interface::bril::StorageType::FLOAT);

        if (lumiAlgo.value_ == "occupancy") {
            aggHistHeader->setTotalsize(interface::bril::hfOcc1AggT::maxsize());
        } else if (lumiAlgo.value_ == "etsum") {
            aggHistHeader->setTotalsize(interface::bril::hfEtSumAggT::maxsize());
        }

        aggHistHeader->setFrequency(integration_period_nb.value_);

        memcpy(aggHistHeader->payloadanchor,aggNormHistPerBX,(interface::bril::MAX_NBX)*sizeof(float));

        QueueStoreIt it = m_topicoutqueues.end();

        if (lumiAlgo.value_ == "occupancy") {
            it=m_topicoutqueues.find(interface::bril::hfOcc1AggT::topicname());
        } else if (lumiAlgo.value_ == "etsum") {
            it=m_topicoutqueues.find(interface::bril::hfEtSumAggT::topicname());
        }                

        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing this agghist to output queue");
            it->second->push(histBufRef); //push agghist to publishing queue
        } else if(histBufRef) { 
            LOG4CPLUS_INFO(getApplicationLogger(),"histBufRef topic is not in topic queue... releasing memory");
            histBufRef->release(); 
            histBufRef= 0;
        }        
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing agghist");
    } catch(xcept::Exception& e) {
        if(histBufRef) { histBufRef->release(); histBufRef= 0;  }
        std::string msg("Failed to process data for agghist");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
    }

    return success;
}



// Calculates the luminosity numbers (full-orbit values 
// and bunch-by-bunch histograms), 
// and queues them for publishing
void bril::hfprocessor::HFProcessor::ComputeLumi(interface::bril::DatumHead& inheader){
    // NOOOOOWWWW for luminosity numbers
    
    toolbox::mem::Reference* lumiHistBufRef = 0;
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Still in do_process: lumiHistBufRef");  
    try{
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::hfoclumiT::payloaddict();
        if (lumiAlgo.value_ == "etsum") {
          pdict = interface::bril::hfetlumiT::payloaddict();
        }
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        // END COMPOUND-SPECIFIC STUFF
    

        //format aggregate hist
        toolbox::mem::Reference * lumiHistBufRef = 0;
        
        if (lumiAlgo.value_ == "occupancy") {
            lumiHistBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfoclumiT::maxsize());
            lumiHistBufRef->setDataSize(interface::bril::hfoclumiT::maxsize());        
        } else if (lumiAlgo.value_ == "etsum") {
            lumiHistBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfetlumiT::maxsize());
            lumiHistBufRef->setDataSize(interface::bril::hfetlumiT::maxsize());
        }
    
        // fill, run, ls, ln, sec, msec
        //interface::bril::hfoclumiT* lumihist = (interface::bril::hfoclumiT*)(lumiHistBufRef->getDataLocation());
        interface::bril::DatumHead* lumiHistHeader= (interface::bril::DatumHead*)(lumiHistBufRef->getDataLocation());
        lumiHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        lumiHistHeader->setFrequency(integration_period_nb.value_);
    
        // sourceid, algo, channel, payloadtype
        lumiHistHeader->setResource(interface::bril::DataSource::HF,0,0,interface::bril::StorageType::COMPOUND);

        if (lumiAlgo.value_ == "occupancy") {
            lumiHistHeader->setTotalsize(interface::bril::hfoclumiT::maxsize());
        } else if (lumiAlgo.value_ == "etsum") {
            lumiHistHeader->setTotalsize(interface::bril::hfetlumiT::maxsize());    
        }
    
        // resetting values of global variables to zero
        muTotal = 0;
        lumiTotal = 0;
    
        unsigned int maskhigh = 0; // board 32
        unsigned int masklow = 0; // boards 22 and 29
    
        for(int boardInd=0; boardInd<36; boardInd++){
            for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
                float ratio=aggHistPerBoardPerBX[boardInd][bxid];
                if( aggValidHistPerBoardPerBX[boardInd][bxid] > 0 ){
                    ratio=ratio/aggValidHistPerBoardPerBX[boardInd][bxid];
                    if (lumiAlgo.value_ == "occupancy") {
                        muHistPerBoardPerBX[boardInd][bxid]=-log(1-ratio);
                    } else if (lumiAlgo.value_ == "etsum") {
                        muHistPerBoardPerBX[boardInd][bxid]=ratio;
                    }
                } else {
                   ratio=0;
                }
            }
        }

        // compute mu's first
        for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
            float ratio=aggHistPerBX[bxid];
            if( aggValidHistPerBX[bxid] > 0 ){
                ratio=ratio/aggValidHistPerBX[bxid];
                if (lumiAlgo.value_ == "occupancy") {
                    muHistPerBX[bxid]=-log(1-ratio);
                } else if (lumiAlgo.value_ == "etsum") {
                    muHistPerBX[bxid]=ratio;
                }
            } else {
                ratio=0;
            }
        }
   
        // if scan is happening, lock the mask--don't change it
        if(!m_vdmflag){
            MakeDynamicBXMask(inheader);
        }
        // correct mu's for afterglow
        if(applyAfterglowCorr.value_){
            ComputeAfterglow(inheader);
        }
        
        if(lumiAlgo.value_ == "etsum"){
            SubtractPedestal(inheader);
        }

        // compute sum of mu's (post correction)
        for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){

		    debugStatement<<" bxid "<<bxid<< " isActive  "<<activeBXMask[bxid]<<" PostCorrection "<<muHistPerBX[bxid];
	            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        	    debugStatement.str("");
            if(activeBXMask[bxid]){
                muTotal+=muHistPerBX[bxid];
            }
        }
        
        
        // compute lumi with corrected mu's (if afterglow corr is on)
        for(int boardInd=0; boardInd<36; boardInd++){
            for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
                lumiHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid]*11245.6/sigmaVis;
                

                // non-linearity correction
                if(activeBXMask[bxid]){
                    lumiHistPerBoardPerBX[boardInd][bxid]=lumiHistPerBoardPerBX[boardInd][bxid]+quadraticCorrection*lumiHistPerBoardPerBX[boardInd][bxid]*lumiHistPerBoardPerBX[boardInd][bxid];
                }
            }
        }

        for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
            lumiHistPerBX[bxid]=(muHistPerBX[bxid])*11245.6/sigmaVis;
            // non-linearity correction
            if(activeBXMask[bxid]){
                lumiHistPerBX[bxid]= lumiHistPerBX[bxid]+quadraticCorrection*lumiHistPerBX[bxid]*lumiHistPerBX[bxid];
            }
        }


        for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
            if(activeBXMask[bxid]){
                lumiTotal+=lumiHistPerBX[bxid];
                for(int boardInd=0; boardInd<36; boardInd++){
                    if(boardAvailableThisBlock[boardInd]==3){
                        lumiHistPerBoard[boardInd]+=lumiHistPerBoardPerBX[boardInd][bxid];
                    }
                }
            }
        }
        
        // masklow is for presence of first two crates
        // maskhigh is for the third crate
        for(int boardInd=0; boardInd<36; boardInd++){
            if(boardAvailableThisBlock[boardInd]==3){
                m_mon_lumiPerBoard[boardInd]= m_mon_lumiPerBoard[boardInd]+lumiHistPerBoard[boardInd];
                if(boardInd<24){
                    masklow  += 2^(boardInd);
                } else {
                    maskhigh += 2^(boardInd-24);
                }
            }
        }
      
        timeNow = toolbox::TimeVal::gettimeofday();
        unsigned int now = timeNow.sec();
        if(now-lastMessageTime>90){
            debugStatement<<"fill,run,LS,LN "<<inheader.fillnum<<","<<inheader.runnum<<","<<inheader.lsnum<<","<<inheader.nbnum
              <<" NActiveBX "<<NActiveBX
              <<" NBoardsCollected "<<NBoardsCollected
              <<" hfAfterGlowTotalScale "<<hfAfterGlowTotalScale
              <<" lumi combined "<<lumiTotal<<" per board ";
            for(int boardInd=0; boardInd<36; boardInd++){
                if(boardAvailableThisBlock[boardInd]==3){
                    debugStatement<<boardInd<<" "<<lumiHistPerBoard[boardInd]<<" ";
                }else{
                    debugStatement<<boardInd<<" N/A ";
                }
            }
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            lastMessageTime=now;
        }
        m_mon_fill = inheader.fillnum;
        m_mon_run  = inheader.runnum;
        m_mon_ls   = inheader.lsnum;
        m_mon_nb   = inheader.nbnum;
        m_mon_timestamp = timeNow;
        m_mon_nActiveBX = m_mon_nActiveBX+NActiveBX;
        m_mon_nBoards   = m_mon_nBoards  +NBoardsCollected;
        m_mon_nCollected++;
       
        if(inheader.lsnum!=lastPubLS){ // monitor every new LS
            //average over the m_mon_nCollected
            m_mon_nActiveBX=m_mon_nActiveBX/m_mon_nCollected;
            m_mon_nBoards=m_mon_nBoards/m_mon_nCollected;
            for(int index=0;index<36; index++){
                m_mon_nbMod4PerBoard[index]=m_mon_nbMod4PerBoard[index]/m_mon_nCollected;
                m_mon_lumiPerBoard[index]=m_mon_lumiPerBoard[index]/m_mon_nCollected;
            }
            //send data
            m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this );
            //reset everything
            m_mon_nActiveBX = 0;
            m_mon_nBoards   = 0;
            m_mon_nbMod4PerBoard.resize(36, -1);
            m_mon_lumiPerBoard.resize(36, 0);
            m_mon_bunchMask.resize(interface::bril::MAX_NBX,0);
            m_mon_nCollected=0;
        }

        std::stringstream ss;
        ss.str("");
        ss << "Avg lumi: " << lumiTotal << "  Avg lumi raw: " << muTotal;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    
        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::CompoundDataStreamer tc(pdict);       
        tc.insert_field( lumiHistHeader->payloadanchor, "calibtag", m_calibtag.value_.c_str());
        tc.insert_field( lumiHistHeader->payloadanchor, "avgraw",   &muTotal);
        tc.insert_field( lumiHistHeader->payloadanchor, "avg",      &lumiTotal);
        tc.insert_field( lumiHistHeader->payloadanchor, "bxraw",    muHistPerBX);
        tc.insert_field( lumiHistHeader->payloadanchor, "bx",       lumiHistPerBX);
        tc.insert_field( lumiHistHeader->payloadanchor, "masklow",  &masklow);
        tc.insert_field( lumiHistHeader->payloadanchor, "maskhigh", &maskhigh);

        // END MORE COMPOUND-SPECIFIC STUFF
        QueueStoreIt it = m_topicoutqueues.end();

        if (lumiAlgo.value_ == "occupancy") {
            it=m_topicoutqueues.find(interface::bril::hfoclumiT::topicname());
        } else if (lumiAlgo.value_ == "etsum") {
            it=m_topicoutqueues.find(interface::bril::hfetlumiT::topicname());
        }

        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "pushing lumi to queueTOPICS");
            it->second->push(lumiHistBufRef);
            LOG4CPLUS_DEBUG(getApplicationLogger(), "pushing lumi to queueTOPICS - done");
        }      
        else
        {
            if(lumiHistBufRef) { lumiHistBufRef->release(); lumiHistBufRef= 0;  }
        }
        //m_topicoutqueues[interface::bril::hfoclumiT::topicname()]->push(lumiHistBufRef);
    } catch(xcept::Exception& e){
        std::string msg("Failed to process data for lumiHistBufRef");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
        if(lumiHistBufRef) {
            lumiHistBufRef->release();
            lumiHistBufRef= 0;
        }
    }
}


void bril::hfprocessor::HFProcessor::MakeDynamicBXMask(interface::bril::DatumHead& inheader, float fracThreshold){
    
    float minv=1;
    float maxv=0;
    NActiveBX=0;
    maxBX=0;
    firstBX=-1;

    for(int bxind=0; bxind<int(MAX_NBX_noAG); bxind++){


        float thisRatio=0;
        if (lumiAlgo.value_ == "occupancy") {
            thisRatio=aggHistPerBX[bxind];
            if(aggValidHistPerBX[bxind]>0){
                thisRatio=thisRatio/aggValidHistPerBX[bxind];
            } else {
                thisRatio=0;
            }
        } else if (lumiAlgo.value_ == "etsum") {
            thisRatio=muHistPerBX[bxind];
        }
        //only look at non-zero BXs
        if(minv>thisRatio && thisRatio!=0) minv=thisRatio;
        if(maxv<thisRatio) {
            maxv=thisRatio;
            maxBX=bxind;
        }
    }

    // if still 1, all bx are 0
    if(minv==1) minv=0;

    // minimum value of min is 1e-6
    float abMin=BMmin;
    int MaskLow = int(HCALMaskLow);
    int MaskHigh = int(HCALMaskHigh);

        //if (lumiAlgo.value_ == "etsum") abMin=1e-2;
    if (lumiAlgo.value_ == "etsum") abMin=BMthresET;
    if (lumiAlgo.value_ == "occupancy") abMin=BMthresOC;

    minv=std::max(minv,abMin);
    
    //make fraction dynamic
    //at very low lumi, need to distinguish more from noise... higher frac of diff
    //need smooth transition to normal fraction
    float nearNoise=NearNoiseValue;
    float aboveNoise=AboveNoiseValue;
    if (lumiAlgo.value_ == "etsum") {
        nearNoise=0.02;
        aboveNoise=0.05;
    }
    if( maxv-minv<nearNoise) fracThreshold=1.;
    else if(maxv<aboveNoise) fracThreshold= (aboveNoise-maxv+minv)/(aboveNoise-nearNoise) + (maxv-minv-nearNoise)*fracThreshold / (aboveNoise-nearNoise);

    float dynamicThreshold=std::max((maxv-minv)*fracThreshold,abMin);
    //float dynamicThreshold=(max-minv)*fracThreshold;
    debugStatement<<"minv, maxv, fracT, dynT "<<minv<<" "<<maxv<<" "<<fracThreshold<<" "<<dynamicThreshold;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    //if the difference between max and min is small compared to noise, then it is noise
    if(maxv-minv > 0.5*minv) {
        for(int bxind=0; bxind<interface::bril::MAX_NBX; bxind++){
            float thisRatio=0;
	    bool MaskLED = bxind < MaskLow || bxind > MaskHigh;

            debugStatement<<"bxind  "<<bxind<<" MaskedLED  "<<MaskLED<<" Low "<<MaskLow<<" High "<<MaskHigh;
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");

            if (lumiAlgo.value_ == "occupancy") {
                thisRatio=aggHistPerBX[bxind];
                if(aggValidHistPerBX[bxind]>0){
                    thisRatio=thisRatio/aggValidHistPerBX[bxind];
                } else {
                    thisRatio=0;
                }
            } else if (lumiAlgo.value_ == "etsum") {
                thisRatio=muHistPerBX[bxind];
            }
    
            if( thisRatio-minv > dynamicThreshold && MaskLED) {
                debugStatement<<"passing threshold "<<bxind<<" "<<thisRatio<<" "<<minv;
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                activeBXMask[bxind]=true;
                m_mon_bunchMask[bxind]=m_mon_bunchMask[bxind]+1;
                NActiveBX++;
                if(firstBX==-1) firstBX=bxind;
            }
        }
    }
    debugStatement<<"NActiveBX "<<NActiveBX;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

        std::stringstream message;
       message<<"================ CHECK THIS !!!!! NActiveBX "<<NActiveBX<<"  BunchMask Thershold  hfet "<<BMthresET<<" hfoc  "<<BMthresOC;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());

    try{
        // FOR COMPOUND DATA TYPE
        std::string pdict = interface::bril::hfoclumiT::payloaddict();
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        // END COMPOUND-SPECIFIC STUFF
        
        //format bx mask hist
        bxBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::bunchmaskT::maxsize());
        bxBufRef->setDataSize(interface::bril::bunchmaskT::maxsize());

        interface::bril::DatumHead* maskHistHeader= (interface::bril::DatumHead*)(bxBufRef->getDataLocation());
        maskHistHeader->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        maskHistHeader->setResource(interface::bril::DataSource::HF,0,0,interface::bril::StorageType::COMPOUND);
        maskHistHeader->setTotalsize(interface::bril::bunchmaskT::maxsize());
        maskHistHeader->setFrequency(integration_period_nb.value_);

        // MORE COMPOUND-SPECIFIC STUFF
        interface::bril::CompoundDataStreamer tc(pdict);       
        tc.insert_field( maskHistHeader->payloadanchor, "ncollidingbx", &NActiveBX);
        tc.insert_field( maskHistHeader->payloadanchor, "firstlumibx",  &firstBX);
        tc.insert_field( maskHistHeader->payloadanchor, "maxlumibx",    &maxBX);
        tc.insert_field( maskHistHeader->payloadanchor, "bxmask",       activeBXMask);
        // END MORE COMPOUND-SPECIFIC STUFF
        QueueStoreIt it = m_topicoutqueues.find( interface::bril::bunchmaskT::topicname() );
        if(it!=m_topicoutqueues.end()){
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing this bx mask to output queue");
            it->second->push(bxBufRef); //push agghist to publishing queue
        } else if(bxBufRef) {
            LOG4CPLUS_INFO(getApplicationLogger(),"release bxref after assigning because topic not in queue's list");
            bxBufRef->release();
            bxBufRef= 0;
        }
        
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing agghist");
    } catch(xcept::Exception& e){
        std::string msg("Failed to process data for lumiHistBufRef");
        LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
        if(bxBufRef) {
            LOG4CPLUS_INFO(getApplicationLogger(),"release bxref in exception");
            bxBufRef->release();
            bxBufRef= 0;
        }
    }
}

void bril::hfprocessor::HFProcessor::ComputeAfterglow(interface::bril::DatumHead& inheader){

    debugStatement<<"Begin afterglow calculation";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str() );
    debugStatement.str("");
    
    //First apply SBR on mu per BX
    //Compute the hf afterglow with corrected mu / uncorrected mu

    memset(muUncorrectedPerBX,   0, sizeof(muUncorrectedPerBX));
    memset(hfafterglowPerBX,     0, sizeof(hfafterglowPerBX));

    afterglowBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfafterglowfracT::maxsize());
    afterglowBufRef->setDataSize(interface::bril::hfafterglowfracT::maxsize());
    
    interface::bril::DatumHead* afterglowHeader = (interface::bril::DatumHead*)(afterglowBufRef->getDataLocation());
    afterglowHeader->setTime(inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
    afterglowHeader->setResource(interface::bril::DataSource::HF, 0, 0, interface::bril::StorageType::FLOAT);
    afterglowHeader->setTotalsize(interface::bril::hfafterglowfracT::maxsize());


    float noise = 0;
    int num_cut = 20;
    float idl = 0;
    
    debugStatement<<"The luminosity before correction is: ";
    for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
        muUncorrectedPerBX[bxid] = muHistPerBX[bxid];
        debugStatement<<", "<<muHistPerBX[bxid];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    for(int ibx=0; ibx<interface::bril::MAX_NBX; ibx++){
        if(activeBXMask[ibx]){
            for(int jbx=ibx+1; jbx<ibx+interface::bril::MAX_NBX; jbx++){

                if(jbx<interface::bril::MAX_NBX){
                    muHistPerBX[jbx]-=muHistPerBX[ibx]*HFSBR[jbx-ibx];
                } else{
                    muHistPerBX[jbx-interface::bril::MAX_NBX]-=muHistPerBX[ibx]*HFSBR[jbx-ibx];
                }
            }
        }
    }

    for(int bxid=0; bxid<num_cut; bxid++){
        noise+=muHistPerBX[bxid];
        idl+=1;
    }

    noise = noise/num_cut;
    
    float totalMuUn=0;
    float totalMuCorr=0;

    for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
        if(muUncorrectedPerBX[bxid]!=0){
            hfafterglowPerBX[bxid]=muHistPerBX[bxid]/muUncorrectedPerBX[bxid];
            totalMuUn+=muUncorrectedPerBX[bxid];
            totalMuCorr+=muHistPerBX[bxid];
        }else{
            hfafterglowPerBX[bxid]=0;
        }
    }

    if(totalMuUn>0){
        hfAfterGlowTotalScale=totalMuCorr/totalMuUn;
    }

    debugStatement<<"The total correction factor is:  "<<hfAfterGlowTotalScale;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    debugStatement<<"The mus after correction are: ";
    for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
        debugStatement<<", "<<muHistPerBX[bxid];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    // send off hf afterglow frac
    memcpy(afterglowHeader->payloadanchor, hfafterglowPerBX, (interface::bril::MAX_NBX)*sizeof(float));
    
    QueueStoreIt it = m_topicoutqueues.find( interface::bril::hfafterglowfracT::topicname() );
    if(it!=m_topicoutqueues.end()){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing the afterglow correction histogram to output queue");
        it->second->push(afterglowBufRef);
    } else if(afterglowBufRef) {
        LOG4CPLUS_INFO(getApplicationLogger(),"release afterglow ref after assigning because topic not in queue's list");
        afterglowBufRef->release();
        afterglowBufRef= 0;
    }

    LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing afterglow hist");


    // apply hf afterglow frac to the rest of the mu's
    for(int boardInd=0; boardInd<36; boardInd++){
        for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
            muHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid]*hfafterglowPerBX[bxid];
        }
    }
}


void bril::hfprocessor::HFProcessor::SubtractPedestal(interface::bril::DatumHead& inheader){
    
    memset(pedestal,     0, sizeof(pedestal));
    
    pedBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfEtPedestalT::maxsize());
    pedBufRef->setDataSize(interface::bril::hfEtPedestalT::maxsize());
    
    interface::bril::DatumHead* pedHeader = (interface::bril::DatumHead*)(pedBufRef->getDataLocation());
    pedHeader->setTime(inheader.fillnum, inheader.runnum, inheader.lsnum, inheader.nbnum, inheader.timestampsec, inheader.timestampmsec);
    pedHeader->setResource(interface::bril::DataSource::HF, 0, 0, interface::bril::StorageType::FLOAT);
    pedHeader->setTotalsize(interface::bril::hfEtPedestalT::maxsize());

    debugStatement<<"Pedestal: ";

    int nSample=13;
    for (int i = 0; i < 4; i++){
        for (int j = i; j < 4*nSample; j+=4) {
            pedestal[i] +=  muHistPerBX[3500 + j]/nSample;
        }
        debugStatement<<" "<<i<<" "<<pedestal[i];
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    for(int bxid=0; bxid<interface::bril::MAX_NBX; bxid++){
        debugStatement<<" "<<bxid<<" "<<muHistPerBX[bxid];
        muHistPerBX[bxid]=(muHistPerBX[bxid] - pedestal[bxid % 4]);
        debugStatement<<" "<<pedestal[bxid % 4]<<" "<<muHistPerBX[bxid]<<"\n";
        for(int boardInd=0; boardInd<36; boardInd++){
            muHistPerBoardPerBX[boardInd][bxid]=muHistPerBoardPerBX[boardInd][bxid] - pedestal[bxid % 4];
        }
    }
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    
    // send off hf pedestal
    memcpy(pedHeader->payloadanchor, pedestal, (4)*sizeof(float));
    
    QueueStoreIt it = m_topicoutqueues.find( interface::bril::hfEtPedestalT::topicname() );
    if(it!=m_topicoutqueues.end()){
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing the afterglow correction histogram to output queue");
        it->second->push(pedBufRef);
    } else if(pedBufRef){
        LOG4CPLUS_INFO(getApplicationLogger(),"release pedestal ref after assigning because topic not in queue's list");
        pedBufRef->release();
        pedBufRef=0;
    }
}


// Clear the fully-accumulated histograms in preparation
// to accumulate more
void bril::hfprocessor::HFProcessor::ClearCache(){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "ClearCache");
    
    memset(boardAvailableThisBlock  ,0,sizeof(boardAvailableThisBlock));
    memset(timeValidHist            ,0,sizeof(timeValidHist          ));
    memset(timeHist                 ,0,sizeof(timeHist            ));
    NBoardsCollected=0;
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "about to clear hfRawHist");
    for(std::map<unsigned int,toolbox::mem::Reference*>::iterator it=hfRawHist.begin();it!=hfRawHist.end();++it){
        if(it->second!=0){
            it->second->release();
            it->second=0;
        }
        delete it->second;
    }
    hfRawHist.clear();

    debugStatement<<"about to clear hfValidHist";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    for(std::map<unsigned int,toolbox::mem::Reference*>::iterator it=hfValidHist.begin();it!=hfValidHist.end();++it){
        if(it->second!=0){
            it->second->release();
            it->second=0;
        }
        delete it->second;
    }
    hfValidHist.clear();
    debugStatement<<"All cleared";
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
}


bool bril::hfprocessor::HFProcessor::publishing(toolbox::task::WorkLoop* wl){
    QueueStoreIt it;
    for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
        std::string topicname = it->first;
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Is there data for "+topicname);
        if(it->second->empty()) continue;
        toolbox::mem::Reference* data = it->second->pop();
    
        std::pair<TopicStoreIt,TopicStoreIt > ret = m_out_topicTobuses.equal_range(topicname);
        for(TopicStoreIt topicit = ret.first; topicit!=ret.second;++topicit){
            if(data){ 
                std::string payloaddict;
                debugStatement<<"Found topic "<<topicname<<" for publishing";
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                if(topicname=="hfOcc1Agg"){
                    payloaddict=interface::bril::hfOcc1AggT::payloaddict();
                }else if(topicname=="hfoclumi"){
                    payloaddict=interface::bril::hfoclumiT::payloaddict();
                }else if(topicname=="hfEtSumAgg"){  
                    payloaddict=interface::bril::hfEtSumAggT::payloaddict();
                }else if(topicname=="hfetlumi"){
                    payloaddict=interface::bril::hfetlumiT::payloaddict();
                }else if(topicname=="bunchmask"){
                    payloaddict=interface::bril::bunchmaskT::payloaddict();
                }else if(topicname=="hfEtPedestal"){
                    payloaddict=interface::bril::hfEtPedestalT::payloaddict();
                }else if(topicname=="hfafterglowfrac"){
                    payloaddict=interface::bril::hfafterglowfracT::payloaddict();
                } 
                else{
                    LOG4CPLUS_ERROR(getApplicationLogger(),"Wrong topic name to publish!");
                }
                do_publish(topicit->second,topicname,payloaddict,data->duplicate());
            }
        }
        if(data) {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Releasing "+topicname+" after publishing");
            data->release();
            data=0;
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Released "+topicname+" after publishing");
        }
    }

    usleep(int(integration_period_secs*1.e6/8.)); //sleep 1/8 of the length of the integration period
    return true;
}

void bril::hfprocessor::HFProcessor::do_publish(const std::string& busname,const std::string& topicname,const std::string& pdict,toolbox::mem::Reference* bufRef){
    std::stringstream msg;
    try{
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
        plist.setProperty("PAYLOAD_DICT",pdict);
        if ( m_beammode == "SETUP" || m_beammode == "ABORT" || m_beammode == "BEAM DUMP" || m_beammode == "RAMP DOWN" || m_beammode == "CYCLING" || m_beammode == "RECOVERY" || m_beammode == "NO BEAM" ){
            plist.setProperty("NOSTORE","1");
        }
        msg << "publish to "<<busname<<" , "<<topicname<<", run " << prevDataHeader.runnum<<" ls "<< prevDataHeader.lsnum <<" nb "<<prevDataHeader.nbnum; 
        LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
        debugStatement<<"Publish "<<topicname<<" to "<<busname;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        this->getEventingBus(busname).publish(topicname,bufRef,plist);
        debugStatement<<"getEventingBus.publish seemed to work";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
    }catch(xcept::Exception& e){
        msg<<"Failed to publish "<<topicname<<" to "<<busname;    
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){
            bufRef->release();
            bufRef = 0;
        }
        //XCEPT_DECLARE_NESTED(bril::hfprocessor::exception::Exception,myerrorobj,msg.str(),e);
        //this->notifyQualified("fatal",myerrorobj);
    }  
}

// When nibble-histogram receiving times out, processor the received data 
// and/or clear the stale data
void bril::hfprocessor::HFProcessor::timeExpired(toolbox::task::TimerEvent& e){
    //if(prevDataHeader.runnum==0) return;//cache is clean, do nothing
    //toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    //unsigned int nowsec = t.sec();
    //if( (nowsec-lastDataAcquired)>5 ){
    //    std::stringstream ss;
    //    ss<<"Found stale cache. run "<<prevDataHeader.runnum<<" ls "<<prevDataHeader.lsnum;
    //    LOG4CPLUS_DEBUG(getApplicationLogger(), "Full statistics, process the cache ");
    //    do_process(prevDataHeader);
    //    ClearCache();
    //}else{
    //    LOG4CPLUS_INFO(getApplicationLogger(), "Insufficient statistics collected, discard the cache ");
    //    prevDataHeader.runnum = 0;
    //    prevDataHeader.lsnum = 0;
    //    //prevDataHeader.cmslsnum = 0;
    //    prevDataHeader.nbnum = 0;
    //    ClearCache();
    //}
}



void bril::hfprocessor::HFProcessor::InitializeHFSBR()
{
  float sbr_oc[interface::bril::MAX_NBX] = {1.0000000000, 0.0091281180, 0.0022473928, 0.0014641597, 0.0013241516, 0.0012761528, 0.0010877388, 0.0007321886, 0.0007198942, 0.0007078063, 0.0006959213, 0.0006842359, 0.0006727467, 0.0006614504, 0.0006503438, 0.0006394237, 0.0006286869, 0.0006181304, 0.0006077512, 0.0005975463, 0.0005875127, 0.0005776476, 0.0005679482, 0.0005584116, 0.0005490351, 0.0005398161, 0.0005307519, 0.0005218399, 0.0005130775, 0.0005044623, 0.0004959917, 0.0004876634, 0.0004794749, 0.0004714239, 0.0004635080, 0.0004557251, 0.0004480729, 0.0004405492, 0.0004331518, 0.0004258786, 0.0004187276, 0.0004116966, 0.0004047837, 0.0003979868, 0.0003913041, 0.0003847336, 0.0003782734, 0.0003719217, 0.0003656767, 0.0003595365, 0.0003534994, 0.0003475637, 0.0003417277, 0.0003359896, 0.0003303479, 0.0003248009, 0.0003193471, 0.0003139849, 0.0003087127, 0.0003035290, 0.0002984323, 0.0002934213, 0.0002884943, 0.0002836501, 0.0002788873, 0.0002742044, 0.0002696002, 0.0002650732, 0.0002606223, 0.0002562461, 0.0002519434, 0.0002477130, 0.0002435535, 0.0002394640, 0.0002354430, 0.0002314897, 0.0002276026, 0.0002237809, 0.0002200233, 0.0002163288, 0.0002126964, 0.0002091250, 0.0002056135, 0.0002021610, 0.0001987664, 0.0001954289, 0.0001921474, 0.0001889210, 0.0001857487, 0.0001826298, 0.0001795632, 0.0001765481, 0.0001735836, 0.0001706689, 0.0001678032, 0.0001649855, 0.0001622152, 0.0001594914, 0.0001568133, 0.0001541802, 0.0001515914, 0.0001490459, 0.0001465433, 0.0001440826, 0.0001416633, 0.0001392846, 0.0001369458, 0.0001346463, 0.0001323854, 0.0001301625, 0.0001279769, 0.0001258280, 0.0001237152, 0.0001216379, 0.0001195954, 0.0001175872, 0.0001156128, 0.0001136715, 0.0001117628, 0.0001098862, 0.0001080410, 0.0001062269, 0.0001044432, 0.0001026895, 0.0001009652, 0.0000992698, 0.0000976030, 0.0000959641, 0.0000943527, 0.0000927684, 0.0000912107, 0.0000911665, 0.0000911223, 0.0000910782, 0.0000910341, 0.0000909899, 0.0000909459, 0.0000909018, 0.0000908577, 0.0000908137, 0.0000907697, 0.0000907257, 0.0000906817, 0.0000906378, 0.0000905939, 0.0000905500, 0.0000905061, 0.0000904623, 0.0000904184, 0.0000903746, 0.0000903308, 0.0000902870, 0.0000902433, 0.0000901996, 0.0000901558, 0.0000901122, 0.0000900685, 0.0000900248, 0.0000899812, 0.0000899376, 0.0000898940, 0.0000898505, 0.0000898069, 0.0000897634, 0.0000897199, 0.0000896764, 0.0000896330, 0.0000895896, 0.0000895461, 0.0000895028, 0.0000894594, 0.0000894160, 0.0000893727, 0.0000893294, 0.0000892861, 0.0000892428, 0.0000891996, 0.0000891564, 0.0000891132, 0.0000890700, 0.0000890268, 0.0000889837, 0.0000889406, 0.0000888975, 0.0000888544, 0.0000888113, 0.0000887683, 0.0000887253, 0.0000886823, 0.0000886393, 0.0000885964, 0.0000885534, 0.0000885105, 0.0000884676, 0.0000884248, 0.0000883819, 0.0000883391, 0.0000882963, 0.0000882535, 0.0000882107, 0.0000881680, 0.0000881253, 0.0000880826, 0.0000880399, 0.0000879972, 0.0000879546, 0.0000879119, 0.0000878693, 0.0000878268, 0.0000877842, 0.0000877417, 0.0000876992, 0.0000876567, 0.0000876142, 0.0000875717, 0.0000875293, 0.0000874869, 0.0000874445, 0.0000874021, 0.0000873598, 0.0000873174, 0.0000872751, 0.0000872328, 0.0000871905, 0.0000871483, 0.0000871061, 0.0000870639, 0.0000870217, 0.0000869795, 0.0000869374, 0.0000868952, 0.0000868531, 0.0000868110, 0.0000867690, 0.0000867269, 0.0000866849, 0.0000866429, 0.0000866009, 0.0000865589, 0.0000865170, 0.0000864751, 0.0000864332, 0.0000863913, 0.0000863494, 0.0000863076, 0.0000862658, 0.0000862239, 0.0000861822, 0.0000861404, 0.0000860987, 0.0000860569, 0.0000860152, 0.0000859736, 0.0000859319, 0.0000858903, 0.0000858486, 0.0000858070, 0.0000857655, 0.0000857239, 0.0000856824, 0.0000856408, 0.0000855993, 0.0000855579, 0.0000855164, 0.0000854750, 0.0000854335, 0.0000853921, 0.0000853508, 0.0000853094, 0.0000852681, 0.0000852267, 0.0000851854, 0.0000851442, 0.0000851029, 0.0000850617, 0.0000850205, 0.0000849793, 0.0000849381, 0.0000848969, 0.0000848558, 0.0000848147, 0.0000847736, 0.0000847325, 0.0000846914, 0.0000846504, 0.0000846094, 0.0000845684, 0.0000845274, 0.0000844864, 0.0000844455, 0.0000844046, 0.0000843637, 0.0000843228, 0.0000842819, 0.0000842411, 0.0000842003, 0.0000841595, 0.0000841187, 0.0000840779, 0.0000840372, 0.0000839964, 0.0000839557, 0.0000839151, 0.0000838744, 0.0000838338, 0.0000837931, 0.0000837525, 0.0000837119, 0.0000836714, 0.0000836308, 0.0000835903, 0.0000835498, 0.0000835093, 0.0000834688, 0.0000834284, 0.0000833880, 0.0000833476, 0.0000833072, 0.0000832668, 0.0000832265, 0.0000831861, 0.0000831458, 0.0000831055, 0.0000830653, 0.0000830250, 0.0000829848, 0.0000829446, 0.0000829044, 0.0000828642, 0.0000828240, 0.0000827839, 0.0000827438, 0.0000827037, 0.0000826636, 0.0000826236, 0.0000825835, 0.0000825435, 0.0000825035, 0.0000824635, 0.0000824236, 0.0000823836, 0.0000823437, 0.0000823038, 0.0000822639, 0.0000822241, 0.0000821842, 0.0000821444, 0.0000821046, 0.0000820648, 0.0000820250, 0.0000819853, 0.0000819456, 0.0000819058, 0.0000818662, 0.0000818265, 0.0000817868, 0.0000817472, 0.0000817076, 0.0000816680, 0.0000816284, 0.0000815889, 0.0000815493, 0.0000815098, 0.0000814703, 0.0000814308, 0.0000813914, 0.0000813519, 0.0000813125, 0.0000812731, 0.0000812337, 0.0000811944, 0.0000811550, 0.0000811157, 0.0000810764, 0.0000810371, 0.0000809978, 0.0000809586, 0.0000809194, 0.0000808801, 0.0000808410, 0.0000808018, 0.0000807626, 0.0000807235, 0.0000806844, 0.0000806453, 0.0000806062, 0.0000805671, 0.0000805281, 0.0000804891, 0.0000804501, 0.0000804111, 0.0000803721, 0.0000803332, 0.0000802942, 0.0000802553, 0.0000802164, 0.0000801776, 0.0000801387, 0.0000800999, 0.0000800611, 0.0000800223, 0.0000799835, 0.0000799447, 0.0000799060, 0.0000798673, 0.0000798286, 0.0000797899, 0.0000797512, 0.0000797126, 0.0000796740, 0.0000796354, 0.0000795968, 0.0000795582, 0.0000795196, 0.0000794811, 0.0000794426, 0.0000794041, 0.0000793656, 0.0000793272, 0.0000792887, 0.0000792503, 0.0000792119, 0.0000791735, 0.0000791351, 0.0000790968, 0.0000790585, 0.0000790202, 0.0000789819, 0.0000789436, 0.0000789053, 0.0000788671, 0.0000788289, 0.0000787907, 0.0000787525, 0.0000787144, 0.0000786762, 0.0000786381, 0.0000786000, 0.0000785619, 0.0000785238, 0.0000784858, 0.0000784477, 0.0000784097, 0.0000783717, 0.0000783338, 0.0000782958, 0.0000782579, 0.0000782199, 0.0000781820, 0.0000781441, 0.0000781063, 0.0000780684, 0.0000780306, 0.0000779928, 0.0000779550, 0.0000779172, 0.0000778795, 0.0000778417, 0.0000778040, 0.0000777663, 0.0000777286, 0.0000776909, 0.0000776533, 0.0000776157, 0.0000775781, 0.0000775405, 0.0000775029, 0.0000774653, 0.0000774278, 0.0000773903, 0.0000773528, 0.0000773153, 0.0000772778, 0.0000772404, 0.0000772030, 0.0000771655, 0.0000771282, 0.0000770908, 0.0000770534, 0.0000770161, 0.0000769788, 0.0000769415, 0.0000769042, 0.0000768669, 0.0000768297, 0.0000767924, 0.0000767552, 0.0000767180, 0.0000766809, 0.0000766437, 0.0000766066, 0.0000765694, 0.0000765323, 0.0000764952, 0.0000764582, 0.0000764211, 0.0000763841, 0.0000763471, 0.0000763101, 0.0000762731, 0.0000762361, 0.0000761992, 0.0000761623, 0.0000761254, 0.0000760885, 0.0000760516, 0.0000760148, 0.0000759779, 0.0000759411, 0.0000759043, 0.0000758675, 0.0000758308, 0.0000757940, 0.0000757573, 0.0000757206, 0.0000756839, 0.0000756472, 0.0000756106, 0.0000755739, 0.0000755373, 0.0000755007, 0.0000754641, 0.0000754275, 0.0000753910, 0.0000753545, 0.0000753179, 0.0000752814, 0.0000752450, 0.0000752085, 0.0000751721, 0.0000751356, 0.0000750992, 0.0000750628, 0.0000750265, 0.0000749901, 0.0000749538, 0.0000749174, 0.0000748811, 0.0000748449, 0.0000748086, 0.0000747723, 0.0000747361, 0.0000746999, 0.0000746637, 0.0000746275, 0.0000745913, 0.0000745552, 0.0000745191, 0.0000744830, 0.0000744469, 0.0000744108, 0.0000743747, 0.0000743387, 0.0000743027, 0.0000742667, 0.0000742307, 0.0000741947, 0.0000741588, 0.0000741228, 0.0000740869, 0.0000740510, 0.0000740151, 0.0000739793, 0.0000739434, 0.0000739076, 0.0000738718, 0.0000738360, 0.0000738002, 0.0000737644, 0.0000737287, 0.0000736930, 0.0000736572, 0.0000736215, 0.0000735859, 0.0000735502, 0.0000735146, 0.0000734790, 0.0000734433, 0.0000734078, 0.0000733722, 0.0000733366, 0.0000733011, 0.0000732656, 0.0000732301, 0.0000731946, 0.0000731591, 0.0000731237, 0.0000730882, 0.0000730528, 0.0000730174, 0.0000729820, 0.0000729467, 0.0000729113, 0.0000728760, 0.0000728407, 0.0000728054, 0.0000727701, 0.0000727348, 0.0000726996, 0.0000726644, 0.0000726291, 0.0000725940, 0.0000725588, 0.0000725236, 0.0000724885, 0.0000724533, 0.0000724182, 0.0000723831, 0.0000723481, 0.0000723130, 0.0000722780, 0.0000722429, 0.0000722079, 0.0000721729, 0.0000721380, 0.0000721030, 0.0000720681, 0.0000720332, 0.0000719983, 0.0000719634, 0.0000719285, 0.0000718936, 0.0000718588, 0.0000718240, 0.0000717892, 0.0000717544, 0.0000717196, 0.0000716849, 0.0000716501, 0.0000716154, 0.0000715807, 0.0000715460, 0.0000715113, 0.0000714767, 0.0000714421, 0.0000714074, 0.0000713728, 0.0000713382, 0.0000713037, 0.0000712691, 0.0000712346, 0.0000712001, 0.0000711656, 0.0000711311, 0.0000710966, 0.0000710622, 0.0000710277, 0.0000709933, 0.0000709589, 0.0000709245, 0.0000708902, 0.0000708558, 0.0000708215, 0.0000707872, 0.0000707529, 0.0000707186, 0.0000706843, 0.0000706500, 0.0000706158, 0.0000705816, 0.0000705474, 0.0000705132, 0.0000704790, 0.0000704449, 0.0000704107, 0.0000703766, 0.0000703425, 0.0000703084, 0.0000702744, 0.0000702403, 0.0000702063, 0.0000701723, 0.0000701383, 0.0000701043, 0.0000700703, 0.0000700363, 0.0000700024, 0.0000699685, 0.0000699346, 0.0000699007, 0.0000698668, 0.0000698330, 0.0000697991, 0.0000697653, 0.0000697315, 0.0000696977, 0.0000696639, 0.0000696302, 0.0000695964, 0.0000695627, 0.0000695290, 0.0000694953, 0.0000694616, 0.0000694280, 0.0000693943, 0.0000693607, 0.0000693271, 0.0000692935, 0.0000692599, 0.0000692264, 0.0000691928, 0.0000691593, 0.0000691258, 0.0000690923, 0.0000690588, 0.0000690253, 0.0000689919, 0.0000689584, 0.0000689250, 0.0000688916, 0.0000688582, 0.0000688249, 0.0000687915, 0.0000687582, 0.0000687249, 0.0000686916, 0.0000686583, 0.0000686250, 0.0000685918, 0.0000685585, 0.0000685253, 0.0000684921, 0.0000684589, 0.0000684257, 0.0000683926, 0.0000683594, 0.0000683263, 0.0000682932, 0.0000682601, 0.0000682270, 0.0000681940, 0.0000681609, 0.0000681279, 0.0000680949, 0.0000680619, 0.0000680289, 0.0000679959, 0.0000679630, 0.0000679301, 0.0000678971, 0.0000678642, 0.0000678314, 0.0000677985, 0.0000677656, 0.0000677328, 0.0000677000, 0.0000676672, 0.0000676344, 0.0000676016, 0.0000675688, 0.0000675361, 0.0000675034, 0.0000674707, 0.0000674380, 0.0000674053, 0.0000673726, 0.0000673400, 0.0000673073, 0.0000672747, 0.0000672421, 0.0000672095, 0.0000671770, 0.0000671444, 0.0000671119, 0.0000670794, 0.0000670469, 0.0000670144, 0.0000669819, 0.0000669494, 0.0000669170, 0.0000668846, 0.0000668522, 0.0000668198, 0.0000667874, 0.0000667550, 0.0000667227, 0.0000666903, 0.0000666580, 0.0000666257, 0.0000665934, 0.0000665612, 0.0000665289, 0.0000664967, 0.0000664645, 0.0000664323, 0.0000664001, 0.0000663679, 0.0000663357, 0.0000663036, 0.0000662714, 0.0000662393, 0.0000662072, 0.0000661752, 0.0000661431, 0.0000661110, 0.0000660790, 0.0000660470, 0.0000660150, 0.0000659830, 0.0000659510, 0.0000659191, 0.0000658871, 0.0000658552, 0.0000658233, 0.0000657914, 0.0000657595, 0.0000657276, 0.0000656958, 0.0000656639, 0.0000656321, 0.0000656003, 0.0000655685, 0.0000655368, 0.0000655050, 0.0000654733, 0.0000654415, 0.0000654098, 0.0000653781, 0.0000653464, 0.0000653148, 0.0000652831, 0.0000652515, 0.0000652199, 0.0000651883, 0.0000651567, 0.0000651251, 0.0000650936, 0.0000650620, 0.0000650305, 0.0000649990, 0.0000649675, 0.0000649360, 0.0000649045, 0.0000648731, 0.0000648416, 0.0000648102, 0.0000647788, 0.0000647474, 0.0000647160, 0.0000646847, 0.0000646533, 0.0000646220, 0.0000645907, 0.0000645594, 0.0000645281, 0.0000644968, 0.0000644656, 0.0000644344, 0.0000644031, 0.0000643719, 0.0000643407, 0.0000643096, 0.0000642784, 0.0000642472, 0.0000642161, 0.0000641850, 0.0000641539, 0.0000641228, 0.0000640917, 0.0000640607, 0.0000640296, 0.0000639986, 0.0000639676, 0.0000639366, 0.0000639056, 0.0000638746, 0.0000638437, 0.0000638128, 0.0000637818, 0.0000637509, 0.0000637200, 0.0000636892, 0.0000636583, 0.0000636274, 0.0000635966, 0.0000635658, 0.0000635350, 0.0000635042, 0.0000634734, 0.0000634427, 0.0000634119, 0.0000633812, 0.0000633505, 0.0000633198, 0.0000632891, 0.0000632584, 0.0000632278, 0.0000631971, 0.0000631665, 0.0000631359, 0.0000631053, 0.0000630747, 0.0000630442, 0.0000630136, 0.0000629831, 0.0000629526, 0.0000629221, 0.0000628916, 0.0000628611, 0.0000628306, 0.0000628002, 0.0000627698, 0.0000627393, 0.0000627089, 0.0000626786, 0.0000626482, 0.0000626178, 0.0000625875, 0.0000625572, 0.0000625268, 0.0000624965, 0.0000624663, 0.0000624360, 0.0000624057, 0.0000623755, 0.0000623453, 0.0000623151, 0.0000622849, 0.0000622547, 0.0000622245, 0.0000621944, 0.0000621642, 0.0000621341, 0.0000621040, 0.0000620739, 0.0000620438, 0.0000620137, 0.0000619837, 0.0000619537, 0.0000619236, 0.0000618936, 0.0000618636, 0.0000618337, 0.0000618037, 0.0000617738, 0.0000617438, 0.0000617139, 0.0000616840, 0.0000616541, 0.0000616242, 0.0000615944, 0.0000615645, 0.0000615347, 0.0000615049, 0.0000614751, 0.0000614453, 0.0000614155, 0.0000613857, 0.0000613560, 0.0000613263, 0.0000612965, 0.0000612668, 0.0000612372, 0.0000612075, 0.0000611778, 0.0000611482, 0.0000611185, 0.0000610889, 0.0000610593, 0.0000610297, 0.0000610002, 0.0000609706, 0.0000609411, 0.0000609115, 0.0000608820, 0.0000608525, 0.0000608230, 0.0000607935, 0.0000607641, 0.0000607346, 0.0000607052, 0.0000606758, 0.0000606464, 0.0000606170, 0.0000605876, 0.0000605583, 0.0000605289, 0.0000604996, 0.0000604703, 0.0000604410, 0.0000604117, 0.0000603824, 0.0000603532, 0.0000603239, 0.0000602947, 0.0000602655, 0.0000602363, 0.0000602071, 0.0000601779, 0.0000601487, 0.0000601196, 0.0000600905, 0.0000600613, 0.0000600322, 0.0000600031, 0.0000599741, 0.0000599450, 0.0000599160, 0.0000598869, 0.0000598579, 0.0000598289, 0.0000597999, 0.0000597709, 0.0000597420, 0.0000597130, 0.0000596841, 0.0000596552, 0.0000596263, 0.0000595974, 0.0000595685, 0.0000595396, 0.0000595108, 0.0000594819, 0.0000594531, 0.0000594243, 0.0000593955, 0.0000593667, 0.0000593379, 0.0000593092, 0.0000592805, 0.0000592517, 0.0000592230, 0.0000591943, 0.0000591656, 0.0000591370, 0.0000591083, 0.0000590797, 0.0000590510, 0.0000590224, 0.0000589938, 0.0000589652, 0.0000589367, 0.0000589081, 0.0000588796, 0.0000588510, 0.0000588225, 0.0000587940, 0.0000587655, 0.0000587370, 0.0000587086, 0.0000586801, 0.0000586517, 0.0000586233, 0.0000585949, 0.0000585665, 0.0000585381, 0.0000585097, 0.0000584814, 0.0000584530, 0.0000584247, 0.0000583964, 0.0000583681, 0.0000583398, 0.0000583115, 0.0000582833, 0.0000582550, 0.0000582268, 0.0000581986, 0.0000581704, 0.0000581422, 0.0000581140, 0.0000580859, 0.0000580577, 0.0000580296, 0.0000580015, 0.0000579734, 0.0000579453, 0.0000579172, 0.0000578891, 0.0000578611, 0.0000578330, 0.0000578050, 0.0000577770, 0.0000577490, 0.0000577210, 0.0000576931, 0.0000576651, 0.0000576372, 0.0000576092, 0.0000575813, 0.0000575534, 0.0000575255, 0.0000574976, 0.0000574698, 0.0000574419, 0.0000574141, 0.0000573863, 0.0000573585, 0.0000573307, 0.0000573029, 0.0000572751, 0.0000572474, 0.0000572196, 0.0000571919, 0.0000571642, 0.0000571365, 0.0000571088, 0.0000570811, 0.0000570535, 0.0000570258, 0.0000569982, 0.0000569706, 0.0000569430, 0.0000569154, 0.0000568878, 0.0000568602, 0.0000568327, 0.0000568051, 0.0000567776, 0.0000567501, 0.0000567226, 0.0000566951, 0.0000566676, 0.0000566402, 0.0000566127, 0.0000565853, 0.0000565579, 0.0000565305, 0.0000565031, 0.0000564757, 0.0000564483, 0.0000564210, 0.0000563936, 0.0000563663, 0.0000563390, 0.0000563117, 0.0000562844, 0.0000562571, 0.0000562299, 0.0000562026, 0.0000561754, 0.0000561482, 0.0000561209, 0.0000560938, 0.0000560666, 0.0000560394, 0.0000560122, 0.0000559851, 0.0000559580, 0.0000559309, 0.0000559038, 0.0000558767, 0.0000558496, 0.0000558225, 0.0000557955, 0.0000557684, 0.0000557414, 0.0000557144, 0.0000556874, 0.0000556604, 0.0000556335, 0.0000556065, 0.0000555795, 0.0000555526, 0.0000555257, 0.0000554988, 0.0000554719, 0.0000554450, 0.0000554181, 0.0000553913, 0.0000553645, 0.0000553376, 0.0000553108, 0.0000552840, 0.0000552572, 0.0000552304, 0.0000552037, 0.0000551769, 0.0000551502, 0.0000551235, 0.0000550968, 0.0000550701, 0.0000550434, 0.0000550167, 0.0000549900, 0.0000549634, 0.0000549368, 0.0000549101, 0.0000548835, 0.0000548569, 0.0000548304, 0.0000548038, 0.0000547772, 0.0000547507, 0.0000547241, 0.0000546976, 0.0000546711, 0.0000546446, 0.0000546182, 0.0000545917, 0.0000545652, 0.0000545388, 0.0000545124, 0.0000544859, 0.0000544595, 0.0000544332, 0.0000544068, 0.0000543804, 0.0000543541, 0.0000543277, 0.0000543014, 0.0000542751, 0.0000542488, 0.0000542225, 0.0000541962, 0.0000541700, 0.0000541437, 0.0000541175, 0.0000540913, 0.0000540650, 0.0000540388, 0.0000540127, 0.0000539865, 0.0000539603, 0.0000539342, 0.0000539080, 0.0000538819, 0.0000538558, 0.0000538297, 0.0000538036, 0.0000537776, 0.0000537515, 0.0000537254, 0.0000536994, 0.0000536734, 0.0000536474, 0.0000536214, 0.0000535954, 0.0000535694, 0.0000535435, 0.0000535175, 0.0000534916, 0.0000534657, 0.0000534398, 0.0000534139, 0.0000533880, 0.0000533621, 0.0000533363, 0.0000533104, 0.0000532846, 0.0000532588, 0.0000532329, 0.0000532072, 0.0000531814, 0.0000531556, 0.0000531298, 0.0000531041, 0.0000530784, 0.0000530526, 0.0000530269, 0.0000530012, 0.0000529756, 0.0000529499, 0.0000529242, 0.0000528986, 0.0000528729, 0.0000528473, 0.0000528217, 0.0000527961, 0.0000527705, 0.0000527450, 0.0000527194, 0.0000526939, 0.0000526683, 0.0000526428, 0.0000526173, 0.0000525918, 0.0000525663, 0.0000525408, 0.0000525154, 0.0000524899, 0.0000524645, 0.0000524391, 0.0000524137, 0.0000523883, 0.0000523629, 0.0000523375, 0.0000523121, 0.0000522868, 0.0000522615, 0.0000522361, 0.0000522108, 0.0000521855, 0.0000521602, 0.0000521350, 0.0000521097, 0.0000520844, 0.0000520592, 0.0000520340, 0.0000520088, 0.0000519836, 0.0000519584, 0.0000519332, 0.0000519080, 0.0000518829, 0.0000518577, 0.0000518326, 0.0000518075, 0.0000517824, 0.0000517573, 0.0000517322, 0.0000517071, 0.0000516821, 0.0000516570, 0.0000516320, 0.0000516070, 0.0000515820, 0.0000515570, 0.0000515320, 0.0000515070, 0.0000514821, 0.0000514571, 0.0000514322, 0.0000514073, 0.0000513824, 0.0000513575, 0.0000513326, 0.0000513077, 0.0000512828, 0.0000512580, 0.0000512331, 0.0000512083, 0.0000511835, 0.0000511587, 0.0000511339, 0.0000511091, 0.0000510844, 0.0000510596, 0.0000510349, 0.0000510101, 0.0000509854, 0.0000509607, 0.0000509360, 0.0000509113, 0.0000508867, 0.0000508620, 0.0000508374, 0.0000508127, 0.0000507881, 0.0000507635, 0.0000507389, 0.0000507143, 0.0000506897, 0.0000506652, 0.0000506406, 0.0000506161, 0.0000505916, 0.0000505670, 0.0000505425, 0.0000505180, 0.0000504936, 0.0000504691, 0.0000504446, 0.0000504202, 0.0000503958, 0.0000503713, 0.0000503469, 0.0000503225, 0.0000502982, 0.0000502738, 0.0000502494, 0.0000502251, 0.0000502007, 0.0000501764, 0.0000501521, 0.0000501278, 0.0000501035, 0.0000500792, 0.0000500550, 0.0000500307, 0.0000500065, 0.0000499822, 0.0000499580, 0.0000499338, 0.0000499096, 0.0000498854, 0.0000498612, 0.0000498371, 0.0000498129, 0.0000497888, 0.0000497647, 0.0000497405, 0.0000497164, 0.0000496924, 0.0000496683, 0.0000496442, 0.0000496201, 0.0000495961, 0.0000495721, 0.0000495480, 0.0000495240, 0.0000495000, 0.0000494761, 0.0000494521, 0.0000494281, 0.0000494042, 0.0000493802, 0.0000493563, 0.0000493324, 0.0000493085, 0.0000492846, 0.0000492607, 0.0000492368, 0.0000492130, 0.0000491891, 0.0000491653, 0.0000491415, 0.0000491176, 0.0000490938, 0.0000490701, 0.0000490463, 0.0000490225, 0.0000489988, 0.0000489750, 0.0000489513, 0.0000489276, 0.0000489038, 0.0000488802, 0.0000488565, 0.0000488328, 0.0000488091, 0.0000487855, 0.0000487618, 0.0000487382, 0.0000487146, 0.0000486910, 0.0000486674, 0.0000486438, 0.0000486202, 0.0000485967, 0.0000485731, 0.0000485496, 0.0000485261, 0.0000485025, 0.0000484790, 0.0000484556, 0.0000484321, 0.0000484086, 0.0000483851, 0.0000483617, 0.0000483383, 0.0000483148, 0.0000482914, 0.0000482680, 0.0000482446, 0.0000482213, 0.0000481979, 0.0000481745, 0.0000481512, 0.0000481279, 0.0000481045, 0.0000480812, 0.0000480579, 0.0000480346, 0.0000480114, 0.0000479881, 0.0000479648, 0.0000479416, 0.0000479184, 0.0000478952, 0.0000478719, 0.0000478487, 0.0000478256, 0.0000478024, 0.0000477792, 0.0000477561, 0.0000477329, 0.0000477098, 0.0000476867, 0.0000476636, 0.0000476405, 0.0000476174, 0.0000475943, 0.0000475712, 0.0000475482, 0.0000475252, 0.0000475021, 0.0000474791, 0.0000474561, 0.0000474331, 0.0000474101, 0.0000473871, 0.0000473642, 0.0000473412, 0.0000473183, 0.0000472954, 0.0000472724, 0.0000472495, 0.0000472266, 0.0000472038, 0.0000471809, 0.0000471580, 0.0000471352, 0.0000471123, 0.0000470895, 0.0000470667, 0.0000470439, 0.0000470211, 0.0000469983, 0.0000469755, 0.0000469528, 0.0000469300, 0.0000469073, 0.0000468845, 0.0000468618, 0.0000468391, 0.0000468164, 0.0000467937, 0.0000467710, 0.0000467484, 0.0000467257, 0.0000467031, 0.0000466805, 0.0000466578, 0.0000466352, 0.0000466126, 0.0000465900, 0.0000465675, 0.0000465449, 0.0000465223, 0.0000464998, 0.0000464773, 0.0000464547, 0.0000464322, 0.0000464097, 0.0000463872, 0.0000463648, 0.0000463423, 0.0000463198, 0.0000462974, 0.0000462750, 0.0000462525, 0.0000462301, 0.0000462077, 0.0000461853, 0.0000461630, 0.0000461406, 0.0000461182, 0.0000460959, 0.0000460735, 0.0000460512, 0.0000460289, 0.0000460066, 0.0000459843, 0.0000459620, 0.0000459397, 0.0000459175, 0.0000458952, 0.0000458730, 0.0000458508, 0.0000458285, 0.0000458063, 0.0000457841, 0.0000457620, 0.0000457398, 0.0000457176, 0.0000456955, 0.0000456733, 0.0000456512, 0.0000456291, 0.0000456070, 0.0000455849, 0.0000455628, 0.0000455407, 0.0000455186, 0.0000454966, 0.0000454745, 0.0000454525, 0.0000454305, 0.0000454084, 0.0000453864, 0.0000453644, 0.0000453425, 0.0000453205, 0.0000452985, 0.0000452766, 0.0000452546, 0.0000452327, 0.0000452108, 0.0000451889, 0.0000451670, 0.0000451451, 0.0000451232, 0.0000451014, 0.0000450795, 0.0000450577, 0.0000450358, 0.0000450140, 0.0000449922, 0.0000449704, 0.0000449486, 0.0000449268, 0.0000449050, 0.0000448833, 0.0000448615, 0.0000448398, 0.0000448181, 0.0000447963, 0.0000447746, 0.0000447529, 0.0000447313, 0.0000447096, 0.0000446879, 0.0000446663, 0.0000446446, 0.0000446230, 0.0000446014, 0.0000445797, 0.0000445581, 0.0000445365, 0.0000445150, 0.0000444934, 0.0000444718, 0.0000444503, 0.0000444287, 0.0000444072, 0.0000443857, 0.0000443642, 0.0000443427, 0.0000443212, 0.0000442997, 0.0000442783, 0.0000442568, 0.0000442354, 0.0000442139, 0.0000441925, 0.0000441711, 0.0000441497, 0.0000441283, 0.0000441069, 0.0000440855, 0.0000440642, 0.0000440428, 0.0000440215, 0.0000440001, 0.0000439788, 0.0000439575, 0.0000439362, 0.0000439149, 0.0000438936, 0.0000438724, 0.0000438511, 0.0000438299, 0.0000438086, 0.0000437874, 0.0000437662, 0.0000437450, 0.0000437238, 0.0000437026, 0.0000436814, 0.0000436602, 0.0000436391, 0.0000436179, 0.0000435968, 0.0000435757, 0.0000435546, 0.0000435334, 0.0000435124, 0.0000434913, 0.0000434702, 0.0000434491, 0.0000434281, 0.0000434070, 0.0000433860, 0.0000433650, 0.0000433440, 0.0000433230, 0.0000433020, 0.0000432810, 0.0000432600, 0.0000432390, 0.0000432181, 0.0000431971, 0.0000431762, 0.0000431553, 0.0000431344, 0.0000431135, 0.0000430926, 0.0000430717, 0.0000430508, 0.0000430300, 0.0000430091, 0.0000429883, 0.0000429675, 0.0000429466, 0.0000429258, 0.0000429050, 0.0000428842, 0.0000428634, 0.0000428427, 0.0000428219, 0.0000428012, 0.0000427804, 0.0000427597, 0.0000427390, 0.0000427183, 0.0000426976, 0.0000426769, 0.0000426562, 0.0000426355, 0.0000426149, 0.0000425942, 0.0000425736, 0.0000425529, 0.0000425323, 0.0000425117, 0.0000424911, 0.0000424705, 0.0000424499, 0.0000424294, 0.0000424088, 0.0000423883, 0.0000423677, 0.0000423472, 0.0000423267, 0.0000423062, 0.0000422857, 0.0000422652, 0.0000422447, 0.0000422242, 0.0000422038, 0.0000421833, 0.0000421629, 0.0000421424, 0.0000421220, 0.0000421016, 0.0000420812, 0.0000420608, 0.0000420404, 0.0000420201, 0.0000419997, 0.0000419793, 0.0000419590, 0.0000419387, 0.0000419183, 0.0000418980, 0.0000418777, 0.0000418574, 0.0000418371, 0.0000418169, 0.0000417966, 0.0000417764, 0.0000417561, 0.0000417359, 0.0000417157, 0.0000416954, 0.0000416752, 0.0000416550, 0.0000416349, 0.0000416147, 0.0000415945, 0.0000415744, 0.0000415542, 0.0000415341, 0.0000415140, 0.0000414938, 0.0000414737, 0.0000414536, 0.0000414335, 0.0000414135, 0.0000413934, 0.0000413733, 0.0000413533, 0.0000413333, 0.0000413132, 0.0000412932, 0.0000412732, 0.0000412532, 0.0000412332, 0.0000412132, 0.0000411933, 0.0000411733, 0.0000411533, 0.0000411334, 0.0000411135, 0.0000410935, 0.0000410736, 0.0000410537, 0.0000410338, 0.0000410139, 0.0000409941, 0.0000409742, 0.0000409544, 0.0000409345, 0.0000409147, 0.0000408948, 0.0000408750, 0.0000408552, 0.0000408354, 0.0000408156, 0.0000407959, 0.0000407761, 0.0000407563, 0.0000407366, 0.0000407168, 0.0000406971, 0.0000406774, 0.0000406577, 0.0000406380, 0.0000406183, 0.0000405986, 0.0000405789, 0.0000405593, 0.0000405396, 0.0000405200, 0.0000405003, 0.0000404807, 0.0000404611, 0.0000404415, 0.0000404219, 0.0000404023, 0.0000403827, 0.0000403632, 0.0000403436, 0.0000403240, 0.0000403045, 0.0000402850, 0.0000402655, 0.0000402459, 0.0000402264, 0.0000402069, 0.0000401875, 0.0000401680, 0.0000401485, 0.0000401291, 0.0000401096, 0.0000400902, 0.0000400708, 0.0000400513, 0.0000400319, 0.0000400125, 0.0000399931, 0.0000399738, 0.0000399544, 0.0000399350, 0.0000399157, 0.0000398963, 0.0000398770, 0.0000398577, 0.0000398384, 0.0000398191, 0.0000397998, 0.0000397805, 0.0000397612, 0.0000397419, 0.0000397227, 0.0000397034, 0.0000396842, 0.0000396650, 0.0000396457, 0.0000396265, 0.0000396073, 0.0000395881, 0.0000395690, 0.0000395498, 0.0000395306, 0.0000395115, 0.0000394923, 0.0000394732, 0.0000394541, 0.0000394349, 0.0000394158, 0.0000393967, 0.0000393776, 0.0000393586, 0.0000393395, 0.0000393204, 0.0000393014, 0.0000392823, 0.0000392633, 0.0000392443, 0.0000392252, 0.0000392062, 0.0000391872, 0.0000391682, 0.0000391493, 0.0000391303, 0.0000391113, 0.0000390924, 0.0000390734, 0.0000390545, 0.0000390356, 0.0000390167, 0.0000389978, 0.0000389789, 0.0000389600, 0.0000389411, 0.0000389222, 0.0000389034, 0.0000388845, 0.0000388657, 0.0000388468, 0.0000388280, 0.0000388092, 0.0000387904, 0.0000387716, 0.0000387528, 0.0000387340, 0.0000387153, 0.0000386965, 0.0000386777, 0.0000386590, 0.0000386403, 0.0000386215, 0.0000386028, 0.0000385841, 0.0000385654, 0.0000385467, 0.0000385281, 0.0000385094, 0.0000384907, 0.0000384721, 0.0000384534, 0.0000384348, 0.0000384162, 0.0000383976, 0.0000383790, 0.0000383604, 0.0000383418, 0.0000383232, 0.0000383046, 0.0000382861, 0.0000382675, 0.0000382490, 0.0000382304, 0.0000382119, 0.0000381934, 0.0000381749, 0.0000381564, 0.0000381379, 0.0000381194, 0.0000381009, 0.0000380825, 0.0000380640, 0.0000380456, 0.0000380271, 0.0000380087, 0.0000379903, 0.0000379719, 0.0000379535, 0.0000379351, 0.0000379167, 0.0000378983, 0.0000378800, 0.0000378616, 0.0000378433, 0.0000378249, 0.0000378066, 0.0000377883, 0.0000377700, 0.0000377517, 0.0000377334, 0.0000377151, 0.0000376968, 0.0000376785, 0.0000376603, 0.0000376420, 0.0000376238, 0.0000376056, 0.0000375873, 0.0000375691, 0.0000375509, 0.0000375327, 0.0000375145, 0.0000374964, 0.0000374782, 0.0000374600, 0.0000374419, 0.0000374237, 0.0000374056, 0.0000373875, 0.0000373694, 0.0000373512, 0.0000373331, 0.0000373151, 0.0000372970, 0.0000372789, 0.0000372608, 0.0000372428, 0.0000372247, 0.0000372067, 0.0000371887, 0.0000371706, 0.0000371526, 0.0000371346, 0.0000371166, 0.0000370987, 0.0000370807, 0.0000370627, 0.0000370447, 0.0000370268, 0.0000370089, 0.0000369909, 0.0000369730, 0.0000369551, 0.0000369372, 0.0000369193, 0.0000369014, 0.0000368835, 0.0000368656, 0.0000368478, 0.0000368299, 0.0000368121, 0.0000367942, 0.0000367764, 0.0000367586, 0.0000367408, 0.0000367230, 0.0000367052, 0.0000366874, 0.0000366696, 0.0000366518, 0.0000366341, 0.0000366163, 0.0000365986, 0.0000365808, 0.0000365631, 0.0000365454, 0.0000365277, 0.0000365100, 0.0000364923, 0.0000364746, 0.0000364569, 0.0000364393, 0.0000364216, 0.0000364040, 0.0000363863, 0.0000363687, 0.0000363511, 0.0000363334, 0.0000363158, 0.0000362982, 0.0000362807, 0.0000362631, 0.0000362455, 0.0000362279, 0.0000362104, 0.0000361928, 0.0000361753, 0.0000361578, 0.0000361402, 0.0000361227, 0.0000361052, 0.0000360877, 0.0000360702, 0.0000360528, 0.0000360353, 0.0000360178, 0.0000360004, 0.0000359829, 0.0000359655, 0.0000359481, 0.0000359307, 0.0000359132, 0.0000358958, 0.0000358784, 0.0000358611, 0.0000358437, 0.0000358263, 0.0000358090, 0.0000357916, 0.0000357743, 0.0000357569, 0.0000357396, 0.0000357223, 0.0000357050, 0.0000356877, 0.0000356704, 0.0000356531, 0.0000356358, 0.0000356185, 0.0000356013, 0.0000355840, 0.0000355668, 0.0000355496, 0.0000355323, 0.0000355151, 0.0000354979, 0.0000354807, 0.0000354635, 0.0000354463, 0.0000354291, 0.0000354120, 0.0000353948, 0.0000353777, 0.0000353605, 0.0000353434, 0.0000353263, 0.0000353091, 0.0000352920, 0.0000352749, 0.0000352578, 0.0000352407, 0.0000352237, 0.0000352066, 0.0000351895, 0.0000351725, 0.0000351554, 0.0000351384, 0.0000351214, 0.0000351044, 0.0000350874, 0.0000350704, 0.0000350534, 0.0000350364, 0.0000350194, 0.0000350024, 0.0000349855, 0.0000349685, 0.0000349516, 0.0000349346, 0.0000349177, 0.0000349008, 0.0000348839, 0.0000348670, 0.0000348501, 0.0000348332, 0.0000348163, 0.0000347994, 0.0000347826, 0.0000347657, 0.0000347489, 0.0000347320, 0.0000347152, 0.0000346984, 0.0000346816, 0.0000346648, 0.0000346480, 0.0000346312, 0.0000346144, 0.0000345976, 0.0000345808, 0.0000345641, 0.0000345473, 0.0000345306, 0.0000345139, 0.0000344971, 0.0000344804, 0.0000344637, 0.0000344470, 0.0000344303, 0.0000344136, 0.0000343970, 0.0000343803, 0.0000343636, 0.0000343470, 0.0000343303, 0.0000343137, 0.0000342971, 0.0000342805, 0.0000342638, 0.0000342472, 0.0000342306, 0.0000342141, 0.0000341975, 0.0000341809, 0.0000341643, 0.0000341478, 0.0000341312, 0.0000341147, 0.0000340982, 0.0000340817, 0.0000340651, 0.0000340486, 0.0000340321, 0.0000340156, 0.0000339992, 0.0000339827, 0.0000339662, 0.0000339498, 0.0000339333, 0.0000339169, 0.0000339004, 0.0000338840, 0.0000338676, 0.0000338512, 0.0000338348, 0.0000338184, 0.0000338020, 0.0000337856, 0.0000337692, 0.0000337529, 0.0000337365, 0.0000337202, 0.0000337038, 0.0000336875, 0.0000336712, 0.0000336548, 0.0000336385, 0.0000336222, 0.0000336059, 0.0000335897, 0.0000335734, 0.0000335571, 0.0000335409, 0.0000335246, 0.0000335084, 0.0000334921, 0.0000334759, 0.0000334597, 0.0000334435, 0.0000334272, 0.0000334111, 0.0000333949, 0.0000333787, 0.0000333625, 0.0000333463, 0.0000333302, 0.0000333140, 0.0000332979, 0.0000332817, 0.0000332656, 0.0000332495, 0.0000332334, 0.0000332173, 0.0000332012, 0.0000331851, 0.0000331690, 0.0000331529, 0.0000331369, 0.0000331208, 0.0000331048, 0.0000330887, 0.0000330727, 0.0000330567, 0.0000330407, 0.0000330246, 0.0000330086, 0.0000329926, 0.0000329767, 0.0000329607, 0.0000329447, 0.0000329287, 0.0000329128, 0.0000328968, 0.0000328809, 0.0000328650, 0.0000328490, 0.0000328331, 0.0000328172, 0.0000328013, 0.0000327854, 0.0000327695, 0.0000327536, 0.0000327378, 0.0000327219, 0.0000327061, 0.0000326902, 0.0000326744, 0.0000326585, 0.0000326427, 0.0000326269, 0.0000326111, 0.0000325953, 0.0000325795, 0.0000325637, 0.0000325479, 0.0000325321, 0.0000325164, 0.0000325006, 0.0000324849, 0.0000324691, 0.0000324534, 0.0000324377, 0.0000324220, 0.0000324062, 0.0000323905, 0.0000323748, 0.0000323592, 0.0000323435, 0.0000323278, 0.0000323121, 0.0000322965, 0.0000322808, 0.0000322652, 0.0000322495, 0.0000322339, 0.0000322183, 0.0000322027, 0.0000321871, 0.0000321715, 0.0000321559, 0.0000321403, 0.0000321247, 0.0000321092, 0.0000320936, 0.0000320781, 0.0000320625, 0.0000320470, 0.0000320315, 0.0000320159, 0.0000320004, 0.0000319849, 0.0000319694, 0.0000319539, 0.0000319384, 0.0000319230, 0.0000319075, 0.0000318920, 0.0000318766, 0.0000318611, 0.0000318457, 0.0000318303, 0.0000318148, 0.0000317994, 0.0000317840, 0.0000317686, 0.0000317532, 0.0000317378, 0.0000317224, 0.0000317071, 0.0000316917, 0.0000316764, 0.0000316610, 0.0000316457, 0.0000316303, 0.0000316150, 0.0000315997, 0.0000315844, 0.0000315691, 0.0000315538, 0.0000315385, 0.0000315232, 0.0000315079, 0.0000314926, 0.0000314774, 0.0000314621, 0.0000314469, 0.0000314316, 0.0000314164, 0.0000314012, 0.0000313860, 0.0000313708, 0.0000313556, 0.0000313404, 0.0000313252, 0.0000313100, 0.0000312948, 0.0000312797, 0.0000312645, 0.0000312494, 0.0000312342, 0.0000312191, 0.0000312040, 0.0000311888, 0.0000311737, 0.0000311586, 0.0000311435, 0.0000311284, 0.0000311133, 0.0000310983, 0.0000310832, 0.0000310681, 0.0000310531, 0.0000310380, 0.0000310230, 0.0000310080, 0.0000309929, 0.0000309779, 0.0000309629, 0.0000309479, 0.0000309329, 0.0000309179, 0.0000309029, 0.0000308880, 0.0000308730, 0.0000308580, 0.0000308431, 0.0000308281, 0.0000308132, 0.0000307983, 0.0000307833, 0.0000307684, 0.0000307535, 0.0000307386, 0.0000307237, 0.0000307088, 0.0000306939, 0.0000306791, 0.0000306642, 0.0000306493, 0.0000306345, 0.0000306196, 0.0000306048, 0.0000305900, 0.0000305752, 0.0000305603, 0.0000305455, 0.0000305307, 0.0000305159, 0.0000305011, 0.0000304864, 0.0000304716, 0.0000304568, 0.0000304421, 0.0000304273, 0.0000304126, 0.0000303978, 0.0000303831, 0.0000303684, 0.0000303537, 0.0000303390, 0.0000303243, 0.0000303096, 0.0000302949, 0.0000302802, 0.0000302655, 0.0000302509, 0.0000302362, 0.0000302215, 0.0000302069, 0.0000301923, 0.0000301776, 0.0000301630, 0.0000301484, 0.0000301338, 0.0000301192, 0.0000301046, 0.0000300900, 0.0000300754, 0.0000300608, 0.0000300463, 0.0000300317, 0.0000300172, 0.0000300026, 0.0000299881, 0.0000299735, 0.0000299590, 0.0000299445, 0.0000299300, 0.0000299155, 0.0000299010, 0.0000298865, 0.0000298720, 0.0000298576, 0.0000298431, 0.0000298286, 0.0000298142, 0.0000297997, 0.0000297853, 0.0000297708, 0.0000297564, 0.0000297420, 0.0000297276, 0.0000297132, 0.0000296988, 0.0000296844, 0.0000296700, 0.0000296556, 0.0000296413, 0.0000296269, 0.0000296125, 0.0000295982, 0.0000295838, 0.0000295695, 0.0000295552, 0.0000295409, 0.0000295265, 0.0000295122, 0.0000294979, 0.0000294836, 0.0000294694, 0.0000294551, 0.0000294408, 0.0000294265, 0.0000294123, 0.0000293980, 0.0000293838, 0.0000293695, 0.0000293553, 0.0000293411, 0.0000293269, 0.0000293127, 0.0000292985, 0.0000292843, 0.0000292701, 0.0000292559, 0.0000292417, 0.0000292275, 0.0000292134, 0.0000291992, 0.0000291851, 0.0000291709, 0.0000291568, 0.0000291427, 0.0000291285, 0.0000291144, 0.0000291003, 0.0000290862, 0.0000290721, 0.0000290580, 0.0000290440, 0.0000290299, 0.0000290158, 0.0000290017, 0.0000289877, 0.0000289736, 0.0000289596, 0.0000289456, 0.0000289315, 0.0000289175, 0.0000289035, 0.0000288895, 0.0000288755, 0.0000288615, 0.0000288475, 0.0000288336, 0.0000288196, 0.0000288056, 0.0000287917, 0.0000287777, 0.0000287638, 0.0000287498, 0.0000287359, 0.0000287220, 0.0000287080, 0.0000286941, 0.0000286802, 0.0000286663, 0.0000286524, 0.0000286386, 0.0000286247, 0.0000286108, 0.0000285969, 0.0000285831, 0.0000285692, 0.0000285554, 0.0000285416, 0.0000285277, 0.0000285139, 0.0000285001, 0.0000284863, 0.0000284725, 0.0000284587, 0.0000284449, 0.0000284311, 0.0000284173, 0.0000284036, 0.0000283898, 0.0000283760, 0.0000283623, 0.0000283485, 0.0000283348, 0.0000283211, 0.0000283073, 0.0000282936, 0.0000282799, 0.0000282662, 0.0000282525, 0.0000282388, 0.0000282251, 0.0000282115, 0.0000281978, 0.0000281841, 0.0000281705, 0.0000281568, 0.0000281432, 0.0000281295, 0.0000281159, 0.0000281023, 0.0000280887, 0.0000280751, 0.0000280615, 0.0000280479, 0.0000280343, 0.0000280207, 0.0000280071, 0.0000279935, 0.0000279800, 0.0000279664, 0.0000279529, 0.0000279393, 0.0000279258, 0.0000279122, 0.0000278987, 0.0000278852, 0.0000278717, 0.0000278582, 0.0000278447, 0.0000278312, 0.0000278177, 0.0000278042, 0.0000277907, 0.0000277773, 0.0000277638, 0.0000277504, 0.0000277369, 0.0000277235, 0.0000277100, 0.0000276966, 0.0000276832, 0.0000276698, 0.0000276564, 0.0000276430, 0.0000276296, 0.0000276162, 0.0000276028, 0.0000275894, 0.0000275761, 0.0000275627, 0.0000275493, 0.0000275360, 0.0000275226, 0.0000275093, 0.0000274960, 0.0000274827, 0.0000274693, 0.0000274560, 0.0000274427, 0.0000274294, 0.0000274161, 0.0000274028, 0.0000273896, 0.0000273763, 0.0000273630, 0.0000273498, 0.0000273365, 0.0000273233, 0.0000273100, 0.0000272968, 0.0000272836, 0.0000272703, 0.0000272571, 0.0000272439, 0.0000272307, 0.0000272175, 0.0000272043, 0.0000271912, 0.0000271780, 0.0000271648, 0.0000271516, 0.0000271385, 0.0000271253, 0.0000271122, 0.0000270991, 0.0000270859, 0.0000270728, 0.0000270597, 0.0000270466, 0.0000270335, 0.0000270204, 0.0000270073, 0.0000269942, 0.0000269811, 0.0000269680, 0.0000269550, 0.0000269419, 0.0000269288, 0.0000269158, 0.0000269028, 0.0000268897, 0.0000268767, 0.0000268637, 0.0000268506, 0.0000268376, 0.0000268246, 0.0000268116, 0.0000267986, 0.0000267857, 0.0000267727, 0.0000267597, 0.0000267467, 0.0000267338, 0.0000267208, 0.0000267079, 0.0000266949, 0.0000266820, 0.0000266691, 0.0000266561, 0.0000266432, 0.0000266303, 0.0000266174, 0.0000266045, 0.0000265916, 0.0000265787, 0.0000265658, 0.0000265530, 0.0000265401, 0.0000265272, 0.0000265144, 0.0000265015, 0.0000264887, 0.0000264759, 0.0000264630, 0.0000264502, 0.0000264374, 0.0000264246, 0.0000264118, 0.0000263990, 0.0000263862, 0.0000263734, 0.0000263606, 0.0000263479, 0.0000263351, 0.0000263223, 0.0000263096, 0.0000262968, 0.0000262841, 0.0000262713, 0.0000262586, 0.0000262459, 0.0000262332, 0.0000262205, 0.0000262077, 0.0000261950, 0.0000261824, 0.0000261697, 0.0000261570, 0.0000261443, 0.0000261316, 0.0000261190, 0.0000261063, 0.0000260937, 0.0000260810, 0.0000260684, 0.0000260558, 0.0000260431, 0.0000260305, 0.0000260179, 0.0000260053, 0.0000259927, 0.0000259801, 0.0000259675, 0.0000259549, 0.0000259423, 0.0000259298, 0.0000259172, 0.0000259046, 0.0000258921, 0.0000258795, 0.0000258670, 0.0000258545, 0.0000258419, 0.0000258294, 0.0000258169, 0.0000258044, 0.0000257919, 0.0000257794, 0.0000257669, 0.0000257544, 0.0000257419, 0.0000257295, 0.0000257170, 0.0000257045, 0.0000256921, 0.0000256796, 0.0000256672, 0.0000256547, 0.0000256423, 0.0000256299, 0.0000256175, 0.0000256051, 0.0000255926, 0.0000255802, 0.0000255679, 0.0000255555, 0.0000255431, 0.0000255307, 0.0000255183, 0.0000255060, 0.0000254936, 0.0000254812, 0.0000254689, 0.0000254566, 0.0000254442, 0.0000254319, 0.0000254196, 0.0000254073, 0.0000253949, 0.0000253826, 0.0000253703, 0.0000253580, 0.0000253458, 0.0000253335, 0.0000253212, 0.0000253089, 0.0000252967, 0.0000252844, 0.0000252722, 0.0000252599, 0.0000252477, 0.0000252354, 0.0000252232, 0.0000252110, 0.0000251988, 0.0000251866, 0.0000251743, 0.0000251621, 0.0000251500, 0.0000251378, 0.0000251256, 0.0000251134, 0.0000251012, 0.0000250891, 0.0000250769, 0.0000250648, 0.0000250526, 0.0000250405, 0.0000250283, 0.0000250162, 0.0000250041, 0.0000249920, 0.0000249799, 0.0000249678, 0.0000249557, 0.0000249436, 0.0000249315, 0.0000249194, 0.0000249073, 0.0000248953, 0.0000248832, 0.0000248711, 0.0000248591, 0.0000248470, 0.0000248350, 0.0000248230, 0.0000248109, 0.0000247989, 0.0000247869, 0.0000247749, 0.0000247629, 0.0000247509, 0.0000247389, 0.0000247269, 0.0000247149, 0.0000247029, 0.0000246910, 0.0000246790, 0.0000246671, 0.0000246551, 0.0000246432, 0.0000246312, 0.0000246193, 0.0000246073, 0.0000245954, 0.0000245835, 0.0000245716, 0.0000245597, 0.0000245478, 0.0000245359, 0.0000245240, 0.0000245121, 0.0000245002, 0.0000244884, 0.0000244765, 0.0000244646, 0.0000244528, 0.0000244409, 0.0000244291, 0.0000244172, 0.0000244054, 0.0000243936, 0.0000243818, 0.0000243700, 0.0000243581, 0.0000243463, 0.0000243345, 0.0000243228, 0.0000243110, 0.0000242992, 0.0000242874, 0.0000242756, 0.0000242639, 0.0000242521, 0.0000242404, 0.0000242286, 0.0000242169, 0.0000242051, 0.0000241934, 0.0000241817, 0.0000241700, 0.0000241583, 0.0000241466, 0.0000241349, 0.0000241232, 0.0000241115, 0.0000240998, 0.0000240881, 0.0000240764, 0.0000240648, 0.0000240531, 0.0000240415, 0.0000240298, 0.0000240182, 0.0000240065, 0.0000239949, 0.0000239833, 0.0000239716, 0.0000239600, 0.0000239484, 0.0000239368, 0.0000239252, 0.0000239136, 0.0000239020, 0.0000238904, 0.0000238789, 0.0000238673, 0.0000238557, 0.0000238442, 0.0000238326, 0.0000238211, 0.0000238095, 0.0000237980, 0.0000237865, 0.0000237749, 0.0000237634, 0.0000237519, 0.0000237404, 0.0000237289, 0.0000237174, 0.0000237059, 0.0000236944, 0.0000236829, 0.0000236714, 0.0000236600, 0.0000236485, 0.0000236370, 0.0000236256, 0.0000236141, 0.0000236027, 0.0000235913, 0.0000235798, 0.0000235684, 0.0000235570, 0.0000235456, 0.0000235342, 0.0000235228, 0.0000235114, 0.0000235000, 0.0000234886, 0.0000234772, 0.0000234658, 0.0000234544, 0.0000234431, 0.0000234317, 0.0000234204, 0.0000234090, 0.0000233977, 0.0000233863, 0.0000233750, 0.0000233637, 0.0000233524, 0.0000233410, 0.0000233297, 0.0000233184, 0.0000233071, 0.0000232958, 0.0000232845, 0.0000232733, 0.0000232620, 0.0000232507, 0.0000232394, 0.0000232282, 0.0000232169, 0.0000232057, 0.0000231944, 0.0000231832, 0.0000231720, 0.0000231607, 0.0000231495, 0.0000231383, 0.0000231271, 0.0000231159, 0.0000231047, 0.0000230935, 0.0000230823, 0.0000230711, 0.0000230599, 0.0000230487, 0.0000230376, 0.0000230264, 0.0000230153, 0.0000230041, 0.0000229930, 0.0000229818, 0.0000229707, 0.0000229595, 0.0000229484, 0.0000229373, 0.0000229262, 0.0000229151, 0.0000229040, 0.0000228929, 0.0000228818, 0.0000228707, 0.0000228596, 0.0000228485, 0.0000228375, 0.0000228264, 0.0000228153, 0.0000228043, 0.0000227932, 0.0000227822, 0.0000227711, 0.0000227601, 0.0000227491, 0.0000227381, 0.0000227270, 0.0000227160, 0.0000227050, 0.0000226940, 0.0000226830, 0.0000226720, 0.0000226610, 0.0000226501, 0.0000226391, 0.0000226281, 0.0000226171, 0.0000226062, 0.0000225952, 0.0000225843, 0.0000225733, 0.0000225624, 0.0000225515, 0.0000225405, 0.0000225296, 0.0000225187, 0.0000225078, 0.0000224969, 0.0000224860, 0.0000224751, 0.0000224642, 0.0000224533, 0.0000224424, 0.0000224315, 0.0000224207, 0.0000224098, 0.0000223990, 0.0000223881, 0.0000223773, 0.0000223664, 0.0000223556, 0.0000223447, 0.0000223339, 0.0000223231, 0.0000223123, 0.0000223015, 0.0000222907, 0.0000222798, 0.0000222691, 0.0000222583, 0.0000222475, 0.0000222367, 0.0000222259, 0.0000222151, 0.0000222044, 0.0000221936, 0.0000221829, 0.0000221721, 0.0000221614, 0.0000221506, 0.0000221399, 0.0000221292, 0.0000221185, 0.0000221077, 0.0000220970, 0.0000220863, 0.0000220756, 0.0000220649, 0.0000220542, 0.0000220435, 0.0000220329, 0.0000220222, 0.0000220115, 0.0000220008, 0.0000219902, 0.0000219795, 0.0000219689, 0.0000219582, 0.0000219476, 0.0000219370, 0.0000219263, 0.0000219157, 0.0000219051, 0.0000218945, 0.0000218839, 0.0000218732, 0.0000218626, 0.0000218521, 0.0000218415, 0.0000218309, 0.0000218203, 0.0000218097, 0.0000217992, 0.0000217886, 0.0000217780, 0.0000217675, 0.0000217569, 0.0000217464, 0.0000217359, 0.0000217253, 0.0000217148, 0.0000217043, 0.0000216938, 0.0000216832, 0.0000216727, 0.0000216622, 0.0000216517, 0.0000216412, 0.0000216308, 0.0000216203, 0.0000216098, 0.0000215993, 0.0000215889, 0.0000215784, 0.0000215679, 0.0000215575, 0.0000215470, 0.0000215366, 0.0000215262, 0.0000215157, 0.0000215053, 0.0000214949, 0.0000214845, 0.0000214741, 0.0000214637, 0.0000214533, 0.0000214429, 0.0000214325, 0.0000214221, 0.0000214117, 0.0000214013, 0.0000213910, 0.0000213806, 0.0000213702, 0.0000213599, 0.0000213495, 0.0000213392, 0.0000213288, 0.0000213185, 0.0000213082, 0.0000212978, 0.0000212875, 0.0000212772, 0.0000212669, 0.0000212566, 0.0000212463, 0.0000212360, 0.0000212257, 0.0000212154, 0.0000212051, 0.0000211949, 0.0000211846, 0.0000211743, 0.0000211641, 0.0000211538, 0.0000211436, 0.0000211333, 0.0000211231, 0.0000211128, 0.0000211026, 0.0000210924, 0.0000210822, 0.0000210720, 0.0000210617, 0.0000210515, 0.0000210413, 0.0000210311, 0.0000210209, 0.0000210108, 0.0000210006, 0.0000209904, 0.0000209802, 0.0000209701, 0.0000209599, 0.0000209497, 0.0000209396, 0.0000209294, 0.0000209193, 0.0000209092, 0.0000208990, 0.0000208889, 0.0000208788, 0.0000208687, 0.0000208586, 0.0000208484, 0.0000208383, 0.0000208282, 0.0000208182, 0.0000208081, 0.0000207980, 0.0000207879, 0.0000207778, 0.0000207678, 0.0000207577, 0.0000207476, 0.0000207376, 0.0000207275, 0.0000207175, 0.0000207075, 0.0000206974, 0.0000206874, 0.0000206774, 0.0000206673, 0.0000206573, 0.0000206473, 0.0000206373, 0.0000206273, 0.0000206173, 0.0000206073, 0.0000205973, 0.0000205874, 0.0000205774, 0.0000205674, 0.0000205575, 0.0000205475, 0.0000205375, 0.0000205276, 0.0000205176, 0.0000205077, 0.0000204978, 0.0000204878, 0.0000204779, 0.0000204680, 0.0000204581, 0.0000204481, 0.0000204382, 0.0000204283, 0.0000204184, 0.0000204085, 0.0000203986, 0.0000203888, 0.0000203789, 0.0000203690, 0.0000203591, 0.0000203493, 0.0000203394, 0.0000203296, 0.0000203197, 0.0000203099, 0.0000203000, 0.0000202902, 0.0000202803, 0.0000202705, 0.0000202607, 0.0000202509, 0.0000202411, 0.0000202313, 0.0000202214, 0.0000202117, 0.0000202019, 0.0000201921, 0.0000201823, 0.0000201725, 0.0000201627, 0.0000201530, 0.0000201432, 0.0000201334, 0.0000201237, 0.0000201139, 0.0000201042, 0.0000200944, 0.0000200847, 0.0000200750, 0.0000200652, 0.0000200555, 0.0000200458, 0.0000200361, 0.0000200264, 0.0000200167, 0.0000200070, 0.0000199973, 0.0000199876, 0.0000199779, 0.0000199682, 0.0000199585, 0.0000199489, 0.0000199392, 0.0000199295, 0.0000199199, 0.0000199102, 0.0000199006, 0.0000198909, 0.0000198813, 0.0000198717, 0.0000198620, 0.0000198524, 0.0000198428, 0.0000198332, 0.0000198236, 0.0000198140, 0.0000198044, 0.0000197948, 0.0000197852, 0.0000197756, 0.0000197660, 0.0000197564, 0.0000197468, 0.0000197373, 0.0000197277, 0.0000197182, 0.0000197086, 0.0000196990, 0.0000196895, 0.0000196800, 0.0000196704, 0.0000196609, 0.0000196514, 0.0000196418, 0.0000196323, 0.0000196228, 0.0000196133, 0.0000196038, 0.0000195943, 0.0000195848, 0.0000195753, 0.0000195658, 0.0000195563, 0.0000195469, 0.0000195374, 0.0000195279, 0.0000195185, 0.0000195090, 0.0000194996, 0.0000194901, 0.0000194807, 0.0000194712, 0.0000194618, 0.0000194524, 0.0000194429, 0.0000194335, 0.0000194241, 0.0000194147, 0.0000194053, 0.0000193959, 0.0000193865, 0.0000193771, 0.0000193677, 0.0000193583, 0.0000193489, 0.0000193395, 0.0000193302, 0.0000193208, 0.0000193114, 0.0000193021, 0.0000192927, 0.0000192834, 0.0000192740, 0.0000192647, 0.0000192554, 0.0000192460, 0.0000192367, 0.0000192274, 0.0000192181, 0.0000192088, 0.0000191994, 0.0000191901, 0.0000191808, 0.0000191716, 0.0000191623, 0.0000191530, 0.0000191437, 0.0000191344, 0.0000191251, 0.0000191159, 0.0000191066, 0.0000190974, 0.0000190881, 0.0000190789, 0.0000190696, 0.0000190604, 0.0000190511, 0.0000190419, 0.0000190327, 0.0000190235, 0.0000190142, 0.0000190050, 0.0000189958, 0.0000189866, 0.0000189774, 0.0000189682, 0.0000189590, 0.0000189498, 0.0000189406, 0.0000189315, 0.0000189223, 0.0000189131, 0.0000189040, 0.0000188948, 0.0000188856, 0.0000188765, 0.0000188673, 0.0000188582, 0.0000188491, 0.0000188399, 0.0000188308, 0.0000188217, 0.0000188126, 0.0000188034, 0.0000187943, 0.0000187852, 0.0000187761, 0.0000187670, 0.0000187579, 0.0000187488, 0.0000187397, 0.0000187307, 0.0000187216, 0.0000187125, 0.0000187035, 0.0000186944, 0.0000186853, 0.0000186763, 0.0000186672, 0.0000186582, 0.0000186491, 0.0000186401, 0.0000186311, 0.0000186220, 0.0000186130, 0.0000186040, 0.0000185950, 0.0000185860, 0.0000185770, 0.0000185680, 0.0000185590, 0.0000185500, 0.0000185410, 0.0000185320, 0.0000185230, 0.0000185140, 0.0000185051, 0.0000184961, 0.0000184871, 0.0000184782, 0.0000184692, 0.0000184603, 0.0000184513, 0.0000184424, 0.0000184335, 0.0000184245, 0.0000184156, 0.0000184067, 0.0000183978, 0.0000183888, 0.0000183799, 0.0000183710, 0.0000183621, 0.0000183532, 0.0000183443, 0.0000183354, 0.0000183266, 0.0000183177, 0.0000183088, 0.0000182999, 0.0000182911, 0.0000182822, 0.0000182733, 0.0000182645, 0.0000182556, 0.0000182468, 0.0000182379, 0.0000182291, 0.0000182203, 0.0000182114, 0.0000182026, 0.0000181938, 0.0000181850, 0.0000181762, 0.0000181674, 0.0000181586, 0.0000181498, 0.0000181410, 0.0000181322, 0.0000181234, 0.0000181146, 0.0000181058, 0.0000180971, 0.0000180883, 0.0000180795, 0.0000180708, 0.0000180620, 0.0000180532, 0.0000180445, 0.0000180358, 0.0000180270, 0.0000180183, 0.0000180095, 0.0000180008, 0.0000179921, 0.0000179834, 0.0000179747, 0.0000179660, 0.0000179572, 0.0000179485, 0.0000179398, 0.0000179312, 0.0000179225, 0.0000179138, 0.0000179051, 0.0000178964, 0.0000178878, 0.0000178791, 0.0000178704, 0.0000178618, 0.0000178531, 0.0000178445, 0.0000178358, 0.0000178272, 0.0000178185, 0.0000178099, 0.0000178013, 0.0000177926, 0.0000177840, 0.0000177754, 0.0000177668, 0.0000177582, 0.0000177496, 0.0000177410, 0.0000177324, 0.0000177238, 0.0000177152, 0.0000177066, 0.0000176980, 0.0000176894, 0.0000176809, 0.0000176723, 0.0000176637, 0.0000176552, 0.0000176466, 0.0000176381, 0.0000176295, 0.0000176210, 0.0000176125, 0.0000176039, 0.0000175954, 0.0000175869, 0.0000175783, 0.0000175698, 0.0000175613, 0.0000175528, 0.0000175443, 0.0000175358, 0.0000175273, 0.0000175188, 0.0000175103, 0.0000175018, 0.0000174933, 0.0000174849, 0.0000174764, 0.0000174679, 0.0000174595, 0.0000174510, 0.0000174425, 0.0000174341, 0.0000174256, 0.0000174172, 0.0000174088, 0.0000174003, 0.0000173919, 0.0000173835, 0.0000173750, 0.0000173666, 0.0000173582, 0.0000173498, 0.0000173414, 0.0000173330, 0.0000173246, 0.0000173162, 0.0000173078, 0.0000172994, 0.0000172910, 0.0000172826, 0.0000172743};

  float sbr_et[interface::bril::MAX_NBX] = { 1.0000000000, 0.0412800000, 0.0046000000, -0.0003400000, 0.0009300000, 0.0007800000, 0.0007150000, 0.0007030000, 0.0004816297, 0.0004761712, 0.0004707746, 0.0004654392, 0.0004601643, 0.0004549491, 0.0004497931, 0.0004446954, 0.0004396556, 0.0004346729, 0.0004297466, 0.0004248762, 0.0004200609, 0.0004153003, 0.0004105936, 0.0004059402, 0.0004013396, 0.0003967911, 0.0003922942, 0.0003878482, 0.0003834526, 0.0003791068, 0.0003748103, 0.0003705625, 0.0003663628, 0.0003622107, 0.0003581057, 0.0003540472, 0.0003500347, 0.0003460677, 0.0003421456, 0.0003382680, 0.0003344343, 0.0003306440, 0.0003268968, 0.0003231920, 0.0003195291, 0.0003159078, 0.0003123276, 0.0003087879, 0.0003052883, 0.0003018284, 0.0002984077, 0.0002950258, 0.0002916822, 0.0002883764, 0.0002851082, 0.0002818770, 0.0002786824, 0.0002755240, 0.0002724014, 0.0002693142, 0.0002662620, 0.0002632444, 0.0002602610, 0.0002573114, 0.0002543952, 0.0002515121, 0.0002486616, 0.0002458435, 0.0002430573, 0.0002403027, 0.0002375792, 0.0002348867, 0.0002322247, 0.0002295928, 0.0002269908, 0.0002244182, 0.0002218748, 0.0002193603, 0.0002168742, 0.0002144163, 0.0002119863, 0.0002095838, 0.0002072085, 0.0002048602, 0.0002025384, 0.0002002430, 0.0001979736, 0.0001957299, 0.0001935116, 0.0001913185, 0.0001891503, 0.0001870066, 0.0001848872, 0.0001827918, 0.0001807202, 0.0001786720, 0.0001766471, 0.0001746451, 0.0001726658, 0.0001707089, 0.0001687743, 0.0001668615, 0.0001649704, 0.0001631008, 0.0001612523, 0.0001594248, 0.0001576180, 0.0001558316, 0.0001540656, 0.0001523195, 0.0001505932, 0.0001488865, 0.0001471991, 0.0001455309, 0.0001438816, 0.0001422509, 0.0001406387, 0.0001390448, 0.0001374690, 0.0001359110, 0.0001343707, 0.0001328479, 0.0001313423, 0.0001298537, 0.0001283821, 0.0001269271, 0.0001254886, 0.0001240664, 0.0001226603, 0.0001212702, 0.0001198958, 0.0001185370, 0.0001171936, 0.0001158654, 0.0001145522, 0.0001132540, 0.0001119705, 0.0001117781, 0.0001115861, 0.0001113944, 0.0001112030, 0.0001110120, 0.0001108212, 0.0001106309, 0.0001104408, 0.0001102511, 0.0001100617, 0.0001098726, 0.0001096838, 0.0001094954, 0.0001093073, 0.0001091195, 0.0001089320, 0.0001087449, 0.0001085581, 0.0001083716, 0.0001081854, 0.0001079995, 0.0001078140, 0.0001076288, 0.0001074439, 0.0001072593, 0.0001070750, 0.0001068911, 0.0001067075, 0.0001065241, 0.0001063411, 0.0001061584, 0.0001059761, 0.0001057940, 0.0001056123, 0.0001054308, 0.0001052497, 0.0001050689, 0.0001048884, 0.0001047082, 0.0001045283, 0.0001043487, 0.0001041695, 0.0001039905, 0.0001038119, 0.0001036335, 0.0001034555, 0.0001032777, 0.0001031003, 0.0001029232, 0.0001027464, 0.0001025699, 0.0001023936, 0.0001022177, 0.0001020421, 0.0001018668, 0.0001016918, 0.0001015171, 0.0001013427, 0.0001011686, 0.0001009948, 0.0001008213, 0.0001006481, 0.0001004752, 0.0001003026, 0.0001001303, 0.0000999583, 0.0000997865, 0.0000996151, 0.0000994440, 0.0000992731, 0.0000991026, 0.0000989323, 0.0000987624, 0.0000985927, 0.0000984233, 0.0000982542, 0.0000980854, 0.0000979169, 0.0000977487, 0.0000975808, 0.0000974132, 0.0000972458, 0.0000970787, 0.0000969120, 0.0000967455, 0.0000965793, 0.0000964133, 0.0000962477, 0.0000960824, 0.0000959173, 0.0000957525, 0.0000955880, 0.0000954238, 0.0000952599, 0.0000950962, 0.0000949328, 0.0000947698, 0.0000946069, 0.0000944444, 0.0000942822, 0.0000941202, 0.0000939585, 0.0000937971, 0.0000936359, 0.0000934751, 0.0000933145, 0.0000931542, 0.0000929942, 0.0000928344, 0.0000926749, 0.0000925157, 0.0000923568, 0.0000921981, 0.0000920397, 0.0000918816, 0.0000917237, 0.0000915662, 0.0000914089, 0.0000912518, 0.0000910950, 0.0000909386, 0.0000907823, 0.0000906264, 0.0000904707, 0.0000903152, 0.0000901601, 0.0000900052, 0.0000898506, 0.0000896962, 0.0000895421, 0.0000893883, 0.0000892347, 0.0000890814, 0.0000889284, 0.0000887756, 0.0000886231, 0.0000884709, 0.0000883189, 0.0000881671, 0.0000880157, 0.0000878645, 0.0000877135, 0.0000875628, 0.0000874124, 0.0000872622, 0.0000871123, 0.0000869627, 0.0000868133, 0.0000866641, 0.0000865152, 0.0000863666, 0.0000862182, 0.0000860701, 0.0000859223, 0.0000857746, 0.0000856273, 0.0000854802, 0.0000853333, 0.0000851867, 0.0000850404, 0.0000848943, 0.0000847484, 0.0000846029, 0.0000844575, 0.0000843124, 0.0000841676, 0.0000840230, 0.0000838786, 0.0000837345, 0.0000835907, 0.0000834471, 0.0000833037, 0.0000831606, 0.0000830177, 0.0000828751, 0.0000827327, 0.0000825906, 0.0000824487, 0.0000823071, 0.0000821657, 0.0000820245, 0.0000818836, 0.0000817429, 0.0000816025, 0.0000814623, 0.0000813224, 0.0000811827, 0.0000810432, 0.0000809040, 0.0000807650, 0.0000806262, 0.0000804877, 0.0000803494, 0.0000802114, 0.0000800736, 0.0000799360, 0.0000797987, 0.0000796616, 0.0000795248, 0.0000793881, 0.0000792518, 0.0000791156, 0.0000789797, 0.0000788440, 0.0000787086, 0.0000785733, 0.0000784384, 0.0000783036, 0.0000781691, 0.0000780348, 0.0000779007, 0.0000777669, 0.0000776333, 0.0000774999, 0.0000773668, 0.0000772339, 0.0000771012, 0.0000769687, 0.0000768365, 0.0000767045, 0.0000765727, 0.0000764412, 0.0000763099, 0.0000761788, 0.0000760479, 0.0000759172, 0.0000757868, 0.0000756566, 0.0000755267, 0.0000753969, 0.0000752674, 0.0000751381, 0.0000750090, 0.0000748801, 0.0000747515, 0.0000746231, 0.0000744949, 0.0000743669, 0.0000742391, 0.0000741116, 0.0000739843, 0.0000738572, 0.0000737303, 0.0000736036, 0.0000734772, 0.0000733509, 0.0000732249, 0.0000730991, 0.0000729735, 0.0000728482, 0.0000727230, 0.0000725981, 0.0000724734, 0.0000723489, 0.0000722246, 0.0000721005, 0.0000719766, 0.0000718530, 0.0000717295, 0.0000716063, 0.0000714833, 0.0000713605, 0.0000712379, 0.0000711155, 0.0000709933, 0.0000708714, 0.0000707496, 0.0000706281, 0.0000705067, 0.0000703856, 0.0000702647, 0.0000701440, 0.0000700235, 0.0000699032, 0.0000697831, 0.0000696632, 0.0000695435, 0.0000694241, 0.0000693048, 0.0000691857, 0.0000690669, 0.0000689482, 0.0000688298, 0.0000687115, 0.0000685935, 0.0000684756, 0.0000683580, 0.0000682406, 0.0000681233, 0.0000680063, 0.0000678895, 0.0000677728, 0.0000676564, 0.0000675402, 0.0000674241, 0.0000673083, 0.0000671927, 0.0000670772, 0.0000669620, 0.0000668470, 0.0000667321, 0.0000666175, 0.0000665030, 0.0000663888, 0.0000662747, 0.0000661609, 0.0000660472, 0.0000659338, 0.0000658205, 0.0000657074, 0.0000655945, 0.0000654818, 0.0000653693, 0.0000652570, 0.0000651449, 0.0000650330, 0.0000649213, 0.0000648098, 0.0000646984, 0.0000645873, 0.0000644763, 0.0000643656, 0.0000642550, 0.0000641446, 0.0000640344, 0.0000639244, 0.0000638146, 0.0000637049, 0.0000635955, 0.0000634862, 0.0000633772, 0.0000632683, 0.0000631596, 0.0000630511, 0.0000629428, 0.0000628346, 0.0000627267, 0.0000626189, 0.0000625114, 0.0000624040, 0.0000622968, 0.0000621897, 0.0000620829, 0.0000619762, 0.0000618698, 0.0000617635, 0.0000616574, 0.0000615514, 0.0000614457, 0.0000613401, 0.0000612348, 0.0000611296, 0.0000610246, 0.0000609197, 0.0000608151, 0.0000607106, 0.0000606063, 0.0000605022, 0.0000603982, 0.0000602945, 0.0000601909, 0.0000600875, 0.0000599842, 0.0000598812, 0.0000597783, 0.0000596756, 0.0000595731, 0.0000594708, 0.0000593686, 0.0000592666, 0.0000591648, 0.0000590631, 0.0000589617, 0.0000588604, 0.0000587593, 0.0000586583, 0.0000585575, 0.0000584569, 0.0000583565, 0.0000582563, 0.0000581562, 0.0000580563, 0.0000579565, 0.0000578570, 0.0000577576, 0.0000576583, 0.0000575593, 0.0000574604, 0.0000573617, 0.0000572632, 0.0000571648, 0.0000570666, 0.0000569685, 0.0000568707, 0.0000567730, 0.0000566754, 0.0000565781, 0.0000564809, 0.0000563838, 0.0000562870, 0.0000561903, 0.0000560937, 0.0000559974, 0.0000559012, 0.0000558051, 0.0000557093, 0.0000556136, 0.0000555180, 0.0000554226, 0.0000553274, 0.0000552324, 0.0000551375, 0.0000550428, 0.0000549482, 0.0000548538, 0.0000547596, 0.0000546655, 0.0000545716, 0.0000544778, 0.0000543842, 0.0000542908, 0.0000541975, 0.0000541044, 0.0000540115, 0.0000539187, 0.0000538261, 0.0000537336, 0.0000536413, 0.0000535491, 0.0000534571, 0.0000533653, 0.0000532736, 0.0000531821, 0.0000530907, 0.0000529995, 0.0000529085, 0.0000528176, 0.0000527268, 0.0000526363, 0.0000525458, 0.0000524556, 0.0000523654, 0.0000522755, 0.0000521857, 0.0000520960, 0.0000520065, 0.0000519172, 0.0000518280, 0.0000517390, 0.0000516501, 0.0000515613, 0.0000514728, 0.0000513843, 0.0000512961, 0.0000512079, 0.0000511200, 0.0000510321, 0.0000509445, 0.0000508569, 0.0000507696, 0.0000506824, 0.0000505953, 0.0000505084, 0.0000504216, 0.0000503350, 0.0000502485, 0.0000501622, 0.0000500760, 0.0000499900, 0.0000499041, 0.0000498184, 0.0000497328, 0.0000496473, 0.0000495620, 0.0000494769, 0.0000493919, 0.0000493070, 0.0000492223, 0.0000491378, 0.0000490534, 0.0000489691, 0.0000488850, 0.0000488010, 0.0000487171, 0.0000486334, 0.0000485499, 0.0000484665, 0.0000483832, 0.0000483001, 0.0000482171, 0.0000481343, 0.0000480516, 0.0000479691, 0.0000478866, 0.0000478044, 0.0000477223, 0.0000476403, 0.0000475584, 0.0000474767, 0.0000473952, 0.0000473137, 0.0000472325, 0.0000471513, 0.0000470703, 0.0000469894, 0.0000469087, 0.0000468281, 0.0000467477, 0.0000466674, 0.0000465872, 0.0000465072, 0.0000464273, 0.0000463475, 0.0000462679, 0.0000461884, 0.0000461090, 0.0000460298, 0.0000459508, 0.0000458718, 0.0000457930, 0.0000457143, 0.0000456358, 0.0000455574, 0.0000454791, 0.0000454010, 0.0000453230, 0.0000452451, 0.0000451674, 0.0000450898, 0.0000450124, 0.0000449350, 0.0000448578, 0.0000447808, 0.0000447038, 0.0000446270, 0.0000445504, 0.0000444738, 0.0000443974, 0.0000443212, 0.0000442450, 0.0000441690, 0.0000440931, 0.0000440174, 0.0000439418, 0.0000438663, 0.0000437909, 0.0000437157, 0.0000436406, 0.0000435656, 0.0000434908, 0.0000434160, 0.0000433415, 0.0000432670, 0.0000431927, 0.0000431185, 0.0000430444, 0.0000429704, 0.0000428966, 0.0000428229, 0.0000427494, 0.0000426759, 0.0000426026, 0.0000425294, 0.0000424564, 0.0000423834, 0.0000423106, 0.0000422379, 0.0000421654, 0.0000420929, 0.0000420206, 0.0000419484, 0.0000418763, 0.0000418044, 0.0000417326, 0.0000416609, 0.0000415893, 0.0000415179, 0.0000414465, 0.0000413753, 0.0000413043, 0.0000412333, 0.0000411625, 0.0000410917, 0.0000410212, 0.0000409507, 0.0000408803, 0.0000408101, 0.0000407400, 0.0000406700, 0.0000406001, 0.0000405304, 0.0000404608, 0.0000403912, 0.0000403219, 0.0000402526, 0.0000401834, 0.0000401144, 0.0000400455, 0.0000399767, 0.0000399080, 0.0000398394, 0.0000397710, 0.0000397027, 0.0000396345, 0.0000395664, 0.0000394984, 0.0000394306, 0.0000393628, 0.0000392952, 0.0000392277, 0.0000391603, 0.0000390930, 0.0000390259, 0.0000389588, 0.0000388919, 0.0000388251, 0.0000387584, 0.0000386918, 0.0000386253, 0.0000385590, 0.0000384927, 0.0000384266, 0.0000383606, 0.0000382947, 0.0000382289, 0.0000381632, 0.0000380976, 0.0000380322, 0.0000379669, 0.0000379016, 0.0000378365, 0.0000377715, 0.0000377066, 0.0000376418, 0.0000375772, 0.0000375126, 0.0000374482, 0.0000373838, 0.0000373196, 0.0000372555, 0.0000371915, 0.0000371276, 0.0000370638, 0.0000370002, 0.0000369366, 0.0000368731, 0.0000368098, 0.0000367466, 0.0000366834, 0.0000366204, 0.0000365575, 0.0000364947, 0.0000364320, 0.0000363694, 0.0000363069, 0.0000362445, 0.0000361823, 0.0000361201, 0.0000360581, 0.0000359961, 0.0000359343, 0.0000358726, 0.0000358109, 0.0000357494, 0.0000356880, 0.0000356267, 0.0000355655, 0.0000355044, 0.0000354434, 0.0000353825, 0.0000353217, 0.0000352610, 0.0000352004, 0.0000351400, 0.0000350796, 0.0000350193, 0.0000349592, 0.0000348991, 0.0000348392, 0.0000347793, 0.0000347196, 0.0000346599, 0.0000346004, 0.0000345409, 0.0000344816, 0.0000344224, 0.0000343632, 0.0000343042, 0.0000342452, 0.0000341864, 0.0000341277, 0.0000340691, 0.0000340105, 0.0000339521, 0.0000338938, 0.0000338355, 0.0000337774, 0.0000337194, 0.0000336615, 0.0000336036, 0.0000335459, 0.0000334883, 0.0000334307, 0.0000333733, 0.0000333160, 0.0000332587, 0.0000332016, 0.0000331446, 0.0000330876, 0.0000330308, 0.0000329740, 0.0000329174, 0.0000328608, 0.0000328044, 0.0000327480, 0.0000326918, 0.0000326356, 0.0000325795, 0.0000325236, 0.0000324677, 0.0000324119, 0.0000323562, 0.0000323006, 0.0000322452, 0.0000321898, 0.0000321345, 0.0000320793, 0.0000320241, 0.0000319691, 0.0000319142, 0.0000318594, 0.0000318046, 0.0000317500, 0.0000316955, 0.0000316410, 0.0000315867, 0.0000315324, 0.0000314782, 0.0000314241, 0.0000313702, 0.0000313163, 0.0000312625, 0.0000312088, 0.0000311551, 0.0000311016, 0.0000310482, 0.0000309948, 0.0000309416, 0.0000308884, 0.0000308354, 0.0000307824, 0.0000307295, 0.0000306767, 0.0000306240, 0.0000305714, 0.0000305189, 0.0000304665, 0.0000304141, 0.0000303619, 0.0000303097, 0.0000302576, 0.0000302057, 0.0000301538, 0.0000301020, 0.0000300503, 0.0000299986, 0.0000299471, 0.0000298956, 0.0000298443, 0.0000297930, 0.0000297418, 0.0000296907, 0.0000296397, 0.0000295888, 0.0000295380, 0.0000294872, 0.0000294366, 0.0000293860, 0.0000293355, 0.0000292851, 0.0000292348, 0.0000291846, 0.0000291345, 0.0000290844, 0.0000290344, 0.0000289846, 0.0000289348, 0.0000288851, 0.0000288354, 0.0000287859, 0.0000287364, 0.0000286871, 0.0000286378, 0.0000285886, 0.0000285395, 0.0000284904, 0.0000284415, 0.0000283926, 0.0000283439, 0.0000282952, 0.0000282466, 0.0000281980, 0.0000281496, 0.0000281012, 0.0000280530, 0.0000280048, 0.0000279566, 0.0000279086, 0.0000278607, 0.0000278128, 0.0000277650, 0.0000277173, 0.0000276697, 0.0000276222, 0.0000275747, 0.0000275274, 0.0000274801, 0.0000274329, 0.0000273857, 0.0000273387, 0.0000272917, 0.0000272448, 0.0000271980, 0.0000271513, 0.0000271046, 0.0000270581, 0.0000270116, 0.0000269652, 0.0000269189, 0.0000268726, 0.0000268265, 0.0000267804, 0.0000267344, 0.0000266884, 0.0000266426, 0.0000265968, 0.0000265511, 0.0000265055, 0.0000264600, 0.0000264145, 0.0000263691, 0.0000263238, 0.0000262786, 0.0000262335, 0.0000261884, 0.0000261434, 0.0000260985, 0.0000260537, 0.0000260089, 0.0000259642, 0.0000259196, 0.0000258751, 0.0000258306, 0.0000257863, 0.0000257420, 0.0000256977, 0.0000256536, 0.0000256095, 0.0000255655, 0.0000255216, 0.0000254778, 0.0000254340, 0.0000253903, 0.0000253467, 0.0000253031, 0.0000252597, 0.0000252163, 0.0000251729, 0.0000251297, 0.0000250865, 0.0000250434, 0.0000250004, 0.0000249575, 0.0000249146, 0.0000248718, 0.0000248291, 0.0000247864, 0.0000247438, 0.0000247013, 0.0000246589, 0.0000246165, 0.0000245742, 0.0000245320, 0.0000244899, 0.0000244478, 0.0000244058, 0.0000243639, 0.0000243220, 0.0000242802, 0.0000242385, 0.0000241969, 0.0000241553, 0.0000241138, 0.0000240724, 0.0000240310, 0.0000239897, 0.0000239485, 0.0000239074, 0.0000238663, 0.0000238253, 0.0000237844, 0.0000237435, 0.0000237027, 0.0000236620, 0.0000236213, 0.0000235808, 0.0000235403, 0.0000234998, 0.0000234594, 0.0000234191, 0.0000233789, 0.0000233387, 0.0000232987, 0.0000232586, 0.0000232187, 0.0000231788, 0.0000231390, 0.0000230992, 0.0000230595, 0.0000230199, 0.0000229804, 0.0000229409, 0.0000229015, 0.0000228621, 0.0000228229, 0.0000227836, 0.0000227445, 0.0000227054, 0.0000226664, 0.0000226275, 0.0000225886, 0.0000225498, 0.0000225111, 0.0000224724, 0.0000224338, 0.0000223952, 0.0000223568, 0.0000223184, 0.0000222800, 0.0000222417, 0.0000222035, 0.0000221654, 0.0000221273, 0.0000220893, 0.0000220513, 0.0000220135, 0.0000219756, 0.0000219379, 0.0000219002, 0.0000218626, 0.0000218250, 0.0000217875, 0.0000217501, 0.0000217127, 0.0000216754, 0.0000216382, 0.0000216010, 0.0000215639, 0.0000215269, 0.0000214899, 0.0000214530, 0.0000214161, 0.0000213793, 0.0000213426, 0.0000213059, 0.0000212693, 0.0000212328, 0.0000211963, 0.0000211599, 0.0000211235, 0.0000210872, 0.0000210510, 0.0000210149, 0.0000209788, 0.0000209427, 0.0000209067, 0.0000208708, 0.0000208350, 0.0000207992, 0.0000207634, 0.0000207278, 0.0000206922, 0.0000206566, 0.0000206211, 0.0000205857, 0.0000205503, 0.0000205150, 0.0000204798, 0.0000204446, 0.0000204095, 0.0000203744, 0.0000203394, 0.0000203045, 0.0000202696, 0.0000202348, 0.0000202000, 0.0000201653, 0.0000201307, 0.0000200961, 0.0000200615, 0.0000200271, 0.0000199927, 0.0000199583, 0.0000199240, 0.0000198898, 0.0000198556, 0.0000198215, 0.0000197875, 0.0000197535, 0.0000197196, 0.0000196857, 0.0000196519, 0.0000196181, 0.0000195844, 0.0000195507, 0.0000195172, 0.0000194836, 0.0000194502, 0.0000194167, 0.0000193834, 0.0000193501, 0.0000193168, 0.0000192837, 0.0000192505, 0.0000192175, 0.0000191844, 0.0000191515, 0.0000191186, 0.0000190857, 0.0000190530, 0.0000190202, 0.0000189875, 0.0000189549, 0.0000189224, 0.0000188899, 0.0000188574, 0.0000188250, 0.0000187927, 0.0000187604, 0.0000187282, 0.0000186960, 0.0000186639, 0.0000186318, 0.0000185998, 0.0000185678, 0.0000185359, 0.0000185041, 0.0000184723, 0.0000184406, 0.0000184089, 0.0000183773, 0.0000183457, 0.0000183142, 0.0000182827, 0.0000182513, 0.0000182199, 0.0000181886, 0.0000181574, 0.0000181262, 0.0000180951, 0.0000180640, 0.0000180329, 0.0000180020, 0.0000179710, 0.0000179402, 0.0000179093, 0.0000178786, 0.0000178479, 0.0000178172, 0.0000177866, 0.0000177560, 0.0000177255, 0.0000176951, 0.0000176647, 0.0000176343, 0.0000176040, 0.0000175738, 0.0000175436, 0.0000175135, 0.0000174834, 0.0000174533, 0.0000174234, 0.0000173934, 0.0000173635, 0.0000173337, 0.0000173039, 0.0000172742, 0.0000172445, 0.0000172149, 0.0000171853, 0.0000171558, 0.0000171263, 0.0000170969, 0.0000170675, 0.0000170382, 0.0000170090, 0.0000169797, 0.0000169506, 0.0000169214, 0.0000168924, 0.0000168633, 0.0000168344, 0.0000168055, 0.0000167766, 0.0000167478, 0.0000167190, 0.0000166903, 0.0000166616, 0.0000166330, 0.0000166044, 0.0000165759, 0.0000165474, 0.0000165190, 0.0000164906, 0.0000164623, 0.0000164340, 0.0000164057, 0.0000163776, 0.0000163494, 0.0000163213, 0.0000162933, 0.0000162653, 0.0000162374, 0.0000162095, 0.0000161816, 0.0000161538, 0.0000161261, 0.0000160984, 0.0000160707, 0.0000160431, 0.0000160155, 0.0000159880, 0.0000159606, 0.0000159331, 0.0000159058, 0.0000158784, 0.0000158512, 0.0000158239, 0.0000157968, 0.0000157696, 0.0000157425, 0.0000157155, 0.0000156885, 0.0000156615, 0.0000156346, 0.0000156078, 0.0000155809, 0.0000155542, 0.0000155275, 0.0000155008, 0.0000154742, 0.0000154476, 0.0000154210, 0.0000153945, 0.0000153681, 0.0000153417, 0.0000153153, 0.0000152890, 0.0000152628, 0.0000152365, 0.0000152104, 0.0000151842, 0.0000151581, 0.0000151321, 0.0000151061, 0.0000150802, 0.0000150542, 0.0000150284, 0.0000150026, 0.0000149768, 0.0000149511, 0.0000149254, 0.0000148997, 0.0000148741, 0.0000148486, 0.0000148231, 0.0000147976, 0.0000147722, 0.0000147468, 0.0000147215, 0.0000146962, 0.0000146709, 0.0000146457, 0.0000146206, 0.0000145955, 0.0000145704, 0.0000145454, 0.0000145204, 0.0000144954, 0.0000144705, 0.0000144457, 0.0000144208, 0.0000143961, 0.0000143713, 0.0000143466, 0.0000143220, 0.0000142974, 0.0000142728, 0.0000142483, 0.0000142238, 0.0000141994, 0.0000141750, 0.0000141507, 0.0000141263, 0.0000141021, 0.0000140778, 0.0000140537, 0.0000140295, 0.0000140054, 0.0000139814, 0.0000139573, 0.0000139334, 0.0000139094, 0.0000138855, 0.0000138617, 0.0000138379, 0.0000138141, 0.0000137904, 0.0000137667, 0.0000137430, 0.0000137194, 0.0000136958, 0.0000136723, 0.0000136488, 0.0000136254, 0.0000136020, 0.0000135786, 0.0000135553, 0.0000135320, 0.0000135087, 0.0000134855, 0.0000134624, 0.0000134392, 0.0000134161, 0.0000133931, 0.0000133701, 0.0000133471, 0.0000133242, 0.0000133013, 0.0000132784, 0.0000132556, 0.0000132329, 0.0000132101, 0.0000131874, 0.0000131648, 0.0000131422, 0.0000131196, 0.0000130970, 0.0000130745, 0.0000130521, 0.0000130297, 0.0000130073, 0.0000129849, 0.0000129626, 0.0000129404, 0.0000129181, 0.0000128959, 0.0000128738, 0.0000128517, 0.0000128296, 0.0000128075, 0.0000127855, 0.0000127636, 0.0000127416, 0.0000127198, 0.0000126979, 0.0000126761, 0.0000126543, 0.0000126326, 0.0000126109, 0.0000125892, 0.0000125676, 0.0000125460, 0.0000125244, 0.0000125029, 0.0000124814, 0.0000124600, 0.0000124386, 0.0000124172, 0.0000123959, 0.0000123746, 0.0000123533, 0.0000123321, 0.0000123109, 0.0000122898, 0.0000122687, 0.0000122476, 0.0000122265, 0.0000122055, 0.0000121846, 0.0000121636, 0.0000121427, 0.0000121219, 0.0000121011, 0.0000120803, 0.0000120595, 0.0000120388, 0.0000120181, 0.0000119975, 0.0000119769, 0.0000119563, 0.0000119357, 0.0000119152, 0.0000118948, 0.0000118743, 0.0000118539, 0.0000118336, 0.0000118132, 0.0000117929, 0.0000117727, 0.0000117525, 0.0000117323, 0.0000117121, 0.0000116920, 0.0000116719, 0.0000116519, 0.0000116318, 0.0000116119, 0.0000115919, 0.0000115720, 0.0000115521, 0.0000115323, 0.0000115125, 0.0000114927, 0.0000114729, 0.0000114532, 0.0000114335, 0.0000114139, 0.0000113943, 0.0000113747, 0.0000113552, 0.0000113357, 0.0000113162, 0.0000112968, 0.0000112773, 0.0000112580, 0.0000112386, 0.0000112193, 0.0000112001, 0.0000111808, 0.0000111616, 0.0000111424, 0.0000111233, 0.0000111042, 0.0000110851, 0.0000110661, 0.0000110470, 0.0000110281, 0.0000110091, 0.0000109902, 0.0000109713, 0.0000109525, 0.0000109337, 0.0000109149, 0.0000108961, 0.0000108774, 0.0000108587, 0.0000108401, 0.0000108214, 0.0000108029, 0.0000107843, 0.0000107658, 0.0000107473, 0.0000107288, 0.0000107104, 0.0000106920, 0.0000106736, 0.0000106553, 0.0000106370, 0.0000106187, 0.0000106005, 0.0000105822, 0.0000105641, 0.0000105459, 0.0000105278, 0.0000105097, 0.0000104917, 0.0000104736, 0.0000104556, 0.0000104377, 0.0000104197, 0.0000104018, 0.0000103840, 0.0000103661, 0.0000103483, 0.0000103305, 0.0000103128, 0.0000102951, 0.0000102774, 0.0000102597, 0.0000102421, 0.0000102245, 0.0000102070, 0.0000101894, 0.0000101719, 0.0000101544, 0.0000101370, 0.0000101196, 0.0000101022, 0.0000100848, 0.0000100675, 0.0000100502, 0.0000100330, 0.0000100157, 0.0000099985, 0.0000099813, 0.0000099642, 0.0000099471, 0.0000099300, 0.0000099129, 0.0000098959, 0.0000098789, 0.0000098619, 0.0000098450, 0.0000098281, 0.0000098112, 0.0000097943, 0.0000097775, 0.0000097607, 0.0000097439, 0.0000097272, 0.0000097105, 0.0000096938, 0.0000096771, 0.0000096605, 0.0000096439, 0.0000096274, 0.0000096108, 0.0000095943, 0.0000095778, 0.0000095614, 0.0000095449, 0.0000095285, 0.0000095122, 0.0000094958, 0.0000094795, 0.0000094632, 0.0000094470, 0.0000094307, 0.0000094145, 0.0000093984, 0.0000093822, 0.0000093661, 0.0000093500, 0.0000093340, 0.0000093179, 0.0000093019, 0.0000092859, 0.0000092700, 0.0000092541, 0.0000092382, 0.0000092223, 0.0000092064, 0.0000091906, 0.0000091748, 0.0000091591, 0.0000091433, 0.0000091276, 0.0000091119, 0.0000090963, 0.0000090807, 0.0000090651, 0.0000090495, 0.0000090339, 0.0000090184, 0.0000090029, 0.0000089875, 0.0000089720, 0.0000089566, 0.0000089412, 0.0000089259, 0.0000089105, 0.0000088952, 0.0000088799, 0.0000088647, 0.0000088495, 0.0000088343, 0.0000088191, 0.0000088039, 0.0000087888, 0.0000087737, 0.0000087586, 0.0000087436, 0.0000087286, 0.0000087136, 0.0000086986, 0.0000086837, 0.0000086687, 0.0000086538, 0.0000086390, 0.0000086241, 0.0000086093, 0.0000085945, 0.0000085798, 0.0000085650, 0.0000085503, 0.0000085356, 0.0000085210, 0.0000085063, 0.0000084917, 0.0000084771, 0.0000084626, 0.0000084480, 0.0000084335, 0.0000084190, 0.0000084046, 0.0000083901, 0.0000083757, 0.0000083613, 0.0000083469, 0.0000083326, 0.0000083183, 0.0000083040, 0.0000082897, 0.0000082755, 0.0000082613, 0.0000082471, 0.0000082329, 0.0000082188, 0.0000082047, 0.0000081906, 0.0000081765, 0.0000081624, 0.0000081484, 0.0000081344, 0.0000081204, 0.0000081065, 0.0000080926, 0.0000080787, 0.0000080648, 0.0000080509, 0.0000080371, 0.0000080233, 0.0000080095, 0.0000079957, 0.0000079820, 0.0000079683, 0.0000079546, 0.0000079409, 0.0000079273, 0.0000079137, 0.0000079001, 0.0000078865, 0.0000078730, 0.0000078594, 0.0000078459, 0.0000078325, 0.0000078190, 0.0000078056, 0.0000077922, 0.0000077788, 0.0000077654, 0.0000077521, 0.0000077388, 0.0000077255, 0.0000077122, 0.0000076989, 0.0000076857, 0.0000076725, 0.0000076593, 0.0000076462, 0.0000076330, 0.0000076199, 0.0000076068, 0.0000075938, 0.0000075807, 0.0000075677, 0.0000075547, 0.0000075417, 0.0000075288, 0.0000075158, 0.0000075029, 0.0000074900, 0.0000074772, 0.0000074643, 0.0000074515, 0.0000074387, 0.0000074259, 0.0000074131, 0.0000074004, 0.0000073877, 0.0000073750, 0.0000073623, 0.0000073497, 0.0000073371, 0.0000073245, 0.0000073119, 0.0000072993, 0.0000072868, 0.0000072743, 0.0000072618, 0.0000072493, 0.0000072368, 0.0000072244, 0.0000072120, 0.0000071996, 0.0000071872, 0.0000071749, 0.0000071626, 0.0000071502, 0.0000071380, 0.0000071257, 0.0000071135, 0.0000071012, 0.0000070890, 0.0000070769, 0.0000070647, 0.0000070526, 0.0000070405, 0.0000070284, 0.0000070163, 0.0000070042, 0.0000069922, 0.0000069802, 0.0000069682, 0.0000069562, 0.0000069443, 0.0000069323, 0.0000069204, 0.0000069085, 0.0000068967, 0.0000068848, 0.0000068730, 0.0000068612, 0.0000068494, 0.0000068376, 0.0000068259, 0.0000068142, 0.0000068025, 0.0000067908, 0.0000067791, 0.0000067675, 0.0000067558, 0.0000067442, 0.0000067326, 0.0000067211, 0.0000067095, 0.0000066980, 0.0000066865, 0.0000066750, 0.0000066635, 0.0000066521, 0.0000066407, 0.0000066293, 0.0000066179, 0.0000066065, 0.0000065951, 0.0000065838, 0.0000065725, 0.0000065612, 0.0000065499, 0.0000065387, 0.0000065275, 0.0000065162, 0.0000065050, 0.0000064939, 0.0000064827, 0.0000064716, 0.0000064605, 0.0000064494, 0.0000064383, 0.0000064272, 0.0000064162, 0.0000064052, 0.0000063942, 0.0000063832, 0.0000063722, 0.0000063613, 0.0000063503, 0.0000063394, 0.0000063285, 0.0000063177, 0.0000063068, 0.0000062960, 0.0000062852, 0.0000062744, 0.0000062636, 0.0000062528, 0.0000062421, 0.0000062313, 0.0000062206, 0.0000062100, 0.0000061993, 0.0000061886, 0.0000061780, 0.0000061674, 0.0000061568, 0.0000061462, 0.0000061357, 0.0000061251, 0.0000061146, 0.0000061041, 0.0000060936, 0.0000060831, 0.0000060727, 0.0000060623, 0.0000060518, 0.0000060414, 0.0000060311, 0.0000060207, 0.0000060104, 0.0000060000, 0.0000059897, 0.0000059794, 0.0000059692, 0.0000059589, 0.0000059487, 0.0000059385, 0.0000059283, 0.0000059181, 0.0000059079, 0.0000058977, 0.0000058876, 0.0000058775, 0.0000058674, 0.0000058573, 0.0000058473, 0.0000058372, 0.0000058272, 0.0000058172, 0.0000058072, 0.0000057972, 0.0000057872, 0.0000057773, 0.0000057674, 0.0000057575, 0.0000057476, 0.0000057377, 0.0000057279, 0.0000057180, 0.0000057082, 0.0000056984, 0.0000056886, 0.0000056788, 0.0000056691, 0.0000056593, 0.0000056496, 0.0000056399, 0.0000056302, 0.0000056205, 0.0000056109, 0.0000056012, 0.0000055916, 0.0000055820, 0.0000055724, 0.0000055628, 0.0000055533, 0.0000055438, 0.0000055342, 0.0000055247, 0.0000055152, 0.0000055058, 0.0000054963, 0.0000054869, 0.0000054774, 0.0000054680, 0.0000054586, 0.0000054492, 0.0000054399, 0.0000054305, 0.0000054212, 0.0000054119, 0.0000054026, 0.0000053933, 0.0000053841, 0.0000053748, 0.0000053656, 0.0000053564, 0.0000053471, 0.0000053380, 0.0000053288, 0.0000053196, 0.0000053105, 0.0000053014, 0.0000052923, 0.0000052832, 0.0000052741, 0.0000052650, 0.0000052560, 0.0000052470, 0.0000052380, 0.0000052290, 0.0000052200, 0.0000052110, 0.0000052020, 0.0000051931, 0.0000051842, 0.0000051753, 0.0000051664, 0.0000051575, 0.0000051487, 0.0000051398, 0.0000051310, 0.0000051222, 0.0000051134, 0.0000051046, 0.0000050958, 0.0000050871, 0.0000050783, 0.0000050696, 0.0000050609, 0.0000050522, 0.0000050435, 0.0000050348, 0.0000050262, 0.0000050176, 0.0000050089, 0.0000050003, 0.0000049917, 0.0000049832, 0.0000049746, 0.0000049661, 0.0000049575, 0.0000049490, 0.0000049405, 0.0000049320, 0.0000049236, 0.0000049151, 0.0000049067, 0.0000048982, 0.0000048898, 0.0000048814, 0.0000048730, 0.0000048646, 0.0000048563, 0.0000048479, 0.0000048396, 0.0000048313, 0.0000048230, 0.0000048147, 0.0000048064, 0.0000047982, 0.0000047899, 0.0000047817, 0.0000047735, 0.0000047653, 0.0000047571, 0.0000047489, 0.0000047408, 0.0000047326, 0.0000047245, 0.0000047164, 0.0000047083, 0.0000047002, 0.0000046921, 0.0000046841, 0.0000046760, 0.0000046680, 0.0000046600, 0.0000046520, 0.0000046440, 0.0000046360, 0.0000046280, 0.0000046201, 0.0000046121, 0.0000046042, 0.0000045963, 0.0000045884, 0.0000045805, 0.0000045727, 0.0000045648, 0.0000045570, 0.0000045491, 0.0000045413, 0.0000045335, 0.0000045257, 0.0000045180, 0.0000045102, 0.0000045024, 0.0000044947, 0.0000044870, 0.0000044793, 0.0000044716, 0.0000044639, 0.0000044562, 0.0000044486, 0.0000044409, 0.0000044333, 0.0000044257, 0.0000044181, 0.0000044105, 0.0000044029, 0.0000043954, 0.0000043878, 0.0000043803, 0.0000043727, 0.0000043652, 0.0000043577, 0.0000043502, 0.0000043428, 0.0000043353, 0.0000043279, 0.0000043204, 0.0000043130, 0.0000043056, 0.0000042982, 0.0000042908, 0.0000042834, 0.0000042761, 0.0000042687, 0.0000042614, 0.0000042541, 0.0000042468, 0.0000042395, 0.0000042322, 0.0000042249, 0.0000042177, 0.0000042104, 0.0000042032, 0.0000041960, 0.0000041888, 0.0000041816, 0.0000041744, 0.0000041672, 0.0000041600, 0.0000041529, 0.0000041458, 0.0000041386, 0.0000041315, 0.0000041244, 0.0000041174, 0.0000041103, 0.0000041032, 0.0000040962, 0.0000040891, 0.0000040821, 0.0000040751, 0.0000040681, 0.0000040611, 0.0000040541, 0.0000040472, 0.0000040402, 0.0000040333, 0.0000040263, 0.0000040194, 0.0000040125, 0.0000040056, 0.0000039987, 0.0000039919, 0.0000039850, 0.0000039782, 0.0000039713, 0.0000039645, 0.0000039577, 0.0000039509, 0.0000039441, 0.0000039373, 0.0000039306, 0.0000039238, 0.0000039171, 0.0000039104, 0.0000039036, 0.0000038969, 0.0000038902, 0.0000038835, 0.0000038769, 0.0000038702, 0.0000038636, 0.0000038569, 0.0000038503, 0.0000038437, 0.0000038371, 0.0000038305, 0.0000038239, 0.0000038173, 0.0000038108, 0.0000038042, 0.0000037977, 0.0000037912, 0.0000037847, 0.0000037782, 0.0000037717, 0.0000037652, 0.0000037587, 0.0000037523, 0.0000037458, 0.0000037394, 0.0000037330, 0.0000037266, 0.0000037201, 0.0000037138, 0.0000037074, 0.0000037010, 0.0000036947, 0.0000036883, 0.0000036820, 0.0000036756, 0.0000036693, 0.0000036630, 0.0000036567, 0.0000036504, 0.0000036442, 0.0000036379, 0.0000036317, 0.0000036254, 0.0000036192, 0.0000036130, 0.0000036068, 0.0000036006, 0.0000035944, 0.0000035882, 0.0000035821, 0.0000035759, 0.0000035698, 0.0000035636, 0.0000035575, 0.0000035514, 0.0000035453, 0.0000035392, 0.0000035331, 0.0000035270, 0.0000035210, 0.0000035149, 0.0000035089, 0.0000035029, 0.0000034969, 0.0000034908, 0.0000034849, 0.0000034789, 0.0000034729, 0.0000034669, 0.0000034610, 0.0000034550, 0.0000034491, 0.0000034432, 0.0000034372, 0.0000034313, 0.0000034254, 0.0000034196, 0.0000034137, 0.0000034078, 0.0000034020, 0.0000033961, 0.0000033903, 0.0000033845, 0.0000033786, 0.0000033728, 0.0000033670, 0.0000033613, 0.0000033555, 0.0000033497, 0.0000033440, 0.0000033382, 0.0000033325, 0.0000033268, 0.0000033211, 0.0000033153, 0.0000033096, 0.0000033040, 0.0000032983, 0.0000032926, 0.0000032870, 0.0000032813, 0.0000032757, 0.0000032701, 0.0000032644, 0.0000032588, 0.0000032532, 0.0000032476, 0.0000032421, 0.0000032365, 0.0000032309, 0.0000032254, 0.0000032198, 0.0000032143, 0.0000032088, 0.0000032033, 0.0000031978, 0.0000031923, 0.0000031868, 0.0000031813, 0.0000031759, 0.0000031704, 0.0000031649, 0.0000031595, 0.0000031541, 0.0000031487, 0.0000031433, 0.0000031379, 0.0000031325, 0.0000031271, 0.0000031217, 0.0000031163, 0.0000031110, 0.0000031057, 0.0000031003, 0.0000030950, 0.0000030897, 0.0000030844, 0.0000030791, 0.0000030738, 0.0000030685, 0.0000030632, 0.0000030580, 0.0000030527, 0.0000030475, 0.0000030422, 0.0000030370, 0.0000030318, 0.0000030266, 0.0000030214, 0.0000030162, 0.0000030110, 0.0000030058, 0.0000030007, 0.0000029955, 0.0000029904, 0.0000029852, 0.0000029801, 0.0000029750, 0.0000029699, 0.0000029648, 0.0000029597, 0.0000029546, 0.0000029495, 0.0000029444, 0.0000029394, 0.0000029343, 0.0000029293, 0.0000029243, 0.0000029192, 0.0000029142, 0.0000029092, 0.0000029042, 0.0000028992, 0.0000028943, 0.0000028893, 0.0000028843, 0.0000028794, 0.0000028744, 0.0000028695, 0.0000028645, 0.0000028596, 0.0000028547, 0.0000028498, 0.0000028449, 0.0000028400, 0.0000028351, 0.0000028303, 0.0000028254, 0.0000028206, 0.0000028157, 0.0000028109, 0.0000028060, 0.0000028012, 0.0000027964, 0.0000027916, 0.0000027868, 0.0000027820, 0.0000027772, 0.0000027725, 0.0000027677, 0.0000027630, 0.0000027582, 0.0000027535, 0.0000027487, 0.0000027440, 0.0000027393, 0.0000027346, 0.0000027299, 0.0000027252, 0.0000027205, 0.0000027159, 0.0000027112, 0.0000027065, 0.0000027019, 0.0000026972, 0.0000026926, 0.0000026880, 0.0000026834, 0.0000026788, 0.0000026742, 0.0000026696, 0.0000026650, 0.0000026604, 0.0000026558, 0.0000026513, 0.0000026467, 0.0000026422, 0.0000026376, 0.0000026331, 0.0000026286, 0.0000026241, 0.0000026195, 0.0000026150, 0.0000026106, 0.0000026061, 0.0000026016, 0.0000025971, 0.0000025927, 0.0000025882, 0.0000025838, 0.0000025793, 0.0000025749, 0.0000025705, 0.0000025660, 0.0000025616, 0.0000025572, 0.0000025528, 0.0000025485, 0.0000025441, 0.0000025397, 0.0000025353, 0.0000025310, 0.0000025266, 0.0000025223, 0.0000025180, 0.0000025136, 0.0000025093, 0.0000025050, 0.0000025007, 0.0000024964, 0.0000024921, 0.0000024878, 0.0000024836, 0.0000024793, 0.0000024750, 0.0000024708, 0.0000024665, 0.0000024623, 0.0000024581, 0.0000024539, 0.0000024496, 0.0000024454, 0.0000024412, 0.0000024370, 0.0000024329, 0.0000024287, 0.0000024245, 0.0000024203, 0.0000024162, 0.0000024120, 0.0000024079, 0.0000024037, 0.0000023996, 0.0000023955, 0.0000023914, 0.0000023873, 0.0000023832, 0.0000023791, 0.0000023750, 0.0000023709, 0.0000023668, 0.0000023628, 0.0000023587, 0.0000023547, 0.0000023506, 0.0000023466, 0.0000023425, 0.0000023385, 0.0000023345, 0.0000023305, 0.0000023265, 0.0000023225, 0.0000023185, 0.0000023145, 0.0000023105, 0.0000023066, 0.0000023026, 0.0000022987, 0.0000022947, 0.0000022908, 0.0000022868, 0.0000022829, 0.0000022790, 0.0000022751, 0.0000022712, 0.0000022673, 0.0000022634, 0.0000022595, 0.0000022556, 0.0000022517, 0.0000022478, 0.0000022440, 0.0000022401, 0.0000022363, 0.0000022324, 0.0000022286, 0.0000022248, 0.0000022209, 0.0000022171, 0.0000022133, 0.0000022095, 0.0000022057, 0.0000022019, 0.0000021982, 0.0000021944, 0.0000021906, 0.0000021868, 0.0000021831, 0.0000021793, 0.0000021756, 0.0000021719, 0.0000021681, 0.0000021644, 0.0000021607, 0.0000021570, 0.0000021533, 0.0000021496, 0.0000021459, 0.0000021422, 0.0000021385, 0.0000021348, 0.0000021312, 0.0000021275, 0.0000021238, 0.0000021202, 0.0000021166, 0.0000021129, 0.0000021093, 0.0000021057, 0.0000021020, 0.0000020984, 0.0000020948, 0.0000020912, 0.0000020876, 0.0000020841, 0.0000020805, 0.0000020769, 0.0000020733, 0.0000020698, 0.0000020662, 0.0000020627, 0.0000020591, 0.0000020556, 0.0000020521, 0.0000020485, 0.0000020450, 0.0000020415, 0.0000020380, 0.0000020345, 0.0000020310, 0.0000020275, 0.0000020240, 0.0000020205, 0.0000020171, 0.0000020136, 0.0000020101, 0.0000020067, 0.0000020032, 0.0000019998, 0.0000019964, 0.0000019929, 0.0000019895, 0.0000019861, 0.0000019827, 0.0000019793, 0.0000019759, 0.0000019725, 0.0000019691, 0.0000019657, 0.0000019623, 0.0000019590, 0.0000019556, 0.0000019522, 0.0000019489, 0.0000019455, 0.0000019422, 0.0000019389, 0.0000019355, 0.0000019322, 0.0000019289, 0.0000019256, 0.0000019223, 0.0000019190, 0.0000019157, 0.0000019124, 0.0000019091, 0.0000019058, 0.0000019025, 0.0000018993, 0.0000018960, 0.0000018927, 0.0000018895, 0.0000018862, 0.0000018830, 0.0000018798, 0.0000018765, 0.0000018733, 0.0000018701, 0.0000018669, 0.0000018637, 0.0000018605, 0.0000018573, 0.0000018541, 0.0000018509, 0.0000018477, 0.0000018446, 0.0000018414, 0.0000018382, 0.0000018351, 0.0000018319, 0.0000018288, 0.0000018256, 0.0000018225, 0.0000018194, 0.0000018162, 0.0000018131, 0.0000018100, 0.0000018069, 0.0000018038, 0.0000018007, 0.0000017976, 0.0000017945, 0.0000017914, 0.0000017883, 0.0000017853, 0.0000017822, 0.0000017791, 0.0000017761, 0.0000017730, 0.0000017700, 0.0000017669, 0.0000017639, 0.0000017609, 0.0000017579, 0.0000017548, 0.0000017518, 0.0000017488, 0.0000017458, 0.0000017428, 0.0000017398, 0.0000017368, 0.0000017338, 0.0000017309, 0.0000017279, 0.0000017249, 0.0000017220, 0.0000017190, 0.0000017160, 0.0000017131, 0.0000017102, 0.0000017072, 0.0000017043, 0.0000017014, 0.0000016984, 0.0000016955, 0.0000016926, 0.0000016897, 0.0000016868, 0.0000016839, 0.0000016810, 0.0000016781, 0.0000016752, 0.0000016723, 0.0000016695, 0.0000016666, 0.0000016637, 0.0000016609, 0.0000016580, 0.0000016552, 0.0000016523, 0.0000016495, 0.0000016467, 0.0000016438, 0.0000016410, 0.0000016382, 0.0000016354, 0.0000016326, 0.0000016298, 0.0000016270, 0.0000016242, 0.0000016214, 0.0000016186, 0.0000016158, 0.0000016130, 0.0000016103, 0.0000016075, 0.0000016047, 0.0000016020, 0.0000015992, 0.0000015965, 0.0000015937, 0.0000015910, 0.0000015883, 0.0000015855, 0.0000015828, 0.0000015801, 0.0000015774, 0.0000015747, 0.0000015720, 0.0000015693, 0.0000015666, 0.0000015639, 0.0000015612, 0.0000015585, 0.0000015558, 0.0000015532, 0.0000015505, 0.0000015478, 0.0000015452, 0.0000015425, 0.0000015399, 0.0000015372, 0.0000015346, 0.0000015319, 0.0000015293, 0.0000015267, 0.0000015241, 0.0000015214, 0.0000015188, 0.0000015162, 0.0000015136, 0.0000015110, 0.0000015084, 0.0000015058, 0.0000015032, 0.0000015007, 0.0000014981, 0.0000014955, 0.0000014929, 0.0000014904, 0.0000014878, 0.0000014853, 0.0000014827, 0.0000014802, 0.0000014776, 0.0000014751, 0.0000014725, 0.0000014700, 0.0000014675, 0.0000014650, 0.0000014625, 0.0000014599, 0.0000014574, 0.0000014549, 0.0000014524, 0.0000014499, 0.0000014474, 0.0000014450, 0.0000014425, 0.0000014400, 0.0000014375, 0.0000014350, 0.0000014326, 0.0000014301, 0.0000014277, 0.0000014252, 0.0000014228, 0.0000014203, 0.0000014179, 0.0000014154, 0.0000014130, 0.0000014106, 0.0000014082, 0.0000014057, 0.0000014033, 0.0000014009, 0.0000013985, 0.0000013961, 0.0000013937, 0.0000013913, 0.0000013889, 0.0000013865, 0.0000013842, 0.0000013818, 0.0000013794, 0.0000013770, 0.0000013747, 0.0000013723, 0.0000013700, 0.0000013676, 0.0000013652, 0.0000013629, 0.0000013606, 0.0000013582, 0.0000013559, 0.0000013536, 0.0000013512, 0.0000013489, 0.0000013466, 0.0000013443, 0.0000013420, 0.0000013397, 0.0000013374, 0.0000013351, 0.0000013328, 0.0000013305, 0.0000013282, 0.0000013259, 0.0000013236, 0.0000013214, 0.0000013191, 0.0000013168, 0.0000013146, 0.0000013123, 0.0000013101, 0.0000013078, 0.0000013056, 0.0000013033, 0.0000013011, 0.0000012988, 0.0000012966, 0.0000012944, 0.0000012922, 0.0000012899, 0.0000012877, 0.0000012855, 0.0000012833, 0.0000012811, 0.0000012789, 0.0000012767, 0.0000012745, 0.0000012723, 0.0000012701, 0.0000012679, 0.0000012658, 0.0000012636, 0.0000012614, 0.0000012593, 0.0000012571, 0.0000012549, 0.0000012528, 0.0000012506, 0.0000012485, 0.0000012463, 0.0000012442, 0.0000012421, 0.0000012399, 0.0000012378, 0.0000012357, 0.0000012335, 0.0000012314, 0.0000012293, 0.0000012272, 0.0000012251, 0.0000012230, 0.0000012209, 0.0000012188, 0.0000012167, 0.0000012146, 0.0000012125, 0.0000012104, 0.0000012084, 0.0000012063, 0.0000012042, 0.0000012021, 0.0000012001, 0.0000011980, 0.0000011959, 0.0000011939, 0.0000011918, 0.0000011898, 0.0000011878, 0.0000011857, 0.0000011837, 0.0000011816, 0.0000011796, 0.0000011776, 0.0000011756, 0.0000011735, 0.0000011715, 0.0000011695, 0.0000011675, 0.0000011655, 0.0000011635, 0.0000011615, 0.0000011595, 0.0000011575, 0.0000011555, 0.0000011535, 0.0000011516, 0.0000011496, 0.0000011476, 0.0000011456, 0.0000011437, 0.0000011417, 0.0000011397, 0.0000011378, 0.0000011358, 0.0000011339, 0.0000011319, 0.0000011300, 0.0000011280, 0.0000011261, 0.0000011242, 0.0000011222, 0.0000011203, 0.0000011184, 0.0000011165, 0.0000011145, 0.0000011126, 0.0000011107, 0.0000011088, 0.0000011069, 0.0000011050, 0.0000011031, 0.0000011012, 0.0000010993, 0.0000010974, 0.0000010955, 0.0000010937, 0.0000010918, 0.0000010899, 0.0000010880, 0.0000010862, 0.0000010843, 0.0000010824, 0.0000010806, 0.0000010787, 0.0000010769, 0.0000010750, 0.0000010732, 0.0000010713, 0.0000010695, 0.0000010676, 0.0000010658, 0.0000010640, 0.0000010622, 0.0000010603, 0.0000010585, 0.0000010567, 0.0000010549, 0.0000010531, 0.0000010513, 0.0000010494, 0.0000010476, 0.0000010458, 0.0000010440, 0.0000010423, 0.0000010405, 0.0000010387, 0.0000010369, 0.0000010351, 0.0000010333, 0.0000010316, 0.0000010298, 0.0000010280, 0.0000010262, 0.0000010245, 0.0000010227, 0.0000010210, 0.0000010192, 0.0000010175, 0.0000010157, 0.0000010140, 0.0000010122, 0.0000010105, 0.0000010088, 0.0000010070, 0.0000010053, 0.0000010036, 0.0000010018, 0.0000010001, 0.0000009984, 0.0000009967, 0.0000009950, 0.0000009933, 0.0000009916, 0.0000009899, 0.0000009882, 0.0000009865, 0.0000009848, 0.0000009831, 0.0000009814, 0.0000009797, 0.0000009780, 0.0000009763, 0.0000009747, 0.0000009730, 0.0000009713, 0.0000009696, 0.0000009680, 0.0000009663, 0.0000009647, 0.0000009630, 0.0000009613, 0.0000009597, 0.0000009580, 0.0000009564, 0.0000009548, 0.0000009531, 0.0000009515, 0.0000009498, 0.0000009482, 0.0000009466, 0.0000009450, 0.0000009433, 0.0000009417, 0.0000009401, 0.0000009385, 0.0000009369, 0.0000009353, 0.0000009336, 0.0000009320, 0.0000009304, 0.0000009288, 0.0000009272, 0.0000009257, 0.0000009241, 0.0000009225, 0.0000009209, 0.0000009193, 0.0000009177, 0.0000009162, 0.0000009146, 0.0000009130, 0.0000009114, 0.0000009099, 0.0000009083, 0.0000009068, 0.0000009052, 0.0000009036, 0.0000009021, 0.0000009005, 0.0000008990, 0.0000008974, 0.0000008959, 0.0000008944, 0.0000008928, 0.0000008913, 0.0000008898, 0.0000008882, 0.0000008867, 0.0000008852, 0.0000008837, 0.0000008821, 0.0000008806, 0.0000008791, 0.0000008776, 0.0000008761, 0.0000008746, 0.0000008731, 0.0000008716, 0.0000008701, 0.0000008686, 0.0000008671, 0.0000008656, 0.0000008641, 0.0000008626, 0.0000008612, 0.0000008597, 0.0000008582, 0.0000008567, 0.0000008553, 0.0000008538, 0.0000008523, 0.0000008509, 0.0000008494, 0.0000008479, 0.0000008465, 0.0000008450, 0.0000008436, 0.0000008421, 0.0000008407, 0.0000008392, 0.0000008378, 0.0000008364, 0.0000008349, 0.0000008335, 0.0000008321, 0.0000008306, 0.0000008292, 0.0000008278, 0.0000008263, 0.0000008249, 0.0000008235, 0.0000008221, 0.0000008207, 0.0000008193, 0.0000008179, 0.0000008165, 0.0000008151, 0.0000008137, 0.0000008123, 0.0000008109, 0.0000008095, 0.0000008081, 0.0000008067, 0.0000008053, 0.0000008039, 0.0000008025, 0.0000008012, 0.0000007998, 0.0000007984, 0.0000007970, 0.0000007957, 0.0000007943, 0.0000007929, 0.0000007916, 0.0000007902, 0.0000007889, 0.0000007875, 0.0000007862, 0.0000007848, 0.0000007835, 0.0000007821, 0.0000007808, 0.0000007794, 0.0000007781, 0.0000007767, 0.0000007754, 0.0000007741, 0.0000007728, 0.0000007714, 0.0000007701, 0.0000007688, 0.0000007675, 0.0000007661, 0.0000007648, 0.0000007635, 0.0000007622, 0.0000007609, 0.0000007596, 0.0000007583, 0.0000007570, 0.0000007557, 0.0000007544, 0.0000007531, 0.0000007518, 0.0000007505, 0.0000007492, 0.0000007479, 0.0000007466, 0.0000007453, 0.0000007441, 0.0000007428, 0.0000007415, 0.0000007402, 0.0000007390, 0.0000007377, 0.0000007364, 0.0000007352, 0.0000007339, 0.0000007326, 0.0000007314, 0.0000007301, 0.0000007289, 0.0000007276, 0.0000007264, 0.0000007251, 0.0000007239, 0.0000007226, 0.0000007214, 0.0000007202, 0.0000007189, 0.0000007177, 0.0000007164, 0.0000007152, 0.0000007140, 0.0000007128, 0.0000007115, 0.0000007103, 0.0000007091, 0.0000007079, 0.0000007067, 0.0000007054, 0.0000007042, 0.0000007030, 0.0000007018, 0.0000007006, 0.0000006994, 0.0000006982, 0.0000006970, 0.0000006958, 0.0000006946, 0.0000006934, 0.0000006922, 0.0000006910, 0.0000006899, 0.0000006887, 0.0000006875, 0.0000006863, 0.0000006851, 0.0000006839, 0.0000006828, 0.0000006816, 0.0000006804, 0.0000006793, 0.0000006781, 0.0000006769, 0.0000006758, 0.0000006746, 0.0000006734, 0.0000006723, 0.0000006711, 0.0000006700, 0.0000006688, 0.0000006677, 0.0000006665, 0.0000006654, 0.0000006642, 0.0000006631, 0.0000006620, 0.0000006608, 0.0000006597, 0.0000006586, 0.0000006574, 0.0000006563, 0.0000006552, 0.0000006540, 0.0000006529, 0.0000006518, 0.0000006507, 0.0000006496, 0.0000006484, 0.0000006473, 0.0000006462, 0.0000006451, 0.0000006440, 0.0000006429, 0.0000006418, 0.0000006407, 0.0000006396, 0.0000006385, 0.0000006374, 0.0000006363, 0.0000006352, 0.0000006341, 0.0000006330, 0.0000006319, 0.0000006308, 0.0000006298, 0.0000006287, 0.0000006276, 0.0000006265, 0.0000006254, 0.0000006244, 0.0000006233, 0.0000006222, 0.0000006212, 0.0000006201, 0.0000006190, 0.0000006180, 0.0000006169, 0.0000006158, 0.0000006148, 0.0000006137, 0.0000006127, 0.0000006116, 0.0000006106, 0.0000006095, 0.0000006085, 0.0000006074, 0.0000006064, 0.0000006053, 0.0000006043, 0.0000006033, 0.0000006022, 0.0000006012, 0.0000006002, 0.0000005991, 0.0000005981, 0.0000005971, 0.0000005961, 0.0000005950, 0.0000005940, 0.0000005930, 0.0000005920, 0.0000005909, 0.0000005899, 0.0000005889, 0.0000005879, 0.0000005869, 0.0000005859, 0.0000005849, 0.0000005839, 0.0000005829, 0.0000005819, 0.0000005809, 0.0000005799, 0.0000005789, 0.0000005779, 0.0000005769, 0.0000005759, 0.0000005749, 0.0000005739, 0.0000005729, 0.0000005720, 0.0000005710, 0.0000005700, 0.0000005690, 0.0000005680, 0.0000005671, 0.0000005661, 0.0000005651, 0.0000005641, 0.0000005632, 0.0000005622, 0.0000005612, 0.0000005603, 0.0000005593, 0.0000005584, 0.0000005574, 0.0000005564, 0.0000005555, 0.0000005545, 0.0000005536, 0.0000005526, 0.0000005517, 0.0000005507, 0.0000005498, 0.0000005488, 0.0000005479, 0.0000005469, 0.0000005460, 0.0000005451, 0.0000005441, 0.0000005432, 0.0000005423, 0.0000005413, 0.0000005404, 0.0000005395, 0.0000005385, 0.0000005376, 0.0000005367, 0.0000005358, 0.0000005349, 0.0000005339, 0.0000005330, 0.0000005321, 0.0000005312, 0.0000005303, 0.0000005294, 0.0000005285, 0.0000005276, 0.0000005266, 0.0000005257, 0.0000005248, 0.0000005239, 0.0000005230, 0.0000005221, 0.0000005212, 0.0000005203, 0.0000005195, 0.0000005186, 0.0000005177, 0.0000005168, 0.0000005159, 0.0000005150, 0.0000005141, 0.0000005132, 0.0000005124, 0.0000005115, 0.0000005106, 0.0000005097, 0.0000005088, 0.0000005080, 0.0000005071, 0.0000005062, 0.0000005054, 0.0000005045, 0.0000005036, 0.0000005028, 0.0000005019, 0.0000005010, 0.0000005002, 0.0000004993, 0.0000004985, 0.0000004976, 0.0000004967, 0.0000004959, 0.0000004950, 0.0000004942, 0.0000004933, 0.0000004925, 0.0000004916, 0.0000004908, 0.0000004900, 0.0000004891, 0.0000004883, 0.0000004874, 0.0000004866, 0.0000004858, 0.0000004849, 0.0000004841, 0.0000004833, 0.0000004824, 0.0000004816, 0.0000004808, 0.0000004799, 0.0000004791, 0.0000004783, 0.0000004775, 0.0000004767, 0.0000004758, 0.0000004750, 0.0000004742, 0.0000004734, 0.0000004726, 0.0000004718, 0.0000004710, 0.0000004701, 0.0000004693, 0.0000004685, 0.0000004677, 0.0000004669, 0.0000004661, 0.0000004653, 0.0000004645, 0.0000004637, 0.0000004629, 0.0000004621, 0.0000004613, 0.0000004605, 0.0000004598, 0.0000004590, 0.0000004582, 0.0000004574, 0.0000004566, 0.0000004558, 0.0000004550, 0.0000004543, 0.0000004535, 0.0000004527, 0.0000004519, 0.0000004511, 0.0000004504, 0.0000004496, 0.0000004488, 0.0000004480, 0.0000004473, 0.0000004465, 0.0000004457, 0.0000004450, 0.0000004442, 0.0000004434, 0.0000004427, 0.0000004419, 0.0000004412, 0.0000004404, 0.0000004397, 0.0000004389, 0.0000004381, 0.0000004374, 0.0000004366, 0.0000004359, 0.0000004351, 0.0000004344, 0.0000004336, 0.0000004329, 0.0000004322, 0.0000004314, 0.0000004307, 0.0000004299, 0.0000004292, 0.0000004285, 0.0000004277, 0.0000004270, 0.0000004263, 0.0000004255, 0.0000004248, 0.0000004241, 0.0000004233, 0.0000004226, 0.0000004219, 0.0000004212, 0.0000004204, 0.0000004197, 0.0000004190, 0.0000004183, 0.0000004175, 0.0000004168, 0.0000004161, 0.0000004154, 0.0000004147, 0.0000004140, 0.0000004133, 0.0000004126, 0.0000004118, 0.0000004111, 0.0000004104, 0.0000004097, 0.0000004090, 0.0000004083, 0.0000004076, 0.0000004069, 0.0000004062, 0.0000004055, 0.0000004048, 0.0000004041, 0.0000004034, 0.0000004027, 0.0000004021, 0.0000004014, 0.0000004007, 0.0000004000, 0.0000003993, 0.0000003986, 0.0000003979, 0.0000003972, 0.0000003966, 0.0000003959, 0.0000003952, 0.0000003945, 0.0000003938, 0.0000003932, 0.0000003925, 0.0000003918, 0.0000003911, 0.0000003905, 0.0000003898, 0.0000003891, 0.0000003885, 0.0000003878, 0.0000003871, 0.0000003865, 0.0000003858, 0.0000003851, 0.0000003845, 0.0000003838, 0.0000003832, 0.0000003825, 0.0000003818, 0.0000003812, 0.0000003805, 0.0000003799, 0.0000003792, 0.0000003786, 0.0000003779, 0.0000003773, 0.0000003766, 0.0000003760, 0.0000003753, 0.0000003747, 0.0000003740, 0.0000003734, 0.0000003728, 0.0000003721, 0.0000003715, 0.0000003708, 0.0000003702, 0.0000003696, 0.0000003689, 0.0000003683, 0.0000003677, 0.0000003670, 0.0000003664, 0.0000003658, 0.0000003651, 0.0000003645, 0.0000003639, 0.0000003633, 0.0000003626, 0.0000003620, 0.0000003614, 0.0000003608, 0.0000003602, 0.0000003595, 0.0000003589, 0.0000003583, 0.0000003577, 0.0000003571, 0.0000003565, 0.0000003558, 0.0000003552, 0.0000003546, 0.0000003540, 0.0000003534, 0.0000003528, 0.0000003522, 0.0000003516, 0.0000003510, 0.0000003504, 0.0000003498, 0.0000003492, 0.0000003486, 0.0000003480, 0.0000003474, 0.0000003468, 0.0000003462, 0.0000003456, 0.0000003450, 0.0000003444, 0.0000003438, 0.0000003432, 0.0000003426, 0.0000003420, 0.0000003415, 0.0000003409, 0.0000003403, 0.0000003397, 0.0000003391, 0.0000003385, 0.0000003380, 0.0000003374, 0.0000003368, 0.0000003362, 0.0000003356, 0.0000003351, 0.0000003345, 0.0000003339, 0.0000003333, 0.0000003328, 0.0000003322, 0.0000003316, 0.0000003311, 0.0000003305, 0.0000003299, 0.0000003294, 0.0000003288, 0.0000003282, 0.0000003277, 0.0000003271, 0.0000003265, 0.0000003260, 0.0000003254, 0.0000003249, 0.0000003243, 0.0000003237, 0.0000003232, 0.0000003226, 0.0000003221, 0.0000003215, 0.0000003210, 0.0000003204, 0.0000003199, 0.0000003193, 0.0000003188, 0.0000003182, 0.0000003177, 0.0000003171, 0.0000003166, 0.0000003160, 0.0000003155, 0.0000003150, 0.0000003144, 0.0000003139, 0.0000003133, 0.0000003128, 0.0000003123, 0.0000003117, 0.0000003112, 0.0000003106, 0.0000003101, 0.0000003096, 0.0000003091};

  if (lumiAlgo.value_ == "occupancy") {
      memcpy(HFSBR,sbr_oc,sizeof(float)*interface::bril::MAX_NBX);
  } else if (lumiAlgo.value_ == "etsum") {
      memcpy(HFSBR,sbr_et,sizeof(float)*interface::bril::MAX_NBX);
  }
}

