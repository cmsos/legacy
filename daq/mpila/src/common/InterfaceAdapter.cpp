/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * InterfaceAdapter.cpp
 *
 *  Created on: Jan 25, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/InterfaceAdapter.hpp"

#include <memory>
#include <string>
#include <utility>

#include "mpila/TransportTest.hpp"
#include "mpila/TransportTestAll.hpp"
#include "mpila/TransportTestSome.hpp"

#include "mpila/globaldef.hpp"

namespace mpila
{
InterfaceAdapter::InterfaceAdapter(size_t pipelineDepth, TransportMode mode)
    : pipelineDepth_(pipelineDepth), transportMode_(mode)
{
    switch (mode)
    {
        case TransportMode::TEST:
            transport_ = std::make_unique<TransportTest>(pipelineDepth_);
            break;
        case TransportMode::TEST_SOME:
            transport_ = std::make_unique<TransportTestSome>(pipelineDepth_);
            break;
        case TransportMode::TEST_ALL:
            transport_ = std::make_unique<TransportTestAll>(pipelineDepth_);
            break;
        default:
            throw std::runtime_error("unknown transport type specified in Interface adapter");
    }
}

QueuePair *InterfaceAdapter::createQueuePair(int rank, size_t capacity, CompletionQueue *const completionQueue)
{
    queuePairs_.emplace_back(new QueuePair(rank, capacity, completionQueue, pipelineDepth_, transportMode_));
    transport_->registerQueuePair(queuePairs_.back().get());
    return queuePairs_.back().get();
}

void InterfaceAdapter::operator()()
{
    (*transport_)();
}
std::string InterfaceAdapter::getStats()
{
    return std::move(transport_->getStats());
}

void InterfaceAdapter::clearQPs()
{
    transport_->clearQPs();
}


} /* namespace mpila */
