// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/OutputStreamBuffer.h"

xdata::exdr::OutputStreamBuffer::OutputStreamBuffer(char* buf, unsigned int  n)
{
	xdrmem_create(&xdr_, buf, n, XDR_ENCODE);
	buffer_ = buf;
	size_ = n;
} 

xdata::exdr::OutputStreamBuffer::~OutputStreamBuffer()
{
	xdr_destroy(&xdr_);
}
