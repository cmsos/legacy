/*
 * XDAQMessageTest
 *
 * $Id: XDAQMessageTest.java,v 4.3 2007/06/18 13:54:02 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import junit.framework.*;
import junit.extensions.TestSetup;

import java.io.*;
import java.util.regex.*;
import java.lang.management.*;
import java.util.concurrent.*;

// DOM
import org.w3c.dom.*;

// SOAP
import javax.xml.soap.*;

/**
 * XDAQMessageTest
 */
public class XDAQMessageTest extends TestCase
{
	private static final long WAIT = 1000; // [ms]
	private static final String XDAQ_NS = "urn:xdaq-soap:3.0";

	public XDAQMessageTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(XDAQMessageTest.class);
	}

	public void testXDAQMessage() throws Exception
	{
		checkXDAQMessage("test/xdaq/soap0.xml", new XDAQMessage("Foo"));

		checkXDAQMessage("test/xdaq/soap1.xml", new XDAQMessage(
				TestUtil.fileToDOM("test/xdaq/test1.xml")));

		checkXDAQMessage("test/xdaq/soap2.xml", new XDAQMessage(
				"Foo", TestUtil.fileToDOM("test/xdaq/config.xml")));
	}

	public void testSetDOM() throws Exception
	{
		XDAQMessage message = new XDAQMessage("Foo");
		Document document = (Document)message.getDOM();

		Element command = (Element)document.getElementsByTagNameNS(
				XDAQ_NS, "Foo").item(0);

		command.setAttribute("foo", "bar");
		command.appendChild(document.createElementNS(XDAQ_NS, "xdaq:Bar"));

		message.setDOM(document);

		checkXDAQMessage("test/xdaq/soap3.xml", message);
	}

	public void testFromSOAP() throws Exception
	{
		MessageFactory mf = MessageFactory.newInstance();
		SOAPMessage original = mf.createMessage();
		SOAPEnvelope envelope = original.getSOAPPart().getEnvelope();
		SOAPBody body = envelope.getBody();
		SOAPElement element = body.addBodyElement(
				envelope.createName("Foo", "xdaq", "urn:xdaq-soap:3.0"));
		element.addTextNode("foo");

		original.getMimeHeaders().addHeader(
				"SOAPAction", "urn:xdaq-application:lid=0");

		XDAQMessage message = new XDAQMessage(original);

		checkXDAQMessage("test/xdaq/soap1.xml", message);
	}

	private void checkXDAQMessage(String good_soap, XDAQMessage message)
			throws Exception
	{
		assertTrue(message != null);

		String result = TestUtil.compareDOMs(
				TestUtil.fileToDOM(good_soap), message.getDOM());

		assertNull(result, result);
	}

	public void testSend() throws Exception
	{
		XDAQMessage message;
		Document document;
		String good_result;
		String result = "", result_code, result_string;
		String exception;

		// First, configure the executive
		message = new XDAQMessage(
				"Configure", TestUtil.fileToDOM("test/xdaq/config.xml"));

		document = message.send("http://localhost:40000", 0);
		assertTrue(document != null);

		good_result =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<xdaq:ConfigureResponse xmlns:xdaq=\"urn:xdaq-soap:3.0\"/>\n";
		result = TestUtil.domToString(document);

		assertEquals(good_result, result);

		// Then, send a command to an application in 4 ways:
		// endPoint+lid, endPoint+class+instance,
		// url(endPoint+lid) and url(endPoint+class+instance).
		message = new XDAQMessage(TestUtil.fileToDOM("test/xdaq/command.xml"));

		good_result =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<xdaq:CommandResponse xmlns:xdaq=\"urn:xdaq-soap:3.0\"/>\n";

		document = message.send("http://localhost:40000", 11);
		assertTrue(document != null);
		assertNull(message.getFaultString());

		result = TestUtil.domToString(document);
		assertEquals(good_result, result);

		document = message.send("http://localhost:40000", "TestXctl", 0);
		assertTrue(document != null);

		result = TestUtil.domToString(document);
		assertEquals(good_result, result);

		document = message.send("http://localhost:40000",
				"urn:xdaq-application:lid=11");
		assertTrue(document != null);

		result = TestUtil.domToString(document);
		assertEquals(good_result, result);

		document = message.send("http://localhost:40000",
				"urn:xdaq-application:class=TestXctl,instance=0");
		assertTrue(document != null);

		result = TestUtil.domToString(document);
		assertEquals(good_result, result);

		// Structured reply message
		message = new XDAQMessage("ParameterQuery");

		document = message.send("http://localhost:40000", 11);
		assertTrue(document != null);

		TestUtil.maskUUID(document); // (mask out UUID before comparison)
		result = TestUtil.compareDOMs(
				TestUtil.fileToDOM("test/xdaq/query.xml"), document);
		assertNull(result, result);

		// Check if it fails correctly
		message = new XDAQMessage("Configure");

		document = message.send("http://localhost:40000", 11);
		assertTrue(document != null);

		good_result =
				"Caught exception: pt::exception::Exception " +
				"'No callback method found for incoming request " +
				"[urn:xdaq-soap:3.0:Configure]' " +
				"raised at processIncomingMessage(SOAPDispatcher.cc:xxx)";

		assertEquals("Fault?: " + message.getFaultString(),
				good_result,
				message.getFaultString().
				replaceFirst("\\(/[./\\w]+/", "(").
				replaceFirst("\\.cc:\\d+\\)", ".cc:xxx)"));
				// leading path elements of the source code are removed.
				// line # is masked as 'xxx'.

		System.out.println("==== string dump of a Fault");
		System.out.println(TestUtil.domToString(document));
	}

	public void testTimeout() throws Exception
	{
		XDAQMessage message;
		Document document;
		String result, good_result;
		String exception;

		// send 'Command'
		message = new XDAQMessage(TestUtil.fileToDOM("test/xdaq/command.xml"));
		message.setTimeout(1000);
		assertEquals(1000, message.getTimeout());

		document = message.send("http://localhost:40000", 11);
		assertTrue(document != null);

		good_result =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<xdaq:CommandResponse xmlns:xdaq=\"urn:xdaq-soap:3.0\"/>\n";
		result = TestUtil.domToString(document);
		assertEquals(good_result, result);

		// 'Freeze' the application
		message = new XDAQMessage("Freeze");
		message.send("http://localhost:40000", 11);

		// Now, response time is > 5 sec.
		message = new XDAQMessage("Command");
		message.setTimeout(1000);

		document = null;
		exception = "";
		long before, after;
		before = System.currentTimeMillis();
		try {
			document = message.send("http://localhost:40000", 11);
		} catch (XDAQTimeoutException e) {
			e.printStackTrace();
			exception = e.toString();
		}
		after = System.currentTimeMillis();

		assertTrue("Exception string: " + exception, exception.startsWith(
				"net.hep.cms.xdaqctl.XDAQTimeoutException: "));
		assertEquals(10, (after - before) / 100); // 1000+-99[ms]
		assertNull(document);
	}

	public void testNoConnection() throws Exception
	{
		XDAQMessage message;
		Document document = null;
		String result, good_result;
		String exception;

		// Clean up the executive
		XDAQMessageTest.oneTimeTearDown();

		message = new XDAQMessage("Command");

		// No connection
		exception = "";
		try {
			document = message.send("http://localhost:40000", 11);
		} catch (Exception e) {
			StringWriter writer = new StringWriter();
			e.printStackTrace(new PrintWriter(writer));
			exception = writer.toString();
		}

		// Some SOAP implementations throw Exception while others return Fault.
		if (exception.length() > 0) {
			assertTrue("Exception string: " + exception,
				exception.startsWith(
				"net.hep.cms.xdaqctl.XDAQMessageException: Failed to send"));
		} else {
			NodeList list = document.getElementsByTagName("faultstring");
			assertNotNull("No faultstring", list.getLength() > 0);
			assertEquals("java.net.ConnectException: Connection refused",
					((Element)list.item(0)).getTextContent());
		}

		// again with timeout
		message.setTimeout(1000);

		exception = "";
		try {
			document = message.send("http://localhost:40000", 11);
		} catch (Exception e) {
			StringWriter writer = new StringWriter();
			e.printStackTrace(new PrintWriter(writer));
			exception = writer.toString();
		}

		// Some SOAP implementations throw Exception while others return Fault.
		if (exception.length() > 0) {
			assertTrue("Exception string: " + exception,
				exception.startsWith(
				"net.hep.cms.xdaqctl.XDAQMessageException: Sender task threw"));
		} else {
			NodeList list = document.getElementsByTagName("faultstring");
			assertNotNull("No faultstring", list.getLength() > 0);
			assertEquals("java.net.ConnectException: Connection refused",
					((Element)list.item(0)).getTextContent());
		}

		XDAQMessageTest.oneTimeSetUp();
		message = new XDAQMessage(
				"Configure", TestUtil.fileToDOM("test/xdaq/config.xml"));
		message.send("http://localhost:40000", 0);
	}

	public void testShutdown() throws Exception
	{
		ThreadMXBean threadMX = ManagementFactory.getThreadMXBean();

		// send 'Command' 20 times
		ExecutorService pool = Executors.newCachedThreadPool();

		class CommandTask implements Runnable {
			private final int id;
			CommandTask(int id) { this.id = id; }
			public void run() {
				try {
					XDAQMessage message = new XDAQMessage("Command");
					message.setTimeout(1000);
					message.send("http://localhost:40000", "TestXctl", 11);
				} catch (XDAQTimeoutException ignored) {
				} catch (Exception e) {
					System.err.println("==== Exeception in CommandTask: " + id);
					e.printStackTrace();
				}
			}
		}

		for (int i = 0; i < 20; ++i) {
			pool.execute(new CommandTask(i));
		}
		pool.shutdown();
		while (!pool.isTerminated()) {
			Thread.sleep(200);
		}

		// check #of threads
		System.out.println(
				"==== #thread (before shutdown): " + threadMX.getThreadCount());

		XDAQMessage.shutdownThreadPool();

		int n = threadMX.getThreadCount();
		System.out.println("==== #thread (after shutdown): " + n);
		assertTrue("Too many threads: " + n, n < 20);
	}

	private static void oneTimeSetUp() throws Exception
	{
		Process boot = Runtime.getRuntime().exec("./test/xdaq/bootxdaq start");
		boot.waitFor();
		String line = new BufferedReader(
				new InputStreamReader(boot.getInputStream())).readLine();
		assertNull("Failed to boot a XDAQ executive", line);

		Thread.sleep(WAIT);
	}

	private static void oneTimeTearDown() throws Exception
	{
		Runtime.getRuntime().exec("./test/xdaq/bootxdaq stop").waitFor();
		Thread.sleep(WAIT);
	}

	public static Test suite()
	{
		TestSuite suite = new TestSuite(XDAQMessageTest.class);

		TestSetup wrapper = new TestSetup(suite) {
			protected void setUp() throws Exception { oneTimeSetUp() ; }
			protected void tearDown() throws Exception { oneTimeTearDown(); }
		};

		return wrapper;
	}
}

// End of file
// vim: set ts=4 sw=4:
