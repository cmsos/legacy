#include "d2s/utils/Utils.hh"
#include "d2s/utils/Exception.hh"
#include "toolbox/string.h"
#include <list>
utils::fedEnableMask
utils::parseFedEnableMask(std::string const& fedEnableMask)
{
    // Instructions on how to interpret the fedEnableMask string can be
    // found in the RCMS documentation:
    // http://cmsdoc.cern.ch/cms/TRIDAS/RCMS/Docs/Manuals/manuals/level1FMFSM_1_10_0.pdf

    // We expect the FED enable mask string to be non-empty. Otherwise:
    // complain.
    if (fedEnableMask.empty())
    {
        std::string const msg =
            "Failed to parse the FED enable-mask. Expected a non-empty string.";
        XCEPT_RAISE(utils::exception::ConfigurationProblem, msg);
    }
    if (!toolbox::endsWith(fedEnableMask, "%"))
    {
        std::string const msg =
            "Failed to parse the FED enable-mask. Expected a '%' at the end.";
        XCEPT_RAISE(utils::exception::ConfigurationProblem, msg);
    }

    std::list<std::string> const pieces =
        toolbox::parseTokenList(fedEnableMask, "%");

    utils::fedEnableMask res;
    for (std::list<std::string>::const_iterator piece = pieces.begin();
            piece != pieces.end();
            ++piece)
    {
        std::list<std::string> const tmp = toolbox::parseTokenList(*piece, "&");

        // The above splitting on '%' and '&' should have left us with
        // two pieces. If not, raise a stink.
        if (tmp.size() != 2)
        {
            std::string const msg =
                "Failed to parse the FED enable-mask. Something appears to be wrong with its structure.";
            XCEPT_RAISE(utils::exception::ConfigurationProblem, msg);
        }

        uint16_t fedId;
        std::stringstream tmpFedId(toolbox::trim(tmp.front()));
        tmpFedId >> fedId;
        if (tmpFedId.fail() || !(tmpFedId >> std::ws).eof())
        {
            std::string const msg =
                toolbox::toString("Failed to parse the FED enable-mask. "
                        "Could not turn '%s' into a FED ID.",
                        tmpFedId.str().c_str());
            XCEPT_RAISE(utils::exception::ConfigurationProblem, msg);
        }

        uint16_t mask;
        std::stringstream tmpMask(toolbox::trim(tmp.back()));
        tmpMask >> mask;
        if (tmpMask.fail() || !(tmpMask >> std::ws).eof())
        {
            std::string const msg =
                toolbox::toString("Failed to parse the FED enable-mask. "
                        "Could not turn '%s' into a mask value.",
                        tmpMask.str().c_str());
            XCEPT_RAISE(utils::exception::ConfigurationProblem, msg);
        }

        res[fedId] = mask;
    }
    return res;
}
