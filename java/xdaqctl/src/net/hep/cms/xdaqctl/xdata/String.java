package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class String extends SimpleItem {

	private java.lang.String value;

	@Override
	public java.lang.String getValue() {
		return value;
	}

	@Override
	public void setValue(Object value) {
		this.value = (java.lang.String)value;
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		writeEXDRString(value, out);
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = readEXDRString(in);
	}

	@Override
	int getEXDRCode() {
		return XDataType.STRING.getEXDRCode();
	}

	static void writeEXDRString(java.lang.String value, DataOutputStream out)
			throws IOException {

		out.writeInt(value.length());
		out.writeBytes(value);

		int padding = 3 - (value.length() + 3) % 4;
		for (int i = 0; i < padding; ++i) {
			out.writeByte(0);
		}
	}
	
	static java.lang.String readEXDRString(DataInputStream in)
			throws IOException {

		int length = in.readInt();
		byte[] array = new byte[length];
		 
		for (int i = 0; i < length; ++i) {
			array[i] = in.readByte();
		}
		
		int padding = 3 - (length + 3) % 4;
		for (int i = 0; i < padding; ++i) {
			in.readByte();
		}
		
		return new java.lang.String(array);
	}
}

// End of file
// vim: set sw=4 ts=4:
