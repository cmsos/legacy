#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"
#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "xdata/InfoSpaceFactory.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/AutoReference.h"
#include "b2in/nub/Method.h"
#include "toolbox/net/UUID.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "bril/dipbridge/exception/Exception.h"
#include "bril/dipbridge/Application.h"
#include "bril/dipbridge/DipErrorHandlers.h"
#include "bril/dipbridge/dipDataExtract.h"
#include "bril/dipbridge/dipDataInsert.h"
#include "bril/dipbridge/exception/Exception.h"

XDAQ_INSTANTIATOR_IMPL (bril::dipbridge::Application)

namespace bril{
  namespace dipbridge{

    Application::Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL),m_busready(false),pm_dip(0),pm_puberrorhandler(0),pm_servererrorhandler(0){
      xgi::framework::deferredbind(this,this,&bril::dipbridge::Application::Default, "Default");
      b2in::nub::bind(this, &bril::dipbridge::Application::onMessage);
      toolbox::net::UUID uuid;
      m_processuuid = uuid.toString();
      m_startsubtimer_name = "brildipbridgeApplication_timer_"+m_processuuid;      
      toolbox::net::URN memurn("toolbox-mem-pool","brildipbridgeApplication_mem"+m_processuuid);
      toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
      pm_memPool  = toolbox::mem::getMemoryPoolFactory()->createPool(memurn,allocator);      
      m_bus.fromString("brildip");
      m_dipDataTopic.fromString("brildipdata");
      m_throttle_threshold_millisec = 900;
      getApplicationInfoSpace()->fireItemAvailable("bus" , &m_bus);
      getApplicationInfoSpace()->fireItemAvailable("dipSubTopics" , &m_dipSubTopics);
      getApplicationInfoSpace()->fireItemAvailable("dipDataTopic" , &m_dipDataTopic);
      getApplicationInfoSpace()->fireItemAvailable("throttleThresholdMillisec" , &m_throttle_threshold_millisec);
      getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
      pm_pubtodip_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_publishingToDip_"+m_processuuid,"waiting");
      pm_pubtodip_as = toolbox::task::bind(this,&bril::dipbridge::Application::publishingToDip,"publishingToDip");
    }
    
    Application::~Application(){
     
      std::map< std::string,DipSubscription* >::iterator subit = m_dipsubs.begin();
      for(; subit!=m_dipsubs.end(); ++subit){
	pm_dip->destroyDipSubscription(subit->second);
      }          
      std::map< dipDataInsert*, toolbox::squeue< xdata::Table::Reference >* >::iterator qit=m_dippubqueuestore.begin();
      for(;qit!=m_dippubqueuestore.end();++qit){
	delete qit->first;
	delete qit->second;
      }
      
      if( pm_servererrorhandler!=0 ){
	delete pm_servererrorhandler;
      }
      if( pm_puberrorhandler!=0 ){
	delete pm_puberrorhandler;
      }
      
    }

    void Application::Default(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){
      std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+getApplicationDescriptor()->getURN();
      *out << "<h2 align=\"center\"> Monitoring "<<appurl<<"</h2>";
      *out << "<p> Eventing Status</p>";
      *out << busesToHTML();
      *out << "<p> bus " << m_bus.toString()<<"</p>";
      *out << "<p> dipSubTopics " << m_dipSubTopics.toString()<<"</p>";
      *out << "<p> dipDataTopic "<< m_dipDataTopic.toString()<<"</p>";
    }
    
    void Application::actionPerformed(xdata::Event& e){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
      if( e.type() == "urn:xdaq-event:setDefaultValues" ){
	 std::stringstream ss;
	 ss<<"InTopics: "<<m_dipSubTopics.toString()<<'\n';
	 ss<<"DipDataTopic: "<<m_dipDataTopic.toString();
	 LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
	 ss.str(""); ss.clear(); 
	 pm_dip = Dip::create( (std::string("brilbridge_")+m_processuuid).c_str() );
	 pm_servererrorhandler=new bril::dipbridge::ServerErrorHandler(getApplicationLogger());
	 Dip::addDipDimErrorHandler(pm_servererrorhandler);
	 pm_puberrorhandler = new PubErrorHandler(getApplicationLogger());
	 toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	 toolbox::TimeInterval vsec(5,0);//5 sec
	 toolbox::TimeVal startT = now + vsec;
	 toolbox::task::Timer* t = toolbox::task::TimerFactory::getInstance()->createTimer( m_startsubtimer_name );
	 t->schedule( this, startT, (void*)0, "start_dipsubscription" );
	 try{
	   this->getEventingBus(m_bus.value_).addActionListener(this);
	   this->getEventingBus(m_bus.value_).subscribe(m_dipDataTopic.value_);
	 }catch(eventing::api::exception::Exception& e){
	   std::string msg("Failed to subscribe to topic "+m_dipDataTopic.toString()+" on eventing bus "+m_bus.value_);
	   LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
	   XCEPT_RETHROW(exception::Exception,msg,e);
	 }
      }	
    }

    void Application::actionPerformed(toolbox::Event& e){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Received toolbox event " << e.type());
      if ( e.type() == "eventing::api::BusReadyToPublish" ){
        std::stringstream msg;
        msg<< "Eventing bus '" << m_bus.value_ << "' is ready to publish";
        LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
        m_busready = true;
      }
    }

    void Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception){
      toolbox::mem::AutoReference refguard(ref); //guarantee ref is released when refguard is out of scope
      std::string action = plist.getProperty("urn:b2in-eventing:action");
      if (action == "notify" && ref!=0 ){
	std::stringstream ss;
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
	if( topic==m_dipDataTopic.value_ ){
	  std::string dipname = plist.getProperty("urn:dipbridge:dipname");
	  if( !dipname.empty() ){	    	    
	    xdata::Table* tab = new xdata::Table;
	    xdata::Table::Reference tabref(tab);
	    char* buf = (char*)ref->getDataLocation();
	    size_t bufsize = ref->getDataSize();
	    xdata::exdr::FixedSizeInputStreamBuffer inBuffer(buf, bufsize);
	    try{
	      m_serializer.import(tab, &inBuffer);
	    }catch(xdata::exception::Exception& e){
	      ss<<"Failed to stream message "<<topic<<" into xdata::Table : "<<e.what();
	      LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	      XCEPT_DECLARE_NESTED(bril::dipbridge::exception::Exception,myerrorobj,ss.str(),e);
	      this->notifyQualified("fatal",myerrorobj);
	      return;
	    }

	    std::map< std::string,dipDataInsert* >::iterator pubit = m_dippubs.find(dipname);
	    dipDataInsert* dippub = 0;
	    if( pubit==m_dippubs.end() ){
	      dippub = new dipDataInsert(pm_dip,pm_puberrorhandler,dipname);
	      m_dippubs.insert( std::make_pair(dipname,dippub) );
	    }else{
	      dippub = pubit->second;
	    }
	    if(m_dippubqueuestore.find(dippub)==m_dippubqueuestore.end()){
	      toolbox::squeue< xdata::Table::Reference >* sq = new toolbox::squeue< xdata::Table::Reference >;
	      m_dippubqueuestore.insert( std::make_pair(dippub,sq) );
	    }
	    try{
	      m_dippubqueuestore[dippub]->push(tabref);
	    }catch(toolbox::exception::QueueFull& e){
	      ss<<"Failed to push "<<dipname<<"into queue";
	      LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
	      XCEPT_DECLARE_NESTED(bril::dipbridge::exception::Exception,myerrorobj,ss.str(),e);
	      this->notifyQualified("fatal",myerrorobj);
	    }
	  }//end if not dipname empty
	}//end if dipDataTopic
      }//end if action notify && ref!=0
    }//end scope of refguard
    
    void Application::handleMessage(DipSubscription* dipsub, DipData& message){
      std::stringstream ss;
      std::string subname(dipsub->getTopicName());
      LOG4CPLUS_DEBUG(getApplicationLogger(),"handleMessage from "+subname);
      if( message.isEmpty() ) {
        LOG4CPLUS_ERROR(getApplicationLogger(),"DIP topic is empty "+subname);
        return;
      }
      DipQuality dipq = message.extractDataQuality();
      if( dipq!=1 ){
	LOG4CPLUS_INFO(getApplicationLogger(),subname+" : No good quality data, skip");
	return;
      }
      const DipTimestamp dipt = message.extractDipTime();
      long long diptmillisec = dipt.getAsNanos()/1000000;
      std::map< std::string,long long >::iterator mit = m_millisecsincelast.find(subname);
      if(mit==m_millisecsincelast.end()){
	return;
      }
      long long deltasincelast = diptmillisec-mit->second;
      ss<<"DeltaT "<<subname<<" "<<deltasincelast<<" : "<<diptmillisec<<" vs "<<mit->second;
      LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
      ss.str("");ss.clear();
      if( mit->second!=0. && int(deltasincelast)<m_throttle_threshold_millisec.value_) {
	ss<<"Throttle "<<subname;
	LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
	return;
      }      
      mit->second = diptmillisec;
      dipDataExtract e(subname);
      xdata::Table::Reference dipmessage;
      try{
	dipmessage = e.getAll(message);
      }catch( bril::dipbridge::exception::dipExtractError& e ){
	ss<<"Failed extracting message "<<subname;
	LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
	XCEPT_DECLARE_NESTED(bril::dipbridge::exception::Exception,myerrorobj,ss.str(),e);
	this->notifyQualified("fatal",myerrorobj);
	return;
      }
      if(m_busready){
	publishDipMessageToEventing( subname, *dipmessage );     
      }      
    }
    
    void Application::publishDipMessageToEventing( const std::string& dipname, xdata::Table& dipmessage){
      std::stringstream ss;      
      //export xdata::Table to buffer
      xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
      try{
	m_serializer.exportAll(&dipmessage,&outBuffer);
      }catch(xdata::exception::Exception& e){
	ss<<"Failed to stream xdata::Table for "<<dipname<<" to buffer : "<<e.what();
	LOG4CPLUS_ERROR(getApplicationLogger(),ss.str());
	XCEPT_DECLARE_NESTED(bril::dipbridge::exception::Exception,myerrorobj,ss.str(),e);
	this->notifyQualified("fatal",myerrorobj);
	return;
      }

      ss<<"Publishing "<<dipname<<" to eventing "<<m_bus.value_;
      LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
      ss.str("");ss.clear();

      char* databuf = outBuffer.getBuffer();
      size_t bufsize = outBuffer.tellp();      
      ss<<"serialized outBuffer size "<<bufsize;
      LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
      ss.str("");ss.clear();      

      //send xdata::Table dipmessage to eventing
      toolbox::mem::Reference* bufRef=0;
      xdata::Properties plist;
      try{
	bufRef = toolbox::mem::getMemoryPoolFactory()->getFrame(pm_memPool,bufsize);
	bufRef->setDataSize(bufsize);	
	memcpy( bufRef->getDataLocation(), databuf, bufsize );
	this->getEventingBus(m_bus.value_).publish(dipname,bufRef,plist);
      }catch(xcept::Exception& e){
	ss<<"Failed to publish "<<dipname<<" to "<<m_bus.toString();
	LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
	if(bufRef) bufRef->release(); 
	XCEPT_DECLARE_NESTED(bril::dipbridge::exception::Exception,myerrorobj,ss.str(),e);
        this->notifyQualified("fatal",myerrorobj);
      }
    }

    void Application::timeExpired( toolbox::task::TimerEvent& e  ){
      if( e.getTimerTask()->name=="start_dipsubscription" ){
	//start subscribe to dip
	size_t nsubs = m_dipSubTopics.elements();
	for(size_t i=0; i<nsubs; ++i){
	  xdata::String topicname = m_dipSubTopics.elementAt(i)->toString();
	  DipSubscription* s=pm_dip->createDipSubscription(topicname.value_.c_str(),this);
	  m_dipsubs.insert( std::make_pair( topicname.value_,s) );
	  m_millisecsincelast.insert( std::make_pair( topicname.value_,0.) );
	}
	//start publishing workloop
	pm_pubtodip_wl->activate();
	pm_pubtodip_wl->submit(pm_pubtodip_as);
	
      }
    }

    bool Application::publishingToDip( toolbox::task::WorkLoop* wl ){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Entering publishingToDip");
      std::stringstream ss;
      m_applock.take();
      if(m_busready){
	std::map< dipDataInsert*, toolbox::squeue< xdata::Table::Reference >* >::iterator it=m_dippubqueuestore.begin();
	for(; it!=m_dippubqueuestore.end(); ++it){
	  if(it->second->empty()) continue;
	  xdata::Table::Reference datatab = it->second->pop();
	  try{
	    it->first->sendtable(*datatab);
	  }catch( bril::dipbridge::exception::dipSendError& e){
	    ss<<"Failed publishingToDip "<<it->first->topicname();
	    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
	    XCEPT_DECLARE_NESTED(bril::dipbridge::exception::Exception,myerrorobj,ss.str(),e);
	    this->notifyQualified("fatal",myerrorobj);
	  }
	}
      }
      m_applock.give();
      usleep(800000);
      return true;
    }
    void Application::connected(DipSubscription* dipsub){
      //LOG4CPLUS_INFO(getApplicationLogger(),"connected to publication "+std::string(dipsub->getTopicName()) );
    }

    void Application::disconnected(DipSubscription* subscription,char *reason){
      //LOG4CPLUS_INFO(getApplicationLogger(),"disconnected from publication "+std::string(subscription->getTopicName()) );
    }
    
    void Application::handleException(DipSubscription* subscription, DipException& ex){
      LOG4CPLUS_INFO(getApplicationLogger(),"Publication "+std::string(subscription->getTopicName())+" exception "+std::string(ex.what()));
    }
    
  }}
