package xdaq.xdesk.jtreeeditor;

import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.tree.TreeNode;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import xdaq.xdesk.tools.IUserObject;

public class JTreeUserObject
    implements IUserObject { //for the interface look at xdaq.xdesk.tools package

//------------------Variables declaration---------------------------------------
  private String name;
  private String value;
  private String attrList;
  private short type;
  private ImageIcon icon;
  private Hashtable attributes;
  private JTreeUserObject parent_ = null;
  public Node domNode;
  public Document DomDocument;
  boolean compress = true; // display the tree compressed?

//========================Constructors=========================================
  /**Constructor*/
  public JTreeUserObject(Node childElement) {
    domNode = childElement;
    DomDocument = childElement.getOwnerDocument();
    attributes = new Hashtable();
    name = childElement.getNodeName();
    value = childElement.getNodeValue();
    type = childElement.getNodeType();
    attrList = getAttrList(childElement);
  }

//==============================================================================
  /**Returns a DOM element*/
  public Node getDomNode() {
    return domNode;
  }

//******************************************************************************
   /**Not implemented*/
   public int changeTextElement(String element, String value) {
     return -1;
   }

//******************************************************************************
   /**Not implemented*/
   public boolean isLeaf() {
     return true;
   }

//******************************************************************************
   /**Not implemented*/
   public String content() {
     return new String("");
   }

//******************************************************************************
   /**Returns a name of DOM element*/
   public String getNodeName() {
     // Here, extract maybe attributes like class name, instance
     // for displaying...
     return domNode.getNodeName();
   }

//******************************************************************************
   /**Not implemented*/
   public JTreeUserObject child(int searchIndex) {
     return null;
   }

//******************************************************************************
   /**Counts an index of DOM element*/
   public int index(JTreeUserObject child) {
     int count = childCount();
     for (int i = 0; i < count; i++) {
       JTreeUserObject n = this.child(i);
       if (child.domNode == n.domNode) {
         return i;
       }
     }
     return -1; // Should never get here.*/
   }

//******************************************************************************
   /**Returns an index of DOM element*/
   public int getIndex(TreeNode node) {
     return index( (JTreeUserObject) node);
   }

//******************************************************************************
   /**Returns a number of child nodes of the DOM element*/
   public int getChildCount() {
     return childCount();
   }

//******************************************************************************
   /**Not implemented*/
   public boolean treeElement(String elementName) {
     return true;
   }

//******************************************************************************

   public boolean getAllowsChildren() {
     return true;
   }

//******************************************************************************
   /**Counts child nodes of the DOM element*/
   public int childCount() {
     if (!compress) {
       // not compress -> return all nodes, included empty #text
       return domNode.getChildNodes().getLength();
     }
     int count = 0;
     for (int i = 0; i < domNode.getChildNodes().getLength(); i++) {
       org.w3c.dom.Node node = domNode.getChildNodes().item(i);

       if (node.getNodeType() == Node.ELEMENT_NODE
           && treeElement(node.getNodeName())) {
         // Note:
         //   Have to check for proper type.
         //   The DOCTYPE element also has the right name
         ++count;
       }
       // If I wanted to include text node, I'd have to
       // insert this here:
       // if (node.getNodeType() == TEXT_TYPE) { ++count; }
     }
     return count;
   }

//******************************************************************************
   /**Creates names and values of the DOM element attribute as a comma separated String*/
   private String getAttrList(Node n_node) {
     String temp_AttrList = "";
     attributes.clear();
     if (n_node.getNodeType() == Node.ELEMENT_NODE) {

       NamedNodeMap elementAttributes =
           n_node.getAttributes();
       if (elementAttributes != null && elementAttributes.getLength() > 0) {
         temp_AttrList = " (";
         int numAttributes = elementAttributes.getLength();
         for (int i = 0; i < numAttributes; i++) {
           Node attribute = elementAttributes.item(i);
           attributes.put(attribute.getNodeName(), attribute.getNodeValue());
           if (i > 0) {
             temp_AttrList = temp_AttrList + ", ";
           }
           temp_AttrList = temp_AttrList + attribute.getNodeName() +
               "=" + attribute.getNodeValue();
         }
         temp_AttrList = temp_AttrList + ")";
       }
     }
     return temp_AttrList;
   }

//******************************************************************************
   public String toString() {
     return name;
   }

//******************************************************************************
   /**Returns type of the DOM element*/
   public short getType() {
     return type;
   }

//******************************************************************************
   /**Returns name of the DOM element*/
   public String getName() {
     return name.toString();
   }

//******************************************************************************
   /**Returns value of the DOM element*/
   public String getValue() {
     return value;
   }

//******************************************************************************
   /**Returns list of attributes of the DOM element as a comma separated String*/
   public String getAttrList() {
     return attrList;
   }

//______________________________________________________________________________
  /**Returns icon for the DOM element*/
  public ImageIcon getIcon() {
    return icon;
  }

//******************************************************************************
   /**Sets icon for the DOM element*/
   public void setIcon(ImageIcon n_icon) {
     icon = n_icon;
   }

//******************************************************************************
   /**Sets name for the DOM element*/
   public void setName(String n_name) {
     name = n_name;
   }

//******************************************************************************
   /**Sets value for the DOM element*/
   public void setValue(String n_value) {
     value = n_value;
   }

//******************************************************************************
   /**Sets parent for the DOM element*/
   public void setParent(JTreeUserObject parent) {
     parent_ = parent;
   }

//******************************************************************************
   /**Returns parent of the DOM element*/
   public IUserObject getParent() {
     return parent_;
   }

//******************************************************************************
   /**Returns attribute value of the DOM element according to provided name*/
   public String getAttribute(String key) {
     String attr = "";
     if (attributes.containsKey(key)) {
       attr = (String) attributes.get(key);
     }
     return attr;
   }

//******************************************************************************
   /**Sets lis of attributes to the DOM element*/
   public void setAttrList(String n_AttrList) {
     fill_Attributes_Table(n_AttrList);
     attrList = n_AttrList;
   }

//******************************************************************************
   /**Sets values at attributes HashTable*/
   private void fill_Attributes_Table(String newAttributesString) {
     String[] mainArray;
     String[] tempArray;
     String list;
     attributes.clear();

     if (newAttributesString.length() == 0) {
       return;
     }
     list = newAttributesString.substring(1, newAttributesString.length() - 1);
     mainArray = list.split(",");
     for (int i = 0; i < mainArray.length; i++) {
       tempArray = mainArray[i].split("=");
       String s1 = "";
       String s2 = "";
       if (tempArray.length > 0) {
         s1 = tempArray[0].trim();
       }
       if (tempArray.length > 1) {
         s2 = tempArray[1].trim();
       }
       attributes.put(s1, s2);
     }
   }

//******************************************************************************

}
