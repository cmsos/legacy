// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>

#include "xcept/tools.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/Runtime.h"
#include "xdaq/exception/Exception.h"
#include "toolbox/string.h"
#include "toolbox/utils.h"

#include "xdaq/ApplicationContextImpl.h"

// LOG4CPLUS INCLUDES
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"

// Xerces includes
#include "xercesc/dom/DOM.hpp"


#include "xdata/xdata.h"
#include "xdata/soap/Serializer.h"
#include "xdata/XMLDOM.h"


#include "ApplicationContext.h"


using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Value;
using v8::Exception;
using v8::Array;

Persistent<Function> nodejs::ApplicationContext::constructor;
 xdaq::ApplicationContextImpl * nodejs::ApplicationContext::context_ = 0;
nodejs::ApplicationContext::ApplicationContext()
{

	//  Create the Runtime object
		//toolbox::getRuntime();

		// Disallow SIGPIPE signals
		// Such, we can handle droped socket connections.
		//
		//sigset_t block_pipe;
		//sigemptyset (&block_pipe);
		//sigaddset (&block_pipe, SIGPIPE);
		//sigprocmask(SIG_BLOCK, &block_pipe, NULL);

		//XMLPlatformUtils::Initialize();
		//XQillaPlatformUtils::initialize();

	    Logger rootLogger = Logger::getRoot();

	    if ( context_ == 0 )
	    {
	    		context_ = new xdaq::ApplicationContextImpl(rootLogger);
	    }

}

nodejs::ApplicationContext::~ApplicationContext() {
}

void nodejs::ApplicationContext::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "ApplicationContext"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
    NODE_SET_PROTOTYPE_METHOD(tpl, "init", init);
    NODE_SET_PROTOTYPE_METHOD(tpl, "createFacadeApplication", createFacadeApplication);
    NODE_SET_PROTOTYPE_METHOD(tpl, "operation", operation);


  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "ApplicationContext"), tpl->GetFunction());
}

void nodejs::ApplicationContext::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  if (args.IsConstructCall()) {
    // Invoked as constructor: `new ApplicationContext(...)`
    //double value = args[0]->IsUndefined() ? 0 : args[0]->NumberValue();
    ApplicationContext* obj = new ApplicationContext();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    // Invoked as plain function `ApplicationContext(...)`, turn into construct call.
    const int argc = 1;
    Local<Value> argv[argc] = { args[0] };
    Local<Context> context = isolate->GetCurrentContext();
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    Local<Object> result =  cons->NewInstance(context, argc, argv).ToLocalChecked();
    args.GetReturnValue().Set(result);
  }
}

void nodejs::ApplicationContext::NewInstance(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  const unsigned argc = 1;
  Local<Value> argv[argc] = { args[0] };
  Local<Function> cons = Local<Function>::New(isolate, constructor);
  Local<Context> context = isolate->GetCurrentContext();
  Local<Object> instance = cons->NewInstance(context, argc, argv).ToLocalChecked();

  args.GetReturnValue().Set(instance);
}

void nodejs::ApplicationContext::init(const FunctionCallbackInfo<Value>& args) {

	 Isolate* isolate = args.GetIsolate();
	 if(args.Length() < 1 || !args[0]->IsObject()) {
		    isolate->ThrowException(Exception::TypeError(
		    String::NewFromUtf8(isolate, "Error: One object expected")));
		    return;
	 }

	 Local<Context> context = isolate->GetCurrentContext();
	 Local<Object> obj = args[0]->ToObject(context).ToLocalChecked();
	 Local<Array> props = obj->GetOwnPropertyNames(context).ToLocalChecked();
	 int argc = props->Length();
	 char* argv[argc];
	 for(int i = 0, l = props->Length(); i < l; i++) {
		    Local<Value> localKey = props->Get(i);
		    Local<Value> localVal = obj->Get(context, localKey).ToLocalChecked();
		    std::string key = *String::Utf8Value(localKey);
		    std::string * val = new std::string(*String::Utf8Value(localVal));
		    std::cout << key << ":" << *val << std::endl;
		    argv[i] = (char*)val->c_str();
	 }


	 ApplicationContext* object = ObjectWrap::Unwrap<ApplicationContext>(args.Holder());

     std::cout << "argc is:" << argc << std::endl;

 	//  Create the Runtime object
 		toolbox::getRuntime();

 		// Disallow SIGPIPE signals
 		// Such, we can handle droped socket connections.
 		//
 		sigset_t block_pipe;
 		sigemptyset (&block_pipe);
 		sigaddset (&block_pipe, SIGPIPE);
 		sigprocmask(SIG_BLOCK, &block_pipe, NULL);

 		XMLPlatformUtils::Initialize();
 		//XQillaPlatformUtils::initialize();

	Logger rootLogger = Logger::getRoot();

	try
	{
		object->context_->init(argc, argv);
		std::cout << "init done" << std::endl;
	}
	catch(toolbox::exception::Exception& te)
	{
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(te));
		args.GetReturnValue().Set(Number::New(isolate, 1));
	}
	catch(xdaq::exception::Exception& xe)
	{
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(xe));
		args.GetReturnValue().Set(Number::New(isolate, 2));
	}
	catch(xcept::Exception& ee)
	{
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(ee));
		args.GetReturnValue().Set(Number::New(isolate, 3));
	}
	catch(std::exception& se)
	{
		LOG4CPLUS_FATAL(rootLogger,"unhandled std exception " << se.what());
		args.GetReturnValue().Set(Number::New(isolate, 4));
	}
	catch(...)
	{
		LOG4CPLUS_FATAL(rootLogger,"unhandled unknown exception");
		args.GetReturnValue().Set(Number::New(isolate, 5));
	}

	 args.GetReturnValue().Set(Number::New(isolate, 0));

}



void nodejs::ApplicationContext::createFacadeApplication(const FunctionCallbackInfo<Value>& args)
{
	 Isolate* isolate = args.GetIsolate();
	 if(args.Length() < 1 || !args[0]->IsObject()) {
		    isolate->ThrowException(Exception::TypeError(
		    String::NewFromUtf8(isolate, "Error: One object expected")));
		    return;
	 }
/*
class:RESTful
id:102
group:profile
service:express
*/

         Local<Context> context = isolate->GetCurrentContext();
         Local<Object> obj = args[0]->ToObject(context).ToLocalChecked();
         /*Local<Array> props = obj->GetOwnPropertyNames(context).ToLocalChecked();
         int argc = props->Length();
         char* argv[argc];
         for(int i = 0, l = props->Length(); i < l; i++) {
                    Local<Value> localKey = props->Get(i);
                    Local<Value> localVal = obj->Get(context, localKey).ToLocalChecked();
                    std::string key = *String::Utf8Value(localKey);
                    std::string * val = new std::string(*String::Utf8Value(localVal));
                    std::cout << key << ":" << *val << std::endl;
                    argv[i] = (char*)val->c_str();
         }
	*/

	Local<Value> argsClass = obj->Get(String::NewFromUtf8(isolate,"class"));
	Local<Value> argsGroup = obj->Get(String::NewFromUtf8(isolate,"group"));
	Local<Value> argsId = obj->Get(String::NewFromUtf8(isolate,"id"));
	Local<Value> argsService = obj->Get(String::NewFromUtf8(isolate,"service"));
	Local<Value> argsIcon = obj->Get(String::NewFromUtf8(isolate,"icon"));

	Logger rootLogger = Logger::getRoot();

	std::string className =  *String::Utf8Value(argsClass);
	std::string groupList =  *String::Utf8Value(argsGroup);
	std::string serviceList =  *String::Utf8Value(argsService);
	std::string icon =  *String::Utf8Value(argsIcon);
        unsigned long localTargetIdentifier = toolbox::toUnsignedLong(*String::Utf8Value(argsId));
        unsigned long instanceNumber = 0;
	std::cout << " got class and group " << className << ", " << groupList << "," << localTargetIdentifier << std::endl;
  	ApplicationContext* object = ObjectWrap::Unwrap<ApplicationContext>(args.Holder());
	
 	try
        {
                std::set < std::string > allowedZones = xdaq::ApplicationDescriptorFactory::getInstance()->getZoneNames();
                std::set < std::string > groups;
                toolbox::StringTokenizer strtok(groupList, ",");
                while (strtok.hasMoreTokens())
                {
                        std::string g = toolbox::trim(strtok.nextToken());
                        groups.insert(g);
                }
                xdaq::ApplicationDescriptor * d = xdaq::ApplicationDescriptorFactory::getInstance()->createApplicationDescriptor(object->context_->getContextDescriptor(), className, localTargetIdentifier, allowedZones, groups);
                ((xdaq::ApplicationDescriptorImpl*) d)->setInstance(instanceNumber);
                ((xdaq::ApplicationDescriptorImpl*) d)->setAttribute("service",serviceList);
                ((xdaq::ApplicationDescriptorImpl*) d)->setAttribute("icon",icon);

        }
        catch (xdaq::exception::DuplicateTid& ex)
        {
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(ex));
        }
        catch (xdaq::exception::Exception& ex)
        {
		LOG4CPLUS_FATAL(rootLogger,xcept::stdformat_exception_history(ex));
        }
        catch (const std::exception& ex)
        {
		LOG4CPLUS_FATAL(rootLogger, ex.what());
        }
}

void nodejs::ApplicationContext::operation(const FunctionCallbackInfo<Value>& args)
{
	Isolate* isolate = args.GetIsolate();
	if(args.Length() < 1 || !args[0]->IsObject()) {
		isolate->ThrowException(Exception::TypeError(
				String::NewFromUtf8(isolate, "Error: One object expected")));
		return;
	}


	Local<Context> context = isolate->GetCurrentContext();
	Local<Object> obj = args[0]->ToObject(context).ToLocalChecked();

	Local<Value> actionValue = obj->Get(String::NewFromUtf8(isolate,"action"));

	std::string actionString =  *String::Utf8Value(actionValue);

	ApplicationContext* object = ObjectWrap::Unwrap<ApplicationContext>(args.Holder());
	xdaq::ApplicationContext * c = dynamic_cast<xdaq::ApplicationContext*>(object->context_);


	if ( actionString == "parameters")
	{
		Local<Value> optionValue = obj->Get(String::NewFromUtf8(isolate,"options"));
		std::string optionString =  *String::Utf8Value(optionValue);

		size_t lid = toolbox::toLong(optionString);
		// get parameters according lid
		xdaq::Application * application = 0;
		try
		{
			std::cout << "1" << std::endl;
			xdaq::ApplicationRegistry * registry = object->context_->getApplicationRegistry();
			std::cout << "2" << std::endl;


			std::list<xdaq::Application*> applications = registry->getApplications();
			for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); i++)
			{
				const xdaq::ApplicationDescriptor* d = (*i)->getApplicationDescriptor();
				std::cout << "found " <<  d->getAttribute("class") << std::endl;
			}
			std::cout << "3" << std::endl;
			application = registry->getApplication(lid);

		}
		catch (xdaq::exception::ApplicationNotFound& e)
		{
			std::cout << xcept::stdformat_exception_history(e) << std::endl;
			return;
			//XCEPT_RETHROW(xdaq::exception::Exception, "Failed to query for XML, the lid (" + optionString +  ") provided is invalid" , e);

		}
		xdata::InfoSpace* infospace = application->getApplicationInfoSpace();

		DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(xdata::XStr("Core"));
		DOMDocument* exportDocument = impl->createDocument(xdata::XStr(""), xdata::XStr("root"), 0);
		DOMElement* node = exportDocument->createElementNS(xdata::XStr("any"), xdata::XStr("Properties"));
		node->setAttribute(xdata::XStr("xmlns"), xdata::XStr("any"));
		xdata::soap::Serializer serializer;

		try
		{
			serializer.exportAll(infospace, node, true);
		}
		catch (xdata::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Faield to export infospace to XML", e);
		}
		std::string xml = "";
		xdata::XMLDOMSerializer s2(xml);
		s2.serialize(node);
		//*out << xml << std::endl;

		Local<Object> result = Object::New(isolate);
		result->Set(String::NewFromUtf8(isolate, "data"), String::NewFromUtf8(isolate,xml.c_str()));
		args.GetReturnValue().Set(result);

	} else if ( actionString == "groups")
	{

		Local<Object> result = Object::New(isolate);
		Local<Array> elements = Array::New(isolate);
		int index = 0;
		std::set < std::string > zones = c->getZoneNames();
		for (std::set<std::string>::iterator j = zones.begin(); j != zones.end(); j++)
		{
			std::set<const xdaq::ApplicationGroup *> groups = c->getZone(*j)->getGroups();
			for (std::set<const xdaq::ApplicationGroup *>::iterator i = groups.begin(); i != groups.end(); i++)
			{

				std::string group = (*i)->getName();
				std::set<const xdaq::ApplicationDescriptor*> applications = (*i)->getApplicationDescriptors();

				Local<Object> item = Object::New(isolate);
				item->Set(String::NewFromUtf8(isolate,"zone"), String::NewFromUtf8(isolate,(*j).c_str()));
				item->Set(String::NewFromUtf8(isolate,"name"), String::NewFromUtf8(isolate,group.c_str()));
				item->Set(String::NewFromUtf8(isolate,"applications"), String::NewFromUtf8(isolate,toolbox::toString("%d", applications.size()).c_str()));
				elements->Set(index++, item);
			}
		}
		result->Set(String::NewFromUtf8(isolate, "data"), elements);
		args.GetReturnValue().Set(result);
	}
	else if ( actionString == "descriptors")
	{
		std::set<const xdaq::ApplicationDescriptor*> merged;

		std::set < std::string > zones = c->getZoneNames();
		for (std::set<std::string>::iterator i = zones.begin(); i != zones.end(); i++)
		{
			std::set<const xdaq::ApplicationGroup *> groups = c->getZone(*i)->getGroups();

			for (std::set<const xdaq::ApplicationGroup *>::iterator j = groups.begin(); j != groups.end(); j++)
			{
				std::set<const xdaq::ApplicationDescriptor*> descriptors = (*j)->getApplicationDescriptors();
				merged.insert(descriptors.begin(), descriptors.end());
			}
		}

		Local<Object> result = Object::New(isolate);
		Local<Array> elements = Array::New(isolate);


		int index = 0;
		for (std::set<const xdaq::ApplicationDescriptor*>::iterator i = merged.begin(); i != merged.end(); i++)
		{
			const xdaq::ApplicationDescriptor* d = *i;
			const xdaq::ContextDescriptor* currentContext = d->getContextDescriptor();
			std::string context = currentContext->getURL();

			std::string icon = d->getAttribute("icon");
			std::string urn = d->getURN();
			std::string className = d->getClassName();
			std::string service = d->getAttribute("service");
			std::string group = d->getAttribute("group");
			size_t lid = d->getLocalId() ;
			std::string instance = "-";
			if ( d->hasInstanceNumber())
			{
				instance = toolbox::toString("%d",d->getInstance()).c_str();
			}

			Local<Object> item = Object::New(isolate);
			item->Set(String::NewFromUtf8(isolate,"context"), String::NewFromUtf8(isolate,context.c_str()));
			item->Set(String::NewFromUtf8(isolate,"icon"), String::NewFromUtf8(isolate,icon.c_str()));
			item->Set(String::NewFromUtf8(isolate,"urn"), String::NewFromUtf8(isolate,urn.c_str()));
			item->Set(String::NewFromUtf8(isolate,"class"), String::NewFromUtf8(isolate,className.c_str()));
			item->Set(String::NewFromUtf8(isolate,"service"), String::NewFromUtf8(isolate,service.c_str()));
			item->Set(String::NewFromUtf8(isolate,"group"), String::NewFromUtf8(isolate,group.c_str()));
			item->Set(String::NewFromUtf8(isolate,"instance"), String::NewFromUtf8(isolate,instance.c_str()));
			item->Set(String::NewFromUtf8(isolate,"lid"), String::NewFromUtf8(isolate,toolbox::toString("%d",lid).c_str()));
			elements->Set(index++, item);
			//xgiheaders->Set(j++, String::NewFromUtf8(isolate, (*i).c_str()));
			// std::cout  << "->" << *i << std::endl;
		}

		result->Set(String::NewFromUtf8(isolate, "data"), elements);
		args.GetReturnValue().Set(result);

	}
	else if (actionString == "endpoints")
	{

		//
		Local<Object> result = Object::New(isolate);
		Local<Array> elements = Array::New(isolate);
		int index = 0;
		std::vector<const xdaq::Network*> networks = c->getNetGroup()->getNetworks();

		for (std::vector<const xdaq::Network*>::iterator j = networks.begin(); j != networks.end(); j++)
		{
			if ((*j)->isEndpointExisting(c->getContextDescriptor()))
			{
				const xdaq::Endpoint* e = (*j)->getEndpoint(c->getContextDescriptor());
				pt::Address::Reference address = e->getAddress();
				std::string protocol = address->getProtocol();
				std::string service = address->getService();


				Local<Object> item = Object::New(isolate);
				item->Set(String::NewFromUtf8(isolate,"url"), String::NewFromUtf8(isolate,address->toString().c_str()));
				item->Set(String::NewFromUtf8(isolate,"protocol"), String::NewFromUtf8(isolate,protocol.c_str()));
				item->Set(String::NewFromUtf8(isolate,"service"), String::NewFromUtf8(isolate,service.c_str()));
				item->Set(String::NewFromUtf8(isolate,"network"), String::NewFromUtf8(isolate,(*j)->getName().c_str()));
				if (e->isAlias())
				{
					item->Set(String::NewFromUtf8(isolate,"alias"), String::NewFromUtf8(isolate,"true"));
				}
				else
				{
					item->Set(String::NewFromUtf8(isolate,"alias"), String::NewFromUtf8(isolate,"false"));
				}
				if (e->isActive())
				{
					item->Set(String::NewFromUtf8(isolate,"active"), String::NewFromUtf8(isolate,"true"));
				}
				else
				{
					item->Set(String::NewFromUtf8(isolate,"active"), String::NewFromUtf8(isolate,"false"));
				}

				elements->Set(index++, item);
			}
		}
		result->Set(String::NewFromUtf8(isolate, "data"), elements);
		args.GetReturnValue().Set(result);
	}
	else if (actionString == "modules")
	{
		Local<Object> result = Object::New(isolate);
		Local<Array> elements = Array::New(isolate);
		int index = 0;

		result->Set(String::NewFromUtf8(isolate, "data"), elements);
		args.GetReturnValue().Set(result);
	}
	else if (actionString == "cpupolicies")
	{
		Local<Object> result = Object::New(isolate);
		Local<Array> elements = Array::New(isolate);
		int index = 0;

		result->Set(String::NewFromUtf8(isolate, "data"), elements);
		args.GetReturnValue().Set(result);
	}
	else if (actionString == "memorypolicies")
	{
		Local<Object> result = Object::New(isolate);
		Local<Array> elements = Array::New(isolate);
		int index = 0;

		result->Set(String::NewFromUtf8(isolate, "data"), elements);
		args.GetReturnValue().Set(result);
	}


}



    /*

	  	std::set<const xdaq::ApplicationDescriptor*> merged;

	  		std::set < std::string > zones = object->context_->getZoneNames();
	  		for (std::set<std::string>::iterator i = zones.begin(); i != zones.end(); i++)
	  		{
	  			std::set<const xdaq::ApplicationGroup *> groups = object->context_->getZone(*i)->getGroups();

	  			for (std::set<const xdaq::ApplicationGroup *>::iterator j = groups.begin(); j != groups.end(); j++)
	  			{
	  				std::set<const xdaq::ApplicationDescriptor*> descriptors = (*j)->getApplicationDescriptors();
	  				merged.insert(descriptors.begin(), descriptors.end());
	  			}
	  		}

	  		size_t index = 0;
	  		const xdaq::ContextDescriptor* localContext = this->getApplicationDescriptor()->getContextDescriptor();
	  		//

	  		//std::list<xdaq::Application*> applications = this->getApplicationContext()->getApplicationRegistry()->getApplications();
	  		//for (std::list<xdaq::Application*>::iterator i = applications.begin(); i != applications.end(); i++)
	  		for (std::set<const xdaq::ApplicationDescriptor*>::iterator i = merged.begin(); i != merged.end(); i++)
	  		{
	  			const xdaq::ApplicationDescriptor* d = *i;
	  			const xdaq::ContextDescriptor* currentContext = d->getContextDescriptor();
	  			//const xdaq::ApplicationDescriptor* d = (*i)->getApplicationDescriptor();
	  			std::string currentURL = currentContext->getURL();
	  			bool matches = false;
	  			try
	  			{
	  				matches = localContext->matchURL(currentURL);
	  			}
	  			catch (xdaq::exception::InvalidURL & e)
	  			{
	  				XCEPT_RETHROW (xdaq::exception::ConfigurationError, "Cannot match context descriptors", e);
	  			}

	  			if (! matches )
	  			{
	  				continue;
	  			}


	  			std::string icon = d->getAttribute("icon");
	  			if (icon == "") // use default icon
	  			{
	  				icon = layout_.defaultIcon_;
	  			}

	  			std::stringstream linkURL;
	  			linkURL << "<a href=\"/" << d->getURN() << "\">";

	  			std::string::size_type pos = d->getClassName().rfind("::Application");
	  			std::string title = d->getClassName().substr(0, pos);
	  			pos = 0;

	  			// Replace "::" namespace delimiters with spaces " "
	  			while ((pos = title.find("::", pos)) != std::string::npos)
	  			{
	  				title.replace(pos, 2, " ");
	  			}

	  			std::string service = d->getAttribute("service");
	  			if (service == "")
	  			{
	  				service = "none";
	  			}
	  			service = "Service: " + service;

	  			*out << "<div class=\"xdaq-hyperdaq-home-widget-container\" data-lid=\"" << d->getLocalId() << "\" data-id=\"" << index << "\" data-name=\"" << d->getClassName() << "\" data-service=\"" << d->getAttribute("service") << "\" data-network=\"" << d->getAttribute("network") << "\">" << std::endl;
	  			*out << "    			" << linkURL.str() << "<img src=\"" << icon << "\" class=\"xdaq-hyperdaq-home-widget-image\" alt=\"XDAQ Application Icon\" /></a>" << std::endl;
	  			*out << "    			<div class=\"xdaq-hyperdaq-home-widget-text\">" << linkURL.str() << "" << std::endl;

	  			*out << "    				<span style=\"font-weight: 600;\">" << title << "</span><br />" << std::endl;
	  			*out << "    				" << service << "<br />" << std::endl;
	  			*out << "    				Network: " << d->getAttribute("network") << "<br />" << std::endl;
	  			*out << "    				" << d->getURN() << "<br />" << std::endl;
	  			*out << "    				<br />" << std::endl;

	  			*out << "    			</a>" << std::endl;
	  			*out << "    			</div>" << std::endl;
	  			*out << "    		</div>";

	  			index++;
	  		}
*/

