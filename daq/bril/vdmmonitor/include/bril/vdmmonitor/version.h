// $Id$
#ifndef _bril_brilvdmmonitor_version_h_
#define _bril_brilvdmmonitor_version_h_
#include "config/PackageInfo.h"
#define BRILVDMMONITOR_VERSION_MAJOR 1
#define BRILVDMMONITOR_VERSION_MINOR 6
#define BRILVDMMONITOR_VERSION_PATCH 2
#define BRILVDMMONITOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRILVDMMONITOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILVDMMONITOR_VERSION_MAJOR,BRILVDMMONITOR_VERSION_MINOR,BRILVDMMONITOR_VERSION_PATCH)
#ifndef BRILVDMMONITOR_PREVIOUS_VERSIONS
#define BRILVDMMONITOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILVDMMONITOR_VERSION_MAJOR,BRILVDMMONITOR_VERSION_MINOR,BRILVDMMONITOR_VERSION_PATCH)
#else
#define BRILVDMMONITOR_FULL_VERSION_LIST  BRILVDMMONITOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILVDMMONITOR_VERSION_MAJOR,BRILVDMMONITOR_VERSION_MINOR,BRILVDMMONITOR_VERSION_PATCH)
#endif
namespace brilvdmmonitor{
  const std::string package = "brilvdmmonitor";
  const std::string versions = BRILVDMMONITOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ vdmmonitor";
  const std::string description = "Monitor vdm scans and fire monitoring data";
  const std::string authors = "Olena";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
