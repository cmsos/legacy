from setuptools import setup

PACKAGE     = 'CmTracComponent'
VERSION     = '0.0.6'
AUTHOR      = 'Roland Moser, Dainius Simelevicius'
AUTHOREMAIL = 'roland.moser@cern.ch, dainius.simelevicius@cern.ch'
LICENSE     = 'TBD'
URL         = 'https://svnweb.cern.ch/trac/macs'
SUMMARY     = 'Configuration Item Plugin'
DESCRIPTION = 'Trac Plugin for Managing Configuration Items'

setup(name=PACKAGE,
	version=VERSION,
	packages=['cmtraccomponent'],
	description=SUMMARY,
	long_description=DESCRIPTION,
	author=AUTHOR,
	author_email=AUTHOREMAIL,
	license=LICENSE,
	url=URL,
	entry_points={'trac.plugins': '%s = cmtraccomponent' % PACKAGE},
	package_data={'cmtraccomponent': ['templates/*.html']},
	zip_safe=True
)

