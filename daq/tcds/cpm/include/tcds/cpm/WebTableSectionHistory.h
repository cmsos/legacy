#ifndef _tcds_cpm_WebTableSectionHistory_h
#define _tcds_cpm_WebTableSectionHistory_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace cpm {

    class WebTableSectionHistory : public tcds::utils::WebObject
    {

    public:
      WebTableSectionHistory(std::string const& name,
                             std::string const& description,
                             tcds::utils::Monitor const& monitor,
                             std::string const& itemSetName,
                             std::string const& tabName,
                             size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_WebTableSectionHistory_h
