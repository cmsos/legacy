<?xml version='1.0'?>

<!--
This is basically a copy of the tcds_cpm_deadtimes flashlist. However, it
is updated at approximately 1 Hz, instead of once per lumi
section. This means that the former flashlist should be used for XMAS
storage, and this flashlist should be used for monitoring only. And
even then: beware of fluctuations.
-->

<xmas:flash
    xmlns:xmas="http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10"
    id="urn:xdaq-flashlist:tcds_cpm_deadtimes_1hz"
    version="1"
    key="fill_number,run_number,section_number">

  <!-- ++++++++++++++++++++++++++++++ -->
  <!-- Common. -->
  <!-- ++++++++++++++++++++++++++++++ -->

  <!-- The application context: the host name and the port number of
       the XDAQ executive. -->
  <xmas:item
      name="context"
      function="context()"
      type="string" />

  <!-- The local-id of the application in the executive. -->
  <xmas:item
      name="lid"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="$id"
      type="string" />

  <!-- The name of the service provided by the XDAQ application. -->
  <xmas:item
      name="service"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="$service"
      type="string" />

  <!-- ++++++++++++++++++++++++++++++ -->
  <!-- CPM-specific. -->
  <!-- ++++++++++++++++++++++++++++++ -->

  <!-- NOTE: The database column names cannot be longer than 30
       characters, hence the messing-about with the parameter
       names. -->

  <!-- Book keeping information. -->
  <xmas:item
      name="fill_number"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />
  <xmas:item
      name="run_number"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />
  <xmas:item
      name="section_number"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />
  <!-- NOTE: The num_nibbles means something slightly different here
       from what it means in the tcds-cpm-deadtimes flashlist. In this
       case it shows the number of nibbles in the sliding window used
       to get the current values. -->
  <xmas:item
      name="num_nibbles"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      type="unsigned int 32" />

  <!-- Plain, ungated deadtimes. -->
  <xmas:item
      name="deadtime_total"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_total"
      type="double" />
  <xmas:item
      name="deadtime_tts"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_tts"
      type="double" />
  <xmas:item
      name="deadtime_trg_rules"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_trigger_rules"
      type="double" />
  <xmas:item
      name="deadtime_bx_mask"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_bunch_mask_veto"
      type="double" />
  <xmas:item
      name="deadtime_retri"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_retri"
      type="double" />
  <xmas:item
      name="deadtime_apve"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_apve"
      type="double" />
  <xmas:item
      name="deadtime_daq_bp"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_daq_backpressure"
      type="double" />
  <xmas:item
      name="deadtime_calib"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_calibration"
      type="double" />
  <xmas:item
      name="deadtime_sw_pause"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_sw_pause"
      type="double" />
  <xmas:item
      name="deadtime_fw_pause"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.plain.deadtime_fw_pause"
      type="double" />

  <!-- Deadtimes gated with beam-presence. -->
  <xmas:item
      name="deadtime_beamactive_total"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_total"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_tts"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_tts"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_trg_rules"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_trigger_rules"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_bx_mask"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_bunch_mask_veto"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_retri"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_retri"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_apve"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_apve"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_daq_bp"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_daq_backpressure"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_calib"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_calibration"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_sw_pause"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_sw_pause"
      type="double" />
  <xmas:item
      name="deadtime_beamactive_fw_pause"
      infospace="urn:tcds-cpm-rates-and-deadtimes"
      source="brildaq.beamactive.deadtime_fw_pause"
      type="double" />

</xmas:flash>
