package xdaq.xdesk.jfinder;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class FinderMouseAdapter implements MouseListener
{
	JFinder finder_;
	
	protected DefaultFinderSelectionModel selectionModel_ = null; // acts as FinderSelectionModel
	
	public FinderMouseAdapter (JFinder finder)
	{
		finder_ = finder;
		selectionModel_ = finder_.getSelectionModel();		
	}
	
	
	// ---------------------------------------------------------------------
	// Mouse listener interfaces
	// ---------------------------------------------------------------------
	public void mouseClicked (MouseEvent e)
	{	
		// just notify that a selection has changed.
		JLabel cell = (JLabel) e.getSource();
		
		if (!selectionModel_.isNodeSelected(cell))
		{
			selectionModel_.setSelectionCell (cell);
		}
		
		if (e.getClickCount() > 1)
		{
			finder_.finderFolderOpen ();
		}
	}
	
	public void mouseEntered (MouseEvent e)
	{
		
	}
	
	public void mouseExited (MouseEvent e)
	{
		
	}
	
	public void mousePressed (MouseEvent e)
	{
		
	}
	
	public void mouseReleased (MouseEvent e)
	{
		
	}
	
	
	
}
