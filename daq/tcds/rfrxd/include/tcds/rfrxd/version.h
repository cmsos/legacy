#ifndef _tcdsrfrxd_version_h_
#define _tcdsrfrxd_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSRFRXD_VERSION_MAJOR 3
#define TCDSRFRXD_VERSION_MINOR 13
#define TCDSRFRXD_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSRFRXD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSRFRXD_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSRFRXD_VERSION_CODE PACKAGE_VERSION_CODE(TCDSRFRXD_VERSION_MAJOR,TCDSRFRXD_VERSION_MINOR,TCDSRFRXD_VERSION_PATCH)
#ifndef TCDSRFRXD_PREVIOUS_VERSIONS
#define TCDSRFRXD_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSRFRXD_VERSION_MAJOR,TCDSRFRXD_VERSION_MINOR,TCDSRFRXD_VERSION_PATCH)
#else
#define TCDSRFRXD_FULL_VERSION_LIST TCDSRFRXD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSRFRXD_VERSION_MAJOR,TCDSRFRXD_VERSION_MINOR,TCDSRFRXD_VERSION_PATCH)
#endif

namespace tcdsrfrxd
{
  const std::string package = "tcdsrfrxd";
  const std::string versions = TCDSRFRXD_FULL_VERSION_LIST;
  const std::string description = "CMS software for the RFRXD.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the RFRXD.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
