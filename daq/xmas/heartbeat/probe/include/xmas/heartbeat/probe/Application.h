// $Id$

/*************************************************************************
 * XDAQ XMAS Heartbeat Probe               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _xmas_heartbeat_probe_Application_h_
#define _xmas_heartbeat_probe_Application_h_

#include <string>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/ApplicationContext.h" 
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/Double.h"
#include "xdata/ActionListener.h"
#include "xdata/exdr/Serializer.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xmas/PulserSettings.h"
#include "xmas/exception/Exception.h"

#include "toolbox/task/AsynchronousEventDispatcher.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/mem/Pool.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/MessengerCache.h"

namespace xmas
{
	namespace heartbeat
	{
		namespace probe
		{
			class Application : public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener, public toolbox::task::TimerListener, public b2in::utils::MessengerCacheListener
			{

				public:

					XDAQ_INSTANTIATOR();

					Application (xdaq::ApplicationStub* s) ;
					~Application ();

					void actionPerformed (xdata::Event& e);

					void actionPerformed (toolbox::Event& event);
					void timeExpired (toolbox::task::TimerEvent& e);

					void Default (xgi::Input * in, xgi::Output * out) ;
					void browseSelection (xgi::Input * in, xgi::Output * out) ;

				protected:

					void StatisticsTabPage (xgi::Output * out);
					void TabPanel (xgi::Output * out);

				private:
					void heartbeat ();
					void asynchronousExceptionNotification (xcept::Exception& e);

					std::set<std::string> scanLocalServices ();

					//void displayTableToCSV (xdata::Table* table);

					//xdata::Vector<xdata::String> settingsURLs_; // path pattern for configuration file

					xdata::String publishGroup_;
					xdata::String heartbeatGroup_;

					//toolbox::task::AsynchronousEventDispatcher dispatcher_;

					// cgi
					//xdata::UnsignedInteger32 lossReportCounter_; // only print send errors every (msg % lossReportCounter) = 1

					xdata::String heartbeatWatchdog_;

					xdata::String heartbeatdURL_;

					b2in::utils::MessengerCache * heartbeatdMessengerCache_;
					xdata::UnsignedInteger64 outgoingHeartbeatCounter_;
					xdata::UnsignedInteger64 outgoingHeartbeatLostCounter_;

			};
		}
	}
}
#endif
