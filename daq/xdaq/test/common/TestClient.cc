#include "XIO.h"
#include "XRU.h"
#include "XRC.h"
#include "XFU.h"
#include "XFN.h"
#include "XEVM.h"
#include "vxtExceptionHandler.h"

// str = IP NUmber of server
extern "C" void Main(char * str)
{
  
  
  
  //
  // Open a UDP output stream
  //
 
  tcpAddr      addr;
  addr.port = 20200;
  strcpy(addr.host,str);
  vxtcphandle * handle = new vxtcphandle();
  vxtcpiostream * iostream = new vxtcpiostream(handle);
  handle->connect(&addr);

 
  cout << "Create XIO" << endl;
  try
  {
  	//
  	// Create virtual I/O channel
  	//
	XIO * xio = new XIO(iostream);
  
   	
  	XRU * xru = new XRU(xio);
    	XRC * xrc = new XRC(xio);

	XFU * xfu = new XFU(xio);
	XFN * xfn = new XFN(xio);
	XEVM *xevm = new XEVM(xio);
   	
	
	char tmpBuf[8];
	FragmentIdentifier fid;
  	EventIdentifier eid;
	EventProfile eprofile;
	DestinationCookie dcookie;
	FragmentSet fset;
	FragmentData fdata;
	FragmentData * fdatav[2];
	fdatav[0] = &fdata;
	fdatav[1] = &fdata;	
	FragmentDataSet fds;
	
	fid = 0xabcd;
  	eid = 0x1020;
	eprofile = 0x3040;
	dcookie.addr = 0x5060;
	dcookie.cookie = 0x7080;
	fset = 0x90a0;
        fdata.length = 0x4;
	fdata.data = tmpBuf;
	fdata.data[0] = 51;
	fdata.data[1] = 0x0;
	fds.num = 2;
	fds.fdata = fdatav;
 	xru->readout(&eid);
  	xru->send(&eid,&dcookie);
	xrc->config("config");
	xrc->start("start");
	xrc->stop("stop");
	xrc->shutdown("shutdown");
	xrc->action("action");
	xrc->suspend("suspend");
	xrc->resume("resume");
	xrc->load("load");
	
	xfu->confirm(&eid);
	xfu->cache(&fid,&eid,&dcookie,&fdata);
	xfu->allocate(&eprofile,&dcookie);
	xfu->collect(&eid, &fset, &dcookie);
	xfn->take(&eid,&fds);
	xfu->discard(&eid);

	xevm->allocate(&eprofile,&dcookie);
	xevm->allocateRetrieve(&fset,&eprofile,&dcookie);
	xevm->retrieve(&eid,&fset,&dcookie);
	xevm->clear(&eid);
	
	// Comparison print
	cout << "readout:" << hex << eid << endl;
	cout << "send:" << hex << eid << dcookie.addr << dcookie.cookie << endl;
	cout << "config:" << "config" << endl;  
	cout << "start:" << "start" << endl;  
 	cout << "stop:" << "stop" << endl; 
   	cout << "shutdown:" << "shutdown" << endl;
   	cout << "action:" << "action" << endl;
  	cout << "suspend:" << "suspend" << endl;
   	cout << "resume:" << "resume" << endl;
   	cout << "load:" << "load" << endl;
   	cout << "confirm:"  << hex << eid << endl;
   	cout << "cache:" <<  hex << fid << eid << dcookie.addr << dcookie.cookie <<  fdata.length <<
	(int)fdata.data[0] << endl;
	cout << "allocate:" <<  hex << eprofile << dcookie.addr << dcookie.cookie << endl;
	cout << "collect:" <<  hex << eid << fset << dcookie.addr << dcookie.cookie << endl;
	cout << "take:" <<  hex << eid << fds.num << fds.fdata[0]->length <<
	(int)fds.fdata[0]->data[0] << fds.fdata[1]->length << (int)fds.fdata[1]->data[0] << endl;
   	cout << "discard:"  << hex << eid << endl;
	
	
	
	
    	cout << "allocate:" <<  hex << eprofile << dcookie.addr << dcookie.cookie << endl;
    	cout << "allocateRetrieve:" << fset << eprofile << dcookie.addr << dcookie.cookie << endl;
    	cout << "retrieve:" <<  hex << eid << fset << dcookie.addr << dcookie.cookie << endl;
    	cout << "clear:" <<  hex << eid << endl;
   
  	delete xio;
  	delete xru;
  	delete xrc;
	delete xevm;
	delete xfu;
	delete handle;
	delete iostream;	
  }
  catch(vxException e)
  {
  	cout << e;
  };
  
}
//
// The main program
//
#ifndef vxworks
int main(int argc, char* argv[])
{
	Main(argv[1]);
 	
}
#endif
