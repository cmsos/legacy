/* fedkit documentation is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-wc-fifo.h,v 1.1 2007/10/09 08:19:01 cano Exp $
*/
#ifndef _FEDKIT_WC_FIFO_H_
#define _FEDKIT_WC_FIFO_H_

static char *rcsid_wc_fifo_h = "@(#) $Id: fedkit-wc-fifo.h,v 1.1 2007/10/09 08:19:01 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_wc_fifo_h);
#endif
/**
 * @function _fedkit_wc_fifo_free_slot_count
 * @return the number of free slots in the word count FIFO
 */
inline int _fedkit_wc_fifo_free_slot_count (struct _FK_wc_fifo * wc_fifo)
{
	int wr = wc_fifo->write & _FK_wc_fifo_mask; /* read this value once as it can change anytime */
	int rd = wc_fifo->read & _FK_wc_fifo_mask;
	
	int used = (wr - rd) & _FK_wc_fifo_mask;
	
	if (used > _FK_wc_fifo_depth) {
		eprintf ("ERROR in wc_fifo_free_slot_count : got more than _FK_wc_fifo_depth used pointers in wc FIFO\n"
                 "wr=0x%x, rd=0x%x, used=0x%0x\n", wr, rd, used);
		return 0;
	}
	
	return _FK_wc_fifo_depth - used;
}

/**
 * @function _fedkit_wc_fifo_used_slot_count
 * @return the number of free slots in the word count FIFO
 */
inline int _fedkit_wc_fifo_used_slot_count (struct _FK_wc_fifo * wc_fifo)
{
	int wr = wc_fifo->write & _FK_wc_fifo_mask; /* read this value once as it can change anytime */
	int rd = wc_fifo->read & _FK_wc_fifo_mask;
	
	int used = (wr - rd) & _FK_wc_fifo_mask;
	
/*	if (used > _FK_wc_fifo_depth) {
		eprintf ("ERROR in wc_fifo_used_slot_count : got more than _FK_wc_fifo_depth used pointers in wc FIFO\n"
                 "wr=0x%x, rd=0x%x, used=0x%0x\n", wr, rd, used);
		return _FK_wc_fifo_depth;
        }*/
	
	return used;
}

/**
 * @function _fedkit_send_wc_fifo_params_to_board 
 * this function retrives the physical addresses of the Dbuff used to implement
 * the wordcount fifo and writes base address of the fifo and address of the 
 * read counter in the right registers of the board. the board can the use the FIFO.
 */
inline void _fedkit_send_wc_fifo_params_to_board (struct fedkit_receiver * rec)
{
    /* rec->wc_fifo_physical was obtained (from where it was copied from) by 
       virt_to_bus(..), so is a physical address */
    struct _FK_wc_fifo * phy_wc_fifo = (struct _FK_wc_fifo *)rec->wc_fifo_physical;
    
    edprintf ("Sending wc params to board : wc fifo=%p, write=%p, phy_wc_fifo=%p\n",
              &(phy_wc_fifo->fifo_elements[0]), 
	      &(phy_wc_fifo->write), 
	      phy_wc_fifo);

    size_t tmp = (size_t) &(phy_wc_fifo->fifo_elements[0]);
    rec->map[_FK_WCFADDR_OFFSET/4] = (uint32_t) tmp;

    tmp = (size_t) &(phy_wc_fifo->write);
    rec->map[_FK_WCFWADDR_OFFSET/4] = (uint32_t) tmp;
    rec->map[_FK_WCFR_OFFSET/4] = rec->wc_fifo->read;
}

/**
 * Function for fetching one element from the word count (WC) 
 * FIFO (called by fedkit_frag_get)
 *
 * @return word count for the next event
 *   the function pops one word count from the word count FIFO.
 *   this includes updating the registers in the board
 */
 
inline struct _FK_wc_fifo_element _fedkit_pop_wc (struct fedkit_receiver * rec)
{
	struct _FK_wc_fifo_element ret={~0,1,1,1};
	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_lock (&(rec->global_mutex));
	#endif
	if (_fedkit_wc_fifo_used_slot_count (rec->wc_fifo) > 0) 
        {
		rec->wc_fifo->read  = (rec->wc_fifo->read + 1) & _FK_wc_fifo_mask; 
		/*rec->wc_fifo->read &=_FK_wc_fifo_mask;*/
		ret = rec->wc_fifo->fifo_elements[rec->wc_fifo->read & _FK_wc_fifo_address_mask];
                ret.no_fragment = 0;
		idprintf ("In _fedkit_pop_wc new read = %d\n", rec->wc_fifo->read);

		// this is probably done here in user space for performance
		// reasons ?
		// TODO: for debugging purposes, would be better to write
		//       to this register in the kernel driver such that
		//       we can keep track of how many blocks were
		//       acknowledged (and keep some statistics
		//       of e.g. the number of fragments and
		//       total amount of data transferred)
		rec->map[_FK_WCFR_OFFSET/4] = rec->wc_fifo->read;
	}
	idprintf ("in _fedkit_pop_wc ret={%d,%d,%d,%d}\n", ret.wc, ret.no_fragment, ret.truncated, ret.CRC_error);
	#ifndef NO_FEDKIT_PTHREAD
	pthread_mutex_unlock (&(rec->global_mutex));
	#endif

	return ret;
}

#endif /* ndef _FEDKIT_WC_FIFO_H_ */
