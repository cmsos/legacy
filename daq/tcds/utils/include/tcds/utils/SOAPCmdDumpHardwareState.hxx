#include "tcds/utils/SOAPUtils.h"

#include <string>

template <class T>
tcds::utils::SOAPCmdDumpHardwareState<T>::SOAPCmdDumpHardwareState(T& controller) :
  SOAPCmdBase<T>(controller, "DumpHardwareState", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdDumpHardwareState<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Do what we were asked to do.
  std::string res = this->controller_.readHardwareState();

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  tcds::utils::soap::addSOAPReplyParameter(reply, "registerDump", res);
  return reply;
}
