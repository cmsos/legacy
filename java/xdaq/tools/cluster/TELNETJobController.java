package xdaq.tools.cluster;

import xdaq.tools.cluster.JobController;
import java.io.*;
import com.oroinc.net.telnet.*;
import com.oroinc.io.*;
import java.util.*;

class TELNETJobController implements JobController
{
	protected String architecture_ = "";
	PrintStream ostream = null;
	StreamTokenizer inStream = null;
	
	public void exec (	String host, 
				String architecture, 
				Vector commands, 
				String username, 
				String password) 
				throws JobControllerException
	{
		//System.out.println("Executing job on:"+architecture);
		architecture_ = architecture;
		client_ = new TelnetClient();
		try {
			client_.connect ( host );
		} catch (IOException e) {
			throw new JobControllerException (e.getMessage());
		}
		try {
			// VxWorks, login without username and password
			BufferedReader in = new BufferedReader ( new InputStreamReader (client_.getInputStream()));
			 inStream = new StreamTokenizer (in);
			//inStream.eolIsSignificant(true);	
			 ostream = new PrintStream(client_.getOutputStream());
			 OutputStream rawOstream = client_.getOutputStream();
			// Go through the login procedure
			if (!architecture_.equals ("vxworks")) {
				this.getNextPrompt ( inStream , ":");
				System.out.println("Giving user name"+ username);
				ostream.println ( username );
				ostream.flush();
				int i =0;
				while (inStream.nextToken() != StreamTokenizer.TT_EOF) 
				{
					String tokenStr = inStream.toString();
					if ( tokenStr.indexOf("Password") != -1 ) {
						inStream.nextToken();
						tokenStr = inStream.toString();
						if ( tokenStr.indexOf(":") != -1) {	
							// found end of password
							//System.out.println("Giving password"+ passwd);
							ostream.println ( password );
							ostream.flush();
						}	
					} else if ( tokenStr.indexOf(">") != -1) {
						// found prompt, send next command
						String cmd = (String) commands.elementAt(i);
						//System.out.println("issuing command:"+cmd+ "\n\n");
						//ostream.println (cmd);
						//ostream.flush();
						rawOstream.write(cmd.getBytes());
						rawOstream.write('\n');
						rawOstream.flush();
						i++;
						if ( i == commands.size()) return ;
					} else if ( tokenStr.indexOf("Login") != -1) {
						inStream.nextToken();
						tokenStr = inStream.toString();
						if ( tokenStr.indexOf("incorrect") != -1) {
							throw new JobControllerException("Login incorrect");
						}	
					}
				}
				
				
			}
			
			
			
		} catch (Exception e) {
			String errorString = e.getMessage();
			try {
				client_.disconnect();
			} catch (Exception f) {
				errorString += ('\n'+f.getMessage());
			}
			throw new JobControllerException(errorString);
		}
		
		
	}
	
	
	private void getNextPrompt (StreamTokenizer in, String what) throws IOException
	{
		//System.out.println("Look for token:"+what);
		try {
			while (in.nextToken() != StreamTokenizer.TT_EOF) 
			{
				String tokenStr = in.toString();
				if ( tokenStr.indexOf(what) != -1 ) {
						return;
				}
			}
		} catch (IOException e) 
		{
			throw new IOException ("Cannot get Telnet prompt: " + e.getMessage());
		}
	}
			
	public void destroy () throws JobControllerException
	{
		try {	
			if (architecture_.equals("vxworks")) {
				// reboot system first
				this.getNextPrompt ( inStream , ">");
				ostream.println("reboot");
				ostream.flush();
			}
			client_.disconnect();
		} catch (IOException e) {
			throw new JobControllerException ("Telnet disconnect error: " + e.getMessage());
		}
	}
	
	public InputStream getInputStream() { return client_.getInputStream();}
	public OutputStream getOutputStream() { return client_.getOutputStream();}
	
	private TelnetClient client_;
}
