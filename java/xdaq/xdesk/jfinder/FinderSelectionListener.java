package xdaq.xdesk.jfinder;

import java.util.EventListener;

public interface FinderSelectionListener extends EventListener
{
	public void valueChanged (FinderSelectionEvent e);
}
