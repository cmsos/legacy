#include "tcds/hwutilsvme/version.h"

#include "config/version.h"
#include "hyperdaq/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xgi/version.h"

#include "tcds/hwlayer/version.h"
#include "tcds/hwlayervme/version.h"
#include "tcds/utils/version.h"

GETPACKAGEINFO(tcdshwutilsvme)

void
tcdshwutilsvme::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(hyperdaq);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);

  CHECKDEPENDENCY(tcdshwlayer);
  CHECKDEPENDENCY(tcdshwlayervme);
  CHECKDEPENDENCY(tcdsutils);
}

std::set<std::string, std::less<std::string> >
tcdshwutilsvme::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, hyperdaq);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);
  ADDDEPENDENCY(dependencies, xdaq);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);

  ADDDEPENDENCY(dependencies, tcdshwlayer);
  ADDDEPENDENCY(dependencies, tcdshwlayervme);
  ADDDEPENDENCY(dependencies, tcdsutils);

  return dependencies;
}
