#include "XDAQDOMSerializerTestSuite.h"
#include "xdata/Serializable.h"
#include "xdata/dom/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/XStr.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Bag.h"
#include "TestBag.h"

CPPUNIT_TEST_SUITE_REGISTRATION( XDAQDOMSerializerTestSuite );




void XDAQDOMSerializerTestSuite::setUp()
{
 	loader_ = new xdata::XMLDOMLoader();
	
}

void XDAQDOMSerializerTestSuite::tearDown()
{
	delete loader_;
}

void XDAQDOMSerializerTestSuite::validate(const string  & filename, xdata::Serializable * serializable, string& iXML, string& eXML) 
throw (CppUnit::Exception, xdata::exception::Exception::Exception)
{	
	DOMDocument * doc = 0;
	xdata::dom::Serializer * serializer = 0;
	
	try
	{ 
		doc = loader_->load(filename);
	}
	catch (xdata::exception::Exception::Exception & e )
	{
		throw CppUnit::Exception ( CppUnit::Message(e.what()), CPPUNIT_SOURCELINE() );	
	}
	
	try
	{ 
		serializer = new xdata::dom::Serializer();

	}
	catch (xdata::exception::Exception & e )
	{
		loader_->release(doc);
		throw CppUnit::Exception ( CppUnit::Message(e.what()), CPPUNIT_SOURCELINE() );	
	}

	try
	{ 

		DOMNode * root = doc->getDocumentElement();

		DOMElement* targetNode =  doc->createElement(xdata::XStr("target"));


		// import DOM into xdata variable
		serializer->import(serializable,root);


		// export xdata variable into a dom node
		DOMElement * exportedNode = serializer->exportAll(serializable, targetNode);


		// serialize into a XML
		eXML = "";
		xdata::XMLDOMSerializer s2(eXML);
		s2.serialize(exportedNode);

		iXML = "";
		xdata::XMLDOMSerializer s(iXML);
		s.serialize(root);


		loader_->release(doc);
		delete serializer;

	} 
	catch (xdata::exception::Exception & e )
	{
		// free document
		//cout << "error" << e.what() << endl;
		loader_->release(doc);
		delete 	serializer;
		throw e;	
	}	
}


//
// Tests for Integer
//

void XDAQDOMSerializerTestSuite::serializeInteger()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/Integer.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeIntegerWrongTag()
{
	xdata::Integer  v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/IntegerWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeIntegerWrongValue()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/IntegerWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeIntegerMissingType()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/IntegerMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeIntegerMissingValue()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/IntegerMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeIntegerWrongType()
{
	xdata::Integer v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/IntegerWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


//
// Tests for Float
//

void XDAQDOMSerializerTestSuite::serializeFloat()
{
	xdata::Float v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/Float.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeFloatWrongTag()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/FloatWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeFloatWrongValue()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/FloatWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeFloatMissingType()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/FloatMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeFloatMissingValue()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/FloatMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeFloatWrongType()
{
	xdata::Float v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/FloatWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeFloatTruncate()
{
	xdata::Float v;
	
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/FloatTruncate.xml", &v, importedXML, exportedXML) );
	string expected = "<Parameter type=\"float\">3.141593</Parameter>";
	CPPUNIT_ASSERT_EQUAL(expected, exportedXML);
	
}



//
// Tests for Boolean
//

void XDAQDOMSerializerTestSuite::serializeBoolean()
{
	
	xdata::Boolean v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/Boolean.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeBooleanWrongTag()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BooleanWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeBooleanWrongValue()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BooleanWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeBooleanMissingType()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BooleanMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeBooleanMissingValue()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BooleanMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeBooleanWrongType()
{
	xdata::Boolean v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BooleanWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}



//
// Tests forString
//

void XDAQDOMSerializerTestSuite::serializeString()
{
	xdata::String v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/String.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeStringWrongTag()
{
	xdata::String v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/StringWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeStringMissingType()
{
	xdata::String v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/StringMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeStringEmptyValue()
{
	xdata::String v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/StringEmptyValue.xml", &v, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeStringWrongType()
{
	xdata::String v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/StringWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}



//
// Tests for Vector
//

void XDAQDOMSerializerTestSuite::serializeVector()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/VectorInteger.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeVectorWrongTag()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/VectorIntegerWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeVectorWrongValue()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/VectorIntegerWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
	
}


void XDAQDOMSerializerTestSuite::serializeVectorMissingType()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/VectorIntegerMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeVectorMissingValue()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/VectorIntegerMissingValue.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeVectorWrongType()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/VectorIntegerWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeVectorMissingIndex()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/VectorIntegerMissingIndex.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeVectorWrongElementType()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/VectorIntegerWrongElementType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeVectorWrongElement()
{
	xdata::Vector<xdata::Integer> v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/VectorIntegerWrongElement.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

//
// Tests for Bag
//

void XDAQDOMSerializerTestSuite::serializeBag()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/Bag.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}


void XDAQDOMSerializerTestSuite::serializeBagWrongTag()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongTag.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeBagWrongValue()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongValue.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeBagMissingType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagMissingType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeBagWrongType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeBagMissingFieldName()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagMissingFieldName.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeBagWrongFieldType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagMissingFieldType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeBagWrongElement()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagWrongElement.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeBagEmpty()
{
	xdata::Bag<TestEmptyBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/BagEmpty.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeBagContentMismatchFieldName()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagContentMismatchFieldName.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}
void XDAQDOMSerializerTestSuite::serializeBagContentMismatchFieldType()
{
	xdata::Bag<TestBag> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/BagContentMismatchFieldType.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeBagWithVector()
{
	xdata::Bag<TestBagWithVector> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/BagWithVector.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeBagWithBag()
{
	xdata::Bag<TestBagWithBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/BagWithBag.xml", &b, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeVectorWithBag()
{
	xdata::Vector<xdata::Bag<TestBag> > v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/VectorWithBag.xml", &v, importedXML, exportedXML));

	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}


//
// Tests for UnsignedShort
//

void XDAQDOMSerializerTestSuite::serializeUnsignedShort()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/UnsignedShort.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeUnsignedShortWrongTag()
{
	xdata::UnsignedShort  v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeUnsignedShortWrongValue()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeUnsignedShortMissingType()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeUnsignedShortMissingValue()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeUnsignedShortWrongType()
{
	xdata::UnsignedShort v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedShortWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

//
// Tests for UnsignedLong
//

void XDAQDOMSerializerTestSuite::serializeUnsignedLong()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/UnsignedLong.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeUnsignedLongWrongTag()
{
	xdata::UnsignedLong  v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeUnsignedLongWrongValue()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeUnsignedLongMissingType()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeUnsignedLongMissingValue()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeUnsignedLongWrongType()
{
	xdata::UnsignedLong v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/UnsignedLongWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

//
// Tests for Double
//

void XDAQDOMSerializerTestSuite::serializeDouble()
{
	xdata::Double v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/Double.xml", &v, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeDoubleWrongTag()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleWrongTag.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeDoubleWrongValue()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleWrongValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeDoubleMissingType()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleMissingType.xml", &v, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeDoubleMissingValue()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleMissingValue.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeDoubleWrongType()
{
	xdata::Double v;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/DoubleWrongType.xml", &v, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeDoubleTruncate()
{
	xdata::Double v;
	
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/DoubleTruncate.xml", &v, importedXML, exportedXML) );
	string expected = "<Parameter type=\"double\">3.141593e+00</Parameter>";
	CPPUNIT_ASSERT_EQUAL(expected, exportedXML);
	
}


//
// Tests for Monitorable
//
/*
void XDAQDOMSerializerTestSuite::serializeMonitorable()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/Monitorable.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}


void XDAQDOMSerializerTestSuite::serializeMonitorableWrongTag()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableWrongTag.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeMonitorableWrongValue()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableWrongValue.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeMonitorableMissingType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableMissingType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}


void XDAQDOMSerializerTestSuite::serializeMonitorableWrongType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableWrongType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeMonitorableMissingFieldName()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableMissingFieldName.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWrongFieldType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableMissingFieldType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWrongElement()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableWrongElement.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeMonitorableEmpty()
{
	xdata::Monitorable<TestEmptyMonitorable> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/MonitorableEmpty.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeMonitorableContentMismatchFieldName()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableContentMismatchFieldName.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}
void XDAQDOMSerializerTestSuite::serializeMonitorableContentMismatchFieldType()
{
	xdata::Monitorable<TestMonitorable> b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableContentMismatchFieldType.xml", &b, importedXML, exportedXML) , xdata::exception::Exception);
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWithVector()
{
	xdata::Monitorable<TestMonitorableWithVector> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/MonitorableWithVector.xml", &b, importedXML, exportedXML));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWithBag()
{
	xdata::Monitorable<TestMonitorableWithBag> b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/MonitorableWithBag.xml", &b, importedXML, exportedXML) );
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeVectorWithMonitorable()
{
	xdata::Vector<xdata::Monitorable<TestMonitorable> > v;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/VectorWithMonitorable.xml", &v, importedXML, exportedXML));

	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWithPropertiesOnly()
{
	TestMonitorableWithPropertiesOnly b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/MonitorableWithPropertiesOnly.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWithPropertiesAndFields()
{
	TestMonitorableWithPropertiesAndFields b;
	CPPUNIT_ASSERT_NO_THROW( this->validate("../../data/MonitorableWithPropertiesAndFields.xml", &b, importedXML, exportedXML ));
	CPPUNIT_ASSERT_EQUAL( importedXML, exportedXML);
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWithMissingPropertyName()
{
	TestMonitorableWithPropertiesOnly b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableWithMissingPropertyName.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}

void XDAQDOMSerializerTestSuite::serializeMonitorableWithWrongPropertyType()
{
	TestMonitorableWithPropertiesOnly b;
	CPPUNIT_ASSERT_THROW( this->validate("../../data/MonitorableWithWrongPropertyType.xml", &b, importedXML, exportedXML), xdata::exception::Exception );
}
*/
