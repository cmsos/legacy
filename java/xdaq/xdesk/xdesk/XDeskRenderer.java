package xdaq.xdesk.xdesk;

import java.awt.Component;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.*;

import java.io.*;

import xdaq.xdesk.jfinder.*;

public class XDeskRenderer extends DefaultFinderRenderer 
{
	DeskletContext context_;

	public XDeskRenderer (DeskletContext context)
	{
		context_ = context;
	}


	public Component getFinderRendererComponent (JFinder finder,
							Object value,
							boolean isSelected,
							boolean hasFocus,
							JLabel where)
	{	
		JLabel label = (JLabel) super.getFinderRendererComponent (finder,value,isSelected,hasFocus,where);
		FinderTreeNode cell = (FinderTreeNode) value;
		File file = cell.getFile();
		try {
			String canonicalPath = file.getCanonicalPath();
			String name = context_.getAppletName(canonicalPath);

			if (name != null)
			{
				// Desklet is running, provide special rendering
				label.setBorder (BorderFactory.createLineBorder(Color.red) );
				return label;		

			}
		} catch (IOException ioe)
		{
			return label;
		}
		
		return label;
	}		
}
