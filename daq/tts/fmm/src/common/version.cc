// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tts/fmm/version.h"

#include "tts/fmmtd/version.h"
#include "tts/cpcibase/version.h"
#include "tts/ttsbase/version.h"
#include "tts/ipcutils/version.h"

//FIXME: add HAL, GenPCI

#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"

GETPACKAGEINFO(ttsfmm)

void ttsfmm::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(ttsfmmtd);  
	CHECKDEPENDENCY(ttscpcibase);  
	CHECKDEPENDENCY(ttsttsbase);  
	CHECKDEPENDENCY(ttsipcutils);  

	CHECKDEPENDENCY(config);  
	CHECKDEPENDENCY(xcept);  
	CHECKDEPENDENCY(toolbox); 

}

std::set<std::string, std::less<std::string> > ttsfmm::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,ttsfmmtd);
	ADDDEPENDENCY(dependencies,ttscpcibase);
	ADDDEPENDENCY(dependencies,ttsttsbase);
	ADDDEPENDENCY(dependencies,ttsipcutils);

	ADDDEPENDENCY(dependencies,config); 
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,toolbox);
	 
	return dependencies;
}	
	
