// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Client_h_
#define _Client_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include <set>
#include "toolbox/Task.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/math/random2.h"
#include "xdata/UnsignedLong.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Event.h"
#include "toolbox/task/Timer.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

//AP
#include <boost/random.hpp>
#include <boost/random/lognormal_distribution.hpp>
#include <boost/random/variate_generator.hpp>
#include <cmath>

#include "toolbox/task/WorkLoop.h"

class Client: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
{	
	public:
		XDAQ_INSTANTIATOR();

		Client(xdaq::ApplicationStub* c) throw(xdaq::exception::Exception);
		~Client();


		void start(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception);

	protected:
	
		// Send an I2O token message to all servers, If last is 1, the flag 'last' in the
		// message is set to one. Otherwise it is set to 0
		//
		int sendMessage();

		bool process(toolbox::task::WorkLoop* wl);

		//
		// used to create the memory pool upon parametrization
		//	
		void actionPerformed (xdata::Event& e);
		
		std::set<const xdaq::ApplicationDescriptor*> destination_;	// Vector of all server tids
		xdaq::ApplicationDescriptor **servers_;	// array of all server tids
		xdata::UnsignedLong committedPoolSize_;		// Total memory for messages

		toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending

    	toolbox::task::WorkLoop* workLoop_;
    	toolbox::task::ActionSignature* process_;
};

#endif
