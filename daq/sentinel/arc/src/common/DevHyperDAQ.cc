// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "sentinel/arc/DevHyperDAQ.h"
#include "sentinel/arc/Application.h"

#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"

#include "sentinel/utils/Alarm.h"

#include <string.h>
#include <jansson.h>

sentinel::arc::DevHyperDAQ::DevHyperDAQ (sentinel::arc::Application * app)
	: HyperDAQ(app)
{
	xgi::framework::deferredbind(app, this, &sentinel::arc::DevHyperDAQ::Default, "Default");
	xgi::deferredbind(app, this, &sentinel::arc::DevHyperDAQ::fire, "fire");
}

void sentinel::arc::DevHyperDAQ::Default (xgi::Input * in, xgi::Output * out) 
{
	*out << "<link href=\"/sentinel/arc/html/css/xdaq-sentinel-arc.css\" rel=\"stylesheet\" />" << std::endl;

	std::stringstream urlBase;
	urlBase << application_->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << application_->getApplicationDescriptor()->getURN() << "/";

	*out << "<div class=\"xdaq-tab-wrapper\" id=\"sentinel-arc-wrapper\" data-url=\"" << urlBase.str() << "\">" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Model\" id=\"model-tab\">" << std::endl;

	*out << "<button id=\"tree-toggle\" onclick=\"toggleGetTree()\">Auto Refresh</button><br /><br />          " << std::endl;

	this->modelTabPage(in, out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Exception Maker\" id=\"exceptions-tab\">" << std::endl;
	this->exceptionMakerTab(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Shelf Rules\">" << std::endl;
	this->shelfRulesTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Auto-Clear Rules\">" << std::endl;
	this->clearRulesTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"De-Bounce Rules\">" << std::endl;
	this->debounceRulesTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->statisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";

	*out << "	<script type=\"text/javascript\" src=\"/sentinel/arc/html/js/xdaq-sentinel-arc.js\"></script>" << std::endl;
	*out << "	<script type=\"text/javascript\" src=\"/sentinel/arc/html/js/xdaq-sentinel-arc-dev.js\"></script>" << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/html; charset=utf-8");
}

void sentinel::arc::DevHyperDAQ::exceptionMakerTab (xgi::Output * out) 
{
	*out << "<table id=\"exception-maker\" class=\"xdaq-table\">          " << std::endl;
	*out << "	<caption>Exception Maker</caption>          " << std::endl;
	*out << "    <tbody>                                                 " << std::endl;
	//*out << "<tr><td style=\"min-width: 100px;\">user</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-user\" value=\"int9r_lb-CMS_LUMI_ERR_LOG\"/></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">DB</td><td><select id=\"exception-maker-user\">";
	std::list < std::string > stores = application_->getAlarmStoreNames();
	for (std::list<std::string>::iterator i = stores.begin(); i != stores.end(); i++)
	{
		*out << "<option value=\"" << (*i) << "\">" << (*i) << "</option>" << std::endl;
	}
	*out << "</select></td></tr>      " << std::endl;

	*out << "<tr><td colspan=\"2\"></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">uuid</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-uuid\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">type</td><td><select id=\"exception-maker-type\"><option value=\"alarm\">Alarm</option><option value=\"exception\">Exception</option></select></td></tr>      " << std::endl;
	*out << "<tr><td colspan=\"2\"></td></tr>      " << std::endl;

	*out << "<tr><td style=\"min-width: 100px;\">message</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-message\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">identifier</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-identifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">notifier</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-notifier\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">severity</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-severity\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">class</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-class\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">tag</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-tag\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">instance</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-instance\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">unamespace</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-unamespace\" /></td></tr>      " << std::endl;
	*out << "<tr><td style=\"min-width: 100px;\">uvalue</td><td><input type=\"text\" style=\"width:200px\" id=\"exception-maker-uvalue\" /></td></tr>      " << std::endl;
	*out << "<tr><td colspan=\"2\"><input type=\"button\" id=\"exception-maker-submit\" value=\"Submit\" class=\"xdaq-submit-button\"/></td></tr>      " << std::endl;

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;
}

void sentinel::arc::DevHyperDAQ::fire (xgi::Input * in, xgi::Output * out) 
{
	try
	{
		std::string uuid = "";
		std::string identifier = "sentinel::arc::exception::Test";
		std::string notifier = "http://srv-c2d06-17.cms:1972/urn:xdaq-application:lid=3";
		std::string severity = "error";
		std::string className = "sentinel::arc::DevHyperDAQ";
		std::string message = "this is an exception";
		std::string tag = "";
		std::string instance = "";
		std::string unamespace = "urn:sentinelarc:test";
		std::string uvalue = "69";
		std::string group = application_->getApplicationDescriptor()->getAttribute("group");
		std::string user = "";
		std::string type = "exception";

		cgicc::Cgicc cgi(in);
		cgicc::const_form_iterator fi = cgi.getElement("identifier");
		if (fi != cgi.getElements().end())
		{
			identifier = (*fi).getValue();
		}

		fi = cgi.getElement("notifier");
		if (fi != cgi.getElements().end())
		{
			notifier = (*fi).getValue();
		}

		fi = cgi.getElement("type");
		if (fi != cgi.getElements().end())
		{
			type = (*fi).getValue();
		}

		fi = cgi.getElement("uuid");
		if (fi != cgi.getElements().end())
		{
			uuid = (*fi).getValue();
		}

		fi = cgi.getElement("severity");
		if (fi != cgi.getElements().end())
		{
			severity = (*fi).getValue();
		}

		fi = cgi.getElement("message");
		if (fi != cgi.getElements().end())
		{
			message = (*fi).getValue();
		}

		fi = cgi.getElement("class");
		if (fi != cgi.getElements().end())
		{
			className = (*fi).getValue();
		}

		fi = cgi.getElement("tag");
		if (fi != cgi.getElements().end())
		{
			tag = (*fi).getValue();
		}

		fi = cgi.getElement("instance");
		if (fi != cgi.getElements().end())
		{
			instance = (*fi).getValue();
		}
		fi = cgi.getElement("unamespace");
		if (fi != cgi.getElements().end())
		{
			unamespace = (*fi).getValue();
		}
		fi = cgi.getElement("uvalue");
		if (fi != cgi.getElements().end())
		{
			uvalue = (*fi).getValue();
		}
		fi = cgi.getElement("user");
		if (fi != cgi.getElements().end())
		{
			user = (*fi).getValue();
		}
		else
		{
			//XCEPT_RAISE(xgi::exception::Exception, "missing user parameter");
		}

		xcept::Exception exception7(identifier, message, __FILE__, __LINE__, __FUNCTION__);

		exception7.setProperty("notifier", notifier);
		exception7.setProperty("severity", severity);
		exception7.setProperty("urn:xdaq-application:class", className);
		exception7.setProperty("tag", tag);
		exception7.setProperty("urn:xdaq-application:instance", instance);
		exception7.setProperty(unamespace, uvalue);
		exception7.setProperty("urn:xdaq-application:group", group);
		exception7.setProperty("args", type);

		//typedef std::pair<sentinel::arc::utils::AlarmStore*, toolbox::TimeVal> AlarmStoreDescriptor;
		if (uuid != "")
		{
			exception7.setProperty("uniqueid", uuid);
		}

		std::cout << "HYPERDAQ firing " << exception7.getProperty("uniqueid") << std::endl;
		application_->alarmStores_[user].first->fire(exception7);

	}
	catch (sentinel::arc::utils::exception::FailedToStore &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RETHROW(xgi::exception::Exception, msg.str(), se);
	}
	catch (std::exception &se)
	{
		std::stringstream msg;
		msg << "Failed to process 'inject' request, error: " << se.what();
		XCEPT_RAISE(xgi::exception::Exception, msg.str());
	}
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}
