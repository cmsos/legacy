// $Id: version.h,v 1.43 2009/05/28 12:46:19 cschwick Exp $
// tracId : 2243
#ifndef _ferol40_version_h_
#define _ferol40_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!
#define FEROL40_VERSION_MAJOR 2
#define FEROL40_VERSION_MINOR 0
#define FEROL40_VERSION_PATCH 6
// If any previous versions available E.g. #define FEROL40_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef FEROL40_PREVIOUS_VERSIONS
#define FEROL40_PREVIOUS_VERSIONS "1.11.0,1.12.0,2.0.0,2.0.2,2.0.3,2.0.4,2.0.5"

//
// Template macros
//
#define FEROL40_VERSION_CODE PACKAGE_VERSION_CODE(FEROL40_VERSION_MAJOR,FEROL40_VERSION_MINOR,FEROL40_VERSION_PATCH)
#ifndef FEROL40_PREVIOUS_VERSIONS
#define FEROL40_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(FEROL40_VERSION_MAJOR,FEROL40_VERSION_MINOR,FEROL40_VERSION_PATCH)
#else 
#define FEROL40_FULL_VERSION_LIST  FEROL40_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(FEROL40_VERSION_MAJOR,FEROL40_VERSION_MINOR,FEROL40_VERSION_PATCH)
#endif 

namespace ferol40
{
	const std::string package  =  "ferol40";
	const std::string versions =  FEROL40_FULL_VERSION_LIST;
	const std::string description = "Contains the ferol40 library.";
	const std::string authors = "Christoph Schwick, Jonathan Fulcher";
	const std::string summary = "CMS Ferol40 Software.";
	const std::string link = "http://makerpmhappy";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
