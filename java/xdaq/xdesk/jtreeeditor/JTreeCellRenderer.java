package xdaq.xdesk.jtreeeditor;

import java.util.Enumeration;
import java.util.EventObject;
import java.util.Vector;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.ListSelectionModel;
import javax.swing.event.CellEditorListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreePath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JTreeCellRenderer
    extends JTable
    implements TreeCellRenderer, TreeCellEditor {
//---------------------------------------Variables Definition-------------------
  private JTreeUserObject nodeUserObject;
  private boolean isNodeSelected;
  private JTreeEditor tree_ = null;
  private JTreeConfig config = null;
  private TreePath path = null;

  //--------------------------------Constructors----------------------------------

  /**An empty constructor*/
  public JTreeCellRenderer() {
    this.setRowSelectionAllowed(false);
    this.setColumnSelectionAllowed(false);
    this.getSelectionModel().setSelectionMode(ListSelectionModel.
                                              SINGLE_SELECTION);
    this.setDefaultRenderer(JTreeEditableText.class, new JTreeTableCellRenderer());
    this.setDefaultRenderer(ImageIcon.class, new JTreeTableCellRenderer());
    this.setDefaultRenderer(JTreeNotEditableText.class,
                            new JTreeTableCellRenderer());
    this.setDefaultRenderer(JTreeIconWithText.class, new JTreeTableCellRenderer());

    this.setDefaultEditor(JTreeEditableText.class, new JTreeTableCellEditor());
    this.setDefaultEditor(ImageIcon.class, new JTreeTableCellEditor());
    this.setDefaultEditor(JTreeNotEditableText.class, new JTreeTableCellEditor());
    this.setDefaultEditor(JTreeIconWithText.class, new JTreeTableCellEditor());
  }

//******************************************************************************
   /**Not implemented*/
   public void removeCellEditorListener(CellEditorListener l) {
   }

//******************************************************************************
   /**Not implemented*/
   public void addCellEditorListener(CellEditorListener l) {
   }

//******************************************************************************
   /**Forwards the message from the CellEditor to the delegate.*/
   public void cancelCellEditing() {
     JTreeTableModel m_mod = (JTreeTableModel)this.getModel();
     try {
       JTreeTableCellEditor ed = (JTreeTableCellEditor)this.getCellEditor();
       JTreeEditableText nn = new JTreeEditableText(ed.getCellEditorValue().
           toString());
       m_mod.setValueAt(nn, 0, ed.col);
     }
     catch (NullPointerException e) {
     }
     switch (nodeUserObject.getType()) {
       case 1:
         Node oldNode = nodeUserObject.domNode;
         Document doc = oldNode.getOwnerDocument();
         Node newNode = doc.createElement(m_mod.getValueAt(0, 1).toString());
         NamedNodeMap nodeMap = oldNode.getAttributes();
         //Import Attributes from an old node to the new one
         for (int i = 0; i < nodeMap.getLength(); i++) {
           ( (Element) newNode).setAttribute(nodeMap.item(i).getNodeName(),
                                             nodeMap.item(i).getNodeValue());
         }
         NodeList nodeList = oldNode.getChildNodes();
         int lngth = nodeList.getLength();

         //must create a vector or an Array
         //Otherwise, if use the nodeList directly then the length
         //of nodeList is decreasing by itself
         // during the loop.
         Vector v = new Vector();
         for (int n = 0; n < lngth; n++) {
           v.add(nodeList.item(n));
         }

         //import childs from an old node to the new one
         for (Enumeration n = v.elements(); n.hasMoreElements(); ) {
           newNode.appendChild( (Node) n.nextElement());
         }
         newNode.normalize();
         oldNode.getParentNode().replaceChild(newNode, oldNode);
         nodeUserObject.setName(m_mod.getValueAt(0, 1).toString());
         nodeUserObject.domNode = newNode;
         break;
       case 2:
         nodeUserObject.setName(m_mod.getValueAt(0, 1).toString());
         nodeUserObject.setValue(m_mod.getValueAt(0, 2).toString());

         DefaultMutableTreeNode temp_node = (DefaultMutableTreeNode) path.
             getLastPathComponent();
         DefaultMutableTreeNode temp_parent_node = (DefaultMutableTreeNode)
             temp_node.getParent();
         JTreeUserObject parent_user_object = (JTreeUserObject)
             temp_parent_node.
             getUserObject();

         String atrrString = "";
         JTreeUserObject tmpUserObject;

         //Remove all the attruibutes from node.
         NamedNodeMap map = ( (Element) parent_user_object.domNode).
             getAttributes();
         for (int i = 0; i < map.getLength(); i++) {
           ( (Element) parent_user_object.domNode).removeAttribute(map.item(i).
               getNodeName());
         }

         for (int i = 0; i < temp_parent_node.getChildCount(); i++) {
           temp_node = (DefaultMutableTreeNode) temp_parent_node.getChildAt(i);
           tmpUserObject = (JTreeUserObject) temp_node.getUserObject();
           if (tmpUserObject.getType() == 2) {
             atrrString = atrrString + tmpUserObject.getName() + "=" +
                 tmpUserObject.getValue() + ", ";
             //Redefine attributes for domNode

             ( (Element) parent_user_object.domNode).setAttribute(tmpUserObject.
                 getName(), tmpUserObject.getValue());
           }

         }

         //attrString used for presentation only of all the attributes
         atrrString = atrrString.substring(0, atrrString.length() - 2);
         atrrString = "(" + atrrString.trim() + ")";
         parent_user_object.setAttrList(atrrString);
         tree_.revalidate();

         //printOut(parent_user_object.domNode.getOwnerDocument());

         break;
       case 3:
         nodeUserObject.setValue(m_mod.getValueAt(0, 1).toString());
         nodeUserObject.domNode.setNodeValue(m_mod.getValueAt(0, 1).toString());
         break;
       case 4:
         nodeUserObject.setValue(m_mod.getValueAt(0, 1).toString());
         nodeUserObject.domNode.setNodeValue(m_mod.getValueAt(0, 1).toString());
         break;
       case 7:
         Node oldNode1 = nodeUserObject.domNode;
         if (oldNode1.getParentNode() == null) {
           break;
         }
         Document doc1 = nodeUserObject.DomDocument;
         Node newNode1 = doc1.createProcessingInstruction(m_mod.getValueAt(0, 1).
             toString(), m_mod.getValueAt(0, 2).toString());
         oldNode1.getParentNode().replaceChild(newNode1, oldNode1);
         nodeUserObject.setName(m_mod.getValueAt(0, 1).toString());
         nodeUserObject.setValue(m_mod.getValueAt(0, 2).toString());
         break;
       case 8:
         nodeUserObject.setValue(m_mod.getValueAt(0, 1).toString());
         nodeUserObject.domNode.setNodeValue(m_mod.getValueAt(0, 1).toString());
         break;
     }

     tree_.revalidate();
     tree_.repaint();

   }

//******************************************************************************
   /**Forwards the message from the CellEditor to the delegate.*/
   public boolean stopCellEditing() {
     return true;
   }

//******************************************************************************
   /**Return current tree*/
   public JTreeEditor get_Tree() {
     return tree_;
   }

//******************************************************************************
   /**Forwards the message from the CellEditor to the delegate.*/
   public boolean shouldSelectCell(EventObject anEvent) {
     return true;
   }

//******************************************************************************
   /**Forwards the message from the CellEditor to the delegate.*/
   public boolean isCellEditable(EventObject ev) {
     boolean returnValue = false;
     if (ev instanceof MouseEvent) {
       MouseEvent evt = (MouseEvent) ev;
       if (evt.getClickCount() > 1) {
         returnValue = true;
       }
     }
     return returnValue;
   }

//******************************************************************************
   /**Sets an initial value for the editor.
    * Implements the TreeCellEditor interface.
    */
   public Component getTreeCellEditorComponent(JTree tree,
                                               Object value,
                                               boolean selected,
                                               boolean expanded,
                                               boolean leaf,
                                               int row
                                               ) {
     tree_ = (JTreeEditor) tree;
     config = tree_.getConfiguration();
     DefaultMutableTreeNode x = (DefaultMutableTreeNode) value;
     path = tree_.getPathForRow(row);

     JTreeUserObject node = (JTreeUserObject) x.getUserObject();

     DefaultMutableTreeNode parentnode = null;
     JTreeUserObject parentobject = null;
     if (!x.isRoot()) {
       parentnode = (DefaultMutableTreeNode) x.getParent();
       parentobject = (JTreeUserObject) parentnode.getUserObject();
     }

     node.setParent(parentobject);

     node.setIcon(config.getIcon(node, tree_.isEditable()));
     isNodeSelected = selected;
     nodeUserObject = node;
     this.setModel(new JTreeTableModel(node));
     setSizes();
     setBorders();

     this.setVisible(true);
     return this;

   }

//******************************************************************************
   /**Sets the value of the current tree cell to value.*/
   public Component getTreeCellRendererComponent(JTree tree,
                                                 Object value,
                                                 boolean selected,
                                                 boolean expanded,
                                                 boolean leaf,
                                                 int row,
                                                 boolean hasFocus) {
     tree_ = (JTreeEditor) tree;
     config = tree_.getConfiguration();
     DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
     path = tree_.getPathForRow(row);
     if (! (tree_.getModel() instanceof JTreeEditorModel)) {
       return new DefaultTreeCellRenderer();
     }

     DefaultMutableTreeNode parentnode = null;
     if (!node.isRoot()) {
       parentnode = (DefaultMutableTreeNode) node.getParent();
     }

     JTreeUserObject nobj = (JTreeUserObject) node.getUserObject();

     JTreeUserObject parentobject = null;
     if (!node.isRoot()) {
       parentobject = (JTreeUserObject) parentnode.getUserObject();
     }
     nobj.setParent(parentobject);

     nobj.setIcon(config.getIcon(nobj, tree_.isEditable()));
     nodeUserObject = nobj;
     isNodeSelected = selected;
     this.setModel(new JTreeTableModel(nobj));
     setSizes();
     setBorders();
     this.setVisible(true);
     return this;
   }

//******************************************************************************
   /**Forwards the message from the CellEditor to the delegate.*/
   public Object getCellEditorValue() {
     return this;
   }

//******************************************************************************
   /**Sets the editor cell sizes*/
   void setSizes() {
     this.setRowHeight(tree_.getConfiguration().getRowHeight());
     switch (nodeUserObject.getType()) {
       case 1:
         this.getColumnModel().getColumn(0).setPreferredWidth(30);
         if (nodeUserObject.getAttrList() != null &&
             nodeUserObject.getAttrList().trim() != "") {
           this.getColumnModel().getColumn(1).setPreferredWidth(170);
           try {
             this.getColumnModel().getColumn(2).setPreferredWidth(300);
           }
           catch (Exception e) {}
         }
         else {
           this.getColumnModel().getColumn(1).setPreferredWidth(470);
         }
         break;
       case 3:
       case 4:
       case 8:
         this.getColumnModel().getColumn(0).setPreferredWidth(100);
         this.getColumnModel().getColumn(1).setPreferredWidth(400);
         break;
       case 2:
       case 7:
         this.getColumnModel().getColumn(0).setPreferredWidth(30);
         this.getColumnModel().getColumn(1).setPreferredWidth(170);
         this.getColumnModel().getColumn(2).setPreferredWidth(300);
         break;
     }
   }

//******************************************************************************
   /**Sets borders of cells according to selection*/
   public void setBorders() {
     if (isNodeSelected) {
       this.setBorder(BorderFactory.createLineBorder(Color.blue));

       this.setBackground(Color.yellow);
     }
     else {
       this.setBorder(BorderFactory.createLineBorder(Color.white));
       this.setBackground(Color.white);
     }
     this.setGridColor(Color.white);
   }

//******************************************************************************
//Used for saving the document during testing
    /*
      private void printOut(Document doc) {
        try {
          TransformerFactory factory = TransformerFactory.newInstance();
          Transformer transformer = factory.newTransformer();
          DOMSource source = new DOMSource(doc);
          File res = new File("C:\\1.xml");
          StreamResult result = new StreamResult(res);
          transformer.setOutputProperty("indent", "yes");
          transformer.transform(source, result);
        }
        catch (Exception e) {
          System.out.println(e.getMessage());
        }
      }*/
}
