/**
 * @file frl_kernel.c
 * kernel side driver of the FRL driver 
 * 
 * at installation time, it has to detect all the receiver boards present
 *
 * first function is to reserve a board (identified by ID) for the calling 
 * file descriptor. Base address for the board is delivered in the reply
 *
 * final one is to release the adapter and to free up everything
 * 
 */ 

/*
 *  Maintainer : $author$
 *  $Revision: 1.5 $ 
 *  $Id: frl_kernel.c,v 1.5 2004/11/04 09:36:50 cschwick Exp $
 */
static char *rcsid = "@(#) $Id: frl_kernel.c,v 1.5 2004/11/04 09:36:50 cschwick Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */

#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid);
#endif
 

#define FRL_DEVNAME_SIZE 20

#ifdef _FRL_BIGPHYS_
#include <linux/bigphysarea.h>
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/pci.h>
#include <linux/types.h>
#include <linux/wrapper.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include "frl-private.h"

#ifndef _FRL_BIGPHYS_

/* new interface 
   extern caddr_t  bigphysarea_alloc_pages(int count, int align, int priority);
   extern void     bigphysarea_free_pages(caddr_t base);
*/

inline void*  bigphysarea_alloc_pages(int count, int align, int priority) {
    printk(KERN_INFO  "normal kmalloc without bigphys\n" );
    return kmalloc( count*PAGE_SIZE, GFP_KERNEL | GFP_DMA);
}

inline void bigphysarea_free_pages( caddr_t base ) {
    kfree( base );
}

#endif /* ndef _FRL_BIGPHYS_ */

/* file operations for the FRL device driver */

int kfrl_open    (struct inode *, struct file *);
int kfrl_ioctl   (struct inode *, struct file *, unsigned int, unsigned long);
int kfrl_mmap    (struct file *,  struct vm_area_struct *);
int kfrl_release (struct inode *, struct file *);

struct file_operations kfrl_fops = {
    ioctl   : kfrl_ioctl,
    mmap    : kfrl_mmap,
    open    : kfrl_open,
    release : kfrl_release
};


/* list of all the allocated receivers */
struct kfrl_receiver_t
{
    struct kfrl_receiver_t  *next;        /* next receiver in linked list */
    struct semaphore         lock;        /* semphore for the receiver */
    int index;                            /* pci device index */
    char device_name [FRL_DEVNAME_SIZE];  /* string of device file */
    U32 base_address0;                    /* BAR0 */
    U32 base1_address0;                   /* BAR1 */
    U32 * map0;                           /* address space of BAR0 bridge FPGA mapped into kernel */
    U32 * map1;                           /* address space of BAR1 FRL    FPGA mapped into kernel */
    U32 FPGA_version;                     /* firmware version of bridge FPGA */
    U32 FPGA1_version;                    /* firmware version of FRL    FPGA */

    int block_number;                     /* number of blocks;    set in ioctl FRL_SETGET_RECEIVER_PARAMETERS */
    int block_size;                       /* size of data blocks; set in ioctl FRL_SETGET_RECEIVER_PARAMETERS */
    void * block_fifo;                    /* fifo for blocks, contains pointers to blocks */
    /* allocated in ioctl FRL_GET_RECEIVER_MEMBLOCK */

    void * wc_fifo;                       /* pointer to wordcount fifo */

    int do_allocate_blocks;               /* a flag indicating that the receiver should allocate the memory */
    void * blocks;                        /* if driver has to allocate the blocks, this points to allocated region */
    /* allocated in ioctl FRL_GET_RECEIVER_MEMBLOCK */

    struct pci_dev          *device;      /* the bridge FPGA */
    struct pci_dev          *device1;     /* the FRL    FPGA */

    /* spy relevant data */
    U32 blocksInSpy;                      /* number of blocks currently in spy; maintained by software */      
    struct _FRL_data_fifo   *data_fifo;   /* the data fifo is located just behind the block fifo */
    
    /* (they are allocated in one call) */

    void                   **data_fifo_tab;
} *kfrl_receivers = NULL;


struct semaphore kfrl_receivers_lock;

/***********************************************************************/

int kfrl_read_proc (char *buf, char **start, off_t offset, 
		    int count, int *eof, void *data){
    struct kfrl_receiver_t* receiver = kfrl_receivers;
    int len = 0;
    int i;
    unsigned int geoAddress;

    len += sprintf (buf+len, "o %d s %d\n", (int)offset, (int)*start);

    if ( receiver == NULL ) {
        len += sprintf( buf+len, "\nNo open FRL devices\n\n" );
        *eof = 1;
        *start = (char *) 0;
        return len;
    }

    if ( offset == 0 ) {
        len += sprintf( buf+len, "\nReport on open FRL devices:\n\n" );
    }

    // advance to the receiver indicated by the offset
    // check for end conditions
  
    for ( i=0; i<offset; i++ ) {
        receiver = receiver->next;
        if ( receiver == NULL ) {
            *eof = 1;
            *start = (char *) 0;
            return len;
        }
    }

    pci_read_config_dword (receiver->device, 0x50, &geoAddress);
    len += sprintf (buf+len, "Device             : %s\n", receiver->device_name);
    len += sprintf (buf+len, "    Spy FPGA       : %08x\n", receiver->FPGA_version );
    len += sprintf (buf+len, "    FRL FPGA       : %08x\n", receiver->FPGA1_version );
    len += sprintf (buf+len, "    # spyblocks    : %d\n", receiver->block_number);
    len += sprintf (buf+len, "    spyblock size  : %08x (hex)\n", receiver->block_size);
    len += sprintf (buf+len, "    slot address   : %d\n", geoAddress);
    len += sprintf (buf+len, "  Parameters read from SPY FPGA registers:\n");
    len += sprintf (buf+len, "    write Ptr Adr  : %03x %08x\n", _FRL_WCFWADDR_OFFSET, receiver->map0[ _FRL_WCFWADDR_OFFSET/4 ] );
    len += sprintf (buf+len, "    control word   : %03x %08x\n", _FRL_CSR_OFFSET, receiver->map0[ _FRL_CSR_OFFSET/4 ] );
    len += sprintf (buf+len, "    size in hard   : %03x %08x\n", _FRL_BKSZ_OFFSET, receiver->map0[ _FRL_BKSZ_OFFSET/4 ] );
    len += sprintf (buf+len, "    debug info     : %03x %08x\n", _FRL_DEBUG_OFFSET, receiver->map0[ _FRL_DEBUG_OFFSET/4 ] );
    len += sprintf (buf+len, "  Parameters read from FRL FPGA registers (byte swapped!):\n");
    len += sprintf (buf+len, "    control word   : %03x %08x\n", _FRL1_CSR, receiver->map1[ _FRL1_CSR/4 ] );
    len += sprintf (buf+len, "    # blocks send  : %03x %08x\n", _FRL1_SEGMENTCNT, receiver->map1[ _FRL1_SEGMENTCNT/4 ] );
    len += sprintf (buf+len, "    # frags  send  : %03x %08x\n", _FRL1_FRAGMENTCNT, receiver->map1[ _FRL1_FRAGMENTCNT/4 ] );
    len += sprintf (buf+len, "    # free blocks  : %03x %08x\n", _FRL1_FREEBLOCK, receiver->map1[ _FRL1_FREEBLOCK/4 ] );
    len += sprintf (buf+len, "    current trigno : %03x %08x\n", _FRL1_CURTRIGNO, receiver->map1[ _FRL1_CURTRIGNO/4 ] );
    len += sprintf (buf+len, "    debug reg      : %03x %08x\n", _FRL1_CSRDBG, receiver->map1[ _FRL1_CSRDBG/4 ] );
    len += sprintf (buf+len, "    fifo status    : %03x %08x\n", _FRL1_FIFOSTAT, receiver->map1[ _FRL1_FIFOSTAT/4 ] );
    len += sprintf (buf+len, "    block  size    : %03x %08x\n", _FRL1_BLOCKSIZE, receiver->map1[ _FRL1_BLOCKSIZE/4 ] );
    len += sprintf (buf+len, "    header size    : %03x %08x\n", _FRL1_HEADERSIZE, receiver->map1[ _FRL1_HEADERSIZE/4 ] );
    len += sprintf (buf+len, "len : %d \n", len);

    *eof = 0;
    *start = (char *) 1;

    return len;
} 


/***********************************************************************/


int init_module (void)
{
    printk(KERN_INFO "Loading FRL driver version $Id: frl_kernel.c,v 1.5 2004/11/04 09:36:50 cschwick Exp $\n");
    printk(KERN_INFO "Compiled "__DATE__ " " __TIME__ "\n" );

    init_MUTEX (&kfrl_receivers_lock)/* = MUTEX*/;

    if (register_chrdev (FRL_MAJOR, "FRL", &kfrl_fops)) {
        printk (KERN_ERR "Failed to register FRL driver in init_module\n");
        return -EIO;
    }

    printk(KERN_INFO "Creating /proc/driver/frl entry\n" );
    create_proc_read_entry( "driver/frl",
			    0,
			    NULL,
			    kfrl_read_proc,
			    NULL );
    
    printk(KERN_INFO "init_module done\n" );
    return 0;
}


/***********************************************************************/

void cleanup_module (void)
{
    /* Extra safety : collect semaphores */
    remove_proc_entry("driver/frl", NULL);
    down (&kfrl_receivers_lock);
    unregister_chrdev (FRL_MAJOR, "FRL");
}



/***********************************************************************/

/** kfrl_nail_pages */
/* Remember: the memory is not administored by the kernel but is allocated
   from the Bigphys. Therefore here we must make sure that the virtual addresses
   which have been mapped to these memory blocks are not used by the kernel 
   at a later point in time! */
#ifdef _FRL_BIGPHYS_
void kfrl_nail_pages (void *kernel_address, int size) 
{
    /* we will nail all the pages */
    printk(KERN_INFO  "nailing pages of bigphys\n" );
    struct page * page;
    for (page = virt_to_page(kernel_address); 
         page <= virt_to_page(kernel_address + size -1); 
         page++) 
        mem_map_reserve(page); 
}
#else
void kfrl_nail_pages (void *kernel_address, int size) {}
#endif


/***********************************************************************/

/** kfrl_unnail_pages */
#ifdef _FRL_BIGPHYS_
void kfrl_unnail_pages (void *kernel_address, int size) 
{
    /* we will nail all the pages */
    printk(KERN_INFO  "unnailing pages of bigphys\n" );
    struct page * page;
    for (page = virt_to_page(kernel_address); 
         page <= virt_to_page(kernel_address + size -1); 
         page++) 
        mem_map_unreserve(page); 
}
#else
void kfrl_unnail_pages (void *kernel_address, int size) {}
#endif


/***********************************************************************/

/* register receiver : check if the receiver is already allocated, if not,
   look for device, allocate, and fill structures with relevant data */
int kfrl_register_receiver (int index, struct inode *inode_p, struct file * file_p)
{
    struct kfrl_receiver_t *receiver;
    int ichar,count;
    struct pci_dev         *tmpDevice;

    tmpDevice = NULL;
    
    idprintf ("In kfrl_register_receiver\n");
    /* critical section */
    down (&kfrl_receivers_lock);
    /* find the receiver in the list */
    for (receiver = kfrl_receivers; receiver != NULL; receiver=receiver->next) 
        {  
            if (receiver->index == index) break;
        }

    if (receiver != NULL) 
        {
            /* receiver was already allocated, sorry (EBUSY) */
            up (&kfrl_receivers_lock);
            return -EBUSY;
        }
    
    /* Allocate a new receiver */
    receiver = (struct kfrl_receiver_t *) kmalloc (sizeof (struct kfrl_receiver_t), GFP_KERNEL);
    if (receiver == NULL) 
        {
            /* could not allocate memory */
            up (&kfrl_receivers_lock);
            return -ENOMEM;
        }
    
    /* The device is not allocated. Let's find it */
    receiver->device = NULL;
    receiver->device1 = NULL;
    count = 0;

    do 
        {
            receiver->device  = pci_find_device(_FRL_VENDOR, _FRL_DEVICE, receiver->device);
            receiver->device1 = pci_find_device(_FRL_VENDOR, _FRL1_DEVICE, receiver->device1);
            if ( count++ == index ) break;
        } while ( NULL != receiver->device && NULL != receiver->device1);
      

    if (receiver->device == NULL || receiver->device1 == NULL) 
        {
            /* the required device could not be found */
            kfree (receiver);
            up (&kfrl_receivers_lock);
            return -ENODEV;
        }
    
    /* everything seems to be in order. Let's get out of the critical section */
    /* put the new receiver in first position of the receiver chain */
    receiver->index = index;
    ichar = snprintf (receiver->device_name, FRL_DEVNAME_SIZE, "frl_%d", receiver->index);
    /* for safety: snprinf does not put a 0 if the string is FRL_DEVNAME_SIZE long */
    receiver->device_name [FRL_DEVNAME_SIZE - 1] = '\0';
    receiver->next  = kfrl_receivers;
    kfrl_receivers  = receiver;

    /* now we can fill the blanks in the strucuture, and return a success */
    receiver->block_fifo   = NULL;
    receiver->blocks       = NULL;
    receiver->wc_fifo      = NULL;
    receiver->block_number = 0;
    receiver->block_size   = 0;
    receiver->blocksInSpy  = 0;

    pci_read_config_dword (receiver->device, 0x10, &(receiver->base_address0));
    pci_read_config_dword (receiver->device, 0x48, &(receiver->FPGA_version));

    pci_read_config_dword (receiver->device1, 0x10, &(receiver->base1_address0));
    pci_read_config_dword (receiver->device1, 0x48, &(receiver->FPGA1_version));
    
    /* map base addresses in the kernel */
    if ( receiver->base_address0 != 0 && receiver->base1_address0 != 0 ) 
        {
            receiver->map0 = ioremap_nocache (receiver->base_address0, _FRL_BAR0_range);
            if (receiver->map0 == NULL) 
                {
                    idprintf ("In kfrl_register_receiver : could not map in kernel base addresse 0 of Bridge FPGA\n");
                    idprintf ("BARS : 0x%08x maps : 0x%08x\n", receiver->base_address0, (int)receiver->map0);
                    kfree (receiver);
                    up (&kfrl_receivers_lock);
                    return -EIO;
                }
            receiver->map1 = ioremap_nocache (receiver->base1_address0, _FRL1_BAR0_range);
            if (receiver->map1 == NULL) 
                {
                    idprintf ("In kfrl_register_receiver : could not map in kernel base addresse 0 of FRL FPGA\n");
                    idprintf ("BARS : 0x%08x maps : 0x%08x\n", receiver->base1_address0, (int)receiver->map1);
	  
                    if (receiver->map0 != NULL)
                        iounmap (receiver->map0);
                    kfree (receiver);
                    up (&kfrl_receivers_lock);
                    return -EIO;
                }
        } else 
            {
                /* something went wrong when the base addresses where read */
                idprintf ("In kfrl_register_receiver : one of the base addresses could not be read from PCI config space\n");
                kfree (receiver);
                up (&kfrl_receivers_lock);
                return -EIO;
            }
    
    init_MUTEX (&(receiver->lock))/* = MUTEX*/;
    
    /* keep a handy pointer to the structure */
    file_p->private_data = receiver;
    MOD_INC_USE_COUNT;
    up (&kfrl_receivers_lock);
    return 0;
}




/***********************************************************************/

/* kfrl_open. Just handles board reservation with kfrl_close */ 
int kfrl_open (struct inode *inode_p, struct file * file_p)
{  
    int receiver_index = MINOR (inode_p->i_rdev);
    idprintf ("Will call kfrl_register_receiver for receiver %d\n", receiver_index);
    return kfrl_register_receiver (receiver_index, inode_p, file_p);
}


/***********************************************************************/

void kfrl_unregister_receiver (struct kfrl_receiver_t *receiver)
{
    struct kfrl_receiver_t **pp_receiver;
    int block_fifo_size;
    /* lock */
    down (&receiver->lock);

    /* deallocate stuff of the receiver */
    block_fifo_size = (receiver->block_number + 1) * sizeof (void *) + sizeof (struct _FRL_data_fifo);
    printk(KERN_INFO "unregister receiver\n");
    if (receiver->block_fifo != NULL) {
        kfrl_unnail_pages (receiver->block_fifo, block_fifo_size);
        bigphysarea_free_pages (receiver->block_fifo);
    }
    if (receiver->blocks != NULL) {
        kfrl_unnail_pages (receiver->blocks, receiver->block_size * receiver->block_number);
        bigphysarea_free_pages (receiver->blocks);
    }
    if (receiver->wc_fifo != NULL) {
        kfrl_unnail_pages (receiver->wc_fifo, sizeof (struct _FRL_wc_fifo));
        bigphysarea_free_pages (receiver->wc_fifo);
    }
    /* unmap the receiver's base addresses */
    if (receiver->map0 != NULL) {
        iounmap (receiver->map0);
    }
    /* remove the receiver from the receiver list */
    down (&kfrl_receivers_lock);
    for (pp_receiver = &kfrl_receivers;*pp_receiver != NULL;) {
        if (*pp_receiver == receiver) *pp_receiver = (*pp_receiver)->next;
        else pp_receiver = &(*pp_receiver)->next;
    }
    up (&kfrl_receivers_lock);
    /* free the receiver structure */
    kfree (receiver);
}




/***********************************************************************/

/** frl_ioctl_receiver : various control specific to the receiver
    board(mainly allcate/delloc bigphys memory */
int kfrl_ioctl_receiver (struct inode * inode_p, struct file *file_p, 
			 unsigned int cmd, unsigned long arg)
{
    struct kfrl_receiver_t *receiver = file_p->private_data;
    switch (cmd) {
        /* just sets the block size */
    case FRL_CONFIGURE_SPY:
        {
            struct _frl_spy_parameters params;
            /* lock the file */
            down (&receiver->lock);
            /* if the memories were already allocated, this change is not allowed anymore */
            if (receiver->block_fifo != NULL) {
                up (&receiver->lock);
                return -EPERM;
            }
            /* get the parameters */
            if (copy_from_user(&params, (void *)arg, sizeof (struct _frl_spy_parameters))) {
                up (&receiver->lock); 
                return -EFAULT;
            }
            /* Check the parameters and take note */
            if ( (params.block_size & 0xffff0007) != 0 ) {
                up (&receiver->lock);
                return -EINVAL;
            }
            receiver->block_size = params.block_size;
            break;
        }
    case FRL_SETGET_RECEIVER_PARAMS:
        {
            struct _frl_receiver_parameters params;
            /* lock the file */
            down (&receiver->lock);
            /* if the memories were already allocated, this change is not allowed anymore */
            if (receiver->block_fifo != NULL) {
                up (&receiver->lock);
                return -EPERM;
            }
            /* get the parameters */
            if (copy_from_user(&params, (void *)arg, sizeof (struct _frl_receiver_parameters))) {
                up (&receiver->lock); 
                return -EFAULT;
            }
            /* Check the parameters and take note */
            if (params.block_number <=0 || params.block_size <= 0) {
                up (&receiver->lock);
                return -EINVAL;
            }

            receiver->block_number = params.block_number;
            receiver->block_size = params.block_size;
            receiver->do_allocate_blocks = params.do_allocate_blocks;

            up (&receiver->lock);
            return 0;
            break;
        }
    case FRL_GET_RECEIVER_MEMBLOCKS:
        {
            struct _frl_memory_blocks mem_blocks;
            int block_fifo_size;
            /* lock the file */
            down (&receiver->lock);
            /* did we get the parameters yet ? */
            if (receiver->block_number == 0) {
                up (&receiver->lock);
                return -EPERM;
            }
            /* did we already allocate? */
            if (receiver->block_fifo != NULL) {
                up (&receiver->lock);
                return -EPERM;
            }
            /* allocate the bigphys buffers */
            block_fifo_size = (receiver->block_number + 1) * sizeof (void *) + sizeof (struct _FRL_data_fifo);
            receiver->block_fifo = bigphysarea_alloc_pages ((block_fifo_size-1) / PAGE_SIZE + 1, 0, GFP_KERNEL);
            receiver->wc_fifo = bigphysarea_alloc_pages ((sizeof (struct _FRL_wc_fifo) -1 ) / PAGE_SIZE + 1,
                                                         0, GFP_KERNEL);
            if (receiver->do_allocate_blocks) {
                printk( KERN_INFO "receiver should allocate blocks: do bigphys allocate\n" );
                receiver->blocks = (void*) bigphysarea_alloc_pages (( (receiver->block_size * receiver->block_number) - 1 ) / PAGE_SIZE + 1, 
                                                                    0, GFP_KERNEL);
#ifdef FRL_PAINT_BIGPHYS
                {
                    int i;
                    U32 * block = (U32 *)receiver->blocks;
                    for (i = 0; i < (receiver->block_size * receiver->block_number) ; i++) {
                        block[i] = virt_to_bus (&(block[i]));
                    }
                }
#endif /* def FRL_PAINT_BIGPHYS */
            } else {
                receiver->blocks = NULL;
            }
            /* check if anything went wrong */
            if (receiver->block_fifo == NULL || receiver->wc_fifo == NULL 
                || (receiver->do_allocate_blocks && receiver->blocks == NULL)) {
                if (receiver->block_fifo != NULL) bigphysarea_free_pages (receiver->block_fifo);
                if (receiver->blocks != NULL) bigphysarea_free_pages (receiver->blocks);
                if (receiver->wc_fifo != NULL) bigphysarea_free_pages (receiver->wc_fifo);
                up(&receiver->lock);
                return -ENOMEM;
            }
            /* do the pages nailing */
            kfrl_nail_pages (receiver->block_fifo, block_fifo_size);
            kfrl_nail_pages (receiver->wc_fifo, sizeof (struct _FRL_wc_fifo));
            if (receiver->do_allocate_blocks) 
                kfrl_nail_pages (receiver->blocks, receiver->block_size * receiver->block_number);
            /* prepare the data_fifo structure */
            receiver->data_fifo = (struct _FRL_data_fifo *) ((void *)receiver->block_fifo + 
                                                             (receiver->block_number + 1) * sizeof (void *));
            receiver->data_fifo_tab = receiver->block_fifo;
            receiver->data_fifo->read = 0;
            receiver->data_fifo->write = 0;
            receiver->data_fifo->done_read = 0;
            receiver->data_fifo->done_write = 0;
            receiver->data_fifo->size = (receiver->block_number + 1);
            /* buildup reply */
            mem_blocks.free_blocks_fifo_kernel = receiver->block_fifo;
            mem_blocks.free_blocks_fifo_physical = (void *)virt_to_bus (receiver->block_fifo);
            mem_blocks.wc_fifo_kernel = receiver->wc_fifo;
            mem_blocks.wc_fifo_physical = (void *)virt_to_bus (receiver->wc_fifo);
            if (receiver->do_allocate_blocks) {
                mem_blocks.data_blocks_kernel = receiver->blocks;
                mem_blocks.data_blocks_physical = (void *)virt_to_bus (receiver->blocks);
            } else {
                mem_blocks.data_blocks_kernel = NULL;
                mem_blocks.data_blocks_physical = NULL;
            }
            /* send reply */
            if (copy_to_user ((void *)arg, &mem_blocks, sizeof (struct _frl_memory_blocks))) {
                /* de allocate all the crap */
                printk(KERN_INFO "copy_to_user of mem_blocks went bad\n");
                if (receiver->block_fifo != NULL) {
                    kfrl_unnail_pages (receiver->block_fifo, block_fifo_size);
                    bigphysarea_free_pages (receiver->block_fifo);
                }
                if (receiver->blocks != NULL) {
                    kfrl_unnail_pages (receiver->blocks, receiver->block_size * receiver->block_number);
                    bigphysarea_free_pages (receiver->blocks);
                }
                if (receiver->wc_fifo != NULL) {
                    kfrl_unnail_pages (receiver->wc_fifo, sizeof (struct _FRL_wc_fifo));
                    bigphysarea_free_pages (receiver->wc_fifo);
                }
                up (&receiver->lock); 
                return -EFAULT;            
            }
            /* complete */
            up (&receiver->lock);
            return 0;
            break;
        }
    default:
        return -EINVAL;
    }
    return -EFAULT;
}



/***********************************************************************/

/** frl_ioctl : various controls for sender and reciever */
int kfrl_ioctl (struct inode * inode_p, struct file *file_p, 
                unsigned int cmd, unsigned long arg)
{
    struct kfrl_receiver_t *receiver = file_p->private_data;
    switch (cmd) {
    case FRL_GET_BOARD_ADDRESSES:
        {
            struct _frl_board_addresses reply = {0, 0, 0, 0};
      
            reply.base_address_0 = receiver->base_address0;
            reply.base1_address_0 = receiver->base1_address0;
            reply.FPGA_version   = receiver->FPGA_version;
            reply.FPGA1_version   = receiver->FPGA1_version;
            /* copy the reply to user sapce*/
            if (copy_to_user((void *)arg, &reply, sizeof (struct _frl_board_addresses)))
                return -EFAULT;
            return 0;
            break;
        }
    default:
        {
            return kfrl_ioctl_receiver (inode_p, file_p, cmd, arg);
        }
    }
    return -EFAULT;
}


/***********************************************************************/

/** kfrl_mmap : has to handle both bigphys areas and DMA
    buffers. Should be fine with physical address*/
int kfrl_mmap (struct file *file_p, struct vm_area_struct *vma)
{
    unsigned long offset = vma->vm_pgoff << PAGE_SHIFT;
  
#if LINUX_VERSION_CODE > KERNEL_VERSION(2,4,18) && RED_HAT_LINUX_KERNEL
    if (remap_page_range (vma, vma->vm_start, offset, 
                          vma->vm_end - vma->vm_start, vma->vm_page_prot))
#else
        if (remap_page_range (vma->vm_start, offset, 
                              vma->vm_end - vma->vm_start, vma->vm_page_prot))
#endif
            {
                eprintf ("kfrl_mmap: fail to remap start=0x%08lx, end=0x%08lx, ofset = 0x%08lx\n", 
                         vma->vm_start, vma->vm_end, offset);
                return -EAGAIN;
            }
    idprintf ("In kfrl_mmap_mmap : mapped successfully vma with :\n"
              "start  = 0x%08x\n"
              "offset = 0x%08x\n"
              "end =    0x%08x\n"
              "(size =  0x%08x)\n",
              (int)vma->vm_start, (int)offset, (int)vma->vm_end, 
              (int)(vma->vm_end-vma->vm_start));
    return (0);   /* happy */
}


/***********************************************************************/

/** frl release : close the file and deallocate everything */
int kfrl_release (struct inode *inode_p, struct file *file_p) 
{
    struct kfrl_receiver_t *receiver = file_p->private_data;
    
    idprintf ("kfrl_release\n");
    kfrl_unregister_receiver( receiver );
    MOD_DEC_USE_COUNT;
    return 0;
}
 

MODULE_LICENSE("GPL");
