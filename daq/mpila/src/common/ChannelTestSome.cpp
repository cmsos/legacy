/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTestSome.cpp
 *
 *  Created on: Mar 23, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/ChannelTestSome.hpp"

#include <vector>

namespace mpila
{
ChannelTestSome::ChannelTestSome(size_t pipelineDepth, WorkRequestType type)
    : ChannelTestMultiple(pipelineDepth, type),
      freeIndexList_(pipelineDepth_),
      pendingWorkRequests_(pipelineDepth_)
{
    freeIndexList_.setName("freeIndexList");
    for (size_t i = 0; i < pendingWorkRequests_.size(); i++)
    {
        freeIndexList_.push_back(i);
    }
}

rlist<size_t>& ChannelTestSome::getFreeIndexList()
{
    return freeIndexList_;
}

std::vector<WorkRequest>& ChannelTestSome::getPendingWorkRequests()
{
    return pendingWorkRequests_;
}

} /* namespace mpila */
