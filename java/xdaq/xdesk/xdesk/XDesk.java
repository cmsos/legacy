package xdaq.xdesk.xdesk;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.applet.*;
import java.io.*;

import org.apache.log4j.*;

import xdaq.xdesk.jfinder.*;
import xdaq.xdesk.tools.*;


public class XDesk extends JApplet implements ActionListener, InfoBusSubscriberListener, MenuBarListener
{
	static Logger logger = Logger.getLogger (XDesk.class);
	
	TaskSwitcher taskSwitcher_;

	//DeskletTableModel model_;
	
	JFinder finder_;
	
	//JTable table_;

	DeskletWindow w_;
	DeskletContext c_;
	
	InfoBusSubscriber deskletCommandSubscriber_;
	

	public XDesk ()
	{	
		
	}
	
	public JFrame getJFrame()
	{
		return (JFrame) w_;
	}
	
	public void init()
	{	
		taskSwitcher_ = new TaskSwitcher(this);
		
		c_ = (DeskletContext) this.getAppletContext();
		
		if (c_ == null)
		{
			logger.fatal ("Could not get applet context for xdesk");			
			// Should throw an exception here!!!			
			return;
		}
		
		
		
		/*
		model_ = new DeskletTableModel ( c_ );
		
		table_ = new JTable();
		table_.setModel (model_);
		table_.setShowHorizontalLines (false);
		table_.setShowVerticalLines (false);
		table_.setShowGrid (false);
		
		table_.addMouseListener ( new MouseAdapter() {
			public void mouseClicked (MouseEvent e) {
				int row = table_.getSelectedRow();
				int col = table_.getSelectedColumn();
				if ( (row==-1) || (col ==-1) ) return;
				else {				
					System.out.println ("Display row " + row );									
					displayDesklet ((String) model_.getValueAt(row,2));				
				}
			}
		});
		*/
		// 
		// Display JFinder
		//
		// default root is current directory
		String path = this.getParameter ("root");
		if (path == null) path = "./";
		
		File file = new File(path);
		FinderTreeNode rootNode = new FinderTreeNode (file);
		FinderModel m = new FinderModel (rootNode);
		finder_ = new JFinder();
		finder_.setRenderer ( new XDeskRenderer (c_) );
		finder_.setActionListener (this);
		finder_.setModel(m);
		
		JScrollPane scrollPane = new JScrollPane (finder_, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		w_ = new DeskletWindow();
		w_.getContentPane().add ( new XDeskMenuBar( this), BorderLayout.NORTH );
		w_.getContentPane().add (scrollPane, BorderLayout.CENTER);
		w_.setSize (this.getWidth(), this.getHeight());
		w_.setVisible(true);
		

		WindowListener l = new WindowAdapter () 
		{
			public void windowClosing (WindowEvent e) 
			{
				exitAction();
			}
		};
		w_.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);		
		w_.addWindowListener (l);
		
		
		// Don't know if still needed???		
		//this.fireTableChanged();
		
		deskletCommandSubscriber_ = new InfoBusSubscriber ("framework");
		deskletCommandSubscriber_.subscribeItem ("command", this );
		
		taskSwitcher_.addTask(c_.getStub("xdesk"));
		
	}

	// --------------------------------------------------------------------	
	
	
	/*
	public void showDesklet (DeskletStub stub)
	{
		if (stub.isActive()) return;
		else
		{
			stub.setActive (true);
			JApplet a = stub.getApplet();	
			JFrame frame = new JFrame (a.getName());
			frame.getContentPane().add (a);
			//frame.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
			frame.addWindowListener (new CloseDeskletListener ( stub ) );

			// Get original width and heigt
			String width = stub.getParameter("width");
			String height = stub.getParameter("height");
			Integer w = new Integer(width);
			Integer h = new Integer (height);

			//frame.setSize (a.getWidth(), a.getHeight());
			frame.setSize (w.intValue(), h.intValue());
			frame.show();
		}		
	}
	*/

	
	// ----------------------------------------------------------------------
	// ActionListener interface implementation
	// ----------------------------------------------------------------------
	
	public void actionPerformed (ActionEvent e)
	{
		if (e.getActionCommand().equals ("open") )
		{
			// Get the file to open from the selection model
			FinderTreeNode node = finder_.getSelectedFinderTreeNode();
			File file = node.getFile();
			
			// Check if the desklet is alreay loaded
			
			try {
				String canonicalPath = file.getCanonicalPath();

				String name = c_.getAppletName(canonicalPath);
				if (name == null)
				{
					// Create applet
					DeskletStub stub = c_.loadDesklet (canonicalPath);
					Applet a = stub.getApplet();
					
					//MenuBar menuBar = new MenuBar(this,a);
					//a.setMenuBar ( menuBar );
					
					a.start();
					
					taskSwitcher_.addTask (stub);
					
					// Fire change to trigger rendering
					finder_.getModel().nodeChanged (node);
									
				} else 
				{
					// Trigger existing applet
					Applet a = c_.getApplet(name);
					a.start();
				} 
			} catch (IOException ioe)
			{
				logger.error ("Could not open applet: " + ioe.toString());
			}
		}		
	}
	
	public void forceQuit (DeskletStub stub)
	{
		c_.unloadDesklet (stub.toString());
		taskSwitcher_.removeTask (stub);
		finder_.refresh();
	}
	
	public void destroy()
	{
		this.exitAction();
	}
	
	public void goTo (DeskletStub stub)
	{
		stub.getApplet().start();
	}
		
	// Publisher Subscriber interface
	public synchronized void  update (Object data, Object source)
	{
			Command command = (Command)data;
			Applet applet = command.getApplet();
			String appletName = applet.getName();
			
			if ( command.getName().equals("quit") )
			{
				// applet must be removed from registry
				//c_.unloadDesklet(appletName);
				// Fire change to trigger rendering
				//finder_.refresh();
				
				this.forceQuit ( c_.getStub(appletName) );
				
			} 
			else if ( command.getName().equals("exit") )
			{
				// Exit the program.
				this.exitAction();
				//WindowEvent event = new WindowEvent (w_, WindowEvent.WINDOW_CLOSING);
				//w_.dispatchEvent(event);
				//w_.dispose();
			
			} 
			else if (command.getName().equals("tasks") )
			{
				// get all running daqlets from the registry and show task manager dialog
				this.switchAction();
			}
			else
			{
				logger.error("framework command unknown");
			
			}
  	}	
	
	
	public void aboutAction()
	{
	}
	
	public void switchAction()
	{
		taskSwitcher_.setVisible(true);
	}
	
	public void exitAction()
	{
	
		int confirm = JOptionPane.showOptionDialog (w_, "Do you really want to exit?",
			"Confirmation",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null, null, null);
		if (confirm == 0)
		{
			logger.info ("Shutdown program XDesk");
			w_.dispose();
			System.exit(0);
		}
	
	}
	
	public void quitAction()
	{
		this.destroy();
	}
		
}	
