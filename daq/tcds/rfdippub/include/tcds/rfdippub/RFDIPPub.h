#ifndef _tcds_rfdippub_RFDIPPub_h_
#define _tcds_rfdippub_RFDIPPub_h_

#include "tcds/utils/XDAQAppBase.h"

namespace xdaq {
  class ApplicationStub;
  class ApplicationDescriptor;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace rfdippub {

    class RF2TTCDataInfoSpaceHandler;
    class RF2TTCDataInfoSpaceUpdater;
    class RFRXDDataInfoSpaceHandler;
    class RFRXDDataInfoSpaceUpdater;

    class RFDIPPub : public tcds::utils::XDAQAppBase
    {

    public:
      XDAQ_INSTANTIATOR();

      RFDIPPub(xdaq::ApplicationStub* const stub);
      virtual ~RFDIPPub();

    protected:
      virtual void setupInfoSpaces();

      // Dummy methods in this case.
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::auto_ptr<RF2TTCDataInfoSpaceUpdater> rf2ttcInfoSpaceUpdaterP_;
      std::auto_ptr<RF2TTCDataInfoSpaceHandler> rf2ttcInfoSpaceP_;
      std::auto_ptr<RFRXDDataInfoSpaceUpdater> rfrxdInfoSpaceUpdaterP_;
      std::auto_ptr<RFRXDDataInfoSpaceHandler> rfrxdInfoSpaceP_;

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RFDIPPub_h_
