###############################################################################

# The addresses of the boards/applications to use in this test.

# One has to choose a TCDS location/setup to use:
# - The production system.
# mode = "tcdsp5"
# - The DAQ2Val.
mode = "tcdsdv"

ALL_CPMS = {
    "tcdsp5": "cpm-pri",
    "tcdsdv": "cpm-dv"
}

ALL_ICIS = {
    "tcdsp5": [
        "ici-bcmutca-pri",
        "ici-bcmutca-sec",
        "ici-bcmvme-pri",
        "ici-bcmvme-sec",
        "ici-bhm-pri",
        "ici-bhm-sec",
        "ici-bpixm-pri",
        "ici-bpixm-sec",
        "ici-bpixp-pri",
        "ici-bpixp-sec",
        "ici-calstage1-pri",
        "ici-calstage1-sec",
        "ici-caltrigup-pri",
        "ici-caltrigup-sec",
        "ici-castor-pri",
        "ici-castor-sec",
        "ici-cscm-pri",
        "ici-cscm-sec",
        "ici-cscp-pri",
        "ici-cscp-sec",
        "ici-ctpps-pri",
        "ici-ctpps-sec",
        "ici-dt0-pri",
        "ici-dt0-sec",
        "ici-dtm-pri",
        "ici-dtm-sec",
        "ici-dtp-pri",
        "ici-dtp-sec",
        "ici-dtup-pri",
        "ici-dtup-sec",
        "ici-ebm-pri",
        "ici-ebm-sec",
        "ici-ebp-pri",
        "ici-ebp-sec",
        "ici-eem-pri",
        "ici-eem-sec",
        "ici-eep-pri",
        "ici-eep-sec",
        "ici-esm-pri",
        "ici-esm-sec",
        "ici-esp-pri",
        "ici-esp-sec",
        "ici-fpixm-pri",
        "ici-fpixm-sec",
        "ici-fpixp-pri",
        "ici-fpixp-sec",
        "ici-gempilot-pri",
        "ici-gempilot-sec",
        "ici-gtup-pri",
        "ici-gtup-sec",
        "ici-gtupspare-pri",
        "ici-gtupspare-sec",
        "ici-hbhea-pri",
        "ici-hbhea-sec",
        "ici-hbheb-pri",
        "ici-hbheb-sec",
        "ici-hbhec-pri",
        "ici-hbhec-sec",
        "ici-hcallaser-pri",
        "ici-hcallaser-sec",
        "ici-hf-pri",
        "ici-hf-sec",
        "ici-ho-pri",
        "ici-ho-sec",
        "ici-milliqan-pri",
        "ici-milliqan-sec",
        "ici-mutfup-pri",
        "ici-mutfup-sec",
        "ici-plt-pri",
        "ici-plt-sec",
        "ici-rct-pri",
        "ici-rct-sec",
        "ici-rpc-pri",
        "ici-rpc-sec",
        "ici-scal-pri",
        "ici-scal-sec",
        "ici-tecm-pri",
        "ici-tecm-sec",
        "ici-tecp-pri",
        "ici-tecp-sec",
        "ici-tibtid-pri",
        "ici-tibtid-sec",
        "ici-tob-pri",
        "ici-tob-sec",
        "ici-totdet-pri",
        "ici-totdet-sec",
        "ici-tottrg-pri",
        "ici-tottrg-sec",
        "ici-twinmux-pri",
        "ici-twinmux-sec"
    ],
    "tcdsdv": [
        "ici-dv11",
        "ici-dv12",
        "ici-dv13",
        "ici-dv17",
        "ici-dv18"
    ]
}

ALL_APVES = {
    "tcdsp5": [
        'apve-tecm-pri',
        'apve-tecp-pri',
        'apve-tibtid-pri',
        'apve-tob-pri'
    ],
    "tcdsdv": []
}

ALL_PIS = {
    "tcdsp5": [
        "pi-bcmutca-pri",
        "pi-bcmvme-pri",
        "pi-bhm-pri",
        "pi-bpixm-pri",
        "pi-bpixp-pri",
        "pi-calstage1-pri",
        "pi-caltrigup-pri",
        "pi-castor-pri",
        "pi-cscm-pri",
        "pi-cscp-pri",
        "pi-ctpps-pri",
        "pi-dt0-pri",
        "pi-dtm1-pri",
        "pi-dtm2-pri",
        "pi-dtp1-pri",
        "pi-dtp2-pri",
        "pi-dtup-pri",
        "pi-ebm-pri",
        "pi-ebp-pri",
        "pi-eem-pri",
        "pi-eep-pri",
        "pi-esm-pri",
        "pi-esp-pri",
        "pi-fpixm-pri",
        "pi-fpixp-pri",
        "pi-gempilot-pri",
        "pi-gtup-pri",
        "pi-gtupspare-pri",
        "pi-hbhea-pri",
        "pi-hbheb-pri",
        "pi-hbhec-pri",
        "pi-hcallaser-pri",
        "pi-hf-pri",
        "pi-ho-pri",
        "pi-milliqan-pri",
        "pi-mutfup-pri",
        "pi-plt-pri",
        "pi-rct-pri",
        "pi-rpc1-pri",
        "pi-rpc2-pri",
        "pi-scal-pri",
        "pi-tecm-pri",
        "pi-tecp-pri",
        "pi-tibtid-pri",
        "pi-tob-pri",
        "pi-totdet-pri",
        "pi-tottrg-pri",
        "pi-twinmux-pri"
    ],
    "tcdsdv": [
        "pi-dv11",
        "pi-dv12",
        "pi-dv13",
        "pi-dv17",
        "pi-dv18"
    ]
}

CPM = ALL_CPMS[mode]
ICIS = ALL_ICIS[mode]
APVES = ALL_APVES[mode]
PIS = ALL_PIS[mode]

# The FED id of the CPM, so we can disable the readout.
FED_ID_CPM = 1024

###############################################################################
