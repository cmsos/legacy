package xmltreeeditor;

import java.io.*;

public class XMLFileFilter
    extends javax.swing.filechooser.FileFilter {
  public boolean accept(File f) {
    boolean accept = f.isDirectory();
    if (!accept) {
      String suffix = getSuffix(f);
      if (suffix != null) {
        accept = suffix.equals("xml");
      }
    }
    return accept;
  }

  public String getDescription() {
    return "XML Files (*.xml)";
  }

  private String getSuffix(File f) {
    String s = f.getPath(), suffix = null;
    int i = s.lastIndexOf('.');
    if (i > 0 && i < s.length() - 1) {
      suffix = s.substring(i + 1).toLowerCase();
    }
    return suffix;
  }
}
