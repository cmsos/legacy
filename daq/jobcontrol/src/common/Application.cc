// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jobcontrol/Application.h"
#include "jobcontrol/exception/JobCrash.h"
#include "jobcontrol/ProcessTree.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/InfoSpace.h"
#include "xdata/ItemGroupEvent.h"

#include "toolbox/string.h"
#include "toolbox/net/UUID.h"
#include "toolbox/Groups.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 
#include "xgi/framework/Method.h" 

#include <netdb.h>
#include <sstream>

XDAQ_INSTANTIATOR_IMPL(jobcontrol::Application)

/*
 * The signal handler function -- only gets called when a SIGCHLD
 * is received, ie when a child terminates
 */
void sig_chld(int signo) 
{
	int status, child_val;

	/* Wait for any child without blocking */
	if (waitpid(-1, &status, WNOHANG) < 0)
	{
		/*
		 * calling standard I/O functions like fprintf() in a
		 * signal handler is not recommended, but probably OK
		 * in toy programs like this one.
		 */
		fprintf(stderr, "waitpid failed\n");
		return;
	}

	/*
	 * We now have the info in 'status' and can manipulate it using
	 * the macros in wait.h.
	 */
	if (WIFEXITED(status))                /* did child exit normally? */
	{
		child_val = WEXITSTATUS(status); /* get child's exit status */
		printf("child's exited normally with status %d\n", child_val);
	}
	else 
	{
		std::cout << "SIGNALLED: " << WIFSIGNALED(status) << ", sig: " << WTERMSIG(status) << std::endl;
		std::cout << "Stopped: " << WIFSTOPPED(status)  << ", sig: " << WSTOPSIG(status) << std::endl;
		std::cout << "Exited: " << WIFEXITED(status) << ", status: " << WEXITSTATUS(status) << std::endl;
	}
}

//
// -------------------------------------------------------------

jobcontrol::Application::Application (xdaq::ApplicationStub *s) 
					: xdaq::Application(s), xgi::framework::UIManager(this), mutex_(toolbox::BSem::FULL)
{
	_exeCmdDatList =  toolbox::rlist<_ExecuteCommandData>::create("ExecuteCommandDataList");
	_exeWatchDogList = toolbox::rlist<_ExecuteCommandData>::create("ExecuteWatchDogList");

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Constructor called.");

	appInfoSpace_  = getApplicationInfoSpace();

	toolbox::net::URN urn = this->createQualifiedInfoSpace("jobcontrol");
	monitorInfoSpace_ = xdata::getInfoSpaceFactory()->get(urn.toString());

	// initialize instance variables
	_jid    = "";
	_intJid = 0;

	_numberOfTryForMyKillDescendants = 10;
	_sleepBetweenTryForMyKillDescendants = 100000;

	// constants
	watchdogName = "watchdog";
	start        = "start";
	stop         = "stop";
	watchdog     = "watchdog";

	s->getDescriptor()->setAttribute("icon16x16", "/jobcontrol/images/jobcontrol-icon.png");
	// set the jobcontrol logo
	s->getDescriptor()->setAttribute("icon","/jobcontrol/images/jobcontrol-icon.png");

	// get environment variable JC_ROOT_ALLOWED to see whether we should be
	// able to start processes under root
	char * cbuffer;
	std::string buffer = "";

	cbuffer = getenv ("JC_ROOT_ALLOWED");
	if (cbuffer!=NULL) buffer = cbuffer;
	if ( buffer=="YES" || buffer=="yes" || buffer=="Yes" )
	{
		_rootAllowed = true;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Processes can be started under root account.");
	}
	else
	{
		_rootAllowed = false;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Processes can not be started under root account.");
	}

	// get the environment variable JC_USERS_ALLOWED
	// if not set or empty all users are allowed
	cbuffer = getenv ("JC_USERS_ALLOWED");
	if (cbuffer!=NULL) _usersAllowed = cbuffer;
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Allowed users list set to " + _usersAllowed);

	// Bind SOAP callbacks for control messages
	xoap::bind (this, &jobcontrol::Application::getState, "getState", XDAQ_NS_URI);
	xoap::bind (this, &jobcontrol::Application::executeCommand, "executeCommand", XDAQ_NS_URI);
	xoap::bind (this, &jobcontrol::Application::startXdaqExe, "startXdaqExe", XDAQ_NS_URI);
	xoap::bind (this, &jobcontrol::Application::killAll, "killAll", XDAQ_NS_URI);
	xoap::bind (this, &jobcontrol::Application::killExec, "killExec", XDAQ_NS_URI);
	xoap::bind (this, &jobcontrol::Application::getJobStatus, "getJobStatus", XDAQ_NS_URI);

	// Bind default web pagex
	xgi::framework::deferredbind(this, this, &jobcontrol::Application::defaultWebPage, "Default");
	xgi::framework::deferredbind(this, this, &jobcontrol::Application::processKillWebPage, "ProcKill");
	xgi::framework::deferredbind(this, this, &jobcontrol::Application::processKillAllWebPage, "ProcKillAll");

	xgi::bind(this, &jobcontrol::Application::getLogFile, "getLogFile");
	//xgi::framework::deferredbind(this, this, &jobcontrol::Application::processLogWebPage, "ProcLog");

	// export parameters
	exportParams();

	// allow unlimited core files
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Set core files to unlimited.");
	struct rlimit rl;
	getrlimit (RLIMIT_CORE, &rl);

	rl.rlim_cur = RLIM_INFINITY;
	int ret = setrlimit (RLIMIT_CORE, &rl);

	if (ret)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
				("jobControl: Could not setrlimit ret = %i , no core files can be seen.",ret) );
		perror ("setrlimit");
	}


	// Set up sigchld handler
	//
	//	struct sigaction act;

	/* Assign sig_chld as our SIGCHLD handler */
	//act.sa_handler = sig_chld;

	/* We don't want to block any other signals in this example */
	//sigemptyset(&act.sa_mask);

	/*
	 * We're only interested in children that have terminated, not ones
	 * which have been stopped (eg user pressing control-Z at terminal)
	 */
	//act.sa_flags = SA_NOCLDSTOP;

	/*
	 * Make these values effective. If we were writing a real
	 * application, we would probably save the old value instead of
	 * passing NULL.
	 */
	//if (sigaction(SIGCHLD, &act, NULL) < 0)
	//{
	//   fprintf(stderr, "sigaction failed\n");
	//}


	//
	// spawn watchdog timer
	//

	watchdogTimer_ = toolbox::task::getTimerFactory()->createTimer(watchdogName);

	watchdogEnabled_ = true;

	toolbox::TimeInterval watchdogInterval(5,0); // period of 5 secs

	toolbox::TimeVal watchdogStart;

	watchdogStart = toolbox::TimeVal::gettimeofday();

	watchdogTimer_->scheduleAtFixedRate( watchdogStart, this, watchdogInterval,  0, watchdogName );

	//
	// start work loop
	//


	// Get a work loop
	workerLoop_ =
			toolbox::task::getWorkLoopFactory()->getWorkLoop("WaitingWorkLoop", "waiting");

	workerLoopActionSignature_ =
			toolbox::task::bind (this, &jobcontrol::Application::workerLoopJob, "workerLoopJob");

	workerLoop_->activate();

	workerLoop_->submit( workerLoopActionSignature_ );

	settings_.setProperty("autorefresh", "0");
}



bool jobcontrol::Application::workerLoopJob(toolbox::task::WorkLoop* wl)
{

	// take
	mutex_.take();

	try
	{

		if (_exeCmdDatList->empty())
		{
			if (!_exeWatchDogList->empty())
			{
				executeCommandInLoop(_exeWatchDogList->front());
				// Clean the rlist of watchDog
				while (!_exeWatchDogList->empty())
				{
					try
					{
						// remove first element
						_exeWatchDogList->pop_front();
					}
					catch (toolbox::exception::RingListEmpty &e)
					{
						const std::string msg =
								"Exception in workerLoopJob: the _exeWatchDogList is empty.\n";
						LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
								<< e.what());
					}
				}
			}
		}
		else
		{

			//
			// jobs to start
			//
			executeCommandInLoop(_exeCmdDatList->front());

			try
			{
				// remove first element
				_exeCmdDatList->pop_front();
			}
			catch (toolbox::exception::RingListEmpty &e)
			{
				const std::string msg =
						"Exception in workerLoopJob: the _exeCmdDatList is empty.\n";
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
			}

		}

	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Exception in workerLoopJob.\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
				<< xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		const std::string msg = "Exception in workerLoopJob.\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(),
				"Unknown Exception in workerLoopJob.");
	}

	// release
	mutex_.give();

	return false;
}


void jobcontrol::Application::doWatchdog () 
{

	try
	{
		std::string logString;
		logString = "doWatchdog() called";
		logString += ", watchdogEnabled_=";
		if (watchdogEnabled_)
			logString += "true";
		else
			logString += "false";

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), logString);

		// check whether watchdog should do something
		if (!watchdogEnabled_) return;

		char filename[1024];

		// loop over all started processes
		// iterator over all started processes
		std::list<_PidUid>::iterator iter = _pidUidList.begin();

		// get the process tree to find child processes
		ProcessTree pt;
		try
		{
			pt.takeSnapshot();
		}
		catch (xcept::Exception &e)
		{
			const std::string msg = "Exception watchdog() pt.takeSnapshot().\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
			return;
		}
		catch (std::exception &e)
		{
			const std::string msg = "Exception watchdog() pt.takeSnapshot().\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
			return;
		}
		catch (...)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(),
					"Exception watchdog() pt.takeSnapshot().\n");
			return;
		}

		for ( ; iter != _pidUidList.end(); iter++)
		{
			if (!(*iter)._notified)
			{

				sprintf(filename,"/proc/%i/stat", (*iter)._pid );

				std::string str = "" ;
				try
				{
					std::fstream file_op(filename, std::ios::in);
					char tmpstr[2000];

					while(file_op >> tmpstr)
					{

						str += tmpstr;
						str += " ";

					}
					file_op.close();
				}
				catch (xcept::Exception &e)
				{
					const std::string msg = "Exception watchdog() instd::fstream file_op.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
					continue;
				}
				catch (std::exception &e)
				{
					const std::string msg = "Exception watchdog() instd::fstream file_op.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
					continue;
				}
				catch (...)
				{
					LOG4CPLUS_ERROR(this->getApplicationLogger(),
							"Exception watchdog() instd::fstream file_op.\n");
					continue;
				}

				unsigned long dummy;
				unsigned long sig_catch;
				long ddummy;
				char sdummy[100];

				int status = 0;
				int retWait = 0;
				int errnoSave = 0;
				int ret = 0;
				try
				{
					ret = sscanf(str.c_str(),"%ld %s %c %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %ld %ld %ld %ld %ld %ld %lu %lu %ld %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %ld %ld\n",
							&dummy, //task->pid,
							sdummy, //task->comm,
							&( (*iter)._state ), //state
							&dummy, //ppid,
							&dummy, //task->pgrp,
							&dummy, //task->session,
							&dummy, //tty_nr,
							&dummy, //tty_pgrp,

							&dummy, //task->flags,
							&dummy, //task->min_flt,
							&dummy, //task->cmin_flt,
							&dummy, //task->maj_flt,
							&dummy, //task->cmaj_flt,
							&dummy, //task->times.tms_utime,
							&dummy, //task->times.tms_stime,
							&ddummy, //task->times.tms_cutime,
							&ddummy, //task->times.tms_cstime,
							&ddummy, //priority,
							&ddummy, //nice,
							&ddummy, //0UL /* removed */,
							&ddummy, //task->it_real_value,
							&dummy, //task->start_time,
							&(*iter)._vsize, //vsize,
							&dummy, //mm ? mm->rss : 0, /* you might want to shift this left 3 */
							&dummy, //task->rlim[RLIMIT_RSS].rlim_cur,
							&dummy, //mm ? mm->start_code : 0,
							&dummy, //mm ? mm->end_code : 0,
							&dummy, //mm ? mm->start_stack : 0,
							&dummy, //esp,
							&dummy, //eip,
							/* The signal information here is obsolete.
							 * It must be decimal for Linux 2.0 compatibility.
							 * Use /proc/#/status for real-time signals.
							 */
							&dummy, //task->pending.signal.sig[0],
							&dummy, //task->blocked.sig[0],
							&dummy, //sigign      .sig[0],
							&sig_catch, //sigcatch    .sig[0],
							&dummy, //wchan,
							&dummy, //task->nswap,
							&dummy, //task->cnswap,
							&ddummy, //task->exit_signal,
							&ddummy  //task->processor
					);

					retWait = ::waitpid( (*iter)._pid, &status, WNOHANG|WUNTRACED); //collect status info
					errnoSave = errno;
				}
				catch (xcept::Exception &e)
				{
					const std::string msg = "Exception watchdog() retWaitpid.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
					continue;
				}
				catch (std::exception &e)
				{
					const std::string msg = "Exception watchdog() retWaitpid.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
					continue;
				}
				catch (...)
				{
					LOG4CPLUS_ERROR(this->getApplicationLogger(),
							"Exception watchdog() retWaitpid.\n");
					continue;
				}

				//std::cout << "Retait: " << retWait << " PROC: " << (*iter)._pid << ", Signalled: " << WIFSIGNALED(status) << ", stopped: " << WIFSTOPPED(status) << ", existed: " << WIFEXITED(status) << ", sig catch: " << sig_catch << std::endl;
				//std::cout << "Retait: " << retWait <<  "PROC: " << (*iter)._pid << "waitpid retval: " << retWait << "sig: " << WTERMSIG(status) << ", stopped: " << WSTOPSIG(status) << ", term: " << WEXITSTATUS(status) << std::endl;

				// log message
				pid_t mypid = getpid();
				std::stringstream oss;
				std::string       s;
				oss << "Called waitpid() on PID=";
				oss << (*iter)._pid;
				oss << ", my pid=";
				oss << mypid;
				oss << ", retWait=";
				oss << retWait;
				oss << ", status=";
				oss << status;
				oss << ", state=";
				oss << (*iter)._state;
				oss << ", errno=";
				oss << strerror(errnoSave);

				s = oss.str();
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), s);


				// check waitpid() return
				if (retWait == -1 )
				{
					// log message
					std::stringstream oss;
					std::string       s;
					oss << "Failed to call waitpid() on PID=";
					oss << (*iter)._pid;
					oss << ", my pid=";
					oss << mypid;
					oss << ", errno=";
					oss << strerror(errnoSave);
					s = oss.str();
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), s);
				}


				// copy status to pid structure success
				if ( (retWait == (*iter)._pid) && (ret != -1) )
				{
					(*iter)._status = status;
				}

				// check for crashed processes
				if (
						!(*iter)._notified &&
						retWait > 0 &&
						(
								WIFEXITED(status) == 0           // did not exit gracefully
								|| WEXITSTATUS(status) != 0      // did exit with wrong error code
								|| WIFSIGNALED(status) != 0      // exited because of an uncaught signal?
						)
				)
				{


					// log message
					std::stringstream oss;
					std::string       s;
					oss << "Detected crashed proces. PID=";
					oss << (*iter)._pid;
					oss << ", status=";
					oss << status;
					oss << ", WIFSIGNALED(status)=";
					oss << WIFSIGNALED(status);
					oss << ", waitpid()=";
					oss << retWait;
					oss << ", notifierURL=";
					oss << (*iter)._notifierURL;
					oss << ", jid=" << (*iter)._jid;
					s = oss.str();
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), s);

					(*iter)._notified = true;                             // flag notified true to avoid multiple notifications

					//-- notify monitoring data change

					this->updateJobTable();
					std::list<std::string> names;
					names.push_back("jobTable");

					try
					{
						monitorInfoSpace_->fireItemGroupChanged(names, this);
					}
					catch (xdata::exception::Exception &e)
					{
						const std::string msg =
								"Caught Exception in monitorInfoSpace_->fireItemGroupChanged on doWatchdog().\n";
						LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
								<< xcept::stdformat_exception_history(e));
					}

					std::stringstream msg;
					msg << "Job '" << (*iter)._jid << "' terminated unexpectedly";
					XCEPT_DECLARE(jobcontrol::exception::JobCrash, e,  msg.str() );

					// Qualify the exception
					//
					e.setProperty("tag", (*iter)._tag);
					// String values
					e.setProperty("urn:xdaq-jobcontrol:childTree", (*iter)._child_tree);
					e.setProperty("urn:xdaq-jobcontrol:configFilePath", (*iter)._configFilePath);
					e.setProperty("urn:xdaq-jobcontrol:notifierURL", (*iter)._notifierURL);
					e.setProperty("urn:xdaq-jobcontrol:jid", (*iter)._jid);
					e.setProperty("urn:xdaq-jobcontrol:user", (*iter)._user);
					e.setProperty("urn:xdaq-jobcontrol:sexe", (*iter)._sexe);
					e.setProperty("urn:xdaq-jobcontrol:state", toolbox::toString("%d",(*iter)._state));

					// Numeric values
					e.setProperty("urn:xdaq-jobcontrol:pid", toolbox::toString("%d",(*iter)._pid));
					e.setProperty("urn:xdaq-jobcontrol:uid", toolbox::toString("%d",(*iter)._uid));
					e.setProperty("urn:xdaq-jobcontrol:port", toolbox::toString("%d",(*iter)._port));

					if (WIFSIGNALED(status))
					{
						e.setProperty("urn:xdaq-jobcontrol:reason", "terminated");
						e.setProperty("urn:xdaq-jobcontrol:status", toolbox::toString("%d",WTERMSIG(status)));
					}
					else if (WIFSTOPPED(status))
					{
						e.setProperty("urn:xdaq-jobcontrol:reason", "stopped");
						e.setProperty("urn:xdaq-jobcontrol:status", toolbox::toString("%d",WSTOPSIG(status)));
					}
					else if (WIFEXITED(status))
					{
						e.setProperty("urn:xdaq-jobcontrol:reason", "exited");
						e.setProperty("urn:xdaq-jobcontrol:status", toolbox::toString("%d",WEXITSTATUS(status)));
					}
					else
					{
						e.setProperty("urn:xdaq-jobcontrol:reason", "unknown");
						e.setProperty("urn:xdaq-jobcontrol:status", toolbox::toString("%d",status));
					}

					e.setProperty("urn:xdaq-jobcontrol:notified", toolbox::toString("%d",(*iter)._notified));
					e.setProperty("urn:xdaq-jobcontrol:vsize", toolbox::toString("%d",(*iter)._vsize));
					e.setProperty("urn:xdaq-jobcontrol:startTime", toolbox::toString("%d",(*iter)._start_time));
					e.setProperty("urn:xdaq-jobcontrol:NumberChildProc", toolbox::toString("%d",(*iter)._n_child_proc));

					this->notifyQualified("error", e );

					if ((*iter)._notifierURL != "")
					{               // check if notifierURL is defined

						try
						{
							//-- notify
							xoap::MessageReference soapMsg =
									createCrashNotificationMsg( (*iter)._jid, WIFSIGNALED(status) );

							xoap::MessageReference msg =
									xdaq2rc::RcmsStateNotifier::createStateNotificationMsg( (*iter)._execURL, "RCMSStateListener",
											0, "Crashed", strsignal( (*iter)._status ), true, 0, ::time(0), 0 );
							postSOAP( msg, (*iter)._notifierURL );
						}
						catch (xoap::exception::Exception &e)
						{
							std::stringstream oss;
							std::string s;
							oss << "Failed to send crash notification message to RCMS";
							oss << " Notifier url: ";
							oss << (*iter)._notifierURL;
							oss << " jid: " << (*iter)._jid;
							s = oss.str();
							LOG4CPLUS_WARN(this->getApplicationLogger(), s << xcept::stdformat_exception_history(e));
						}
						catch (xcept::Exception &e)
						{
							std::stringstream oss;
							std::string       s;
							oss << "Failed to send crash notification message to RCMS";
							oss << " Notifier url: ";
							oss << (*iter)._notifierURL;
							oss << " jid: " << (*iter)._jid;
							s = oss.str();
							LOG4CPLUS_WARN(this->getApplicationLogger(), s<< xcept::stdformat_exception_history(e));
						}
						catch (std::exception &e)
						{
							std::stringstream oss;
							std::string       s;
							oss << "Failed to send crash notification message to RCMS";
							oss << " Notifier url: ";
							oss << (*iter)._notifierURL;
							oss << " jid: " << (*iter)._jid;
							s = oss.str();
							LOG4CPLUS_WARN(this->getApplicationLogger(), s<< e.what());
						}
						catch (...)
						{
							std::stringstream oss;
							std::string       s;
							oss << "Failed to send crash notification message to RCMS";
							oss << " Notifier url: ";
							oss << (*iter)._notifierURL;
							oss << " jid: " << (*iter)._jid;
							s = oss.str();
							LOG4CPLUS_WARN(this->getApplicationLogger(), s);
						}
					}
				}
				else
				{
					try
					{
						std::list<int>  descendants = pt.getDescendantPIDs( (*iter)._pid );
						(*iter)._n_child_proc = descendants.size();
						(*iter)._child_tree   = pt.getDescendantTree( (*iter)._pid );
						(*iter)._children_list =  pt.getDescendantPIDs( (*iter)._pid );

					}
					catch (xcept::Exception &e)
					{
						const std::string msg = "Exception watchdog() pt.getDescendantPIDs.\n";
						LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
					}
					catch (std::exception &e)
					{
						const std::string msg =
								"Exception watchdog() pt.getDescendantPIDs.\n";
						LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
								<< e.what());
					}
					catch (...)
					{
						LOG4CPLUS_ERROR(this->getApplicationLogger(),
								"Exception watchdog() pt.getDescendantPIDs.\n");
					}
				}

			}
		}

	}
	catch (xdata::exception::Exception &e)
	{
		const std::string msg = "Caught Exception on doWatchdog().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Caught Exception on doWatchdog().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e) {
		const std::string msg = "Caught Exception on doWatchdog().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Caught Unknown Exception on doWatchdog().\n");
	}


	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "doWatchdog() end.");
	return;
}

void jobcontrol::Application::timeExpired (toolbox::task::TimerEvent& e)
{

	// perform some diagnostic here

	toolbox::task::TimerTask * tt = e.getTimerTask();

	std::string timerTaskName = tt->name;


	//
	// branch depending which taskName
	//
	if (timerTaskName == watchdogName)
	{
		try
		{
			// do the watchdog in the workthread to have acccess to waitpid() call
			mutex_.take();
			_ExecuteCommandData ecd;
			ecd._command         = watchdog;
			_exeWatchDogList->push_back( ecd );
			mutex_.give();
			workerLoop_->submit( workerLoopActionSignature_ );
		}
		catch (toolbox::exception::RingListFull &e)
		{
			// release
			mutex_.give();

			const std::string msg = "Exception in timeExpired: _exeCmdDatList (rlist) is full.\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
		}
		catch (toolbox::task::exception::Exception &e)
		{
			// release
			mutex_.give();

			const std::string msg = "Exception in timeExpired while submit workerLoopActionSignature_.\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
		}
		catch (xcept::Exception &e)
		{
			// release
			mutex_.give();

			const std::string msg = "Exception in timeExpired.\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
		}
		catch (std::exception &e)
		{
			// release
			mutex_.give();
			const std::string msg = "Exception in timeExpired.\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
		}
		catch (...)
		{
			// release
			mutex_.give();
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "Unknown Exception in timeExpired.");
		}
	}
}       



// Format of input message:
//<executeCommand execPath=""  jid="" uid="" user=""  argv="" notifierURL="">
//  <EnvironmentVariable name1="value1"  name2="value2"/>
//  <EnvironmentVariable name3="value3" />
//  <EnvironmentVariable ...
// </executeCommand>

xoap::MessageReference jobcontrol::Application::executeCommand(xoap::MessageReference msg) 

{

	startXdaqExe( msg );

	//
	// generate dummy SOAP reply
	//
	xoap::MessageReference reply = createPIDReplyMsg( "0" ); // dummy PID , FIX_ME: pid will be depracated in a future release of RCMS3

	return reply;
}


// Format of input message:
//<startXdaqExe execPath=""  jid="" uid="" user=""  argv="" notifierURL="">
//  <EnvironmentVariable name1="value1"  name2="value2"/>
//  <EnvironmentVariable name3="value3" />
//  <EnvironmentVariable ...
//  <ConfigFile> 
//  CONFIGURATION TEXT
//  </ConfigFile>
// </startXdaqExe>

xoap::MessageReference jobcontrol::Application::startXdaqExe(xoap::MessageReference msg) 

{

	try
	{
		// check SOAP msg

		// retrieve DOM from SOAP msg

		xoap::SOAPPart part = msg->getSOAPPart();
		xoap::SOAPEnvelope env = part.getEnvelope();
		xoap::SOAPBody body = env.getBody();
		DOMNode* node = body.getDOMNode();
		DOMDocument* doc = node->getOwnerDocument();
		DOMNodeList * msgDOMnodeList =  doc->getElementsByTagNameNS( XMLString::transcode( XDAQ_NS_URI ), XMLString::transcode ( "startXdaqExe" ) );

		if (msgDOMnodeList->getLength() != 1)
		{ // try alternative to be backward compatible
			msgDOMnodeList =  doc->getElementsByTagNameNS( XMLString::transcode( XDAQ_NS_URI ), XMLString::transcode ( "executeCommand" ) );
		}


		if (msgDOMnodeList->getLength() != 1)
		{

			xoap::MessageReference faultReply   = createStandardReplyMsg("startXdaqExe");
			xoap::SOAPBody replyBody  = faultReply->getSOAPPart().getEnvelope().getBody();
			xoap::SOAPFault fault     = replyBody.addFault();
			fault.setFaultString("Badly formatted message");


			return faultReply;

		}

		//////////////////////////////////////////////////
		// reset environment list

		std::list<std::string> tmpExecArgList;
		std::list<std::string> tmpExecEnvList;

		tmpExecEnvList.clear();
		tmpExecArgList.clear();

		// retrieve arguments from soap message

		std::string tmpPath        = "";
		std::string tmpJid         = "";
		std::string tmpUid         = "";
		std::string tmpUser        = "";
		std::string tmpArgv        = "";
		std::string tmpNotifierURL = "";
		std::string tmpExecURL     = "";
		std::string tmpConfigFile  = "";
		std::string tmpConfigFilePath  = "";
		std::string tmpTag	       = "";
		std::string tmpLogFilePath = "";
		int tmpGid                 = 0;

		//
		// parse SOAP msg to get JID needed for reply.
		//

		DOMNode * msgDOMnode = msgDOMnodeList->item(0); // get a DOMnode

		// get attribute
		if ( msgDOMnode->hasAttributes() )
		{

			DOMNamedNodeMap * map = msgDOMnode->getAttributes();

			for (int l=0 ; l< (int)map->getLength() ; l++)
			{ // loop over attributes of node
				DOMNode * node = map->item(l);

				std::string attributeName="";
				std::string attributeValue="";
				attributeName = XMLString::transcode(node->getNodeName());
				attributeValue = xoap::XMLCh2String(node->getNodeValue());

				if (attributeName == "execPath")
				{ // get named attributes
					tmpPath = attributeValue;
				}
				else if (attributeName == "jid")
				{ // get named attributes
					tmpJid = attributeValue;
				}
				else if (attributeName == "uid")
				{ // get named attributes
					tmpUid = attributeValue;
				}
				else if (attributeName == "user")
				{ // get named attributes
					tmpUser = attributeValue;
				}
				else if (attributeName == "argv")
				{ // get named attributes
					tmpArgv =  attributeValue;
				} // END if get parameter
				else if (attributeName == "notifierURL")
				{ // get named attributes
					tmpNotifierURL =  attributeValue;
				}
				else if (attributeName == "execURL")
				{
					tmpExecURL = attributeValue;
				}
				else if (attributeName == "tag")
				{ // get named attributes
					tmpTag =  attributeValue;
				} // END if get parameter
				else if (attributeName == "logFilePath")
				{ // get named attributes
					tmpLogFilePath =  toolbox::trim(attributeValue);
				}

			} // END loop over attribute node

		} //END if has attribute


		////////////////////////////////////////////////////////
		// GET THE ENVIRONMENT LIST, CONFIGFILE (CHILD NODE)

		DOMNodeList * envNodeList =  msgDOMnode->getChildNodes();
		std::string envName = "";
		std::string envValue  = "";


		for ( int j=0; j < (int)envNodeList->getLength() ; j++ )
		{
			std::string nodeName = "";
			DOMNode * envVarNode = envNodeList->item(j); // get a DOMnode
			nodeName = XMLString::transcode(envVarNode->getNodeName());

			if (nodeName == "ConfigFile")
			{
				if (envVarNode->hasChildNodes())
				{
					tmpConfigFile = xoap::XMLCh2String( envVarNode->getFirstChild()->getNodeValue() );
					// set path to config file
					toolbox::net::UUID uuid;
					tmpConfigFilePath = "/tmp/xdaqconf_" + uuid.toString() + ".xml";
				}
			}

			if (nodeName == "EnvironmentVariable")
			{

				DOMNamedNodeMap * envAttributes = envVarNode->getAttributes();
				for (int l=0 ; l < (int)envAttributes->getLength() ; l++) { // loop over attributes of node
					DOMNode * node = envAttributes->item(l);
					std::string attributeName="";
					std::string attributeValue="";
					attributeName = XMLString::transcode(node->getNodeName());
					attributeValue = xoap::XMLCh2String(node->getNodeValue());

					std::string environment = attributeName;
					environment       += "=";
					environment       += attributeValue;

					// put on environment list
					tmpExecEnvList.push_back(environment);

				}
			}
		}
		// check the value of the execJid, if 0 no JobID is given with the start command.
		// In this case the JID is created. If the JID is given, this overrides the creation.
		if (tmpJid == "")
		{
			tmpJid = intToString(_intJid++);
		}

		// check if a configFile was given. If so start the executive with -c CONFIG_FILE flag in argv list.
		// CONFIG_FILE = /tmp/JID.xml
		if ( tmpConfigFile != "" )
		{
			tmpArgv += " -c " + tmpConfigFilePath;
		}

		// check for a valid username entry,
		// if present try to get the userID from the username

		if ( tmpUser != "" )
		{
			errno = 0;
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Before getpwnam( tmpUser.c_str() ) tmpUser = " << tmpUser);
			struct passwd * pwd  = getpwnam( tmpUser.c_str() );
			if ( pwd != 0 )
			{
				// found entry, get the UID
				tmpUid = intToString( (int) pwd->pw_uid );
				tmpGid = (int)pwd->pw_gid;
			}
			else
			{
				std::stringstream msg;
				msg << "Failed in  getpwnam() for '" << tmpUser << "' with '" << strerror(errno) << "' errno:" << errno;
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), msg.str());
				XCEPT_RAISE (xoap::exception::Exception, msg.str());
			}
		}
		else
		{ // check for valid uid and get username for it
			errno = 0;
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Before getpwuid( atoi(tmpUid.c_str()) ) tmpUid = " << tmpUid);
			struct passwd * pwd  = getpwuid( atoi(tmpUid.c_str()) );
			if ( pwd != 0 )
			{
				// found entry, get the user name
				tmpUser = pwd->pw_name;
				tmpGid = (int)pwd->pw_gid;
			}
			else
			{
				std::stringstream msg;
				msg << "Failed in  getpwuid() for '" << tmpUid << "' with '" << strerror(errno) << "' errno:" << errno;
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), msg.str());
				XCEPT_RAISE (xoap::exception::Exception, msg.str());
			}

		}

		////////////////////////////////////////////////////////
		// GET THE ARG LIST
		std::cout << "tmpArgv        = " << tmpArgv        << std::endl;
		buildArgList(tmpArgv,tmpExecArgList);



		//
		// put data into _ExecuteCommandData struct and put it on rlist
		//

		_ExecuteCommandData ecd;
		ecd._command         = start;
		ecd._jid             = tmpJid;
		ecd._args            = tmpExecArgList;
		ecd._env             = tmpExecEnvList;
		ecd._execUser        = tmpUser;

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Conversion using atoi tmpUid = " << tmpUid);
		ecd._execUid         = atoi(tmpUid.c_str());

		ecd._execGid         = tmpGid;
		ecd._execPath        = tmpPath;
		ecd._execNotifierURL = tmpNotifierURL;
		ecd._execURL 		 = tmpExecURL;
		ecd._execConfigFile  = tmpConfigFile;
		ecd._execConfigFilePath  = tmpConfigFilePath;
		ecd._tag		 = tmpTag;
		ecd._logFilePath		 = tmpLogFilePath;

		ecd._pid = -1; //unused till fork(?)



		/**********
			int                    _intJid;               // int job id for automatic generation of jid

			pid_t                  _pid;                  // PID argument for kill and check

		 **********/



		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Parsed the start message for executive=" + ecd._execURL );

		// block
		mutex_.take();

		_exeCmdDatList->push_back( ecd );

		// release
		mutex_.give();

		LOG4CPLUS_DEBUG(this->getApplicationLogger(),  "Inserted the start message in the queue for executive=" + ecd._execURL +
				"\n _exeCmdDatList size=" + sizetToString(_exeCmdDatList->elements()) +
				" _exeWatchDogList size=" + sizetToString(_exeWatchDogList->elements()));

		//
		// generate SOAP reply
		//
		xoap::MessageReference reply = createJIDReplyMsg( tmpJid ); // JID

		//
		// generate work loop event and activate
		//
		workerLoop_->submit( workerLoopActionSignature_ );

		// std::cout << "<<< startXdaqExe" << std::endl;
		return reply;
	}
	catch (xoap::exception::Exception& e)
	{
		// release
		mutex_.give();

		XCEPT_RETHROW (xoap::exception::Exception, "XDAQ exception on startXdaqExe method.", e);
	}
	catch (xcept::Exception &e)
	{
		// release
		mutex_.give();

		XCEPT_RETHROW(xoap::exception::Exception, "XDAQ exception on startXdaqExe method.", e);
	}
	catch (std::exception &e)
	{
		// release
		mutex_.give();

		std::stringstream oss;
		oss << e.what();
		XCEPT_RAISE(xoap::exception::Exception, "XDAQ exception on startXdaqExe method.\n" + oss.str());
	}
	catch (...)
	{
		// release
		mutex_.give();

		XCEPT_RAISE(xoap::exception::Exception, "Unknown exception on startXdaqExe method.");
	}

}

void jobcontrol::Application::executeCommandInLoop( _ExecuteCommandData ecd  ) {

	try
	{

		// std::cout << ">>> executeCommandInLoop enter" << std::endl;

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "executeCommand called");

		_caller = "ExecuteCommand";

		//
		// check command
		if (ecd._command == watchdog)
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "executeCommand: watchdog");
			doWatchdog();
		}
		else if (ecd._command == stop)
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "executeCommand: stop");
			killByPidInWorkLoop( ecd._pid );
		}
		else if (ecd._command == start)
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "executeCommand: start");


			// invoke start of job
			StartCommand(ecd);
		}
	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Exception in executeCommandInLoop method.\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
				<< xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		const std::string msg = "Exception in executeCommandInLoop method.\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(),
				"Unknown Exception in executeCommandInLoop method.");
	}

	return;

}


void jobcontrol::Application::buildArgList(const std::string &sString, std::list<std::string> &lList) 
{
	//
	// build a list of strings out of a string
	// strings separated by rDelimiters
	//

	std::string rDelimiters = " ";
	std::string::size_type lastPos(sString.find_first_not_of(rDelimiters, 0));
	std::string::size_type pos(sString.find_first_of(rDelimiters, lastPos));
	while (std::string::npos != pos || std::string::npos != lastPos)
	{
		lList.push_back(sString.substr(lastPos, pos - lastPos));
		lastPos = sString.find_first_not_of(rDelimiters, pos);
		pos = sString.find_first_of(rDelimiters, lastPos);
	}

}


std::string jobcontrol::Application::intToString(const int i)
{

	std::string s = "";
	char c[12];

	sprintf( c, "%i", i);

	s += c;

	return (s);
}

std::string jobcontrol::Application::sizetToString(const size_t i)
{

	std::string s = "";
	char c[12];

	sprintf( c, "%zu", i);

	s += c;

	return (s);
}

int jobcontrol::Application::stringToInt(const std::string s) {

	int i=0;

	sscanf( s.c_str(), "%i", &i);

	return (i);
}



xoap::MessageReference jobcontrol::Application::killExec(
		xoap::MessageReference msg) 
								{

	xoap::MessageReference reply;
	try
	{
		// user command

		// extract arguments from soap message
		// arguments:
		//
		// sPid:  Process ID of process to be killed (optional)
		// sExe:  executable name to be killed (optional)
		// sUser: UserName (optional)
		// sUid : UserID (optional)
		//
		// soap message contains arguments as attributes
		// <killExec pid="" execPath="" user="" uid="" />

		std::string msgString;

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "killExec() called.");

		// initilize arguments
		std::string sExe = "";
		std::string sPid = "";
		std::string sUser = "";
		std::string sUid = "";
		std::string sJid = "";

		// retrieve arguments from soap message
		xoap::SOAPPart part = msg->getSOAPPart();
		xoap::SOAPEnvelope env = part.getEnvelope();

		// retrieve DOM from SOAP msg
		xoap::SOAPBody body = env.getBody();
		DOMNode* node = body.getDOMNode();
		DOMDocument* doc = node->getOwnerDocument();
		DOMNodeList * msgDOMnodeList = doc->getElementsByTagNameNS(
				XMLString::transcode(XDAQ_NS_URI), XMLString::transcode(
						"killExec"));

		DOMNode * msgDOMnode = msgDOMnodeList->item(0); // get a DOMnode

		// get attribute
		if (0 != msgDOMnode && msgDOMnode->hasAttributes())
		{

			DOMNamedNodeMap * map = msgDOMnode->getAttributes();

			for (int l = 0; l < (int) map->getLength(); l++)
			{ // loop over attributes of node
				DOMNode * node = map->item(l);

				std::string attributeName = "";
				std::string attributeValue = "";
				attributeName = XMLString::transcode(node->getNodeName());
				attributeValue = xoap::XMLCh2String(node->getNodeValue());

				if (attributeName == "execPath")
				{ // get named attributes
					sExe = attributeValue;
				}
				else if (attributeName == "uid")
				{ // get named attributes
					sUid = attributeValue;
				}
				else if (attributeName == "user")
				{ // get named attributes
					sUser = attributeValue;
				}
				else if (attributeName == "jid")
				{ // get named attributes
					sJid = attributeValue;
				}
				else if (attributeName == "pid")
				{ // get named attributes
					sPid = attributeValue;
				} // END if get parameter
			} // END loop over attribute node

		} //END if has attribute


		if (sPid != "")
		{
			// if pid is given, try to kill it directly and ignore other arguments
			// since it can not be more specific.
			// TODO: owner check if owner is given.
			killByPid(stringToInt(sPid));
			msgString = "killed by pid";
		}
		else if (sJid != "")
		{
			// kill by JobID
			LOG4CPLUS_INFO(this->getApplicationLogger(),
					"killExec(): kill by sJid=" + sJid + " .");
			// fetch the pid(s) for the user

			// take
			mutex_.take();

			// iterator over all started processes
			std::list<_PidUid>::iterator iter = _pidUidList.begin();

			// declare list containing all processes with given user/exe
			std::list<_PidUid> procKillList;

			// book keeping
			bool bkill = false;

			for (; iter != _pidUidList.end(); iter++)
			{
				if ((*iter)._jid == sJid)
				{
					procKillList.push_back(*iter); // put it on the kill list
					bkill = true; // flag killing
				}
			}

			// give
			mutex_.give();
			// loop over kill list and kill processes
			iter = procKillList.begin();
			for (; iter != procKillList.end(); iter++)
				killByPid(((*iter)._pid));

			if (bkill)
			{
				// reply
				msgString = "killed by JID";

			}
			else
			{
				// reply
				msgString = "no job killed.";

			}

		}
		else if ((sUser != "" || sUid != "") && sExe != "")
		{
			// fetch the pid(s) for the user/exe combo

			// take
			mutex_.take();
			// iterator over all started processes
			std::list<_PidUid>::iterator iter = _pidUidList.begin();

			// declare list containing all processes with given user/exe
			std::list<_PidUid> procKillList;

			for (; iter != _pidUidList.end(); iter++)
			{
				if (((*iter)._user == sUser || intToString((*iter)._uid)
						== sUid) && (*iter)._sexe == sExe)
				{
					procKillList.push_back(*iter); // put it on the kill list
				}
			}
			// give
			mutex_.give();
			// loop over kill list and kill processes
			iter = procKillList.begin();
			for (; iter != procKillList.end(); iter++)
				killByPid(((*iter)._pid));

			// reply
			msgString = "killed by Name/Executable";

		}
		else if ((sUser != "" || sUid != "") && sExe == "")
		{
			// fetch the pid(s) for the user

			// take
			mutex_.take();
			// iterator over all started processes
			std::list<_PidUid>::iterator iter = _pidUidList.begin();

			// declare list containing all processes with given user/exe
			std::list<_PidUid> procKillList;

			for (; iter != _pidUidList.end(); iter++)
			{
				if (((*iter)._user == sUser || intToString((*iter)._uid)
						== sUid))
				{
					procKillList.push_back(*iter); // put it on the kill list
				}
			}

			// give
			mutex_.give();

			// loop over kill list and kill processes
			iter = procKillList.begin();
			for (; iter != procKillList.end(); iter++)
				killByPid(((*iter)._pid));

			// reply
			msgString = "killed by Name";

		}
		else
		{
			msgString = "invalid request for kill";

		}

		reply = createStandardReplyMsg(msgString);
		// -- notify monitoring data change

		// take
		mutex_.take();
		this->updateJobTable();
		// give
		mutex_.give();

		std::list<std::string> names;
		names.push_back("jobTable");
		try
		{
			monitorInfoSpace_->fireItemGroupChanged(names, this);
		}
		catch (xdata::exception::Exception &e)
		{
			const std::string msg =
					"Caught Exception in monitorInfoSpace_->fireItemGroupChanged on killExec().\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
					<< xcept::stdformat_exception_history(e));
		}

	}
	catch (xoap::exception::Exception& e)
	{
		mutex_.give();
		XCEPT_RETHROW(xoap::exception::Exception, "Exception on killExec().\n",
				e);
	}
	catch (xcept::Exception &e)
	{
		mutex_.give();
		XCEPT_RETHROW(xoap::exception::Exception, "Exception on killExec().\n",
				e);
	}
	catch (std::exception &e)
	{
		mutex_.give();
		std::stringstream oss;
		oss << e.what();
		XCEPT_RAISE(xoap::exception::Exception, "Exception on killExec().\n"
				+ oss.str());
	}
	catch (...)
	{
		mutex_.give();
		XCEPT_RAISE(xoap::exception::Exception, "Exception on killExec().\n");
	}

	return reply;
								}

xoap::MessageReference jobcontrol::Application::getJobStatus( xoap::MessageReference msg ) 
		{


	try
	{
		// check SOAP msg

		// retrieve DOM from SOAP msg

		xoap::SOAPPart part = msg->getSOAPPart();
		xoap::SOAPEnvelope env = part.getEnvelope();
		xoap::SOAPBody body = env.getBody();
		DOMNode* node = body.getDOMNode();
		DOMDocument* doc = node->getOwnerDocument();
		DOMNodeList * msgDOMnodeList =  doc->getElementsByTagNameNS( XMLString::transcode( XDAQ_NS_URI ), XMLString::transcode ( "getJobStatus" ) );

		if (msgDOMnodeList->getLength() != 1)
		{

			xoap::MessageReference faultReply   = createStandardReplyMsg("getJobStatus");
			xoap::SOAPBody replyBody  = faultReply->getSOAPPart().getEnvelope().getBody();
			xoap::SOAPFault fault     = replyBody.addFault();
			fault.setFaultString("Badly formatted message");

			return faultReply;
		}

		std::string tmpJid="";

		DOMNode * msgDOMnode = msgDOMnodeList->item(0); // get a DOMnode

		// get attribute
		if ( msgDOMnode->hasAttributes() )
		{
			DOMNamedNodeMap * map = msgDOMnode->getAttributes();

			for (int l=0 ; l< (int)map->getLength() ; l++)
			{ // loop over attributes of node
				DOMNode * node = map->item(l);

				std::string attributeName="";
				std::string attributeValue="";
				attributeName = XMLString::transcode(node->getNodeName());
				attributeValue = xoap::XMLCh2String(node->getNodeValue());

				if (attributeName == "jid")
				{ // get named attributes
					tmpJid = attributeValue;
				} // END if get parameter
			} // END loop over attribute node
		} //END if has attribute

		// block
		mutex_.take();

		std::list<_PidUid>::iterator iter = _pidUidList.begin();

		for ( iter = _pidUidList.begin() ; iter!=_pidUidList.end() ; iter++)
		{
			if((*iter)._jid == tmpJid)
			{
				xoap::MessageReference responseMsg = xoap::createMessage();
				xoap::SOAPEnvelope envelope = responseMsg->getSOAPPart().getEnvelope();
				xoap::SOAPBody body = envelope.getBody();
				xoap::SOAPName responseName =
						envelope.createName("getJobStatusResponse","rcms","urn:rcms-soap:2.0");
				xoap::SOAPBodyElement responseElement =
						body.addBodyElement(responseName);
				responseElement.addNamespaceDeclaration("xsi",
						"http://www.w3.org/2001/XMLSchema-instance");
				responseElement.addNamespaceDeclaration("xsd",
						"http://www.w3.org/2001/XMLSchema");
				responseElement.addNamespaceDeclaration("soapenc",
						"http://schemas.xmlsoap.org/soap/encoding/");

				{
					xoap::SOAPName pidName =
							envelope.createName("pid","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement pidElement =
							responseElement.addChildElement(pidName);
					xoap::SOAPName pidTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					pidElement.addAttribute( pidTypeName, "xsd:string" );
					pidElement.addTextNode( intToString((*iter)._pid) );
				}

				{
					xoap::SOAPName uidName =
							envelope.createName("uid","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement uidElement =
							responseElement.addChildElement(uidName);
					xoap::SOAPName uidTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					uidElement.addAttribute( uidTypeName, "xsd:integer" );
					uidElement.addTextNode( intToString((*iter)._uid) );
				}

				{
					xoap::SOAPName portName =
							envelope.createName("port","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement portElement =
							responseElement.addChildElement(portName);
					xoap::SOAPName portTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					portElement.addAttribute( portTypeName, "xsd:integer" );
					portElement.addTextNode( intToString((*iter)._port) );
				}

				{
					xoap::SOAPName sexeName =
							envelope.createName("sexe","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement sexeElement =
							responseElement.addChildElement(sexeName);
					xoap::SOAPName sexeTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					sexeElement.addAttribute( sexeTypeName, "xsd:string" );
					sexeElement.addTextNode( (*iter)._sexe );
				}

				{
					xoap::SOAPName userName =
							envelope.createName("user","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement userElement =
							responseElement.addChildElement(userName);
					xoap::SOAPName userTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					userElement.addAttribute( userTypeName, "xsd:string" );
					userElement.addTextNode( (*iter)._user );
				}

				{
					xoap::SOAPName jidName =
							envelope.createName("jid","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement jidElement =
							responseElement.addChildElement(jidName);
					xoap::SOAPName jidTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					jidElement.addAttribute( jidTypeName, "xsd:string" );
					jidElement.addTextNode( (*iter)._jid );
				}

				{
					xoap::SOAPName statusName =
							envelope.createName("status","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement statusElement =
							responseElement.addChildElement(statusName);
					xoap::SOAPName statusTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					statusElement.addAttribute( statusTypeName, "xsd:integer" );
					statusElement.addTextNode( intToString((*iter)._status) );
				}

				{
					xoap::SOAPName notifiedName =
							envelope.createName("notified","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement notifiedElement =
							responseElement.addChildElement(notifiedName);
					xoap::SOAPName notifiedTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					notifiedElement.addAttribute( notifiedTypeName, "xsd:boolean" );
					std::string notified = "0";
					if((*iter)._notified)
						notified = "1";
					notifiedElement.addTextNode( notified );
				}

				{
					xoap::SOAPName notifierurlName =
							envelope.createName("notifierURL","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement notifierurlElement =
							responseElement.addChildElement(notifierurlName);
					xoap::SOAPName notifierurlTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					notifierurlElement.addAttribute( notifierurlTypeName, "xsd:string" );
					notifierurlElement.addTextNode( (*iter)._notifierURL );
				}

				{
					xoap::SOAPName stateName =
							envelope.createName("state","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement stateElement =
							responseElement.addChildElement(stateName);
					xoap::SOAPName stateTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					stateElement.addAttribute( stateTypeName, "xsd:string" );
					std::stringstream value;
					value << (*iter)._state;
					stateElement.addTextNode( value.str() );
				}

				{
					xoap::SOAPName vsizeName =
							envelope.createName("vsize","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement vsizeElement =
							responseElement.addChildElement(vsizeName);
					xoap::SOAPName vsizeTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					vsizeElement.addAttribute( vsizeTypeName, "xsd:integer" );
					vsizeElement.addTextNode( intToString((*iter)._vsize) );
				}

				{
					xoap::SOAPName starttimeName =
							envelope.createName("starttime","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement starttimeElement =
							responseElement.addChildElement(starttimeName);
					xoap::SOAPName starttimeTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					starttimeElement.addAttribute( starttimeTypeName, "xsd:integer" );
					starttimeElement.addTextNode( intToString((*iter)._start_time) );
				}

				{
					xoap::SOAPName nchildprocName =
							envelope.createName("nchildproc","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement nchildprocElement =
							responseElement.addChildElement(nchildprocName);
					xoap::SOAPName nchildprocTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					nchildprocElement.addAttribute( nchildprocTypeName, "xsd:integer" );
					nchildprocElement.addTextNode( intToString((*iter)._n_child_proc) );
				}

				{
					xoap::SOAPName childtreeName =
							envelope.createName("childtree","rcms","urn:rcms-soap:2.0");
					xoap::SOAPElement childtreeElement =
							responseElement.addChildElement(childtreeName);
					xoap::SOAPName childtreeTypeName = envelope.createName("type", "xsi",
							"http://www.w3.org/2001/XMLSchema-instance");
					childtreeElement.addAttribute( childtreeTypeName, "xsd:string" );
					childtreeElement.addTextNode( (*iter)._child_tree );
				}

				// release
				mutex_.give();

				return responseMsg;
			}
		}

		// release
		mutex_.give();

		xoap::MessageReference faultReply   = createStandardReplyMsg("getJobStatus");
		xoap::SOAPBody replyBody  = faultReply->getSOAPPart().getEnvelope().getBody();
		xoap::SOAPFault fault     = replyBody.addFault();
		fault.setFaultString("Jid not found");

		return faultReply;
	}
	catch (xoap::exception::Exception& e)
	{
		// release
		mutex_.give();

		XCEPT_RETHROW (xoap::exception::Exception, "Exception on getJobStatus method.", e);
	}
	catch (xcept::Exception &e)
	{
		// release
		mutex_.give();

		XCEPT_RETHROW(xoap::exception::Exception, "Exception on getJobStatus method", e);
	}
	catch (std::exception &se)
	{
		// release
		mutex_.give();

		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xoap::exception::Exception, "Exception on getJobStatus method.\n" + oss.str() );
	}
	catch (...)
	{
		// release
		mutex_.give();

		XCEPT_RAISE(xoap::exception::Exception, "Unknown exception on getJobStatus method.");
	}
		}



xoap::MessageReference jobcontrol::Application::killAll(
		xoap::MessageReference msg) 
		{
	// user command

	// no arguments
	//
	// kills all processes started by JobControl
	killAllProcs();

	// reply
	xoap::MessageReference reply = createStandardReplyMsg("killed");

	return reply;

		}


void jobcontrol::Application::killAllProcs() 

{

	try
	{

		mutex_.take();

		//
		// kills all processes started by JobControl

		// iterator over all started processes
		std::list<_PidUid>::iterator iter = _pidUidList.begin();

		// declare list containing all processes with given user/exe
		std::list<_PidUid> procKillList;

		for (; iter != _pidUidList.end(); iter++)
		{

			procKillList.push_back(*iter); // put it on the kill list

		}

		mutex_.give();

		// loop over kill list and kill processes
		iter = procKillList.begin();
		for (; iter != procKillList.end(); iter++)
			killByPid(((*iter)._pid));
		// -- notify monitoring data change


		mutex_.take();
		this->updateJobTable();
		mutex_.give();

		std::list<std::string> names;
		names.push_back("jobTable");
		try
		{
			monitorInfoSpace_->fireItemGroupChanged(names, this);
		}
		catch (xdata::exception::Exception &e)
		{
			const std::string
			msg =
					"Caught Exception in monitorInfoSpace_->fireItemGroupChanged on killAllProcs() ...\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
					<< xcept::stdformat_exception_history(e));
		}

	}
	catch (xoap::exception::Exception& e)
	{
		mutex_.give();
		XCEPT_RETHROW(xoap::exception::Exception,
				"Exception on killAllProcs().\n", e);

	}
	catch (xcept::Exception &e)
	{
		mutex_.give();
		XCEPT_RETHROW(xoap::exception::Exception,
				"Exception on killAllProcs().\n", e);
	}
	catch (std::exception &e)
	{
		mutex_.give();
		std::stringstream oss;
		oss << e.what();
		XCEPT_RAISE(xoap::exception::Exception,
				"Exception on killAllProcs().\n" + oss.str());
	}
	catch (...)
	{
		mutex_.give();
		XCEPT_RAISE(xoap::exception::Exception,
				"Exception on killAllProcs().\n");
	}

}


xoap::MessageReference jobcontrol::Application::getState(xoap::MessageReference msg)  {
	//
	// user command
	//
	// no arguments
	//
	// reply state.
	//
	// Since JC is stateless, Enabled is replied.
	//

	xoap::MessageReference reply = createStateReplyMsg("Enabled");

	return reply;

}



xoap::MessageReference jobcontrol::Application::createPIDReplyMsg(const std::string pid) 
		{
	try
	{
		xoap::MessageReference responseMsg = xoap::createMessage();
		xoap::SOAPEnvelope envelope = responseMsg->getSOAPPart().getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
		xoap::SOAPName responseName =
				envelope.createName("pidResponse","rcms","urn:rcms-soap:2.0");
		xoap::SOAPBodyElement responseElement =
				body.addBodyElement(responseName);
		responseElement.addNamespaceDeclaration("xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		responseElement.addNamespaceDeclaration("xsd",
				"http://www.w3.org/2001/XMLSchema");
		responseElement.addNamespaceDeclaration("soapenc",
				"http://schemas.xmlsoap.org/soap/encoding/");
		xoap::SOAPName pidName =
				envelope.createName("pid","rcms","urn:rcms-soap:2.0");
		xoap::SOAPElement pidElement =
				responseElement.addChildElement(pidName);
		xoap::SOAPName pidTypeName = envelope.createName("type", "xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		pidElement.addAttribute( pidTypeName, "xsd:string" );
		pidElement.addTextNode( pid );

		return responseMsg;
	}
	catch (xoap::exception::Exception& e)
	{
		XCEPT_RETHROW(
				xoap::exception::Exception,
				"On createPIDReplyMsg method: failed to create reply message.",
				e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(
				xoap::exception::Exception,
				"On createPIDReplyMsg method: failed to create reply message.",
				e);
	}
	catch (std::exception& se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xoap::exception::Exception,
				"On createPIDReplyMsg method: failed to create reply message.\n"
				+ oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(
				xoap::exception::Exception,
				"Unknown exception on createPIDReplyMsg method: failed to create reply message.");
	}
		}


xoap::MessageReference jobcontrol::Application::createJIDReplyMsg(const std::string jid) 
				{

	xoap::MessageReference responseMsg;
	try
	{
		responseMsg = xoap::createMessage();
		xoap::SOAPEnvelope envelope = responseMsg->getSOAPPart().getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
		xoap::SOAPName responseName = envelope.createName("jidResponse",
				"rcms", "urn:rcms-soap:2.0");
		xoap::SOAPBodyElement responseElement = body.addBodyElement(
				responseName);
		responseElement.addNamespaceDeclaration("xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		responseElement.addNamespaceDeclaration("xsd",
				"http://www.w3.org/2001/XMLSchema");
		responseElement.addNamespaceDeclaration("soapenc",
				"http://schemas.xmlsoap.org/soap/encoding/");
		xoap::SOAPName jidName = envelope.createName("jid", "rcms",
				"urn:rcms-soap:2.0");
		xoap::SOAPElement jidElement = responseElement.addChildElement(jidName);
		xoap::SOAPName jidTypeName = envelope.createName("type", "xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		jidElement.addAttribute(jidTypeName, "xsd:string");
		jidElement.addTextNode(jid);
	}
	catch (xoap::exception::Exception& e)
	{
		XCEPT_RETHROW(
				xoap::exception::Exception,
				"On createJIDReplyMsg method: failed to create reply message.",
				e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(
				xoap::exception::Exception,
				"On createJIDReplyMsg method: failed to create reply message.",
				e);
	}
	catch (std::exception& se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xoap::exception::Exception,
				"On createJIDReplyMsg method: failed to create reply message.\n"
				+ oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(
				xoap::exception::Exception,
				"Unknown exception on createJIDReplyMsg method: failed to create reply message.");
	}

	return responseMsg;

				}

xoap::MessageReference jobcontrol::Application::createStateReplyMsg(
		const std::string state) 
								{

	try
	{
		xoap::MessageReference responseMsg = xoap::createMessage();
		xoap::SOAPEnvelope envelope = responseMsg->getSOAPPart().getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
		xoap::SOAPName responseName = envelope.createName("getStateResponse",
				"rcms", "urn:rcms-soap:2.0");
		xoap::SOAPBodyElement responseElement = body.addBodyElement(
				responseName);
		responseElement.addNamespaceDeclaration("xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		responseElement.addNamespaceDeclaration("xsd",
				"http://www.w3.org/2001/XMLSchema");
		responseElement.addNamespaceDeclaration("soapenc",
				"http://schemas.xmlsoap.org/soap/encoding/");
		xoap::SOAPName stateName = envelope.createName("stateName", "rcms",
				"urn:rcms-soap:2.0");
		xoap::SOAPElement stateElement = responseElement.addChildElement(
				stateName);
		xoap::SOAPName stateTypeName = envelope.createName("type", "xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		stateElement.addAttribute(stateTypeName, "xsd:string");
		stateElement.addTextNode(state);

		return responseMsg;
	}
	catch (xoap::exception::Exception& e)
	{
		XCEPT_RETHROW (xoap::exception::Exception, "On createStateReplyMsg method: failed to create reply message.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xoap::exception::Exception, "On createStateReplyMsg method: failed to create reply message.", e);
	}
	catch (std::exception& se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xoap::exception::Exception, "On createStateReplyMsg method: failed to create reply message.\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(
				xoap::exception::Exception,
				"Unknown exception on createStateReplyMsg method: failed to create reply message.");
	}
								}

xoap::MessageReference jobcontrol::Application::createStandardReplyMsg
(
		const std::string &replyText
) 
{
	try
	{
		xoap::MessageReference responseMsg = xoap::createMessage();
		xoap::SOAPEnvelope envelope = responseMsg->getSOAPPart().getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
		xoap::SOAPName responseName = envelope.createName("getStateResponse",
				"rcms", "urn:rcms-soap:2.0");
		xoap::SOAPBodyElement responseElement = body.addBodyElement(
				responseName);
		responseElement.addNamespaceDeclaration("xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		responseElement.addNamespaceDeclaration("xsd",
				"http://www.w3.org/2001/XMLSchema");
		responseElement.addNamespaceDeclaration("soapenc",
				"http://schemas.xmlsoap.org/soap/encoding/");
		xoap::SOAPName replyName = envelope.createName("reply", "rcms",
				"urn:rcms-soap:2.0");
		xoap::SOAPElement replyElement = responseElement.addChildElement(
				replyName);
		xoap::SOAPName replyTypeName = envelope.createName("type", "xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		replyElement.addAttribute(replyTypeName, "xsd:string");
		replyElement.addTextNode(replyText);

		return responseMsg;
	}
	catch (xoap::exception::Exception& e)
	{
		XCEPT_RETHROW(xoap::exception::Exception,
				"On createStandardReplyMsg method: failed to create reply message.",
				e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xoap::exception::Exception,
				"On createStandardReplyMsg method: failed to create reply message.",
				e);
	}
	catch (std::exception& se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xoap::exception::Exception,
				"On createStandardReplyMsg method: failed to create reply message.\n"
				+ oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(
				xoap::exception::Exception,
				"Unknown exception on createStandardReplyMsg method: failed to create reply message.");
	}
}


xoap::MessageReference jobcontrol::Application::createCrashNotificationMsg(const std::string jid, const int signal) 
		{
	try
	{
		xoap::MessageReference responseMsg = xoap::createMessage();
		xoap::SOAPEnvelope envelope = responseMsg->getSOAPPart().getEnvelope();
		xoap::SOAPBody body = envelope.getBody();
		xoap::SOAPName responseName = envelope.createName(
				"jcCrashNotification", "rcms", "urn:rcms-soap:2.0");
		xoap::SOAPBodyElement responseElement = body.addBodyElement(
				responseName);
		responseElement.addNamespaceDeclaration("xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		responseElement.addNamespaceDeclaration("xsd",
				"http://www.w3.org/2001/XMLSchema");
		responseElement.addNamespaceDeclaration("soapenc",
				"http://schemas.xmlsoap.org/soap/encoding/");

		// add jid
		xoap::SOAPName jidName = envelope.createName("jid", "rcms",
				"urn:rcms-soap:2.0");
		xoap::SOAPElement jidElement = responseElement.addChildElement(jidName);
		xoap::SOAPName jidTypeName = envelope.createName("type", "xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		jidElement.addAttribute(jidTypeName, "xsd:string");
		jidElement.addTextNode(jid);

		// add signal
		xoap::SOAPName signalName = envelope.createName("signal", "rcms",
				"urn:rcms-soap:2.0");
		xoap::SOAPElement signalElement = responseElement.addChildElement(
				signalName);
		xoap::SOAPName signalTypeName = envelope.createName("type", "xsi",
				"http://www.w3.org/2001/XMLSchema-instance");
		signalElement.addAttribute(signalTypeName, "xsd:integer");
		signalElement.addTextNode(intToString(signal));

		return responseMsg;
	}
	catch (xoap::exception::Exception &e)
	{
		XCEPT_RETHROW(
				xoap::exception::Exception,
				"On createCrashNotificationMsgmethod: failed to create reply message.",
				e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(
				xoap::exception::Exception,
				"On createCrashNotificationMsg method: failed to create reply message.",
				e);
	}
	catch (std::exception& se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xoap::exception::Exception,
				"On createCrashNotificationMsg method: failed to create reply message.\n"
				+ oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(
				xoap::exception::Exception,
				"Unknown exception on createCrashNotificationMsg method: failed to create reply message.");
	}
		}


void jobcontrol::Application::killByPid( int pid ) 

{
	_ExecuteCommandData ecd;
	ecd._command         = stop;
	ecd._pid             = pid;

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Created the _exeCmd stop message for pid=" + intToString(ecd._pid) );

	mutex_.take();

	try
	{
		_exeCmdDatList->push_back( ecd );
		mutex_.give();
	}
	catch (toolbox::exception::RingListFull &e)
	{
		// release
		mutex_.give();

		const std::string msg = "Exception in killByPid: _exeCmdDatList (rlist) is full.\n";
		XCEPT_RETHROW (xcept::Exception, msg, e);
	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Inserted the _exeCmd stop message in the _exeCmdDatList queue for pid=" + intToString(ecd._pid) +
			"\n _exeCmdDatList size=" + sizetToString(_exeCmdDatList->elements()) +
			" _exeWatchDogList size=" + sizetToString(_exeWatchDogList->elements()));


	//
	// generate work loop event and activate
	//
	try
	{
		workerLoop_->submit( workerLoopActionSignature_ );
	}
	catch (toolbox::task::exception::Exception &e)
	{
		const std::string msg = "Exception in killByPid while submit workerLoopActionSignature_.\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}


}

void jobcontrol::Application::killByPidInWorkLoop( int pid ) 

{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Called the killByPidInWorkLoop for pid=" + intToString(pid) );
	try
	{
		std::string logString;

		// check that pid is in the list of my processes
		std::list<_PidUid>::iterator iter = _pidUidList.begin();
		for ( ; iter != _pidUidList.end(); iter++)
		{
			if ( (*iter)._pid == pid )
			{

				std::list<int> descendants;
				try
				{
					// Take a snapshot of the processes and get list of descendants of pid
					ProcessTree pt;
					pt.takeSnapshot();
					descendants = pt.getDescendantPIDs(pid);
					logString = "Found ";
					logString += intToString(descendants.size());
					logString += " descendants of process ";
					logString += intToString((*iter)._pid);
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), logString);
				}
				catch (xcept::Exception &e)
				{
					const std::string msg =
							"Exception killByPidInWorkLoop pt.getDescendantPIDs.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
							<< xcept::stdformat_exception_history(e));
				}
				catch (std::exception &e)
				{
					const std::string msg =
							"Exception killByPidInWorkLoop pt.getDescendantPIDs.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
							<< e.what());
				}
				catch (...)
				{
					LOG4CPLUS_ERROR(this->getApplicationLogger(),
							"Exception killByPidInWorkLoop pt.getDescendantPIDs.\n");
				}

				if (descendants.size() != (*iter)._children_list.size())
				{
					descendants = (*iter)._children_list;
				}

				// logging of killing
				logString = "killByPid:: try to deliver kill signal";

				// kill process
				myKill( (*iter)._pid );

				// logging
				logString += "\n pid             = ";
				logString += intToString((*iter)._pid);
				logString += "\n uid             = ";
				logString += intToString((*iter)._uid);

				//
				// Kill descendant processes
				if(descendants.size() > 0)
				{
					myKillDescendants((*iter)._pid,descendants,_numberOfTryForMyKillDescendants, _sleepBetweenTryForMyKillDescendants);
				}


				try
				{
					if (! (*iter)._keepLogFile )
					{
						//
						// try to remove logfile
						char logPath[100];
						sprintf( logPath,"/tmp/xdaqjcPID%i.log", (*iter)._pid );

						char newPath[100];
						sprintf( newPath,"/tmp/xdaqjcPIDlast.log");

						if ( rename( logPath, newPath ) == -1 )
							logString += "\nCould not move log-file";

						else
						{
							logString += "\nLog-file successfully moved to ";
							logString += newPath;
						}
					}

					//
					// try to remove config file
					remove( (*iter)._configFilePath.c_str() );
				}
				catch (xcept::Exception &e)
				{
					const std::string msg =
							"Exception on killByPidInWorkLoop method: could not remove log and configuration file.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
							<< xcept::stdformat_exception_history(e));
				}
				catch (std::exception &e)
				{
					const std::string msg =
							"Exception on killByPidInWorkLoop method: could not remove log and configuration file.\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg
							<< e.what());
				}
				catch (...)
				{
					LOG4CPLUS_ERROR(this->getApplicationLogger(),
							"Unknown exception on killByPidInWorkLoop method: could not remove log and configuration file.\n");
				}

				// log message
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), logString);

				iter = _pidUidList.erase(iter);                // remove element from list

				return;
			}
		}

		logString = "killByPid:: no proces found with pid =  ";
		logString += intToString( pid );

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), logString);
	}
	catch (xdata::exception::Exception &e)
	{
		const std::string msg = "Caught Exception on killByPidInWorkLoop().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Caught Exception on killByPidInWorkLoop().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		const std::string msg = "Caught Exception on killByPidInWorkLoop().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Caught Exception on killByPidInWorkLoop().\n");
	}
	return;
}


/**
 *  kill routine
 **/
void jobcontrol::Application::myKillDescendants( pid_t pid, std::list<int> descendants, int numberOfTry, long sleepBetweenTry)
{
	try
	{
		timeval tim;
		gettimeofday(&tim, NULL);
		std::ostringstream s;
		double t1=tim.tv_sec+(tim.tv_usec/1000000.0);

		//Send the Kill -9 to all descendants
		std::list<int>::iterator iter = descendants.begin();
		while (iter != descendants.end())
		{
			int pItem = *iter;
			try
			{
				int retKill= ::kill(pItem,SIGKILL);
				int retErrno = errno;
				if (retKill == -1)
				{
					if (retErrno != 3)
					{
						const std::string msg = "Failed to send SIGKILL to process=" + intToString(pItem) + " parentPID=" + intToString(pid) +
								" with the following errno: " + strerror(retErrno) + " (" + intToString(retErrno) + ").";
						LOG4CPLUS_ERROR(this->getApplicationLogger(),msg);
					}
					iter = descendants.erase(iter);
				}
				else
				{
					++iter;
				}
			}
			catch (xcept::Exception &e)
			{
				const std::string msg = "Caught Exception on myKillDescendants() while sending SIGKILL for process=" +intToString(*iter) +
						+ " parentPID=" + intToString(pid) + " .\n";
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
				iter = descendants.erase(iter);
			}
			catch (std::exception &e)
			{
				const std::string msg = "Caught Exception on myKillDescendants() while sending SIGKILL for process=" +intToString(*iter) +
						+ " parentPID=" + intToString(pid) + " .\n";
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
				iter = descendants.erase(iter);
			}
			catch (...)
			{
				LOG4CPLUS_ERROR(this->getApplicationLogger(), "Caught Exception on myKillDescendants() while sending SIGKILL for process=" +intToString(*iter) +
						+ " parentPID=" + intToString(pid) + " .\n");
				iter = descendants.erase(iter);
			}
		}


		int counter = 0;
		while (counter < numberOfTry && descendants.size() > 0)
		{
			std::list<int>::iterator iter = descendants.begin();
			while (iter != descendants.end())
			{
				int pItem = *iter;
				try
				{
					struct stat stFileInfo;
					std::string strFilename = "/proc/" + intToString(pItem);
					int intStat = stat(strFilename.c_str(),&stFileInfo);
					int statErrno = errno;
					if (intStat == 0)
					{
						++iter;
					}
					else
					{
						if (statErrno != 2)
						{
							const std::string msg = "Failed to check the stat of process=" + intToString(pItem) + " parentPID=" + intToString(pid) + " with the following errno: "
									+ strerror(statErrno) + " (" + intToString(statErrno) + ").";
							LOG4CPLUS_ERROR(this->getApplicationLogger(),msg);
						}
						iter = descendants.erase(iter);
					}
				}
				catch (xcept::Exception &e)
				{
					const std::string msg = "Caught Exception on myKillDescendants() while checking stat for process=" +intToString(pItem) +
							+ " parentPID=" + intToString(pid) + " .\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
					++iter;
				}
				catch (std::exception &e)
				{
					const std::string msg = "Caught Exception on myKillDescendants() while checking stat for process=" +intToString(pItem) +
							+ " parentPID=" + intToString(pid) + " .\n";
					LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
					++iter;
				}
				catch (...)
				{
					LOG4CPLUS_ERROR(this->getApplicationLogger(), "Caught Exception on myKillDescendants() while checking stat for process=" +intToString(pItem) +
							+ " parentPID=" + intToString(pid) + " .\n");
					++iter;
				}
			}
			if (descendants.size() > 0)
			{
				usleep(sleepBetweenTry);
			}
			counter++;
		}
		gettimeofday(&tim, NULL);
		double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
		s << t2-t1;

		std::string logString;
		if (descendants.size() > 0)
		{
			logString = "Failed to Kill processes for parentPID=" + intToString(pid) + ": ";
			for (std::list<int>::iterator di = descendants.begin(); di != descendants.end(); ++di)
			{
				logString += " " + intToString(*di);
			}
			logString += " with SIGKILL signal in " + s.str() + " seconds";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), logString);
		}
		else
		{
			logString = "Killed descendants for parentPID=" + intToString(pid) +  " with SIGKILL signal in " + s.str() + " seconds.";
			LOG4CPLUS_INFO(this->getApplicationLogger(), logString);
		}

	}
	catch (xdata::exception::Exception &e)
	{
		const std::string msg = "Caught Exception on myKillDescendants().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Caught Exception on myKillDescendants().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		const std::string msg = "Caught Exception on myKillDescendants().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Caught Exception on myKillDescendants().\n");
	}
}


void jobcontrol::Application::myKill(pid_t pid)
{
	try
	{
		timeval tim;
		gettimeofday(&tim, NULL);
		std::ostringstream s;
		double t1=tim.tv_sec+(tim.tv_usec/1000000.0);

		pid_t mypid = getpid();
		std::string logString;
		logString = "My pid: " + intToString(mypid) + " ,sending  process " + intToString(pid) + " a signal...";

		int retKill = ::kill( pid, SIGKILL);                 // kill with SIGKILL
		int errnoKill = errno;
		int status = 0;
		int retWait = ::waitpid( pid, &status, WUNTRACED); //collect process info
		int errnoWait = errno;
		if ( retKill == -1 || retWait == -1 )
		{              // SIGTERM did not work
			logString += "\nProblem occured:";
			if (retWait == -1)
			{
				logString += "wait errno=";
				logString += strerror(errnoWait);
			}
			if (retKill == -1)
			{
				logString += ", kill errno=";
				logString += strerror(errnoKill);
			}
		}
		else
		{
			logString += "\nKilled with SIGKILL.";
		}

		gettimeofday(&tim, NULL);
		double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
		s << t2-t1;

		if ( retWait == -1 )
		{
			logString += "...failed in " + s.str() + " seconds:";
			logString += strerror(errnoWait);
		}
		else
		{
			logString += "...delivered in " + s.str() + " seconds.";
		}
		LOG4CPLUS_INFO(this->getApplicationLogger(), logString);

	}catch (xdata::exception::Exception &e)
	{
		const std::string msg = "Caught Exception on myKill().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Caught Exception on myKill().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		const std::string msg = "Caught Exception on myKill().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Caught Exception on myKill().\n");
	}
}


void jobcontrol::Application::exportParams()
{

	// exported parameter used to communicate state
	// JobControl has only one state "Enabled"
	//

	stateName_ = "Enabled";

	appInfoSpace_->fireItemAvailable("stateName"         ,&stateName_         );
	appInfoSpace_->fireItemAvailable("numberOfTryForMyKillDescendants",&_numberOfTryForMyKillDescendants);
	appInfoSpace_->fireItemAvailable("sleepBetweenTryForMyKillDescendants",&_sleepBetweenTryForMyKillDescendants);


	jobTable_.addColumn("jid","string");
	jobTable_.addColumn("tag","string");
	jobTable_.addColumn("userName","string");
	jobTable_.addColumn("userId","unsigned int");
	jobTable_.addColumn("execPath","string");
	jobTable_.addColumn("execNotifierUrl","string");
	jobTable_.addColumn("pid","unsigned int");
	jobTable_.addColumn("status","string");
	jobTable_.addColumn("state","string");
	jobTable_.addColumn("startTime","string");
	jobTable_.addColumn("vsize","unsigned long");

	monitorInfoSpace_->fireItemAvailable("jobTable"         ,&jobTable_          );
	monitorInfoSpace_->fireItemAvailable("stateName"         ,&stateName_         );

}


void jobcontrol::Application::updateJobTable() 
{

	monitorInfoSpace_->lock();

	try
	{
		// iterate over pid list and fill into table
		std::list<_PidUid>::iterator iter = _pidUidList.begin();
		int i=0;

		if (jobTable_.getRowCount() > 0)
		{ // iterate over table and clear content

			xdata::Table::iterator ti;
			ti = jobTable_.begin();
			while ( ti != jobTable_.end() )
			{
				ti = jobTable_.erase(ti);
			}
		}


		for ( ; iter != _pidUidList.end(); iter++)
		{

			xdata::String user = ((std::string)(*iter)._user).c_str();
			jobTable_.setValueAt(i,"userName", user);

			xdata::String sexe = ((std::string)(*iter)._sexe).c_str();
			jobTable_.setValueAt(i,"execPath", sexe);

			xdata::String jid = ((std::string)(*iter)._jid).c_str();
			jobTable_.setValueAt(i,"jid", jid);

			xdata::String tag = ((std::string)(*iter)._tag).c_str();
			jobTable_.setValueAt(i,"tag", tag);

			// call kill, but don't deliver signal. just check status.
			int istatus = ::kill((*iter)._pid,0);
			xdata::String status = "";
			if ( istatus == 0 )
			{
				status = "alive";
			}
			else if ( istatus == -1 && errno == ESRCH )
			{
				// put in status info from waitpid()
				status = strsignal( (*iter)._status );
			}
			jobTable_.setValueAt(i,"status", status);

			std::string sstate = "";
			sstate += ((*iter)._state);
			xdata::String state = sstate.c_str();
			jobTable_.setValueAt(i,"state", state);

			xdata::UnsignedInteger uid = ( (int)((*iter)._uid) + 0x00000000);
			jobTable_.setValueAt(i,"userId", uid);

			xdata::UnsignedInteger pid = ( (int)((*iter)._pid) + 0x00000000);
			jobTable_.setValueAt(i,"pid", pid);

			xdata::String execNotifierUrl = ((std::string)(*iter)._notifierURL).c_str();
			jobTable_.setValueAt(i,"execNotifierUrl", execNotifierUrl);

			xdata::UnsignedLong vsize = (int)(*iter)._vsize;
			jobTable_.setValueAt(i,"vsize", vsize);

			toolbox::TimeVal tv = toolbox::TimeVal( (double)((*iter)._start_time) );
			xdata::String startTime = tv.toString( toolbox::TimeVal::gmt ).c_str();
			jobTable_.setValueAt(i,"startTime", startTime);

			i++;
		}

	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Caught Exception in updateJobTable() ...\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		const std::string msg = "Caught Exception in updateJobTable() ...\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Caught Exception in updateJobTable() ...\n");
	}

	monitorInfoSpace_->unlock();
	// std::cout << "<<< updateJobTable exit" << std::endl;
}


void jobcontrol::Application::StartCommand( _ExecuteCommandData &ecd )

{

	pid_t mypid;
	char executivePath[_jobControl_NMAX];
	char argv[_jobControl_MMAX][_jobControl_NMAX];  // build and initialize argv[][]
	char *pArgv[_jobControl_MMAX];
	char envp[_jobControl_MAXENV][_jobControl_NMAX];
	char* pEnvp[_jobControl_MAXENV];
	_PidUid p;                                 // add pid to pid-uid list
	std::string logdate = toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt);
	try
	{
		mypid = getpid();

		// try to do as much as possible before forking.
		// this is to avoid dead-locks.
		std::string logString;
		logString = "StartCommand() called. mypid=";
		logString += intToString(mypid);

		LOG4CPLUS_DEBUG(this->getApplicationLogger(), logString);

		// Executive Path
		strncpy(executivePath, ecd._execPath.c_str(), _jobControl_NMAX);


		// fills arguments list

		for (int i = 0; i<_jobControl_MMAX ; i++)
		{
			for (int j = 0; j<_jobControl_NMAX ; j++)
			{
				argv[i][j] = (char)NULL;
			}
		}

		int i=1;
		for (std::list<std::string>::iterator iter = ecd._args.begin(); iter != ecd._args.end(); iter++)
		{
			sprintf( argv[i], "%s", (*iter).c_str());
			pArgv[i] = & argv[i][0];
			i++;
		}
		pArgv[0] = executivePath;
		pArgv[i] = NULL;

		// Fills environment list

		i=0;
		for (std::list<std::string>::iterator iter = ecd._env.begin(); iter != ecd._env.end(); iter++)
		{
			sprintf( envp[i], "%s", (*iter).c_str());
			std::stringstream oss;
			oss << "environment list : " << i << " : " << (*iter) << std::endl;
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), oss.str());
			pEnvp[i] = & envp[i][0];
			i++;
		}
		pEnvp[i] = NULL;


		// Execute command
		// check access rights
		// root check
		if ( ecd._execUid < 0 || (ecd._execUid == 0 && !_rootAllowed) )
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "ERROR not allowed to call setuid() with uid = " << ecd._execUid << ", _rootAllowed = " << _rootAllowed << " No job started.");
			return;
		}

		// user check

		if ( ecd._execUser != "" && _usersAllowed != "" )
		{
			std::string::size_type ifound = _usersAllowed.find( ecd._execUser, 1 );
			if (  // check that it's not a part of another username
					ifound == std::string::npos  ||
					(
							_usersAllowed.length() != ecd._execUser.length() &&                                       // one user only
							_usersAllowed.find( ":" , ifound ) != ( ifound + _usersAllowed.length() + 1 ) &&     // within std::string back
							_usersAllowed.find( ":" , ifound - 1 ) != ( ifound -  1 )                            // within std::string front
					)
			)
			{
				/*
	  std::cout << " ifound: " << ifound << std::endl;
	  std::cout << "  _usersAllowed.find( : , ifound ) " <<  _usersAllowed.find( ":" , ifound ) << std::endl;
	  std::cout << "  _usersAllowed.find( : , ifound - 1 " <<  _usersAllowed.find( ":" , ifound - 1) << std::endl;
	  std::cout << "( ifound + _usersAllowed.length() + 1 ) " << ( ifound + _usersAllowed.length() + 1 ) << std::endl;
				 */
				LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
						("ERROR user not in access list JC_USERS_ALLOWED. User = %s . No job started.\n",ecd._execUser.c_str())
				);
				return;
			}
		}

		// manipulate file descriptors
		//  for (int i = 3; i < 10; i++) {
		//    fcntl(i,F_SETFD,FD_CLOEXEC); // set close-on-exec bit active
		//  }

		p._uid             = ecd._execUid;
		p._user            = ecd._execUser;
		p._sexe            = ecd._execPath;
		p._jid             = ecd._jid;
		p._notifierURL     = ecd._execNotifierURL;
		p._execURL         = ecd._execURL;
		p._configFile      = ecd._execConfigFile;
		p._configFilePath  = ecd._execConfigFilePath;
		p._tag             = ecd._tag;
		p._notified        = false;
		p._port            = 0;
		p._status          = 0;
		p._state           = 'S';
		p._n_child_proc    = 0;
		p._child_tree      = "";
		p._keepLogFile  = false;





	}
	catch (xdata::exception::Exception &e)
	{
		const std::string msg = "Caught Exception on StartCommand() before fork().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (xcept::Exception &e)
	{
		const std::string msg = "Caught Exception on StartCommand() before fork().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		const std::string msg = "Caught Exception on StartCommand() before fork().\n";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Unknown Exception on StartCommand() before fork().\n");
	}

	int ret=0;
	// fork
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Before fork() for executive= " + ecd._jid );
	pid_t pid = fork();  // fork xdaq.exe

	// Create new process for the new command (forking)

	if (pid == -1)
	{                               // check pid
		LOG4CPLUS_ERROR(this->getApplicationLogger(), " jobControl FATAL: couldn't fork process. ");
		XCEPT_RAISE (xcept::Exception, "*** jobControl FATAL jobControl: couldn't fork. ");
	}




	if (pid != 0 )                                 // if we are not the child return
	{
		std::stringstream logPath;

		if (ecd._logFilePath != "")
		{
			p._keepLogFile  = true;
			logPath << ecd._logFilePath << "-" << logdate << "-" << pid << ".log";
		}
		else
		{
			p._keepLogFile  = false;
			logPath << "/tmp/xdaqjcPID" << pid << ".log";
		}

		p._logFilePath = logPath.str();


		try
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),toolbox::toString("Parent: forked with pid = %i \n ",pid));

			p._pid             = pid;
			// get the start time
			p._start_time =  toolbox::TimeVal::gettimeofday();

			try
			{
				_pidUidList.push_back(p);
			}
			catch (toolbox::exception::RingListFull &e)
			{
				const std::string msg = "Caught Exception on StartCommand() in parent process after fork().\n";
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
			}

			// -- notify monitoring data change
			this->updateJobTable();

			std::list<std::string> names;

			try
			{
				names.push_back("jobTable");
				monitorInfoSpace_->fireItemGroupChanged(names, this);
			}
			catch (toolbox::exception::RingListFull &e)
			{
				const std::string msg = "Caught Exception on StartCommand() in parent process after fork().\n";
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
			}
			catch (xdata::exception::Exception &e)
			{
				const std::string msg = "Caught Exception in monitorInfoSpace_->fireItemGroupChanged on StartCommand() ...\n";
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
			}

			// -- notify monioring data change
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Parent returning.");
		}
		catch (xdata::exception::Exception &e)
		{
			const std::string msg = "Caught Exception on StartCommand() in parent process after fork().\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
		}
		catch (xcept::Exception &e)
		{
			const std::string msg = "Caught Exception on StartCommand() in parent process after fork().\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
		}
		catch (std::exception &e)
		{
			const std::string msg = "Caught Exception on StartCommand() in parent process after fork().\n";
			LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
		}
		catch (...)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "Unknown Exception on StartCommand() in parent process after fork().\n");
		}
		return;

	}

	//
	// we are in the child
	//

	// stop the watchdog
	//  watchdogEnabled_ = false;
	//  watchdogTimer_->stop();


	// brute force close
	// xdaq only opens first 5.
	//
	close(0);
	for (int i = 3; i < 32; i++) {
		close(i);
	}

	// set supplementary groups
	errno = 0;
	struct passwd * pwd  = getpwnam( ecd._execUser.c_str() );
	if ( pwd != 0 )
	{
		try
		{
			toolbox::setSupplementaryGroups( pwd );
		}
		catch( xcept::Exception& e )
		{
			std::stringstream msg;
			msg << "Child failed to set the supplementary groups of the process to those of the user " << ecd._execUser << ": " << xcept::stdformat_exception_history(e);
			LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
			exit(-1);
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Child failed in getpwnam() for '" << ecd._execUser << "' with '" << strerror(errno) << "' errno:" << errno;
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
		exit(-1);
	}

	// set new group id
	if ( ecd._execGid != 0)
	{
		errno = 0;
		ret = setgid(ecd._execGid);
		if ( ret != 0 )
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), toolbox::toString
					("child: FATAL couldn't setgid() to %i.\n", ecd._execGid)
			);
		}
	}

	// set new user id
	errno = 0;
	ret = setuid(ecd._execUid);
	if ( ret != 0 ) {
		//Let's try a second time
		errno = 0;
		ret = setuid(ecd._execUid);
		if ( ret != 0 )
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
					("child: FATAL couldn't setuid() to %i.\n",ecd._execUid)
			);
			exit(-1);
		}
	}

	// check for config file existence
	if ( ecd._execConfigFile != "" )
	{
		// write file to /tmp/*.xml
		try
		{
			int fdout = open( ecd._execConfigFilePath.c_str() , O_RDWR|O_CREAT|O_TRUNC, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH );  // open file
			ret = write( fdout, ecd._execConfigFile.c_str() , ecd._execConfigFile.size() );                     // write the data
			if ( ret !=  (int)ecd._execConfigFile.size() )
			{                                                   // check size
				LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString   	                    // failed, make a log message
						("child: FATAL couldn't write config file to %s.\n", ecd._execConfigFilePath.c_str()));
				exit(-1);
			}// exit child process
		}
		catch (xdata::exception::Exception &e)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
					("child: FATAL couldn't write config file to %s.\n", ecd._execConfigFilePath.c_str())
			<< xcept::stdformat_exception_history(e));
			exit(-1);
		}
		catch (xcept::Exception &e)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
					("child: FATAL couldn't write config file to %s.\n", ecd._execConfigFilePath.c_str())
			<< xcept::stdformat_exception_history(e));
			exit(-1);
		}
		catch (std::exception &e)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
					("child: FATAL couldn't write config file to %s.\n", ecd._execConfigFilePath.c_str())
			<< e.what());
			exit(-1);
		}
		catch (...)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
					("child: FATAL couldn't write config file to %s.\n", ecd._execConfigFilePath.c_str()));
			exit(-1);
		}
	}
	mypid = getpid(); // child process
	std::stringstream logPath;
	if (ecd._logFilePath != "")
	{
		p._keepLogFile  = true;
		logPath << ecd._logFilePath << "-" << logdate << "-" << mypid << ".log";
	}
	else
	{
		p._keepLogFile  = false;
		logPath << "/tmp/xdaqjcPID" << mypid << ".log";
	}

	// open procID+log for stdout and stderr


	try
	{
		int tmpout = open( logPath.str().c_str() , O_RDWR|O_CREAT|O_APPEND, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH );    // open file
		dup2( tmpout, 1 );                                         // stdout to file
		dup2( tmpout, 2 );                                         // stderr to file
		close( tmpout );                                       // close unused descriptor
	}
	catch (xdata::exception::Exception &e)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
				("child: FATAL couldn't write log file to %s.\n", ecd._execConfigFilePath.c_str())
		<< xcept::stdformat_exception_history(e));
		exit(-1);
	}
	catch (xcept::Exception &e)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
				("child: FATAL couldn't write log file to %s.\n", ecd._execConfigFilePath.c_str())
		<< xcept::stdformat_exception_history(e));
		exit(-1);
	}
	catch (std::exception &e)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
				("child: FATAL couldn't write log file to %s.\n", ecd._execConfigFilePath.c_str())
		<< e.what());
		exit(-1);
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
				("child: FATAL couldn't write log file to %s.\n", ecd._execConfigFilePath.c_str()));
		exit(-1);
	}


	ret = execve( executivePath, pArgv, pEnvp); 	       // execute the new xdaq ; should never come back!
	LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString
			("jobControl: FATAL OOps, we came back with ret = %i , dying",ret) );
	exit(-1);
}


void jobcontrol::Application::defaultWebPage (xgi::Input *in, xgi::Output *out) 
{
	this->TabPanel(out);
}

void jobcontrol::Application::redirectToDefaultWebPage (xgi::Output *out) 
{

	try
	{
		*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
		*out << cgicc::html().set("lang", "EN").set("dir", "LTR") << std::endl;

		// Set up the page's header and title.
		*out << cgicc::head() << std::endl;
		*out << cgicc::title() << "Redirect to the default web page of JobControl" << cgicc::title() << std::endl;
		*out << cgicc::head() << std::endl;

		// Start the HTML body
		*out << cgicc::body() << std::endl;
		*out << "<meta HTTP-EQUIV=\"REFRESH\" content=\"0; url=" << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN() << "\">" << std::endl;

		// Close the document
		*out << cgicc::body() << cgicc::html();
	}
	catch (xgi::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on redirectToDefaultWebPage method.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on redirectToDefaultWebPage method", e);
	}
	catch (std::exception &se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xgi::exception::Exception, "Exception on redirectToDefaultWebPage method.\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(xgi::exception::Exception, "Unknown exception on redirectToDefaultWebPage method.");
	}
}

//void jobcontrol::Application::processLogWebPage (xgi::Input *in, xgi::Output *out) 
void jobcontrol::Application::getLogFile (xgi::Input *in, xgi::Output *out) 
{
	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::form_iterator logElement = cgi.getElement("log");
		std::string log = "";

		std::string urn = getApplicationDescriptor()->getURN();

		if (logElement != cgi.getElements().end())
		{

			log = (*logElement).getValue();

			//*out << "<div STYLE=\"color: blue\">Logfile: " << log << "</div><br />";

			//*out << "<textarea name=\"log\" style=\"width: 100%; height: 600px;\" readonly=true>";

			std::string line;
			std::ifstream myfile(log.c_str());
			if (myfile.is_open())
			{
				while (!myfile.eof())
				{
					getline(myfile, line);
					*out << line << std::endl;

				}

				//*out << "</textarea>";

				myfile.close();
			}
			else
			{
				*out << "Unable to open file " << log << std::endl;
				//*out << "</textarea>";
			}
		}

	}
	catch (xgi::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on processLogWebPage method.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "XDAQ exception on processLogWebPage method", e);
	}
	catch (std::exception &se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xgi::exception::Exception, "Exception on processLogWebPage method.\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(xgi::exception::Exception, "Unknown exception on processLogWebPage method.");
	}

}

void jobcontrol::Application::processKillWebPage (xgi::Input *in, xgi::Output *out) 
{

	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::form_iterator killElement = cgi.getElement("kill");
		std::string kill = "";

		if (killElement != cgi.getElements().end())
		{

			kill = (*killElement).getValue();

			// kill the pid
			killByPid(stringToInt(kill));

		}
	}
	catch (xoap::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on processKillWebPage method.", e);
	}
	catch (xgi::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on processKillWebPage method.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on processKillWebPage method.", e);
	}
	catch (std::exception &se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xgi::exception::Exception, "Exception on processKillWebPage method.\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(xgi::exception::Exception, "Unknown exception on processKillWebPage method.");
	}

	// redirect to the default web page
	redirectToDefaultWebPage(out);

}

void jobcontrol::Application::processKillAllWebPage (xgi::Input *in, xgi::Output *out) 
{

	try
	{
		// kill all processes
		killAllProcs();
	}
	catch (xoap::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on processKillAllWebPage method.", e);
	}

	// redirect to the default web page
	redirectToDefaultWebPage(out);

}


void jobcontrol::Application::actionPerformed (xdata::Event& e) 
{ 
	try
	{
		if ( e.type() == "urn:xdata-event:ItemGroupRetrieveEvent" )
		{
			xdata::ItemGroupRetrieveEvent & event = dynamic_cast<xdata::ItemGroupRetrieveEvent&>(e);
			if ( event.infoSpace()->name().find("urn:jobcontrol") != std::string::npos )
			{
				mutex_.take();
				this->updateJobTable();
				mutex_.give();
			}
		}
	}
	catch (xcept::Exception &e)
	{
		mutex_.give();
		const std::string msg = "Exception in actionPerformed.";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << xcept::stdformat_exception_history(e));
	}
	catch (std::exception &e)
	{
		mutex_.give();
		const std::string msg = "Exception in actionPerformed.";
		LOG4CPLUS_ERROR(this->getApplicationLogger(), msg << e.what());
	}
	catch (...)
	{
		mutex_.give();
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Unknown Exception in actionPerformed .");
	}
}	



xoap::MessageReference jobcontrol::Application::postSOAP 
(
		xoap::MessageReference message,
		std::string url
)

{


	bool setSOAPAction = false;
	DOMNode* command;

	try {
		if ( message->getMimeHeaders()->getHeader("SOAPAction").size() == 0 )
		{
			message->getMimeHeaders()->setHeader("SOAPAction", url );
			setSOAPAction = true;
		}

		xoap::SOAPBody b = message->getSOAPPart().getEnvelope().getBody();
		DOMNode* node = b.getDOMNode();

		DOMNodeList* bodyList = node->getChildNodes();
		command = bodyList->item(0);
	}
	catch (xoap::exception::Exception &e)
	{
		XCEPT_RETHROW(xdaq::exception::Exception,
				"Failed to get message on postSOAP method.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xdaq::exception::Exception,
				"Failed to get message on postSOAP method.", e);
	}
	catch (std::exception &se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xdaq::exception::Exception,
				"Failed to get message on postSOAP method..\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(xdaq::exception::Exception,
				"Unknown exception: Failed to get message on postSOAP method.");
	}

	if (command->getNodeType() == DOMNode::ELEMENT_NODE)
	{
		try
		{
			// Local dispatch: if remote and local address are on same host, get local messenger

			// Get the address on the fly from the URL
			pt::Address::Reference remoteAddress = pt::getPeerTransportAgent()
			->createAddress(url,"soap");

			pt::Address::Reference localAddress =
					pt::getPeerTransportAgent()->createAddress(getApplicationDescriptor()->getContextDescriptor()->getURL(),"soap");

			// force here protocol http, service soap, because at this point we know over withc protocol/service to send.
			// this allows specifying a host URL without the SOAP service qualifier
			//
			std::string protocol = remoteAddress->getProtocol();

			pt::PeerTransportSender* s = dynamic_cast<pt::PeerTransportSender*>(pt::getPeerTransportAgent()->getPeerTransport (protocol, "soap", pt::Sender));

			// These two lines cannot be merges, since a reference that is a temporary object
			// would delete the contained object pointer immediately after
			//
			pt::Messenger::Reference mr = s->getMessenger(remoteAddress, localAddress);
			pt::SOAPMessenger& m = dynamic_cast<pt::SOAPMessenger&>(*mr);
			xoap::MessageReference rep = m.send(message);

			if (setSOAPAction)
			{
				message->getMimeHeaders()->removeHeader("SOAPAction");
			}
			return rep;
		}
		catch (xdaq::exception::HostNotFound& hnf)
		{
			/*applicationDescriptorFactory_.unlock();
			 */
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post SOAP message", hnf);
		}
		catch (xdaq::exception::ApplicationDescriptorNotFound& acnf)
		{
			/*applicationDescriptorFactory_.unlock();
			 */
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post SOAP message", acnf);
		}
		catch (pt::exception::Exception& pte)
		{
			/*applicationDescriptorFactory_.unlock();
			 */
			XCEPT_RETHROW (xdaq::exception::Exception, "Failed to post SOAP message", pte);
		}
		catch (std::exception& se)
		{
			/*applicationDescriptorFactory_.unlock();
			 */
			std::stringstream oss;
			oss << se.what();

			XCEPT_RAISE (xdaq::exception::Exception, "Failed to post SOAP message" + oss.str());
		}
		catch (...)
		{
			/*applicationDescriptorFactory_.unlock();
			 */
			XCEPT_RAISE (xdaq::exception::Exception, "Failed to post SOAP message, unknown exception");
		}
	} 
	else
	{
		/*applicationDescriptorFactory_.unlock();
		 */
		XCEPT_RAISE (xdaq::exception::Exception, "Bad SOAP message. Cannot find command tag");
	}
	/*applicationDescriptorFactory_.unlock();
	 */
}


void jobcontrol::Application::TabPanel (xgi::Output * out) 
{
	*out << "	<script type=\"text/javascript\" src=\"/jobcontrol/html/js/xdaq-jobcontrol.js\"></script>" << std::endl;

	try
	{
		std::stringstream to;
		to << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << this->getApplicationDescriptor()->getURN();

		if (settings_.getProperty("autorefresh") != "0")
		{
			//*out << "<script>window.onload=function(){window.setTimeout(\"document.location.reload()\"," <<
			*out << "<script>";
			*out << "window.onload=function(){";
			*out << "window.setTimeout(\"document.location.href='" << to.str() << "'\"," << settings_.getProperty("autorefresh") << ");";
			*out << "};";
			*out << "</script>" << std::endl;
		}

		*out << "<div class=\"xdaq-tab-wrapper\" id=\"xdaq-jobcontrol-tabs\">" << std::endl;

		// Tabbed pages

		*out << "<div class=\"xdaq-tab\" title=\"Jobs\" id=\"xdaq-jobcontrol-jobs-tab\">" << std::endl;
		this->JobsTabPage(out);
		*out << "</div>";

		*out << "<div class=\"xdaq-tab\" title=\"Log\" id=\"xdaq-jobcontrol-log-tab\">" << std::endl;
		this->LogTabPage(out);
		*out << "</div>";

		*out << "</div>";
	}
	catch (xgi::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on TabPanel method.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "XDAQ exception on TabPanel method", e);
	}
	catch (std::exception &se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xgi::exception::Exception, "Exception on TabPanel method.\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(xgi::exception::Exception, "Unknown exception on TabPanel method.");
	}
}

void jobcontrol::Application::JobsTabPage (xgi::Output * out) 
{
	try
	{
		std::string urn = getApplicationDescriptor()->getURN();

		toolbox::Properties attributes;
		//*out << getApplicationDescriptor()->getContextDescriptor()->getURL() << std::endl;

		*out << "Jobs requested to start: <br /><br />" << std::endl;

		std::list<_PidUid>::iterator iter = _pidUidList.begin();

		std::string logString = "";

		if (iter != _pidUidList.end())
		{
			*out << "<table class=\"xdaq-table\">" << std::endl;
			*out << "<thead>" << std::endl;

			/*
			logString += "<tr><td bgcolor=\"#FF2222\">";
			logString += "<a href=\"/" + urn + "/ProcKillAll\">";
			logString += "kill all</a>";
			logString += "</td>";
			logString += "<td> <a href=\"/" + urn + "/ProcLog?log=/tmp/xdaqjcPIDlast.log\"> View last log.</a>";
			logString += "</td> </tr>";
			 */
			logString += "<tr>";

			logString += "<th>";
			logString += "PID";
			logString += "</th>";

			logString += "<th>";
			logString += "JID";
			logString += "</th>";

			logString += "<th>";
			logString += "Tag";
			logString += "</th>";

			logString += "<th>";
			logString += "UID";
			logString += "</th>";

			logString += "<th>";
			logString += "User";
			logString += "</th>";

			logString += "<th>";
			logString += "exe";
			logString += "</th>";

			logString += "<th>";
			logString += "Start Time";
			logString += "</th>";

			logString += "<th>";
			logString += "vsize";
			logString += "</th>";

			logString += "<th>";
			logString += "Children";
			logString += "</th>";

			logString += "<th>";
			logString += "State";
			logString += "</th>";

			logString += "<th>";
			logString += "Status";
			logString += "</th>";

			logString += "<th>";
			std::string lastLogName = "/tmp/xdaqjcPIDlast.log";
			logString += "Log File (<a class=\"xdaq-jobcontrol-viewlog\" href=\"/" + urn + "/getLogFile?log=" + lastLogName + "\" data-log-name=\"" + lastLogName + "\"> View last log</a>)";
			logString += "</th>";

			logString += "<th>";
			logString += "Kill (<a href=\"/" + urn + "/ProcKillAll\">Kill All</a>)";
			logString += "</th>";

			logString += "</tr>";
			logString +=  "</thead>";
			logString +=  "<tbody>";

			for (; iter != _pidUidList.end(); iter++)
			{

				logString += "<tr> <td>";
				logString += intToString((*iter)._pid);
				logString += "</td> ";

				std::size_t found = (*iter)._jid.find("http");
				if (found != std::string::npos)
				{
					logString += "<td>";
					logString += "<a href=\"" + (*iter)._jid + "\">";
					logString += (*iter)._jid;
					logString += "<a>";
					logString += "</td>";
				}
				else
				{
					logString += "<td>";
					logString += (*iter)._jid;
					logString += "</td>";
				}

				logString += "<td>";
				logString += (*iter)._tag;
				logString += "</td>";

				logString += "<td>";
				logString += intToString((*iter)._uid);
				logString += "</td>";

				logString += "<td>";
				logString += (*iter)._user;
				logString += "</td>";

				logString += "<td>";
				logString += (*iter)._sexe;
				logString += "</td>";

				// start time
				toolbox::TimeVal tv = toolbox::TimeVal((double) ((*iter)._start_time));
				logString += "<td>";
				logString += tv.toString(toolbox::TimeVal::gmt);
				logString += "</td>";

				// vsize
				logString += "<td>";
				logString += intToString((*iter)._vsize);
				logString += "</td>";

				// child procs
				logString += "<td>";
				logString += intToString((*iter)._n_child_proc);
				if ((*iter)._n_child_proc > 0)
				{
					logString += "<div style=\"color: blue; sans-serif; font-style: normal; font-size: 0.8em;\">";
					logString += (*iter)._child_tree;
					logString += "</div>";
				}
				logString += "</td>";

				// state
				logString += "<td>";
				logString += (*iter)._state;
				logString += "</td>";

				// call kill, but don't deliver signal. just check status.
				int status = ::kill((*iter)._pid, 0);
				if (status == 0)
				{
					logString += "<td style=\"text-align: center;\"> alive";
				}
				else if (status == -1 && errno == ESRCH)
				{
					logString += "<td style=\"text-align: center;\">";
					// put in status info from waitpid()
					logString += "";
					logString += strsignal((*iter)._status);
				}
				else
				{
					logString += "<td style=\"text-align: center;\"> dead";
				}

				logString += "<td style=\"text-align: center;\">";
				std::string logName;
				if ( (*iter)._logFilePath != "" )
				{
					logName = (*iter)._logFilePath;
				}
				else
				{
					logName = "/tmp/xdaqjcPID" + intToString((*iter)._pid) + ".log";
				}

				logString += "<a class=\"xdaq-jobcontrol-viewlog\" href=\"/" + urn + "/getLogFile?log=" + logName + "\" data-log-name=\"" + logName + "\">";
				logString += "view log";
				logString += "</a>";
				logString += "</td>";

				logString += "<td style=\"text-align: center;\">";
				logString += "<a href=\"/" + urn + "/ProcKill?kill=" + intToString((*iter)._pid) + "\">";
				logString += "kill</a>";
				logString += "</td> </tr>";

			}
			logString += "</tbody>";

			*out << logString << std::endl;
			*out << "</table> " << std::endl;
		}
	}
	catch (xgi::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on :JobsTabPage method.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "XDAQ exception on :JobsTabPage method", e);
	}
	catch (std::exception &se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xgi::exception::Exception, "Exception on :JobsTabPage method.\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(xgi::exception::Exception, "Unknown exception on :JobsTabPage method.");
	}
}

void jobcontrol::Application::LogTabPage (xgi::Output * out) 
{
	// binding  on hyperdaq implementation
	std::string log = "file:/tmp/xdaqjcPIDlast.log";
	std::stringstream to;
	to << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << "urn:xdaq-application:service=hyperdaq";

	std::stringstream headerTitle;
	headerTitle << "Log Viewer - <input type=\"text\" style=\"width: 500px;\"id=\"xdaq-log-viewer-file\" value=\"" << log << "\">";
	*out << cgicc::h3(headerTitle.str()) << std::endl;

	*out << "<div id=\"xdaq-logviewer-wrapper\" data-callback=\"" << to.str() << "\">" << std::endl;

	*out << "Lines : <input type=\"text\" id=\"xdaq-log-viewer-length\" value=\"5000\" onChange=\"len=Number(this.value)\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"All\" onClick=\"viewAll()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Head\" onClick=\"viewHead()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Tail\" onClick=\"viewTail()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Forward\" onClick=\"viewForward()\">" << std::endl;
	*out << "<input class=\"xdaq-controlpanel-button\" type=\"button\" value=\"Backward\" onClick=\"viewBackward()\">" << std::endl;

	*out << "<br /><br />" << std::endl;

	*out << "<textarea id=\"xdaq-logviewer-textarea\" style=\"width: 100%; height: 600px;\"></textarea>" << std::endl;

	*out << "</div>";

	*out << "<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-controlpanel-log-viewer.js\"></script>" << std::endl;
}

void jobcontrol::Application::applySettings (xgi::Input * in, xgi::Output * out) 
{
	try
	{
		cgicc::Cgicc cgi(in);

		std::vector < std::string > names = settings_.propertyNames();
		for (std::vector<std::string>::iterator i = names.begin(); i != names.end(); i++)
		{
			if (xgi::Utils::hasFormElement(cgi, (*i)))
			{
				std::string value = xgi::Utils::getFormElement(cgi, (*i))->getValue();
				if (settings_.getProperty(*i) != value)
				{
					if ((*i) == "autorefresh")
					{

					}

					settings_.setProperty((*i), value);
				}

			}
		}
	}
	catch (xgi::exception::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Exception on  applySettings method.", e);
	}
	catch (xcept::Exception &e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "XDAQ exception on applySettings method", e);
	}
	catch (std::exception &se)
	{
		std::stringstream oss;
		oss << se.what();
		XCEPT_RAISE(xgi::exception::Exception, "Exception on applySettings method.\n" + oss.str());
	}
	catch (...)
	{
		XCEPT_RAISE(xgi::exception::Exception, "Unknown exception on applySettings method.");
	}

	this->TabPanel(out);

}
