// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/
#include "ApplicationDescriptor.h"


using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Value;

Persistent<Function> nodejs::ApplicationDescriptor::constructor;

nodejs::ApplicationDescriptor::ApplicationDescriptor()
{
}

nodejs::ApplicationDescriptor::~ApplicationDescriptor() {
}

void nodejs::ApplicationDescriptor::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "ApplicationDescriptor"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);



  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "ApplicationDescriptor"), tpl->GetFunction());
}

void nodejs::ApplicationDescriptor::New(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();

  if (args.IsConstructCall()) {
    // Invoked as constructor: `new ApplicationDescriptor(...)`
    //double value = args[0]->IsUndefined() ? 0 : args[0]->NumberValue();
    ApplicationDescriptor* obj = new ApplicationDescriptor();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    // Invoked as plain function `ApplicationDescriptor(...)`, turn into construct call.
    const int argc = 1;
    Local<Value> argv[argc] = { args[0] };
    Local<Context> context = isolate->GetCurrentContext();
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    Local<Object> result =  cons->NewInstance(context, argc, argv).ToLocalChecked();
    args.GetReturnValue().Set(result);
  }
}

