package xdaq.tools.cluster;

import javax.swing.*;
import java.awt.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.util.*;
import java.net.*;
import java.io.*;

import xdaq.tools.cluster.ClusterTable;
import xdaq.tools.cluster.ClusterLogger;
import xdaq.tools.cluster.CommandHistoryFile;

public class Control extends Box implements ActionListener
{
	private JLabel urlLabel_;
	private JComboBox urlField_;
	private JButton loadButton_;
	private JButton executeButton_;
	private JButton haltButton_;
	private JTable table_;
	private Identification identification_;
	private ClusterLogger logger_;
	private ClusterControl clusterControl_;
	private JCheckBox soundCheckBox_;
	
	private PlaySoundFile connectionFailed_;
	private PlaySoundFile okSound_;
	
	boolean sound_;
	
	public Control(Identification identification)
	{
		super(BoxLayout.Y_AXIS);
	
		sound_ = true;
		connectionFailed_ = new PlaySoundFile ("./audio/itineris.wav");
		okSound_ = new PlaySoundFile ("./audio/OK5.wav");
		
		
		identification_ = identification;
		soundCheckBox_ = identification_.getSoundToggle();
		soundCheckBox_.addActionListener (this);
		
		Box urlBox = new Box (BoxLayout.X_AXIS);
		Box buttonBox = new Box (BoxLayout.X_AXIS);
		
		table_ = new JTable();
		JScrollPane scrollPane = new JScrollPane ( table_ );
		
		urlLabel_ = new JLabel ("URL: ", JLabel.CENTER);
		urlField_ = new JComboBox ();
		urlField_.setEditable(true);
		urlField_.addActionListener(this);
		loadButton_ = new JButton ("Load");
		loadButton_.addActionListener (this);
		
		urlBox.add (urlLabel_);
		urlBox.add (urlField_);
		
		executeButton_ = new JButton ("Execute");
		executeButton_.addActionListener (this);
		haltButton_ = new JButton ("Halt");
		
		haltButton_.addActionListener (this);
		haltButton_.setEnabled(false);
		executeButton_.setEnabled(false);
		
		buttonBox.add (loadButton_);
		buttonBox.add ( executeButton_);
		buttonBox.add ( haltButton_);
		
		this.add (scrollPane);
		this.add (Box.createVerticalStrut(10));
		this.add (urlBox);
		this.add (buttonBox);
		
		
		String[] commandHistory = identification_.retrieveCommandHistory();
		for (int i = 0; i < commandHistory.length; i++) 
		{
			System.out.println ("Read: " + commandHistory[i]);
			urlField_.addItem( commandHistory[i] );
		}
		
	}
	
	
	
	public void actionPerformed (ActionEvent evt) {
		Object source = evt.getSource();
		
		if (source == soundCheckBox_) {
			clusterControl_.setSound ( soundCheckBox_.isSelected() );
			sound_ = soundCheckBox_.isSelected();
		}
		if (source == loadButton_) {
			try {
				ClusterTable table = new ClusterTable ( (String)urlField_.getSelectedItem() );
				table_.setModel ( table );
				clusterControl_ = new ClusterControl (table);
				executeButton_.setEnabled(true);
			} catch (ClusterTableException e) {
				System.out.println ("Load table: " + e);
			}
		}
		if (source == executeButton_) {
		
			try {
				clusterControl_.execute (identification_.username(),
						identification_.password());
				
			} catch (ClusterControlException e) {
				if (sound_ == true) connectionFailed_.play();
					JOptionPane.showMessageDialog (null,
						e.getMessage(), "Cluster control failure", JOptionPane.WARNING_MESSAGE);
						
				if (sound_ == true) okSound_.play();
			}
			executeButton_.setEnabled(false);
			haltButton_.setEnabled(true);
			logger_ = new ClusterLogger (clusterControl_);
		}
		if (source == haltButton_) {
			try {
				clusterControl_.destroy();
			} catch (ClusterControlException e) {
				JOptionPane.showMessageDialog (null,
					e.getMessage(), "Cluster control failure", JOptionPane.WARNING_MESSAGE);	
				if (sound_ == true) okSound_.play();
			}
				
			logger_.kill();
			logger_ = null;
			executeButton_.setEnabled (true);
			haltButton_.setEnabled(false);
		}
		// Manage the url history field
		if (source == urlField_) {
			String url = (String)urlField_.getSelectedItem();
			
			// chek if url is already specified
			for ( int i =0; i< urlField_.getItemCount(); i++ ) {
				if ( urlField_.getItemAt(i).equals(url) )
					return;
			}	
			// new item provided then store to file	
			urlField_.addItem(url);
			
			identification_.storeCommandHistory (url);
		}
	}
}
