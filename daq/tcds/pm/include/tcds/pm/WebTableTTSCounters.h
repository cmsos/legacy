#ifndef _tcds_pm_WebTableTTSCounters_h
#define _tcds_pm_WebTableTTSCounters_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace pm {

    class WebTableTTSCounters : public tcds::utils::WebObject
    {

    public:
      WebTableTTSCounters(std::string const& name,
                          std::string const& description,
                          tcds::utils::Monitor const& monitor,
                          std::string const& itemSetName,
                          std::string const& tabName,
                          size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_WebTableTTSCounters_h
