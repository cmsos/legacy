/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-types.h,v 1.3 2007/10/24 17:36:44 cano Exp $
*/
/** @file fedkit-types.h 
 * Headers used to handle the file-types pain.
 * Documentation can be found at http://cern.ch/cano/fedkit
 */

#ifndef _FEDKIT_TYPES_H_ 
#define _FEDKIT_TYPES_H_
/* include what is needed for definitions of fixed-size types (uint32_t and friends). */
#include <linux/types.h>
#ifdef __KERNEL__
        #include <linux/kernel.h>
        #include <asm/types.h>
#else
	#include <stdio.h>
        #include <stdint.h>
#endif

/* Various versions of printf (eprintf for errors, iprintf for information) */

#ifdef __GNUC__
	#ifdef __KERNEL__
		#define eprintf(format, args...) printk (KERN_ERR format , ## args)
		#define iprintf(format, args...) printk (KERN_INFO format , ## args)
	#else /* defined __KERNEL__ */
		#define eprintf(format, args...) fprintf (stderr, format , ## args)
		#define iprintf(format, args...) printf (format , ## args)
	#endif /* defined __KERNEL__ */
#else /*defined __GNUC__ */ /* if it's not gnu C, let's try the C standard way */
	#ifdef __KERNEL__
		#define eprintf(...) printk (KERN_ERR __VA_ARGS__)
		#define iprintf(...) printk (KERN_INFO __VA_ARGS__)
	#else /* defined __KERNEL__ */
		#define eprintf(...) fprintf (stderr, __VA_ARGS__)
		#define iprintf(...) printf (__VA_ARGS__)
	#endif /* defined __KERNEL__ */
#endif /*defined __GNUC__ */

#ifdef I2ODEBUG
        #ifdef __KERNEL__
                extern unsigned kfedkit_debuglevel;
        #else
                extern unsigned fedkit_debuglevel;
        #endif /* __KERNEL__ */

	#ifdef __GNUC__
		#ifdef __KERNEL__
			#define edprintf(format, args...)\
                                if (kfedkit_debuglevel) printk (KERN_CRIT __FILE__ " :%d:" format , __LINE__ , ## args)
			#define idprintf(format, args...)\
                                if (kfedkit_debuglevel) printk (KERN_CRIT __FILE__ " :%d:" format , __LINE__ , ## args)
		#else /* defined __KERNEL__ */
			#define edprintf(format, args...)\
                                if (fedkit_debuglevel) fprintf (stderr, __FILE__ " :%d:" format , __LINE__ , ## args)
			#define idprintf(format, args...)\
                                if (fedkit_debuglevel) printf (__FILE__ " :%d:" format , __LINE__ , ## args)
		#endif /* defined __KERNEL__ */
	#else /*defined __GNUC__ */ /* if it's not gnu C, let's try the C standard way */
		#ifdef __KERNEL__
			#define edprintf(...) if (kfedkit_debuglevel) printk (KERN_CRIT __VA_ARGS__)
			#define idprintf(...) if (kfedkit_debuglevel) printk (KERN_CRIT __VA_ARGS__)
		#else /* defined __KERNEL__ */
			#define edprintf(...) fprintf (stderr, __VA_ARGS__)
			#define idprintf(...) printf (__VA_ARGS__)
		#endif /* defined __KERNEL__ */
	#endif /*defined __GNUC__ */
#else /* def I2ODEBUG */
	#ifdef __GNUC__
		#define edprintf(args...) {}
		#define idprintf(args...) {}
	#else /*defined __GNUC__ */ /* if it's not gnu C, let's try the C standard way */
		#define edprintf(...) {}
		#define idprintf(...) {}
	#endif /*defined __GNUC__ */
#endif /* def I2ODEBUG */

#endif /* ndef _FEDKIT_TYPES_H_ */
