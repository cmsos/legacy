// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/CompletionQueue.h"

#include "ibvla/Context.h"

#include <sstream>

ibvla::CompletionQueue::CompletionQueue (Context &ctx, ibv_cq *cq)
	: context_(ctx), cq_(cq)
{
}

ibvla::CompletionQueue::~CompletionQueue ()
{
}

ibvla::Context& ibvla::CompletionQueue::getContext ()
{
	return context_;
}

void ibvla::CompletionQueue::resize (int size) 
{
	int result = ibv_resize_cq(cq_, size);
	if (result != 0)
	{
		std::stringstream ss;
		ss << "Failed to resize completion queue for device'" << getContext().getDeviceName() << "'";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}
}

/*
 * ibv_poll_cq() polls the CQ cq for work completions and returns the first num_entries (or all available completions
 * if the CQ contains fewer than this number) in the array wc.
 * The argument wc is a pointer to an array of ibv_wc structs, as defined in <infiniband/verbs.h>.
 *
 * @return number of completed events
 */
unsigned ibvla::CompletionQueue::poll (int num_entries, ibv_wc *wc) 
{
	int result = ibv_poll_cq(cq_, num_entries, wc);

	if (result < 0)
	{
		std::stringstream ss;
		ss << "Failed to poll completion queue for device'" << getContext().getDeviceName() << "'";
		XCEPT_RAISE(ibvla::exception::Exception, ss.str());
	}

	return unsigned(result);

}
