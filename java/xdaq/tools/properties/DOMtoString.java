package xdaq.tools.properties;

import org.w3c.dom.*;
import java.io.StringWriter;
import java.util.Properties;
// import org.apache.xml.serialize.*;
import java.io.*;

public class DOMtoString
{

/* USES XERCES
public static String serialize ( Document doc )
{
	try{
		ByteArrayOutputStream ostream = new ByteArrayOutputStream();
		OutputFormat oformat = new OutputFormat("XML","ISO-8859-1",true);
		oformat.setIndent(1);
		oformat.setIndenting(true);

		XMLSerializer serializer = new XMLSerializer(ostream,oformat);
		// As a DOM Serializer
		serializer.asDOMSerializer();
		serializer.serialize( doc.getDocumentElement()  );
		return ostream.toString();
	}
	catch (	java.io.IOException ex)
	{
		return ex.toString();
	}
}
*/

public static String serialize ( Document doc )
{
	try {
		StringWriter w = new StringWriter();
		DOMWriter serializer = new DOMWriter(w,true);
		serializer.print( doc.getDocumentElement()  );
		return w.toString();
	}
	catch (	java.io.IOException ex)
	{
		return ex.toString();
	}
}

	/*public static String serialize ( Node doc )
	{
	System.out.println ("a");
		StringWriter outText = new StringWriter();
	System.out.println ("b");
		StreamResult sr = new StreamResult ( outText );
	System.out.println ("c");
		Properties oprops = new Properties();
	System.out.println ("d");
		oprops.put (OutputKeys.METHOD, "html");
	System.out.println ("e");
		oprops.put (OutputKeys.INDENT, "yes");
	System.out.println ("f");
		
		TransformerFactory tf = TransformerFactory.newInstance();
	System.out.println ("g");
		Transformer t = null;
	System.out.println ("h");
		try {
			t = tf.newTransformer();
		System.out.println ("i");
			t.setOutputProperties ( oprops );
		System.out.println ("j");
			t.transform ( new DOMSource ( doc ), sr );
		System.out.println ("a");
		} catch (Exception e )
		{
			System.out.println ("Could not serialize dom: " + e.toString());
			return e.toString();
		}
		return outText.toString();
	}
	*/
}
