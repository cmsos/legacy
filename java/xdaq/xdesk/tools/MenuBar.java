package xdaq.xdesk.tools;

import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import org.w3c.dom.*;
import java.awt.*;
import java.io.*;
//import javax.help.*;
import org.apache.log4j.*;
import java.awt.event.*;

public class MenuBar extends JMenuBar 
{
	static Logger logger = Logger.getLogger (MenuBar.class);
	MenuBarListener listener_;
	
	public MenuBar(String name, MenuBarListener listener )
	{
		listener_ = listener;
		JMenu menu;
		JMenuItem item;
		
		// Globe menu
		//		
		menu = new JMenu ();
		
		ImageIcon icon = new ImageIcon (this.getClass().getResource ("/xdaq/xdesk/tools/icons/xdesk16x16.png"));
		menu.setIcon (icon);
		menu.setMnemonic ('x');
		
		// About action
		item = new JMenuItem (  new AbstractAction("About") {
						public void actionPerformed (ActionEvent e) {
							listener_.aboutAction();
						}
					}
				      );
		menu.add (item);
		
		// Switch action
		item = new JMenuItem (  new AbstractAction("Switch...") {
						public void actionPerformed (ActionEvent e) {
							listener_.switchAction();
						}
					}
				      );
		menu.add (item);
		
		// Exit action
		item = new JMenuItem ( new AbstractAction("Exit") {
						public void actionPerformed (ActionEvent e) {
							listener_.exitAction();
						}
					}
				      );
		menu.add (item);
		
		
		
		this.add (menu);
		
		
		// Applet Menu
		menu = new JMenu (name);
		
		// For all applets but xdesk add a quit applet action
		//
		
		item = new JMenuItem ( new AbstractAction("Quit " + name) {
						public void actionPerformed (ActionEvent e) {
							listener_.quitAction();
						}
					}
				      );
		menu.add (item);
		
		
		this.add (menu);
		
		
	}		
	
	
}
