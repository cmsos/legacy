#include <stdint.h>
#include <string>

#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdDisableRandomTriggers<T>::SOAPCmdDisableRandomTriggers(T& controller) :
  SOAPCmdBase<T>(controller, "DisableRandomTriggers", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdDisableRandomTriggers<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Do what we were asked to do.
  this->controller_.getHw().disableRandomTriggers();

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
