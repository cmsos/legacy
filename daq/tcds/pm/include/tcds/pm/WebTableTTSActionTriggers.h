#ifndef _tcds_pm_WebTableTTSActionTriggers_h
#define _tcds_pm_WebTableTTSActionTriggers_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace pm {

    class WebTableTTSActionTriggers : public tcds::utils::WebObject
    {

    public:
      WebTableTTSActionTriggers(std::string const& name,
                                std::string const& description,
                                tcds::utils::Monitor const& monitor,
                                std::string const& itemSetName,
                                std::string const& tabName,
                                size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_WebTableTTSActionTriggers_h
