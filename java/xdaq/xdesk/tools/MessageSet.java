package xdaq.xdesk.tools;

import java.util.*;

import org.apache.log4j.*;

public class MessageSet {
  Logger logger = Logger.getLogger(MessageSet.class);

  Vector messages_;

  public MessageSet() {
    messages_ = new Vector();
  }

  // Get the number of commands stored in the model
  public int size() {
    return messages_.size();
  }

  // Retrieve the i-th command element
  public Message getMessage(int i) {
    return (Message) messages_.get(i);
  }

  // Add a command element
  public void add(Message message) {
    messages_.add(message);
  }

  public void removeAll() {
    messages_.removeAllElements();
  }

  public void remove(Message message) {
    messages_.remove(message);
  }
}
