#include "tcds/apve/Utils.h"

#include <cassert>
#include <map>

std::string
tcds::apve::APVEReadoutModeToString(tcds::definitions::APVE_READOUT_MODE const modeIn)
{
  std::string res = "UNKNOWN";
  switch (modeIn)
    {
    case tcds::definitions::APVE_READOUT_MODE_DECONVOLUTION:
      res = "deconvolution";
      break;
    case tcds::definitions::APVE_READOUT_MODE_PEAK:
      res = "peak";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  return res;
}

std::string
tcds::apve::APVETriggerModeToString(tcds::definitions::APVE_TRIGGER_MODE const modeIn)
{
  std::string res = "UNKNOWN";
  switch (modeIn)
    {
    case tcds::definitions::APVE_TRIGGER_MODE_DECONVOLUTION:
      res = "deconvolution";
      break;
    case tcds::definitions::APVE_TRIGGER_MODE_PEAK_SINGLE:
      res = "peak";
      break;
    default:
      // ASSERT ASSERT ASSERT
      assert (false);
      // ASSERT ASSERT ASSERT end
      break;
    }
  return res;
}

uint8_t
tcds::apve::pipelineAddressToGrayCode(uint8_t const address)
{
  // This is hard-coded since these are 'hard' to calculate.
  std::map<uint8_t, uint8_t> codeMap;
  codeMap[0] = 0x30;
  codeMap[1] = 0x31;
  codeMap[2] = 0x33;
  codeMap[3] = 0x32;
  codeMap[4] = 0x36;
  codeMap[5] = 0x37;
  codeMap[6] = 0x35;
  codeMap[7] = 0x34;
  codeMap[8] = 0x3c;
  codeMap[9] = 0x3d;
  codeMap[10] = 0x3f;
  codeMap[11] = 0x3e;
  codeMap[12] = 0x3a;
  codeMap[13] = 0x3b;
  codeMap[14] = 0x39;
  codeMap[15] = 0x38;
  codeMap[16] = 0x28;
  codeMap[17] = 0x29;
  codeMap[18] = 0x2b;
  codeMap[19] = 0x2a;
  codeMap[20] = 0x2e;
  codeMap[21] = 0x2f;
  codeMap[22] = 0x2d;
  codeMap[23] = 0x2c;
  codeMap[24] = 0x24;
  codeMap[25] = 0x25;
  codeMap[26] = 0x27;
  codeMap[27] = 0x26;
  codeMap[28] = 0x22;
  codeMap[29] = 0x23;
  codeMap[30] = 0x21;
  codeMap[31] = 0x20;
  codeMap[32] = 0x60;
  codeMap[33] = 0x61;
  codeMap[34] = 0x63;
  codeMap[35] = 0x62;
  codeMap[36] = 0x66;
  codeMap[37] = 0x67;
  codeMap[38] = 0x65;
  codeMap[39] = 0x64;
  codeMap[40] = 0x6c;
  codeMap[41] = 0x6d;
  codeMap[42] = 0x6f;
  codeMap[43] = 0x6e;
  codeMap[44] = 0x6a;
  codeMap[45] = 0x6b;
  codeMap[46] = 0x69;
  codeMap[47] = 0x68;
  codeMap[48] = 0x78;
  codeMap[49] = 0x79;
  codeMap[50] = 0x7b;
  codeMap[51] = 0x7a;
  codeMap[52] = 0x7e;
  codeMap[53] = 0x7f;
  codeMap[54] = 0x7d;
  codeMap[55] = 0x7c;
  codeMap[56] = 0x74;
  codeMap[57] = 0x75;
  codeMap[58] = 0x77;
  codeMap[59] = 0x76;
  codeMap[60] = 0x72;
  codeMap[61] = 0x73;
  codeMap[62] = 0x71;
  codeMap[63] = 0x70;
  codeMap[64] = 0x50;
  codeMap[65] = 0x51;
  codeMap[66] = 0x53;
  codeMap[67] = 0x52;
  codeMap[68] = 0x56;
  codeMap[69] = 0x57;
  codeMap[70] = 0x55;
  codeMap[71] = 0x54;
  codeMap[72] = 0x5c;
  codeMap[73] = 0x5d;
  codeMap[74] = 0x5f;
  codeMap[75] = 0x5e;
  codeMap[76] = 0x5a;
  codeMap[77] = 0x5b;
  codeMap[78] = 0x59;
  codeMap[79] = 0x58;
  codeMap[80] = 0x48;
  codeMap[81] = 0x49;
  codeMap[82] = 0x4b;
  codeMap[83] = 0x4a;
  codeMap[84] = 0x4e;
  codeMap[85] = 0x4f;
  codeMap[86] = 0x4d;
  codeMap[87] = 0x4c;
  codeMap[88] = 0x44;
  codeMap[89] = 0x45;
  codeMap[90] = 0x47;
  codeMap[91] = 0x46;
  codeMap[92] = 0x42;
  codeMap[93] = 0x43;
  codeMap[94] = 0x41;
  codeMap[95] = 0x40;
  codeMap[96] = 0xc0;
  codeMap[97] = 0xc1;
  codeMap[98] = 0xc3;
  codeMap[99] = 0xc2;
  codeMap[100] = 0xc6;
  codeMap[101] = 0xc7;
  codeMap[102] = 0xc5;
  codeMap[103] = 0xc4;
  codeMap[104] = 0xcc;
  codeMap[105] = 0xcd;
  codeMap[106] = 0xcf;
  codeMap[107] = 0xce;
  codeMap[108] = 0xca;
  codeMap[109] = 0xcb;
  codeMap[110] = 0xc9;
  codeMap[111] = 0xc8;
  codeMap[112] = 0xd8;
  codeMap[113] = 0xd9;
  codeMap[114] = 0xdb;
  codeMap[115] = 0xda;
  codeMap[116] = 0xde;
  codeMap[117] = 0xdf;
  codeMap[118] = 0xdd;
  codeMap[119] = 0xdc;
  codeMap[120] = 0xd4;
  codeMap[121] = 0xd5;
  codeMap[122] = 0xd7;
  codeMap[123] = 0xd6;
  codeMap[124] = 0xd2;
  codeMap[125] = 0xd3;
  codeMap[126] = 0xd1;
  codeMap[127] = 0xd0;
  codeMap[128] = 0xf0;
  codeMap[129] = 0xf1;
  codeMap[130] = 0xf3;
  codeMap[131] = 0xf2;
  codeMap[132] = 0xf6;
  codeMap[133] = 0xf7;
  codeMap[134] = 0xf5;
  codeMap[135] = 0xf4;
  codeMap[136] = 0xfc;
  codeMap[137] = 0xfd;
  codeMap[138] = 0xff;
  codeMap[139] = 0xfe;
  codeMap[140] = 0xfa;
  codeMap[141] = 0xfb;
  codeMap[142] = 0xf9;
  codeMap[143] = 0xf8;
  codeMap[144] = 0xe8;
  codeMap[145] = 0xe9;
  codeMap[146] = 0xeb;
  codeMap[147] = 0xea;
  codeMap[148] = 0xee;
  codeMap[149] = 0xef;
  codeMap[150] = 0xed;
  codeMap[151] = 0xec;
  codeMap[152] = 0xe4;
  codeMap[153] = 0xe5;
  codeMap[154] = 0xe7;
  codeMap[155] = 0xe6;
  codeMap[156] = 0xe2;
  codeMap[157] = 0xe3;
  codeMap[158] = 0xe1;
  codeMap[159] = 0xe0;
  codeMap[160] = 0xa0;
  codeMap[161] = 0xa1;
  codeMap[162] = 0xa3;
  codeMap[163] = 0xa2;
  codeMap[164] = 0xa6;
  codeMap[165] = 0xa7;
  codeMap[166] = 0xa5;
  codeMap[167] = 0xa4;
  codeMap[168] = 0xac;
  codeMap[169] = 0xad;
  codeMap[170] = 0xaf;
  codeMap[171] = 0xae;
  codeMap[172] = 0xaa;
  codeMap[173] = 0xab;
  codeMap[174] = 0xa9;
  codeMap[175] = 0xa8;
  codeMap[176] = 0xb8;
  codeMap[177] = 0xb9;
  codeMap[178] = 0xbb;
  codeMap[179] = 0xba;
  codeMap[180] = 0xbe;
  codeMap[181] = 0xbf;
  codeMap[182] = 0xbd;
  codeMap[183] = 0xbc;
  codeMap[184] = 0xb4;
  codeMap[185] = 0xb5;
  codeMap[186] = 0xb7;
  codeMap[187] = 0xb6;
  codeMap[188] = 0xb2;
  codeMap[189] = 0xb3;
  codeMap[190] = 0xb1;
  codeMap[191] = 0xb0;

  return codeMap[address];
}
