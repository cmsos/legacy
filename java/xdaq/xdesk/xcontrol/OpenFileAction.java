package xdaq.xdesk.xcontrol;

import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.*;
import java.util.*;
import java.lang.*;
import java.io.*;

import org.apache.log4j.*;

public class OpenFileAction extends AbstractAction 
{
	Logger logger = Logger.getLogger (OpenFileAction.class);

	JFileChooser fc_;
	XControlViewer control_;
	
	public OpenFileAction ()
	{
		super ("Open");
		control_ = null;
		
	}
	
	public OpenFileAction (XControlViewer control)
	{
		super ("Open");
		control_ = control;
		openChooser();
	}
	
	public void openChooser ()
	{
		String startDir = System.getProperty("user.dir");
		fc_ = new JFileChooser (new File (startDir));
		fc_.setFileFilter (new SuffixFilter ("xml", "XML file"));
	}
	
	public void actionPerformed (ActionEvent e)
	{	
		if (control_ == null)
		{
			//control_ = new XControlWindow();
			//openChooser ();
			//control_.show();
			logger.fatal ("Open file on empty XControlViewer. Must never happen");
		}
	
		int retVal = fc_.showOpenDialog ( null );
		if (retVal == JFileChooser.APPROVE_OPTION) 
		{
			File dir = fc_.getCurrentDirectory ();
			File fname = fc_.getSelectedFile();
			if ((fname != null) && (!fname.isDirectory() )) 
			{
				this.fileSelected (fname);
				
			}
		}
	}
	
	
	public void fileSelected (File f)
	{
		//try {
			control_.displayConfiguration ( "file:" + f.toString() );
			
		//} catch (xdaqWinException e)
		//{
			// do nothing
		//}
	}

}
