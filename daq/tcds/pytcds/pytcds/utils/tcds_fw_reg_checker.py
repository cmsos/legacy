#!/usr/bin/env python

###############################################################################
## Utility to check for double-assignments in the TCDS firmware VHDL code.
## NOTE: This is very much a hands-on expert tool.
###############################################################################

import re
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class AssignmentInfo(object):

    def __init__(self, reg_name, reg_word, reg_bit):
        self.reg_name = reg_name
        self.reg_word = reg_word
        self.reg_bit = reg_bit
        # End of __init__().

    def __str__(self):
        tmp = "Register '{0:s}', word {1:d}, bit {2:d}"
        res = tmp.format(self.reg_name, self.reg_word, self.reg_bit)
        # End of __str__().
        return res

    def __eq__(self, other):
        res = (self.reg_name == other.reg_name) \
              and (self.reg_word == other.reg_word) \
              and (self.reg_bit == other.reg_bit)
        # End of __eq__().
        return res

    def __ne__(self, other):
        return not self.__eq__(other)

    # NOTE: The following is very fragile. (And possibly technically
    # not even the right thing to do.)
    def __hash__(self):
        return hash(str(self))

    # End of class AssignmentInfo.

###############################################################################

class Checker(CmdLineBase):

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def connection_mapper(self, raw_lines, check_inputs):

        group_indices = {
            'in': {
                'name': 3,
                'word': 4,
                'bits': 5
                },
            'out': {
                'name': 2,
                'word': 3,
                'bits': 4
                }
            }
        if check_inputs:
            regex_str = "^(.*)<=(.*)(gp_regs_.*)\(([0-9]+?)\).*?\((.*?)\)(.*)$"
            check_type = 'in'
        else:
            regex_str = "^(.*)(gp_regs_.*)\(([0-9]+?)\).*?\((.*?)\).*<=(.*)$"
            check_type = 'out'

        regex = re.compile(regex_str)
        index_name = group_indices[check_type]['name']
        index_word = group_indices[check_type]['word']
        index_bits = group_indices[check_type]['bits']

        register_assignments = {}
        for line_raw in raw_lines:
            line = line_raw.strip()

            match = regex.match(line)
            if match:

                # print self.sep_line
                # print line

                # Do a few stupidity checks to catch 'editing
                # anomalies.'
                if line.count("<=") > 1:
                    # Problem.
                    pdb.set_trace()
                if line.startswith("--"):
                    # Comment.
                    continue

                register_name = match.group(index_name)
                register_number = int(match.group(index_word))
                bits_string = match.group(index_bits)
                if bits_string.find("downto") > -1:
                    regex_str_bits = "^.*?([0-9]+).*downto.*?([0-9]+).*$"
                    regex_bits = re.compile(regex_str_bits)
                    match = regex_bits.match(bits_string)
                    if not match:
                        # Problem.
                        pdb.set_trace()
                    bit_lo = int(match.group(2))
                    bit_hi = int(match.group(1))
                    bits = range(bit_lo, bit_hi + 1)
                else:
                    bit = int(bits_string)
                    bits = [bit]

                for register_bit in bits:
                    reg_key = AssignmentInfo(register_name, register_number, register_bit)
                    # print "  register {0:s}".format(reg_key)
                    try:
                        register_assignments[reg_key].append(line)
                    except KeyError:
                        register_assignments[reg_key] = [line]

        # End of connection_mapper().
        return register_assignments

    def main(self):

        # # LPM.
        # vhdl_file_name = "/afs/cern.ch/work/j/jhegeman/cms_tcds/firmware/cms_tcds/boards/lpm/trunk/fc7/src/usr/user_logic_lpm.vhd"
        # PI.
        vhdl_file_name = "/afs/cern.ch/work/j/jhegeman/cms_tcds/firmware/cms_tcds/boards/pi/trunk/fc7/src/usr/user_logic_pi.vhd"
        vhdl_file_name = "/afs/cern.ch/user/j/jhegeman/user_logic_pi.vhd"
        # # PhaseMon.
        # vhdl_file_name = "/afs/cern.ch/work/j/jhegeman/cms_tcds/firmware/cms_tcds/boards/phasemon/trunk/fc7/src/usr/user_logic_phasemon.vhd"

        # Check input connections.
        print "Reading VHDL file '{0:s}'.".format(vhdl_file_name)
        raw_lines = []
        with open(vhdl_file_name) as in_file:
            for line_raw in in_file:
                raw_lines.append(line_raw)
        print "  Found {0:d} lines.".format(len(raw_lines))

        print "Splitting lines."
        tmp = []
        for line_raw in raw_lines:
            lines = line_raw.split(";")
            tmp.extend(lines)
        raw_lines = tmp
        print "  Now there are {0:d} lines.".format(len(raw_lines))

        print "Processing VHDL."

        # Find double input assignments.
        print "  Checking for double input assignments."
        register_assignments_in = self.connection_mapper(raw_lines, check_inputs=True)
        double_assignments_in = []
        for (assignment, lines) in sorted(register_assignments_in.iteritems(),
                                          key=lambda (x, y): (x.reg_name, x.reg_word, x.reg_bit)):
            if len(lines) != 1:
                double_assignments_in.append((assignment, lines))

        print self.sep_line
        if len(double_assignments_in):
            print "Double input assignments:"
            for tmp in sorted(double_assignments_in):
                print "  {0:s} is assigned in the following lines:".format(tmp[0])
                for line in tmp[1]:
                    print "    {0:s}".format(line)
        else:
            print "No double input assignments found."
        print self.sep_line

        # Find double output assignments.
        print "  Checking for double output assignments."
        register_assignments_out = self.connection_mapper(raw_lines, check_inputs=False)
        double_assignments_out = []
        for (assignment, lines) in sorted(register_assignments_out.iteritems(),
                                          key=lambda (x, y): (x.reg_name, x.reg_word, x.reg_bit)):
            if len(lines) != 1:
                double_assignments_out.append((assignment, lines))

        print self.sep_line
        if len(double_assignments_out):
            print "Double output assignments:"
            for tmp in sorted(double_assignments_out):
                print "  {0:s} is assigned in the following lines:".format(tmp[0])
                for line in tmp[1]:
                    print "    {0:s}".format(line)
        else:
            print "No double output assignments found."
        print self.sep_line

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Utility to check for double-assignments" \
               + " in the TCDS firmware VHDL code." \
               + " NOTE: This is very much a hands-on expert tool."

    res = Checker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
