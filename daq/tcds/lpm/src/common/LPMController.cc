#include "tcds/lpm/LPMController.h"

#include <stdexcept>
#include <stdint.h>
#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/exception/ApplicationInstantiationFailed.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/DeviceBase.h"
#include "tcds/hwlayer/Utils.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceHandler.h"
#include "tcds/hwutilstca/CyclicGensInfoSpaceUpdater.h"
#include "tcds/hwutilstca/Utils.h"
#include "tcds/hwutilstca/HwIDInfoSpaceHandlerTCA.h"
#include "tcds/hwutilstca/HwIDInfoSpaceUpdaterTCA.h"
#include "tcds/lpm/ConfigurationInfoSpaceHandler.h"
#include "tcds/lpm/HwStatusInfoSpaceHandler.h"
#include "tcds/lpm/HwStatusInfoSpaceUpdater.h"
#include "tcds/lpm/LPMInputsInfoSpaceHandler.h"
#include "tcds/lpm/LPMInputsInfoSpaceUpdater.h"
#include "tcds/lpm/TCADeviceLPM.h"
#include "tcds/lpm/version.h"
#include "tcds/pm/ActionsInfoSpaceHandler.h"
#include "tcds/pm/ActionsInfoSpaceUpdater.h"
#include "tcds/pm/CountersInfoSpaceHandler.h"
#include "tcds/pm/CountersInfoSpaceUpdater.h"
#include "tcds/pm/Definitions.h"
#include "tcds/pm/DAQInfoSpaceHandler.h"
#include "tcds/pm/DAQInfoSpaceUpdater.h"
#include "tcds/pm/ReTriInfoSpaceHandler.h"
#include "tcds/pm/ReTriInfoSpaceUpdater.h"
#include "tcds/pm/SchedulingInfoSpaceHandler.h"
#include "tcds/pm/SchedulingInfoSpaceUpdater.h"
#include "tcds/pm/SequencesInfoSpaceHandler.h"
#include "tcds/pm/SequencesInfoSpaceUpdater.h"
#include "tcds/pm/TTSCountersInfoSpaceHandler.h"
#include "tcds/pm/TTSCountersInfoSpaceUpdater.h"
#include "tcds/pm/TTSInfoSpaceHandler.h"
#include "tcds/pm/TTSInfoSpaceUpdater.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"

XDAQ_INSTANTIATOR_IMPL(tcds::lpm::LPMController);

tcds::lpm::LPMController::LPMController(xdaq::ApplicationStub* stub)
try
  :
  tcds::utils::XDAQAppWithFSMForPMs(stub,
                                    std::auto_ptr<tcds::hwlayer::DeviceBase>(new tcds::lpm::TCADeviceLPM())),
    actionsInfoSpaceUpdaterP_(0),
    actionsInfoSpaceP_(0),
    countersInfoSpaceUpdaterP_(0),
    countersInfoSpaceP_(0),
    cyclicGensInfoSpaceUpdaterP_(0),
    cyclicGensInfoSpaceP_(0),
    daqInfoSpaceUpdaterP_(0),
    daqInfoSpaceP_(0),
    hwIDInfoSpaceUpdaterP_(0),
    hwIDInfoSpaceP_(0),
    hwStatusInfoSpaceUpdaterP_(0),
    hwStatusInfoSpaceP_(0),
    inputsInfoSpaceUpdaterP_(0),
    inputsInfoSpaceP_(0),
    retriInfoSpaceUpdaterP_(0),
    retriInfoSpaceP_(0),
    schedulingInfoSpaceUpdaterP_(0),
    schedulingInfoSpaceP_(0),
    sequencesInfoSpaceUpdaterP_(0),
    sequencesInfoSpaceP_(0),
    ttsCountersInfoSpaceUpdaterP_(0),
    ttsCountersInfoSpaceP_(0),
    ttsInfoSpaceUpdaterP_(0),
    ttsInfoSpaceP_(0),
    soapCmdDisableCyclicGenerator_(*this),
    soapCmdDisableRandomTriggers_(*this),
    soapCmdDumpHardwareState_(*this),
    soapCmdEnableCyclicGenerator_(*this),
    soapCmdEnableRandomTriggers_(*this),
    soapCmdInitCyclicGenerator_(*this),
    soapCmdInitCyclicGenerators_(*this),
    soapCmdReadHardwareConfiguration_(*this),
    soapCmdSendBgo_(*this),
    soapCmdSendBgoTrain_(*this),
    soapCmdSendL1A_(*this),
    controllerHelper_(*this, getHw())
    {
      // Create the InfoSpace holding all configuration information.
      cfgInfoSpaceP_ =
        std::auto_ptr<tcds::lpm::ConfigurationInfoSpaceHandler>(new tcds::lpm::ConfigurationInfoSpaceHandler(*this));

      // Make sure the correct default hardware configuration file is found.
      cfgInfoSpaceP_->setString("defaultHwConfigurationFilePath",
                                "${XDAQ_ROOT}/etc/tcds/lpm/hw_cfg_default_lpm.txt");
    }
catch (tcds::exception::Exception const& err)
  {
    std::string msgBase = "Something went wrong instantiating the LPMController application";
    std::string msg = toolbox::toString("%s: '%s'.", msgBase.c_str(), err.what());
    XCEPT_RAISE(xdaq::exception::ApplicationInstantiationFailed, msg.c_str());
  }

tcds::lpm::LPMController::~LPMController()
{
  hwRelease();
}

void
tcds::lpm::LPMController::setupInfoSpaces()
{
  // Make sure the configuration settings are up-to-date.
  cfgInfoSpaceP_->readInfoSpace();

  // Instantiate all hardware-dependent InfoSpaceHandlers and InfoSpaceUpdaters.
  actionsInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::ActionsInfoSpaceUpdater>(new tcds::pm::ActionsInfoSpaceUpdater(*this, getHw()));
  actionsInfoSpaceP_ =
    std::auto_ptr<tcds::pm::ActionsInfoSpaceHandler>(new tcds::pm::ActionsInfoSpaceHandler(*this, actionsInfoSpaceUpdaterP_.get(), false));
  countersInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::CountersInfoSpaceUpdater>(new tcds::pm::CountersInfoSpaceUpdater(*this, getHw()));
  countersInfoSpaceP_ =
    std::auto_ptr<tcds::pm::CountersInfoSpaceHandler>(new tcds::pm::CountersInfoSpaceHandler(*this, countersInfoSpaceUpdaterP_.get()));
  cyclicGensInfoSpaceUpdaterP_ = std::auto_ptr<tcds::hwutilstca::CyclicGensInfoSpaceUpdater>(new tcds::hwutilstca::CyclicGensInfoSpaceUpdater(*this, getHw()));
  cyclicGensInfoSpaceP_ =
    std::auto_ptr<tcds::hwutilstca::CyclicGensInfoSpaceHandler>(new tcds::hwutilstca::CyclicGensInfoSpaceHandler(*this, cyclicGensInfoSpaceUpdaterP_.get(), tcds::definitions::kNumCyclicGensPerPM));
  daqInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::DAQInfoSpaceUpdater>(new tcds::pm::DAQInfoSpaceUpdater(*this, getHw()));
  daqInfoSpaceP_ =
    std::auto_ptr<tcds::pm::DAQInfoSpaceHandler>(new tcds::pm::DAQInfoSpaceHandler(*this, daqInfoSpaceUpdaterP_.get()));
  hwIDInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA>(new tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA(*this, getHw()));
  hwIDInfoSpaceP_ =
    std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA>(new tcds::hwutilstca::HwIDInfoSpaceHandlerTCA(*this, hwIDInfoSpaceUpdaterP_.get()));
  hwStatusInfoSpaceUpdaterP_ =
    std::auto_ptr<tcds::lpm::HwStatusInfoSpaceUpdater>(new tcds::lpm::HwStatusInfoSpaceUpdater(*this, getHw()));
  hwStatusInfoSpaceP_ =
    std::auto_ptr<tcds::lpm::HwStatusInfoSpaceHandler>(new tcds::lpm::HwStatusInfoSpaceHandler(*this, hwStatusInfoSpaceUpdaterP_.get()));
  inputsInfoSpaceUpdaterP_ = std::auto_ptr<tcds::lpm::LPMInputsInfoSpaceUpdater>(new tcds::lpm::LPMInputsInfoSpaceUpdater(*this, getHw()));
  inputsInfoSpaceP_ =
    std::auto_ptr<tcds::lpm::LPMInputsInfoSpaceHandler>(new tcds::lpm::LPMInputsInfoSpaceHandler(*this, inputsInfoSpaceUpdaterP_.get()));
  retriInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::ReTriInfoSpaceUpdater>(new tcds::pm::ReTriInfoSpaceUpdater(*this, getHw()));
  retriInfoSpaceP_ =
    std::auto_ptr<tcds::pm::ReTriInfoSpaceHandler>(new tcds::pm::ReTriInfoSpaceHandler(*this, retriInfoSpaceUpdaterP_.get()));
  schedulingInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::SchedulingInfoSpaceUpdater>(new tcds::pm::SchedulingInfoSpaceUpdater(*this, getHw()));
  schedulingInfoSpaceP_ =
    std::auto_ptr<tcds::pm::SchedulingInfoSpaceHandler>(new tcds::pm::SchedulingInfoSpaceHandler(*this, schedulingInfoSpaceUpdaterP_.get()));
  sequencesInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::SequencesInfoSpaceUpdater>(new tcds::pm::SequencesInfoSpaceUpdater(*this, getHw()));
  sequencesInfoSpaceP_ =
    std::auto_ptr<tcds::pm::SequencesInfoSpaceHandler>(new tcds::pm::SequencesInfoSpaceHandler(*this, sequencesInfoSpaceUpdaterP_.get()));
  ttsCountersInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::TTSCountersInfoSpaceUpdater>(new tcds::pm::TTSCountersInfoSpaceUpdater(*this, getHw()));
  ttsCountersInfoSpaceP_ =
    std::auto_ptr<tcds::pm::TTSCountersInfoSpaceHandler>(new tcds::pm::TTSCountersInfoSpaceHandler(*this, ttsCountersInfoSpaceUpdaterP_.get(), false));
  ttsInfoSpaceUpdaterP_ = std::auto_ptr<tcds::pm::TTSInfoSpaceUpdater>(new tcds::pm::TTSInfoSpaceUpdater(*this, getHw()));
  ttsInfoSpaceP_ =
    std::auto_ptr<tcds::pm::TTSInfoSpaceHandler>(new tcds::pm::TTSInfoSpaceHandler(*this, ttsInfoSpaceUpdaterP_.get(), false));

  // Register all InfoSpaceItems with the Monitor.
  cfgInfoSpaceP_->registerItemSets(monitor_, webServer_);
  appStateInfoSpace_.registerItemSets(monitor_, webServer_);
  hwIDInfoSpaceP_->registerItemSets(monitor_, webServer_);
  hwStatusInfoSpaceP_->registerItemSets(monitor_, webServer_);
  inputsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  schedulingInfoSpaceP_->registerItemSets(monitor_, webServer_);
  sequencesInfoSpaceP_->registerItemSets(monitor_, webServer_);
  actionsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  cyclicGensInfoSpaceP_->registerItemSets(monitor_, webServer_);
  retriInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttsInfoSpaceP_->registerItemSets(monitor_, webServer_);
  ttsCountersInfoSpaceP_->registerItemSets(monitor_, webServer_);
  countersInfoSpaceP_->registerItemSets(monitor_, webServer_);
  daqInfoSpaceP_->registerItemSets(monitor_, webServer_);

  // Force a write of all TTS InfoSpaces. This triggers an XMAS update
  // in order to populate the flashlists such that other applications
  // can pick them up.
  ttsInfoSpaceP_->writeInfoSpace(true);
}

tcds::lpm::TCADeviceLPM&
tcds::lpm::LPMController::getHw() const
{
  return static_cast<tcds::lpm::TCADeviceLPM&>(*hwP_.get());
}

void
tcds::lpm::LPMController::hwConnectImpl()
{
  tcds::hwutilstca::tcaDeviceHwConnectImpl(*cfgInfoSpaceP_, getHw());
}

void
tcds::lpm::LPMController::hwReleaseImpl()
{
  getHw().hwRelease();
}

void
tcds::lpm::LPMController::hwCfgInitializeImpl()
{
  // Check for the presence of the TTC clock. Without TTC clock it is
  // no use continuing (and apart from that, some registers will not
  // be accessible).
  if (!(getHw().isTTCClockUp() && getHw().isTTCClockStable()))
    {
      std::string const msg = "Could not configure the hardware: no TTC clock present, or clock not stable.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCClockProblem, msg.c_str());
    }

  controllerHelper_.hwCfgInitialize();
}

void
tcds::lpm::LPMController::hwCfgFinalizeImpl()
{
  // Check for a valid orbit signal. Without that, it's no use
  // continuing.
  if (!getHw().isOrbitSignalOk())
    {
      std::string const msg = "Could not configure the hardware: no orbit signal present, or orbit signal unstable/incorrect length.";
      ERROR(msg);
      XCEPT_RAISE(tcds::exception::TTCOrbitProblem, msg.c_str());
    }

  //----------

  // The LPM does not have an APVE, so let's disable the PM APVE TTS
  // input.
  getHw().writeRegister("main.inselect.apve_enable", 0);

  //----------

  controllerHelper_.hwCfgFinalize();

  // Configure the trigger BX-alignment registers with the right
  // values.
  getHw().configureTriggerAlignment();

  // And, special for the LPM, configure the external-trigger sources.
  getHw().configureExternalTriggerSettings();

  // Configure the software version, in case anyone is interested.
  getHw().setSoftwareVersion(getSwVersion());

  // Enable/disable DAQ backpressure based on whether we're going to
  // be read out or not.
  std::string const fedEnableMaskStr = cfgInfoSpaceP_->getString("fedEnableMask");
  tcds::hwlayer::fedEnableMask fedEnableMask =
    tcds::hwlayer::parseFedEnableMask(fedEnableMaskStr);
  uint32_t const fedId =
    cfgInfoSpaceP_->getUInt32("fedId");
  uint8_t mask = 0x0;
  try
    {
      mask = fedEnableMask.at(fedId);
    }
  catch (std::out_of_range)
    {
      std::string msg =
        toolbox::toString("Expected FED ID %d in the FED vector but could not find it.",
                          fedId);
      XCEPT_RAISE(tcds::exception::ValueError, msg.c_str());
    }
  bool const useDAQLink = ((mask & 0x5) == 0x1);
  if (useDAQLink)
    {
      getHw().enableDAQBackpressure();
    }
  else
    {
      getHw().disableDAQBackpressure();
    }
}

void
tcds::lpm::LPMController::coldResetActionImpl(toolbox::Event::Reference event)
{
  // Explicitly do nothing. The LPM is a shared piece of
  // hardware. Cannot just colReset that.
}

// void
// tcds::lpm::LPMController::configureActionImpl(toolbox::Event::Reference event)
// {
// }

void
tcds::lpm::LPMController::enableActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.enableAction(event);
}

// void
// tcds::lpm::LPMController::failActionImpl(toolbox::Event::Reference event)
// {
// }

// void
// tcds::lpm::LPMController::haltActionImpl(toolbox::Event::Reference event)
// {
//   controllerHelper_.haltAction(event);
// }

void
tcds::lpm::LPMController::pauseActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.pauseAction(event);
}

void
tcds::lpm::LPMController::resumeActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.resumeAction(event);
}

void
tcds::lpm::LPMController::stopActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.stopAction(event);
}

void
tcds::lpm::LPMController::ttcHardResetActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.ttcHardResetAction(event);
}

void
tcds::lpm::LPMController::ttcResyncActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.ttcResyncAction(event);
}

void
tcds::lpm::LPMController::zeroActionImpl(toolbox::Event::Reference event)
{
  controllerHelper_.zeroAction(event);

  // BUG BUG BUG
  // Force a write of all TTS InfoSpaces. This triggers an XMAS update
  // in order to populate the flashlists such that other applications
  // can pick them up.
  ttsInfoSpaceP_->writeInfoSpace(true);
  // BUG BUG BUG end
}

bool
tcds::lpm::LPMController::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // Start by applying the default selection.
  bool res = tcds::utils::XDAQAppBase::isRegisterAllowed(regInfo);

  // Then the XPM-common selection.
  res = res && controllerHelper_.isRegisterAllowed(regInfo);

  // There is no APVE in the LPM, so we should not fiddle with this
  // register.
  res = res && (regInfo.name().find("inselect.apve_enable") == std::string::npos);

  return res;
}

std::vector<tcds::utils::FSMSOAPParHelper>
tcds::lpm::LPMController::expectedFSMSoapParsImpl(std::string const& commandName) const
{
  // Define what we expect in terms of parameters for each SOAP FSM
  // command.
  std::vector<tcds::utils::FSMSOAPParHelper> params =
    tcds::utils::XDAQAppWithFSMForPMs::expectedFSMSoapParsImpl(commandName);

  // Configure for the takes the usual parameters as well as
  // 'xdaq:fedEnableMask'.
  if ((commandName == "Configure") ||
      (commandName == "Reconfigure"))
    {
      params.push_back(tcds::utils::FSMSOAPParHelper(commandName, "fedEnableMask", true));
    }
  return params;
}

void
tcds::lpm::LPMController::loadSOAPCommandParameterImpl(xoap::MessageReference const& msg,
                                                       tcds::utils::FSMSOAPParHelper const& param)
{
  // NOTE: The assumption is that when entering this method, the
  // command-to-parameter mapping, required parameter presence,
  // etc. have all been checked already. This method only concerns the
  // actual loading of the parameters.

  std::string const parName = param.parameterName();

  if (parName == "fedEnableMask")
    {
      // Import 'xdaq:fedEnableMask'.
      std::string fedEnableMask = "";
      fedEnableMask = tcds::utils::soap::extractSOAPCommandParameterString(msg, parName);
      cfgInfoSpaceP_->setString(parName, fedEnableMask);
    }
  else
    {
      // For other commands: use the usual approach.
      tcds::utils::XDAQAppWithFSMForPMs::loadSOAPCommandParameterImpl(msg, param);
    }
}

uint32_t
tcds::lpm::LPMController::getSwVersion() const
{
  // NOTE: No checks on ranges are made before masking. Gonna break at
  // some point...
  uint32_t const swVersion =
    ((TCDSLPM_VERSION_MAJOR & 0xff) << 24) +
    ((TCDSLPM_VERSION_MINOR & 0xfff) << 12) +
    (TCDSLPM_VERSION_PATCH & 0xfff);
  return swVersion;
}
