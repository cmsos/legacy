#ifndef _tcds_cpm_CPMInputsInfoSpaceUpdater_h_
#define _tcds_cpm_CPMInputsInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cpm {

    class TCADeviceCPMT1;
    class TCADeviceCPMT2;

    class CPMInputsInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      CPMInputsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                TCADeviceCPMT1 const& hwT1,
                                TCADeviceCPMT2 const& hwT2);
      ~CPMInputsInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      TCADeviceCPMT1 const& getHwT1() const;
      TCADeviceCPMT2 const& getHwT2() const;

      TCADeviceCPMT1 const& hwT1_;
      TCADeviceCPMT2 const& hwT2_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_CPMInputsInfoSpaceUpdater_h_
