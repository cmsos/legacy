#ifndef _bril_dipbridge_dipDataExtract_h
#define _bril_dipbridge_dipDataExtract_h
#include "dip/DipData.h"
#include "xdata/Table.h"
#include <string>

namespace bril{
  namespace dipbridge{

    /**
     *  Extract xdata::Serializable pointer and type string of dipdata 
     */
    class dipDataExtract{
    public:
      //constructor with dipdata and topic name
      explicit dipDataExtract( const std::string& topicname );
      //destructor
      ~dipDataExtract();
      //topic name
      std::string topicname() const;
      //extract dipdata to xdata::Table
      xdata::Table::Reference getAll( const DipData& dipdata );
    private:
      std::string m_topicname;      
    };       
}}
#endif
