package xdaq.xdesk.jfinder;

import java.awt.Component;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.*;

import java.io.File;

public class DefaultFinderRenderer
implements FinderRenderer

{
	protected static Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);
	private Color unselectedForeground = null;
	private Color unselectedBackground = null;
	
	//FileSystemView fsView_ = FileSystemView.getFileSystemView();
	
	public DefaultFinderRenderer() {
		
	}
	
	public void setForeground(Color c) {
		
		unselectedForeground = c;
	}
	
	public void setBackground(Color c) {
		
		unselectedBackground = c;
	}
	
	public void setSelected(JLabel label, boolean selected, JFinder finder) {
		if (selected) {
			label.setForeground(finder.getSelectionForeground());
			label.setBackground(finder.getSelectionBackground());
			//label.setEnabled(false);
		}
		else {
			label.setForeground( (unselectedForeground != null) ?
								 unselectedForeground
																: finder.getForeground());
			
			label.setBackground( (unselectedBackground != null) ?
								 unselectedBackground
																: finder.getBackground());
			
			//label.setEnabled(true);
			
		}
	}
	
	public Component getFinderRendererComponent(JFinder finder,
												Object value,
												boolean isSelected,
												boolean hasFocus,
												JLabel where) {
		
		JLabel label;
		FinderTreeNode cell = (FinderTreeNode) value;
		if (where == null) {
			label = new JLabel();
			label.setText(cell.toString());
		}
		else {
			label = where;
		}
		
		label.setOpaque(true);
		label.setBorder(noFocusBorder);
		
		//label.setFont(finder.getFont());
		
		// focus ????? what is it ???
		
		/*
			File file = cell.getFile();
		 Icon icon = fsView_.getSystemIcon(file);
		 */
		
		ImageIcon icon = null;
		if (cell.getAllowsChildren()) {
			// set icon for directory
			
			// icon = new ImageIcon(this.getClass().getResource(
			//     "/xdaq/xdesk/jfinder/icons/folder.png"));
			//System.out.println(cell.toString());
			icon = finder.getConfiguration().getIcon(label.getText(), true);
		}
		else {
			// set icon for file
			// icon = new ImageIcon(this.getClass().getResource(
			//     "/xdaq/xdesk/jfinder/icons/file.png"));
			icon = finder.getConfiguration().getIcon(label.getText(), false);
		}
		
		label.setIcon(icon);
		label.setVerticalTextPosition(JLabel.BOTTOM);
		label.setHorizontalTextPosition(JLabel.CENTER);
		
		if (isSelected) {
			this.setSelected(label, true, finder);
		}
		else {
			this.setSelected(label, false, finder);
		}
		
		return label;
	}
	
}
