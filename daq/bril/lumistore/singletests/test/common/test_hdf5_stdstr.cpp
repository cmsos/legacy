#include "hdf5.h"
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
class WriteString{
public:
  WriteString (hid_t dataset, hid_t datatype, hid_t dataspace, hid_t memspace) : m_dataset (dataset), m_datatype (datatype), m_dataspace (dataspace), m_memspace (memspace), m_pos(){}
public:
  void operator()(std::vector<std::string>::value_type const & v){
    hsize_t count[] = {1};
    hsize_t offset[] = {m_pos++};
    H5Sselect_hyperslab( m_dataspace, H5S_SELECT_SET, offset, NULL, count, NULL );
    const char * s =v.c_str();
    H5Dwrite(m_dataset, m_datatype, m_memspace, m_dataspace, H5P_DEFAULT, &s );
  }
private:
  hid_t m_dataset;
  hid_t m_datatype;
  hid_t m_dataspace;
  hid_t m_memspace;
  int m_pos;
};

int main(){
  hid_t file = H5Fcreate ("h5vlstring.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  hid_t group = H5Gcreate(file,"strgroup",H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
  std::vector<std::string> v;
  v.push_back("Common");
  v.push_back("Well");
  v.push_back("Table");

  hsize_t dims[] = {4};
  std::cout<<"sizeof(dims) "<<sizeof(dims)<<" sizeof(*dims) "<<sizeof(*dims)<<std::endl;
  hid_t dataspace = H5Screate_simple(sizeof(dims)/sizeof(*dims),dims,NULL);
  dims[0] = 1;
  hid_t memspace = H5Screate_simple(sizeof(dims)/sizeof(*dims),dims,NULL);
  hid_t datatype = H5Tcopy(H5T_C_S1);
  H5Tset_size(datatype,H5T_VARIABLE);
  hid_t dataset = H5Dcreate(group,"str",datatype,dataspace,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
  // Select the "memory" to be written out - just 1 record.
  hsize_t offset[] = {0};
  hsize_t count[] = {1};
  H5Sselect_hyperslab(memspace, H5S_SELECT_SET, offset, NULL, count, NULL);
  std::for_each(v.begin(), v.end(), WriteString(dataset, datatype, dataspace, memspace));
  H5Dclose(dataset);
  H5Sclose(dataspace);
  H5Sclose(memspace);
  H5Tclose(datatype);
}
