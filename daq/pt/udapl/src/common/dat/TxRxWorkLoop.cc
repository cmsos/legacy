// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/udapl/dat/TxRxWorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include <sstream>

pt::udapl::dat::TxRxWorkLoop::TxRxWorkLoop (const std::string & name, DAT_EVD_HANDLE evd_handle_tx, DAT_EVD_HANDLE evd_handle_rx, pt::udapl::dat::EventHandler * event_handler, pt::udapl::dat::ErrorHandler * error_handler) 
	: evd_handle_tx_(evd_handle_tx), evd_handle_rx_(evd_handle_rx), event_handler_(event_handler), error_handler_(error_handler)
{
	name_ = name;
	process_ = toolbox::task::bind(this, &TxRxWorkLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(pt::udapl::exception::Exception, "Failed to submitjob", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '";
		msg << se.what() << "'";
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(pt::udapl::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}
}

pt::udapl::dat::TxRxWorkLoop::~TxRxWorkLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->cancel();
}

bool pt::udapl::dat::TxRxWorkLoop::process (toolbox::task::WorkLoop* wl)
{
	//std::cout << "Activated workloop " << name_ << std::endl;

	DAT_RETURN ret;
	//DAT_COUNT nmore;
	DAT_EVENT event;

	for (size_t i = 0; i < 100000; i++)
	{

		for (size_t j = 0; j < 100; j++)
		{
			ret = dat_evd_dequeue(this->evd_handle_tx_, &event);

			if ((ret & DAT_QUEUE_EMPTY) == 0) // found something
			{
				if (ret != DAT_SUCCESS)
				{
					const char *major_msg, *minor_msg;
					dat_strerror(ret, &major_msg, &minor_msg);
					std::stringstream msg;
					msg << "Fatal error waiting for event on tx " << name_ << " with code:" << major_msg << ":" << minor_msg;
					std::cerr << msg << std::endl;
					XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
					error_handler_->handleError(e);
					return false;
				}

				event_handler_->handleEvent(event);

			}
			else
			{
				break;
			}
		}

		for (size_t k = 0; k < 100; k++)
		{
			ret = dat_evd_dequeue(this->evd_handle_rx_, &event);

			if ((ret & DAT_QUEUE_EMPTY) == 0) // found something
			{
				if (ret != DAT_SUCCESS)
				{
					const char *major_msg, *minor_msg;
					dat_strerror(ret, &major_msg, &minor_msg);
					std::stringstream msg;
					msg << "Fatal error waiting for event on rx " << name_ << " with code:" << major_msg << ":" << minor_msg;
					std::cerr << msg << std::endl;
					XCEPT_DECLARE(pt::udapl::exception::Exception, e, msg.str());
					error_handler_->handleError(e);
					return false;
				}

				event_handler_->handleEvent(event);

			}
			else
			{
				break;
			}
		}

		// handoff event to listener
		//std::cout << "handle event before" << std::endl;
		//std::cout << "handle event done" << std::endl;
	}
	return true;
}

