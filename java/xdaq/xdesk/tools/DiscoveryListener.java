package xdaq.xdesk.tools;

import javax.infobus.*;

public interface DiscoveryListener {

	public void objectAdvertised(DataItem item);
	public void objectRevoked(DataItem object);
	public void objectChanged(DataItem object);

}
