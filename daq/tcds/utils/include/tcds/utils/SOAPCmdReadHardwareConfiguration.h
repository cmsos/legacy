#ifndef _tcds_utils_SOAPCmdReadHardwareConfiguration_h_
#define _tcds_utils_SOAPCmdReadHardwareConfiguration_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdReadHardwareConfiguration : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdReadHardwareConfiguration(T& controller);

      /**
       * Reads back the hardware configuration. Bound to the SOAP
       * command ReadHardwareConfiguration.
       * - SOAP command parameters: none.
       * - SOAP command return value:
       *   a big string containing the hardware configuration in a
       *   format that can be copy-pasted straight back into a
       *   'Configure' command.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdReadHardwareConfiguration.hxx"

#endif // _tcds_utils_SOAPCmdReadHardwareConfiguration_h_
