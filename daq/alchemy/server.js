// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/


const addon = require('./build/Release/alchemy');

var http = require("http");
var dns = require('dns');
var url = require('url');
const path = require('path');
const fs = require('fs');
var express = require('express');
var layout = require('ejs-mate');




var querystring = require('querystring');


//
var rootDir = process.env.XDAQ_DOCUMENT_ROOT;


function processPost(request, response, callback) {
	var queryData = "";
	if(typeof callback !== 'function') return null;

	if(request.method == 'POST') {
		request.on('data', function(data) {
			queryData += data;
			if(queryData.length > 1e6) {
				queryData = "";
				response.writeHead(413, {'Content-Type': 'text/plain'}).end();
				request.connection.destroy();
			}
		});

		request.on('end', function() {
			request.post = querystring.parse(queryData);
			callback(queryData);
		});

	} else {
		response.writeHead(405, {'Content-Type': 'text/plain'});
		response.end();
	}
}



function getCallerIP(request) {
	var ip = request.headers['x-forwarded-for'] ||
	request.connection.remoteAddress ||
	request.socket.remoteAddress ||
	request.connection.socket.remoteAddress ||
	request.ip;
	ip = ip.split(',')[0];
	//console.log(ip);
	ip = (ip.split(':').slice(-1))[0]; //in case the ip returned in a format: "::ffff:146.xxx.xxx.xxx"
	return ip;
}


function getHost(fullhostname) {
	host = fullhostname.split(':')[0]; 
	return host;
}

function getPort(fullhostname) {
	port = fullhostname.split(':')[1]; 
	return port;
}



function getHostByIP(ip) {
	dns.reverse(ip, function(err, domains) {
		if(err) {
			console.log(err.toString());
			return "unknown";
		}
		//console.log(domains);
		return domains[0];
	});
}

//Callback
function foo(msg) {
	  console.log(msg);
	  console.log("Ciao Ragazzi");
}

//Create ApplicationContext
var context = addon.getApplicationContext();
//context.init();
var pta = addon.getPeerTransportAgent();
var xgil = pta.getListener('cgi');
var xoapl = pta.getListener('soap');

console.log("Done ...");
console.log(pta);
console.log(xgil);
console.log(xoapl);



//addon.setCallback(foo);
//addon.call();
//addon.callThis(() => console.log("this too!"));
//addon(foo);

// End Callback


	
class Server {
	constructor(paramaters, descriptor) {
		this.paramaters = paramaters;
		//pta.addSenderSupport ("http", foo);
		console.log('create http Server App');
		this._app = express();
		this._descriptor = descriptor;
        this._descriptor.icon = "/alchemy/images/http.png";
        
        this._router = express.Router();
        //this._router.get('/applications/myapplication/whoami', this.whoami);
		this._router.get('/urn:xdaq-application:lid=' + descriptor.id + '/', this.home);
		this._router.get('/urn:xdaq-application:service=server', this.home);
        //this._router.get('/urn:xdaq-application:service=server', this.home);
        
       /* this._router.get('/urn:xdaq-application:lid=101', function(req, res, next) {
 			res.redirect('/urn:xdaq-application:service=hyperdaq');
		});*/
		
	}

	home(req,res) {
		var descriptors = context.get({ action: 'descriptors'});
		console.log(descriptors);
	   	res.render('server', { applications: descriptors.data, zone: 'default', html: '<h1>I am the Alchemy HTTP Server</h1>' })
	 }
	 
	display() {
		console.log(this.paramaters);
	}

	// peer transport specific
	service() {
		return "cgi";
	}

	protocol() {
		return "http";
	}

	process (request, response)
	{
		//
		// Add CGI/XDAQ specific headers 
		//
		request.headers["request_method"] = request.method;
		request.headers["request_uri"] = request.url;
		request.headers["server_protocol"] = 'HTTP/'+ request.httpVersion;
		request.headers["x-xdaq-receivetimestamp"] = new Date().valueOf();
		request.headers["x-xdaq-remote-addr"] = getCallerIP(request);
		// request.headers["x-xdaq-remote-host"] = getHostByIP(request.headers["x-xdaq-remote-addr"]); this is asynchronous cannot work
		request.headers["x-xdaq-remote-host"] = request.headers["x-xdaq-remote-addr"];  // let the user to find

		const request_url = url.parse(request.url);
		//console.log(request_url);
		var script = '';
		var script_extract = (request_url.pathname.split('/'))[1];
		if (script_extract)
		{
			script = script_extract;
		}
		var path_info = '';
		var path_info_extract = (request_url.pathname.split('/'))[2];
		if (path_info_extract)	
		{
			path_info = path_info_extract;
		}
		var query_string = '';
		if (request_url.query)
		{
			query_string = request_url.query;
		}
		var content_type = '';

		if ( request.headers['content-type'] )
		{
			content_type = request.headers['content-type'] ;
		}
		var content_length = '';
		if ( request.headers['content-length'] )
		{
			content_length = request.headers['content-length'] ;
		}

		var accept = '';
		if ( request.headers['accept'] )
		{
			accept = request.headers['accept'] ;
		}

		var user_agent = '';
		if ( request.headers['user-agent'] )
		{
			user_agent = request.headers['user-agent'] ;
		}

		var referer = '';
		if ( request.headers['referer'] )
		{
			referer = request.headers['referer'] ;
		}

		var cookie = '';
		if ( request.headers['cookie'] )
		{
			cookie = request.headers['cookie'] ;
		}
		
		var soapaction = '';
		if ( request.headers['soapaction'] )
		{
			soapaction = request.headers['soapaction'] ;
		}

		var auth_type = '';
		var remote_user = '';
		if ( request.headers['authorization'] )
		{
			var authorization = request.headers['authorization'] ;
			var auth_type_extract = (auth_type.split(' '))[1];
			if (auth_type_extract)
			{
				auth_type = auth_type_extract;
			}
			var remote_user_extract = (auth_type.split(' '))[2];
			if (remote_user_extract)
			{
				remote_user = Buffer.from(remote_user_extract, 'base64'); 
			}
		}

		//
		// Add  CGI/XDAQ specific environment 
		//
		//ENVIRONMENT ORIGINAL: SERVER_SOFTWARE=XDAQ/3.0
		//ENVIRONMENT ORIGINAL: SERVER_NAME=kvm-s3562-1-ip151-66.cms
		//ENVIRONMENT ORIGINAL: GATEWAY_INTERFACE=CGI/1.1
		//ENVIRONMENT ORIGINAL: SERVER_PROTOCOL=HTTP/1.1
		//ENVIRONMENT ORIGINAL: SERVER_PORT=1973
		//ENVIRONMENT ORIGINAL: REQUEST_METHOD=GET
		//ENVIRONMENT ORIGINAL: PATH_TRANSLATED=/urn:xdaq-application:service=hyperdaq
		//ENVIRONMENT ORIGINAL: SCRIPT_NAME=urn:xdaq-application:service=hyperdaq
		//ENVIRONMENT ORIGINAL: PATH_INFO=
		//ENVIRONMENT ORIGINAL: QUERY_STRING=
		//ENVIRONMENT ORIGINAL: REMOTE_HOST=kvm-s3562-1-ip150-16.cms
		//ENVIRONMENT ORIGINAL: REMOTE_ADDR=10.176.140.50
		//ENVIRONMENT ORIGINAL: AUTH_TYPE=
		//ENVIRONMENT ORIGINAL: REMOTE_USER=
		//ENVIRONMENT ORIGINAL: REMOTE_IDENT=
		//ENVIRONMENT ORIGINAL: CONTENT_TYPE=
		//ENVIRONMENT ORIGINAL: CONTENT_LENGTH=
		//ENVIRONMENT ORIGINAL: HTTP_ACCEPT=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
		//ENVIRONMENT ORIGINAL: HTTP_USER_AGENT=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0
		//ENVIRONMENT ORIGINAL: REDIRECT_REQUEST=
		//ENVIRONMENT ORIGINAL: REDIRECT_URL=
		//ENVIRONMENT ORIGINAL: REDIRECT_STATUS=
		//ENVIRONMENT ORIGINAL: HTTP_REFERER=
		//ENVIRONMENT ORIGINAL: HTTP_COOKIE=
		//ENVIRONMENT ORIGINAL: ACCEPT_ENCODING=gzip, deflate
		//ENVIRONMENT ORIGINAL: ACCEPT_LANGUAGE=en-US,en;q=0.5

		//
		var evironment = {
				'SERVER_SOFTWARE': 'XDAQ/3.0',
				'SERVER_NAME': getHost(request.headers['host']),
				'GATEWAY_INTERFACE' : 'CGI/1.1',
				'SERVER_PROTOCOL' : 'HTTP/'+ request.httpVersion,
				'SERVER_PORT': getPort(request.headers['host']),
				'REQUEST_METHOD': request.method,
				'PATH_TRANSLATED': request.url,
				'SCRIPT_NAME': script,        // actually the full path to the executing program
				'PATH_INFO': path_info,       // www.cern.ch/pippo.cgi/path -> path
				'QUERY_STRING': query_string,  // everthing after question mark
				'REMOTE_HOST':  request.headers["x-xdaq-remote-addr"],   //it comes from the socket connection
				'REMOTE_ADDR': request.headers["x-xdaq-remote-addr"],  // IP from client (it comes from the socket connection
				'AUTH_TYPE' : auth_type,
				'REMOTE_USER': remote_user,
				'REMOTE_IDENT': '',              // according RFC931
				'CONTENT_TYPE' :   content_type,
				'CONTENT_LENGTH':  content_length,
				'HTTP_ACCEPT':     accept,
				'HTTP_USER_AGENT': user_agent,
				//REDIRECT_REQUEST=
				//REDIRECT_URL=
				//REDIRECT_STATUS=
				'HTTP_REFERER':    referer,
				'HTTP_COOKIE':     cookie,
				'ACCEPT_ENCODING': request.headers["accept-encoding"],
				'ACCEPT_LANGUAGE': request.headers["accept-language"],
		};

		// 
		// Process HTTP request/reponse
		//
		var xgiinput = {
				'url': request.url, 
				'method' : request.method,
				'headers': request.headers, 
				'environment': evironment,
				'data' : request.post
		};	

		console.log (xgiinput);


		// check for SOAP
		if ( ( content_type.indexOf('application/soap+xml') > -1 ) || (content_type.indexOf('text/xml') > -1 ) || soapaction != '')
		{
				//console.log("Got SOAP message" );
				//console.log (xgiinput);
				
				//console.log ("+++++++++++++++++++++++++++++++++++End SOAP message++++++++++++++++++++++++++++++++");
				var xgiout = xoapl.processIncoming(xgiinput);
				
				response.statusCode = xgiout.status;
				response.statusMessage = xgiout.reason;
				response.httpVersion = xgiout.version;

				xgiout.headers.forEach(function(value){
					var mimetype = value.split(':');
					//console.log(mimetype);
					response.setHeader(mimetype[0], mimetype[1]);

				});
				response.end(xgiout.data, 'utf-8');
				//console.log(xgiout.data);
				
				
		}	
		else if (request.url == '/')
		{
			// redirect to hyperdaq or as environment XDAQ_REDIRECT
		
			var rurl = process.env.XDAQ_REDIRECT || 'http://' + getHost(request.headers['host']) + ":" + getPort(request.headers['host']) + '/urn:xdaq-application:service=' + this._descriptor.redirect;
			response.writeHead(301, "Moved Permanently", {'Location': rurl });
			response.end();
		}
		else
		{	
			
			// check file is requested 
			//http://kvm-s3562-1-ip151-66.cms:1973//hyperdaq/images/framework/xdaq-footer-border.png
			var script = '';
			if ( request.url.indexOf('/urn:xdaq-application:') == 0) 
			{			

			
				var xgiout = xgil.processIncoming(xgiinput);

				//console.log ("+++++++++++++++++++++++++++++++++++++++++++++++++++++1+++++++++++++++++++++++++++++++++++");
				//console.log (xgiout.headers);
				//console.log (JSON.stringify(xgiout.headers));

			
				//console.log (xgiout);
				//console.log ("+++++++++++++++++++++++++++++++++++++++++++++++++++++4+++++++++++++++++++++++++++++++++++");

				response.statusCode = xgiout.status;
				response.statusMessage = xgiout.reason;
				response.httpVersion = xgiout.version;

				xgiout.headers.forEach(function(value){
					var mimetype = value.split(':');
					//console.log(mimetype);
					response.setHeader(mimetype[0], mimetype[1]);

				});


				//response.writeHead(200, "OK", {'Content-Type': 'text/html; charset=utf-8', });
				//response.write(xgiout.headers, 'utf-8');
				response.end(xgiout.data, 'utf-8');
			}
			else
			{
				// serve file
				// parse URL
				const parsedUrl = url.parse(request.url);
				// extract URL path
				let pathname = parsedUrl.pathname;
				// based on the URL path, extract the file extention. e.g. .js, .doc, ...
				const ext = path.parse(pathname).ext;
				// maps file extention to MIME typere
				const map = {
						'.ico': 'image/x-icon',
						'.html': 'text/html',
						'.js': 'text/javascript',
						'.json': 'application/json',
						'.css': 'text/css',
						'.png': 'image/png',
						'.jpg': 'image/jpeg',
						'.wav': 'audio/wav',
						'.mp3': 'audio/mpeg',
						'.svg': 'image/svg+xml',
						'.pdf': 'application/pdf',
						'.doc': 'application/msword'
				};

				var filename = rootDir + pathname;
				//console.log(ext);
				// console.log(filename);

				fs.exists(filename, function (exist) {
					if(!exist) {
						// if the file is not found, return 404
						response.statusCode = 404;
						response.end(`File ${pathname} not found!`);
						return;
					}

					// if is a directory search for index file matching the extention
					if (fs.statSync(filename).isDirectory()) pathname += '/index' + ext;

					// read file from file system
					fs.readFile(filename, function(err, data){
						if(err){
							response.statusCode = 500;
							response.end(`Error getting the file: ${err}.`);
						} else {
							// if the file is found, set Content-type and send data
							var contentType =  map[ext] || 'text/plain';
							response.writeHead(200, { 'Content-Type': contentType });
							response.end(data, 'utf-8');
						}
					});
				});
			}
		}

	}
	
	
	config( host, port)
	{
			//http.createServer(app).listen(8080);


	
		
		var callback = function(request, response) {
			//const { headers, method, url } = request;
			//console.log(headers);
			//console.log(method);
			//console.log(url);
			//console.log('---begin----');
			request.post = '';
			var queryData = '';
			const chunks = [];

			request.on('data', chunk => {
				//console.log('A chunk of data has arrived: ' + request.method);
				queryData += chunk.toString();
				chunks.push(chunk);
				if(queryData.length > 0x10000000 ) {
					queryData = "";
					response.writeHead(413, {'Content-Type': 'text/plain'}).end();
					request.connection.destroy();
				}
			});

			request.on('end', () => {
				//console.log('No more data' + request.method);
				request.post = queryData;
				const data = Buffer.concat(chunks);
				object.process(request, response)
			});
			
			// Install sender support
			

		}
		var processByLID = function (req, res, next) {
        		var script_name = 'urn:xdaq-application:lid=' + req.params.lid;
			
        		var path_translated =  '/' +script_name;
			if ( req.params.resource != undefined )
			{
        		 path_translated =   path_translated+ '/' +  req.params.resource;
			}
			
			
			path_translated = path_translated + '?' + querystring.stringify(req.query);
			
        		//req.environment["PATH_TRANSLATED"] = path_translated;
			req.url = path_translated;
        		//req.environment["SCRIPT_NAME"] = script_name ;
        		//req.environment["PATH_INFO"] = req.params.resource ;

        		next();
		}


		// set the view engine to ejs
		this._app.engine('ejs', layout );
		this._app.set('view engine', 'ejs');
		this._app.set('views','./sorcerer/html/views'); // this is a temporary solution, need to generalize for RPM installation
		

		var router = express.Router();
		router.get('/application/:lid/:resource', processByLID, callback);
		router.get('/application/:lid', processByLID, callback);
		router.post('/application/:lid/:resource', processByLID, callback);
		router.post('/application/:lid', processByLID, callback);
                this._app.use('/api', router);

		this._app.get('/', callback);
		this._app.get('/*', callback);
		this._app.post('/', callback);
		this._app.post('/*', callback);

		

		const server = http.createServer(this._app);
		//const server = http.createServer(callback);
		console.log ("going to listen for " + host + " on  port " + port);
		server.listen(port, host);
		var object = this;


	} // end of config
} // end of class

module.exports = class Instantiator {
	constructor() {
	}
	
	create (args, descriptor) {
		return new Server(args,descriptor)
	}
	
	type() {
		return "Server";
	}
	
	
}

//module.exports.app = app;

//module.exports.server = function(port) {
//}; // exported server

/* Use express
* 
* var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.urlencoded({extended : true}));

app.post("/pathpostdataissentto", function(request, response) {
console.log(request.body);
//Or
console.log(request.body.fieldName);
});

app.listen(8080);
*/
/* BACKUP
var callback = function(request, response) {
	//const { headers, method, url } = request;
	//console.log(headers);
	//console.log(method);
	//console.log(url);
	//console.log('---begin----');
 	request.post = '';
	var queryData = '';
	const chunks = [];
	if(request.method == 'POST' || request.method == 'PUT') { 
		console.log('It is a ',  request.method);

		request.on('data', chunk => {
			console.log('A chunk of data has arrived: ' + request.method);
			queryData += chunk.toString();
			chunks.push(chunk);
			if(queryData.length > 0x100000 ) {
				queryData = "";
				response.writeHead(413, {'Content-Type': 'text/plain'}).end();
				request.connection.destroy();
			}
		});

		request.on('end', () => {
			console.log('No more data' + request.method);
			//request.post = querystring.parse(queryData);
			request.post = queryData;
			const data = Buffer.concat(chunks);
			// console.log('---------------------------------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Data: ', request.post);
			object.process(request, response)
		});
	}
	else if ( request.method == 'GET' )
	{
		//console.log('It is a GET');
		object.process(request, response)
	}
	else
	{
		console.log(request.method + " not supported");
		response.writeHead(413, {'Content-Type': 'text/plain'}).end();
		request.connection.destroy();
	}

	//console.log('---end----');

} */


