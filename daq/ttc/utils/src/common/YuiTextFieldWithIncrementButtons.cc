#include "ttc/utils/YuiTextFieldWithIncrementButtons.hh"

#include "ttc/utils/JavaScriptUtils.hh"

#include <sstream>


using namespace std;


// class ttc::YuiTextFieldWithIncrementButtons - static

string ttc::YuiTextFieldWithIncrementButtons::getYuiIncludes()
{
  vector<string> vars;
  vars.push_back("yahoo/yahoo");
  vars.push_back("dom/dom");
  vars.push_back("event/event");
  vars.push_back("yahoo-dom-event/yahoo-dom-event");
  vars.push_back("dragdrop/dragdrop");
  vars.push_back("connection/connection");
  vars.push_back("element/element-beta");
  vars.push_back("menu/menu");

  return YuiWidgetWithPostRequest::getYuiIncludes(vars);
}


string ttc::YuiTextFieldWithIncrementButtons::getCommonJavaScriptCode()
{
  ostringstream buf;

  buf
      << "<script type=\"text/javascript\">" << endl
      << "//----------------------------------------" << endl
      << JavaScriptUtils::makeSubmitCommand()
      << "//----------------------------------------" << endl
      << JavaScriptUtils::TextFieldWithIncrementButtons()
      << "//----------------------------------------" << endl
      << "</script>" << endl;

  return buf.str();
}


// class ttc::YuiTextFieldWithIncrementButtons - members

ttc::YuiTextFieldWithIncrementButtons::YuiTextFieldWithIncrementButtons(
    const string&_name,
    int _initial_value,
    int _min_value,
    int _max_value)
:
    YuiWidgetWithPostRequest(_name),
    initial_value(_initial_value),
    min_value(_min_value),
    max_value(_max_value)
{}


string ttc::YuiTextFieldWithIncrementButtons::getTextInputHtmlCode()
{
  ostringstream buf;

  const int text_field_size = 4;
  const int text_field_max_length = 4;

  buf
      << "<script type=\"text/javascript\">" << endl
      << "  var " << getJavaScriptObjectName() << " = new TextFieldWithIncrementButtons(" << endl
      << "      \"" << getTextFieldName() << "\",             // name of the text field" << endl
      << "      " << min_value << "," << max_value << ",           // minimum and maximum values" << endl
      << "      \"" << value_changed_url << "\",                       // URL to be called when value changes" << endl
      << "       " << makeHttpRequestParametersJavascriptCode() << "   // additional arguments when calling the above URL" << endl
      << "      );" << endl
      << endl
      << "  // alert(\"GJGJG \" + "
      << getJavaScriptObjectName() + ".putValueInRange);" << endl
      << endl
      << "</script>" << endl
      << endl
      << "<form onsubmit=\"return " + getJavaScriptObjectName() + ".onSubmitCallBack(); \">" << endl
      << "  <input "
          "autocomplete=\"off\" "
          "name=\"" << getTextFieldName() << "\" "
          "id=\"" << getTextFieldName() << "\" "
          "value=\"" << initial_value << "\" "
          "size=\"" << text_field_size << "\" "
          "maxlength=\"" << text_field_max_length << "\" "
          "type=\"text\""
          "> " << endl
      << "</form>" << endl;

  return buf.str();
}


string ttc::YuiTextFieldWithIncrementButtons::getIncrementButtonCreationCode(
    const string &button_suffix,
    const string &button_label,
    int increment) const
{
  ostringstream buf;

  string button_name = name + "_" + button_suffix;

  buf
      << "<span id=\"" << button_name << "\" class=\"yui-button yui-push-button\">" << endl
      << "  <span class=\"first-child\"> <button type=\"button\">" << button_label << "</button></span>" << endl
      << "</span>" << endl
      << "<script type=\"text/javascript\">" << endl
      << endl
      << "// create this button on page load" << endl
      << "YAHOO.util.Event.on(window, \"load\", function()" << endl
      << "{" << endl
      << "  " << getJavaScriptObjectName() << ".makeIncrementButton(\"" << button_name << "\",\"" << button_label << "\"," << increment << ");" << endl
      << "});  " << endl
      << "</script>" << endl;

  return buf.str();
}


string ttc::YuiTextFieldWithIncrementButtons::getJavaScriptObjectName() const
{
  return name + "_object";
}


string ttc::YuiTextFieldWithIncrementButtons::getTextFieldName() const
{
  return name + "_textinput";
}
