#include "tcds/ttcspy/TCADeviceTTCSpy.h"

// #include <bitset>
#include <cassert>
// #include <climits>
// #include <iomanip>
// #include <sstream>

// #include "uhal/HwInterface.hpp"
// #include "uhal/Node.hpp"
// #include "uhal/ValMem.hpp"

// #include "toolbox/string.h"
// #include "xcept/Exception.h"
// #include "xdaq/Application.h"

// #include "tcds/ttcspy/SpyMemEntry.h"
// #include "tcds/exception/Exception.h"
// #include "tcds/utils/InfoSpaceHandler.h"
// #include "tcds/utils/Lock.h"
// #include "tcds/utils/LockGuard.h"

#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierGLIB.h"

tcds::ttcspy::TCADeviceTTCSpy::TCADeviceTTCSpy() :
  tcds::hwlayertca::TCADeviceBase(std::auto_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierGLIB(hwDevice_)))
{
}

tcds::ttcspy::TCADeviceTTCSpy::~TCADeviceTTCSpy()
{
}

// std::string
// tcds::ttcspy::TCADeviceTTCSpy::addressTableFileName() const
// {
//   return "file://${XDAQ_ROOT}/etc/tcds/addresstables/"
//     "device_address_table_glib_ttcspy.xml";
// }

// std::string
// tcds::ttcspy::TCADeviceTTCSpy::ipbusProtocolVersion() const
// {
//   return tcds::hwlayertca::TCADeviceBase::ipbusProtocolVersion();
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::hwConfigure()
// {
  // BUG BUG BUG
  // tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  // if (!isHwConnected())
  //   {
  //     std::string msg = "Could not configure the hardware. "
  //       "(No hardware is connected.)";
  //     XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
  //   }

  // hwInit();

  // // Select the desired input TTC stream.
  // // NOTE: This also configures the clocking scheme, so it has to be
  // // the first step
  // uint32_t ttcSource = cfgInfoSpaceP_->getUInt32("ttcSource");
  // try
  //   {
  //     selectSource(ttcSource);
  //   }
  // catch (tcds::exception::ValueError& err)
  //   {
  //     std::string const msg = err.message();
  //     ERROR(msg);
  //     XCEPT_RETHROW(tcds::exception::ConfigurationProblem, msg, err);
  //   }

  // // Stop logging.
  // stopLogging();

  // // Set the logging mode and logic operator.
  // uint32_t loggingMode = cfgInfoSpaceP_->getUInt32("loggingMode");
  // try
  //   {
  //     setLoggingMode(loggingMode);
  //   }
  // catch (tcds::exception::ValueError& err)
  //   {
  //     std::string const msg = err.message();
  //     ERROR(msg);
  //     XCEPT_RETHROW(tcds::exception::ConfigurationProblem, msg, err);
  //   }

  // uint32_t loggingLogic = cfgInfoSpaceP_->getUInt32("loggingLogic");
  // try
  //   {
  //     setLoggingLogic(loggingLogic);
  //   }
  // catch (tcds::exception::ValueError& err)
  //   {
  //     std::string const msg = err.message();
  //     ERROR(msg);
  //     XCEPT_RETHROW(tcds::exception::ConfigurationProblem, msg, err);
  //   }

  // // Configure the filter and trigger masks.
  // uint32_t filterMask=  createFilterMask();
  // writeRegister("ttcspy.filter_mask", filterMask);
  // uint32_t triggerMask=  createTriggerMask();
  // writeRegister("ttcspy.trigger_mask", triggerMask);

  // // Reset whatever is currently in the memory.
  // resetLogging();
  // BUG BUG BUG end
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::hwEnable()
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   if (!isHwConnected())
//     {
//       std::string msg = "Could not enable the hardware. "
//         "(No hardware is connected.)";
//       XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
//     }

//   zeroSpyMemory();
//   startLogging();
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::hwHalt()
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   // NOTE: Since the Halt transition is the one used to recover from
//   // the Failed state, in this case nothing is done when it is found
//   // that the hardware has not been initialized. This in contrast to
//   // methods like hwPause().
//   if (isHwConnected())
//     {
//       stopLogging();
//     }
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::hwPause()
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   if (!isHwConnected())
//     {
//       std::string msg = "Could not pause the hardware. "
//         "(No hardware is connected.)";
//       XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
//     }

//   stopLogging();
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::hwResume()
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   if (!isHwConnected())
//     {
//       std::string msg = "Could not resume the hardware. "
//         "(No hardware is connected.)";
//       XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
//     }

//   startLogging();
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::hwStop()
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   if (!isHwConnected())
//     {
//       std::string msg = "Could not stop the hardware. "
//         "(No hardware is connected.)";
//       XCEPT_RAISE(tcds::exception::HardwareProblem, msg);
//     }

//   stopLogging();
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::selectSource(uint32_t const src) const
// {
//   // Switch between the front-panel and backplane (port 3) TTC streams
//   // to spy on. Two steps:
//   // 1) Switch the actual source.
//   // 2) Make sure the clocking is setup correctly.

//   std::string const regNameSource = "ttcspy.spy_source";
//   switch (src)
//     {
//     case TTC_SOURCE_FRONTPANEL:
//       {
//         // Step 1.
//         writeRegister(regNameSource, 0x0);
//         // Step 2.
//         configureXPoint1(HwDeviceGLIB::XPOINT1_SOURCE_XPOINT2_OUT1);
//         configureXPoint2(HwDeviceGLIB::XPOINT2_SOURCE_FMC1_CLK0);
//         break;
//       }
//     case TTC_SOURCE_BACKPLANE:
//       {
//         // Step 1.
//         writeRegister(regNameSource, 0x1);
//         // Step 2.
//         configureXPoint1(HwDeviceGLIB::XPOINT1_SOURCE_FCLKA);
//         break;
//       }
//     default:
//       {
//         std::string const msg =
//           toolbox::toString("'%d' Is not a valid value for the TTC stream source.",
//                             src);
//         XCEPT_RAISE(tcds::exception::ValueError, msg);
//       }
//     }
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::setLoggingMode(uint32_t const src) const
// {
//   std::string const regName = "ttcspy.logging_control.logging_mode";
//   switch (src)
//     {
//     case LOGGING_MODE_ONLY:
//       {
//         writeRegister(regName, 0x0);
//         break;
//       }
//     case LOGGING_MODE_EXCEPT:
//       {
//         writeRegister(regName, 0x1);
//         break;
//       }
//     default:
//       {
//         std::string const msg =
//           toolbox::toString("'%d' Is not a valid value for the logging mode.",
//                             src);
//         XCEPT_RAISE(tcds::exception::ValueError, msg);
//       }
//     }
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::setLoggingLogic(uint32_t const src) const
// {
//   std::string const regName = "ttcspy.logging_control.logging_logic";
//   switch (src)
//     {
//     case LOGGING_LOGIC_OR:
//       {
//         writeRegister(regName, 0x0);
//         break;
//       }
//     case LOGGING_LOGIC_AND:
//       {
//         writeRegister(regName, 0x1);
//         break;
//       }
//     default:
//       {
//         std::string const msg =
//           toolbox::toString("'%d' Is not a valid value for the logging logic.",
//                             src);
//         XCEPT_RAISE(tcds::exception::ValueError, msg);
//       }
//     }
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::startLogging() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   writeRegister("ttcspy.logging_control.enabled", 0x1);
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::stopLogging() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   writeRegister("ttcspy.logging_control.enabled", 0x0);
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::resetLogging() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   zeroSpyMemory();
//   // NOTE: The actual reset is triggered by the 1 -> 0 transition.
//   writeRegister("ttcspy.logging_control.reset", 0x1);
//   writeRegister("ttcspy.logging_control.reset", 0x0);
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::restartLogging() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   stopLogging();
//   resetLogging();
//   startLogging();
// }

// bool
// tcds::ttcspy::TCADeviceTTCSpy::isLoggingEnabled() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   uint32_t loggingEnabled = readRegister("ttcspy.logging_control.enabled");
//   return loggingEnabled;
// }

// bool
// tcds::ttcspy::TCADeviceTTCSpy::isSpyMemoryFull() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   uint32_t spyMemFull = readRegister("ttcspy.logging_status.spy_memory_full");
//   return (spyMemFull != 0);
// }

// uint32_t
// tcds::ttcspy::TCADeviceTTCSpy::getSpyMemoryMaxEntries() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   uint32_t tmp = getHwInterface().getNode("ttcspy.spy_memory").getSize();
//   uint32_t spyMemSize = tmp / SpyMemEntry::kNumWordsPerSpyMemEntry;
//   // ASSERT ASSERT ASSERT
//   assert ((tmp % SpyMemEntry::kNumWordsPerSpyMemEntry) == 0);
//   // ASSERT ASSERT ASSERT end
//   return spyMemSize;
// }

// uint32_t
// tcds::ttcspy::TCADeviceTTCSpy::getSpyMemoryEntries() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   // This returns the number of entries currently in the spy memory.

//   // NOTE: This is a little tricky. As long as the memory is not full,
//   // the address pointer indicates the next entry to be written. Since
//   // the first entry has address zero, the address pointer indicates
//   // the number of entries currently in the register. Once the memory
//   // is full, though, the address pointer will not move forward (and
//   // roll over) but still stick to max_entries - 1.

//   if (isSpyMemoryFull())
//     {
//       return getSpyMemoryMaxEntries();
//     }
//   else
//     {
//       uint32_t numEntries = readRegister("ttcspy.logging_status.spy_memory_address_pointer");
//       // ASSERT ASSERT ASSERT
//       // Check if the address table matches what the firmware says.
//       assert (numEntries <= getSpyMemoryMaxEntries());
//       // ASSERT ASSERT ASSERT end
//       return numEntries;
//     }
// }


// std::vector<uint32_t>
// tcds::ttcspy::TCADeviceTTCSpy::readSpyMemory() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   uint32_t numEntries = getSpyMemoryEntries();
//   uint32_t blockSize = numEntries * SpyMemEntry::kNumWordsPerSpyMemEntry;
//   std::vector<uint32_t> res;
//   res = readBlock("ttcspy.spy_memory", blockSize);
//   return res;
// }

// void
// tcds::ttcspy::TCADeviceTTCSpy::zeroSpyMemory() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   uint32_t spyMemSize = getSpyMemoryMaxEntries();
//   std::vector<uint32_t> zeros(spyMemSize);
//   writeBlock("ttcspy.spy_memory", zeros);
// }

// bool
// tcds::ttcspy::TCADeviceTTCSpy::isCDRLocked() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   // NOTE: The default state of the loss-of-lock pin of the CDR chip
//   // on the TTC FMC is programmable. This means that this pin can show
//   // a locked state without any input signal. Therefore the CDR
//   // locking status is ANDed here with the presence of an input
//   // signal.
//   uint32_t cdrLockStatus = readRegister("ttcspy.signal_status.loss_of_lock");
//   bool cdrLocked = ((cdrLockStatus == 0) && (isSFPConnected()));
//   return cdrLocked;
// }

// bool
// tcds::ttcspy::TCADeviceTTCSpy::isSFPConnected() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   uint32_t cdrSignalStatus = readRegister("ttcspy.signal_status.loss_of_signal");
//   bool sfpConnected = (cdrSignalStatus == 0);
//   return sfpConnected;
// }

// bool
// tcds::ttcspy::TCADeviceTTCSpy::isTTCClockUp() const
// {
//   tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
//   uint32_t ttcClockStatus = readRegister("ttcspy.signal_status.ttc_clock_up");
//   bool ttcClockPresent = (ttcClockStatus != 0);
//   return ttcClockPresent;
// }

// uint32_t
// tcds::ttcspy::TCADeviceTTCSpy::createMask(bool const bitBCntRes,
//                                          bool const bitEvCntRes,
//                                          uint32_t const bitsShortSystemData,
//                                          uint32_t const bitsShortUserData,
//                                          bool const bitShortSystemDataAll,
//                                          bool const bitShortUserDataAll,
//                                          bool const bitShortCommandsAll,
//                                          bool const bitLongCommandsAll,
//                                          bool const bitL1A,
//                                          bool const bitSingleBitError,
//                                          bool const bitDoubleBitError,
//                                          bool const bitCommunicationError) const
// {
//   // Build a filter/trigger mask from the different pieces.

//   // These are all single-bit flags.
//   uint32_t maskBCntRes = uint32_t(bitBCntRes) << kBitPosMaskBCntRes;
//   uint32_t maskEvCntRes = uint32_t(bitEvCntRes) << kBitPosMaskEvCntRes;
//   uint32_t maskShortSystemDataAll = uint32_t(bitShortSystemDataAll) << kBitPosMaskShortSystemDataAll;
//   uint32_t maskShortUserDataAll = uint32_t(bitShortUserDataAll) << kBitPosMaskShortUserDataAll;
//   uint32_t maskShortCommandsAll = uint32_t(bitShortCommandsAll) << kBitPosMaskShortCommandsAll;
//   uint32_t maskLongCommandsAll = uint32_t(bitLongCommandsAll) << kBitPosMaskLongCommandsAll;
//   uint32_t maskL1A = uint32_t(bitL1A) << kBitPosMaskL1A;
//   uint32_t maskSingleBitError = uint32_t(bitSingleBitError) << kBitPosMaskSingleBitError;
//   uint32_t maskDoubleBitError = uint32_t(bitDoubleBitError) << kBitPosMaskDoubleBitError;
//   uint32_t maskCommunicationError = uint32_t(bitCommunicationError) << kBitPosMaskCommunicationError;

//   // These are multi-bit masks. Have to take a bit more care. Mask out
//   // anything that exceeds the bits associated with the mask range.
//   // NOTE: This is tricky. If one specifies a mask value that exceeds
//   // the corresponding bit range the extra pieces are quietly cut off.
//   // ASSERT ASSERT ASSERT
//   assert (kBitPosMaskShortSystemDataLo <= kBitPosMaskShortSystemDataHi);
//   assert (kBitPosMaskShortUserDataLo <= kBitPosMaskShortUserDataHi);
//   // ASSERT ASSERT ASSERT end

//   uint32_t maskTmp = 0;
//   for (size_t i = kBitPosMaskShortSystemDataLo;
//        i <= kBitPosMaskShortSystemDataHi;
//        ++i)
//     {
//       maskTmp |= (uint32_t(1) << (i - kBitPosMaskShortSystemDataLo));
//     }
//   uint32_t tmpShortSystemData = bitsShortSystemData & maskTmp;
//   uint32_t maskShortSystemData = tmpShortSystemData << kBitPosMaskShortSystemDataLo;

//   maskTmp = 0;
//   for (size_t i = kBitPosMaskShortUserDataLo;
//        i <= kBitPosMaskShortUserDataHi;
//        ++i)
//     {
//       maskTmp |= (uint32_t(1) << (i - kBitPosMaskShortUserDataLo));
//     }
//   uint32_t tmpShortUserData = bitsShortUserData & maskTmp;
//   uint32_t maskShortUserData = tmpShortUserData << kBitPosMaskShortUserDataLo;

//   // ASSERT ASSERT ASSERT
//   // This at least 'warns' when something unexpected happens.
//   assert (tmpShortSystemData == bitsShortSystemData);
//   assert (tmpShortUserData == bitsShortUserData);
//   // ASSERT ASSERT ASSERT end

//   uint32_t mask =
//     maskBCntRes |
//     maskEvCntRes |
//     maskShortSystemDataAll |
//     maskShortUserDataAll |
//     maskShortCommandsAll |
//     maskLongCommandsAll |
//     maskL1A |
//     maskSingleBitError |
//     maskDoubleBitError |
//     maskCommunicationError |
//     maskShortSystemData |
//     maskShortUserData;
//   return mask;
// }

// uint32_t
// tcds::ttcspy::TCADeviceTTCSpy::createFilterMask() const
// {
//   // BUG BUG BUG end
//   // bool const bitBCntRes = cfgInfoSpaceP_->getBool("filterBCntRes");
//   // bool const bitEvCntRes = cfgInfoSpaceP_->getBool("filterEvCntRes");
//   // uint32_t const bitsShortSystemData = cfgInfoSpaceP_->getUInt32("filterShortSystemData");
//   // uint32_t const bitsShortUserData = cfgInfoSpaceP_->getUInt32("filterShortUserData");
//   // bool const bitShortSystemDataAll = cfgInfoSpaceP_->getBool("filterShortSystemDataAll");
//   // bool const bitShortUserDataAll = cfgInfoSpaceP_->getBool("filterShortUserDataAll");
//   // bool const bitShortCommandsAll = cfgInfoSpaceP_->getBool("filterShortCommandsAll");
//   // bool const bitLongCommandsAll = cfgInfoSpaceP_->getBool("filterLongCommandsAll");
//   // bool const bitL1A = cfgInfoSpaceP_->getBool("filterL1A");
//   // bool const bitSingleBitError = cfgInfoSpaceP_->getBool("filterSingleBitError");
//   // bool const bitDoubleBitError = cfgInfoSpaceP_->getBool("filterDoubleBitError");
//   // bool const bitCommunicationError = cfgInfoSpaceP_->getBool("filterCommunicationError");

//   // uint32_t mask = createMask(bitBCntRes,
//   //                            bitEvCntRes,
//   //                            bitsShortSystemData,
//   //                            bitsShortUserData,
//   //                            bitShortSystemDataAll,
//   //                            bitShortUserDataAll,
//   //                            bitShortCommandsAll,
//   //                            bitLongCommandsAll,
//   //                            bitL1A,
//   //                            bitSingleBitError,
//   //                            bitDoubleBitError,
//   //                            bitCommunicationError);
//   // return mask;
//   // BUG BUG BUG end
//   return 0;
// }

// uint32_t
// tcds::ttcspy::TCADeviceTTCSpy::createTriggerMask() const
// {
//   // BUG BUG BUG
//   // bool const bitBCntRes = cfgInfoSpaceP_->getBool("triggerBCntRes");
//   // bool const bitEvCntRes = cfgInfoSpaceP_->getBool("triggerEvCntRes");
//   // uint32_t const bitsShortSystemData = cfgInfoSpaceP_->getUInt32("triggerShortSystemData");
//   // uint32_t const bitsShortUserData = cfgInfoSpaceP_->getUInt32("triggerShortUserData");
//   // bool const bitShortSystemDataAll = cfgInfoSpaceP_->getBool("triggerShortSystemDataAll");
//   // bool const bitShortUserDataAll = cfgInfoSpaceP_->getBool("triggerShortUserDataAll");
//   // bool const bitShortCommandsAll = cfgInfoSpaceP_->getBool("triggerShortCommandsAll");
//   // bool const bitLongCommandsAll = cfgInfoSpaceP_->getBool("triggerLongCommandsAll");
//   // bool const bitL1A = cfgInfoSpaceP_->getBool("triggerL1A");
//   // bool const bitSingleBitError = cfgInfoSpaceP_->getBool("triggerSingleBitError");
//   // bool const bitDoubleBitError = cfgInfoSpaceP_->getBool("triggerDoubleBitError");
//   // bool const bitCommunicationError = cfgInfoSpaceP_->getBool("triggerCommunicationError");

//   // uint32_t mask = createMask(bitBCntRes,
//   //                            bitEvCntRes,
//   //                            bitsShortSystemData,
//   //                            bitsShortUserData,
//   //                            bitShortSystemDataAll,
//   //                            bitShortUserDataAll,
//   //                            bitShortCommandsAll,
//   //                            bitLongCommandsAll,
//   //                            bitL1A,
//   //                            bitSingleBitError,
//   //                            bitDoubleBitError,
//   //                            bitCommunicationError);
//   return 0;
//   // BUG BUG BUG end
// }

// // tcds::ttcspy::TTCSpyMonitor*
// // tcds::ttcspy::TCADeviceTTCSpy::getMonitor() const
// // {
// //   return static_cast<TTCSpyMonitor*>(tcds::utils::HwDeviceGLIB::getMonitor());
// // }
