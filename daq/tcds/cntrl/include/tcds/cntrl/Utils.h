#ifndef _tcds_cntrl_Utils_h_
#define _tcds_cntrl_Utils_h_

namespace xdaq {
  class ApplicationDescriptor;
}

namespace tcds {
  namespace cntrl {

    // Helper to sort XDAQ applications by port number, host, and then
    // local-id.
    // NOTE: This sounds a bit counter-intuitive, but it works out
    // quite well with the naming/numbering scheme used in the TCDS.
    class compareApplications
    {

    public:
      bool operator() (xdaq::ApplicationDescriptor const* const& lhs,
                       xdaq::ApplicationDescriptor const* const& rhs) const;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_Utils_h_
