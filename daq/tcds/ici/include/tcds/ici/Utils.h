#ifndef _tcds_ici_Utils_h_
#define _tcds_ici_Utils_h_

#include <string>

#include "tcds/ici/Definitions.h"

namespace tcds {
  namespace ici {

    // Mapping of some B-channel-related types to strings.
    /* std::string BchannelEmissionModeToString(tcds::definitions::BCHANNEL_EMISSION_MODE const mode); */
    std::string BchannelTimingModeToString(tcds::definitions::BCHANNEL_TIMING_MODE const mode);

    void verifyCyclicGeneratorNumber(unsigned int const genNumber);

  } // namespace ici
} // namespace tdcs

#endif // _tcds_ici_Utils_h_
