#ifndef _tcds_utils_HwInfoSpaceUpdaterBase_h_
#define _tcds_utils_HwInfoSpaceUpdaterBase_h_

#include "tcds/utils/InfoSpaceUpdater.h"
#include "tcds/utils/TCDSObject.h"

namespace tcds {
  namespace hwlayer {
    class DeviceBase;
  }
}

namespace tcds {
  namespace utils {

    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;

    class HwInfoSpaceUpdaterBase :
      public tcds::utils::InfoSpaceUpdater,
      public tcds::utils::TCDSObject
    {

    public:
      virtual ~HwInfoSpaceUpdaterBase();

    protected:
      using TCDSObject::getOwnerApplication;

      HwInfoSpaceUpdaterBase(tcds::utils::XDAQAppBase& xdaqApp,
                             tcds::hwlayer::DeviceBase const& hw);

      virtual void updateInfoSpaceImpl(InfoSpaceHandler* const infoSpaceHandler);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      tcds::hwlayer::DeviceBase const& getHw() const;

    private:
      tcds::hwlayer::DeviceBase const& hw_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_HwInfoSpaceUpdaterBase_h_
