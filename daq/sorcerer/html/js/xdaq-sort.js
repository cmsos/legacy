/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: A.Forrest, L.Orsini, A.Petrucci								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

function xdaqAddSortHandlers()
{
	// homepage sort
	$(document).on('change', '#xdaq-hyperdaq-home-sort', function(e) {
		var option = $(this).val();
			
		var wrapper = $('#xdaq-hyperdaq-home-widget-wrapper');
		
		if (option == "default")
		{
			wrapper.find('.xdaq-hyperdaq-home-widget-container').sort(function (a, b) {
				return +a.getAttribute('data-id') - +b.getAttribute('data-id');
			})
			.appendTo( wrapper );
		}
		else if (option == "alphabetically")
		{
			wrapper.find('.xdaq-hyperdaq-home-widget-container').sort(function (a, b) {
				if(a.getAttribute('data-name') <= b.getAttribute('data-name')) 
				{
					return -1;
				}
				else
				{
					return 1;
				}
			})
			.appendTo(wrapper);
		}
		else if (option == "lid")
		{
			wrapper.find('.xdaq-hyperdaq-home-widget-container').sort(function (a, b) {
				return +a.getAttribute('data-lid') - +b.getAttribute('data-lid');
			})
			.appendTo(wrapper);
		}
		else if (option == "service" || option == "network")
		{
			wrapper.find('.xdaq-hyperdaq-home-widget-container').sort(function (a, b) {
				var aopt = a.getAttribute('data-' + option);
				var bopt = b.getAttribute('data-' + option);
				// force empty to end
				if (aopt == "")
				{
					aopt = "zzzzzzzzz";
				}
				if (bopt == "")
				{
					bopt = "zzzzzzzzz";
				}
								
				if(aopt <= bopt) 
				{
					return -1;
				}
				else
				{
					return 1;
				}
			})
			.appendTo(wrapper);
		}
		else
		{
			console.log("option not recognized");
		}
	});
	
	/* for existing .xdaq-sortable elements, remove the class from that element and append a span to the end of its content (similar to way tree works) */
	$('.xdaq-sortable').each(function(e) {
		$(this).append('<span class="xdaq-sorter" xdaq-auto-gen-tag="TH"><a href="#" title="Sort"> </a></span>');
	});
	
	/* Table sort */
	$(document).on('click', '.xdaq-sorter', function(e) {
		
		var sorter = $(this);
		var sorterType = e.target.nodeName;
		var wrapper = "";
		var targets = "";
		var option = "alpha";
		
		//console.log($(this));
		//console.log("Sorter is of type : "+sorterType);
		
		var index = $(this).parent().parent().children().index($(this).parent()) + 1;
		
		if (sorterType == "TH" | sorter.attr("xdaq-auto-gen-tag") == "TH")
		{
			wrapper = $(this).closest("table").children("tbody").first();
			//var index = $(this).parent().children().index($(this)) + 1;
		
			//console.log("index : "+index);
			
			//targets = "tr>td:nth-child("+index+")";
			targets = "tr";
		}
		else
		{
			if ($(this).attr("data-wrapper")) {
			    wrapper = $(this).attr("data-wrapper");
			}
			else
			{
				console.err("Could not establish wrapper for sorting");
			}	
			
			if ($(this).attr("data-targets")) {
			    targets = $(this).attr("data-type");
			}
			else
			{
				console.err("Could not establish targets for sorting");
			}	
		}
					
		if ($(this).attr("data-type")) {
		    option = $(this).attr("data-type");
		}
			
		var sorted = "none";
				
		if ($(this).attr("data-sorted")) {
		    sorted = $(this).attr("data-sorted");
		}
		
		// Alpha-numeric sort
		if (option == "alpha")
		{
			wrapper.children(targets).sort(function (a, b) {
				var res;
				
				var childA = $(a).children("td:nth-child("+index+")").first().text();
				var childB = $(b).children("td:nth-child("+index+")").first().text();
				
				//var resultOfThing = (childA <= childB)
				
				//console.log(childA+" <= "+childB+" = "+resultOfThing);
				
				if(childA <= childB) 
				{
					res = -1;
				}
				else
				{
					res = 1;
				}
				if (sorted == "none" || sorted == "desc")
				{
					sorter.attr("data-sorted", "asc");
					return res;
				}
				else
				{
					sorter.attr("data-sorted", "desc");
					return -res;
				}
			}).appendTo(wrapper);
		}
		// numeric sort
		else if (option == "numeric")
		{
			wrapper.children(targets).sort(function (a, b) {
				var childA = $(a).children("td:nth-child("+index+")").first().text();
				var childB = $(b).children("td:nth-child("+index+")").first().text();
				
				var res = +childA - +childB;
				if (sorted == "none" || sorted == "desc")
				{
					sorter.attr("data-sorted", "asc");
					return res;
				}
				else
				{
					sorter.attr("data-sorted", "desc");
					return -res;
				}
			}).appendTo( wrapper );
		}
		else
		{
			console.err("option not recognized");
		}
		
		var p = $(this);
		if (!p.hasClass("xdaq-sortable"))
		{
			p = p.closest(".xdaq-sortable");
		}
		
		// reset sorting icons on the rest of the table to default
		$(this).closest("table").find(".xdaq-sortable").each(function() {
			$(this).removeClass("xdaq-sortable-down");
			$(this).removeClass("xdaq-sortable-up");
		});
		
		if (sorted == "none" || sorted == "desc")
		{
			p.removeClass("xdaq-sortable-up");
			p.addClass("xdaq-sortable-down");
		}
		else
		{
			p.addClass("xdaq-sortable-up");
			p.removeClass("xdaq-sortable-down");
		}
		
		$(this).blur();
	});
	
	
}