// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tstore/View.h"
#include "tstore/client/LoadDOM.h"
#include "xoap/DOMParserFactory.h"
#include <iostream>

DOMNode *tstore::View::getConfiguration(std::string configurationPath)  {
	try {
		DOMDocument* doc=xoap::getDOMParserFactory()->get("configure")->loadXML(tstoreclient::parsePath(configurationPath));
		if (doc) {
			DOMNodeList* lists = doc->getElementsByTagNameNS(xoap::XStr("urn:xdaq-tstore:1.0"), xoap::XStr("view"));
			for (unsigned long i = 0; i < lists->getLength(); i++) {
				DOMNode* viewNode = lists->item(i);
				if (name_==xoap::getNodeAttribute(viewNode, "id")) {
					return viewNode;
				}
			}
		}
		return NULL;
	} catch (xoap::exception::Exception) {
		return NULL;
	}
}

tstore::View::View(std::string configurationPath,std::string name)   {
	setName(name);
}

void tstore::View::removeTables(TStoreAPI *API)  { 
	std::vector<std::string> tableNames;
	API->getTableNames(tableNames);
	std::vector<std::string>::const_iterator tableName;
	for (tableName=tableNames.begin();tableName!=tableNames.end();++tableName) {
		try {
			API->removeTable(*tableName);
		} catch (tstore::exception::Exception &e) {
			XCEPT_RETHROW(tstore::exception::Exception,"Could not drop table '"+*tableName+"' from the database.",e);
		}
	}
}

void tstore::View::addTables(TStoreAPI *API)  { 
	std::map<const std::string,std::string> tableNames;
	API->getTableNames(tableNames);
	std::map<const std::string,std::string>::const_iterator tableName=tableNames.begin();
	try {
		for (;tableName!=tableNames.end();++tableName) {
			xdata::Table definition;
			API->getTable(definition,(*tableName).first);
			API->addTable((*tableName).first,(*tableName).second,definition);
		}
	} catch (tstore::exception::Exception &addException) {
		std::map<const std::string,std::string>::const_iterator failedTable=tableName;
		std::string errorMessage="Could not create table '"+(*failedTable).first+"'.";
		for (tableName=tableNames.begin();tableName!=failedTable;++tableName) {
			//API->cancelAddTable(tableName);
			try {
				API->removeTable((*tableName).first);
			} catch (tstore::exception::Exception &removeException) {
				errorMessage+=" Additionally, encountered the following error while trying to clean up by removing '"+(*tableName).first+"': "+removeException.what();
			}
		}
		XCEPT_RETHROW(tstore::exception::Exception,errorMessage,addException);
	}
}

void tstore::View::readParameterDefinition(parameterList &parameters,parameterList &bindParameters,DOMNode *node) {
	std::string name=xoap::getNodeAttribute(node, "name");
	//std::cout << "parameter name is " << name << std::endl;
	if (name!="") {
		DOMNodeList *children=node->getChildNodes();
		int childrenCount=children->getLength();
		int childIndex;
		std::string defaultValue=""; //in case there is no default value set, the default value should be an empty string
		for (childIndex=0;childIndex<childrenCount;childIndex++) {
			DOMNode *child=children->item(childIndex);
			if ((DOMNode::NodeType)child->getNodeType()==DOMNode::CDATA_SECTION_NODE) {
				defaultValue=xoap::XMLCh2String(child->getNodeValue());
			}
		}
		if (xoap::getNodeAttribute(node,"bind")=="yes") {
			bindParameters[name]=defaultValue;
			//std::cout << "Read bind parameter " << name << " with default value " << defaultValue << std::endl;
		} else {
			parameters[name]=defaultValue;
			//std::cout << "Read parameter " << name << " with default value " << defaultValue << std::endl;
		}
	}
}

void tstore::View::setParameter(parameterList &parameters,parameterList &bindParameters,const std::string &parameterName,const std::string &value)  {
	if (parameters.count(parameterName)) {
		parameters[parameterName]=value;
	} else if (bindParameters.count(parameterName)) {
		bindParameters[parameterName]=value;
	} else { //if there were a parameter with this name, its default value would have been set in the map on initialisation
		XCEPT_RAISE(tstore::exception::Exception, "No such parameter: "+parameterName);
	}
}

/*
From Oracle Database SQL Reference:
Nonquoted identifiers can contain only alphanumeric characters from your
database character set and the underscore (_), dollar sign ($), and pound sign (#).
Database links can also contain periods (.) and "at" signs (@). Oracle strongly
discourages you from using $ and # in nonquoted identifiers.
Quoted identifiers can contain any characters and punctuations marks as well as
spaces. However, neither quoted nor nonquoted identifiers can contain double
quotation marks or the null character (\0).

So I suppose we should use something other than $ to denote parameters, but the parsing routine knows which parameter names
to look for so it won't mistake a $... sequence for a parameter unless it happens to match a parameter name. In that case, the
parameter name could easily enough be changed.
*/

const std::string parameterMarker="$";

std::string tstore::View::substituteParameters(std::vector<std::string> *bindParameterValues,parameterList &parameters,parameterList &bindParameters,const std::string &statement) { 
	//make a copy of query_ and replace any parameter names with their values
	if (parameters.empty() && bindParameters.empty()) return statement;
	
	std::string parsedStatement;
	std::string::size_type possibleParameterPosition=0;
	std::string::size_type startSearch=0;
	while (std::string::npos!=(possibleParameterPosition=statement.find(parameterMarker,startSearch))) {
		bool parameterFound=false;
		std::map<const std::string,std::string>::iterator parameter;
		//look for plain string-substitute parameters
		for (parameter=parameters.begin();parameter!=parameters.end() && !parameterFound;++parameter) {
			if (possibleParameterPosition+1==statement.find((*parameter).first,possibleParameterPosition)) {
				parsedStatement+=statement.substr(startSearch,possibleParameterPosition-startSearch)+(*parameter).second;
				startSearch=possibleParameterPosition+1+(*parameter).first.size();
				parameterFound=true;
			}
		}
		for (parameter=bindParameters.begin();parameter!=bindParameters.end() && !parameterFound;++parameter) {
			if (possibleParameterPosition+1==statement.find((*parameter).first,possibleParameterPosition)) {
				parsedStatement+=statement.substr(startSearch,possibleParameterPosition-startSearch)+":"+(*parameter).first;
				startSearch=possibleParameterPosition+1+(*parameter).first.size();
				if (bindParameterValues) bindParameterValues->push_back((*parameter).second);
				parameterFound=true;
			}
		}
		if (!parameterFound) { //if we didn't find a matching parameter name
			startSearch=possibleParameterPosition+1;
		}
	}
	parsedStatement+=statement.substr(startSearch);
	return parsedStatement;
}
