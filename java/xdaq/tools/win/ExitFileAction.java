package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ExitFileAction extends AbstractAction
{
	xdaqWin frame_;
	
	public ExitFileAction (xdaqWin frame)
	{
		super ("Exit");
		frame_ = frame;
		
		// What to do, when the main window is closed.
		//
		frame_.setDefaultCloseOperation (
			WindowConstants.DO_NOTHING_ON_CLOSE);
			
		WindowListener l = new WindowAdapter () 
		{
			public void windowClosing (WindowEvent e) 
			{
				closeAction();
			}
		};
		
		frame_.addWindowListener (l);
	}
	
	public void actionPerformed (ActionEvent e)
	{
		this.closeAction();
	}
	
	public void closeAction()
	{
		int confirm = JOptionPane.showOptionDialog (null, "Really Exit?",
			"Confirmation",
			JOptionPane.YES_NO_OPTION,
			JOptionPane.QUESTION_MESSAGE,
			null, null, null);
		if (confirm == 0)
		{
			frame_.dispose();
			System.exit(0);
		}
	}
}
