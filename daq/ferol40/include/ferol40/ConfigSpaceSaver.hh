#ifndef __ConfigSpaceSaver
#define __ConfigSpaceSaver

#include "hal/PCIDevice.hh"
#include "ferol40/ferol40Constants.h"

namespace ferol40 
{
    class ConfigSpaceSaver
    {
    public:
        ConfigSpaceSaver( HAL::PCIDevice **device );
        virtual ~ConfigSpaceSaver() {};
        void saveConfigSpace() throw( HAL::HardwareAccessException );
        void writeConfigSpace() throw( HAL::HardwareAccessException );
        void rescanBAbits() throw( HAL::HardwareAccessException );
        virtual void hwlock() = 0;
        virtual void hwunlock() = 0;
    private:
        uint32_t configSpace_[16];
        HAL::PCIDevice **device_H;
    };
}

#endif // __ConfigSpaceSaver
