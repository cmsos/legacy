package test.wse;

import java.lang.*;

public class Client extends Thread
{
	private WseAPI wse;

	private String eventingurn;
	private String consumerurn;
	private int port;
	private String xpath;

	private int expire;
	private int renew;

	private String subscriptionid;

	public static void main(String[] args)
	{
		String eventingurn = "http://lxcmd101:1980/urn:xdaq-application:lid=400";
		int port = 1111;
		String xpath = "//*";
		// no expire specified
		int expire = 0;
		// no renew is done by default
		int renew = 0;

		if(args.length>0)
		{
			eventingurn = args[0];
		}
		if(args.length>1)
		{
			port = Integer.parseInt(args[1]);
		}
		if(args.length>2)
		{
			xpath = args[2];
		}
		if(args.length>3)
		{
			expire = Integer.parseInt(args[3]);
		}
		if(args.length>4)
		{
			renew = Integer.parseInt(args[4]);
		}

		Client client = new Client(eventingurn, port, xpath, expire, renew);

		try
		{
			client.listen();
			client.subscribe();
		}
		catch(WseException e)
		{
			// close all open sockets, ...
	 		client.cleanup();
			e.printStackTrace();
		}
	}

	public Client(String eventingurn, int port, String xpath, int expire, int renew)
	{
		this.eventingurn=eventingurn;
		this.port=port;
		this.xpath=xpath;
		this.expire=expire;
		this.renew=renew;

		// create simple wrapper class for subscribe, listen, renew, ...
		wse = new WseAPI();

		subscriptionid = null;

		// shutdown gracefully in case of ctrl+c
		Runtime.getRuntime().addShutdownHook(this);

		System.out.println("WS-Eventing URL: ["+eventingurn+"]");
	}

	// start Listener - received soap messages are forwarded to this handler
	public void listen() throws WseException
	{
		Consumer handler = new Consumer();
		consumerurn=wse.listen(port, handler);
		System.out.println("Consumer URL: ["+consumerurn+"]");
	}

	// subscribe to eventing URL with listener on consumer URL
	public void subscribe() throws WseException
	{
		subscriptionid = wse.subscribe(eventingurn, consumerurn, xpath, expire, renew);
	}

	public void cleanup()
	{
		wse.cleanup();
	}

	public void run()
	{
		if(subscriptionid != null)
		{
			System.out.println ("unsubscribe: "+subscriptionid);
			try
			{
				wse.unsubscribe(eventingurn, subscriptionid);
			}
			catch(WseException e)
			{
				System.err.println(e.getMessage());
			}
		}
	}
}

