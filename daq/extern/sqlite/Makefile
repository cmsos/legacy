# $Id: Makefile,v 1.12 2009/03/04 10:41:12 lorsini Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
Package=extern/sqlite

PackageName=sqlite

PACKAGE_VER_MAJOR=3
PACKAGE_VER_MINOR=15
PACKAGE_VER_PATCH=2

Summary= SQLite is a software library that implements SQL database engine

Description= \
SQLite is a C library that implements an embeddable SQL database engine. \
Programs that link with the SQLite library can have SQL database access \
without running a separate RDBMS process. The distribution comes with a \
standalone command-line access program (sqlite) that can be used to \
administer an SQLite database and which serves as an example of how to \
use the SQLite library.


Link=http://www.sqlite.org

CFLAGS:=${CFlags}
CPPFLAGS:=${CCFlags}
export CFLAGS
export CPPFLAGS

export CC
export CXX
LD:=$(LDD)
export LD

build: _buildall

_buildall: all

_all: all

default: all

sqlite-autoconf-3150200/configure:
	-rm -rf ./$(XDAQ_PLATFORM);
	-rm -rf ./sqlite-autoconf-3150200;
	tar -zxf sqlite-autoconf-3150200.tar.gz;
	cd ./sqlite-autoconf-3150200
all: sqlite-autoconf-3150200/configure
	cd ./sqlite-autoconf-3150200; \
	./configure --enable-threadsafe --enable-cross-thread-connection --prefix $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM); \
	make; make install

_installall: install

install:
	cd ./sqlite-autoconf-3150200; \
	./configure --enable-threadsafe --enable-cross-thread-connection --prefix $(INSTALL_PREFIX)/$(XDAQ_PLATFORM); \
	make; make install

_cleanall: clean
clean:
	-rm -rf ./$(XDAQ_PLATFORM);
	-rm -rf ./sqlite-autoconf-3150200

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules
