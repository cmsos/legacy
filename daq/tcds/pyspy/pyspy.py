#!/usr/bin/env python

###############################################################################

import time

from po_boys_uhal import AddressTable
from po_boys_uhal import IPBusDeviceUDP
# BUG BUG BUG
# Should get rid of this one at this level.
from log_entry import LAST_BX
from log_entry import LogEntry
# BUG BUG BUG end
from log_contents import LogContents

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PySpy(IPBusDeviceUDP):

    # This is the number of four-word entries in the TTCSpy memory in
    # firmware. So the total size is 16k times 4 = 65536 IPbus words
    # (of 32 bits each).
    SPY_MEM_ENTRIES_IN_FW = 16384
    # This is the number of memory entries used in the PySpy.
    # NOTE: Since this is all Python-based and PySpyRunner uses
    # curses, it would a) be too slow, and b) not even work (i.e., too
    # many lines for curses to handle) with the full TTCSpy memory.
    SPY_MEM_ENTRIES = 1024

    # The GLIB implements the XILINX ICAP business to load firmware
    # from two difference images stored in flash memory:
    # - page 0: The 'golden' image, supposed to always work. Loaded at power-on.
    # - page 2: The 'user' image. Can be loaded via a command.
    FLASH_PAGE_NUM_GOLDEN = 0
    FLASH_PAGE_NUM_USER = 2

    # After triggering a firmware reload, give the board some time to
    # actually load the firmware.
    TIME_WAIT_FIRMWARE_LOAD = 2

    # A magic constant related to one of the GLIB cross-point switches.
    GLIB_XPOINT_AND_MASK = 0xff000fff

    # These are the different sources of the (GLIB) clock, basically.
    # - Select fmc1_clk0 (the clock coming from FMC1).
    FMC1_CLK0_OR_MASK = 0x00000000
    # - Select the GLIB on-board default clock (xtal/sma).
    SMA_OR_XTAL_OR_MASK = 0x00aa0000
    # - Select FCLKA (the clock coming from the backplane).
    FCLKA_OR_MASK = 0x00ff0000

    # TTC stream sources: either front-panel or backplane.
    TTC_SOURCE_FRONTPANEL = 0
    TTC_SOURCE_BACKPLANE = 1

    def __init__(self, address_table_file_name, ip_address, port_number):
        address_table = AddressTable(address_table_file_name)
        IPBusDeviceUDP.__init__(self, address_table, ip_address, port_number)
        # Now make sure that this is a TTCSpy we're connected to.
        system_id = self.read_as_string("ttcspy.system_id")
        # This works only with a dedicated (GLIB-based) TTCSpy, not
        # with the (FC7-based) TTCSpy in the PI.
        system_id_ttcspy = "TTCs"
        if system_id != system_id_ttcspy:
            if system_id == "gold":
                msg = "Device at address {0:s} looks like it's on the golden firmware image. " \
                      "Trying to load the user firmware image."
                msg = msg.format(ip_address)
                print msg
                self.load_firmware("user")
                system_id = self.read_as_string("ttcspy.system_id")
            if system_id != system_id_ttcspy:
                msg = "Device at address {0:s} is not a TTCSpy " \
                      "(but something with system id '{1:s}')."
                msg = msg.format(ip_address, system_id)
                raise RuntimeError(msg)
        # Normally this should result in SPY_MEM_ENTRIES, but one never knows.
        self._spy_mem_size = min(PySpy.SPY_MEM_ENTRIES, PySpy.SPY_MEM_ENTRIES_IN_FW)
        self._ttc_source = None
        # End of __init__().

    def fw_version_sys(self):
        version_major = self.read("glib.firmware_id.ver_major")
        version_minor = self.read("glib.firmware_id.ver_minor")
        version_build = self.read("glib.firmware_id.ver_build")
        res = ".".join([str(version_major), str(version_minor), str(version_build)])
        # End of fw_version_sys().
        return res

    def fw_version_usr(self):
        version_major = self.read("ttcspy.firmware_id.ver_major")
        version_minor = self.read("ttcspy.firmware_id.ver_minor")
        version_build = self.read("ttcspy.firmware_id.ver_build")
        res = ".".join([str(version_major), str(version_minor), str(version_build)])
        # End of fw_version_usr().
        return res

    def load_firmware(self, image_name=None):
        # If a firmware image is specified: tell the hardware which
        # firmware page to jump to.
        if image_name:
            tmp_image_name = image_name.lower()
            if tmp_image_name == "golden":
                image_number = PySpy.FLASH_PAGE_NUM_GOLDEN
            elif tmp_image_name == "user":
                image_number = PySpy.FLASH_PAGE_NUM_USER
            else:
                # ASSERT ASSERT ASSERT
                msg = "ERROR Unknown firmware image: '{0:s}'.".format(image_name)
                assert False, msg
                # ASSERT ASSERT ASSERT end
            self.write("glib.ctrl2.flash_firmware_page", image_number)

        # Perform the jump/load the firmware.
        self.write("glib.ctrl2.load_flash_firmware", 0x1)
        # Give the board a little bit of time to recover.
        time.sleep(PySpy.TIME_WAIT_FIRMWARE_LOAD)
        # End of load_firmware().

    def configure_glib_xpoint_switch(self, source):
        # Configure the GLIB cross-point switch defining the clocking
        # scheme.
        reg_name = "glib.ctrl"
        ctrl = self.read(reg_name)
        or_mask = None
        tmp_source = source.lower()
        if tmp_source == "fmc1_clk0":
            or_mask = PySpy.FMC1_CLK0_OR_MASK
        elif tmp_source == "sma_or_xtal":
            or_mask = PySpy.SMA_OR_XTAL_OR_MASK
        elif tmp_source == "fclka":
            or_mask = PySpy.FCLKA_OR_MASK
        else:
            # ASSERT ASSERT ASSERT
            assert False, "Clock source '{0:s}' not understood.".format(source)
            # ASSERT ASSERT ASSERT end
        ctrl = (ctrl & PySpy.GLIB_XPOINT_AND_MASK) | or_mask
        self.write(reg_name, ctrl)
        # End of configure_glib_xpoint_switch().

    def select_source(self, source):
        """Choose between the front-panel and backplane TTC streams."""
        reg_name = "ttcspy.spy_source"
        tmp_source = source.lower()
        if tmp_source == "frontpanel":
            self.write(reg_name, 0x0)
            self.configure_glib_xpoint_switch("fmc1_clk0")
            # Configure the xpoint switch on the TTC FMC to use the 40
            # MHz clock from its crystal (instead of from the incoming
            # TTC stream).
            self.write("ttcspy.clock_control", 0x23D)
            self._ttc_source = PySpy.TTC_SOURCE_FRONTPANEL
        elif tmp_source == "backplane":
            self.write(reg_name, 0x1)
            self.configure_glib_xpoint_switch("fclka")
            self._ttc_source = PySpy.TTC_SOURCE_BACKPLANE
        else:
            # ASSERT ASSERT ASSERT
            assert False, "Source '{0:s}' not understood.".format(source)
            # ASSERT ASSERT ASSERT end
        # End of select_source().

    def get_source(self):
        reg_name = "ttcspy.spy_source"
        res = self.read(reg_name)
        # End of get_source().
        return res

    def set_logging_mode(self, mode):
        reg_name = "ttcspy.logging_control.logging_mode"
        tmp_mode = mode.lower()
        if tmp_mode == "log_only":
            self.write(reg_name, 0x0)
        elif tmp_mode == "log_all_except":
            self.write(reg_name, 0x1)
        else:
            # ASSERT ASSERT ASSERT
            assert False, "Logging mode '{0:s}' not understood.".format(mode)
            # ASSERT ASSERT ASSERT end
        # End of set_logging_mode().

    def set_logging_logic(self, logic):
        reg_name = "ttcspy.logging_control.trigger_combination_operator"
        tmp_logic = logic.lower()
        if tmp_logic == "or":
            self.write(reg_name, 0x0)
        elif tmp_logic == "and":
            self.write(reg_name, 0x1)
        else:
            # ASSERT ASSERT ASSERT
            assert False, "Logging logic '{0:s}' not understood.".format(logic)
            # ASSERT ASSERT ASSERT end
        # End of set_logging_logic().

    def set_bc0_val(self, bc0_val):
        self.write("ttcspy.logging_control.bc0_val", bc0_val)
        # End of set_bc0_val().

    def set_bc0_bx(self, bc0_bx):
        self.write("ttcspy.logging_control.expected_bc0_bx", bc0_bx)
        # End of set_bc0_bx().

    def set_logging_buffer_mode(self, buffer_mode):
        reg_name = "ttcspy.logging_control.logging_buffer_mode"
        tmp_buffer_mode = buffer_mode.lower()
        if tmp_buffer_mode == "single_shot":
            self.write(reg_name, 0x0)
        elif tmp_buffer_mode == "rolling":
            self.write(reg_name, 0x1)
        else:
            # ASSERT ASSERT ASSERT
            assert False, "Logging buffer mode '{0:s}' not understood.".format(logic)
            # ASSERT ASSERT ASSERT end
        # End of set_logging_buffer_mode();

    def spy_mem_size(self):
        return self._spy_mem_size

    def is_spy_mem_full(self):
        tmp = self.read("ttcspy.logging_status.spy_memory_full")
        res = (tmp != 0)
        # End of is_spy_mem_full().
        return res

    def get_spy_mem_contents_raw(self):
        if self.is_spy_mem_full():
            num_entries = self.spy_mem_size()
        else:
            mem_spy_pointer = self.read("ttcspy.logging_status.spy_memory_address_pointer")
            num_entries = min(mem_spy_pointer, self.spy_mem_size())
        num_words = num_entries * LogEntry.NUM_WORDS_PER_SPY_MEM_ENTRY
        raw_mem_contents = self.read_block("ttcspy.spy_memory", num_words)
        # End of get_spy_mem_contents_raw().
        return raw_mem_contents

    def get_spy_mem_contents_formatted(self, verbose=False):
        tmp = self.get_spy_mem_contents_raw()
        backplane_mode = (self._ttc_source == PySpy.TTC_SOURCE_BACKPLANE)
        log_contents = LogContents(tmp, backplane_mode)
        res = log_contents.get_formatted(verbose)
        # End of get_spy_mem_contents_formatted().
        return res

    def is_cdr_ok(self):
        lol = self.read("ttcspy.signal_status.loss_of_lock")
        los = self.read("ttcspy.signal_status.loss_of_signal")
        res = ((not lol) and (not los))
        # End of is_cdr_ok().
        return res

    def is_clk_ok(self):
        tmp = self.read("ttcspy.signal_status.ttc_clock_up")
        res = (tmp != 0)
        # End of is_clk_ok().
        return res

    def start_logging(self):
        self.write("ttcspy.logging_control.enabled", 0x1)
        # End of start_logging().

    def stop_logging(self):
        self.write("ttcspy.logging_control.enabled", 0x0)
        # End of stop_logging().

    def reset_logging(self):
        self.write("ttcspy.logging_control.reset", 0x1)
        self.write("ttcspy.logging_control.reset", 0x0)
        # End of reset_logging().

    def clear_memory(self):
        mem_size = self.spy_mem_size() * LogEntry.NUM_WORDS_PER_SPY_MEM_ENTRY
        zeroes = [0x0] * mem_size
        self.write_block("ttcspy.spy_memory", zeroes)
        # End of clear_memory().

    def set_trigger_mask(self,
                         brc_b=False,
                         brc_e=False,
                         brc_dddd=0x0,
                         brc_tt=0x0,
                         brc_dddd_all=False,
                         brc_tt_all=False,
                         brc_all=False,
                         adr_all=False,
                         l1accept=False,
                         brc_zero_data=False,
                         adr_zero_data=False,
                         err_com=False,
                         brc_val0=0x0,
                         brc_val1=0x0,
                         brc_val2=0x0,
                         brc_val3=0x0,
                         brc_val4=0x0,
                         brc_val5=0x0):
        self.write("ttcspy.trigger_mask.brc_bc0", brc_b)
        self.write("ttcspy.trigger_mask.brc_ec0", brc_e)
        self.write("ttcspy.trigger_mask.brc_dddd", brc_dddd)
        self.write("ttcspy.trigger_mask.brc_tt", brc_tt)
        self.write("ttcspy.trigger_mask.brc_dddd_all", brc_dddd_all)
        self.write("ttcspy.trigger_mask.brc_tt_all", brc_tt_all)
        self.write("ttcspy.trigger_mask.brc_all", brc_all)
        self.write("ttcspy.trigger_mask.add_all", adr_all)
        self.write("ttcspy.trigger_mask.l1a", l1accept)
        self.write("ttcspy.trigger_mask.brc_zero_data", brc_zero_data)
        self.write("ttcspy.trigger_mask.adr_zero_data", adr_zero_data)
        self.write("ttcspy.trigger_mask.err_com", err_com)
        self.write("ttcspy.trigger_mask.brc_val0", brc_val0)
        self.write("ttcspy.trigger_mask.brc_val1", brc_val1)
        self.write("ttcspy.trigger_mask.brc_val2", brc_val2)
        self.write("ttcspy.trigger_mask.brc_val3", brc_val3)
        self.write("ttcspy.trigger_mask.brc_val4", brc_val4)
        self.write("ttcspy.trigger_mask.brc_val5", brc_val5)
        # End of set_trigger_mask().

    # End of class PySpy.

###############################################################################
