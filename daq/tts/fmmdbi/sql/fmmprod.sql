select * from firmware, log, fmmuser where
  firmware.logid = log.logid
  AND log.userid = fmmuser.userid;

insert into board values ('31000008', 'FMM');

insert into log values (3, sysdate, 1, 'update'); 
insert into fw_load_log values (05092100, '31000008', 3, 'pcepcmd50.cern.ch');


/* get latest fw on board */
SELECT * FROM fw_load_log, log where fw_load_log.logid = log.logid 
AND log.logdate = (
SELECT max(log.logdate) from fw_load_log, log where fw_load_log.logid = log.logid 
and serialnumber = 3100102010040200001);

select test.testid, test.serialnumber, log.logdate, log.login, log. remarks, 
fwid_altera, fwid_xilinx, tester_sn, finish_time, TO_CHAR(finish_time - logdate, 'HH:MM:SS'), fmm_basictest_result.*
from test, log, fmm_stage2_details, fmm_basictest_result where
test.mainlogid = log.logid AND
test.testid = fmm_stage2_details.testid AND
test.testid = fmm_basictest_result.testid;


select test.testid, test.serialnumber, log.logdate, log.login, log. remarks, 
fwid_altera, fwid_xilinx, tester_sn, finish_time, 
ROUND((finish_time - logdate)*24,2) as "hours" , fmm_basictest_result.*,
ROUND(duration_seconds/360000000,4) as "hours1"
from test, log, fmm_stage2_details, fmm_basictest_result where
test.mainlogid = log.logid AND
test.testid = fmm_stage2_details.testid AND
test.testid = fmm_basictest_result.testid;



select test.*, fmm_stage2_details.*, 
ROUND((finish_time - testdate)*24,2) as "hours", fmm_basictest_result.*,
ROUND(duration_seconds/3600000000,4) as "hours1"
from test, fmm_stage2_details, fmm_basictest_result where
test.testid = fmm_stage2_details.testid AND
test.testid = fmm_basictest_result.testid
order by test.testid desc ;


insert into test values(test_testid_sq.nextval, '3100102010040200001', '2005-11-04', 'hsakulin', 'stage1');
insert into fmm_stage1_details values(test_testid_sq.currval, 872, 270);

insert into log values(log_logid_sq.nextval, sysdate, 'hsakulin', 'dead-times disagree only by slightly more than 1%');
insert into test_log values (507, log_logid_sq.currval);


select * from board_log, log where
board_log.logid = log.logid;

select test.serialnumber, log.* from test, test_log, log where
test.testid = test_log.testid and test_log.logid = log.logid;  