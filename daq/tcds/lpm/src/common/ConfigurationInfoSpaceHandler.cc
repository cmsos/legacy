#include "tcds/lpm/ConfigurationInfoSpaceHandler.h"

#include <stddef.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/pm/Definitions.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::lpm::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA(xdaqApp)
{
  // The FED id number (twelve bits, strictly speaking) to be used in
  // the DAQ link.
  createUInt32("fedId",
               0,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  // The FED vector.
  createString("fedEnableMask",
               "",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);

  //----------

  // Text labels for each of the TTS inputs.
  // For the iCIs.
  for (size_t iciNum = 1; iciNum <= tcds::definitions::kNumICIsPerLPM; ++iciNum)
    {
      createString(toolbox::toString("partitionLabelICI%d", iciNum),
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }
  // For the APVEs.
  for (size_t apveNum = 1; apveNum <= tcds::definitions::kNumAPVEsPerLPM; ++apveNum)
    {
      createString(toolbox::toString("partitionLabelAPVE%d", apveNum),
                   "",
                   "",
                   tcds::utils::InfoSpaceItem::NOUPDATE,
                   true);
    }
}

void
tcds::lpm::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  tcds::hwutilstca::ConfigurationInfoSpaceHandlerTCA::registerItemSetsWithMonitor(monitor);

  std::string const itemSetName = "Application configuration";

  // The FED id.
  monitor.addItem(itemSetName,
                  "fedId",
                  "FED id",
                  this);

  // The FED vector.
  monitor.addItem(itemSetName,
                  "fedEnableMask",
                  "FED vector",
                  this);
}

std::string
tcds::lpm::ConfigurationInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "fedEnableMask")
        {
          // This thing can become annoyingly long. The idea here is
          // to insert some zero-width spaces in order to make it
          // break into pieces when formatted in the web interface.
          // NOTE: Not very efficient/pretty, but it works.
          std::string const valRaw = getString(name);
          std::string const valNew = tcds::utils::chunkifyFEDVector(valRaw);
          res = tcds::utils::escapeAsJSONString(valNew);
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
