#include "gevb2g/RU.h"
#include "i2o/utils/AddressMap.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"

// Log4CPLUS
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"
#include "toolbox/hexdump.h"


#include <byteswap.h>

#include "toolbox/task/WorkLoopFactory.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL (gevb2g::RU)

gevb2g::RU::RU (xdaq::ApplicationStub* c) 
	: xdaq::Application(c), xgi::framework::UIManager(this)
{
	i2o::bind(this, &gevb2g::RU::shipFragments, I2O_SHIP_FRAGMENTS, XDAQ_ORGANIZATION_ID);
	i2o::bind(this, &gevb2g::RU::dataFragmentAvailable, I2O_SUPER_FRAGMENT_READY, XDAQ_ORGANIZATION_ID);

	xoap::bind(this, &gevb2g::RU::Configure, "Configure", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::RU::Enable, "Enable", XDAQ_NS_URI);
	xoap::bind(this, &gevb2g::RU::Halt, "Halt", XDAQ_NS_URI);

	stopped_ = true;

	mutex_ = new toolbox::BSem(toolbox::BSem::FULL);

	// set default values
	inputDataFifoSize_ = 2048;
	maxRequestsQueue_ = 2048;
	maxDataFrameSize_ = 4096;

	createPool_ = true;
	poolName_ = "ruPool";

	preAllocateDAPL_ = 600;
	this->dropAtRU_ = false;

	this->counter_ = 0;

	this->state_ = "halted";

	this->doPacking_ = false;
	this->packingSize_ = 0x10000; // 64kb

	// export variables to run control
	getApplicationInfoSpace()->fireItemAvailable("maxRequestsQueue", &maxRequestsQueue_);
	getApplicationInfoSpace()->fireItemAvailable("maxDataFrameSize", &maxDataFrameSize_);
	getApplicationInfoSpace()->fireItemAvailable("frameSendCounter", &frameSendCounter_);
	getApplicationInfoSpace()->fireItemAvailable("inputDataFifoSize", &inputDataFifoSize_);

	getApplicationInfoSpace()->fireItemAvailable("createPool", &createPool_);
	getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);

	getApplicationInfoSpace()->fireItemAvailable("dropAtRU", &dropAtRU_);
	getApplicationInfoSpace()->fireItemAvailable("counter", &counter_);
	getApplicationInfoSpace()->fireItemAvailable("preAllocateDAPL", &preAllocateDAPL_);
	getApplicationInfoSpace()->fireItemAvailable("state", &state_);

	getApplicationInfoSpace()->fireItemAvailable("doPacking", &doPacking_);
	getApplicationInfoSpace()->fireItemAvailable("packingSize", &packingSize_);

	LOG4CPLUS_DEBUG(getApplicationLogger(), "going to retrieve BU descritptors");
	std::set<const xdaq::ApplicationDescriptor*> destination;

	try
	{
		destination = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("gevb2g::BU");
	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW(xcept::Exception, "No Server application instance found. Client cannot be configured.", e);
	}

	//requestQueue_.setName("RU/requestQueue_");
	//inputDataFifo_.setName("RU/inputDataFifo_");

	outputQueue_.resize(destination.size());
	outputDescriptor_.resize(destination.size());
	packedReferences_.resize(destination.size());

	LOG4CPLUS_DEBUG(getApplicationLogger(), "going to assign TID for BU descritptors for " << destination.size());
	std::set<const xdaq::ApplicationDescriptor*>::iterator iter;

	for (iter = destination.begin(); iter != destination.end(); iter++)
	{
		LOG4CPLUS_DEBUG(getApplicationLogger(), "going to assign TID on " << (*iter)->getInstance());
		size_t instance = (*iter)->getInstance();
		outputQueue_[instance].tid = i2o::utils::getAddressMap()->getTid(*iter);
		outputDescriptor_[instance] = *iter;
		packedReferences_[instance] = 0;
	}

	LOG4CPLUS_DEBUG(getApplicationLogger(), "get RU local TID");
	tid_ = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());

	LOG4CPLUS_DEBUG(getApplicationLogger(), "dobe CTOR for RU");

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}
void gevb2g::RU::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		if (doPacking_)
		{
			std::stringstream ss;
			ss << "Using packing with size of " << packingSize_;
			LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
		}

		requestQueue_ = toolbox::rlist < ShipRequest > ::create("gevb2g-RU-rlist-requestqueue", maxRequestsQueue_);
		inputDataFifo_ = toolbox::rlist<toolbox::mem::Reference*>::create("gevb2g-RU-rlist-inputdataFIFO", inputDataFifoSize_);

		try
		{
			if (createPool_)
			{
				toolbox::mem::HeapAllocator* a = new toolbox::mem::HeapAllocator();
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			}
			else
			{
				toolbox::net::URN urn("toolbox-mem-pool", poolName_);
				pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
			}
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
			return;
		}

		process_ = toolbox::task::bind(this, &gevb2g::RU::process, "process");

		try
		{
			std::stringstream wlss;
			wlss << "gevb2g-ru-loop-" << this->getApplicationDescriptor()->getInstance();
			toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->activate();
			toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->submit(process_);
		}
		catch (toolbox::task::exception::Exception& e)
		{
			std::stringstream ss;
			ss << "Failed to submit workloop ";

			std::cerr << ss.str() << std::endl;
		}
		catch (std::exception& se)
		{
			std::stringstream ss;
			ss << "Failed to submit notification to worker thread, caught standard exception '";
			ss << se.what() << "'";
			std::cerr << ss.str() << std::endl;
		}
		catch (...)
		{
			std::cerr << "Failed to submit notification to worker pool, caught unknown exception" << std::endl;
		}

	}
}

bool gevb2g::RU::process (toolbox::task::WorkLoop* wl)
{
	while (true)
	{
		this->matchRequests();
		if (stopped_)
		{
			this->flush();
		}
	}

	return false;
}

xoap::MessageReference gevb2g::RU::Configure (xoap::MessageReference message) 
{
	this->state_ = "configured";
	frameSendCounter_ = 0;

	//clear any spurious data from previous runs
	this->clear();

	requestQueue_->resize(maxRequestsQueue_);
	inputDataFifo_->resize(inputDataFifoSize_);
	std::vector<toolbox::mem::Reference*> samples(preAllocateDAPL_);
	//samples.resize(900);
	for (size_t k = 0; k < preAllocateDAPL_; k++)
	{
		try
		{
			toolbox::mem::Reference* dref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxDataFrameSize_);
			samples[k] = dref;
			//samples.push_back(dref);
			//	dref->release();
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to allocate mem ref : " << xcept::stdformat_exception_history(e));
			//return false;
		}
	}
	for (unsigned int i = 0; i < preAllocateDAPL_; ++i)
	{
		samples[i]->release();
	}

	LOG4CPLUS_INFO(this->getApplicationLogger(), "Successfully configured");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("ConfigureResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;
}


void gevb2g::RU::dataFragmentAvailable (toolbox::mem::Reference * ref)
{
	counter_++;

	if (dropAtRU_)
	{
		ref->release();
		return;
	}

	if (stopped_)
	{
		ref->release();
		return;
	}

	// just push chain into inpout fifo
	try
	{
		mutex_->take();
		inputDataFifo_->push_back(ref);
		mutex_->give();

	}
	catch (toolbox::exception::Exception te)
	{
		mutex_->give();
		ref->release();
		LOG4CPLUS_FATAL(getApplicationLogger(), "RU input overflow (input data). Stopping operation.");
		stopped_ = true;
		//state(Failed);
	}

	// check whether a request for shipment is available
	//this->matchRequests();
}

void gevb2g::RU::shipFragments (toolbox::mem::Reference * ref)
{
	if (stopped_)
	{
		ref->release();
		return;
	}

	PI2O_SHIP_FRAGMENTS_MESSAGE_FRAME frame = (PI2O_SHIP_FRAGMENTS_MESSAGE_FRAME) ref->getDataLocation();
	LOG4CPLUS_DEBUG(getApplicationLogger(), toolbox::toString("shipFragments:  %d", frame->fragments));

	for (size_t i = 0; i < frame->fragments; i++)
	{
		LOG4CPLUS_DEBUG(getApplicationLogger(), toolbox::toString("shipFragments: destination %d bx: %d resourceId:%d context: %x", frame->request[i].destination, frame->request[i].bx, frame->request[i].resourceId, frame->request[i].context));

		try
		{
			requestQueue_->push_back(frame->request[i]);
		}
		catch (toolbox::exception::Exception & te)
		{
			ref->release();
			LOG4CPLUS_FATAL(getApplicationLogger(), "RU input overflow (event manager ship requests). Stopping operation.");
			stopped_ = true;
			//state(Failed);
			return;
		}
	}

	//this->matchRequests();

	ref->release();
}

void gevb2g::RU::matchRequests ()
{
	//mutex_->take();

	// check if requests and data are pending
	// 

	while ((!requestQueue_->empty()) && (!inputDataFifo_->empty()))
	{
		ShipRequest request = requestQueue_->front();

		requestQueue_->pop_front();

		toolbox::mem::Reference * ref = inputDataFifo_->front();

		// prepare fragment for shipping

		FragmentDescriptor fdes;
		fdes.destination = request.destination;
		fdes.resourceId = request.resourceId;
		fdes.context = request.context;
		fdes.ref = ref;
		fdes.bx = request.bx;

		// retrieved from first block
		PI2O_DATA_READY_MESSAGE_FRAME frame = (PI2O_DATA_READY_MESSAGE_FRAME) ref->getDataLocation();
		fdes.totalSize = frame->totalLength;
		inputDataFifo_->pop_front();

		this->postFragment(fdes);
	}
	//mutex_->give();
}

//
//	Send a fragment that consists of one or more data
//	blocks. The last data block may not be completely
//	filled.
//
void gevb2g::RU::postFragment (gevb2g::RU::FragmentDescriptor &fdes) 
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "postFragment, size: " << fdes.totalSize << ", destination: " << fdes.destination << ", resource id: " << fdes.resourceId << ", context:" << fdes.context);

	toolbox::mem::Reference * currentItem = fdes.ref;
	size_t destination = fdes.destination;

	// Remember first totalLength field. Will not fail the check number 3 in the first frame
	size_t lastTotalLength = fdes.totalSize;

	U32 sumOfParts = 0;

	while (currentItem != (toolbox::mem::Reference*) 0)
	{
		PI2O_CACHE_MESSAGE_FRAME frame = (PI2O_CACHE_MESSAGE_FRAME)(currentItem->getDataLocation());
		frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
		frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
		frame->PvtMessageFrame.XFunctionCode = I2O_CACHE;
		frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
		frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
		frame->PvtMessageFrame.StdMessageFrame.TargetAddress = outputQueue_[destination].tid;
		frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;

		frame->elements = 1; // will go away, since only zero copy operation exists. No accumulation of elements.

		frame->fragmentId = this->getApplicationDescriptor()->getInstance();
		frame->context = fdes.context;
		frame->resourceId = fdes.resourceId;
		frame->bx = fdes.bx;

		U32 size = frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
		sumOfParts += frame->partLength;

		// Check 1: I2O message size consistency
		// check message size. Log error and truncate if larger then maxDataFrameSize in Bytes
		//
		if (size > maxDataFrameSize_)
		{
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "Maximum frame size exceeded: " << size << ", maximum is " << maxDataFrameSize_);

			fdes.ref->release();
			sendEmptyFrame(fdes);
			return;

		}

		// Check 2: Part size consistency check
		// Partsize + I2O header == message size
		if (frame->partLength + sizeof(I2O_CACHE_MESSAGE_FRAME) != size)
		{
			LOG4CPLUS_ERROR(getApplicationLogger(), toolbox::toString("Inconsistent part size %d, part + header size %d not equal to frame size %d", frame->partLength, sizeof(I2O_CACHE_MESSAGE_FRAME), size));

			fdes.ref->release();
			sendEmptyFrame(fdes);
			return;

		}

		// Check 3: All total sizes must be the same
		if (frame->totalLength != lastTotalLength)
		{
			LOG4CPLUS_ERROR(getApplicationLogger(), toolbox::toString("Total length (%d, %d) different in two frames", frame->totalLength, lastTotalLength));
			fdes.ref->release();
			sendEmptyFrame(fdes);
			return;
		}

		frameSendCounter_++;
		currentItem = currentItem->getNextReference();
	} //End of While

	// Check4: Data size consistency
	// Sum of all parts is equal to total size
	if (lastTotalLength != sumOfParts)
	{
		LOG4CPLUS_ERROR(getApplicationLogger(), toolbox::toString("Total length %d differs from sum of part lengths %d", lastTotalLength, sumOfParts));
		fdes.ref->release();
		sendEmptyFrame(fdes);
		return;
	}
//#ifdef LUCIANO
	toolbox::mem::Reference * reference = 0;
	// We have created an internal frame
	try
	{
		reference = this->packFrame(fdes.ref, destination);
	}
	catch (xdaq::exception::Exception & e)
	{
		LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}

	if (reference == 0)
	{
		return;
	}
//#endif

	try
	{
		// debug
		// PI2O_CACHE_MESSAGE_FRAME frameTest = (PI2O_CACHE_MESSAGE_FRAME)(fdes.ref->getDataLocation());
		//                U32 sizeTest = frameTest->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
		//std::cout << "***************************************** size is " << sizeTest << " refeence size is " << fdes.ref->getDataSize() << std::endl;
		// debug

		/*
		char c;
		std::cout << "enter to send :";
		std::cin >> c;
		*/
		//toolbox::hexdump(reference->getDataLocation(),reference->getDataSize() );

		//std::cout << "Sending " << reference->getDataSize() << " bytes" << std::endl;
		getApplicationContext()->postFrame(reference, this->getApplicationDescriptor(), outputDescriptor_[destination]);
		//getApplicationContext()->postFrame(fdes.ref, this->getApplicationDescriptor(), outputDescriptor_[destination]);
	}
	catch (xdaq::exception::Exception & e)
	{
		LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
}

void gevb2g::RU::flush () 
{
	try
	{
		for (std::vector<size_t>::size_type i = 0; i < outputDescriptor_.size(); i++)
		{
			if ((packedReferences_[i] != 0) && (packedReferences_[i]->getDataSize() > sizeof(I2O_PACKED_CACHE_MESSAGE_FRAME)))
			{
				std::cout << "Flush" << std::endl;
				getApplicationContext()->postFrame(packedReferences_[i], this->getApplicationDescriptor(), outputDescriptor_[i]);
				packedReferences_[i] = 0;
			}
		}
	}
	catch (xdaq::exception::Exception & e)
	{
		LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
}

toolbox::mem::Reference * gevb2g::RU::packFrame (toolbox::mem::Reference * ref, size_t index) 
{
	if (!doPacking_ || ref->getDataSize() >= packingSize_)
	{
		return ref;
	}
	else
	{
		toolbox::mem::Reference * pref = 0;
		// pack
		if ((packedReferences_[index] != 0) && (ref->getDataSize() + packedReferences_[index]->getDataSize() > packingSize_))
		{
			// send existing
			pref = packedReferences_[index];
			packedReferences_[index] = 0;
		}

		if (packedReferences_[index] == 0)
		{
			bool allocated = false;
			while (!allocated)
			{
				try
				{
					packedReferences_[index] = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, packingSize_);
					PI2O_PACKED_CACHE_MESSAGE_FRAME frame = (PI2O_PACKED_CACHE_MESSAGE_FRAME)(packedReferences_[index]->getDataLocation());
					frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
					frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
					frame->PvtMessageFrame.XFunctionCode = I2O_PACKED_CACHE;
					frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
					frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
					frame->PvtMessageFrame.StdMessageFrame.TargetAddress = outputQueue_[index].tid;
					frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;
					frame->frames = 0;
					frame->PvtMessageFrame.StdMessageFrame.MessageSize = sizeof(I2O_PACKED_CACHE_MESSAGE_FRAME) >> 2;
					packedReferences_[index]->setDataSize(sizeof(I2O_PACKED_CACHE_MESSAGE_FRAME));

					//toolbox::hexdump(packedReferences_[index]->getDataLocation(),packedReferences_[index]->getDataSize() );

					allocated = true;
				}
				catch (toolbox::mem::exception::Exception & e)
				{
					//XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
					std::cout << "Failed to allocate" << std::endl;
					::sched_yield();
				}
			}
		}

		this->copyFrame(packedReferences_[index], ref);

		ref->release();

		// remove for release code
		/*
		if (pref != 0)
		{
			PI2O_PACKED_CACHE_MESSAGE_FRAME frame = (PI2O_PACKED_CACHE_MESSAGE_FRAME)(pref->getDataLocation());
			std::cout << "Sending packed frame, contains " << frame->frames << " messages of total size (msg)" << (frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2) << ", total size (ref) " << pref->getDataSize() << std::endl;
		}
	*/
		return pref;
	}
}

void gevb2g::RU::copyFrame (toolbox::mem::Reference * target, toolbox::mem::Reference * source)
{
	memcpy((char*)target->getDataLocation() + target->getDataSize(), source->getDataLocation(), source->getDataSize());
	target->setDataSize(target->getDataSize() + source->getDataSize());

	PI2O_PACKED_CACHE_MESSAGE_FRAME frame = (PI2O_PACKED_CACHE_MESSAGE_FRAME)(target->getDataLocation());
	frame->frames = frame->frames + 1;
	frame->PvtMessageFrame.StdMessageFrame.MessageSize = target->getDataSize() >> 2;


}

void gevb2g::RU::sendEmptyFrame (gevb2g::RU::FragmentDescriptor fdes)
{
	toolbox::mem::Reference* ref = 0;
	try
	{
		// allocate frame
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, sizeof(I2O_CACHE_MESSAGE_FRAME));
	}
	catch (toolbox::mem::exception::Exception & e)
	{
		XCEPT_RETHROW(xdaq::exception::Exception, "failed to get frame", e);
	}

	//toolbox::mem::Reference* ref = xdaq::frameAlloc(sizeof (I2O_CACHE_MESSAGE_FRAME));
	PI2O_CACHE_MESSAGE_FRAME frame = (PI2O_CACHE_MESSAGE_FRAME)(ref->getDataLocation());
	frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
	frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
	frame->PvtMessageFrame.XFunctionCode = I2O_CACHE;
	frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID; // function group offset
	frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;  // normal message
	frame->PvtMessageFrame.StdMessageFrame.TargetAddress = outputQueue_[fdes.destination].tid;
	frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = tid_;

	frame->elements = 1; // will go away, since only zero copy operation exists. No accumulation of elements.

	frame->fragmentId = this->getApplicationDescriptor()->getInstance();
	frame->context = fdes.context;
	frame->resourceId = fdes.resourceId;
	frame->bx = fdes.bx;

	// Indicate an empty/erroneous data frame
	frame->partLength = 0;
	frame->totalLength = 0;
	frame->PvtMessageFrame.StdMessageFrame.MessageSize = sizeof(I2O_CACHE_MESSAGE_FRAME) >> 2;

	ref->setDataSize(sizeof(I2O_CACHE_MESSAGE_FRAME));

	try
	{
		//std::cout << "--------------------------------------------------" << std::endl;
		getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), outputDescriptor_[fdes.destination]);
	}
	catch (xdaq::exception::Exception & e)
	{
		LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
	}

}

xoap::MessageReference gevb2g::RU::Enable (xoap::MessageReference message) 
{
	stopped_ = false;
	this->state_ = "enabled";
	LOG4CPLUS_INFO(getApplicationLogger(), "Enable. Event building will start");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("EnableResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

xoap::MessageReference gevb2g::RU::Halt (xoap::MessageReference message) 
{
	stopped_ = true;
	this->state_ = "halted";
	// !!!! cannot clear now because of race conditions
	//this->clear();
	LOG4CPLUS_INFO(getApplicationLogger(), "Halt now.");
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName("HaltResponse", "xdaq", XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);
	return reply;

}

void gevb2g::RU::clear ()
{
	while (!inputDataFifo_->empty())
	{
		toolbox::mem::Reference * ref = inputDataFifo_->front();
		ref->release();
		inputDataFifo_->pop_front();
	}

	while (!requestQueue_->empty())
	{
		requestQueue_->pop_front();
	}

	LOG4CPLUS_INFO(getApplicationLogger(), "Pending requests and pending data flushed from queues");
}

I2O_TID gevb2g::RU::getTID ()
{
	return tid_;
}
