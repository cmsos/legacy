#ifndef _tcds_ici_BchannelRAMEntry_h_
#define _tcds_ici_BchannelRAMEntry_h_

#include <stdint.h>

namespace tcds {
  namespace ici {

    class BchannelRAMEntry
    {

    public:

      BchannelRAMEntry(uint32_t const ctrlWord, uint32_t const dataWord);

      uint32_t ctrlWord() const;
      uint32_t dataWord() const;

      bool isDoNotEmit() const;
      bool isEndOfSequence() const;

      bool isAchannelCommand() const;
      bool isBchannelCommand() const;
      bool isBchannelCommandShort() const;
      bool isBchannelCommandLong() const;
      uint8_t bcommandData() const;
      uint16_t bcommandAddress() const;
      uint8_t bcommandSubaddress() const;
      bool bcommandIsAddressExternal() const;
      bool bcommandIsReservedBit16Correct() const;

      uint16_t dynamicPostscale() const;

    private:

      static uint32_t const ctrlWordMaskBcommandType_ = 0x00000001;
      static uint32_t const ctrlWordMaskDoNotEmit_ = 0x00000002;
      static uint32_t const ctrlWordMaskEndOfSequence_ = 0x00000004;
      static uint32_t const ctrlWordMaskCommandType_ = 0x00000008;
      static uint32_t const ctrlWordMaskDynamicPostscale_ = 0xffff0000;
      static uint32_t const dataWordMaskBcommandData_ = 0x000000ff;
      static uint32_t const dataWordMaskBcommandAddress_ = 0xfffc0000;
      static uint32_t const dataWordMaskBcommandSubaddress_ = 0x0000ff00;
      static uint32_t const dataWordMaskBcommandIsAddressExternal_ = 0x00020000;
      static uint32_t const dataWordMaskBcommandIsReservedBit16Correct_ = 0x00010000;

      // This constructor is not supposed to be used.
      BchannelRAMEntry();

      uint32_t ctrlWord_;
      uint32_t dataWord_;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_BchannelRAMEntry_h_
