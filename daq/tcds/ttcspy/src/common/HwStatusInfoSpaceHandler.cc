#include "tcds/ttcspy/HwStatusInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::ttcspy::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                 tcds::utils::InfoSpaceUpdater* updater) :
  HwStatusInfoSpaceHandlerTCA(xdaqApp, "tcds-hw-status-ttcspy", updater)
{
}

tcds::ttcspy::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}
