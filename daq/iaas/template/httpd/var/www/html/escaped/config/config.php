<?php
    
    $config['dbname'] = 'myDB';
    $config['dbuser'] = 'user';
    //$config['root'] = 'http://pc-c2e11-34-01:8282/';
    $config['flash_index'] = 'flashlist';
    $config['shelf_index'] = 'shelflist';
    $config['heart_index'] = 'heartbeat';

    // all php scripts will access elasticsearch through haproxy frontend as defined below
    $config['host'] = 'pc-c2e11-34-01';
    $config['port'] = '8383';

    $config['maxsources'] = 5000;

    //cluster of elasticsearch daemons
    $config['hosts'] = array("pc-c2e11-34-01","pc-c2e11-35-01","pc-c2e11-36-01","pc-c2e11-37-01","pc-c2e11-38-01");
    $config['esport'] = '9200';
   
    $config['zone'] = 'eaas';

    $config['heartbeathost'] = 'pc-c2e11-19-01.cms';
    //$config['heartbeathost'] = 'dvsrv-c2f36-09-01.cms';

    $config['heartbeatport'] = '9948';
   //$config['autoconfsearchpath'] = 'https://gitlab.cern.ch/lorsini/cmsos/raw/master';
   //$config['autoconfsearchpath'] = 'https://gitlab.cern.ch/cmsos/iaas/raw/master';
   // $config['autoconfsearchpath'] = 'https://gitlab.cern.ch/cmsos/iaas/raw/default';
    $config['autoconfsearchpath'] = 'http://pc-c2e11-34-01.cms/cmsos/iaas';
    //$config['autoconfsearchpath'] = '\${XDAQ_ROOT}/share/\${XDAQ_ZONE}';
$config['elasticsearchurl'] = 'http://cmsos-iaas-cdaq.cms:9200';

?>
