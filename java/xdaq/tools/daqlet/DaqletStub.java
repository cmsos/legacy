//
//  DaqletStub.java
//  
//
//  Created by Luciano Orsini on Fri Sep 06 2002.
//  Copyright (c) 2002 __CERN__. All rights reserved.
//

package xdaq.tools.daqlet;

import java.applet.*;
import java.net.*;
import java.lang.*;
import java.util.*;


public class DaqletStub implements AppletStub {

    DaqletContext context_;
    Properties properties_;
    
    public DaqletStub( Properties properties, DaqletContext context)
    {
            properties_ = properties;
            context_ = context;
    }

    // Called when the applet wants to be resized.
    public void appletResize(int width, int height) 
    {
    }

    // Gets a handler to the applet's context.
    public AppletContext getAppletContext() 
    {
        return context_;
    }
   
     // Gets the base URL.
    public URL	getCodeBase()
    {
	URL url = null;
    	try {
    		url = new URL(properties_.getProperty("codebase"));
	} 
	catch (java.net.MalformedURLException e)
	{
		return url;
	}
        return url;
    }
    
    // Returns an absolute URL naming the directory of the document in which the applet is embedded.
    public URL	getDocumentBase() 
    {
    	URL url = null;
    	try {
    		url = new URL(properties_.getProperty("http://xdaq.web.cern.ch/??????"));
	} 
	catch (java.net.MalformedURLException e)
	{
		return url;
	}
        return url;
    }
   
     // Returns the value of the named parameter in the XML tag.
    public String getParameter(String name) 
    {
          return properties_.getProperty(name);
    }
    
    // Determines if the applet is active.
    public boolean isActive()
    {
        // ?????
        return true;
    } 
}
