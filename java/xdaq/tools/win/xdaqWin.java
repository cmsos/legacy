package xdaq.tools.win;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.event.*;

import javax.xml.parsers.*;


import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;


import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;


import xdaq.tools.win.xdaqWinMenuBar;
import xdaq.tools.win.xdaqDOMDocument;
import xdaq.tools.win.xdaqWinTreeSelectionListener;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.xdaqSOAPException;
import xdaq.tools.win.xdaqWinStatusBar;
import xdaq.tools.win.xdaqSounds;
import xdaq.tools.win.xdaqDOMDocumentException;

import org.apache.log4j.helpers.LogLog;

public class xdaqWin extends JFrame 
{
	xdaqWinSplitPane splitPane_;
	xdaqWinMenuBar menuBar_;
	xdaqWinTree tree_;
	xdaqWinStatusBar  statusPane_;
	JPanel emptyPanel_;
	
	public xdaqWin () {
		super ("xdaqWin");
		setSize (600, 400);
		LogLog.setQuietMode (true);
		
		menuBar_ = new xdaqWinMenuBar( this );
		this.getContentPane().add (menuBar_, BorderLayout.NORTH);
		
		splitPane_ = new xdaqWinSplitPane();
		
		this.closeConfiguration();
		
		this.getContentPane().add (splitPane_, BorderLayout.CENTER);
		
		statusPane_ = new xdaqWinStatusBar();
		
		this.getContentPane().add (statusPane_, BorderLayout.SOUTH);
		
		statusPane_.setStatus ("Open an XML configuration file to start.", "NONE");
		//statusPane_.setProgressItems (10);
	//statusPane_.advanceProgressBar (4);
		
	this.show();
	}
	
	public void closeConfiguration () {
		splitPane_.addTreePane ( new JPanel() );
		menuBar_.enable ("Close", false);
		menuBar_.enable ("Save", false);
		menuBar_.enable ("SaveAs", false);
		menuBar_.enable ("Select", false);
		menuBar_.enable ("Script...", false);
		menuBar_.enable ("User command...", false);
	}
	
	TreePath[] getPaths() {
		return tree_.getSelectionModel().getSelectionPaths();
	}
	
	// Send a SOAP command to the selected nodes and return the reply as DOM
     //
     // Pass a TreePath[] paths to this command
     //
	public Vector executeCommandOnSelection(String command, org.w3c.dom.Node userNode, TreePath[] paths) 
	throws xdaqWinException	{
		Vector replyV = new Vector(); 
		
		statusPane_.setProgressItems ( paths.length );
		statusPane_.resetProgressBar();
		
		String errorString = new String();
		
		for (int i = 0; i < paths.length; i++) {
			statusPane_.advanceProgressBar (1);
			
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			String url = null;
			xdaqSOAPMessage msg = null;
			
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();

			String targetAddress="0"; // by default executive tid

		try {
				if (node.toString().equals("Context")) {
					url = aNode.getAttribute ("url");
					msg = new xdaqSOAPMessage ( command, "0" );  // executive tid
			   msg.addAttribute ("id", aNode.getAttribute ("id"));
					if ( command.equals("Configure"))
				msg.setParameters ( aNode.domNode.getOwnerDocument().getDocumentElement() );
				} 
				else if (node.toString().equals ("Application")) {
					xdaqWinTreeAdapterNode father = ((xdaqWinTreeAdapterNode) node.getUserObject()).getParent();
					url = father.getAttribute ("url");
					targetAddress = aNode.getAttribute ("id");
					msg = new xdaqSOAPMessage ( command, targetAddress );
				}
				// add user parameters
		     if ( userNode != null ) {
					msg.setParameters ( userNode);
				}
			
				statusPane_.setStatus ("Connecting: " + url, "NONE");
				SOAPMessage reply = null;
				//try {
		     xdaqSOAPConnection con = new xdaqSOAPConnection();
				reply = con.call ( msg, url );
				if (reply != null) {
					xdaqSOAPMessage replyMsg = new xdaqSOAPMessage(reply);
					Document document = replyMsg.getDOM();
					replyV.add ( document );
					
					NodeList faultList = document.getElementsByTagNameNS ("*", "faultstring");										
					if (faultList.getLength() == 1) {
						errorString += url + " (";
						errorString += faultList.item(0).getFirstChild().getNodeValue();
						errorString += ")\n";
						statusPane_.setStatus (url+": "+faultList.item(0).getFirstChild().getNodeValue(), "ERROR");
					} else {
						statusPane_.setStatus ("Done", "NONE");
					}
				} else {
					statusPane_.setStatus ("Did not receive reply from " + url, "ERROR");
				}
			} catch (xdaqSOAPException e) {
				errorString += url + ": Cannot connect, are xdaq executives running?";
				errorString += "\n";
				statusPane_.setStatus ("Failed: sending " + command, "ERROR");
			}
		}
		if (!errorString.equals("")) {
			JOptionPane.showMessageDialog ( null, errorString, "Configure error", JOptionPane.ERROR_MESSAGE );
			throw new xdaqWinException();
		}
		statusPane_.resetProgressBar();
		return replyV;
	}
	
	public void displayConfiguration ( String url ) throws xdaqWinException	{
		statusPane_.setStatus ("Load: " + url, "NONE" );
		statusPane_.setProgressItems ( 5 );
		statusPane_.resetProgressBar();
		statusPane_.advanceProgressBar(1);
		
		try {
			Document document = xdaqDOMDocument.load (url);
			// clear any previous display if any
		statusPane_.advanceProgressBar(1);
			if (document != null) {
				tree_ = new xdaqWinTree ( document );
				statusPane_.advanceProgressBar(1);
				tree_.addTreeSelectionListener ( new xdaqWinTreeSelectionListener( this ) );
				statusPane_.advanceProgressBar(1);
				splitPane_.addTreePane ( tree_ );
				statusPane_.advanceProgressBar(1);
			
				menuBar_.enable ("Select", true);
				menuBar_.enable ("Script...", true);
				statusPane_.setStatus ("Done", "NONE");
			} else {
				statusPane_.setStatus ("Load failed.", "ERROR");
			}
		} catch (xdaqDOMDocumentException e) {
			JOptionPane.showMessageDialog ( null, e.toString(), "Configuration load", JOptionPane.ERROR_MESSAGE );
			statusPane_.setStatus ("Load failed.", "ERROR");
			throw new xdaqWinException();
		} 
		statusPane_.resetProgressBar();
	}
	
	public xdaqWinMenuBar getMenu () {
		return menuBar_;
	}
	
	public xdaqWinTree getTree() {
		return tree_;
	}
	
	public xdaqWinStatusBar getStatusBar() {
		return statusPane_;
	}
	
	 protected EventListenerList listenerList = new EventListenerList();
	 protected HashMap callbackEvents_ = new HashMap();
    
	public void displayInfo(xdaqWinTreeAdapterNode adapterNode) {
		splitPane_.showInfo(adapterNode.content());		
	}
}	
