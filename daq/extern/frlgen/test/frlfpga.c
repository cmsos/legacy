/* 
 * frlfpga.c
 *
 * example to load FPGA block#1 (in range 0-3) on geoslots 1,2,3,4 
 * frlfpga -block 1 -mask F 
 *
 * $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/test/frlfpga.c,v 1.1 2007/01/04 14:08:50 frans Exp $ 
 *
 */

/*************************************************************************
 *
 * system headers 
 *
 *************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdarg.h>
#include <sys/resource.h>
#include <sched.h>
#include <netinet/in.h>
#include <unistd.h>
#include <pwd.h>
#include <sys/types.h>


/*************************************************************************
 *
 * flags and parameters 
 *
 *************************************************************************/

//#define DEBUG

/*************************************************************************
 *
 * local headers 
 *
 *************************************************************************/

#include "frl_hw.h"
#include "frl_api.h"
#include "zfrl.h"

#include "rngs.h"
#include "rvgs.h"


/*************************************************************************
 *
 * definitions  
 *
 *************************************************************************/

#define SIGINT 2
#define SIGTERM  15

#define MAX_UNITS 16
#define MAX_DEVICES 16



/*************************************************************************
 *
 * global vars  
 *
 *************************************************************************/

/* default values for command line args */

int arg_verb = 0;  // verbose
int arg_quit = 0;

int arg_block = -1 ;  // fpga blockno (in range 0-3, inclusive)
int arg_mask = 0 ;  // mask in terms of devices participating, bit=0 corresponds to geoslot=1, etc

static int quit_flag = 0 ;

/* array to store hostname */
#define LEN_HOSTNAME 64
char hostname[LEN_HOSTNAME] ;

struct zfrl_board_info devs_binfo [MAX_DEVICES] ;

/* setup configs */

static int setup_unitno [MAX_DEVICES] ;

/* some forward function declarations */

/*************************************************************************
 *
 * 
 *
 *************************************************************************/


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
handler_sig (int s)
{
  //  int dt ;

  printf("trapped signal %d ..\n",s) ;
  fflush(stdout) ;

  switch (s) {

    //  case SIGQUIT:
  case SIGINT:
    printf("attempting orderly shutdown ..\n") ;
    fflush(stdout) ;
    quit_flag = 1 ;
    break ;
  default:
    printf("exiting .. \n") ;
    fflush(stdout) ;
    exit(1);
  }
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/

#ifdef OUT

static
void subtractTime(struct timeval *t, struct timeval *sub)
{
  signed long sec, usec;

  sec = t->tv_sec - sub->tv_sec;
  usec = t->tv_usec - sub->tv_usec;
  if (usec < 0) {
    sec--;
    usec += 1000000;
  }
  if (sec < 0) {
    t->tv_sec = 0;
    t->tv_usec = 0;
  }
  else {
    t->tv_sec = (unsigned long) sec;
    t->tv_usec = (unsigned long) usec;
  }
}

//
// TODO: change to something which actually sleeps instead of busy wait
//   

static void udelay(int n_us)
{
  struct timeval time_start;
  struct timeval time_stop;
  int nbsecs,nbusecs;
  
  nbsecs = 0;
  nbusecs = 0;
  gettimeofday (&time_start, NULL);
  
  while ( (nbsecs==0) && (nbusecs<n_us)) {
    gettimeofday (&time_stop, NULL);
    subtractTime (&time_stop, &time_start);
    nbsecs = time_stop.tv_sec;
    nbusecs = time_stop.tv_usec;
  }
  
  //  printf(" start %d %d stop %d %d \n",time_start.tv_sec,time_start.tv_usec,time_stop.tv_sec,time_stop.tv_usec) ;
}

#endif

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
die (char *fmt,...)
{
  va_list ap;
  va_start(ap, fmt);
  fprintf(stderr, "options:\n");
  fprintf(stderr, "-v \n");
  fprintf(stderr, "-quit \n");
  fprintf(stderr, "-block <fpga-block 0..3> \n");
  fprintf(stderr, "-mask <mask in hex; bit=0 is geoslot=1, etc> \n");
  //fprintf(stderr, "last words:\n");
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  va_end(ap);
  exit(1);
}

/*************************************************************************
 *
 * 
 *
 *************************************************************************/

void 
command_line (int argc, char **argv)
{
  int i;

  for (i = 1 ; i < argc ; i++) {

    if (!strcmp(argv[i], "-v")) {
      arg_verb = 1 ;
    }
    else if (!strcmp(argv[i], "-quit")) {
      arg_quit = 1 ;
    }
    else if (!strcmp(argv[i], "-mask")) {
      if (++i == argc)
	die("no value");
      if (1 != sscanf(argv[i], "%x", &arg_mask))
	die("couldn't convert %s into (hex)");
    }
    else if (!strcmp(argv[i], "-block")) {
      if (++i == argc)
    	die("no value");
      if (1 != sscanf(argv[i], "%d", &arg_block))
    	die("couldn't convert %s into (dec)");
    }
    else
      die("couldn't figure out what %s means", argv[i]);
  }
}


/*************************************************************************
 *
 * 
 *
 *************************************************************************/


int init_frl () 
{
  int num_units;
  int rt ;
  struct zfrl_board_info *p_bi ;
  //  int iunit ;
  int idev ;
  int unitno ;
  int geoslot ;
  uint32_t fpga_version_main ;
  int blockno ;

  /*
   * open the FRL devices, 
   * there is a single call for any number of NICs 
   */
  rt = frl_zfrl_open (&num_units);
  printf("frl_zfrl_open() rt=%d=x%08X, num_units=%d \n",rt,rt,num_units);
  if(num_units <= 0) {
    printf("frl_zfrl_open() rt=%d=x%08X, num_units=%d \n",rt,rt,num_units);
    printf("no FRL board found \n");
    fflush(stdout) ;
    goto error ;
  }


  // initialise unitno in table with invalid value
  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {
    setup_unitno[idev] = -1 ;
  }

  // loop over all unitno's to find their geoslot's
  for (unitno=0 ; unitno < MAX_UNITS ; unitno ++) {
    
    /* get board info and set pointers for memory mapped access */
    
    p_bi = &devs_binfo[unitno] ;
    rt = zfrl_get_board_info (unitno,p_bi);
    if (rt)
      break ;
    
    printf("unit=%d zfrl_get_board_info() rt=%d iobase pci x%08X user x%08X geoslot=%d  fpga_bridge=x%08X fpga_main=x%08X \n"
	   ,unitno,rt,p_bi->iobase_main_pci,p_bi->iobase_main_user,p_bi->frl_geoslot,p_bi->fpga_version_bridge,p_bi->fpga_version_main);
    geoslot = p_bi->frl_geoslot ;
    
    // geoslot runs 1-16, inclusive
    if (geoslot < 1 || geoslot > 16) 
      goto error ;
    idev = geoslot - 1 ;
    setup_unitno[idev] = unitno ;
    
    //  p_frlc = (frl_control_t * ) p_bi->iobase_user ;
  }
  
  if (arg_mask == 0) {
     printf("fatal mask=x%X empty \n",arg_mask) ;
      goto error ;
    }

  // check devices selected in the mask are present
  for (idev=0 ; idev < MAX_DEVICES ; idev ++) {
    if ((arg_mask & (1<<idev)) == 0) 
      continue ;   // skip those not in the mask
    unitno =setup_unitno[idev] ; 
    if (unitno == -1) {
      printf("fatal mask=x%X idev=%d not present \n",arg_mask,idev) ;
      goto error ;
    }

    // check correct fpga-main
    p_bi = &devs_binfo[unitno] ;
    fpga_version_main = p_bi->fpga_version_main ;  

    printf("idev=%d fpga_version_main=x%08X \n",idev,fpga_version_main) ;

    blockno = arg_block ;
    rt = zfrl_fpga (unitno, blockno) ;
    if (rt) {
      printf("idev=%d u=%d fatal zfrl_fpga rt=%d errno=%d \n",idev,unitno,rt,errno) ;
      goto error ;
    }

    // read board info again main_fpga might have been updated
    rt = zfrl_get_board_info (unitno,p_bi);
    if (rt) {
      printf("idev=%d u=%d fatal zfrl_get_board_info rt=%d errno=%d \n",idev,unitno,rt,errno) ;
      goto error ;
    }

    printf("idev=%d unit=%d zfrl_get_board_info() rt=%d iobase pci x%08X user x%08X geoslot=%d  fpga_bridge=x%08X fpga_main=x%08X \n"
	   ,idev,unitno,rt,p_bi->iobase_main_pci,p_bi->iobase_main_user,p_bi->frl_geoslot,p_bi->fpga_version_bridge,p_bi->fpga_version_main);
  }
  
  fflush(stdout) ;  
  
  return 0 ;
 error:
  fflush(stdout) ;  
  return -1;
}



/*************************************************************************
 *
 * 
 *
 *************************************************************************/


int main(argc,argv)
     int    argc;
     char **argv;
{
  int rt ;
  char loginname[512] ;
  char homename[512] ;
  char *p ;
  struct passwd * pwd_ent;

  gethostname (hostname,LEN_HOSTNAME);
/*  getlogin_r (loginname,512) ;*/
  if ((pwd_ent = getpwuid(geteuid()))) {
          strncpy (loginname, pwd_ent->pw_name, 512);
          printf("login name=%s \n",loginname);
  } else {
          die ("Don't know who I am...");
  }

  // find out home directory 
  // in order to put status file later
  strncpy (homename,"",512) ;
  p = getenv("HOME") ;
  if (p) {
    strncpy (homename,p,512) ;
  }
  printf("home name=%s \n",homename);    

  fflush(stdout) ;


  /* get cmd line args */

  command_line (argc, argv) ;

  /* sanity check of args */

  if (arg_mask == 0) 
    die ("bad mask ..") ; 
 if (arg_block <0 || arg_block > 3) 
    die ("bad block ..") ; 

  /* install signal handler */

  signal (SIGINT,handler_sig);
  signal (SIGTERM,handler_sig);

  if (arg_quit) {
    printf("exiting .. \n") ;
    exit (0) ;
  }

  /* initialise frl driver and unit ufrl */
 
  rt = init_frl() ;
  if (rt) {
    printf("init_frl_on_bus() error \n") ;
    exit(1);
  }

  // error:
  printf("exiting ..") ;
  printf("\n") ;
  fflush(stdout) ;

  exit (0);
}



/* eof */
