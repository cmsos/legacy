package xdaq.xdesk.xtree;

import javax.swing.*;
import javax.swing.tree.*;
import org.w3c.dom.*;
import java.util.*;

public class XTreeFilter 
{
        HashSet filter_;

	public XTreeFilter ()
	{
		filter_ = new HashSet ();
	}
	
	public void addFilter (String nodeName)
	{
		filter_.add(nodeName);
	}
        
        public boolean contains (String nodeName )
        {
		return filter_.contains(nodeName);
	}
        
}
