#ifndef _tcds_ici_ICIInfoSpaceUpdater_h_
#define _tcds_ici_ICIInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace ici {

    class TCADeviceICI;

    class ICIInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      ICIInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                          TCADeviceICI const& hw);
      virtual ~ICIInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      tcds::ici::TCADeviceICI const& getHw();

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ICIInfoSpaceUpdater_h_
