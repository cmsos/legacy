//
//  XScriptToolBar.java
//  
//
//  Created by Luciano Orsini on Sat Jun 28 2003.
//  Copyright (c) 2003 CERN. All rights reserved.
//
package xdaq.xdesk.xscript;

import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.net.*;


public class XScriptToolBar extends JToolBar
{   
    XScriptToolBarListener listener_ = null;
    
    JButton executeButton_;
    JButton stopButton_;    
    JComboBox partitions_;    
    JTextField parameters_;
    
    public XScriptToolBar ()
    {
        this.setFloatable(false);
        
        //first button
	URL url = null;
	Image img = null;

	url = this.getClass().getResource("/xdaq/xdesk/xscript/icons/Play24.gif");
        executeButton_ = new JButton(new ImageIcon(url));
        executeButton_.setEnabled(false);
        executeButton_.setToolTipText("Run script");
        executeButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("execute");
            }
            });
        this.add(executeButton_);
        
	
	url = this.getClass().getResource("/xdaq/xdesk/xscript/icons/Stop24.gif");
        stopButton_ = new JButton(new ImageIcon(url));
        stopButton_.setEnabled(false);
        stopButton_.setToolTipText("Interrupt currently running script");
        stopButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("stop");
            }
            });
        this.add(stopButton_);

	// Command combo box
	partitions_ = new JComboBox();
	partitions_.setMaximumSize   ( new Dimension (200,25 ) );
	partitions_.setPreferredSize ( new Dimension (200, 25) );
	this.add (partitions_);
	
	parameters_ = new JTextField();
	this.add (parameters_);
	
    }  
    

    protected void dispatch(String name) 
    {
            if ( listener_ != null )
                listener_.buttonPressed(name);
    }
    
    void setToolBarListener(XScriptToolBarListener l ) 
    {
            listener_ = l;
    }

    // Toggle all buttons enabled/stop button disabled and vice versa
    public void stopButtonEnable (boolean enabled)
    {
     	stopButton_.setEnabled(enabled);
    }


    public void setEnabled(boolean enabled)
    {
	executeButtonEnable(enabled);	
    }    
    
    public void executeButtonEnable (boolean enabled)
    {    	
	  executeButton_.setEnabled(enabled);	
    }
    
    public boolean hasPartitions()
    {
    	return partitions_.getItemCount() > 0;
    }
   

	public String getSelectedPartition()
	{
		return (String) partitions_.getSelectedItem();
	}

	public void setPartitionsModel (DefaultComboBoxModel m)
	{
		partitions_.setModel (m);
	}
	
	
    
}
