#ifndef _tcds_cpm_CPMRatesInfoSpaceUpdater_h_
#define _tcds_cpm_CPMRatesInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cpm {

    class BRILDAQLoop;
    class TCADeviceCPMT1;

    class CPMRatesInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      CPMRatesInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::cpm::TCADeviceCPMT1 const& hwT1,
                               tcds::cpm::BRILDAQLoop const& brildaqLoop);
      virtual ~CPMRatesInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      TCADeviceCPMT1 const& getHwT1() const;

      TCADeviceCPMT1 const& hwT1_;
      tcds::cpm::BRILDAQLoop const& brildaqLoop_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_CPMRatesInfoSpaceUpdater_h_
