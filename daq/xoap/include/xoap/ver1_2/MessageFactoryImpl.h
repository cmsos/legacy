// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xoap_ver1_2_MessageFactoryImpl_h_
#define _xoap_ver1_2_MessageFactoryImpl_h_

#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"
#include "xoap/MessageFactory.h"

namespace xoap
{

	namespace ver1_2
	{

	class MessageFactoryImpl: public xoap::MessageFactory
        {
                public:

                static MessageFactory* getInstance();

                static void destroyInstance();


		MessageReference createMessage() ;

		MessageReference createMessage(char* buf, int size) 
			;

		MessageReference createMessage(DOMNode* node) 
			;
			
		MessageReference createMessage(const std::string& filename) 
			;
	
		MessageReference createMessage(MessageReference msg)
			;


		std::string getProtocolVersion();
                std::string getMediaType();
                std::string getEnvelopePrefix();
                std::string getURINSEnvelope();
                std::string getURINSEncoding();

		private:

		static MessageFactory * instance;


        };

	}

	
}

#endif
