#ifndef _tcds_freqmon_FreqMonInfoSpaceUpdater_h_
#define _tcds_freqmon_FreqMonInfoSpaceUpdater_h_

#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace freqmon {

    class FreqMonLoop;

    class FreqMonInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      FreqMonInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::freqmon::FreqMonLoop const& freqMonLoop);
      virtual ~FreqMonInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::freqmon::FreqMonLoop const& freqMonLoop_;

      // The 'forced update' prescale: once per minute if the measured
      // value does not change.
      static int const kUpdatePrescale_;
      // The minimal change required to trigger a normal update (Hz).
      static double const kUpdateDeadBand_;

      double freqValPrev_;
      int updatePrescaleCounter_;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMonInfoSpaceUpdater_h_
