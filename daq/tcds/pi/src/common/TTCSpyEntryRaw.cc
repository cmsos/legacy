#include "tcds/pi/TTCSpyEntryRaw.h"

#include <cassert>
#include <iomanip>
#include <sstream>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/pi/Utils.h"

tcds::pi::TTCSpyEntryRaw::TTCSpyEntryRaw(std::vector<uint32_t> const dataIn)
{
  if (dataIn.size() != kNumWordsPerLogEntry)
    {
      XCEPT_RAISE(tcds::exception::SoftwareProblem,
                  toolbox::toString("Got more or less data than expected "
                                    "(%d words instead of %d) "
                                    "for a single TTC spy log entry.",
                                    dataIn.size(), kNumWordsPerLogEntry));
    }
  rawData_ = dataIn;

  // NOTE: The order of words is 'inverted.' See the documentation of
  // the spy log entry format in TTCSpyEntryRaw.h.
  for (std::vector<uint32_t>::const_reverse_iterator it = dataIn.rbegin();
       it != dataIn.rend();
       ++it)
    {
      std::vector<bool> tmp = intToVecBool<uint32_t>(*it);
      data_.insert(data_.begin(), tmp.begin(), tmp.end());
    }
}

std::string
tcds::pi::TTCSpyEntryRaw::rawDataAsString() const
{
  // NOTE: The order of words is 'inverted.' See the documentation of
  // the spy log entry format in TTCSpyEntryRaw.h. The data used
  // internally by this class (data_) has been put in the right order,
  // but the raw data has not.
  std::stringstream res;
  res << std::hex
      << std::setfill('0');
  for (std::vector<uint32_t>::const_reverse_iterator it = rawData_.rbegin();
       it != rawData_.rend();
       ++it)
    {
      res << "0x"
          << std::setw(8)
          << *it;
      if (it != (rawData_.rend() - 1))
        {
          res << " ";
        }
    }
  return res.str();
}

std::string
tcds::pi::TTCSpyEntryRaw::type() const
{
  std::stringstream res;
  if (isL1A())
    {
      res << "L1A";
    }
  else if (isShortCommand())
    {
      res << "Broadcast command";
    }
  else if (isLongCommand())
    {
      res << "Addressed command";
    }
  else
    {
      res << "Unknown";
    }
  return res.str();
}

uint64_t
tcds::pi::TTCSpyEntryRaw::orbitNumber() const
{
  std::vector<bool> const tmpVec = getBits(kBitPosOrbitLo, kBitPosOrbitHi);
  uint64_t const res = vecBoolToUInt<uint64_t>(tmpVec);
  return res;
}

uint16_t
tcds::pi::TTCSpyEntryRaw::bxNumber() const
{
  std::vector<bool> const tmpVec = getBits(kBitPosBXNumLo, kBitPosBXNumHi);
  uint16_t const res = vecBoolToUInt<uint16_t>(tmpVec);
  return res;
}

bool
tcds::pi::TTCSpyEntryRaw::isL1A() const
{
  bool const res = isBitSet(kBitPosL1Accept);
  return res;
}

bool
tcds::pi::TTCSpyEntryRaw::isBCntRes() const
{
  bool const res = isBitSet(kBitPosShortBCntRes);
  return res;
}

bool
tcds::pi::TTCSpyEntryRaw::isEvCntRes() const
{
  bool const res = isBitSet(kBitPosShortEvCntRes);
  return res;
}

bool
tcds::pi::TTCSpyEntryRaw::isLongCommand() const
{
  // Look for either a non-zero field in the logging related to short
  // commands, or a short command strobe (which is only set when
  // catching all short B-commands).
  bool res = false;
  if ((longData() != 0) ||
      (longAddress() != 0) ||
      (longSubAddress() != 0) ||
      hasLongCommandStrobeSet())
    {
      res = true;
    }
  return res;
}

bool
tcds::pi::TTCSpyEntryRaw::isShortCommand() const
{
  // Look for either a non-zero field in the logging related to long
  // commands, or a long command strobe (which is only set when
  // catching all long B-commands).
  bool res = false;
  if ((shortData() != 0) ||
      isBCntRes() ||
      isEvCntRes() ||
      hasShortCommandStrobeSet())
    {
      res = true;
    }
  return res;
}

bool
tcds::pi::TTCSpyEntryRaw::isLongExternal() const
{
  return isBitSet(kBitPosLongExternalFlag);
}

bool
tcds::pi::TTCSpyEntryRaw::hasSngErrStrSet() const
{
  return isBitSet(kBitPosSingleBitError);
}

bool
tcds::pi::TTCSpyEntryRaw::hasDblErrStrSet() const
{
  return isBitSet(kBitPosDoubleBitError);
}

bool
tcds::pi::TTCSpyEntryRaw::hasCommErrorBitSet() const
{
  return isBitSet(kBitPosCommError);
}

bool
tcds::pi::TTCSpyEntryRaw::hasLongCommandStrobeSet() const
{
  return isBitSet(kBitPosLongCommandStrobe);
}

bool
tcds::pi::TTCSpyEntryRaw::hasShortCommandStrobeSet() const
{
  return isBitSet(kBitPosShortCommandStrobe);
}

uint8_t
tcds::pi::TTCSpyEntryRaw::longData() const
{
  std::vector<bool> tmpVec = getBits(kBitPosLongDataLo,
                                     kBitPosLongDataHi);
  uint8_t res = vecBoolToUInt<uint8_t>(tmpVec);
  return res;
}

uint16_t
tcds::pi::TTCSpyEntryRaw::longAddress() const
{
  std::vector<bool> tmpVec = getBits(kBitPosLongAddressLo,
                                     kBitPosLongAddressHi);
  uint16_t res = vecBoolToUInt<uint16_t>(tmpVec);
  return res;
}

uint8_t
tcds::pi::TTCSpyEntryRaw::longSubAddress() const
{
  std::vector<bool> tmpVec = getBits(kBitPosLongSubAddressLo,
                                     kBitPosLongSubAddressHi);
  uint8_t res = vecBoolToUInt<uint8_t>(tmpVec);
  return res;
}

uint8_t
tcds::pi::TTCSpyEntryRaw::shortData() const
{
  std::vector<bool> tmpVec = getBits(kBitPosShortBCntRes,
                                     kBitPosShortUserDataHi);
  uint8_t res = vecBoolToUInt<uint8_t>(tmpVec);
  return res;
}

bool
tcds::pi::TTCSpyEntryRaw::isBitSet(size_t const bitNum) const
{
  std::vector<bool> tmpVec = getBit(bitNum);
  uint32_t tmp = vecBoolToUInt<uint32_t>(tmpVec);
  // DEBUG DEBUG DEBUG
  assert(tmpVec.size() == 1);
  // DEBUG DEBUG DEBUG end
  return (tmp != 0);
}

std::vector<bool>
tcds::pi::TTCSpyEntryRaw::getBit(size_t const bitNum) const
{
  return getBits(bitNum, bitNum);
}

std::vector<bool>
tcds::pi::TTCSpyEntryRaw::getBits(size_t const bitNumLo,
                                  size_t const bitNumHi) const
{
  // DEBUG DEBUG DEBUG
  assert (bitNumHi >= bitNumLo);
  // DEBUG DEBUG DEBUG end
  size_t tmpLo = bitNumLo;
  size_t tmpHi = bitNumHi;
  if (tmpHi == 0)
    {
      tmpHi = tmpLo;
    }
  std::vector<bool>::const_iterator bitLo = data_.begin() + tmpLo;
  std::vector<bool>::const_iterator bitHi = data_.begin() + tmpHi + 1;
  std::vector<bool> res(bitLo, bitHi);
  return res;
}
