package xdaq.xdesk.jfinder;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import javax.swing.*;

import org.apache.xpath.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class FinderGUIConfiguration {
	//---------------------------Variables Definition-------------------------------
	
	private Hashtable defaultIcons;
	private Hashtable filenameIcons;
	private Hashtable extensionIcons;
	
	private Document document;
	private Class currentClass;
	private DocumentBuilderFactory documentBuilderFactory;
	private DocumentBuilder documentBuilder;
	
	//------------------Constructors------------------------------------------------
	
	/**An empty constructor*/
	public FinderGUIConfiguration() {
		defaultIcons = new Hashtable();
		filenameIcons = new Hashtable();
		extensionIcons = new Hashtable();
		document = null;
		currentClass = this.getClass();
		initializeTables();
	}
	
	/**Constructor by DOM Document*/
	public FinderGUIConfiguration(Document doc) throws IOException,
		ParserConfigurationException, TransformerConfigurationException,
		SAXException, TransformerException {
			this();
			this.setDocument(doc);
		}
	
	/**Constructor by XML file*/
	public FinderGUIConfiguration(File file) {
		this();
		Document doc_ = null;
		if (file.isFile()) {
			try {
				javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.
				DocumentBuilderFactory.newInstance();
				javax.xml.parsers.DocumentBuilder builder = factory.newDocumentBuilder();
				doc_ = builder.parse(file);
				this.setDocument(doc_);
			}
			catch (Exception ex) {
				System.out.println("FinderGUIConfiguration - An exception occured : " +
								   ex);
			}
		}
		else {
			System.out.println("FinderGUIConfiguration - Bad file or file doesn't exist. Loading default configuration.");
		}
	}
	
	//------------CREATE DEFAULT VALUES TABLE---------------------------------------
	//**********************************************************************
	//All the icons used as default icons are precompiled to the
	//jexplorerconfig.jar/icons
	//**********************************************************************
    /**Sets default values at configuration tables*/
    private void initializeTables() {
		//initialize default icons
		String resoursePath = "/xdaq/xdesk/jfinder/icons/";
		defaultIcons.put("default_file", resoursePath + "file.png");
		defaultIcons.put("default_folder", resoursePath + "folder.png");
		defaultIcons.put("default_up", resoursePath + "file.png");
    }
	
	//*****************************************************************************
	//                 CHANGE A DEFAULT VALUES AT TABLE
	//            TO THE VALUES FROM CONFIGURATION DOMDocument
	/**Sets values at configuration tables according to DOM document*/
	private void setDocument(Document doc) throws IOException,
		ParserConfigurationException, TransformerConfigurationException,
		SAXException, TransformerException {
			document = doc;
			document.normalize();
			
			String xpath = "//ICONS/DEFAULT/item";
				NodeList iconNodes = XPathAPI.selectNodeList(document, xpath);
				if (iconNodes != null) {
					for (int i = 0; i < iconNodes.getLength(); i++) {
						
						defaultIcons.remove(iconNodes.item(i).getAttributes().item(0).
											getNodeValue());
						defaultIcons.put(iconNodes.item(i).getAttributes().item(0).
										 getNodeValue().toString().toLowerCase(),
										 iconNodes.item(i).getAttributes().item(1).
										 getNodeValue().toString());
					}
				}
				
				xpath = "//ICONS/FILENAME/item";
					iconNodes = XPathAPI.selectNodeList(doc, xpath);
					
					if (iconNodes != null) {
						for (int i = 0; i < iconNodes.getLength(); i++) {
							
							filenameIcons.remove(iconNodes.item(i).getAttributes().item(0).
												 getNodeValue());
							filenameIcons.put(iconNodes.item(i).getAttributes().item(0).
											  getNodeValue().toString().toLowerCase(),
											  iconNodes.item(i).getAttributes().item(1).
											  getNodeValue().toString());
						}
					}
					
					xpath = "//ICONS/EXTENSION/item";
						
						iconNodes = XPathAPI.selectNodeList(doc, xpath);
						if (iconNodes != null) {
							for (int i = 0; i < iconNodes.getLength(); i++) {
								extensionIcons.remove(iconNodes.item(i).getAttributes().item(0).
													  getNodeValue());
								extensionIcons.put(iconNodes.item(i).getAttributes().item(0).
												   getNodeValue().toString().toLowerCase(),
												   iconNodes.item(i).getAttributes().item(1).
												   getNodeValue().toString());
							}
						}
		}
	
	//*****************************************************************************
	/**Returns ImageIcon according to name of file and type of file (file or folder)*/
	public ImageIcon getIcon(String fileName, boolean isFolder) {
		String name = fileName;
		String path = "";
		URL url = null;
		ImageIcon icon = null;
		if (isFolder) {
			path = getFolderIconPath(fileName);
		}
		else {
			path = getFileIconPath(fileName);
		}
		
		try {
			url = new URL(path);
		}
		catch (MalformedURLException ex) {
			url = currentClass.getResource(path);
		}
		icon = new ImageIcon(url);
		return icon;
	}
	
	//******************************************************************************
	//Returns path for an icon
	private String getFolderIconPath(String folderName) {
		String path = "";
		if (filenameIcons.containsKey(folderName.toLowerCase())) {
			path = filenameIcons.get(folderName.toLowerCase()).toString();
		}
		else {
			path = (folderName.toLowerCase().compareTo("up") == 0) ?
			defaultIcons.get("default_up").toString() : //if true
			defaultIcons.get("default_folder").toString(); //else
		}
		return path;
	}
	
	//******************************************************************************
	private String getFileIconPath(String fileName) {
		String name = "";
		String extension = "";
		String path = "";
		int index = fileName.lastIndexOf(".");
		if (index == -1) {
			name = fileName;
		}
		else {
			name = fileName.substring(0, index);
			extension = fileName.substring(index + 1, fileName.length());
		}
		path = defaultIcons.get("default_file").toString();
		if (extension.length() > 0) {
			if (extensionIcons.containsKey(extension.toLowerCase())) {
				path = extensionIcons.get(extension.toLowerCase()).toString();
			}
		}
		if (filenameIcons.containsKey(name.toLowerCase())) {
			path = (filenameIcons.get(name.toLowerCase())).toString();
		}
		return path;
	}
	
	//******************************************************************************
	/**Creates and returns a new DOM document by configuration tables content*/
	public Document getDocument() throws TransformerException,
		ParserConfigurationException {
			documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document doc = documentBuilder.newDocument();
			Element root = doc.createElement("CONFIGURATION");
			Element mainelement = doc.createElement("ICONS");
			mainelement.appendChild(createDefaultIconsElement(doc));
			mainelement.appendChild(createFileNameIconsElement(doc));
			mainelement.appendChild(createExtensionIconsElement(doc));
			root.appendChild(mainelement);
			doc.appendChild(root);
			doc.normalize();
			return doc;
		}
	
	//******************************************************************************
	/**Creates DEFAULT tag and attachs an items according to defaultIcons Hashtable content*/
	private Element createDefaultIconsElement(Document doc) throws
		TransformerException {
			Element defaultIconsElement = doc.createElement("DEFAULT");
			String name;
			String key;
			Vector v = new Vector(defaultIcons.keySet());
			Collections.sort(v);
			defaultIconsElement.appendChild(doc.createComment(
															  "Please do not change value of the name attribute"));
			for (Enumeration e = v.elements(); e.hasMoreElements(); ) {
				name = e.nextElement().toString();
				key = defaultIcons.get(name).toString();
				Element defaultIconNode = doc.createElement("item");
				defaultIconNode.setAttribute("name", name);
				defaultIconNode.setAttribute("url", key);
				defaultIconsElement.appendChild(defaultIconNode);
			}
			return defaultIconsElement;
		}
	
	//******************************************************************************
	/**Creates a FILENAME tag and attachs childrens
		* from filenameIcons configuration table
		*/
	private Element createFileNameIconsElement(Document doc) {
		Element fileNameIconsElement = doc.createElement("FILENAME");
		Vector v = new Vector(filenameIcons.keySet());
		Collections.sort(v);
		for (Enumeration e = v.elements(); e.hasMoreElements(); ) {
			Element node = doc.createElement("item");
			String name = e.nextElement().toString();
			String url = filenameIcons.get(name).toString();
			node.setAttribute("name", name);
			node.setAttribute("url", url);
			fileNameIconsElement.appendChild(node);
		}
		return fileNameIconsElement;
	}
	
	//******************************************************************************
	/**Creates an EXTENSION tag and attachs values from extensionIcons configuration table*/
	private Element createExtensionIconsElement(Document doc) {
		Element extensionIconsElement = doc.createElement("EXTENSION");
		Vector v = new Vector(extensionIcons.keySet());
		Collections.sort(v);
		for (Enumeration e = v.elements(); e.hasMoreElements(); ) {
			Element node = doc.createElement("item");
			String name = e.nextElement().toString();
			String url = extensionIcons.get(name).toString();
			node.setAttribute("name", name);
			node.setAttribute("url", url);
			extensionIconsElement.appendChild(node);
		}
		return extensionIconsElement;
	}
	
	//******************************************************************************
	/**Save DOM document*/
	public void save(File file) {
		try {
			Document doc = this.getDocument();
			StreamResult result = new StreamResult(file);
			DOMSource sourse = new DOMSource(this.getDocument());
			Transformer transformer = TransformerFactory.newInstance().
				newTransformer();
			transformer.setOutputProperty("indent", "yes");
			transformer.transform(sourse, result);
		}
		catch (Exception e) {
			System.out.println("Saving GUI configuration error " + e.getMessage());
		}
	}
	
}
