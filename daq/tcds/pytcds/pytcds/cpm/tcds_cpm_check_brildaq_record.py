#!/usr/bin/env python

###############################################################################
## Little helper to check the book-keeping numbers in the CPM BRILDAQ
## record.
###############################################################################

from pytcds.utils.tcds_constants import FIRST_BX
from pytcds.utils.tcds_constants import LAST_BX
from pytcds.utils.tcds_constants import NUM_BX_PER_ORBIT
from pytcds.utils.tcds_utils_hw_connect import get_amc13t1_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Checker(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_amc13t1_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(network_address))

        #----------

        # Read the active BX mask.
        bx_mask = []
        for bx in xrange(FIRST_BX, LAST_BX + 1):
            reg_name = "cpmt1.ipm.bunch_mask.bx{0:d}".format(bx)
            tmp = cpm.read(reg_name)
            bx_mask.append(tmp)

        active_bxs = [i for (i, j) in enumerate(bx_mask) if (j & 0x4)]

        num_active_bxs = len(active_bxs)
        print "Found {0:d} beam-active BXs in the CPM bunch mask".format(num_active_bxs)

        #----------

        # Read the number of orbits that should be in each lumi
        # nibble.
        num_orbits_per_nibble = cpm.read("cpmt1.brildaq.gated.num_orbits")
        print "According to the CPM BRILDAQ record," \
            "there are {0:d} orbits in each nibble".format(num_orbits_per_nibble)

        num_bx_total_expected = NUM_BX_PER_ORBIT * num_orbits_per_nibble
        num_bx_beamactive_expected = num_active_bxs * num_orbits_per_nibble

        # Read the total number of BXs in the lumi nibble.
        num_bx_total = cpm.read("cpmt1.brildaq.plain.deadtime_bx_count_total")
        print "The total BX count in the lumi nibble," \
            "according to the CPM BRILDAQ packet, is {0:d}".format(num_bx_total)
        if (num_bx_total != num_bx_total_expected):
            print "  !!! --> expected {0:d} BXs but found {1:d} !!!".format(num_bx_total_expected, num_bx_total)

        num_bx_beamactive = cpm.read("cpmt1.brildaq.gated.deadtime_bx_count_total")
        print "The beam-active BX count in the lumi nibble," \
            "according to the CPM BRILDAQ packet, is {0:d}".format(num_bx_beamactive)
        if (num_bx_beamactive != num_bx_beamactive_expected):
            print "  !!! --> expected {0:d} BXs but found {1:d} !!!".format(num_bx_beamactive_expected, num_bx_beamactive)

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    Checker().run()
    print "Done"

###############################################################################
