<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xj="http://xdaq.web.cern.ch/xdaq/xsd/2017/XMLJS10"  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application class="executive::Application" instance="0" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>
	
	<!-- xp:Application class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local" service="pthttp">
		 <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
		 	<documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
            <aliasName xsi:type="xsd:string">tmp</aliasName>
            <aliasPath xsi:type="xsd:string">/tmp</aliasPath>
            <expiresByType xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[6]">
			    <item xsi:type="soapenc:Struct" soapenc:position="[0]">
			        <name xsi:type="xsd:string">image/png</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[1]">
			        <name xsi:type="xsd:string">image/jpg</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[2]">
			        <name xsi:type="xsd:string">image/gif</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[3]">
			        <name xsi:type="xsd:string">application/x-shockwave-flash</name>
			        <value xsi:type="xsd:string">PT120H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[4]">
			        <name xsi:type="xsd:string">application/font-woff</name>
			        <value xsi:type="xsd:string">PT8600H</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[5]">
			        <name xsi:type="xsd:string">text/css</name>
			        <value xsi:type="xsd:string">PT4300H</value>
			    </item>
		   </expiresByType>
		   <httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[3]">
		   		<item xsi:type="soapenc:Struct" soapenc:position="[0]">
		        	<name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
		        	<value xsi:type="xsd:string">*</value>
		    	</item>
		    	<item xsi:type="soapenc:Struct" soapenc:position="[1]">
			        <name xsi:type="xsd:string">Access-Control-Allow-Methods</name>
			        <value xsi:type="xsd:string">POST, GET, OPTIONS</value>
			    </item>
			    <item xsi:type="soapenc:Struct" soapenc:position="[2]">
			        <name xsi:type="xsd:string">Access-Control-Allow-Headers</name>
			        <value xsi:type="xsd:string">x-requested-with</value>
		    	</item>
		   </httpHeaderFields>
    	</properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module -->
	<!-- HyperDAQ -->
	<xp:Application class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	

	<xp:Application class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" service="ptfifo"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>

	
	<!-- XRelay -->
	<!-- xp:Application class="xrelay::Application" id="4"  service="xrelay" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libxrelay.so</xp:Module -->
	
	
	<!-- xj:Endpoint protocol="http" service="cgi" hostname="xdaqdev1.cms" port="1911"/>
	<xj:Endpoint protocol="http" service="restful" hostname="xdaqdev1.cms" port="1914"/ -->
	
	
	<!-- xj:Application class="Server" id="101" group="profile" service="server" redirect="hyperdaq">
	 <properties xmlns="urn:js-application:Server" xsi:type="soapenc:Struct">
            	<aliasName xsi:type="xsd:string">api</aliasName>
            	<aliasPath xsi:type="xsd:string">/api</aliasPath>
      </properties>
	</xj:Application>
	
	
	<xj:Module>/tmp/devel/trunk/daq/alchemy/server.js</xj:Module -->
	
	   <xj:Application class="Interact" id="103" group="profile" service="interact" redirect="hyperdaq">
         <properties xmlns="urn:js-application:Interact" xsi:type="soapenc:Struct">
      	</properties>
		</xj:Application>
		
        <xj:Module>/tmp/devel/trunk/daq/interact/interact.js</xj:Module>
	
	
	<!-- xj:Application class="Sorcerer" id="104" group="profile" service="sorcerer">
		<properties xmlns="urn:js-application:Sorcerer" xsi:type="soapenc:Struct">
			<route xsi:type="xsd:string">user</route>
            <info xsi:type="xsd:string">test application</info>
        </properties>
	</xj:Application>

	
	<xj:Module>/tmp/devel/trunk/daq/sorcerer/sorcerer.js</xj:Module -->
	
	<xp:Application heartbeat="true" class="b2in::eventing::Application" id="42" network="slimnet" group="sentinel,xmas" service="b2in-eventing" publish="true" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:b2in::eventing::Application" xsi:type="soapenc:Struct">
  	</properties>
 	</xp:Application>
 <xp:Application heartbeat="true" class="xmas::slash2g::Application" id="16" network="slimnet" group="xmas,collection" service="xmaslas2g" publish="true" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:xmas::las2g::Application" xsi:type="soapenc:Struct">
   <subscribeGroup xsi:type="xsd:string">xmas</subscribeGroup>
   <!-- Load only flashlists that the slash for this domain should see, e.g. las-pool1 -->
   <settings xsi:type="xsd:string">${XDAQ_ROOT}/share/${XDAQ_ZONE}/sensor/xmas-slash2g-1.settings</settings>
   <!-- scan for eventings every 10 seconds -->
   <scanPeriod xsi:type="xsd:string">PT30S</scanPeriod>
   <!-- subscription expiration times out after 30 seconds without renewal -->
   <subscribeExpiration xsi:type="xsd:string">PT60S</subscribeExpiration>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libjansson.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasslash2g.so</xp:Module>
 
 	<xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 	<xp:Module>${XDAQ_ROOT}/lib/libb2ineventing.so</xp:Module>

 	<!-- xj:Application class="RESTful" id="102" group="profile" service="restful">
         <properties xmlns="urn:js-application:RESTful" xsi:type="soapenc:Struct">
                <aliasName xsi:type="xsd:string">api</aliasName>
                <aliasPath xsi:type="xsd:string">/api</aliasPath>
      	</properties>
	</xj:Application>
        <xj:Module>/tmp/devel/trunk/daq/restful/restful.js</xj:Module -->
        
        
     


	
	
</xp:Profile>
