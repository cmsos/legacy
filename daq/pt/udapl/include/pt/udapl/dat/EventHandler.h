// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_EventHandler_h_
#define _pt_udapl_dat_EventHandler_h_

#include <iostream>
#include <string>

#include "pt/udapl/dat/Utils.h"

namespace pt
{
	namespace udapl
	{
		namespace dat
		{
			class EventHandler
			{
				public:
					virtual ~EventHandler ()
					{
					}

					virtual void handleEvent (DAT_EVENT & e) = 0;

			};
		}
	}
}

#endif
