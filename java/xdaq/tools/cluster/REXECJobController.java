package xdaq.tools.cluster;

import xdaq.tools.cluster.JobController;
import java.io.*;
import com.oroinc.net.bsd.*;
import java.util.*;

class REXECJobController implements JobController
{
	public void exec (	String host, 
				String architecture, 
				Vector commands, 
				String username, 
				String password) 
				throws JobControllerException
	{
		client_ = new RExecClient();
		try {
			client_.connect ( host );
		} catch (IOException e) {
			throw new JobControllerException (e.getMessage());
		}
		try {
			for (int i = 0; i < commands.size(); i++) {
				String cmd = (String) commands.elementAt(i);
				client_.rexec (username, password, cmd);
			}
		} catch (IOException e) {
			String errorString = e.getMessage();
			try {
				client_.disconnect();
			} catch (IOException f) {
				errorString += ('\n'+f.getMessage());
			}
			throw new JobControllerException (errorString);
		}
		
		
	}
			
	public void destroy () throws JobControllerException
	{
		try {
			client_.disconnect();
		} catch (IOException e) {
			throw new JobControllerException ("Rexec disconnect error: "+e.getMessage());
		}
	}
	
	public InputStream getInputStream() { return client_.getInputStream();}
	public OutputStream getOutputStream() { return client_.getOutputStream();}
	
	private RExecClient client_;
}
