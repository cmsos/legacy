###############################################################################
## Utilities related to the FC7 PhaseMon board.
###############################################################################

from pytcds.phasemon.tcds_phasemon_constants import ADDRESS_TABLE_FILE_NAME_PHASEMON
from pytcds.utils.tcds_constants import BOARD_TYPE_FC7
from pytcds.utils.tcds_constants import BOARD_TYPE_PHASEMON
from pytcds.utils.tcds_fc7 import TCDSFC7
from pytcds.utils.tcds_utils_uhal import get_hw_tca

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PhaseMon(TCDSFC7):

    pass

    # End of class PhaseMon.

###############################################################################

def get_phasemon_hw(address_or_name,
                    controlhub_address=None,
                    verbose=False,
                    hw_cls=PhaseMon):

    phasemon = get_hw_tca(BOARD_TYPE_FC7,
                          BOARD_TYPE_PHASEMON,
                          ADDRESS_TABLE_FILE_NAME_PHASEMON,
                          address_or_name,
                          controlhub_address,
                          verbose,
                          hw_cls)

    return phasemon

###############################################################################
