package xdaq.tools.soaputils;

import java.io.*;
import java.util.*;

/*
   read from a socket blocks until it timeouts. Therefore this wrapper
   uses the content length to determine the end of the reading
*/
public class httpInputStream  extends InputStream 
{
    BufferedInputStream iR;
    InputStreamReader iSr;
    int bytesRead, contentLength;
    byte[] charBuff;
    HashMap headerLines;
    String method, url, httpVersion;
    boolean debug;

    public httpInputStream(InputStream iR_, boolean debug_)
	{
	    iR= new BufferedInputStream(iR_);
	    charBuff = new byte[1000];
	    contentLength=-1;
	    bytesRead = 0;
	    debug = debug_;
	}

    public void readHeader() throws IOException
	{
	    headerLines = new HashMap();
	    boolean header = true;
	    int colonPos, i;
	    String keyword, lineContent;
	    while (header)
	    {
		colonPos = -1;
		for (i=0 ; i<1000 ; i++)
		{
		    charBuff[i] = (byte) iR.read();
		    if (charBuff[i] == '\r') charBuff[i] = (byte) iR.read(); // ignore /r 
		    if (charBuff[i] == '\n')
		    {
			if (i == 0) header = false;
			break;
		    }
		    else if (charBuff[i] == ':') colonPos = i;
		}
		if (debug) System.out.println("Line: " +new String(charBuff,0,i));
		if (colonPos > 0) 
		{
		    keyword = new String(charBuff,0,colonPos);
		    lineContent = new String(charBuff,colonPos+1,i-colonPos-1);
		    headerLines.put(keyword,lineContent);
		    if (keyword.toLowerCase().equals("content-length")) contentLength = Integer.parseInt(lineContent.trim());		    
		    if (debug) System.out.println("Read line: " +keyword  + ": " + lineContent);
		}
		else 
		{
		    StringTokenizer tokenizer = new StringTokenizer(new String(charBuff,0,i));
		    if (tokenizer.countTokens() >= 2) {
			// Get the method (command), url, and HTTP version.
			method = tokenizer.nextToken(); // GET, PUT, POST, ...
			if (tokenizer.countTokens() > 2) url = tokenizer.nextToken();
			httpVersion = tokenizer.nextToken();
			if (debug) System.out.println("RequestThread: " + method + " " + url + " " + httpVersion);
		    }
		    
		}
	    }
	}

    public HashMap getHeaderLines()
	{
	    return (headerLines);
	}

    public String getMethod()
	{
	    return (method);
	}

    public String getURL()
	{
	    return (url);
	}

    public String getHttpVersion()
	{
	    return (httpVersion);
	}

    public int read() throws IOException 
	{
	    bytesRead++;
	    return(iR.read());
	}

    public int read(byte[] b) throws IOException 
	{
	    if (contentLength-bytesRead == 0) return (0);
	    int bread = iR.read(b);
	    bytesRead += bread;
	    return(bread);
	}

    public int read(byte[] b, int off, int len) throws IOException
	{
	    int bread = 0;
	    boolean doRun = true;
	    while (doRun)
	    {
		if (debug) 
		{
		    System.out.println ("Asking for "+Integer.toString(len)+" bytes of available "+Integer.toString(iR.available()));
		    System.out.println ("Content length: "+Integer.toString(contentLength)+" - Bytes read:  "+Integer.toString(bytesRead));
		}
		if (contentLength-bytesRead == 0) return (0);
		try
		{ 
		    bread = iR.read(b, off, len);
		}
		catch(IOException ioe) 
		{
		    continue;
		}
		doRun = false;
	    }
	    bytesRead += bread;
	    return(bread);
	}

    public long skip(long n) throws IOException 
	{
	    long skipped = iR.skip(n);
	    bytesRead += skipped;
	    return(skipped);
	}

}
