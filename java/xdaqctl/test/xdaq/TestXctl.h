#ifndef __TEST_XCTL_H__
#define __TEST_XCTL_H__

#include "xdaq/Application.h"
#include "xdata/UnsignedLong.h"
#include "xdata/String.h"

class TestXctl : public xdaq::Application  
{
public:
	XDAQ_INSTANTIATOR();

	TestXctl(xdaq::ApplicationStub *stub);
	xoap::MessageReference OnCommand(xoap::MessageReference message)
			throw (xoap::exception::Exception);
	xoap::MessageReference OnFreeze(xoap::MessageReference message)
			throw (xoap::exception::Exception);
	xoap::MessageReference ParameterQuery(xoap::MessageReference message)
			throw (xoap::exception::Exception);
	xoap::MessageReference ParameterGet(xoap::MessageReference message)
			throw (xoap::exception::Exception);
	xoap::MessageReference ParameterSet(xoap::MessageReference message)
			throw (xoap::exception::Exception);

protected:
	xdata::UnsignedLong number_;
	xdata::String       line_;
	xdata::String       nullstr_;

private:
	bool frozen_;
};

#endif  // ifndef __TEST_XCTL_H__
// vim: set ai sw=4 ts=4:
