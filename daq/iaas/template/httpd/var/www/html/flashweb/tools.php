<?php
require 'vendor/autoload.php';

// retrieve all templates according to a regexp, e.g., "cdaq*" for cdaq zone or "*" for all zones
function retrieveTemplates($url, $regex) {
	// echo $url . "/_template/" . $regex;
	$response = \Httpful\Request::get ( $url . "/_template/" . $regex )->send ();
	
	$json = json_decode ( $response, true );
	
	return $json;
}

// retrieve statistics of all indices according to a regexp, e.g., "cdaq*" for cdaq zone or "*" for all zones
function retrieveIndicesStats($url, $regex) {
	$response = \Httpful\Request::get ( $url . "/" . $regex . "/_stats" )->send ();
	
	$json = json_decode ( $response, true );
	
	return $json;
}

// retrieve cluster health
function getClusterHealth($url) {
	$response = \Httpful\Request::get ( $url . "/_cluster/health" )->send ();
	
	$json = json_decode ( $response, true );
	
	return $json;
}

// retrieve cluster health
// return true if it is green
// return false otherwise
function checkHealth($url) {
	$json = getClusterHealth($url);
	
	if ( $json["status"] == "green") {
		return true;
	} else {
		return false;
	}
}

// retrieve cluster health in pretty format
function getClusterHealthJSONPretty($url) {
	$uri = $url . "/_cluster/health?pretty";
	echo "curl -XGET " . $uri;
	$response = \Httpful\Request::get ( $uri )->send ();
	return $response;
}

// retrieve a raw list of index names
function retrieveIndexList($url, $regex) {
	$response = \Httpful\Request::get ( $url . '/_cat/indices/' . $regex . '?h=index' )->send ();

	$index_list = array();
	if (! $response->hasErrors () && $response->hasBody()) {
		// $index_list = explode (" \n", trim($response));
		$index_list = preg_split ( "/( \n|\n|\r\n|\r)/", trim ( $response ));
	}
	return $index_list;
}

// retrieve indices according to a regexp, e.g., "cdaq*" for cdaq zone or "*" for all zones
function retrieveIndices($url, $regex) {
	$index_list = retrieveIndexList ( $url, $regex );

	$full_list = array ();
	foreach ( $index_list as $index_name ) {
		$response = \Httpful\Request::get ( $url . "/" . $index_name )->send ();
		$json = json_decode ( $response, true );
		$full_list [$index_name] = $json [$index_name];
	}
	return $full_list;
}

// delete all indices according to a regexp, e.g., "cdaq-jobcontrol*"
function deleteIndices($url, $regex) {
	$response = \Httpful\Request::delete ( $url . "/" . $regex )->send ();
	
	$json = json_decode ( $response, true );
	
	return $json;
}

// close all indices according to a regexp, e.g., "cdaq-jobcontrol*"
function closeIndices($url, $regex) {
	$response = \Httpful\Request::post ( $url . "/" . $regex . "/_close")->send ();

	$json = json_decode ( $response, true );

	return $json;
}

// delete all template according to a regexp, e.g., "cdaq-jobcontrol*"
function deleteTemplates($url, $regex) {
	$response = \Httpful\Request::delete ( $url . "/_template/" . $regex )->send ();
	
	$json = json_decode ( $response, true );
	
	return $json;
}

// calculate the number of indices retrieved using regexp
function indexCount($url, $regex) {
	$index_list = retrieveIndexList ( $url, $regex );
	return count ( $index_list );
}

// calculate the number of document from indices retrieved using regexp
function documentCount($url, $regex) {
	$response = \Httpful\Request::get ( $url . "/" . $regex . "/_count" )->send ();
	
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["count"] )) {
		return $json ["count"];
	} else {
		return "unknown";
	}
}

// check if response has an error (argument should be json decoded)
function isErrorResponse($json) {
	return array_key_exists ( "error", $json );
}

// returns the date when the last document was created in the index/indices indicated by parameter $regex
function lastUpdate($url, $regex) {
	$query = '{ "aggs" : { "last_update" : { "max" : { "field" : "creationtime_", "format": "date_time_no_millis" } } } }';
	
	$response = \Httpful\Request::post ( $url . "/" . $regex . "/_search?ignore_unavailable=true" )->sendsJson ()->body ( $query )->send ();
	
	// echo 'query = ' . $query . '<br>';
	// echo 'url = ' . $url . "/" . $regex . "/_search" . '<br>';
	// echo 'response = ' . $response . '<br>';
	
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["aggregations"] ["last_update"] ["value_as_string"] )) {
		return $json ["aggregations"] ["last_update"] ["value_as_string"];
	} else {
		return "unknown";
	}
}

// returns the date when the last document was created in the index/indices indicated by parameter $regex
function lastUpdateMillis($url, $regex) {
	$query = '{ "aggs" : { "last_update" : { "max" : { "field" : "creationtime_" } } } }';
	
	$response = \Httpful\Request::post ( $url . "/" . $regex . "/_search" )->sendsJson ()->body ( $query )->send ();
	
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["aggregations"] ["last_update"] ["value"] )) {
		return intval ( $json ["aggregations"] ["last_update"] ["value"] );
	} else {
		return -1;
	}
}

// format size units to KiB, MiB, GiB
function formatSizeUnits($bytes) {
	if ($bytes >= 1073741824) {
		$bytes = number_format ( $bytes / 1073741824, 2 ) . ' GiB';
	} elseif ($bytes >= 1048576) {
		$bytes = number_format ( $bytes / 1048576, 2 ) . ' MiB';
	} elseif ($bytes >= 1024) {
		$bytes = number_format ( $bytes / 1024, 2 ) . ' KiB';
	} else {
		$bytes = $bytes . ' B';
	}
	
	return $bytes;
}

// returns the size of the index in bytes
function getStoreSize($url, $indexname) {
	$response = \Httpful\Request::get ( $url . "/" . $indexname . "/_stats" )->send ();
	
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["_all"] ["primaries"] ["store"] ["size_in_bytes"] )) {
		return formatSizeUnits ( $json ["_all"] ["primaries"] ["store"] ["size_in_bytes"] );
	} else {
		return 'unknown';
	}
}

// retrieve a template description from Elasticsearch in pretty JSON format
function retrieveTemplateJSONPretty($url, $name) {
	$uri = $url . "/_template/" . $name . "?pretty";
	echo "curl -XGET " . $uri;
	$response = \Httpful\Request::get ( $uri )->send ();
	return $response;
}

// retrieve an index description from Elasticsearch in pretty JSON format
function retrieveIndexJSONPretty($url, $name) {
	$uri = $url . "/" . $name . "?pretty";
	echo "curl -XGET " . $uri;
	$response = \Httpful\Request::get ( $uri )->send ();
	return $response;
}

// retrieve last document from index in pretty JSON format
function retrieveLastDocumentJSONPretty($url, $indexname) {
	// $query = '{ "aggs" : { "last_update" : { "max" : { "field" : "creationtime_" } } } }';
	$query = '{ "query": { "match_all": {} }, "size": 1, "sort": [ { "creationtime_": { "order": "desc" } } ] }';
	
	$uri = $url . "/" . $indexname . "/_search?pretty";
	
	echo "curl -XGET '" . $uri . "' -d '" . $query . "'";
	
	$response = \Httpful\Request::post ( $uri )->sendsJson ()->body ( $query )->send ();
	
	return $response;
}

// build filter for a query
function buildFilter($filter) {
	$query = '"query": {"match_all": {}';
	
	$arrayobject = new ArrayObject ( $filter );
	$iterator = $arrayobject->getIterator ();
	
	if ($arrayobject->count () > 0) {
		
		$query .= ',"filter": {"and": [';
		
		while ( $iterator->valid () ) {
			
			$val = $iterator->current ();
			
			$query .= '{"regexp": { "' . $iterator->key () . '": "' . $val . '"}}';
			
			$iterator->next ();
			
			if ($iterator->valid ()) {
				$query .= ',';
			}
		}
		
		$query .= ']}';
	}
	$query .= '}';
	return $query;
}

// return field type
function getFieldType($mapping, $flash, $name) {
	if (isset ( $mapping [$flash] ['properties'] [$name] ['type'] )) {
		return $mapping [$flash] ['properties'] [$name] ['type'];
	} else {
		return "unknown";
	}
}

// get flashlist type
function getFlashType($mapping, $flash, $fieldname) {
	if (isset ( $mapping [$flash] ['properties'] [$fieldname] ['type'] )) {
		return 'simple';
	} else {
		return 'complex';
	}
}

// check if index has expired
// $result->invert == true: index has expired
// $result->invert == false: index has not expired
function hasExpired($starttime, $timeinterval, $timetolive) {
	$startTime_o = new DateTime ( $starttime, new DateTimeZone ( 'UTC' ) );
	$timeInterval_o = new DateInterval ( $timeinterval );
	$timeToLive_o = new DateInterval ( $timetolive );
	
	// calculating expiration time
	$expirationTime_o = $startTime_o->add ( $timeInterval_o );
	$expirationTime_o->add ( $timeToLive_o );
	
	$currentTime_o = new DateTime ( null, new DateTimeZone ( 'UTC' ) );
	
	return $currentTime_o->diff ( $expirationTime_o );
}

// delete alias from index
function deleteAlias($url, $index_name, $alias_name) {
	//echo "deleteAlias: index_name = " . $index_name . ", alias_name = " . $alias_name . PHP_EOL;
	$response = \Httpful\Request::delete ( $url . "/" . $index_name . "/_alias/" . $alias_name )->send ();
	
	$json = json_decode ( $response, true );
	
	return $json;
}

// checking the rotate status of the type
function getRotateStatus($url, $zone, $flashlist) {
	$query = '{"query": {"bool": {"must": [{"match": {"zone": "' . $zone . '"}}, {"match": {"flashlist": "' . $flashlist . '"}}]}}}';
	$response = \Httpful\Request::get ( $url . "/flashweb-metadata/rotate/_search" )->sendsJson ()->body ( $query )->send ();
	
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["hits"] ["total"] ) && ($json ["hits"] ["total"] == 1)) {
		return $json ["hits"] ["hits"] [0] ["_source"] ["rotate"];
	}
	
	return true;
}

// returns index state (open/closed)
function getIndexState($url, $index_name) {
	$response = \Httpful\Request::get ( $url . "/_cluster/state/metadata/" . $index_name . "/?filter_path=metadata.indices.*.state" )->send ();
	
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["metadata"] ["indices"] [$index_name] ["state"] )) {
		return $json ["metadata"] ["indices"] [$index_name] ["state"];
	} else {
		return 'unknown';
	}
}

// setting the rotate status
function setRotate($url, $zone, $flashlist, $value) {
	$query = '{"query": {"bool": {"must": [{"match": {"zone": "' . $zone . '"}}, {"match": {"flashlist": "' . $flashlist . '"}}]}}}';
	$response = \Httpful\Request::get ( $url . "/flashweb-metadata/rotate/_search" )->sendsJson ()->body ( $query )->send ();
	
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["hits"] ["total"] ) && ($json ["hits"] ["total"] == 1)) {
		$docId = $json ["hits"] ["hits"] [0] ["_id"];
		$query = '{"doc": {"rotate": ' . $value . '}}';
		$response = \Httpful\Request::post ($url . "/flashweb-metadata/rotate/" . $docId . "/_update")->sendsJson ()->body ( $query )->send ();
	} else {
		//creating index
		\Httpful\Request::put ( $url . "/flashweb-metadata" )->send ();
		//creating mapping
		$mappings = '{"properties": {"zone": {"type": "string"}, "flashlist": {"type": "string"}, "rotate": {"type": "boolean"}}}';
		\Httpful\Request::put ( $url . "/flashweb-metadata/_mapping/rotate" )->sendsJson ()->body ( $mappings )->send ();
		//creating a document
		$document = '{"zone": "' . $zone . '", "flashlist": "' . $flashlist . '", "rotate": ' . $value . '}';
		$response = \Httpful\Request::post ( $url . "/flashweb-metadata/rotate" )->sendsJson ()->body ( $document )->send ();
	}
}

// function returns the last key of an array (empty string in case of an empty array)
function endKey($array) {
	end ( $array ); // move the internal pointer to the end of the array
	return key ( $array );
}

// function prints comma separated alias list
function printAliases($aliases) {
	$last_key = endKey ( $aliases );
	foreach ( $aliases as $key => $val ) {
		echo $key;
		if ($key != $last_key) {
			echo ", ";
		}
	}
}

// function returns the IP address of master node
function getMasterIP($url) {
	// curl -XGET http://cmsos-iaas-cdaq.cms:9200/_cluster/state/master_node
	$response = \Httpful\Request::get ( $url . "/_cluster/state/master_node" )->send ();
	$json = json_decode ( $response, true );
	
	if (isset ( $json ["master_node"] )) {
		$masterNode = $json ["master_node"];
		$response = \Httpful\Request::get ( $url . "/_nodes" )->send ();
		$json = json_decode ( $response, true );
		if (isset ( $json ['nodes'] [$masterNode] ["ip"] )) {
			return $json ['nodes'] [$masterNode] ["ip"];
		} else {
			throw new Exception ( "Cannot get master node!" );
		}
	} else {
		throw new Exception ( "Cannot get master node!" );
	}
}

function validateSignature($flashlist, $mappings, $signature) {
	if (($flashlist != NULL) && isset ( $mappings [$flashlist] ["_meta"] ["signature"] )) {
		if ($mappings [$flashlist] ["_meta"] ["signature"] == $signature) {
			return true;
		}
	}
	return false;
}

function indexRotate($url, $signature, $zone, $force) {
	// Skipping rotate routine on Elasticsearch non-master nodes
	$masterIP = getMasterIP ( $url );
	$hostname = getHostname ();
	$ipAddresses = gethostbynamel ( $hostname );
	// If it is not a master and we not force-run the script then we exit
	if (! in_array ( $masterIP, $ipAddresses ) && ! $force) {
		return;
	}

	// 1 min
	define ( "MAX_SLEEP", 60000000 );
	// 100 ms
	define ( "THROTTLE_TIME", 100000 );
	$total_sleep = 0;

	// Looping over templates
	$templates = retrieveTemplates ( $url, $zone . '-*-template' );
	foreach ( $templates as $key => $val ) {
		$mappings = $val ["mappings"];
		$flashlist = key ( $mappings );
		if (validateSignature ( $flashlist, $mappings, $signature )) {
			$index_template = $val ["template"];
				
			// Check if index rotate is enabled
			if (getRotateStatus ( $url, $mappings [$flashlist] ["_meta"] ["zone"], $flashlist ) || $force) {
				// echo "rotate is enabled flashlist = " . $flashlist . PHP_EOL;
				// Looping over indices belonging to the template
				$indices = retrieveIndices ( $url, $index_template );
				foreach ( $indices as $key => $val ) {
					$mappings = $val ["mappings"];
					// Returns a key of the first element of the array $mappings
					$flashlist = key ( $mappings );
					if (validateSignature ( $flashlist, $mappings, $signature )) {

						$index_name = $key;

						$starttime = $mappings [$flashlist] ["_meta"] ["starttime"];
						$timeinterval = $mappings [$flashlist] ["_meta"] ["timeinterval"];
						$timetolive = $mappings [$flashlist] ["_meta"] ["timetolive"];

						// echo 'starttime = ' . $starttime . PHP_EOL;
						// echo 'timeinterval = ' . $timeinterval . PHP_EOL;
						// echo 'timetolive = ' . $timetolive . PHP_EOL;
						$expiry = hasExpired ( $starttime, $timeinterval, $timetolive );
						if ($expiry->invert) {
							// Backoff if cluster is not healthy
							while ( ! checkHealth ( $url ) && ($total_sleep < MAX_SLEEP) ) {
								// echo 'sleeping for deleting $total_sleep = ' . $total_sleep . PHP_EOL;
								usleep ( THROTTLE_TIME );
								$total_sleep += THROTTLE_TIME;
							}
								
							// If timeout reached we give up
							if ($total_sleep > MAX_SLEEP) {
								return;
							} else {
								// echo "Deleting index: " . $index_name . PHP_EOL;
								deleteIndices ( $url, $index_name );
							}
						} else if (isset ( $mappings [$flashlist] ["_meta"] ["openinterval"] )) {
							// if openinterval is specified we may need to close the index
							$openinterval = $mappings [$flashlist] ["_meta"] ["openinterval"];
							$closing = hasExpired ( $starttime, $timeinterval, $openinterval );
							if ($closing->invert) {
								// Backoff if cluster is not healthy
								while ( ! checkHealth ( $url ) && ($total_sleep < MAX_SLEEP) ) {
									// echo 'sleeping for closing $total_sleep = ' . $total_sleep . PHP_EOL;
									usleep ( THROTTLE_TIME );
									$total_sleep += THROTTLE_TIME;
								}

								// If timeout reached we give up
								if ($total_sleep > MAX_SLEEP) {
									return;
								} else {
									// echo "Closing index: " . $index_name . PHP_EOL;
									closeIndices ( $url, $index_name );
								}
							}
						}
					}
				}
			}
		}
	}
}

function aliasRotate($url, $signature, $zone, $force) {
	// Skipping rotate routine on Elasticsearch non-master nodes
	$masterIP = getMasterIP ( $url );
	$hostname = getHostname ();
	$ipAddresses = gethostbynamel ( $hostname );
	if (! in_array ( $masterIP, $ipAddresses ) && ! $force) {
		return;
	}

	// sorting function
	function sortFunction($a, $b) {
		return ( int ) ($b ["settings"] ["index"] ["creation_date"]) - ( int ) ($a ["settings"] ["index"] ["creation_date"]);
	}

	// Looping over templates
	$templates = retrieveTemplates ( $url, $zone . '-*-template' );
	foreach ( $templates as $key => $val ) {
		$mappings = $val ["mappings"];
		$flashlist = key ( $mappings );
		if (validateSignature ( $flashlist, $mappings, $signature )) {
			$zone = $mappings [$flashlist] ["_meta"] ["zone"];
			$prefix = $zone . "-" . strtolower ( $flashlist );
			$index_template = $val ["template"];
				
			// Check if index rotate is enabled
			if (getRotateStatus ( $url, $mappings [$flashlist] ["_meta"] ["zone"], $flashlist ) || $force) {
				// echo "rotate is enabled flashlist = " . $flashlist . PHP_EOL;
				// Looping over indices belonging to the template
				$validIndices = array ();
				$indices = retrieveIndices ( $url, $index_template );
				foreach ( $indices as $key => $val ) {
					$mappings = $val ["mappings"];
					if (validateSignature ( $flashlist, $mappings, $signature )) {
						$validIndices [$key] = $val;
					}
				}

				uasort ( $validIndices, "sortFunction" );

				// We skip last two indices created (we rely on sorting by create_date)
				$validIndices = array_slice ( $validIndices, 2 );
				foreach ( $validIndices as $key => $val ) {
					$index_name = $key;
					// echo "Deleting alias from index: " . $index_name . PHP_EOL;
					deleteAlias ( $url, $index_name, $prefix . "-flash" );
				}
			}
		}
	}
}

// date validation
// Usage:
// if (validateDate ( $creationtime, 'Y-m-d\tH:i:s\z' ))
function validateDate($date, $format) {
	$d = DateTime::createFromFormat ( $format, $date, new DateTimeZone ( 'UTC' ) );
	return $d && $d->format ( $format ) == $date;
}

// Transforming array keys to lower case
// $mappings = array_change_key_case ( $val ["mappings"], CASE_LOWER );

//$timeperiod = preg_replace ( "/" . $_prefix . "/", '', $index_name );
//list ( $creationtime, $duration ) = explode ( "_", $timeperiod );

?>
