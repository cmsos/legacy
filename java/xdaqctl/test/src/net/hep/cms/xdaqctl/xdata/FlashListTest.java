package net.hep.cms.xdaqctl.xdata;

import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.lang.String;
import java.util.Set;

public class FlashListTest extends TestCase {

	public void testCreate() throws Exception {
		
		FlashList fl = new FlashList();
		
		assertNotNull(fl);
		assertEquals("default", fl.getName());
		
		fl = new FlashList("foo");
		assertEquals("foo", fl.getName());
	}
	
	public void testAddItem() throws Exception {
		
		XDataItem xItem;
		SimpleItem sItem;
		FlashList fl = new FlashList();
		
		assertNull(fl.get("string"));
		fl.add("string", XDataType.STRING);
		xItem = fl.get("string");
		assertNotNull(xItem);
		sItem = (SimpleItem)xItem;
		assertNull(sItem.getValue());
		sItem.setValue("");
		assertEquals("", sItem.getValue());
	
		fl.add("integer", XDataType.INTEGER);
		xItem = fl.get("integer");
		assertNotNull(xItem);
		sItem = (SimpleItem)xItem;
		assertNull(sItem.getValue());
		sItem.setValue(0);
		assertEquals(0, sItem.getValue());
		
		// checking dependency between the object used to set value and
		// flashlist internal value
		SimpleItem value = (SimpleItem)fl.get("string");

		value.setValue("new string");
		assertEquals("new string", ((SimpleItem)fl.get("string")).getValue());
	}

	public void testSerialize() throws Exception {
	
		FlashList fl = new FlashList();

		String varName = "string";	
		String varValue = "LongerThanFour";
		String varIntegerName = "integer";	
		int varIntegerValue = 3;
		
		// add string
		fl.add(varName, XDataType.STRING);	
		SimpleItem item = (SimpleItem)fl.get(varName);
		item.setValue(varValue);

		// add integer
		fl.add(varIntegerName, XDataType.INTEGER);
		SimpleItem itemInteger = (SimpleItem)fl.get(varIntegerName);	
		itemInteger.setValue(varIntegerValue);
	 
		ByteArrayInputStream s = fl.serialize();
		
		assertNotNull(s);
		DataInputStream resultStream = new DataInputStream(s);  // to be checked later

		// header 
		ByteArrayOutputStream headerOutputStream = new ByteArrayOutputStream();
		DataOutputStream headerOut = new DataOutputStream( headerOutputStream );

		// string
		ByteArrayOutputStream stringOutputStream = new ByteArrayOutputStream();
		DataOutputStream stringOut = new DataOutputStream( stringOutputStream );

		// integer
		ByteArrayOutputStream integerOutputStream = new ByteArrayOutputStream();
		DataOutputStream integerOut = new DataOutputStream( integerOutputStream );

		headerOut.writeInt(11);      // table type descriptor
		headerOut.writeInt(15);      // #col, uint32
		headerOut.writeInt(2);       // fl size (number of members)
		headerOut.writeInt(15);      // #row, uint32
		headerOut.writeInt(1);       // 1 row
		
		stringOut.writeInt(4);       // string type
		stringOut.writeInt(varName.length());
		stringOut.writeBytes(varName);
		int padding = 3 - (varName.length() + 3 ) % 4;
		for (int i = 0; i < padding; ++i) {
			stringOut.writeByte(0);
		}

		stringOut.writeInt(15);  // uint32
		stringOut.writeInt(4);   // string
		
		stringOut.writeInt(9);   // vector, length 1
		stringOut.writeInt(1);
		
		stringOut.writeInt(4);   // string
		stringOut.writeInt(varValue.length());
		stringOut.writeBytes(varValue);
		padding = 3 - (varValue.length() + 3 ) % 4; // padding
		for (int i = 0; i < padding; ++i) {
			stringOut.writeByte(0);
		}
		
		integerOut.writeInt(4);       // string type
		integerOut.writeInt(varIntegerName.length());
		integerOut.writeBytes(varIntegerName);
		padding = 3 - (varIntegerName.length() + 3 ) % 4;
		for (int i = 0; i < padding; ++i) {
			integerOut.writeByte(0);
		}

		integerOut.writeInt(15);  // uint32
		integerOut.writeInt(1);   // integer
		
		integerOut.writeInt(9);   // vector, length 1
		integerOut.writeInt(1);
		
		integerOut.writeInt(1);   // integer
		integerOut.writeInt(varIntegerValue);
			
		DataInputStream expectedStream = new DataInputStream(
				new ByteArrayInputStream(headerOutputStream.toByteArray()));
		
		int expected, result;
		
		assertEquals("Stream length is different",
				headerOut.size() + stringOut.size() + integerOut.size(),
				resultStream.available());
		
		int index = 0;
		int indexMarked;

		while (expectedStream.available() > 0) {
			expected = expectedStream.readInt();
			result=resultStream.readInt();
			assertEquals("Difference at " + index +":", expected, result);
			++index;
		}		
		
		// mark end of header part
		resultStream.mark(0);
		indexMarked = index;
		
		ByteArrayOutputStream out;
		String error = "";
		
		// test string + integer
		boolean passed1 = true;
		out = new ByteArrayOutputStream();
		out.write(stringOutputStream.toByteArray());
		out.write(integerOutputStream.toByteArray());
		
		expectedStream = new DataInputStream(
				new ByteArrayInputStream(out.toByteArray()));
		
		while (expectedStream.available() > 0) {
			expected = expectedStream.readInt();
			result=resultStream.readInt();
			if (expected != result) {
				error = "expected " + expected + ", but was " + result + " at " + index + "\n";
				passed1 = false;
				break;
			}
			++index;
		}
		
		resultStream.reset();
		index = indexMarked;
		
		// test integer + string 
		boolean passed2 = true;
		out = new ByteArrayOutputStream();
		out.write(integerOutputStream.toByteArray());
		out.write(stringOutputStream.toByteArray());
		
		expectedStream = new DataInputStream(
				new ByteArrayInputStream(out.toByteArray()));
		
		while (expectedStream.available() > 0) {
			expected = expectedStream.readInt();
			result=resultStream.readInt();
			if (expected != result) {
				error += "expected " + expected + ", but was " + result + " at " + index + "\n";
				passed2 = false;
				break;
			}
			++index;
		}		
	
		assertTrue(error, passed1 ^ passed2);
	}
	
	public void testSerializeTable() throws Exception {
		
		FlashList fl = new FlashList("test");

		String varTableName = "table";
		String colName = "uint32";
		
		fl.add(varTableName, XDataType.TABLE);

		// create a 1x2 table with two uint32 values
		net.hep.cms.xdaqctl.xdata.Table table =
				(net.hep.cms.xdaqctl.xdata.Table)fl.get(varTableName);
		table.addColumn(colName, XDataType.UNSIGNED_INTEGER32);
		
		java.util.Vector<Object> row = new java.util.Vector<Object>();
		row.add(0x80000000);
		table.add(row);
		row.set(0, 0x7fffffff);
		table.add(row);

		ByteArrayInputStream s = fl.serialize();
		assertNotNull(s);
		DataInputStream resultStream = new DataInputStream(s);  // to be checked later

		// table
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		DataOutputStream tableOut = new DataOutputStream( outputStream );
		
		net.hep.cms.xdaqctl.xdata.UnsignedInteger32 uint32 =
				new net.hep.cms.xdaqctl.xdata.UnsignedInteger32();
		net.hep.cms.xdaqctl.xdata.String string =
				new net.hep.cms.xdaqctl.xdata.String();

		tableOut.writeInt(11);      // table type descriptor
		uint32.setValue(1);
		uint32.writeEXDR(tableOut); // #col = 1
		uint32.writeEXDR(tableOut); // #row = 1
		
		string.setValue(varTableName);
		string.writeEXDR(tableOut);  // name = "table"
		uint32.setValue(11);
		uint32.writeEXDR(tableOut);  // type = table	

		tableOut.writeInt(9);        // vector, length 1
		tableOut.writeInt(1);
		
		tableOut.writeInt(11);       // table
		uint32.setValue(1);
		uint32.writeEXDR(tableOut); // #col = 1
		uint32.setValue(2);
		uint32.writeEXDR(tableOut); // #row = 2
	
		string.setValue(colName);
		string.writeEXDR(tableOut);  // name = "uint32"
		uint32.setValue(15);
		uint32.writeEXDR(tableOut);  // type = uint32

		tableOut.writeInt(9);        // vector, length 2
		tableOut.writeInt(2);
		
		uint32.setValue(0x80000000);
		uint32.writeEXDR(tableOut);  // the first element
		uint32.setValue(0x7fffffff);
		uint32.writeEXDR(tableOut);  // the second element
		
		// compare the streams
		DataInputStream expectedStream = new DataInputStream(
				new ByteArrayInputStream(outputStream.toByteArray()));
		
		int expected, result;
		
		assertEquals("Stream length is different",
				tableOut.size(), resultStream.available());
		
		int index = 0;
		while (expectedStream.available() > 0) {
			expected = expectedStream.readInt();
			result = resultStream.readInt();
			assertEquals("Difference at " + index +":", expected, result);
			++index;
		}
	}

	public void testDeserializeTable() throws Exception {
		
		
		FlashList flExp = new FlashList("test");
		
		flExp.add("int32", XDataType.INTEGER32);
		flExp.add("string", XDataType.STRING);
		((SimpleItem)flExp.get("int32")).setValue(-9);
		((SimpleItem)flExp.get("string")).setValue("rcms");

		FlashList flRes = new FlashList();
		flRes.deserialize(flExp.serialize());
		
		for (String name: flExp.getItemNames()) {
			assertEquals("failed for item " + name,
					((SimpleItem)flExp.get(name)).getValue(),
					((SimpleItem)flRes.get(name)).getValue());
		}
	}
	
	public void testGetItems() throws Exception 
	{
		XDataItem xItem;
		SimpleItem sItem;
		
		FlashList fl = new FlashList("test");
		
		String varTableName = "table";
		String colName = "uint32";
		
		fl.add(varTableName, XDataType.TABLE);

		// create a 1x2 table with two uint32 values
		net.hep.cms.xdaqctl.xdata.Table table =
				(net.hep.cms.xdaqctl.xdata.Table)fl.get(varTableName);
		table.addColumn(colName, XDataType.UNSIGNED_INTEGER32);
		
		java.util.Vector<Object> row = new java.util.Vector<Object>();
		row.add(0x80000000);
		table.add(row);
		row.set(0, 0x7fffffff);
		table.add(row);

		fl.add("string", XDataType.STRING);
		xItem = fl.get("string");
		sItem = (SimpleItem)xItem;
		sItem.setValue("");
	
		fl.add("integer", XDataType.INTEGER);
		xItem = fl.get("integer");
		sItem = (SimpleItem)xItem;
		sItem.setValue(0);
		
		Set<String> l = fl.getItemNames();
		assertNotNull(l);
		assertEquals(3, l.size());
		
		for (String name: l) {
			assertNotNull(fl.get(name));
		}

	}
}
