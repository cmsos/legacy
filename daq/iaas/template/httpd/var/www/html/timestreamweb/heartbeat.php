<?php
require 'vendor/autoload.php';
function searchTimestreamServices($url) {
	//print ("going to retrieve " . $url . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?classname=elastic::timestream::Application\n") ;
	$response = \Httpful\Request::get ( $url . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?classname=elastic::timestream::Application" )->send ();
	$json = json_decode ( $response, true );
	return $json;
}
function disableXtremeServices($host, $port) {
	$json = searchXtremeServices ( $host, $port );
	
	foreach ( $json ['table'] ['rows'] as $row ) {
		$context = $row ['context'];
		$lid = $row ['id'];
		$clasname = $row ['class'];
		$age = floatval ( $row ['age'] );
		
		//print ('going to disable' . $context . ' on lid' . $lid . ' of age ' . $age) ;
		if ($age < 1.0) {
			if ("http://cms-kvm-31.cms:9950" != $context) {
				$response = \Httpful\Request::get ( $context . "/" . "urn:xdaq-application:lid=" . $lid . "/" . "disableESCloud" )->send ();
			}
		}
	}
}
function enableXtremeServices($host, $port) {
	$json = searchXtremeServices ( $host, $port );
	
	foreach ( $json ['table'] ['rows'] as $row ) {
		$context = $row ['context'];
		$lid = $row ['id'];
		$clasname = $row ['class'];
		$age = floatval ( $row ['age'] );
		
		print ('going to enable' . $context . ' on lid' . $lid . ' of age ' . $age) ;
		if ($age < 1.0) {
			if ("http://cms-kvm-31.cms:9950" != $context) {
				$response = \Httpful\Request::get ( $context . "/" . "urn:xdaq-application:lid=" . $lid . "/" . "enableESCloud" )->send ();
			}
		}
	}
}


?>
