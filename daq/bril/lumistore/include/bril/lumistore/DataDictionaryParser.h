#ifndef _bril_lumistore_DataDictionaryParser_h
#define _bril_lumistore_DataDictionaryParser_h
#include <string>
#include <sstream>
#include "toolbox/string.h"
namespace bril{
  namespace lumistore{
    size_t extractPrimitiveSize(const std::string& typecode){
      size_t last_index = typecode.find_last_not_of("0123456789");
      std::string digit = typecode.substr(last_index + 1);
      std::istringstream iss(digit);
      size_t result;
      iss >> result;
      if(typecode.substr(0,last_index)=="STRING"){
	return result;
      }
      return result/8;
    }
    
    std::string extractPrimitiveName(const std::string& typecode){
      size_t last_index = typecode.find_last_not_of("0123456789");
      return typecode.substr(0,last_index);
    }
    
    std::string payloadTableName(const std::string& typecode, const std::string& payloadTablePrefix){
      std::string primitivename = extractPrimitiveName(typecode);
      if(typecode=="STRING") return payloadTablePrefix+"_STRING";
      return payloadTablePrefix+"_"+typecode;
    }
    
    struct DataDictionaryParser{
      DataDictionaryParser(const std::string& datadict,bool getalias=false){
	// <typecode>:<fieldlength>:[alias] , <typecode>:<fieldlength>:[alias]
	std::list<std::string> datafields = toolbox::parseTokenList(datadict,",");	
	for(std::list<std::string>::iterator it=datafields.begin();it!=datafields.end();++it){
	  std::string field = *it;
	  std::list<std::string> fieldinfo = toolbox::parseTokenList(field,":");
	  std::list<std::string>::iterator infoIt = fieldinfo.begin();
	  std::string typecode = *infoIt;
	  std::advance(infoIt,1);
	  std::string fieldsizeStr = *infoIt;
	  std::istringstream iss(fieldsizeStr);
	  size_t fieldsize;
	  iss >> fieldsize;
	  fields.push_back(std::make_pair(typecode,fieldsize));
	  if(getalias){
	    if(fieldinfo.size()>2){
	      std::advance(infoIt,2);
	      std::string alias = *infoIt;
	      fieldalias.push_back( alias );
	    }else{
	      fieldalias.push_back("");
	    }
	  }
	}
      }
      std::vector< std::pair<std::string,size_t> > fields; //typecode:fieldlength
      std::vector<std::string> fieldalias; //aliases match fields vector pos
    };  
  }}//ns
#endif
