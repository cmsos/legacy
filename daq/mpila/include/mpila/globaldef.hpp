/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * globaldef.hpp
 *
 *  Created on: Mar 20, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once
#include <memory>
#include <stdexcept>

namespace std
{
// Implementation for "make_unique" in c++11 as
// it doesn't contain this function.  (see std::make_shared)
// This function is part of the C++14 (and newer) standard.
/*template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{  // NOLINT(build/c++11)
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
*/
#if __cplusplus < 201300
// Implementation for "make_unique" in c++11 as
// it doesn't contain this function.  (see std::make_shared)
// This function is part of the C++14 (and newer) standard.
template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{  // NOLINT(build/c++11)
     return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
#endif
} /* namespace std */

namespace mpila
{

enum class TransportMode
{
    TEST,
    TEST_SOME,
    TEST_ALL
};

}  // namespace mpila
