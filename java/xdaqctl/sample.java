/*
 * sample.java
 *
 * Sample XDAQ control applicaton using the xdaqctl Java library.
 */

import net.hep.cms.xdaqctl.*;

public class sample
{
	final static String APPLICATION = "TestXctl";

	public static void main(String argv[])
	{
		try {
			// Create an instance of XDAQDocument reading from
			// a sample XML configuration file, 'sample.xml'.
			//
			XDAQDocument xdaq = new XDAQDocument("sample.xml");
			xdaq.assignLocalID();

			// 'Configure' all hosts.
			System.out.print(xdaq.configureHost());

			// Get parameters, change a value and
			// set it to the XDAQ executive
			XDAQParameter param = xdaq.getParameter(APPLICATION, 0);
			System.out.println("Old number: " + param.getValue("number"));
			System.out.println("New number: " + param.setValue("number", "7"));
			param.send();

			// Send a command to the application
			System.out.print(xdaq.getResponseString("Command", APPLICATION));

			// After waiting for 10 seconds, just quit
			try { Thread.sleep(10000); } catch (Exception ignored) {}

			System.exit(0);

		} catch (XDAQException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}

// End of file
// vim: set sw=4 ts=4:
