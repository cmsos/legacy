#ifndef __FrlDataSource
#define __FrlDataSource

#include "ferol/DataSourceIF.hh"
#include "ferol/ferolConstants.h"
#include <sstream>
#include "d2s/utils/Exception.hh"

namespace ferol
{
    /**
     *
     *
     *     @short Configures the FRL SLINK inputs.
     *            
     *       @see 
     *   $Author$
     * $Revision$
     *     $Date$
     *
     **/
    class FrlDataSource : public DataSourceIF {
    public:
        FrlDataSource( HAL::HardwareDeviceInterface *device_P,
                       utils::InfoSpaceHandler &appIS,
                       Logger logger );
        /**
         *
         *     @short Sets up the data source of the Ferol for SLINK use.
         *            
         *            This     function     just      sets     the     register 
         *            "FRAGMENT_DATA_SOURCE" to 0x0.
         *
         **/
        void setDataSource() const;
    };
}

#endif /* __FrlDataSource */
