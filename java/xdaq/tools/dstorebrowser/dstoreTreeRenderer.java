package xdaq.tools.dstorebrowser;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.awt.*;
import java.util.*;
import org.w3c.dom.*;
import java.net.*;

import xdaq.tools.win.xdaqWinTreeAdapterNode;

public class dstoreTreeRenderer extends DefaultTreeCellRenderer
{
    URL dataSourceURL, tableURL, emptyTableURL, integerURL, floatURL, textURL;
	public dstoreTreeRenderer ()
	{
	    dataSourceURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/database.gif"); 
	    tableURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/table.gif"); 
	    emptyTableURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/table_empty.gif"); 
	    integerURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/integer.gif");
	    floatURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/float.gif");
	    textURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/text.gif"); 
	}
	
	public Component getTreeCellRendererComponent (
		JTree tree,
		Object value,
		boolean sel,
		boolean expanded,
		boolean leaf,
		int row,
		boolean hasFocus )
	{
	    // override leaf always false
		super.getTreeCellRendererComponent (tree, value, sel, expanded, leaf, row, hasFocus);

		
		Object obj = (((DefaultMutableTreeNode) value).getUserObject());
		if (obj instanceof Boolean) 
		{
		    setText ("Retrieving data...");
		    return this;
		}

		xdaqWinTreeAdapterNode node = (xdaqWinTreeAdapterNode)obj;
		
		String nodeName = node.domNode.getNodeName();
		
		String type = node.getAttribute ("_type");

		String hiddenAttribs = "";
		
		NamedNodeMap attributes = node.domNode.getAttributes();
		if (type.startsWith("Module"))
		{
		    this.setIcon ( new ImageIcon (tableURL));
		    String attrName, leafName = nodeName;
		    for (int j = 0; j < attributes.getLength(); j++) {
			attrName = attributes.item(j).getNodeName();
			if (! attrName.startsWith("_")) 
			{
			    leafName = leafName + " " + attrName + "=" + attributes.item(j).getNodeValue();
 			}
 			else if (attrName.startsWith("_operation")) 
			{
				this.setForeground(Color.blue);
 			}
			else hiddenAttribs = hiddenAttribs + " " + attrName + "=" + attributes.item(j).getNodeValue();
		    }
		    this.setText ( leafName);
		}
		else if (type.startsWith("Expandable"))
		{
		    this.setIcon ( new ImageIcon (emptyTableURL));
		    String attrName, leafName = nodeName;
		    for (int j = 0; j < attributes.getLength(); j++) {
			attrName = attributes.item(j).getNodeName();
			if (! attrName.startsWith("_")) 
			{
			    leafName = leafName + " " + attrName + "=" + attributes.item(j).getNodeValue();
			}
			else hiddenAttribs = hiddenAttribs + " " + attrName + "=" + attributes.item(j).getNodeValue();
		    }
		    this.setText ( leafName);

		    DefaultMutableTreeNode n = (DefaultMutableTreeNode) value;
		    if (n.getChildCount() == 0)
		    {
			n.add ( new DefaultMutableTreeNode ( new Boolean (true) ) );
		    }
		}
		else
		{
		    if (type.startsWith("number")) 
		    {	    
			this.setIcon ( new ImageIcon (integerURL));
		    }
		    else if (type.startsWith("int")) 
		    {	    
			this.setIcon ( new ImageIcon (integerURL));
		    }
		    else if (type.startsWith("float")) 
		    {	    
			this.setIcon ( new ImageIcon (floatURL));
		    }
		    else if (type.startsWith("string")) 
		    {	    
			this.setIcon ( new ImageIcon (textURL));
		    }
		    else if (nodeName.startsWith("DataSource")) 
		    {	    
			this.setIcon ( new ImageIcon (dataSourceURL));
		    }
		    this.setText ( node.getNodeName () + ": " + node.domNode.getFirstChild().getNodeValue() );
		    String attrName;
		    for (int j = 0; j < attributes.getLength(); j++) {
			attrName = attributes.item(j).getNodeName();
			if (attrName.startsWith("_"))  hiddenAttribs = hiddenAttribs + " " + attrName + "=" + attributes.item(j).getNodeValue();
		    }
		}
		setToolTipText(hiddenAttribs);
		return this;	
	}
}
