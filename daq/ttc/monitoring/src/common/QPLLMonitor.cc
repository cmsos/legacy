#include "ttc/monitoring/QPLLMonitor.h"

#include "xdata/String.h"
#include "xdata/Table.h"
#include "xcept/tools.h"
#include "log4cplus/loggingmacros.h"

using namespace std;


ttc::QPLLMonitor::QPLLMonitor(
    xdaq::Application* app,
    bool autoInit,
    const double& refreshPeriodSecs,
    const double& refreshOffsetSecs)
:
    PeriodicListener(
        app,
        "ttc_qpll" /* infospaceBaseName */,
        autoInit,
        refreshPeriodSecs,
        refreshOffsetSecs,
        uint16_t(3600/refreshPeriodSecs) /* forceUpdateEvery 1h */ )
{}


void ttc::QPLLMonitor::setQPLLLockingStateLatched(
    const Component& component,
    QPLLLockingState& qpllLockingStateLatched)
{}


void ttc::QPLLMonitor::addItems()
{
  addItem<xdata::String>("LockingState");
  addItem<xdata::String>("LockingStateLatched");
}


bool ttc::QPLLMonitor::refreshItems(const Component& component)
{
  // initialize values
  QPLLLockingState qpllLockingState = QPLLNotAvailable;
  QPLLLockingState qpllLockingStateLatched = QPLLNotAvailable;

  // set current and latched QPLL locking states (calls to abstract methods)
  TRY_CATCH_ALL( setQPLLLockingState(component, qpllLockingState) );
  TRY_CATCH_ALL( setQPLLLockingStateLatched(component, qpllLockingStateLatched) );

  bool componentChanged = false;
  componentChanged |= setItem(component.getKey(), "LockingState",        xdata::String(getStringFromState(qpllLockingState)));
  componentChanged |= setItem(component.getKey(), "LockingStateLatched", xdata::String(getStringFromState(qpllLockingStateLatched)));
  return componentChanged;
}


std::string ttc::QPLLMonitor::getStringFromState(ttc::QPLLLockingState s)
{
  switch(s)
  {
    case QPLLLocked:        return "Locked";
    case QPLLUnlocked:      return "Unlocked";
    case QPLLUndefined:     return "Undefined";
    case QPLLNotAvailable:  return "NotAvailable";
  }
  return "";
}
