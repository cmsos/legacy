#include "ttc/utils/CyclicStringArray.hh"


using namespace std;


ttc::CyclicStringArray::CyclicStringArray(const char* data[])
:
    cur_ptr(0)
{
  while (*data != 0)
  {
    strings.push_back(*data);
    ++data;
  }
}


string ttc::CyclicStringArray::next()
{
  ++cur_ptr;
  if (cur_ptr >= strings.size())
  {
    cur_ptr = 0;
  }

  return strings[cur_ptr];
}
