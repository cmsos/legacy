package xdaq.xdesk.tools;

import java.net.MalformedURLException;
import java.net.URLClassLoader;
import java.net.URL;
import java.lang.*;
import java.util.HashSet;
import java.io.*;

import org.apache.log4j.*;

public class JarClassLoader extends URLClassLoader
{
	Logger logger = Logger.getLogger (JarClassLoader.class);

	public JarClassLoader (URL url)
	{		
		super ( new URL[] {url} );
		loadedURL_ = new HashSet();
		loadedURL_.add (url);
	}

	public JarClassLoader (String href) throws MalformedURLException
	{		
		this ( new URL (href) );
	}
	
	public void addJarURL (String href) throws MalformedURLException
	{
		if (href != null) 
		{
			URL url = new URL (href );
			if (loadedURL_.contains(url)) return; // already loaded
			else addURL ( url );
		}
	}	
	
	HashSet loadedURL_;
}
