package xdaq.tools.win;

import tcl.lang.*;
import xdaq.tools.win.*;


class TclInterpreter {

	Interp interp = null;
	xdaqWin win = null;
	
	public TclInterpreter(xdaqWin win) {
		this.win =win;
		interp = new Interp();
		interp.createCommand("Configure", new TclXdaqCommand(win));
		interp.createCommand("Enable", new TclXdaqCommand(win));
		interp.createCommand("Disable", new TclXdaqCommand(win));
		interp.createCommand("Halt", new TclXdaqCommand(win));
		interp.createCommand("Suspend", new TclXdaqCommand(win));
		interp.createCommand("Resume", new TclXdaqCommand(win));
		interp.createCommand("Reset", new TclXdaqCommand(win));
		interp.createCommand("ParameterGet", new TclXdaqCommand(win));
		interp.createCommand("ParameterSet", new TclXdaqCommand(win));
		interp.createCommand("UserCommand", new TclXdaqCommand(win));
	}
	
	public void dispose() {
		interp.dispose();
	
	}
	
	
	String eval(String filename) {
		try 
		{
			interp.evalFile(filename);
			return "";
		}
		catch(TclException e)
		{
			return "Error in script:"+ e.toString();
		
		}
	}
	String evalScript(String script) {
		try 
		{
			//System.out.println("start evalutaion");
			interp.eval(script);
			//System.out.println("finish evaluation");
			return "";
		}
		catch(TclException e)
		{
			return "Error in script:"+ e.toString();
		
		}
	}
	
	public void interruptEval()
	{
		interp.interruptEval();
	}


}
