// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_pvss_HandlerService_h_
#define _psx_appweb_pvss_HandlerService_h_

#include "appWeb/appWeb.h"

// This file includes abstract SOAP communication routines
//
#include "psx/appweb/PeerTransportService.h"

// This file include the functions to communicate with PVSS
//
#include "psx/sapi/ApplicationService.h"
    
namespace psx {
	namespace appweb {
		namespace pvss {

			class HandlerService : public MaHandlerService , public psx::appweb::PeerTransportService
			{
			      public:

				HandlerService();
				~HandlerService();
				MaHandler *newHandler(MaServer *server, MaHost *host, char *ext);

				/*//! Implements the interface of the PeerTransportService to send a SOAP message to any destination
				//
				xoap::MessageReference post(const std::string &  url, xoap::MessageReference request) 
					;
				*/	

				int start();
				int stop();

				// Extension to initialize when the module starts			
				void init(const std::string & projectName, const std::string & projectNumber,
				         const std::string & databasetManager, const std::string & eventManager);
				psx::sapi::ApplicationService * getApplicationService();

				private:

				psx::sapi::ApplicationService * as_;
			};
		}	
	}
}

#endif 

