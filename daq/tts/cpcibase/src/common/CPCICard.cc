/**
*      @file CPCICard.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.7 $
*     $Date: 2007/03/27 07:53:25 $
*
*
**/
#include "tts/cpcibase/CPCICard.hh"

#include <iostream>
#include <sstream>
#include <iomanip>

tts::CPCICard::CPCICard( HAL::PCIDevice* device, ipcutils::SemaphoreArray& cratelock ) 
  : _device(device), 
    _serialnumber(""), 
    _geoslot(999), 
    _cratelock(cratelock), 
    _have_write_lock(false),
    _locking_enabled(true) {};

tts::CPCICard::~CPCICard() {

  if (_have_write_lock) {
    try {
      releaseWriteLock();
    }
    catch (...) {
    }
  }

  delete _device;
};

bool tts::CPCICard::getWriteLock() 
  throw (ipcutils::exception::Exception) {

  if (_have_write_lock)
    XCEPT_RAISE( ipcutils::exception::Exception, 
		 "internal error: getWriteLock called and already have write lock.");

  if ( _locking_enabled )
    _have_write_lock = _cratelock.takeIfUnlocked( getGeoSlot() );
  else
    _have_write_lock = true;

  return _have_write_lock;
}

void tts::CPCICard::releaseWriteLock() 
  throw (ipcutils::exception::Exception) {

  if (! _have_write_lock)
    XCEPT_RAISE( ipcutils::exception::Exception, 
		 "internal error: releaseWriteLock called and do not have write lock.");

  _have_write_lock = false;

  if ( _locking_enabled ) {

    try {
      _cratelock.give( getGeoSlot() );
    }
    catch (...) {
      _have_write_lock = true;
      throw;
    }

  }

}

bool tts::CPCICard::haveWriteLock() {
  return _have_write_lock;
}

void tts::CPCICard::check_write_lock() 
  throw (tts::exception::NoWriteLock) {

  if ( !_have_write_lock )
    XCEPT_RAISE( tts::exception::NoWriteLock, 
		 "do not have write lock for this FMM. Obtain it with getWriteLock()");
}

int tts::CPCICard::getWriteLockOwnerPID() 
  throw (ipcutils::exception::Exception) {

  if ( ! _locking_enabled ) 
    return -1;

  if ( ! _cratelock.isLocked( getGeoSlot() ) )
    return -1;

  return _cratelock.getPID( getGeoSlot() );
}


void tts::CPCICard::disableLocking() {
  _locking_enabled = false;
}

void tts::CPCICard::enableLocking() {
  _locking_enabled = true;
}

bool tts::CPCICard::isLockingEnabled() {
  return _locking_enabled;
}


std::string tts::CPCICard::readFirmwareRevAltera() 
  throw (HAL::HardwareAccessException) {

  
  uint32_t fwid_altera;
  _dev()->read("FWID_ALTERA", &fwid_altera);

  std::stringstream rev;
  rev << std::hex << std::setw(8) << std::setfill('0') << fwid_altera;

  return rev.str();
}


std::string tts::CPCICard::getSerialNumber() 
  throw (HAL::HardwareAccessException) {

  if (_serialnumber == "") {
    std::stringstream sn;
    uint32_t value;
    
    _dev()->read("sn_c", &value);  
    sn << std::setw(3) << std::setfill('0') << std::hex << value;

    _dev()->read("sn_b", &value);
    sn << std::setw(8) << std::setfill('0') << std::hex << value;

    _dev()->read("sn_a", &value);
    sn << std::setw(8) << std::setfill('0') << std::hex << value;

    _serialnumber = sn.str();
  }
  
  return _serialnumber;
};



uint32_t tts::CPCICard::getGeoSlot() 
  throw (HAL::HardwareAccessException) {
  
  if (_geoslot == 999) {

    uint32_t val;
    _dev()->read("GeoSlot", &val);
    _geoslot = (uint32_t) val;

  }

  return _geoslot;
}
