#include "ttc/utils/VME64xDeviceInfo.hh"

#include "ttc/utils/VMEUtils.hh"

#include "hal/VMEConstants.h"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEBusAdapterInterface.hh"
#include "hal/VMEConfigurationSpaceHandler.hh"

#include <sstream>
#include <iomanip>
#include <cstring>


using namespace std;


// class ttc::VME64xDeviceInfo

ttc::VME64xDeviceInfo::VME64xDeviceInfo()
:
    Valid_(false),
    Addresses_(NUMBER_OF_VME64XFUNCTIONS)
{}


ttc::VME64xDeviceInfo::VME64xDeviceInfo(
    uint32_t ManufacturerId,
    uint32_t BoardId,
    int Location,
    const char* RevisionId,
    const char* SerialNumber,
    const char* Description)
:
    Valid_(true),
    ManufacturerId_(ManufacturerId),
    BoardId_(BoardId),
    hasBoardId_(true),
    hasRevisionId_(RevisionId),
    hasSerialNumber_(SerialNumber),
    hasDescription_(Description),
    Location_(Location),
    Enabled_(true),
    Addresses_(NUMBER_OF_VME64XFUNCTIONS)
{
  if (RevisionId)
  {
    if (strlen(RevisionId) > 4)
    {
      throw std::invalid_argument("Revision Id must be at most 4 characters long");
    }
    RevisionId_ = RevisionId;
  }

  if (SerialNumber)
  {
    SerialNumber_ = SerialNumber;
  }

  if (Description)
  {
    Description_ = Description;
  }
}


ttc::VME64xDeviceInfo::VME64xDeviceInfo(HAL::VMEConfigurationSpaceHandler& handler, int slot)
:
    Valid_(false),
    Location_(slot),
    Enabled_(false),
    Addresses_(NUMBER_OF_VME64XFUNCTIONS)
{
  if (!handler.containsVME64xModule(slot))
    return;

  Valid_ = true;
  ManufacturerId_ = VMEUtils::configread(handler, slot, "manufacturerId");
  BoardId_ = VMEUtils::configread(handler, slot, "boardId");
  hasBoardId_ = (BoardId_ != 0);
  RevisionId_ = VMEUtils::make_str(VMEUtils::configread(handler, slot, "revisionId"));
  hasRevisionId_ = (RevisionId_ != "");
  SerialNumber_ = VMEUtils::configreadstr(handler, slot, "serialNumberStart", "serialNumberEnd");
  hasSerialNumber_ = (SerialNumber_ != "");
  Description_ = VMEUtils::configreadzstr(handler, slot, "descriptionPtr");
  hasDescription_ = (Description_ != "");

  if (handler.enabled(slot))
  {
    Enabled_ = true;

    // loop over VME64X functions
    for (int func = 0; func < NUMBER_OF_VME64XFUNCTIONS; ++func)
    {
      if (handler.functionIsImplemented(slot, func))
      {
        uint32_t add = VMEUtils::configread(handler, slot, string("ADER-F") + char('0' + func));
        uint32_t mask = VMEUtils::configread(handler, slot, string("ADEM-F") + char('0' + func));
        Addresses_[func] = add & mask & 0xFFFFFF00;
      }
    }
  }
}


bool ttc::VME64xDeviceInfo::operator==(const VME64xDeviceInfo& rhs) const
{
  if (!Valid_ || !rhs.Valid_)
    return false;

  if ((ManufacturerId_ != MF_ANY) && (rhs.ManufacturerId_ != MF_ANY))
  {
    if (ManufacturerId_ != rhs.ManufacturerId_)
      return false;
  }

  if (hasBoardId_ && rhs.hasBoardId_)
  {
    if (BoardId_ != rhs.BoardId_)
      return false;
  }

  if (Location_ > 0 && rhs.Location_ > 0)
  {
    if (Location_ != rhs.Location_)
      return false;
  }

  if (hasRevisionId_ && rhs.hasRevisionId_)
  {
    if (RevisionId_ != rhs.RevisionId_)
      return false;
  }

  if (hasSerialNumber_ && rhs.hasSerialNumber_)
  {
    if (SerialNumber_ != rhs.SerialNumber_)
      return false;
  }

  if (hasDescription_ && rhs.hasDescription_)
  {
    if (Description_ != rhs.Description_)
      return false;
  }

  return true;
}


bool ttc::VME64xDeviceInfo::operator!=(const VME64xDeviceInfo & rhs) const
{
  return !(*this == rhs);
}


bool ttc::VME64xDeviceInfo::isValid() const
{
  return Valid_;
}


bool ttc::VME64xDeviceInfo::isEnabled() const
{
  return Enabled_;
}


int ttc::VME64xDeviceInfo::location() const
{
  return Location_;
}


uint32_t& ttc::VME64xDeviceInfo::address(int i)
{
  return Addresses_[i];
}


vector<uint32_t> & ttc::VME64xDeviceInfo::addresses()
{
  return Addresses_;
}


HAL::VMEDevice
ttc::VME64xDeviceInfo::MakeVMEDevice(HAL::VMEBusAdapterInterface& bus, HAL::VMEAddressTable& table)
{
  return HAL::VMEDevice(table, bus, Addresses_);
}


void ttc::VME64xDeviceInfo::print(ostream& out) const
{
  out << dec;

  if (!Valid_)
  {
    out << "Invalid" << endl;
    return;
  }

  out << "Position:       ";

  if (Location_ > 0)
  {
    out << "Slot " << Location_;
  }
  else if (Location_ == 0)
  {
    out << "Unique";
  }
  else
  {
    out << "Index " << -Location_;
  }

  out << "\n";
  out << "Enabled:        " << (Enabled_ ? "Yes" : "No") << "\n";
  out << "ManufacturerId: ";

  if (ManufacturerId_ == MF_ANY)
  {
    out << "-\n";
  }
  else
  {
    out << hex << setfill('0') << setw(6) << ManufacturerId_ << dec << setfill(' ');
    if (ManufacturerId_ == MF_CERN)
    {
      out << "   (CERN)";
    }
    out << "\n";
  }

  out << "BoardId:        ";

  if (!hasBoardId_)
  {
    out << "-\n";
  }
  else
  {
    out << hex << setfill('0') << setw(8) << BoardId_ << dec << setfill(' ');
    out << " (" << VMEUtils::make_str(BoardId_) << ")\n";
  }

  out << "RevisionId:     ";
  if (!hasRevisionId_)
  {
    out << "-\n";
  }
  else
  {
    out << hex << setfill('0') << setw(2) << int(RevisionId_[0]) << setw(2) << int(RevisionId_[1])
        << setw(2) << int(RevisionId_[2]) << setw(2) << int(RevisionId_[3]) << dec << setfill(' ');
    out << " (" << RevisionId_ << ")\n";
  }

  out << "Serial Number:  ";
  if (!hasSerialNumber_)
  {
    out << "-\n";
  }
  else
  {
    out << hex;
    for (size_t i = 0; i < SerialNumber_.length(); ++i)
    {
      out << setw(2) << setfill('0') << int(SerialNumber_[i]);
    }
    if (SerialNumber_.length() < 4)
      out << "  ";
    if (SerialNumber_.length() < 3)
      out << "  ";
    if (SerialNumber_.length() < 2)
      out << "  ";
    if (SerialNumber_.length() < 1)
      out << "  ";
    out << " (" << SerialNumber_ << ")\n";
  }

  out << "Description:    " << (hasDescription_ ? Description_ : "-") << dec << "\n";
}


// non-member functions in namespace ttc

ostream& ttc::operator<<(ostream& out, const VME64xDeviceInfo& info)
{
  info.print(out);
  return out;
}
