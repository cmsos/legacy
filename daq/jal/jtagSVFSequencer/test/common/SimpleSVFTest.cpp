#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/DTTFJTAGController.h"
#include "jal/jtagController/HALScanPSC100Adapter.h"
#include "jal/jtagSVFSequencer/JTAGSVFSequencer.h"

#include "hal/VMEDevice.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
//#include "SBS620x86LinuxBusAdapter.hh"
#include "hal/VMEDummyBusAdapter.hh"

#include <iostream>

#include <stdint.h>

using namespace std;
using namespace HAL;
using namespace jal;



int main(int argc, char **argv) {

  uint32_t VMEbase = 0xA10000;
  char *addrtable_fn = "/home/hsakulin/jtag_test2/AddressTable_Outcard.txt";

  try {

    VMEAddressTableASCIIReader addressTableReader(addrtable_fn);
    VMEAddressTable addressTable("Outcard address table", addressTableReader);

    //    SBS620x86LinuxBusAdapter busAdapter(0);
    VMEDummyBusAdapter busAdapter (VMEDummyBusAdapter::VERBOSE_OFF);

    VMEDevice outcard(addressTable, busAdapter, VMEbase);

    // create a HAL adapter for a ScanPSC type JTAG Controller
    HALScanPSC100Adapter scanpsc_adapter (outcard, "PSC100_BASE"); 

    // create the JTAG Controller using the adapter
    DTTFJTAGController jtag_controller( scanpsc_adapter );
    //    ScanPSC100JTAGController jtag_controller( scanpsc_adapter );

    jtag_controller.setDebugFlag(5);

    // open JTAG Chain for chain 0 on the controller
    JTAGChain jtag_chain( jtag_controller, 0 );
      
    // instantiate SVF Sequencer
    JTAGSVFSequencer seq;

    // load SVF file

    if (argc==2) {
      cout << "Loading File " << argv[1] << endl;
      seq.loadSVFFile(argv[1]);
    }

    cout << "Commands in the chain : " << endl << endl;

    // print the sequence
    //    seq.print();

    cout << endl << endl;

    // run it
    bool status = seq.execute( jtag_chain );

    if (status)
      cout << "successfully played SVF file." << endl;
    else
      cout << "error playing SVF file." << endl;
  }
  catch (exception &e) {
    cout << "Unknown Exception caught: " << e.what() << endl;
    return 1;
  }
  return 0;
}

