
/**
 * This is a generated class and is not intended for modification.  
 */
package valueObjects
{
import com.adobe.fiber.styles.IStyle;
import com.adobe.fiber.styles.Style;
import com.adobe.fiber.styles.StyleValidator;
import com.adobe.fiber.valueobjects.AbstractEntityMetadata;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import mx.events.ValidationResultEvent;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IModelType;
import mx.events.PropertyChangeEvent;

use namespace model_internal;

[ExcludeClass]
internal class _RowsEntityMetadata extends com.adobe.fiber.valueobjects.AbstractEntityMetadata
{
    private static var emptyArray:Array = new Array();

    model_internal static var allProperties:Array = new Array("category", "module", "dateTime", "tag", "notifier", "line", "severity", "sessionid", "type", "version", "message", "schema", "lid", "uniqueid", "occurrences", "service", "context", "func", "uuid", "instance", "identifier", "zone", "groups", "count");
    model_internal static var allAssociationProperties:Array = new Array();
    model_internal static var allRequiredProperties:Array = new Array("category", "module", "line", "func", "count");
    model_internal static var allAlwaysAvailableProperties:Array = new Array("category", "module", "dateTime", "tag", "notifier", "line", "severity", "sessionid", "type", "version", "message", "schema", "lid", "uniqueid", "occurrences", "service", "context", "func", "uuid", "instance", "identifier", "zone", "groups", "count");
    model_internal static var guardedProperties:Array = new Array();
    model_internal static var dataProperties:Array = new Array("category", "module", "dateTime", "tag", "notifier", "line", "severity", "sessionid", "type", "version", "message", "schema", "lid", "uniqueid", "occurrences", "service", "context", "func", "uuid", "instance", "identifier", "zone", "groups", "count");
    model_internal static var sourceProperties:Array = emptyArray
    model_internal static var nonDerivedProperties:Array = new Array("category", "module", "dateTime", "tag", "notifier", "line", "severity", "sessionid", "type", "version", "message", "schema", "lid", "uniqueid", "occurrences", "service", "context", "func", "uuid", "instance", "identifier", "zone", "groups", "count");
    model_internal static var derivedProperties:Array = new Array();
    model_internal static var collectionProperties:Array = new Array();
    model_internal static var collectionBaseMap:Object;
    model_internal static var entityName:String = "Rows";
    model_internal static var dependentsOnMap:Object;
    model_internal static var dependedOnServices:Array = new Array();
    model_internal static var propertyTypeMap:Object;

    
    model_internal var _categoryIsValid:Boolean;
    model_internal var _categoryValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _categoryIsValidCacheInitialized:Boolean = false;
    model_internal var _categoryValidationFailureMessages:Array;
    
    model_internal var _moduleIsValid:Boolean;
    model_internal var _moduleValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _moduleIsValidCacheInitialized:Boolean = false;
    model_internal var _moduleValidationFailureMessages:Array;
    
    model_internal var _lineIsValid:Boolean;
    model_internal var _lineValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _lineIsValidCacheInitialized:Boolean = false;
    model_internal var _lineValidationFailureMessages:Array;
    
    model_internal var _funcIsValid:Boolean;
    model_internal var _funcValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _funcIsValidCacheInitialized:Boolean = false;
    model_internal var _funcValidationFailureMessages:Array;
    
    model_internal var _countIsValid:Boolean;
    model_internal var _countValidator:com.adobe.fiber.styles.StyleValidator;
    model_internal var _countIsValidCacheInitialized:Boolean = false;
    model_internal var _countValidationFailureMessages:Array;

    model_internal var _instance:_Super_Rows;
    model_internal static var _nullStyle:com.adobe.fiber.styles.Style = new com.adobe.fiber.styles.Style();

    public function _RowsEntityMetadata(value : _Super_Rows)
    {
        // initialize property maps
        if (model_internal::dependentsOnMap == null)
        {
            // dependents map
            model_internal::dependentsOnMap = new Object();
            model_internal::dependentsOnMap["category"] = new Array();
            model_internal::dependentsOnMap["module"] = new Array();
            model_internal::dependentsOnMap["dateTime"] = new Array();
            model_internal::dependentsOnMap["tag"] = new Array();
            model_internal::dependentsOnMap["notifier"] = new Array();
            model_internal::dependentsOnMap["line"] = new Array();
            model_internal::dependentsOnMap["severity"] = new Array();
            model_internal::dependentsOnMap["sessionid"] = new Array();
            model_internal::dependentsOnMap["type"] = new Array();
            model_internal::dependentsOnMap["version"] = new Array();
            model_internal::dependentsOnMap["message"] = new Array();
            model_internal::dependentsOnMap["schema"] = new Array();
            model_internal::dependentsOnMap["lid"] = new Array();
            model_internal::dependentsOnMap["uniqueid"] = new Array();
            model_internal::dependentsOnMap["occurrences"] = new Array();
            model_internal::dependentsOnMap["service"] = new Array();
            model_internal::dependentsOnMap["context"] = new Array();
            model_internal::dependentsOnMap["func"] = new Array();
            model_internal::dependentsOnMap["uuid"] = new Array();
            model_internal::dependentsOnMap["instance"] = new Array();
            model_internal::dependentsOnMap["identifier"] = new Array();
            model_internal::dependentsOnMap["zone"] = new Array();
            model_internal::dependentsOnMap["groups"] = new Array();
            model_internal::dependentsOnMap["count"] = new Array();

            // collection base map
            model_internal::collectionBaseMap = new Object();
        }

        // Property type Map
        model_internal::propertyTypeMap = new Object();
        model_internal::propertyTypeMap["category"] = "String";
        model_internal::propertyTypeMap["module"] = "String";
        model_internal::propertyTypeMap["dateTime"] = "String";
        model_internal::propertyTypeMap["tag"] = "String";
        model_internal::propertyTypeMap["notifier"] = "String";
        model_internal::propertyTypeMap["line"] = "String";
        model_internal::propertyTypeMap["severity"] = "String";
        model_internal::propertyTypeMap["sessionid"] = "String";
        model_internal::propertyTypeMap["type"] = "String";
        model_internal::propertyTypeMap["version"] = "String";
        model_internal::propertyTypeMap["message"] = "String";
        model_internal::propertyTypeMap["schema"] = "String";
        model_internal::propertyTypeMap["lid"] = "String";
        model_internal::propertyTypeMap["uniqueid"] = "String";
        model_internal::propertyTypeMap["occurrences"] = "String";
        model_internal::propertyTypeMap["service"] = "String";
        model_internal::propertyTypeMap["context"] = "String";
        model_internal::propertyTypeMap["func"] = "String";
        model_internal::propertyTypeMap["uuid"] = "String";
        model_internal::propertyTypeMap["instance"] = "String";
        model_internal::propertyTypeMap["identifier"] = "String";
        model_internal::propertyTypeMap["zone"] = "String";
        model_internal::propertyTypeMap["groups"] = "String";
        model_internal::propertyTypeMap["count"] = "String";

        model_internal::_instance = value;
        model_internal::_categoryValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCategory);
        model_internal::_categoryValidator.required = true;
        model_internal::_categoryValidator.requiredFieldError = "category is required";
        //model_internal::_categoryValidator.source = model_internal::_instance;
        //model_internal::_categoryValidator.property = "category";
        model_internal::_moduleValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForModule);
        model_internal::_moduleValidator.required = true;
        model_internal::_moduleValidator.requiredFieldError = "module is required";
        //model_internal::_moduleValidator.source = model_internal::_instance;
        //model_internal::_moduleValidator.property = "module";
        model_internal::_lineValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForLine);
        model_internal::_lineValidator.required = true;
        model_internal::_lineValidator.requiredFieldError = "line is required";
        //model_internal::_lineValidator.source = model_internal::_instance;
        //model_internal::_lineValidator.property = "line";
        model_internal::_funcValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForFunc);
        model_internal::_funcValidator.required = true;
        model_internal::_funcValidator.requiredFieldError = "func is required";
        //model_internal::_funcValidator.source = model_internal::_instance;
        //model_internal::_funcValidator.property = "func";
        model_internal::_countValidator = new StyleValidator(model_internal::_instance.model_internal::_doValidationForCount);
        model_internal::_countValidator.required = true;
        model_internal::_countValidator.requiredFieldError = "count is required";
        //model_internal::_countValidator.source = model_internal::_instance;
        //model_internal::_countValidator.property = "count";
    }

    override public function getEntityName():String
    {
        return model_internal::entityName;
    }

    override public function getProperties():Array
    {
        return model_internal::allProperties;
    }

    override public function getAssociationProperties():Array
    {
        return model_internal::allAssociationProperties;
    }

    override public function getRequiredProperties():Array
    {
         return model_internal::allRequiredProperties;   
    }

    override public function getDataProperties():Array
    {
        return model_internal::dataProperties;
    }

    public function getSourceProperties():Array
    {
        return model_internal::sourceProperties;
    }

    public function getNonDerivedProperties():Array
    {
        return model_internal::nonDerivedProperties;
    }

    override public function getGuardedProperties():Array
    {
        return model_internal::guardedProperties;
    }

    override public function getUnguardedProperties():Array
    {
        return model_internal::allAlwaysAvailableProperties;
    }

    override public function getDependants(propertyName:String):Array
    {
       if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a data property of entity Rows");
            
       return model_internal::dependentsOnMap[propertyName] as Array;  
    }

    override public function getDependedOnServices():Array
    {
        return model_internal::dependedOnServices;
    }

    override public function getCollectionProperties():Array
    {
        return model_internal::collectionProperties;
    }

    override public function getCollectionBase(propertyName:String):String
    {
        if (model_internal::collectionProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a collection property of entity Rows");

        return model_internal::collectionBaseMap[propertyName];
    }
    
    override public function getPropertyType(propertyName:String):String
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
            throw new Error(propertyName + " is not a property of Rows");

        return model_internal::propertyTypeMap[propertyName];
    }

    override public function getAvailableProperties():com.adobe.fiber.valueobjects.IPropertyIterator
    {
        return new com.adobe.fiber.valueobjects.AvailablePropertyIterator(this);
    }

    override public function getValue(propertyName:String):*
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity Rows");
        }

        return model_internal::_instance[propertyName];
    }

    override public function setValue(propertyName:String, value:*):void
    {
        if (model_internal::nonDerivedProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " is not a modifiable property of entity Rows");
        }

        model_internal::_instance[propertyName] = value;
    }

    override public function getMappedByProperty(associationProperty:String):String
    {
        switch(associationProperty)
        {
            default:
            {
                return null;
            }
        }
    }

    override public function getPropertyLength(propertyName:String):int
    {
        switch(propertyName)
        {
            default:
            {
                return 0;
            }
        }
    }

    override public function isAvailable(propertyName:String):Boolean
    {
        if (model_internal::allProperties.indexOf(propertyName) == -1)
        {
            throw new Error(propertyName + " does not exist for entity Rows");
        }

        if (model_internal::allAlwaysAvailableProperties.indexOf(propertyName) != -1)
        {
            return true;
        }

        switch(propertyName)
        {
            default:
            {
                return true;
            }
        }
    }

    override public function getIdentityMap():Object
    {
        var returnMap:Object = new Object();

        return returnMap;
    }

    [Bindable(event="propertyChange")]
    override public function get invalidConstraints():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_invalidConstraints;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_invalidConstraints;        
        }
    }

    [Bindable(event="propertyChange")]
    override public function get validationFailureMessages():Array
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_validationFailureMessages;
        }
    }

    override public function getDependantInvalidConstraints(propertyName:String):Array
    {
        var dependants:Array = getDependants(propertyName);
        if (dependants.length == 0)
        {
            return emptyArray;
        }

        var currentlyInvalid:Array = invalidConstraints;
        if (currentlyInvalid.length == 0)
        {
            return emptyArray;
        }

        var filterFunc:Function = function(element:*, index:int, arr:Array):Boolean
        {
            return dependants.indexOf(element) > -1;
        }

        return currentlyInvalid.filter(filterFunc);
    }

    /**
     * isValid
     */
    [Bindable(event="propertyChange")] 
    public function get isValid() : Boolean
    {
        if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
        {
            return model_internal::_instance.model_internal::_isValid;
        }
        else
        {
            // recalculate isValid
            model_internal::_instance.model_internal::_isValid = model_internal::_instance.model_internal::calculateIsValid();
            return model_internal::_instance.model_internal::_isValid;
        }
    }

    [Bindable(event="propertyChange")]
    public function get isCategoryAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isModuleAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isDateTimeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTagAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isNotifierAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLineAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSeverityAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSessionidAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isTypeAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isVersionAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isMessageAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isSchemaAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isLidAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isUniqueidAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isOccurrencesAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isServiceAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isContextAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isFuncAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isUuidAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isInstanceAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isIdentifierAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isZoneAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isGroupsAvailable():Boolean
    {
        return true;
    }

    [Bindable(event="propertyChange")]
    public function get isCountAvailable():Boolean
    {
        return true;
    }


    /**
     * derived property recalculation
     */
    public function invalidateDependentOnCategory():void
    {
        if (model_internal::_categoryIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCategory = null;
            model_internal::calculateCategoryIsValid();
        }
    }
    public function invalidateDependentOnModule():void
    {
        if (model_internal::_moduleIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfModule = null;
            model_internal::calculateModuleIsValid();
        }
    }
    public function invalidateDependentOnLine():void
    {
        if (model_internal::_lineIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfLine = null;
            model_internal::calculateLineIsValid();
        }
    }
    public function invalidateDependentOnFunc():void
    {
        if (model_internal::_funcIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfFunc = null;
            model_internal::calculateFuncIsValid();
        }
    }
    public function invalidateDependentOnCount():void
    {
        if (model_internal::_countIsValidCacheInitialized )
        {
            model_internal::_instance.model_internal::_doValidationCacheOfCount = null;
            model_internal::calculateCountIsValid();
        }
    }

    model_internal function fireChangeEvent(propertyName:String, oldValue:Object, newValue:Object):void
    {
        this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, propertyName, oldValue, newValue));
    }

    [Bindable(event="propertyChange")]   
    public function get categoryStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get categoryValidator() : StyleValidator
    {
        return model_internal::_categoryValidator;
    }

    model_internal function set _categoryIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_categoryIsValid;         
        if (oldValue !== value)
        {
            model_internal::_categoryIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "categoryIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get categoryIsValid():Boolean
    {
        if (!model_internal::_categoryIsValidCacheInitialized)
        {
            model_internal::calculateCategoryIsValid();
        }

        return model_internal::_categoryIsValid;
    }

    model_internal function calculateCategoryIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_categoryValidator.validate(model_internal::_instance.category)
        model_internal::_categoryIsValid_der = (valRes.results == null);
        model_internal::_categoryIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::categoryValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::categoryValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get categoryValidationFailureMessages():Array
    {
        if (model_internal::_categoryValidationFailureMessages == null)
            model_internal::calculateCategoryIsValid();

        return _categoryValidationFailureMessages;
    }

    model_internal function set categoryValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_categoryValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_categoryValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "categoryValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get moduleStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get moduleValidator() : StyleValidator
    {
        return model_internal::_moduleValidator;
    }

    model_internal function set _moduleIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_moduleIsValid;         
        if (oldValue !== value)
        {
            model_internal::_moduleIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "moduleIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get moduleIsValid():Boolean
    {
        if (!model_internal::_moduleIsValidCacheInitialized)
        {
            model_internal::calculateModuleIsValid();
        }

        return model_internal::_moduleIsValid;
    }

    model_internal function calculateModuleIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_moduleValidator.validate(model_internal::_instance.module)
        model_internal::_moduleIsValid_der = (valRes.results == null);
        model_internal::_moduleIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::moduleValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::moduleValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get moduleValidationFailureMessages():Array
    {
        if (model_internal::_moduleValidationFailureMessages == null)
            model_internal::calculateModuleIsValid();

        return _moduleValidationFailureMessages;
    }

    model_internal function set moduleValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_moduleValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_moduleValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "moduleValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get dateTimeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get tagStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get notifierStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get lineStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get lineValidator() : StyleValidator
    {
        return model_internal::_lineValidator;
    }

    model_internal function set _lineIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_lineIsValid;         
        if (oldValue !== value)
        {
            model_internal::_lineIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "lineIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get lineIsValid():Boolean
    {
        if (!model_internal::_lineIsValidCacheInitialized)
        {
            model_internal::calculateLineIsValid();
        }

        return model_internal::_lineIsValid;
    }

    model_internal function calculateLineIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_lineValidator.validate(model_internal::_instance.line)
        model_internal::_lineIsValid_der = (valRes.results == null);
        model_internal::_lineIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::lineValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::lineValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get lineValidationFailureMessages():Array
    {
        if (model_internal::_lineValidationFailureMessages == null)
            model_internal::calculateLineIsValid();

        return _lineValidationFailureMessages;
    }

    model_internal function set lineValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_lineValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_lineValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "lineValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get severityStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get sessionidStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get typeStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get versionStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get messageStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get schemaStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get lidStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get uniqueidStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get occurrencesStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get serviceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get contextStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get funcStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get funcValidator() : StyleValidator
    {
        return model_internal::_funcValidator;
    }

    model_internal function set _funcIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_funcIsValid;         
        if (oldValue !== value)
        {
            model_internal::_funcIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "funcIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get funcIsValid():Boolean
    {
        if (!model_internal::_funcIsValidCacheInitialized)
        {
            model_internal::calculateFuncIsValid();
        }

        return model_internal::_funcIsValid;
    }

    model_internal function calculateFuncIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_funcValidator.validate(model_internal::_instance.func)
        model_internal::_funcIsValid_der = (valRes.results == null);
        model_internal::_funcIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::funcValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::funcValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get funcValidationFailureMessages():Array
    {
        if (model_internal::_funcValidationFailureMessages == null)
            model_internal::calculateFuncIsValid();

        return _funcValidationFailureMessages;
    }

    model_internal function set funcValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_funcValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_funcValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "funcValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }

    [Bindable(event="propertyChange")]   
    public function get uuidStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get instanceStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get identifierStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get zoneStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get groupsStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    [Bindable(event="propertyChange")]   
    public function get countStyle():com.adobe.fiber.styles.Style
    {
        return model_internal::_nullStyle;
    }

    public function get countValidator() : StyleValidator
    {
        return model_internal::_countValidator;
    }

    model_internal function set _countIsValid_der(value:Boolean):void 
    {
        var oldValue:Boolean = model_internal::_countIsValid;         
        if (oldValue !== value)
        {
            model_internal::_countIsValid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "countIsValid", oldValue, value));
        }                             
    }

    [Bindable(event="propertyChange")]
    public function get countIsValid():Boolean
    {
        if (!model_internal::_countIsValidCacheInitialized)
        {
            model_internal::calculateCountIsValid();
        }

        return model_internal::_countIsValid;
    }

    model_internal function calculateCountIsValid():void
    {
        var valRes:ValidationResultEvent = model_internal::_countValidator.validate(model_internal::_instance.count)
        model_internal::_countIsValid_der = (valRes.results == null);
        model_internal::_countIsValidCacheInitialized = true;
        if (valRes.results == null)
             model_internal::countValidationFailureMessages_der = emptyArray;
        else
        {
            var _valFailures:Array = new Array();
            for (var a:int = 0 ; a<valRes.results.length ; a++)
            {
                _valFailures.push(valRes.results[a].errorMessage);
            }
            model_internal::countValidationFailureMessages_der = _valFailures;
        }
    }

    [Bindable(event="propertyChange")]
    public function get countValidationFailureMessages():Array
    {
        if (model_internal::_countValidationFailureMessages == null)
            model_internal::calculateCountIsValid();

        return _countValidationFailureMessages;
    }

    model_internal function set countValidationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_countValidationFailureMessages;

        var needUpdate : Boolean = false;
        if (oldValue == null)
            needUpdate = true;
    
        // avoid firing the event when old and new value are different empty arrays
        if (!needUpdate && (oldValue !== value && (oldValue.length > 0 || value.length > 0)))
        {
            if (oldValue.length == value.length)
            {
                for (var a:int=0; a < oldValue.length; a++)
                {
                    if (oldValue[a] !== value[a])
                    {
                        needUpdate = true;
                        break;
                    }
                }
            }
            else
            {
                needUpdate = true;
            }
        }

        if (needUpdate)
        {
            model_internal::_countValidationFailureMessages = value;   
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "countValidationFailureMessages", oldValue, value));
            // Only execute calculateIsValid if it has been called before, to update the validationFailureMessages for
            // the entire entity.
            if (model_internal::_instance.model_internal::_cacheInitialized_isValid)
            {
                model_internal::_instance.model_internal::isValid_der = model_internal::_instance.model_internal::calculateIsValid();
            }
        }
    }


     /**
     * 
     * @inheritDoc 
     */ 
     override public function getStyle(propertyName:String):com.adobe.fiber.styles.IStyle
     {
         switch(propertyName)
         {
            default:
            {
                return null;
            }
         }
     }
     
     /**
     * 
     * @inheritDoc 
     *  
     */  
     override public function getPropertyValidationFailureMessages(propertyName:String):Array
     {
         switch(propertyName)
         {
            case("category"):
            {
                return categoryValidationFailureMessages;
            }
            case("module"):
            {
                return moduleValidationFailureMessages;
            }
            case("line"):
            {
                return lineValidationFailureMessages;
            }
            case("func"):
            {
                return funcValidationFailureMessages;
            }
            case("count"):
            {
                return countValidationFailureMessages;
            }
            default:
            {
                return emptyArray;
            }
         }
     }

}

}
