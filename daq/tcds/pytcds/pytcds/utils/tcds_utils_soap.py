###############################################################################
## Utilities to help with SOAP messages.
###############################################################################

import httplib
import sys
import urllib2
from xml.dom import minidom

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# All kinds of constants related to SOAP messages and formats.
SOAP_PROTOCOL_VERSION_1_1 = "soap11"
SOAP_PROTOCOL_VERSION_1_2 = "soap12"
SOAP_PROTOCOL_VERSION_DEFAULT = SOAP_PROTOCOL_VERSION_1_1

SOAP_CONTENT_TYPE_1_1 = "text/xml"
SOAP_CONTENT_TYPE_1_2 = "application/soap+xml"
URI_NS_SOAP_ENVELOPE_1_1 = "http://schemas.xmlsoap.org/soap/envelope/"
URI_NS_SOAP_ENVELOPE_1_2 = "http://www.w3.org/2003/05/soap-envelope"
SOAP_ENV_PREFIX = "env"

SOAP_CONTENT_TYPE_BY_VERSION = {
    SOAP_PROTOCOL_VERSION_1_1 : SOAP_CONTENT_TYPE_1_1,
    SOAP_PROTOCOL_VERSION_1_2 : SOAP_CONTENT_TYPE_1_2
}
URI_NS_SOAP_ENVELOPE_BY_VERSION = {
    SOAP_PROTOCOL_VERSION_1_1 : URI_NS_SOAP_ENVELOPE_1_1,
    SOAP_PROTOCOL_VERSION_1_2 : URI_NS_SOAP_ENVELOPE_1_2
}
SOAP_ENVELOPE_PREFIX_BY_VERSION = {
    SOAP_PROTOCOL_VERSION_1_1 : SOAP_ENV_PREFIX,
    SOAP_PROTOCOL_VERSION_1_2 : SOAP_ENV_PREFIX
}

URI_NS_XSD = "http://www.w3.org/2001/XMLSchema"
URI_NS_XSI = "http://www.w3.org/2001/XMLSchema-instance"

SOAP_PREFIX_XDAQ = "xdaq"
URI_NS_XDAQ = "urn:xdaq-soap:3.0"

###############################################################################

def build_xdaq_soap_parameterget_message(class_name,
                                         par_name,
                                         par_type,
                                         soap_protocol_version=None):

    if soap_protocol_version is None:
        soap_protocol_version = SOAP_PROTOCOL_VERSION_DEFAULT

    uri_ns_soap_envelope = URI_NS_SOAP_ENVELOPE_BY_VERSION[soap_protocol_version]
    soap_envelope_prefix = SOAP_ENVELOPE_PREFIX_BY_VERSION[soap_protocol_version]

    # Create the main DOM document.
    doc = minidom.Document()

    # Create and attach the SOAP envelope.
    envelope = doc.createElement("{0:s}:Envelope".format(soap_envelope_prefix))
    envelope.setAttribute("xmlns:{0:s}".format(soap_envelope_prefix),
                          uri_ns_soap_envelope)
    envelope.setAttribute("xmlns:xsd", URI_NS_XSD)
    envelope.setAttribute("xmlns:xsi", URI_NS_XSI)
    doc.appendChild(envelope)

    # Create the header and attach it to the envelope.
    header = doc.createElement("{0:s}:Header".format(soap_envelope_prefix))
    envelope.appendChild(header)

    # Create the SOAP body and attach it to the envelope.
    body = doc.createElement("{0:s}:Body".format(soap_envelope_prefix))
    envelope.appendChild(body)

    # Create and attach the body element.
    cmd_element = doc.createElement("{0:s}:ParameterGet".format(SOAP_PREFIX_XDAQ))
    cmd_element.setAttribute("xmlns:{0:s}".format(SOAP_PREFIX_XDAQ),
                             URI_NS_XDAQ)
    body.appendChild(cmd_element)

    # Append the 'properties' element.
    properties_element = doc.createElement("p:properties")
    uri = "urn:xdaq-application:{0:s}".format(class_name)
    properties_element.setAttribute("xmlns:p", uri)
    properties_element.setAttribute("xsi:type", "soapenc:Struct")
    cmd_element.appendChild(properties_element)

    # Attach the parameter name and type to the 'properties' element.
    param_element = doc.createElement("p:{0:s}".format(par_name))
    param_element.setAttribute("xsi:type", "xsd:{0:s}".format(par_type))
    properties_element.appendChild(param_element)

    # End of build_xdaq_soap_parameterget_message().
    return doc

###############################################################################

def build_xdaq_soap_command_message(rc_session_id,
                                    lease_owner_id,
                                    command_name,
                                    rcms_url=None,
                                    parameters=None,
                                    soap_protocol_version=None):

    if parameters is None:
        parameters = {}

    if soap_protocol_version is None:
        soap_protocol_version = SOAP_PROTOCOL_VERSION_DEFAULT

    uri_ns_soap_envelope = URI_NS_SOAP_ENVELOPE_BY_VERSION[soap_protocol_version]
    soap_envelope_prefix = SOAP_ENVELOPE_PREFIX_BY_VERSION[soap_protocol_version]

    # Create the main DOM document.
    doc = minidom.Document()

    # Create and attach the SOAP envelope.
    envelope = doc.createElement("{0:s}:Envelope".format(soap_envelope_prefix))
    envelope.setAttribute("xmlns:{0:s}".format(soap_envelope_prefix),
                          uri_ns_soap_envelope)
    envelope.setAttribute("xmlns:xsd", URI_NS_XSD)
    envelope.setAttribute("xmlns:xsi", URI_NS_XSI)
    doc.appendChild(envelope)

    # Create the header and attach it to the envelope.
    header = doc.createElement("{0:s}:Header".format(soap_envelope_prefix))
    envelope.appendChild(header)

    # Create the SOAP body and attach it to the envelope.
    body = doc.createElement("{0:s}:Body".format(soap_envelope_prefix))
    envelope.appendChild(body)

    # Create and attach the body element.
    cmd_element = doc.createElement("{0:s}:{1:s}".format(SOAP_PREFIX_XDAQ,
                                                         command_name))
    cmd_element.setAttribute("xmlns:{0:s}".format(SOAP_PREFIX_XDAQ),
                             URI_NS_XDAQ)
    body.appendChild(cmd_element)

    # Attach the RunControl session identifier as attribute of the
    # command node.
    if not rc_session_id is None:
        cmd_element.setAttribute("{0:s}:rcmsSessionId".format(SOAP_PREFIX_XDAQ),
                                 str(rc_session_id))

    # Attach the hardware lease owner identifier as attribute of the
    # command node.
    if not lease_owner_id is None:
        cmd_element.setAttribute("{0:s}:actionRequestorId".format(SOAP_PREFIX_XDAQ),
                                 lease_owner_id)

    # Attach the RCMS state notification listener URL (if given) as
    # attribute of the command node.
    if not rcms_url is None:
        cmd_element.setAttribute("{0:s}:rcmsURL".format(SOAP_PREFIX_XDAQ),
                                 rcms_url)

    # If any parameters were given, attach those to the command node.
    if len(parameters):
        # cmd_element.setAttribute("xsi:type", "soapenc:Struct")
        parameter_names = parameters.keys()
        parameter_names.sort()
        for par_name in parameter_names:
            (par_type, par_val) = parameters[par_name]
            param_element = doc.createElement("{0:s}:{1:s}".format(SOAP_PREFIX_XDAQ,
                                                                   par_name))
            param_element.setAttribute("xsi:type", "xsd:{0:s}".format(par_type))
            value_element = doc.createTextNode(str(par_val))
            param_element.appendChild(value_element)
            cmd_element.appendChild(param_element)

    # End of build_xdaq_soap_command_message().
    return doc

###############################################################################

def send_xdaq_soap_message(host, port, lid, msg, soap_protocol_version=None, verbose=False):

    if soap_protocol_version is None:
        soap_protocol_version = SOAP_PROTOCOL_VERSION_DEFAULT

    if verbose:
        print "Sending SOAP message:"
        print msg.toprettyxml()

    soap_message_txt = msg.toxml()
    headers = {
        "Content-Location" : "urn:xdaq-application:lid={0:d}".format(lid),
        "Content-type" : SOAP_CONTENT_TYPE_BY_VERSION[soap_protocol_version],
        "Content-length" :len(soap_message_txt)
    }
    url = "http://{0:s}:{1:d}".format(host, port)
    response_tmp = send_message(url, soap_message_txt, headers, verbose)

    response = minidom.parseString(response_tmp)
    if verbose:
        print "Received SOAP response:"
        print response.toprettyxml()

    # End of send_xdaq_soap_message().
    return response

###############################################################################

def send_message(url, content=None, headers={}, verbose=False):

    # In verbose mode: install a custom HTTP handler with increased
    # debuglevel.
    # if verbose:
    #     handler = urllib2.HTTPHandler(debuglevel=1)
    #     opener = urllib2.build_opener(handler)
    #     urllib2.install_opener(opener)

    req = urllib2.Request(url, content, headers)
    response = None

    try:
        reply = urllib2.urlopen(req)
        response = reply.read()
    except httplib.BadStatusLine, err:
        msg = "It looks like the application crashed before responding."
        print >> sys.stderr, msg
        sys.exit(1)
    except urllib2.HTTPError, err:
        msg = "Problem sending message to {0:s}, " \
              "the server could not fullfill the request. " \
              "Error code: {1:d}"
        print >> sys.stderr, msg.format(url, err.code)
        sys.exit(1)
    except urllib2.URLError, err:
        msg =  "Problem sending message to {0:s}, " \
               "failed to reach the server: '{1:s}'."
        print >> sys.stderr, msg.format(url, str(err.reason))
        sys.exit(1)

    # End of send_message().
    return response

###############################################################################

def find_child_node_by_name(parent_node, child_node_name, case_insensitive=False):
    res = None
    if parent_node:
        child_node_name_tmp = child_node_name
        if case_insensitive:
            child_node_name_tmp = child_node_name_tmp.lower()
        for node in parent_node.childNodes:
            tmp = node.nodeName
            index = tmp.find(":")
            if index > -1:
                tmp = tmp[index + 1:]
            if case_insensitive:
                tmp = tmp.lower()
            if tmp == child_node_name_tmp:
                res = node
                break
    # End of find_child_node_by_name().
    return res

###############################################################################

def extract_xdaq_parameterget_result(soap_response, par_name):
    # Note: Some of this is a bit of a hack. But we somehow have to
    # recognize both SOAP v1.1 and v1.2.

    envelope = find_child_node_by_name(soap_response, "envelope", True)
    body = find_child_node_by_name(envelope, "body", True)
    response_element = find_child_node_by_name(body, "parametergetresponse", True)
    properties_element = find_child_node_by_name(response_element, "properties", True)
    par_element = find_child_node_by_name(properties_element, par_name, True)
    par_val = par_element.firstChild.nodeValue

    # End of extract_xdaq_parameterget_result().
    return par_val

###############################################################################

def extract_xdaq_soap_fault(soap_response):
    # Note: Some of this is a bit of a hack. But we somehow have to
    # recognize both SOAP v1.1 and v1.2.

    error_msg = None
    envelope = find_child_node_by_name(soap_response, "envelope", True)
    body = find_child_node_by_name(envelope, "body", True)
    fault = find_child_node_by_name(body, "fault", True)
    if fault:
        error_msg = None

        #----------
        # The fault string itself.
        #----------
        # This should work for SOAP v1.1.
        reason = find_child_node_by_name(fault, "faultstring", True)
        if reason:
            error_msg = reason.firstChild.nodeValue
        else:
            # This should work for SOAP v1.2.
            reason = find_child_node_by_name(fault, "reason", True)
            if reason:
                error_msg = reason.firstChild.firstChild.nodeValue

        #----------
        # The fault detail, if it exists.
        #----------
        # This should work for SOAP v1.1.
        detail = find_child_node_by_name(fault, "detail", True)
        if detail and detail.firstChild.nodeValue:
            error_msg = "{0:s} {1:s}".format(error_msg, detail.firstChild.nodeValue)
        else:
            # This should work for SOAP v1.2.
            detail = find_child_node_by_name(fault, "Detail", True)
            if detail and detail.firstChild.nodeValue:
                error_msg = "{0:s} {1:s}".format(error_msg, detail.firstChild.firstChild.nodeValue)

    # End of extract_xdaq_soap_fault().
    return error_msg

###############################################################################
