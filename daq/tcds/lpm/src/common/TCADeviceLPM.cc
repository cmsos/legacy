#include "tcds/lpm/TCADeviceLPM.h"

#include <algorithm>
#include <cassert>

#include "toolbox/string.h"

#include "tcds/hwlayertca/BootstrapHelper.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierFC7.h"
#include "tcds/lpm/Utils.h"

tcds::lpm::TCADeviceLPM::TCADeviceLPM() :
  tcds::pm::TCADevicePMCommonBase(std::auto_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierFC7(hwDevice_)))
{
}

tcds::lpm::TCADeviceLPM::~TCADeviceLPM()
{
}

std::string
tcds::lpm::TCADeviceLPM::regNamePrefixImpl() const
{
  // All the LPM registers start with 'ipm.'.
  return "ipm";
}

bool
tcds::lpm::TCADeviceLPM::bootstrapDoneImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  return h.bootstrapDone();
}

void
tcds::lpm::TCADeviceLPM::runBootstrapImpl() const
{
  tcds::hwlayertca::BootstrapHelper h(*this);
  h.runBootstrap();
  // Reset the backplane link to the CPM.
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x1);
  hwDevice_.writeRegister("lpm_main.backplane_tts_control.gtx_reset", 0x0);
}

void
tcds::lpm::TCADeviceLPM::configureTriggerAlignment() const
{
  // This one is not really trigger alignment, but it aligns the BX
  // number reported for each trigger in the DAQ event record.
  std::string regName = "main.orbit_alignment_config.daq_record_adjust";
  writeRegister(regName, 0xd6e);

  //----------

  // BX-alignment for cyclic triggers.
  regName = "main.trigger_delay.cyclic_trigger_adjust";
  writeRegister(regName, 0x7a);

  // BX-alignment for the bunch-mask.
  regName = "main.trigger_delay.bunch_mask_adjust";
  writeRegister(regName, 0x7b);

  // BX-alignment for the sequence triggers.
  regName = "bgo_trains.bgo_train_config.calib_trigger_adjust";
  writeRegister(regName, 0x7b);
}

void
tcds::lpm::TCADeviceLPM::configureExternalTriggerSettings() const
{
  // NOTE: This is a bit of a gimmick. Since the software does not
  // really have access to any registers above ipm.*, and the external
  // trigger input/polarity selection is done in lpm_main.control, the
  // software writes several flags in ipm.main. This method reads
  // these flags and propagates these settings to lpm_main.control.

  for (int i = 0; i != 2; ++i)
    {
      std::string const trigNum = toolbox::toString("%d", i);

      // Trigger source.
      uint32_t const tmpSrc =
        readRegister("main.lpm_external_trigger" + trigNum + "_source_mode");
      tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE const srcMode =
        static_cast<tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE>(tmpSrc);
      if (srcMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_NIM)
        {
          hwDevice_.writeRegister("lpm_main.control.external_l1a_" + trigNum + "_source", 0x0);
          hwDevice_.writeRegister("lpm_main.control.invert_external_l1a_" + trigNum, 0x0);
        }
      else if (srcMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_TTL)
        {
          hwDevice_.writeRegister("lpm_main.control.external_l1a_" + trigNum + "_source", 0x1);
          hwDevice_.writeRegister("lpm_main.control.invert_external_l1a_" + trigNum, 0x1);
        }
      else
        {
          // ASSERT ASSERT ASSERT
          assert (false);
          // ASSERT ASSERT ASSERT end
        }

      // Trigger polarity inversion flag.
      uint32_t const tmpPol =
        readRegister("main.lpm_external_trigger" + trigNum + "_inversion_mode");
      hwDevice_.writeRegister("lpm_main.control.invert_external_l1a_" + trigNum, tmpPol);
    }
}

tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE
tcds::lpm::TCADeviceLPM::readExternalTriggerSourceMode(uint32_t const trigNum) const
{
  std::string const regName =
    toolbox::toString("lpm_main.control.external_l1a_%d_source", trigNum);
  uint32_t const tmp = hwDevice_.readRegister(regName);
  return static_cast<tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE>(tmp);
}

tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE
tcds::lpm::TCADeviceLPM::readExternalTriggerInversionMode(uint32_t const trigNum) const
{
  std::string const regName =
    toolbox::toString("lpm_main.control.invert_external_l1a_%d", trigNum);
  uint32_t const tmp =  hwDevice_.readRegister(regName);
  return static_cast<tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE>(tmp);
}

tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE
tcds::lpm::TCADeviceLPM::readExternalTriggerLogicMode(uint32_t const trigNum) const
{
  tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE res;

  tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE srcMode =
    readExternalTriggerSourceMode(trigNum);
  tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE invMode =
    readExternalTriggerInversionMode(trigNum);

  if (srcMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_NIM)
    {
      if (invMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_OFF)
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_LO;
        }
      else
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_HI;
        }
    }
  else
    {
      if (invMode == tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_OFF)
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_HI;
        }
      else
        {
          res = tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_ACTIVE_LO;
        }
    }

  return res;
}
