/**
 *      @file FMMTransitionAnalyzer.cc
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.6 $
 *     $Date: 2007/03/28 12:06:23 $
 *
 *
 **/
#include "tts/fmmcontroller/FMMTransitionAnalyzer.hh"
#include "tts/fmmcontroller/FMMHistoryConsumer.hh"

#include "tts/fmm/FMMTimer.hh"
#include "toolbox/string.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <unistd.h>

#include "stdlib.h"

tts::FMMTransitionAnalyzer::FMMTransitionAnalyzer(log4cplus::Logger* logger)
  : _previous_hi(), _countersSema(toolbox::BSem::FULL,true) {

  _transitionCounters.resize(20);
  _transitionCountersOld.resize(20);
  _transitionFrequencies.resize(20);
  _timesPerState.resize(20);

  for (uint32_t i=0; i < (uint32_t) _transitionCounters.size() ; i++) {
    _transitionCounters[i].resize(16);
    _transitionCountersOld[i].resize(16);
    _transitionFrequencies[i].resize(16);
    _timesPerState[i].resize(16);
    for (uint32_t j=0; j < (uint32_t) _transitionCounters[i].size() ; j++) {
      _transitionCounters[i][j] = 0;
      _transitionCountersOld[i][j] = 0;
      _transitionFrequencies[i][j] = 0.;
      _timesPerState[i][j] = 0;
    }
  }

  _timestamp_lasttransition.resize(20);
  for (uint32_t j=0; j < (uint32_t) _timestamp_lasttransition.size(); j++) 
    _timestamp_lasttransition[j]=0;

  _t_lastupdate = tts::FMMTimer::getMicroTime();;
};

tts::FMMTransitionAnalyzer::~FMMTransitionAnalyzer() {

};

void tts::FMMTransitionAnalyzer::beforeMonitorStart() {

  std::vector<tts::TTSState> ready20(20, tts::TTSState::READY);
 
  // set the previous states to ready.
  // with the current trick of starting the FMMHistoryMonitor, 
  // any transition to a state other than ready will create an FMMHistoryItem.
  //
  _previous_hi = FMMHistoryItem( 0, ready20, false );

  _countersSema.take();


  for (uint32_t i=0; i < (uint32_t) _transitionCounters.size() ; i++) {
    for (uint32_t j=0; j < (uint32_t) _transitionCounters[i].size() ; j++) {
      _transitionCounters[i][j] = 0;
      _transitionCountersOld[i][j] = 0;
      _transitionFrequencies[i][j] = 0.;
      _timesPerState[i][j] = 0;
    }
  }

  for (uint32_t j=0; j < (uint32_t) _timestamp_lasttransition.size(); j++) 
    _timestamp_lasttransition[j]=0;

  _t_lastupdate = tts::FMMTimer::getMicroTime();;
  
  _countersSema.give();

};

void tts::FMMTransitionAnalyzer::afterMonitorStop( uint64_t extendedTimeStamp ) {

  std::vector<tts::TTSState> previous_states = _previous_hi.getInputStates();

  _countersSema.take();

  // add time between last transition and stop of run to the time counters
  for (uint32_t i=0; i < (uint32_t) previous_states.size(); ++i) {
    
    _timesPerState[i][ previous_states[i] ] += extendedTimeStamp - _previous_hi.getExtendedTimestamp();

  }  

  _countersSema.give();

};


void tts::FMMTransitionAnalyzer::processItem( tts::FMMHistoryItem const& hi, uint32_t address ) {

  std::vector<tts::TTSState> current_states = hi.getInputStates();
  std::vector<tts::TTSState> previous_states = _previous_hi.getInputStates();


  _countersSema.take();

  if (hi.getExtendedTimestamp() != (uint64_t) 0) {

    for (uint32_t i=0; i < (uint32_t) current_states.size(); ++i) {
      
      if (current_states[i] != previous_states[i]) {	
	_transitionCounters[i][ current_states[i] ]++;
	_timesPerState[i][ previous_states[i] ] += hi.getExtendedTimestamp() - _timestamp_lasttransition[i];
	_timestamp_lasttransition[i] = hi.getExtendedTimestamp();
      }
    }

  }

  _countersSema.give();

  _previous_hi = hi;
} 


std::vector<std::vector<uint64_t> > tts::FMMTransitionAnalyzer::getTransitionCounters() {

  _countersSema.take();
  
  std::vector<std::vector<uint64_t> > tmp = _transitionCounters;

  _countersSema.give();

  return tmp;
}

std::vector<std::vector<uint64_t> > tts::FMMTransitionAnalyzer::getTimesPerState() {

  _countersSema.take();
  
  std::vector<std::vector<uint64_t> > tmp = _timesPerState;

  _countersSema.give();

  return tmp;
}

uint64_t tts::FMMTransitionAnalyzer::getTransitionCounter(uint32_t io, tts::TTSState state) {

  _countersSema.take();
  
  uint64_t tmp = _transitionCounters[io][state];

  _countersSema.give();

  return tmp;
}

double tts::FMMTransitionAnalyzer::getTransitionFrequency(uint32_t io, tts::TTSState state) {

  _countersSema.take();
  
  double tmp = _transitionFrequencies[io][state];

  _countersSema.give();

  return tmp;
}

uint64_t tts::FMMTransitionAnalyzer::getTimeInState(uint32_t io, tts::TTSState state) {

  _countersSema.take();
  
  uint64_t tmp = _timesPerState[io][state];

  _countersSema.give();

  return tmp;
}

uint64_t tts::FMMTransitionAnalyzer::getTimeInStateCorrected(uint32_t io, tts::TTSState state, uint64_t timestamp) {

  _countersSema.take();
  
  uint64_t tmp = _timesPerState[io][state];
  if (_previous_hi.getInputStates()[io] == state && (timestamp > _timestamp_lasttransition[io]) )
    tmp += timestamp - _timestamp_lasttransition[io];

  _countersSema.give();

  return tmp;
}

void tts::FMMTransitionAnalyzer::lock() {
  _countersSema.take();
}

void tts::FMMTransitionAnalyzer::unlock() {
  _countersSema.give();
}

void tts::FMMTransitionAnalyzer::updateTransitionFrequencies() {

  uint64_t t_now = tts::FMMTimer::getMicroTime();

  double deltaT = ( t_now - _t_lastupdate ) / 1.e6;
  _t_lastupdate = t_now;

  _countersSema.take();
  
  for (uint32_t i=0; i < (uint32_t) _transitionCounters.size() ; i++) {
    for (uint32_t j=0; j < (uint32_t) _transitionCounters[i].size() ; j++) {

      _transitionFrequencies[i][j] = (_transitionCounters[i][j] - _transitionCountersOld[i][j]) / deltaT;

    }
  }

  _transitionCountersOld = _transitionCounters;
  _countersSema.give();
}
