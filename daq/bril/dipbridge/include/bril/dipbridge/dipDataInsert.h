#ifndef _bril_dipbridge_dipDataInsert_h
#define _bril_dipbridge_dipDataInsert_h
#include "dip/DipData.h"
#include "xdata/Serializable.h"
#include "xdata/Table.h"
#include <string>

class DipFactory;
class DipPublication;
class DipPublicationErrorHandler;

namespace bril{
  namespace dipbridge{
    /**
     *  Insert dipdata of topic
     */
    class dipDataInsert{
    public:
      //constructor
      dipDataInsert( DipFactory* dip, DipPublicationErrorHandler* errHandler, const std::string& topicname );
      //destructor
      ~dipDataInsert();
      DipFactory* dipFactory();
      //topic name
      std::string topicname() const;      
      //publish xdata::Table to dip
      void sendtable( xdata::Table& dipmessage );
    private:
      //insert field
      void insert( const std::string& fieldname, xdata::Serializable* fieldvalue );           
    private:
      DipFactory* pm_dipfactory;
      std::string m_topicname;  
      DipData* pm_dipdata;
      DipPublication* pm_dippub;
    };
    
}}
#endif
