#include "tcds/pi/HwStatusInfoSpaceUpdater.h"

#include <string>

#include "tcds/pi/TCADevicePI.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::pi::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                             tcds::pi::TCADevicePI const& hw) :
  tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA(xdaqApp, hw),
  hw_(hw)
{
}

tcds::pi::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

tcds::pi::TCADevicePI const&
tcds::pi::HwStatusInfoSpaceUpdater::getHw() const
{
  return hw_;
}
