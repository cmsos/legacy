// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/


#ifndef _pt_blit_EventHandler_h_
#define _pt_blit_EventHandler_h_

#include "tcpla/EventHandler.h"
#include "tcpla/Event.h"

#include <iostream>
#include <string>
#include <streambuf>
#include <istream>

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdaq/Object.h"

#include "pt/blit/Dispatcher.h"

namespace pt
{
	namespace blit
	{
		class PeerTransport;

		class EventHandler: public tcpla::EventHandler,  public xdaq::Object
		{
			public:
				EventHandler ( xdaq::Application * owner, size_t maxClients , size_t maxPSPs );

				virtual ~EventHandler ();

				void handleEvent ( tcpla::Event & e );
				void setDispatcher ( int index, pt::blit::Dispatcher * d  );
				pt::blit::Dispatcher *  getDispatcher ( int index  );

				std::string toHTML ();

			protected:

				size_t eventCounter_[tcpla::MAX_TCPLA_EVENT_NUMBER];
				std::vector<pt::blit::Dispatcher*> dispatcher_;

		};

	}
}

#endif
