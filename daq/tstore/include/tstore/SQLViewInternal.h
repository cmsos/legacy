/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_SQLViewInternal_h_
#define _tstore_SQLViewInternal_h_


#include <string>
#include <list>
#include <vector>

#include <set>
#include "xoap/DOMParser.h"
#include "xoap/domutils.h"
#include "tstore/SQLDataManipulation.h" //for toolbox::toupper, which is looking for a good home
#include "tstore/exception/Exception.h"
#include "tstore/TStoreAPI.h"
#include "tstore/View.h" //for parameterList

namespace tstore 
{

	//this is just a container for a set of column names for now,
	//but perhaps columns will gain some extra properties eventually.
	class ViewTable { 
		public:
			ViewTable(DOMNode *node);
			bool isColumnNode(const DOMNode *node);
			void addColumn(std::string column) {
				columns_.insert( column );
			}
			const std::set<std::string> &columns() {
				return columns_;
			}
			std::string tableName() const {
				return name_;
			}
			void setTableName(std::string name) {
				name_= toolbox::toupper(name);
			}
		protected:
			std::set<std::string> columns_;
			std::string name_;
	};

	class SQLViewInternal {
		public:
		SQLViewInternal(const std::string &name) : name_(name) {}
		SQLViewInternal() {}
		//standard View interface
		virtual ~SQLViewInternal() {}
		virtual bool canQuery() const;
		virtual bool canUpdate() const;
		virtual bool canInsert() const;
		virtual bool canCreate() const;
		virtual bool canRemove() const;
		virtual bool canDestroy() const;

		virtual void create(TStoreAPI *API) ;
	    virtual void query(TStoreAPI *API) ;
	    virtual void update(TStoreAPI *API) ;
	    virtual void insert(TStoreAPI *API) ;
	    virtual void definition(TStoreAPI *API) ;
	    virtual void clear(TStoreAPI *API) ;
	    virtual void remove(TStoreAPI *API) ;
		virtual void destroy(TStoreAPI *API) ;
		virtual void setParameter(const std::string &parameterName,const std::string &value) ;
		virtual void resetParameters();

		void setQuery(const std::string query);
		//returns the query string with any parameters filled in, and fills in *bindParameters with the values of any bind parameters 
		//used in the query, in the order that they appear in the query string.
		virtual const std::string queryString(std::vector<std::string> *bindParameters=NULL);

		//I suppose a more complicated update could be stored with 'parameters' named after the column names being replaced
		//by data rather than parameters passed in the SOAP message.
		//e.g. update employees set last_name=$last_name,status=$s where first_name=$first_name
		//where '$s' is a parameter (it will be the same for all rows updated in that update) but last_name and first_name come from
		//the xdata::Table. But for now we can just specify tables and columns to update.

		//the following methods are to be used for display only
		std::string rawQueryString(); //returns the query with $parameters showing, rather than replaced by their values
		std::map<const std::string,std::string> queryParameters();
		bool isBindParameter(std::string parameter);	
		std::vector<tstore::ViewTable>::iterator beginUpdateTables();
		std::vector<tstore::ViewTable>::iterator endUpdateTables();
		std::vector<tstore::ViewTable>::iterator beginInsertTables();
		std::vector<tstore::ViewTable>::iterator endInsertTables();
		std::vector<tstore::ViewTable>::reverse_iterator beginDeleteTables();
		std::vector<tstore::ViewTable>::reverse_iterator endDeleteTables();

		std::string name() { return name_; }
		void addConfiguration(const std::string &name,DOMNode *node);
		void addConfiguration(DOMNode *node,std::vector<std::string> &columns,const std::string &tableName);
		std::vector<std::string> allTables();
		protected:
		virtual void addQuery(DOMNode *node);
		virtual void addUpdate(const DOMNode *node);
		virtual void addInsert(const DOMNode *node) ;
		virtual void addCreate(DOMNode *node);

		static bool isColumnNode(const DOMNode *node);
		static bool isTableNode(const DOMNode *node);
		static bool isMappingNode(const DOMNode *node);
		void addTablesToVector(const DOMNode *node,std::vector<tstore::ViewTable> &vectorToUpdate);
		void readParameterDefinition(std::map<const std::string,std::string> &parameters,DOMNode *node);

		//adding configuration to the xml
		void addSimpleConfigurationSection(DOMNode *node,std::vector<std::string> &columns,const std::string &tableName,const std::string &sectionName);
		void addTableToNode(DOMNode *node,std::vector<std::string> &columns,const std::string &tableName);
		private:
		void addTableNamesToCollection(std::set<std::string> &allTables,const std::vector<tstore::ViewTable> &tables);
		//returns a query string based on the table and column list specified in node
		std::string createSimpleQuery(DOMNode *node,std::string &table/*,std::map<const std::string,std::string> &customTypes*/);
		void getDefinition(TStoreAPI *API,xdata::Table &results) ;
		bool tableMatchesDefinition(TStoreAPI *API,xdata::Table &input);

		std::string caseForColumnName(const std::string &columnName);
		std::string query_;
		unsigned int queryExpectedRows_;
		void addCreateStatement(std::string statement);
		void addDropStatement(std::string statement);
		std::vector<std::string> createStatements_;
		std::vector<std::string> dropStatements_;
		std::string queryTable_;
		std::vector<tstore::ViewTable> insertTables_;
		std::vector<tstore::ViewTable> updateTables_;
		std::map<const std::string,std::string> insertColumnNames_;
		parameterList queryParameters_;
		parameterList queryBindParameters_;
		//save the default values to set during the resetParameters() method
		//we could just save the location of the XML file and reload them from there, but this is easier and not prone to errors if the XML file is changed.
		parameterList defaultQueryParameters_;
		parameterList defaultQueryBindParameters_;
		static std::string prefix();
		std::string name_;
		std::string simpleQueryTableName_;
		bool getTable(TStoreAPI *API,xdata::Table &table);
		void addResultTable(TStoreAPI *API,xdata::Table &table);
	}; 


}

#endif
