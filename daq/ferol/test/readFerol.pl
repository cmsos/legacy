#!/usr/bin/perl 
use strict;

if ( $#ARGV != 3 && $#ARGV != 4 && $#ARGV != 5  ) {
    print "usage : $0 host port instance item [device [offset]]\n";
    print "        device may be one of 'ferol','frl','bridge'\n";
    exit -1;
}

my $device = "ferol";
my $host = $ARGV[0];
my $port = $ARGV[1];
my $instance = $ARGV[2];
my $item = $ARGV[3];
my $offset = 0;

$device = $ARGV[4] if ( $#ARGV >= 4 );
$offset = $ARGV[5] if ( $#ARGV == 5 );

my $cmd;

$cmd = "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\\\"http://schemas.xmlsoap.org/soap/encoding/\\\" xmlns:SOAP-ENV=\\\"http://schemas.xmlsoap.org/soap/envelope/\\\" xmlns:xsi=\\\"http://www.w3.org/2001/XMLSchema-instance\\\" xmlns:xsd=\\\"http://www.w3.org/2001/XMLSchema\\\" xmlns:SOAP-ENC=\\\"http://schemas.xmlsoap.org/soap/encoding/\\\">
  <SOAP-ENV:Header>
  </SOAP-ENV:Header>
  <SOAP-ENV:Body>
    <xdaq:ReadItem xmlns:xdaq=\\\"urn:xdaq-soap:3.0\\\" device=\\\"$device\\\" offset=\\\"$offset\\\" item=\\\"$item\\\"/>
  </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
";


#print "\n\n$cmd\n\n";
if ( $instance =~ m/^\d+$/ ) {
    getInstance ( $instance );

} elsif( $instance =~ m/^(\d+)[:-](\d+)$/ ) {
    for ( my $inst = $1; $inst <= $2; $inst++ ) {
	print "\ninstance is $inst:\n";
	getInstance ( $inst );
    }
}

sub getInstance {
    my ($inst) = @_;
#    print "send command to $host $port $inst\n";
    my $reply = `./sendCmdToApp.pl $host $port 'ferol::FerolController' $inst '$cmd'`;

#    print "\n\n$reply\n\n";
    if ( $reply =~ s/OK\n(.*)/$1/ ) {
	my $data = $1;
#	print "data : $data\n";
	printf ("0x%08x : %d\n", $data, $data);
#	while ( $data =~ m/(\d+) : ([0-9a-fA-F]+) /g ) {
#	    printf ("%2d : %4x : 0x%08x  %d", $1, $offset, $2, $2);
#	}
    } else {
	print "$reply";
    }
}
