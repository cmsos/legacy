/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * OddEvenParitioner.hpp
 *
 *  Created on: Mar 27, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/helpers/Partitioner.hpp"

namespace mpila
{

namespace helpers
{

class OddEvenParitioner : public Partitioner
{
   public:
    OddEvenParitioner(size_t commSize, PeerType startingType = PeerType::RECEIVER);

   private:
    void swapPeerType(PeerType& type);
};

} /* namespace helpers */
}
