# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

BUILD_HOME:=$(shell pwd)/../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

PROJECT_FULL_NAMESPACE=$(PROJECT_NAME)-

ifdef PROJECT_NAMESPACE
PROJECT_FULL_NAMESPACE=$(PROJECT_NAMESPACE)$(PROJECT_NAME)-
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)
#
# Adapted Makefile for use with XDAQ make system
#
Project=$(PROJECT_NAME)
Package=xpci/drv


include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules

MHOME =  $(BUILD_HOME)/$(Project)/$(Package)

ifeq ($(XDAQ_PLATFORM),x86_64_slc6)
export KBUILD_NOPEDANTIC=1
endif
ifeq ($(XDAQ_PLATFORM),x86_slc6)
export KBUILD_NOPEDANTIC=1
endif
ifeq ($(XDAQ_PLATFORM),x86_64_centos7)
export KBUILD_NOPEDANTIC=1
endif
ARCH=intel_linux

AR = ar cru
RANLIB = ranlib

SRCDIR = $(MHOME)/src/common
OBJDIR = $(MHOME)/src/linux/$(XDAQ_PLATFORM)
LIBDIR = $(MHOME)/lib/linux/$(XDAQ_PLATFORM)
DRIVER = $(MHOME)/lib/linux/$(XDAQ_PLATFORM)
INCDIR = $(MHOME)/include/$(Package)


#KERNEL_VERSION=$(shell uname -r)
KERNEL_INCLUDE=-I/lib/modules/$(KERNEL_VERSION)/build/include

KERNEL_VERSION := $(shell uname -r)
KERNEL_MAJOR   := $(shell echo $(KERNEL_VERSION) | perl -p -e 's/^(\d+\.\d+)\..*/$$1/')

#CMEM plug macro
#CMTCONFIG      := "x86_64-slc5-gcc41"
#CMTRELEASE     := "CMSOS"
#CURRENTPWD     := $(shell pwd)
#TAG            := $(shell cat $(MHOME)/version)
#RELEASE_NAME   := $(CMTRELEASE)$(TAG)
#KERNEL_VERSION := $(shell uname -r)
#

KERNELDIR ?= /lib/modules/$(shell uname -r)/build

KERNEL_INCLUDE=-I/usr/src/kernels/$(shell uname -r)/include

ifeq ($(shell uname -r),2.4.21-15.0.2.EL.cernsmp)
KERNEL_INCLUDE=-I/usr/src/linux-2.4.21-15.0.2.EL.cern/include -I/usr/lib/gcc-lib/i386-redhat-linux/3.2.3/include/
endif

ifeq ($(shell uname -r),2.6.9-55.EL.cernsmp)
KERNEL_INCLUDE=-I/usr/src/kernels/2.6.9-55.EL.cern-smp-i686/include/ -I/usr/lib/gcc-lib/i386-redhat-linux/3.2.3/include/
endif

ifeq ($(shell uname -r),2.6.18-164.6.1.el5)
KERNEL_INCLUDE=-I/usr/src/kernels/2.6.18-164.6.1.el5-x86_64/include
endif

INCLUDE = $(KERNEL_INCLUDE) -I$(INCDIR)

CFLAGS = -Wall -Wstrict-prototypes  \
         -D$(ARCH) -D_KERNEL_ -D_KERNEL -D__KERNEL


CFLAGS.intel_linux = -DPRINT_LEVEL=0 -O2

#CC = gcc $(CFLAGS) $(CFLAGS.$(ARCH))
CPLUS = g++ -static -DASSERT $(CFLAGS) $(CFLAGS.$(ARCH))
KMOD_MAJOR := 123

##
## LOCAL REDEFINITION OF RULES FOR DRIVER CREATION
##

_buildall: $(LIBDIR)/xpci

KMOD_BASE = xpci
ifeq ($(KERNEL_MAJOR), 2.6)
KMOD = $(addsuffix .ko,$(KMOD_BASE))
else
KMOD = $(addsuffix .o,$(KMOD_BASE))
endif

ifeq ($(KERNEL_MAJOR), 2.6)

# delegate to Makefile in src/common

# Use make M=dir to specify directory of external module to build
#
$(LIBDIR)/$(KMOD_BASE): src/common/xpci.c
	mkdir -p $(LIBDIR)
ifeq ($(XDAQ_PLATFORM),x86_64_slc6)
	make -C /lib/modules/$(KERNEL_VERSION)/build M=$(BUILD_HOME)/$(Project)/$(Package)/src/common \
	EXTRA_CFLAGS=-I$(BUILD_HOME)/$(Project)/$(Package)/include INCDIR0=$(BUILD_HOME)/$(Project)/$(Package)/include KMOD_MAJOR=$(KMOD_MAJOR)  modules
else ifeq ($(XDAQ_PLATFORM),x86_slc6)
	make -C /lib/modules/$(KERNEL_VERSION)/build M=$(BUILD_HOME)/$(Project)/$(Package)/src/common \
	EXTRA_CFLAGS=-I$(BUILD_HOME)/$(Project)/$(Package)/include INCDIR0=$(BUILD_HOME)/$(Project)/$(Package)/include KMOD_MAJOR=$(KMOD_MAJOR)  modules
else
	make -C /lib/modules/$(KERNEL_VERSION)/build M=$(BUILD_HOME)/$(Project)/$(Package)/src/common \
	INCDIR0=$(BUILD_HOME)/$(Project)/$(Package)/include KMOD_MAJOR=$(KMOD_MAJOR)  modules
endif
	cp src/common/$(KMOD_BASE).ko $(LIBDIR)/.
else if ($(KERNEL_MAJOR), 3.10)
$(LIBDIR)/$(KMOD_BASE): src/common/xpci.c
	mkdir -p $(LIBDIR)
	echo "CIAO"
	make -C /lib/modules/$(KERNEL_VERSION)/build M=$(BUILD_HOME)/$(Project)/$(Package)/src/common \
	EXTRA_CFLAGS=-I$(BUILD_HOME)/$(Project)/$(Package)/include INCDIR0=$(BUILD_HOME)/$(Project)/$(Package)/include KMOD_MAJOR=$(KMOD_MAJOR)  modules
	cp src/common/$(KMOD_BASE).ko $(LIBDIR)/.
else

$(LIBDIR)/$(KMOD_BASE): $(OBJDIR)/xpci.o 
	$(MakeDir) $(PackageLibDir)
	ld $(LFLAGS.$(ARCH)) -r -o $(LIBDIR)/$(KMOD_BASE) $(OBJDIR)/xpci.o 

$(OBJDIR)/xpci.o: $(SRCDIR)/xpci.c $(INCDIR)/xpci-kernel.h $(INCDIR)/xpci-types.h
	$(MakeDir) $(@D)
	$(CC)  $(CFLAGS) $(CFLAGS.$(ARCH))  -D__KERNEL__ -DMODULE $(INCLUDE) \
           -o $(OBJDIR)/xpci.o -c $(SRCDIR)/xpci.c 

endif

# FM thinks next target is obsolete. No library is created
OBJS = $(OBJDIR)/xpci.o

$(LIBDIR)/libxpci.a : $(OBJS)
	$(MakeDir) $(PackageLibDir)
	rm -f $@
	$(AR) $@ $(OBJS)
	$(RANLIB) $@

_cleanall:
	make -C /lib/modules/$(KERNEL_VERSION)/build M=$(BUILD_HOME)/$(Project)/$(Package)/src/common \
	INCDIR0=$(BUILD_HOME)/$(Project)/$(Package)/include clean 
	rm -f *~ $(OBJDIR)/*.o $(LIBDIR)/*.{so,a} $(LIBDIR)/$(KMOD_BASE).ko

load: _loadall

_loadall:
	/bin/rm -f /dev/xpci0
ifeq ($(KERNEL_MAJOR), 2.6)
	/sbin/insmod ./$(LIBDIR)/xpci
else ifeq ($(KERNEL_MAJOR), 3.10)
	/sbin/insmod ./$(LIBDIR)/xpci
else
	/sbin/insmod -v -o xpci ./$(LIBDIR)/xpci
endif
	/bin/mknod /dev/xpci0 c `grep xpci /proc/devices | cut -d ' ' -f 1` 0
	/bin/chmod a+rw /dev/xpci0
ifeq ($(KERNEL_MAJOR), 2.6)
	/bin/rm -f /etc/udev/devices/xpci0
	/bin/mknod /etc/udev/devices/xpci0 c `grep xpci /proc/devices | cut -d ' ' -f 1` 0
	/bin/chmod a+rw /etc/udev/devices/xpci0
endif
ifeq ($(KERNEL_MAJOR), 3.10)
	/bin/rm -f /etc/udev/devices/xpci0
	/bin/mknod /etc/udev/devices/xpci0 c `grep xpci /proc/devices | cut -d ' ' -f 1` 0
	/bin/chmod a+rw /etc/udev/devices/xpci0
endif

unload: _unloadall

_unloadall:
	/sbin/rmmod xpci 

reload:
	make unload 
	make load 

_releaseall:
	mkdir -p $(ReleaseDir)/$(Project)/$(Package)/$(XDAQ_OS)/$(XDAQ_PLATFORM); \
        cp -r  $(XDAQ_ROOT)/$(Project)/$(Package) $(ReleaseDir)/$(Project)/xphys
	@find $(ReleaseDir)/$(Project)/$(Package) -name "*.d" -exec rm {} \;
	@find $(ReleaseDir)/$(Project)/$(Package) -name "*.o" -exec rm {} \;
	@find $(ReleaseDir)/$(Project)/$(Package) -name "CVS" -exec rm -rf {} \; -print 2>&1  | cat > /dev/null


######################## make rpms (preliminary) #####################
ifndef BUILD_VERSION
BUILD_VERSION=1
endif

ifndef BUILD_COMPILER
BUILD_COMPILER :=gcc$(shell $(CC) -dumpversion | sed -e 's/\./_/g')
endif

ifndef BUILD_DISTRIBUTION
BUILD_DISTRIBUTION := $(shell $(XDAQ_ROOT)/$(BUILD_SUPPORT)/checkos.sh)
endif

ifndef PACKAGE_RELEASE
ifeq ( exists, $(shell [ -d $(BUILD_HOME)/$(BUILD_SUPPORT) ] ) )
include  $(BUILD_HOME)/$(BUILD_SUPPORT)/mfRPM.release
else
include  $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.release
endif
endif

ifndef CONFIG_DIR
ifeq ( exists, $(shell [ -d $(BUILD_HOME)/$(BUILD_SUPORT) ] ) )
CONFIG_DIR=$(BUILD_HOME)
else
CONFIG_DIR=$(XDAQ_ROOT)
endif
endif



PACKAGE_NAME := $(PROJECT_FULL_NAMESPACE)xpcidrv
RPM_VER      :=$(shell cat $(BUILD_HOME)/$(Project)/$(Package)/version)
PACKAGE_VER  :=$(shell cat $(BUILD_HOME)/$(Project)/$(Package)/version | perl -p -e 's/^(\d+\.\d+)\.(\d+)/\1/' )
PACKAGE_PAT  :=$(shell cat $(BUILD_HOME)/$(Project)/$(Package)/version | perl -p -e 's/^(\d+\.\d+)\.(\d+)/\2/' )
#PACKAGE_REL  :=$(shell cat release)
PACKAGE_REL  :=$(BUILD_VERSION).$(PACKAGE_RELEASE).$(BUILD_DISTRIBUTION).$(BUILD_COMPILER)
RELEASE_DIR  :=$(PACKAGE_NAME)-$(RPM_VER)
RELEASE_TAR  :=$(RELEASE_DIR).tar.gz
DISTRIB      :=$(shell ${XDAQ_ROOT}/$(BUILD_SUPPORT)/checkos.sh)


RELEASE_SUBDIRS = $(Project)/$(Package)/include/xpci \
                  $(Project)/$(Package)/src/common \
                  $(BUILD_SUPPORT)

RELEASE_FILES = $(Project)/$(Package)/spec.template \
		$(Project)/$(Package)/xpci \
		$(Project)/$(Package)/Makefile \
		$(Project)/$(Package)/version \
                $(Project)/$(Package)/include/xpci/xpci-types.h   \
                $(Project)/$(Package)/include/xpci/xpci-kernel.h   \
                $(Project)/$(Package)/src/common/xpci.c \
                $(Project)/$(Package)/src/common/Makefile

# treat them extra since they can come from different places
CONFIG_FILES =  $(BUILD_SUPPORT)/mfAutoconf.rules \
                $(BUILD_SUPPORT)/mfDefs.linux \
                $(BUILD_SUPPORT)/mfDefs.version \
                $(BUILD_SUPPORT)/mfRPM.release \
                $(BUILD_SUPPORT)/checkos.sh \
                $(BUILD_SUPPORT)/Makefile.rules


# spec_update #######################################################
.PHONY: spec_update
spec_update:
	perl -p -i -e 's/^(Version:).*/\1 $(RPM_VER)/' $(PackageName).spec # set release version in RPM
	perl -p -i -e 's/^(Release:).*/\1 $(PACKAGE_REL)/' $(PackageName).spec
	perl -p -i -e 's/__libversion__/$(PACKAGE_VER)/' $(PackageName).spec
	perl -p -i -e 's/__projectnamespace__/$(PROJECT_FULL_NAMESPACE)/' $(PackageName).spec
	perl -p -i -e 's/__projectname__/$(PROJECT_NAME)/' $(PackageName).spec
	perl -p -i -e 's/__platform__/$(XDAQ_PLATFORM)/' $(PackageName).spec
	perl -p -i -e 's/__author__/$(Authors)/' $(PackageName).spec

# package ###########################################################
.PHONY: release
release: 
	rm -rf $(RELEASE_DIR)                                              # make directories
	mkdir $(RELEASE_DIR)
	for i in $(RELEASE_SUBDIRS); do mkdir -p $(RELEASE_DIR)/$$i; done;
	cd ../../..; for i in $(RELEASE_FILES); do cp -v $$i $(Project)/$(Package)/$(RELEASE_DIR)/$$i; done;  # copy release files
	cd ../../..; cp -v $(Project)/$(Package)/spec.template $(Project)/$(Package)/$(RELEASE_DIR)/$(Project)/$(Package)/$(PackageName).spec
	cd ../../..; for i in $(CONFIG_FILES); do cp -v $(CONFIG_DIR)/$$i $(Project)/$(Package)/$(RELEASE_DIR)/$$i; done;  # copy config files
	make -C $(RELEASE_DIR)/$(Project)/$(Package) spec_update				   # set release version in RPM
	tar cvfz $(RELEASE_TAR) $(RELEASE_DIR)                             # make tarball
	rm -rf $(RELEASE_DIR)						   # cleanup

# srpm ###############################################################
.PHONY: srpm
srpm: release
	mkdir -p rpm
	mkdir -p RPMBUILD/{RPMS/{i386,i586,i686,x86_64},SPECS,BUILD,SOURCES,SRPMS}
	rpmbuild -ts --define "_topdir $(BUILD_HOME)/$(Project)/$(Package)/RPMBUILD" \./$(RELEASE_TAR) ./$(RELEASE_TAR)
	find ./RPMBUILD -name "*.rpm" | xargs -i mv {} .
	rm -rf ./RPMBUILD rpm{rc,macros}.local
	mv $(PACKAGE_NAME)-$(RPM_VER)-$(PACKAGE_REL).src.rpm rpm/.
# In this version no distribution indicator in the rpm name
#	mv $(PACKAGE_NAME)-$(RPM_VER)-$(PACKAGE_REL).src.rpm rpm/$(PACKAGE_NAME)-$(RPM_VER)-$(PACKAGE_REL).$(DISTRIB).src.rpm 

.PHONY: rpm
# rpm ###############################################################
rpm: srpm
	mkdir -p rpm
	mkdir -p RPMBUILD/{RPMS/{i386,i586,i686,x86_64},SPECS,BUILD,SOURCES,SRPMS}
	rpmbuild -tb  --define "_topdir $(BUILD_HOME)/$(Project)/$(Package)/RPMBUILD" \./$(RELEASE_TAR)
	find ./RPMBUILD -name "*.rpm" | xargs -i mv {} .
	rm -rf ./RPMBUILD rpm{rc,macros}.local
	mv *.rpm rpm/.

_rpmall: rpm

# rpm installation rules #############################################
BUILD_HOME     ?= $(XDAQ_ROOT)
INSTALL_PREFIX ?= $(BUILD_HOME)

.PHONY: cleanrpm
cleanrpm:
	-rm -rf rpm

_cleanrpmall: cleanrpm

.PHONY: installrpm
installrpm:
	mkdir -p $(INSTALL_PREFIX)/rpm
	cp rpm/*.rpm $(INSTALL_PREFIX)/rpm

_installrpmall: installrpm

.PHONY: changelog
changelog:
	cd $(BUILD_HOME)/$(Project)/$(Package);\
	svn --verbose log > ChangeLog


_changelogall: changelog

