/**
 *      @file version.cc
 *
 *     @short Provides run-time versioning and dependency checking of libraries
 *
 *       @see ---
 *    @author Johannes Gutleber, Luciano Orsini, Hannes Sakulin
 * $Revision: 1.2 $
 *     $Date: 2007/03/28 12:06:24 $
 *
 *
 **/
#include "tts/fmmdbi/version.h"
#include "config/version.h"

GETPACKAGEINFO(ttsfmmdbi)

void ttsfmmdbi::checkPackageDependencies() throw (config::PackageInfo::VersionException)
{
	CHECKDEPENDENCY(config);  
}

std::set<std::string, std::less<std::string> > ttsfmmdbi::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,config); 

	return dependencies;
}	
	
