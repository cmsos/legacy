package xdaq.xdesk.jtreeeditor;

import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;

public class JTreeTableModel
    extends AbstractTableModel {
  //---------------------------------Variables definition-----------------------
  short nodeType;
  Vector data = null;
  Vector names = null;
  JTreeUserObject n_Object = null;
//-------------------------------------Constructor------------------------------
  /**Constructor*/
  public JTreeTableModel(JTreeUserObject nodeUserObject) {
    nodeType = nodeUserObject.getType();
    n_Object = nodeUserObject;
    initializeDataVector();
  }

//******************************************************************************
   /**Initialize columns data*/
   private void initializeDataVector() {
     data = new Vector();
     switch (nodeType) {
       case 1:
         data.addElement( (ImageIcon) n_Object.getIcon());
         data.addElement(new JTreeEditableText(n_Object.getName()));
         if (n_Object.getAttrList() != "") {
           data.addElement(new JTreeNotEditableText(n_Object.getAttrList()));
         }
         break;
       case 2:
       case 7:
         data.addElement( (ImageIcon) n_Object.getIcon());
         data.addElement(new JTreeEditableText(n_Object.getName()));
         data.addElement(new JTreeEditableText(n_Object.getValue()));
         break;
       case 3:
         data.addElement(new JTreeIconWithText(n_Object.getIcon(), "#text"));
         data.addElement(new JTreeEditableText(n_Object.getValue()));
         break;
       case 4:
         data.addElement(new JTreeIconWithText(n_Object.getIcon(), "#cdata"));
         data.addElement(new JTreeEditableText(new String(n_Object.
             getValue())));
         break;
       case 8:
         data.addElement(new JTreeIconWithText(n_Object.getIcon(), "#comment"));
         data.addElement(new JTreeEditableText(n_Object.getValue()));
         break;
     }
   }

//******************************************************************************
   /**Returns type of XML elementet associated with tree node*/
   public short get_NodeType() {
     return nodeType;
   }

//******************************************************************************
   /**Returns the most specific superclass for all the cell values in the column.*/
   public Class getColumnClass(int col) {
     return data.elementAt(col).getClass();
   }

//******************************************************************************
   /**Returns the number of rows in the model.*/
   public int getRowCount() {
     return data == null ? 0 : 1;
   }

//******************************************************************************
   /**Returns the number of columns in the model.*/
   public int getColumnCount() {
     return data == null ? 0 : data.size();
   }

//******************************************************************************
   /**Returns the value for the cell at columnIndex and rowIndex.*/
   public Object getValueAt(int row, int column) {
     return data.elementAt(column);
   }

//******************************************************************************
   /**Sets the value in the cell at columnIndex and rowIndex to aValue.*/
   public void setValueAt(Object value, int row, int col) {
     if (value instanceof JTreeEditableText) {
       data.setElementAt(value, col);
       fireTableCellUpdated(row, col);
     }
   }

//******************************************************************************
   /**Returns true if the cell at rowIndex and columnIndex is editable.*/
   public boolean isCellEditable(int rowIndex, int columnIndex) {
     return (data.elementAt(columnIndex) instanceof JTreeEditableText);
   }

//******************************************************************************
}
