package xdaq.xdesk.tools;

public interface MenuBarListener
{
	public void aboutAction();
	public void switchAction();
	public void exitAction();
	public void quitAction();
}
