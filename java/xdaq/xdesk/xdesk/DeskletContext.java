//
//  DaqletContext.java
//  
//
//  Created by Luciano Orsini on Fri Sep 06 2002.
//  Copyright (c) 2002 __CERN__. All rights reserved.
//
package xdaq.xdesk.xdesk;

import java.applet.*;
import java.util.*;
import java.awt.*;
import java.net.*;
import java.io.*;
import javax.swing.JApplet;

import org.apache.log4j.*;

import xdaq.xdesk.tools.*;


public class DeskletContext implements AppletContext 
{
	static Logger logger = Logger.getLogger (DeskletContext.class);

	XDesk xdesk_;
	String path_ = null;
	DeskletFileScanner scanner_;

	public DeskletContext (String rootPath)
	{
		path_ = rootPath;
		xdesk_ = null;
		registry_ = new DeskletRegistry();
		scanner_ = new DeskletFileScanner ();
		
		// Load the XDesk desklet
		// Create the absolute path to the XDesk.xml file to guarantee
		// the uniqueness of the entry in the desklet registry.
		//
		File xdeskPath = new File (path_ + "/xdesk.xml");
		DeskletStub xdeskStub = null;
		try {
			xdeskStub = loadDesklet ( xdeskPath.getCanonicalPath() );
		} catch (IOException e)
		{
			logger.error (e.toString());
			return;
		}
		
		if (xdeskStub != null)
		{		
			xdesk_ = (XDesk) xdeskStub.getApplet(); // force call of init for xdesk applet
			xdeskStub.setActive (true);
			xdesk_.start();
		} else 
		{
			logger.error ("Cannot load applet XDesk");
		}
	}
	
	public boolean isLoaded (String pathname)
	{
		String name = registry_.getName(pathname);
		if (name == null) return false;
		else return true;
	}
	
	public String getAppletName (String pathname)
	{
		return registry_.getName(pathname);		
	}
	
	public int getNumApplets()
	{
		return registry_.size();
	}
	
	public void unloadDesklet(String name)
	{
		Applet  applet = this.getApplet(name);
		System.out.println ("Get applet by name " + name +", got: " + applet.getName());
		applet.destroy(); // inoke user implementation of destroy
		registry_.removeApplet(name);
	}
	
	public DeskletStub loadDesklet (String filename)
	{
		System.out.println (filename);
		Properties p = scanner_.getProperties (filename);
				
		String codebase = p.getProperty ("codebase");
		String code = p.getProperty ("code");
		String name = p.getProperty ("name");
		String archive = p.getProperty ("archive");
		String width = p.getProperty ("width");
		String height = p.getProperty ("height");

		String classPath = "";

		if (codebase == null) 
		{
			codebase = new String(".");
		}

		// if no archive load from codebase + code
		// otherwise load from archive
		if (archive == null)
		{
			classPath = codebase+"/"+code;
		} else {
			classPath = codebase+"/"+archive;
		}
		
		// check if an applet with the same name is already loaded
		if ( registry_.getApplet(name) != null )
		{
		
			logger.error("Applet name: "+ name+ " already existing");
			return null;
		}		
		
		
								
		try {
			// Load jars					
			JApplet applet = null;

			if (loader_ == null) {
				loader_ = new JarClassLoader ( classPath );
			} else {
				loader_.addJarURL ( classPath );
			}
			Class c = loader_.loadClass (code);
			applet = (JApplet) c.newInstance();

			DeskletStub stub = new DeskletStub (p, this, applet);
			applet.setStub (stub);
			applet.setName (name);
			int w = (Integer.decode(width)).intValue();
			int h = (Integer.decode(height)).intValue();
			applet.setSize (w, h);

			registry_.addApplet (name, stub, filename);

			stub.setActive (false);
			
			// Initialize applet
			applet.init();
			
			return stub;
		} 
		catch ( java.net.MalformedURLException mue )
		{
			logger.error ("Malformed URL for desklet " + name + ": " + mue.toString());
		}
		catch ( java.lang.ClassNotFoundException cnf)
		{
			logger.error ("Class not found for desklet " + name + ": " + cnf.toString());
		} 	
		catch ( java.lang.InstantiationException ie)
		{
			logger.error ("Instantiation failed for desklet " + name + ": " + ie.toString());
		}
		catch ( java.lang.IllegalAccessException iae)
		{
			logger.error ("Failed to access class for desklet " + name + ": " + iae.toString());
		}
		return null;
	}
		
	
        // Finds and returns the applet in the document represented by this applet context with the given name.
        // When the shared is set to false the name must be concatenated to the instance number.
	public Applet getApplet(String name) 
        {
		DeskletStub stub = registry_.getApplet (name);
		if (stub != null) return stub.getApplet();
		return null;
        }
	
	public DeskletStub getStub(String name) 
        {
		return registry_.getApplet (name);
        }
	
	//public DeskletStub getStub(int n) 
        //{
	//	return registry_.getApplet (n);
        //}

        // Finds all the applets in the document represented by this applet context.
        public Enumeration getApplets() 
        {
        	Enumeration e = registry_.getApplets();
		if (e != null)
		{
			Vector v = new Vector();
			while (e.hasMoreElements())
			{
				DeskletStub s = (DeskletStub) e.nextElement();
				v.add ( s.getApplet() );
			}
			return v.elements();
		}
		return null;
        }
	
        // Creates an audio clip.
        public AudioClip getAudioClip(URL url) 
        {
		return null;
        }

        // Returns an Image object that can then be painted on the screen.
        public Image getImage(URL url) 
        {
		return null;
        }

        // Replaces the Web page currently being viewed with the given URL.
	public void showDocument(URL url) 
        {
        }
 
        // Requests that the browser or applet viewer show the Web page indicated by the url argument.
        public void showDocument(URL url, String target) 
        {
        }
        
        // Requests that the argument string be displayed in the "status window".
        public void showStatus(String status) 
        {
        }
	
	// Returns the stream to which specified key is associated within this applet context.
	public InputStream	getStream(String key) 
	{
		return null;
	}

	// Finds all the keys of the streams in this applet context.
	public Iterator	getStreamKeys()
	{
		return null;
	} 
	
	// Associates the specified stream with the specified key in this applet context.
	public void setStream(String key, InputStream stream) 
	{
	
	}
	
	DeskletRegistry registry_;
	JarClassLoader loader_;
}

