#ifndef _pt_jxta_exception_Exception_h_
#define _pt_jxta_exception_Exception_h_

#include "pt/exception/Exception.h"

namespace jxta {
	namespace exception { 
		class Exception: public pt::exception::Exception 
		{
			public: 
			Exception( std::string name, std::string message, std::string module, int line, std::string function ): 
					pt::exception::Exception(name, message, module, line, function) 
			{} 
			
			Exception( std::string name, std::string message, std::string module, int line, std::string function,
				xcept::Exception& e ): 
					pt::exception::Exception(name, message, module, line, function, e) 
			{} 
		}; 
	} 
}

#endif
