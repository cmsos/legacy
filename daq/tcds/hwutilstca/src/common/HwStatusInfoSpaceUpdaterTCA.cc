#include "tcds/hwutilstca/HwStatusInfoSpaceUpdaterTCA.h"

#include <string>

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"

tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA::HwStatusInfoSpaceUpdaterTCA(tcds::utils::XDAQAppBase& xdaqApp,
                                                                           tcds::hwlayertca::TCADeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA::~HwStatusInfoSpaceUpdaterTCA()
{
}

bool
tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                             tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayertca::TCADeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "ttc_clock_stable")
            {
              bool const newVal = hw.isTTCClockStable();
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (name == "ttc_clock_up")
            {
              bool const newVal = hw.isTTCClockUp();
              infoSpaceHandler->setBool(name, newVal);
              updated = true;
            }
          else if (name == "ttc_phase_mon.meas_40.measurement_value")
            {
              // NOTE: Update the 'in-application' variable (in ps),
              // as well as the 'for the database' variable (in s).
              double const newVal = hw.readPhaseMonitoring40MHz();
              infoSpaceHandler->setDouble(name, 1.e12 * newVal);
              infoSpaceHandler->setDouble(name + "_for_db", newVal);
              updated = true;
            }
          else if (name == "ttc_phase_mon.meas_160.measurement_value")
            {
              // NOTE: Update the 'in-application' variable (in ps),
              // as well as the 'for the database' variable (in s).
              double const newVal = hw.readPhaseMonitoring160MHz();
              infoSpaceHandler->setDouble(name, 1.e12 * newVal);
              infoSpaceHandler->setDouble(name + "_for_db", newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayertca::TCADeviceBase const&
tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA::getHw() const
{
  return dynamic_cast<tcds::hwlayertca::TCADeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
