#include "tcds/cpm/CPMInputsInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>
#include <utility>

#include "tcds/cpm/TCADeviceCPMT1.h"
#include "tcds/cpm/TCADeviceCPMT2.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::cpm::CPMInputsInfoSpaceUpdater::CPMInputsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                TCADeviceCPMT1 const& hwT1,
                                                                TCADeviceCPMT2 const& hwT2) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hwT1),
  hwT1_(hwT1),
  hwT2_(hwT2)
{
}

tcds::cpm::CPMInputsInfoSpaceUpdater::~CPMInputsInfoSpaceUpdater()
{
}

bool
tcds::cpm::CPMInputsInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                          tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  TCADeviceCPMT1 const& hwT1 = getHwT1();
  TCADeviceCPMT2 const& hwT2 = getHwT2();
  bool updated = false;
  if (hwT1.isHwConnected() && hwT2.isHwConnected())
    {
      std::string name = item.name();
      tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
        {
          // The 'PROCESS' update type means that there is something
          // special to the variable. Figure out what to do based on the
          // variable name.
          if (name == "random_trigger_rate")
            {
              uint32_t const newVal = hwT1.readRandomTriggerRate();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "bunch_mask_trigger_pattern_size")
            {
              std::vector<uint16_t> tmp = hwT1.readBunchMaskTriggerBXs();
              uint32_t const newVal = tmp.size();
              infoSpaceHandler->setUInt32(name, newVal);
              updated = true;
            }
          else if (name == "bunch_mask_trigger_pattern")
            {
              std::vector<uint16_t> bxList = hwT1.readBunchMaskTriggerBXs();
              std::vector<std::pair<uint16_t, uint16_t> > bxRanges =
                tcds::utils::groupBXListIntoRanges(bxList);
              std::string const newVal =  tcds::utils::formatBXRangeList(bxRanges);
              infoSpaceHandler->setString(name, newVal);
              updated = true;
            }
        }
      if (!updated)
        {
          // NOTE: This only works for T1 quantities.
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::cpm::TCADeviceCPMT1 const&
tcds::cpm::CPMInputsInfoSpaceUpdater::getHwT1() const
{
  return hwT1_;
}

tcds::cpm::TCADeviceCPMT2 const&
tcds::cpm::CPMInputsInfoSpaceUpdater::getHwT2() const
{
  return hwT2_;
}
