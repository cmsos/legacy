<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>CMS - ARC</title>
	<link href="css/tables.css" rel="stylesheet" />
	<link href="css/arc.css" rel="stylesheet" />
</head>
<body>

	<?php
		
		ini_set('display_errors', 'On');
		error_reporting(E_ALL | E_STRICT);
		
		$nodeID = 1;
		if(isset($_GET["node"])){
			$nodeID = $_GET["node"];
		}
		
		function makeRow($uuid, $severity)
		{
			
		}

		$root_url_ = "http://cmsdaq0.cern.ch/cmsarc/urn:xdaq-application:service=sentinelarc/";

		$path_ = "";

		
		function makePath($id, $label)
		{
			global $path_, $root_url_;
			$path_ = '<a href="index.php?node=' . $id . '">' . $label . '</a> / ' . $path_;
			//echo "<tr><td>$id</td><td>$label</td></tr>\n";
		}
	
		
		$exceptions = file_get_contents($root_url_ . "getNodeSummary?node=" . $nodeID);
		$json = json_decode($exceptions, true);
		
		foreach ($json['path'] as $val) { // for each exception
			//echo "loop";
			makePath($val['id'], $val['label']);
		}
		
		echo $path_;
		
		echo '<br />';
		
		echo '<table class="xdaq-table">';
		echo '<thead>';
		echo '<tr>';
		echo '<th></th>';
		echo '<th>New</th>';
		
		foreach ($json['severities'] as $val) { // for each exception
			echo '<th>' . ucfirst($val['label']) . '</th>';
		}

		echo '<th>Shelved</th>';
		echo '<th>Cleared</th>';
		echo '<th>Solved</th>';
		echo '</tr>';
		echo '</thead>';
		
		echo '<tbody>';
		
		foreach ($json['children'] as $val) { // for each exception
			echo "<tr>";
			echo '<td><a href="index.php?node=' . $val['id'] . '">' . $val['label'] . '</a></td>'; // node
			
			echo '<td><a href="exceptionList.php?node=' . $val['id'] . '&list=raisedUnaccepted"><button>' . $val['raisedUnaccepted'] . "</button></a></td>";
			
			foreach ($json['severities'] as $sev) { // for each exception
				echo '<td><a href="exceptionList.php?node=' . $val['id'] . '&list=raisedAccepted&severity=' . $sev['label'] . '"><button>' . $val['raisedAccepted'][$sev['label']] . '</button></a></td>';
			}
			
			echo '<td><a href="exceptionList.php?node=' . $val['id'] . '&list=shelved"><button>' . ($val['shelvedUnaccepted'] + $val['shelvedAccepted']). "</button></a></td>";
			echo '<td><a href="exceptionList.php?node=' . $val['id'] . '&list=clearedUnaccepted"><button>' . $val['clearedUnaccepted'] . "</button></a></td>";
			echo '<td><a href="exceptionList.php?node=' . $val['id'] . '&list=clearedAccepted"><button>' . $val['clearedAccepted'] . "</button></a></td>";
			
			echo "</tr>";
		}
		
		//////////
		
		echo "<tr>";
		echo '<td>Total</td>';
		echo '<td><a href="exceptionList.php?node=' . $nodeID . '&list=raisedUnaccepted"><button>' . $json['node']['raisedUnaccepted'] . "</button></a></td>";
		
		foreach ($json['severities'] as $sev) { // for each exception
			echo '<td><a href="exceptionList.php?node=' . $nodeID . '&list=raisedAccepted&severity=' . $sev['label'] . '"><button>' . $json['node']['raisedAccepted'][$sev['label']] . "</button></a></td>";
		}
		
		echo '<td><a href="exceptionList.php?node=' . $nodeID . '&list=shelved"><button>' . ($json['node']['shelvedUnaccepted'] + $json['node']['shelvedAccepted']). "</button></a></td>";
		echo '<td><a href="exceptionList.php?node=' . $nodeID . '&list=clearedUnaccepted"><button>' . $json['node']['clearedUnaccepted'] . "</button></a></td>";
		echo '<td><a href="exceptionList.php?node=' . $nodeID . '&list=clearedAccepted"><button>' . $json['node']['clearedAccepted'] . "</button></a></td>";
		
		echo "</tr>";
		
		//////////

		echo '</tbody>';
		echo '</table>';
	?>
		
</body>
</html>