package xdaq.xdesk.tools;

import java.util.*;
import javax.infobus.*;
import java.beans.*;

import org.apache.log4j.*;

public class InfoBusPublisher implements InfoBusDataProducer
{
	Logger logger = Logger.getLogger (InfoBusPublisher.class);

	InfoBusMemberSupport infobusSupport_;
	HashMap changeManagers_; // this belongs only to publishers

	// command, e.g.: xcommand
	public InfoBusPublisher (String infobus)
	{
		changeManagers_ = new HashMap();
		infobusSupport_ = new InfoBusMemberSupport (null);

		try
		{
			infobusSupport_.joinInfoBus(infobus);
			infobusSupport_.getInfoBus().addDataProducer (this);
		} catch (Exception e)
		{
			logger.error ("Cannot access infobus " + infobus + ": " + e.toString());
		}
	}

	public void publishItem (String item)
	{
		if (infobusSupport_ != null)
		{
			if (changeManagers_.get(item) == null)
			{
				// not yet published
				changeManagers_.put (item, new DataItemChangeManagerSupport(this));
				infobusSupport_.getInfoBus().fireItemAvailable (item,null,this);
			} else
			{
				logger.error ("Cannot publish item " + item + ": item has already been published");
			}
		} else
		{
			logger.error ("Cannot publish infobus item " + item + ": infobus not available");
		}
	}


	public void revokeItem (String item)
	{
		DataItemChangeManagerSupport manager = (DataItemChangeManagerSupport) changeManagers_.get(item);
		if (manager != null)
		{
			infobusSupport_.getInfoBus().fireItemRevoked(item, this);
			manager.removeAllListeners();
			changeManagers_.remove (item);
		} else
		{
			logger.error ("Cannot revoke infobus item " + item + ": item not available");
		}

	}

	public void fireItemValueChanged (String item, Object data)
	{
		// PUT dataitem on infobus
		// getPaths(), command, source=this
		//SelectionTableModel selection = new SelectionTableModel( getPaths(), command, controller_);

		DataItemChangeManagerSupport manager = (DataItemChangeManagerSupport) changeManagers_.get (item);
		if (manager != null)
		{
                  manager.fireItemValueChanged (data, null);
		} else
		{
			logger.error ("Cannot change infobus item " + item + ": item was not published");
		}
	}


	// A consumer requests to listen to xcommand. Pass it the change manager for subscription
	public void dataItemRequested (InfoBusItemRequestedEvent e)
	{
		String item = e.getDataItemName();
		DataItemChangeManagerSupport manager = (DataItemChangeManagerSupport) changeManagers_.get (item);
		if (manager != null)
		{
			e.setDataItem ( manager );
		}
	}

	public void propertyChange (PropertyChangeEvent e)
  	{
  		logger.error ("proprtyChange function not yet implemented");
  	}

	public void shutdown()
	{
	 	Set keySet = changeManagers_.keySet();
		Object [] names = keySet.toArray(new Object [] {} );
		for ( int i=0; i < names.length; i++ )
		{
			this.revokeItem((String)names[i]);
		}
		try {
			infobusSupport_.getInfoBus().addDataProducer (this);
			infobusSupport_.leaveInfoBus();
		}
		catch (Exception e )
		{
			logger.error(e.toString());

		}
	}
}
