// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_admin_GetConfig_h_
#define _tstore_api_admin_GetConfig_h_

#include <string>
#include "toolbox/net/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/admin/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
namespace admin
{
	class GetConfiguration: public tstore::api::Request
	{
		public:
		
		GetConfiguration(const std::string & path, const std::string & view );
		
		GetConfiguration(xoap::MessageReference& msg) ;
        
		virtual ~GetConfiguration();
		
		ws::addressing::Headers & getAddressingHeaders();		
		
		xoap::MessageReference toSOAP();
				
		std::string getView();
		std::string getPath();
		
		private:
		
		std::string  path_;
		std::string  view_;
	};
}}}

#endif
