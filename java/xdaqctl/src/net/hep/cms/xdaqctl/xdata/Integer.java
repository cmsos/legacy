package net.hep.cms.xdaqctl.xdata;

public class Integer extends Integer32 {
	@Override
	int getEXDRCode() {
		return XDataType.INTEGER.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
