// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "ibvla/ConnectionRequest.h"

#include "ibvla/Acceptor.h"

ibvla::ConnectionRequest::ConnectionRequest (int lid, int psn, int qpn, ibvla::Acceptor * acceptor, int sockfd, const std::string & ip, const std::string & hostname)
	: remote_lid_(lid), remote_psn_(psn), remote_qpn_(qpn), acceptor_(acceptor), sockfd_(sockfd), ip_(ip), hostname_(hostname)
{
}
