package xdaq.xdesk.jfinder;

import javax.swing.JLabel;

public interface FinderSelectionModel
{
	public static final int SINGLE_SELECTION = 0;
	public static final int MULTILPE_SELECTION = 1;
	
	public void addFinderSelectionListener (FinderSelectionListener l);
	public void removeFinderSelectionListener (FinderSelectionListener l);
	
	public void clearSelection();	
	
	public int getSelectionMode();
	public void setSelectionMode (int mode);
	
	public void setSelectionCell (JLabel n);	
	public void addSelectionCell (JLabel n);	
	public void removeSelectionCell (JLabel n);
	
	public JLabel[] getSelectionCells();
	public JLabel getSelectionCell();
	
	public int getSelectionCount();
	
	public boolean 	isNodeSelected (JLabel n);
	
	public boolean isSelectionEmpty();
	
	
	
}
