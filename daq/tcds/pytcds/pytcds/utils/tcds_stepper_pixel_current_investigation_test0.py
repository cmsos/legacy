#!/usr/bin/env python

###############################################################################
## Script to help track down/diagnose the issue with the varying
## Phase-1 pixel digital curents as a function of CPM activity.
###############################################################################

import signal
import sys

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_step_runner import Step
from pytcds.utils.tcds_step_runner import StepPrintMessage
from pytcds.utils.tcds_step_runner import StepRunner
from pytcds.utils.tcds_step_runner import StepSleep
from pytcds.utils.tcds_step_runner import StepWaitForEnter

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

FIVE_MINUTES = 5 * 60
TWO_MINUTES = 2 * 60
STEP_TIME = TWO_MINUTES

CPM = "cpm-pri-t1"

###############################################################################

def exit_strategy(signal, frame):
    print
    sys.exit()
    # End of exit_strategy().

###############################################################################

class StepCPMDisableTriggerSources(Step):

    def __init__(self, hw):
        description = "Disabling all CPM trigger sources"
        function = self.tweak_inselect
        super(StepCPMDisableTriggerSources, self).__init__(description, function)
        self.hw = hw
        # End of __init__().

    def tweak_inselect(self):
        self.hw.write("cpmt1.ipm.main.inselect.external_trigger0_enable", 0x0)
        self.hw.write("cpmt1.ipm.main.inselect.external_trigger1_enable", 0x0)
        self.hw.write("cpmt1.ipm.main.inselect.external_trigger_from_cpm_l1a_enable", 0x0)
        self.hw.write("cpmt1.ipm.main.inselect.random_trigger_enable", 0x0)
        self.hw.write("cpmt1.ipm.main.inselect.bunch_mask_trigger_enable", 0x0)
        self.hw.write("cpmt1.ipm.main.inselect.cyclic_trigger_enable", 0x0)
        # End of tweak_inselect().
        return True

    # End of class StepCPMDisableTriggerSources.

###############################################################################

class StepCPMSetRandomTriggerRate(Step):

    def __init__(self, hw, rate):
        description = "Disabling CPM random triggers"
        if rate > 0:
            description = "Enabling CPM random trigger rate at {0:d} Hz".format(int(rate))
        function = self.set_random_trigger_rate
        super(StepCPMSetRandomTriggerRate, self).__init__(description, function)
        self.hw = hw
        self.rate = rate
        # End of __init__().

    def set_random_trigger_rate(self):
        enable = 0
        val = 0
        if self.rate > 0:
            enable = 1
            val = int(self.rate / (3564. * 11246 / 2**32))
        self.hw.write("cpmt1.ipm.main.random_trigger_config", val)
        self.hw.write("cpmt1.ipm.main.inselect.random_trigger_enable", enable)
        # End of set_random_trigger_rate().
        return True

    # End of class StepCPMSetRandomTriggerRate.

###############################################################################

class StepCPMSetROCResetRate(Step):

    def __init__(self, hw, rate):
        description = "Disabling CPM periodic ROC reset sequence"
        if rate > 0:
            description = "Enabling CPM periodic ROC reset sequence at {0:d} Hz".format(int(rate))
        function = self.set_roc_reset_rate
        super(StepCPMSetROCResetRate, self).__init__(description, function)
        self.hw = hw
        self.rate = rate
        # End of __init__().

    def set_roc_reset_rate(self):
        enable = 0
        val = 0
        if self.rate > 0:
            enable = 1
            val = 11246 / self.rate
        self.hw.write("cpmt1.ipm.cyclic_generator6.configuration.prescale", val)
        self.hw.write("cpmt1.ipm.cyclic_generator6.configuration.enabled", enable)
        # End of set_roc_reset_rate().
        return True

    # End of class StepCPMSetROCResetRate.

###############################################################################

class Runner(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Runner, self).__init__(description, usage, epilog)
        self.target = CPM
        # End of __init__().

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def handle_args(self):
        self.args.target = self.target
        super(Runner, self).handle_args()
        # End of handle_args().

    def main(self):

        # Validate the CPM target and connect to the hardware.
        print "Validating CPM target, and connecting to the hardware"
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        cpm = get_cpm_hw(network_address, controlhub_address, verbose=self.verbose)
        if not cpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        print "Building procedure steps"
        steps = [
            StepPrintMessage("Starting"),

            StepWaitForEnter("Please 'Configure' a global run _with_ pixel included and then press Enter..."),
            StepWaitForEnter("Please 'Configure' a global run _without_ pixel included and then press Enter..."),
            StepCPMDisableTriggerSources(cpm),
            StepWaitForEnter("Please 'Start' the global run and then press Enter..."),
            StepSleep(STEP_TIME),

            StepCPMSetRandomTriggerRate(cpm, 10.e3),
            StepSleep(STEP_TIME),
            StepCPMSetRandomTriggerRate(cpm, 30.e3),
            StepSleep(STEP_TIME),
            StepCPMSetRandomTriggerRate(cpm, 50.e3),
            StepSleep(STEP_TIME),
            StepCPMSetRandomTriggerRate(cpm, 75.e3),
            StepSleep(STEP_TIME),
            StepCPMSetRandomTriggerRate(cpm, 0),
            StepSleep(STEP_TIME),

            StepCPMSetROCResetRate(cpm, 70),
            StepSleep(STEP_TIME),
            StepCPMSetROCResetRate(cpm, 50),
            StepSleep(STEP_TIME),
            StepCPMSetROCResetRate(cpm, 0),
            StepSleep(STEP_TIME),

            StepCPMSetRandomTriggerRate(cpm, 10.e3),
            StepSleep(STEP_TIME),
            StepCPMSetRandomTriggerRate(cpm, 75.e3),
            StepSleep(STEP_TIME),

            StepPrintMessage("Done")
        ]

        #----------

        print "Running procedure steps"
        stepper = StepRunner(steps)
        stepper.run()

        #----------

        # End of main().

    # End of class Runner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to help track down/diagnose the issue" \
               " with the varying Phase-1 pixel digital curents" \
               " as a function of CPM activity."

    signal.signal(signal.SIGINT, exit_strategy)
    res = Runner(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
