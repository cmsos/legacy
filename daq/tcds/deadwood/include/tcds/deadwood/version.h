#ifndef _tcds_deadwood_version_h_
#define _tcds_deadwood_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSDEADWOOD_VERSION_MAJOR 3
#define TCDSDEADWOOD_VERSION_MINOR 13
#define TCDSDEADWOOD_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSDEADWOOD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSDEADWOOD_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSDEADWOOD_VERSION_CODE PACKAGE_VERSION_CODE(TCDSDEADWOOD_VERSION_MAJOR,TCDSDEADWOOD_VERSION_MINOR,TCDSDEADWOOD_VERSION_PATCH)
#ifndef TCDSDEADWOOD_PREVIOUS_VERSIONS
#define TCDSDEADWOOD_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSDEADWOOD_VERSION_MAJOR,TCDSDEADWOOD_VERSION_MINOR,TCDSDEADWOOD_VERSION_PATCH)
#else
#define TCDSDEADWOOD_FULL_VERSION_LIST TCDSDEADWOOD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSDEADWOOD_VERSION_MAJOR,TCDSDEADWOOD_VERSION_MINOR,TCDSDEADWOOD_VERSION_PATCH)
#endif

namespace tcdsdeadwood
{
  const std::string package = "tcdsdeadwood";
  const std::string versions = TCDSDEADWOOD_FULL_VERSION_LIST;
  const std::string description = "Dummy application to test some features of the CMS TCDS control framework.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "Dummy application to test some features of the CMS TCDS control framework.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
