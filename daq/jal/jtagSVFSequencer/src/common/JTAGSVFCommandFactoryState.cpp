#include "jal/jtagSVFSequencer/JTAGSVFCommandFactoryState.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandGotoState.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandGotoStateByPath.h"

#include "jal/jtagChain/JTAGState.h"

using namespace std;


jal::JTAGSVFCommand* jal::JTAGSVFCommandFactoryState::create(vector<string> const& args) 
  throw (jal::SVFSyntaxException) {

  if (args.size() < 2)    
    XCEPT_RAISE(jal::SVFSyntaxException, "State command needs at least one parameter.");

  if (args.size() == 2) {

    jal::JTAGState state( args[1] );
    if ( !state.isStable() )
      XCEPT_RAISE(jal::SVFSyntaxException, "State command needs stable (end-)state as parameter.");

    return (jal::JTAGSVFCommand *) new jal::JTAGSVFCommandGotoState( state );
  
  } 
  else {

    vector <jal::JTAGState> statepath;

    for (std::vector<std::string>::size_type i=1;i<args.size();i++)
      statepath.push_back( jal::JTAGState( args[i] ) );

    // TBD: validation of state path

    //    XCEPT_RAISE(jal::SVFSyntaxException, "State command needs exactly one parameter.");
      
    return (jal::JTAGSVFCommand *) new jal::JTAGSVFCommandGotoStateByPath( statepath );
  }
}

