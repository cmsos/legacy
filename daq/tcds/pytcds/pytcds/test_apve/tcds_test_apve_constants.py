###############################################################################
## Various constants used in the TCDS monitoring stress-tests in the lab.
###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
ICIS = ["ici-41"]
APVES = ["apve-41"]
PIS = ["pi-41"]

# The FED id of the xPM.
FED_ID = 1024

###############################################################################
