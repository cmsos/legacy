package net.hep.cms.xdaqctl.xdata;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.String;

/**
 * Java representation of the XDAQ FlashList
 * <p/>
 * Typical usage:
 * <pre>
 * Flashlist list = new FlashList("mylist");
 * list.add("name", XDataType.STRING);
 * list.add("age", XDataType.INTEGER);
 *
 * SimpleItem item;
 * item = (SimpleItem)list.get("name");
 * item.setValue("John Doe");
 * item = (SimpleItem)list.get("age");
 * item.setValue(32);
 * </pre>
 */

public class FlashList {

	private final static String DEFAULT_NAME = "default";
	
	protected String name;
	private Map<String, XDataItem> map;
	
	/**
	 * Create a flashlist with the default name
	 */
	public FlashList() {
		this(DEFAULT_NAME);
	}

	/**
	 * Create a flashlist with a name
	 */
	public FlashList(String name) {
		this.name = name;
		map = new LinkedHashMap<String, XDataItem>();
	}

	/**
	 * @return name of the flashlist
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name name of a flashlist item
	 */
	public XDataItem get(String name) {
		return map.get(name);
	}

	/**
	 * add an item to the list.
	 *
	 * @param name name of a flashlist item
	 * @param type type of the item, one of the enum from XDataType
	 */
	public void add(String name, XDataType type) {
		map.put(name, type.getInstance());
	}

	/**
	 * serialize the flashlist in EXDR format binary stream
	 */
	public ByteArrayInputStream serialize() throws IOException {
	
		ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(byteOut);
		
		out.writeInt(11);      // table type descriptor
		out.writeInt(15);      // #col, uint32
		out.writeInt(map.size());       // fl size (number of members)
		out.writeInt(15);      // #row, uint32
		out.writeInt(1);       // 1 row (no history supported)
				
		for (String name: map.keySet()) {		
			net.hep.cms.xdaqctl.xdata.String colName = 
					new net.hep.cms.xdaqctl.xdata.String();
			colName.setValue(name);
			colName.writeEXDR(out);
			
			XDataItem item = map.get(name);

			out.writeInt(15);  // uint32
			out.writeInt(item.getEXDRCode()); // data type 
			
			out.writeInt(9);   // vector, length 1
			out.writeInt(1);
			
			item.writeEXDR(out);
		}
		
		return new ByteArrayInputStream(byteOut.toByteArray());
	}

	/**
	 * deserialize the flashlist from an EXDR format binary stream
	 */
	public void deserialize(ByteArrayInputStream byteIn) throws IOException {		
		map.clear();
		DataInputStream in = new DataInputStream(byteIn);

		in.readInt(); // EXDR code
		
		int ncol;
		UnsignedInteger32 n = new UnsignedInteger32();
		n.readEXDR(in);
		ncol = n.getValue().intValue();
		
		n.readEXDR(in);  // #row, not used here
		
		for (int i = 0; i < ncol; ++i) {
			net.hep.cms.xdaqctl.xdata.String name = new net.hep.cms.xdaqctl.xdata.String();
			name.readEXDR(in);
			
			UnsignedInteger32 type = new UnsignedInteger32();
			type.readEXDR(in);
			
			Vector vec = new Vector();
			vec.readEXDR(in);
			
			// catch the case that the vector has ZERO lenth
			if (vec.size()!=0) 
				map.put(name.getValue(), vec.get(0));
		}
	}

	/**
	 * @return all the item names in the flashlist
	 */
	public Set<String> getItemNames() {
		return map.keySet();
	}

}

// End of file
// vim: set sw=4 ts=4:
