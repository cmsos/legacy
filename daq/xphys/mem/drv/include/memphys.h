// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <linux/ioctl.h>

#define MEMPHYS_IOC_MAGIC 0xEE

struct free_info
{
	unsigned long num_chunks;
	unsigned long *ids;
};

#define FREE_INFO_LEN sizeof(struct free_info)

struct chunk_addrs {
  unsigned long index;		/* The index of the chunk - used
				   internally by memphys */
  unsigned long virt_adr;	/* Kernel virtual address of
				   (beginning of) the chunk */
  unsigned long phys_adr;	/* Physical address of the chunk */
  unsigned long bus_adr;	/* Bus address of the chunk */
};

struct pool_info {
  unsigned long chunk_order;
  unsigned long chunks_available;
};

struct alloc_info {
  unsigned long num_chunks;
  struct chunk_addrs *result;
};

#define MEMPHYS_IOC_ALLOC _IOWR(MEMPHYS_IOC_MAGIC, 0, struct alloc_info)
#define MEMPHYS_IOC_FREE _IOW(MEMPHYS_IOC_MAGIC, 1, struct free_info)
#define MEMPHYS_IOC_POOL_INFO _IOR(MEMPHYS_IOC_MAGIC, 2, struct pool_info)

// These are for debugging
#define MEMPHYS_IOC_CLIENT_ID _IO(MEMPHYS_IOC_MAGIC, 3)
#define MEMPHYS_IOC_DUC _IO(MEMPHYS_IOC_MAGIC, 4)
