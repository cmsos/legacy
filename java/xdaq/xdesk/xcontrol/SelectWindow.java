package xdaq.tools.win;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.border.*;
import org.w3c.dom.*;
import xdaq.tools.win.xdaqWin;

public class SelectWindow extends JDialog
{
	xdaqWin win_;
	JComboBox nodes_;
	JPanel po;
	Box applicationOptions_;
	Box hostOptions_;
	
	JComboBox urlField;
	JComboBox idField;
	JComboBox classField;
	JComboBox instanceField;
	JComboBox tidField;
	JComboBox transportField;
	
	public SelectWindow( xdaqWin win )
	{
		super (win, "Select", true); 
		win_ = win;
		
		applicationOptions_ = prepareApplicationOptions();
		hostOptions_ = prepareHostOptions();
		
		Container mainPanel = getContentPane();
		nodes_ = new JComboBox();
		nodes_.addItem ( "Host" );
		nodes_.addItem ( "Application/Transport" );
		
		ActionListener nodeSelectListener = new ActionListener()
		{
			public void actionPerformed (ActionEvent e)
			{
				buildOptionsInformation();
			}
		};
		nodes_.addActionListener ( nodeSelectListener );
		
		
		JPanel comboPanel = new JPanel();
		comboPanel.add ( nodes_ );
		mainPanel.add ( comboPanel , BorderLayout.NORTH );
			
		po = new JPanel();
		
		po.setBorder ( new TitledBorder ( new EtchedBorder(), "Options" ) );
		mainPanel.add (po, BorderLayout.CENTER);
		
		JPanel buttonPanel = new JPanel();
		
		JButton okB = new JButton ("OK");
		ActionListener selectAction = new ActionListener()
		{
			public void actionPerformed ( ActionEvent e) 
			{
				setSelection ( );
				setVisible ( false );
			}
		};
		okB.addActionListener ( selectAction );
		
		JButton cancelB = new JButton ("Cancel");
		ActionListener closeAction = new ActionListener()
		{
			public void actionPerformed ( ActionEvent e) 
			{
				setVisible ( false );
			}
		};
		cancelB.addActionListener ( closeAction );
		
		buttonPanel.add ( okB );
		buttonPanel.add ( cancelB );
		mainPanel.add ( buttonPanel, BorderLayout.SOUTH );
		buildOptionsInformation();
		this.setSize (400,275);
		this.show();
	}
	
	
	Box prepareApplicationOptions()
	{
		Box content = Box.createHorizontalBox();
		Box checkBox = Box.createVerticalBox();
		Box textBox = Box.createVerticalBox();
		content.add (checkBox);
		content.add (textBox);
		
		JLabel classcb = new JLabel ("Class  ");
		checkBox.add (classcb);
			
		classField = createComboBox("ClassDef", "NodeValue"); 
		textBox.add (classField);
			
		JLabel instancecb = new JLabel ("Instance  ");
		checkBox.add (instancecb);
		instanceField = createComboBox("Application", "instance"); 
		textBox.add (instanceField);
			
		JLabel tidcb = new JLabel ("Target address  ");
		checkBox.add (tidcb);
		tidField = createComboBox("Application", "targetAddr"); 
		textBox.add (tidField);
		
		/*	
		JLabel transportcb = new JLabel ("Transport  ");
		checkBox.add (transportcb);
		transportField = createComboBox("Application", "transport"); 
		textBox.add (transportField);
	   */
		return content;
	}
	
	Box prepareHostOptions()
	{
		Box content = Box.createHorizontalBox();
		Box checkBox = Box.createVerticalBox();
		Box textBox = Box.createVerticalBox();
		content.add (checkBox);
		content.add (textBox);	
		
		JLabel idcb = new JLabel ("id  ");
		checkBox.add (idcb);
		idField = createComboBox("Host", "id"); 
		textBox.add (idField);
			
		JLabel urlcb = new JLabel ("url  ");
		checkBox.add (urlcb);
		urlField = createComboBox("Host", "url"); 
		textBox.add (urlField);	
		return content;	
	}
	
	void buildOptionsInformation()
	{
		po.removeAll();
		
		String selection = (String) nodes_.getSelectedItem();
		
		if ( selection.equals("Application/Transport") )
		{
			po.add (applicationOptions_);	
		} 
		else if ( selection.equals ("Host") )
		{
			po.add (hostOptions_);
		}
		
		this.validate();
		this.repaint();
	}
	
	void setSelection ( )
	{
		String selection = (String) nodes_.getSelectedItem();
		
		if ( selection.equals("Application/Transport") )
		{
			String className  = (String) classField.getSelectedItem();
			String instance   = (String) instanceField.getSelectedItem();
			String tid        =  (String) tidField.getSelectedItem();
			//String transport  = (String) transportField.getSelectedItem();
			
			win_.getTree().selectApplication ( className, instance, tid, "Any" );	
		} 
		else if ( selection.equals ("Host") )
		{
			String id  = (String) idField.getSelectedItem();
			String url = (String) urlField.getSelectedItem();
			win_.getTree().selectHost ( id, url );
		}
	
		
	}
	
	JComboBox createComboBox(String nodeName, String attribute) 
	{
		JComboBox cbox = new JComboBox();
		cbox.addItem ( "Any" );
		org.w3c.dom.Document doc = win_.getTree().getDocument();
		NodeList childList = doc.getElementsByTagName(nodeName);
		for (int i =0; i < childList.getLength(); i++)
		{
			String entry;
			if ( attribute.equals("NodeValue") ){
				entry = childList.item(i).getFirstChild().getNodeValue();
			} else {
				NamedNodeMap attributes = childList.item(i).getAttributes();
				entry = attributes.getNamedItem(attribute).getNodeValue();
			}
			
			// handle multiple equal entries
			int j;
			for ( j= 0; j< cbox.getItemCount(); j++ ) {
				if ( ((String)cbox.getItemAt(j)).equals(entry) ) break;
			}
			if ( j == cbox.getItemCount()	 ) cbox.addItem (entry);

		}
		return cbox;
	
	}	
}
