// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/ApplicationService.h"
#include "psx/mapi/ConnectRequest.h"
#include "psx/mapi/Monitor.h"

psx::mapi::Monitor::Monitor(psx::mapi::ConnectRequest* request)
	: SmiObject((char*) request->formatObjectName().c_str()) 
{ 
	request_ = request;
	
	// Convert this pointer to a transaction identifier
	//char oid[char(2*sizeof(unsigned int)+1)]; // +1 for \0 at the end
	//snprintf (oid, 2*sizeof(unsigned int), "%x", (unsigned int) this);
	//id_ = oid;

	std::stringstream val; 
 	val << std::hex << (void*)this << std::dec; 
 	id_ = val.str(); 
}

psx::mapi::Monitor::~Monitor()
{ 
	delete request_;
	// delete domain_;
}

void psx::mapi::Monitor::connect() 
{
	// No need to connect...
	
	//domain_ = new SmiDomain((char*) request_->getObjectName().c_str());
    
    	//if ( domain_->getNObjects() == 0 ) 
	//{
	//	std::string msg = "Cannot connect to domain ";
	//	msg += request_->getObjectName();
        //	XCEPT_RAISE (psx::mapi::exception::Exception, msg);
    	//}
}

void psx::mapi::Monitor::smiStateChangeHandler() 
{
	char *state = this->getState();

	if(state != 0)
	{
		request_->getApplicationService()->notify(request_->getObjectName(), 
							request_->getURL(), 
							request_->getAction(),
							request_->getContext(),
							id_,
							state);
	}
	else
	{
		request_->getApplicationService()->notify(request_->getObjectName(), 
							request_->getURL(), 
							request_->getAction(),
							request_->getContext(),
							id_,
							"DEAD");
	}
}

std::string psx::mapi::Monitor::getId()
{
	return id_;	
}

void psx::mapi::Monitor::smiExecutingHandler() 
{
    // std::cout << this->getName() << " Busy" << std::endl;
}

std::string psx::mapi::Monitor::getURL()
{
	return request_->getURL();
}

std::string psx::mapi::Monitor::getOwner()
{
	return request_->getOwner();
}
