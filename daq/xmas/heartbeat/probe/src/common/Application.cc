// $Id$

/*************************************************************************
 * XDAQ XMAS Heartbeat Probe               								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/heartbeat/probe/Application.h"
#include "xmas/heartbeat/probe/exception/Exception.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"

#include "xmas/xmas.h"

#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
//#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"

#include "xoap/DOMParserFactory.h"

#include "b2in/utils/exception/SendFailure.h"
#include "b2in/utils/exception/CheckConnectionFailure.h"

XDAQ_INSTANTIATOR_IMPL (xmas::heartbeat::probe::Application);

xmas::heartbeat::probe::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this)
{
	outgoingHeartbeatCounter_ = 0;
	outgoingHeartbeatLostCounter_ = 0;

	heartbeatdMessengerCache_ = 0;

	heartbeatdURL_ = "";

	this->getApplicationInfoSpace()->fireItemAvailable("heartbeatdURL", &heartbeatdURL_);

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/xmas/heartbeat/probe/images/heartbeat-probe-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/heartbeat/probe/images/heartbeat-probe-icon.png");

	// This application is an asynchronous event processor
	//dispatcher_.addActionListener(this);

	// Activates work loop for sensor asynchronous operations
	(void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:heartbeatprobe", "waiting")->activate();

	// heartbeat report interval
	heartbeatWatchdog_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("heartbeatWatchdog", &heartbeatWatchdog_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &xmas::heartbeat::probe::Application::Default, "Default");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);
}

xmas::heartbeat::probe::Application::~Application ()
{

}

void xmas::heartbeat::probe::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string timerTaskName = e.getTimerTask()->name;
	if (timerTaskName == "heartbeatprobe:scan")
	{
		try
		{
			this->heartbeat();
		}
		catch (b2in::utils::exception::Exception& f)
		{
			std::stringstream msg;
			msg << "Failed to scan b2in-eventing (for heartbeat) services";
			XCEPT_DECLARE_NESTED(xmas::heartbeat::probe::exception::Exception, e, msg.str(), f);
			this->notifyQualified("fatal", e);
		}

	}

	// check connection to sensord

	if (heartbeatdMessengerCache_ == 0)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Messenger cache not ready");
		return;
	}

	if (!heartbeatdMessengerCache_->hasMessenger(heartbeatdURL_))
	{
		try
		{
			//std::cout << "create messenger " << std::endl;
			heartbeatdMessengerCache_->createMessenger(heartbeatdURL_); // trigger  caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << heartbeatdURL_.toString() << ", " << e.what());
			this->notifyQualified("error", e);
			return;
		}
	}

}

void xmas::heartbeat::probe::Application::actionPerformed (xdata::Event& event)
{

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		toolbox::task::Timer * timer = 0;

		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:probe-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:probe-timer");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:probe-timer");
		}

		toolbox::TimeInterval interval;

		int delay = (rand() % 14) + 1;
		toolbox::TimeVal start(toolbox::TimeVal::gettimeofday().sec() + delay);
		interval.fromString(heartbeatWatchdog_);

		timer->scheduleAtFixedRate(start, this, interval, 0, "heartbeatprobe:scan");
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(xmas::heartbeat::probe::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void xmas::heartbeat::probe::Application::actionPerformed (toolbox::Event& event)
{

	if (event.type() == "xdaq::EndpointAvailableEvent")
	{
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(event);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName() << " on application network " << networkName);

		if (network->getName() == networkName)
		{
			try
			{
				heartbeatdMessengerCache_ = new b2in::utils::MessengerCache(this->getApplicationContext(), networkName, this);
			}
			catch (b2in::utils::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to create b2in messenger cache on network '" << network << "'";
				LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
				LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
				return;
			}

			try
			{
				heartbeatdMessengerCache_->createMessenger(heartbeatdURL_); // trigger  caching of messenger for the remote endpoint
			}
			catch (b2in::utils::exception::Exception& e)
			{
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << heartbeatdURL_.toString() << ", " << e.what());
				this->notifyQualified("error", e);
				return;
			}
		}
		else
		{
			std::stringstream ss;
			ss << "No match found for network name, not creating messenger cache : configured network name = '" << networkName << "', available = '" << network->getName() << "'";
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());
			return;
		}
	}
}

std::set<std::string> xmas::heartbeat::probe::Application::scanLocalServices ()
{
	std::set < std::string > services;

	const xdaq::Zone* zone = getApplicationContext()->getDefaultZone();
	std::set<const xdaq::ApplicationDescriptor*> local = zone->getApplicationDescriptors(this->getApplicationContext()->getContextDescriptor());
	for (std::set<const xdaq::ApplicationDescriptor*>::iterator vi = local.begin(); vi != local.end(); ++vi)
	{
		if ((*vi)->getAttribute("service") != "")
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Found local service:" << (*vi)->getAttribute("service"));
			services.insert((*vi)->getAttribute("service"));
		}
		else
		{

			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "No service attribute provided for application class '" << (*vi)->getAttribute("class") << "', cannot load sensor settings");

		}

	}
	return services;
}

void xmas::heartbeat::probe::Application::asynchronousExceptionNotification (xcept::Exception& ex)
{
	LOG4CPLUS_TRACE(this->getApplicationLogger(), "Sending a heartbeat or messenger cache check connection failed, " << ex.what());

	if (heartbeatdURL_ != "")
	{
		if (!heartbeatdMessengerCache_->hasMessenger(heartbeatdURL_))
		{
			try
			{
				heartbeatdMessengerCache_->createMessenger(heartbeatdURL_); // trigger caching of messenger for the remote endpoint
			}
			catch (b2in::utils::exception::Exception& e)
			{
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << heartbeatdURL_.toString() << ", " << e.what());
				this->notifyQualified("error", e);
				return;
			}
		}
		else
		{
			LOG4CPLUS_TRACE(this->getApplicationLogger(), "Network exception, messenger already exists");
		}
	}

	if (ex.name() == "b2in::utils::exception::SendFailure")
	{
		outgoingHeartbeatLostCounter_++;
	}
}

void xmas::heartbeat::probe::Application::heartbeat ()
{
	if (heartbeatdMessengerCache_ == 0)
	{
		outgoingHeartbeatLostCounter_++;
		return;
	}

	// retrieve all local instantiated applications
	std::list<xdaq::Application*> instances = this->getApplicationContext()->getApplicationRegistry()->getApplications();
	for (std::list<xdaq::Application*>::iterator w = instances.begin(); w != instances.end(); w++)
	{

		xdaq::ApplicationDescriptor * descriptor = const_cast<xdaq::ApplicationDescriptor*>((*w)->getApplicationDescriptor());
		const xdata::Properties & attributes = dynamic_cast<const xdata::Properties&>(*descriptor);

		xdata::Properties plist;
		for (std::map<std::string, std::string, std::less<std::string> >::const_iterator i = attributes.begin(); i != attributes.end(); ++i)
		{
			std::string id = "urn:xdaq-application-descriptor:" + (*i).first;
			plist.setProperty(id, (*i).second);
		}

		if (plist.getProperty("urn:xdaq-application-descriptor:heartbeat") == "false")
		{
			// skip this application for heartbeat report 
			continue;
		}

		plist.setProperty("urn:b2in-protocol:service", "heartbeatd");
		plist.setProperty("urn:b2in-protocol:action", "heartbeat");

		if (!heartbeatdMessengerCache_->hasMessenger(heartbeatdURL_))
		{
			try
			{
				heartbeatdMessengerCache_->createMessenger(heartbeatdURL_); // trigger caching of messenger for the remote endpoint
			}
			catch (b2in::utils::exception::Exception& e)
			{
				std::stringstream ss;
				ss << "Failed to create messenger to " << heartbeatdURL_.toString();
				LOG4CPLUS_FATAL(this->getApplicationLogger(), ss.str() << " : " << xcept::stdformat_exception_history(e));
				XCEPT_RETHROW(heartbeat::probe::exception::Exception, ss.str(), e);
				return;
			}
		}

		try
		{
			heartbeatdMessengerCache_->send(heartbeatdURL_, 0, plist);
			outgoingHeartbeatCounter_++;
		}
		catch (b2in::nub::exception::InternalError & e)
		{
			/*
			xcept::ExceptionHistory eh(e);
			if (eh.hasMore())
			{
				if (eh.getPrevious()->getProperty("identifier") == "b2in::nub::exception::ConnectionRequestTimeout")
				{
					LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to send heartbeat to sensord " << ", " << e.what());
					outgoingHeartbeatLostCounter_++;
					return;
				}
			}
			*/
			LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to send heartbeat to sensord " << ", " << e.what());
			outgoingHeartbeatLostCounter_++;
			//monitor->incrementCommunicationLossCounter();
		}
		catch (b2in::nub::exception::QueueFull & e)
		{
			// ignore
			outgoingHeartbeatLostCounter_++;
			//monitor->incrementCommunicationLossCounter();
			LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to send heartbeat to sensord " << ", " << e.what());
		}
		catch (b2in::nub::exception::OverThreshold & e)
		{
			// ignore
			//monitor->incrementCommunicationLossCounter();
			outgoingHeartbeatLostCounter_++;
			LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to send heartbeat to sensord " << ", " << e.what());
		}

	} // for local instantiated
}

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void xmas::heartbeat::probe::Application::TabPanel (xgi::Output * out)
{
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void xmas::heartbeat::probe::Application::StatisticsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Network Status");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	if (heartbeatdMessengerCache_ == 0)
	{
		*out << "idle";
	}
	else
	{
		if (!heartbeatdMessengerCache_->hasMessenger(heartbeatdURL_))
		{
			*out << "no messenger";

		}
		else
		{
			*out << "ready";
		}
	}

	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Outgoing Loss Counter";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingHeartbeatLostCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Total Heartbeat Enqueued 
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total Enqueued Heartbeat";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingHeartbeatCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

void xmas::heartbeat::probe::Application::Default (xgi::Input * in, xgi::Output * out) 
{

	this->TabPanel(out);

}
