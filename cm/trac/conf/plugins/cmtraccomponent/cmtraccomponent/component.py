from trac.core import *
from trac.db import DatabaseManager
from trac.util.html import html
from trac.perm import IPermissionRequestor
from trac.web import IRequestHandler
from trac.web.chrome import add_ctxtnav, INavigationContributor, ITemplateProvider
from trac.env import IEnvironmentSetupParticipant
from trac.ticket import Milestone
from trac.ticket.api import TicketSystem
from trac.db import Table, Column, Index
from trac.util.translation import _

from datetime import datetime
from trac.util.datefmt import to_timestamp, utc

from genshi.builder import tag

import subprocess

class ComponentPluginError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class ComponentPlugin(Component):
	implements(INavigationContributor, IRequestHandler, ITemplateProvider,IPermissionRequestor,IEnvironmentSetupParticipant)

	schema = [
		Table('cm_trac_component_ci', key='identifier')[
			Column('identifier'),                    # Component Identifier
			Column('name'),                          # Human readable name
			Column('creationdate', type='integer'),  # Date when the component was created
			Column('obsoletiondate', type='integer'),# Date when the component was obsoleted
			Column('path'),                          # SVN directory path where the CI is stored
		]
	]
	schema_version = 1

	# IEnvironmentSetupParticipant methods
	def environment_created(self):
		"""Called when a new Trac environment is created."""
		db = self.env.get_db_cnx()
		connector, _ = DatabaseManager(self.env)._get_connector()
		cursor = db.cursor()

		# Create all tables
		for table in self.schema:
			for stmt in connector.to_sql(table):
				cursor.execute(stmt)

		# Insert a global version flag
		cursor.execute("INSERT INTO system (name,value) "
			"VALUES ('cm_trac_component_version',%s)", (self.schema_version,))
		db.commit()

	def environment_needs_upgrade(self,db):
		cursor = db.cursor()
		cursor.execute("SELECT value FROM system WHERE name='cm_trac_component_version'")
		row = cursor.fetchone()
		if not row or int(row[0]) < self.schema_version:
			return True
		return False

	def upgrade_environment(self,db):
		"""Actually perform an environment upgrade, but don't commit as
		that is done by the common upgrade procedure when all plugins are done."""
		cursor = db.cursor()
		cursor.execute("SELECT value FROM system WHERE name='cm_trac_component_version'")
		row = cursor.fetchone()
		if not row:
			self.environment_created()

	# IPermissionRequestor methods
	def get_permission_actions(self):
		return [
			'CM_TRAC_COMPONENT_VIEW',
			'CM_TRAC_COMPONENT_CREATE',
			'CM_TRAC_COMPONENT_DELETE',
			'CM_TRAC_COMPONENT_MODIFY',
      ('CM_TRAC_COMPONENT_ADMIN', ['CM_TRAC_COMPONENT_VIEW', 'CM_TRAC_COMPONENT_CREATE', 'CM_TRAC_COMPONENT_DELETE', 'CM_TRAC_COMPONENT_MODIFY'])
			]

	# INavigationContributor methods
	def get_active_navigation_item(self, req):
		return 'cmtraccomponent'

	def get_navigation_items(self, req):
		if 'CM_TRAC_COMPONENT_VIEW' in req.perm:
			yield ('mainnav', 'component', html.A('Component', href=req.href.cmtraccomponent()))

	# IRequestHandler methods
	def match_request(self, req):
		if req.path_info == '/cmtraccomponent':
			return True
		elif req.path_info == '/cmtraccomponent/create':
			return True
		elif req.path_info.startswith('/cmtraccomponent/view/'):
			return True
		else:
			return False

	def createComponent(self, req):
		req.perm.require('CM_TRAC_COMPONENT_CREATE')
		name        = req.args.get('name')
		identifier  = req.args.get('identifier')
		owner       = req.args.get('owner')
		description = req.args.get('description')
		path        = req.args.get('path')

		db = self.env.get_db_cnx()
		cursor = db.cursor()
		try:
			cursor.execute("INSERT INTO cm_trac_component_ci \
				VALUES ('%s','%s','%ld', '%ld', '%s')" % (identifier, name, to_timestamp(datetime.now(utc)), 0, path))
			cursor.execute("INSERT INTO component \
				VALUES ('%s', '%s', '%s')" % (identifier, owner, description))
			db.commit()
		except:
			db.rollback()
			raise
			#raise ComponentPluginError('Component already existing')

		TicketSystem(self.env).reset_ticket_fields()

		return identifier

	def modifyComponent(self, req):
		req.perm.require('CM_TRAC_COMPONENT_MODIFY')

		identifier = req.args.get('id')
#		newidentifier = req.args.get('identifier')
		name = req.args.get('name')
		owner = req.args.get('owner')
		description = req.args.get('description')
		path = req.args.get('path')
		if req.args.get('obsoletiondate') == 'true':
			now = datetime.now(utc)
			obsoletiondate = to_timestamp(now)
		else:
			obsoletiondate = 0

		db = self.env.get_db_cnx()
		cursor = db.cursor()
		try:
			cursor.execute("UPDATE component \
				SET owner='%s', description='%s' \
				WHERE name = '%s'" % (owner, description, identifier))
			cursor.execute("UPDATE cm_trac_component_ci \
				SET name='%s', path='%s', obsoletiondate='%ld' \
				WHERE identifier = '%s'" % (name, path, obsoletiondate, identifier))
			# no entry (row) modified due to non-existance
			cursor.execute("SELECT * FROM cm_trac_component_ci WHERE identifier = '%s'" % (identifier))
			if cursor.rowcount == 0:
				cursor.execute("INSERT INTO cm_trac_component_ci \
					VALUES ('%s','%s','%ld', '%ld', '%s')" % (identifier, name, to_timestamp(datetime.now(utc)), obsoletiondate, path))
			db.commit()
		except:
			db.rollback()
			raise
			#raise ComponentPluginError('Modification of component '+identifier+' failed')

		#TODO return new identifier
		newidentifier = identifier
		return newidentifier

	def deleteComponent(self, req):
		req.perm.require('CM_TRAC_COMPONENT_DELETE')

		identifier = req.args.get('id')

		db = self.env.get_db_cnx()
		cursor = db.cursor()
		cursor.execute("SELECT count(*) FROM ticket WHERE component='%s'" %(identifier))
		row = cursor.fetchone()
		if not row:
			raise ComponentPluginError('ticket table could not be queried')
		elif int(row[0]) <> 0:
			raise ComponentPluginError('Component '+identifier+' cannot be deleted as there are tickets associated')
		else:
			try:
				cursor.execute("DELETE FROM cm_trac_component_ci \
					WHERE identifier = '%s'" % (identifier))
				cursor.execute("DELETE FROM component \
					WHERE name = '%s'" % (identifier))
				db.commit()
			except:
				db.rollback()
				raise
				#raise ComponentPluginError('Deletion of component '+identifier+' failed')

	def process_request(self, req):
		data = {}
		template = 'cmtraccomponenterrorpage.html'

		add_ctxtnav(req, tag.a(_('View Component List'), href=req.href.cmtraccomponent()))
		add_ctxtnav(req, tag.a(_('Create Component'), href=req.href.cmtraccomponent('create')))
		
		db = self.env.get_db_cnx()

		# check all posts (modify, delete and create)
		if req.method == 'POST':
			action = req.args.get('action')
			if action == 'modify':
				try:
					identifier = self.modifyComponent(req)
					req.redirect(req.href.cmtraccomponent('view/'+identifier))
				except ComponentPluginError, e:
					return ('cmtraccomponenterrorpage.html', {'error': e.__str__()}, None)
			elif action == 'delete':
				try:
					self.deleteComponent(req)
					req.redirect(req.href.cmtraccomponent())
				except ComponentPluginError, e:
					return ('cmtraccomponenterrorpage.html', {'error': e.__str__()}, None)
			elif action == 'create':
				try:
					identifier = self.createComponent(req)
					req.redirect(req.href.cmtraccomponent('view/'+identifier))
				except ComponentPluginError, e:
					return ('cmtraccomponenterrorpage.html', {'error': e.__str__()}, None)
			else:
				msg = "Plugin called with unknown action '%s'" % (action)
				return ('cmtraccomponenterrorpage.html', {'error': msg}, None)

		if req.path_info == '/cmtraccomponent':
			req.perm.require('CM_TRAC_COMPONENT_VIEW')
			template = 'cmtraccomponentlist.html'

			cursor = db.cursor()
			cursor.execute("SELECT component.name,ci.name,ci.obsoletiondate,component.owner \
				FROM component LEFT OUTER JOIN cm_trac_component_ci as ci ON ci.identifier=component.name")
			components = []
			for row in cursor:
				components.append({
					'identifier':     row[0],
					'name':           row[1],
					'obsoletiondate': row[2],
					'owner':          row[3],
				})
			data = {
				'components': components,
			}
		elif req.path_info.startswith('/cmtraccomponent/view/'):
			req.perm.require('CM_TRAC_COMPONENT_VIEW')
			identifier = req.path_info[len('/cmtraccomponent/view/'):]

			# generate view
			cursor = db.cursor()
			cursor.execute("SELECT component.name,ci.name,ci.creationdate, ci.obsoletiondate,component.owner,component.description,ci.path \
				FROM component LEFT OUTER JOIN cm_trac_component_ci as ci ON ci.identifier=component.name \
				WHERE component.name='%s'" % (identifier))

			row = cursor.fetchone()
			if row:
				template = 'cmtraccomponentview.html'

				creationdate = row[2]
				if creationdate == None:
					creationdate = 0

				obsoletiondate = row[3]
				if obsoletiondate == None:
					obsoletiondate = 0

				path = row[6];
				if path == None:
					path = ""

				data = {
					'identifier':     row[0],
					'name':           row[1],
					'creationdate':   creationdate,
					'obsoletiondate': obsoletiondate,
					'owner':          row[4],
					'description':    row[5],
					'path':           path,
				}
		elif req.path_info == '/cmtraccomponent/create':
			req.perm.require('CM_TRAC_COMPONENT_CREATE')

			template = 'cmtraccomponentcreate.html'
			data = {
				'error':       '',
				'name':        '',
				'identifier':  '',
				'owner':       '',
				'path':        '',
				'description': '',
			}

		return (template, data, None)

	# ITemplateProvider methods
	def get_htdocs_dirs(self):
		return []	

	def get_templates_dirs(self):
		"""Return a list of directories containing the provided ClearSilver templates.
		"""
		from pkg_resources import resource_filename
		return [resource_filename('cmtraccomponent', 'templates')]

