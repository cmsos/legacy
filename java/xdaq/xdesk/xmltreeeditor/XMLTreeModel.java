package xmltreeeditor;

import java.io.*;
import javax.xml.parsers.*;

import javax.swing.tree.*;

import org.w3c.dom.*;

public class XMLTreeModel
    extends DefaultTreeModel {

  private Document d1;
  private Document document;
  private DefaultMutableTreeNode n;
  static int count;

   //Constructor by File

  public XMLTreeModel(File file) throws Exception {
    this(getDocument(file)); //call constructor by document
  }

//Constructor by Document

  public XMLTreeModel(Document doc) {
    this(makeRootNode(doc)); //call constructor by TreeNode

  }

//Main constructor by TreeNode

  public XMLTreeModel(DefaultMutableTreeNode treeNode) {
    super(treeNode);
    count = 0;
  }

//create document for saving

  public Document getDocument() {
    document = null;
    try {
      DocumentBuilderFactory builderFactory =
          DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = builderFactory.newDocumentBuilder();
      document = builder.newDocument();
      Element rootElement = makeRootElement();
      document.appendChild(rootElement);

    }
    catch (Exception e) {

    }
    return document;

  }

// create root document element
  private Element
      makeRootElement() {
    Element rootElement = null;
    try {
      DefaultMutableTreeNode rootNode=(DefaultMutableTreeNode) this.getRoot();
      System.out.println("MakeRootelement rootelement " + rootNode.toString());
      rootElement = buildDocument(rootNode);
      System.out.println("MakeRootelement");
    }
    catch (Exception e) {
      System.out.println("Exception" + e.getMessage() + " " + e.getLocalizedMessage());
    }

    return rootElement;
  }

//build all the elements of document
  private Element

      buildDocument(DefaultMutableTreeNode rootNode) {
    Element rootElement=null;
try{
       rootElement = document.createElement(rootNode.toString());
    }
    catch(Exception e){
    System.out.println("buildDocumentException " + e.getMessage());
    }

    addChildrenElement(rootElement, rootNode);
    return (rootElement);
  }

//add childrens to the document elements
  private void addChildrenElement
      (Element parent_Element,
       DefaultMutableTreeNode parent_TreeNode) {

    Element childElement = null;
    for (int i = 0; i < parent_TreeNode.getChildCount(); i++) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) parent_TreeNode.
          getChildAt(i);
      XMLTreeNodeUserObject obj = (XMLTreeNodeUserObject) node.getUserObject();
      if (obj.getType() == 1) {
        childElement = (Element) document.createElement(node.toString());
        parent_Element.appendChild(childElement);
        addChildrenElement(childElement, node);
      }
      else {
        switch (obj.getType()) {
          case Node.TEXT_NODE:
            parent_Element.appendChild(document.createTextNode(obj.getValue()));
            break;
          case Node.COMMENT_NODE:
            parent_Element.appendChild(document.createComment(obj.getValue()));
            break;
          case Node.ATTRIBUTE_NODE:
            parent_Element.setAttribute(obj.getName(),obj.getValue());
            break;
          case Node.CDATA_SECTION_NODE:
            parent_Element.appendChild(document.createCDATASection(obj.
                getValue()));
            break;
          case Node.PROCESSING_INSTRUCTION_NODE:
            parent_Element.appendChild(document.createProcessingInstruction(obj.
                getName(), obj.getValue()));
            break;
        }
      }
    }
  }

//create document from file for parsing to the tree

  private static Document getDocument(File file) {
    Document doc = null;
    try {
      DocumentBuilder builder = DocumentBuilderFactory.newInstance().
          newDocumentBuilder();
      doc = builder.parse(file);
    }
    catch (Exception ex) {}
    return doc;
  }

// creating a root node of the tree

  private static DefaultMutableTreeNode
      makeRootNode(Document document) {
    try {

      document.getDocumentElement().normalize();
      Element rootElement = document.getDocumentElement();
      DefaultMutableTreeNode rootTreeNode =
          buildTree(rootElement);
      return (rootTreeNode);
    }
    catch (Exception e) {
      String errorMessage =
          "Error making root node: " + e;
      System.err.println(errorMessage);
      e.printStackTrace();
      return (new DefaultMutableTreeNode(errorMessage));
    }
  }

//Creating nodes of the tree

  private static DefaultMutableTreeNode
      buildTree(Element rootElement) {
    DefaultMutableTreeNode rootTreeNode = new DefaultMutableTreeNode(new
        XMLTreeNodeUserObject(rootElement));
    addChildren(rootTreeNode, rootElement);
    return (rootTreeNode);
  }

//Adding child nodes

  private static void addChildren
      (DefaultMutableTreeNode parentTreeNode,
       Node parentXMLElement) {

if (count == 0) { //for the first time : add attributes to root node if it has them
      NamedNodeMap attr = parentXMLElement.getAttributes();
      if (attr != null && attr.getLength() > 0) {
        for (int j = 0; j < attr.getLength(); j++) {
          Node attr1 = attr.item(j);
          parentTreeNode.add(new DefaultMutableTreeNode(new
              XMLTreeNodeUserObject(attr1)));
          addChildren(parentTreeNode, (Node) attr.item(j));
        }
      }
      count++;
    }

    NodeList childElements =
        parentXMLElement.getChildNodes();

    for (int i = 0; i < childElements.getLength(); i++) {

      Node childElement = childElements.item(i);

      switch (childElement.getNodeType()) {
        case 1:

          DefaultMutableTreeNode childTreeNode =
              new DefaultMutableTreeNode(new XMLTreeNodeUserObject(childElement));

          NamedNodeMap args = childElement.getAttributes();

          if (args != null && args.getLength() > 0) {
            for (int j = 0; j < args.getLength(); j++) {
              Node attr1 = args.item(j);

              childTreeNode.add(new DefaultMutableTreeNode(new
                  XMLTreeNodeUserObject(attr1)));
              addChildren(childTreeNode, (Node) args.item(j));
            }
          }

          parentTreeNode.add(childTreeNode);
          addChildren(childTreeNode, childElement);

          break;
        case 4:
        case 8:
        case 7:
        case 3:
          if (childElement.getNodeValue().trim().length() != 0) {
            DefaultMutableTreeNode childTreeNode1 =
                new DefaultMutableTreeNode(new XMLTreeNodeUserObject(
                childElement));
            parentTreeNode.add(childTreeNode1);
            addChildren(childTreeNode1, childElement);
          }
          break;
      }

    }
  }

}
