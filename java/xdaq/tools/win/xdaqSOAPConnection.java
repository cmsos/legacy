package xdaq.tools.win;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;

import javax.xml.parsers.*;

import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPException;

public class xdaqSOAPConnection
{
	SOAPConnection con;

	public xdaqSOAPConnection() throws xdaqSOAPException
	{
		try {
			SOAPConnectionFactory scf =
				SOAPConnectionFactory.newInstance();
			con = scf.createConnection();
		} catch (Exception e) {
			throw new xdaqSOAPException ("Cannot create connection.");
		}
	}
	
	public SOAPMessage call (xdaqSOAPMessage msg, String url) throws xdaqSOAPException
	{
		SOAPMessage reply = null;
		
		try {
			URLEndpoint urlEndpoint = new URLEndpoint (url);
			
			//System.out.println ("Message to send:");
			//msg.getSOAPMessage().writeTo (System.out);
			
			reply = con.call ( msg.getSOAPMessage() , urlEndpoint );
			
			if (reply == null ) 
			{
				throw new xdaqSOAPException ("Empty reply");
			}
		} catch (SOAPException e) {
			throw new xdaqSOAPException ( e.getMessage() );
		} catch (Throwable e) {
			throw new xdaqSOAPException ("Send to " + url + " failed.");
		} 
		return reply;
	}
}
