// $Id: version.h,v 1.43 2009/05/28 12:46:19 cschwick Exp $
// tracId : 2243
#ifndef _ferol_version_h_
#define _ferol_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!
#define FEROL_VERSION_MAJOR 2
#define FEROL_VERSION_MINOR 0
#define FEROL_VERSION_PATCH 0
// If any previous versions available E.g. #define FEROL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef FEROL_PREVIOUS_VERSIONS
#define FEROL_PREVIOUS_VERSIONS "1.12.8"
//
// Template macros
//
#define FEROL_VERSION_CODE PACKAGE_VERSION_CODE(FEROL_VERSION_MAJOR,FEROL_VERSION_MINOR,FEROL_VERSION_PATCH)
#ifndef FEROL_PREVIOUS_VERSIONS
#define FEROL_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(FEROL_VERSION_MAJOR,FEROL_VERSION_MINOR,FEROL_VERSION_PATCH)
#else 
#define FEROL_FULL_VERSION_LIST  FEROL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(FEROL_VERSION_MAJOR,FEROL_VERSION_MINOR,FEROL_VERSION_PATCH)
#endif 

namespace ferol
{
	const std::string package  =  "ferol";
	const std::string versions =  FEROL_FULL_VERSION_LIST;
	const std::string description = "Contains the ferol library.";
	const std::string authors = "Christoph Schwick";
	const std::string summary = "CMS Ferol software.";
	const std::string link = "http://makerpmhappy";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
