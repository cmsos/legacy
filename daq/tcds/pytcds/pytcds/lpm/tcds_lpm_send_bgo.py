#!/usr/bin/env python

###############################################################################
## Send a B-go from an iPM/iCI.
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BGO_NUMBERS
from pytcds.utils.tcds_constants import ICI_NUMBERS
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def parse_as_bgo_number(bgo_number_string):
    bgo_number = None
    try:
        bgo_number = BGO_NUMBERS[bgo_number_string]
    except KeyError:
        try:
            bgo_number = int(bgo_number_string)
        except ValueError:
            msg = "Could not translate '{0:s}' into a B-go number."
            raise ValueError(msg.format(bgo_number_string))

    # ASSERT ASSERT ASSERT
    assert (bgo_number >= 0) and (bgo_number <= 15), \
        "Invalid B-go number: {0:d}.".format(bgo_number)
    # ASSERT ASSERT ASSERT end

    # End of parse_as_bgo_number().
    return bgo_number

###############################################################################

class BgoSender(CmdLineBase):

    def pre_hook(self):
        self.ici_number_string = None
        self.ici_number = None
        self.bgo_number_string = None
        self.bgo_number = None
        # End of pre_hook().

    def handle_args(self):
        # Three arguments are expected. The first argument has to
        # sound like an IP address.
        if len(self.args) != 3:
            msg = "Three arguments are required: " \
                  "the IP address of the device, " \
                  "the iCI number ({0:s}), and " \
                  "the B-go number to send."
            self.error(msg.format(", ".join(sorted(ICI_NUMBERS.keys()))))
        else:
            # Extract the IP address.
            ip_address_arg = self.args[0]
            ip_address = resolve_network_address(ip_address_arg)
            if not ip_address:
                msg = "'{0:s}' does not sound like a valid network address."
                msg = msg.format(ip_address_arg)
                self.error(msg)
            else:
                self.ip_address = ip_address
            # Extract the iCI number.
            self.ici_number_string = self.args[1]
            try:
                self.ici_number = ICI_NUMBERS[self.ici_number_string]
            except KeyError:
                msg = "Failed to interpret '{0:s}' as a valid iCI number. " \
                      "Options are: {1:s}."
                self.error(msg.format(self.ici_number_string,
                                      ", ".join(sorted(ICI_NUMBERS.keys()))))
            # Extract the B-go number.
            self.bgo_number_string = self.args[2]
            try:
                self.bgo_number = parse_as_bgo_number(self.bgo_number_string)
            except Exception:
                msg = "Failed to interpret '{0:s}' as a B-go number."
                self.error(msg.format(self.bgo_number_string))
        # End of handle_args().

    def main(self):
        lpm = get_lpm_hw(self.ip_address, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            bgo_number_string = str(self.bgo_number)
            if self.bgo_number_string != bgo_number_string:
                bgo_number_string = "{0:s} (B-go #{1:d})".format(self.bgo_number_string,
                                                                 self.bgo_number)
            if self.ici_number == 0:
                msg = "Sending B-go {0:s} through the iPM."
                print msg.format(bgo_number_string)
            else:
                msg = "Sending B-go {0:s} through iCI #{1:d}."
                print msg.format(bgo_number_string, self.ici_number)

        #----------

        lpm.enable_ipbus_requests(self.ici_number)
        lpm.send_ipbus_bgo(self.ici_number, self.bgo_number)
        lpm.disable_ipbus_requests(self.ici_number)

        #----------

        # End of main().

    # End of class BgoSender.

###############################################################################

if __name__ == "__main__":

    description = "Send a B-go from an iPM/iCI."
    usage = "usage: %prog [options] IP ICI_NUMBER BGO_NUMBER"
    epilog = "ICI_NUMBER: {0:s}.".format(", ".join(sorted(ICI_NUMBERS.keys())))
    res = BgoSender(description, usage, epilog).run()
    print "Done"
    sys.exit(res)

###############################################################################
