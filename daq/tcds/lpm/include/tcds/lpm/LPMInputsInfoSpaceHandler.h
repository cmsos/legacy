#ifndef _tcds_lpm_LPMInputsInfoSpaceHandler_h_
#define _tcds_lpm_LPMInputsInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace lpm {

    class LPMInputsInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      LPMInputsInfoSpaceHandler(xdaq::Application& xdaqApp,
                                tcds::utils::InfoSpaceUpdater* updater);
      virtual ~LPMInputsInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace lpm
} // namespace tcds

#endif // _tcds_lpm_LPMInputsInfoSpaceHandler_h_
