#ifndef _tcds_ttcspy_version_h_
#define _tcds_ttcspy_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSTTCSPY_VERSION_MAJOR 3
#define TCDSTTCSPY_VERSION_MINOR 13
#define TCDSTTCSPY_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSTTCSPY_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSTTCSPY_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSTTCSPY_VERSION_CODE PACKAGE_VERSION_CODE(TCDSTTCSPY_VERSION_MAJOR,TCDSTTCSPY_VERSION_MINOR,TCDSTTCSPY_VERSION_PATCH)
#ifndef TCDSTTCSPY_PREVIOUS_VERSIONS
#define TCDSTTCSPY_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSTTCSPY_VERSION_MAJOR,TCDSTTCSPY_VERSION_MINOR,TCDSTTCSPY_VERSION_PATCH)
#else
#define TCDSTTCSPY_FULL_VERSION_LIST TCDSTTCSPY_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSTTCSPY_VERSION_MAJOR,TCDSTTCSPY_VERSION_MINOR,TCDSTTCSPY_VERSION_PATCH)
#endif

namespace tcdsttcspy
{
  const std::string package = "tcdsttcspy";
  const std::string versions = TCDSTTCSPY_FULL_VERSION_LIST;
  const std::string description = "CMS software to spy on TTC signals using the CERN GLIB board.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software to spy on TTC signals using the CERN GLIB board.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
