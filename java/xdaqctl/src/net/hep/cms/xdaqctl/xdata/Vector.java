package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Typical user code:
 *
 * <pre>
 * Vector vector = new Vector();
 *
 * Integer element = new Integer();   
 * for (int i = 0; i < 10; ++i) {
 *   element.setValue(i);
 *   vector.add(element);
 * }
 * 
 * if (vector.size() < 5) { return; }
 * int i = vector.get(4).getValue();
 * System.out.println("vector[4] is " + i);
 * </pre>
 *
 * @author A. Oh &lt;Alexander.Oh@cern.ch&gt; and I. Suzuki &lt;ichiro@fnal.gov&gt;
 */

public class Vector extends XDataItem {
	
	private java.util.Vector<XDataItem> value;
	
	public Vector() {
		value = new java.util.Vector<XDataItem>();
	}
	
	/**
	 * append an item at the end.
	 * </p>
	 * Bug: it doesn't check if the type is same as other existing items.
	 *
	 * @param item item to add
	 * @return true
	 * @see java.util.Vector#add
	 */
	public boolean add(XDataItem item) {
		return value.add(item);
	}
	
	/**
	 * @see java.util.Vector#get
	 */
	public XDataItem get(int index) {
		return value.get(index);
	}
	
	public int size() {
		return value.size();
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		// XDAQ writes only 32bit size of a vector.
		out.writeInt(value.size());
		
		for (XDataItem item: value) {
			item.writeEXDR(out);
		}
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		
		value.clear();
		
		int size = in.readInt();
		
		for (int i = 0; i < size; ++i) {
			in.mark(8);
			int exdrCode = in.readInt();
			in.reset();
			
			XDataItem item = XDataType.getInstance(exdrCode);
			item.readEXDR(in);
			value.add(item);
		}
	}

	@Override
	int getEXDRCode() {
		return XDataType.VECTOR.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:

