// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _psx_sapi_VariableFactory_h_
#define _psx_sapi_VariableFactory_h_

#include "Variable.hxx" 

#include "psx/sapi/exception/Exception.h"

#include <string>

namespace psx {
	namespace sapi {
	
		class VariableFactory
		{
			public:
			
			static Variable* createVariable(VariableType vt, const std::string& val)
				;
				
			static std::string toString(Variable * v);

		};
	
	}
}

#endif
