#include "amc13controller/StatusInfoSpaceHandler.hh"
#include "d2s/utils/loggerMacros.h"

amc13controller::StatusInfoSpaceHandler::StatusInfoSpaceHandler( xdaq::Application* xdaq, 
                                                       utils::InfoSpaceUpdater *updater )
    : AMC13BaseInfoSpaceHandler( xdaq, "Status_IS", updater )
{
//Application state:
createuint32("testCounter", 0, "", NOUPDATE, "This is a dummy parameter which is counting up to see that the AJAX monitoring is working. It is useless to set this parameter to some value.");
createuint32( "instance", 0, "", NOUPDATE );
createuint32( "slotNumber", 0, "", NOUPDATE );
createstring( "stateName", "uninitialized" );
createstring( "subState", "" );
createstring( "failedReason", "");
createstring( "lockStatus", "unknown" );


//----------3_Control----------
createuint32( "Reg_3_DAQ", 0, "hex", UPDATE, "'1' to ignore DAQ data (TTS still working)", 0);

//----------TTC_BGO----------
createuint32( "Command_BGO0", 0, "hex", UPDATE, "Short or long TTC command for BGO channel 0", 1);
createuint32( "Command_BGO1", 0, "hex", UPDATE, "Short or long TTC command for BGO channel 1", 1);
createuint32( "Command_BGO2", 0, "hex", UPDATE, "Short or long TTC command for BGO channel 2", 1);
createuint32( "Command_BGO3", 0, "hex", UPDATE, "Short or long TTC command for BGO channel 3", 1);
createuint32( "Enable_BGO0", 0, "hex", UPDATE, "Enable BGO channel 0 for repeated command", 1);
createuint32( "Enable_BGO1", 0, "hex", UPDATE, "Enable BGO channel 1 for repeated command", 1);
createuint32( "Enable_BGO2", 0, "hex", UPDATE, "Enable BGO channel 2 for repeated command", 1);
createuint32( "Enable_BGO3", 0, "hex", UPDATE, "Enable BGO channel 3 for repeated command", 1);
createuint32( "Enable_Repeat_BGO0", 0, "hex", UPDATE, "Enable BGO channel 0 for repeated command", 1);
createuint32( "Enable_Repeat_BGO1", 0, "hex", UPDATE, "Enable BGO channel 1 for repeated command", 1);
createuint32( "Enable_Repeat_BGO2", 0, "hex", UPDATE, "Enable BGO channel 2 for repeated command", 1);
createuint32( "Enable_Repeat_BGO3", 0, "hex", UPDATE, "Enable BGO channel 3 for repeated command", 1);
createuint32( "L1A_after_BGO0", 0, "hex", UPDATE, "'1' for L1A after TTC command", 1);
createuint32( "L1A_after_BGO1", 0, "hex", UPDATE, "'1' for L1A after TTC command", 1);
createuint32( "L1A_after_BGO2", 0, "hex", UPDATE, "'1' for L1A after TTC command", 1);
createuint32( "L1A_after_BGO3", 0, "hex", UPDATE, "'1' for L1A after TTC command", 1);
createuint32( "L1A_offset_ALL", 0, "hex", UPDATE, "Delay from end of TTC command to L1A if enabled in BX", 1);
createuint32( "Long_Command_BGO0", 0, "hex", UPDATE, "'1' for long command on BGO channel 0", 1);
createuint32( "Long_Command_BGO1", 0, "hex", UPDATE, "'1' for long command on BGO channel 1", 1);
createuint32( "Long_Command_BGO2", 0, "hex", UPDATE, "'1' for long command on BGO channel 2", 1);
createuint32( "Long_Command_BGO3", 0, "hex", UPDATE, "'1' for long command on BGO channel 3", 1);
createuint32( "Orbit_Prescale_BGO0", 0, "hex", UPDATE, "Orbit prescale-1 for BGO channel 0", 1);
createuint32( "Orbit_Prescale_BGO1", 0, "hex", UPDATE, "Orbit prescale-1 for BGO channel 1", 1);
createuint32( "Orbit_Prescale_BGO2", 0, "hex", UPDATE, "Orbit prescale-1 for BGO channel 2", 1);
createuint32( "Orbit_Prescale_BGO3", 0, "hex", UPDATE, "Orbit prescale-1 for BGO channel 3", 1);
createuint32( "Single_BGO0", 0, "hex", UPDATE, "'1' for single command only", 1);
createuint32( "Single_BGO1", 0, "hex", UPDATE, "'1' for single command only", 1);
createuint32( "Single_BGO2", 0, "hex", UPDATE, "'1' for single command only", 1);
createuint32( "Single_BGO3", 0, "hex", UPDATE, "'1' for single command only", 1);
createuint32( "Starting_BX_BGO0", 0, "hex", UPDATE, "Starting BX for BGO channel 0", 1);
createuint32( "Starting_BX_BGO1", 0, "hex", UPDATE, "Starting BX for BGO channel 1", 1);
createuint32( "Starting_BX_BGO2", 0, "hex", UPDATE, "Starting BX for BGO channel 2", 1);
createuint32( "Starting_BX_BGO3", 0, "hex", UPDATE, "Starting BX for BGO channel 3", 1);

//----------2_Control----------
createuint32( "Local_Trigger_BurstN", 0, "hex", UPDATE, "Local L1A burst length (N+1) so =0 means 1 L1A", 0);
createuint32( "Local_Trigger_Mode", 0, "hex", UPDATE, "Local L1A type: 0=per orbit 2=per BX 3=random", 0);
createuint32( "Local_Trigger_Rate", 0, "hex", UPDATE, "Local L1A rate. L1A every N+1 orbits@BcN=0x1f4, N+1 BX or 2*N's random", 0);
createuint32( "Local_Trigger_Rules", 0, "hex", UPDATE, "Local L1A trig rules: 0=all, 1=all but rule 4, 2=rules 1+2, 3=only rule 1", 0);
createuint32( "Reg_1_BGO", 0, "hex", UPDATE, "'1' enables locally generated 'BGO' commands", 1);
createuint32( "Reg_1_DAQ", 0, "hex", UPDATE, "'1' enables DAQLSC", 1);
createuint32( "Reg_1_DAQTTS", 0, "hex", UPDATE, "Enable warning due to daq TTS state (0x6)", 1);
createuint32( "Reg_1_EnaMaskedEvn", 0, "hex", UPDATE, "If set to '1', bit 22-19 determine which events will be saved", 0);
createuint32( "Reg_1_Fake", 0, "hex", UPDATE, "if '1', generate fake event upon receiving L1A", 1);
createuint32( "Reg_1_ForceCRC", 0, "hex", UPDATE, "If '1', inject CRC errors when Evn[7:0] = 0xff for test", 1);
createuint32( "Reg_1_LTrig", 0, "hex", UPDATE, "if '1', uses internally generated L1A", 1);
createuint32( "Reg_1_MT32", 0, "hex", UPDATE, "if '0', memory test uses 64bit PRBS. If '1', uses 32 bit sequencial numbers.", 1);
createuint32( "Reg_1_MemTest", 0, "hex", UPDATE, "'1' enables memory self test", 1);
createuint32( "Reg_1_MonBP", 0, "hex", UPDATE, "if '1', monitor buffer full will stop event builder", 1);
createuint32( "Reg_1_MonOverwrite", 0, "hex", UPDATE, "If '1', overwrite old events in monitor buffer when full", 1);
createuint32( "Reg_1_NoFakeResync", 0, "hex", UPDATE, "If '1', disable fake event generation after TTC resync", 1);
createuint32( "Reg_1_Run", 0, "hex", UPDATE, "run mode", 1);
createuint32( "Reg_1_Stop", 0, "hex", UPDATE, "if '1', pauses event building. For debugging only", 1);
createuint32( "Reg_1_T3Trig", 0, "hex", UPDATE, "If '1', accept triggers from T3 (ENABLE_INTERNAL_L1A must also be '1')", 1);
createuint32( "Reg_1_TTCLoop", 0, "hex", UPDATE, "if '1', TTS output is 80MHz clock which can be looped back to TTC input", 1);
createuint32( "Reg_1_TTSTest", 0, "hex", UPDATE, "if '1', TTS test mode (outputs from TTS_TEST_PATTERN)", 1);
createuint32( "Reg_2_Prescale", 0, "hex", UPDATE, "scale factor( = contents + 1). Note: if bit 18 is set to '1', these bits are ignored.", 0);
createuint32( "Reg_2_SelMaskedEvn", 0, "hex", UPDATE, "Value n=0..15, save events with EvN low (20-n) bits '0'", 0);
createuint32( "Reg_2_StopOnCRC", 0, "hex", UPDATE, "If '1', stop monitor buffer write on AMC CRC error", 2);

//----------Monitor_Buffer----------
createuint32( "Available_Value", 0, "hex", UPDATE, "monitor buffer available", 2);
createuint32( "EOI_Type_Value", 0, "hex", UPDATE, "all 0 if not in catch mode, otherwise gives the type of error of the bad event", 9);
createuint32( "Empty_Value", 0, "hex", UPDATE, "monitor buffer empty", 2);
createuint32( "Evt_After_EOI_Value", 0, "hex", UPDATE, "all 0 if not in catch mode, otherwise gives the number of events stored after the bad event", 9);
createuint32( "Full_Value", 0, "hex", UPDATE, "monitor buffer full", 1);
createuint32( "Overflow_Warning_Value", 0, "hex", UPDATE, "monitor buffer overflow", 1);
createuint32( "Unread_Blk_Count", 0, "hex", UPDATE, "number of unread blocks captured by monitor", 1);
createuint32( "Words_SFP0", 0, "hex", UPDATE, "SFP 0 monitored event size in 32-bit word. 0 if no data available", 1);
createuint32( "Words_SFP1", 0, "hex", UPDATE, "SFP 1 monitored event size in 32-bit word. 0 if no data available", 1);
createuint32( "Words_SFP2", 0, "hex", UPDATE, "SFP 2 monitored event size in 32-bit word. 0 if no data available", 1);

//----------TTC_Rx----------
createuint64( "Bcnt_Error_Count", 0, "hex", UPDATE, "TTC BC0 error counter", 1);
createuint32( "Dbuffer_command_Value", 0, "hex", UPDATE, "Double buffer TTC command", 2);
createuint32( "Dbuffer_mask_Value", 0, "hex", UPDATE, "Ignore '1' bits when comparing dbl buffer TTC command", 2);
createuint64( "Mult_Bit_error_Count", 0, "hex", UPDATE, "TTC multi-bit error counter", 1);
createuint32( "OCR_command_Value", 0, "hex", UPDATE, "Orbit count reset TTC command", 2);
createuint32( "OCR_mask_Value", 0, "hex", UPDATE, "Ignore '1' bits when comparing orbit count reset TTC command", 2);
createuint64( "Resync_Count", 0, "hex", UPDATE, "Count of TTC resync commands received", 1);
createuint32( "Resync_Count_obs", 0, "hex", UPDATE, "Count of TTC resync commands received (obsolete)", 1);
createuint32( "Resync_command_Value", 0, "hex", UPDATE, "Resync TTC command", 2);
createuint32( "Resync_mask_Value", 0, "hex", UPDATE, "Ignore '1' bits when comparing resync TTC command", 2);
createuint64( "Sgl_Bit_Error_Count", 0, "hex", UPDATE, "TTC single bit error counter", 1);
createuint32( "State_BCNT", 0, "hex", UPDATE, "TTC bcnt error", 1);
createuint32( "State_Enc", 0, "TTSEnc", ALWAYSUPDATE, "encoded TTS from enabled AMCs", 1);
createuint32( "State_MULT", 0, "hex", UPDATE, "TTC multi-bit error", 1);
createuint32( "State_OFW", 0, "hex", UPDATE, "L1A overflow warning", 1);
createuint32( "State_Raw", 0, "TTSRaw", ALWAYSUPDATE, "Current T1 overall TTS state", 1);
createuint32( "State_SGL", 0, "hex", UPDATE, "TTC single bit error", 1);
createuint32( "State_SYN", 0, "hex", UPDATE, "TTC sync lost (L1A buffer overflow)", 1);
createuint32( "State_nRDY", 0, "hex", UPDATE, "TTC not ready", 1);

//----------Slink_Express----------
createuint32( "Any_Down_L", 0, "hex", UPDATE, "reads '1' when any of the enabled SFP ports is down", 2);
createuint32( "LOS_or_LOL_TTC", 0, "hex", UPDATE, "'1' indicates TTC_LOS or TTC_LOL", 2);
createuint32( "SFP_ABSENT_TTC", 0, "hex", UPDATE, "'1' indicates TTC/TTS SFP absent", 2);
createuint32( "TX_FAULT_TTC", 0, "hex", UPDATE, "'1' indicates TTS TxFault", 2);

//----------Event_Builder----------
createuint64( "L1A_Count", 0, "hex", UPDATE, "L1A counter", 1);
createuint32( "L1A_RateHz", 0, "dec", UPDATE, "L1A rate in Hz", 1);

//----------1_Config----------
createuint32( "AMC_Links_BC0_Offset", 0, "hex", UPDATE, "AMC set BC0 compensation, default to 0x18", 0);
createuint32( "AMC_Links_Enable_Mask", 0, "hex", UPDATE, "'1' enables AMC1..12", 0);
createuint32( "AMC_Links_Fake_size", 0, "hex", UPDATE, "Number of 64 bit words send for fake event payload", 1);
createuint32( "Calib_Trig_Enable", 0, "hex", UPDATE, "Enable calibration events in orbit gap (HCAL), default '1'", 0);
createuint32( "Calib_Trig_Lower_BX", 0, "hex", UPDATE, "read only entire lower window limit", 0);
createuint32( "Calib_Trig_Upper_BX", 0, "hex", UPDATE, "read only entire upper window limit", 0);
createuint32( "General_BC0_Offset", 0, "hex", UPDATE, "BcN offset", 0);
createuint32( "General_BC0_Once", 0, "hex", UPDATE, "if '1'', TTC BC0 command works only once after reset", 0);
createuint32( "General_OrN_Offset", 0, "hex", UPDATE, "OrN offset", 0);
createuint32( "HCAL_Trigger_TTS_Disable", 0, "hex", UPDATE, "Disable corresponding AMCs TTS input signal", 0);
createuint32( "HCAL_Trigger_Trigger_Mask", 0, "hex", UPDATE, "AMC Trigger Mask Register", 0);
createuint32( "Output_SrcID", 0, "hex", UPDATE, "CMS Source ID for output data", 1);
createuint32( "SFP_DisableTTS", 0, "hex", UPDATE, "'1' disables TTS transmitter", 0);
createuint32( "SFP_DisableTX", 0, "hex", UPDATE, "'1' disables SFP0..2 transmitter", 0);
createuint32( "SFP_Enable", 0, "hex", UPDATE, "'1' enables SFP0..2", 0);
createuint32( "SrcID_SFP0", 0, "hex", UPDATE, "CMS Source ID for SFP0 output data", 1);
createuint32( "SrcID_SFP1", 0, "hex", UPDATE, "CMS Source ID for SFP1 output data", 1);
createuint32( "SrcID_SFP2", 0, "hex", UPDATE, "CMS Source ID for SFP2 output data", 1);
createuint32( "TTS_TestPatt", 0, "TTSRaw", UPDATE, "TTS output test pattern", 0);

//----------State_Timers----------
createuint64( "Busy_Count", 0, "hex", UPDATE, "busy time counter", 1);
createuint64( "L1A_in_BSY_Count", 0, "hex", UPDATE, "L1A received when in BSY state", 1);
createuint64( "L1A_in_OFW_Count", 0, "hex", UPDATE, "L1A received when in OFW state", 1);
createuint64( "L1A_in_SYN_Count", 0, "hex", UPDATE, "L1A received when in SYN state", 1);
createuint64( "OF_WARN_DAQ_BP_Count", 0, "hex", UPDATE, "L1A overflow warning with DAQ backpressure on time counter", 1);
createuint64( "Overflow_Warning_Count", 0, "hex", UPDATE, "L1A overflow warning time counter", 1);
createuint32( "READY_Percent", 0, "dec", ALWAYSUPDATE, "Live time (TTS ready) percent", 1);
createuint64( "Ready_Count", 0, "hex", UPDATE, "ready time counter", 1);
createuint64( "Run_Count", 0, "hex", ALWAYSUPDATE, "run time counter", 1);
createuint64( "Sync_Lost_Count", 0, "hex", UPDATE, "L1A sync lost time counter", 1);
createuint64( "TTS_STATE_9_INSTANCE_Count", 0, "hex", UPDATE, "TTS state 0x9 instance counter", 1);
createuint64( "TTS_STATE_9_TIME_Count", 0, "hex", UPDATE, "TTS state 0x9 time counter", 1);
createuint64( "TTS_STATE_A_INSTANCE_Count", 0, "hex", UPDATE, "TTS state 0xa instance counter", 1);
createuint64( "TTS_STATE_A_TIME_Count", 0, "hex", UPDATE, "TTS state 0xa time counter", 1);
createuint64( "TTS_STATE_B_INSTANCE_Count", 0, "hex", UPDATE, "TTS state 0xb instance counter", 1);
createuint64( "TTS_STATE_B_TIME_Count", 0, "hex", UPDATE, "TTS state 0xb time counter", 1);

//----------AMC13_Errors----------
createuint32( "DDR_Not_Ready_V", 0, "hex", UPDATE, "if 0, DDR memory reset done", 1);

//----------Temps_Voltages----------
createuint32( "0V75a_mV", 0, "dec", UPDATE, "0.75V DDR3_Vref power voltage in millivolt", 3);
createuint32( "0V75b_mV", 0, "dec", UPDATE, "0.75V DDR3_Vtt power voltage in millivolt", 3);
createuint32( "12V0_mV", 0, "dec", UPDATE, "12V power voltage in millivolt", 3);
createuint32( "1V0a_mV", 0, "dec", UPDATE, "1.0V analog power voltage in millivolt", 3);
createuint32( "1V0b_mV", 0, "dec", UPDATE, "1.0V VccBRAM power voltage in millivolt", 3);
createuint32( "1V0c_mV", 0, "dec", UPDATE, "1.0V Vccint power voltage in millivolt", 3);
createuint32( "1V2_mV", 0, "dec", UPDATE, "1.2V analog power voltage in millivolt", 3);
createuint32( "1V5_mV", 0, "dec", UPDATE, "1.5V power voltage in millivolt", 3);
createuint32( "1V8a_mV", 0, "dec", UPDATE, "1.8V VccAux power voltage in millivolt", 3);
createuint32( "1V8b_mV", 0, "dec", UPDATE, "1.8V VccAuxGTX power voltage in millivolt", 3);
createuint32( "2V0_mV", 0, "dec", UPDATE, "2.0V VccAuxIO power voltage in millivolt", 3);
createuint32( "2V5_mV", 0, "dec", UPDATE, "2.5V power voltage in millivolt", 3);
createuint32( "3V3_mV", 0, "dec", UPDATE, "3.3V power voltage in millivolt", 3);
createuint32( "T_Die_Temp_Deg_C", 0, "dec", UPDATE, "V6 die temperature in unit of 0.1 degree Celsius", 3);

//----------Local_Trigger----------
createuint32( "Continuous_On_Value", 0, "hex", UPDATE, "continous local L1A on (setup with register 0x1c)", 3);
createuint32( "Orbit_Gap_Begin", 0, "hex", UPDATE, "Beginning of orbit gap; triggers excluded", 2);
createuint32( "Orbit_Gap_End", 0, "hex", UPDATE, "End of orbit gap; triggers excluded", 2);

//----------DT_Trig----------
createuint32( "Input_Delay_AMC_00", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_01", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_02", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_03", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_04", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_05", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_06", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_07", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_08", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_09", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_10", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_AMC_11", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for AMC input", 1);
createuint32( "Input_Delay_TRIG0", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for TRIG0 input", 1);
createuint32( "Input_Delay_TRIG1", 0, "hex", UPDATE, "Set delay in 1/8 BX steps for TRIG1 input", 1);
createuint32( "LUT_word_Value", 0, "hex", UPDATE, "Look-up table for trigger inputs (512 words)", 1);
createuint32( "Sample_buffer_enable_Value", 0, "hex", UPDATE, "Trigger time alignment sample buffer when 1", 1);
createuint32( "Trigger_enable_Value", 0, "hex", UPDATE, "Enable DT LUT trigger", 1);

//----------SFP_ROM----------
createuint32( "ROM_Data_SFP0", 0, "hex", UPDATE, "SFP0 ROM data(first 128 bytes, little endian)", 9);
createuint32( "ROM_Data_SFP1", 0, "hex", UPDATE, "SFP1 ROM data(first 128 bytes, little endian)", 9);
createuint32( "ROM_Data_SFP2", 0, "hex", UPDATE, "SFP2 ROM data(first 128 bytes, little endian)", 9);
createuint32( "ROM_Data_TTC", 0, "hex", UPDATE, "TTC/TTS SFP ROM data(first 128 bytes, little endian)", 9);

//----------0_Board----------
createuint64( "Info_DNA", 0, "hex", NOUPDATE, "Kintex FPGA DNA", 3);
createuint32( "Info_Init_B", 0, "hex", NOUPDATE, "if '1', virtex chip INIT_B is low", 4);
createuint32( "Info_SerialNo", 0, "dec", NOUPDATE, "T1 board SN", 1);
createuint32( "Info_T1_Done", 0, "hex", NOUPDATE, "if '1', virtex chip DONE is low", 4);
createuint32( "Info_T1_Ver", 0, "hex", NOUPDATE, "read only Virtex firmware version", 1);
createuint64( "Info_T2_DNA", 0, "hex", NOUPDATE, "Kintex FPGA DNA", 2);
createuint32( "Info_T2_Rev", 0, "hex", NOUPDATE, "T2 Firmware Version Number", 1);
createuint32( "Info_T2_SerNo", 0, "hex", NOUPDATE, "T2 Serial Number", 1);
createuint32( "Output_FED", 0, "dec", NOUPDATE, "SLINK ID (bits 17-16 always '0')", 1);
createuint64( "info_SerialNo", 0, "dec", NOUPDATE, "Serial Number Low eight bits", 1);

//----------T2_TTC----------
createuint32( "Config_OcR_cmd", 0, "hex", UPDATE, "TTC OrN  reset command", 3);
createuint32( "Config_OcR_mask", 0, "hex", UPDATE, "TTC OrN reset mask", 3);
createuint32( "Config_TTC_enable_override_mask", 0, "hex", UPDATE, "Bitmask of additional locations to send TTC clock/data", 2);
createuint32( "Value_BC0_Run", 0, "hex", UPDATE, "BC0 counter (always running)", 3);
createuint32( "Value_BcN_Run", 0, "hex", UPDATE, "Bunch count (always running)", 3);
createuint32( "Value_BcntErr", 0, "hex", UPDATE, "TTC Bunch count error counter (8 bits only)", 1);
createuint32( "Value_ClkFreq", 0, "dec", UPDATE, "TTC clock freq divided by 50", 3);
createuint32( "Value_L1A_Count", 0, "hex", UPDATE, "L1A counter", 3);
createuint32( "Value_Last_BcN", 0, "hex", UPDATE, "TTC bunch count of last received L1A", 3);
createuint32( "Value_Last_L1A", 0, "hex", UPDATE, "TTC event number of last received L1A", 3);
createuint32( "Value_Last_OrN", 0, "hex", UPDATE, "TTC orbit number of last received L1A", 3);
createuint32( "Value_MultiErr", 0, "hex", UPDATE, "TTC multi bit error count (8 bits only)", 1);
createuint32( "Value_SglErr", 0, "hex", UPDATE, "TTC single bit error count (8 bits only)", 1);

//----------AMC_Links----------
createuint32( "BC0_Offset_Value", 0, "hex", UPDATE, "Set BX offset for AMC inputs", 3);
createuint32( "CRC_Err_Any", 0, "hex", UPDATE, "AMC even CRC error detected", 1);
}
