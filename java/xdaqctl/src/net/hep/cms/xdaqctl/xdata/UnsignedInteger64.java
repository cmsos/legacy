package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class UnsignedInteger64 extends SimpleItem {
	
	private java.lang.Long value;
	
	@Override
	public java.lang.Long getValue() {
		return value;
	}
	
	/**
	 * @param value java Long is used as an internal representation
	 *    Use a nagative number (like, -0x8000000000000000) to set the MSB.
	 */
	@Override
	public void setValue(Object value) {
		this.value = ((Number)value).longValue();
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		out.writeLong(value);
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = in.readLong();
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.UNSIGNED_INTEGER64.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
