/************************************************************************/
/*									*/
/*  This is the common header file for the CMEM_RCC driver & library	*/
/*									*/
/*  12. Dec. 01  MAJO  created						*/
/*									*/
/*******C 2005 - The software with that certain something****************/

#ifndef _CMEM_RCC_IOCTL_H
#define _CMEM_RCC_IOCTL_H

#include "cmem_rcc_common.h"

// Constants
#define MAX_BUFFS          1000    // Max. number of buffers for all processes
#define MAX_PROC_TEXT_SIZE 0x10000 //The output of "more /proc/cmem_rcc" must not generate more characters than that

/********/
/*Macros*/
/********/
#ifdef DRIVER_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef DRIVER_ERROR
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif


// Types
typedef struct
{
  u_long paddr;
  u_long kaddr;
  u_long uaddr;
  u_int size;
  u_int locked;
  u_int order;
  u_int type;
  u_int used;
  int pid;
  char name[40];
} buffer_t;

typedef struct
{
  u_int buffer[MAX_BUFFS];
} private_stuff;  

struct cmem_proc_data_t
{
  char name[10];
  char value[100];
};

typedef struct range_struct 
{
  struct range_struct *next;
  caddr_t base;			// base of allocated block 
  size_t  size;			// size in bytes 
} range_t;

#if LINUX_VERSION_CODE > KERNEL_VERSION(2,6,36)
#define UNLOCKED 1
#endif

/******************************/
/*Standard function prototypes*/
/******************************/
static int cmem_rcc_open(struct inode *inode, struct file *file);
static int cmem_rcc_release(struct inode *inode, struct file *file);
#ifdef UNLOCKED
static long cmem_rcc_ioctl(struct file *file,unsigned int cmd, unsigned long arg);
#else
static int cmem_rcc_ioctl(struct inode *inode, struct file *file, u_int cmd, u_long arg);
#endif
static int cmem_rcc_mmap(struct file *file, struct vm_area_struct *vma);
#ifdef UNLOCKED
static ssize_t cmem_rcc_proc_write(struct file *filp,const char *buf,size_t count,loff_t *offp);
static ssize_t cmem_rcc_proc_read(struct file *filp,char *buf,size_t count,loff_t *offp);
#else
static int cmem_rcc_proc_write(struct file *file, const char *buffer, u_long count, void *data);
static int cmem_rcc_proc_read(char *buf, char **start, off_t offset, int count, int *eof, void *data);
#endif

/*****************************/
/*Service function prototypes*/
/*****************************/
static int membpa_init2(int priority, u_int btype);
static int gfpbpa_init(void);
static caddr_t membpa_alloc_pages(int count, int align, int priority, u_int btype);
static void membpa_free_pages(caddr_t base, u_int btype);
static void cmem_rcc_vmaClose(struct vm_area_struct *vma);
static void cmem_rcc_vmaOpen(struct vm_area_struct *vma);

#endif

