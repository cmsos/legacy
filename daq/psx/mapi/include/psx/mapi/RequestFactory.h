// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _psx_mapi_RequestFactory_h_
#define _psx_mapi_RequestFactory_h_

#include "xoap/domutils.h"
#include "psx/mapi/exception/Exception.h"
#include "psx/mapi/Request.h"   
#include "xoap/SOAPMessage.h"
#include "xoap/MessageReference.h"

namespace psx {
	namespace mapi {
	
		class ApplicationService;
	
		class RequestFactory
		{
			public:
			
			static psx::mapi::Request* createRequest(xoap::MessageReference message,
										psx::mapi::ApplicationService* as) 
				;

		};
	
	}
}

#endif
