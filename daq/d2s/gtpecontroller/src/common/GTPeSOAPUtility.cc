/**
*      @file GTPeSOAPUtility.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.1 $
*     $Date: 2007/03/29 16:58:32 $
*
*
**/
#include "d2s/gtpecontroller/GTPeSOAPUtility.hh"

// for SOAP messaging
#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"

#include "xoap/exception/Exception.h"

std::string d2s::GTPeSOAPUtility::extractSOAPCommand(xoap::MessageReference msg) 
   {

  DOMNode* node = msg->getSOAPPart().getEnvelope().getBody().getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();
  for (XMLSize_t i = 0; i < bodyList->getLength(); i++) 
    {
      DOMNode* command = bodyList->item(i);
      
      if (command->getNodeType() == DOMNode::ELEMENT_NODE)
	{
	  std::string commandName = xoap::XMLCh2String (command->getLocalName());
	  return commandName;
	}
    }
  
   XCEPT_RAISE(xoap::exception::Exception, "No command found");    
}

xoap::MessageReference d2s::GTPeSOAPUtility::createSOAPResponse(std::string const& response, 
							  std::string const& state) 
   {

  // create a reply message
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();

  // create a response of Format:
  //
  // <xdaq:response>
  //   <xdaq:state stateName="xxxx"/>
  // </xdaq:response>

  xoap::SOAPName responseName = envelope.createName( response, "xdaq", XDAQ_NS_URI);
  xoap::SOAPBodyElement responseElement = envelope.getBody().addBodyElement( responseName );
  
  xoap::SOAPName stateName =  envelope.createName( "state", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement stateElement = responseElement.addChildElement( stateName );
  
  xoap::SOAPName attributeName = envelope.createName( "stateName", "xdaq", XDAQ_NS_URI);
  stateElement.addAttribute(attributeName, state);

  return reply;
}
