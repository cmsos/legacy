//
//  XPropTooBarListener.java
//  
//
//  Created by Luciano Orsini on Sat Jun 28 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

package xdaq.xdesk.xprop;

public interface XPropToolBarListener {
    public void buttonPressed(String buttonName);
}
