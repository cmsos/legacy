#ifndef _tcdsrfdippub_version_h_
#define _tcdsrfdippub_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSRFDIPPUB_VERSION_MAJOR 3
#define TCDSRFDIPPUB_VERSION_MINOR 13
#define TCDSRFDIPPUB_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSRFDIPPUB_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSRFDIPPUB_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSRFDIPPUB_VERSION_CODE PACKAGE_VERSION_CODE(TCDSRFDIPPUB_VERSION_MAJOR,TCDSRFDIPPUB_VERSION_MINOR,TCDSRFDIPPUB_VERSION_PATCH)
#ifndef TCDSRFDIPPUB_PREVIOUS_VERSIONS
#define TCDSRFDIPPUB_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSRFDIPPUB_VERSION_MAJOR,TCDSRFDIPPUB_VERSION_MINOR,TCDSRFDIPPUB_VERSION_PATCH)
#else
#define TCDSRFDIPPUB_FULL_VERSION_LIST TCDSRFDIPPUB_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSRFDIPPUB_VERSION_MAJOR,TCDSRFDIPPUB_VERSION_MINOR,TCDSRFDIPPUB_VERSION_PATCH)
#endif

namespace tcdsrfdippub
{
  const std::string package = "tcdsrfdippub";
  const std::string versions = TCDSRFDIPPUB_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS CPM.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS central monitoring application.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
