#ifndef _tcds_bobr_BSTReceiverInfoSpaceHandler_h_
#define _tcds_bobr_BSTReceiverInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace bobr {

    class BSTReceiverInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      BSTReceiverInfoSpaceHandler(xdaq::Application& xdaqApp,
                                  tcds::utils::InfoSpaceUpdater* updater,
                                  unsigned int const bstrNumber);
      virtual ~BSTReceiverInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      // The number of the BST receiver to use: 1 or 2.
      unsigned int const bstrNumber_;

    };

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_BSTReceiverInfoSpaceHandler_h_
