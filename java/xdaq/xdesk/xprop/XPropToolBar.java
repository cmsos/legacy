//
//  XPropToolBar.java
//  
//
//  Created by Luciano Orsini on Sat Jun 28 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//
package xdaq.xdesk.xprop;


import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JPanel;

import java.awt.*;
import java.awt.event.*;
import java.net.*;


public class XPropToolBar extends JToolBar {
   
    XPropToolBarListener listener_ = null;
    JButton defaultButton_;
    JButton getButton_;
    JButton putButton_;
    JButton queryButton_;
    JButton stopButton_;
    
    public XPropToolBar ()
    {
        this.setFloatable(false);
        
        //first button
	URL url = null;
	Image img = null;
        url = this.getClass().getResource("/xdaq/xdesk/xprop/icons/Properties24.gif");
        //img = Toolkit.getDefaultToolkit().getImage(url);
        defaultButton_ = new JButton(new ImageIcon(url));
        defaultButton_.setToolTipText("Set default parameters");
        defaultButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("default");
            }
        });
        this.add(defaultButton_);

        //second button
	url = this.getClass().getResource("/xdaq/xdesk/xprop/icons/Refresh24.gif");
        getButton_ = new JButton(new ImageIcon(url));
        getButton_.setToolTipText("Load selected parameters from Executive/Application");
        getButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("get");
            }
        });
        this.add(getButton_);

        //third button
	url = this.getClass().getResource("/xdaq/xdesk/xprop/icons/SendMail24.gif");
        putButton_ = new JButton(new ImageIcon(url));
        putButton_.setEnabled(false);
        putButton_.setToolTipText("Upload selected parameters to Executive/Application");
        putButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("set");
            }
            });
        this.add(putButton_);
        
        
         //Fourth button
	url = this.getClass().getResource("/xdaq/xdesk/xprop/icons/Information24.gif");
        queryButton_ = new JButton(new ImageIcon(url));
        queryButton_.setEnabled(false);
        queryButton_.setToolTipText("Query all parameters from Executive/Application");
        queryButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("query");
            }
            });
        this.add(queryButton_);
	
	 //Fifth button
	url = this.getClass().getResource("/xdaq/xdesk/xprop/icons/Stop24.gif");
        stopButton_ = new JButton(new ImageIcon(url));
        stopButton_.setEnabled(false);
        stopButton_.setToolTipText("Interrupt currently running operation");
        stopButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("stop");
            }
            });
        this.add(stopButton_);

        
        // dummy listener for debug
        this.setXPropToolBarListener(new XPropToolBarListener() {
            public void buttonPressed(String buttonName) {
                System.out.println("Clicked"+ buttonName);
            }
        });

        
    }  
    
    // Toggle all buttons enabled/stop button disabled and vice versa
    public void stopButtonEnable (boolean enabled)
    {
     	stopButton_.setEnabled(enabled);
    }

    protected void dispatch(String name) 
    {
            if ( listener_ != null )
                listener_.buttonPressed(name);
    }
    
    void setXPropToolBarListener(XPropToolBarListener l ) {
            listener_ = l;
    }

    public void setEnabled(boolean enabled)
    {
            putButton_.setEnabled(enabled);
            getButton_.setEnabled(enabled);
            defaultButton_.setEnabled(enabled);
            queryButton_.setEnabled(enabled);
    }

    
    
    
}
