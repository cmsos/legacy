package xdaq.tools.win;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import xdaq.tools.win.ComboRenderer;
import xdaq.tools.win.CanEnable;

public class ComboListener implements ActionListener {
    JComboBox combo;
    Object currentItem;
    
    ComboListener(JComboBox combo) {
      this.combo  = combo;
      combo.setSelectedIndex(0);
      currentItem = combo.getSelectedItem();
    }
    
    public void actionPerformed(ActionEvent e) {
      Object tempItem = combo.getSelectedItem();
      if (! ((CanEnable)tempItem).isEnabled()) {
        combo.setSelectedItem(currentItem);
      } else {
        currentItem = tempItem;
      }
    }
}
