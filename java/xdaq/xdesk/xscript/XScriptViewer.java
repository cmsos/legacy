package xdaq.xdesk.xscript;

import java.awt.*;
import java.awt.datatransfer.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.tree.*;
import javax.swing.text.*;
import javax.infobus.*;
import java.net.*;
import java.io.*;
import org.w3c.dom.*;
import org.apache.log4j.*;
import xdaq.xdesk.xtree.*;
import xdaq.xdesk.tools.*;
//
// XPropViewer is the main application class that allows
// viewing XDAQ properties.

public class XScriptViewer extends JPanel implements 	XScriptToolBarListener, 
							DiscoveryListener,
							ListSelectionListener,
							ActionListener
{
	Logger logger = Logger.getLogger (XScriptViewer.class);

	JSplitPane splitPane_;
	JSplitPane scriptPane_;
	JEditorPane editPane_;
	JEditorPane outputPane_;	
	JList scriptList_;
	HashMap scripts_;
	HashMap labels_; // store script URL and JLabel association
	DefaultListModel listModel_;
	
        XScriptToolBar toolBar_;
	boolean waitingForReply_ = false;
	XScript xscript_;
	
	// Infobus stuff
	Discovery scriptDiscovery_; // find new scripts on the infobus
	Advertiser scriptAdvertiser_; // put a script on the infobus
	// End of infobus stuff
	
	HashMap openScripts_; // all scripts that have been opened by this applet
	
	JFrame frame_;
	
	Interpreter interpreter_;
	InterpreterFactory interpreterFactory_;
	
	ImmediateAccess lastAccessed_ = null; // remember the last accessed transferable object
	
	
	
	
	// this inner class serves for putting a script
	// on the databus. It is also defined as an innter
	// class in XControlViewer
	class Script implements ImmediateAccess
	{
		String name_;
		Data data_;
		
		public Script (String name, Data data)
		{
			name_ = name;			
			data_ = data;
		}
		
		// Immediate Access interfaces
		public String getPresentationString (java.util.Locale locale)
		{
			return name_;
		}

		// Returns the TreeModel of the configuration tree
		public Object getValueAsObject()
		{
			return data_;
		}

		// Return name of partition 
		public String getValueAsString()
		{
			return name_;
		}

		public void setValue ( Object newValue ) throws InvalidDataException
		{
			if (newValue instanceof Data) 
			{
				data_ = (Data) newValue;			
			} else {
				throw new InvalidDataException ("setValue of Script failed. Wrong Object type");
			}
		}
	};
	
	
	class ScriptListRenderer extends JLabel implements ListCellRenderer
	{
		public Component getListCellRendererComponent (
			JList list,
			Object value, // value to display
			int index, // cell index
			boolean isSelected, // if cell is selected
			boolean cellHasFocus )
		{
			JLabel l = (JLabel) value;
			String s = l.getText();
			setText (s);
			setToolTipText (l.getToolTipText());
			if (isSelected)
			{
				setBackground (list.getSelectionBackground());
				setForeground (list.getSelectionForeground());
			} else {
				setBackground (list.getBackground());
				setForeground (list.getForeground());
			}
			
			setEnabled (list.isEnabled());
			setFont (list.getFont());
			setOpaque (true);
			return this;
		}
	}
		
		
	public XScriptViewer(JFrame frame, XScript xscript)
	{	
		frame_ = frame;
		frame_.setTitle("XScript");
		xscript_ = xscript;
		this.setLayout (new BorderLayout());	
		
		// Properties, right
		editPane_ = new JEditorPane("text/plain", "");
		editPane_.setEditorKit ( new StyledEditorKit() );
		editPane_.setEditable(true);
		this.createKeyMapForEditor (editPane_);
		
		outputPane_ = new JEditorPane("text/plain", "");
		outputPane_.setEditorKit ( new StyledEditorKit() );
		editPane_.setEditable(true);
		this.createKeyMapForEditor (outputPane_);
		
		scriptList_ = new JList();
		scriptList_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scriptList_.setCellRenderer (new ScriptListRenderer());
		scripts_ = new HashMap();
		labels_ = new HashMap();
		listModel_ = new DefaultListModel();
		scriptList_.setModel (listModel_);
		scriptList_.addListSelectionListener (this);
		
		scriptPane_ = new JSplitPane ( JSplitPane.VERTICAL_SPLIT,
						new JScrollPane(editPane_), 
						new JScrollPane(outputPane_));
		
		
		// Split Pane
		splitPane_ = new JSplitPane (	JSplitPane.HORIZONTAL_SPLIT, 
						scriptList_, scriptPane_ );
		splitPane_.setOneTouchExpandable(true);
		splitPane_.setContinuousLayout (true);
		
		
		Dimension minSize = new Dimension (200,200);

                // Create ToolBar
                toolBar_ =  new XScriptToolBar();
                toolBar_.setToolBarListener(this);
                
                this.add(toolBar_,BorderLayout.NORTH);	
		this.add (splitPane_, BorderLayout.CENTER);
           
		
		// --------------------------------------------------
		// InfoBus attachment
		scriptDiscovery_ = new Discovery ("xdatabus", "script", this);
		scriptAdvertiser_ = new Advertiser ("xdatabus", "script");
		// --------------------------------------------------
		
		interpreterFactory_ = new InterpreterFactory();
		
		openScripts_ = new HashMap();
	}
	
	public void createKeyMapForEditor (JEditorPane editor)
	{
		Keymap keymap = editor.addKeymap ("Editor Keys", editor.getKeymap());
		
		// Ctrl-c to copy text
		Action action = new DefaultEditorKit.CopyAction();
		KeyStroke key = KeyStroke.getKeyStroke (KeyEvent.VK_C, Event.CTRL_MASK);
		keymap.addActionForKeyStroke (key, action);
		
		// Ctrl-v to paste text
		action = new DefaultEditorKit.PasteAction();
		key = KeyStroke.getKeyStroke (KeyEvent.VK_V, Event.CTRL_MASK);
		keymap.addActionForKeyStroke (key, action);
		
		// Ctrl-x to cut text
		action = new DefaultEditorKit.CutAction();
		key = KeyStroke.getKeyStroke (KeyEvent.VK_X, Event.CTRL_MASK);
		keymap.addActionForKeyStroke (key, action);
	}
	
	public void setDividerLocation()
	{
		scriptPane_.setDividerLocation (0.5); // 50% for each panel
		splitPane_.setDividerLocation (0.3); // 30% for tree size
	}
	
	public void setPartitionComboBoxModel(DefaultComboBoxModel model)
	{
		toolBar_.setPartitionsModel(model);
	}
	
	JFrame getFrame()
	{
		return frame_;
	}
	
	public XScript getController()
	{
		return xscript_;
	}
        
        // event generated bx ToolBar
        public synchronized void buttonPressed(String buttonName)
        {
		if (buttonName.equals ("execute"))
		{
			toolBar_.executeButtonEnable(false); // disactivate toolbar
			toolBar_.stopButtonEnable(true); // activate stop button
			this.setEnabledComponent(splitPane_, false); // grey out panel

			// find out what is selected			
			JLabel selected = (JLabel) scriptList_.getSelectedValue();
			if (selected != null)
			{
				// get the transferable
				String scriptUrl = selected.getToolTipText();
				ImmediateAccess ia = (ImmediateAccess) scripts_.get (scriptUrl);
				Transferable tr = (Transferable) ia.getValueAsObject();			
				
				// get the interpreter according to the primary data flavor
				DataFlavor[] f = tr.getTransferDataFlavors();
				for (int i = 0; i < f.length; i++)
				{
					if (f[i].getPrimaryType().equals ("script"))
					{					
						interpreter_ = interpreterFactory_.createInterpreter(f[i]);
						interpreter_.setActionListener (this);
						interpreter_.eval( editPane_.getText() );
						interpreter_.start();
						break;
					}
				}
			}
		} 
		else if (buttonName.equals ("stop"))
		{				
			// stop running interpreter
			interpreter_.interrupt();
	
			toolBar_.executeButtonEnable(false); // disactivate toolbar
			toolBar_.stopButtonEnable(false); // activate stop button
			this.setEnabledComponent(splitPane_, false); // grey out panel
			
		}
	      
	
        }
	
	public void setEnabledComponent ( Container c, boolean enabled )
	{
		Component[] list = c.getComponents();
		
		for (int i = 0; i < list.length; i++)
		{
			Container cont = (Container) list[i];
			cont.setEnabled(enabled);
			setEnabledComponent ( cont, enabled );
		}
	}
	
	
	void shutdown()
	{	
		scriptAdvertiser_.leave();
		scriptDiscovery_.leave();
	}
	
	public void openScript (File f)
	{
		long fileLength = f.length();
		String scriptName = f.getPath();
		
		// If the script was already loaded, don't
		// open it again
		if (openScripts_.containsKey(scriptName))
		{
			logger.warn ("Script " + scriptName + " is already open.");
			return;
		}
		 
		int i = scriptName.lastIndexOf ('.');
			
		// scriptTYpe should follow MIME spec. e.g: script/tcl
		// When loading from file, take filename extension as script name, e.g.: .tcl
		//
		String scriptType = "script/" + scriptName.substring (i+1, scriptName.length());
		
		// load the script		
		try {
			FileInputStream fis = new FileInputStream (scriptName);

			// Cast from long to int required by interface.
			// Let's hope the script is not longer than max int!!!
			byte[] buf = new byte [(int)fileLength];

			int offset = 0;
			int numRead = 0;
			while ((offset < buf.length) &&
				(numRead = fis.read (buf, offset, buf.length-offset)) >= 0)
			{
				offset += numRead;
			}

			if (offset < buf.length)
			{
				logger.error ("Short file read: " + scriptName);
				return;
			}

			fis.close();

			String script = new String(buf);

			
			try {
				Data d = new Data();
				d.addDataFlavor (script, DataFlavor.stringFlavor);
				DataFlavor df = new DataFlavor( scriptType );
				d.addDataFlavor (script, df );
				ImmediateAccess s = new Script (scriptName, d);
				
				// Remember the script
				openScripts_.put (scriptName, s);
				
				// put script on infobus
				scriptAdvertiser_.advertise (s);
			} catch (Exception e)
			{
				logger.error (e.toString());
			}
			
		} catch (java.io.IOException ioex)
		{
			logger.error (ioex.toString());
		} 
	}
	
	public String newScript()
	{
		// Modal dialog: enter scriptName
		/*
		String shortName = (String) JOptionPane.showInputDialog (
				this,
				"Name for new script (include extension, e.g. .tcl)",
				"Script Name",
				JOptionPane.PLAIN_MESSAGE,
				null,
				null,
				null );
		*/
		JFileChooser fc = new JFileChooser();		
		int retVal = fc.showSaveDialog ( this );
		if (retVal == JFileChooser.APPROVE_OPTION) 
		{
			File dir = fc.getCurrentDirectory ();
			File f = fc.getSelectedFile();
			
			if ((f != null) && (!f.isDirectory() )) 
			{
				long fileLength = f.length();
				String scriptName = f.getPath();
				
				// If the script was already loaded, don't
				// open it again
				if (openScripts_.containsKey(scriptName))
				{
					String msg = "Script " + scriptName + " is already open.";
					logger.warn (msg);
					JOptionPane.showMessageDialog (this, msg);
					return null;
				}
				
				// scriptTYpe should follow MIME spec. e.g: script/tcl
				// When loading from file, take filename extension as script name, e.g.: .tcl
				//
				int i = scriptName.lastIndexOf ('.');
				
				if (i == -1)
				{
					String msg = "Missing filename extension of script " + scriptName;
					logger.warn (msg);
					JOptionPane.showMessageDialog (this, msg);
					return null;
				}

				String scriptType = "script/" + scriptName.substring (i+1, scriptName.length());				
				
				try {
					Data d = new Data();
					String script = new String();
					d.addDataFlavor ( script , DataFlavor.stringFlavor);
					DataFlavor df = new DataFlavor( scriptType );
					d.addDataFlavor (script, df );
					ImmediateAccess s = new Script (scriptName, d);

					// Remember the script
					openScripts_.put (scriptName, s);

					// put script on infobus
					scriptAdvertiser_.advertise (s);
					
					return scriptName; 
				} catch (Exception e)
				{
					logger.error (e.toString());
				}
			}
		}
		
		return null;
	}
	
	public void saveAsScript()
	{
		JLabel selected = (JLabel) scriptList_.getSelectedValue();
		if (selected != null)
		{

		String scriptName = this.newScript();
		
		// Copy selected script into newly created one
			String scriptUrl = selected.getToolTipText();
			
			// Find script in advertised ones			
			ImmediateAccess ia = (ImmediateAccess) scripts_.get (scriptUrl);
			
			// if not found, try to find script in opened ones
			if (ia == null)
			{
				logger.error ("Could not find script " + scriptUrl);
				return;
			}
			
			ImmediateAccess newScript = (ImmediateAccess) scripts_.get(scriptName);
			// if not found, try to find script in opened ones
			if (newScript == null)
			{
				logger.error ("Could not find script " + scriptName);
				return;
			}
			
			String scriptText = editPane_.getText();			
			Data d = new Data();

			Transferable tr = (Transferable) ia.getValueAsObject();
			DataFlavor[] f = tr.getTransferDataFlavors();
			for (int i = 0; i < f.length; i++)
			{
				d.addDataFlavor (scriptText, f[i] );
			}

			try {
				newScript.setValue (d);
			} catch (InvalidDataException ide)
			{
				logger.error (ide.toString());
			}
			
			this.saveScript (scriptName);					
		} else {
			String msg = "Cannot save, no script selected.";
			logger.warn (msg);
			JOptionPane.showMessageDialog (this, msg);	
		}	
	}
	
	public void saveScript (String filename)
	{
		ImmediateAccess ia = (ImmediateAccess) scripts_.get (filename);			
		// if not found, try to find script in opened ones
		if (ia == null)
		{
			logger.error ("Could not find script " + filename);
		}
		
		try {
			Transferable tr = (Transferable) ia.getValueAsObject();	
			String scriptText = (String) tr.getTransferData(DataFlavor.stringFlavor);

			File outFile = new File (filename);
			outFile.createNewFile(); // creates only if doesn't exist
			FileOutputStream so = new FileOutputStream(outFile, true);
			so.write (scriptText.getBytes());

		} catch (java.io.IOException ioe)
		{
			String msg = "Could not save " + filename + ": " + ioe.toString();
			logger.warn (msg);
			JOptionPane.showMessageDialog (this, msg);
		} catch (Exception ex)
		{
			String msg = "Could not save " + filename + ": " + ex.toString();
			logger.warn (msg);
			JOptionPane.showMessageDialog (this, msg);
		}
	}
	
	public void saveScript()
	{
		JLabel selected = (JLabel) scriptList_.getSelectedValue();
		if (selected != null)
		{
			String scriptUrl = selected.getToolTipText();
			this.saveScript(scriptUrl);
		}  else {
			String msg = "Cannot save, no script selected.";
			logger.warn (msg);
			JOptionPane.showMessageDialog (this, msg);	
		}
	}
	
	// Close a selected script
	public void closeScript()
	{
		JLabel selected = (JLabel) scriptList_.getSelectedValue();
		if (selected != null)
		{
			String scriptUrl = selected.getToolTipText();
			
			// If the script was created/loaded by XScript, revoke it
			if (openScripts_.containsKey(scriptUrl))
			{
				ImmediateAccess ia = (ImmediateAccess) openScripts_.get (scriptUrl);
				scriptAdvertiser_.revoke (ia);
				openScripts_.remove(scriptUrl);
			} 
			else 
			{
				String msg = "Cannot close script " + scriptUrl + ". Was not created/opened by XScript.";
				logger.warn (msg);
				JOptionPane.showMessageDialog (this, msg);
				return;
			}			
		}  else {
			String msg = "Cannot close, no script selected.";
			logger.warn (msg);
			JOptionPane.showMessageDialog (this, msg);	
		}
	}
	
	
	// --- DiscoveryListener interface
	//
	public void objectAdvertised(DataItem item)
	{
		ImmediateAccess ia = (ImmediateAccess) item;
		String scriptUrl = ia.getValueAsString();	// name is unique
		
		// Extract script name from unique partition+script string
		File f = new File (scriptUrl);
		String name = f.getName();
		
		JLabel label = new JLabel (name);
		label.setToolTipText (scriptUrl);
					
		scripts_.put (scriptUrl, ia);
		labels_.put (scriptUrl, label);
		// cut of partition prefix and put it as tooltip.
		// in the list allow scripts with the same name		
		
		listModel_.addElement(label);		
	}
	
	public void objectChanged(DataItem item)
	{
		logger.info ("Script changed not implemented");
	}
	
	public void objectRevoked(DataItem item)
	{	
		ImmediateAccess ia = (ImmediateAccess) item;
		String scriptUrl = ia.getValueAsString();
		JLabel label = (JLabel) labels_.get(scriptUrl);
		scripts_.remove (scriptUrl);
		listModel_.removeElement(label);
		labels_.remove (scriptUrl);
	}
	
	// ListSelectionListener interface
	public void valueChanged (ListSelectionEvent e)
	{
		if ( e.getValueIsAdjusting() == false ) {
			int first =e.getFirstIndex();
			int last = e.getLastIndex();
			JLabel selected = (JLabel) scriptList_.getSelectedValue();

			if (selected != null)
			{
				String scriptUrl = selected.getToolTipText();
				
				// Take modified text from JEditorPane and put it into the script object
				// Only do this action for locally created scripts!
				// Scripts that have been published on the infobus by other Applets
				// cannot be modified!!!
				//
				if ((lastAccessed_ != null) && (openScripts_.containsKey (scriptUrl)))
				{
					String scriptText = (String) editPane_.getText();
					Data d = new Data();
					d.addDataFlavor (scriptText, DataFlavor.stringFlavor);

					Transferable tr = (Transferable) lastAccessed_.getValueAsObject();
					String myname = (String)lastAccessed_.getValueAsString();	
					DataFlavor[] f = tr.getTransferDataFlavors();
					for (int i = 0; i < f.length; i++)
					{
						if (f[i].getPrimaryType().equals ("script"))
						{	
							d.addDataFlavor (scriptText, f[i] );
						}
					}

					try {
						lastAccessed_.setValue (d);
					} catch (InvalidDataException ide)
					{
						logger.error (ide.toString());
					}
				} 

				toolBar_.executeButtonEnable(true);

				
				//String script = (String) ((ImmediateAccess) scripts_.get (scriptUrl)).getValueAsObject();
				ImmediateAccess ia = (ImmediateAccess) scripts_.get (scriptUrl);

				lastAccessed_ = ia;

				Transferable tr = (Transferable) ia.getValueAsObject();			
				try {
					String script = (String) tr.getTransferData(DataFlavor.stringFlavor);			
					editPane_.setText(script);
				} catch (Exception ex)
				{
					logger.error (ex.toString());
				}
			}
		} // adjusting false	
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
		if (interpreter_ != null)
		{
			interpreter_.stop();
		}
		toolBar_.executeButtonEnable(true); 
		toolBar_.stopButtonEnable(false);
		this.setEnabledComponent(splitPane_, true); // grey out panel	
	}

}

