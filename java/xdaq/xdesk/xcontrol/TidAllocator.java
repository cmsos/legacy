package xdaq.xdesk.xcontrol;

import java.lang.String;
import java.util.HashMap;

public class TidAllocator
{
	public TidAllocator()
	{
		map_ = new HashMap();
		lastTid_ = 11;
	}
	
	public String allocate (String className, String instance, String targetAddr) throws TidAllocatorException
	{
		String key = className+instance;
		String value = (String) map_.get(key);
		
		if (targetAddr.equals("auto"))
		{
			if (value != null)
			{
				String msg = "Cannot allocate tid for class " + className + " instance " + instance;
				msg += ". Tid exists: " + value;
				throw new TidAllocatorException (msg);
			} else {
				lastTid_++;
				value = String.valueOf (lastTid_);
				map_.put (key, value);
				return value;
			}
			
		} else 
		{
			if (value == null) 
			{
				map_.put (key, targetAddr);
				return targetAddr;
			} else {
				String msg = "Cannot insert tid " + targetAddr + ". It has already been assigned to class " + className + ", instance: " + instance;
				throw new TidAllocatorException (msg);
			}
		}
	}
	
	private HashMap map_;
	private int lastTid_;
}
