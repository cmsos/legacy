#ifndef _tcds_brildaqchecker_BRILDAQListener_h_
#define _tcds_brildaqchecker_BRILDAQListener_h_

#include <deque>
#include <string>

#ifndef XDAQ_TARGET_XDAQ15
#include "b2in/nub/exception/Exception.h"
#endif
#include "eventing/api/Member.h"
#include "toolbox/lang/Class.h"
#include "xdata/Table.h"

#include "tcds/brildaqchecker/BRILDAQSnapshot.h"
#include "tcds/utils/Lock.h"
#include "tcds/utils/TCDSObject.h"

namespace toolbox {
  namespace mem {
    class Reference;
  }
}

namespace xdata {
  class Properties;
}

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace brildaqchecker {

    class BRILDAQListener :
      public tcds::utils::TCDSObject,
      public eventing::api::Member,
      public toolbox::lang::Class
    {

      using tcds::utils::TCDSObject::getOwnerApplication;

    public:
      BRILDAQListener(tcds::utils::XDAQAppBase& xdaqApp);
      virtual ~BRILDAQListener();

      void start();
      void stop();
      void clearHistory();

      BRILDAQSnapshot snapshot() const;

#ifndef XDAQ_TARGET_XDAQ15
      void onMessage(toolbox::mem::Reference* ref, xdata::Properties& plist) throw (b2in::nub::exception::Exception);
#else
      void onMessage(toolbox::mem::Reference* ref, xdata::Properties& plist);
#endif

    private:
      static size_t const kMaxHistoryLength = 100;

      std::deque<xdata::Table> nibbles_;
      std::string busName_;
      std::string topicName_;

      mutable tcds::utils::Lock lock_;

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_BRILDAQListener_h_
