// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/ExcludeRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::ExcludeRequest::ExcludeRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("exclude",as)
{
	owner_ = "";
	applyToChildren_ = false;
}

void psx::mapi::ExcludeRequest::applyToChildren(bool all)
{
	applyToChildren_ = all;
}

std::string psx::mapi::ExcludeRequest::formatObjectName()
{
 	std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

        format += "::";
        format += name_;
        format += "_FWM";
        return format;
}

std::string psx::mapi::ExcludeRequest::formatCommand()
{
	std::string format = "EXCLUDE";
	if ( applyToChildren_ )
	{
		format = "EXCLUDEALL";
	}
	
	
	if (owner_ != "")
	{
		format += "/OWNER=\"";
		format += owner_;
		format += "\"";
	}
	
	return format;
}



