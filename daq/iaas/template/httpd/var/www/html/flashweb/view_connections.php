<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
<script type="text/javascript" src="js/toggle.js"></script>
</head>

<body>

<?php
include_once ('config.php');
    
    require 'vendor/autoload.php';
    use Httpful\Exception;
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    $url_ = $config ['elasticsearchurl'] . '/_nodes/stats/http';
    
    //echo $host;
    
    // perform request to ES
    $response = \Httpful\Request::get($url_)->send();
    
    // parse response into object code
    $json = json_decode($response, true);
    
    echo '<br />';
    
    $header_fields = array("current_open", "total_opened");
    
    // Output table in html
    echo '<table class="xdaq-table">';
    echo '<thead>';
    echo '<th>';
    echo "Host";
    echo '</th>';
    echo '<th>';
    echo "current open";
    echo '</th>';
    echo '<th>';
    echo "total open";
    echo '</th>';
    echo '</thead>';
    
    echo '<tbody>';
   
    
    foreach ($json['nodes'] as $val) {
        echo "<tr>";
        echo '<td>';
        echo $val['host'] ;
        echo '</td>';
        echo '<td>';
        echo $val['http']['current_open'] ;
        echo '</td>';
        echo '<td>';
        echo $val['http']['total_opened'] ;
        echo '</td>';
        echo "</tr>";

    }

    echo '</tbody>';
    echo '</table>';
    
    
?>

<br>

        <form action="index.php">
                <input type="submit" value="Go back" method="get"/>
        </form>

</body>
<footer>Copyright © XDAQ 2016</footer>

</html>

