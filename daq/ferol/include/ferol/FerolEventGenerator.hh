#ifndef __FerolEventGenerator
#define __FerolEventGenerator

#include "d2s/utils/EventGenerator.hh"

#define MAX_EVENT_DESCRIPTORS 1024

namespace ferol
{
    class FerolEventGenerator : public utils::EventGenerator {

    public: 
        FerolEventGenerator( HAL::HardwareDeviceInterface *ferol,                             
                             Logger logger );
        virtual ~FerolEventGenerator() {};
    protected:
        virtual void checkParams( uint32_t &nevt, uint32_t streamNo );
        virtual void writeDescriptors( uint32_t streamNo );

    private:
        HAL::HardwareDeviceInterface *ferol_P;
    };
}
#endif /* __FerolEventGenerator */
