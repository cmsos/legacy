package net.hep.cms.xdaqctl;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.xml.soap.AttachmentPart;

import org.w3c.dom.Document;

import junit.framework.TestCase;
import net.hep.cms.xdaqctl.xdata.FlashList;
import net.hep.cms.xdaqctl.xdata.XDataType;

public class XMASMessageTest extends TestCase {
	
	private final static String RCMS_ORIGINATOR =
			"http://localhost:0/urn:rcms-fm:fullpath=/rcms/test";
	
	public XMASMessageTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(XMASMessageTest.class);
	}

	public void testXMASMessage() throws Exception
	{
		// create a simple flash list
		FlashList fl = new FlashList("test");

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);

		fl.get("string").setValue("LongerThanFour");
		fl.get("integer").setValue(3);

		XMASMesssage message = new XMASMesssage(fl, RCMS_ORIGINATOR);

		checkSOAPPart("test/xdaq/xmas-soap.xml", message);
		checkAttachmentHeader("test/xdaq/xmas-att.txt", message);	
	}

	public void testSend() throws Exception
	{
		// create a simple flash list
		FlashList fl = new FlashList("test");

		fl.add("string", XDataType.STRING);
		fl.add("integer", XDataType.INTEGER);

		fl.get("string").setValue("LongerThanFour");
		fl.get("integer").setValue(3);

		XMASMesssage message = new XMASMesssage(fl, RCMS_ORIGINATOR);

		Document document = message.send("http://localhost:40000");

		assertNotNull(document);

		String  good_result =
				"<?xml version='1.0' encoding='UTF-8'?>\n" +
				"<xmas:reportResponse xmlns:xmas='http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10'/>\n";

		String result = TestUtil.compareDOMs(
				TestUtil.stringToDOM(good_result), document);

		assertNull(result, result);

	}

	private void checkSOAPPart(String string, Message message)
	throws Exception
	{	
		assertNotNull(message);

		String result = TestUtil.compareDOMs(
				TestUtil.fileToDOM(string), message.getDOM());

		assertNull(result, result);
	}

	private void checkAttachmentHeader(String filename, XMASMesssage message)
	throws Exception  {

		assertNotNull(message);

		AttachmentPart part = (AttachmentPart)message.message.getAttachments().next();
		BufferedReader in = new BufferedReader(new FileReader(filename));

		String line;
		while ((line = in.readLine()) != null) {
			int i = line.indexOf(':');

			String name = line.substring(0, i);
			String value = line.substring(i + 1).trim();

			String[] values = part.getMimeHeader(name);
			assertNotNull("No header of " + name, values);
			assertEquals("Found " + values.length + " values for " + name, 1, values.length);

			if (values[0].matches(".*\\d{4}-\\d{2}-\\d{2}.*")) {
				values[0] = values[0].replaceAll("\\d", "n"); // mask out date string
			}
			assertEquals("Header value for " + name, value, values[0]);
		}

	}


}
