// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "elastic/timestream/Event.h"
#include <string>
#include <sstream>
#include <map>
#include "xdata/Table.h"
#include "xmas/exception/Exception.h"
#include "xmas/FlashListDefinition.h"
			
elastic::timestream::Event::Event( xdata::Properties & plist, toolbox::mem::Reference * ref )
	: toolbox::Event("urn:elastic-timestream:Event", 0), plist_(plist), ref_(ref)
{
}
