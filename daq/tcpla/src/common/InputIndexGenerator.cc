// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tcpla/InputIndexGenerator.h"

tcpla::InputIndexGenerator* tcpla::InputIndexGenerator::instance_ = 0;

tcpla::InputIndexGenerator* tcpla::getInputIndexGenerator ()
{
	return tcpla::InputIndexGenerator::getInstance();
}

tcpla::InputIndexGenerator* tcpla::InputIndexGenerator::getInstance ()
{
	if (instance_ == 0)
	{
		instance_ = new tcpla::InputIndexGenerator();
	}
	return instance_;
}

// Set Reference Pool size in CTOR
//
tcpla::InputIndexGenerator::InputIndexGenerator ()
	: mutex_(toolbox::BSem::FULL), index_(0)
{
}

size_t tcpla::InputIndexGenerator::getIndex ()
{
	size_t i;
	mutex_.take();
	i = index_++;
	mutex_.give();
	return i;
}
