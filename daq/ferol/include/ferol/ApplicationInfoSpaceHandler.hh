#ifndef __ApplicationInfoSpaceHandler
#define __ApplicationInfoSpaceHandler

#include "xdaq2rc/ClassnameAndInstance.h"

#include "d2s/utils/InfoSpaceHandler.hh"
namespace ferol {
    class ApplicationInfoSpaceHandler : public utils::InfoSpaceHandler, 
                                        public xdata::ActionListener
    {
    public:
        ApplicationInfoSpaceHandler( xdaq::Application *xdaq );
        // callback for Parameter Setting
        void actionPerformed( xdata::Event& e );
        
        // a special call for the ApplicationInfoSpace to set the parameters of the statelistener.
        void setRCMSStateListenerParameters(  xdata::Bag<xdaq2rc::ClassnameAndInstance> *RcmsStateListenerParameters,
                                              xdata::Boolean *FoundRcmsStateListener );
    private:
        
    };
}

#endif /* __ApplicationInfoSpaceHandler */
