package xdaq.xdesk.xprop;

import javax.accessibility.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.table.*;

import java.awt.Dimension;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.*;
import java.awt.Container;


/**
 * This example shows how to create a simple JTreeTable component, 
 * by using a JTree as a renderer (and editor) for the cells in a 
 * particular column in the JTable.  
 *
 */

public class JTreeTable extends JTable 
{
	protected TreeTableCellRenderer tree;
	protected StringCellRenderer stringCellRenderer;
	protected BooleanCellRenderer booleanCellRenderer;
	
	TreeTableModelAdapter adapter = null;
	boolean enabled_;
	
	public TreeTableModelAdapter getModelAdapter() 
	{
		return adapter;
	}
	
	
	
	public void setEnabled (boolean enabled)
	{
		super.setEnabled(enabled);
		enabled_ = enabled;
		tree.setEnabled(enabled);
	}
	
	public void setModel (TreeTableModel treeTableModel)
	{
		tree = new TreeTableCellRenderer(treeTableModel); 
		
		//System.out.println ("Enabled in setModel: " + enabled_);
		
		stringCellRenderer = new StringCellRenderer ();
		booleanCellRenderer = new BooleanCellRenderer ();
		
		// Install a tableModel representing the visible rows in the tree. 
		adapter = new TreeTableModelAdapter(treeTableModel, tree);
		super.setModel(adapter);

		// Force the JTable and JTree to share their row selection models. 
		tree.setSelectionModel(new DefaultTreeSelectionModel() { 
		
		// Extend the implementation of the constructor, as if: 
		 /* public this() */ {
			setSelectionModel(listSelectionModel); 
			} 
		}); 
		// Make the tree and table row heights the same. 
		tree.setRowHeight(getRowHeight());

		// Install the tree editor renderer and editor. 
		setDefaultRenderer(TreeTableModel.class, tree); 
		setDefaultEditor(TreeTableModel.class, new TreeTableCellEditor());  
		
		// Install renderer for strings
		setDefaultRenderer(String.class, stringCellRenderer); 
		setDefaultRenderer(Boolean.class, booleanCellRenderer); 
		
		setShowGrid(true);
		setIntercellSpacing(new Dimension(0, 0)); 	        
	}

   	 public JTreeTable(TreeTableModel treeTableModel) 
	{
		super();
		enabled_ = true;
		this.setModel (treeTableModel);  	
		installMouseAdapter();
    	}
	
	// Add a mouse click listener for
	// EXPERIMENTAL JASPLOT SUPPORT
	private void installMouseAdapter()
	{
		this.addMouseListener ( new MouseAdapter()
			{
				public void mouseClicked (MouseEvent e)
				{
					int r = getSelectedRow();
					int c = getSelectedColumn();
					TreeTableModelAdapter a = (TreeTableModelAdapter) getModel();
					if (c == 3)
					{
						String type = (String) a.getValueAt (r, 2);
						if (type.equals ("jasplot"))
						{
							ParameterNode n = (ParameterNode) a.nodeForRow (r);
							n.showViewer();
						}
					}
				}
			}); 
	}
	
	public JTreeTable()
	{
		super();
		enabled_ = true;
		installMouseAdapter();
	}

    /* Workaround for BasicTableUI anomaly. Make sure the UI never tries to 
     * paint the editor. The UI currently uses different techniques to 
     * paint the renderers and editors and overriding setBounds() below 
     * is not the right thing to do for an editor. Returning -1 for the 
     * editing row in this case, ensures the editor is never painted. 
     */
    public int getEditingRow() {
        return (getColumnClass(editingColumn) == TreeTableModel.class) ? -1 : editingRow;  
    }

    // 
    // The renderer used to display the tree nodes, a JTree.  
    //
    public class TreeTableCellRenderer extends JTree implements TableCellRenderer 
    {
	protected int visibleRow;
   
	public TreeTableCellRenderer(TreeModel model) 
	{ 
	    super(model); 
	}

	public void setBounds(int x, int y, int w, int h) 
	{
	    super.setBounds(x, 0, w, JTreeTable.this.getHeight());
	}

	public void paint(Graphics g) 
	{
	    g.translate(0, -visibleRow * getRowHeight());
	    super.paint(g);
	}

	public Component getTableCellRendererComponent(JTable table,
						       Object value,
						       boolean isSelected,
						       boolean hasFocus,
						       int row, int column) 
	{		
	    if(isSelected)
		setBackground(table.getSelectionBackground());
	    else
		setBackground(table.getBackground());
       
	    visibleRow = row;
	    
	    return this;
	    
	}
    }

    // 
    // The editor used to interact with tree nodes, a JTree.  
    //

    public class TreeTableCellEditor extends AbstractCellEditor implements TableCellEditor {
	public Component getTableCellEditorComponent(JTable table, Object value,
						     boolean isSelected, int r, int c) 
	{
	    return tree;
	}
    }



    // 
    // The renderer used to display the String.class entries  
    //
    //public class StringCellRenderer extends JLabel implements TableCellRenderer 
    public class StringCellRenderer extends DefaultTableCellRenderer
    {
	public Component getTableCellRendererComponent(JTable table,
						       Object value,
						       boolean isSelected,
						       boolean hasFocus,
						       int row, int column) 
	{
		JLabel retVal = (JLabel) super.getTableCellRendererComponent (table, value, isSelected, hasFocus, row, column);
		// EXPERIMENTAL JASPLOT CUSTOM RENDERING OF THE TREE
		
		TreeTableModelAdapter a = (TreeTableModelAdapter) table.getModel();
		if (column == 3)
		{
			String type = (String) a.getValueAt (row, 2);
			if (type.equals ("jasplot"))
			{
				Icon i = new ImageIcon ("./icons/plugin.gif");
				JLabel l = new JLabel ();
				l.setIcon(i);
				l.setOpaque (true);
				l.setText ( value.toString() );
				l.setFont (retVal.getFont());
				retVal  = l;
			}
		} 
		
		
	    if(isSelected)
		retVal.setBackground(table.getSelectionBackground());
	    else
		retVal.setBackground(table.getBackground());
       	
	    
		
	    retVal.setEnabled(enabled_);
		
	    return retVal;
	}
    }
    
    public class BooleanCellRenderer extends DefaultTableCellRenderer
    {
    	JCheckBox b = new JCheckBox();
    
	public Component getTableCellRendererComponent(JTable table,
						       Object value,
						       boolean isSelected,
						       boolean hasFocus,
						       int row, int column) 
	{
		if (value instanceof Boolean)
		{
			b.setSelected ( ((Boolean) value).booleanValue() );
			b.setHorizontalAlignment (JLabel.CENTER);
			b.setBackground(table.getBackground());
			b.setEnabled(enabled_);
			return b;
		} else {
			System.out.println ("Boolean renderer assigned to wrong cell type!.");
			return null;
		}
		//Component retVal = (Component) super.getTableCellRendererComponent (table, value, isSelected, hasFocus, row, column);
		//System.out.println ("Component is: " + retVal.toString());
	    	//retVal.setEnabled(enabled_);
		
	    	//return retVal;
	}
    }
} // end of JTreeTable class
