#include "xdaq/XceptSerializer.h"
#include "xdaq/exception/Exception.h"
#include "xcept/tools.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"

#include "xoap/Method.h"
#include "xoap/MessageFactory.h"

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"


#include "toolbox/TimeVal.h"

#define SIZE 10000

int main(int argc, char **argv)
{
	XMLPlatformUtils::Initialize();

	XCEPT_DECLARE(xdaq::exception::Exception, e10, "Most inner Exception");
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e9, "Inner Exception", e10);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e8, "Inner Exception", e9);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e7, "Inner Exception", e8);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e6, "Inner Exception", e7);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e5, "Inner Exception", e6);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e4, "Inner Exception", e5);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e3, "Inner Exception", e4);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e2, "Inner Exception", e3);
	XCEPT_DECLARE_NESTED(xdaq::exception::Exception, e1, "Outer Exception", e2);

	size_t size = e1.getHistory().size();
	std::cout << "Levels: " << e1.getHistory().size() << std::endl;
	for(int i=0 ; i < size ; i++)
	{
		std::cout << e1.getHistory()[i].getProperty("message") << std::endl;
	}

	std::cout << stdformat_exception_history(e1) << std::endl;

	char * buf = new char[1000000];

	toolbox::TimeVal a(toolbox::TimeVal::gettimeofday());
	for(int i=0; i<SIZE ; i++)
	{
		xdata::exdr::FixedSizeOutputStreamBuffer sbuf(buf, 1000000);
		xdaq::XceptSerializer::writeTo(e1, &sbuf);
	//std::cout << "Size of the Exception: " << sbuf.tellp() << std::endl;
	}
	toolbox::TimeVal b(toolbox::TimeVal::gettimeofday());

	double diff = ((double)(b)-(double)(a));

	std::cout << "interval: " << diff << std::endl;

	std::string x;
	toolbox::TimeVal c(toolbox::TimeVal::gettimeofday());
	for(int i=0; i<SIZE ; i++)
	{
        	// Create SOAP message
        	xoap::MessageReference msg = xoap::createMessage();
        	xoap::SOAPPart soap = msg->getSOAPPart();
        	xoap::SOAPEnvelope envelope = soap.getEnvelope();
        	xoap::SOAPBody body = envelope.getBody();
       		xoap::SOAPName command = envelope.createName("pippo","xen", "http://xdaq.web.cern.ch/xdaq/xsd/2005/ErrorNotification-11.xsd");
        	xoap::SOAPElement element = body.addBodyElement(command);
        	//element.addNamespaceDeclaration("xen","http://xdaq.web.cern.ch/xdaq/xsd/2005/ErrorNotification-11.xsd");

        	DOMNode * notifyNode = element.getDOMNode();

		xdaq::XceptSerializer::writeTo(e1, notifyNode);

		msg->writeTo(x);
	//	std::cout << "SOAP Size: " << x.length() << std::endl;
	}
	toolbox::TimeVal d(toolbox::TimeVal::gettimeofday());

	double diff2 = ((double)(d)-(double)(c));

	std::cout << "interval: " << diff2 << std::endl;


	std::cout << "---" << std::endl;
	toolbox::TimeVal e(toolbox::TimeVal::gettimeofday());
	for(int i=0; i<SIZE ; i++)
	{
		xcept::Exception n;
		xdata::exdr::FixedSizeInputStreamBuffer sbuf2(buf, 1000000);

		xdaq::XceptSerializer::importFrom(&sbuf2, n);
	}
	toolbox::TimeVal f(toolbox::TimeVal::gettimeofday());

	double diff3 = ((double)(f)-(double)(e));

	std::cout << "interval: " << diff3 << std::endl;

	toolbox::TimeVal g(toolbox::TimeVal::gettimeofday());
	for(int i=0; i<SIZE ; i++)
	{
		xoap::MessageReference msg = xoap::createMessage((char*)x.c_str(), x.length());
	        // Extract info for reply
	        DOMNodeList* bodyList = msg->getSOAPPart().getEnvelope().getBody().getDOMNode()->getChildNodes();
	        std::string namespaceURI = "";
	        std::string namespacePrefix = "";
	        //std::string commandName = "";
	        for (XMLSize_t i = 0; i < bodyList->getLength(); i++)
	        {
	                DOMNode* command = bodyList->item(i);
	
	                if (command->getNodeType() == DOMNode::ELEMENT_NODE)
	                {
				xcept::Exception ex;
	                        namespaceURI = xoap::XMLCh2String (command->getNamespaceURI());
	                        namespacePrefix = xoap::XMLCh2String (command->getPrefix());

				xdaq::XceptSerializer::importFrom(command,ex);
				//std::cout << "Found" << std::endl;
			}
		}
	}
	toolbox::TimeVal h(toolbox::TimeVal::gettimeofday());

	double diff4 = ((double)(h)-(double)(g));

	std::cout << "interval: " << diff4 << std::endl;

//	std::cout << stdformat_exception_history(n) << std::endl;
}

