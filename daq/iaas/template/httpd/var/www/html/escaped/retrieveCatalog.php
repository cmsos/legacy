<?php

    include_once ('tools.php');
    include_once('config/config.php');
    
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    // get args
    $fmt_ = $_GET["fmt"];
    $tophits_ = true;
    $index_ = $config['flash_index'];
    
    if(!empty($_GET["tophits"]))
    {
        $val = $_GET["tophits"];
        if ( $val == "false")
        {
            $tophits_ = false;
        }
    }
    
    if(!empty($_GET["index"]))
    {
        $index_ = $_GET["index"];
    }
    
    // retrieve full set of flashlists
    //$es_index_ = "flashlist";
    
    // build url
    //$url_ = 'http://' . $config['host']. ":" .  $config['flash_index']. "/" . $flash_ . "/_search?";
    
   
    if ( $fmt_ == "csv")
    {
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); // required for certain browsers
        header("Content-type: application/csv"); // I always use this
        header("Content-Disposition: attachment; filename=catalog.csv");
        header("Content-Transfer-Encoding: binary");
        
       // header('Content-type: application/csv');
       // header('Content-Disposition: attachment; filename=catalog.csv');
        //header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1

        retrieveCatalogCSV($config['host'],$config['port'], $index_);

    }
    else if ( $fmt_ == "html")
    {
        echo '<!doctype html>';
        echo '<html>';
        echo '<head>';
        echo '<meta charset="utf-8">';
        echo '<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>';
        echo '<link href="css/tables.css" rel="stylesheet" />';
        echo '</head>';
        echo '<body>';

        retrieveCatalogHTML($config['host'],$config['port'], $index_);
        echo '</body>';
        echo '</html>';
    }
    else if ( $fmt_ == "json")
    {
        
        header('Content-type: application/json');
        header('Content-Disposition: attachment; filename=catalog.json');
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        
        retrieveCatalogJSON($config['host'],$config['port'], $index_);

    }
    else if ( $fmt_ == "plain")
    {
        header('Content-type: text/plain');
        
        retrieveCatalogCSV($config['host'],$config['port'], $index_);
        
    }
    else
    {
       header("HTTP/1.1 404 Not Found");
        
    }
    ?>
