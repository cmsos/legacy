package xdaq.xdesk.xdesk;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public class XDeskMain
{
	static Logger logger = Logger.getRootLogger();

	public static void main (String args[])
	{
		// Read logger properties file
		DOMConfigurator.configure ("logconf.xml");
		
		logger.info ("Starting up XDesk");
	
		DeskletContext context = new DeskletContext("../plugins");
		
		// Loads the applet descriptions, instantiates Apple objects (calls their CTOR)
		//context.loadDeskletProperties ("../plugins/");
		
		
		logger.info ("XDesk is ready to run");
	}
	
}	
