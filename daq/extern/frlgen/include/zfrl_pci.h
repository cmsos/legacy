/*
 *  zlanai_pci.h
 * 
 *  taken from linux's pci.h
 *  added underscore at beginning of names
 *
 *  $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/include/zfrl_pci.h,v 1.2 2006/12/22 07:31:14 frans Exp $
 *
 *
 */

#ifndef ZLANAI_PCI_H
#define ZLANAI_PCI_H

/*
 * Under PCI, each device has 256 bytes of configuration address space,
 * of which the first 64 bytes are standardized as follows:
 */
#define _PCI_VENDOR_ID		0x00	/* 16 bits */
#define _PCI_DEVICE_ID		0x02	/* 16 bits */
#define _PCI_COMMAND		0x04	/* 16 bits */
#define  _PCI_COMMAND_IO		0x1	/* Enable response in I/O space */
#define  _PCI_COMMAND_MEMORY	0x2	/* Enable response in Memory space */
#define  _PCI_COMMAND_MASTER	0x4	/* Enable bus mastering */
#define  _PCI_COMMAND_SPECIAL	0x8	/* Enable response to special cycles */
#define  _PCI_COMMAND_INVALIDATE	0x10	/* Use memory write and invalidate */
#define  _PCI_COMMAND_VGA_PALETTE 0x20	/* Enable palette snooping */
#define  _PCI_COMMAND_PARITY	0x40	/* Enable parity checking */
#define  _PCI_COMMAND_WAIT 	0x80	/* Enable address/data stepping */
#define  _PCI_COMMAND_SERR	0x100	/* Enable SERR */
#define  _PCI_COMMAND_FAST_BACK	0x200	/* Enable back-to-back writes */

#define _PCI_STATUS		0x06	/* 16 bits */
#define  _PCI_STATUS_66MHZ	0x20	/* Support 66 Mhz PCI 2.1 bus */
#define  _PCI_STATUS_UDF		0x40	/* Support User Definable Features */

#define  _PCI_STATUS_FAST_BACK	0x80	/* Accept fast-back to back */
#define  _PCI_STATUS_PARITY	0x100	/* Detected parity error */
#define  _PCI_STATUS_DEVSEL_MASK	0x600	/* DEVSEL timing */
#define  _PCI_STATUS_DEVSEL_FAST	0x000	
#define  _PCI_STATUS_DEVSEL_MEDIUM 0x200
#define  _PCI_STATUS_DEVSEL_SLOW 0x400
#define  _PCI_STATUS_SIG_TARGET_ABORT 0x800 /* Set on target abort */
#define  _PCI_STATUS_REC_TARGET_ABORT 0x1000 /* Master ack of " */
#define  _PCI_STATUS_REC_MASTER_ABORT 0x2000 /* Set on master abort */
#define  _PCI_STATUS_SIG_SYSTEM_ERROR 0x4000 /* Set when we drive SERR */
#define  _PCI_STATUS_DETECTED_PARITY 0x8000 /* Set on parity error */

#define _PCI_CLASS_REVISION	0x08	/* High 24 bits are class, low 8
					   revision */
#define _PCI_REVISION_ID         0x08    /* Revision ID */
#define _PCI_CLASS_PROG          0x09    /* Reg. Level Programming Interface */
#define _PCI_CLASS_DEVICE        0x0a    /* Device class */

#define _PCI_CACHE_LINE_SIZE	0x0c	/* 8 bits */
#define _PCI_LATENCY_TIMER	0x0d	/* 8 bits */
#define _PCI_HEADER_TYPE		0x0e	/* 8 bits */
#define  _PCI_HEADER_TYPE_NORMAL	0
#define  _PCI_HEADER_TYPE_BRIDGE 1
#define  _PCI_HEADER_TYPE_CARDBUS 2

#define _PCI_BIST		0x0f	/* 8 bits */
#define _PCI_BIST_CODE_MASK	0x0f	/* Return result */
#define _PCI_BIST_START		0x40	/* 1 to start BIST, 2 secs or less */
#define _PCI_BIST_CAPABLE	0x80	/* 1 if BIST capable */

/*
 * Base addresses specify locations in memory or I/O space.
 * Decoded size can be determined by writing a value of 
 * 0xffffffff to the register, and reading it back.  Only 
 * 1 bits are decoded.
 */
#define _PCI_BASE_ADDRESS_0	0x10	/* 32 bits */
#define _PCI_BASE_ADDRESS_1	0x14	/* 32 bits [htype 0,1 only] */
#define _PCI_BASE_ADDRESS_2	0x18	/* 32 bits [htype 0 only] */
#define _PCI_BASE_ADDRESS_3	0x1c	/* 32 bits */
#define _PCI_BASE_ADDRESS_4	0x20	/* 32 bits */
#define _PCI_BASE_ADDRESS_5	0x24	/* 32 bits */
#define  _PCI_BASE_ADDRESS_SPACE	0x01	/* 0 = memory, 1 = I/O */
#define  _PCI_BASE_ADDRESS_SPACE_IO 0x01
#define  _PCI_BASE_ADDRESS_SPACE_MEMORY 0x00
#define  _PCI_BASE_ADDRESS_MEM_TYPE_MASK 0x06
#define  _PCI_BASE_ADDRESS_MEM_TYPE_32	0x00	/* 32 bit address */
#define  _PCI_BASE_ADDRESS_MEM_TYPE_1M	0x02	/* Below 1M */
#define  _PCI_BASE_ADDRESS_MEM_TYPE_64	0x04	/* 64 bit address */
#define  _PCI_BASE_ADDRESS_MEM_PREFETCH	0x08	/* prefetchable? */
#define  _PCI_BASE_ADDRESS_MEM_MASK	(~0x0fUL)
#define  _PCI_BASE_ADDRESS_IO_MASK	(~0x03UL)
/* bit 1 is reserved if address_space = 1 */

/* Header type 0 (normal devices) */
#define _PCI_CARDBUS_CIS		0x28
#define _PCI_SUBSYSTEM_VENDOR_ID	0x2c
#define _PCI_SUBSYSTEM_ID	0x2e  
#define _PCI_ROM_ADDRESS		0x30	/* Bits 31..11 are address, 10..1 reserved */
#define  _PCI_ROM_ADDRESS_ENABLE	0x01
#define _PCI_ROM_ADDRESS_MASK	(~0x7ffUL)

/* 0x34-0x3b are reserved */
#define _PCI_INTERRUPT_LINE	0x3c	/* 8 bits */
#define _PCI_INTERRUPT_PIN	0x3d	/* 8 bits */
#define _PCI_MIN_GNT		0x3e	/* 8 bits */
#define _PCI_MAX_LAT		0x3f	/* 8 bits */


/*
 * The PCI interface treats multi-function devices as independent
 * devices.  The slot/function address of each device is encoded
 * in a single byte as follows:
 *
 *	7:3 = slot
 *	2:0 = function
 */
#define _PCI_DEVFN(slot,func)	((((slot) & 0x1f) << 3) | ((func) & 0x07))
#define _PCI_SLOT(devfn)		(((devfn) >> 3) & 0x1f)
#define _PCI_FUNC(devfn)		((devfn) & 0x07)



#endif 






