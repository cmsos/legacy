#include "tcds/freqmon/FreqMonInfoSpaceUpdater.h"

#include <tgmath.h>

#include "tcds/freqmon/FreqMonLoop.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/XDAQAppBase.h"

int const
tcds::freqmon::FreqMonInfoSpaceUpdater::kUpdatePrescale_ = 60;

double const
tcds::freqmon::FreqMonInfoSpaceUpdater::kUpdateDeadBand_ = 2;

tcds::freqmon::FreqMonInfoSpaceUpdater::FreqMonInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                tcds::freqmon::FreqMonLoop const& freqMonLoop) :
  tcds::utils::InfoSpaceUpdater(xdaqApp),
  freqMonLoop_(freqMonLoop),
  freqValPrev_(0),
  updatePrescaleCounter_(0)
{
}

tcds::freqmon::FreqMonInfoSpaceUpdater::~FreqMonInfoSpaceUpdater()
{
}

void
tcds::freqmon::FreqMonInfoSpaceUpdater::updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;

  if (!freqMonLoop_.isAllOk())
    {
      infoSpaceHandler->setInvalid();
      updatePrescaleCounter_ = 0;
    }
  else
    {
      infoSpaceHandler->setValid();

      // Little tricky here. When we update the InfoSpace here, it
      // automatically goes into the database. In order not to upset
      // the database, we selectively store new values when they
      // change, or once every so often.
      double const freqVal = freqMonLoop_.snapshot();
      if ((fabs(freqVal - freqValPrev_) >= kUpdateDeadBand_) ||
          (updatePrescaleCounter_ == 0))
        {
          infoSpaceHandler->setDouble("frequency", freqVal);
          updated = true;
          updatePrescaleCounter_ = kUpdatePrescale_;
          freqValPrev_ = freqVal;
        }
    }

  // Keep track of the 'forced update' prescale.
  if (updatePrescaleCounter_ == 0)
    {
      updatePrescaleCounter_ = kUpdatePrescale_;
    }
  --updatePrescaleCounter_;

  if (updated)
    {
      // Now sync everything from the cache to the InfoSpace itself.
      infoSpaceHandler->writeInfoSpace();
    }
}
