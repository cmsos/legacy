#!/usr/bin/env python

import sys, os
import socket
import time
import atexit
import urllib2

from XMLUtils import *

import SOAPUtils

# test the compiled fedkit library with the
# fedstreamer
#
# - starts the fedstreamer
# - fetches rate from the fedstreamer page


#----------------------------------------------------------------------
xdaqRoot = "/opt/xdaq"

scriptDir = os.path.dirname(os.path.abspath(__file__))

port = 40000

host = socket.gethostname()

#----------------------------------------------------------------------

class TempFile:
    """ auto-deleting temporary file which only gets deleted
    at program exit (not when the file is closed).

    Unfortunately, the corresponding flag for NamedTemporaryFile is not available
    in python 2.4 (which is the standard on SLC5)
    """

    import tempfile, os

    def __init__(self, suffix='', prefix='tmp', dir=None, text=False):

        fd, self.name = tempfile.mkstemp(suffix, prefix, dir, text)
        self.file = os.fdopen(fd, "w+")

        import atexit
        atexit.register(lambda: self.__delete())

    def __delete(self):
        import os
        os.unlink(self.name)

    def write(self, data):
        self.file.write(data)

    def close(self):
        self.file.close()

    def flush(self):
        self.file(flush)

        

#----------------------------------------------------------------------

def makeXDAQconfig(fedkitSharedObjectPath = scriptDir + "/../libfedkit.so",
                   host = socket.gethostname(),
                   port = 40000,
                   ):
    """
    @return the xml document object
    @param fedkitSharedObjectPath: is the path to the fedkit shared
           library to test
           """
    
    # from xml.dom.minidom import Document

    # doc = Document()

    # see also http://stackoverflow.com/questions/863774/how-to-generate-xml-documents-with-namespaces-in-python

    # read the template file
    import xml.dom.minidom
    doc = xml.dom.minidom.parse(scriptDir + "/" + "fedstreamer-config-template.xml")

    # adapt the necessary parameters 
    import xml.xpath
    global context
    context = xml.xpath.Context.Context(doc)
    context.setNamespaces(dict(
        xc="http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30",

        ))

    #--------------------
    # adapt default values
    #--------------------
    contextNode = singleNode(context, '/xc:Partition/xc:Context')

    contextNode.setAttribute('url','http://%s:%d' % (host, port))

    # add modules
    for modpath in [
        '${XDAQ_ROOT}/lib/libpheaps.so',
        fedkitSharedObjectPath,
        '${XDAQ_ROOT}/lib/libfedstreamer.so',
        ]:

        moduleNode = addNode(contextNode,"xc:Module")
        

        # print dir(doc)
        # sys.exit(1)
        addTextChild(moduleNode, modpath)
    return doc


#----------------------------------------------------------------------

def startXDAQ(configFname,port = 40000, host = socket.gethostname()):

    # os.system("/bin/cat " + configFname)
    # sys.exit(1)
    # return 

    # get ip address of host we're running on 
    hostIP = socket.gethostbyname(host)

    cmdParts = [
        'export XDAQ_ROOT=%s' % xdaqRoot,
        'export XDAQ_DOCUMENT_ROOT=${XDAQ_ROOT}/htdocs',
        'export XDAQ_SETUP_ROOT=${XDAQ_ROOT}/share',
        'export LD_LIBRARY_PATH=${XDAQ_ROOT}/lib',

        " ".join([
            "${XDAQ_ROOT}/bin/xdaq.exe",
            "-e ${XDAQ_ROOT}/etc/default.profile",
            "-c %s" % configFname,
            "-h %s" % hostIP,
            "-p %d" % port,

            ">/dev/null 2>&1",

            ]),

        # avoid this in order to get nominal performance
        # "-l DEBUG"
        ]

    # cmd = " ".join(cmdParts)

    # write the above commands to a temporary shell script
    ftemp = TempFile(suffix = ".sh")
    for line in cmdParts:
        print >> ftemp,line

    ftemp.close()

    # make the script executable
    import stat
    os.chmod(ftemp.name, stat.S_IXUSR | stat.S_IRUSR)

    import subprocess
    child = subprocess.Popen("/bin/sh -c " + ftemp.name, shell = True)

    import signal

    def killProc():
        # print "killing pid",child.pid
        # os.killpg(- child.pid, signal.SIGKILL)

        # see http://stackoverflow.com/a/6481337/288875
        os.system("pkill -TERM -P %d" % child.pid)

    atexit.register(killProc)

    # wait until the xdaq process starts
    while True:
        import urllib2
        try:
            url = "http://%s:%d" % (host, port)
            # print "TRYING TO OPEN",url
            urllib2.urlopen(url)
            break
        except:
            time.sleep(1)



    return child

#----------------------------------------------------------------------

def sendFedStreamerCommand(cmd, host, port):
    """ sends a SOAP command to the fedstreamer """
    # run the start soap command
    soap_action,msg = makeSimpleCommandSoapMessage(classname = "FEDStreamer",instance = 0, command = cmd)

    url = "http://%s:%d" % (host, port)
    # print "url=",url

    req = urllib2.Request(url, msg,
                          { "SOAPAction": soap_action}
                          )
    u = urllib2.urlopen(req)
    headers = u.info()
    data = u.read()

    # print msg

#----------------------------------------------------------------------
# main
#----------------------------------------------------------------------
from optparse import OptionParser

parser = OptionParser(
    """
   usage: %prog [options]

   tool to test the fedkit library with the fedstreamer

   """)

# parser.add_option("--tree",
#                   dest="treeName",
#                   default = "Events",
#                   help="name of the tree to look at",
#                   metavar="TREE")

(options, ARGV) = parser.parse_args()


configDoc = makeXDAQconfig(host = host, port = port)
import tempfile

configFile = tempfile.NamedTemporaryFile(suffix = ".xml")
configFile.write(toXML(configDoc))
configFile.flush() # do not close, will be deleted when closed

# this will be automatically killed at program exit
xdaqProc = startXDAQ(configFile.name, host = host, port = port)
print >> sys.stderr, "XDAQ started"


soapClient = SOAPUtils.SOAPClient(host, port, "FEDStreamer", 0)

#----------

soapClient.send(soapClient.makeSimpleCommandMessage("start"), parseResponse = False)
# TODO: we should check whether this actually succeeded
# print response
# print "----------------------------------------"
print >> sys.stderr, "FedStreamer started"

# fetch the data rate periodically


# time.sleep(3600)
bandwidthValues = []

for i in range(10):

    # get the counters from the infospace
    response = soapClient.send(soapClient.makeParameterQueryMessage())
    # print response

    # get the item node (note the short xpath expression, we could
    # use the full one here but would need to define more namespaces...)
    import xml.xpath
    global context
    context = xml.xpath.Context.Context(response)
    context.setNamespaces(dict(
        p="urn:xdaq-application:FEDStreamer",
        soapenc="http://schemas.xmlsoap.org/soap/encoding/",
        ))

    # get the node (and ensure there is exactly one such node)
    measurementsNode = singleNode(context, "//p:item[@soapenc:position='[0]']")

    #print dir(measurementsNode)
    # print toXML(measurementsNode)
    #print [ x.nodeName for x in measurementsNode.childNodes ]

    # p:counter
    # p:latency
    # p:rate
    bandwidthNode = singleChildNodeByName(measurementsNode,'p:bandwidth')
    bandwidth = float(textContent(bandwidthNode).strip())

    bandwidthValues.append(bandwidth)

    print "bandwidth: %6.1f MByte/s" % bandwidth

    time.sleep(1)

# stop the fedstreamer
soapClient.send(soapClient.makeSimpleCommandMessage("stop"), parseResponse = False)
print >> sys.stderr, "FedStreamer stopped"

# check whether it still responds
time.sleep(1)
soapClient.send(soapClient.makeParameterQueryMessage())

print >> sys.stderr,"FEDStreamer responded after stop"

# calculate the average bandwidth, excluding the first measurement
bandwidthValues = bandwidthValues[1:]

avgBandwidth = sum(bandwidthValues) / float(len(bandwidthValues))

print >> sys.stderr, "average bandwidth: %6.1f MByte/s" % avgBandwidth

if avgBandwidth >= 400:
    print >> sys.stderr,"test successful"
else:
    print >> sys.stderr,"TEST FAILED"

