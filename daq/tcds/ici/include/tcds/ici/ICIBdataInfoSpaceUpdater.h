#ifndef _tcds_ici_ICIBdataInfoSpaceUpdater_h_
#define _tcds_ici_ICIBdataInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace ici {

    class TCADeviceICI;

    class ICIBdataInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      ICIBdataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               TCADeviceICI const& hw);
      virtual ~ICIBdataInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      tcds::ici::TCADeviceICI const& getHw();

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ICIBdataInfoSpaceUpdater_h_
