#include "bril/CommonDataFormat.h"
#include "bril/LUMITopics.hh"
#include <iostream>
#include <cassert>
#include <cstdlib>
#include <string>
#include <vector>
/**
 * unit tests of some helpers defined in the interface header files
 */
using namespace interface::bril;
int main(int argc, char** argv){
  std::cout<<"  === data version: "<<DATA_VERSION<<std::endl;
  std::cout<<"  === head dictionary: "<<bestlumiT::headdict()<<std::endl;
  std::cout<<"  === payload dictionary: "<<bestlumiT::payloaddict()<<std::endl;
  std::cout<<"  === topic description: "<<bestlumiT::description()<<std::endl;
  std::cout<<"  === topic unit: "<<bestlumiT::unit()<<std::endl;
  std::cout<<"  ===testing data type in/out dataholder === "<<std::endl;
  size_t totalsize = bestlumiT::maxsize();
  unsigned char *buffer=(unsigned char*)malloc(totalsize);
  if (buffer==NULL) exit (1);
  ((DatumHead*)buffer)->setTime(2364,123450,12,1,34556,0);
  ((DatumHead*)buffer)->setResource(DataSource::LUMI,0,0,StorageType::COMPOUND);
  ((DatumHead*)buffer)->setTotalsize(totalsize);
  ((DatumHead*)buffer)->setFrequency(4);
  std::cout<<((DatumHead*)buffer)->totalsize()<<std::endl;

  std::cout<<"  === payload stream in === "<<std::endl;
  CompoundDataStreamer cdata(bestlumiT::payloaddict());
  const char* lumiprovider = "bcm1f";
  const char* calibtag = "bcm1flumicalib_v1";
  float delivered=0.5;
  float recorded=0.45;
  float avgpu=0.1;
  float bxdelivered[3564];
  std::fill(bxdelivered,bxdelivered+3564,0.);
  for(int i=0;i<3564;++i){bxdelivered[i]=0.5*i;}
  cdata.insert_field( ((DatumHead*)buffer)->payloadanchor, "provider", (void*)lumiprovider );
  cdata.insert_field( ((DatumHead*)buffer)->payloadanchor, "calibtag", (void*)calibtag );
  cdata.insert_field( ((DatumHead*)buffer)->payloadanchor, "recorded", (void*)(&recorded) );
  cdata.insert_field( ((DatumHead*)buffer)->payloadanchor, "delivered", (void*)(&delivered) );
  cdata.insert_field( ((DatumHead*)buffer)->payloadanchor, "avgpu", (void*)(&avgpu) );
  cdata.insert_field( ((DatumHead*)buffer)->payloadanchor, "bxdelivered", (void*)bxdelivered );

  std::cout<<"  === payload stream out === "<<std::endl;
  char out_lumiprovider[] = "";
  char out_calibtag[] = "";
  float out_delivered = 0;
  float out_recorded = 0;
  float out_avgpu = 0;
  float out_bxdelivered[3564];
  std::fill(out_bxdelivered,out_bxdelivered+3564,0.);
  cdata.extract_field(out_lumiprovider, "provider", ((DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(out_calibtag, "calibtag", ((DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_delivered, "delivered", ((DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_recorded, "recorded", ((DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_avgpu, "avgpu", ((DatumHead*)buffer)->payloadanchor);
  cdata.extract_field(&out_bxdelivered, "bxdelivered", ((DatumHead*)buffer)->payloadanchor);
  std::cout<<"fillnum "<<((DatumHead*)buffer)->fillnum<<std::endl;
  std::cout<<"runnum "<<((DatumHead*)buffer)->runnum<<std::endl;
  std::cout<<"lumiprovider "<<out_lumiprovider<<std::endl;
  std::cout<<"calibtag "<<out_calibtag<<std::endl;
  std::cout<<"delivered "<<out_delivered<<std::endl;
  std::cout<<"recorded "<<out_recorded<<std::endl;
  std::cout<<"avgpu "<<out_avgpu<<std::endl;
  for(int i=0;i<3564;++i){ std::cout<<i<<" "<<out_bxdelivered[i]<<" ";}
  std::cout<<std::endl;
}
