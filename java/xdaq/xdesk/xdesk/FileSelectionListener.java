package xdaq.xdesk.xdesk;

import java.io.*;

public interface FileSelectionListener
{
	public void fileSelected (File f);
}
