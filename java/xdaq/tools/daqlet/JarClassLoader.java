package xdaq.tools.daqlet;

import java.net.MalformedURLException;
import java.net.URLClassLoader;
import java.net.URL;
import java.lang.*;
import java.io.*;

public class JarClassLoader extends URLClassLoader
{
	public JarClassLoader (URL url)
	{
		super ( new URL[] {url} );
	}

	public JarClassLoader (String href) throws MalformedURLException
	{		
		this ( new URL (href) );
	}
	
	public void addJarURL (String href) throws MalformedURLException
	{
		if (href != null)
			addURL ( new URL (href ) );
	}	
	
	
}
