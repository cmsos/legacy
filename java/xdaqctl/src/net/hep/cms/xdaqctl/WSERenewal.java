package net.hep.cms.xdaqctl;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;

import org.w3c.dom.Document;

/**
 * renewal messages for a WS-Eventing subscription
 *
 * @author Alexander Oh (alexander.oh@cern.ch)
 * @author Ichiro Suzuki (ichiro@fnal.gov)
 */
class WSERenewal extends Message {
	
	private String wseEndpoint;
	private SOAPElement expires;

	public WSERenewal(String wseEndpoint, String uuid)
			throws XDAQMessageException {

		super();
		
		this.wseEndpoint = wseEndpoint;
		String wseURL = wseEndpoint + "/" + WSE_TARGET;
		
		message.getMimeHeaders().addHeader("SOAPAction", WSE_TARGET);

		try {
			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
			SOAPElement e;
			
			// Header : { Action, To, ReplyTo, Identifier }
			SOAPHeader header = envelope.getHeader();
			e = header.addChildElement("Action", "wsa", WSA_NS);
			e.addTextNode(WSE_NS + "/Renew");
			e = header.addChildElement("To", "wsa", WSA_NS);
			e.addTextNode(wseURL);
			e = header.addChildElement("ReplyTo", "wsa", WSA_NS);
			e.addTextNode(wseURL);
			e = header.addChildElement("Identifier", "wse", WSE_NS);
			e.addTextNode(uuid);
			
			// Body : Renew : Expires
			SOAPBody body = envelope.getBody();
			SOAPElement renew = body.addBodyElement(
					envelope.createName("Renew", "wse", WSE_NS));
			
			expires = renew.addChildElement("Expires", "wse", WSE_NS);
			expires.addTextNode("PT5M");
			
		} catch (SOAPException e) {
			throw new XDAQMessageException(
					"Failed to create a WS-Eventing subscription SOAP message", e);
		}
	}

	public void renew() throws XDAQMessageException, XDAQTimeoutException {
		Document reply = send(wseEndpoint, WSE_TARGET);
		if (getFaultString() != null) {
			throw new XDAQMessageException("SOAP fault occured when sending renewal to " + wseEndpoint + ": " + getFaultString());
		}
	}

	public void setExpires(String expires) {
		this.expires.setValue(expires);
	}
}

// End of file
// vim: set sw=4 ts=4:
