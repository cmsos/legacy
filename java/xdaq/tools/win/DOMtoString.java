package xdaq.tools.win;

import org.w3c.dom.Node;
import java.io.StringWriter;
import java.util.Properties;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;

public class DOMtoString
{
	public static String serialize ( Node doc )
	{
		StringWriter outText = new StringWriter();
		StreamResult sr = new StreamResult ( outText );
		Properties oprops = new Properties();
		oprops.put (OutputKeys.METHOD, "html");
		oprops.put (OutputKeys.INDENT, "yes");
		
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = null;
		try {
			t = tf.newTransformer();
			t.setOutputProperties ( oprops );
			t.transform ( new DOMSource ( doc ), sr );
		} catch (Exception e )
		{
			System.out.println ("Could not serialize dom: " + e.toString());
			return e.toString();
		}
		return outText.toString();
	}
}
