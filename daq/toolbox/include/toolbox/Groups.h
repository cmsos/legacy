// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _toolbox_Groups_h_
#define _toolbox_Groups_h_

#include <pwd.h>

namespace toolbox 
{
	void setSupplementaryGroups(const struct passwd* pwd);
}

#endif
