/*
 * XDAQParameter
 *
 * $Id: XDAQParameter.java,v 4.3 2008/06/27 13:59:33 aohcms Exp $
 */

package net.hep.cms.xdaqctl;

import java.util.Vector;
import java.util.List;
import java.io.StringReader;

// DOM
import org.w3c.dom.*;
import org.w3c.dom.Node;  // to solve ambiguity with soap.Node

// XML transformer
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

// Constants
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI;
import static javax.xml.soap.SOAPConstants.URI_NS_SOAP_ENCODING;

/**
 * A class to retrieve/change XDAQ exported parameters
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XDAQParameter
{
	private String   endPoint = "";
	private String   soapAction = "";

	private Document domParameter = null;
	private DocumentFragment domDescriptor = null;
	private DefaultNamespaceContext nsContext;

	private int timeout = 0;

	/**
	 * Constructor of XDAQParameter for a specified target application
	 *
	 * @param  endPoint XDAQ executive endpoint, e.g. 'http://localhost:40000'
	 * @param  applicationClass XDAQ application class name of the target
	 * @param  instance XDAQ application instance number of the target
	 */
	public XDAQParameter(String endPoint, String applicationClass, int instance)
	{
		this(endPoint, "urn:xdaq-application:" +
				"class=" + applicationClass + ",instance=" + instance);
	}

	/**
	 * Constructor of XDAQParameter for a specified target application
	 *
	 * @param  endPoint XDAQ executive endpoint, e.g. 'http://localhost:40000'
	 * @param  soapAction SOAPAction string of the target,
	 *         e.g. 'urn:xdaq-application:lid=11'
	 *              'urn:xdaq-application:class=MyApp,instance=0' .
	 */
	public XDAQParameter(String endPoint, String soapAction)
	{
		this.endPoint = endPoint;
		this.soapAction = soapAction;

		this.domParameter = null;

		nsContext = new DefaultNamespaceContext();

		// Do nothing here.  
		// The parameter is lazily initialized with queryParameter() .
	}

	/**
	 *
	 */
	private void queryParameter()
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (endPoint == null || soapAction == null) {
			throw new XDAQException("EndPoint or SOAP action are not set yet.");
		}

		Document reply = sendMessage("ParameterQuery");

		if (reply != null) {
			try {
				analyzeReply(reply);
			} catch (XPathAPIException e) {
				throw new XDAQException(
						"Failed to analyze the reply from " + endPoint + ".",
						e);
			}
		}
	}

	/**
	 * Constructor.  Parameter structure is supplied as an XML string.
	 *
	 * @param  endPoint XDAQ executive endpoint, e.g. 'http://localhost:40000'
	 * @param  applicationClass XDAQ application class name of the target
	 * @param  instance XDAQ application instance number of the target
	 * @param  xmlString XDAQ parameter XML
	 */
	public XDAQParameter(String endPoint,
			String applicationClass, int instance, String xmlString)
			throws XDAQException
	{
		this(endPoint, "urn:xdaq-application:" + "class=" + applicationClass +
				",instance=" + instance, xmlString);
	}

	/**
	 * Constructor.  Parameter structure is supplied as an XML string.
	 *
	 * @param  endPoint XDAQ executive endpoint, e.g. 'http://localhost:40000'
	 * @param  soapAction SOAPAction string of the target,
	 *         e.g. 'urn:xdaq-application:lid=11'
	 *              'urn:xdaq-application:class=MyApp,instance=0' .
	 * @param  xmlString XDAQ parameter XML
	 */
	public XDAQParameter(String endPoint, String soapAction, String xmlString)
			throws XDAQException
	{
		this.endPoint   = endPoint;
		this.soapAction = soapAction;

		setParameterDOM(xmlString);

		nsContext = new DefaultNamespaceContext();
		nsContext.put("p", domParameter.getDocumentElement().getNamespaceURI());
		nsContext.put("xsi", W3C_XML_SCHEMA_INSTANCE_NS_URI);
	}

	/**
	 * Copy constructor
	 *
	 * @param  original Original XDAQParameter instance
	 */
	public XDAQParameter(XDAQParameter original)
			throws XDAQException,
			       XDAQTimeoutException
	{
		endPoint   = original.endPoint;
		soapAction = original.soapAction;
		nsContext  = new DefaultNamespaceContext(original.nsContext);
		timeout    = original.timeout;

		domParameter = null;

		if (original.domParameter != null) {
			// Create a transformer
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer;
			try {
				transformer = tf.newTransformer();
			} catch (TransformerConfigurationException e) {
				throw new XDAQRuntimeException(e);
			}

			// Create an instance of Result for a DOM tree
			DOMSource domSource = new DOMSource(original.domParameter);
			DOMResult domResult = new DOMResult();

			// Transform !!
			try {
				transformer.transform(domSource, domResult);
			} catch (TransformerException e) {
				throw new XDAQRuntimeException(e);
			}

			domParameter = (Document)domResult.getNode();
		}
	}

	/** 
	 * Set the parameter's internal XML respresentation from a string.
	 *
	 * @param  xmlString
	 *
	 * @see    XDAQDocument.getXDAQParameter(String, String, int)
	 */
	private void setParameterDOM(String xmlString) 
			throws XDAQException
	{
		// Create a transformer
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
		} catch (TransformerConfigurationException e) {
			throw new XDAQRuntimeException(e);
		}

		// Create an instance of Result for a DOM tree
		StreamSource streamSource =
				new StreamSource(new StringReader(xmlString));
		DOMResult domResult = new DOMResult();

		// Transform !!
		try {
			transformer.transform(streamSource, domResult);
		} catch (TransformerException e) {
			throw new XDAQException(e);
		}

		domParameter = (Document)domResult.getNode();
	}

	/**
	 * Expose the internal DOM document to be manipulated by users.
	 *
	 * @return org.w3c.dom.Document
	 */
	public Document getDOMParameter()
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }

		return domParameter;
	}

	/**
	 * Get a text value of a parameter.
	 *
	 * The value currently kept inside the instance is returned.
	 * This is not necessary same as application's current value.
	 *
	 * @param  name Parameter name
	 * @return String value of the parameter
	 *
	 * @see    #get()
	 */
	public String getValue(String name)
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return null; }

		Node parameter;
		try {
			parameter = XPathAPI.selectSingleNode(
					domParameter, "//p:" + name, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find a parameter " + name + ".", e);
		}

		if (parameter == null) { return null; }

		Node valueNode = parameter.getFirstChild();
		
		if (valueNode != null) {
			return valueNode.getNodeValue();
		} else {
			return "";
		}
	}

	/**
	 * Get a text value of a descriptor parameter.
	 *
	 * The value currently kept inside the instance is returned.
	 * This is not necessary same as application's current value.
	 *
	 * @param  name A descriptor name (className, instance, ...)
	 * @return String Value of the descriptor
	 */
	public String getDescriptor(String name)
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return null; }

		if (domDescriptor == null) { return null; }

		Node parameter;
		try {
			parameter = XPathAPI.selectSingleNode(
					domDescriptor, "//*[local-name()='" + name + "']");
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find a descriptor " + name + ".", e);
		}

		return parameter.getFirstChild().getNodeValue();
	}

	/**
	 * Get a vector of parameters.
	 *
	 * The value currently kept inside the instance is returned.
	 * This is not necessary same as application's current value.
	 *
	 * @param  name vector Parameter name
	 * @return String[] values of the vector
	 *
	 * @see    #get()
	 */
	public String[] getVector(String name)
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return null; }

		Element parameter;
		try {
			parameter = (Element)XPathAPI.selectSingleNode(
					domParameter, "//p:" + name, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find a vector " + name + ".", e);
		}

		if (parameter == null) { return null; }

		String s; // temp. variable

		s = parameter.getAttributeNS(W3C_XML_SCHEMA_INSTANCE_NS_URI, "type");
		if (!s.equals("soapenc:Array")) { return null; }

		s = parameter.getAttributeNS(URI_NS_SOAP_ENCODING, "arrayType");
		int size = Integer.parseInt(
				s.substring(s.indexOf("[") + 1, s.indexOf("]")));


		String[] result = new String[size];
		if (size == 0) { return result; }

		NodeList items;
		try {
			items = XPathAPI.selectNodeList(parameter, "p:item", nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find items in the vector " + name + ".", e);
		}

		if (items == null || items.getLength() < 1) { return null; }

		for (int i = 0; i < items.getLength(); ++i) {
			Element item = (Element)items.item(i);

			s = item.getAttributeNS(URI_NS_SOAP_ENCODING, "position");
			int pos = Integer.parseInt(s.substring(1, s.indexOf("]")));

			result[pos] = item.getFirstChild().getNodeValue();
		}

		return result;
	}

	/**
	 * Remove a parameter.
	 *
	 * Only the internal XML structure is manipulated.
	 * No effect to the owner XDAQ application.
	 *
	 * @param  name Parameter name to remove
	 * @return String value of the removed parameter
	 *
	 * @see    #get() #send()
	 */
	public String remove(String name)
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return null; }

		// Find a parameter node with given name
		Node parameter;
		try {
			parameter = XPathAPI.selectSingleNode(
					domParameter, "//p:" + name, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find a parameter " + name + ".", e);
		}

		if (parameter == null) { return null; }

		// Remove the parameter and return the value of the removed parameter
		String value = parameter.getFirstChild().getNodeValue();
		parameter.getParentNode().removeChild(parameter);

		return value;
	}

	/**
	 * Remove all the parameters except the specified one.
	 *
	 * Only the internal XML structure is manipulated.
	 * No effect to the owner XDAQ application.
	 *
	 * @param  name Parameter name to keep
	 * @return # of removed parameters
	 *
	 * @see    #get() #send()
	 */
	public int select(String name)
			throws XDAQException,
			       XDAQTimeoutException
	{
		return select(new String[] { name });
	}

	/**
	 * Remove all the parameters except the specified one.
	 *
	 * Only the internal XML structure is manipulated.
	 * No effect to the owner XDAQ application.
	 *
	 * @param  names Parameter names to keep
	 * @return # of removed parameters
	 *
	 * @see    #get() #send()
	 */
	public int select(String[] names)
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return -1; }

		// Nodes, which is neither ancestor or descendant of the target name
		// //*[not(descendant::p:v)][not(ancestor::p:v)][name()!='p:v']
		String path = "//*";
		for (int i = 0; i < names.length; ++i) {
			String n = "p:" + names[i];
			path += "[not(descendant::" + n + ")][not(ancestor::" +
					n + ")][name()!='" + n + "']";
		}

		NodeList parameters;
		try {
			parameters = XPathAPI.selectNodeList(domParameter, path, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find parameter with an expression, " +
					path + ".", e);
		}

		for (int i = 0; i < parameters.getLength(); ++i) {
			Node aParameter = parameters.item(i);

			aParameter.getParentNode().removeChild(aParameter);
		}

		return parameters.getLength();
	}

	/**
	 * Set a parameter to a value.
	 *
	 * Only the internal XML structure is manipulated.
	 * The changed value is not propagated to the owner XDAQ application.
	 *
	 * @param  name Parameter name
	 * @param  value Parameter value
	 * @return The value set
	 *
	 * @see    #send()
	 */
	public String setValue(String name, String value)
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return null; }

		// Find a parameter node with given name
		Node parameter;
		try {
			parameter = XPathAPI.selectSingleNode(
					domParameter, "//p:" + name, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find a parameter " + name + ".", e);
		}

		if (parameter == null) { return null; }

		// Change the parameter value and return it
		Node valueNode = parameter.getFirstChild();

		if (valueNode != null) {
			valueNode.setNodeValue(value);
		} else {
			valueNode = domParameter.createTextNode(value);
			parameter.appendChild(valueNode);
		}

		return valueNode.getNodeValue();
	}

	/**
	 * Set a vector parameter
	 *
	 * Only the internal XML structure is manipulated.
	 * The changed value is not propagated to the owner XDAQ application.
	 *
	 * @param  name Parameter vector name
	 * @param  values Parameter vector of values
	 * @return clone of the input argument, 'values'
	 *
	 * @see    #send()
	 */
	public String[] setVector(String name, String[] values)
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return null; }

		// Find a parameter node with given name
		Element parameter;
		try {
			parameter = (Element)XPathAPI.selectSingleNode(
					domParameter, "//p:" + name, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find a vector " + name + ".", e);
		}

		if (parameter == null) { return null; }
		parameter.getAttributeNodeNS(URI_NS_SOAP_ENCODING, "arrayType")
				.setValue("xsd:ur-type[" + values.length + "]");

		// Keep the first item to get the type information,
		// delete all the existing elements, and
		// and add the new elements.
		// If the vector has no element, return null.
		// (The first item is necessary to find out the element type.)

		Element item, first;

		try {
			first = (Element)XPathAPI.selectSingleNode(
					parameter, "p:item", nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find items in the vector " + name + ".", e);
		}

		NodeList items;
		try {
			items = XPathAPI.selectNodeList(parameter, "p:item", nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find items in the vector " + name + ".", e);
		}
		for (int i = 0; i < items.getLength(); ++i) {
			parameter.removeChild(items.item(i));
		}

		// Create new elements
		for (int i = 0; i < values.length; ++i) {
			item = (Element)first.cloneNode(true);
			parameter.appendChild(item);

			item.getAttributeNodeNS(URI_NS_SOAP_ENCODING, "position")
					.setValue("[" + i + "]");
			item.getFirstChild().setNodeValue(values[i]);
		}

		return (String[])values.clone();
	}

	/**
	 * Get a list of all 'simple value parameter' names
	 * Structured ('bag' or 'vector') type parameters are excluded.
	 * Nested paramters are also excluded because
	 *  uniqueness of names is not guaranteed.
	 *
	 * @return list of simple, top-level parameter names
	 */
	public List<String> getNames()
			throws XDAQException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return null; }

		Vector<String> names = new Vector<String>();

		// all nodes directly under <properties> and not complex types
		NodeList nodes;
		try {
			nodes = XPathAPI.selectNodeList(domParameter,
					"p:properties/p:*[@xsi:type!='soapenc:Struct']" +
					"[@xsi:type!='soapenc:Array']",
					nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to list parameter names.", e);
		}

		for (int i = 0; i < nodes.getLength(); ++i) {
			names.add(nodes.item(i).getLocalName());
		}

		return names;
	}

	/**
	 * Get the current parameters.
	 *
	 * Retrieve the owner XDAQ application's current parameter values.
	 * Only parameters listed in the internal XML structure are updated.
	 * 
	 * @return false in case of &lt;Fault>
	 * 
	 * @see    #getValue(String)
	 */
	public boolean get()
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) {
			queryParameter();
			return (domParameter != null);
		}

		Document reply = sendMessage("ParameterGet");

		if (reply != null) {
			try {
				analyzeReply(reply);
			} catch (XPathAPIException e) {
				throw new XDAQException(
						"Failed to analyze the reply from " + endPoint + ".",
						e);
			}
		}

		// returns true in success, false in failure
		return (reply != null);
	}

	/**
	 * Send parameters to the owner XDAQ application.
	 *
	 * XDAQ application's exported parameters are modified.
	 *
	 * @return false in case of &lt;Fault>
	 *
	 * @see    #setValue(String, String)
	 */
	public boolean send()
			throws XDAQException,
			       XDAQTimeoutException
	{
		if (domParameter == null) { queryParameter(); }
		if (domParameter == null) { return false; }

		Document reply = sendMessage("ParameterSet");

		// returns true in success, false in failure
		return (reply != null);
	}

	/**
	 * Common part of get() and send() (and, queryParameter())
	 *
	 * @param  command SOAP command string
	 *
	 * @return reply DOM document, null in case of &lt;Fault>
	 */
	private Document sendMessage(String command)
			throws XDAQException,
			       XDAQTimeoutException
	{
		// Almost same as the constructor,
		// except reuse of the connection information
		XDAQMessage message;
		Document reply;
		try {
			if (command.equals("ParameterQuery")) {
				message = new XDAQMessage(command);
			} else {
				message = new XDAQMessage(
						command, domParameter.getDocumentElement());
			}
			message.setTimeout(timeout);
			reply = message.send(endPoint, soapAction);
		} catch (XDAQMessageException e) {
			throw new XDAQException("Failed to send a SOAP command, " +
					command + ", to " + endPoint + ".", e);
		}

		if (message.getFaultString() != null) { 
			throw new XDAQException(
					"Could not get XDAQ parameter. SOAP fault occured:" 
					+ message.getFaultString()
					);
		}

		return reply;
	}

	/**
	 * @return timeout value in milliseconds
	 */
	public int getTimeout()
	{
		return timeout;
	}

	/**
	 * Set timeout.
	 * If timeout is non-zero, additional thread is created at eash send().
	 *
	 * @param  aTimeout timeout value set, in milliseconds
	 */
	public void setTimeout(int aTimeout)
	{
		timeout = aTimeout;
	}

	/**
	 * Strip the return Document to a properties tree
	 *
	 * Changes private variables, domParameter, domDescriptor and nsContext.
	 */
	private void analyzeReply(Document reply)
			throws XPathAPIException
	{
		Element root = reply.getDocumentElement();

		// Find the root <properties> node to be used as the new root element.
		Element properties = (Element)XPathAPI.selectSingleNode(
				reply, "//*[local-name()='properties']");

		// find out the namespace
		final String nsURI = properties.getNamespaceURI();
		nsContext.put("p", nsURI);
		nsContext.put("xsi", W3C_XML_SCHEMA_INSTANCE_NS_URI);

		// Remove unnecessary properties, i.e. stub and descriptor.
		// Descriptor is removed, but set aside in 'descriptor'.
		Node node;

		node = reply.getElementsByTagNameNS(nsURI, "stub").item(0);
		if (node != null ) { node.getParentNode().removeChild(node); }

		node = reply.getElementsByTagNameNS(nsURI, "descriptor").item(0);
		if (node != null ) {
			domDescriptor = reply.createDocumentFragment();
			domDescriptor.appendChild(node.cloneNode(true));

			node.getParentNode().removeChild(node);
		}

		// Replace the top Element (<ParameterQueryResponse>)
		// with the parameter properties
		reply.replaceChild(properties, root);

		domParameter = reply;

		return;
	}

	/**
	 * Shut down thread pools used in XDAQMessage for time out management.
	 */
	public static void shutdownThreadPool() throws InterruptedException
	{
		XDAQMessage.shutdownThreadPool();
	}
}

// End of file
// vim: set ts=4 sw=4:
