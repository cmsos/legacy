package xdaq.xdesk.tools;

import java.net.*;

import javax.swing.tree.*;

import org.apache.log4j.*;
import org.w3c.dom.*;


public class MessageFactory {
  static Logger logger = Logger.getLogger(MessageFactory.class);

  /*! Pass a JTree selection and a command.
   Currently only a single command is allowed that will be used
   for all items selected in the tree
   */
  static public MessageSet createMessageSet(TreePath[] paths, String command,
                                            Object source) {
    MessageSet ms = new MessageSet();
    System.out.println("paths = " + paths);
    for (int i = 0; i < paths.length; i++) {
      Message m = createMessage(paths[i], command, source);
      ms.add(m);
    }

    return ms;
  }

  static public String createURL(TreePath p) {
    DefaultMutableTreeNode node = (DefaultMutableTreeNode) p.
        getLastPathComponent();
    IUserObject aNode = (IUserObject) node.getUserObject();

    String url = null;

    if (node.toString().startsWith("Host")) {
      url = new String(aNode.getAttribute ("url"));
      url += "/xdaq-" + aNode.getAttribute ("id");
      try {
        URL u = new URL(url);
      }
      catch (MalformedURLException urlException) {
        logger.error("Cannot create URL: " + urlException.toString());
      }
    }
    else if (node.toString().startsWith("Application") ||
             node.toString().startsWith("Transport")) {
      //Find out the closest encapsulating <Host> tag for the url
      IUserObject father = ((IUserObject) node.getUserObject()).getParent();

      url = new String(father.getAttribute ("url"));
      url += "/tid-" + aNode.getAttribute ("targetAddr");
    }

    return url;
  }

  static public Message createMessage(TreePath p, String command, Object source) {
    System.out.println("The source is " + source.getClass().toString());
    Document doc = null;

    DefaultMutableTreeNode node = (DefaultMutableTreeNode) p.
        getLastPathComponent();
    System.out.println("node = " + node);
    IUserObject aNode = (IUserObject) node.getUserObject();
    doc = aNode.getDomNode().getOwnerDocument();
    Message m = null;

    String url = null;

    if (node.toString().startsWith("Host")) {
      url = new String(aNode.getAttribute("url"));
      url += "/xdaq-" + aNode.getAttribute("id") + "#" + command;
      try {
        URL u = new URL(url);
        m = new Message(command, u, source);

        // Pass whole configuration when message goes to executive and command is configure
        if (command.startsWith("Configure")) {
          m.setData(doc.getDocumentElement());
        }
      }
      catch (MalformedURLException urlException) {
        logger.error("Cannot create SOAP message: " + urlException.toString());
      }
    }
    else if (node.toString().startsWith("Application") ||
             node.toString().startsWith("Transport")) {
      // Find out the closest encapsulating <Host> tag for the url
      IUserObject father = ( (IUserObject) node.getUserObject()).getParent();

      url = new String(father.getAttribute("url"));
      url += "/tid-" + aNode.getAttribute("targetAddr") + "#" + command;

      try {
        m = new Message(command, new URL(url), source);
      }
      catch (MalformedURLException urlException) {
        logger.error("MessageFactory:Cannot create SOAP message: " +
                     urlException.toString());
      }
    }

System.out.println("URL: " + m.getDOM().toString());
    return m;
  }
}
