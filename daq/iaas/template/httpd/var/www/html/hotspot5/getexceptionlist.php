<?php
require '../utils/vendor/autoload.php';
include_once ('config.php');

$_nodeid = $_GET ['nodeid'];
$_state = $_GET ['state'];
$_arcurl = $_GET ['arcurl'];

$response = Network::httpget ( $_arcurl . "/getExceptionList?nodeid=" . $_nodeid . "&state=" . $_state );

header ( 'Content-Type: application/json; charset=utf-8' );
echo $response->getBody();
?>