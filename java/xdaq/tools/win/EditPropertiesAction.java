package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.PropertiesWindow;

public class EditPropertiesAction extends AbstractAction
{
	xdaqWin win_;

	public EditPropertiesAction (xdaqWin frame)
	{
		super ("Properties");
		win_ = frame;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		PropertiesWindow w = new PropertiesWindow ( win_ );
	}
}
