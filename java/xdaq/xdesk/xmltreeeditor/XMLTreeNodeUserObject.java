package xmltreeeditor;

import java.util.*;

import javax.swing.*;

import org.w3c.dom.*;

public class XMLTreeNodeUserObject {
  private Hashtable icons;
  private String name;
  private String value;
  private String attrList;
  private short type;

  private ImageIcon icon;

  public XMLTreeNodeUserObject(String new_name, String new_value,
                               short new_type) {
    initializeHashtable();
    icon = getIcon(new_type);
    name = new_name;
    value = new_value;
    type = new_type;
    attrList = "";
  }

  public XMLTreeNodeUserObject(Node childElement) {
    initializeHashtable();
    name = childElement.getNodeName();
    value = childElement.getNodeValue();
    type = childElement.getNodeType();
    attrList = getAttrList(childElement);
    icon = getIcon(type);

  }

  private void initializeHashtable() {
    icons = new Hashtable();
    icons.put("1", new ImageIcon("Element16.gif"));
    icons.put("2", new ImageIcon("Attribute16.gif"));
    icons.put("3", new ImageIcon("Text16.gif"));
    icons.put("4", new ImageIcon("cdata16.gif"));
    icons.put("7", new ImageIcon("comment16.gif"));
    icons.put("8", new ImageIcon("processing16.gif"));

  }

  public ImageIcon getIcon(short n_type) {
    return (ImageIcon) icons.get(new Short(n_type).toString());
  }

  String getAttrList(Node n_node) {
    String temp_AttrList = "";

    if (n_node.getNodeType() == Node.ELEMENT_NODE) {

      NamedNodeMap elementAttributes =
          n_node.getAttributes();

      if (elementAttributes != null && elementAttributes.getLength() > 0) {
        temp_AttrList = " (";
        int numAttributes = elementAttributes.getLength();
        for (int i = 0; i < numAttributes; i++) {
          Node attribute = elementAttributes.item(i);
          if (i > 0) {
            temp_AttrList = temp_AttrList + ", ";
          }
          temp_AttrList = temp_AttrList + attribute.getNodeName() +
              "=" + attribute.getNodeValue();
        }
        temp_AttrList = temp_AttrList + ")";
      }

    }
    return temp_AttrList;
  }

  public String toString() {
    return name;
  }

  public short getType() {
    return type;
  }

  public String getName() {
    return name.toString();
  }

  public String getValue() {
    return value;
  }

  public String getAttrList() {
    return attrList;
  }

  public ImageIcon getIcon() {
    return icon;
  }

  public void setName(String n_name) {
    name = n_name;
  }

  public void setValue(String n_value) {
    value = n_value;
  }

  public void setAttrList(String n_AttrList) {
    attrList = n_AttrList;
  }

}
