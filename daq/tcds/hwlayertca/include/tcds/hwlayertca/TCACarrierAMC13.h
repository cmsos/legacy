#ifndef _tcds_hwlayertca_TCACarrierAMC13_h_
#define _tcds_hwlayertca_TCACarrierAMC13_h_

#include <string>
#include <vector>

#include "tcds/hwlayertca/TCACarrierBase.h"

namespace tcds {
  namespace hwlayertca {

    class HwDeviceTCA;

    /**
     * 'Fake' FMC carrier implementation to support the AMC13.
     */
    class TCACarrierAMC13 : public TCACarrierBase
    {

    public:
      TCACarrierAMC13(HwDeviceTCA& device);
      ~TCACarrierAMC13();

    protected:
      virtual std::string registerNamePrefix() const;
      virtual std::vector<std::string> fmcNamesImpl() const;
      virtual bool isFMCPowerGoodImpl(std::string const& fmcName) const;
      virtual void loadFirmwareImageImpl(std::string const& imageName) const;
      virtual void powerUpImpl() const;
      virtual void selectClockSourceImpl(tcds::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const;
      virtual void enableSFPsImpl() const;
      virtual void disableSFPsImpl() const;

      virtual std::string expectedCarrierType() const;

    };

  } // namespace hwlayertca
} // namespace tcds

#endif // _tcds_hwlayertca_TCACarrierAMC13_h_
