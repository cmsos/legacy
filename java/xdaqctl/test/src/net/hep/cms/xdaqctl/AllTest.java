/*
 * AllTest
 *
 * $Id: AllTest.java,v 4.2 2007/11/30 11:21:20 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import junit.framework.*;

/**
 * AllTest, runs all the tests
 */
public class AllTest extends TestCase
{
	public AllTest(String name)
	{
		super(name);
	}

	public static Test suite()
	{
		TestSuite suite = new TestSuite();

		suite.addTest(net.hep.cms.xdaqctl.xdata.AllTest.suite());
		suite.addTest(XPathAPITest.suite());
		suite.addTest(XDAQMessageTest.suite());
		suite.addTest(XDAQParameterTest.suite());
		suite.addTest(XDAQDocumentTest.suite());
		suite.addTest(ComplexParamTest.suite());

		return suite;
	}
}

// End of file
// vim: set ai ts=4 sw=4:
