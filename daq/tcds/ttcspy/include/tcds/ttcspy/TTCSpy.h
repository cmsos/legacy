#ifndef _tcds_ttcspy_TTCSpy_h_
#define _tcds_ttcspy_TTCSpy_h_

#include <memory>

#include "xdaq/Application.h"

#include "tcds/ttcspy/TCADeviceTTCSpy.h"
#include "tcds/utils/XDAQAppWithFSMBasic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwutilstca {
    class HwIDInfoSpaceHandlerTCA;
    class HwIDInfoSpaceUpdaterTCA;
  }
}

namespace tcds {
  namespace ttcspy {

    class HwStatusInfoSpaceHandler;
    class HwStatusInfoSpaceUpdater;
    class TTCSpyInfoSpaceHandler;
    class TTCSpyInfoSpaceUpdater;

    class TTCSpy : public tcds::utils::XDAQAppWithFSMBasic
    {

    public:
      XDAQ_INSTANTIATOR();

      TTCSpy(xdaq::ApplicationStub* const stub);
      ~TTCSpy();

    protected:
      /**
       * Access the hardware pointer in TCADeviceTTCSpy* format.
       */
      virtual TCADeviceTTCSpy& getHw() const;

      /**
       * Connect to hardware/release hardware.
       */
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

      virtual void hwCfgInitializeImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceUpdaterTCA> hwIDInfoSpaceUpdaterP_;
      std::auto_ptr<tcds::hwutilstca::HwIDInfoSpaceHandlerTCA> hwIDInfoSpaceP_;
      std::auto_ptr<tcds::ttcspy::HwStatusInfoSpaceUpdater> hwStatusInfoSpaceUpdaterP_;
      std::auto_ptr<tcds::ttcspy::HwStatusInfoSpaceHandler> hwStatusInfoSpaceP_;
      // BUG BUG BUG
      // Needs fixing!
      // std::auto_ptr<TTCSpyInfoSpaceHandler> ttcspyInfoSpaceUpdaterP_;
      // BUG BUG BUG end
      std::auto_ptr<TTCSpyInfoSpaceHandler> ttcspyInfoSpaceP_;

    };

  } // namespace ttcspy
} // namespace tcds

#endif // _tcds_ttcspy_TTCSpy_h_
