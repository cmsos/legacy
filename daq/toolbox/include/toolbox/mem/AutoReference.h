// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_task_AutoReference_h_
#define _toolbox_task_AutoReference_h_

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/CountingPtr.h"
#include "toolbox/mem/SimpleReferenceCount.h"
#include "toolbox/mem/StandardObjectPolicy.h"

namespace toolbox 
{
	namespace mem 
	{

		class ReferenceObjectPolicy
		{
  			public:
   			template<typename T> void dispose (T* object)
    			{
        			if (object != 0 ) object->release();
    			}
		};

		typedef toolbox::mem::CountingPtr<toolbox::mem::Reference, toolbox::mem::SimpleReferenceCount,toolbox::mem::ReferenceObjectPolicy> AutoReference;
	}
}
#endif
