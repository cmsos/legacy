/*
 * Message
 *
 * $Id: Message.java,v 4.3 2008/06/27 13:59:33 aohcms Exp $
 */

package net.hep.cms.xdaqctl;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeoutException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * XDAQ SOAP message class
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
abstract class Message
{
	static final String WSA_NS = "http://www.w3.org/2005/08/addressing";
	static final String WSE_NS = "http://schemas.xmlsoap.org/ws/2004/08/eventing";
	static final String XPATH_DIALECT = "http://www.w3.org/TR/1999/REC-xpath-19991116";

	static final String XDAQ_NS = "urn:xdaq-soap:3.0";
	static final String XMAS_NS =
			"http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10";
	static final String XMAS_SENSOR_NS =
			"http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-sensor-10";

	static final String WSE_TARGET = "urn:xdaq-application:service=ws-eventing";

	private final static int N_POOLED_THREADS = 100;

	private static ExecutorService primaryExecutor = null;
	private static ExecutorService secondaryExecutor = null;

	private SOAPConnection connection;
	protected SOAPMessage message;
	private SOAPMessage senderReply;
	private int timeout;
	private Transformer transformer;

	
	private static SOAPMessage call_with_retry(SOAPConnection conn, SOAPMessage message, String endpoint) throws SOAPException {
		final int delays_ms[] = { 100, 100, 200, 500, 1000, 1000, 2000, 2000, 5000, 5000, 5000};
		
		SOAPMessage m = null;
		int itry=0;
		while (m==null && (itry++) < delays_ms.length) {
			try {
				m=conn.call(message, endpoint);
			}
			catch (SOAPException e) {
				if (e.getCause() != null &&
					e.getCause().getCause() != null &&
					e.getCause().getCause() instanceof java.net.UnknownHostException ) {
					
					int delay = delays_ms[itry-1];
					System.err.println("net.hep.cms.xdaqctl.Message: SAAJ0009 error caused by java.net.UnknownHostException on try #" + itry + "\n" +
							"endpoint=" + endpoint + " - retrying in " + delay + " ms. Stack trace follows:\n");
					e.printStackTrace();
					try {
						Thread.sleep(delay); //millis
					} catch (InterruptedException e1) {
					}
				}
				else
					throw e;
			}
		}
		return m;		
	}

	private static SOAPMessage call_with_log(SOAPConnection conn, SOAPMessage message, String endpoint) throws SOAPException {
		SOAPMessage m = null;
		try {
			m=conn.call(message, endpoint);
		}
		catch (SOAPException e) {

			System.err.println(makeTimestamp()  + " net.hep.cms.xdaqctl.Message: Caught SOAPException, probably due to SAAJ0009. endpoint = " + endpoint + "\nStack trace follows:\n");
			e.printStackTrace();
			System.err.println("\n");
			throw e;
		}
		return m;		
	}

	private static String makeTimestamp() {
		Date time = Calendar.getInstance().getTime();				
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
		dfs.setGroupingSeparator('\'');
		DecimalFormat df = new DecimalFormat(",###", dfs);
		return "[" + dateFormat.format(time.getTime()) + "]";
	}
	
	
	/**
	 * Helper task to send a message.
	 */
	private class Sender implements Callable<SOAPMessage>
	{
		private String endpoint;

		public Sender(String anEndpoint)
		{
			endpoint = anEndpoint;
		}

		public SOAPMessage call() throws SOAPException
		{
			return call_with_retry(connection, message, endpoint);
		}
	}

	/**
	 * Stopwatch process
	 */
	private class Watcher implements Callable<SOAPMessage>
	{
		private Callable<SOAPMessage> task;

		public Watcher(Callable<SOAPMessage> aTask)
		{
			task = aTask;
		}

		public SOAPMessage call()
				throws TimeoutException,
				       ExecutionException,
				       InterruptedException
		{
			Future<SOAPMessage> future = secondaryExecutor.submit(task);

			SOAPMessage result;
			try {
				result = future.get(timeout, MILLISECONDS);
			}
			finally {
				future.cancel(true);
			}
			
			return result;
		}
	}

	/**
	 * Create an empty SOAP message.
	 */
	public Message() throws XDAQMessageException {
		try {
			initialize();
		} catch (XDAQMessageException e) {
			throw new XDAQMessageException("Failed to initialize" , e);
		}
	}

	/**
	 * Create a SOAP message from a SOAP message
	 * The message contains a copy of the SOAP message given in the argument.
	 * Only SOAPAction header is rewritten in send().
	 * 
	 * @param  soapMessage SOAP message
	 */
	public Message(SOAPMessage soapMessage)
			throws XDAQMessageException
	{
		initialize(); // to prepare all other stuff

		message = soapMessage;
	}

	/**
	 */
	protected void initialize() throws XDAQMessageException
	{
		// by default, no timeout
		timeout = 0;

		// Create a SOAP connection
		try {
			SOAPConnectionFactory factory = SOAPConnectionFactory.newInstance();
			connection = factory.createConnection();
		} catch (SOAPException e) {
			throw new XDAQMessageException(
					"Failed to create a SOAP connection", e);
		}

		// Create a SOAP message
		try {
			MessageFactory mf = MessageFactory.newInstance();
			message = mf.createMessage();
		} catch (SOAPException e) {
			throw new XDAQRuntimeException(
					"Failed to create a new SOAP message", e);
		}

		// for a work-around of a SOAPPart bug
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
		} catch (TransformerConfigurationException e) {
			throw new XDAQRuntimeException(
					"Failed to create a new XML transformer.", e);
		}

	}

	/**
	 * Send the message.
	 *
	 * @param  endpoint XDAQ executive endpoint, e.g. 'http://localhost:40000'
	 * @param  localID  local ID of the target application
	 *
	 * @return DOM document, which is either the node directly under <Body>,
	 *         or <Fault>
	 */
	public Document send(String endpoint, int localID)
			throws XDAQMessageException,
			       XDAQTimeoutException
	{
		return send(endpoint, "urn:xdaq-application:" +
				"lid=" + localID);
	}

	/**
	 * Send the message.
	 *
	 * @param  endpoint XDAQ executive endpoint, e.g. 'http://localhost:40000'
	 * @param  applicationClass XDAQ application class name of the target
	 * @param  instance XDAQ application instance number of the target
	 *
	 * @return DOM document, which is either the node directly under <Body>,
	 *         or <Fault>
	 */
	public Document send(String endpoint, String applicationClass, int instance)
			throws XDAQMessageException,
			       XDAQTimeoutException
	{
		return send(endpoint, "urn:xdaq-application:" +
				"class=" + applicationClass + ",instance=" + instance);
	}

	/**
	 * Send the message.
	 *
	 * If timeout=0, no extra thread is created.
	 *
	 * If timeout>0, send() is submitted to the primary fixed number thread
	 *  pool first, which regulates # of concurrent threads.  Then, when
	 *  executed, it is submitted to the secondary cached thread pool
	 *  where actual messaging happens.
	 * So, some of the send() calls on multiple instances may be delayed
	 *  due to congestions.
	 *
	 * @param  endpoint XDAQ executive endpoint, e.g. 'http://localhost:40000'
	 * @param  soapAction SOAPAction string of the target,
	 *         e.g. 'urn:xdaq-application:lid=11'
	 *              'urn:xdaq-application:class=MyApp,instance=0' .
	 *
	 * @return DOM document, which is the node directly under <Body>,
	 *         including <Fault>.
	 */
	public Document send(String endpoint, String soapAction)
			throws XDAQMessageException,
			       XDAQTimeoutException
	{
		// Set SOAPAction in MIME header to specify a receiver
		message.getMimeHeaders().setHeader("SOAPAction", soapAction);

		// Send this message
		senderReply = null;
		
		long time1=0,time =0;

		try {
			if (getTimeout() == 0) {
				time1= System.currentTimeMillis();
				senderReply = call_with_retry(connection, message, endpoint);
			} else {  // with timeout
				if (primaryExecutor == null) {
					primaryExecutor = Executors.newFixedThreadPool(
							N_POOLED_THREADS);
					secondaryExecutor = Executors.newCachedThreadPool();
				}

				Sender sender = new Sender(endpoint);
				Watcher watcher = new Watcher(sender);

				Future<SOAPMessage> future = primaryExecutor.submit(watcher);
				try {
					time1= System.currentTimeMillis();
					senderReply = future.get();
				} catch (ExecutionException e) {
					if (e.getCause() instanceof TimeoutException) {
						time = System.currentTimeMillis() - time1;
						throw new XDAQTimeoutException(
								 "Elapsed time=" + time + " ms Timeout=" + timeout + " ms on : " + endpoint, e);
					} else {
						time = System.currentTimeMillis() - time1;
						throw new XDAQMessageException(
								"Sender task threw an exception: " + endpoint + " Elapsed time=" + time + " ms.",
								e);
					}
				} catch (InterruptedException e) {
					throw new XDAQMessageException(e);
				}
			}

		} catch (SOAPException e) {
			time = System.currentTimeMillis() - time1;
			throw new XDAQMessageException(
					"Failed to send a SOAP message to " + endpoint + ": Elapsed time=" + time + " ms.", e);
		}

		
		// Convert SOAP reply message to a DOM document
		//
		// The steps are;
		// 1. creating a DOM document from the SOAP reply
		// 2. find the body element node from the document
		// 3. replace the document element by the body element node
		//
		// Simpler steps of creating a new document and putting
		// the SOAP body element directory via 'importNode() + appendChild()'
		// didn't work.

		Document replyDOM;

		try {
			SOAPPart part = senderReply.getSOAPPart();
			replyDOM = (Document)getDOM(part);
		} catch (SOAPException e) {
			throw new XDAQMessageException(
					"The reply message from " + endpoint +
					" contains a bad SOAP structure.", e);
		} catch (TransformerException e) {
			throw new XDAQRuntimeException(e);
		}

		// returnNode: the DocumentElement returned.
		//    The root element is the body element of the SOAP reply;
		//    <....Response> in case of successful SOAP call, and
		//    <...:Fault> in case of error.
		//
		// The fist getElementsByTagNameNS() looks for <Body>.
		// The second just picks up any Elements.
		Element body = (Element)replyDOM.getElementsByTagNameNS(
				SOAPConstants.URI_NS_SOAP_ENVELOPE, "Body").item(0);
		Element returnNode = (Element)
				body.getElementsByTagNameNS("*", "*").item(0);

		// XDAQ applications are allowed to reply with an empty SOAP message.
		if (returnNode != null) {
			replyDOM.replaceChild(returnNode, replyDOM.getDocumentElement());
		}

		return replyDOM;
	}

	/**
	 * @return timeout value in milliseconds
	 */
	public int getTimeout()
	{
		return timeout;
	}

	/**
	 * Set timeout.
	 * If timeout is non-zero, all send() calls from different instances
	 *  are executed sequentially.
	 *
	 * @param  aTimeout timeout value set, in milliseconds
	 *
	 * @see    #send(String, String)
	 */
	public void setTimeout(int aTimeout)
	{
		timeout = aTimeout;
	}

	/**
	 * SOAP to DOM transformer.
	 */
	private Node getDOM(SOAPPart soapPart)
			throws TransformerException,
			       SOAPException
	{
		// *hack* This line is necessary due to a bug of Axis SOAP.
		//        Without this line, SOAPPart.getContent() fails when
		//        it contains <Fault>.
		//        Just using 'new DOMSource(SOAPPart)' in the latter transform
		//        also doesn't work.
		try {
			transformer.transform(new DOMSource(soapPart), new DOMResult());
		} catch (TransformerException ignored) {}

		// Create an instance of Result for a DOM tree
		DOMResult domResult = new DOMResult();

		// Transform !!
		transformer.transform(soapPart.getContent(), domResult);

		return domResult.getNode();
	}

	/**
	 * Expose the internal DOM for debug
	 */
	public Node getDOM()
			throws XDAQMessageException
	{
		Node node;

		try {
			node = getDOM(message.getSOAPPart());
		} catch (Exception e) {
			throw new XDAQMessageException(e);
		}

		return node;
	}

	/**
	 * replace the content with a DOM document
	 */
	public void setDOM(Node document)
			throws XDAQMessageException
	{
		DOMSource source = new DOMSource(document);

		try {
			message.getSOAPPart().setContent(source);
		} catch (SOAPException e) {
			throw new XDAQMessageException(e);
		}
	}

	/**
	 * Extract faultstring from the DOM document returned by send().
	 *
	 * @return If no fault: null<br/>
	 *         If Fault->faultstring exists:
	 *           the text content of the faultstring<br/>
	 */
	public String getFaultString()
	{
		if (senderReply == null) { return null; }

		SOAPBody body;
		try {
			body = senderReply.getSOAPPart().getEnvelope().getBody();
		} catch (SOAPException e) {
			throw new XDAQRuntimeException(
					"Can't get a Body element from the SOAP reply.");
		}

		if (!body.hasFault()) { return null; }

		String result = body.getFault().getFaultString();
		if (result == null) { result = ""; }
				// null is returned only when 'fault' doesn't exist.

		return result;
	}

	/**
	 * Shut down the thread pools used for timeout management
	 *
	 * If setTimeout() is ever used, class member thread pools are
	 * created internally. 
	 */
	public static synchronized void shutdownThreadPool()
			throws InterruptedException
	{
		if (secondaryExecutor != null) {
			secondaryExecutor.shutdown();
			secondaryExecutor.awaitTermination(10, SECONDS);
			secondaryExecutor = null;
		}

		if (primaryExecutor != null) {
			primaryExecutor.shutdown();
			primaryExecutor.awaitTermination(10, SECONDS);
			primaryExecutor = null;
		}
	}
}

// End of file
// vim: set ts=4 sw=4:
