package xdaq.xdesk.tools;


import  java.applet.*;

public class Command {
	
	String name_;
	Applet applet_;
	
	public Command(String name, Applet applet )
	{
		name_ = name;
		applet_ = applet;
	}
	
	
	public String getName()
	{
		return name_;
	}
	
	public Applet getApplet()
	{
		return applet_;
	}


}
