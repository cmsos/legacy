/*
 * zfrl.h 
 *
 * contains definitions for zfrl device driver
 *
 * $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/include/zfrl.h,v 1.5 2007/01/04 14:08:46 frans Exp $
 */


#undef PRINT_LEVEL
#define PRINT_LEVEL 0


#define PRINTF(x)	if (PRINT_LEVEL>=(x)) printk

// change of major number alos requires moification in ../drv/Makefile
#define ZFRL_MAJOR	125

#define ZFRL_PCI_VENDORID 0xECD6
#define ZFRL_PCI_DEVICEID 0xFF10
#define ZFRL_BRIDGE_PCI_VENDORID 0xECD6
#define ZFRL_BRIDGE_PCI_DEVICEID 0xFF01

//#define ZFRL_PCI_BOARDSIZE (64*1024)


/*
 * ioctl's
 */

struct  zfrl_board_info {
  //  int revision ;
  //  int pci_busnmb ;
  //  unsigned int dma_block_bus ;
  // addresses must be unsigned long 
  unsigned long iobase_main_pci ;
  unsigned long iobase_main_user ;
  //
  uint32_t pci_rsrc0_size_fpga_main ; 
  //
  int frl_geoslot ;
  uint32_t fpga_version_main ;
  uint32_t fpga_version_bridge ;
} ;

struct  zfrl_drv_info {
  uint32_t stamp1 ;
  uint32_t nmb_interrupts ;  
  uint32_t nmb_spurious ;
  uint32_t stamp2 ;
} ;

struct  zfrl_wait {
  uint32_t dummy ;
} ;


struct zfrl_load_fpga {
  uint32_t blockno ;    /* blockno  0: standard FRL  1: FRL+generator */
} ;


#define ZFRL_GET_BOARD_INFO  _IOR ('M',0,struct zfrl_board_info)
#define ZFRL_GET_DRV_INFO    _IOR ('M',1,struct zfrl_drv_info)
#define ZFRL_WAIT_INTR       _IOWR('M',2,struct zfrl_wait)
#define ZFRL_LOAD_FPGA       _IOWR('M',3,struct zfrl_load_fpga)



typedef struct zfrl_dev {
  int unit ;
  // 
  struct pci_dev *pcidev_frl_bridge_p ;  
  struct pci_dev *pcidev_frl_main_p ;  
  struct pci_dev *pcidev_nic_p ;  
  // note: addresses must be unsigned-long
  unsigned long iobase_main_pci ;
  unsigned long iobase_main_remap ;
  unsigned long iobase_bridge_pci ;
  //  unsigned int dma_block_size ;
  //  unsigned int dma_block ;
  //  unsigned int dma_block_bus ;
  //  unsigned int dma_block_phys ;
  //
  uint32_t pci_rsrc0_size_fpga_main ; 
  //  int irq ;
  //  int revision ;
  //  int busnmb ;

  //  unsigned char frldev_pb ;
  //  unsigned char frldev_dev_num ;
  //  unsigned int frldev_config_space[16] ;
  //  unsigned char frlbridge_pb ;
  //  unsigned char frlbridge_dev_num ;
  uint32_t frl_geoslot ;
  uint32_t fpga_version_main ;
  uint32_t fpga_version_bridge ;
} zfrl_dev_t ;


/* 
 * zfrl_xx() functions
 * they requires architecture dependant zfrl driver support
 * libzfrldev
 *   
 * _open() opens zfrl device to get user space mapping to FRL board
 *  and initialises internal pointers
 */
 

int zfrl_get_board_info (int unit, struct zfrl_board_info *) ; 
int zfrl_fpga (int unit, int blockno) ; 

/* eof */
