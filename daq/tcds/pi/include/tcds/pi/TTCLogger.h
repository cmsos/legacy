#ifndef _tcds_pi_TTCLogger_h_
#define _tcds_pi_TTCLogger_h_

#include <stdint.h>
#include <string>
#include <vector>

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace pi {

    /**
     * Core TTCSpy functionality: the part that is shared between the
     * stand-alone TTCSpy and the TTCSpy iside the PI.
     */
    class TTCLogger
    {

    public:

      enum LOGGING_BUFFER_MODE {LOGGING_BUFFER_MODE_SINGLE = 0, LOGGING_BUFFER_MODE_CIRCULAR = 1};
      enum LOGGING_LOGIC {LOGGING_LOGIC_OR = 0, LOGGING_LOGIC_AND = 1};
      enum LOGGING_MODE {LOGGING_MODE_ONLY = 0, LOGGING_MODE_EXCEPT = 1};

      TTCLogger(tcds::hwlayertca::TCADeviceBase& hw);
      ~TTCLogger();

      void startLogging() const;
      void stopLogging() const;
      void resetLogging() const;
      void restartLogging() const;
      bool isLoggingEnabled() const;
      bool isLogFull() const;
      void setBufferMode(LOGGING_BUFFER_MODE const val) const;
      void setLoggingLogic(LOGGING_LOGIC const val) const;
      void setLoggingMode(LOGGING_MODE const val) const;
      void setBC0Val(uint8_t const val) const;
      void setBC0BX(uint16_t const val);
      uint16_t bc0BX() const;
      void setTriggerMask(bool const brcBC0,
                          bool const brcEC0,
                          uint8_t const brcVal0,
                          uint8_t const brcVal1,
                          uint8_t const brcVal2,
                          uint8_t const brcVal3,
                          uint8_t const brcVal4,
                          uint8_t const brcVal5,
                          uint8_t const brcDDDD,
                          uint8_t const brcTT,
                          bool const brcDDDDAll,
                          bool const brcTTAll,
                          bool const brcAll,
                          bool const addAll,
                          bool const l1a,
                          bool const brcZeroData,
                          bool const adrZeroData,
                          bool const errCom) const;
      std::vector<uint32_t> readSpyLog() const;
      std::string readSpyLogFormatted() const;

    private:
      void zeroSpyMemory() const;
      uint32_t logSize() const;
      uint32_t getNumLogEntries() const;

      tcds::hwlayertca::TCADeviceBase& hw_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTCLogger_h_
