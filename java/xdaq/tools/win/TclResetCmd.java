package xdaq.tools.win;

import tcl.lang.*;
import xdaq.tools.win.*;
/**
 * This class implements the "Configure" command in XdaqPackage.
 */

class TclResetCmd implements Command {
    // This procedure is invoked to process the "sayhello" Tcl command.
    // It takes no arguments and returns "Hello World!" string as
    // its result.
	xdaqWin win;
	
	public TclResetCmd(xdaqWin win) {
		this.win = win;
	
	}
    public void cmdProc(Interp interp, TclObject argv[]) throws TclException 
	{
		if (argv.length != 3) {
	    	throw new TclException(interp,"Reset command error: wrong arguments use");
		}
		String mode = argv[1].toString();
		String option = argv[2].toString();
		
		if (  mode.equals("-host")) {
			//throw new TclException(interp,"Reset command error: not implemented yet");
			if ( option.equals("all") ) {
				win.getTree().selectHost ( "Any", "Any" );
			}
			else 
			{
				win.getTree().selectHost ( option, "Any" );
			
			}	
		} else {
			throw new TclException(interp,"Reset command error:  unknown mode/option");
		
		}
		
		//
		// execute the command here
		//
		win.executeCommandOnSelection("Reset", win.getPaths() );
				
		//interp.setResult("Configure Command executed");
    }
}
