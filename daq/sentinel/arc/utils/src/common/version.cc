// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "config/version.h"
#include "xcept/version.h"
#include "toolbox/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xoap/version.h"
#include "xgi/version.h"
#include "sentinel/arc/utils/version.h"

GETPACKAGEINFO (sentinelarcutils)

void sentinelarcutils::checkPackageDependencies () 
{
	CHECKDEPENDENCY (config);
	CHECKDEPENDENCY (xcept);
	CHECKDEPENDENCY (toolbox);
	CHECKDEPENDENCY(xdaq);
	CHECKDEPENDENCY (xdata);
	CHECKDEPENDENCY(xoap);
	CHECKDEPENDENCY (xgi);
}

std::set<std::string, std::less<std::string> > sentinelarcutils::getPackageDependencies ()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies, config);
	ADDDEPENDENCY(dependencies, xcept);
	ADDDEPENDENCY(dependencies, toolbox);
	ADDDEPENDENCY(dependencies, xdaq);
	ADDDEPENDENCY(dependencies, xdata);
	ADDDEPENDENCY(dependencies, xoap);
	ADDDEPENDENCY(dependencies, xgi);

	return dependencies;
}

