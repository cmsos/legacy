// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 	       *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			             *
 *************************************************************************/

#ifndef _pt_utcp_b2inheader_info_h_
#define _pt_utcp_b2inheader_info_h_

#include <cstddef>

namespace pt
{
	namespace utcp
	{
		//#define B2IN_SIGNATURE 0x6232696E
		//#define B2IN_SIGNATURE 0x70746370
		const uint32_t B2IN_SIGNATURE = 0x70746370;
		const uint8_t B2IN_VERSION = 0x20;

		typedef struct _B2IN_MESSAGE_FRAME
		{
				uint32_t signature;
				uint8_t version;
				uint8_t flags;
				uint16_t qos;
				uint32_t length;
				uint16_t sequence; // this was 32 bit, but we wanted a longer packet and total, so it was downgraded, but left in to keep alignment
				uint16_t identification;
				uint16_t packet;
				uint16_t total;
				uint32_t protocol;
		} B2IN_MESSAGE_FRAME, *PB2IN_MESSAGE_FRAME;

		class B2INHeaderInfo
		{
			public:

				static size_t getLength (char * bufferPtr)
				{
					PB2IN_MESSAGE_FRAME frame = (PB2IN_MESSAGE_FRAME) bufferPtr;

					return (frame->length);
					//return (frame->length << 2);

				}

				static size_t getHeaderSize ()
				{
					return sizeof(B2IN_MESSAGE_FRAME);
				}
		};
	}
}
#endif
