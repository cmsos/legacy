#!/usr/bin/env python

###############################################################################

import errno
import socket
import sys
from array import array
from xml.dom.minidom import parse

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def chunkify(input_list, chunk_size):
    res = [input_list[i:i + chunk_size] for i in range(0, len(input_list), chunk_size)]
    # End of chunkify().
    return res

###############################################################################

def invert_u32(value):
    return (value ^ 0xffffffff)

###############################################################################

def unmask_value(raw_value, mask):
    """Applies a mask to a raw value straight from an IPbus read."""

    # First step: apply the mask.
    tmp = raw_value & mask
    # Second step: shift the result down by the number of
    # zero-bits at the low end of the mask.
    num_bits_to_shift = 0
    mask_tmp = mask
    while (mask_tmp & 0x1) == 0:
        mask_tmp >>= 1
        num_bits_to_shift += 1
    unmasked_value = tmp >> num_bits_to_shift

    # End of unmask_value().
    return unmasked_value

###############################################################################

def mask_value(masked_value, mask):
    """Applies a mask to an unmasked value. The result can be used in an IPbus write."""

    # Step 0: mask out anything outside the 32-bits used in IPbus.
    tmp = masked_value & 0xffffffff
    # First step: shift the clean value up by the number of zero-bits
    # at the low end of the mask.
    num_bits_to_shift = 0
    mask_tmp = mask
    while (mask_tmp & 0x1) == 0:
        mask_tmp >>= 1
        num_bits_to_shift += 1
    tmp = tmp << num_bits_to_shift
    # Second step: apply the mask.
    raw_value = tmp & mask
    # Check.
    if (raw_value & ~mask) != 0:
        msg = "Data 0x{0:x} exceeds mask 0x{1:x}."
        msg = msg.format(masked_value, mask)
        raise RuntimeError(msg)

    # End of mask_value().
    return raw_value

###############################################################################

class AddressTable(object):

    def __init__(self, address_table_file_name):
        # NOTE: This is the name of the top-level address table
        # file. From there on different address table files can be
        # included as sub-modules.
        self.address_table_file_name = address_table_file_name
        try:
            dom = parse(self.address_table_file_name)
        except Exception, err:
            msg = "Could not read address table from file '{0:s}': {1:s}."
            msg = msg.format(self.address_table_file_name, str(err))
            raise RuntimeError(msg)
        # sep_line = "-" * 50
        # print sep_line
        # print "DOM before processing includes"
        # print sep_line
        # print dom.toxml()
        # print sep_line
        # dom = self.process_includes(dom)
        # print sep_line
        # print "DOM after processing includes"
        # print sep_line
        # print dom.toxml()
        # print sep_line
        self.registers = {}
        self.process([dom])
        # Fix up the naming of the registers.
        tmp = self.registers
        self.registers = {}
        for (reg_name, item) in tmp.iteritems():
            index = reg_name.find(".")
            if index > -1:
                new_name = reg_name[index + 1:]
                self.registers[new_name] = item
        # End of __init__().

    # def process_includes(self, node):
    #     if (node.nodeType == node.ELEMENT_NODE) and \
    #        node.hasAttribute("module"):
    #         module_name = node.attributes["module"].value
    #         magic_str = "file://"
    #         if not module_name.startswith(magic_str):
    #             msg = "Expected module specifier '{0:s}' should start with '{1:s}'."
    #             msg = msg.format(module_name, magic_str)
    #             raise RuntimeError(msg)
    #         file_name = module_name[len(magic_str):]
    #         try:
    #             dom = parse(file_name)
    #         except Exception, err:
    #             msg = "Could not read address table module from file '{0:s}': {1:s}."
    #             msg = msg.format(file_name, str(err))
    #             raise RuntimeError(msg)
    #         node.appendChild(dom.childNodes[0])
    #     else:
    #         for sub_node in node.childNodes:
    #             self.process_includes(sub_node)
    #     # End of process_includes().
    #     return node

    def process(self, nodes, base_name=None, base_address=0x0):
        if base_name is None:
            base_name = ""
        for node in nodes:
            if node.nodeType == node.DOCUMENT_NODE:
                self.process(node.childNodes, None, 0x0)
            elif node.nodeType == node.ELEMENT_NODE:
                if node.hasAttribute("id"):
                    node_id = node.attributes["id"].value
                    if base_name:
                        tmp_name = "{0:s}.{1:s}".format(base_name, node_id)
                    else:
                        tmp_name = node_id
                    reg_name = tmp_name
                    node_address = 0x0
                    if node.hasAttribute("address"):
                        node_address = int(node.attributes["address"].value, 16)
                    address = base_address + node_address
                    mask = 0xffffffff
                    if node.hasAttribute("mask"):
                        mask = int(node.attributes["mask"].value, 16)
                    permission = ""
                    if node.hasAttribute("permission"):
                        permission = node.attributes["permission"].value.lower()
                    is_readable = (permission in AddressTableItem.PERMISSIONS_READ)
                    is_writable = (permission in AddressTableItem.PERMISSIONS_WRITE)
                    size = 1
                    if node.hasAttribute("size"):
                        size = int(node.attributes["size"].value)
                    item = AddressTableItem(reg_name, address, mask, is_readable, is_writable, size)
                    self.registers[reg_name] = item
                children = node.childNodes
                if len(children):
                    self.process(children, tmp_name, address)
        # End of process().

    def __getitem__(self, item_name):
        return self.registers[item_name]

    # End of class AddressTable.

###############################################################################

class AddressTableItem(object):

    PERMISSIONS_READ = ["read", "readwrite", "r", "rw"]
    PERMISSIONS_WRITE = ["write", "readwrite", "w", "rw"]

    def __init__(self, name, address, mask, is_readable, is_writable, size):
        self.name = name
        self.address = address
        self.mask = mask
        self.is_readable = is_readable
        self.is_writable = is_writable
        self.size = size
        # End of __init__().

    # End of class AddressTableItem.

###############################################################################

class IPBusPacketHeader(object):
    """This is the overall IPbus header with the byte-order and the packet ID."""

    PACKET_TYPE_CONTROL = 0x0
    PACKET_TYPE_STATUS = 0x1
    PACKET_TYPE_RESEND = 0x2

    BYTE_ORDER_QUALIFIER = 0xf

    @classmethod
    def from_raw(cls, raw_value):
        version = (raw_value >> 28) & 0xf
        packet_id = (raw_value >> 8) & 0xffff
        byte_order_qualifier = (raw_value >> 4) & 0xf
        packet_type = raw_value & 0xf
        res = cls(version, packet_id, data, packet_type, byte_order_qualifier)
        # ASSERT ASSERT ASSERT
        assert res.raw_header == raw_value
        # ASSERT ASSERT ASSERT end
        # End of from_raw().
        return res

    def __init__(self, version, packet_id, packet_type, byte_order_qualifier=None):
        self.version = version
        self.packet_id = packet_id
        if byte_order_qualifier is None:
            byte_order_qualifier = IPBusPacketHeader.BYTE_ORDER_QUALIFIER
        self.byte_order_qualifier = byte_order_qualifier
        self.packet_type = packet_type
        self.raw_header = ((self.version & 0xf) << 28) | \
                          ((self.packet_id & 0xffff) << 8) | \
                          ((self.byte_order_qualifier & 0xf) << 4) | \
                          (self.packet_type & 0xf)
        # End of __init__().

    # End of class IPBusPacketHeader.

###############################################################################

class IPBusTransactionHeader(object):

    # These are the different IPbus transaction identifiers.
    # - Read.
    TYPE_ID_READ = 0x0
    # - Write.
    TYPE_ID_WRITE = 0x1
    # - Non-incrementing read.
    TYPE_ID_NON_INCR_READ = 0x2
    # - Non-incrementing write.
    TYPE_ID_NON_INCR_WRITE = 0x3
    # - Read-modify-write bits.
    TYPE_ID_RMW_BITS = 0x4
    # - Read-modify-write sum.
    TYPE_ID_RMW_SUM = 0x5
    # - Reserved address info request.
    TYPE_ID_RSVD_ADDR_INFO = 0xE

    # This is a list of the all IPbus "info codes" that define packet
    # direction and error status.
    # - Host reports that the request was handled successfully.
    INFO_CODE_RESPONSE = 0x0
    # - Host reports that the client request had a bad header.
    INFO_CODE_BAD_HEADER = 0x1
    # - Host reports that there was a bus error on read.
    INFO_CODE_BUS_ERR_ON_RD = 0x2
    # - Host reports that there was a bus error on write.
    INFO_CODE_BUS_ERR_ON_WR = 0x3
    # - Host reports that there was a bus timeout on read.
    INFO_CODE_BUS_TIMEOUT_ON_RD = 0x4
    # - Host reports that there was a bus timeout on write.
    INFO_CODE_BUS_TIMEOUT_ON_WR = 0x5
    # - A plain client transaction request.
    INFO_CODE_REQUEST = 0xf

    # The expected (request, response) minimum data sizes based on the
    # protocol definition. For read operations one of course has to
    # add the read results as well.
    MIN_DATA_SIZES = {
        TYPE_ID_READ           : (1, 0),
        TYPE_ID_WRITE          : (1, 0),
        TYPE_ID_RMW_BITS       : (3, 1),
        TYPE_ID_RMW_SUM        : (2, 1),
        TYPE_ID_NON_INCR_READ  : (1, 0),
        TYPE_ID_NON_INCR_WRITE : (1, 0),
        TYPE_ID_RSVD_ADDR_INFO : (0, 2)
        }

    @classmethod
    def from_raw(cls, raw_value):
        version = (raw_value >> 28) & 0xf
        transaction_id = (raw_value >> 16) & 0xfff
        data = (raw_value >> 8) & 0xff
        type_id = (raw_value >> 4) & 0xf
        info_code = raw_value & 0xf
        res = cls(version, transaction_id, data, type_id, info_code)
        # ASSERT ASSERT ASSERT
        assert res.raw_header == raw_value
        # ASSERT ASSERT ASSERT end
        # End of from_raw().
        return res

    def __init__(self, version, transaction_id, data, type_id, info_code):
        self.version = version
        self.transaction_id= transaction_id
        self.data = data
        self.type_id = type_id
        self.info_code = info_code
        self.raw_header = ((self.version & 0xf) << 28) | \
                          ((self.transaction_id & 0xfff) << 16) | \
                          ((self.data & 0xff) << 8) | \
                          ((self.type_id & 0xf) << 4) | \
                          (self.info_code & 0xf)
        # End of __init__().

    def type_id(self):
        return self.type_id

    def is_request(self):
        return (self.info_code == IPBusTransactionHeader.INFO_CODE_REQUEST)

    def is_response(self):
        return (self.info_code == IPBusTransactionHeader.INFO_CODE_RESPONSE)

    def is_response_success(self):
        return (self.info_code == IPBusTransactionHeader.INFO_CODE_RESPONSE)

    def is_response_error(self):
        return (self.is_response() and (not self.is_response_success()))

    def get_number_of_words(self):
        """Returns the value stored in the 'words' field of a raw IPbus header."""
        # End of get_number_of_words().
        return self.data

    def expected_data_size(self):
        """Returns the expected data size based on the type id."""
        expected_size = None
        if self.is_request():
            expected_size = IPBusTransactionHeader.MIN_DATA_SIZES[self.type_id][0]
        else:
            expected_size = IPBusTransactionHeader.MIN_DATA_SIZES[self.type_id][1]
        if self.type_id in [IPBusTransactionHeader.TYPE_ID_READ,
                            IPBusTransactionHeader.TYPE_ID_NON_INCR_READ]:
            expected_size += self.get_number_of_words()
        # End of expected_data_size().
        return expected_size

    # End of class IPBusTransactionHeader.

###############################################################################

class IPBusTransactionPacket(object):
    """A single IPbus request/response packet."""

    def __init__(self, header, data=None):
        if data is None:
            data = []
        self.header = header
        self.data = data
        self.raw_data = array("I", [header.raw_header])
        self.raw_data.extend(array("I", data))
        # End of __init__().

    def is_data_size_correct(self):
        """Checks the data size against the specified data size in the header."""

        data_size_from_data = len(self.data)
        data_size_from_header = self.header.expected_data_size()
        res = (data_size_from_data == data_size_from_header)

        # End of is_data_size_correct().
        return res

    # End of class IPBusTransactionPacket.

###############################################################################

class IPBusTransaction(object):
    """A single IPbus client(!) transaction.

    NOTE: Only IPbus client requests (i.e., from the software client
    to the hardware host) are implemented.

    """

    def __init__(self, ipbus_device, ipbus_packet):
        self.device = ipbus_device
        self.packet = ipbus_packet
        # End of __init__().

    def transact(self):
        # Add the overall IPbus packet header.
        # NOTE: Hard-coded packet id of zero. Circumvents all safety
        # systems in the IPbus protocol.
        packet_id = 0
        packet_header = IPBusPacketHeader(IPBusDeviceBase.IPBUS_PROTOCOL_VERSION,
                                          packet_id,
                                          IPBusPacketHeader.PACKET_TYPE_CONTROL)
        request = array("I", [packet_header.raw_header])
        request.extend(self.packet.raw_data)

        # Send the IPbus request.
        try:
            self.device._socket_send(request)
        except socket.error, err:
            msg = "A socket error occurred while sending the IPbus request: {0:s}."
            msg = msg.format(str(err))
            raise RuntimeError(msg)

        # Read the IPbus response.
        try:
            response = self.device._socket_receive()
        except socket.error, err:
            msg = "A socket error occurred while receiving the IPbus response: {0:s}."
            msg = msg.format(str(err))
            raise RuntimeError(msg)

        # Unpack the response into an array of unsigned 32-bit integers.
        tmp = array("I", response)
        # Turn this array into an IPbus response packet.
        response_header = IPBusTransactionHeader.from_raw(tmp[1])
        response_data = tmp[2:]
        response = IPBusTransactionPacket(response_header, response_data)
        # Check the response against the request.
        self.validate_response(response)

        # ASSERT ASSERT ASSERT
        # If this fails the IPbus target inverted the endianness.
        assert tmp[0] == request[0]
        # ASSERT ASSERT ASSERT end

        # End of transact().
        return response

    def validate_response(self, response):
        request = self.packet

        request_header = request.header
        response_header = response.header

        # Make sure that the request is a request and the response is
        # a response.
        if not request_header.is_request():
            msg = "Request packet is not a request. It has info code 0x{0:x} instead."
            msg = msg.format(request_header.info_code)
            raise RuntimeError(msg)
        if not response_header.is_response():
            msg = "Response packet is not a response. It has info code 0x{0:x} instead."
            msg = msg.format(response_header.info_code)
            raise RuntimeError(msg)

        # Check for matching type ids.
        if request_header.type_id != response_header.type_id:
            msg = "Response type id (0x{0:x}) does not match request type id (0x{1:x})."
            msg = msg.format(request_header.type_id, response_header.type_id)
            raise RuntimeError(msg)

        # Check for matching transaction ids.
        if request_header.transaction_id != response_header.transaction_id:
            msg = "Response transaction id (0x{0:x}) does not match request transaction id (0x{1:x})."
            msg = msg.format(request_header.transaction_id, response_header.transaction_id)
            raise RuntimeError(msg)

        # Check if the response indicates success or failure.
        if response_header.is_response_error():
            msg = "Response indicates an error: info code 0x{0:x}."
            msg = msg.format(response_header.info_code)
            raise RuntimeError(msg)

        # Check the size of the data of the message received against
        # the one specified in the header.
        if not response.is_data_size_correct():
            msg = "Response data size ({0:d}) does not match expected data size (from header: {1:d})."
            msg = msg.format(len(response.data), response_header.expected_data_size())
            raise RuntimeError(msg)

    # End of validate_response().

# End of class IPBusTransaction.

###############################################################################

class IPBusDeviceBase(object):
    """Base class for uTCA devices that communicate using IPbus.

    Don't use this class directly. Use IPBusDeviceUDP or
    IPBusDeviceTCP instead.

    NOTE: All of this is basically a simplified extract of what used
    to be the PyChips package.

    """

    # The default socket time-out is 1 second.
    DEFAULT_SOCKET_TIMEOUT = 1

    # The size of the buffer associated with the UDP/TCP socket (in
    # bytes).
    SOCKET_BUFFER_SIZE = 32768

    # The only protocol supported is IPbus version 2.
    IPBUS_PROTOCOL_VERSION = 2

    # The minimum and maximum value of the transaction id
    # counter. After reaching the maximum value the next transaction
    # makes this counter roll over to MIN_TRANSACTION_ID.
    MIN_TRANSACTION_ID = 1
    MAX_TRANSACTION_ID = 2047

    # The maximum number of words in a single block read/write
    # transaction cannot exceed this value. Larger block transactions
    # have to be split into multiple IPbus transactions.
    MAX_BLOCK_SIZE = 255

    def __init__(self, address_table, ip_address, port_number):
        self.address_table = address_table
        self.ip_address = ip_address
        self.port_number = port_number
        self._socket = None
        self._transaction_id = 1
        # End of __init__().

    def _init_socket(self):
        self._socket.settimeout(IPBusDeviceBase.DEFAULT_SOCKET_TIMEOUT)
        # End of _init_socket().

    def _socket_send(self, transaction):
        """Place-holder for the underlying socket transmission implementation."""
        raise NotImplementedError("IPBusDeviceBase is an abstract base class.\n" \
                                  "Please use IPBusDeviceUDP or IPBusDeviceTCP instead.")

    def _socket_receive(self):
        # NOTE: When the socket transaction gets interrupted by a
        # system call (e.g., because someone is resizing the terminal
        # in which we are running), we retry a bunch of times, and
        # otherwise forget about it.
        response = None
        num_tries = 100
        num = 0
        while response is None:
            try:
                response = self._socket.recv(IPBusDeviceBase.SOCKET_BUFFER_SIZE)
            except socket.error, err:
                if err.errno != errno.EINTR:
                    raise
                if num >= num_tries:
                    raise
            num += 1
        # End of _socket_receive().
        return response

    def _get_transaction_id(self):
        """Returns the current value of the transaction ID counter and increments.

        Note: Transaction id 0 is reserved for byte-order
        transactions. For any other kind of transaction this can be
        used to get access to an incrementing counter that will cycle
        from MIN_TRANSACTION_ID to MAX_TRANSACTION_ID before rolling
        over to MIN_TRANSACTION_ID.
        """
        res = self._transaction_id
        if self._transaction_id < IPBusDeviceBase.MAX_TRANSACTION_ID:
            self._transaction_id += 1
        else:
            self._transaction_id = IPBusDeviceBase.MIN_TRANSACTION_ID
        # End of _get_transaction_id().
        return res

    def read(self, reg_name):
        address_table_item = self.address_table[reg_name]
        if not address_table_item.is_readable:
            msg = "Register '{0:s}' is not readable.".format(reg_name)
            raise ValueError(msg)

        type_id = IPBusTransactionHeader.TYPE_ID_READ
        read_depth = 1
        read_header = IPBusTransactionHeader(IPBusDeviceBase.IPBUS_PROTOCOL_VERSION,
                                             self._get_transaction_id(),
                                             read_depth,
                                             type_id,
                                             IPBusTransactionHeader.INFO_CODE_REQUEST)
        read_body = [address_table_item.address]
        read_packet = IPBusTransactionPacket(read_header, read_body)
        read_transaction = IPBusTransaction(self, read_packet)
        tmp = read_transaction.transact()
        tmp = tmp.data[0]

        # Mask as 32-bit IPbus word. Just in case.
        tmp = tmp & 0xffffffff
        # Now mask with the correct register mask.
        mask = address_table_item.mask
        res = unmask_value(tmp, mask)

        # End of read().
        return res

    def write(self, reg_name, value):
        address_table_item = self.address_table[reg_name]
        if not address_table_item.is_writable:
            msg = "Register '{0:s}' is not writeable.".format(reg_name)
            raise ValueError(msg)

        type_id = IPBusTransactionHeader.TYPE_ID_RMW_BITS
        write_depth = 1
        write_header = IPBusTransactionHeader(IPBusDeviceBase.IPBUS_PROTOCOL_VERSION,
                                              self._get_transaction_id(),
                                              write_depth,
                                              type_id,
                                              IPBusTransactionHeader.INFO_CODE_REQUEST)
        mask = address_table_item.mask
        write_body = [address_table_item.address,
                      invert_u32(mask),
                      mask_value(value, mask)]
        write_packet = IPBusTransactionPacket(write_header, write_body)
        write_transaction = IPBusTransaction(self, write_packet)
        tmp = write_transaction.transact()
        # End of write().

    def read_block(self, reg_name, num_words, is_fifo=False):
        address_table_item = self.address_table[reg_name]
        if not address_table_item.is_readable:
            msg = "Register '{0:s}' is not readable.".format(reg_name)
            raise ValueError(msg)
        block_size = address_table_item.size
        num_words = min(block_size, num_words)
        base_address = address_table_item.address
        res = []
        block_sizes = [num_words]
        if num_words > IPBusDeviceBase.MAX_BLOCK_SIZE:
            block_sizes = [IPBusDeviceBase.MAX_BLOCK_SIZE] * (num_words / IPBusDeviceBase.MAX_BLOCK_SIZE) + \
                          [num_words % IPBusDeviceBase.MAX_BLOCK_SIZE]
        for (i, block_size) in enumerate(block_sizes):
            address = base_address
            if not is_fifo:
                address += (i * IPBusDeviceBase.MAX_BLOCK_SIZE)
            tmp = self._read_block(address, block_size, is_fifo)
            res.extend(tmp)
        # End of read_block().
        return res

    def _read_block(self, address, num_words, is_fifo):
        type_id = IPBusTransactionHeader.TYPE_ID_READ
        read_depth = num_words
        if is_fifo:
            type_id = IPBusTransactionHeader.TYPE_ID_NON_INCR_READ
        read_header = IPBusTransactionHeader(IPBusDeviceBase.IPBUS_PROTOCOL_VERSION,
                                             self._get_transaction_id(),
                                             read_depth,
                                             type_id,
                                             IPBusTransactionHeader.INFO_CODE_REQUEST)
        read_body = [address]
        read_packet = IPBusTransactionPacket(read_header, read_body)
        read_transaction = IPBusTransaction(self, read_packet)
        tmp = read_transaction.transact()
        res = tmp.data.tolist()
        # End of _read_block().
        return res

    def write_block(self, reg_name, values, is_fifo=False):
        address_table_item = self.address_table[reg_name]
        if not address_table_item.is_writable:
            msg = "Register '{0:s}' is not writeable.".format(reg_name)
            raise ValueError(msg)
        block_size = address_table_item.size
        num_words = min(block_size, len(values))
        values = values[:num_words]
        base_address = address_table_item.address
        if num_words > IPBusDeviceBase.MAX_BLOCK_SIZE:
            data = chunkify(values, IPBusDeviceBase.MAX_BLOCK_SIZE)
        else:
            data = [values]
        for (i, block) in enumerate(data):
            address = base_address
            if not is_fifo:
                address += (i * IPBusDeviceBase.MAX_BLOCK_SIZE)
            self._write_block(address, block, is_fifo)
        # End of write_block().

    def _write_block(self, address, block, is_fifo):
        # NOTE: This method receives an address instead of a register
        # name.
        type_id = IPBusTransactionHeader.TYPE_ID_WRITE
        if is_fifo:
            type_id = IPBusTransactionHeader.TYPE_ID_NON_INCR_WRITE
        write_depth = len(block)
        write_header = IPBusTransactionHeader(IPBusDeviceBase.IPBUS_PROTOCOL_VERSION,
                                              self._get_transaction_id(),
                                              write_depth,
                                              type_id,
                                              IPBusTransactionHeader.INFO_CODE_REQUEST)
        write_body = [address] + block
        write_packet = IPBusTransactionPacket(write_header, write_body)
        write_transaction = IPBusTransaction(self, write_packet)
        tmp = write_transaction.transact()
        # End of _write_block().

    def read_as_string(self, reg_name):
        tmp = self.read(reg_name)
        # Treat the 32-bit word as four independent chars.
        char0 = chr((tmp & 0xff000000) >> 24)
        char1 = chr((tmp & 0x00ff0000) >> 16)
        char2 = chr((tmp & 0x0000ff00) >> 8)
        char3 = chr(tmp & 0x000000ff)
        res = "".join([char0, char1, char2, char3])
        # End of read_as_string().
        return res

    # End of class IPBusDeviceBase.

class IPBusDeviceUDP(IPBusDeviceBase):

    def __init__(self, address_table, ip_address, port_number):
        IPBusDeviceBase.__init__(self, address_table, ip_address, port_number)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._init_socket()
        # End of __init__().

    def _socket_send(self, data):
        self._socket.sendto(data.tostring(), (self.ip_address, self.port_number))
        # End of _socket_send().

    # End of class IPBusDeviceUDP.

class IPBusDeviceTCP(IPBusDeviceBase):

    def __init__(self, address_table, ip_address, port_number):
        IPBusDeviceBase.__init__(self, address_table, ip_address, port_number)
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._init_socket()
        self._socket.connect((ip_address, port_number))
        # End of __init__().

    def _socket_send(self, data):
        """Underlying TCP socket transmission implementation."""
        self._socket.sendto(data.tostring())
        # End of _socket_send().

    # End of class IPBusDeviceTCP.

###############################################################################

# OBSOLETE OBSOLETE OBSOLETE
if __name__ == "__main__":

    ip_address = "192.168.0.163"
    port_number = 50001
    address_table_file_name = "pyspy_address_table.xml"
    address_table = AddressTable(address_table_file_name)
    dev = IPBusDeviceUDP(address_table, ip_address, port_number)

    brd_char = ["?"] * 4
    for i in xrange(4):
        brd_char[i] = chr(dev.read("glib.board_id.board_id_char{0:d}".format(i + 1)))
    board_id = "".join(brd_char)
    print "-> board type  : '{0:s}'".format(board_id)

    sys_char = ["?"] * 4
    for i in xrange(4):
        sys_char[i] = chr(dev.read("glib.system_id.system_id_char{0:d}".format(i + 1)))
    sys_id = "".join(sys_char)
    print "-> system type : '{0:s}'".format(sys_id)

    version_major = dev.read("glib.firmware_id.ver_major")
    version_minor = dev.read("glib.firmware_id.ver_minor")
    version_build = dev.read("glib.firmware_id.ver_build")
    version_str = ".".join([str(version_major), str(version_minor), str(version_build)])
    print "-> version num : {0:s}".format(version_str)

    yyyy = 2000 + dev.read("glib.firmware_id.date_yy")
    mm = dev.read("glib.firmware_id.date_mm")
    dd = dev.read("glib.firmware_id.date_dd")
    date_str = "/".join([str(dd), str(mm), str(yyyy)])
    print "-> sys fw date : {0:s}".format(date_str)

    import random
    print "Testing single-register write"
    reg_name = "glib.test"
    number = random.randint(0, 255)
    dev.write(reg_name, number)
    res = dev.read(reg_name)
    if res == number:
        print "  Success"
    else:
        print "  Failure"

    print "Testing block write"
    reg_name = "ttcspy.spy_memory"
    num_words = 10
    numbers = []
    for i in xrange(num_words):
        numbers.append(random.randint(0, 255))
    dev.write_block(reg_name, numbers)
    res = dev.read_block(reg_name, num_words)
    if res == numbers:
        print "  Success"
    else:
        print "  Failure"

    print "Done"
# OBSOLETE OBSOLETE OBSOLETE end

###############################################################################
