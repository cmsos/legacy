package net.hep.cms.xdaqctl.xdata;

public class UnsignedInteger extends UnsignedInteger32 {
	@Override
	int getEXDRCode() {
		return XDataType.UNSIGNED_INTEGER.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:

