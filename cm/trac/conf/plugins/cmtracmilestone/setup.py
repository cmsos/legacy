from setuptools import setup

PACKAGE     = 'CmTracMilestone'
VERSION     = '0.0.6'
AUTHOR      = 'Roland Moser, Michael Thonke, Dainius Simelevicius'
AUTHOREMAIL = 'roland.moser@cern.ch, dainius.simelevicius@cern.ch'
LICENSE     = 'TBD'
URL         = 'https://svnweb.cern.ch/trac/macs'
SUMMARY     = 'Milestone Management Plugin'
DESCRIPTION = 'Trac Plugin for Managing Milestones'

setup(name=PACKAGE,
	version=VERSION,
	packages=['cmtracmilestone'],
	description=SUMMARY,
	long_description=DESCRIPTION,
	author=AUTHOR,
	author_email=AUTHOREMAIL,
	license=LICENSE,
	url=URL,
	entry_points={'trac.plugins': '%s = cmtracmilestone' % PACKAGE},
	package_data={'cmtracmilestone': ['templates/*.html']},
	zip_safe=True
)

