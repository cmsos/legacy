#include "ttc/monitoring/PeriodicListener.h"

#include "xdaq/Application.h"
#include "xdata/exception/Exception.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include "xdata/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include <exception>


using namespace std;


ttc::PeriodicListener::PeriodicListener(
    xdaq::Application* app,
    const string& infoSpaceBaseName,
    bool autoInit,
    const double& refreshPeriodSecs,
    const double& refreshOffsetSecs,
    const uint16_t& forceUpdateEvery)
:
    DataListener(app, infoSpaceBaseName, autoInit),
    refreshPeriodSecs_(refreshPeriodSecs),
    refreshOffsetSecs_(refreshOffsetSecs),
    forceUpdateEvery_(forceUpdateEvery),
    xmasAlmostReadyTimeSecs_(0.0),
    xmasReadyTimeSecs_(0.0),
    timerName_(),
    timer_(0)
{
  // create/get named timer
  string appUUID= getApp().getApplicationDescriptor()->getUUID().toString();
  timerName_= "timer_" + getInfoSpaceBaseName() + appUUID + toolbox::toString(".%d", rand());
  timer_ = toolbox::task::getTimerFactory()->createTimer(timerName_);

  // normalize time grid offset:
  if (refreshPeriodSecs_ > 0)
  {
    int64_t n = int64_t(refreshOffsetSecs_ / refreshPeriodSecs_);
    refreshOffsetSecs_ -= n *  refreshPeriodSecs_;
    if (refreshOffsetSecs_ < 0)
    {
      refreshOffsetSecs_ += refreshPeriodSecs_;
    }
  }
}


ttc::PeriodicListener::~PeriodicListener()
{
  // remove timer
  toolbox::task::getTimerFactory()->removeTimer(timerName_);
}


double ttc::PeriodicListener::getRefreshPeriodSecs()
{
  return refreshPeriodSecs_;
}


double ttc::PeriodicListener::getRefreshOffsetSecs()
{
  return refreshOffsetSecs_;
}


uint16_t ttc::PeriodicListener::getForceUpdateEvery()
{
  return forceUpdateEvery_;
}


void ttc::PeriodicListener::run()
{
  // store init time (used for initial forced updates due to XMAS monitoring delay)
  double now = toolbox::TimeVal::gettimeofday();
  xmasAlmostReadyTimeSecs_ = now + 45. - refreshPeriodSecs_;
  xmasReadyTimeSecs_       = now + 45. + refreshPeriodSecs_;

  // schedule first regular refresh
  scheduleNextRefresh();
}


void ttc::PeriodicListener::timeExpired(toolbox::task::TimerEvent& e)
{
  // launch a forced refresh iff the last task was scheduled with name=="force"
  bool force = e.getTimerTask()->name == "force";

  TRY_CATCH_ALL( refreshComponents(force) );

  // schedule the next refresh
  TRY_CATCH_ALL( scheduleNextRefresh() );
}


void ttc::PeriodicListener::scheduleNextRefresh()
{
  // current system time
  double now = toolbox::TimeVal::gettimeofday();

  // if refreshPeriodSecs_ > 0, start timer and schedule next event
  // as per given interval and aligned to global start time
  if (refreshPeriodSecs_ > 0.0)
  {
    // next refresh:
    // the nearest integer multiple of the refresh period away from global start time:
    int64_t n = 1 + int64_t((now-refreshOffsetSecs_)/refreshPeriodSecs_);
    double next = refreshOffsetSecs_ + n*refreshPeriodSecs_;

    // force monitoring updates either if we are waiting for XMAS to get ready (initially after startup),
    // or if a regular forced update is due:
    bool force =
        (next >= xmasAlmostReadyTimeSecs_ && next <= xmasReadyTimeSecs_) ||
        (n % forceUpdateEvery_ == 0);

    toolbox::TimeVal nextTime(next);
    timer_->schedule(this, nextTime, this, force ? "force" : "");
  }
}
