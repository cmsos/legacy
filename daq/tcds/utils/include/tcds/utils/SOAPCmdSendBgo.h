#ifndef _tcds_utils_SOAPCmdSendBgo_h_
#define _tcds_utils_SOAPCmdSendBgo_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdSendBgo : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdSendBgo(T& controller);

      /**
       * Sends a single B-go. Bound to the SOAP command SendBgo.
       * - SOAP command parameters:
       *   either one of the following two
       *   -  xsd:string `bgoName': the B-go name (e.g., 'Bgo12', 'Start').
       *   -  xsd:unsignedInt `bgoNumber': the B-go number (e.g., '1').
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdSendBgo.hxx"

#endif // _tcds_utils_SOAPCmdSendBgo_h_
