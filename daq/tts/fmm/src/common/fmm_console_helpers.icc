/**-*-C++-*-
*      @file fmm_console_helpers.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.11 $
*     $Date: 2006/09/28 16:36:53 $
*
*
**/
/**c++-mode**/


void fmm_multi_test(std::vector<tts::FMMCard*> &fmms, uint32_t active_fmm, tts::FMMTDCard* td);


void display_fmm_menu( std::vector<tts::FMMCard*> const &fmms, uint32_t active_fmm, tts::FMMTDCard* td ) {
  std::cout << std::endl;
  std::cout << "--->>> FMM " << std::dec << active_fmm << "/" << fmms.size() 
	    << " <<<---SN=" << fmms[active_fmm]->getSerialNumber() 
	    << "---Geoslot=" << std::dec << fmms[active_fmm]->getGeoSlot() 
	    << "---FW_Rev=" << fmms[active_fmm]->readFirmwareRev() 
	    << "-----------------------------" << std::endl;	
  display_fmm_status(fmms[active_fmm]);
  std::cout << "--------------------------------------------------------------------------------" << std::endl;
  std::cout << "BASIC REGISTER ACCESS (using the HAL PCIDevice)" << std::endl;
  if (fmms.size()> 1)
    std::cout << "0 ... Select another FMM" << std::endl;
  std::cout << "1 ... write item                                 3 ... print address table" << std::endl;
  std::cout << "2 ... read item                                  7 ... dump all registers" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "LOCKING      (using the FMMCard class)" << std::endl;
  std::cout << "fal ... Get FMM write lock                       far ... release FMM write lock" << std::endl;
  std::cout << "fan ... disbale locking (still have to use fal)" << std::endl;
  if (td)
    std::cout << "dal ... Get TD write lock                        dar ... release TD write lock" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "ACCESS FUNCTIONS      (using the FMMCard class)" << std::endl;
  std::cout << "fsm ... Set mask                                 fsl ... Toggle LEDs " << std::endl;
  std::cout << "fst20   Set threshold 20->1 out-of-sync merge    fdh ... display history memory entries " << std::endl;
  std::cout << "fst10A  Set thresholdA 10->1 out-of-sync merge   fch ... clear histor memory" << std::endl;
  std::cout << "fst10B  Set thresholdB 10->1 out-of-sync merge   fdd ... display dead-time monitors" << std::endl;
  std::cout << "fss ... Toggle input simulation mode             frd ... reset dead time monitors" << std::endl;		    
  std::cout << "fsi ... Change simulated input states            frh ... reset history write address" << std::endl;	    
  std::cout << "fst ... Toggle output test mode                  frm ... reset transition miss counter" << std::endl;	    
  std::cout << "fso ... Set output test values                   frt ... reset time tag counter" << std::endl;		    
  std::cout << "fsd ... Toggle dual-FMM mode                     fra ... reset All FPGA (does not change regs)" << std::endl; 
  std::cout << "" << std::endl;
  std::cout << "ADVANCED FUNCTIONS & TESTS" << std::endl;
  std::cout << "ftf ... functional test                          ftt ... random transition test " << std::endl;
  std::cout << "ftr ... reg r/w test                             ftc ... clever transition test (random config)" << std::endl;
  std::cout << "ftz ... ZBT test                                 fts ... full FMM self test " << std::endl;
  std::cout << "fto ... Open Input Test                          fta ... full FMM self test for all FMMs in the crate" << std::endl;
  if (td)
    std::cout << "ftb ... Time Tag reset from backplane test       dsm ... Set TD Mode,   dap ... generate TD reset pulse" << std::endl;
}

bool do_fmm_action(std::string const& st, std::vector<tts::FMMCard*> &fmms, uint32_t& active_fmm, tts::FMMTDCard* td) {
	
  //--------------------------------------------------------------------------------
  // Basic register R/W
  //--------------------------------------------------------------------------------
  if (st == "0") {
    if (fmms.size()>1) {
      std::cout << "FMMs in the crate:" << std::endl;
      for (uint32_t i=0; i<fmms.size(); i++) {
	std::cout << "FMM nr " << i 
		  << ": SN=" << fmms[i]->getSerialNumber()
		  << "  Geoslot=" << std::dec << fmms[i]->getGeoSlot() << std::endl;
      }
      uint32_t af =0;  
      std::cout << "Select which FMM (0-" << std::dec << fmms.size()-1 << "): "; 
      std::cin >> std::dec >> af;
      if (af < fmms.size()) active_fmm = af;
    }  
  } 
  else if (st == "1") {
    std::string item;
    std::cout << "Enter item : ";
    std::cin >> item;

    uint32_t value;
    std::cout << "Enter value (hex) : ";
    std::cin >> std::hex >> value;

    fmms[active_fmm]->device().write(item, value);
  } 
  else if (st == "2") {
    std::string item;
    std::cout << "Enter item : ";
    std::cin >> item;

    uint32_t value;
    fmms[active_fmm]->device().read(item, &value);

    std::cout << "result : " << std::hex << std::setfill('0') << std::setw(8) <<
      value << std::endl;
  } 
  else if (st == "3") {
    fmms[active_fmm]->device().printAddressTable();
  } 
  else if (st == "7") {
    const char* reg_names[] = { "ID", "CNTL", "STAT", "FUNC_A", "FUNC_B", "Test_in_ready",
			  "Test_in_busy", "Test_in_synch", "Test_in_overflow",
			  "MASK", "ZBT_WR_ADD", "TRT_MISS", "Synch_thres", "Input_spy_ready",
			  "Input_spy_busy", "Input_spy_synch", "Input_spy_overflow", "OutputTest_value"  };

    for (uint32_t i=0; i<sizeof(reg_names)/sizeof(char*); i++) {
      uint32_t val;
      fmms[active_fmm]->device().read( reg_names[i], &val);
      std::cout << reg_names[i] << " value is : " << std::hex << val << std::endl; 
    }
  }
  //--------------------------------------------------------------------------------
  // Simple functions
  //--------------------------------------------------------------------------------
  
  else if (st == "fal") {
    bool success = fmms[active_fmm]->getWriteLock();

    if (success)
      std::cout << "successfully obtained write lock for this FMM";
    else {
      std::cout << "*** could not obtain write lock for this FMM. Lock is owned by process PID=" 
		<< std::dec << fmms[active_fmm]->getWriteLockOwnerPID() << std::endl;
      std::cout<< "Stale locks can be reset using 'daq/tts/ipcutils/test/linux/x86/cpci_lock_util.exe r'.";
    }

  }
  else if (st == "far") {
    fmms[active_fmm]->releaseWriteLock();
  }
  else if (st == "fan") {
    fmms[active_fmm]->disableLocking();
  }

  else if (st == "dal") {
    if (td) {
      bool success = td->getWriteLock();

      if (success)
	std::cout << "successfully obtained write lock for TD";
      else {
	std::cout << "*** could not obtain write lock for TD. Lock is owned by process PID=" 
		  << std::dec << td->getWriteLockOwnerPID() << std::endl;
	std::cout<< "Stale locks can be reset using 'daq/tts/ipcutils/test/linux/x86/cpci_lock_util.exe r'.";
      }
    }
  }
  else if (st == "dar") {
    if (td) 
      td->releaseWriteLock();
  }

  else if (st == "fsm") {
    uint32_t value;
    std::cout << "Enter mask (20-bit hex) : ";
    std::cin >> std::hex >> value;

    fmms[active_fmm]->setMask(value);
  } 
  else if (st == "fst20") {
    uint32_t value;
    std::cout << "Enter threshold for 20->1 out-of-synch merging (5-bit hex) : ";
    std::cin >> std::hex >> value;

    fmms[active_fmm]->setThreshold20(value);
  } 
  else if (st == "fst10A") {
    uint32_t value;
    std::cout << "Enter threshold for 10->1 out-of-synch merging of half-FMM A (4-bit hex) : ";
    std::cin >> std::hex >> value;

    fmms[active_fmm]->setThreshold10A(value);
  } 
  else if (st == "fst10B") {
    uint32_t value;
    std::cout << "Enter threshold for 10->1 out-of-synch merging of half-FMM B (4-bit hex) : ";
    std::cin >> std::hex >> value;

    fmms[active_fmm]->setThreshold10B(value);
  } 
  else if (st == "fst") {
    std::cout << "Enter enable bits for test outputs 3 to 0 (hex) 0..f: ";
    uint32_t test_output_enable;
    std::cin >> std::hex >> test_output_enable;
	  
    fmms[active_fmm]->enableTestOutputs(test_output_enable);
  } 
  else if (st == "fso") {
    std::cout << "Number of test output to change (0..3): ";
    uint32_t istate;
    std::cin >> std::dec >> istate;
    std::cout << "Enter state of output - ";
    tts::TTSState state = readState();
	  
    fmms[active_fmm]->setTestOutputValue(istate, state);
  } 
  else if (st == "fsi") {
    int istate=0;
    std::cout << "Number of channel to change (0..19): ";
    std::cin >> std::dec >> istate;
    std::cout << "Simulated State - ";
    tts::TTSState state = readState();
	  
    fmms[active_fmm]->setSimulatedInputState(istate, state);
  } 
  else if (st == "fss") {
    std::cout << "Enter '1' for simulation mode, '0' for normal operation : ";
    bool simu;
    std::cin >> simu;
	  
    fmms[active_fmm]->toggleSimuMode(simu);
  } 
  else if (st == "fsd") {
    std::cout << "Enter '1' for dual-FMM mode, '0' for single-FMM mode : ";
    bool dual;
    std::cin >> dual;
	  
    fmms[active_fmm]->toggleDualMode(dual);
  } 
  else if (st == "fsl") {
    std::cout << "Enter setting for LEDs 4 to 2 (hex) 0..7: ";
    uint32_t led;
    std::cin >> std::hex >> led;
	  
    fmms[active_fmm]->setLEDs(led);
  } 
  else if (st == "fdh") {
    uint32_t from, to;
    std::cout << "Enter first address in history buffer (hex) 0 .. 1FFFF) :";
    std::cin >> std::hex >> from;
    std::cout << "Enter last address in history buffer (hex) 0 .. 1FFFF) :";
    std::cin >> std::hex >> to;

    for(uint32_t add=from; add!=(to+1) % 0x20000; add = (add+1) % 0x20000) {
      tts::FMMHistoryItem hi;
      fmms[active_fmm]->readHistoryItem(add, hi);
      std::cout << "addr = " << std::setw(6) << std::hex << add << "   " << hi.toString() << std::endl;
    }
  }
  else if (st == "fch") {
    std::cout << "clearing history memory" << std::flush;
    fmms[active_fmm]->clearHistoryMemory();
    std::cout << " ... done." << std::endl;
  }	
  else if (st == "fdd") {
    std::vector<uint32_t> dc;
    fmms[active_fmm]->readDeadTimeCounters(dc);
    const char* deadtime_categories[] = { "/BUSY   ", "/WARNING", "/READY  ", "/OOS    ", "/ERROR  " };

    for (int i=0; i< tts::FMMCard::NumCounters; i++) {
      std::cout << "ch = " << std::setw(2) << std::dec << (i%22) 
		<< deadtime_categories[i/22]
		<< "  hw dead time (ticks) = " << std::setw(10) << dc[i] << std::endl;
    }
  }
  else if (st == "frd") 
    fmms[active_fmm]->resetDeadTimeMonitors(); 
  
  else if (st == "frh") 
    fmms[active_fmm]->resetHistoryAddress(); 
  
  else if (st == "frm") 
    fmms[active_fmm]->resetTransitionMissCounter(); 
  
  else if (st == "frt") 
    fmms[active_fmm]->resetTimeTag(); 
  
  else if (st == "fra") 
    fmms[active_fmm]->resetAll(); 
  else if (st == "dsm") {
    if (td) {
      std::cout << "Enter '1' for PCI (register) mode, '0' for LEMO mode : ";
      bool pci;
      std::cin >> pci;
	  
      td->togglePCIMode(pci);
    }
  } 
  else if (st == "dap") {
    if (td) {
      std::cout << "Generating reset pulse from Trigger Distributor." << std::endl;
      td->generateResetPulse();
    }
  } 
  
  //--------------------------------------------------------------------------------
  // More advanced functions
  //--------------------------------------------------------------------------------

  else if (st == "ftr") {
    tts::FMMRegisterTest rt(fmms[active_fmm]->device(), std::cout, tts::FMMBasicTest::V_INFO);
    rt.run();
  } 
  else if (st == "ftz") { 
    tts::FMMZBTTest zt(fmms[active_fmm]->device(), std::cout, tts::FMMBasicTest::V_INFO);
    zt.run();
  } 
  else if (st == "ftf") { 
    tts::FMMFunctionalTest ft(*fmms[active_fmm], std::cout, tts::FMMBasicTest::V_INFO);
    ft.run(10);
  } 
  else if (st == "ftt") { 
    tts::FMMRandomTransitionTest rt(*fmms[active_fmm], std::cout, tts::FMMBasicTest::V_INFO); 
    rt.run(100000);
  } 
  else if (st == "fto") { 
    std::cout << "FMM Open Input Test. Please disconnect all cables at the inputs of FMM " << active_fmm 
	      << "  SN=" << fmms[active_fmm]->getSerialNumber() << ". Start the test? (y/n) " ;
    char ch; 
    std::cin >> ch;
    if (ch=='y') {
      tts::FMMOpenInputsTest ot(*fmms[active_fmm], std::cout, tts::FMMBasicTest::V_INFO);
      ot.run(10);
    }
  } 
  else if (st == "ftc") { 
    tts::FMMPoissonTransitionGenerator tg;
    tts::FMMTransitionTest tt(*fmms[active_fmm], tg, std::cout, tts::FMMBasicTest::V_INFO);
    tt.setConfiguration(true);
    tt.run(100000);
  } 
  else if (st == "fts") { 
    if (td && td->haveWriteLock()) {
      tts::FMMFullSelfTest ft(*fmms[active_fmm], td, std::cout, tts::FMMBasicTest::V_INFO);
      ft.run();
    }
    else {
      std::cout << "*** skipping TTReset test because do not have FMMTD or do not have write lock for FMMTD" << std::endl;
      tts::FMMFullSelfTest ft(*fmms[active_fmm], 0, std::cout, tts::FMMBasicTest::V_INFO);
      ft.run();
    }
  } 
  else if (st == "ftb") {
    if (td) {
      if (td->haveWriteLock()) {
	tts::FMMTTResetTest ft(*fmms[active_fmm], *td, std::cout, tts::FMMBasicTest::V_INFO);
	ft.run();
      }
      else {
	std::cout << "*** skipping test because do not have write lock for FMMTD" << std::endl;
      }
    }
  } 
  else if (st == "fta") { 
    std::cout << "Multiple FMM Full Self-Test Requested ... " << std::endl;
    fmm_multi_test(fmms, active_fmm, td);
  } 
  else
    return false;

  return true;
}

void display_fmm_status( tts::FMMCard* fmm ) {

  std::cout << ">Simu mode is " << ( fmm->isSimuMode() ? "ON " : "OFF" )
	    << "  Dual mode is " << ( fmm->isDualMode() ? "ON " : "OFF" )
	    << "  HistoryMode is " << ( fmm->isDMAMode() ? "DMA" : "ZBT" ) 
	    << "  HaveWriteLock=" << ( fmm->haveWriteLock() ? "Yes":"No" ) << std::endl;
  std::cout << "  Sync merge threshold (20->1) = 0x" << std::hex << fmm->getThreshold20()
	    << "  (10->1)A = 0x" << std::hex << fmm->getThreshold10A()
	    << "  (10->1)B = 0x" << std::hex << fmm->getThreshold10B()
	    << std::endl;

  std::cout << ">         channel nr =>     ";
  for (int i=19;i>=0;i--) std::cout << std::dec << std::setw(2) << std::setiosflags(std::ios::left) << i << "  ";
  std::cout << std::setiosflags(std::ios::right) << std::endl;

  std::cout << ">mask[19:0] ('1' is off)  = ";
  uint32_t mask = fmm->getMask();
  for (int i=19;i>=0;i--) std::cout << ( (mask & (1<<i))>>i ) << "   ";
  std::cout << std::endl;

  std::cout << ">current inp states[19:0] = ";
  if (fmm->haveWriteLock()) {
    std::vector<tts::TTSState> states = fmm->readInputs();
    for (int i=19;i>=0;i--) std::cout << states[i] << " ";
    std::cout << std::endl;
  }
  else
    std::cout << "[need write lock to latch and read input states]" << std::endl; 

  std::cout << ">simu input states [19:0] = ";
  std::vector<tts::TTSState> states = fmm->getSimulatedInputStates();
  for (int i=19;i>=0;i--) std::cout << states[i] << " ";
  std::cout << std::endl;

  std::cout << ">ResultA output = " << fmm->readResultA().getName() 
	    << "     ResultB output = " << fmm->readResultB().getName() 
	    << " Test output enables = 0x" << std::hex << fmm->readTestOutputEnables() << std::endl;

  for (int i=0; i<4; i++) {
    std::cout << "OUT" << std::dec << (20+i) << "=";
    if (fmm->readTestOutputEnables() & (1<<i) ) 
      std::cout << fmm->readTestOutputValue(i) << " (TEST MODE)";
    else {
      if (i<2) 
	std::cout << fmm->readResultA() << " (RESULT A)";
      else
	std::cout << fmm->readResultB() << " (RESULT B)";
    }
    std::cout << std::endl;
  }
  std::cout << ">History memory write address = 0x" << std::setw(6) << std::hex << fmm->readHistoryAddress()
	    << "   Transition miss counter = 0x" << std::setw(6) << std::hex << fmm->readTransitionMissCounter() 
    //       << "   TimeTag counter = 0x" << std::setw(12) << std::hex << fmm->readTimeTag() 
	    << std::endl;
}

tts::TTSState readState() {
  tts::TTSState state = tts::TTSState::INVALID;
  std::string entry;
  std::cout << "R, B, S, W, E, d(=0x0), D(=0xF), I(=0x3) or hex with prefix '0x': ";
  std::cin >> entry;
  if (entry.find("0x")==0) {
    int istate;
    std::istringstream is(entry.substr(2));
    is >> std::hex >> istate;
    state = istate;
  } else {
    switch(entry.c_str()[0]) {
    case 'R' : state = tts::TTSState::READY; break; 
    case 'B' : state = tts::TTSState::BUSY; break; 
    case 'S' : state = tts::TTSState::OUTOFSYNC; break; 
    case 'W' : state = tts::TTSState::WARNING; break; 
    case 'E' : state = tts::TTSState::ERROR; break; 
    case 'd' : state = tts::TTSState::DISCONNECT1; break; 
    case 'D' : state = tts::TTSState::DISCONNECT2; break; 
    case 'I' : state = tts::TTSState::INVALID; break; 
    }
  }
  std::cout << "selected STATE: " << state.getName() << std::endl;
  return state;
}



int kbhit() /* returns the number of waiting chars on the buffer */
{
  int i;
  ioctl(0, FIONREAD, &i);
  return i;
} 

std::string deltat_to_hms(uint32_t secs) {
  uint32_t hrs = secs / (60*60);
  secs -= hrs * (60*60);
  uint32_t mins = secs / 60;
  secs -= mins * 60;
  
  std::stringstream ss;  
  ss << std::setfill('0') << std::setw(2) << std::dec << hrs << ":" 
     << std::setfill('0') << std::setw(2) << std::dec << mins << ":" 
     << std::setfill('0') << std::setw(2) << std::dec << secs << std::setfill(' ');

  return ss.str();
}


void fmm_multi_test(std::vector<tts::FMMCard*> &fmms, uint32_t active_fmm, tts::FMMTDCard* td) {

  std::cout << "Do you want to log test results in the database (y/n)?";
  char ch;
  std::cin >> ch;
  std::cout << std::endl;
  tts::FMMDBI* dbi = 0;
#ifndef FMM_NODB
  if (ch == 'y')
    dbi = tts::fmmdbi::getOracleDBI("CMS_DAQ_HSAKULIN", "a3a3a3a3", "devdb10");
#endif

  bool continuous_db_update = true;

  //
  // First decide for which FMMs to run the test
  //

  std::cout << "FMMs in the crate:" << std::endl;
  for (uint32_t i=0; i<fmms.size(); i++) {
    std::string sn = fmms[i]->getSerialNumber();
    std::cout << "Nr " << std::setw(2) << std::dec << i << ": "
	      << "SN=" << sn
	      << "  Xilinx Firmware ID=" << fmms[i]->readFirmwareRev()
	      << "  Altera Firmware ID=" << fmms[i]->readFirmwareRevAltera();
    if (dbi)
      std::cout << "  in DB = " << dbi->boardDefined(sn);
    std::cout << std::endl;
  }

  int selection;
  do {
    std::cout << std::endl << "Select which FMM(s) to test (0.." << fmms.size()-1 << ", -1 to test all, -2 to select one by one): ";
    std::cin >> selection;
  } while ( selection < -2 || selection >= (int)fmms.size() );

  
  std::vector < std::pair<int, tts::FMMFullSelfTest*> > tests;
  //  ofstream nul("/dev/null");
  
  for (uint32_t i=0; i<fmms.size(); i++) {

    std::cout <<  "Nr " << std::setw(2) << std::dec << i << ": SN=" << fmms[i]->getSerialNumber();
    bool use_this_fmm = false;
    if (selection==-2) {
      std::cout << "  Test this FMM (y/n)? ";
      char ch;
      std::cin >> ch;
      use_this_fmm = (ch=='y');
    }

    if ( selection==-1 || selection==(int)i )
      use_this_fmm = true;
    
    if (use_this_fmm) {
      std::cout << "  using this FMM." << std::endl;
     
      // create the test
      tts::FMMFullSelfTest* fst = new tts::FMMFullSelfTest(*fmms[i], td);

      int testid = -1;
      if ( continuous_db_update && dbi && dbi->boardDefined(fmms[i]->getSerialNumber()) ) {
	std::cout << "registering test in DB" << std::endl;
	testid = dbi->logTest(fmms[i]->getSerialNumber(),
			      getenv("LOGNAME"),
			      getenv("HOSTNAME"));
	dbi->logSelfTestDetails(testid, fmms[i]->readFirmwareRevAltera(), fmms[i]->readFirmwareRev());

	// log results
	std::vector <tts::FMMBasicTestResult> results;
	fst->getResults(results);
	dbi->logBasicTestResult(testid, results);

      }
      tests.push_back( std::make_pair(testid, fst) );


      

    }
    else 
      std::cout << "  not using this FMM" << std::endl;
  }

  //
  // Start the test
  //
  
  time_t tstart;
  time(&tstart);
  std::cout << "===FMM Multi Test  started on " << ctime(&tstart) << std::endl;

  bool done = false;
  do {
    
    std::vector< std::pair<int, tts::FMMFullSelfTest*> >::iterator it = tests.begin();
    for (; it != tests.end(); it++) {
 
      (*it).second->run();
      (*it).second->printStatus();

      if (continuous_db_update && dbi && (*it).first != -1) {
	dbi->updateSelfTestDetails( (*it).first );
	// log results
	std::vector <tts::FMMBasicTestResult> results;
	(*it).second->getResults(results);
	dbi->updateBasicTestResult((*it).first, results);
      }
      
      if ( kbhit() ) break;
    }

    time_t tlap;
    time(&tlap);

    std::cout << "====================== accumulated test time = " << deltat_to_hms(tlap-tstart) 
	      << " <to end the test press ENTER and wait>"<< std::endl;

    // check for key pressed
    if (kbhit()) {
      //      cin.ignore(std::numeric_limits<int>::max(), '\n');
      //      cin.sync();

      std::cout << "Do you really want to end the test (yes/no)? ";
      
      std::string dummy;
      std::cin >> dummy;
      if (dummy == "yes") 
	done = true;
      else
	std::cout << "Continuing test..." << std::endl;
    }
    
  } while (! done );
  

  //
  // Finish the test
  //

  time_t tstop;
  time(&tstop);
  std::cout << "===FMM Multi Test finished on " << ctime(&tstop) 
	    << "  [duration = " << (tstop-tstart) << " sec]" << std::endl;

  if (!continuous_db_update && dbi) {
    std::cout << "Do you want to log the test results in the database (y/n)? ";
    char input;
    std::cin >> input;
    
    if (input == 'y') {
      std::vector< std::pair<int, tts::FMMFullSelfTest*> >::iterator it = tests.begin();
      for (; it != tests.end(); it++) {
	std::string sn = (*it).second->card().getSerialNumber();
	std::cout << "registering test results for FMM " << sn << " in database." << std::endl;
	dbi->logTest( sn, 
		      getenv("LOGNAME"),
		      getenv("HOSTNAME"));
      }
    }
    
    
  }
  

  // Log the result in the database

  // delete the tests
  std::vector< std::pair<int, tts::FMMFullSelfTest*> >::iterator it = tests.begin();
  for (; it != tests.end(); it++) 
    delete (*it).second;
}
