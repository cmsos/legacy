//
//  DaqletStub.java
//  
//
//  Created by Luciano Orsini on Fri Sep 06 2002.
//  Copyright (c) 2002 __CERN__. All rights reserved.
//

package xdaq.xdesk.xdesk;

import java.applet.*;
import java.net.*;
import java.lang.*;
import java.util.*;
import javax.swing.JApplet;

import org.apache.log4j.*;


public class DeskletStub implements AppletStub 
{
	static Logger logger = Logger.getLogger (DeskletStub.class);

	DeskletContext context_;
	Properties properties_;
	boolean active_;
	boolean initialized_;
	JApplet applet_;

	public DeskletStub( Properties properties, DeskletContext context, JApplet applet)
	{
	    properties_ = properties;
	    context_ = context;
	    active_ = false;
	    applet_ = applet;
	    initialized_ = false;
	}
	
	public String  toString ()
	{
		return applet_.getName();
	}
	
	public boolean isInitialized()
	{
		return initialized_;
	}
	
	public void initialize()
	{
		if (initialized_ == false)
		{
			applet_.init();
			initialized_ = true;	
		}
	}

	// Called when the applet wants to be resized.
	public void appletResize(int width, int height) 
	{
	}
    
	public JApplet getApplet ()
	{
    		return applet_;
	}

    // Gets a handler to the applet's context.
    public AppletContext getAppletContext() 
    {
        return context_;
    }
   
     // Gets the base URL.
    public URL	getCodeBase()
    {
	URL url = null;
    	try 
	{
    		url = new URL(properties_.getProperty("codebase"));
	} 
	catch (java.net.MalformedURLException e)
	{
		logger.error ("Malformed 'codebase' for desklet "+properties_.getProperty("name")+": "+e.toString());
		return url;
	}
        return url;
    }
    
    // Returns an absolute URL naming the directory of the document in which the applet is embedded.
    public URL	getDocumentBase() 
    {
    	logger.debug ("getDocumentBase function has only dummy implementation.");
	
    	URL url = null;
    	try 
	{
    		url = new URL(properties_.getProperty("http://xdaq.web.cern.ch/??????"));
	} 
	catch (java.net.MalformedURLException e)
	{
		return url;
	}
        return url;
    }
   
     // Returns the value of the named parameter in the XML tag.
    public String getParameter(String name) 
    {
          return properties_.getProperty(name);
    }
    
    // Determines if the applet is active.
    public boolean isActive()
    {
        return active_;
    } 
    
    public void setActive (boolean active)
    {
    	active_ = active;
    }
}
