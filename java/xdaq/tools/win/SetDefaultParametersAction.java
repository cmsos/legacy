package xdaq.tools.win;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.ParameterTableModel;

public class SetDefaultParametersAction implements ActionListener
{
	xdaqWin win_;
	PropertiesWindow propertiesWin_;

	public SetDefaultParametersAction (xdaqWin win, PropertiesWindow propertiesWin)
	{
		win_ = win;
		propertiesWin_ = propertiesWin;
	}
	
	public void actionPerformed (ActionEvent e)
	{		
		ParameterTableModel model = propertiesWin_.getModel();
		TreePath[] paths = propertiesWin_.getSelection();
		
		for (int i = 0; i < paths.length; i++) 
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();	
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();
			if (node.toString().equals("Context") || node.toString().equals ("Application") || node.toString().equals ("Transport"))
			{
				xdaqWinTreeAdapterNode defaultParameters = aNode.firstChild( "DefaultParameters" );
				if (defaultParameters.domNode != null)
				{
					model.update (defaultParameters.domNode);
				} else
				{
					JOptionPane.showMessageDialog ( null, 
									"XML configuration file does not contain default parameters for this application", "SetDefault error", JOptionPane.ERROR_MESSAGE );
				}
			} else {
				return; // don't update information
			}
		}
			
	}
			
}
