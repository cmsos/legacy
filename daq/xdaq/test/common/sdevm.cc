#include "sdevm.h"

extern "C" int Main(char * evmhost, char * ruhost)
{
  	TcpDispatcherAcceptorTaskS * d = new TcpDispatcherAcceptorTaskS(evmhost,20200);
	TcpXIOConnectorS * c = new TcpXIOConnectorS(ruhost, 20301);
	c->connect();
	XFU * fu = new XFU(d->getXIO());
	XRU * ru = new XRU(c->getXIO());
	SDEVM * sdevm = new SDEVM(fu,ru, 100);

  	TaskGroup g;
  	d->initTaskGroup(&g);
	sdevm->initTaskGroup(&g);
	d->addListener(sdevm);
  
  	d->activate();
	sdevm->activate();
	
 	g.join();
	delete d ;
	delete fu;
	delete ru;
	delete sdevm;
	return(0);	
}
#ifndef vxworks
int main(int argc, char* argv[])
{

	return Main(argv[1], argv[2]);
}
#endif
