#!/usr/bin/env python

###############################################################################
## A little script to switch maintenance mode on/off on TCDS control
## applications. Either by specifying a single target application, or
## by specifying 'all' to switch all applications at the same time.
###############################################################################

import os
import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_soap import build_xdaq_soap_command_message
from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_DEFAULT

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class ModeSwitcher(CmdLineBase):

    # The (integer) identifier of the RunControl session 'owning' the
    # TCDS hardware.
    RC_SESSION_ID = 666
    # The (string) identifier of the RunControl session 'owning' the
    # TCDS hardware.
    LEASE_OWNER_ID = "TEST_JEROEN_MAINTENANCE_MODE"

    STATE_CHOICES = ["on", "off"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(ModeSwitcher, self).__init__(description, usage, epilog)
        self.rc_session_id = ModeSwitcher.RC_SESSION_ID
        self.lease_owner_id = ModeSwitcher.LEASE_OWNER_ID
        self.state = None
        # End of __init__().

    def setup_parser_custom(self):
        # Start with the basic parser configuration.
        super(ModeSwitcher, self).setup_parser_custom()
        parser = self.parser

        # Add the target maintenance mode state argument.
        help_str = "The target maintenance mode state"
        parser.add_argument("state",
                            type=str,
                            choices=ModeSwitcher.STATE_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(ModeSwitcher, self).handle_args()
        # Extract the desired new state maintenance mode state (i.e.,
        # on or off).
        self.state = self.args.state.lower()
        # End of handle_args().

    def main(self):

        if self.target == 'all':
            targets = self.setup.sw_targets.keys()
        else:
            targets = [self.target]

        for target in targets:
            # Check if the target name is valid.
            self.validate_target(target, target_type=CmdLineBase.TARGET_TYPE_SW)

            #----------

            # Now actually do what we have been asked to do: send a SOAP
            # command to some targets.
            target_info = self.setup.sw_targets[target]
            host_name = target_info.host
            port_number = target_info.port
            lid_number = target_info.lid
            command_name = "EnterMaintenanceMode"
            if self.state == 'off':
                command_name = "ReleaseMaintenanceMode"
            soap_msg = build_xdaq_soap_command_message(self.rc_session_id,
                                                       self.lease_owner_id,
                                                       command_name,
                                                       None,
                                                       None)
            soap_reply = send_xdaq_soap_message(host_name,
                                                port_number,
                                                lid_number,
                                                soap_msg,
                                                verbose=self.verbose)
            soap_fault = extract_xdaq_soap_fault(soap_reply)
            if soap_fault:
                msg = "SOAP reply indicates a problem: '{0:s}'"
                self.error(msg.format(soap_fault))

        # End of main().

    # End of class ModeSwitcher.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to switch maintenance mode on/off on TCDS control applications."

    res = ModeSwitcher(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
