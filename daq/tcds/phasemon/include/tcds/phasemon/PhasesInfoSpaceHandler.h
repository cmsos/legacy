#ifndef _tcds_phasemon_PhasesInfoSpaceHandler_h_
#define _tcds_phasemon_PhasesInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/MultiInfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    /* class Monitor; */
    /* class WebServer; */
  }
}

namespace tcds {
  namespace phasemon {

    class PhaseMon;

    class PhasesInfoSpaceHandler : public tcds::utils::MultiInfoSpaceHandler
    {

    public:
      PhasesInfoSpaceHandler(tcds::phasemon::PhaseMon& xdaqApp,
                             tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PhasesInfoSpaceHandler();

      tcds::phasemon::PhaseMon& getOwnerApplication() const;

    /* protected: */
    /*   virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor); */
    /*   virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer, */
    /*                                              tcds::utils::Monitor& monitor, */
    /*                                              std::string const& forceTabName=""); */

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_PhasesInfoSpaceHandler_h_
