/**
 *      @file FMMWebInterface.cc
 *
 *       @see ---
 *    @author Hannes Sakulin
 * $Revision: 1.21 $
 *     $Date: 2008/09/02 15:21:21 $
 *
 *
 **/
#include "tts/fmmcontroller/FMMWebInterface.hh"
#include "tts/fmmcontroller/version.h"

// for web callback binding and HTML formatting
#include "xgi/Method.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"

// to access URL and URN of owner
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"

// for access to FMM Hardware
#include "tts/fmm/FMMCard.hh"
#include "tts/fmmtd/FMMTDCard.hh"
#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMDeadTimeMonitor.hh"

#include "toolbox/string.h"

#include <iostream>
#include <iomanip>
#include <time.h>

static std::string lastFormAction("");

tts::FMMWebInterface::FMMWebInterface(xdaq::Application* owner,
		xdata::Vector<xdata::Bag<tts::FMMParameters> >& config, 
		tts::FMMCrate* fmmcrate, 
		std::map<std::string, tts::FMMDeadTimeMonitor*>& dtmons,
		log4cplus::Logger& logger) 
: xdaq::Object(owner), _config(config), _fmmcrate(fmmcrate), _dtmons(dtmons), _logger(logger), _autoupdate(false) {
		}	

		tts::FMMWebInterface::~FMMWebInterface() {
		}

		void tts::FMMWebInterface::displayDefaultWebPage(xgi::Input * in, xgi::Output * out, 
				std::string const& statename) 
		throw (xgi::exception::Exception) {

			/// check if we have parameters
			bool formUsed = false;
			try {
				cgicc::Cgicc cgi(in);    
				if ( xgi::Utils::hasFormElement(cgi,"autoUpd") ) {
					_autoupdate = xgi::Utils::getFormElement(cgi, "autoUpd")->getIntegerValue()  != 0;
					formUsed = true;
				}
				if ( xgi::Utils::hasFormElement(cgi,"bsubmit1") || xgi::Utils::hasFormElement(cgi,"bsubmit2")) {
					lastFormAction = "";
					formUsed = true;
					
					bool doDisable = false;
					if ( xgi::Utils::hasFormElement(cgi,"bsubmit1") ) { 
						lastFormAction += "*DISABLING*";
						doDisable = true;
					}
					else {
						lastFormAction += "*ENABLING*";
						doDisable = false;
					}

					std::vector<cgicc::FormEntry> boxes;
					if (cgi.getElement("fmminput", boxes) ) {
						std::vector<cgicc::FormEntry>::iterator it = boxes.begin();
						for ( ;it != boxes.end(); ++it) {
							lastFormAction += "g=" + (*it).getValue() + ";";
							
							uint32_t boxnr = (*it).getIntegerValue();
							uint32_t igeoslot = boxnr / 20;
							uint32_t inp = boxnr % 20;
							
							
							lastFormAction += toolbox::toString("slot=%d,bit=%d",igeoslot,inp);
							
							// find FMM for igeoslot
							uint32_t i=999;
							for (uint32_t ifmm=0; ifmm<_fmmcrate->numFMMs();ifmm++)
								if (_fmmcrate->getFMM(ifmm).getGeoSlot() == igeoslot)
									i = ifmm;

							if (i==999) {// no FMM found in this igeoslot 
								lastFormAction += "no fmm found";
								continue;
							}
						
							if (_fmmcrate->getFMM(i).haveWriteLock()) {
								
								uint32_t mask = _fmmcrate->getFMM(i).getMask();
								
								if (doDisable)
									mask |= (1<<inp);
								else
									mask &= ~(1<<inp);
								_fmmcrate->getFMM(i).setMask(mask);
								lastFormAction += "mask updated.";
							}
							else {
								lastFormAction += "no write lock";
							}
							
						}
					}

					
					
					
					
					
					
				}
				
//				if ( xgi::Utils::hasFormElement(cgi,"fmminput") )
//					lastFormAction += "*fmminput found*";
				
//				const std::vector< cgicc::FormEntry > &elems = cgi.getElements();
//				std::vector< cgicc::FormEntry >::const_iterator it2  = elems.begin();
//				for (;it2!=elems.end();++it2) {
//					lastFormAction += (*it2).getName() + "=" + (*it2).getValue() + ";";
//				}
				
				
//				if ( xgi::Utils::hasFormElement(cgi,"bsubmit1") )
//					lastFormAction += std::string("bsubmit1value=") + xgi::Utils::getFormElement(cgi, "bsubmit1")->getValue();
				
				
				
			}
			catch (const std::exception & e) {
				lastFormAction += std::string("caught exception:") + e.what();
			}


			try {
				if (formUsed) {
					out->setHTTPResponseHeader( cgicc::HTTPResponseHeader(out->getHTTPResponseHeader().getHTTPVersion(), 303, std::string("redirect") ) ); 
					// constructing a cgicc::HTTPResponseHeader on the stack is ok, since setHTTPResponseHeader will make a copy     
					out->getHTTPResponseHeader().addHeader("Location", 
							this->getOwnerApplication()->getApplicationDescriptor()->getContextDescriptor()->getURL() + std::string("/") + 
							this->getOwnerApplication()->getApplicationDescriptor()->getURN() );
					return;
				}

				out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

				*out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
				*out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;

				//--------------------------------------------------------------------------------
				// HTML Header
				//--------------------------------------------------------------------------------
				std::string title = "FMM Controller";
				char * hostname = getenv("HOSTNAME");
				if (hostname != 0) title += std::string(" on host") + hostname;

				*out << cgicc::head() 
				<< cgicc::title( title );
				if (_autoupdate) {
					*out << "<meta http-equiv=\"refresh\" content=\"2\">";
				}
				*out << cgicc::head() << std::endl;

				//--------------------------------------------------------------------------------
				// HTML Body
				//--------------------------------------------------------------------------------
				*out << cgicc::body() << std::endl;

				time_t now;
				now = time(NULL);

				
				*out << cgicc::table() << cgicc::tr();

				*out << cgicc::td() << cgicc::form().set("method","POST").set("action", std::string("/") + this->getOwnerApplication()->getApplicationDescriptor()->getURN() + "/Default") << std::endl;
				*out << cgicc::input().set("type","hidden").set("name","autoUpd").set("value", _autoupdate?"0":"1") << std::endl;
				*out << cgicc::input().set("type","submit").set("value",_autoupdate?"Toggle autoUpdate off":"Toggle autoUpdate on")  << std::endl;
				*out << cgicc::form() << std::endl;
				*out << cgicc::td();

				*out << cgicc::td() << "FMMController V_" << TTSFMMCONTROLLER_VERSION_MAJOR
				<< "_" << TTSFMMCONTROLLER_VERSION_MINOR
				<< "_" << TTSFMMCONTROLLER_VERSION_PATCH
				<< "&nbsp;&nbsp;&nbsp;"  << cgicc::td() ;

				*out << cgicc::td() << ctime(&now)  << "&nbsp;&nbsp;&nbsp;" << cgicc::td() << std::endl;

				*out << cgicc::td() << "Current state: " << statename << cgicc::td() << std::endl;

				*out <<  cgicc::tr() << cgicc::table();

				*out << "FMMs in the crate - dynamic information as read from hardware (plus labels from configuration parameters)" << std::endl;

				//--------------------------------------------------------------------------------
				// Display list of FMMs in the crate
				//--------------------------------------------------------------------------------
				*out << cgicc::form().set("method","POST").set("action", std::string("/") + this->getOwnerApplication()->getApplicationDescriptor()->getURN() + "/Default") << std::endl;

				*out << cgicc::table().set("border","1") << std::endl;
				*out << cgicc::tr().set("bgcolor","#CCFFCC")
				.add(cgicc::td("GeoSlot").set("rowspan","2"))
				.add(cgicc::td("cfg'd").set("rowspan","2"))
				.add(cgicc::td("haveLock").set("rowspan","2"))
				.add(cgicc::td("HistAddr").set("rowspan","2"))
				.add(cgicc::td("TRMiss").set("rowspan","2"))
				.add(cgicc::td("Current Input states (19..0) - Disabled channels in italics").set("colspan","20")) 
				.add(cgicc::td("").set("bgcolor", "#FFFFFF")) 
				.add(cgicc::td("Output States (3..0)").set("colspan","4")) 
				<< std::endl;

				*out << cgicc::tr().set("bgcolor","#CCFFCC");

				for (int i=19;i>=0;i--)
					*out << cgicc::td() << i << cgicc::td();

				*out << cgicc::td("").set("bgcolor", "#FFFFFF");

				for (int i=3;i>=0;i--)
					*out << cgicc::td() << i << cgicc::td();

				*out << cgicc::tr() << std::endl;

				bool starstar=false;
				for (uint32_t igeoslot=0; igeoslot<=20; ++igeoslot) {

					// find FMM for igeoslot
					uint32_t i=999;
					for (uint32_t ifmm=0; ifmm<_fmmcrate->numFMMs();ifmm++)
						if (_fmmcrate->getFMM(ifmm).getGeoSlot() == igeoslot)
							i = ifmm;

					if (i==999) // no FMM found in this igeoslot 
						continue;


					//
					// empty row
					//

					*out << cgicc::tr().set("bgcolor", "#FFFFFF")
					<< cgicc::td("").set("colspan","30") << cgicc::tr() << std::endl;

					//
					// chek if we are in the configuration of this FMMController
					//

					bool in_config = false;
					xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
					for ( ; it != _config.end(); it++) {
						uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
						if (geoslot != 999) {
							if (_fmmcrate->getFMM(i).getGeoSlot() == geoslot)
							{ in_config = true; break; }

						}
						else {
							if (_fmmcrate->getFMM(i).getSerialNumber() == (*it).getField("serialnumber")->toString()) 
							{ in_config = true; break; }
						}
					}


					if (in_config && _fmmcrate->getFMM(i).haveWriteLock() ) {


						// get states, first
						std::vector<tts::TTSState> istates = _fmmcrate->getFMM(i).readInputs();
						uint32_t mask = _fmmcrate->getFMM(i).getMask();

						std::vector<tts::TTSState> outStates;
						tts::TTSState resultA = _fmmcrate->getFMM(i).readResultA();
						outStates.push_back( resultA );
						outStates.push_back( resultA );
						if ( _fmmcrate->getFMM(i).isDualMode() ) {
							tts::TTSState resultB = _fmmcrate->getFMM(i).readResultB();
							outStates.push_back( resultB );
							outStates.push_back( resultB );
						}      
						else {
							outStates.push_back( resultA );
							outStates.push_back( resultA );
						}

						//
						// first row (labels)
						//
						bool haveLabelsRow = false;
						if ((*it).getField("label")->toString().size() != 0 ||
								(*it).getField("inputLabels")->toString().size() != 0 ||
								(*it).getField("outputLabels")->toString().size() != 0 ) { // with labels

							haveLabelsRow = true;
							*out << cgicc::tr().set("bgcolor", in_config? "#F2F458" : "#CFCFCF")
							<< cgicc::td().set("rowspan","7") <<  _fmmcrate->getFMM(i).getGeoSlot() << cgicc::td();

							*out << formatLabelsSubRow((*it).getField("label")->toString(),
									(*it).getField("inputLabels")->toString(),
									(*it).getField("outputLabels")->toString(),
									istates,
									outStates,
									mask,
									in_config,
									igeoslot);

							*out << cgicc::tr() << std::endl;
						}


						//
						// second row (states)
						//
						if (!haveLabelsRow) 
							*out << cgicc::tr().set("bgcolor", in_config? "#F2F458" : "#CFCFCF")
							<< cgicc::td().set("rowspan","6") <<  _fmmcrate->getFMM(i).getGeoSlot() << cgicc::td();
						else
							*out << cgicc::tr().set("bgcolor", in_config? "#F2F458" : "#CFCFCF");

						*out << cgicc::td( in_config?"Y":"N" ).set("rowspan","6")
						<< cgicc::td( "Y" ).set("rowspan","1")
						<< cgicc::td().set("rowspan","1") <<  std::dec << _fmmcrate->getFMM(i).readHistoryAddress()  << cgicc::td()
						<< cgicc::td().set("rowspan","1") <<  std::dec << _fmmcrate->getFMM(i).readTransitionMissCounter()  << cgicc::td()
						<< formatStates(istates, mask, in_config );

						*out << cgicc::td("").set("bgcolor", "#FFFFFF");

						*out << formatStates( outStates, 0, in_config );    
						*out << cgicc::tr() << std::endl;


						if ( _dtmons.count( _fmmcrate->getFMM(i).getSerialNumber() ) ) {

						  std::vector<double> deadTimeFractions;
						  _dtmons[_fmmcrate->getFMM(i).getSerialNumber()]->readDeadTimeFractionsLastInterval( deadTimeFractions );
						  

						  //
						  // third to seventh row (dead times busy, warning, ready, oos, error)
						  //
						  const char* dtTypes[] = { "Busy", "Warning", "Ready", "OOS", "Error" };

						  for (uint32_t itype =0; itype<5; ++itype) {

						    *out << cgicc::tr().set("bgcolor", in_config? "#F2F458" : "#CFCFCF");
						    *out << cgicc::td().set("colspan","3") << "deadtime frac " << dtTypes[itype] << " (*)" << cgicc::td();

						    // dead time fractions xx for inputs
						    for (int j=19; j>=0; j--) {
						      *out << cgicc::td().set("bgcolor", mask&(1<<j)? "#CFCFCF" : "#F2F458") 
							   << std::fixed << std::setw(3) << std::setprecision(3) << deadTimeFractions[j + 22*itype];

						      *out << cgicc::td();
						    }

						    *out << cgicc::td("").set("bgcolor", "#FFFFFF");

						    if (_fmmcrate->getFMM(i).readFirmwareRev() >= "060515_00") {

						      // dead time fractions xx for output B
						      for (int j=3;j>=2;j--) 
							*out << cgicc::td().set("bgcolor", "#F2F458") 
							     << std::fixed << std::setw(3) << std::setprecision(3) << deadTimeFractions[21+ 22*itype] << cgicc::td();	  

						      // dead time fractions xx for output A
						      for (int j=1;j>=0;j--) 
							*out << cgicc::td().set("bgcolor", "#F2F458") 
							     << std::fixed << std::setw(3) << std::setprecision(3) << deadTimeFractions[20+ 22*itype] << cgicc::td();	  
						    }
						    else {
						      for (int j=3;j>=0;j--) 
							*out << cgicc::td().set("bgcolor", "#F2F458") 
							     << std::setw(3) << "N/A" << cgicc::td();	  
						    }

						    *out << cgicc::tr();
						  }
						}


					}
					else { // not in config or no write lock
					  *out << cgicc::tr().set("bgcolor", in_config? "#F2F458" : "#CFCFCF")
					       << cgicc::td() <<  _fmmcrate->getFMM(i).getGeoSlot() << cgicc::td()
					       << cgicc::td( in_config?"Y":"N" )
					       << cgicc::td() <<  ( _fmmcrate->getFMM(i).haveWriteLock() ? "Y":"N" );
					  if ( in_config && (statename == "Ready" || statename == "Enabled") && !_fmmcrate->getFMM(i).haveWriteLock() ) {
					    *out << "(**)"; 
					    starstar = true;
					  }
					  *out << cgicc::td() << cgicc::td() <<  std::dec << _fmmcrate->getFMM(i).readHistoryAddress()  << cgicc::td()
					       << cgicc::td() <<  std::dec << _fmmcrate->getFMM(i).readTransitionMissCounter()  << cgicc::td()
					       << cgicc::td().set("colspan","20") << cgicc::td()
					       << cgicc::td("").set("bgcolor", "#FFFFFF")
					       << cgicc::td().set("colspan","4") << cgicc::td();
					  *out << cgicc::tr() << std::endl;
					}

				}
				*out << cgicc::table() << std::endl;
				*out << "<b>HotMask:</b> " << std::endl;
				*out << cgicc::input().set("type","submit").set("name", "bsubmit1").set("value", "Disable Checked Inputs")  << std::endl;
				*out << cgicc::input().set("type","submit").set("name", "bsubmit2").set("value", "Enable Checked Inputs")  << std::endl;
				*out << cgicc::form() << std::endl;


				*out << "<p>(*) dead time fractions indicate the average fraction of time spent in busy or warning state in the last second (when enabled)." << std::endl;
				if (starstar)
				  *out << "<p>(**) FMM is not being controlled since all inputs were disabled at configure time." << std::endl;

				*out << "<p><p>FMMs in the crate - version & configuration information as read from hardware" << std::endl;

				*out << cgicc::table().set("border","1") << std::endl;
				*out << cgicc::tr().set("bgcolor","#CCFFCC")
                                .add(cgicc::td("GeoSlot"))
				.add(cgicc::td("SN"))
				.add(cgicc::td("FWRev_Xilinx"))
				.add(cgicc::td("FWRev_Altera"))
				.add(cgicc::td("Dual"))
				.add(cgicc::td("InputEnableMask"))
				.add(cgicc::td("Thresh20"))
				.add(cgicc::td("Thresh10A"))
				.add(cgicc::td("Thresh10B"))
				<< std::endl;

				for (uint32_t i=0; i<_fmmcrate->numFMMs();i++) {

					bool in_config = false;
					xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
					for ( ; it != _config.end(); it++) {
						uint32_t geoslot = * dynamic_cast<xdata::UnsignedShort*> ( (*it).getField("geoslot") );
						if (geoslot != 999) {
							if (_fmmcrate->getFMM(i).getGeoSlot() == geoslot)
							{ in_config = true; break; }

						}
						else {
							if (_fmmcrate->getFMM(i).getSerialNumber() == (*it).getField("serialnumber")->toString()) 
							{ in_config = true; break; }
						}
					}

					*out << cgicc::tr().set("bgcolor", in_config? "#F2F458" : "#CFCFCF")
					<< cgicc::td() <<  _fmmcrate->getFMM(i).getGeoSlot() << cgicc::td()
					<< cgicc::td( _fmmcrate->getFMM(i).getSerialNumber() )
					<< cgicc::td( _fmmcrate->getFMM(i).readFirmwareRev() )
					<< cgicc::td( _fmmcrate->getFMM(i).readFirmwareRevAltera() )
					<< cgicc::td() << (_fmmcrate->getFMM(i).isDualMode()?"Y":"N") << cgicc::td()
					<< cgicc::td() << toolbox::toString("0x%05lx",~_fmmcrate->getFMM(i).getMask() & 0xfffff ) << cgicc::td()
					<< cgicc::td() << std::dec << _fmmcrate->getFMM(i).getThreshold20() << cgicc::td()
					<< cgicc::td() << std::dec << _fmmcrate->getFMM(i).getThreshold10A() << cgicc::td()
					<< cgicc::td() << std::dec << _fmmcrate->getFMM(i).getThreshold10B() << cgicc::td()
					<< cgicc::tr() << std::endl;
				};

				*out << cgicc::table() << std::endl;

				//
				// Display FMM Configuration
				// 
				*out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;");
				*out << cgicc::legend("FMM Controller Configuration from XDAQ parameters");

				*out << cgicc::table() << std::endl;
				*out << cgicc::tr()
				.add(cgicc::td("GeoSlot"))
				.add(cgicc::td("SN"))
				.add(cgicc::td("Dual"))
				.add(cgicc::td("InputEnableMask"))
				.add(cgicc::td("Thr20"))
				.add(cgicc::td("Thr10A"))
				.add(cgicc::td("Thr10B")) << std::endl;

				xdata::Vector<xdata::Bag<tts::FMMParameters> >::iterator it = _config.begin();
				for ( ; it != _config.end(); it++) {

					std::stringstream maskhex;
					maskhex << "0x" << std::hex << (* dynamic_cast<xdata::UnsignedInteger32*> ((*it).getField("inputEnableMask")));

					*out << cgicc::tr() 
					<< cgicc::td((*it).getField("geoslot")->toString())
					<< cgicc::td((*it).getField("serialnumber")->toString())
					<< cgicc::td((*it).getField("dual")->toString())
					<< cgicc::td(maskhex.str())
					<< cgicc::td((*it).getField("threshold20")->toString())
					<< cgicc::td((*it).getField("threshold10A")->toString())
					<< cgicc::td((*it).getField("threshold10B")->toString())
					<< cgicc::tr() << std::endl;
				};


				*out << cgicc::table() << std::endl;
				*out << cgicc::fieldset();


				*out << "<br>lastFormAction=" << lastFormAction << std::endl;

				*out << cgicc::body() << cgicc::html();

				*out << "<style>div { visibility : hidden; }</style>"; // ad blocker
			}
			catch (xcept::Exception &e) {
				XCEPT_RETHROW( xgi::exception::Exception, "cannot create web page", e);
			}
			catch (std::exception &e) {
				XCEPT_RAISE( xgi::exception::Exception, std::string("cannot create web page. reason:") + e.what());
			}

		}




		std::string tts::FMMWebInterface::formatTableEntries(std::vector<std::string> entries, 
				std::vector<tts::TTSState> states, 
				uint32_t mask, 
				bool in_config) 
		throw (xgi::exception::Exception) {

			std::stringstream ss;

			for (int i=states.size()-1;i>=0;i--) {
				if ( (mask & (1<<i)) || !in_config) 
					ss << cgicc::td().set("bgcolor", "#CFCFCF") 
					<< cgicc::span().set("style","font-style: italic;");
				else {
					switch (states[i]) {
					case tts::TTSState::READY: ss << cgicc::td(); break;
					case tts::TTSState::BUSY: ss << cgicc::td().set("bgcolor", "#ff3333"); break; // red
					case tts::TTSState::WARNING: ss << cgicc::td().set("bgcolor", "#ff6600"); break; // orange
					default : ss << cgicc::td().set("bgcolor", "#33ccff"); break; // light blue
					}
				}

				if ( i < (int) entries.size() )
					ss << entries[i];

				if (mask & (1<<i)) 
					ss << cgicc::span() << cgicc::td();
				else
					ss << cgicc::td();
			}

			return ss.str();
		}




		std::string tts::FMMWebInterface::formatStates(std::vector<tts::TTSState> states, uint32_t mask, bool in_config) 
		throw (xgi::exception::Exception) {

			std::vector<std::string> stateNames(states.size());
			for (uint32_t i=0; i<states.size(); ++i)
				stateNames[i] = states[i].getShortName();

			return formatTableEntries(stateNames, states, mask, in_config);
		}




		std::string tts::FMMWebInterface::formatLabelsSubRow(std::string const& label,
				std::string const& inputLabels,
				std::string const& outputLabels,
				std::vector<tts::TTSState> istates,
				std::vector<tts::TTSState> ostates,
				uint32_t mask, bool in_config,
				uint32_t igeoslot)
		
		throw (xgi::exception::Exception) {

			std::stringstream ss;

			ss << cgicc::td().set("colspan","4") <<  "TTCP: <b>" << label << "</b> FEDSrcId:" << cgicc::td();


			toolbox::StringTokenizer tokenizer( inputLabels, ";" );

			std::vector<std::string> ilabels;
			int iinput = 0;
			while ( tokenizer.hasMoreTokens() ) 
				ilabels.push_back( std::string("<b>") + tokenizer.nextToken() + "</b>" 
			
//			  +std::string("<form method=\"POST\" action=\"/" + this->getOwnerApplication()->getApplicationDescriptor()->getURN() + "/Default\">") 
			  +std::string("<input type=\"checkbox\" name=\"fmminput\" value=\"") + toolbox::toString("%d",(20* igeoslot) + (iinput++)) + std::string("\">")
//			  +std::string("<input type=\"submit\" value=\"ignore\">") 
	//		  +std::string("</form>") 
				
				);


			ss << formatTableEntries( ilabels, istates, mask, in_config );

			ss << cgicc::td("").set("bgcolor", "#FFFFFF");

			toolbox::StringTokenizer tokenizer1( outputLabels, ";" );

			std::vector<std::string> olabels;
			while ( tokenizer1.hasMoreTokens() ) 
				olabels.push_back( std::string("<b>") + tokenizer1.nextToken() + "</b>" );

			ss << formatTableEntries( olabels, ostates, 0, in_config );

			return ss.str();
		}




