package xdaq.xdesk.xcommander;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.tree.*;
import javax.swing.text.*;
import javax.infobus.*;
import java.net.*;
import org.w3c.dom.*;
import org.apache.log4j.*;
import xdaq.xdesk.xtree.*;
import xdaq.xdesk.tools.*;
//
// XPropViewer is the main application class that allows
// viewing XDAQ properties.

public class XCommanderViewer extends JPanel implements PartitionSelectorListener,
                                                   	XCommanderToolBarListener, 
							InfoBusSubscriberListener
{
	Logger logger = Logger.getLogger (XCommanderViewer.class);

	JSplitPane splitPane_;
	JSplitPane ioPane_;
	JEditorPane inputPane_;
	JEditorPane outputPane_;
	
	JPanel partitionNavigator_;
	JComboBox partitionComboBox_;
	JPanel dummyTreePanel_;
        XCommanderToolBar toolBar_;
	PartitionDefaultModel defaultModel_;
	PartitionSelector partitionSelector_;
	boolean waitingForReply_ = false;
	XCommander xcommander_;
	
	// Infobus stuff
	InfoBusPublisher commandPublisher;
	InfoBusSubscriber replySubscriber;
	JFrame frame_;
	
	HashMap appInterfaces_; // remember the commands for each application
		
	public XCommanderViewer(JFrame frame, XCommander xcommander)
	{	
		frame_ = frame;
		frame_.setTitle("XCommander");
		xcommander_ = xcommander;
		
		appInterfaces_ = new HashMap();
       		
		this.setLayout (new BorderLayout());	
		
		defaultModel_ = xcommander.getPartitionModel();	
		
		String [] filters = {"urlTransport", "Definitions", "Commands", "urlApplication", "Address", "DefaultParameters"}; 
		partitionSelector_ = new PartitionSelector(defaultModel_, filters);
		partitionSelector_.setPartitionSelectorListener(this);
				
		// Properties, right
		inputPane_ = new JEditorPane("text/plain", "");
		inputPane_.setEditorKit ( new StyledEditorKit() );
		inputPane_.setEditable(true);
		this.createKeyMapForEditor (inputPane_);
		
		outputPane_ = new JEditorPane("text/plain", "");
		outputPane_.setEditorKit ( new StyledEditorKit() );
		inputPane_.setEditable(true);
		this.createKeyMapForEditor (outputPane_);
		
		ioPane_ = new JSplitPane ( JSplitPane.VERTICAL_SPLIT,
						new JScrollPane(inputPane_), 
						new JScrollPane(outputPane_));
		
		
		// Split Pane
		splitPane_ = new JSplitPane (	JSplitPane.HORIZONTAL_SPLIT, 
						partitionSelector_, ioPane_ );
		splitPane_.setOneTouchExpandable(true);
		splitPane_.setContinuousLayout (true);
		
		
		Dimension minSize = new Dimension (200,200);

                // Create ToolBar
                toolBar_ =  new XCommanderToolBar();
                toolBar_.setToolBarListener(this);
                
                this.add(toolBar_,BorderLayout.NORTH);	
		this.add (splitPane_, BorderLayout.CENTER);
           
		
		// --------------------------------------------------
		// InfoBus attachment
		commandPublisher = new InfoBusPublisher ("xcontrolbus");
		replySubscriber = new InfoBusSubscriber ("xcontrolbus");
		commandPublisher.publishItem ("xcommand");
		replySubscriber.subscribeItem ("xreply", this );
		// --------------------------------------------------
	}
	
	public void createKeyMapForEditor (JEditorPane editor)
	{
		Keymap keymap = editor.addKeymap ("Editor Keys", editor.getKeymap());
		
		// Ctrl-c to copy text
		Action action = new DefaultEditorKit.CopyAction();
		KeyStroke key = KeyStroke.getKeyStroke (KeyEvent.VK_C, Event.CTRL_MASK);
		keymap.addActionForKeyStroke (key, action);
		
		// Ctrl-v to paste text
		action = new DefaultEditorKit.PasteAction();
		key = KeyStroke.getKeyStroke (KeyEvent.VK_V, Event.CTRL_MASK);
		keymap.addActionForKeyStroke (key, action);
		
		// Ctrl-x to cut text
		action = new DefaultEditorKit.CutAction();
		key = KeyStroke.getKeyStroke (KeyEvent.VK_X, Event.CTRL_MASK);
		keymap.addActionForKeyStroke (key, action);
	}
	
	public void setDividerLocation()
	{
		ioPane_.setDividerLocation (0.5); // 50% for each panel
		splitPane_.setDividerLocation (0.3); // 30% for tree size
	}
	
	
	JFrame getFrame()
	{
		return frame_;
	}
	
	public XCommander getController()
	{
		return xcommander_;
	}


	// InfoBusAdapter callback function
	//
	public synchronized void update (Object data, Object source)
	{
		// Check:
		// If somebody has pressed stop, the incoming reply is ignored.
		// For that purpose, we check if the buttonbar is greyed out.
		// if it is greyed out, the reply is accepted and the button bur is re-activated.
		
		if ( waitingForReply_ == false)
		{
			return;
		}
		
		waitingForReply_ = false;
		
			
		partitionSelector_.setEnabled(true);
		this.setEnabledComponent(splitPane_, true);
		
		MessageSet ms = (MessageSet) data;

		String replyStr = new String("");
		for (int i = 0; i < ms.size(); i++)
		{
			Message msg = ms.getMessage(i);
			Object sourceObj = msg.getSource();

			if (sourceObj == this)
			{				
				String status = msg.getStatus();
				if (status != null)
				{
					replyStr += msg.getUrl().toString() + ": " + status+"\n";
					System.out.println ("Error reply: " + replyStr);
				} else {
					this.processReply (msg);
				}
			}

		}
		
				
	}
	
	

	protected void processReply (Message msg)
	{
		String replyCommand = msg.getFunction() + "Response";
		org.w3c.dom.Document document = msg.getDOM();
		org.w3c.dom.NodeList nodeList = document.getElementsByTagName (replyCommand);
		if (nodeList.getLength() == 1)
		{
			
			if (replyCommand.equals ("InterfaceQueryResponse"))
			{
				NodeList parameterList = document.getElementsByTagName ("Method");
				Vector methodVector = new Vector();

				for (int j = 0; j < parameterList.getLength(); j++)
				{
					String value = parameterList.item(j).getFirstChild().getNodeValue();
					methodVector.addElement(value);
				}

				URL url = msg.getUrl();
				String urlString = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort() + url.getPath();
				System.out.println ("The URL is: " + urlString);
				appInterfaces_.put ( urlString, methodVector);
				this.selectionChanged();
				
			}
				
			// output reply into out panel
			String r = DOMToString.serialize ( document.getDocumentElement() );
			outputPane_.setText ( r );
			
		} else
		{
			System.out.println ("Invalid responde. Multiple Parameter nodes in reply.");
		}
	}
        
	
        public void selectionChanged ()
        {
		// set correct combo box model
		String partition = partitionSelector_.getSelectedPartition();
		frame_.setTitle("XCommander: "+partition);
		JTree tree = partitionSelector_.getJTree(partition);
		String url = MessageFactory.createURL (tree.getSelectionPath());
		Vector commands = (Vector) appInterfaces_.get (url);  
		DefaultComboBoxModel m;
		if (commands != null)
		{   
			m = new DefaultComboBoxModel(commands);			
			toolBar_.executeButtonEnable(true);
			toolBar_.queryButtonEnable(true);
		} else {
			m = new DefaultComboBoxModel();
			toolBar_.queryButtonEnable(true);
			toolBar_.executeButtonEnable(false);			
		}
		
		toolBar_.stopButtonEnable(false);
		toolBar_.setCommandModel(m);
				
		//splitPane_.invalidate();
		//splitPane_.revalidate();
		splitPane_.setVisible(true);
        }
	
	public void refresh()
	{
		splitPane_.setVisible(true);
	}
	
        
        
        // event generated bx ToolBar
        public synchronized void buttonPressed(String buttonName)
        {
             // here I perfrom the action on the current displayed properties editor if any
	      
		if (buttonName.equals ("execute"))
		{
			toolBar_.executeButtonEnable(false); // disactivate toolbar
			toolBar_.queryButtonEnable(false); 
			toolBar_.stopButtonEnable(true); // activate stop button
			this.setEnabledComponent(splitPane_, false); // grey out panel
			waitingForReply_ = true;
			
			JTree tree = partitionSelector_.getJTree(partitionSelector_.getSelectedPartition());
			String command = toolBar_.getSelectedCommand();
			
			
			MessageSet ms = MessageFactory.createMessageSet (
				tree.getSelectionModel().getSelectionPaths(), 
				command, this);
			
			// Add XML data in input panel to command body	
			String parameterText = inputPane_.getText();
			
			if ( (parameterText != null) && (!parameterText.equals("")) )
			{ // if nothing to send just ignore it
				org.w3c.dom.Document doc  = StringToDOM.dom(parameterText);
				if ( doc == null ) 
				{
					logger.error ("Malformed XML in input panel.");
					JOptionPane.showMessageDialog(null,"Malformed xml tag", "Parameter error" ,JOptionPane.ERROR_MESSAGE);
					return;
				}
				Node node = doc.getDocumentElement();
				for (int i = 0; i < ms.size(); i++)
				{
					ms.getMessage(i).setData ( node );
				}
			}
							
			commandPublisher.fireItemValueChanged ("xcommand", ms);
		} 
	      	else if (buttonName.equals ("query"))
		{
			// ParameterQuery: buttonName: "query"
			
			toolBar_.executeButtonEnable(false);
  			toolBar_.queryButtonEnable(false); 
			toolBar_.stopButtonEnable(true); // activate stop button
			this.setEnabledComponent(splitPane_, false); // grey out panel
			waitingForReply_ = true;
			
			JTree tree = partitionSelector_.getJTree(partitionSelector_.getSelectedPartition());
			
			MessageSet ms = MessageFactory.createMessageSet (
				tree.getSelectionModel().getSelectionPaths(), 
				"InterfaceQuery", this);
							
			commandPublisher.fireItemValueChanged ("xcommand", ms);
		}
		else if (buttonName.equals ("stop"))
		{		
			if (toolBar_.hasCommands() )	
				toolBar_.executeButtonEnable(true);
			else
				toolBar_.executeButtonEnable(false);	
				
  			toolBar_.queryButtonEnable(true); 
			toolBar_.stopButtonEnable(false);

			// prepare call her			
			partitionSelector_.setEnabled(true);
			setEnabledComponent(splitPane_, true);
			waitingForReply_ = false;
		}
	      
	
        }
	
	public void setEnabledComponent ( Container c, boolean enabled )
	{
		Component[] list = c.getComponents();
		
		for (int i = 0; i < list.length; i++)
		{
			Container cont = (Container) list[i];
			cont.setEnabled(enabled);
			setEnabledComponent ( cont, enabled );
		}
	}
	
	
	void shutdown()
	{
		try {
			commandPublisher.shutdown();
			replySubscriber.shutdown();
		} catch (Exception e)
		{
			logger.error (e.toString());
		}
	}
	

}

