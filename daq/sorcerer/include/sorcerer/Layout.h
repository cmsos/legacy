// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini                                                     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _sorcerer_Layout_h_
#define _sorcerer_Layout_h_
#include <node.h>
#include "xgi/exception/Exception.h"
#include "xgi/Output.h"
#include "xgi/Input.h"


#include "xgi/framework/AbstractLayout.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/exception/Exception.h"

using v8::Persistent;
using v8::Function;

namespace sorcerer
{
		class UIManager;

		class Layout: public xgi::framework::AbstractLayout
		{

			public:

				Layout ();

				void getHTMLHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out);
				void getHTMLFooter (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out);
				void Default (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out);
				void noCallbackFound (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out);
                void errorPage (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out, xcept::Exception& e);


			protected:

				void resetCGICC();

			public:
				Persistent< Function > transmute_;
		};
}

#endif

