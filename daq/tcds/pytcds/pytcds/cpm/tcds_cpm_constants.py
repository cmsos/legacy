# This is the overall directory for all address table files.
from pytcds.utils.tcds_constants import ADDRESS_TABLE_DIR

# The uhal address table for the CPMT1.
ADDRESS_TABLE_FILE_NAME_CPMT1 = "%s/%s" % \
                                (ADDRESS_TABLE_DIR,
                                 "device_address_table_amc13_cpmt1.xml")

# The uhal address table for the CPMT2.
ADDRESS_TABLE_FILE_NAME_CPMT2 = "%s/%s" % \
                                (ADDRESS_TABLE_DIR,
                                 "device_address_table_amc13_cpmt2.xml")
