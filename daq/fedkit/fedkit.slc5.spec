# SLC5 version of the spec file (seems to be identical to SLC4
# but for the moment still kept separate). Should check whether
# we actually need a spec file (other packages don't but myrics does).

%{!?kernel:       %define kernel       %(uname -r)}
%{!?kernel_short: %define kernel_short %(echo %{kernel} | sed -e 's/smp$//')}
%{!?karch:        %define karch        %(uname -m)}
%{!?ksrc:         %define ksrc         /lib/modules/%{kernel}/build}
%{!?ksmp:         %define ksmp         %(if ( echo %{kernel} | grep -q smp ); then echo "smp"; fi;)}
%{!?ksmp_hyphen:  %define ksmp_hyphen  %(if ( echo %{kernel} | grep -q smp ); then echo "-smp"; fi;)}
%{!?ksmp_type:    %define ksmp_type    %(if ( echo %{kernel} | grep -q smp ); then echo "SMP"; else echo "UP"; fi;)}
%{!?kver:         %define kver         %( echo %{kernel} | perl -p -e 's/^([^-]*)-.*$/\1/' )}
%{!?kcfg:         %define kcfg         %{ksrc}/configs/kernel-%{kver}-%{karch}%{ksmp}.config}
%{!?kmajor:       %define kmajor       123}
%{!?version:      %define version      1.0.0}
%{!?release:      %define release      1}

Name: daq-fedkit
Summary: Fedkit PCI cards' library, header files and test programs. 
Version: %{version}
Release: %{release}
License: GPL
Group: CMS/System
Vendor: Eric Cano <Eric.Cano@cern.ch>, CERN-PH/CMD
Packager: Eric Cano <Eric.Cano@cern.ch>, CERN-PH/CMD
URL: http://cern.ch/cano/fedkit
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
ExclusiveOS: linux
BuildPreReq: kernel%{ksmp_hyphen}-devel = %{kernel_short}

%description
Libraries for using fedkit, plus header files and test programs (fedkit_test_merge, fedkit_link_dump)

%package -n kernel-module-daq-fedkit-%{kernel}
Summary:  Fedkit's kernel module
Requires: kernel%{ksmp_hyphen} = %{kernel_short}
Group:    CMS/System

%description -n kernel-module-daq-fedkit-%{kernel}
This sub-package contains the kernel module for the fedkit library

%package -n daq-fedkit-devel
Summary:  Generic PCI access's development RPM
Group:    CMS/System
Requires: daq-fedkit

%description -n daq-fedkit-devel
This sub-package contains the include files for the fedkit library

%prep
%setup -c 

%build
cd fedkit-%{version}
make KERNEL_VERSION=%{kernel} KERNEL_TYPE=%{ksmp_type} MAJOR_NUMBER=%{kmajor}

%install
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT
cd fedkit-%{version}
mkdir -p $RPM_BUILD_ROOT{/lib/modules/%{kernel}{,smp}/extra,/usr/{include/fedkit,share/fedkit-%{version}},%{_prefix}/{lib,bin},/dev,/etc/init.d}
install -m 755 driver/fedkit.ko $RPM_BUILD_ROOT/lib/modules/%{kernel}/extra
install -m 755 libfedkit.so libfedkit.a $RPM_BUILD_ROOT/usr/lib/
install -m 644 include/*.h $RPM_BUILD_ROOT/usr/include/fedkit/
install -m 755 fedkit-dump-receiver fedkit-dump-receiver-check fedkit-test-merge $RPM_BUILD_ROOT/usr/bin
install -m 755 fedkit.init $RPM_BUILD_ROOT/etc/init.d/fedkit
install -m 644 fedkit-example.c $RPM_BUILD_ROOT/usr/share/fedkit-%{version}/

mkdir -p $RPM_BUILD_ROOT/usr/share/hwdata/pci.ids.d/
install -m 644 driver/cms-daq-fedkit.ids $RPM_BUILD_ROOT/usr/share/hwdata/pci.ids.d/

%clean
[ $RPM_BUILD_ROOT != / ] && rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/etc/init.d/*
%{_prefix}/lib/*
%{_prefix}/bin/*
%{_prefix}/share/hwdata/pci.ids.d/cms-daq-fedkit.ids

%files -n daq-fedkit-devel
%defattr(-,root,root,-)
/usr/include/*
/usr/share/fedkit-%{version}/*

%files -n kernel-module-daq-fedkit-%{kernel}
%defattr(-,root,root,-)
/lib/modules/%{kernel}/extra/*

%post
/sbin/chkconfig --add fedkit
/sbin/ldconfig

%preun
if [ "$1" = "0" ] ; then # last uninstall
  exists=`/sbin/chkconfig --list | grep fedkit`
  if [ x"$exists" != x ] ; then
  /sbin/service fedkit stop > /dev/null 2>&1
  /sbin/chkconfig --del fedkit  > /dev/null 2>&1
  fi
fi

%post -n kernel-module-daq-fedkit-%{kernel}
/sbin/depmod -ae %{kernel}

%postun -n kernel-module-daq-fedkit-%{kernel}
/sbin/depmod -ae %{kernel}

%changelog
* Thu Apr  8 2010 Eric CANO <cano@srv-c2c04-30.cms> - 
- Made fedkit packaging for SLC4

* Tue Nov 22 2005 Eric Cano. <cano@pccmsdaqs2.cern.ch> 1.0-0
- Initial build, RPM packaging and independant build from older sub-project.
