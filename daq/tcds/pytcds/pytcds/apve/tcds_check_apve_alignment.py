#!/usr/bin/env python

###############################################################################
## A little script to check the alignment of the simulated pipeline
## history between different APVEs and the CPM.
###############################################################################

# NOTE: This is a bit quick-n-dirty.

import json
import os
import sys
import urllib2

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Checker(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Checker, self).__init__(description, usage, epilog)
        # End of pre_hook().

    def handle_args(self):
        # Expect at least two targets on the command line.
        if len(self.args) < 2:
            self.error("At least two APVE(-like) targets were expected.")
        self.targets = self.args
        # End of handle_args().

    def main(self):

        targets = self.targets

        data = {}
        for target in targets:
            # Check if the target name is valid.
            self.validate_target(target, target_type=CmdLineBase.TARGET_TYPE_SW)

            target_info = self.setup.sw_targets[target]
            url = "{0:s}/update".format(target_info.url)

            req = urllib2.Request(url)
            opener = urllib2.build_opener()
            f = opener.open(req)
            contents = json.load(f)
            history_contents =  contents['itemset-simhist']['Simulated APV pipeline history']

            if history_contents == "-":
                msg = "No data from '{0:s}'." \
                      " The application appears to be halted."
                print msg.format(target)
            else:
                data[target] = history_contents

        # A little bit of data reformatting.
        data_tmp = {}
        for (key, hist) in data.iteritems():
            # NOTE: Event number 0 does not exist! (A zero here means
            # 'empty entry.')
            tmp0 = [int(i['EventNumber']) for i in hist if int(i['EventNumber']) != 0]
            tmp1 = [i for i in hist if int(i['EventNumber']) != 0]
            hist_tmp = dict(zip(tmp0, tmp1))
            data_tmp[key] = hist_tmp
        data = data_tmp

        if not len(data):
            print "No data at all..."
        else:
            # Find out the total list of event numbers we need to span.
            tmp_numbers = []
            for tmp in data.values():
                tmp_numbers.extend(tmp.keys())

            event_numbers = list(set(tmp_numbers))

            # Print table header.
            targets = sorted(data.keys())
            print "Event number {0:s}".format(" ".join(["{0:>10s}".format(i) for i in targets]))

            # Print table data.
            for event_number in event_numbers[::-1]:
                vals = []
                for target in targets:
                    try:
                        val = int(data[target][event_number]['PipelineAddress'])
                    except KeyError:
                        val = None
                    vals.append(val)
                tmp = " ".join(["{0:10d}".format(i) \
                                if not i is None \
                                else "{0:>10s}".format("-") \
                                for i in vals])
                print "{0:12d} {1:s}".format(event_number, tmp)

        # End of main().

    # End of class Checker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to check the alignment " \
               "of the simulated pipeline history " \
               "between different APVEs and the CPM."
    usage_str = "usage: %prog [options] APVE1 APVE2 ... APVEn"

    res = Checker(desc_str, usage_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
