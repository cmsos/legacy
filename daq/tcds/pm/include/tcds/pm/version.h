#ifndef _tcdspm_version_h_
#define _tcdspm_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSPM_VERSION_MAJOR 3
#define TCDSPM_VERSION_MINOR 13
#define TCDSPM_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSPM_PREVIOUS_VERSIONS "2.7.0"
// else:
#undef TCDSPM_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSPM_VERSION_CODE PACKAGE_VERSION_CODE(TCDSPM_VERSION_MAJOR,TCDSPM_VERSION_MINOR,TCDSPM_VERSION_PATCH)
#ifndef TCDSPM_PREVIOUS_VERSIONS
#define TCDSPM_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSPM_VERSION_MAJOR,TCDSPM_VERSION_MINOR,TCDSPM_VERSION_PATCH)
#else
#define TCDSPM_FULL_VERSION_LIST TCDSPM_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSPM_VERSION_MAJOR,TCDSPM_VERSION_MINOR,TCDSPM_VERSION_PATCH)
#endif

namespace tcdspm
{
  const std::string package = "tcdspm";
  const std::string versions = TCDSPM_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS CPM and LPM.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS CPM and LPM.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
