// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius                                     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include "pt/blit/PipeAdvertisement.h"

pt::blit::PipeAdvertisement::PipeAdvertisement( tcpla::PublicServicePoint * psp): psp_(psp)
{

}

tcpla::PublicServicePoint *  pt::blit::PipeAdvertisement::getPSP()
{
	return psp_;
}

int pt::blit::PipeAdvertisement::getIndex()
{
	return psp_->indexId_;
}

std::string pt::blit::PipeAdvertisement::toString()
{
	return psp_->toString();
}

