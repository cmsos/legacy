package xdaq.xdesk.jtreeeditor;

public class JTreeNotEditableText {

  private String text;

//******************************************************************************
   public JTreeNotEditableText(String str) {
     text = str;
   }

//******************************************************************************
   public String toString() {
     return text;
   }
}
