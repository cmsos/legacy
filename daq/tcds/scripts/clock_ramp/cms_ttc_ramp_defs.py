#!/usr/bin/env python

######################################################################
## File       : cms_ttc_ramp_defs.py
## Author     : Jeroen Hegeman
##              jeroen.hegeman@cern.ch
## Last change: 20111101
##
## Purpose :
##   This is the library of clock ramp definitions used by
##   cms_ttc_clock_ramper.py
##
##   The reference used for the LHC proton and heavy-ion ramps is a
##   presentation by Sophie Baron:
##   http://indico.cern.ch/conferenceDisplay.py?confId=12273
######################################################################

######################################################################
## NOTES: - Ramps in the code in principle have a precision of better
## than a second for each step. The same holds for the frequency
## values. In the database we only store values with one second and
## one Hz precision. This is Python timing anyway, and one-second
## precision should be good enough for any TTC ramping purposes.
######################################################################

######################################################################
## TODO:
## - Wrap an accessor around the _ramp_info to access frequencies and
##   seconds independently.
## - Allow deletion of part of the ramp sequence.
######################################################################

import copy
import math

# Debugging stuff.
try:
    import debug_hook
    import pdb
except ImportError:
    pass

######################################################################
## Module globals.
######################################################################

# These are global bunch crossing frequencies for LHC running. For the
# heavy ion frequencies the source is a presentation given by Sophie
# Baron:
#   http://indico.cern.ch/materialDisplay.py?materialId=slides&confId=12273
# For the proton frequencies the source is an update email from Sophie
# after the November 7/8/9 2009 LHC RF tests.

# LHC proton ramp starting frequency (Hz).
freq_lhc_proton_lo = 40078896.3
# LHC proton ramp final frequency (Hz).
freq_lhc_proton_hi = 40078971.5

# LHC heavy-ion ramp starting frequency (Hz).
freq_lhc_heavy_ion_lo = 40078413.9
# LHC heavy-ion ramp final frequency (Hz).
freq_lhc_heavy_ion_hi = 40078971.5

# According to the QPLL manual
# (http://proj-qpll.web.cern.ch/proj-qpll/images/qpllManual.pdf) the
# locking range is 'about +/- 3.7 KHz around f = 40.0786 MHz.'
qpll_freq_center = 40078600
qpll_freq_delta = 3700
qpll_freq_lo = qpll_freq_center - qpll_freq_delta
qpll_freq_hi = qpll_freq_center + qpll_freq_delta

######################################################################
## RampInfo class.
######################################################################

class RampInfo(object):
    """Simple class to gather all information specifying a ramp.

    Combines all information necessary to document a clock ramp
    together with a descriptive name and some utility methods.

    """

    def __init__(self, name, description):

        # A name and a description to help us remember what kind of
        # ramp this is.
        self._name = name
        self._desc = description

        # A flag that can be set if the original ramp configuration
        # has been changed somehow (e.g. inverted).
        self._is_modified = False

        # This variable holds the real ramp information as an array of
        # tuples: (time after start in seconds, frequency in Hertz).
        self._ramp_info = None
        # And if we modify the ramp in any way we'll keep a copy of
        # the original ramp setting in this variable.
        self._ramp_info_ori = None

        # End of __init__.

    ##########

    def __str__(self):

        # Provide a user-friendly way to look at the ramp information.

        string_lines = ["Ramp '%s': %s" % (self._name, self._desc)]
        if self._is_modified:
            string_lines.append("!!! Ramp has been modified " \
                                "--> not original !!!")
        if len(self._ramp_info) < 1:
            string_lines.append("  !!! Empty ramp !!!")
        else:
            string_lines.append("  -> %d steps over:" % \
                                (len(self._ramp_info) - 1))
            string_lines.append("     -> %d seconds " \
                                "(%.1f minutes)" % \
                                (self._ramp_info[-1][0],
                                 self._ramp_info[-1][0] / 60.))
            delta_freq = self._ramp_info[-1][1] - \
                         self._ramp_info[0][1]
            step_dir = "up"
            if delta_freq < 0:
                step_dir = "down"
            string_lines.append("     -> %d Hz (stepping %s)" % \
                                (abs(delta_freq), step_dir))
            string_lines.append("  -> starting frequency: %.1f Hz" % \
                                self._ramp_info[0][1])
            string_lines.append("  -> central frequency : %.1f Hz" % \
                                (.5 * (self._ramp_info[0][1] + \
                                       self._ramp_info[-1][1])))
            string_lines.append("  -> final frequency   : %.1f Hz" % \
                                self._ramp_info[-1][1])

            values_t = [i[0] for i in self._ramp_info]
            deltas_t = [(values_t[i + 1] - values_t[i]) \
                        for i in xrange(len(values_t) - 1)]
            if len(values_t) > 0 and \
                   len(deltas_t) > 0:
                min_delta_t = min(deltas_t)
                max_delta_t = max(deltas_t)
                values_f = [i[1] for i in self._ramp_info]
                deltas_f = [(values_f[i + 1] - values_f[i]) \
                            for i in xrange(len(values_f) - 1)]
                min_delta_f = min(deltas_f)
                max_delta_f = max(deltas_f)
                string_lines.append("  -> min delta-t: %.2f s" % min_delta_t)
                string_lines.append("  -> max delta-t: %.2f s" % max_delta_t)
                string_lines.append("  -> min delta-f: %.2f Hz" % min_delta_f)
                string_lines.append("  -> max delta-f: %.2f Hz" % max_delta_f)

        # End of __str__.
        return "\n".join(string_lines)

    ##########

    def dump(self, verbose=False):

        string_lines = [self.__str__()]

        if len(self._ramp_info) > 0:
            string_lines.append("-" * 80)
            string_lines.append("  step # | " \
                                "seconds since start | " \
                                "frequency (Hz) | " \
                                "delta-t (s) | " \
                                "  delta-f (Hz)")
            string_lines.append("-" * 80)
            string_lines.extend(["    %4d |             " \
                                 "%7.2f |     " \
                                 "%9.1f |   " \
                                 "       -- |   " \
                                 "          --" % \
                                 (index, i[0], i[1]) \
                                     for (index, i) \
                                     in enumerate([self._ramp_info[0]])])
            string_lines.extend(["    %4d |             " \
                                 "%7.2f |     " \
                                 "%9.1f |  " \
                                 "%10.2f |   " \
                                 "%12.1f" % \
                                 (index + 1, i[0], i[1],
                                  (self._ramp_info[index + 1][0] - \
                                   self._ramp_info[index][0]),
                                  (self._ramp_info[index + 1][1] - \
                                   self._ramp_info[index][1])) \
                                 for (index, i) \
                                 in enumerate(self._ramp_info[1:])])

        print "\n".join(string_lines)

    ##########

    def validate(self):
        """Perform a sanity check on the ramp.

        Things checked:
        - Strictly increasing time values.
        - Time values should start at zero.

        If all is well True is returned (together with None for the
        message). If any of the above does not check out False is
        returned together with a descriptive failure message.

        NOTE: The first failure is reported, further checks are not
        performed.

        NOTE: Two points with the same frequency value can exist,
        either directly after each other or separated by any number of
        points.

        """

        valid = True
        msg = None

        # Check for strictly increasing time values
        if valid:
            tmp_times0 = [i for (i, j) in self._ramp_info]
            tmp_times1 = list(set(tmp_times0))
            tmp_times1.sort()
            if tmp_times1 != tmp_times0:
                valid = False
                msg = "Time values should be strictly increasing."

        # Check for a zero starting time.
        if valid:
            if self._ramp_info[0][0] != 0:
                valid = False
                msg = "The first time value for a ramp should be zero."

        # End of validate.
        return (valid, msg)

    ##########

    def sanitize(self, min_delta_t=0.2, min_delta_f=0.1):
        """Clean up the ramp a little."""

        # Remember to set the modification flag.
        self._set_modified()

        (sec_lo, freq_lo) = self._ramp_info[0]
        ramp_info_sane = [(sec_lo, freq_lo)]

        for (sec, freq) in self._ramp_info[1:]:
            delta_t = sec - sec_lo
            delta_f = freq - freq_lo

            if (delta_t >= min_delta_t) and \
               (delta_f >= min_delta_f):
                sec_lo = sec
                freq_lo = freq
                ramp_info_sane.append((sec, freq))

        self._ramp_info = ramp_info_sane

         # End of sanitize.

    ##########

    def _set_modified(self):

        # If _ramp_info_ori is None this is the first modification. In
        # that case we have to store the original info first.
        if self._ramp_info_ori is None:
            self._ramp_info_ori = copy.copy(self._ramp_info)
            self._is_modified = True

        # End of _set_modified.

    ##########

    def reset(self):

        # If we did not modify anything there's nothing to undo,
        # otherwise restore the original ramping information and reset
        # the modification flag.
        if not self._ramp_info_ori is None:
            # BUG BUG BUG
            # This needs to be checked to see if this really works!
            self._ramp_info = self._ramp_info_ori
            self._ramp_info_ori = None
            # BUG BUG BUG end
            self._is_modified = False

        # End of reset.

    ##########

    def invert(self):
        "Turn this ramp around, literally."

        # Remember to set the modification flag.
        self._set_modified()

        # Extract the frequencies
        tmp_freqs = [i[1] for i in self._ramp_info]
        # and turn them around.
        tmp_freqs.reverse()

        # Extract the time steps
        tmp_times = [i[0] for i in self._ramp_info]
        # turn them around and move the start back to zero.
        tmp_times = [(tmp_times[i + 1] - tmp_times[i]) \
                     for i in range(len(tmp_times) - 1)]
        tmp_times.append(0)
        tmp_times.reverse()
        tmp_sum = 0
        for (i, tmp) in enumerate(tmp_times):
            tmp_sum += tmp
            tmp_times[i] = tmp_sum

        # Store back the result.
        self._ramp_info = zip(tmp_times, tmp_freqs)

        # End of invert.

    ##########

    def interpolate(self, npoints=1):
        """Add one point between each two points of the ramp.

        Interpolate the current ramp. I.e. add npoints points
        (linearly interpolated in both time and frequency) between
        each two points of the current ramp.

        """

        # Remember to set the modification flag.
        self._set_modified()

        # Maybe this could be done more efficiently, but here goes.
        new_ramp_info = []
        for i in xrange(len(self._ramp_info) - 1):
            (sec0, freq0) = self._ramp_info[i]
            (sec1, freq1) = self._ramp_info[i + 1]

            # BUG BUG BUG
            # This needs to be improved a bit as soon as I have the
            # ramp validity checker in place.

            # Two equal values (be they time or frequency) we can
            # handle: just use the same value for the middle point as
            # well.
            secs_new = [sec0 + i * (1. * sec1 - sec0) / (npoints + 1) \
                        for i in xrange(1, npoints + 1)]
            freqs_new = [freq0 + i * (1. * freq1 - freq0) / (npoints + 1) \
                         for i in xrange(1, npoints + 1)]

            # Add the original point to the new sequence.
            new_ramp_info.append((sec0, freq0))
            # Add the new points (the ones following the original
            # point) to the new sequence.
            new_ramp_info.extend([(i, j) for (i, j) in \
                                  zip(secs_new, freqs_new)])

            # BUG BUG BUG end
        # Don't forget the last point.
        new_ramp_info.append(self._ramp_info[-1])
        self._ramp_info = new_ramp_info

        # End of interpolate.

    ##########

    def shift(self, freq_shift):
        """Move the current ramp up or down in frequency.

        Of course moving things in time does not do anything.

        """

        new_ramp_info = [(i, (j + freq_shift)) \
                         for (i, j) in self._ramp_info]

        self._ramp_info = new_ramp_info

        # Remember to set the modification flag.
        self._set_modified()

        # End of shift.

    ##########

    def scale(self, scale_axis, scale_factor):
        """Scale current ramp along either scale_axis by scale_factor.

        NOTE: In the case of frequency scaling the ramp is scaled
        around its central value. This implementation is a little bit
        sloppy but should be good enough for our TTC ramping purposes.

        """

        axis = scale_axis.lower()

        if axis == "time":
            # Since the time values start at zero (at least: they
            # should) we can just scale things in this case.
            # DEBUG DEBUG DEBUG
            assert self._ramp_info[0][0] == 0
            # DEBUG DEBUG DEBUG end
            new_ramp_info = [(scale_factor * i, j) \
                             for (i, j) in self._ramp_info]
        elif axis in ["freq", "frequency"]:
            # For the frequency scaling the ramp is scaled around its
            # central frequency.
            central_freq = .5 * (self._ramp_info[0][1] + \
                                 self._ramp_info[-1][1])
            # First move things down to around zero (i.e. subtract the
            # central frequency).
            tmp = [(i, (j - central_freq)) \
                   for (i, j) in self._ramp_info]
            # Then scale around zero.
            tmp = [(i, (scale_factor * j)) for (i, j) in tmp]
            # Finally move back up to around the central frequency.
            new_ramp_info = [(i, j + central_freq) \
                             for (i, j) in tmp]
        else:
            raise ValueError, "No such ramp axis: '%s'" % \
                  scale_axis

        self._ramp_info = new_ramp_info

        # End of scale.

    ##########

    # End of class RampInfo.

######################################################################
## RampTest class.
######################################################################

class RampTest(RampInfo):
    "This is a very simple test ramp. Only useful for debugging."

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Test ramp for debugging."

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [
            ( 0., 40078879.),
            ( 1., 40078879.1),
            ( 1.2, 40078879.12),
            ( 1.4, 40078879.14),
            ( 1.6, 40078879.16),
            ( 1.8, 40078879.18),
            ( 2., 40078879.2),
            ( 3., 40078879.3),
            ( 4., 40078879.4),
            ( 5., 40078901.),
            (10., 40078923.),
            (15., 40078945.),
            (20., 40078966.)]

    # End of class RampTest.

######################################################################
## RampLHCProton2008 class.
######################################################################

class RampLHCProton2008(RampInfo):
    """This is an LHC proton ramp.

    The frequencies and time steps were measured some time in 2008
    during an LHC ramp test. The starting and final frequencies match
    with the nominal ones presented by Sophie Baron here:
    http://indico.cern.ch/conferenceDisplay.py?confId=12273 (slide
    35). The measured times correspond to a ramping time of 15
    minutes, the real LHC ramps will take 28 minutes.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "LHC proton ramp measured in 2008."

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [
            (  0., 40078879.),
            ( 30., 40078880.),
            ( 47., 40078881.),
            ( 57., 40078882.),
            ( 66., 40078883.),
            ( 72., 40078884.),
            ( 78., 40078885.),
            ( 85., 40078886.),
            ( 91., 40078887.),
            ( 98., 40078888.),
            (102., 40078889.),
            (113., 40078891.),
            (124., 40078893.),
            (134., 40078895.),
            (144., 40078897.),
            (154., 40078899.),
            (164., 40078902.),
            (174., 40078904.),
            (178., 40078905.),
            (188., 40078907.),
            (197., 40078909.),
            (204., 40078911.),
            (214., 40078913.),
            (227., 40078916.),
            (242., 40078919.),
            (256., 40078922.),
            (268., 40078924.),
            (279., 40078926.),
            (294., 40078929.),
            (310., 40078932.),
            (322., 40078934.),
            (340., 40078937.),
            (353., 40078939.),
            (369., 40078941.),
            (384., 40078943.),
            (409., 40078946.),
            (429., 40078948.),
            (453., 40078950.),
            (483., 40078952.),
            (509., 40078954.),
            (537., 40078956.),
            (577., 40078958.),
            (628., 40078960.),
            (677., 40078962.),
            (775., 40078964.),
            (934., 40078966.)]

    # End of class RampLHCProton2008.

######################################################################
## RampLHCProton20091015 class.
######################################################################

class RampLHCProton20091015(RampInfo):
    """This is the LHC proton ramp measured on October 15, 2009.

    This ramp was measured using the CMS frequency counter at P5
    during an RF test on 2009-10-15. The data was extracted from the
    CMS monitoring database using the following SQL query:

      select to_char(time, 'YYYY-MM-DD HH24:MI:SS'), frequency from mon_freqmeter where (time >= to_date('2009-10-15 15:15:30', 'YYYY-MM-DD HH24:MI:SS')) and (time <= to_date('2009-10-15 15:40:07', 'YYYY-MM-DD HH24:MI:SS')) order by time;

    All frequencies were corrected for the CMS counter offset of 218
    Hz to obtain the values stored below.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "LHC proton ramp measured October 15, 2009."

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [
            (   0., 40078879.),
            ( 188., 40078881.),
            ( 200., 40078883.),
            ( 221., 40078885.),
            ( 232., 40078887.),
            ( 244., 40078889.),
            ( 264., 40078892.),
            ( 275., 40078894.),
            ( 284., 40078897.),
            ( 304., 40078900.),
            ( 320., 40078904.),
            ( 342., 40078909.),
            ( 360., 40078913.),
            ( 375., 40078916.),
            ( 388., 40078919.),
            ( 401., 40078921.),
            ( 416., 40078924.),
            ( 429., 40078926.),
            ( 442., 40078929.),
            ( 456., 40078931.),
            ( 477., 40078934.),
            ( 497., 40078937.),
            ( 517., 40078940.),
            ( 538., 40078942.),
            ( 551., 40078944.),
            ( 578., 40078946.),
            ( 593., 40078948.),
            ( 619., 40078950.),
            ( 645., 40078952.),
            ( 675., 40078954.),
            ( 715., 40078956.),
            ( 755., 40078958.),
            ( 804., 40078960.),
            ( 883., 40078962.),
            (1061., 40078964.),
            (1367., 40078964.),
            (1476., 40078966.)
            ]

    # End of class RampLHCProton20091015.

######################################################################
## RampLHCIon class.
######################################################################

class RampLHCIon20091015(RampInfo):
    """This is the LHC heavy ion ramp.

    This ramp was measured using the CMS frequency counter at P5
    during an RF test on 2009-10-15. The data was extracted from the
    CMS monitoring database using the following SQL query:

      select to_char(time, 'YYYY-MM-DD HH24:MI:SS'), frequency from mon_freqmeter where (time >= to_date('2009-10-16 09:59:11', 'YYYY-MM-DD HH24:MI:SS')) and (time <= to_date('2009-10-16 10:46:45', 'YYYY-MM-DD HH24:MI:SS')) order by time;

    All frequencies were corrected for the CMS counter offset of 218
    Hz to obtain the values stored below.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "LHC heavy ion ramp measured October 15, 2009."

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [
            (   0., 40078418.),
            ( 336., 40078419.),
            ( 639., 40078424.),
            ( 726., 40078494.),
            ( 740., 40078510.),
            ( 741., 40078513.),
            ( 771., 40078552.),
            ( 773., 40078555.),
            ( 775., 40078557.),
            ( 777., 40078560.),
            ( 780., 40078563.),
            ( 781., 40078565.),
            ( 782., 40078567.),
            ( 784., 40078569.),
            ( 787., 40078573.),
            ( 789., 40078576.),
            ( 791., 40078579.),
            ( 793., 40078581.),
            ( 794., 40078583.),
            ( 824., 40078624.),
            ( 826., 40078626.),
            ( 867., 40078680.),
            ( 870., 40078683.),
            ( 871., 40078685.),
            ( 873., 40078687.),
            ( 875., 40078689.),
            ( 876., 40078691.),
            ( 878., 40078693.),
            ( 942., 40078763.),
            ( 945., 40078766.),
            ( 948., 40078768.),
            ( 950., 40078770.),
            ( 967., 40078785.),
            ( 979., 40078796.),
            ( 990., 40078804.),
            (1001., 40078812.),
            (1009., 40078818.),
            (1035., 40078836.),
            (1038., 40078838.),
            (1064., 40078853.),
            (1079., 40078861.),
            (1104., 40078872.),
            (1119., 40078879.),
            (1132., 40078884.),
            (1142., 40078888.),
            (1154., 40078893.),
            (1173., 40078899.),
            (1183., 40078902.),
            (1205., 40078908.),
            (1223., 40078913.),
            (1238., 40078917.),
            (1251., 40078919.),
            (1264., 40078923.),
            (1278., 40078925.),
            (1291., 40078928.),
            (1306., 40078930.),
            (1317., 40078932.),
            (1327., 40078934.),
            (1347., 40078936.),
            (1357., 40078938.),
            (1376., 40078941.),
            (1401., 40078944.),
            (1428., 40078946.),
            (1454., 40078948.),
            (1482., 40078950.),
            (1508., 40078952.),
            (1549., 40078954.),
            (1598., 40078956.),
            (1667., 40078958.),
            (1776., 40078960.),
            (1934., 40078962.),
            (2240., 40078963.),
            (2546., 40078963.),
            (2853., 40078964.)
            ]

    # End of class RampLHCIon20091015.

######################################################################
## RampLHCProton20091015UpDown class.
######################################################################

class RampLHCProton20091015UpDown(RampInfo):
    """This is the LHC proton ramp measured on October 15, 2009.

    NOTE: This is a ramp up followed by a (n almost) linear ramp down.

    This ramp was measured using the CMS frequency counter at P5
    during an RF test on 2009-10-15. The data was extracted from the
    CMS monitoring database using the following SQL query:

      select to_char(time, 'YYYY-MM-DD HH24:MI:SS'), frequency from mon_freqmeter where (time >= to_date('2009-10-15 15:15:30', 'YYYY-MM-DD HH24:MI:SS')) and (time <= to_date('2009-10-15 18:19:42', 'YYYY-MM-DD HH24:MI:SS')) order by time;

    All frequencies were corrected for the CMS counter offset of 218
    Hz to obtain the values stored below.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "LHC proton ramp measured October 15, 2009." \
               " (Includes slow down-ramp.)"

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [
            (    0., 40078879.),
            (  188., 40078881.),
            (  200., 40078883.),
            (  221., 40078885.),
            (  232., 40078887.),
            (  244., 40078889.),
            (  264., 40078892.),
            (  275., 40078894.),
            (  284., 40078897.),
            (  304., 40078900.),
            (  320., 40078904.),
            (  342., 40078909.),
            (  360., 40078913.),
            (  375., 40078916.),
            (  388., 40078919.),
            (  401., 40078921.),
            (  416., 40078924.),
            (  429., 40078926.),
            (  442., 40078929.),
            (  456., 40078931.),
            (  477., 40078934.),
            (  497., 40078937.),
            (  517., 40078940.),
            (  538., 40078942.),
            (  551., 40078944.),
            (  578., 40078946.),
            (  593., 40078948.),
            (  619., 40078950.),
            (  645., 40078952.),
            (  675., 40078954.),
            (  715., 40078956.),
            (  755., 40078958.),
            (  804., 40078960.),
            (  883., 40078962.),
            ( 1061., 40078964.),
            ( 1367., 40078964.),
            ( 1476., 40078966.),
            ( 1783., 40078966.),
            ( 2089., 40078966.),
            ( 2316., 40078964.),
            ( 2514., 40078962.),
            ( 2722., 40078960.),
            ( 2929., 40078958.),
            ( 3146., 40078956.),
            ( 3344., 40078954.),
            ( 3552., 40078952.),
            ( 3719., 40078950.),
            ( 3951., 40078948.),
            ( 4127., 40078946.),
            ( 4304., 40078944.),
            ( 4482., 40078942.),
            ( 4674., 40078940.),
            ( 4901., 40078938.),
            ( 5088., 40078936.),
            ( 5277., 40078934.),
            ( 5455., 40078932.),
            ( 5653., 40078930.),
            ( 5872., 40078928.),
            ( 6048., 40078926.),
            ( 6238., 40078924.),
            ( 6484., 40078922.),
            ( 6660., 40078920.),
            ( 6852., 40078918.),
            ( 7096., 40078916.),
            ( 7274., 40078914.),
            ( 7506., 40078912.),
            ( 7697., 40078910.),
            ( 7939., 40078908.),
            ( 8116., 40078906.),
            ( 8291., 40078904.),
            ( 8509., 40078902.),
            ( 8705., 40078900.),
            ( 8893., 40078898.),
            ( 9081., 40078896.),
            ( 9279., 40078894.),
            ( 9486., 40078892.),
            ( 9650., 40078890.),
            ( 9868., 40078888.),
            (10032., 40078886.),
            (10260., 40078884.),
            (10455., 40078882.),
            (10680., 40078880.),
            (10992., 40078879.)
            ]

    # End of class RampLHCProton20091015UpDown.

######################################################################
## RampHCALProblem class.
######################################################################

class RampHCALProblem(RampInfo):
    """Ramp measured on July 31, 2011, just before 0500 UTC.

    Presumably this makes the HCAL HBHEB fall over and produce all
    kinds of different event numbers.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "LHC proton ramp that supposedly makes HCAL HBHEB fall over."

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [
            (  0., 40078886.),
            ( 10., 40078886.),
            ( 20., 40078886.),
            ( 30., 40078886.),
            ( 40., 40078887.),
            ( 50., 40078889.),
            ( 60., 40078891.),
            ( 70., 40078894.),
            ( 80., 40078898.),
            ( 90., 40078902.),
            (100., 40078906.),
            (110., 40078910.),
            (120., 40078914.),
            (130., 40078918.),
            (140., 40078923.),
            (150., 40078927.),
            (160., 40078931.),
            (170., 40078933.),
            (180., 40078938.),
            (190., 40078941.),
            (200., 40078944.),
            (210., 40078946.),
            (220., 40078948.),
            (230., 40078951.),
            (240., 40078952.),
            (250., 40078955.),
            (260., 40078956.),
            (270., 40078958.),
            (280., 40078959.),
            (290., 40078960.),
            (300., 40078961.),
            (310., 40078962.),
            (320., 40078963.),
            (330., 40078964.),
            (340., 40078965.),
            (350., 40078965.),
            (360., 40078966.),
            (370., 40078967.),
            (380., 40078967.),
            (390., 40078967.),
            (400., 40078968.),
            (410., 40078968.),
            (420., 40078969.),
            (430., 40078969.),
            (440., 40078969.),
            (450., 40078969.),
            (460., 40078969.),
            (470., 40078969.),
            (480., 40078970.),
            (490., 40078970.),
            (500., 40078970.),
            (510., 40078970.),
            (520., 40078970.),
            (530., 40078971.),
            (540., 40078971.),
            (550., 40078971.),
            (560., 40078970.),
            (570., 40078971.),
            (580., 40078971.),
            (590., 40078971.),
            (600., 40078971.),
            (610., 40078971.),
            (620., 40078971.),
            (630., 40078972.),
            (640., 40078971.),
            (650., 40078971.),
            (660., 40078971.),
            (670., 40078972.),
            (680., 40078972.),
            (690., 40078972.)
            ]

    # End of class RampHCALProblem

######################################################################
## RampScanLinearLo class.
######################################################################

class RampScanLinearLo(RampInfo):
    """Linear test ramp to scan lower half of QPLL range."""

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Linear test ramp to scan lower half of QPLL range."

        RampInfo.__init__(self, name, desc)

        headroom_factor = .05
        headroom = int(math.ceil(headroom_factor * qpll_freq_delta))
        freq_start = qpll_freq_center + headroom
        freq_end = qpll_freq_lo - headroom
        freq_step = -2
        time_step = 2

        freqs = range(freq_start, freq_end - 1, freq_step)
        times = range(0, len(freqs) * time_step, time_step)

        self._ramp_info = zip(times, freqs)

    # End of class RampScanLinearLo

######################################################################
## RampScanLinearHi class.
######################################################################

class RampScanLinearHi(RampInfo):
    """Linear test ramp to scan upper half of QPLL range."""

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Linear test ramp to scan upper half of QPLL range."

        RampInfo.__init__(self, name, desc)

        headroom_factor = .05
        headroom = int(math.ceil(headroom_factor * qpll_freq_delta))
        freq_start = qpll_freq_center - headroom
        freq_end = qpll_freq_hi + headroom
        freq_step = 2
        time_step = 2

        freqs = range(freq_start, freq_end + 1, freq_step)
        times = range(0, len(freqs) * time_step, time_step)

        self._ramp_info = zip(times, freqs)

    # End of class RampScanLinearHi

######################################################################
## RampScanLinearDefault class.
######################################################################

class RampScanLinearDefault(RampInfo):
    """Linear test ramp to scan the full QPLL range."""

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Linear test ramp to scan the full of QPLL range."

        RampInfo.__init__(self, name, desc)

        headroom_factor = .05
        headroom = int(math.ceil(headroom_factor * qpll_freq_delta))
        freq_start = qpll_freq_lo - headroom
        freq_end = qpll_freq_hi + headroom
        freq_step = 200
        time_step = 30

        freqs = range(freq_start, freq_end + 1, freq_step)
        times = range(0, len(freqs) * time_step, time_step)

        self._ramp_info = zip(times, freqs)

    # End of class RampScanLinearDefault

######################################################################
## RampScanJumpsDefault class.
######################################################################

class RampScanJumpsDefault(RampInfo):
    """Jumping scan of QPLL frequency range.

    Start at the middle frequency, jump to x-100, x+100, x-200, x+200,
    etc.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Jumping scan of QPLL frequency range."

        RampInfo.__init__(self, name, desc)

        # According to the QPLL manual
        # (http://proj-qpll.web.cern.ch/proj-qpll/images/qpllManual.pdf)
        # the locking range is 'about +/- 3.7 KHz around f = 40.0786
        # MHz.'

        freq_lo = qpll_freq_lo
        freq_hi = qpll_freq_hi
        freq_start = 40078983
        freq_step = 100
        time_step = 25

        # Set this flag to true to keep the whole scan symmetric, even
        # if that means going outside the [freq_lo, freq_hi] range.
        keep_symmetric = True

        freqs = [freq_start]
        add_lo = True
        add_hi = True
        count = 1
        while add_lo or add_hi:
            if add_lo:
                freq = freq_start - count * freq_step
                freqs.append(freq)
                add_lo = (freq > freq_lo)
            if add_hi:
                freq = freq_start + count * freq_step
                freqs.append(freq)
                add_hi = (freq < freq_hi)
            count += 1
            if keep_symmetric:
                if add_lo or add_hi:
                    add_lo = True
                    add_hi = True
        times = range(0, len(freqs) * time_step, time_step)

        self._ramp_info = zip(times, freqs)

    # End of class RampScanJumpsDefault

######################################################################
## Default proton and heavy ion ramps.
######################################################################

########################################
## RampLHCDefaultProton class.
########################################
class RampLHCDefaultProton(RampLHCProton20091015):
    pass

########################################
## RampLHCDefaultIon class.
########################################
class RampLHCDefaultIon(RampLHCIon20091015):
    pass

######################################################################
## Some special fixed value 'ramps' to facilitate switching
## frequencies.
######################################################################

########################################
## RampFixedFreqProtonInjection class.
########################################
class RampFixedFreqProtonInjection(RampInfo):
    """Fixed frequency 'ramp' to be used to switch to the proton
    injection frequency.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Fixed frequency 'ramp' to be used " \
               "to switch to the proton injection frequency " \
               "(%.1f Hz)." % freq_lhc_proton_lo

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [(0, freq_lhc_proton_lo)]

    # End of class RampFixedFreqProtonInjection.

########################################
## RampFixedFreqProtonFlatTopSevenTeV class.
########################################
class RampFixedFreqProtonFlatTopSevenTeV(RampInfo):
    """Fixed frequency 'ramp' to be used to switch to the proton 7 TeV
    flat-top frequency.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Fixed frequency 'ramp' to be used " \
               "to switch to the proton 7 TeV flat-top frequency " \
               "(%.1f Hz)." % freq_lhc_proton_hi

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [(0, freq_lhc_proton_hi)]

    # End of class RampFixedFreqProtonFlatTopSevenTeV.

########################################
## RampFixedFreqIonInjection class.
########################################
class RampFixedFreqIonInjection(RampInfo):
    """Fixed frequency 'ramp' to be used to switch to the ion
    injection frequency.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Fixed frequency 'ramp' to be used " \
               "to switch to the ion injection frequency " \
               "(%.1f Hz)." % freq_lhc_heavy_ion_lo

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [(0, freq_lhc_heavy_ion_lo)]

    # End of class RampFixedFreqIonInjection.

########################################
## RampFixedFreqIonFlatTopSevenTeV class.
########################################
class RampFixedFreqIonFlatTopSevenTeV(RampInfo):
    """Fixed frequency 'ramp' to be used to switch to the ion 7 TeV
    flat-top frequency.

    """

    def __init__(self):

        # Cut off the leading 'Ramp' bit.
        name = self.__class__.__name__[4:]
        desc = "Fixed frequency 'ramp' to be used " \
               "to switch to the ion 7 TeV flat-top frequency " \
               "(%.1f Hz)." % freq_lhc_heavy_ion_hi

        RampInfo.__init__(self, name, desc)

        self._ramp_info = [(0, freq_lhc_heavy_ion_hi)]

    # End of class RampFixedFreqIonFlatTopSevenTeV.

######################################################################
