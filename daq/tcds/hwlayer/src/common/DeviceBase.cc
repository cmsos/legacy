#include "tcds/hwlayer/DeviceBase.h"

tcds::hwlayer::DeviceBase::DeviceBase()
{
}

tcds::hwlayer::DeviceBase::~DeviceBase()
{
}

bool
tcds::hwlayer::DeviceBase::isReadyForUse() const
{
  return isReadyForUseImpl();
}

bool
tcds::hwlayer::DeviceBase::isReadyForUseImpl() const
{
  // The default is to say no. This will force descendant classes to
  // handle this correctly.
  return false;
}

std::vector<std::string>
tcds::hwlayer::DeviceBase::getRegisterNames() const
{
  return getRegisterNamesImpl();
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::hwlayer::DeviceBase::getRegisterInfos() const
{
  return getRegisterInfosImpl();
}

uint32_t
tcds::hwlayer::DeviceBase::readRegister(std::string const& regName) const
{
  return readRegisterImpl(regName);
}

void
tcds::hwlayer::DeviceBase::writeRegister(std::string const& regName, uint32_t const regVal) const
{
  writeRegisterImpl(regName, regVal);
}

std::vector<uint32_t>
tcds::hwlayer::DeviceBase::readBlock(std::string const& regName,
                                     uint32_t const nWords) const
{
  return readBlockImpl(regName, nWords);
}

std::vector<uint32_t>
tcds::hwlayer::DeviceBase::readBlock(std::string const& regName) const
{
  return readBlock(regName, getBlockSize(regName));
}

std::vector<uint32_t>
tcds::hwlayer::DeviceBase::readBlockOffset(std::string const& regName,
                                           uint32_t const nWords,
                                           uint32_t const offset) const
{
  return readBlockOffsetImpl(regName, nWords, offset);
}

void
tcds::hwlayer::DeviceBase::writeBlock(std::string const& regName,
                                      std::vector<uint32_t> const& regVals) const
{
  writeBlockImpl(regName, regVals);
}

uint32_t
tcds::hwlayer::DeviceBase::getBlockSize(std::string const& regName) const
{
  return getBlockSizeImpl(regName);
}

void
tcds::hwlayer::DeviceBase::writeHardwareConfiguration(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const
{
  writeHardwareConfigurationImpl(cfg);
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::hwlayer::DeviceBase::readHardwareConfiguration(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  return readHardwareConfigurationImpl(regInfos);
}

tcds::hwlayer::RegDumpVec
tcds::hwlayer::DeviceBase::dumpRegisterContents() const
{
  return dumpRegisterContentsImpl();
}
