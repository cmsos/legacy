#ifndef __StreamInfoSpaceHandler
#define __StreamInfoSpaceHandler

#include "d2s/utils/InfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
#include <vector>
//#include "amc13controller/amc13Constants.h"

namespace utils
{
    class StreamInfoSpaceHandler : public utils::InfoSpaceHandler 
    {

    public:
        

        StreamInfoSpaceHandler(  xdaq::Application *xdaq,
                                 std::string name,
                                 utils::InfoSpaceUpdater *updater, 
                                 utils::InfoSpaceHandler *appIS,
				 //std::vector<bool>& streams,
                                 bool noAutoPush = false,
                                 uint32_t nbStreams = 0);

        /*
         * Overwriting of the base class functions: call baseclass function and then create
         * small vector with 2 entries to hold the values for the 2 streams (a buffer for the
         * debugging web pages).
         */
        virtual void createstring( std::string name, std::string value, std::string format = "", UpdateType updateType = PROCESS, std::string doc="", uint32_t level=1 );
        virtual void createuint32( std::string name, uint32_t value, std::string format = "", UpdateType updateType = HW32, std::string doc="", uint32_t level=1 );
        virtual void createuint64( std::string name, uint64_t value, std::string format = "", UpdateType updateType = HW64, std::string doc="", uint32_t level=1 );
        virtual void createdouble( std::string name, double value, std::string format = "", UpdateType updateType = PROCESS, std::string doc="", uint32_t level=1 );
        virtual void createbool( std::string name, bool value, std::string format = "", UpdateType updateType = HW32, std::string doc="", uint32_t level=1 );

        /*
         * Overwriting the base class functions: call the baseclass function and then copy
         * the value in the buffer vector created in the create... functions.
         */
        virtual void setstring( std::string name, std::string value, bool push=false );
        virtual void setuint32( std::string name, uint32_t value, bool push=false );
        virtual void setuint64( std::string name, uint64_t value, bool push=false );
        virtual void setuint64( std::string name, uint32_t low, uint32_t high, bool push=false );
        virtual void setbool( std::string name, bool value, bool push=false );
        virtual void setdouble( std::string name, double value, bool push=false );

        /*
         * New functions which retrieve the values for a specific stream (0 or 1)
         * from the buffer vectors.
         */
        std::string getstring( std::string name, uint32_t streamNo );
        uint32_t getuint32( std::string name, uint32_t streamNo );
        uint64_t getuint64( std::string name, uint32_t streamNo );
        bool getbool( std::string name, uint32_t streamNo );
        double getdouble( std::string name, uint32_t streamNo );

        /* 
         * used by web page and json 
         */
        virtual std::string getFormatted( std::string name, uint32_t streamNo, std::string format = "" );

        /**
         *
         *     @short This routine is adapting the item names to the stream number.
         *            
         *            Before the  infospace is  updated, the  stream has  to be 
         *            selected.  (This  is done  in  the  update routine).  The 
         *            stream number is  then used to update on the  fly all the 
         *            hardware register  names by inserting the  correct stream 
         *            index  (0 or  1 in  the AMC13).  Also the  _LO suffix  is 
         *            added for 64bit items.
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        virtual void setSuffix( uint32_t suffix );

        virtual void update();//uint32_t stream_index);

    protected:
        utils::InfoSpaceHandler *appIS_P;
        uint32_t currentStream_;
	//JRF TODO check if we need this. 
	//std::vector<bool> streams_;
        // containers to buffer the values of all types for all streams
        std::tr1::unordered_map< std::string, std::vector<std::string> > stringStreamValues_;
        std::tr1::unordered_map< std::string, std::vector<uint32_t> >    uint32StreamValues_;
        std::tr1::unordered_map< std::string, std::vector<uint64_t> >    uint64StreamValues_;
        std::tr1::unordered_map< std::string, std::vector<bool> >        boolStreamValues_;
        std::tr1::unordered_map< std::string, std::vector<double> >      doubleStreamValues_;
       
    private:
        uint32_t nbStreams_;
    };
}

#endif /* __StreamInfoSpaceHandler */
