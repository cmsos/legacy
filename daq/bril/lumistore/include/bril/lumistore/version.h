// $Id$
#ifndef _bril_lumistore_version_h_
#define _bril_lumstore_version_h_
#include "config/PackageInfo.h"
#define BRILLUMISTORE_VERSION_MAJOR 1
#define BRILLUMISTORE_VERSION_MINOR 6
#define BRILLUMISTORE_VERSION_PATCH 4
#define BRILLUMISTORE_PREVIOUS_VERSIONS 

// Template macros
//
#define BRILLUMISTORE_VERSION_CODE PACKAGE_VERSION_CODE(BRILLUMISTORE_VERSION_MAJOR,BRILLUMISTORE_VERSION_MINOR,BRILLUMISTORE_VERSION_PATCH)
#ifndef BRILLUMISTORE_PREVIOUS_VERSIONS
#define BRILLUMISTORE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILLUMISTORE_VERSION_MAJOR,BRILLUMISTORE_VERSION_MINOR,BRILLUMISTORE_VERSION_PATCH)
#else
#define BRILLUMISTORE_FULL_VERSION_LIST  BRILLUMISTORE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILLUMISTORE_VERSION_MAJOR,BRILLUMISTORE_VERSION_MINOR,BRILLUMISTORE_VERSION_PATCH)
#endif
namespace brillumistore{
  const std::string package = "brillumistore";
  const std::string versions = BRILLUMISTORE_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ lumistore";
  const std::string description = "bril daq storage";
  const std::string authors = "Z. Xie";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
