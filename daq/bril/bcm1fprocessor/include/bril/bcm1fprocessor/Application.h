
#ifndef _bril_bcm1fprocessor_Application_h_
#define _bril_bcm1fprocessor_Application_h_
#include <string>
#include <list>
#include <array>
#include <deque>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xgi/framework/UIManager.h"

#include "eventing/api/Member.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/squeue.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/EventDispatcher.h"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"

#include "toolbox/BSem.h"
#include "log4cplus/logger.h"
namespace toolbox{
  namespace task{
    class WorkLoop;
  }
}
namespace bril{
      namespace webutils
        {
          class WebChart;
         }
  namespace bcm1fprocessor{
    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener{
    public:
      struct InData{
      InData(unsigned short n,toolbox::mem::Reference* d):nbcount(n),dataref(d){}
      InData():nbcount(0),dataref(0){}
        unsigned short nbcount;
	  toolbox::mem::Reference* dataref;
      };

      const int s_nBcm1fChannels = 48;
      const int s_nAllChannels = 64;

      //static const float s_areaDiamond = 0.06125; // cm^2.  3.5mm*1.75mm = 0.35 cm * 0.175 cm = 0.06125 cm^2
      const float s_areaDiamond = 0.0722; // cm^2.  EFFECTIVE AREA (incl edge effects): 3.8mm*1.9mm = 0.38 cm * 0.19 cm = 0.0722 cm^2
      const float s_areaSilicon = 0.0625; // cm^2. 2.5mm*2.5mm = 0.25cm * 0.25cm = 0.0625 cm^2

    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
      // plotting webpage
      void plotting (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
      void RawRHUhists (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception);
      // timer callback
      virtual void timeExpired(toolbox::task::TimerEvent& e);
      
    private:
      // members
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;
      #ifdef x86_64_centos7
      const
      #endif
      xdaq::ApplicationDescriptor *m_appDescriptor;
      std::string m_classname;

      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;     
      unsigned int m_collectnb;
      unsigned int m_collectchannel;
      bool m_alignedNB4;
      unsigned int m_numberOfNibbles;
      unsigned int m_nChannelsThisNibble;
      //xdata::UnsignedInteger m_numberOfNibbles;

      // application lock, it's useful to always include one
      toolbox::BSem m_applock;

      //eventinginput, eventingoutput configuration  
      xdata::Vector<xdata::Properties> m_datasources;
      xdata::Vector<xdata::Properties> m_outputtopics;
      std::map<std::string, std::set<std::string> > m_in_busTotopics;
      typedef std::multimap< std::string, std::string > TopicStore;
      typedef std::multimap< std::string, std::string >::iterator TopicStoreIt;
      TopicStore m_out_topicTobuses;
      //count how many outgoing buses not ready
      std::set<std::string> m_unreadybuses;

      // incoming data cache
      typedef std::map<unsigned int,InData*> InDataCache;
      InDataCache m_bcm1fhistcache;
      unsigned int m_lastreceivedsec;

      // outgoing data cache
      interface::bril::DatumHead m_lastheader;

      typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* > QueueStore;
      typedef std::map<std::string, toolbox::squeue< toolbox::mem::Reference* >* >::iterator QueueStoreIt;
      QueueStore m_topicoutqueues;
      toolbox::task::WorkLoop* m_publishing;


      // data cache for albedo
      typedef std::deque< std::array<double,3564> > AlbedoCache;
      typedef std::deque< std::array<double,3564> >::iterator AlbedoIt;
      AlbedoCache m_albedoqueuepcvd;
      AlbedoCache m_albedoqueuesi;
      toolbox::task::WorkLoop* m_albedo_calculation;
      double m_albedo_fraction_pcvd[3564];
      double m_raw_noise_pcvd;
      double m_albedo_fraction_si[3564];
      double m_raw_noise_si;


      // counters for number of nibbles received
      //xdata::UnsignedInteger
      xdata::UnsignedInteger m_nHistsDiscarded;

      // background numbers
      xdata::Float m_BG1_plus;
      xdata::Float m_BG2_minus;
      xdata::Float m_last4_bkg1[4];
      xdata::Float m_last4_bkg2[4];
      /* float m_BG1_plus; */
      /* float m_BG2_minus; */

      // lumi calibration and numbers to publish
      xdata::String m_calibtag;
      xdata::String m_best_sensor_type;
      /* xdata::Float m_sigma_vis[3]; // order: pCVD, sCVD, Si */
      xdata::Float m_sigma_vis_scvd;
      xdata::Float m_sigma_vis_pcvd;
      xdata::Float m_sigma_vis_si;
      xdata::Float m_calib_at_SBIL_scvd;
      xdata::Float m_calib_at_SBIL_pcvd;
      xdata::Float m_calib_at_SBIL_si;
      xdata::Float m_nonlinearity_scvd;
      xdata::Float m_nonlinearity_pcvd;
      xdata::Float m_nonlinearity_si;
      xdata::UnsignedInteger m_tp_mask_start;
      xdata::UnsignedInteger m_tp_mask_end;

      xdata::UnsignedInteger m_albedo_calculation_time;
      xdata::UnsignedInteger m_albedo_queue_length;
      xdata::UnsignedInteger m_noise_calc_start;
      xdata::UnsignedInteger m_noise_calc_end;


      xdata::Float m_lumi_avg[3]; // sCVD, pCVD, Si
      xdata::Float m_lumi_avg_raw[3]; // sCVD, pCVD, Si
      float m_lumi_bx[3][3564];
      float m_lumi_bx_raw[3][3564];
      float m_raw_uncor_pcvd[3564];
      float m_raw_uncor_si[3564];
      float m_albedoqueue_hist_pcvd_uncor[3564];
      float m_albedoqueue_hist_pcvd_cor[3564];
      float m_albedoqueue_hist_si_uncor[3564];
      float m_albedoqueue_hist_si_cor[3564];
      float m_devel_hist1[3564];
      float m_devel_hist2[3564];
      float m_devel_hist3[3564];
      float m_devel_hist4[3564];
	  float m_fourbinsperbx[56][14265];
      xdata::String m_albedo_model_pcvd_str;
      xdata::String m_albedo_model_si_str;
      std::vector<float> m_albedo_model_pcvd;
      std::vector<float> m_albedo_model_si;

      // channel configuration
      //bool m_useChannelForLumi[48];
      bool m_useChannelForLumi[64];
      //      bool m_useChannelForBkgd[48];
      // bool m_sensorConnected[64];
      bool m_useChannelForBkgd[64];
      //double m_channelSigmaVis[48]; 
      //float m_channelSigmaVis[48]; 
      double m_channelScaleFactor[48]; 
      xdata::UnsignedInteger m_nRHUs;
      bool m_isChannelpCVD[48];
      bool m_isChannelsCVD[48];
      bool m_isChannelSilicon[48];
      bool m_isChannelValid[48];


      xdata::Boolean m_verbose;

      xdata::String m_beammode;

      xdata::Vector<xdata::Boolean> m_activeChannelsLumi;
      xdata::Vector<xdata::Boolean> m_activeChannelsBkgd;

      /* xdata::Boolean m_useChannelCalibrations; */

      /* xdata::Vector<xdata::Float> m_channelCalibrations; */
      xdata::Vector<xdata::Float> m_channelAcceptances;
      xdata::Vector<xdata::Float> m_channelEfficiencies;
      xdata::Vector<xdata::Float> m_channelCorrections;

      struct ChannelCounts
      {
	unsigned int channel_number;
	double hit_count;
	bool operator< (const ChannelCounts& rhs) const { return (hit_count < rhs.hit_count); }
      };

      // storage of total channel rates (excl. TP)
      //double m_totalChannelRate[48];
      //double m_totalChannelRate[64];
      xdata::Vector<xdata::Float> m_totalChannelRate;

      // bunch masks for background and collisions
      bool m_bkgd1mask[3564];
      bool m_bkgd2mask[3564];
      bool m_collmask[3564];

      // beam currents for normalization of background
      float m_iTot1;
      float m_iTot2;
      float m_iBunch1[3564];
      float m_iBunch2[3564];

      // albedo subtraction of background
      unsigned long m_countAlbedoHigherThanBackground;

      //plotting
      bril::webutils::WebChart* m_chart_lumi_raw_pcvd;
      bril::webutils::WebChart* m_chart_lumi_raw_si;
      bril::webutils::WebChart* m_chart_albedoqueue_pcvd;
      bril::webutils::WebChart* m_chart_albedoqueue_si;
      bril::webutils::WebChart* m_chart_devel;
	  bril::webutils::WebChart* m_chart_fourbinsperbx1;
	  bril::webutils::WebChart* m_chart_fourbinsperbx2;
	  bril::webutils::WebChart* m_chart_fourbinsperbx3;
	  bril::webutils::WebChart* m_chart_fourbinsperbx4;
	  bril::webutils::WebChart* m_chart_fourbinsperbx5;
	  bril::webutils::WebChart* m_chart_fourbinsperbx6;
    private: //flashlist stuff
      xdata::InfoSpace* m_mon_infospace;
      std::list<std::string> m_mon_varlist;
      std::vector<xdata::Serializable*> m_mon_vars;
      //xdata::String m_mon_beammode;
      xdata::UnsignedInteger m_mon_fill;
      xdata::UnsignedInteger m_mon_run;
      xdata::UnsignedInteger m_mon_ls;
      xdata::UnsignedInteger m_mon_nb;
      xdata::TimeVal m_mon_timestamp;

    private:
      // methods
      //      void do_process(void* dataptr,size_t buffersize);
      void do_process(interface::bril::DatumHead& inheader);
      void clear_cache();
      void hist_accumulateInChannel(InDataCache::iterator channelit,interface::bril::bcm1fhistT* indata);
      bool check_channelstatistics(unsigned int nchannels);
      bool check_channelstatistics_old(unsigned int nchannels);
      bool check_nbstatistics(unsigned short nnbs);
      bool check_nbstatistics_simple(unsigned short nnbs);
      bool publishing(toolbox::task::WorkLoop* wl);
      bool albedo_calculation(toolbox::task::WorkLoop* wl);
      void do_publish(const std::string& busname,const std::string& topicname,const std::string& pdict,toolbox::mem::Reference* bufRef);
      void subscribeall();
      void makeAggHistsAndTotalRates(interface::bril::DatumHead& inheader);
      void makeBkgNumbers(interface::bril::DatumHead& inheader);
      void makeLumiNumbers(interface::bril::DatumHead& inheader);
	  void makebxhistnumbers(interface::bril::DatumHead& inheader);
      void retrieveSourceHistos(toolbox::mem::Reference * ref, interface::bril::DatumHead * inheader);
      void makeBunchMasks(interface::bril::DatumHead * inheader);
      void setupMonitoring();
      void setupCharts();
      void requestData( xgi::Input* in, xgi::Output* out ) throw ( xgi::exception::Exception );
      template <typename T>
      std::vector<float> filterHistForPlot(T in, int len);


      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif
