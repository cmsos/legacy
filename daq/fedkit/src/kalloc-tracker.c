/* fedkit documentation is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: kalloc-tracker.c,v 1.1 2007/10/09 08:19:02 cano Exp $
*/
static char *rcsid_tracker = "@(#) $Id: kalloc-tracker.c,v 1.1 2007/10/09 08:19:02 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_tracker);
#endif
#include "kalloc-tracker.h"
#ifdef DEBUG_ALLOCATION
#ifdef __KERNEL__
#undef kmalloc
#undef kfree
#else
#undef malloc
#undef free
#endif

#ifdef __KERNEL__
#include <linux/kernel.h>
#define real_kmalloc(A,B) kmalloc((A),(B))
#define real_kfree(A) kfree(A)
#define eprintf(format, args...) printk (KERN_ERR format , ## args)
#else
#include <stdio.h>
#include <malloc.h>
#define real_kmalloc(A,B) malloc(A)
#define real_kfree(A) free(A)
#define eprintf(format, args...) fprintf (stderr, format , ## args)
#endif

#undef xdsh_alloc
#undef xdsh_freedefine 

#define filename_size  (20)

typedef struct tracker_elem {
	char file[filename_size];
	int line;
	void * pointer;
	struct tracker_elem * next;
} tracker_elem;

tracker_elem *alloc_tracker_first = NULL;
int alloc_tracker_number = 0;

inline void alloc_tracker_copy_string (const char * file, tracker_elem * elem)
{
	int size = 0;
	int copy_start = 0;
	/*int copy_end = 0;*/
	int copy_size = 0;
	int i;
	while (file[size] != '\0')
		size++;
	if (size < filename_size) {
		copy_start = 0;
		/*copy_end = size - 1;*/
		copy_size = size;
	} else {
		copy_start = size - (filename_size - 1);
		/*copy_end = size -1;*/
		copy_size = filename_size - 1;
	}
	for (i=0; i< copy_size;i++)
		elem->file[i] = file[copy_start + i];
	elem->file[copy_size] = '\0';
}

inline
void alloc_tracker_add(void *p, const char * file, int line)
{
	tracker_elem * new_elem = (tracker_elem *)real_kmalloc (sizeof (tracker_elem), GFP_KERNEL);
	if (new_elem == NULL) {
		eprintf ("Alloc tracker : couldn't alloc 0x%08x \"%s\", line %d not tracked\n",
			(int)p, file, line);
	}
	new_elem->pointer = p;
	new_elem->next = alloc_tracker_first;
	alloc_tracker_copy_string (file, new_elem);
	new_elem->line = line;
	alloc_tracker_first = new_elem;
}

inline
void alloc_tracker_remove (void *p, const char * file, int  line)
{
	tracker_elem ** ppelem = &alloc_tracker_first;
	
	while (*ppelem != NULL) {
		if (p == (*ppelem)->pointer) {
			tracker_elem *removed = *ppelem;
			*ppelem = removed->next;
			real_kfree (removed);
			return;
		} else 
			ppelem = &((*ppelem)->next);
	}
	eprintf ("Alloc tracker : freeing unknown 0x%08x \"%s\" line %d\n", 
		(int)p, file, line);
}
 
void alloc_tracker_final_cleanup (void)
{
	tracker_elem * pelem = alloc_tracker_first;
	int count = 0;
	eprintf ("Starting kalloc_tracker cleanup...\n");
	alloc_tracker_first = NULL;
	while (pelem != NULL) {
		tracker_elem *removed = pelem;
		eprintf ("Alloc tracker : forgotten pointer 0x%08x \"%s\" line %d\n",
			(int)removed->pointer, removed->file, removed->line);
		pelem = pelem->next;
		real_kfree (removed);
		count ++;
	}
	eprintf ("kalloc_tracker : cleanup complete, a total of %d forgotten pointers was found\n", count);
}

void * kmalloc_tracked (size_t size, int flags, const char *file, int line)
{
  void * ret;
  ret = (void *)real_kmalloc (size, flags);
  if (ret != NULL)
	  alloc_tracker_add (ret, file, line);
  iprintf ("alloc %x file \"%s\" line %d\n",(int)ret, file, line);
  return ret;
}

void kfree_tracked (void *p, const char *file, int line)
{
  alloc_tracker_remove (p, file, line);
  iprintf ("free %x file \"%s\" line %d\n",(int)p, file, line);
  real_kfree (p);
}

struct xdsh_msg * xdsh_alloc_tracked (U32 size, const char *file, int line)
{
  struct xdsh_msg * ret;
  ret = (void *)xdsh_alloc (size);
  if (ret != NULL)
	  alloc_tracker_add ((void *)((int)ret | 0x1), file, line); /* trick! we know that least significant bit will be 0 thanks to alignement */
	  /* so we avoid interfering with real malloc (same address) */ 
  return ret;
}

void xdsh_free_tracked (struct xdsh_msg *msg, const char *file, int line)
{
   alloc_tracker_remove ((void *)((int)msg | 0x1), file, line);
   xdsh_free (msg);
}


#endif /* def DEBUC_ALLOCATION */
