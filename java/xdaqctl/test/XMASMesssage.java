package net.hep.cms.xdaqctl;

import net.hep.cms.xdaqctl.xdata.FlashList;

import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPException;

import org.w3c.dom.Document;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * XMAS SOAP message class
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @author  Alexander Oh (alexander.oh@cern.ch)
 * @version $Name:  $
 */

public class XMASMesssage extends Message {
	
	private static final String WSE_TARGET = "urn:xdaq-application:service=ws-eventing";
	private static final String WSA_NS = "http://www.w3.org/2005/08/addressing";
	private static final String XMAS_NS =
			"http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-10";
	private static final String XMAS_SENSOR_NS =
			"http://xdaq.web.cern.ch/xdaq/xsd/2006/xmas-sensor-10";
	
	// TODO: set the endpoint address
	private String endpoint="FIXME";

	/**
	 * Create a SOAP message.
	 * The message contains a serialized FlashList object as MIME attachment.
	 * 
	 * @param fl FlashList to send.
	 * @param originator URL indicating the message originator.
	 */
	public XMASMesssage(FlashList fl, String originator)
			throws XDAQMessageException
	{
		try {
			initialize();
		} catch (XDAQMessageException e) {
			throw new XDAQMessageException(
					"Failed to initialize for flashlist.", e);
		}
	
		// "SOAPAction" MIME header should be added when the message is sent.
		// Set SOAPAction header with a default target
		message.getMimeHeaders().addHeader("SOAPAction", WSE_TARGET);

		try {
			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
			
			// SOAPHeader->{Action,To}
			SOAPHeader header = envelope.getHeader();
			SOAPElement e;
			e = header.addChildElement("Action", "wsa", WSA_NS);
			e.addTextNode(WSE_TARGET);
			e = header.addChildElement("To", "wsa", WSA_NS);
			e.addTextNode("ENDPOINT");
			
			// SOAPBody->report->sample
			SOAPBody body = envelope.getBody();
			SOAPElement report = body.addBodyElement(envelope.createName(
					"report", "xmas", XMAS_NS));
			report.addNamespaceDeclaration("xmas-sensor", XMAS_SENSOR_NS);
			SOAPElement sample = report.addChildElement(
					"sample", "xmas", XMAS_NS);
			sample.addAttribute(envelope.createName("flashlist"),
						"urn:xdaq-flashlist:" + fl.getName());
			sample.addAttribute(envelope.createName("tag"), "instant");
			sample.addAttribute(envelope.createName("originator"), originator);
		} catch (SOAPException e) {
			throw new XDAQMessageException(
					"Failed to create XMAS SOAP message", e);
		}
			
		try {
			// binary flashlsit attachment
			AttachmentPart attachment = message.createAttachmentPart(
					fl.serialize(), "application/x-xdata+exdr");
			attachment.setMimeHeader("Content-Disposition",
					"attachment; filename=" + "listname" + ".exdr; "
					+ "creation-date=\"" + getDateString() + "\"");
			message.addAttachmentPart(attachment);
		} catch (Exception e) {
			throw new XDAQMessageException(
					"Failed to attach a serialized flash list", e);
		}
	}
	
	public Document send(String endpoint)
			throws XDAQMessageException,
			       XDAQTimeoutException {

//		try {
//			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
//			SOAPElement to = (SOAPElement)
//					envelope.getHeader().getChildElements(
//					envelope.createName("To", "wsa", WSA_NS))
//					.next();
//			
//			to.removeContents();
//			to.addTextNode(endpoint + "/" + WSE_TARGET);
//			
//			System.out.println("====");
//			try {
//				message.writeTo(System.out);
//			} catch (Exception ignored) {}
//			System.out.println("====");
//		} catch (SOAPException e) {
//			throw new XDAQMessageException(
//					"Failed to rewrite SOAP header 'To'", e);
//		}
//		
		return super.send(endpoint, WSE_TARGET);
	}

	private String getDateString() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		
		return format.format(new Date());
	}


}
