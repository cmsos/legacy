// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_store2g_TStoreProxy_h_
#define _xmas_store2g_TStoreProxy_h_

#include <string>
#include <map>

#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/Object.h" 
#include "xmas/store2g/DescriptorsCache.h" 
#include "xmas/FlashListDefinition.h" 
#include "xmas/store2g/exception/Exception.h"
#include "xmas/store2g/Settings.h"
#include "xmas/store2g/exception/IncompatibleDefinition.h"
#include "xmas/store2g/exception/FailedToCheckTable.h"
#include "xdata/Table.h"

namespace xmas 
{
	namespace store2g 
	{
		class TStoreProxy : xdaq::Object
		{
			public:
			
			TStoreProxy(xdaq::Application * owner, xmas::store2g::DescriptorsCache * descriptorsCache);
			
			void insert (xmas::store2g::Settings::Properties & p, xdata::Table::Reference& table, 
				const std::string& store2gGroup, const std::string& store2gView, const std::string& database)
				;
				
			void authenticate(const std::string & authentication, const std::string & credentials);

			size_t discoveredTStores(const std::string  & store2gGroup);

            		void renewConnection(const std::string& store2gGroup) ;


			size_t getConnectionCounter();

			void connect 
			(
				const std::string& store2gGroup,
				const std::string& store2gView
			)
			;

            		void closeConnection(const std::string& store2gGroup) ;

			protected:

			
			bool hasView 
			(
				const std::string& store2gGroup,
				const std::string& store2gView
			)
			;
			
			void createView 
			(
				const std::string& store2gGroup, 
				const std::string& store2gView, 
				const std::string& database
			)
				;
			
				
			void syncToDB (xmas::FlashListDefinition* definition, const std::string& store2gGroup)
				;
			
			void syncFromDB (xmas::FlashListDefinition* definition, const std::string& store2gGroup)
				;	
				
			bool isConnected();
			
			std::string getConnectionId();
			
			void configureView
			(
				xmas::FlashListDefinition * definition,
				const std::string& store2gGroup,
				const std::string& store2gView
			)
			;
		
			bool hasTableDefinition 
			(
				xmas::FlashListDefinition * definition, 
				const std::string& store2gGroup,
				const std::string& store2gView
			)
			;

			bool hasMetaData 
			(
				const std::string& store2gGroup,
				const std::string& database 
			)
			;

			void createMetaData 
			(
				const std::string& store2gGroup,
				const std::string& database 
			)
			;


			void insertMetaData
			(
				xmas::FlashListDefinition * definition, 
				const std::string& store2gGroup, 
				const std::string& database
			)
			;

			void validate
			(
				xmas::FlashListDefinition * definition, 
				const std::string& store2gGroup, 
				const std::string& store2gView,
				const std::string& database
			)
			;
		


			private:
			
			void addTableDefinition(DOMDocument * document, xmas::FlashListDefinition * flashlist );
			void addTableDefinition(DOMElement * tableNode, const std::string & name, const std::string key, 
						   xmas::ItemDefinition * inner,
						   const std::string & outer ,std::list<xmas::ItemDefinition *> keyItems );

			/*! Pass a set to the function and add all children of \parameter item that are 'table' types to the set			
			*/
			void addInnerTableNames(xmas::ItemDefinition* item, std::set<std::string>& tables);
			
			void addDwellTableDefinition
			(
				DOMDocument * document,
				xmas::FlashListDefinition * flashlist
			);
				
			void clearTable
			(
				std::list<xdaq::ApplicationDescriptorImpl>& descriptors, 
				const std::string& name
			)
			;
				
			void insertTable
			(
				std::list<xdaq::ApplicationDescriptorImpl>& descriptors, 
				const std::string& name,
				xdata::Table::Reference& table,
				size_t depth
			)
			;
			
			xmas::store2g::DescriptorsCache * descriptorsCache_;
			bool isConnected_;
			std::string connectionId_;
			std::string authentication_;
  			std::string credentials_; 
  			std::string timeout_; 
			size_t connections_;
		};
	}
}
#endif
