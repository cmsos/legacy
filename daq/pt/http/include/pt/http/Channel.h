// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_http_Channel_h_
#define _pt_http_Channel_h_

#include <netinet/in.h>

#include "pt/Address.h"
#include "pt/http/exception/Exception.h"

#include "toolbox/BSem.h"

namespace pt
{
namespace http
{

class Channel 
{
	public:
	
	Channel(pt::Address::Reference address) ;
		
	virtual ~Channel();
	
	//! connect channel according configuration
	virtual void connect()  = 0;
	
	//! disconnect but keep channel alive
	virtual void disconnect()  = 0;
	
	//! receive len characters into buf
	virtual ssize_t receive(char * buf, size_t len )  = 0;
	
	//! send buffer of given lenght
	virtual void send(const char * buf, size_t len)  = 0;
	
	//! Close a connection definitely
	virtual void close()  = 0;
		
	//! Check if the connection is up
	virtual bool isConnected()  = 0;
	
	//! Get exclusive access to the Channel
	//
	void lock();
	
	//! Release exclusive access to the Channel
	//
	void unlock();
		
	protected:
	
	struct sockaddr_in sockaddress_;
	socklen_t sockaddressSize_;
	int socket_;
	toolbox::BSem mutex_;
	
	
};


class ClientChannel: public Channel
{
	public:
	
	ClientChannel(pt::Address::Reference address) ;
	
	//! connect channel according configuration
	void connect() ;
	
	//! disconnect
	void disconnect() ;
	
	//! receive len characters into buf
	ssize_t receive(char * buf ,size_t len ) ;
	
	//! send buffer of given lenght
	void send(const char * buf, size_t len) ;
	
	//! Close a connection definitely
	void close() ;
	
	//! Check if the connection is up
	bool isConnected() ;

	void setConnectionTimeout(time_t timeout);

	
	private:
	
		bool connected_;
		time_t timeout_;
};
}


/*
class ServerChannel: public Channel
{
	public:
	
	ServerChannel(http::Address* address) ;
	
	//! reconnect a dropped incoming connection after disconnect
	void connect() ;
	
	//! disconnect but keep channel alive
	void disconnect() ;
	
	//! receive len characters into buf
	int receive(char * buf ,int len ) ;
	
	//! send buffer of given lenght
	void send(const char * buf, int len) ;
	
	//! Close a connection definitely
	void close() ;
	
	//! Check if the connection is up
	bool isConnected() ;
};
*/
}

#endif
