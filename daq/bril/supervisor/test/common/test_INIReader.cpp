#include <iostream>
#include "bril/supervisor/INIReader.h"
#include "bril/supervisor/Decoder.h"
#include <cassert>
int main(){
  INIReader reader("test.ini");

  if (reader.ParseError() < 0) {
    std::cout << "Can't load 'test.ini'\n";
    return 1;
  }
  std::string name(reader.Get("user", "name", "UNKNOWN"));
  std::string t(reader.Get("user", "encodedname",""));
  std::string decoded=base64_decode(t);
  std::cout << "Config loaded from 'test.ini': version="
	    << reader.GetInteger("protocol", "version", -1) << ", name="
	    << name << ", email="
	    << reader.Get("user", "email", "UNKNOWN") << ", pi="
	    << reader.GetReal("user", "pi", -1) << ", active="
	    << reader.GetBoolean("user", "active", true) << "\n";
  assert(name==decoded);
  return 0;
}
