#ifndef __FerolWebServer
#define __FerolWebServer

#include "log4cplus/logger.h"
#include "d2s/utils/Monitor.hh"
#include "d2s/utils/WebServer.hh"
#include "d2s/utils/HardwareDebugItem.hh"
#include "ferol/Ferol.hh"
#include "ferol/Frl.hh"
//#include "ferol/FerolController.hh"


namespace ferol
{
    class FerolController;


    class FerolWebServer : public utils::WebServer
    {

    public: 
        FerolWebServer( utils::Monitor &monitor, std::string url, std::string urn, Logger logger );
        std::string getCgiPara(  cgicc::Cgicc cgi, std::string name, std::string defaultval );
        void printDebugTable( std::string name, std::list< utils::HardwareDebugItem >, xgi::Input *in, xgi::Output *out);
        void printHeader( xgi::Output * out );
        void expertDebugging( xgi::Input *in, xgi::Output *out, Ferol *ferol, Frl *frl, FerolController *const fctl );
    };
};

#endif /* __FerolWebServer */
