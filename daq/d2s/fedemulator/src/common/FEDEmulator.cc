#include "d2s/fedemulator/FEDEmulator.hh"
#include "d2s/fedemulator/FEDEmulatorAddressTableReader.hh"
#include "xdata/InfoSpaceFactory.h"

XDAQ_INSTANTIATOR_IMPL(d2s::FEDEmulator);

d2s::FEDEmulator::FEDEmulator(xdaq::ApplicationStub* stub)  
  :
  xdaq::WebApplication(stub),
  logger_(getApplicationLogger()),
  thresholdTTSReady_(0x40),
  thresholdTTSBusy_(0x60)
{
	stub->getDescriptor()->setAttribute("icon","/d2s/fedemulator/images/FEDEmulator.png");

  try
  {
    busAdapter_ = new HAL::PCILinuxBusAdapter();
  }
  catch(xcept::Exception &e)
  {
    busAdapter_ = 0;

    XCEPT_RETHROW(xdaq::exception::Exception,
      "Failed to instantiate HAL::PCILinuxBusAdapter", e);
  }
  catch(...)
  {
    busAdapter_ = 0;

    XCEPT_RAISE(xdaq::exception::Exception,
      "Failed to instantiate HAL::PCILinuxBusAdapter Unknown exception");
  }
  
  try
    {
      
      d2s::FEDEmulatorAddressTableReader reader;
       
      addressTable_p  = new HAL::PCIAddressTable( "Pseudo FED AddressTable", reader );
    }
  catch( HAL::HardwareAccessException& e) 
    {
      std::string msg = e.what();
      XCEPT_RAISE( xdaq::exception::Exception, msg );
    }  

  
  className_ = getApplicationDescriptor()->getClassName();
  instance_ = getApplicationDescriptor()->getInstance();
  std::string url = getApplicationDescriptor()->getContextDescriptor()->getURL();
  url += "/";
  url += getApplicationDescriptor()->getURN();

  url_ = url;

  LOG4CPLUS_DEBUG (getApplicationLogger(), "FEDEmulator constructor start" );
  
  // initialize the member variables 

  fedEmulatorCard_p      = (FEDEmulatorCard*)0;
  fedEmulatorCardExists_ = false;
  eventFilePath_         = "hello";
  expEventFile_.push_back( "hello" );

  slot_                   = 999;
  instantiateByIndex_     = false;

  // setupName_ characterizes the setup used, and it affects the 
  // slot-pcislot conversion (this is PC-dependent)
  setupName_             = "CESSY";
  
  triggerSource_         = SOFTWARE; // default mode 0=external trigger,
                                     // 1=software trigger, 
                                     // 2=hardware-loop (= self-triggered)
  FedSourceId_           = 0;
  eventTableRank_        = 0;
  eventSizeSeed_         = 0;
  eventSize_             = 0;        // mean event size
  deltaT_                = 0;
  eventSizeStdDev_       = 0;        // if set to 0 means fixed event size
  minEventSize_          = 0;
  maxEventSize_          = 0;
  eventPreload_          = false;
  initialEventNumber_    = 1;
  triggerNum_            = 0;
  pendingTriggers_       = 999;
  MonFile_               = "DefaultMonFile.txt";
  writeFile_             = 0; 
  softSTTSEnable_        = 0;

  // For RunControl to determine the state of the application

  getApplicationInfoSpace()->fireItemAvailable("stateName",  &stateName_);
  getApplicationInfoSpace()->fireItemAvailable("setupName",  &setupName_);
  getApplicationInfoSpace()->fireItemAvailable("GIIIversion",&GIIIversion_);

  // to be set BEFORE configure (in ParametersSetDefault)
  getApplicationInfoSpace()->fireItemAvailable("eventFilePath", &eventFilePath_);
  getApplicationInfoSpace()->fireItemAvailable("MonFile", &MonFile_);
  
  // to be set via the configuration file before enable
  
  getApplicationInfoSpace()->fireItemAvailable("triggerSource", &triggerSource_);
  getApplicationInfoSpace()->fireItemAvailable("FedSourceId", &FedSourceId_);
  getApplicationInfoSpace()->fireItemAvailable("eventTableRank", &eventTableRank_);
  getApplicationInfoSpace()->fireItemAvailable("eventSizeSeed", &eventSizeSeed_);
  getApplicationInfoSpace()->fireItemAvailable("eventSize", &eventSize_);
  getApplicationInfoSpace()->fireItemAvailable("deltaTevt", &deltaT_);
  getApplicationInfoSpace()->fireItemAvailable("eventSizeStdDev", &eventSizeStdDev_);
  getApplicationInfoSpace()->fireItemAvailable("minEventSize", &minEventSize_);
  getApplicationInfoSpace()->fireItemAvailable("maxEventSize", &maxEventSize_);
  getApplicationInfoSpace()->fireItemAvailable("slot", &slot_);
  getApplicationInfoSpace()->fireItemAvailable("instantiateByIndex", &instantiateByIndex_);
  getApplicationInfoSpace()->fireItemAvailable("pciIndex", &pciIndex_);
  getApplicationInfoSpace()->fireItemAvailable("eventPreload", &eventPreload_);

  getApplicationInfoSpace()->fireItemAvailable("initialEventNumber", &initialEventNumber_);

  getApplicationInfoSpace()->fireItemAvailable("thresholdTTSReady", &thresholdTTSReady_);
  getApplicationInfoSpace()->fireItemAvailable("thresholdTTSBusy", &thresholdTTSBusy_);

  getApplicationInfoSpace()->fireItemAvailable("softSTTSEnable", &softSTTSEnable_);

  // Bind SOAP callbacks for control messages
  xoap::bind (this, &FEDEmulator::changeState, "Configure", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::changeState, "Enable", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::changeState, "Suspend", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::changeState, "Resume", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::changeState, "Halt", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::changeState, "Stop", XDAQ_NS_URI);
	
  // Bind SOAP callback to fire a trigger via a SOAP message called "userTrigger".
  xoap::bind (this, &FEDEmulator::UserTrigger, "UserTrigger", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::readItem, "readItem", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::writeItem, "writeItem", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::readpar, "readpar", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::setWriteFile, "setWriteFile", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::setSTTSPattern, "setSTTSPattern", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::cycleSTTSPatterns, "cycleSTTSPatterns", XDAQ_NS_URI);
  xoap::bind (this, &FEDEmulator::setSTTSControl, "setSTTSControl", XDAQ_NS_URI);

  // Bind default web page
  xgi::bind(this, &FEDEmulator::defaultWebPage, "Default");
  xgi::bind(this, &FEDEmulator::setParameter, "setParameter");
  
  // initialize parameter to a default value
  // note that eventSize_ etc not yet set at this stage
  fragSizMean_ = 0;
  fragSizStdDev_ = 0;  
  
  getApplicationInfoSpace()->fireItemAvailable("fragSizMean", &fragSizMean_);
  getApplicationInfoSpace()->fireItemAvailable("fragSizStdDev", &fragSizStdDev_);
  
  // Define FSM
  fsm_.addState ('H', "Halted"   , this, &FEDEmulator::stateChanged );
  fsm_.addState ('R', "Ready"    , this, &FEDEmulator::stateChanged );
  fsm_.addState ('E', "Enabled"  , this, &FEDEmulator::stateChanged );
  fsm_.addState ('S', "Suspended", this, &FEDEmulator::stateChanged );
  
  fsm_.addStateTransition ('H','R', "Configure", this, &FEDEmulator::ConfigureAction);
  fsm_.addStateTransition ('R','E', "Enable",    this, &FEDEmulator::EnableAction);
  fsm_.addStateTransition ('E','R', "Stop",      this, &FEDEmulator::StopAction);
  fsm_.addStateTransition ('E','H', "Halt",      this, &FEDEmulator::HaltAction);
  fsm_.addStateTransition ('R','H', "Halt",      this, &FEDEmulator::HaltAction);
  fsm_.addStateTransition ('S','H', "Halt",      this, &FEDEmulator::HaltAction);
  fsm_.addStateTransition ('H','H', "Halt",      this, &FEDEmulator::HaltAction);
  fsm_.addStateTransition ('E','S', "Suspend",   this, &FEDEmulator::SuspendAction);
  fsm_.addStateTransition ('S','E', "Resume",    this, &FEDEmulator::ResumeAction);
  
  // Failure state setting
  fsm_.setFailedStateTransitionAction(this, &FEDEmulator::failedTransition );
  fsm_.setFailedStateTransitionChanged(this, &FEDEmulator::stateChanged );
  
  fsm_.setInitialState('H');
  fsm_.reset();
  stateName_ = "Halted";
  
  // Monitorable variables
  
  xdata::InfoSpace * isStatus;
  isStatus = xdata::getInfoSpaceFactory()->create(toolbox::toString("urn:xdaq-monitorable:category=status,class=%s,instance=%d",className_.toString().c_str(), (uint32_t) instance_));
  
  isStatus->fireItemAvailable("className",&className_);
  isStatus->fireItemAvailable("instance",&instance_);
  isStatus->fireItemAvailable("url",&url_);
  isStatus->fireItemAvailable("stateName",&stateName_);

  xdata::InfoSpace * is;
  is = xdata::getInfoSpaceFactory()->create(toolbox::toString("urn:xdaq-monitorable:category=card,class=%s,instance=%d",className_.toString().c_str(), (uint32_t) instance_));
  
  is->fireItemAvailable("className",&className_);
  is->fireItemAvailable("instance",&instance_);
  is->fireItemAvailable("url",&url_);
  is->fireItemAvailable("slot",&slot_);
  is->fireItemAvailable("FedSourceId",&FedSourceId_);
  is->fireItemAvailable("pendingTriggers",&pendingTriggers_);
  is->fireItemAvailable("triggerNum",&triggerNum_);
  is->fireItemAvailable("linkNotFull",&linkNotFull_);
  is->fireItemAvailable("linkNotDown",&linkNotDown_);

  is->addItemRetrieveListener ("pendingTriggers", this);

  LOG4CPLUS_DEBUG(getApplicationLogger(), "FEDEmulator constructor end");

}


d2s::FEDEmulator::~FEDEmulator() {

  if(busAdapter_)
  {
    delete busAdapter_;
    busAdapter_ = (HAL::PCILinuxBusAdapter*)0;
  }

  fedEmulatorCardExists_ = false;
  if ( addressTable_p ) {
    delete addressTable_p;
    addressTable_p = (HAL::PCIAddressTable*)0;
  }
  if ( fedEmulatorCard_p ) {
    delete fedEmulatorCard_p;
    fedEmulatorCard_p = (FEDEmulatorCard*)0;
  }
}


xoap::MessageReference d2s::FEDEmulator::changeState (xoap::MessageReference msg) 
   {
  
  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();
  DOMNode* node = body.getDOMNode();
  DOMNodeList* bodyList = node->getChildNodes();
  for (XMLSize_t i = 0; i < bodyList->getLength(); i++) 
    {
      DOMNode* command = bodyList->item(i);
      
      if (command->getNodeType() == DOMNode::ELEMENT_NODE)
	{
	  std::string commandName = xoap::XMLCh2String (command->getLocalName());
	  try 
	    {
	      toolbox::Event::Reference e(new toolbox::Event(commandName, this));
	    fsm_.fireEvent(e);
	    // Synchronize Web state machine
	    // wsm_.setInitialState(fsm_.getCurrentState());
	    }
	  catch (toolbox::fsm::exception::Exception & e)
	    {
	      LOG4CPLUS_ERROR(getApplicationLogger(),  
			      toolbox::toString("Command not allowed : %s", e.what()));
	      XCEPT_RETHROW(xoap::exception::Exception, "Command not allowed", e);		
	    }
	  
	  try
	    {
	      return createFsmSoapResponseMsg(commandName,
					      stateName_.toString());
	    }
	  catch(xcept::Exception e)
	    {
	      XCEPT_RETHROW(xoap::exception::Exception,
                            "Failed to create FSM SOAP response message", e);
	    }
	  
	}
    }
  
  LOG4CPLUS_ERROR(getApplicationLogger(), "No command found");
  XCEPT_RAISE(xoap::exception::Exception, "No command found");		
}


void d2s::FEDEmulator::stateChanged (toolbox::fsm::FiniteStateMachine & fsm) 
   {

  LOG4CPLUS_DEBUG (getApplicationLogger(), "New state is:" << 
                  fsm.getStateName (fsm.getCurrentState()) );
}

void d2s::FEDEmulator::failedTransition (toolbox::Event::Reference e) 
   {

  toolbox::fsm::FailedEvent & fe = dynamic_cast<toolbox::fsm::FailedEvent&>(*e);
  LOG4CPLUS_ERROR(getApplicationLogger(), 
		  "Failure occurred when performing transition from: "
		  << fe.getFromState() <<  " to: " << fe.getToState() << 
		  " exception: " << fe.getException().what() );
  stateName_="Failed";
}





void d2s::FEDEmulator::actionPerformed (xdata::Event& e) {

  // this callback is called by the monitor collector
  uint32_t readparam;
  
  if (e.type() == "ItemRetrieveEvent")  {
    
    if ( fedEmulatorCardExists_ ) {
      
      std::string item = dynamic_cast<xdata::ItemRetrieveEvent&>(e).itemName();

      try 
	{

	  if ( item == "pendingTriggers") {
	    fedEmulatorCard_p->read( "pendingTriggers", &readparam);
	    pendingTriggers_ = readparam;
	    fedEmulatorCard_p->read( "triggerNum",  &readparam );
	    triggerNum_ = readparam;
	    fedEmulatorCard_p->read( "linkNotFull",  &readparam );
	    linkNotFull_ = readparam;
	    fedEmulatorCard_p->read( "linkNotDown",  &readparam );
	    linkNotDown_ = readparam;
	  }
	  
	} 
      catch (HAL::HardwareAccessException& e) 
	{
	  
	  std::string msg = e.what();
	  LOG4CPLUS_ERROR(getApplicationLogger(), "ERROR ==> ");
	  XCEPT_RAISE( xcept::Exception, msg );
	  
	}
    } else {
      LOG4CPLUS_WARN(getApplicationLogger(), "Cannot readout fedEmulatorCard since it is not existing");
    }
  }
}

void d2s::FEDEmulator::webTableEntry( xgi::Output *out, const std::string name, const std::string value ) const {

  *out << "<tr>"                                 << std::endl;
  *out << "<td>"             << std::endl;
  *out << name                                   << std::endl;
  *out << "</td>"                                << std::endl;
  *out << "<td>"             << std::endl;
  *out << value                                  << std::endl;
  *out << "</td>"                                << std::endl;
  *out << "</tr>"                                << std::endl;
  
}

void d2s::FEDEmulator::webTableEntry( xgi::Output *out, const std::string name, const uint32_t value ) const {

  *out << "<tr>"                                 << std::endl;
  *out << "<td>"             << std::endl;
  *out << name                                   << std::endl;
  *out << "</td>"                                << std::endl;
  *out << "<td>"             << std::endl;
  *out << value                                  << std::endl;
  *out << "</td>"                                << std::endl;
  *out << "</tr>"                                << std::endl;  
}

void d2s::FEDEmulator::webTableEntry( xgi::Output *out, const std::string name, const bool value ) const {

  *out << "<tr>"                                 << std::endl;
  *out << "<td>"             << std::endl;
  *out << name                                   << std::endl;
  *out << "</td>"                                << std::endl;
  *out << "<td>"             << std::endl;
  *out << value                                  << std::endl;
  *out << "</td>"                                << std::endl;
  *out << "</tr>"                                << std::endl;  
}

void d2s::FEDEmulator::defaultWebPage(xgi::Input *in, xgi::Output *out)
   {

  FEDEmulator::Get_Counters();
  
  *out << "<html>"                        << std::endl;
  
  *out << "<head>"                        << std::endl;
  *out << "<title>FEDEmulator hyperDAQ page</title>"     << std::endl;
  *out << "</head>"                                   << std::endl;
  
  *out << "<body>"                                    << std::endl;
  
  *out << "<table border=1 bgcolor=\"#CFCFCF\">" << std::endl;
  
  webTableEntry( out, "instantiateByIndex", instantiateByIndex_ );
  webTableEntry( out, "slot", slot_ );
  webTableEntry( out, "pciIndex", pciIndex_ );
  webTableEntry( out, "FedId", FedSourceId_ );
  webTableEntry( out, "status", fsm_.getStateName( fsm_.getCurrentState()) );
  webTableEntry( out, "eventPreload", eventPreload_ );
  webTableEntry( out, "trigger Number", triggerNum_ );
  webTableEntry( out, "pending triggers", pendingTriggers_  );
  webTableEntry( out, "link NOT full", linkNotFull_ );
  webTableEntry( out, "link NOT down", linkNotDown_ );
  webTableEntry( out, "setloop", setloop_ );
  webTableEntry( out, "softwareTriggerEnable", softwareTriggerEnable_ );
  webTableEntry( out, "fragSize Mean", eventSize_ );    
  webTableEntry( out, "fragSize StdDev", eventSizeStdDev_ );    
  webTableEntry( out, "thresholdTTSReady", (uint32_t) thresholdTTSReady_ );    
  webTableEntry( out, "thresholdTTSBusy", (uint32_t) thresholdTTSBusy_ );    
  webTableEntry( out, "softSTTSEnable", (uint32_t) softSTTSEnable_ );

  *out << "</table>"                             << std::endl;
  *out << "<p>If setloop_ = 1 the hardware is generating triggers in a saturation loop.<br>"
       << "If setloop = 0 and softwareTriggerEnable = 0 the hardware expects triggers from the Lemo.<br>"
       << "If setloop = 0 and softwareTriggerEnable = 1 triggers can be given only by software.</p>"
       << std::endl;


  // This method can be invoked using Linux 'wget' command
  // e.g http://lxcmd101:1972/urn:xdaq-application:lid=23/setParameter?value=24
  std::string method = toolbox::toString("/%s/setParameter",getApplicationDescriptor()->getURN().c_str());
  
  
  *out << cgicc::fieldset().set("style","font-size: 10pt;  font-family: arial;") << std::endl;

  *out << cgicc::legend("Set fragment-size-mean (bytes)") << cgicc::p() << std::endl;
  *out << cgicc::form().set("method","GET").set("action", method) << std::endl;
  // copy last values in
  fragSizMean_ = eventSize_ ;
  fragSizStdDev_  = eventSizeStdDev_ ; 
 
  *out << cgicc::input().set("type","text").set("name","fragSizMean").set("value", fragSizMean_.toString())   << std::endl;
  *out << cgicc::input().set("type","text").set("name","fragSizStdDev").set("value", fragSizStdDev_.toString())   << std::endl;
  *out << cgicc::input().set("type","submit").set("value","Apply")  << std::endl;

  *out << cgicc::form() << std::endl;
  *out << cgicc::fieldset();
  
  *out << "</body>"                       << std::endl;
  *out << "</html>"                       << std::endl;

}



void d2s::FEDEmulator::setParameter(xgi::Input * in, xgi::Output * out )
   {

  // ignore when not in appropriate state
  if (! (fsm_.getStateName( fsm_.getCurrentState()) == "Enabled"))
    return ;
  
  try  
    {
      cgicc::Cgicc cgi(in);
      
      // search fragSizMean exported variable
      cgicc::form_iterator iter = cgi.getElement("fragSizMean");
      if (iter != cgi.getElements().end()) {
	
	// retrieve value 
	fragSizMean_ = iter->getIntegerValue();
	
	LOG4CPLUS_DEBUG(getApplicationLogger(),toolbox::toString("setFragSizMean() called value %d",(uint32_t)fragSizMean_));
	{
	  fragSizMean_.value_ &= ~0x7 ;  // round to multiple of 8 
	  
	  if (fragSizMean_.value_ < 16) fragSizMean_.value_ = 16 ; // at least 16
	
	  // update the eventSize_ member filled at config time
	  eventSize_ = fragSizMean_ ;
	}
      }

      // search fragSizStdDev exported variable
      iter = cgi.getElement("fragSizStdDev");
      if (iter != cgi.getElements().end()) {
	
	// retrieve value 
	fragSizStdDev_ = iter->getIntegerValue();
	
	LOG4CPLUS_DEBUG(getApplicationLogger(),toolbox::toString("setFragSizStdDev() called value %d",(uint32_t)fragSizStdDev_));
	// update the  member filled at config time
	eventSizeStdDev_ = fragSizStdDev_ ;
      }
    }
  catch (const std::exception & e)
    {
      XCEPT_RAISE(xgi::exception::Exception, e.what());
    }	      
  

  try 
    {
      fedEmulatorCard_p->resetBit( "start" );  // stop the eFED                
      
      if ( (uint32_t)eventSizeStdDev_ == 0 ) {
	
	LOG4CPLUS_DEBUG(getApplicationLogger(),toolbox::toString("setting fixed event size with size %d",(uint32_t)eventSize_));
	fedEmulatorCard_p->setFixedEventSize( eventSize_, FedSourceId_,deltaT_ );
      } else {
	
	LOG4CPLUS_DEBUG(getApplicationLogger(),
		       toolbox::toString("setting log normal event size\n Parameters :\n    mean size    : %d\n    rms          : %d\n    minimal size : %d\n    maximal size : %d\n    source       : %d\n    deltaT       : %d\n    seed         : %d\n    rank         : %d\n",(uint32_t)eventSize_ ,
					 (uint32_t)eventSizeStdDev_,
					 (uint32_t)minEventSize_,
					 (uint32_t) maxEventSize_,
					 (uint32_t)FedSourceId_,
					 (uint32_t)deltaT_ ,
					 (uint32_t)eventSizeSeed_ ,
					 (uint32_t)eventTableRank_));
	
	fedEmulatorCard_p->setLogNormalEventSize( eventSize_,
						  FedSourceId_,
						  deltaT_,
						  eventTableRank_,
						  eventSizeSeed_,
						  eventSizeStdDev_,
						  minEventSize_,
						  maxEventSize_ );
      }
      
      
      
      // the start is the last to be set, in case of HARDWARELOOP the FEDEmulator will start sending data immediately
      fedEmulatorCard_p->setBit( "start" );                
      
    } 
  catch (HAL::HardwareAccessException& e ) 
    {
      std::string msg( e.what() );
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
    }
  
  // re-display form page 
  this->defaultWebPage(in,out);
  
}

void d2s::FEDEmulator::StopAction( toolbox::Event::Reference e )
  throw(toolbox::fsm::exception::Exception) {

  try 
    {
      fedEmulatorCard_p->resetBit( "start" );
      fedEmulatorCard_p->setBit( "softReset" ); 
    }  
  catch (HAL::HardwareAccessException& e ) 
    {
      std::string msg( e.what() );
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
    }
  stateName_ = "Ready";
}

void d2s::FEDEmulator::ConfigureAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {
  
  uint32_t giiiversion;

  LOG4CPLUS_DEBUG(getApplicationLogger(), "FEDEmulator configure start");

  // expanding pathname for xmlfile
  try 
    {
      
      if ( fedEmulatorCard_p ) {
	LOG4CPLUS_ERROR( getApplicationLogger(), "FEDEmulatorCard already exists !" );
      }

      if ( instantiateByIndex_ ) {
	fedEmulatorCard_p = new FEDEmulatorCard( *addressTable_p, 
						 *busAdapter_, 
						 pciIndex_, 
						 getApplicationLogger() );
	
      } else {

	uint32_t slotread;
	bool foundCard = false;
	
	//
	// Scan PCI to find slot 
	//
	for ( uint32_t i=0;i<16;i++) 
	  {
            try 
	      {
		// open first device, to read the geographic slot
		fedEmulatorCard_p = new FEDEmulatorCard( *addressTable_p, 
							 *busAdapter_, 
							 i, 
							 getApplicationLogger() );
		
		fedEmulatorCard_p->read("geogrSlot", &slotread);
		
		if ( slotread == 0 || slotread > 21 ) 
		  {
		    std::ostringstream msg; 
		    msg <<  "Geographic Slot for pci-index " << i << " badly read:" << slotread << std::endl;
		    XCEPT_RAISE( toolbox::fsm::exception::Exception, msg.str() );
		  }
		
		if( slot_ == slotread ) 
		  {
		    foundCard = true;
		    break;			
		  } 
		else
		  {
		    delete fedEmulatorCard_p;
		  }
	      } 
            catch(HAL::NoSuchDeviceException & e) 
	      {
		LOG4CPLUS_DEBUG (this->getApplicationLogger(),"No card found at index." << i );
	      }
            catch(HAL::HardwareAccessException &e)
	      {
		std::string msg( e.what() );
		XCEPT_RETHROW( toolbox::fsm::exception::Exception, ("Failed to create PCIDevice and read the geo-slot: " + msg), e );
	      }
	  }
	

	if( ! foundCard ) 
	  {
	    std::ostringstream msg; 
	    msg << "No FEDEmulator card in geographic slot " << slot_.toString();
	    XCEPT_RAISE(toolbox::fsm::exception::Exception, msg.str() );
	  }
      
      }
    

      // At this point we should have a working fedemulatorCard:
      LOG4CPLUS_DEBUG (this->getApplicationLogger(),"Found a card in slot " << slot_ );

      fedEmulatorCard_p->setBit( "softReset" );

      fedEmulatorCard_p->read( "compileVersion",&giiiversion );

      LOG4CPLUS_WARN (this->getApplicationLogger(),"giiiversuib is " << giiiversion );
      if(giiiversion< GIIIversion_) {

	std::string msg = toolbox::toString("Not the good firmware, %8x instead of %8x \n",
					    giiiversion,(uint32_t)GIIIversion_);
	LOG4CPLUS_ERROR(getApplicationLogger(), msg);
	XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
      }
      
      LOG4CPLUS_DEBUG(getApplicationLogger(),toolbox::toString("GIII fpga version  %x", giiiversion));
      
      LOG4CPLUS_DEBUG(getApplicationLogger(),toolbox::toString("generated new pseudo fed card in slot  %d",(uint32_t)slot_));
      fedEmulatorCardExists_ = true;
    } 
  catch( HAL::HardwareAccessException& e) 
    {
      std::string msg( e.what() );
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      // XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
      XCEPT_RETHROW( toolbox::fsm::exception::Exception, ("Cannot configure card: " + msg), e );
    } 
  catch (...)
    {
      LOG4CPLUS_ERROR(getApplicationLogger(),"Unknown exception");
    }
      

  /** 
   * Moved from Enable to Configure
   * set the event sizes to fixed size or log normal depending on the 
   * setting of standard deviation parameter
   */

  uint32_t minFragSize = 24;
  if ( eventSize_ < minFragSize ) {
    std::string msg = toolbox::toString("The minimal event size is currently %d\n", minFragSize);
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
  }
  
  try 
    {
      if ( eventPreload_ ) {
	
	expEventFile_ = toolbox::getRuntime()->expandPathName(eventFilePath_);

	fedEmulatorCard_p->preloadEvents( expEventFile_[0].c_str());

      } else {

	if ( (uint32_t)eventSizeStdDev_ == 0 ) {
	
	  LOG4CPLUS_DEBUG(getApplicationLogger(),toolbox::toString("setting fixed event size with size %d",(uint32_t)eventSize_));
	  fedEmulatorCard_p->setFixedEventSize( eventSize_, FedSourceId_,deltaT_ );
	} else {
	  
	  LOG4CPLUS_DEBUG(getApplicationLogger(),
			 toolbox::toString("setting log normal event size\n Parameters :\n    mean size    : %d\n    rms          : %d\n    minimal size : %d\n    maximal size : %d\n    source       : %d\n    deltaT       : %d\n    seed         : %d\n    rank         : %d\n",
					   (uint32_t)eventSize_ ,
					   (uint32_t)eventSizeStdDev_,
					   (uint32_t)minEventSize_,
					   (uint32_t)maxEventSize_,
					   (uint32_t)FedSourceId_,
					   (uint32_t)deltaT_ ,
					   (uint32_t)eventSizeSeed_ ,
					   (uint32_t)eventTableRank_));
	  
	  fedEmulatorCard_p->setLogNormalEventSize( eventSize_,
						    FedSourceId_,
						    deltaT_,
						    eventTableRank_,
						    eventSizeSeed_,
						    eventSizeStdDev_,
						    minEventSize_,
						    maxEventSize_ );




	}

      }

      // TTS config
      fedEmulatorCard_p->write("TTSControl",0); // do not force TTS state
      fedEmulatorCard_p->write("thresholdTTSReady", thresholdTTSReady_);
      fedEmulatorCard_p->write("thresholdTTSBusy", thresholdTTSBusy_);


    } 
  catch (HAL::HardwareAccessException& e ) {
    std::string msg( e.what() );
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
  }

  LOG4CPLUS_DEBUG(getApplicationLogger(),"FEDEmulator configure end");
  stateName_ = "Ready";
  
}

void d2s::FEDEmulator::EnableAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {
  
  LOG4CPLUS_DEBUG(getApplicationLogger(), "FEDEmulator enable start");
  
  try 
    {
      //set event number from counter
      // repeat this in case somebody played with the tts after configure and 
      // forgot to put the bit back.
      fedEmulatorCard_p->write("TTSControl",0); // do not force TTS state
      fedEmulatorCard_p->setBit( "eventCounterOn" );
      fedEmulatorCard_p->write( "triggerNum", initialEventNumber_ );
      
      switch (triggerSource_) {
      case EXTERN:
	fedEmulatorCard_p->write( "softwareTriggerEnable", 0 ); 
	LOG4CPLUS_DEBUG(getApplicationLogger(), "EXTERN trigger set");
	break;
      case SOFTWARE:
	fedEmulatorCard_p->write( "softwareTriggerEnable", 1 );
	LOG4CPLUS_DEBUG(getApplicationLogger(), "SOFTWARE trigger set");
	break;
      case HARDWARELOOP:
	fedEmulatorCard_p->setBit( "setloop" ); 
	LOG4CPLUS_DEBUG(getApplicationLogger(), "HARDWARELOOP trigger set");
	break;
      default:
	LOG4CPLUS_ERROR(getApplicationLogger(), toolbox::toString("INVALID CONFIGURATION of the triggerSource %d",(uint32_t)triggerSource_));
	break;
      }
      
      // the start is the last to be set, in case of HARDWARELOOP the FEDEmulator will start sending data immediately
      fedEmulatorCard_p->setBit( "start" );                
      
    } 
  catch (HAL::HardwareAccessException& e ) {
    std::string msg( e.what() );
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
  }
  
  LOG4CPLUS_DEBUG(getApplicationLogger(),toolbox::toString("FEDEmulator %d enabled", (uint32_t)slot_));
  stateName_ = "Enabled";
}



void d2s::FEDEmulator::SuspendAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {

  try 
    {
      fedEmulatorCard_p->setBit( "wait" );
    } 
  catch (HAL::HardwareAccessException& e ) 
    {
      std::string msg( e.what() );
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
    }
  stateName_ = "Suspended";
}


void d2s::FEDEmulator::ResumeAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {

  try 
    {
      fedEmulatorCard_p->resetBit( "wait" );
    } 
  catch (HAL::HardwareAccessException& e ) 
    {
      std::string msg( e.what() );
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
    }
  stateName_ = "Enabled";
}


void d2s::FEDEmulator::HaltAction(toolbox::Event::Reference e) 
  throw(toolbox::fsm::exception::Exception) {
  
  if( fsm_.getStateName( fsm_.getCurrentState()) != "Halted") { 
    try 
      {
	if(writeFile_) {
	  std::ostringstream strStream;
	  FILE           *file ;
	  time_t  now;
	  struct tm date;

	  now = time(NULL);
	  localtime_r(&now, &date); 
	  std::string date_msg = 
	    toolbox::toString("%04d-%02d-%02d %02d:%02d:%02d", 
			      date.tm_year + 1900, date.tm_mon + 1, 
			      date.tm_mday, date.tm_hour, 
			      date.tm_min, date.tm_sec );
	  
	  FEDEmulator::Get_Counters();  
	
	  try
	    {
	      expMonFile_ = toolbox::getRuntime()->expandPathName(MonFile_);
	      MonFile_ = expMonFile_[0].c_str();
	    }
	  catch (toolbox::exception::Exception& tbe)
	    {
	      XCEPT_RETHROW (xcept::Exception, 
			     "Expansion of path failed, cannot parse path ",tbe);
	    }
    
	  file=fopen( MonFile_.toString().c_str() , "a");
	  
	  if(!file){
	    LOG4CPLUS_ERROR(getApplicationLogger(),"Error opening monitor file");
	  }else {
	    strStream << "Date_"           <<  slot_  << "," <<         date_msg           << std::endl; 
	    strStream << "fedSlot_"        <<  slot_  << "," << std::dec <<  slot_              << std::endl;  
	    strStream << "fedSourceId_"    <<  slot_  << "," << std::dec <<  FedSourceId_       << std::endl;
	    strStream << "fedTrigNum_"     <<  slot_  << "," << std::dec <<  triggerNum_        << std::endl;
	    strStream << "fedLinkNotdown_" <<  slot_  << "," << std::dec <<  linkNotDown_       << std::endl;
	    
	    writeFile_ = 0; 
	    std::string stringToBeWritten = strStream.str();
	    // LOG4CPLUS_DEBUG(getApplicationLogger(),stringToBeWritten );
	    char   *charsToBeWritten = (char*)stringToBeWritten.c_str();
	    fprintf(file, "%s", charsToBeWritten);
	    fflush(file);
	    int closeStatus = fclose(file);
	    if(closeStatus != 0){
	      LOG4CPLUS_WARN(getApplicationLogger(), "Error closing stats file");
	    }
	  }
	}

	fedEmulatorCard_p->resetBit( "start" );
	::usleep(10000); // to avoid trunkated fragments in the generator
	fedEmulatorCard_p->setBit( "softReset" );
      } 
    catch (HAL::HardwareAccessException& e ) 
      {
	std::string msg( e.what() );
	LOG4CPLUS_ERROR(getApplicationLogger(), msg);
	XCEPT_RAISE( toolbox::fsm::exception::Exception, msg );
      }
    
    // delete the addressTable
    
    fedEmulatorCardExists_ = false; 
    
    // delete the fedEmulatorCard
    // Mmmh! no serialization instruction available in C++.
    // Let's hope that the Xeon does not want to be too intelligent...
    // (concerning prefetch truck machin..) 
    delete fedEmulatorCard_p;
    fedEmulatorCard_p = 0;
    
  }
  stateName_ = "Halted";
}


xoap::MessageReference d2s::FEDEmulator::setWriteFile ( xoap::MessageReference message) 
  throw(xcept::Exception) {
  
  writeFile_ = 1; 
  // send back a reply:
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName responseName = envelope.createName( "setWriteFileResponse", "xdaq", XDAQ_NS_URI);
  envelope.getBody().addBodyElement ( responseName );
  return reply;                        
}


xoap::MessageReference d2s::FEDEmulator::readpar ( xoap::MessageReference message) 
  throw(xcept::Exception) {
  
  FEDEmulator::Get_Counters();      
  
  // send back a reply:
  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName responseName = envelope.createName( "readparResponse", "xdaq", XDAQ_NS_URI);
  envelope.getBody().addBodyElement ( responseName );
  return reply;                        
}


//  Format of the setSTTSControl message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:setSTTSControl xmlns:xdaq=\"urn:xdaq-soap:3.0\" control=\"{software|hardware}\"/>
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
xoap::MessageReference d2s::FEDEmulator::setSTTSControl( xoap::MessageReference msg ) 
  throw(xcept::Exception, xoap::exception::Exception) {

  uint32_t control = 0;

  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();

  xoap::SOAPName command("setSTTSControl","","");
  std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
  if ( bodyElements.size() != 1 ) { 
    return makeFaultReply( "setSTTSControlResponse", 
			   toolbox::toString("syntax error in request: wrong bodyElementCount %d", 
					     bodyElements.size())  );
  }
  
  xoap::SOAPName dataName("control","","");
  try {
    std::string controlStr = bodyElements[0].getAttributeValue( dataName );
    if ( controlStr == "software" ) {
      control = 1;
    } else if( controlStr == "hardware" ) {
      control = 0;
    } else {
      control = 0;
      return makeFaultReply( "setSTTSControlResponse", "wrong argument " + controlStr + " (allowed: software|hardware)" );
    }
    
  } catch ( ... ) {
    return makeFaultReply( "setSTTSControlResponse", "no cycles attribute in cycleSTTSPatterns soap request" );
  }

  // do the real work
  try { 
    if ( fedEmulatorCard_p ) {
      // write the STTS Control bit
      fedEmulatorCard_p->write( "TTSControl", control, HAL::HAL_NO_VERIFY );
    } else { 
      LOG4CPLUS_WARN(getApplicationLogger(), toolbox::toString("Currently no HardwareDevice instantiated. No write possible." ));
      return makeFaultReply( "setSTTSControl", "No FEDEmulatorCard object instantiated !" );
    }
  } catch (HAL::HardwareAccessException& e) {
    std::string msg( e.what() );
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    return makeFaultReply( "setSTTSControl", msg );
  }

  // send back a reply:
  return makeReply( "setSTTSControlResponse", "");

}


//  Format of the cycleSTTSPatterns message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:cycleSTTSPatterns xmlns:xdaq=\"urn:xdaq-soap:3.0\" cycles=\"13\" />
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
xoap::MessageReference d2s::FEDEmulator::cycleSTTSPatterns( xoap::MessageReference msg ) 
  throw(xcept::Exception, xoap::exception::Exception) {

  uint32_t cycles = 0;

  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();

  xoap::SOAPName command("cycleSTTSPatterns","","");
  std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
  if ( bodyElements.size() != 1 ) { 
    return makeFaultReply( "cycleSTTSPatternsResponse", 
			   toolbox::toString("syntax error in request: wrong bodyElementCount %d", 
					     bodyElements.size())  );
  }
  
  xoap::SOAPName dataName("cycles","","");
  try {
    std::string cyclesStr = bodyElements[0].getAttributeValue( dataName );
    cycles = strtoul( cyclesStr.c_str(), NULL, 0);
  } catch ( ... ) {
    return makeFaultReply( "cycleSTTSPatternsResponse", "no cycles attribute in cycleSTTSPatterns soap request" );
  }

  // do the real work
  try { 
    if ( fedEmulatorCard_p ) {
      // set the bitmask and the control bit
      for ( uint32_t ic1 = 0; ic1 < cycles; ic1++ ) {
	for ( uint32_t pattern = 0; pattern<16; pattern++ ) {
	  fedEmulatorCard_p->write( "TTSBits", pattern, HAL::HAL_NO_VERIFY );
	  ::usleep(100);
	}
      }

      // reset the STTS Control bit
      fedEmulatorCard_p->write( "TTSBits", 0, HAL::HAL_NO_VERIFY );

    } else { 
      LOG4CPLUS_WARN(getApplicationLogger(), toolbox::toString("Currently no HardwareDevice instantiated. No write possible." ));
      return makeFaultReply( "cycleSTTSPatterns", "No FEDEmulatorCard object instantiated !" );
    }
  } catch (HAL::HardwareAccessException& e) {
    std::string msg( e.what() );
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    return makeFaultReply( "cycleSTTSPatterns", msg );
  }

  // send back a reply:
  return makeReply( "cycleSTTSPatternsResponse", "");

}


//  Format of the setSTTSPattern message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:setSTTSPattern xmlns:xdaq=\"urn:xdaq-soap:3.0\" pattern=\"13\" />
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
xoap::MessageReference d2s::FEDEmulator::setSTTSPattern( xoap::MessageReference msg ) 
  throw(xcept::Exception, xoap::exception::Exception) {

  uint32_t pattern = 0;

  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();

  xoap::SOAPName command("setSTTSPattern","","");
  std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
  if ( bodyElements.size() != 1 ) { 
    return makeFaultReply( "setSTTSPatternResponse", 
			   toolbox::toString("syntax error in request: wrong bodyElementCount %d", 
					     bodyElements.size())  );
  }
  
  xoap::SOAPName dataName("pattern","","");
  try {
    std::string patternStr = bodyElements[0].getAttributeValue( dataName );
    pattern = strtoul( patternStr.c_str(), NULL, 0);
  } catch ( ... ) {
    return makeFaultReply( "setSTTSPatternResponse", "no pattern attribute in setSTTSPattern soap request" );
  }

  // do the real work
  try { 
    if ( fedEmulatorCard_p ) {
      // set the bitmask and the control bit
      pattern = pattern & 0x0f;
      fedEmulatorCard_p->write( "TTSBits", pattern, HAL::HAL_NO_VERIFY );
    } else { 
      LOG4CPLUS_WARN(getApplicationLogger(), toolbox::toString("Currently no HardwareDevice instantiated. No write possible." ));
      return makeFaultReply( "setSTTSPattern", "No FEDEmulatorCard object instantiated !" );
    }
  } catch (HAL::HardwareAccessException& e) {
    std::string msg( e.what() );
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    return makeFaultReply( "setSTTSPattern", msg );
  }

  // send back a reply:
  return makeReply( "setSTTSPatternResponse", "");

}


//  Format of the readItem message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:readItem xmlns:xdaq=\"urn:xdaq-soap:3.0\" item=\"triggerNum\" offset=\"0\"/>
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
xoap::MessageReference d2s::FEDEmulator::readItem( xoap::MessageReference msg ) 
  throw(xcept::Exception, xoap::exception::Exception) {

  uint32_t result = 0;
  uint32_t offset = 0;
  std::string item = "";

  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();

  xoap::SOAPName command("readItem","","");
  std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
  if ( bodyElements.size() != 1 ) { 
    return makeFaultReply( "readItemResponse", "syntax error in request: wrong bodyElementCount" );
  }
  
  xoap::SOAPName offsetName("offset","","");
  try {
    std::string offsetStr = bodyElements[0].getAttributeValue( offsetName );
    offset = strtoul( offsetStr.c_str(), NULL, 0);
  } catch ( ... ) {
    // nothing to be done since no offset is legal
  }

  xoap::SOAPName itemName("item","","");
  try {
    item = bodyElements[0].getAttributeValue( itemName );
  } catch ( ... ) {
    return makeFaultReply( "readItemResponse", 
			   "no item attribute in readItem soap request");
  }

  try { 
    if ( fedEmulatorCard_p ) {
      fedEmulatorCard_p->read( item, &result, offset );
    } else { 
      LOG4CPLUS_WARN(getApplicationLogger(), toolbox::toString("Currently no HardwareDevice instantiated. No read or write possible." ));
      return makeFaultReply( "readItem", "No FEDEmulatorCard object instantiated !" );
    }
  } catch (HAL::HardwareAccessException& e) {
    std::string msg( e.what() );
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    return makeFaultReply( "readItem", msg );
  }

  // send back a reply:
  std::stringstream resultStr;
  resultStr << result << std::ends;
  return makeReply( "readItemResponse", resultStr.str());

}

//  Format of the writeItem message:
//
// <SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
//   <SOAP-ENV:Header>
//   </SOAP-ENV:Header>
//   <SOAP-ENV:Body>
//
//     <xdaq:writeItem xmlns:xdaq=\"urn:xdaq-soap:3.0\" item=\"trigggerNum\" data=\"0x34\" offset=\"0\"/>
//
//   </SOAP-ENV:Body>
// </SOAP-ENV:Envelope>
// 
xoap::MessageReference d2s::FEDEmulator::writeItem( xoap::MessageReference msg ) 
  throw(xcept::Exception, xoap::exception::Exception) {

  uint32_t data   = 0;
  uint32_t offset = 0;
  std::string item = "";

  xoap::SOAPPart part = msg->getSOAPPart();
  xoap::SOAPEnvelope env = part.getEnvelope();
  xoap::SOAPBody body = env.getBody();

  xoap::SOAPName command("writeItem","","");
  std::vector<xoap::SOAPElement> bodyElements = body.getChildElements( command );
  if ( bodyElements.size() != 1 ) { 
    return makeFaultReply( "writeItemResponse", 
			   toolbox::toString("syntax error in request: wrong bodyElementCount %d", 
					     bodyElements.size())  );
  }
  
  xoap::SOAPName dataName("data","","");
  try {
    std::string dataStr = bodyElements[0].getAttributeValue( dataName );
    data = strtoul( dataStr.c_str(), NULL, 0);
  } catch ( ... ) {
    return makeFaultReply( "writeItemResponse", "no data attribute in writeItem soap request" );
  }

  xoap::SOAPName offsetName("offset","","");
  try {
    std::string offsetStr = bodyElements[0].getAttributeValue( offsetName );
    offset = strtoul( offsetStr.c_str(), NULL, 0);
  } catch ( ... ) {
    // nothing to be done since no offset is legal
  }

  xoap::SOAPName itemName("item","","");
  try {
    item = bodyElements[0].getAttributeValue( itemName );
  } catch ( ... ) {
    return makeFaultReply( "writeItemResponse", "no item attribute in writeItem soap request" );
  }

  try { 
    if ( fedEmulatorCard_p ) {
      fedEmulatorCard_p->write( item, data, HAL::HAL_NO_VERIFY, offset );
    } else { 
      LOG4CPLUS_WARN(getApplicationLogger(), toolbox::toString("Currently no HardwareDevice instantiated. No write possible." ));
      return makeFaultReply( "writeItem", "No FEDEmulatorCard object instantiated !" );
    }
  } catch (HAL::HardwareAccessException& e) {
    std::string msg( e.what() );
    LOG4CPLUS_ERROR(getApplicationLogger(), msg);
    return makeFaultReply( "writeItem", msg );
  }

  // send back a reply:
  return makeReply( "writeItemResponse", "");

}


xoap::MessageReference d2s::FEDEmulator::makeReply( std::string command, std::string answerString  ) {

  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName responseName = envelope.createName( command, "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement bodyElement = envelope.getBody().addBodyElement( responseName );
  std::stringstream resultStr;
  resultStr << answerString << std::ends;
  bodyElement.addTextNode( resultStr.str() );
  return reply;
}

xoap::MessageReference d2s::FEDEmulator::makeFaultReply( std::string command, std::string answerString  ) {

  xoap::MessageReference reply = xoap::createMessage();
  xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
  xoap::SOAPName faultName = envelope.createName( "Fault", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement bodyElement = envelope.getBody().addBodyElement( faultName );
  xoap::SOAPName faultCodeName = envelope.createName( "faultcode", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement faultCodeElement = bodyElement.addChildElement( faultCodeName );
  faultCodeElement.addTextNode( "Server" );

  xoap::SOAPName faultStringName = envelope.createName( "faultstring", "xdaq", XDAQ_NS_URI);
  xoap::SOAPElement faultStringElement = bodyElement.addChildElement( faultStringName );
  faultStringElement.addTextNode( answerString );

  return reply;
}

xoap::MessageReference d2s::FEDEmulator::UserTrigger( xoap::MessageReference message ) 
  throw(xcept::Exception) {
  try 
    {
      uint32_t oldState;
      if ( (fsm_.getStateName( fsm_.getCurrentState()) == "Enabled") && ((uint32_t)triggerSource_ == SOFTWARE || (uint32_t)triggerSource_ == EXTERN )  ) {
	
	fedEmulatorCard_p->read( "softwareTriggerEnable", &oldState );
	fedEmulatorCard_p->write( "softwareTriggerEnable", 1 );
	fedEmulatorCard_p->writePulse( "trigger" );
	fedEmulatorCard_p->write( "softwareTriggerEnable", oldState );
	
      } else if ( fsm_.getStateName( fsm_.getCurrentState()) == "Suspended" ) {
	
	if ( (uint32_t)triggerSource_ == HARDWARELOOP )  fedEmulatorCard_p->resetBit( "setloop" );
	fedEmulatorCard_p->read( "softwareTriggerEnable", &oldState );
	fedEmulatorCard_p->write( "softwareTriggerEnable", 1 );
	fedEmulatorCard_p->setBit( "start" );
	fedEmulatorCard_p->writePulse( "trigger" );
	fedEmulatorCard_p->resetBit( "start" );        
	fedEmulatorCard_p->write( "softwareTriggerEnable", oldState );
	if ( (uint32_t) triggerSource_ == HARDWARELOOP )  fedEmulatorCard_p->setBit( "setloop" );  
	
      } else {
	
	LOG4CPLUS_WARN(getApplicationLogger(), toolbox::toString("Could not issue PCI software trigger!! wrong state or triggerSource (%d)",
						  (uint32_t)triggerSource_));
      }
    } 
  catch (HAL::HardwareAccessException& e ) 
    {
      std::string msg( e.what() );
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      XCEPT_RAISE( xcept::Exception, msg );
    }
  
  return makeReply("UserTriggerResponse", "" );                        
}

void d2s::FEDEmulator::Get_Counters()  
  throw(xcept::Exception) {

  uint32_t readparam;

  if ( fedEmulatorCardExists_ ) {
    try 
      {
	fedEmulatorCard_p->read( "pendingTriggers", &readparam); 
	pendingTriggers_ = readparam;
	fedEmulatorCard_p->read( "triggerNum", &readparam );
	triggerNum_ = readparam;
	fedEmulatorCard_p->read( "linkNotFull", &readparam );
	linkNotFull_ = readparam;
	fedEmulatorCard_p->read( "linkNotDown", &readparam );
	linkNotDown_ =  readparam;
	fedEmulatorCard_p->read( "softwareTriggerEnable", &softwareTriggerEnable_ );
	fedEmulatorCard_p->read( "setloop", &setloop_ );
	fedEmulatorCard_p->read( "TTSControl", &readparam );
	softSTTSEnable_ = readparam;
      } 
    catch (HAL::HardwareAccessException& e) 
      {
	std::string msg = e.what();
	LOG4CPLUS_ERROR(getApplicationLogger(), msg);
	XCEPT_RAISE( xcept::Exception, msg );
      }
  } else {
    LOG4CPLUS_WARN(getApplicationLogger(), "Cannot readout fedEmulatorCard since it is not existing");
  }
}


xoap::MessageReference d2s::FEDEmulator::createFsmSoapResponseMsg( const std::string event,const std::string state )
   {

  try 
    {
      xoap::MessageReference message = xoap::createMessage();
      xoap::SOAPEnvelope envelope = message->getSOAPPart().getEnvelope();
      xoap::SOAPBody body = envelope.getBody();
      std::string responseString = event + "Response";
      xoap::SOAPName responseName =
	envelope.createName(responseString, "xdaq", XDAQ_NS_URI);
      xoap::SOAPBodyElement responseElement =
	body.addBodyElement(responseName);
      xoap::SOAPName stateName =
	envelope.createName("state", "xdaq", XDAQ_NS_URI);
      xoap::SOAPElement stateElement =
	responseElement.addChildElement(stateName);
      xoap::SOAPName attributeName =
	envelope.createName("stateName", "xdaq", XDAQ_NS_URI);
      
      stateElement.addAttribute(attributeName, state);
      
      return message;
    }
  catch(xcept::Exception e)
    {
      XCEPT_RETHROW(xcept::Exception,
		    "Failed to create FSM SOAP response message for event:" +
		    event + " and result state:" + state,  e);
    }
}

