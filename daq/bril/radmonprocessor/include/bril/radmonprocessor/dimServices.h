#ifndef DIMSERVICES_H
#define DIMSERVICES_H

#define NCHANNELS 16

//DataData:
#define radmonDataService "I:8;F:16;F:16;F:16;S:16"
typedef struct {
    int tstamp;
    int msec;
    int dt_readout; // internal readout time
    int fill;
    int runnr;
    int lumisection;
    int nibble;
    int dummy;
    float rate[NCHANNELS];        // rates
    float voltage[NCHANNELS];	
    float current[NCHANNELS];
    unsigned short status[NCHANNELS];
} radmon_data_t;

typedef struct {
    float rate;
    float voltage;
    float current;
    unsigned short status;
    unsigned short readouttime;
} radmonraw_topic_t;

//Commands
#define commandStructure "I:8;F:16"
typedef struct {
    int tstamp;
    int msec;
    int command;
    unsigned int cmd_arg;
    int fill;
    int runnr;
    int lumisection;
    int nibble;
    float data[NCHANNELS];
}radmon_cmd_t;



#endif
