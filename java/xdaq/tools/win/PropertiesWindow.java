package xdaq.tools.win;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import xdaq.tools.win.ParameterTableModel;

import xdaq.tools.win.xdaqWin;

import xdaq.tools.win.SetDefaultParametersAction;

//import xdaq.tools.win.ParameterTableRenderer;

import xdaq.tools.win.TimerKnob;
import xdaq.tools.win.QueryParametersAction;
import xdaq.tools.win.SetParametersAction;
import xdaq.tools.win.GetParametersAction;
import xdaq.tools.win.TreeTableModelAdapter;

public class PropertiesWindow extends JFrame
{
	xdaqWin win_;
	JPanel informationPane_;
	JPanel buttonPane_;
	JButton refreshButton_;
	JButton applyButton_  ;
	JButton defaultButton_;
	TreePath[] paths_;
	
	//JTable parameterTable_;
	JTreeTable parameterTable_;
	
	TimerKnob timerKnob_;

	public PropertiesWindow(xdaqWin frame)
	{
		super ("Properties");
		setSize (300, 300);
		win_ = frame;
		
		JTabbedPane tabbed = new JTabbedPane();
		
		// General information tab
		//
		informationPane_ = new JPanel();
		tabbed.addTab ("General", informationPane_);
		
		// Parameter tab
		//
		Box parameterBox = Box.createVerticalBox();
		
		parameterTable_ = new JTreeTable();
		
		JScrollPane parameterScrollPane = new JScrollPane ( parameterTable_ );
		
		parameterBox.add ( parameterScrollPane );
		tabbed.addTab ("Parameters", parameterBox);
		
		// Parameter Buttons
		//
		Box parameterButtons = Box.createHorizontalBox();
		refreshButton_ = new JButton ("Refresh");
		refreshButton_.addActionListener ( new GetParametersAction ( frame, this ) );
		applyButton_ = new JButton ("Apply");
		applyButton_.addActionListener ( new SetParametersAction ( frame, this ) );
		defaultButton_ = new JButton ("Set Default");
		defaultButton_.addActionListener ( new SetDefaultParametersAction ( frame, this ) );
		
		parameterButtons.add ( refreshButton_ );
		parameterButtons.add ( applyButton_   );
		parameterButtons.add ( defaultButton_ );
		
		refreshButton_.setEnabled (true);
		applyButton_.setEnabled (false);
		defaultButton_.setEnabled (true);
		
		parameterBox.add ( parameterButtons );
		
		// General buttons to close the window
		//
		//JButton okButton = new JButton("OK");
		//JButton cancelButton = new JButton ("Cancel");
		
		//JPanel windowButtons = new JPanel();
		//windowButtons.add (okButton);
		//windowButtons.add (cancelButton);
		
		// Periodic update pane
		//
		JPanel timerBox = new JPanel(); //Box.createVerticalBox();
		timerKnob_ = new TimerKnob ( new GetParametersAction ( frame, this ) );
		JLabel help = new JLabel ("Set time to get periodic parameter refresh.");
		timerBox.add ( help, BorderLayout.NORTH );
		timerBox.add ( timerKnob_ );
		timerBox.add ( timerKnob_.getLabel() );
		tabbed.addTab ("Timer", timerBox);
		
		tabbed.addChangeListener ( 
			new QueryParametersAction (tabbed, frame, this) );
		
		this.getContentPane().add (tabbed, BorderLayout.CENTER);
		//this.getContentPane().add (windowButtons, BorderLayout.SOUTH);
		
		rememberTreeSelection();
		
		setInformationPane();
		
		this.show();
		
	}
	
	public void setInformationPane()
	{
		Box infoBox = Box.createVerticalBox();
		
	
	   if (paths_.length == 1) 
	   {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths_[0].getLastPathComponent();
			
		xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();

		if (node.toString().equals("Application") || node.toString().equals("Transport"))
		{
			xdaqWinTreeAdapterNode father = ((xdaqWinTreeAdapterNode) node.getUserObject()).getParent();	
			String targetAddress = aNode.getAttribute ("id");
			String className = aNode.getAttribute ("class");
			String instance = aNode.getAttribute ("instance");
			String url = father.getAttribute ("url");
			
			this.setTitle ("Properties: " + className + " (" + instance + ")");
			
			infoBox.add ( new JLabel ( node.toString() ) );
			infoBox.add ( new JLabel ("Class: " + className));
			infoBox.add ( new JLabel ("Instance:" + instance));
			infoBox.add ( new JLabel ("Target id: " + targetAddress));
			infoBox.add ( new JLabel ("Context url: " + url));
		} 
		else if (node.toString().equals("Context") ) 
		{
			defaultButton_.setEnabled (false);
			
			String id = aNode.getAttribute ("id");
			String url = aNode.getAttribute ("url");
			infoBox.add ( new JLabel ("Context id: " + id) );
			infoBox.add ( new JLabel ("Class: Executive"));
			infoBox.add ( new JLabel ("Target id: 0")); // executive
			infoBox.add ( new JLabel ("Host url: " + url));
			
			this.setTitle ("Properties: Context (" + id +")");
		}
	   } else {
	   	infoBox.add ( new JLabel ( paths_.length + " elements selected.") );	
		this.setTitle ("Properties: " + paths_.length + " items");
	   }
	   
	   informationPane_.add ( infoBox );
	}
	
	public void rememberTreeSelection()
	{
		TreePath[] paths = (TreePath[]) win_.getTree().getSelectionModel().getSelectionPaths();
		
		int pathLength = paths.length;
		paths_ = (TreePath[]) new TreePath [pathLength];
		for (int k = 0; k < pathLength; k++)
		{
			paths_[k] =  new TreePath ( paths[k].getPath() );
		}
	}
	
	public TreePath[] getSelection()
	{
		return paths_;
	}
	
	
	public void setModel ( ParameterTableModel model )
	{
		parameterTable_.setModel (model);
		applyButton_.setEnabled (true);
	}
	
	
	public ParameterTableModel getModel()
	{
		TreeTableModelAdapter adapter = (TreeTableModelAdapter)parameterTable_.getModel();
		return (ParameterTableModel) adapter.getModel();
	}
	
	
	
	
}
