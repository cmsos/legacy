<?php
//phpinfo();
ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );

// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
    $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}
// extract zone and flashlist from RESTful statement
// e.g. http://pc-c2e11-34-01.cms/flashweb/cdaq/hostInfo?fmt=plain
$method = $_SERVER['REQUEST_METHOD'];
$args = explode('/', rtrim( $_GET ["request"], '/'));
$zone = array_shift($args);
$_GET ["zone"] = $zone;
// set default format
if (!array_key_exists('fmt', $_GET)) {
	$_GET ["fmt"] = "plain";
}

switch ($method) {
  case 'GET':
	// if flashlist is provided then get collection, otherwise just catalog for this zone
	if (array_key_exists(0, $args)) {
      		$flashlist = array_shift($args);
		$_GET ["flash"] = 'urn:xdaq-flashlist:' .$flashlist;
		include('retrieveCollection.php');
	}
	else
	{
		include('retrieveCatalog.php');
	}
	break;
  case 'PUT':
  	header("HTTP/1.0 405 Method Not Allowed");
	break;
  case 'POST':
  	header("HTTP/1.0 405 Method Not Allowed");
	break;
  case 'DELETE':
  	header("HTTP/1.0 405 Method Not Allowed");
	break;
}

?>

