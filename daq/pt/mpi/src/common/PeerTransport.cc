// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/exception/Exception.h"

#include "pt/mpi/PeerTransport.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "pt/exception/ReceiveFailure.h"
#include "i2o/i2o.h"
#include "xcept/tools.h"
#include "tcpla/InterfaceAdapterPoll.h"
#include "tcpla/InterfaceAdapterSelect.h"

#include "mpila/mpila.hpp"
#include "mpila/helpers/helpers.hpp"

#include "pt/mpi/I2OHeaderInfo.h"

#include "toolbox/hexdump.h"
#include <thread>

pt::mpi::PeerTransport::PeerTransport (xdaq::Application * owner, toolbox::mem::Pool * pool, int commSize, size_t maxReceiveBuffers, size_t ioQueueSize, size_t maxBlockSize, size_t pipeLineDepth, const std::string & protocol)
	: xdaq::Object(owner), pool_(pool), mutex_(toolbox::BSem::FULL)
{
	this->maxReceiveBuffers_ = maxReceiveBuffers;
	this->ioQueueSize_ = ioQueueSize;
	this->pipeLineDepth_ = pipeLineDepth;
	this->commSize_ = commSize;
	this->maxBlockSize_ = maxBlockSize;
	this->protocol_ = protocol;
	factory_ = toolbox::mem::getMemoryPoolFactory();

	// std::vector<mpila::QueuePair*> queuePairs;

	    cq_ = new mpila::CompletionQueue(commSize_ * ioQueueSize_ * 2);
	    ia_ = new mpila::InterfaceAdapter(pipeLineDepth_, mpila::TransportMode::TEST_SOME);

}

pt::mpi::PeerTransport::~PeerTransport ()
{
	delete ia_;
}


pt::Messenger::Reference pt::mpi::PeerTransport::getMessenger (pt::Address::Reference destination, pt::Address::Reference local)
{
	if (destination->getProtocol() != this->protocol_ || (local->getProtocol() != this->protocol_))
	{
		std::stringstream ss;
		ss << "Cannot handle protocol service combination, destination protocol was :" << destination->getProtocol() << " destination service was:" << destination->getService() << " while local protocol was:" << local->getProtocol() << "and local service was:" << local->getService();

		XCEPT_RAISE(pt::exception::UnknownProtocolOrService, ss.str());
	}

	// Look if a messenger from the local to the remote destination exists and return it.
	// If it doesn't exist, create it and return it.
	// It accept the service to be null, in this case, it assume that it is soap service. Of course this is not secure but it provide a
	// useful omission as default
	if ((destination->getService() == "i2o") && (local->getService() == "i2o"))

	{

		// Create new messenger
		if (local->equals(destination))
		{
			// Creating a local messenger (within same logical host == executive) should not be supported.
			// In this case another transport protocol should be used.
			//
			// http::SOAPLoopbackMessenger* m = new http::SOAPLoopbackMessenger(destination, local);
			// soapMessengers_.push_back(m);
			// return m;
			std::stringstream msg;
			msg << "MPI protocol communication within the same XDAQ process is not supported. Use the fifo transport for local communication.";
			XCEPT_RAISE(pt::exception::UnknownProtocolOrService, msg.str());
		}
		else
		{
			pt::mpi::Address & destAddr = dynamic_cast<pt::mpi::Address&>(*destination);

			//mpila::InterfaceAdapter * ia = 0;

			//pt::Messenger::Reference m;

			mutex_.take();

			for (std::list<pt::Messenger::Reference>::iterator mi = messengers_.begin(); mi != messengers_.end(); mi++)
			{
				pt::Messenger::Reference refcnt = *mi;
				pt::mpi::I2OMessenger * messenger = dynamic_cast<pt::mpi::I2OMessenger*>(&(*refcnt));
				// this is not precise enough, it fails if you have two processes on the same machine
				//if ( messenger->getDestinationAddress()->toString() == destination->toString() )

				pt::mpi::Address & currentAddr = dynamic_cast<pt::mpi::Address&>(*(messenger->getDestinationAddress()));

				if (currentAddr.getRank() == destAddr.getRank())
				{
					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Messenger already created then use cached");

					pt::Messenger::Reference mr;
					mr = *mi;

					mutex_.give();

					return mr;
				}
			}

			std::stringstream ss;
			ss << "create Messenger for endpoints:" << destination->toString() << " - " << local->toString();
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());

			// find QP for given destination
			int rank = ::atoi(destAddr.getRank().c_str());
			mpila::QueuePair * qp = queuePairs_[rank];

			pt::mpi::I2OMessenger* m = new pt::mpi::I2OMessenger(this, qp, local, destination);
			pt::Messenger::Reference mr(m);

			messengers_.push_back(mr); // we remember any created messenger

			mutex_.give();


			return mr;
		}
	}
	else
	{
		std::stringstream ss;
		ss << "Cannot handle protocol service combination, destination protocol was :" << destination->getProtocol() << " destination service was:" << destination->getService() << " while local protocol was:" << local->getProtocol() << "and local service was:" << local->getService();

		XCEPT_RAISE(pt::exception::UnknownProtocolOrService, ss.str());
	}

}


pt::TransportType pt::mpi::PeerTransport::getType ()
{
	return pt::SenderReceiver;
}

pt::Address::Reference pt::mpi::PeerTransport::createAddress (const std::string& url, const std::string& service)
{
	//std::cout << "pt::mpi::PeerTransport::createAddress URL:" << url << " Service: " << service << std::endl;
	std::map<std::string, std::string, std::less<std::string> > plist;

	toolbox::net::URL urltmp(url);

	plist["protocol"] = urltmp.getProtocol();
	plist["service"] = service;
	plist["rank"] = urltmp.getHost();


	return this->createAddress(plist);
}

pt::Address::Reference pt::mpi::PeerTransport::createAddress (std::map<std::string, std::string, std::less<std::string> >& address)
{

	std::string protocol = address["protocol"];

	if (protocol == this->protocol_)
	{
		//port scanning here

		return pt::Address::Reference(new pt::mpi::Address(address));
	}
	else
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}
}

std::string pt::mpi::PeerTransport::getProtocol ()
{
	return this->protocol_;
}


void pt::mpi::PeerTransport::config (pt::Address::Reference address)
{

	int commRank;
	MPI_Comm_rank(MPI_COMM_WORLD, &commRank);

	try
	{
		mutex_.take();

		for (int rank = 0; rank < commSize_; rank++)
		{
			if (commRank != rank) // may be the itself can be enabled
			{
				mpila::QueuePair * qp = ia_->createQueuePair(rank, ioQueueSize_, cq_);
				queuePairs_.emplace_back(qp);
				for (size_t j = 0; j < maxReceiveBuffers_; j++)
				{
					toolbox::mem::Reference * reference;
					try
					{
						reference = factory_->getFrame(pool_, maxBlockSize_);
					}
					catch (toolbox::mem::exception::Exception & ex)
					{
						std::stringstream msg;
						msg << "Failed to allocate buffer for QuePair on rank " << rank;
						XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
						this->getOwnerApplication()->notifyQualified("fatal", e);
						return;
					}

					mpila::WorkRequest recvRequest;
					recvRequest.memoryBuffer =  (mpila::memoryBuffer_t*) reference->getDataLocation();
					recvRequest.cookie = reference;
					recvRequest.size = maxBlockSize_;
					recvRequest.tag = MPI_ANY_TAG;
					recvRequest.tag = 0;
					recvRequest.context = qp;
					recvRequest.workRequestType = mpila::WorkRequestType::RECV;
					qp->post(recvRequest);
				}

				//sendPools.push_back(std::make_unique<mpila::MemoryPool>(poolSize, maxBlockSize, commRank));
				//recvPools.push_back(std::make_unique<mpila::MemoryPool>(poolSize, maxBlockSize, commRank));
			}
			else
			{
				queuePairs_.emplace_back(nullptr);
			}
		}

		std::thread transport([this]()
		{
			mpila::helpers::io::setThreadAffinity(9);
			while (true)
			{
				(*this->ia_)();
			}
		});
		transport.detach();
		std::thread completion([this]()
		{
			mpila::helpers::io::setThreadAffinity(10);
			while (true)
			{
				(*this)();
			}
		});
		completion.detach();

		//mpila::Address & a = dynamic_cast<mpila::Address &>(*address);

		mutex_.give();

	}
	catch (pt::mpi::exception::Exception &e)
	{
		mutex_.give();
		XCEPT_RETHROW(pt::exception::Exception, "Failed to configure receiver port in IA", e);

	}
}

void pt::mpi::PeerTransport::operator()()
{
	if (!cq_->empty())
	{

		mpila::CompletionEvent completionEvent = cq_->front();
		cq_->pop_front();

		toolbox::mem::Reference * reference = (toolbox::mem::Reference*)completionEvent.workRequest.cookie;

		if (completionEvent.workRequest.workRequestType == mpila::WorkRequestType::SEND)
		{
			// count send completions
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "got completion for send");
			if (reference) reference->release();
			return;

		}
		else if (completionEvent.workRequest.workRequestType == mpila::WorkRequestType::RECV)
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "got completion for recv");

			// prepost new buffer
			auto qp = reinterpret_cast<mpila::QueuePair*>(completionEvent.workRequest.context);

			mpila::WorkRequest recvRequest;
			toolbox::mem::Reference * newreference;
			try
			{
				newreference = factory_->getFrame(pool_, maxBlockSize_);
			}
			catch (toolbox::mem::exception::Exception & ex)
			{
				std::stringstream msg;
				msg << "Failed to allocate buffer for QueuePair on rank " << qp->getRank();
				XCEPT_DECLARE_NESTED(pt::exception::ReceiveFailure, e, msg.str(), ex);
				this->getOwnerApplication()->notifyQualified("fatal", e);
				return;
			}
			recvRequest.memoryBuffer =  (mpila::memoryBuffer_t*) newreference->getDataLocation();
			recvRequest.cookie = newreference;
			recvRequest.size = maxBlockSize_;
			recvRequest.tag = MPI_ANY_TAG;
			recvRequest.context = qp;
			recvRequest.workRequestType = mpila::WorkRequestType::RECV;

			qp->post(recvRequest);

			if (listener_ != 0)
			{
				//toolbox::hexdump ((void*) ref->getDataLocation(), 256);
				try
				{
					PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) reference->getDataLocation();
					reference->setDataSize(frame->MessageSize << 2);

					//mutex_.take();
					listener_->processIncomingMessage(reference);

					return;

					//mutex_.give();
				}
				catch (pt::exception::Exception & ex)
				{
					//mutex_.give();
					std::string msg = toolbox::toString("Failed to dispatch message to application listener");
					//std::cout << msg << std::endl;
					XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg);
					this->getOwnerApplication()->notifyQualified("fatal", e);
				}
			}
			else
			{
				std::string msg = toolbox::toString("No Listener for i2o found in ReceiverLoop of pt::mpi");
				//std::cout << msg << std::endl;
				XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg);
				this->getOwnerApplication()->notifyQualified("fatal", e);
			}

		}
		else
		{
			std::string msg = toolbox::toString("Invalid mpila::WorkRequestType::RECV");
			//std::cout << msg << std::endl;
			XCEPT_DECLARE(pt::exception::ReceiveFailure, e, msg);
			this->getOwnerApplication()->notifyQualified("fatal", e);

		}
		// error, free reference
		if (reference) reference->release();


	}
}

std::vector<std::string> pt::mpi::PeerTransport::getSupportedServices ()
{
	std::vector<std::string> s;
	s.push_back("i2o");
	return s;
}

bool pt::mpi::PeerTransport::isServiceSupported (const std::string& service)
{

	if ("i2o" == service)
	{
			return true;
	}

	return false;
}

void pt::mpi::PeerTransport::addServiceListener (pt::Listener* listener)
{
	pt::PeerTransportReceiver::addServiceListener(listener);

	if (listener->getService() == "i2o")
	{
			listener_ = dynamic_cast<i2o::Listener *>(listener);
	}
	else
	{
		XCEPT_RAISE(xdata::exception::Exception, "Failed to add listener of unknown type");
	}
}

void pt::mpi::PeerTransport::removeServiceListener (pt::Listener* listener)
{
	pt::PeerTransportReceiver::removeServiceListener(listener);

	if (listener->getService() == "i2o")
	{
		listener_ = 0;
	}
	else
	{

		XCEPT_RAISE(xdata::exception::Exception, "Failed to remove listener of unknown type");
	}
}

void pt::mpi::PeerTransport::removeAllServiceListeners ()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();

	listener_ = 0;
}


// this callback can be invoked by several threads

namespace pt
{
namespace mpi
{
std::ostream& operator<< (std::ostream &sout, pt::mpi::PeerTransport & pt)
{
	pt.mutex_.take();

	sout << "<table class=\"xdaq-table\">" << std::endl;
	sout << "<thead>" << std::endl;
	sout << "<tr>";
	sout << "<td>Rank</td>" <<  std::endl;
	sout << "<td>QP</td>" <<  std::endl;
	sout << "</tr>";
	sout <<"	<thead>" << std::endl;
	sout << "<tbody>" << std::endl;

	for (auto qp : pt.queuePairs_)
	{
		if ( ! qp ) continue; // protect against null pointer for self qp

		sout << "<tr>" << std::endl;
		sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
		sout << qp->getRank() << std::endl;
		sout << "</td>" << std::endl;
		sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
		sout << *qp << std::endl;
		sout << "</td>" << std::endl;
		sout << "</tr>" << std::endl;
	}

	sout << "</tbody>" << std::endl;
	sout << "</table>" << std::endl;

	pt.mutex_.give();

	return sout;
}
}
}
