/***************************************************************************** 
 * File  rhuclient_shm.cpp
 * created on 13.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/



//#include <rhu/rhuclient.hpp>
#include <bril/bcm1fsource/rhuclient.hpp>
#include "bril/bcm1fsource/rhuclient_shm.hpp"
#include <stdexcept>
//#include "tools/bits.hpp"
#include "bril/bcm1fsource/bits.hpp"
#include "boost/make_shared.hpp"
#include "boost/shared_ptr.hpp"
//#include "shm/rhu_shmclient.hpp"
#include "bril/bcm1fsource/rhu_int_shmdata_fwd.hpp" // HACK ADDING THIS HERE
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
//#include <rhu/rhuclient_exceptions.hpp>
//#include <random>
#include <ctime>

namespace rhu {

  namespace shm {

    // Event Implementation

    // fabric method
    PEvent Event::create(PHistogramData ahist, PPostmortemData apostmortem)
    {
      return PEvent(new Event(ahist, apostmortem));
    }

    // if no connection is available, throws an EClientNotConnected exception
    void Client::checkConnection() const
    {
      if (dummy_isConnected)
	std::cout << "CheckConnection: dummy connected" << std::endl;
      else
	std::cout << "CheckConnection: dummy not connected" << std::endl;
    }

    // Connection Handling
    // connect to the rhu-device
    // virtual
    void Client::connect(const std::string& device)
    {
      std::cout << "Dummy connecting to device " << device << std::endl;
      dummy_isConnected = true;
    }

    // Connection Handling
    // connect to the rhu-device
    // virtual
    void Client::connect()
    {
      std::cout << "Dummy connecting" << std::endl;
      dummy_isConnected = true;
    }


    // Reconnect to the rhu-device
    // virtual
    void Client::reconnect()
    {
      std::cout << "Dummy reconnecting." << std::endl;
    }
    // Disconnect from the rhu-device
    // virtual
    void Client::disconnect()
    {
      std::cout << "Dummy disconnecting." << std::endl;
      dummy_isConnected = false;
    }
    // returns true, if connection is active
    // virtual
    bool Client::isConnected() const
    {
      return dummy_isConnected;
    }

    // receive data,

    // -- Configuration

    // Disable/Enable Channels
    // Enables or Disables the given Channel
    // Disabled channels are not included in the acquisition
    // Index must be >=0 <10 or an exception is thrown
    // virtual
    void Client::Config::setChannelEnable(int index, bool enable)
    {
      //      m_parent.checkConnection();

      if (index<0) throw std::range_error("channel index < 0");
      if (index>7) throw std::range_error("channel index > 7");

      // RhuClientDataLock lock(m_parent.m_interface.get());
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.setChannelEnable(index, enable);
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_isChannelEnabled[index] = enable;
    }
    // returns true, if given channel is enabled
    // Index must be >=0 <10 or an exception is thrown
    // virtual
    bool Client::Config::isChannelEnabled(int index) const
    {
      //      m_parent.checkConnection();
      return dummy_isChannelEnabled[index];
    }

    // Configures the Orbit Trigger Delay
    // delay >=0
    // virtual
    void Client::Config::setOrbitTriggerDelay(int delay)
    {
      //      m_parent.checkConnection();
      if (delay<0) throw std::range_error("orbitTriggerDelay < 0");
      if (delay>=14256) throw std::range_error("orbitTriggerDelay >= 14256");

      // RhuClientDataLock lock(m_parent.m_interface.get());
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.orbitTriggerDelay=delay;
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_orbitTriggerDelay = delay;
    }

    // Returns actual Orbit Trigger Delay
    // virtual
    int Client::Config::getOrbitTriggerDelay() const
    {
      //      m_parent.checkConnection();
      //      return m_parent.m_interface->getDeviceConfig().orbitTriggerDelay;
      return dummy_orbitTriggerDelay;
    }

    // Configures Bunch Trigger Delay
    // virtual
    void Client::Config::setBunchTriggerDelay(int delay)
    {
      throw std::runtime_error("set 'bunch trigger delay' not implemented");
      //m_parent.checkConnection();
      //if (delay<0) throw std::range_error("bunchTriggerDelay < 0");
      //if (delay>15) throw std::range_error("bunchTriggerDelay > 15");
      //
      //RhuClientDataLock lock(m_parent.m_interface.get());
      //DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      //config.bunchTriggerDelay=delay;
      //m_parent.m_interface->setDeviceConfig(config);
    }

    // Returns actual Bunch Trigger Delay
    // virtual
    int Client::Config::getBunchTriggerDelay() const
    {
      //m_parent.checkConnection();
      //return m_parent.m_interface->getDeviceConfig().bunchTriggerDelay;
      throw std::runtime_error("get 'bunch trigger delay' not implemented");
    }

    // sets the number of orbits to continue daq after beam abort event
    // virtual
    void Client::Config::setBeamAbortOrbits(int orbits)
    {
      //      m_parent.checkConnection();
      if (orbits<0) throw std::range_error("beam abort orbits < 0");
      if (orbits>15) throw std::range_error("beam abort orbits > 15");

      // RhuClientDataLock lock(m_parent.m_interface.get());
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.beamAbortOrbits=orbits;
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_beamAbortOrbits = orbits;
    }
    // returns beam abort orbits
    // virtual
    int Client::Config::getBeamAbortOrbits() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceConfig().beamAbortOrbits;
      return dummy_beamAbortOrbits;
    }

    // activate beam abort timeout and auto continue
    // if timeout > 0, acquisition will start automatically after N ms
    // if timeout = 0, no auto continuation is active
    // timeout >=0 ; < 65536
    // virtual
    void Client::Config::setBeamAbortTimeout(int timeout)
    {
      //      m_parent.checkConnection();
      if (timeout<0) throw std::range_error("beam abort timeout < 100");
      if (timeout>65535) throw std::range_error("beam abort timeout > 65535");

      // RhuClientDataLock lock(m_parent.m_interface.get());
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.beamAbortTimeout=timeout;
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_beamAbortTimeout = timeout;
    }

    // returns the beam abort timeout setting
    // virtual
    int Client::Config::getBeamAbortTimeout() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceConfig().beamAbortTimeout;

      return dummy_beamAbortTimeout;
    }

    // set Histogram Orbit Counter, the number of orbits to fill into a
    // histogram
    // orbits > 100 ; < 65536
    // virtual
    void Client::Config::setHistogramOrbitCount(int orbits)
    {
      // m_parent.checkConnection();
      if (orbits<100) throw std::range_error("histogram orbit count < 100");
      if (orbits>65535) throw std::range_error("histogram orbit count > 65535");

      // RhuClientDataLock lock(m_parent.m_interface.get());
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.histogramOrbitsThreshold=orbits;
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_histogramOrbitCount = orbits;
    }

    // Returns the Number of Orbits filled into a histogram
    // virtual
    int Client::Config::getHistogramOrbitCount() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceConfig().histogramOrbitsThreshold;

      return dummy_histogramOrbitCount;
    }

    // virtual
    bool Client::Config::isSimulatorEnable() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceConfig().isSimulatorEnabled();
      return dummy_isSimulatorEnable;
    }
    // virtual
    void Client::Config::setSimulatorEnable(bool enable)
    {
      // m_parent.checkConnection();
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.setSimulatorEnabled(enable);
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_isSimulatorEnable = enable;
    }
    // virtual
    bool Client::Config::isSampleModeEdge() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceConfig().isSamplingModeEdge();

      return dummy_isSampleModeEdge;
    }
    // virtual
    void Client::Config::setSampleModeEdge(bool enable)
    {
      // m_parent.checkConnection();
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.setSamplingModeEdge(enable);
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_isSampleModeEdge = enable;
    }
    // virtual
    bool Client::Config::isPostmortemAutoRestart() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceConfig().isPostmortemRestart();

      return dummy_isPostmortemAutoRestart;
    }
    // virtual
    void Client::Config::setPostmortemAutoRestart(bool enable)
    {
      // m_parent.checkConnection();
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.setPostmortemRestart(enable);
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_isPostmortemAutoRestart = enable;
    }

    // // Returns a Device Config Listener
    // // virtual
    // DeviceConfigListener Client::Config::getConfigListener()
    // {
    //   return m_parent.m_interface->getDeviceConfigListener();
    // }


    // --- Get Status and Statistics

    // // returns the actual orbit counter
    // //  - attention, this is only snapshot of a 10KHz Counter
    // //    at the moment of reading the value, the counter will have changed
    // // virtual
    // u64_t Client::Status::getActualOrbitCounter() const
    // {
    //   m_parent.checkConnection();
    //   return m_parent.m_interface->getDeviceStatus().actualOrbitCounter;
    // }

    // // resets the orbit counter to 0
    // // virtual
    // void  Client::Status::resetOrbitCounter()
    // {
    //   m_parent.checkConnection();
    //   m_parent.m_interface->getCommandInterface().resetOrbitCounter();
    // }

    // // returns the statistic of the module
    // // virtual
    // void Client::Status::getStatistic(Statistic& statistic) const
    // {
    //   m_parent.checkConnection();
    //   statistic.histogramOverflowCount=m_parent.m_interface->getDeviceStatus().histogramOverflowCount;
    //   statistic.irqCount=m_parent.m_interface->getDeviceStatus().irqCount;
    // }

    // returns true, if orbit trigger is available
    // virtual
    bool Client::Status::isOrbitTriggerAvailable() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceStatus().isOrbitTriggerOk();

      return dummy_isOrbitTriggerAvailable;
    }

    // returns true, if bunch clock is available
    // virtual
    bool Client::Status::isBunchClockAvailable() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceStatus().isBunchClockLocked();

      return dummy_isBunchClockAvailable;
    }

    // returns the actual board firmware revision
    // virtual
    unsigned short Client::Status::getFirmewareRevision() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceStatus().firmwareRevision;

      return dummy_firmwareRevision;
    }

    // returns the actual board firmware revision
    // virtual
    unsigned short Client::Status::getTemperature() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceStatus().temperature;

      return dummy_temperature;
    }

    // // Returns a Device Config Listener
    // // virtual
    // DeviceStatusListener Client::Status::getStatusListener()
    // {
    //   m_parent.checkConnection();
    //   return m_parent.m_interface->getDeviceStatusListener();
    // }


    // --- State Control of the Module

    // returns actual operation mode
    // virtual
    DeviceMode Client::DAQ::getOperationMode() const
    {
      // m_parent.checkConnection();
      // return m_parent.m_interface->getDeviceConfig().mode;

      return dummy_operationMode;
    }

    // sets the operation mode
    // virtual
    void Client::DAQ::setOperationMode(DeviceMode mode)
    {
      // m_parent.checkConnection();
      // //m_parent.m_interface->getCommandInterface().setOperationMode(mode);
      // DeviceConfig config=m_parent.m_interface->getDeviceConfig();
      // config.mode=mode;
      // m_parent.m_interface->setDeviceConfig(config);

      dummy_operationMode = mode;
    }

    // --- Event Handling

    // Waits for an Event from the rhu-device
    // returns 0 if data is available
    // returns error code on error
    // virtual
    error_code_t Client::DAQ::waitForEvent(int timeout, PEvent& event)
    {
      EventEntry dum;
      // m_parent.checkConnection();
      // if (m_parent.m_interface->waitForEvent(dum, timeout)) {
      //	Event::PHistogramData histdata;
	Event::PPostmortemData postdata;

	// HERE'S WHERE WE START THE DATA HACKING
	sleep(1);

	// std::default_random_engine generator;
	// std::poisson_distribution<u16_t> distribution(1000);

	std::srand( time(0) );
	rand();

	HistogramData dummy_HistogramData;
	for (int index = 0; index < HIST_FAST_CHANNEL_COUNT; index++)
	  {
	    dummy_HistogramData.fastChannel[index].valid = true;
	    dummy_HistogramData.fastChannel[index].channel = index;
	    dummy_HistogramData.fastChannel[index].orbitCounter = 999999;
	    dummy_HistogramData.fastChannel[index].orbits = 9999;
	    for (int nBin = 0; nBin < HIST_FAST_BIN_COUNT; nBin++)
	      {
		//		dummy_HistogramData.fastChannel[index].bins[nBin] = distribution(generator);
		dummy_HistogramData.fastChannel[index].bins[nBin] = rand()%10000;
	      }
	  }
	for (int index = 0; index < HIST_SLOW_CHANNEL_COUNT; index++)
	  {
	    dummy_HistogramData.slowChannel[index].valid = true;
	    dummy_HistogramData.slowChannel[index].channel = index;
	    dummy_HistogramData.slowChannel[index].orbitCounter = 999999;
	    dummy_HistogramData.slowChannel[index].orbits = 9999;
	    for (int nBin = 0; nBin < HIST_SLOW_BIN_COUNT; nBin++)
	      {
		//		dummy_HistogramData.slowChannel[index].bins[nBin] = distribution(generator);
		dummy_HistogramData.slowChannel[index].bins[nBin] = rand()%10000;
	      }
	  }
	boost::posix_time::ptime t(boost::posix_time::microsec_clock::universal_time());
	//	time_t t(0);
	dummy_HistogramData.timeStamp = t;

	Event::PHistogramData histdata = boost::make_shared<HistogramData>(dummy_HistogramData);

	// END HACKING

	// Check for hist data
	// if (dum.histogramIndex>=0) {
	//   histdata=boost::make_shared<HistogramData>();
	  // m_parent.m_interface->copyHistogramData(dum.histogramIndex,histdata.get());
	// }
	// // Check for postmortem data
	// if (dum.postmortemIndex>=0) {
	//   postdata=boost::make_shared<PostmortemData>();
	//   m_parent.m_interface->copyPostmortemData(dum.postmortemIndex,postdata.get());
	// }
	event=Event::create(histdata,postdata);
	return ERROR_OK;
      // } else {
      // 	return ERROR_TIMEOUT;
      // }
    }

    // --- Postmortem Data Handling

    // Retrieves Postmortem Data
    // index=0, latest Postmortem Buffer
    // index=1, Postmortem Buffer just before latest
    // index = 0..4
    // Returns 0 on success or error code
    // virtual
    error_code_t Client::DAQ::getPostmortemBuffer(int index)
    {
      // m_parent.checkConnection();
      return ERROR_BUFFERS_NOT_AVAILABLE;
    }

    // Returns number of existing Postmortem Buffer that are filled
    // virtual
    int Client::DAQ::getPostmortemBufferCount() const
    {
      // m_parent.checkConnection();
      return 0;
    }

    // returns a reference to the Config Object
    // virtual
    IClient::Config& Client::config()
    {
      // checkConnection();
      return m_config;
    }
    // returns reference to the status object
    // virtual
    IClient::Status& Client::status()
    {
      // checkConnection();
      return m_status;
    }
    // returns reference to the DAQ Object
    // virtual
    IClient::DAQ& Client::daq()
    {
      // checkConnection();
      return m_daq;
    }


  } // namespace shm

} // namespace rhu
