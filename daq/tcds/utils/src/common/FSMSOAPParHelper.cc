#include "tcds/utils/FSMSOAPParHelper.h"

tcds::utils::FSMSOAPParHelper::FSMSOAPParHelper(std::string const& commandName,
                                                std::string const& parameterName,
                                                bool const isRequired// ,
                                                // std::string const& defaultValue
                                                )
  :
  commandName_(commandName),
  parameterName_(parameterName),
  isRequired_(isRequired)// ,
  // defaultValue_(defaultValue)
{
}

tcds::utils::FSMSOAPParHelper::~FSMSOAPParHelper()
{
}

std::string
tcds::utils::FSMSOAPParHelper::commandName() const
{
  return commandName_;
}

std::string
tcds::utils::FSMSOAPParHelper::parameterName() const
{
  return parameterName_;
}

bool
tcds::utils::FSMSOAPParHelper::isRequired() const
{
  return isRequired_;
}

// std::string
// tcds::utils::FSMSOAPParHelper::defaultValue() const
// {
//   return defaultValue_;
// }
