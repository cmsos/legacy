package xdaq.xdesk.jtreeeditor;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.w3c.dom.Node;

public class JTreePopupTrigger
    extends MouseAdapter {
//----------------------------Variables declaration----------------------------
  JPopupMenu popup_menu = null;
  JTreeConfig config = null;
  JTreeEditor tree;
  DefaultMutableTreeNode currentNode;
  JTreeUserObject currentNodeUserObject;
  DefaultMutableTreeNode previousNode;
  JTreeUserObject previousNodeNodeUserObject;
  DefaultMutableTreeNode nextNode;
  JTreeUserObject nextNodeUserObject;
  int index, childCount;
  DefaultMutableTreeNode parentNode;
  TreePath currentPath;
  int x, y;

//---------------------------Constructor----------------------------------------
  /**Constructor*/
  public JTreePopupTrigger() {
    super();
  }

//******************************************************************************
//Prevent ROOT Element collapse
   public void mousePressed(MouseEvent e) {
     if (e.getClickCount() > 1 && (e.getSource() instanceof JTreeEditor)) {
       JTreeEditor tree = (JTreeEditor) e.getSource();
       if (tree.getSelectionPath() == null) {
         return;
       }
       DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.
           getSelectionPath().getLastPathComponent();
       if (node.isRoot()) {
         tree.expandRow(0);
       }
     }
   }

//******************************************************************************
   /**Creates and shows the Popup Menu by right mouse click*/
   public void mouseReleased(MouseEvent e) {
     if (!e.isPopupTrigger()) {
       return;
     }
     x = e.getX();
     y = e.getY();
     tree = (JTreeEditor) e.getSource();
     config = tree.getConfiguration();
     currentPath = tree.getPathForLocation(x, y);
     if (currentPath == null) {
       return;
     }
     currentNode = (DefaultMutableTreeNode) currentPath.getLastPathComponent();
     currentNodeUserObject = (JTreeUserObject) currentNode.getUserObject();
     previousNode = null;
     previousNodeNodeUserObject = null;
     nextNode = null;
     nextNodeUserObject = null;
     parentNode = (DefaultMutableTreeNode) currentNode.getParent();
     try {
       index = parentNode.getIndex(currentNode);
       childCount = parentNode.getChildCount();
     }
     catch (Exception ex) {
       index = 0;
       childCount = 0;
     }

     previousNode = ( (index - 1) >= 0) ?
         (DefaultMutableTreeNode) parentNode.getChildAt(index - 1) : null;
     nextNode = ( (index + 1) < childCount) ?
         (DefaultMutableTreeNode) parentNode.getChildAt(index + 1) : null;

     previousNodeNodeUserObject = (previousNode == null) ?
         currentNodeUserObject :
         (JTreeUserObject) previousNode.getUserObject();
     nextNodeUserObject = (nextNode == null) ? currentNodeUserObject :
         (JTreeUserObject) nextNode.getUserObject();

     tree.setSelectionPath(currentPath);
     currentNode = (DefaultMutableTreeNode) currentPath.
         getLastPathComponent();
     popup_menu = new JPopupMenu();
     buildPopup();

     if (popup_menu.getComponentCount() > 0) {
       popup_menu.show(tree, x, y);
     }

   }

//******************************************************************************
   private void buildPopup() {
     /*--------------------If the tree is not editable----------------------*/
     if (!tree.isEditable()) {
       popup_menu.add(getAction("Start editing", "", true));
       popup_menu.addSeparator();
       if (!currentNode.isLeaf() && !currentNode.isRoot()) {
         if (tree.isExpanded(currentPath)) {
           popup_menu.add(getAction("Collapse", "Collapse", true));
         }
         else {
           popup_menu.add(getAction("Expand", "Expand", true));
         }
       }

       if (!lastItemIsSeparator()) {
         popup_menu.addSeparator();
       }
       popup_menu.add(getAction("Load GUI configuration",
                                "LoadGUIconfiguration", true));

       popup_menu.add(getAction("Save GUI configuration",
                                "SaveGUIconfiguration", true));

       popup_menu.add(getAction("Load default GUI configuration",
                                "LoaddefaultGUIconfiguration", true));
       return;
     }

     /*--------------------If the tree is editable--------------------------*/

     popup_menu.add(getAction("Stop editing", "", true));
     popup_menu.addSeparator();

     if (!currentNode.isLeaf() && !currentNode.isRoot()) {
       String tmpActionName = (tree.isExpanded(currentPath)) ? "Collapse" :
           "Expand";
       popup_menu.add(getAction(tmpActionName, tmpActionName, true));
     }

     if (!lastItemIsSeparator()) {
       popup_menu.addSeparator();
     }

     popup_menu.add(getAction("Cut", "Cut", isCutAllowed()));

     popup_menu.add(getAction("Copy", "Copy", isCopyAllowed()));

     JTreePopupAction pasteAction = getAction("Paste", "Paste", true);
     pasteAction.setEnabled(isPasteAllowed(pasteAction));
     popup_menu.add(pasteAction);

     popup_menu.addSeparator();

     if (isAllowInsertElementBefore()) {
       popup_menu.add(getAction("Insert Element Before", "InsertElementBefore", true));

     }
     if (isAllowInsertElementAfter()) {
       popup_menu.add(getAction("Insert Element After", "InsertElementAfter", true));
     }

     if (!lastItemIsSeparator()) {
       popup_menu.addSeparator();
     }
//submenu
     if ( ( (JTreeUserObject) currentNode.getUserObject()).getType() ==
         org.w3c.dom.Node.ELEMENT_NODE) {
       JMenu addMenu = new JMenu(getAction("Add..", "Add", true));
       if (isAllowElement()) {
         addMenu.add(getAction("Element", "Element", true));
       }
       if (isAllowAttributes()) {
         addMenu.add(getAction("Attribute", "Attribute", true));
       }
       if (isAllowText()) {
         addMenu.add(getAction("Text", "Text", true));
       }
       addMenu.add(getAction("Comment", "Comment", true));
       addMenu.add(getAction("CData", "CData", true));
       addMenu.add(getAction("Processing Instruction", "ProcessingInstruction", true));
       popup_menu.add(addMenu);
     }

     if (!lastItemIsSeparator()) {
       popup_menu.addSeparator();
     }

     if (isMoveUpAllowed()) {
       popup_menu.add(getAction("Move Up", "MoveUp", true));
     }
     if (isMoveDownAllowed()) {
       popup_menu.add(getAction("Move Down", "MoveDown", true));
     }

     if (!lastItemIsSeparator()) {
       popup_menu.addSeparator();
     }

     if (isDeleteAllowed()) {
       popup_menu.add(getAction("Delete", "Delete", true));
     }
     if (!lastItemIsSeparator()) {
       popup_menu.addSeparator();
     }

     popup_menu.add(getAction("Normalize tree", "Normalizetree", true));

     if (!lastItemIsSeparator()) {
       popup_menu.addSeparator();
     }

     popup_menu.add(getAction("Load GUI configuration", "LoadGUIconfiguration", true));
     popup_menu.add(getAction("Save GUI configuration", "SaveGUIconfiguration", true));
     popup_menu.add(getAction("Load default GUI configuration",
                              "LoaddefaultGUIconfiguration", true));
     tree.add(popup_menu);
   }

//******************************************************************************
   /**Returns true if the last item of menu is Separator*/
   private boolean lastItemIsSeparator() {
     if (popup_menu.getComponentCount() > 0) {
       return (popup_menu.getComponent(popup_menu.getComponentCount() - 1).
               getClass() == JPopupMenu.Separator.class);
     }
     return false;
   }

//******************************************************************************
   /**Creates menu Action and associates an icon according to tag name*/
   private JTreePopupAction getAction(String actionName, String iconName,
                                      boolean isEnabled) {
     JTreePopupAction retAction = null;
     ImageIcon icon = null;
     if (iconName.length() > 0) {
       icon = (ImageIcon) config.getPupupIcon(iconName);
     }
     retAction = new JTreePopupAction(actionName, icon, tree);
     retAction.setEnabled(isEnabled);
     return retAction;
   }

//******************************************************************************
   /**Returns true if copy action is allowed for this type of node*/
   boolean isCopyAllowed() {
     return (!currentNode.isRoot() && currentNodeUserObject.getType() != 2);
   }

//******************************************************************************
   /**Returns true if cut action is allowed for this type of node*/
   boolean isCutAllowed() {
     return (!currentNode.isRoot() && currentNodeUserObject.getType() != 2);
   }

//******************************************************************************
   /**Returns true if paste action is allowed for this type of node*/
   boolean isPasteAllowed(JTreePopupAction action) {
     if (action.getBuffer() == null) {
       return false;
     }
     if (currentNodeUserObject.getType() != 1 ||
         (action.getBuffer().equals(currentNode) && action.isCuted())) {
       return false;
     }
     return true;
   }

//******************************************************************************
   /**Returns true if there is a possibility to move the node down*/
//returns true if current selected node is not a root
//and not the last element in parent's child collections
   boolean isMoveDownAllowed() {
     if (currentNode.isRoot()) {
       return false;
     }
     if (index == (childCount - 1)) {
       return false;
     }
     if (nextNode == null) {
       return false;
     }
     if ( (currentNodeUserObject.getType() != nextNodeUserObject.getType()) &&
         (currentNodeUserObject.getType() == Node.ATTRIBUTE_NODE ||
          nextNodeUserObject.getType() == Node.ATTRIBUTE_NODE)) {
       return false;
     }
     return true;
   }

//==============================================================================
//return true if current selected node is not a root
//and not the top element in parent's child collections
  /**Returns true if there is a pissibility to move the node up*/
  boolean isMoveUpAllowed() {
    if (currentNode.isRoot()) {
      return false;
    }
    if ( (currentNodeUserObject.getType() !=
          previousNodeNodeUserObject.getType()) &&
        (currentNodeUserObject.getType() == Node.ATTRIBUTE_NODE ||
         previousNodeNodeUserObject.getType() == Node.ATTRIBUTE_NODE)) {
      return false;
    }
    if (previousNode == null) {
      return false;
    }
    return true;
  }

//==============================================================================

  /**Returns true if delete action is allowed for the current node*/
  boolean isDeleteAllowed() {
    return! (currentNode.isRoot());
  }

//==============================================================================
  /**Returns true if there is a possibility to add a node before the current node*/
  boolean isAllowInsertElementBefore() {
    JTreeUserObject userObject = (JTreeUserObject) currentNode.
        getUserObject();
    boolean returnValue = true;
    if (currentNode.isRoot() || userObject.getType() != 1) {
      returnValue = false;
    }
    return returnValue;
  }

//==============================================================================
  /**Returns true if there is a possibility to add a node after the current node*/
  boolean isAllowInsertElementAfter() {
    return isAllowInsertElementBefore();
  }

//==============================================================================
  /**Returns true if an XML tag attached to the current node allows attributes*/
  boolean isAllowAttributes() {
    JTreeUserObject userObject = (JTreeUserObject) currentNode.
        getUserObject();
    return (userObject.getType() == 1);
  }

//==============================================================================
  /**Return true if there is a possibility to attache element to the current node*/
  boolean isAllowElement() {
    JTreeUserObject userObject = (JTreeUserObject) currentNode.
        getUserObject();
    return (userObject.getType() == 1);
  }

//==============================================================================
  /**Returns true if there is a posibility to attach a text element to the DOM
   * element which is attached to the current tree node
   */
  boolean isAllowText() {
    boolean retValue = true;
    if (childCount > 0) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) parentNode.
          getLastChild();
      JTreeUserObject object = (JTreeUserObject) node.getUserObject();
      if (object.getType() == org.w3c.dom.Node.TEXT_NODE) {
        return false;
      }
    }
    return retValue;
  }
//==============================================================================
}
