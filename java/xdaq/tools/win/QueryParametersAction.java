package xdaq.tools.win;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;
import org.w3c.dom.*;
import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.ParameterTableModel;
import xdaq.tools.win.TreeTableModelAdapter;
public class QueryParametersAction implements ChangeListener
{
	xdaqWin win_;
	ParameterTableModel model_;
	PropertiesWindow propertiesWin_;
	JTabbedPane pane_;

	public QueryParametersAction (JTabbedPane pane, xdaqWin win, PropertiesWindow propertiesWin)
	{
		pane_ = pane;
		win_ = win;
		propertiesWin_ = propertiesWin;
		model_ = null;
	}
	
	public SOAPMessage sendCommand (String targetAddr, String url) 
	{
			try {
				xdaqSOAPMessage msg =  new xdaqSOAPMessage ( "ParameterQuery", targetAddr );
				xdaqSOAPConnection con = new xdaqSOAPConnection();
				
			//statusPane_.advanceProgressBar (1);
			
				return con.call ( msg, url );
				
			} catch (xdaqSOAPException e) {
				System.out.println (e);
			} 
			
			return null;
	}
	
	public void stateChanged ( ChangeEvent e )
	{
		// If the parameter pane was selected
		// issue a ParameterQuery
		//
		if ( (pane_.getSelectedIndex() == 1) && (model_ == null) )
		{
			// Check if the model has already been set.
			// If not, issue the command
			actionPerformed();
		}
	}
	
	public void actionPerformed ()
	{		
		TreePath[] paths = propertiesWin_.getSelection();
		String errorString = new String ();
		
		for (int i = 0; i < paths.length; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();
			
			if (node.toString().equals("Context") || node.toString().equals ("Application") || node.toString().equals ("Transport"))
			{
				String targetAddress;
				String url;
				
				if (node.toString().equals ("Application") || node.toString().equals ("Transport"))
				{
				  	xdaqWinTreeAdapterNode father = ((xdaqWinTreeAdapterNode) node.getUserObject()).getParent();
				  	targetAddress = aNode.getAttribute ("id");
				  	url = father.getAttribute ("url");
				} else {
					targetAddress = new String ("0"); // executive tid
					url = aNode.getAttribute ("url");
				}
				
				// Send the message that returns the parameters
				//	
				SOAPMessage reply = sendCommand ( targetAddress, url );
				if (reply != null)
				{	
					xdaqSOAPMessage replyMsg = new xdaqSOAPMessage(reply);
					Document document = replyMsg.getDOM();
				
					// Check for xdaq error first
					//
					//NodeList faultList = document.getElementsByTagName ("faultstring");
					// Match all faultstring tags in all namespaces
					NodeList faultList = document.getElementsByTagNameNS ("*", "faultstring");
					if (faultList.getLength() == 1)
					{
						errorString += url + " (";
						errorString += faultList.item(0).getFirstChild().getNodeValue();
						errorString += ")\n";
						model_ = null;	
					}
					else {
					 		org.w3c.dom.Node root = document.getDocumentElement();
							NodeList responseList = document.getElementsByTagNameNS ("*","ParameterQueryResponse");
							if (responseList.getLength() == 1)
							{
								// find bag
								NodeList parameters = responseList.item(0).getChildNodes();
								for (int j=0; j < parameters.getLength(); j++ ) {
									if ( parameters.item(j).getLocalName().equals("properties") ) {
										model_ = new ParameterTableModel(parameters.item(j));
										break;
									}
								}
							} else {
								errorString += url + " (";
								errorString += "Query parameters did not contain a response";
								errorString += ")\n";
								model_ = null;	
							}
					}

					
				} else {
					// The action failed...
					model_ = null;
					errorString += url;
					errorString += '\n';
				}
			} else {
				return; // don't update information
			}
		}
		if (model_ == null) {
			JOptionPane.showMessageDialog ( null, errorString, "Connection error", JOptionPane.ERROR_MESSAGE );
			pane_.setSelectedIndex(0); // display the first pane
		} else {
			propertiesWin_.setModel ( model_ );
		}
	}
			
}
