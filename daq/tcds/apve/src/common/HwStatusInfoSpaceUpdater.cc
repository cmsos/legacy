#include "tcds/apve/HwStatusInfoSpaceUpdater.h"

#include "tcds/apve/TCADeviceAPVE.h"

tcds::apve::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                               tcds::apve::TCADeviceAPVE const& hw) :
  tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA(xdaqApp, hw),
  hw_(hw)
{
}

tcds::apve::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

tcds::apve::TCADeviceAPVE const&
tcds::apve::HwStatusInfoSpaceUpdater::getHw() const
{
  return hw_;
}
