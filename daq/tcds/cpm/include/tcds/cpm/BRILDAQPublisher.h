#ifndef _tcds_cpm_BRILDAQPublisher_h_
#define _tcds_cpm_BRILDAQPublisher_h_

#include <stdint.h>
#include <string>

#include "eventing/api/Member.h"

namespace toolbox {
  namespace mem{
    class MemoryPoolFactory;
    class Pool;
  }
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace cpm {

    class LumiNibble;

    class BRILDAQPublisher : public eventing::api::Member
    {

    public:
      BRILDAQPublisher(xdaq::Application& xdaqApp,
                       std::string const& busName,
                       std::string const& topicName);
      virtual ~BRILDAQPublisher();

      void publishBRILDAQTable(uint32_t const swVersion,
                               uint32_t const fwVersion,
                               uint32_t const dataSourceId,
                               tcds::cpm::LumiNibble const& nibble);

    private:
      std::string memPoolName_;
      toolbox::mem::MemoryPoolFactory* memPoolFactoryP_;
      toolbox::mem::Pool* memPoolP_;

      std::string const busName_;
      std::string const topicName_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQPublisher_h_
