<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
 <xp:Application heartbeat="false" class="executive::Application" id="0" group="profile" service="executive" network="local" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
   <logUrl xsi:type="xsd:string">console</logUrl>
   <logLevel xsi:type="xsd:string">INFO</logLevel>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>
 <xp:Application class="pt::utcp::Application" id="20" instance="0" network="local" heartbeat="false" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:pt::utcp::Application" xsi:type="soapenc:Struct">
   <maxBlockSize xsi:type="xsd:unsignedInt">4096</maxBlockSize>
   <committedPoolSize xsi:type="xsd:double">0x500000</committedPoolSize>
   <ioQueueSize xsi:type="xsd:unsignedInt">1024</ioQueueSize>
   <autoConnect xsi:type="xsd:boolean">false</autoConnect>
   <protocol xsi:type="xsd:string">utcp</protocol>
   <maxReceiveBuffers xsi:type="xsd:unsignedInt">4</maxReceiveBuffers>
  </properties>
 </xp:Application>
 <xp:Module>/opt/xdaq/lib/libtcpla.so</xp:Module>
 <xp:Module>/opt/xdaq/lib/libptutcp.so</xp:Module>
 <xp:Application heartbeat="false" class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
   <documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
   <aliasName xsi:type="xsd:string">/directory</aliasName>
   <aliasPath xsi:type="xsd:string">${XDAQ_SETUP_ROOT}/${XDAQ_ZONE}</aliasPath>
   <httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[3]">
    <item xsi:type="soapenc:Struct" soapenc:position="[0]">
        <name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
        <value xsi:type="xsd:string">*</value>
    </item>
    <item xsi:type="soapenc:Struct" soapenc:position="[1]">
        <name xsi:type="xsd:string">Access-Control-Allow-Methods</name>
        <value xsi:type="xsd:string">POST, GET, OPTIONS</value>
    </item>
    <item xsi:type="soapenc:Struct" soapenc:position="[2]">
        <name xsi:type="xsd:string">Access-Control-Allow-Headers</name>
        <value xsi:type="xsd:string">x-requested-with</value>
    </item>
   </httpHeaderFields>
   <expiresByType xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[5]">
   <item xsi:type="soapenc:Struct" soapenc:position="[0]">
    <name xsi:type="xsd:string">image/png</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[1]">
    <name xsi:type="xsd:string">image/jpg</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[2]">
    <name xsi:type="xsd:string">image/gif</name>
    <value xsi:type="xsd:string">PT4300H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[3]">
    <name xsi:type="xsd:string">application/x-shockwave-flash</name>
    <value xsi:type="xsd:string">PT120H</value>
   </item>
   <item xsi:type="soapenc:Struct" soapenc:position="[4]">
    <name xsi:type="xsd:string">application/font-woff</name>
    <value xsi:type="xsd:string">PT8600H</value>
   </item>
   </expiresByType>
         </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>
 <xp:Application heartbeat="false" class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local" logpolicy="inherit" />
 <xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
 <!-- HyperDAQ -->
 <xp:Application heartbeat="false" class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local" logpolicy="inherit"/>
 <xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>
 <!-- XMem probe-->
 <xp:Application class="xmem::probe::Application" id="7" network="local" logpolicy="inherit" />
 <xp:Module>${XDAQ_ROOT}/lib/libxmemprobe.so</xp:Module>
 <xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" port="1810" maxport="1950" autoscan="true" network="snet" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000" publish="true"/>
 <xp:Endpoint protocol="utcp" service="b2in" hostname="localhost" port="2437" network="localnet" smode="select" rmode="select" nonblock="true" sndTimeout="2000" rcvTimeout="2000"/>
 <!-- Sentinel Probe -->
 <!-- xp:Application heartbeat="true" class="sentinel::probe::Application" id="6" network="localnet" group="sentinel" service="sentinelprobe" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:sentinel::probe::Application" xsi:type="soapenc:Struct">
   <sentineldURL xsi:type="xsd:string">utcp://localhost:2437</sentineldURL>
  </properties>
 </xp:Application -->
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libsentinelutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libsentinelprobe.so</xp:Module>
 <!-- Heartbeat Probe -->
 <xp:Application heartbeat="true" class="xmas::heartbeat::probe::Application" id="4" network="localnet" group="sentinel" service="heartbeatprobe" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:xmas::heartbeat::probe::Application" xsi:type="soapenc:Struct">
   <heartbeatdURL xsi:type="xsd:string">utcp://localhost:2437</heartbeatdURL>
   <heartbeatWatchdog xsi:type="xsd:string">PT10S</heartbeatWatchdog>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libb2inutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libwsaddressing.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasutils.so</xp:Module>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasheartbeatprobe.so</xp:Module>
<!-- #include "module/sensorprobe.module" -->
 <!-- Tracer DEAMON -->
 <xp:Application heartbeat="true" class="tracer::tracerd::Application" id="35" network="snet" group="tracer" service="tracerd" logpolicy="inherit">
   <properties xmlns="urn:xdaq-application:tracer::tracerd::Application" xsi:type="soapenc:Struct">
   <publishGroup xsi:type="xsd:string">tracer</publishGroup>
   <brokerWatchdog xsi:type="xsd:string">PT30S</brokerWatchdog>
            <brokerURL xsi:type="xsd:string">utcp://dvsrv-c2f36-09-01.cms:1985</brokerURL>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libtracerd.so</xp:Module>
 <!-- Sentinel DEAMON -->
 <xp:Application heartbeat="true" class="sentinel::sentineld::Application" id="33" network="snet" group="sentinel" service="sentineld" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:sentinel::sentineld::Application" xsi:type="soapenc:Struct">
   <publishGroup xsi:type="xsd:string">sentinel</publishGroup>
   <brokerDialupWatchdog xsi:type="xsd:string">PT30S</brokerDialupWatchdog>
   <brokerURL xsi:type="xsd:string">utcp://dvsrv-c2f36-09-01.cms:1998</brokerURL>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libsentinelsentineld.so</xp:Module>
 <!-- Sensor DEAMON -->
 <xp:Application heartbeat="true" class="xmas::sensord::Application" id="32" network="snet" group="xmas" service="sensord" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:xmas::sensord::Application" xsi:type="soapenc:Struct">
   <autoConfSearchPath xsi:type="xsd:string">${XDAQ_ROOT}/share/${XDAQ_ZONE}/sensor</autoConfSearchPath>
   <publishGroup xsi:type="xsd:string">xmas</publishGroup>
   <brokerDialupWatchdog xsi:type="xsd:string">PT30S</brokerDialupWatchdog>
   <brokerTopicsWatchdog xsi:type="xsd:string">PT3S</brokerTopicsWatchdog>
   <brokerURL xsi:type="xsd:string">utcp://dvsrv-c2f36-09-01.cms:1999</brokerURL>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libxmassensord.so</xp:Module>
 
 <xp:Application heartbeat="true" class="elastic::timestream::Application" id="34" network="snet" group="xmas" service="sensord" logpolicy="inherit">
	 	<properties xmlns="urn:xdaq-application:elastic::timestream::Application" xsi:type="soapenc:Struct">
   <autoConfSearchPath xsi:type="xsd:string">${XDAQ_ROOT}/share/${XDAQ_ZONE}</autoConfSearchPath>
    <elasticsearchShelfIndexStoreType xsi:type="xsd:string">niofs</elasticsearchShelfIndexStoreType>
    <elasticsearchFlashIndexStoreType xsi:type="xsd:string">mmapfs</elasticsearchFlashIndexStoreType>
    <elasticsearchShelfIndexName xsi:type="xsd:string">shelflist</elasticsearchShelfIndexName>
    <elasticsearchFlashIndexName xsi:type="xsd:string">flashlist</elasticsearchFlashIndexName>
    <elasticsearchClusterUrl xsi:type="xsd:string">http://cmsos-iaas-cdaq.cms:8282</elasticsearchClusterUrl>
    <enableESCloud xsi:type="xsd:boolean">false</enableESCloud>
    <ttl xsi:type="xsd:string">60s</ttl>
   <numberOfChannels xsi:type="xsd:string">1</numberOfChannels>
   <httpVerbose xsi:type="xsd:boolean">false</httpVerbose>
   <tcpNoDelay xsi:type="xsd:boolean">true</tcpNoDelay>

		</properties>
	</xp:Application>
		<xp:Module>${XDAQ_ROOT}/lib/libjansson.so</xp:Module>
	
 <xp:Module>/cmsnfshome0/nfshome0/lorsini/devel/baseline13/trunk/daq/elastic/api/lib/linux/x86_64_slc6/libelasticapi.so</xp:Module>
	<xp:Module>/cmsnfshome0/nfshome0/lorsini/devel/baseline13/trunk/daq/elastic/timestream/lib/linux/x86_64_slc6/libelastictimestream.so</xp:Module>
 
 
 <!-- Heartbeat DEAMON -->
 <xp:Application heartbeat="true" class="xmas::heartbeat::heartbeatd::Application" id="31" network="snet" group="heartbeat" service="heartbeatd" logpolicy="inherit">
  <properties xmlns="urn:xdaq-application:xmas::heartbeat::heartbeatd::Application" xsi:type="soapenc:Struct">
   <publishGroup xsi:type="xsd:string">heartbeat</publishGroup>
   <brokerDialupWatchdog xsi:type="xsd:string">PT30S</brokerDialupWatchdog>
   <brokerURL xsi:type="xsd:string">utcp://dvsrv-c2f36-09-01.cms:1996</brokerURL>
  </properties>
 </xp:Application>
 <xp:Module>${XDAQ_ROOT}/lib/libxmasheartbeatheartbeatd.so</xp:Module>
</xp:Profile>
