
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//##ModelId=417FB7400282
#include "jxta/Advertisement.h"
#include "jxta/AdvertisementImpl.h"
#include "jxta_pa.h"
#include "jxta_pga.h"
#include "jxta_pipe_adv.h"
//#include "jxta_mca.h"
#include "jxta_id.h"

// static initialization

std::string jxta::Advertisement::PEER = "jxta:PA";
//##ModelId=417FB74002E6
std::string jxta::Advertisement::GROUP = "jxta:PGA";
//##ModelId=417FB74002F0
std::string jxta::Advertisement::PIPE = "PipeAdvertisement";
//##ModelId=419C78340058
std::string jxta::Advertisement::SVC = "jxta:SvcAdv";   // not standard; implemented by means of the Pipe adv.


jxta::AdvertisementImpl::AdvertisementImpl(Jxta_advertisement* adv): adv_(adv)
{
	JXTA_OBJECT_SHARE(adv_);
	type_ = jxta_advertisement_get_document_name(adv_);

	if(type_ == PIPE) {
		std::string name = jxta_pipe_adv_get_Name((Jxta_pipe_adv*)adv_);
		if(name.find(SVC) == 0)
			type_ = SVC;		
	}	
}

jxta::AdvertisementImpl::AdvertisementImpl(std::string type, std::string xmlDoc): type_(type)
{
	if(type == PEER) {
		adv_ = (Jxta_advertisement*)jxta_PA_new();
		jxta_PA_parse_charbuffer((Jxta_PA*)adv_, (const char*) xmlDoc.c_str(), xmlDoc.length());
	}
	else if(type == GROUP) {
		adv_ = (Jxta_advertisement*)jxta_PGA_new();
		jxta_PGA_parse_charbuffer((Jxta_PGA*)adv_, (const char*) xmlDoc.c_str(), xmlDoc.length());
	}
	else if(type_ == PIPE || type_ == SVC) {
		adv_ = (Jxta_advertisement*)jxta_pipe_adv_new();
		jxta_pipe_adv_parse_charbuffer((Jxta_pipe_adv*)adv_, (const char*) xmlDoc.c_str(), xmlDoc.length());
	}
	else {
		XCEPT_RAISE(jxta::exception::Exception, "Jxta adv type not recognized in building advertisement.");
	}
}

// static "factory" function
jxta::Advertisement::Reference jxta::newModuleAdv(std::string name, std::string desc)
{
	/* The following is the standard implementation; however, MCA are not yet supported in JxtaC,
	so a custom advertisement is currently handled as a pipe advertisement 
	with a name = SVC + # + user provided name */
	/*
	Jxta_id* jmcid;
	jxta_id_moduleclassid_new_1(&jmcid);
    JString* js;
    jxta_id_to_jstring(jmcid, &js);
    std::string xmlMCA = (char *)jstring_get_string(js);
    JXTA_OBJECT_RELEASE(js);
    JXTA_OBJECT_RELEASE(jmcid);
	
	xmlMCA = "<jxta:MCA><MCID>" + xmlMCA + "</MCID><Name>" + name + "</Name><Desc>" + desc + "</Desc></jxta:MCA>";
	Jxta_advertisement* jad = jxta_advertisement_new();
	jxta_advertisement_parse_charbuffer(jad, xmlMCA.c_str(), xmlMCA.length());
	jxta::Advertisement* ad = new jxta::AdvertisementImpl(jad);
	//JXTA_OBJECT_RELEASE(jad);
	return jxta::Advertisement::Reference(ad);
	*/
	name = jxta::Advertisement::SVC + "#" + name;   // this prefix allows distinguishing proper Pipe adv from service adv

	Jxta_id* jmcid;
	JString* tmpString;
	jxta_id_moduleclassid_new_1(&jmcid);
	jxta_id_to_jstring(jmcid, &tmpString);

	Jxta_pipe_adv* jad = jxta_pipe_adv_new();
	jxta_pipe_adv_set_Id(jad, (char*) jstring_get_string(tmpString));
	jxta_pipe_adv_set_Name(jad, (char*) name.c_str());
	//jxta_pipe_adv_set_Description(jad, (char*) desc.c_str());

	jxta::Advertisement* ad = new jxta::AdvertisementImpl((Jxta_advertisement*)jad);
	JXTA_OBJECT_RELEASE(tmpString);
	JXTA_OBJECT_RELEASE(jmcid);
	return jxta::Advertisement::Reference(ad);
}


jxta::AdvertisementImpl::~AdvertisementImpl()
{
	JXTA_OBJECT_RELEASE(adv_);
}

std::string jxta::AdvertisementImpl::getType()
{
	return type_;
}

std::string jxta::AdvertisementImpl::getID()
{
    std::string id;
    Jxta_id* jid;
	if(type_ == PEER)
		jid = jxta_PA_get_PID((Jxta_PA*)adv_);
	else if(type_ == GROUP)
		jid = jxta_PGA_get_GID((Jxta_PGA*)adv_);
	else if(type_ == PIPE || type_ == SVC)
		jid = jxta_pipe_adv_get_pipeid((Jxta_pipe_adv*)adv_);
	else
		return "";

    JString* js;
    jxta_id_to_jstring(jid, &js);
    id = jstring_get_string(js);
    JXTA_OBJECT_RELEASE(js);
    JXTA_OBJECT_RELEASE(jid);
    return id;
}

std::string jxta::AdvertisementImpl::getName()
{
	JString* tmp;
	if(type_ == PEER)
		tmp = jxta_PA_get_Name((Jxta_PA*)adv_);
	else if(type_ == GROUP)
		tmp = jxta_PGA_get_Name((Jxta_PGA*)adv_);
	else if(type_ == PIPE)
		return jxta_pipe_adv_get_Name((Jxta_pipe_adv*)adv_);
	else if(type_ == SVC) {
		std::string name = jxta_pipe_adv_get_Name((Jxta_pipe_adv*)adv_);
		return name.substr(name.find("#")+1);
	}
	else
		return "";
	std::string name = jstring_get_string(tmp);
	JXTA_OBJECT_RELEASE(tmp);
	return name;
}


std::string jxta::AdvertisementImpl::getXmlDocument()
{
	JString* tmp;
	if(type_ == PEER)
		jxta_PA_get_xml((Jxta_PA*)adv_, &tmp);
	else if(type_ == GROUP)
		jxta_PGA_get_xml((Jxta_PGA*)adv_, &tmp);
	else if(type_ == PIPE || type_ == SVC)
		jxta_pipe_adv_get_xml((Jxta_pipe_adv*)adv_, &tmp);
	else
		return "";
	std::string xml = jstring_get_string(tmp);
	JXTA_OBJECT_RELEASE(tmp);
	return xml;
}

// implementation related method
Jxta_advertisement* jxta::AdvertisementImpl::getJxtaAdv()
{
	return adv_;
}

