#include "ferol40/Ferol40WebServer.hh"
#include "ferol40/Ferol40Controller.hh"
#include "ferol40/loggerMacros.h"

ferol40::Ferol40WebServer::Ferol40WebServer( utils::Monitor &monitor, 
        std::string url,
        std::string urn,
        Logger logger )
: utils::WebServer( monitor, urn, url, logger )
{
    utils::WebTableTab *configTab = this->registerTableTab( "Config", 
            "Configuration Items for application and shared hardware components.",
            4);

    configTab->registerTable( "Application Configuration", 
            "Items with which the Ferol40 have been configured.<br>They usually do not change during a run.<br><br>", 
            "Application Config" );

    configTab->registerTable( "Temporary Application Config for Funtion Manager",
            "Items with which are accessed by the Function Manager <br>for configuration and special runs. These should move <br>into the vector of bags once the framework can handle it.",
            "Temp App Config For FM" );


    configTab->registerTable( "FEROL40 Configuration", 
            "Items to configure the FEROL40.<br><br><br>", 
            "Ferol40 Config" );

    configTab->registerTable( "Version State",
            "Items from the Ferol40 Hardware and Firmware Version.<br><br><br>",
            "Version State" );


    utils::WebTableTab *streamsConfigTab = this->registerTableTab( "Inputs Config",
            "Configuration Items for the Input Ports",
            4 );



    //JRF TODO put these in a loop over streams and fill them from the StreamConfigInfoSpace
    //JRF TODO in order to do that we will need a new registerTable Method which contains the stream number.
    //
    streamsConfigTab->registerTable( "Input Port 0 Configuration", 
            "Items to configure the Input Port 0.", 
            "Input Port 0 Config" );

    streamsConfigTab->registerTable( "Input Port 1 Configuration",
            "Items to configure the Input Port 1.",
            "Input Port 1 Config" );

    streamsConfigTab->registerTable( "Input Port 2 Configuration",
            "Items to configure the Input Port 2.",
            "Input Port 2 Config" );

    streamsConfigTab->registerTable( "Input Port 3 Configuration",
            "Items to configure the Input Port 3.",
            "Input Port 3 Config" );

    streamsConfigTab->registerTable( "Input Port 0 Generator Configuration",
            "Items to configure the Input Port 0.",
            "Input Port 0 Gen Config" );

    streamsConfigTab->registerTable( "Input Port 1 Generator Configuration",
            "Items to configure the Input Port 1.",
            "Input Port 1 Gen Config" );


    streamsConfigTab->registerTable( "Input Port 2 Generator Configuration", 
            "Items to configure the Input Port 2.", 
            "Input Port 2 Gen Config" );

    streamsConfigTab->registerTable( "Input Port 3 Generator Configuration",
            "Items to configure the Input Port 3.",
            "Input Port 3 Gen Config" );


    utils::WebTableTab *monTab = this->registerTableTab( "Monitoring", 
            "Monitoring information for various components.", 4  );

    monTab->registerTable( "Application State", 
            "Items from the Ferol40Controller appl.", 
            "Application State" );

    //monTab->registerTable( "FEROL40", 
    //                     "Items FEROL40 Hardware", 
    //                     "FEROL40" );


    // make an empty entry to leave empty this position in the table grid.
    monTab->registerTable( "__notab__", 
            "Dummy", 
            "" );
    monTab->registerTable( "__notab__",
            "Dummy",
            "" );
    monTab->registerTable( "__notab__",
            "Dummy",
            "" );


    monTab->registerTable( "Input Port 0",
            "Items from the input port 0 (SlinkExpress or Generator).",
            "Input Port 0" );

    monTab->registerTable( "Input Port 1",
            "Items from the input port 1 (SlinkExpress or Generator).",
            "Input Port 1" );

    monTab->registerTable( "Input Port 2",
            "Items from the input port 2(SlinkExpress or Generator).",
            "Input Port 2" );

    monTab->registerTable( "Input Port 3",
            "Items from the input port 3 (SlinkExpress or Generator).",
            "Input Port 3" );


    // make an empty entry to leave empty this position in the table grid.
    //    monTab->registerTable( "__notab__", 
    //                         "Dummy", 
    //                         "" );

    monTab->registerTable( "TCP Stream 0", 
            "Items From the Ferol40 TCP/IP Stream 0", 
            "TCP Stream 0" );

    monTab->registerTable( "TCP Stream 1", 
            "Items From the Ferol40 TCP/IP Stream 1", 
            "TCP Stream 1" );


    monTab->registerTable( "TCP Stream 2",
            "Items From the Ferol40 TCP/IP Stream 2",
            "TCP Stream 2" );

    monTab->registerTable( "TCP Stream 3",
            "Items From the Ferol40 TCP/IP Stream 3",
            "TCP Stream 3" );


    //JRF Add a Tab to monitor the QSFP and output Ports info.

    utils::WebTableTab *outputMonTab = this->registerTableTab( "Ouput Mon", 
            "Monitoring information from the output Ports and QSFP.", 4  );

    outputMonTab->registerTable( "Hardware State", 
            "Items from the Ferol40 Hardware", 
            "Hardware State" );

    // make an empty entry to leave empty this position in the table grid.
    outputMonTab->registerTable( "__notab__", 
            "Dummy", 
            "" );
    outputMonTab->registerTable( "__notab__",
            "Dummy",
            "" );
    outputMonTab->registerTable( "__notab__",
            "Dummy",
            "" );


    outputMonTab->registerTable( "Output Port 0",
            "Items from the Output port 0.",
            "Output Port 0" );

    outputMonTab->registerTable( "Output Port 1",
            "Items from the Output port 1.",
            "Output Port 1" );

    outputMonTab->registerTable( "Output Port 2",
            "Items from the Output port 2.",
            "Output Port 2" );

    outputMonTab->registerTable( "Output Port 3",
            "Items from the Output port 3.",
            "Output Port 3" );

    /*JRF TODO, put this back in

      utils::WebTableTab *sfpTab = this->registerTableTab( "SFP+ Info", 
      "Monitoring information for the 10 Gbps SFP+ components.", 2  );

      sfpTab->registerTable( "SFP 1", 
      "Monitoring data of the 10Gb/s SFP+ 1 (upper SFP+).<br>The SFP+ 1 is really the upper one! This is the SFP+ with the 10Gbb/s TCP/IP link to the DAQ.", 
      "FEROL40 SFP 1" );

      sfpTab->registerTable( "SFP 0", 
      "Monitoring data of the 10Gbb/s SFP 0 (lower SFP).<br>The SFP 0 is really the lower one! This is the SFP+ with the potential 10Gb/s input link for the AMC13v2.", 
      "FEROL40 SFP 0" );

      sfpTab->registerTable( "Thresholds SFP 1",
      "Thresholds for various quantities programmed into the SFP+",
      "Thresholds SFP1" );

      sfpTab->registerTable( "Thresholds SFP 0",
      "Thresholds for various quantities programmed into the SFP+",
      "Thresholds SFP0" );
      */
}


    void 
ferol40::Ferol40WebServer::printDebugTable( std::string name, std::list< utils::HardwareDebugItem > regList, xgi::Input *in, xgi::Output *out )
{
    printHeader( out );

    std::list< utils::HardwareDebugItem >::iterator it;

    *out << "<p class=itemTableTitle>" << name << "</p>\n";

    *out << "<table id=\"registerTable\" class=\"tablesorter\">\n<thead><tr><th>item</th><th>value</th><th>offset</th><th>description</th></tr></thead>\n<tbody>\n";
    for( it = regList.begin(); it != regList.end(); it++ )
    {
        *out << "<tr><td>" << (*it).item << "</td><td>" << (*it).valStr << "</td><td>" << (*it).adrStr << "</td><td>" << (*it).description << "</td></tr>\n";
    }
    *out << "</tbody>\n</table>\n";
    *out << "<script>\n $(document).ready(function() \n{\n $('#registerTable').tablesorter( { headers: { 1 : {sorter:false}, 3 : {sorter:false}}, sortList:  [[2,0]] }); \n}); \n</script>";
    //printFooter( out );

}

    std::string
ferol40::Ferol40WebServer::getCgiPara( cgicc::Cgicc cgi, std::string name, std::string defaultval )
{
    cgicc::form_iterator iter = cgi.getElement(name);
    if ( iter == cgi.getElements().end() )
    {
        return defaultval;
    }
    else 
    {
        return **iter;
    }
}

    void
ferol40::Ferol40WebServer::expertDebugging( xgi::Input *in, xgi::Output *out, Ferol40 *ferol40,   Ferol40Controller *const fctl )
{
    printHeader( out );
    cgicc::Cgicc cgi(in);
    uint32_t linkno = atoi( getCgiPara( cgi, "linkId", "0" ).c_str() );
    std::string command = getCgiPara( cgi, "command", "" );
    std::string parameter = getCgiPara( cgi, "parameter", "" );

    //std::cout << "\ncommand " << command << std::endl;
    //std::cout << "parameter " << parameter << std::endl;

    if ( ! ferol40->getFerol40Device() ) {
        *out << "<h1>Ferol40 is not configured, you must be configured to use this page!</h1>\n";
        return;
    }

    if ( command == "dumpRegisters" ) 
    {
        fctl->dumpHardwareRegisters();
        *out << "<p> Dumped registers in /tmp.</p>\n";
    }
    else if ( command == "resyncSlinkexpress" )
    {
        ferol40->resyncSlinkExpress( linkno );
        *out << "<p>Did resync of Slink Express no " << linkno << ".</p>\n";
    }
    else if ( command == "softwareReset" )
    {
        //std::cout << "about to reset slink!!!, ferol40* = " << ferol40->getFerol40Device() << std::endl;
        ferol40->hwlock();
        ferol40->resyncSlinkExpress( linkno );
        //std::cout << "about to do general reset" << std::endl;
        ferol40->getFerol40Device()->setBit("General_reset_bit");
        usleep(5000000);
        ferol40->hwunlock();
        *out << "<p>Did a softwareReset of the Ferol40.</p>\n";
    }
    else if ( command == "daqOn" )
    {
        ferol40->daqOn( linkno );
        *out << "<p>Did daqOn of Slink Express no " << linkno << ".</p>\n";
    }
    else if ( command == "daqOff" )
    {
        ferol40->daqOff( linkno );
        *out << "<p>Did daqOff of Slink Express no " << linkno << ".</p>\n";
    }

    //JRF TODO remove all Frl stuff...
    else if ( command == "generateFrlError" )
    {
        if ( parameter == "crc" )
        {
            DEBUG("generating a crc error in the FRL" );
            //frl->getFrlDevice()->write( "gen_error", 1 );
        }
        else if ( parameter == "sizeError" ) 
        {
            // frl->getFrlDevice()->write( "gen_error", 2 );                  
        }
        else if ( parameter == "insertWrongEvt" ) 
        {
            //   frl->getFrlDevice()->write( "gen_Outsync_gen", 1 );                  
        }
        else if ( parameter == "wrongEvtNr" ) 
        {
            //    frl->getFrlDevice()->write( "gen_Outsync_gen", 2 );                  
        }
        else if ( parameter == "dropEvent" )
        {
            //   frl->getFrlDevice()->write( "gen_Outsync_gen", 4 );                  
        }
        else if ( parameter == "skipEvents" )
        {
            //   frl->getFrlDevice()->write( "gen_Outsync_gen", 8 );                  
        }
    }

    *out << "<form id=\"debugForm\" method=\"post\" >\n";

    *out << "<h3> Here you really need to know what you are doing... </h3>\n";
    *out << "<p>urn: " << urn_ << "<br>url: " << url_ << " </p>\n";
    *out << "<p><h3>General debugging features</h3>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'dumpRegisters\', \'\' )\">Register Dump</button> Dump all registers of the Ferol40 into files in /tmp<br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'softwareReset\', \'\' )\"> Software reset of Ferol40 </button> Resets the Ferol40. This is the main reset.<br>\n";
    *out << "</p><hr/>\n";

    *out << "<p><h3>Commands for a specific 10Gb Slink Express input link</h3>\n";
    *out << "<input type=\"hidden\" id=\"command\" name=\"command\" value=\"\"/>\n";
    *out << "<input type=\"hidden\" id=\"parameter\" name=\"parameter\" value=\"\"/>\n";
    *out << "10GB link number [0-3] : <input id=\"linkId\" name=\"linkId\" type=\"text\" value=\"0\"><br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'resyncSlinkexpress\', \'\' )\"> Resync SlinkExpress </button> Send a resync command to the selected Slink Express.<br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'daqOnSlinkexpress\', \'\' )\"> DaqOn </button> Send a daqOn command to the selected Slink Express.<br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'daqOffSlinkexpress\', \'\' )\"> DaqOff </button> Send a daqOff command to the selected Slink Express.<br>\n";
    *out << "</p><hr/>\n";

/*    *out << "<p><h3>Generate artificial errors in the plastic events from the FRL event generator</h3>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'generateFrlError\', \'crc\' )\"> Generate CRC error </button> Generates an SLINK CRC error.<br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'generateFrlError\', \'sizeError\' )\"> Generate wrong length</button> Generates an event with a wrong length in the trailer.<br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'generateFrlError\', \'insertWrongEvt\' )\"> Insert Event </button> Generates an additional event with a wrong event number, e.g. 1,2,3,199,4,5,6... <br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'generateFrlError\', \'wrongEvtNr\' )\"> Wrong event number </button> Generates an event with a wrong event number, e.g. 1,2,3,199,5,6... <br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'generateFrlError\', \'dropEvent\' )\"> Miss an event </button> Drops one event number, e.g. 1,2,3,5,6... <br>\n";
    *out << "<button onclick=\"ferol40WebCommand( \'generateFrlError\', \'skipEvents\' )\"> Skip events </button> Skip many events, e.g. 1,2,3,342,343... <br>\n";

    *out << "</p>\n";
    *out << "</p>To generate the \"wrong\" items the 2 comlement of the correct item is used. This holds for the event numbers, fragment length,  and the CRC.</p>\n";
*/
    *out << "</form>\n";
}

    void 
ferol40::Ferol40WebServer::printHeader( xgi::Output * out )
{

    std::stringstream urn,url;
    out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << "\n\
        <html>\n\
        <head>\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tab.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/properties.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/ferol40.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/blue/style.css\">\n\
        \n\
        <script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.1.0.min.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/ferol40/html/jquery.tablesorter.min.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/xgi/html/tabpane.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/ferol40/html/ferol40.js\"></script>\n\
        </head>\n\
        <body>\n\
        <table class=\"ferol40header\"><tr>\
        <th>FEROL40 Controller</th>\n";
    *out << "<td><a href=\"" << url_ << "\">XDAQ page</a></td>\n";
    *out << "<td><a href=\"/" << urn_ << "\">Main</a></td>\n";
    *out << "<td><a href=\"/" << urn_ << "/debugFerol40\"> Ferol40-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/debugSlinkExpress\"> SlinkExpress-Core-registers </a></td>\n"; 
    *out << "<td><a href=\"/" << urn_ << "/expertDebugPage\"> Expert Debugging </a></td>\n"; 
    *out << "</tr></table><hr>\n                  \
        <div id=\"debug\">\n\
        </div>\n\
        ";

}

    void 
ferol40::Ferol40WebServer::printBody( xgi::Output * out )
{
    *out << "\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tab.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/tablesort.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/xgi/html/properties.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/ferol40.css\">\n\
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/ferol40/html/blue/style.css\">\n\
        \n\
        <script type=\"text/javascript\" src=\"/hyperdaq/html/js/jquery-2.1.0.min.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/ferol40/html/jquery.tablesorter.min.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/xgi/html/tabpane.js\"></script>\n\
        <!--\
        <script type=\"text/javascript\" src=\"/xgi/html/favicon/util.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/xgi/html/favicon/ajaxCaller.js\"></script>\n\
        <script type=\"text/javascript\" src=\"/xgi/html/favicon/favicon.js\"></script>\n\
        -->\
        <script type=\"text/javascript\" src=\"/ferol40/html/ferol40.js\"></script>\n\
        </head>\n\
        <body>\n\
        <table class=\"header\"><tr>\
        <th>FEROL40 Controller</th><td><a href=\"basic\">Basic xdaq web page</a></td>\n\
        </tr></table><hr>\n\
        <div id=\"debug\">\n\
        </div>\n";


    std::string updateLink = "/" + urn_ + "/update";
    *out << "\n\
        <div class=\"tab-pane\" id=\"pane1\">\n";

    //resetTabs();
    printTabs( out );


    *out << "</div>\n\
        <script type=\"text/javascript\">\n\
        startUpdate( \"" << updateLink << "\" );\n\
        </script>\n\
        \n\
        ";
}

    void
ferol40::Ferol40WebServer::printFooter( xgi::Output * out )
{
    *out << "\n\
        <hr>\n\
        <div class=\"footer\">\n\
        <p>Ferol40 footer</p>\n\
        </div>\n\
        </body>\n\
        </html>\n";
    //<div style=\"display: none\">";
}
