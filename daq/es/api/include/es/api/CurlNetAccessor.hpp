// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#if !defined(XERCESC_INCLUDE_GUARD_CURLNETACCESSOR_HPP)
#define XERCESC_INCLUDE_GUARD_CURLNETACCESSOR_HPP

#include <iostream>
#include <list>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/util/XMLURL.hpp>
//#include <xercesc/util/BinInputStream.hpp>
//#include <xercesc/util/XMLNetAccessor.hpp>
#include "es/api/CurlURLInputStream.hpp"
#include "es/api/Curl.h"
XERCES_CPP_NAMESPACE_USE

//
// This class is the wrapper for the socket based code which
// provides the ability to fetch a resource specified using
// a HTTP or FTP URL.
//
namespace es
{
	namespace api
	{
		class CurlURLInputStream;

		class CurlNetAccessor
		{
			public :


				CurlNetAccessor();
				~CurlNetAccessor();

				virtual  CurlURLInputStream* makeNew(const XMLURL&  urlSource, es::api::HTTPMethod fHTTPMethod, std::list<std::string> & headers, const es::api::NetHTTPInfo* httpInfo);

				virtual void initCurl(void);
				virtual void cleanupCurl(void);

			private :
				static int fgCurlInitCount;

				CurlNetAccessor(const CurlNetAccessor&);
				CurlNetAccessor& operator=(const CurlNetAccessor&);

		}; // CurlNetAccessor

	}
}


#endif // CURLNETACCESSOR_HPP


