// addon.cc
#include <node.h>


#include "sorcerer/Layout.h"
#include "xgi/framework/UIManager.h"

using namespace v8;

static sorcerer::Layout _layout;


Persistent< Function > _transmute;


void setLayout(const FunctionCallbackInfo<Value>& args) {
	Isolate* isolate = args.GetIsolate();
	Local<Function> cb = Local<Function>::Cast(args[0]);

	_layout.transmute_.Reset(isolate, cb);


	//nodejs::UIManager::NewInstance(args);
	xgi::framework::UIManager::setLayout (&_layout);
}

/*void nodejs::PeerTransportAgent::addSenderSupport(const FunctionCallbackInfo<Value>& args)
{
	Isolate* isolate = args.GetIsolate();

	std::string service(*v8::String::Utf8Value(args[0]->ToString()));
	Local<Function> callback = Local<Function>::Cast(args[1]);

	std::cout << " Add " << service << " sender support" << std::endl;
	nodejs::PeerTransportAgent* object = ObjectWrap::Unwrap<nodejs::PeerTransportAgent>(args.Holder());
	Logger rootLogger = Logger::getRoot();

	nodejs::HTTPSender * hs = new nodejs::HTTPSender( rootLogger);

	hs->callback_.Reset(isolate, callback);

	object->pta_->addPeerTransport(hs);


	std::cout << "done adding sender support " << hs->getProtocol() << std::endl;


}
*/




void InitAll(Local<Object> exports) {
  //nodejs::Layout::Init(exports);

  NODE_SET_METHOD(exports, "setLayout", setLayout);

}

NODE_MODULE(NODE_GYP_MODULE_NAME, InitAll)

//}  // namespace demo




