#!/usr/bin/env python

###############################################################################
## Utility to scan the TTC idelay setting on an (FC7) LPM and find the
## 'locking' range for the TTS link from the PI to the LPM.
###############################################################################

# import ast
# import ConfigParser
import json
import os
import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

TTS_VALUE = 0x0

IDELAY_MIN = 0x00
# IDELAY_MAX = IDELAY_MIN
IDELAY_MAX = 0x1f

NUM_TRIES = 1000

###############################################################################

class Scanner(CmdLineBase):

    # def pre_hook(self):
    #     # The target comes from the command line.
    #     self.target = None
    #     # The following are read from the config file.
    #     self.targets = {}
    #     # End of pre_hook().

    def setup_parser(self):
        # Start with the basic parser configuration.
        super(Scanner, self).setup_parser()

        # # Add the option to choose a custom cfg file.
        # help_str = "Configuration file to use [default: %default]"
        # self.parser.add_option("--cfg-file",
        #                        action="store",
        #                        default="tcds_setup.cfg",
        #                        help=help_str)

    def handle_args(self):
        # A few arguments are expected:
        # - the network name of the LPM to scan,
        # - the ICI number to scan on that LPM,
        # - the network name of the PI connected to that LPM and
        #   input.
        if len(self.args) != 3:
            self.error("Some arguments are required: " \
                       "the network name of the LPM to scan, " \
                       "the ICI number to scan on that LPM, " \
                       "and the network name of the PI " \
                       "connected to that LPM and input.")
        else:
            # Try to verify the parameters.
            tmp_network_address_lpm = self.args[0]
            ici_number = int(self.args[1])
            tmp_network_address_pi = self.args[2]
            network_address_lpm = resolve_network_address(tmp_network_address_lpm)
            network_address_pi = resolve_network_address(tmp_network_address_pi)
            if not network_address_lpm or not network_address_pi:
                msg = "'{0:s}' does not sound like a valid partition name.".format(partition_name)
                self.error(msg)
            else:
                self.network_address_lpm = network_address_lpm
                self.network_address_pi = network_address_pi
                self.ici_number = ici_number

        # self.cfg_file_name = self.opts.cfg_file
        # End of handle_args().

    def main(self):

        # # Host parameters etc. come from the configuration file.
        # cfg_parser = ConfigParser.SafeConfigParser()
        # config_file_name = self.cfg_file_name
        # if self.verbose:
        #     print "Reading configuration from '{0:s}'".format(config_file_name)
        # if not os.path.exists(config_file_name):
        #     self.error("Config file '{0:s}' does not exist.".format(config_file_name))
        # cfg_parser.read(config_file_name)

        # # Find all possible target applications.
        # targets = cfg_parser.items("targets")
        # for (target_name, target_dict) in targets:
        #     self.targets[target_name] = ast.literal_eval(target_dict)
        # if self.verbose:
        #     if len(self.targets):
        #         print "Found the following target applications: "
        #         target_names = self.targets.keys()
        #         target_names.sort()
        #         for target_name in target_names:
        #             print "  {0:s}".format(target_name)
        #     else:
        #         print "No target applications found"

        #----------

        ip_address_pi = resolve_network_address(self.network_address_pi)
        pi = get_pi_hw(ip_address_pi, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to PI {0:s}.".format(ip_address_pi))

        ip_address_lpm = resolve_network_address(self.network_address_lpm)
        lpm = get_lpm_hw(ip_address_lpm, verbose=self.verbose)
        if not lpm:
            self.error("Could not connect to LPM {0:s}.".format(ip_address_lpm))

        #----------

        # Force the TTS output state of the PI.
        pi.write("pi.tts_output_control.tts_out_disabled_value", TTS_VALUE)
        pi.write("pi.tts_output_control.tts_out_enable", 0x0)

        # # Find out which ICI number this partition is.
        # tmp = self.targets['ici-' + self.target]
        # lid_number = tmp['lid']
        # ici_number = lid_number % 100

        # Scan on the LPM.
        print "Starting scan (with TTS value 0x{0:x})...".format(TTS_VALUE)
        results = {}
        lpm.write("lpm_main.tts_idelay_ctrl.align_mode", 0x1)
        for idelay in xrange(IDELAY_MIN, IDELAY_MAX + 1):
            lpm.write("lpm_main.tts_idelay_ctrl.tap_ld.pi{0:d}".format(self.ici_number), 0x0)
            lpm.write("lpm_main.tts_idelay_ctrl.tap_ctrl.pi{0:d}".format(self.ici_number), idelay)
            lpm.write("lpm_main.tts_idelay_ctrl.tap_ld.pi{0:d}".format(self.ici_number), 0x1)
            time.sleep(1)
            lpm.write("lpm_main.tts_idelay_ctrl.tap_ld.pi{0:d}".format(self.ici_number), 0x0)

            for i in xrange(NUM_TRIES):
                tmp_aligned = lpm.read("lpm_main.tts_link_status.link_aligned.pi{0:d}".format(self.ici_number))
                tmp_data_valid = lpm.read("lpm_main.tts_link_status.data_valid.pi{0:d}".format(self.ici_number))
                raw_tts_value = lpm.read("lpm_main.tts_incoming.tts_from_pi{0:d}".format(self.ici_number))
                aligned = (tmp_aligned == 0x1)
                data_valid = (tmp_data_valid == 0x1)

                # print "0x{0:02x}: aligned {1:d}, data_valid {2:d}". \
                #         format(idelay, aligned, data_valid)

                try:
                    results[idelay]['aligned'].append(aligned)
                    results[idelay]['data_valid'].append(data_valid)
                    results[idelay]['raw_tts'].append(raw_tts_value)
                except KeyError:
                    results[idelay] = {
                        'aligned' : [aligned],
                        'data_valid' : [data_valid],
                        'raw_tts' : [raw_tts_value]
                    }
                # time.sleep(.1)

            tmp = [i for i in results[idelay]['raw_tts'] if (i != TTS_VALUE)]
            tmp = list(set(tmp))
            tmp.sort()
            tmp_str = ""
            if len(tmp):
                tmp_str = ", unexpected TTS values encountered: " + \
                          " ".join([hex(i) for i in tmp])
            print "0x{0:02x}: {1:4d} tries, " \
                "{2:4d} aligned" \
                ", {3:4d} data-valid" \
                ", {4:4d} correct TTS" \
                "{5:s}" \
                .format(idelay,
                        len(results[idelay]['aligned']),
                        sum(results[idelay]['aligned']),
                        sum(results[idelay]['data_valid']),
                        len([i for i in results[idelay]['raw_tts'] if (i == TTS_VALUE)]),
                        tmp_str)

        #----------

        # Restore a bit of sanity in the hardware.
        lpm.write("lpm_main.tts_idelay_ctrl.align_mode", 0x0)
        pi.write("pi.tts_output_control.tts_out_enable", 0x1)

        #----------

        # Store all results together with the settings.
        settings = {
            'lpm' : self.network_address_lpm,
            'pi' : self.network_address_pi,
            'ici_number' : self.ici_number,
            'tts_value' : TTS_VALUE
            }
        data = {
            'settings' : settings,
            'results' : results
            }
        name_base = "tts_idelay_scan_{0:s}_{1:d}_{2:s}.json"
        out_file_name = name_base.format(self.network_address_lpm, self.ici_number, self.network_address_pi, TTS_VALUE)
        with open(out_file_name, 'w') as outfile:
            json.dump(data, outfile)

        #----------

        # End of main().

    # End of class Scanner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Utility to scan the TTC idelay setting on an (FC7) LPM " \
               "and find the 'locking' range for the TTS link from the PI to the LPM."
    usage_str = "usage: %prog [OPTIONS] partition"

    Scanner(desc_str, usage_str).run()

    print "Done"

###############################################################################
