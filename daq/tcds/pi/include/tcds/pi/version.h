#ifndef _tcdspi_version_h_
#define _tcdspi_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSPI_VERSION_MAJOR 3
#define TCDSPI_VERSION_MINOR 13
#define TCDSPI_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSPI_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSPI_VERSION_CODE PACKAGE_VERSION_CODE(TCDSPI_VERSION_MAJOR,TCDSPI_VERSION_MINOR,TCDSPI_VERSION_PATCH)
#ifndef TCDSPI_PREVIOUS_VERSIONS
#define TCDSPI_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSPI_VERSION_MAJOR,TCDSPI_VERSION_MINOR,TCDSPI_VERSION_PATCH)
#else
#define TCDSPI_FULL_VERSION_LIST TCDSPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSPI_VERSION_MAJOR,TCDSPI_VERSION_MINOR,TCDSPI_VERSION_PATCH)
#endif

namespace tcdspi
{
  const std::string package = "tcdspi";
  const std::string versions = TCDSPI_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS PI.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS PI.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
