#include "sdfuM.h"


//test
#include "TcpXIOConnectorS.h"
#include "TcpDispatcherAcceptorTaskM.h"
#include "TcpDispatcherAcceptorTaskS.h"
#include "TcpDispatcherConnectorTaskM.h"
#include "TcpDispatcherSelectorTask.h"
#include "TcpXIOAcceptorS.h"
#include "TcpXIOConnectorM.h"
//test


extern "C" int Main(char * server, char * ua)
{
	//test
	TcpXIOConnectorS                  * csPtr;
	TcpDispatcherAcceptorTaskM        * amtPtr;
	TcpDispatcherAcceptorTaskS        * astPtr;
	TcpDispatcherConnectorTaskM       * cmtPtr;
	TcpDispatcherSelectorTask         * stPtr;
	TcpXIOAcceptorS                   * asPtr;
	TcpXIOConnectorM                  * cmPtr;
	//test
	
	
	UAddress uaddr;
	sscanf(ua,"%d",&uaddr);
	//Test Select
	vector<Addr*>  addrv;
	addrv.resize(2);
		
		
	addrv[0] = new tcpAddr();
	strcpy(((tcpAddr*)addrv[0])->host,"137.138.82.59");
	((tcpAddr*)addrv[0])->port = 20300;
		
	addrv[1] = new tcpAddr();
	strcpy(((tcpAddr*)addrv[1])->host,"137.138.82.28");
	((tcpAddr*)addrv[1])->port = 20300;
	  	TaskGroup g;

	//TcpDispatcherConnectorTaskM * s = new TcpDispatcherConnectorTaskM(&addrv);

	//s->activate();
	
	//test
	TcpDispatcherSelectorTask * s;
	s = new TcpDispatcherSelectorTask();
	TcpXIOConnectorM * c;
	c = new TcpXIOConnectorM(&addrv);
	c->connect();
	s->putXIO((*c->getXIO())[0]);
	s->putXIO((*c->getXIO())[1]);
	s->initTaskGroup(&g); // mind the order! 1
	s->activate();        // 2
	//test

	//c1->sync(); //wait everything is connected
	vector<RUInterface*> ruv;
	ruv.resize(2);
	ruv[0] = new XRU((*s->getXIO())[0]);
	ruv[1] = new XRU((*s->getXIO())[1]);
	XEVM * xevm = new XEVM((*s->getXIO())[0]);
	SDFU * sdfu = new SDFU(&ruv,xevm,uaddr);	

	s->addListener(sdfu);
	
	char str[10];
	cout << "Go?" << endl;
	cin >> str;
	sdfu->start("Go!");
	//pause();
 	g.join();
	delete c;
	delete ruv[0];
	delete ruv[1];
	delete xevm;
	delete sdfu;
	return(0);	
}
#ifndef vxworks
int main(int argc, char* argv[])
{

	return Main(argv[1],argv[2]);
}
#endif
