<?php
require '../utils/vendor/autoload.php';
include_once ('config.php');

$classcounters = array ();
$keycounter = 0;
$heartbeaturl = $_GET ['heartbeaturl'];
$json = array(
		'table' => array('rows'=> array())
);
if( isset($_GET ['classname'])) {
	$classname = $_GET ['classname'];
	$response = Network::httpget ( $heartbeaturl . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?fmt=json&classname=" . $classname);
	$json = json_decode ( $response->getBody (), true );
}
foreach ($json ['table']['rows'] as &$record) {
	$record['progress'] = $record['age'];
}

echo json_encode ($json ['table']['rows']);

?>
