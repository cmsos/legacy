package xdaq.xdesk.tools;

import java.util.*;
import javax.infobus.*;

import java.beans.*;

import org.apache.log4j.*;



public class Discovery implements  InfoBusDataConsumer
{
	Logger logger = Logger.getLogger (Discovery.class);

	InfoBusMemberSupport infobusSupport_;
	DiscoveryListener listener_;
	HashMap mapper_;
	String name_;
	//
	// end of inner class for listening to xreply events
	// LO begin
	public class ObjectChangeListener extends DataItemChangeListenerSupport implements DataItemChangeListener
  	{
		ObjectChangeListener ( DataItem item )
		{
			item_ = item;
		}

  		public void dataItemValueChanged (DataItemValueChangedEvent event)
		{
			// Object o = event.getChangedItem();

			// invoke here DiscoveryListener function!!!!!!!
			if ( listener_ != null )
				listener_.objectChanged(item_);
		}

		DataItem getItem()
		{
			return item_;
		}

		DataItem item_;
  	};



	// LO end


	// command, e.g.: xcommand
	public Discovery (String infoBus, String name,  DiscoveryListener l)
	{
          	name_ = name;
		mapper_ = new HashMap();
		listener_ = l;
		infobusSupport_ = new InfoBusMemberSupport (null);

		try
		{
			infobusSupport_.joinInfoBus(infoBus);
			infobusSupport_.getInfoBus().addDataConsumer (this);

			Object[] objects = this.retrieve(name);
			if (objects == null) return;

			logger.debug("found " + objects.length + " items on: " + infoBus + "  for item " + name);

			for (int i= 0; i < objects.length; i++ )
			{
				mapper_.put(((DataItem)objects[i]).getSource(),objects[i]);
				ObjectChangeListener ocl = new ObjectChangeListener ( (DataItem)objects[i] );
      				((DataItemChangeManagerSupport)objects[i]).addDataItemChangeListener(ocl);

				ImmediateAccess item = (ImmediateAccess) ocl.getItem();

				if ( listener_ != null )
				{
					listener_.objectAdvertised((DataItem)objects[i]);
				}
			}
		} catch (Exception e)
		{
			logger.error ("Discovery on " + infoBus + ": " + e.toString());
			e.printStackTrace();
		}
	}


	protected Object [] retrieve(String itemName)
	{

		if (infobusSupport_ != null)
		{
			try {
				// subscribe to infobus events
				// If there are multiple sources, subscribe the same listener to all sources
				// with the same name
				//
				Object[] dataItems = infobusSupport_.getInfoBus().findMultipleDataItems(itemName, null, this);
				return dataItems;

			} catch (Exception e)
			{
				logger.error ("Cannot subscribe to infobus item " + itemName + ": " + e.toString());
			}

		} else
		{
			logger.error ("Cannot subscribe to infobus item " + itemName + ": infobus unavailable");
		}

		return null;
	}



	public void leave ()
	{

		try
		{

			mapper_.clear();
			infobusSupport_.getInfoBus().removeDataConsumer (this);
			infobusSupport_.leaveInfoBus();
		} catch (Exception e)
		{
			logger.error ("Cannot leave infobus: " + e.toString());
		}
	}



	public void dataItemAvailable ( InfoBusItemAvailableEvent e )
	{
		String name = e.getDataItemName();
		if ( name != name_ ) // we are not interested in this item
			return;

		Object object = (Object) e.requestDataItem (this, null);
		Object source = ((DataItem)object).getSource();
		if ( mapper_.containsKey(source) ) {
			logger.error("item already advertised");
			return;
		}

		mapper_.put(source,object);

		// LO begin
		if ((object != null) && (object instanceof DataItemChangeManagerSupport))
		{
			ObjectChangeListener ocl = new ObjectChangeListener ((DataItem)object);
			((DataItemChangeManagerSupport) object).addDataItemChangeListener ( ocl );
			String itemName = ((ImmediateAccess)object).getValueAsString();


		}
		// LO end

		if ( listener_ != null )
		{

			listener_.objectAdvertised((DataItem)object);

		}

	}

	public void dataItemRevoked ( InfoBusItemRevokedEvent e )
	{
		//String item = e.getDataItemName();
		Object producer = e.getSourceAsProducer();

		Object object = mapper_.get(producer);

		if ( mapper_.containsKey(producer) )
		{
			mapper_.remove(producer);

			// LO begin
			// need to retrieve ocl
			// TBD
			//((DataItemChangeManagerSupport) ocl.getItem() ).removeDataItemChangeListener ( ocl );
			// LO end

			if (listener_ != null)
			{
				listener_.objectRevoked((DataItem)object);
			}
		}
		//else
		//{
		//	logger.error("cannt find item to be revoked");
		//
		//}
	}

	public void propertyChange (PropertyChangeEvent e)
	{

	}
}
