//
//  XPropTooBarListener.java
//  
//
//  Created by Luciano Orsini on Sat Jun 28 2003.
//  Copyright (c) 2003 CERN. All rights reserved.
//

package xdaq.xdesk.xcommander;

public interface XCommanderToolBarListener {
    public void buttonPressed(String buttonName);
}
