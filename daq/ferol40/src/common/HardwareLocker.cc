#include "ferol40/HardwareLocker.hh"
#include "ferol40/ferol40Constants.h"
//#include "d2s/utils/loggerMacros.h"


// 33 locks: 
// [0] is not used.
// [1]-[16] for slot 1-16 in amc13 mode.
// [17]-[33] for 16 possible MOL units. 
ferol40::HardwareLocker::HardwareLocker( Logger &logger, 
                                       ApplicationInfoSpaceHandler &appIS,
                                       StatusInfoSpaceHandler &statusIS)
    : utils::HardwareLocker(logger, dynamic_cast<utils::InfoSpaceHandler&>(appIS), dynamic_cast<utils::InfoSpaceHandler&>(statusIS), LOCKFILE, LOCK_PROJECT_ID) {}


// returns true if
//  - initally the status was "unlocked"
//  - and the lock could be taken successfully.
// returns false in all other cases.
//
// E.g. if initially the lockstatus is unknown (since the slot/unit 
// was not yet set) this routing never succeeds.
bool ferol40::HardwareLocker::lock()
{
    updateLockStatus();
    // lock if we are unlocked and there is the need to lock. 
    // otherwise leave unlocked to give the hardware to minidaq or others.
    if ( (lockStatus_ == "unlocked") &&
         (dynamic_cast<xdata::Boolean *>(appIS_.getvector( "InputPorts" )[0].getField("enable"))->value_ || 
	  dynamic_cast<xdata::Boolean *>(appIS_.getvector( "InputPorts" )[1].getField("enable"))->value_ ||
	  dynamic_cast<xdata::Boolean *>(appIS_.getvector( "InputPorts" )[2].getField("enable"))->value_ ||
	  dynamic_cast<xdata::Boolean *>(appIS_.getvector( "InputPorts" )[3].getField("enable"))->value_   
	  )
         )

        {
            bool res = hwLock_P->takeIfUnlocked( slotunit_ );
            updateLockStatus();
            return res;
        }

    else

        {
            return false;
        }
}


/*
// returns true if 
//    - initially the lock was locked by us
//    - after the operation the lock is NOT anymore locked by us
// returns false in all other cases.
bool ferol40::HardwareLocker::unlock()
{

    updateLockStatus();
    
    if ( lockStatus_ == "locked by us" )
        {
            hwLock_P->give( slotunit_ );
            updateLockStatus();
            if ( lockStatus_ != "locked by us" ) 
                {
                    return true;
                }
            else
                {
                    return false;
                }
        }
    else
        {
            return false;
        }
}

std::string 
ferol40::HardwareLocker::updateLockStatus()
{
    // if the slot_ is not yet set we do not know which lock to look at.
   
    getSlotUnit();
    if ( slotunit_ == 0 )
        {
            lockStatus_ =  "unknown";
            statusIS_.setstring( "lockStatus", lockStatus_, true );
            return lockStatus_;
        }

                  
    if ( hwLock_P->isLocked( slotunit_ ) )
        {
            int32_t pid = hwLock_P->getPID( slotunit_ );
            if ( pid == getpid() )
                {
                    lockStatus_ =  "locked by us";
                    statusIS_.setstring( "lockStatus",lockStatus_ , true );
                    return lockStatus_;
                }
            else
                {
                    lockStatus_ =  "locked by others";
                    statusIS_.setstring( "lockStatus",lockStatus_ , true );
                    return lockStatus_;
                }
        }
    else
        {
            lockStatus_ =  "unlocked";
            statusIS_.setstring( "lockStatus",lockStatus_ , true );
            return lockStatus_;
        }
         
}


bool
ferol40::HardwareLocker::lockedByUs()
{
    if( updateLockStatus() == "locked by us" )
        return true;
    else
        return false;
}

// The test on slotNumber == 0 is a clear indication that the 
// application does not yet have the information about slot OR unit.
// If the FEDKIT_MODE is active, the unit number may be 0. In this case
// this routine returns 17. 
uint32_t
ferol40::HardwareLocker::getSlotUnit()
{
    uint32_t slotunit = appIS_.getuint32( "slotNumber" );
    std::string opMode = appIS_.getstring( "OperationMode" );

    // JRF, removing FEDKIT_MODE
    // if we are in fedkit mode we do not look for slot numbers 
    // but for unit numbers. These can start with 0. We arrange 
    // them to start with 17 (there are max 16 slots). 
    if (  opMode == FEDKIT_MODE )
        {
            slotunit += 17;
        }
    slotunit_ = slotunit;
    if ( slotunit_ > 33 ) 
        {
            FATAL( "crazy slotunit number given: " << slotunit );
            exit(-1);
        }
    return slotunit_;
}
*/
