// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_bcmlprocessor_version_h_
#define _bril_bcmlprocessor_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILBCMLPROCESSOR_VERSION_MAJOR 1
#define BRILBCMLPROCESSOR_VERSION_MINOR 5
#define BRILBCMLPROCESSOR_VERSION_PATCH 1
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILBCMLPROCESSOR_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILBCMLPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILBCMLPROCESSOR_VERSION_MAJOR,BRILBCMLPROCESSOR_VERSION_MINOR,BRILBCMLPROCESSOR_VERSION_PATCH)
#ifndef BRILBCMLPROCESSOR_PREVIOUS_VERSIONS
#define BRILBCMLPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBCMLPROCESSOR_VERSION_MAJOR,BRILBCMLPROCESSOR_VERSION_MINOR,BRILBCMLPROCESSOR_VERSION_PATCH)
#else
#define BRILBCMLPROCESSOR_FULL_VERSION_LIST  BRILBCMLPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBCMLPROCESSOR_VERSION_MAJOR,BRILBCMLPROCESSOR_VERSION_MINOR,BRILBCMLPROCESSOR_VERSION_PATCH)
#endif

namespace brilbcmlprocessor
{
	const std::string package = "brilbcmlprocessor";
	const std::string versions = BRILBCMLPROCESSOR_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ bcmlprocessor";
	const std::string description = "BCML processor";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
