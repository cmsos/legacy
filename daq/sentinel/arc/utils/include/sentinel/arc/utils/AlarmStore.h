// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_utils_AlarmStore_h_
#define _sentinel_arc_utils_AlarmStore_h_

#include "sentinel/arc/utils/AlarmStoreListener.h"

#include "xdaq/Application.h"

#include <occi.h>

#include "xgi/Method.h"

#include "sentinel/arc/utils/exception/ConstraintViolated.h"
#include "sentinel/arc/utils/exception/FailedToArchive.h"
#include "sentinel/arc/utils/exception/FailedToClose.h"
#include "sentinel/arc/utils/exception/FailedToOpen.h"
#include "sentinel/arc/utils/exception/FailedToRead.h"
#include "sentinel/arc/utils/exception/FailedToRemove.h"
#include "sentinel/arc/utils/exception/FailedToStore.h"
#include "sentinel/arc/utils/exception/NotFound.h"

#include "toolbox/Properties.h"
#include "toolbox/TimeVal.h"
#include "toolbox/BSem.h"
#include "toolbox/net/UUID.h"

namespace sentinel
{
	namespace arc
	{
		namespace utils
		{

			static const std::string EVENT_NULL_STR = "null";

			static const std::string EVENT_FIRE_STR = "fire";
			static const std::string EVENT_ACCEPT_STR = "accept";
			static const std::string EVENT_REVOKE_STR = "revoke";
			static const std::string EVENT_CLEAR_STR = "clear";
			/////
			static const std::string EVENT_CLEARON_STR = "clearon";
			static const std::string EVENT_CLEAROFF_STR = "clearoff";
			static const std::string EVENT_SHELFON_STR = "shelfon";
			static const std::string EVENT_SHELFOFF_STR = "shelfoff";
			static const std::string EVENT_DEBOUNCEON_STR = "debounceon";
			static const std::string EVENT_DEBOUNCEOFF_STR = "debounceoff";

			class AlarmStore
			{
				public:

					AlarmStore (xdaq::Application * application, const std::string& username, const std::string& password, const std::string& tnsname, bool readOnly, unsigned int scatterReadNum,
							unsigned int maxInitExceptionNum, unsigned int flashbackInitWindow) ;
					~AlarmStore ();

					std::string getName();
					toolbox::TimeVal lastStoredEvents (sentinel::arc::utils::AlarmStoreListener * listener, toolbox::TimeVal & since);
					// initialization
					toolbox::TimeVal initialize (sentinel::arc::utils::AlarmStoreListener * listener, toolbox::TimeVal & since);
					//static void hasInsertedfirstRow (bool b);
					void readBlob (const std::string& zKey, std::string& pzBlob);
					/*! Check if database contain the exception for uuid
										 */
					bool hasException (const std::string& uuid);

					void accept (const std::string & exception, const std::string & source);
					void clear (const std::string & exception, const std::string & source);

				private:
					/*! Store an exception object
					 This function is thread safe (read/write lock)
					 */
					void fire (xcept::Exception& ex) ;
					//void fireNew (xcept::Exception& ex) ;
					void revoke (xcept::Exception& ex) ;

					void ruleon (toolbox::Properties& rule, const std::string& ruleType, const std::string & source) ;
					void ruleoff (const std::string & ruleUUID, const std::string & ruleType, const std::string & source) ;

					/*! Store an exception, all values are in the properties object
					 The function does not store a chained exception (blob)
					 */
					void storeNewException (toolbox::Properties& properties, const std::string & blob) ;
					void storeNewCatalogEntry (toolbox::Properties& properties) ;
					void storeNewXDAQEntry (const std::string& uniqueid, toolbox::Properties& properties) ;

					void storeNewEvent (const std::string & type, toolbox::Properties& properties) ;


					std::pair<std::string, std::string> getLastEvent (const std::string& uuid, const std::string& time) ; // returns fire, clear or null

					bool ruleOnExists (const std::string& ruleUUID, const std::string& ruleType) ;

					/*! Retrieve a single exception by uuid in the format it has been stored.
					 The name of the format is returned in \param format.
					 */
					//void retrieve (const std::string& uuid, const std::string& format, xgi::Output* out) ;

					/*! Return a list of stored exceptions in the \param ex set that match the search query
					 This function is thread safe (read lock)*/
					void catalog (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format) ;

					/*! Return a list of stored exceptions in the \param ex set that match the search query , order desc by dateTime
					 */
					void catalog (toolbox::TimeVal & age, int (*callback) (void*, int, char**, char**), void * context) ;

					void events (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format) ;

					void remove (toolbox::TimeVal & age) ;

					/*! Retrieve the time at which the last exception has been stored from the database */
					toolbox::TimeVal getLatestStoreTime ();

					/*! Retrieve the time of the most recent event in the database */
					toolbox::TimeVal getLatestEventTime () ;

					/*! Retrieve the time of the most recent exception in the database */
					toolbox::TimeVal getLatestExceptionTime () ;

					/*! Retrieve the time of the oldest exception in the database */
					toolbox::TimeVal getOldestExceptionTime ();

					/*! Retrieve the number of exceptions stored */
					std::string getNumberOfExceptions () ;

					size_t getSize ();

					/*! Average time to store a single exception */
					double getAverageTimeToStore ();

					/*! Average time to retrieve a catalog */
					double getAverageTimeToRetrieveCatalog ();

					/*! Average time to retrieve a single exception by uuid */
					double getAverageTimeToRetrieveException ();

					void writeBlob (const std::string & zKey, const std::string & zBlob) ;

					void query (xgi::Output* out, const std::string& query) ;

					void lock ();

					void unlock ();

					// Vacuum the database, other operations TBD
					//
					void maintenance () ;

					std::list<toolbox::Properties> getOpenArchivesInfo ();

				private:
					bool inCatalog (const std::string& zKey) ;

					toolbox::TimeVal outputCatalog (sentinel::arc::utils::AlarmStoreListener * listener, oracle::occi::ResultSet* occiResult) ;

					// Use for coldspot query
					//toolbox::TimeVal outputCatalogJSONExtended (xgi::Output* out, oracle::occi::ResultSet* occiResult) ;

					void prepareDatabase (bool readOnly) ;

					std::string escape (const std::string& s);

					toolbox::BSem mutex_; // used for db management operations

					double averageTimeToStore_;
					double averageTimeToRetrieveCatalog_;
					double averageTimeToRetrieveException_;
					toolbox::TimeVal lastExceptionTime_;
					toolbox::TimeVal maintenanceSinceTime_;

					unsigned int scatterReadNum_;
					unsigned int maxInitExceptionNum_; // maximum number of last occurred exception to be loaded in the model
					unsigned int  flashbackInitWindow_; // time window in seconds of last occurred exception

					oracle::occi::Environment* environment_;
					oracle::occi::Connection* connection_;

					oracle::occi::Statement* insertCatalogStmt_;
					oracle::occi::Statement* insertExceptionStmt_;
					oracle::occi::Statement* insertApplicationStmt_;
					oracle::occi::Statement* insertEventStmt_;

					/// NEW STATEMENTS

					oracle::occi::Statement* updateEventStmt_;

					oracle::occi::Statement* openRulesStmt_;
					oracle::occi::Statement* openExceptionsStmt_;

					xdaq::Application * application_;

					std::string name_;
			};
		}
	}
}
#endif
