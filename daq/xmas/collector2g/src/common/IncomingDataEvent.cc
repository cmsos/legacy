// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/collector2g/IncomingDataEvent.h"
#include <string>
#include <sstream>
#include <map>
#include "xmas/exception/Exception.h"
#include "xmas/collector2g/Application.h"
			
xmas::collector2g::IncomingDataEvent::IncomingDataEvent
(
	toolbox::mem::Reference* ref,
	xdata::Properties& plist,
	xmas::collector2g::Application* collector
)
	: 	
	toolbox::Event("urn:xmas-collecto2g-event:IncomingData", 0),
	ref_(ref),
	plist_(plist),
	collector_(collector)
{
}

xmas::collector2g::IncomingDataEvent::~IncomingDataEvent()
{
}
