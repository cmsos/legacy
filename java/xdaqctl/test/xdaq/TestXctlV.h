#ifndef __TEST_XCTL_V_H__
#define __TEST_XCTL_V_H__

#include "config/PackageInfo.h"

namespace TestXctl {
	const std::string package     = "TestXctl";
	const std::string versions    = "3.0";
	const std::string description = "dummy XDAQ application for xdaqctl tests";
	const std::string summary     = "xdaqctl/test/xdaq";
	const std::string authors     = "Ichiro Suzuki";
	const std::string link        = "http://home.fnal.gov/~ichiro/xdaqctl";

	config::PackageInfo getPackageInfo();

	void checkPackageDependencies()
			throw (config::PackageInfo::VersionException);

	std::set<std::string, std::less<std::string> > getPackageDependencies();
};

#endif  // ifndef __TEST_XCTL_V_H__
// vim: set ai sw=4 ts=4:
