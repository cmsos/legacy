/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * channelFactory.hpp
 *
 *  Created on: Mar 22, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once
#include <memory>

#include "mpila/Channel.hpp"
#include "mpila/ChannelTest.hpp"
#include "mpila/InterfaceAdapter.hpp"
#include "mpila/globaldef.hpp"

namespace mpila
{

namespace channelFactory
{

std::unique_ptr<Channel> buildChannel(TransportMode mode, size_t pipelineDepth, WorkRequestType type);

} /* namespace channelFactory */
} /* namespace mpila */
