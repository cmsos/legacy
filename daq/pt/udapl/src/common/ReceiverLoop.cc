// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/udapl/ReceiverLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include <sstream>

pt::udapl::ReceiverLoop::ReceiverLoop (const std::string & name, i2o::Listener * listener) 
	: listener_(listener)
{
	name_ = name;
	process_ = toolbox::task::bind(this, &ReceiverLoop::process, "process");

	try
	{
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->submit(process_);
	}
	catch (toolbox::task::exception::Exception& e)
	{
		XCEPT_RETHROW(pt::udapl::exception::Exception, "Failed to submitjob", e);

	}
	catch (std::exception& se)
	{
		std::stringstream msg;
		msg << "Failed to submit notification to worker thread, caught standard exception '";
		msg << se.what() << "'";
		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}
	catch (...)
	{
		XCEPT_RAISE(pt::udapl::exception::Exception, "Failed to submit notification to worker pool, caught unknown exception");
	}

}

pt::udapl::ReceiverLoop::~ReceiverLoop ()
{
	toolbox::task::getWorkLoopFactory()->getWorkLoop(name_, "polling")->cancel();
}

bool pt::udapl::ReceiverLoop::process (toolbox::task::WorkLoop* wl)
{
	dispatchQueue_.resize(8192);
	while (1)
	{
		/*
		 if ( ! dispatchQueue_.empty())
		 {
		 toolbox::mem::Reference * reference = dispatchQueue_.front();
		 dispatchQueue_.pop_front();
		 listener_->processIncomingMessage(reference);
		 }
		 */
		toolbox::mem::Reference * reference = dispatchQueue_.pop();
		listener_->processIncomingMessage(reference);
	}
	return true;
}

