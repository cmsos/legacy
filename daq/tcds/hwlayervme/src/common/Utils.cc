#include "tcds/hwlayervme/Utils.h"

std::string
tcds::hwlayervme::boardTypeToString(BOARD_ID_TYPE const boardType)
{
  std::string res;
  switch (boardType)
    {
    case BOARD_ID_RF2TTC:
      res = "RF2TTC";
      break;
    case BOARD_ID_RFRXD:
      res = "RFRXD";
      break;
    default:
      res = "unknown";
      break;
    }
  return res;
}
