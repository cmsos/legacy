#include <iostream>
#include <string>
#include "toolbox/net/URL.h"
#include "toolbox/net/exception/MalformedURL.h"

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << "Usage: " << argv[0] << " url " << std::endl;
		return 1;
	}

	try
	{
		toolbox::net::URL u(argv[1]);
		std::cout << "String representation is " << u.toString() << std::endl; 
		std::cout << "Path: " << u.getPath() << std::endl;
		std::cout << "Fragment: " << u.getFragment() << std::endl;
		std::cout << "Query: " << u.getQuery() << std::endl;
	} 
	catch (toolbox::net::exception::MalformedURL& mfu)
	{
		std::cout << "Exception: " << mfu.what() << std::endl;
		return 1;
	}

	return 0;
}
