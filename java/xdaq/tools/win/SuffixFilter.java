package xdaq.tools.win;

import java.io.*;
import java.util.*;
import java.lang.*;
import javax.swing.filechooser.FileFilter;

class SuffixFilter extends FileFilter
{
    	private String m_description = null;
   	private String m_extension = null;

    	public SuffixFilter(String extension, String description) 
	{
                m_description = description;
                m_extension = "." + extension.toLowerCase();
    	}

   	public String getDescription() 
	{ 
               	return m_description; 
    	}

    	public boolean accept(File f) 
	{
                if (f == null) return false;
                if (f.isDirectory()) return true;
                return f.getName().toLowerCase().endsWith(m_extension);
	}
};
