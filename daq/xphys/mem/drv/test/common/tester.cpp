// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, S. Markan and L. Orsini                         *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

/*
 * tester.cpp
 *
 *
 * This program runs a set of CppUnit tests on the memphys driver.  The
 * actual tests are in memphysTest.h and memphysTest.cpp.
 * Compile with make test2
 *
 * Note that the two insmod error messages that occur when the program
 * is run are supposed to be there.
 *
 * More information in readme.html
 */
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

int main(int argc, char* argv[]) {
  CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();

  CppUnit::TextUi::TestRunner runner;
  runner.addTest(suite);

  runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(), std::cerr));

  bool wasSuccessful = runner.run();

  return wasSuccessful ? 0 : 1;
}
