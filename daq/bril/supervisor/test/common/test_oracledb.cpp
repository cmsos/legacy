#include "soci/soci.h"
#include "soci/error.h"
#include <exception>
#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <cstdio>
#include <jansson.h>
#include "bril/supervisor/INIReader.h"
#include "bril/supervisor/Decoder.h"
#include <algorithm>

struct CalibStringData{
  CalibStringData():since(0),till(0){}
  unsigned int since;
  unsigned int till;
  std::string func_name;
  std::string func_payload;
};

struct SinceData{
  SinceData():since(0){}
  long long since; //long long maps to NUMBER(10)
  std::string func;
  std::string payload;
};
typedef std::map< unsigned int, SinceData > Sincemap;

namespace soci{
  template<>
  struct type_conversion<SinceData>{
    typedef values base_type;

    static void from_base(const values& v, indicator ind,  SinceData& p)
    {
      p.since = v.get<long long>("SINCE");
      p.payload = v.get<std::string>("PAYLOAD");
      p.func = v.get<std::string>("FUNC");

      // p.func will be set to the default value "unknown"
      // when the column is null:
      p.since = v.get<long long>("SINCE",0);
      p.payload = v.get<std::string>("PAYLOAD", "unknown");
      p.func = v.get<std::string>("FUNC", "unknown");
    }
    
    static void to_base(const SinceData & p, values & v, indicator & ind)
    {
      v.set("SINCE", p.since);
      v.set("PAYLOAD", p.payload);
      v.set("FUNC", p.func);
      ind = i_ok;
    }
  };
}

unsigned long long iov_gettag( soci::session& sql,const std::string& tagname, std::string schema="cms_lumi_prod"){
  /**
     select tagid from iovtags where tagname=:tagname
   */
  unsigned long long result = 0;
  std::string tablename = schema+"."+"iovtags";
  soci::statement st = ( sql.prepare << "select tagid from "<<tablename<<" where tagname=:tagname",soci::into(result),soci::use(tagname,"tagname") );
  st.execute();  
  while( st.fetch() ){
  }
  return result;
}

CalibStringData iov_gettagdata( soci::session& sql,unsigned long long iovtagnameid,int runnum, std::string schema="cms_lumi_prod"){
  CalibStringData result;
  /**
     select since,payload,func from iovtagdata
  */
  std::string tablename = schema+"."+"iovtagdata";
  SinceData r;
  Sincemap allsincedata;
  try{
    soci::statement st = (sql.prepare << "select since,payload,func from "<<tablename<<" where tagid=:iovtagnameid" ,soci::into(r), soci::use(iovtagnameid,"iovtagnameid") );
    st.execute();
    while( st.fetch() ){
      allsincedata.insert( std::make_pair(r.since,r) );
    }
  }catch( const soci::soci_error& er){
    std::cout<<"DB error "<<er.what()<<std::endl;
  }catch( const std::exception& er ){
    std::cout << "std error: "<<er.what() << std::endl;
  }

  Sincemap::iterator it;
  it = allsincedata.upper_bound(runnum);
  unsigned int since = 1;
  unsigned int till = 999999;
  if( it!=allsincedata.end() ){
    till = (it->first)-1;
    if( it==allsincedata.begin() ){
      since = 0;//exception!!!!
    }else{
      since = (--it)->first;
    }
  }else{
    since = allsincedata.rbegin()->first;
  }
  if( since!=0 ){
    it = allsincedata.find(since);
    result.since = since;
    result.till = till;
    std::string fname = it->second.func;//json string must be double quoted
    std::replace( fname.begin(), fname.end(), '\'', '\"' );
    std::string fpayload = it->second.payload;
    std::replace( fpayload.begin(),fpayload.end(), '\'', '\"' );
    result.func_name = fname;
    result.func_payload = fpayload;
  }
  return result;
}
/*
  
**/
bool parseDBparam(const std::string& authf, const std::string& dbalias, std::string& o_netservice, std::string& o_dbuser, std::string& o_dbpass){
  INIReader reader(authf);
  if (reader.ParseError() < 0) {
    std::cout << "Can't load ini file\n";
  }
  o_netservice = reader.Get(dbalias,"service","");
  if(o_netservice.empty()){
    std::cout<<"empty netservice"<<std::endl;
    return false;
  }
  o_dbuser = reader.Get(dbalias,"user","");
  if(o_dbuser.empty()){
    std::cout<<"empty user"<<std::endl;
    return false;
  }
  std::string pass = reader.Get(dbalias,"pwd","");
  if(pass.empty()){
    std::cout<<"empty pass"<<std::endl;
    return false;
  }
  o_dbpass = base64_decode(pass); 
  return true;
}

void parsePayloadString(const std::string& func_payload, std::map<std::string,std::string>& result){
  const char* strpayload=func_payload.c_str();
  json_error_t jerror;
  json_t* jpayload = json_loads(strpayload, 0, &jerror);
  if(!jpayload){
    std::cout<<"error: on line "<<jerror.line<<" message "<<jerror.text<<std::endl;
  }      
  const char *key;
  json_t *value;   
  void* iter = json_object_iter(jpayload);
  while(iter){
    key = json_object_iter_key(iter);
    value = json_object_iter_value(iter);
    std::string valuestr( json_string_value(value) );
    result.insert( std::make_pair(key,valuestr) );
    iter = json_object_iter_next(jpayload, iter);
  }
  json_decref( jpayload );
}

int main(){
  // phase 1: preparation
  const size_t poolSize = 10;
  soci::connection_pool pool(poolSize);
  std::string service;
  std::string dbuser;
  std::string dbpass;
  if(!parseDBparam("/brildata/db/db.ini","online",service,dbuser,dbpass)){
    std::cout<<"authpath parsing error"<<std::endl;
    return 0;
  }
  std::cout<<service<<" "<<dbuser<<" "<<dbpass<<std::endl;
  char connectstr[80];
  sprintf(connectstr, "service=%s user=%s password=%s",service.c_str(),dbuser.c_str(),dbpass.c_str());
  std::cout<<std::string(connectstr)<<std::endl;

  size_t sessionid;
  int leasetimeout_millisec=100;
  bool leased = pool.try_lease(sessionid,leasetimeout_millisec);
  if(leased){
    std::cout<<"got sessionid "<<sessionid<<std::endl;
  }else{
    std::cout<<"faied to lease session from connection pool"<<std::endl;
    }
  soci::session& s = pool.at(sessionid);      
  s.open("oracle",std::string(connectstr));
  //soci::session s(pool);
  unsigned long long iovtagid = iov_gettag( s,"bcm1fv1" );
  std::cout<<"iovtagid "<<iovtagid<<std::endl;
  //int runnum = 246456;    
  //int runnum = 246435;
  int runnum = 260576;
  CalibStringData r = iov_gettagdata( s,iovtagid,runnum );
  s.close();
  pool.give_back(sessionid);
  std::cout<<"valid tag data for run "<<runnum<<std::endl;
  std::cout<<"since "<<r.since<<std::endl;
  std::cout<<"till "<<r.till<<std::endl;
  std::cout<<"funcname "<<r.func_name<<std::endl;
  std::cout<<"funcpayload "<<r.func_payload<<std::endl;
  std::map<std::string, std::string> payloadresult;
  parsePayloadString(r.func_payload, payloadresult);
  std::cout<<"payload key value"<<std::endl;
  for(std::map<std::string, std::string>::iterator it=payloadresult.begin(); it!=payloadresult.end(); ++it){
    std::cout<<it->first<<" "<<it->second<<std::endl;
  }    
}
