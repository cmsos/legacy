#!/usr/bin/env python

###############################################################################
## Configure the PI to report WARNING on its TTS output.
###############################################################################

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PIWarn(CmdLineBase):

    def main(self):
        pi = get_pi_hw(self.ip_address, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Forcing TTS WARNING state."

        pi.write("pi.tts_output_control.tts_out_disabled_value", 0x1)
        pi.write("pi.tts_output_control.tts_out_enable", 0x0)

        # End of main().

    # End of class PIWarn.

###############################################################################

if __name__ == "__main__":

    PIWarn().run()
    print "Done"

###############################################################################
