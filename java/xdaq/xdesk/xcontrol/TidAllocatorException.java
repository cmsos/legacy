package xdaq.xdesk.xcontrol;

public class TidAllocatorException extends Exception
{
	TidAllocatorException () { super(); }

	TidAllocatorException ( String msg )
	{
		super (msg);
	}
}
