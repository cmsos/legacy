//
//  File Name:
//
//      RUInterface.h
//
//  File Type:
//
//      C++ Header file
//
//  Creative Authors:
//
//      Luciano B. ORSINI
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71615 
//      E-mail:Luciano.Orsini@cern.ch
//      
//      
//      Johannes Gutleber
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71536
//	E-mail:Johannes.Gutleber@cern.ch
//
//  Change History:
//
//      $Log: RUInterface.h,v $
//      Revision 1.1  1999/07/30 16:12:24  lorsini
//      Reference: LORSINI
//      Software Change:
//      	Source Created.
//
//
//
//  Version Number
//
//      @(#)$Id: RUInterface.h,v 1.1 1999/07/30 16:12:24 lorsini Exp $
//
//

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// The following pre-processor directives ensure that spurious errors will not
// be generated in the event that this header file is included several times.
 
#ifndef __RUInterface_h__
#define __RUInterface_h__

//  ------------------------------------------------------------------------ 
//  ------------------------  Include Header Files  ------------------------
//  ------------------------------------------------------------------------
 
// Include the following system header files:
 

  
// Include the following vxRU  header files:
 
#include "DAQBinding.h" 
 
//  ------------------------------------------------------------------------ 
//  ----------------------  Public Class Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//

//
//  Class:
//
//     RUInterfaceI 
//
//  Superclasses:
//
//     NONE 
//
//
// Class Arguments
//
//
//  Description:
//
//  The interface for the Readout Unit. From this file the stubs and 
//  skeletons have to be derived using an agreed protocol.
// 
class RUInterface
{

//
//  Member Functions:
//

//
//      This class defines the following public member functions:
//
//	
	public:
 	 
	 //
	 // Called by EVM to request a readout for an event.
 	 // If the event exists the command override the existing
	 // event with the new one requested
 	 //
 	 virtual void readout (EventIdentifier* eid) = 0;
 	 //
 	 // Called by EVM or FUI to request a fragment for an event.
 	 // The cookie is opaque and is returned in the "cache" call 
  	 // of class "FUI"
  	 //
  	 virtual void send (EventIdentifier* eid, DestinationCookie* dcookie) = 0;

  	
};


//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// Terminate the #if ... #endif pre-processor control directive located at the
// start of this header file.
 
#endif /* ifndef  __RUInterface_h__ */

