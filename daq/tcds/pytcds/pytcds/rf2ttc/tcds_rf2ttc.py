###############################################################################
## Utilities related to the RF2TTC VME board.
###############################################################################

import time

from pytcds.rf2ttc.tcds_rf2ttc_constants import ADDRESS_TABLE_FILE_NAME_RF2TTC
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_constants import BOARD_TYPE_RF2TTC
from pytcds.utils.tcds_vmeboard import TCDSVME
from pytcds.utils.tcds_utils_vme import get_hw_vme

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class RF2TTC(TCDSVME):

    @staticmethod
    def get_base_address(slot_number):
        res = (slot_number << 20)
        # End of get_base_address().
        return res

    def __init__(self, board_type, vme_chain, vme_unit, slot_number, nodes, verbose=False):
        base_address = RF2TTC.get_base_address(slot_number)
        super(RF2TTC, self).__init__(board_type, vme_chain, vme_unit, slot_number, base_address, nodes, verbose)
        # End of __init__().

    def get_firmware_version(self, which="system"):
        res = "n/a"
        if which == "system":
            tmp = self.read("firmware_id")
            res = "{0:x}".format(tmp)
        # End of get_firmware_version()
        return res

    def get_firmware_date(self, which=None):
        res = "n/a"
        # End of get_firmware_date()
        return res

    # The RF2TTC I2C registers (Delay25 and TTCrx) have to be treated
    # a bit differently.
    def read(self, reg_name_or_address):
        res = None
        res_reg = None
        if reg_name_or_address.startswith("bc_delay25_") or \
           reg_name_or_address.startswith("orbin_delay25_") or \
           reg_name_or_address.startswith("orbout_delay25_"):
            res_reg = "delay25_reg"
            super(RF2TTC, self).read(reg_name_or_address)
            time.sleep(1.e-2)
            res = super(RF2TTC, self).read(res_reg)
            # Apply mask based on the original register!
            node = self.get_node(reg_name_or_address)
            mask = node.mask
            if not mask is None:
                res &= mask
                helper = mask
                while ((helper & 0x1) == 0x0):
                    res >>= 1
                    helper >>= 1
        # elif reg_name_or_address.startswith("_") or \
        #     res_reg = "ttcrx_reg"
        #     super(RF2TTC, self).read(reg_name_or_address)
        #     time.sleep(1.e-2)
        #     res = super(RF2TTC, self).read(res_reg)
        else:
            res = super(RF2TTC, self).read(reg_name_or_address)
        return res

    # End of class RF2TTC.

###############################################################################

def get_rf2ttc_hw(vme_chain,
                  vme_unit,
                  vme_slot,
                  verbose=False):

    rf2ttc = get_hw_vme(BOARD_TYPE_RF2TTC,
                        ADDRESS_TABLE_FILE_NAME_RF2TTC,
                        vme_slot,
                        vme_chain,
                        vme_unit,
                        verbose,
                        RF2TTC)

    return rf2ttc

###############################################################################
