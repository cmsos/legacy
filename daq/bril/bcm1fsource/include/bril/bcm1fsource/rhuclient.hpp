/***************************************************************************** 
 * File  rhuclient.hpp
 * created on 04.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/

#ifndef RHUCLIENT_HPP_
#define RHUCLIENT_HPP_

#include <string>
#include <boost/shared_ptr.hpp>
//#include "tools/Interface.hpp"
//#include "tools/DataSink.hpp"
#include "Interface.hpp"
#include "DataSink.hpp"
#include "rhutypes.hpp"

namespace rhu {

  typedef enum {
    ERROR_OK = 0,
    ERROR_BUFFERS_NOT_AVAILABLE,
    ERROR_NO_CONNECTION,
    ERROR_TIMEOUT,
    ERROR_NO_ACQUISITION
  } error_code_t;

  typedef int event_type_t;
  typedef int run_state_t;

typedef struct
{
  // number of histogram where an overflow has been detected
  int histogramOverflowCount;
  // number of total irq
  int irqCount;
} Statistic;

  // Class to hold histogram data
  // This class is the owner of the data
  // thus, pointer retrieved by this class will
  // invalidate once the last copy of this class is disposed
  class IEvent : private Interface
  {
  public:
    typedef boost::shared_ptr<struct HistogramData> PHistogramData;
    typedef boost::shared_ptr<struct PostmortemData> PPostmortemData;

    // returns true, if event contains Histogramdata
    bool isHistogramData() const
    {
      return getHistogramData();
    }
    // returns true, if event contains Postmortemdata
    bool isPostmortemData() const
    {
      return getPostmortemData();
    }
    // get Pointer to HistogramData
    virtual HistogramData const* getHistogramData() const = 0;
    // get PostmortemData
    virtual PostmortemData const* getPostmortemData() const = 0;
  };

  typedef boost::shared_ptr<IEvent> PEvent;

  typedef DataSinkListener<struct DeviceConfig> DeviceConfigListener;
  typedef DataSinkListener<struct DeviceStatus> DeviceStatusListener;

  // Abstract Client Interface to Connect to a RHU Device
  class IClient : public Interface
  {
  private:
    void dummy();
  public:
    virtual ~IClient();
    // Connection Handling
    // connect to the rhu-device
    virtual void connect(const std::string& device) = 0;
    // Reconnect to the rhu-device
    virtual void reconnect() = 0;
    // Disconnect from the rhu-device
    virtual void disconnect() = 0;
    // returns true, if connection is active
    virtual bool isConnected() const = 0;

    // receive data,

    // -- Configuration

    class Config : public Interface
    {
    public:
      // Disable/Enable Channels
      // Enables or Disables the given Channel
      // Disabled channels are not included in the acquisition
      // Index must be >=0 <10 or an exception is thrown
      virtual void setChannelEnable(int index, bool enable) = 0;
      // returns true, if given channel is enabled
      // Index must be >=0 <10 or an exception is thrown
      virtual bool isChannelEnabled(int index) const = 0;

      // Configures the Orbit Trigger Delay
      // delay >=0
      virtual void setOrbitTriggerDelay(int delay) = 0;
      // Returns actual Orbit Trigger Delay
      virtual int getOrbitTriggerDelay() const = 0;

      // Configures Bunch Trigger Delay
      virtual void setBunchTriggerDelay(int delay) = 0;
      // Returns actual Bunch Trigger Delay
      virtual int getBunchTriggerDelay() const = 0;

      // sets the number of orbits to continue daq after beam abort event
      virtual void setBeamAbortOrbits(int orbits) = 0;
      // returns beam abort orbits
      virtual int getBeamAbortOrbits() const = 0;

      // activate beam abort timeout and auto continue
      // if timeout > 0, acquisition will start automatically after N ms
      // if timeout = 0, no auto continuation is active
      // timeout >=0 ; < 65536
      virtual void setBeamAbortTimeout(int timeout) = 0;
      // returns the beam abort timeout setting
      virtual int getBeamAbortTimeout() const = 0;

      // set Histogram Orbit Counter, the number of orbits to fill into a
      // histogram
      // orbits > 100 ; < 65536
      virtual void setHistogramOrbitCount(int orbits) = 0;
      // Returns the Number of Orbits filled into a histogram
      virtual int getHistogramOrbitCount() const = 0;

      virtual bool isSimulatorEnable() const = 0;
      virtual void setSimulatorEnable(bool enable) = 0;

      virtual bool isSampleModeEdge() const = 0;
      virtual void setSampleModeEdge(bool enable) = 0;

      virtual bool isPostmortemAutoRestart() const = 0;
      virtual void setPostmortemAutoRestart(bool enable) = 0;

      // Returns a Device Config Listener
      // virtual DeviceConfigListener getConfigListener() = 0;
    };

    // --- Get Status and Statistics

    class Status : public Interface
    {
    public:
      // returns the actual orbit counter
      //  - attention, this is only snapshot of a 10KHz Counter
      //    at the moment of reading the value, the counter will have changed
      // virtual u64_t getActualOrbitCounter() const = 0;
      // resets the orbit counter to 0
      // virtual void  resetOrbitCounter() = 0;

      // returns the statistic of the module
      // virtual void getStatistic(Statistic& statistic) const = 0;

      // returns true, if orbit trigger is available
      virtual bool isOrbitTriggerAvailable() const = 0;
      // returns true, if bunch clock is available
      virtual bool isBunchClockAvailable() const = 0;

      // returns the actual board firmware revision
      virtual unsigned short getFirmewareRevision() const = 0;

      // returns the actual board firmware revision
      virtual unsigned short getTemperature() const = 0;

      // Returns a Device Config Listener
      // virtual DeviceStatusListener getStatusListener() = 0;
    };

    // --- State Control of the Module

    class DAQ : public Interface
    {
    public:
      // returns actual operation mode
      virtual DeviceMode getOperationMode() const = 0;
      // sets the operation mode
      virtual void setOperationMode(DeviceMode mode) = 0;

      // --- Event Handling

      // Waits for an Event from the rhu-device
      // returns 0 if data is available
      // returns error code on error
      virtual error_code_t waitForEvent(int timeout, PEvent& event) = 0;

      // --- Postmortem Data Handling

      // Retrieves Postmortem Data
      // index=0, latest Postmortem Buffer
      // index=1, Postmortem Buffer just before latest
      // index = 0..4
      // Returns 0 on success or error code
      virtual error_code_t getPostmortemBuffer(int index) = 0;

      // Returns number of existing Postmortem Buffer that are filled
      virtual int getPostmortemBufferCount() const = 0;
    };

    // returns a reference to the Config Object
    virtual Config& config() = 0;
    // returns reference to the status object
    virtual Status& status() = 0;
    // returns reference to the DAQ Object
    virtual DAQ& daq() = 0;

  };

} // namespace rhu

#endif /* RHUCLIENT_HPP_ */
