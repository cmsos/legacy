# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2017, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

##
#
# This is the TriDAS/daq/xdata Package Makefile
#
##
BUILD_HOME:=$(shell pwd)/../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

ifndef MFDEFS_SUPPORT
include  $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.extern_coretools
include  $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.coretools
else
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)
endif

#
# Packages to be built
#
Project=$(PROJECT_NAME)
Package=xdata

EXDRSources=\
	exdr/FixedSizeInputStreamBuffer.cc \
	exdr/AutoSizeOutputStreamBuffer.cc \
	exdr/InputStreamBuffer.cc \
	exdr/OutputStreamBuffer.cc \
	exdr/StreamBuffer.cc \
	exdr/FixedSizeOutputStreamBuffer.cc \
	exdr/IntegerSerializer.cc \
	exdr/UnsignedShortSerializer.cc \
	exdr/UnsignedLongSerializer.cc \
	exdr/UnsignedIntegerSerializer.cc \
	exdr/UnsignedInteger64Serializer.cc \
	exdr/UnsignedInteger8Serializer.cc \
	exdr/UnsignedInteger16Serializer.cc \
	exdr/UnsignedInteger32Serializer.cc \
	exdr/Integer64Serializer.cc \
	exdr/Integer8Serializer.cc \
	exdr/Integer16Serializer.cc \
	exdr/Integer32Serializer.cc \
	exdr/FloatSerializer.cc \
	exdr/DoubleSerializer.cc \
	exdr/BooleanSerializer.cc \
	exdr/StringSerializer.cc \
	exdr/VectorSerializer.cc \
	exdr/BagSerializer.cc \
	exdr/PropertiesSerializer.cc \
	exdr/TableSerializer.cc \
	exdr/MimeSerializer.cc \
	exdr/TimeValSerializer.cc \
	exdr/VectorIntegerSerializer.cc \
	exdr/VectorInteger8Serializer.cc \
	exdr/VectorInteger16Serializer.cc \
	exdr/VectorInteger32Serializer.cc \
	exdr/VectorInteger64Serializer.cc \
	exdr/VectorUnsignedIntegerSerializer.cc \
	exdr/VectorUnsignedInteger8Serializer.cc \
	exdr/VectorUnsignedInteger16Serializer.cc \
	exdr/VectorUnsignedInteger32Serializer.cc \
	exdr/VectorUnsignedInteger64Serializer.cc \
	exdr/VectorUnsignedShortSerializer.cc \
	exdr/VectorBooleanSerializer.cc \
	exdr/VectorFloatSerializer.cc \
	exdr/VectorDoubleSerializer.cc \
	exdr/VectorTimeValSerializer.cc \
	exdr/VectorStringSerializer.cc \
	exdr/Serializer.cc

SOAPSources=\
	soap/Serializer.cc \
	soap/TimeValSerializer.cc \
	soap/BagSerializer.cc \
	soap/PropertiesSerializer.cc \
	soap/BooleanSerializer.cc \
	soap/DoubleSerializer.cc \
	soap/FloatSerializer.cc \
	soap/InfoSpaceSerializer.cc \
	soap/IntegerSerializer.cc \
	soap/StringSerializer.cc \
	soap/UnsignedLongSerializer.cc \
	soap/UnsignedIntegerSerializer.cc \
	soap/UnsignedInteger32Serializer.cc \
	soap/UnsignedInteger64Serializer.cc \
	soap/Integer32Serializer.cc \
	soap/Integer64Serializer.cc \
	soap/UnsignedShortSerializer.cc \
	soap/VectorSerializer.cc \
	soap/MimeSerializer.cc \
	soap/TableSerializer.cc

CSVSources=\
	csv/Serializer.cc \
	csv/BagSerializer.cc \
	csv/PropertiesSerializer.cc \
	csv/BooleanSerializer.cc \
	csv/DoubleSerializer.cc \
	csv/FloatSerializer.cc \
	csv/InfoSpaceSerializer.cc \
	csv/IntegerSerializer.cc \
	csv/StringSerializer.cc \
	csv/UnsignedLongSerializer.cc \
	csv/UnsignedShortSerializer.cc \
	csv/VectorSerializer.cc

Sources=\
	AbstractVector.cc \
	Mime.cc \
	Table.cc \
	TableIndex.cc \
	TableIterator.cc \
	TableAlgorithms.cc \
	XStr.cc\
	Bag.cc\
	Properties.cc\
	String.cc\
	XMLDOM.cc \
	EventDispatcher.cc \
	Event.cc \
	ItemEvent.cc \
	ItemGroupEvent.cc \
	InfoSpace.cc \
	InfoSpaceFactory.cc \
	Serializable.cc \
	version.cc \
	$(EXDRSources) \
	$(SOAPSources) \
	$(CSVSources)

#ifndef 


IncludeDirs = \
	$(XERCES_INCLUDE_PREFIX) \
	$(MIMETIC_INCLUDE_PREFIX) \
	$(CONFIG_INCLUDE_PREFIX) \
	$(XCEPT_INCLUDE_PREFIX) \
	$(TOOLBOX_INCLUDE_PREFIX) \
	$(XOAP_INCLUDE_PREFIX)

	
LibraryDirs = \
	$(LOG4CPLUS_LIB_PREFIX)  \
	$(XERCES_LIB_PREFIX) \
	$(MIMETIC_LIB_PREFIX) \
	$(XEPT_LIB_PREFIX) \
	$(TOOLBOX_LIB_PREFIX)

TestIncludeDirs = \
	$(XDAQ_ROOT)/daq/extern/cppunit/$(XDAQ_OS)$(XDAQ_PLATFORM)/include \
	$(XOAP_INCLUDE_PREFIX)

TestLibraryDirs = \
	$(XDAQ_ROOT)/daq/extern/cppunit/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/extern/xerces/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/extern/log4cplus/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/extern/mimetic/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/extern/asyncresolv/$(XDAQ_OS)$(XDAQ_PLATFORM)/lib \
	$(XDAQ_ROOT)/daq/xcept/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM) \
	$(XDAQ_ROOT)/daq/toolbox/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)
	

UserCFlags = -DHAVE_INTTYPES_H
UserCCFlags = -DHAVE_INTTYPES_H
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =

# These libraries can be platform specific and
# potentially need conditional processing
#
Libraries =

#
# Compile the source files and create a shared library
#
DynamicLibrary=xdata
StaticLibrary=
Executables=

TestDynamicLibrary=testSuites
TestLibraries=asyncresolv log4cplus toolbox xcept xdata cppunit xerces-c mimetic

#TableTestSuite.cpp \

TestSources= \
	EXDRSerializerTestSuite.cc \
	SOAPSerializerTestSuite.cc \
	InfoSpaceTestSuite.cc \
	FloatTestSuite.cpp \
	DeclarationTestSuite.cc \
	IntegerTestSuite.cpp

	
TestExecutables=Main.cpp

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
