package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.UserCommandWindow;

public class CommandUserAction extends AbstractAction
{
	xdaqWin win_;

	public CommandUserAction (xdaqWin frame)
	{
		super ("User command...");
		win_ = frame;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		UserCommandWindow w = new UserCommandWindow ( win_ );
	}
}
