#ifndef __StreamConfigInfoSpaceHandler
#define __StreamConfigInfoSpaceHandler

#include "d2s/utils/StreamInfoSpaceHandler.hh"
//#include "amc13controller/DataTracker.hh"

namespace amc13controller
{
    class StreamConfigInfoSpaceHandler:public utils::StreamInfoSpaceHandler {
    public:
        StreamConfigInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS );

        //void registerTrackerItems( DataTracker &tracker );
  
    private:
    };
}

#endif /* __StreamConfigInfoSpaceHandler */
