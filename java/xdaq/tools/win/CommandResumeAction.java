package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CommandResumeAction extends AbstractAction
{
	xdaqWin win_;

	public CommandResumeAction (xdaqWin win)
	{
		super ("Resume");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {
			win_.executeCommandOnSelection ("Resume", null, win_.getPaths());
		}
		catch(xdaqWinException ex )
		{
		
		}

	}
}
