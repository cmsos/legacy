#ifndef _tcds_ici_TCADeviceLPM_h_
#define _tcds_ici_TCADeviceLPM_h_

#include <stdint.h>
#include <string>

#include "tcds/lpm/Definitions.h"
#include "tcds/pm/TCADevicePMCommonBase.h"

namespace tcds {
  namespace lpm {

    /**
     * Implementation of Local Partition Manager (LPM)
     * functionality. The LPM is based on the FC7 FMC carrier board.
     */
    class TCADeviceLPM : public tcds::pm::TCADevicePMCommonBase
    {

    public:
      TCADeviceLPM();
      virtual ~TCADeviceLPM();

      void configureTriggerAlignment() const;
      void configureExternalTriggerSettings() const;
      tcds::definitions::LPM_EXTERNAL_TRIGGER_SOURCE_MODE readExternalTriggerSourceMode(uint32_t const trigNum) const;
      tcds::definitions::LPM_EXTERNAL_TRIGGER_INVERSION_MODE readExternalTriggerInversionMode(uint32_t const trigNum) const;
      tcds::definitions::LPM_EXTERNAL_TRIGGER_LOGIC_MODE readExternalTriggerLogicMode(uint32_t const trigNum) const;

    protected:
      virtual std::string regNamePrefixImpl() const;

      virtual bool bootstrapDoneImpl() const;
      virtual void runBootstrapImpl() const;

    };

  } // namespace lpm
} // namespace tcds

#endif // _tcds_ici_TCADeviceLPM_h_
