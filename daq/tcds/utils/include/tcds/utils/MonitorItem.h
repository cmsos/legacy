#ifndef _tcds_utils_MonitorItem_h_
#define _tcds_utils_MonitorItem_h_

#include <string>

namespace tcds {
  namespace utils {

    class InfoSpaceHandler;

    class MonitorItem
    {

    public:
      MonitorItem(std::string const& name,
                  std::string const& description,
                  InfoSpaceHandler* const infoSpaceHandler,
                  std::string const& docString="");

      std::string getName() const;
      std::string getDescription() const;
      InfoSpaceHandler* getInfoSpaceHandler() const;
      std::string getDocString() const;

    private:
      std::string name_;
      std::string description_;
      InfoSpaceHandler* infoSpaceHandlerP_;
      std::string docString_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_MonitorItem_h_
