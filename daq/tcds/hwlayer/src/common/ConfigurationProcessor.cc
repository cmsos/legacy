#include "tcds/hwlayer/ConfigurationProcessor.h"

tcds::hwlayer::ConfigurationProcessor::ConfigurationProcessor()
{
}

tcds::hwlayer::ConfigurationProcessor::~ConfigurationProcessor()
{
}

tcds::hwlayer::ConfigurationProcessor::RegValVec
tcds::hwlayer::ConfigurationProcessor::parse(std::string const& configurationString) const
{
  return parseImpl(configurationString);
}

std::string
tcds::hwlayer::ConfigurationProcessor::compose(tcds::hwlayer::ConfigurationProcessor::RegValVec const& hwConfiguration) const
{
  return composeImpl(hwConfiguration);
}
