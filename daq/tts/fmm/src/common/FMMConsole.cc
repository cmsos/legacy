/**
*      @file FMMConsole.cc
*
*            Console App to control the tts::FMM.
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.11 $
*     $Date: 2007/03/27 07:53:29 $
*
*
**/
#include "tts/fmm/FMMCrate.hh"
#include "tts/fmm/FMMDeadTimeMonitor.hh"
#include "tts/fmm/FMMRegisterTest.hh"
#include "tts/fmm/FMMZBTTest.hh"
#include "tts/fmm/FMMFunctionalTest.hh"
#include "tts/fmm/FMMRandomTransitionTest.hh"
#include "tts/fmm/FMMPoissonTransitionGenerator.hh"
#include "tts/fmm/FMMTransitionTest.hh"
#include "tts/fmm/FMMOpenInputsTest.hh"
#include "tts/fmm/FMMFullSelfTest.hh"
#include "tts/ttsbase/TTSState.hh"

#include "tts/fmmtd/FMMTDCard.hh"
#include "tts/fmm/FMMTTResetTest.hh"


#include "tts/fmmdbi/FMMDBI.hh"
namespace tts {
  namespace fmmdbi {
    tts::FMMDBI* getOracleDBI(std::string user, std::string pwd, std::string db);
  }
}


#include <iostream>
#include <iomanip>
#include <vector>
#include <sstream>
#include <sys/ioctl.h>
#include <stdint.h>
#include <stdlib.h>

#include <unistd.h>

void display_fmm_status( tts::FMMCard* fmm );
void display_fmm_menu( std::vector<tts::FMMCard*> const &fmms, uint32_t active_fmm, tts::FMMTDCard* td );
bool do_fmm_action( std::string const& st, std::vector<tts::FMMCard*> &fmms, uint32_t& active_fmm, tts::FMMTDCard* td);
tts::TTSState readState();

int main() {

  try {

    std::cout << "==== FMMConsole started ... " << std::endl << std::endl;
    bool dummy = false;
    tts::FMMCrate crate(dummy);
  
    if (crate.numFMMs() < 1) { 
      std::cout << "no FMMs found. bye!" << std::endl;
      return -1;
    }

    std::cout << std::endl << "==== detected " << crate.numFMMs() << " FMMs in the crate." << std::endl;
    
    //
    // instantiate drivers
    //
    std::vector<tts::FMMCard*> fmms;
    for (uint32_t i=0; i<crate.numFMMs(); i++) {
      fmms.push_back( &crate.getFMM(i) );
      std::cout << "FMM nr " << i << ": "
	   << fmms[i]->getSerialNumber() << std::endl;
    }
    
    uint32_t active_fmm = 0;

    tts::FMMTDCard *td=0;
    if (crate.hasTD()) td = &crate.getTD();

    std::string st;

    do {
      display_fmm_menu( fmms, active_fmm, td );

      std::cout << std::endl;
      std::cout << "q ... quit" << std::endl;
      std::cout << std::endl;
      std::cout << "your selection: ";
      std::cin >> st;
      std::cout << std::endl;

      bool valid_cmd = do_fmm_action(st, fmms, active_fmm, td);

      if (!valid_cmd && st != "q")
	std::cout << "i'm afraid i don't know what you are talking about ... " << std::endl;

    } while (st != "q");

    std::cout << std::endl << "==== FMMConsole done ... " << std::endl;

  }  catch (std::exception &e) {

    std::cout << "*** Exception occurred : " << std::endl;
    std::cout << e.what() << std::endl;

  }


return 0;
}

#include "fmm_console_helpers.icc"
