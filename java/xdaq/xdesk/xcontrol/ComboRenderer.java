package xdaq.tools.win;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import xdaq.tools.win.CanEnable;

                      
    //JComboBox combo = new JComboBox(items);
    //combo.setRenderer(new ComboRenderer());
    //combo.addActionListener(new ComboListener(combo));
    
public class ComboRenderer extends JLabel implements ListCellRenderer 
{
    public ComboRenderer() 
    {
      setOpaque(true);
      setBorder(new EmptyBorder(1, 1, 1, 1));
    }

    public Component getListCellRendererComponent( JList list, 
           Object value, int index, boolean isSelected, boolean cellHasFocus) {
      if (isSelected) {
        setBackground(list.getSelectionBackground());
        setForeground(list.getSelectionForeground());
      } else {
        setBackground(list.getBackground());
        setForeground(list.getForeground());
      } 
      if (! ((CanEnable)value).isEnabled()) {
        setBackground(list.getBackground());
        setForeground(UIManager.getColor("Label.disabledForeground"));
      }
      setFont(list.getFont());
      setText((value == null) ? "" : value.toString());
      return this;
    }  
}
  

  


