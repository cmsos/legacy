#include "xdata/Properties.h"
#include "bril/bhmprocessor/Application.h"

namespace bril
 {

  namespace bhmprocessor
   {

    class AggregateAlgorithm
     {

      public:
      virtual ~AggregateAlgorithm();

      virtual void aggregate( const Application::OccupancyCache&, Application::AggregatedData&, Application::AggregatedData& ) = 0;

      virtual Application::ChannelIdVector desired_channels() const = 0;

      virtual std::string name() const = 0;

     };

    class AmplitudeAlgorithm
     {

      public:
      virtual ~AmplitudeAlgorithm();

      virtual void compute( const Application::AmplitudeCache&, Application::AmplitudeOutput& ) = 0;

      virtual Application::ChannelIdVector desired_channels() const = 0;

      virtual std::string name() const = 0;

     };

    class BackgroundAlgorithm
     {

      public:
      virtual ~BackgroundAlgorithm();

      virtual void compute( const Application::AggregatedData&, const Application::BeamData&, Application::BackgroundOutput& ) = 0;

      virtual std::string name() const = 0;

     };

    class AlgorithmFactory
     {

      public:
      static AggregateAlgorithm* aggregate( /*const*/ xdata::Properties& );

      static AmplitudeAlgorithm* amplitude( /*const*/ xdata::Properties& );

      static BackgroundAlgorithm* background( /*const*/ xdata::Properties& );

     };

   }

 }
