#include "tcpla/EventHandler.h"

namespace tcpla
{
	friend std::ostream& operator<< (std::ostream &sout, tcpla::EventHandler & eh)
	{
		sout << eh.toHTML();

		return sout;
	}
}

