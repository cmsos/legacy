#ifndef _tcds_hwutilsvme_version_h_
#define _tcds_hwutilsvme_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSHWUTILSVME_VERSION_MAJOR 3
#define TCDSHWUTILSVME_VERSION_MINOR 13
#define TCDSHWUTILSVME_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSHWUTILSVME_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSHWUTILSVME_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSHWUTILSVME_VERSION_CODE PACKAGE_VERSION_CODE(TCDSHWUTILSVME_VERSION_MAJOR,TCDSHWUTILSVME_VERSION_MINOR,TCDSHWUTILSVME_VERSION_PATCH)
#ifndef TCDSHWUTILSVME_PREVIOUS_VERSIONS
#define TCDSHWUTILSVME_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSHWUTILSVME_VERSION_MAJOR,TCDSHWUTILSVME_VERSION_MINOR,TCDSHWUTILSVME_VERSION_PATCH)
#else
#define TCDSHWUTILSVME_FULL_VERSION_LIST TCDSHWUTILSVME_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSHWUTILSVME_VERSION_MAJOR,TCDSHWUTILSVME_VERSION_MINOR,TCDSHWUTILSVME_VERSION_PATCH)
#endif

namespace tcdshwutilsvme {

  const std::string package = "tcdshwutilsvme";
  const std::string versions = TCDSHWUTILSVME_FULL_VERSION_LIST;
  const std::string description = "CMS TCDS helper software.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "Part of the CMS TCDS software.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdshwutilsvme

#endif
