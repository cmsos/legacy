package xdaq.xdesk.jtreeeditor;

import java.io.File;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.tree.DefaultMutableTreeNode;

import org.w3c.dom.Document;

public class MainFrame
    extends JFrame {

  private JTreeEditor editor;
  private JScrollPane scrollPane = new JScrollPane();

//******************************************************************************
   /**An empty constructor*/
   public MainFrame() {
     super("XML Editor");
     this.setSize(700, 500);
     this.getContentPane().setLayout(new BorderLayout());
     this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
     editor = null;
     try {
       editor = new JTreeEditor();
     }
     catch (Exception e) {
       JOptionPane.showMessageDialog(null, "Cannot create Tree");
       return;
     }
     scrollPane.getViewport().add(editor);
     this.getContentPane().add(getTreeMenuBar(), BorderLayout.NORTH);
     this.getContentPane().add(scrollPane, BorderLayout.CENTER);
     this.setVisible(true);
   }

//******************************************************************************
   /**Constructor by file*/
   public MainFrame(File file) {
     super("XML Editor");
     this.setSize(700, 500);
     this.getContentPane().setLayout(new BorderLayout());
     this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

     editor = null;
     try {
       editor = new JTreeEditor(new JTreeEditorModel(file));
     }
     catch (Exception e) {
       JOptionPane.showMessageDialog(null, "Cannot create Tree");
       return;
     }
     scrollPane.getViewport().add(editor);
     this.getContentPane().add(getTreeMenuBar(), BorderLayout.NORTH);
     this.getContentPane().add(scrollPane, BorderLayout.CENTER);
     this.setVisible(true);
   }

//******************************************************************************
   /**Constructor by DOM Document*/
   public MainFrame(Document document) {
     super("XML Editor");
     this.setSize(700, 500);
     this.getContentPane().setLayout(new BorderLayout());
     this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

     editor = null;
     try {
       editor = new JTreeEditor(new JTreeEditorModel(document));
     }
     catch (Exception e) {
       JOptionPane.showMessageDialog(null, "Cannot create Tree");
       return;
     }
     scrollPane.getViewport().add(editor);
     this.getContentPane().add(getTreeMenuBar(), BorderLayout.NORTH);
     this.getContentPane().add(scrollPane, BorderLayout.CENTER);
     this.setVisible(true);
   }

//******************************************************************************
   /**Constructor by DefaultMutableTreeNode*/
   public MainFrame(DefaultMutableTreeNode rootNode) {
     super("XML Editor");
     this.setSize(700, 500);
     this.getContentPane().setLayout(new BorderLayout());
     this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

     editor = null;
     try {
       editor = new JTreeEditor(new JTreeEditorModel(rootNode));
     }
     catch (Exception e) {
       JOptionPane.showMessageDialog(null, "Cannot create Tree");
       return;
     }
     scrollPane.getViewport().add(editor);
     this.getContentPane().add(getTreeMenuBar(), BorderLayout.NORTH);
     this.getContentPane().add(scrollPane, BorderLayout.CENTER);
     this.setVisible(true);
   }

//******************************************************************************
   /**Creates a MenuBar*/
   private JMenuBar getTreeMenuBar() {
     JMenuBar bar = new JMenuBar();
     JMenu fileMenu = new JMenu("File");
     fileMenu.add(new JTreePopupAction("New file", null, editor));
     fileMenu.add(new JTreePopupAction("Load file", null, editor));
     fileMenu.add(new JTreePopupAction("Save as..", null, editor));
     bar.add(fileMenu);
     bar.add(new JTreeMenu(editor, "Edit"));
     return bar;
   }

  public static void main(String[] args) {
    new MainFrame();
  }
}
