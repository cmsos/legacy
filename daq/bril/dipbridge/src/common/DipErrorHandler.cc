#include "bril/dipbridge/DipErrorHandlers.h"
#include <sstream>
#include <iostream>
#include "dip/DipPublication.h"
#include "log4cplus/logger.h"
#include "log4cplus/loggingmacros.h"

namespace bril{
  namespace dipbridge{

    void PubErrorHandler::handleException(DipPublication* publication, DipException& ex){
      std::stringstream ss;
      ss<<"dip pub error "<<ex.what();
      if(!pm_logger){
	LOG4CPLUS_ERROR(*pm_logger,ss.str());
      }else{
	std::cout<<ss.str()<<std::endl;
      }
    }

    void ServerErrorHandler::handleException(int severity, int code, char *msg){
      std::stringstream ss;
      ss<<"dns server error severity "<<severity<<" code "<<code<<" msg "<<msg;	
      if(!pm_logger){
	LOG4CPLUS_ERROR(*pm_logger,ss.str());
      }else{
	std::cout<<ss.str()<<std::endl;
      }
    }

  }}

