package xdaq.tools.win;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class EditSelectAction extends AbstractAction
{
	xdaqWin win_;
	
	public EditSelectAction (xdaqWin win)
	{
		super ("Select");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		SelectWindow w = new SelectWindow ( win_ );
	}
}
