#include "bril/supervisor/Events.h"

bril::supervisor::LumiSectionChangedEvent::LumiSectionChangedEvent():toolbox::Event("urn:bril-supervisor-event:LumiSectionChanged",0){}

bril::supervisor::LumiSectionChangedEvent::~LumiSectionChangedEvent(){}

bril::supervisor::TopicLumiSectionChangedEvent::TopicLumiSectionChangedEvent( const std::string& topicname ):topicname_(topicname){}

bril::supervisor::TopicLumiSectionChangedEvent::~TopicLumiSectionChangedEvent(){}
        
