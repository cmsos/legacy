#include <sstream>
#include "ferol/FerolDataSource.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/ferolConstants.h"
#include "ferol/loggerMacros.h"

ferol::FerolDataSource::FerolDataSource( HAL::HardwareDeviceInterface *device_P,
                                         ferol::MdioInterface *mdioInterface_P,
                                         utils::InfoSpaceHandler &appIS,
                                         Logger logger)
    : OpticalDataSource( device_P, mdioInterface_P, appIS, logger )
{
}

void 
ferol::FerolDataSource::setDataSource() const
{
    if ( dataSource_ == GENERATOR_SOURCE )
        {
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x1 );
            DEBUG( "Setting data source to 1 for Ferol-Generator source operation" );
        }
    else if ( ( dataSource_ == L6G_SOURCE ) || ( dataSource_ == L6G_CORE_GENERATOR_SOURCE ) )
        {
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x2 );
            DEBUG("Setting data source to 2 for L6G operation" );

            this->setup6GInputs(); // this also resyncs i.e. cleans the input fifos.

        }
    else if ( ( dataSource_ == L10G_SOURCE ) || ( dataSource_ == L10G_CORE_GENERATOR_SOURCE ) )
        {
            device_P->write( "FRAGMENT_DATA_SOURCE", 0x3 );
            DEBUG( "Setting data source to 3 for 10G-input operation");
            
            this->setup10GInput(); // this also resyncs i.e. cleans the input fifos.
        }
    else
        {
            std::stringstream msg;
            msg << "Encountered illegal DataSource for operationMode \"" 
                << operationMode_ 
                << "\" : \""
                << dataSource_
                << "\". Cannot continue!";
            ERROR( msg );
            XCEPT_RAISE( utils::exception::FerolException, msg.str() );

        }
}
