package xdaq.tools.win;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;


import javax.swing.JSplitPane;

import org.w3c.dom.*;

// This class defines three panes, the left one is
// dedicated to display the XDAQ configuration tree.
// The right one is split into two halfs. The upper pane
// can display a list of daqlets, the lower one displays
// a selected Daqlet
//


public class xdaqWinSplitPane extends JPanel
{
	//JSplitPane daqletViewSplit_;
	private JSplitPane treeViewSplit_;
	JEditorPane infoPanel_ = new JEditorPane ("text/html", "");
		
	public xdaqWinSplitPane()
	{
		createLayout();
		infoPanel_.setEditable(false);
	}
	
	
	private void createLayout()
	{
		setLayout(new GridLayout(1, 1));

				
		treeViewSplit_ = new JSplitPane
		(
			JSplitPane.HORIZONTAL_SPLIT,
			false,
			null,
			null
		);
		
		treeViewSplit_.setDividerLocation(100);
		treeViewSplit_.setRightComponent ( new JScrollPane (infoPanel_));
		add(treeViewSplit_);
	}
		
	public void addTreePane ( JPanel dummytree )
	{
		treeViewSplit_.setLeftComponent (new JScrollPane (dummytree));
	}
	
	public void addTreePane ( JTree tree )
	{
		treeViewSplit_.setLeftComponent (new JScrollPane (tree));
	}
	
	public void showInfo (String content )
	{
		infoPanel_.setText (content);
	
	}
	
		
}
