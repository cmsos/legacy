#ifndef _bril_vdmmonitor_SigmaData_h_
#define _bril_vdmmonitor_SigmaData_h_
#include <string>
#include <vector>
#include <sstream>
namespace bril{
  namespace vdmmonitor{
    class SigmaData{
    public:
    SigmaData():fillnum(0),runnum(0),timestampsec(0),avgsigma(0),avgsigmaerror(0),avgpileup(0),avgemittanceX(0),avgemittanceY(0),avglongitudinal_term(0),avgemittanceX_err(0),avgemittanceY_err(0){ 
	bxsigma.resize(3564,0);
	bxsigmaerror.resize(3564,0);
  pileup.resize(3564,0);
  bxemittanceX.resize(3564,0);
  bxemittanceY.resize(3564,0);
  bxlongitudinal_term.resize(3564,0);
  bxemittanceX_err.resize(3564,0);
  bxemittanceY_err.resize(3564,0);

      }
      unsigned int fillnum;
      unsigned int runnum;
      unsigned int timestampsec;
      float avgsigma; // [mm]
      float avgsigmaerror; // [mm]
      float avgpileup; 
      float avgemittanceX; // [um]
      float avgemittanceY; // [um]
      float avglongitudinal_term;
      float avgemittanceX_err; // [um]
      float avgemittanceY_err; // [um]
      std::vector<float> bxsigma; // [mm]
      std::vector<float> bxsigmaerror; // [mm]
      std::vector<float> pileup;
      std::vector<float> bxemittanceX; // [um]
      std::vector<float> bxemittanceY; // [um]
      std::vector<float> bxlongitudinal_term;
      std::vector<float> bxemittanceX_err; // [um]
      std::vector<float> bxemittanceY_err; // [um]
      std::string toString() const{
	std::stringstream ss;
	ss<<" fill "<<fillnum<<" run "<<runnum<<" timestampsec "<<timestampsec<<" avgsigma "<<avgsigma<<" avgpileup "<<avgpileup<<" avgsigmaerror "<< avgsigmaerror << " avgemittanceX "<< avgemittanceX << " avgemittanceY "<<avgemittanceY<< " avgemittanceX_err "<< avgemittanceX_err<< " avgemittanceY_err "<<avgemittanceY_err<< " avglongitudinal_term "<< avglongitudinal_term;
	return ss.str();
      }
    };
  }//end ns vdmmonitor
}//end ns bril
#endif
