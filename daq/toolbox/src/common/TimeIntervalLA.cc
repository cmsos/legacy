// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius				                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "toolbox/TimeIntervalLA.h"
#include "toolbox/exception/Exception.h"



toolbox::TimeIntervalLA::TimeIntervalLA(const std::string & in) :  lexeme_("")
{
	//std::cout << "--->>>>i got the following time: " << in << std::endl;
	instream_.str(in);
	//std::cout << "--->>>>i got the following stream: " << instream_.str() << std::endl;

}

toolbox::TimeIntervalLA::~TimeIntervalLA()
{

}

std::string  toolbox::TimeIntervalLA::getLexeme()
{
	return lexeme_;
}

toolbox::TimeIntervalLA::Token toolbox::TimeIntervalLA::getNextToken()
{
	char c;
	lexeme_="";
	if (instream_ >> c)
	{
		lexeme_ += c;
		switch (c)
		{
		case 'P':
			return PSYMBOL;
			break;
		case 'T':
			return TSYMBOL;
			break;
		case 'Y':
			return YEARS;
			break;
		case 'M':
			return MSYMBOL; // minutes or months, let syntax analyzer to resolve
			break;
		case 'D':
			return DAYS;
			break;
		case 'W':
			return WEEKS;
			break;
		case 'H':
			return HOURS;
			break;
		case 'S':
			return SECONDS;
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			while (std::isdigit(instream_.peek()))
			{
				instream_ >> c;
				lexeme_ += c;
			}
			return INTVAL;
			break;
		default:
			std::stringstream msg;
			msg << "Lexical error, invalid character [" << std::hex << (size_t)c << std::dec << "]";
			XCEPT_RAISE(toolbox::exception::Exception, msg.str());
		}
	}
	return EMPTY;
}
