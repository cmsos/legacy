// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdlib.h>
 
#include "psx/mapi/ApplicationService.h"
#include "psx/mapi/RequestFactory.h"
#include "xoap/domutils.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"

#include "psx/mapi/TakeRequest.h"
#include "psx/mapi/ReleaseRequest.h"
#include "psx/mapi/IncludeRequest.h"
#include "psx/mapi/ExcludeRequest.h"
#include "psx/mapi/ManualRequest.h"
#include "psx/mapi/IgnoreRequest.h"
#include "psx/mapi/SendRequest.h"
#include "psx/mapi/GetStateRequest.h"
#include "psx/mapi/ConnectRequest.h"
#include "psx/mapi/DisconnectRequest.h"

psx::mapi::Request* psx::mapi::RequestFactory::createRequest(xoap::MessageReference message, psx::mapi::ApplicationService* as) 
	
{
	psx::mapi::Request* retVal = 0;

	DOMNode* body  = message->getSOAPPart().getEnvelope().getBody().getDOMNode();
	DOMNodeList * children = body->getChildNodes();
	for ( XMLSize_t i = 0 ; i < children->getLength(); i++ )
	{
		DOMNode * node = children->item(i);
				
		if ( node->getNodeType() == DOMNode::ELEMENT_NODE ) 
		{ 
			if ( xoap::XMLCh2String(node->getLocalName()) == "take" )
			{
				psx::mapi::TakeRequest * rqst = new psx::mapi::TakeRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				std::string exclusive = xoap::getNodeAttribute(node, "exclusive" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
				
				if (exclusive == "true")
				{
					rqst->setExclusive(true);
				}
				else
				{
					rqst->setExclusive(false);
				}
				
				retVal = rqst;
				break;	
			
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "release" )
			{
				psx::mapi::ReleaseRequest * rqst = new psx::mapi::ReleaseRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string all = xoap::getNodeAttribute(node, "all" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
				
				if (all == "true")
				{
					rqst->applyToChildren(true);
				}
				else
				{
					rqst->applyToChildren(false);
				}
				
				retVal = rqst;
				break;				
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "include" )
			{
				psx::mapi::IncludeRequest * rqst = new psx::mapi::IncludeRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
								
				retVal = rqst;	
				break;			
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "exclude" )
			{
				psx::mapi::ExcludeRequest * rqst = new psx::mapi::ExcludeRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
				std::string all = xoap::getNodeAttribute(node, "all" );
				
				if (all == "true")
				{
					rqst->applyToChildren(true);
				}
				else
				{
					rqst->applyToChildren(false);
				}
								
				retVal = rqst;	
				break;			
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "manual" )
			{
				psx::mapi::ManualRequest * rqst = new psx::mapi::ManualRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
								
				retVal = rqst;
				break;				
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "ignore" )
			{
				psx::mapi::IgnoreRequest * rqst = new psx::mapi::IgnoreRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
								
				retVal = rqst;	
				break;			
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "getState" )
			{
				psx::mapi::GetStateRequest * rqst = new psx::mapi::GetStateRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner("");
				
				retVal = rqst;	
				break;			
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "send" )
			{
				psx::mapi::SendRequest * rqst = new psx::mapi::SendRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string command = xoap::getNodeAttribute(node, "command" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				
				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
				rqst->setCommand(command);
				
				retVal = rqst;	
				break;		
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "connect" )
			{
				psx::mapi::ConnectRequest * rqst = new psx::mapi::ConnectRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string object = xoap::getNodeAttribute(node, "object" );
				std::string domain = xoap::getNodeAttribute(node, "domain" );
				std::string owner = xoap::getNodeAttribute(node, "owner" );
				std::string action = xoap::getNodeAttribute(node, "action" );
				std::string url = xoap::getNodeAttribute(node, "url" );
				std::string context = xoap::getNodeAttribute(node, "context" );

				rqst->setObjectName(object);
				rqst->setDomainName(domain);
				rqst->setOwner(owner);
				rqst->setAction(action);
				rqst->setURL(url);
				rqst->setContext(context);
				
				retVal = rqst;	
				break;		
			}
			else if ( xoap::XMLCh2String(node->getLocalName()) == "disconnect" )
			{
				psx::mapi::DisconnectRequest * rqst = new psx::mapi::DisconnectRequest( as );
				std::string nsURI = xoap::XMLCh2String(node->getNamespaceURI());		
				if ( nsURI != psx::mapi::NSURI )
				{
					delete rqst;
					std::string msg = "invalid namespace URI";
					msg += nsURI;
					XCEPT_RAISE(psx::mapi::exception::Exception, msg ); 
				} 
				
				std::string id = xoap::getNodeAttribute(node, "id" );
				rqst->setId(id);
				
				retVal = rqst;	
				break;		
			}
			else
			{
				XCEPT_RAISE(psx::mapi::exception::Exception,"unknown command");
			}			
		}
		
		
	}
	return retVal;	
}
