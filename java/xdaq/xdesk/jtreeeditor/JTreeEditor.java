package xdaq.xdesk.jtreeeditor;

import javax.swing.JTree;

import org.w3c.dom.Document;

/**
 * <p>Title: JTreeEditor </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: CERN</p>
 * @author Sergey Shepelevich
 * @version 1.0
 */

public class JTreeEditor
    extends JTree {
//--------------------------Variables Definition--------------------------------
  private JTreePopupTrigger popupTrigger = null;
  private JTreeCellRenderer treeCellRenderer = null;
  private JTreeConfig config = null;

//---------------------------Constructors---------------------------------------
  /**An empty constructor*/
  public JTreeEditor() {
    super();
    addRenderersAndListeners();
    this.setRowHeight(this.getConfiguration().getRowHeight());
    this.getSelectionModel().setSelectionMode(this.config.
                                              isTreeMultiselectionAllowed() ?
                                              this.getSelectionModel().
                                              DISCONTIGUOUS_TREE_SELECTION :
                                              this.getSelectionModel().
                                              SINGLE_TREE_SELECTION);
  }

//******************************************************************************
   /**Constructor by TreeModel*/
   public JTreeEditor(JTreeEditorModel model) {
     super(model);
     addRenderersAndListeners();
     this.setRowHeight(this.getConfiguration().getRowHeight());
     this.getSelectionModel().setSelectionMode(this.config.
                                               isTreeMultiselectionAllowed() ?
                                               this.getSelectionModel().
                                               DISCONTIGUOUS_TREE_SELECTION :
                                               this.getSelectionModel().
                                               SINGLE_TREE_SELECTION);

   }

//******************************************************************************
   private void addRenderersAndListeners() {
     popupTrigger = new JTreePopupTrigger();
     treeCellRenderer = new JTreeCellRenderer();
     this.addMouseListener(popupTrigger);
     this.setCellEditor(treeCellRenderer);
     this.setCellRenderer(treeCellRenderer);
     if (config == null) {
       config = new JTreeConfig();
     }

     if (this.getModel() != null) {
       this.getModel().addTreeModelListener(treeModelListener);
     }
     this.addTreeSelectionListener(new JTreeEditorSelectionListener());
   }

//******************************************************************************
   /**Changes the GUI configuration*/
   public void setConfiguration(JTreeConfig config) {
     this.config = config;
     this.setRowHeight(config.getRowHeight());
     this.getSelectionModel().setSelectionMode(this.config.
                                               isTreeMultiselectionAllowed() ?
                                               this.getSelectionModel().
                                               DISCONTIGUOUS_TREE_SELECTION :
                                               this.getSelectionModel().
                                               SINGLE_TREE_SELECTION);

   }

//******************************************************************************
   /**Returns current GUI configuration*/
   public JTreeConfig getConfiguration() {
     return config;
   }

//******************************************************************************
   /**Returns a DOM Document according to last changes in tree structure*/
   public Document getDocument() {
     Document doc = null;
     try {
       doc = ( (JTreeEditorModel)this.getModel()).getDocument();
     }
     catch (Exception e) {
       System.out.println(
           "JTreeEditor():getDocument():Cannot create document for saving");
     }
     return doc;
   }
}
