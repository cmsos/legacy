package xdaq.tools.win;

/* Program : A vertical text scroller
/* Title   : Vscroller.java
 * Purpose : Web Text Display Applet
 * Author  : Bill Boulton
 * Date    : July 12, 1997
 *           Final Revision July 15 1997
 * Version : 1.0
 * email   : boulton@amadeus.ccs.queensu.ca
 * Terms   : Distribution of this source is being made in the 
             hope that it may be found useful. It can be
             freely modified and redistributed provided this 
             entire notice is left on the redistribution.
 *           You'll notice that where code is used which was
 *           modified from other source the authors are referenced
 *           as a courtesy and in appreciation of their efforts
 *           to promote "Public Domain" software and the understanding
 *           of the Java Language.
*/

import java.applet.*;
import java.awt.*;
import java.util.*;
import java.awt.image.*;

public class vscroller extends Applet
implements Runnable{
   int NumMessages=0 ;
   String Message[];
   String Messages; /* get number of messages from tags */
   int MaxLines;
   int Columns = 0;
   int NumLines = 0;
   
   char Screen[][]; /* array Maxlines Columns */
   int YLinePos[];
   int YLast[];
   String FontColor;
   String ImageLoadMessage;
   int CursorX;
   int CursorY;
   int Index=0;
   int Xlimit=0,LoopCount=0;
   Thread Life=null;
   char  DisplayMessage[];
   String ScrollParam;
   boolean ScrollFlag = true;
   boolean set = false;
   String BackgroundImage;
   Image Tube;
   boolean ImageAvailable = false;
   boolean MsgDone = false;
   Color BackColor;
   String Bgcolor;

/**************************/ 
/*                        */  
/* Offscreen graphics     */
/* Variables              */
/*                        */
/**************************/

Image FinalScreen; 
Graphics osg;
 
/**************************/ 
/*                        */  
/* Screen graphics     */
/* Variables              */
/*                        */
/**************************/

Graphics g;

/**************************/
/*    Font Variables      */
/**************************/

   Font Screen_Font;
   Font normalFont;
   FontMetrics fm; 
   Color Screen_Font_Color; /* Parameter from html tag */
   private int charWidth;
   private int charHeight;
   private int charDescent;
   private int NumChars; //The number of chars in the string
   private int NumSpaces; // The Number of spaces in the string
   private int LineFeed;

/************************/
/*    Screen Margins    */
/************************/

  private int TopMargin;
  private int LeftMargin;
  private int RightMargin;
  private int BottomMargin;
  int offset;
/************************/
/* Speed and distance   */
/* Variables from Parms */
/************************/
String Divider;
int Divisor;
String Pause;
int Delay;


/*****************************************/
/* Variables used to wrap the Messages  */
/* to which fit into the  Display        */
/* area.                                  */
/*****************************************/

  StringBuffer sb = new StringBuffer(); 
  StringTokenizer st;
  String Line[];

  public void init(){
      BackgroundImage =getParameter("bgImage");
      g=this.getGraphics();
      if(getParameter("NumberMsg") != null)
      {
         Messages = getParameter("NumberMsg");
         NumMessages = Integer.parseInt(Messages);
      }
      else
      NumMessages = 3;
     if(getParameter("Offset") != null) /* used to caclulate the yoffset */
      {
         Divider = getParameter("Offset");
         Divisor = Integer.parseInt(Divider);
      }
      else
      Divisor = 3;
      if(getParameter("TimeMs") != null) /* used to caclulate the yoffset */
      {
         Pause = getParameter("TimeMS");
         Delay = Integer.parseInt(Pause);
      }
      else
      Delay = 300;

      Message = new String[NumMessages];
      Message[0] = getParameter("Msg0");
      if (NumMessages > 1)
      Message[1] = getParameter("Msg1");
      if (NumMessages > 2)
      Message[2]=  getParameter("Msg2");
      if (NumMessages > 3)
      Message[3]=  getParameter("Msg3");
      if (NumMessages > 4)
      Message[4]=  getParameter("Msg4");
      ImageLoadMessage="Loading Image";
      resize(size().width, size().height);
      Screen_Font= initFont();
      setFont(Screen_Font);
     fm = getFontMetrics(Screen_Font);
     if(fm != null)
     {
       charWidth = fm.charWidth(' ');
       charHeight = fm.getHeight();
       charDescent = fm.getDescent();
     }
     
     LineFeed = charHeight + 1; 
     LeftMargin = 5;
     RightMargin = size().width - 5;
     TopMargin = 0;
     BottomMargin = size().height;
     CursorY = size().height;
     CursorX = LeftMargin;
     
         

     /*************************************************/
     /*                                               */
     /*    Find the Maximum Number of                 */
     /*    Characters that will fit into the          */
     /*    Screen Area.                               */ 
     /*    This is used to define the number          */
     /*    of rows and columns in the screen.         */
     /*                                               */
     /*************************************************/  

       
     for (LoopCount = 0; LoopCount < NumMessages-1;LoopCount++)
         FindMaxLength(Message[LoopCount]);

     YLinePos = new int[MaxLines];
     
     initYpos(); /* Initialize the y position array */
      g.setColor(Color.black);
     Screen = new char[MaxLines][Columns];
     Init_Screen_Array(); /* MSIE 3.02 sets elements to null not good */
     if (getParameter("Scroll") != null)
     {
     ScrollParam = getParameter("Scroll");
     if(ScrollParam.equalsIgnoreCase("no"))
     ScrollFlag = false;
     }
     else
     ScrollFlag = true;
     if(getParameter("Colour") != null) 
     {
      FontColor = getParameter("Colour");
      Screen_Font_Color = Set_Color(FontColor);
     }
     else
     Screen_Font_Color = Color.yellow; 
     if(getParameter("Bg") != null)
     {
     Bgcolor = getParameter("Bg");
     BackColor = Set_Color(Bgcolor);
     }
     else
     BackColor = Color.black;
     setBackground(BackColor);
     FinalScreen = createImage(size().width,size().height );
     osg = FinalScreen.getGraphics();
} // end init

public void initYpos()
{
     /*************************************************/
     /*                                               */
     /*    Set up the Line Y positions.               */
     /*    These positions are used by the Insert     */
     /*    Line routine to draw the Strings           */
     /*                                               */
     /*************************************************/
          for (LoopCount = 0; LoopCount < MaxLines-1;LoopCount++)
     {
       if (LoopCount == 0)
       YLinePos[LoopCount] = size().height; 
       else
       {
       YLinePos[LoopCount] = YLinePos[LoopCount - 1] + LineFeed ;
              }
      } // end for
        } // end initialize y positions

/*********************************/
/*                               */
/* The Following 2 routines load */
/* the graphics images well      */
/* they are needed.              */
/* The initscreen also inits     */
/* the font and colour for the   */
/* offscreen graphics.           */
/*                               */
/*********************************/

public void initmain()
{
    if (!ImageAvailable)
    g.drawImage(Tube,0,0,size().width,size().height,this);
    else
    mainnoImage();
}

/*********************************/
/* The Following two routines    */
/* are used to set the background*/
/* to a solid color if no image  */
/* is provided.                  */
/*********************************/ 

public void offsnoImage()
{
    osg.setColor(BackColor);
    osg.fillRect(0,0,size().width,size().height);
    osg.setColor (Screen_Font_Color);
}
public void mainnoImage()
{
    g.setColor(BackColor);
    g.fillRect(0,0,size().width,size().height);
    g.setColor (Screen_Font_Color);
}

public void initscreen()
{   osg.setColor(Screen_Font_Color);
    osg.setFont(Screen_Font); 
    if(!ImageAvailable)
    osg.drawImage(Tube,0,0,size().width,size().height,this);
    else
    offsnoImage();
}
   
/***********************************************/
/*                                             */
/* Microsoft Internet Explorer version 3.0     */
/* initalizes the array with null characters.  */
/* As this causes some unpleasant results it   */
/* is necessary to initialize the screen array */
/* with spaces... Hence the Init_Screen_Array()*/
/* routine.                                    */
/*                                             */
/***********************************************/
public void Init_Screen_Array()
{
   int Lcount, Rcount = 0;
   char Temp;
   try
       {
       for(Lcount = 0; Lcount <=MaxLines;Lcount++)
       for(Rcount = 0; Rcount <=Columns - 1 ;Rcount++)
       {
         Temp = ' ';
         Screen[Lcount][Rcount] = Temp;
       }
       }
       catch (ArrayIndexOutOfBoundsException e){}
} /* Screen array init ends */

/********************************************************/
/*    This routine was designed to return a color       */
/*    for a given parameter.                            */
/*    The default color is green. This generic          */
/*    module can be easily imported into any other      */
/*    Progam.                                           */
/********************************************************/

   public Color Set_Color(String Target)
{
     
     if (Target.equalsIgnoreCase("black"))
         return( Color.black);
      else if (Target.equalsIgnoreCase("white"))
         return(Color.white);
      else if (Target.equalsIgnoreCase("gray"))
         return(Color.gray);
      else if (Target.equalsIgnoreCase("red"))
         return(Color.red);
      else if (Target.equalsIgnoreCase("green"))
        return(Color.green);
      else if (Target.equalsIgnoreCase("blue"))
        return(Color.blue);
      else if (Target.equalsIgnoreCase("yellow"))
        return(Color.yellow);
      else if (Target.equalsIgnoreCase("orange"))
        return(Color.orange);
      else if (Target.equalsIgnoreCase("magenta"))
        return(Color.magenta);
      else if (Target.equalsIgnoreCase("pink"))
        return(Color.pink);
      else if (Target.equalsIgnoreCase("cyan"))
        return(Color.cyan);
      else
      {
         try{
            return(new Color(Integer.parseInt(Target, 16)));}
         catch (NumberFormatException e)
            {return( Color.yellow); }/* Default FontColor not valid */ 
      }          
}
/* This routine simply returns a font for our use */
   public Font initFont()
   {
        String fontname;
        String fontstyle;  
        int style = -1;
        int points;
        Font font;
        Font defaultfont;
        String pointstring;

        defaultfont = getFont();
        fontname = getParameter("Font");
        if(fontname == null)
            fontname = defaultfont.getName();
        fontstyle = getParameter("Style");
        if(fontstyle == null)
            style = defaultfont.getStyle();

        // Get the Font
        if(fontname.equalsIgnoreCase("TimesRoman")  ||
           fontname.equalsIgnoreCase("Helvetica")   ||
           fontname.equalsIgnoreCase("Courier") )
           {
                // Nothing to do the fontname is supported
           }
        else
           {
               fontname = defaultfont.getName();
           }

        if(style == -1) {
            // Get the Font Style
            if(fontstyle.equalsIgnoreCase("bold"))
                style = Font.BOLD;
            else if(fontstyle.equalsIgnoreCase("italic"))
                style = Font.ITALIC;
            else if(fontstyle.equalsIgnoreCase("bolditalic"))
                style = Font.ITALIC|Font.BOLD;
            else
                style = Font.PLAIN;
        }
        pointstring = getParameter("Points");
        if(pointstring == null)
            points = defaultfont.getSize();
        else {
            try {
                points = Integer.parseInt(pointstring);
            } catch (NumberFormatException e) {
                points = defaultfont.getSize();
                
            }
        }
                      
        font = new Font(fontname, style, points);
        return font;
    }       
   public void start(){
      if(Life==null){
         Life=new Thread(this);
         Life.start();
         
      }
   }

   public void stop()
{
      Life.stop();
      Life=null;
      }

 public void run(){
      Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
      while(Life!=null){
      int FinalY = 0;
      int Count= 0;
      String msg = " ";
      for (Count = 0; Count <= NumMessages-1; Count++)
       {
           offset = -2*charHeight;
           CursorX = LeftMargin;
           NumLines = 0;   
           try{
           Process_Line(Message[Count]); // set the screen character array
           }
           catch(ArrayIndexOutOfBoundsException e)
           {g.drawString("run count out of bounds",20,20);}
           FinalY =(NumLines *(LineFeed+charHeight));
           if (NumLines == MaxLines -1)
           FinalY = FinalY - 2*(LineFeed+charHeight); // try to reduce the delay)
           if(ScrollFlag)
           while(!MsgDone )
           { 
             
             update(g);
             offset = offset + charHeight/Divisor;
             try {Thread.sleep(Delay);}
                 catch (InterruptedException e){}
            
             if (FinalY - offset <= 0)
                    MsgDone = true;
           } //end while
           Screen = new char[MaxLines][Columns]; /* clean array */
           Init_Screen_Array();
           MsgDone = false;
           FinalY = 0;
       } // end for
    
   }
}// end run

public synchronized void paint(Graphics g)
{
 int scrollcount;
 CursorX = LeftMargin;
 osg.setFont(Screen_Font);
 osg.setColor(Screen_Font_Color);  
 if(!set)
     {  if(getParameter("bgImage") != null)
     {
      set = true;
      Tube = getImage(getDocumentBase(),BackgroundImage);
      loadImageAndWait(Tube); /* Prepare image before routines start */
      initscreen();
      g.drawString(ImageLoadMessage,20,20);
               
      }
      else 
      {
         ImageAvailable = true;
         set = true;
         initscreen();
      }
  } // end if not set
  if(!ImageAvailable)
  osg.drawImage(Tube,0,0,size().width,size().height,this);
  else
  offsnoImage();
  try
  {
 for (scrollcount = 0;scrollcount <= NumLines-1;  scrollcount++)
 {
   if(YLinePos[scrollcount] - offset <= size().height + 2*charHeight)
   { 
   if (YLinePos[scrollcount] - offset >= - charHeight)
   osg.drawChars(Screen[scrollcount],0,Columns,CursorX,(YLinePos[scrollcount]-offset));
   }
 } //end for
 }
 catch (ArrayIndexOutOfBoundsException e){}
g.drawImage(FinalScreen,0,0,this);
} // end paint
 
public void FindMaxLength(String Input_String)
{
  st = new StringTokenizer(Input_String," ");
  int Lines = 0;
   while (st.hasMoreTokens()) {
      String nextword = st.nextToken();
      if (fm.stringWidth(sb.toString() + nextword) <
        (RightMargin - LeftMargin)) {
        sb.append(nextword);
        sb.append(' ');
      }
      else if (sb.length() == 0) {
         Lines++;
         if(Lines>MaxLines)
         MaxLines = Lines;
       }
      else {
        if (Columns < sb.length())
             Columns = sb.length();
        Lines++ ;
        if (Lines > MaxLines)
        MaxLines = Lines;
        sb = new StringBuffer(nextword + " ");
      }

    } // end While
    if (sb.length() > 0) {
     if (Columns < sb.length())
     {
             Columns = sb.length();
             Lines++;
             MaxLines = Lines;
             
     }
    }

      sb = new StringBuffer();
      MaxLines++;
  } // end find max length

/*************************************************/
/* This routine is passed a string. It then      */
/* wraps the string by measuring the string      */
/* and comparing it to the Display width         */
/* This portion is a modified copy of the source */
/* available from sunsite.unc.edu written by     */
/* Eliotte Rusty Harold elharo@susnite.unc.edu.  */
/* All Rights to this routine belong to Elliote. */ 
/*************************************************/

public void Process_Line(String Input_String)
{
   st = new StringTokenizer(Input_String," ");
   while (st.hasMoreTokens()) {
      String nextword = st.nextToken();
      if (fm.stringWidth(sb.toString() + nextword) <
        (RightMargin - LeftMargin)) {
        sb.append(nextword);
        sb.append(' ');
      }
      else if (sb.length() == 0) {
        Typer(nextword); /* catches the one word case */
         if(NumLines <MaxLines)
         NumLines++;
      }
      else {
        Typer(sb.toString());
         if(NumLines <MaxLines)
         NumLines++;
        sb = new StringBuffer(nextword + " ");
      }

    } // end While
    if (sb.length() > 0) {
      Typer(sb.toString());
       if(NumLines <MaxLines)
       NumLines++;
    }

      sb = new StringBuffer();
  } 

/************************************************/
/* This is the routine that actually does the   */
/* character insertion task. Placing the        */
/* characters in the screen[][] array.          */
/************************************************/ 

  public void Typer(String InString)
{
      if(Life != null)
{
      Xlimit = InString.length() - 1;
     
      DisplayMessage = InString.toCharArray();
      try
      {
      for (NumChars = 0;NumChars<Xlimit;NumChars++)
      {    
           Screen[NumLines][NumChars] = DisplayMessage[NumChars];
         
      } // end For
      }
     catch(ArrayIndexOutOfBoundsException e)
         {
         };
     
                  
              
} // end if !done
} /* end Typer*/


public synchronized void update(Graphics g){
       paint(g);
}//end update
public String getAppletInfo()
{
      return "Scroller  version 1.0 written by Bill Boulton";
}

/***************************************************************/
/*                                                             */
/* The Following two routines were "borrowed" from the creator */
/* of Instant Java. The site where this and much more code is  */
/* available is www.vivids.com.                                */
/* These routines get rid of that pesky image not loaded before*/
/* charcters are displayed problem that has plagued me.        */
/* Many thanks to the author of this code.                     */
/*                                                             */
/***************************************************************/
 /**
     * Begins the preparation (loading) of the image
     * This function returns immediately
     * The image is loaded in a thread
     *
     * @param image the image to prepare
     */
    public void prepareImage(Image image) {
        boolean ImagePrepared;
        ImagePrepared = prepareImage(image, this);
    }      
 public synchronized void loadImageAndWait(Image image) {
        int checkImageFlags;
        boolean ImagePrepared;

        ImagePrepared = prepareImage(image, this);
        if(ImagePrepared == false) {
            while(((checkImageFlags =
                    checkImage(image, this)) &
                            ImageObserver.ALLBITS) == 0) {
                try {
                    wait(100);
                } catch (InterruptedException e){}
            }
        }
    }     
} // end scroller
 
