#include "ttc/ttcci/TTCci.hh"

#include "ttc/ttcci/TTCciConfiguration.hh"
#include "ttc/ttcci/TTCciAddresses.hh"
#include "ttc/utils/RAMTriggers.hh"
#include "ttc/utils/VME64xPNP.hh"
#include "ttc/utils/BGOMap.hh"

#include "hal/VMEBusAdapterInterface.hh"
#include "xcept/tools.h"

#include <boost/lexical_cast.hpp>
#include <unistd.h>
#include <cmath>

// macros

#define N_CYCL_TRIG_ 3
#define N_CYCL_BGO_ 5

// bit packing
#define QPLLAUTORESTART_BIT 4
#define QPLLRESET_BIT 5
#define QPLLENABLE_BIT 6

// Trigger rules start with 2 triggers.
#define TTCCI_NTRR_OFFSET 1

#define DFLTBGOMAP 0xa98765

// for the INSELECT register.
#define CLOCK_BIT(I) (I==TTC?17:(I==CTC?19:(I==LTCIN?18:16)))

// ? (What is bit 13?)
#define TRIGGER_BIT(I) (I==INTERNAL?8:(I==FRONTP1?9:(I==FRONTP0?10:(I==LTCIN?14:(I==CTC?15:(I==CYCLIC?11:(I==BCHANNEL?12:13)))))))
#define BGO_BIT(I) (I==CTC?7:(I==LTCIN?6:(I==VME?5:4)))
#define ORBIT_BIT(I) (I==TTC?1:(I==CTC?3:(I==LTCIN?2:0)))
#define STARTCYCLATSTART_BIT 28


using namespace std;
using namespace ttc::TTCciConfigWords;


// methods in anonymous namespace

namespace
{

ttc::VME64xDeviceInfo
GetTTCciInfo(HAL::VMEBusAdapterInterface& bus, int location)
{
  ttc::VME64xDeviceInfo info(
      ttc::VME64xDeviceInfo::MF_CERN,
      ttc::VME64xDeviceInfo::BOARD_ID_TTCCI,
      location);

  return ttc::VME64xPNP::GetDevice(bus, info);
}

}


// class ttc::TTCci (statics)

uint32_t ttc::TTCci::ComposeLongCommand(
    unsigned address,
    bool isExternal,
    unsigned char subaddr,
    unsigned char data)
{
  assert(address < (1<<14));
  uint32_t retval = address << 18;
  if (isExternal)
    retval |= (1 << 17);

  retval |= (1 << 16); // this bit seems to be fixed to one

  retval |= ((unsigned) subaddr) << 8;
  retval |= data;

  return retval;
}


uint32_t ttc::TTCci::GetTTCrxParallelBusEnableCommand()
{
  return ComposeLongCommand(0, false, 3, 0xb3);
}


bool ttc::TTCci::IsRamBufferAtEnd(uint32_t rmc_register_content)
{
  return ((rmc_register_content >> 31) & 1) != 0;
}


size_t ttc::TTCci::NChannels()
{
  return 16;
}


// class ttc::TTCci (members)

ttc::TTCci::TTCci(
    HAL::VMEBusAdapterInterface& bus,
    int location,
    uint32_t btimecorr,
    uint32_t delayt2corr,
    bool enable_vme_writes_)
:
    GenericTTCModule(
        Address::TTCci,
        bus,
        GetTTCciInfo(bus, location),
        enable_vme_writes_),
    configuringResetsQPLL_(false),
    L1Aenabled_(false),
    bTimeCorrection_(btimecorr),
    delayT2Correction_(delayt2corr),
    trigrules(8),
    boardStatusSaved_(0),
    _n_ctg(N_CYCL_TRIG_),
    _n_cbg(N_CYCL_BGO_),
    _ResumeCylic(false),
    bgo_map(0),
    stop_thread_flag(false)
{
  ram_triggers = new RAMTriggers(*this, TTCciAdd::StartTrigDPRAM);
  bgo_map = new BGOMap(*this, TTCciAdd::BGO_MAP, false);

  for (size_t i = 0; i < NChannels(); ++i)
  {
    bgo.push_back(BChannel(i, delayT2Correction_));
  }

  std::vector<std::string> emptyseq;
  _sequences.push_back(Sequence(emptyseq, "coldReset", true));
  _sequences.push_back(Sequence(emptyseq, "configure", true));
  _sequences.push_back(Sequence(emptyseq, "enable", true));
  _sequences.push_back(Sequence(emptyseq, "stop", true));
  _sequences.push_back(Sequence(emptyseq, "suspend", true));
  _sequences.push_back(Sequence(emptyseq, "periodic", true));
  _sequences.push_back(Sequence(emptyseq, "user", true));

  // Initialize periodic sequence structure.
  PeriodicSeqParameters.newSetup = true;
  PeriodicSeqParameters.myTTCci = this;
  PeriodicSeqParameters.waitNsec = -99.;
  PeriodicSeqParameters.enabled = false;

  pthread_create(&thethread, NULL, Thread4PeriodicSequence, (void*) &PeriodicSeqParameters);

  // Cyclic trigger/BGO generators.
  for (size_t i = 0; i < _n_ctg; ++i)
  {
    _ctg.push_back(CyclicTriggerOrBGO(true, i, bTimeCorrection_));
  }

  for (size_t i = 0; i < _n_cbg; ++i)
  {
    _cbg.push_back(CyclicTriggerOrBGO(false, i, bTimeCorrection_));
  }

  // init trigger rules from hardware
  for (size_t i = FirstTriggerRule(); i < trigrules.size(); ++i)
  {
    GetTriggerRule(i, true);
  }

  initHardware();

  LOG4CPLUS_INFO(logger_, "Initialised TTCci hardware");

  LOG4CPLUS_INFO(logger_, "  firmware version: " << firmwareVersionString());
  LOG4CPLUS_INFO(logger_, "  Board-ID " << GetBoardID());

  if ((GetFirmwareVersion() > 0) && (GetFirmwareVersion() < 2))
  {
    std::ostringstream msg;
    msg << "Potential incompatibility between TTCci software and firmware " << "(FW version "
        << firmwareVersionString() << ")";
    LOG4CPLUS_FATAL(logger_, msg.str());
    XCEPT_RAISE(xcept::Exception, msg.str());
  }
}


string ttc::TTCci::firmwareVersionString()
{
  stringstream s;
  s << "Version " << GetFirmwareVersion() << " (date: " << hex;
  uint32_t date = GetFirmwareDate();
  for (int i = 2; i >= 0; --i)
  {
    if (i == 2)
      s << "20";
    else
      s << "-";
    uint32_t dum = (date >> (i * 8)) & 0xff;
    if (dum < 0x10)
      s << "0";
    s << dum;
  }
  s << ")";

  return s.str();
}


uint32_t ttc::TTCci::getBTimeCorrection()
{
  return bTimeCorrection_;
}


uint32_t ttc::TTCci::getDelayT2Correction()
{
  return delayT2Correction_;
}


void ttc::TTCci::initHardware()
{
  // read board status register (just a check)
  BoardStatus();

  // Trigger rules.
  SetTriggerRule(1, 3);
  SetTriggerRule(2, 25);
  SetTriggerRule(3, 100);
  SetTriggerRule(4, 240);
  SetTriggerRule(5, 0);
  SetTriggerRule(6, 0);
  SetTriggerRule(7, 0);

  // Load the default BGO map.
  bgo_map->SetBGOMap(DFLTBGOMAP);

  // bgo_map changed, update BChannel names
  SetBChannelNames();
}


void ttc::TTCci::SetBChannelNames()
{
  std::vector<std::string> channel_names = bgo_map->getChannelNames();
  assert(channel_names.size() == NChannels());

  for (size_t i = 0; i < NChannels(); ++i)
  {
    bgo[i].SetName(channel_names[i]);
    bgo[i].AddAlternativeNames(bgo_map->getChannelNameAlternatives(i));
  }
}


uint32_t ttc::TTCci::GetFirmwareVersion() const
{
  return Read(TTCciAdd::FWARE_VER, "(GetFirmwareVersion())") & 0xff;
}


uint32_t ttc::TTCci::GetFirmwareDate() const
{
  return (Read(TTCciAdd::FWARE_VER, "(GetFirmwareVersion())") >> 8) & 0xffffff;
}


uint32_t ttc::TTCci::GetBoardID() const
{
  return Read(TTCciAdd::BRD_ID, "(GetBoardID())");
}


void ttc::TTCci::SendInputSelection(uint32_t inselect_new)
{
  SetBit(inselect_new, STARTCYCLATSTART_BIT, _ResumeCylic);
  Write(TTCciAdd::INSELECT, inselect_new, "(INSELECT)");
}


void ttc::TTCci::ResumeCylicGenAtNextStart(bool resume)
{
  _ResumeCylic = resume;
  uint32_t inselect = ReadInputSelection();
  SetBit(inselect, STARTCYCLATSTART_BIT, _ResumeCylic);
  Write(TTCciAdd::INSELECT, inselect, "(Resume Cycl)");
}


uint32_t ttc::TTCci::ReadInputSelection() const
{
  return (Read(TTCciAdd::INSELECT) & 0xfffff);
}


void ttc::TTCci::SelectClock(const ExternalInterface itf)
{
  std::vector<ExternalInterface> vec;
  vec.push_back(itf);
  SelectSource("Clock", vec);
}


bool ttc::TTCci::ConfiguringResetsQPLL() const
{
  return configuringResetsQPLL_;
}


void ttc::TTCci::ConfiguringResetsQPLL(bool value)
{
  configuringResetsQPLL_ = value;
}


uint32_t ttc::TTCci::ReadEventCounter() const
{
  return Read(TTCciAdd::EVNCNT);
}


uint32_t ttc::TTCci::ReadOrbitCounter() const
{
  return Read(TTCciAdd::ORBCNT);
}


uint32_t ttc::TTCci::ReadStrobeCounter() const
{
  return Read(TTCciAdd::STRCNT);
}


void ttc::TTCci::ResetCounters()
{
  Write(TTCciAdd::CNTRST, 0xfffffeff, "(Counter reset)");
}


void ttc::TTCci::ClearLatchedStatus()
{
  boardStatusSaved_ = BoardStatus();
  // NOTE: The moment one writes anything into any of these sixteen
  // bits, the whole register clears.
  Write(TTCciAdd::STATUS, 0x0, "(Latched board status reset)");
}


void ttc::TTCci::MainReset()
{
  DisableL1A();
  SwitchOffBGOs();
  ram_triggers->ClearTriggerDelays();
  ram_triggers->ClearDPRAM();
  ResetAllBGODataOnTTCci();
  ResetCounters();
}


void ttc::TTCci::SendShortBGODataFromVME(uint32_t d)
{
  Write(TTCciAdd::VMEDATS, (d & 0xff));
}


void ttc::TTCci::SendLongBGODataFromVME(uint32_t d)
{
  Write(TTCciAdd::VMEDATL, (d));
}


ttc::BChannel*
ttc::TTCci::GetBChannel(const size_t channel)
{
  if (channel >= NChannels())
  {
    stringstream my;
    my << "TTCci::GetBChannel(channel='" << channel << "'): Invalid argument!";
    throw std::invalid_argument(my.str());
  }
  return &(bgo[channel]);
}


void ttc::TTCci::SelectOrbitSource(const ExternalInterface itf)
{
  std::vector<ExternalInterface> vec;
  vec.push_back(itf);
  SelectSource("Orbit", vec);
}


ttc::ExternalInterface
ttc::TTCci::getSelectedOrbitSource() const
{
  // Get all possible sources for the given signal.
  const vector<ttc::ExternalInterface> interfaces = GetSourceList("Orbit");

  if (interfaces.size() == 0)
  {
    // Unknown signal type.
    LOG4CPLUS_ERROR(
        logger_,
        "ttc::TTCci::CheckOrbitSource(): "
        "List of accepted sources for 'Orbit' is empty!");

    return UNDEFINED;
  }

  vector<ttc::ExternalInterface> sources;

  // read INSELECT register
  uint32_t inselect = ReadInputSelection();

  // the start of the four orbit selection bits in the INSELECT register
  uint32_t orbitBitsStart = ORBIT_BIT(INTERNAL);

  // the three most significant orbit selection bits
  uint32_t orbitBits3of4 = MaskOut(inselect, 3, orbitBitsStart+1);

  for (vector<ttc::ExternalInterface>::const_iterator
          it = interfaces.begin();
          it != interfaces.end(); ++it)
  {
    if (*it == INTERNAL)
    {
      // internal orbit source is selected iff three most significant orbit selection bits are zero;
      // i.e. both 0b0000 and 0b0001 are valid
      if (orbitBits3of4 == 0)
      {
        sources.push_back(*it);
      }
    }
    else
    {
      // other orbit source is selected iff the respective orbit selection bit is set
      if ((inselect >> ORBIT_BIT(*it)) & 0x1)
      {
        sources.push_back(*it);
      }
    }
  }

  // error if no orbit source is selected
  if (sources.size() == 0)
  {
    LOG4CPLUS_ERROR(
        logger_,
        "ttc::TTCci::CheckOrbitSource(): No orbit source selected?"
        "INSELECT = 0x" << hex << inselect  << dec);
  }

  // error if more than one orbit source is selected
  if (sources.size() > 1)
  {
    std::stringstream sourcesStr;

    for (size_t j = 0; j < sources.size(); ++j)
      sourcesStr << " " << INAME(sources[j]);

    LOG4CPLUS_ERROR(
        logger_,
        "ttc::TTCci::CheckOrbitSource: "
        << "Only one source allowed, but" << sourcesStr.str() << " are selected!");
  }

  return (sources.size() > 0 ? sources[0] : UNDEFINED);
}


void ttc::TTCci::SelectSource(const std::string& Name, const std::vector<ttc::ExternalInterface>& itf)
{
  // NOTE: This does not actually check for invalid interface names!

  const vector<ttc::ExternalInterface> interfaces = GetSourceList(Name);
  uint32_t old_inselect = ReadInputSelection();
  if (interfaces.size() == 0)
  {
    stringstream my;
    my << "TTCci::SelectSource(Name='" << Name << "', ...): List of accepted sources for '" << Name << "' is empty!";
    LOG4CPLUS_ERROR(logger_, my.str());
    throw std::invalid_argument(my.str());
  }

  vector<ttc::ExternalInterface>::const_iterator it;
  bool valid_interface = false;
  uint32_t inselect = ReadInputSelection();
  if ((Name == string("Trigger")) || (Name == string("trigger")) || (Name == string("TRIGGER")))
  {
    for (it = interfaces.begin(); it != interfaces.end(); ++it)
    {
      SetBit_off(inselect, TRIGGER_BIT((*it)));
      for (size_t j = 0; j < itf.size(); ++j)
      {
        if (itf[j] == (*it))
        {
          valid_interface = true;
          if (L1Aenabled_)
          {
            SetBit_on(inselect, TRIGGER_BIT(itf[j]));
          }
          triggersource_ = itf;
        }
      }
    }
  }
  else if ((Name == string("Clock")) || (Name == string("clock")) || (Name == string("CLOCK")))
  {
    for (it = interfaces.begin(); it != interfaces.end(); ++it)
    {
      SetBit_off(inselect, CLOCK_BIT((*it)));
      for (size_t j = 0; j < itf.size(); ++j)
      {
        if (itf[j] == (*it))
        {
          valid_interface = true;
          if (itf[j] == INTERNAL)
          {
            // Do something to the QPLL settings here.
            DisableQPLL();
          }
          else
          {
            SetBit_on(inselect, CLOCK_BIT(itf[j]));
            EnableQPLL();
          }
        }
      }
    }
  }
  else if ((Name == string("BGO")) || (Name == string("bgo")) || (Name == string("Bgo")) || (Name == string("BGo")))
  {
    for (it = interfaces.begin(); it != interfaces.end(); ++it)
    {
      SetBit_off(inselect, BGO_BIT((*it)));
      for (size_t j = 0; j < itf.size(); ++j)
      {
        if (itf[j] == (*it))
        {
          valid_interface = true;
          SetBit_on(inselect, BGO_BIT(itf[j]));
        }
      }
    }
  }
  else if ((Name == string("Orbit")) || (Name == string("orbit")) || (Name == string("ORBIT")))
  {
    for (it = interfaces.begin(); it != interfaces.end(); ++it)
    {
      SetBit_off(inselect, ORBIT_BIT((*it)));
      for (size_t j = 0; j < itf.size(); ++j)
      {
        if (itf[j] == (*it))
        {
          valid_interface = true;
          SetBit_on(inselect, ORBIT_BIT(itf[j]));
        }
      }
    }
  }
  else
  {
    stringstream my;
    my << "TTCci::SelectSource(Name='" << Name << "', ...): Unkown argument Name='" << Name << "'!";
    LOG4CPLUS_ERROR(logger_, my.str());
    throw std::invalid_argument(my.str());
  }

  if (!valid_interface)
  {
    stringstream my;
    my << "TTCci::SelectSource(Name='" << Name << "', ...): No valid interfaces specified";
    LOG4CPLUS_ERROR(logger_, my.str());
    throw std::invalid_argument(my.str());
  }

  if (old_inselect != inselect)
  {
    SendInputSelection(inselect);
  }
}


void ttc::TTCci::SwitchOffBGOs()
{
  uint32_t old_inselect = ReadInputSelection();
  uint32_t inselect = old_inselect;

  const vector<ttc::ExternalInterface> interfaces = GetSourceList("BGO");

  for (vector<ttc::ExternalInterface>::const_iterator
      it = interfaces.begin();
      it != interfaces.end(); ++it)
  {
    SetBit_off(inselect, BGO_BIT(*it));
  }

  if (old_inselect != inselect)
    SendInputSelection(inselect);
}


void ttc::TTCci::SelectTrigger(const std::vector<ExternalInterface>& itf)
{
  SelectSource("Trigger", itf);
}


std::vector<ttc::ExternalInterface>
ttc::TTCci::getSelectedTriggerSources() const
{
  // Get all possible sources
  const vector<ttc::ExternalInterface> interfaces = GetSourceList("Trigger");

  if (interfaces.size() == 0)
  {
    // Unknown signal type.
    LOG4CPLUS_ERROR(
        logger_,
        "ttc::TTCci::CheckTrigger(): "
        "List of accepted sources for 'Trigger' is empty!");

    return interfaces;
  }

  vector<ttc::ExternalInterface> sources;

  uint32_t inselect = ReadInputSelection();

  bool nomore = false;

  if (L1Aenabled_)
  {
    for (vector<ttc::ExternalInterface>::const_iterator
        it = interfaces.begin();
        it != interfaces.end(); ++it)
    {
      if ((inselect >> TRIGGER_BIT((*it))) & 0x1)
      {
        if (!nomore)
        {
          sources.push_back(*it);
        }
        else
        {
          LOG4CPLUS_WARN(
              logger_,
              "ttc::TTCci::CheckTrigger(): "
              "Inconsistency: " << INAME(sources[0]) << " is set, so ignoring " << INAME((*it)));
        }

        if ((*it == CTC) || (*it == LTCIN))
        {
          nomore = true;
        }
      }
    } // Loop over all possible sources.

    // Check whether triggersource_ & sources have same content!
    bool same = true;

    for (size_t i = 0; i < triggersource_.size(); ++i)
    {
      if (find(sources.begin(), sources.end(), triggersource_[i]) == sources.end())
      {
        same = false;
        break;
      }
    }

    if (triggersource_.size() != sources.size())
    {
      same = false;
    }

    if (!same)
    {
      stringstream sourcesExpStr;
      stringstream sourcesStr;

      for (size_t j = 0; j < triggersource_.size(); ++j)
      {
        sourcesExpStr << " " << INAME(triggersource_[j]);
      }

      for (size_t j = 0; j < sources.size(); ++j)
      {
        sourcesStr << " " << INAME(sources[j]);
      }

      LOG4CPLUS_ERROR(
          logger_,
          "ttc::TTCci::CheckTrigger(): Trigger Source inconsistency: "
          "expecting << " << sourcesExpStr.str()
          << " but << " << sourcesStr.str() << " are selected.");
    }
  }
  else
  {
    sources = triggersource_;
  }

  return sources;
}


void ttc::TTCci::SelectBGOSource(const std::vector<ExternalInterface>& itf)
{
  SelectSource("Bgo", itf);
}


std::vector<ttc::ExternalInterface>
ttc::TTCci::CheckBGOSource() const
{
  // Get all possible sources.
  const vector<ttc::ExternalInterface> interfaces = GetSourceList("Bgo");

  if (interfaces.size() == 0)
  {
    // Unknown signal type.
    LOG4CPLUS_ERROR(
        logger_,
        "ttc::TTCci::CheckBGOSource(): List of accepted sources for 'Bgo' is empty!");

    return interfaces;
  }

  vector<ttc::ExternalInterface> sources;

  uint32_t inselect = ReadInputSelection();

  bool nomore = false;
  for (vector<ttc::ExternalInterface>::const_iterator
      it = interfaces.begin();
      it != interfaces.end(); ++it)
  {
    if ((inselect >> BGO_BIT(*it)) & 0x1)
    {
      if (!nomore)
      {
        sources.push_back(*it);
      }
      else
      {
        LOG4CPLUS_ERROR(
            logger_,
            "ttc::TTCci::CheckBGOSource(): "
            "Inconsistency: " << INAME(sources[0]) << " is set, so " << "ignoring " << INAME((*it)));
      }

      if ((*it == CTC) || (*it == LTCIN))
      {
        nomore = true;
      }
    }
  }

  return sources;
}


ttc::ExternalInterface
ttc::TTCci::getSelectedClockSource() const
{
  // Get all possible sources for the given signal.
  const vector<ttc::ExternalInterface> interfaces = GetSourceList("Clock");

  if (interfaces.size() == 0)
  {
    // Unknown signal type.
    LOG4CPLUS_ERROR(
        logger_,
        "TTCci::CheckClock(): "
        "List of accepted sources for 'Clock' is empty!");

    return UNDEFINED;
  }

  vector<ttc::ExternalInterface> sources;

  // read INSELECT register
  uint32_t inselect = ReadInputSelection();

  // the start of the four clock selection bits in the INSELECT register
  uint32_t clockBitsStart = CLOCK_BIT(INTERNAL);

  // the four clock selection bits
  uint32_t clockBits = MaskOut(inselect, 4, clockBitsStart);

  for (vector<ttc::ExternalInterface>::const_iterator
      it = interfaces.begin();
      it != interfaces.end(); ++it)
  {
    if (*it == INTERNAL)
    {
      // internal clock source is selected iff all four clock selection bits are zero
      if (clockBits == 0)
      {
        sources.push_back(*it);
      }
    }
    else
    {
      // other clock source is selected iff the respective clock selection bit is set
      if ((inselect >> CLOCK_BIT(*it)) & 0x1)
      {
        sources.push_back(*it);
      }
    }
  }

  // error if no clock source is selected
  if (sources.size() == 0)
  {
    LOG4CPLUS_ERROR(
        logger_,
        "TTCci::CheckClock(): No clock source selected? "
        "INSELECT = 0x" << hex << inselect  << dec);
  }

  // error if more than one clock source is selected
  if (sources.size() > 1)
  {
    std::stringstream sourcesStr;

    for (size_t j = 0; j < sources.size(); ++j)
      sourcesStr << " " << INAME(sources[j]);

    LOG4CPLUS_ERROR(
        logger_,
        "TTCci::CheckClock(): "
        << "Only one source allowed, but" << sourcesStr.str() << " are selected!");
  }

  return (sources.size() > 0 ? sources[0] : UNDEFINED);
}


bool ttc::TTCci::isBGOSourceSelected(const ttc::ExternalInterface& source) const
{
  vector<ttc::ExternalInterface> bgoSources = CheckBGOSource();

  return find(bgoSources.begin(), bgoSources.end(), source) != bgoSources.end();
}


void ttc::TTCci::EnableL1A()
{
  L1Aenabled_ = true;
  uint32_t inselect = ReadInputSelection();
  for (size_t i = 0; i < triggersource_.size(); ++i)
  {
    SetBit_on(inselect, TRIGGER_BIT(triggersource_[i]));
  }
  // Send this to the TTCci.
  SendInputSelection(inselect);

  // Start the RAM trigger, if trigger source is INTERNAL.
  if (find(triggersource_.begin(), triggersource_.end(), INTERNAL) != triggersource_.end())
  {
    Write(TTCciAdd::CNTRST, 0x100, "(Start int.trigs)");
  }
}


void ttc::TTCci::DisableL1A()
{
  L1Aenabled_ = false;
  uint32_t inselect = ReadInputSelection();
  for (size_t i = 0; i < triggersource_.size(); ++i)
    SetBit_off(inselect, TRIGGER_BIT(triggersource_[i]));
  // Send this to the TTCci.
  SendInputSelection(inselect);

  // Stop the RAM trigger, if trigger source is INTERNAL.
  if (find(triggersource_.begin(), triggersource_.end(), INTERNAL) != triggersource_.end())
  {
    Write(TTCciAdd::CNTRST, 0x1, "(Stop int.trigs)");
  }

  {
    // Check thoroughly!
    uint32_t inselect = ReadInputSelection();
    const vector<ttc::ExternalInterface> interfaces = GetSourceList("Trigger");
    for (vector<ttc::ExternalInterface>::const_iterator
        it = interfaces.begin();
        it != interfaces.end(); ++it)
    {
      if (((inselect >> TRIGGER_BIT(*it)) & 0x1) == 1)
      {
        stringstream my;
        my << "TTCci::DisableL1A(): " << "interface '" << INAME(*it) << "' is selected";
        LOG4CPLUS_ERROR(logger_, my.str());
        SetBit_off(inselect, TRIGGER_BIT(*it));
        SendInputSelection(inselect);
      }
    }
  }
}


bool ttc::TTCci::IsL1AEnabled() const
{
  return L1Aenabled_;
}


void ttc::TTCci::SelectInputs(
    const ExternalInterface clockSource,
    const ExternalInterface orbitSource,
    const std::vector<ttc::ExternalInterface>& triggerSource,
    const std::vector<ttc::ExternalInterface>& bgoSource)
{
  SelectClock(clockSource);
  SelectSource("Trigger", triggerSource);
  SelectSource("Bgo", bgoSource);
  SelectOrbitSource(orbitSource);
}


void ttc::TTCci::SelectInputs(
    const std::string clockSource,
    const std::string orbitSource,
    const std::vector<std::string>& triggerSource,
    const std::vector<std::string>& bgoSource)
{
  std::vector<ttc::ExternalInterface> trig, bgo;
  for (size_t i = 0; i < triggerSource.size(); ++i)
  {
    trig.push_back(INTERFACE(triggerSource[i]));
  }
  for (size_t i = 0; i < bgoSource.size(); ++i)
  {
    bgo.push_back(INTERFACE(bgoSource[i]));
  }
  SelectClock(INTERFACE(clockSource));
  SelectSource("Trigger", trig);
  SelectSource("Bgo", bgo);
  SelectOrbitSource(INTERFACE(orbitSource));
}


std::vector<ttc::ExternalInterface> ttc::TTCci::GetSourceList(const std::string& Name) const
{
  static std::vector<ttc::ExternalInterface> triggerSources;
  static std::vector<ttc::ExternalInterface> orbitSources;
  static std::vector<ttc::ExternalInterface> bgoSources;
  static std::vector<ttc::ExternalInterface> clockSources;
  if ((Name == string("Trigger")) || (Name == string("trigger")))
  {
    std::vector<ttc::ExternalInterface>* myvec = &triggerSources;
    if (myvec->size() == 0)
    {
      myvec->push_back(CTC);
      myvec->push_back(LTCIN);
      myvec->push_back(SCOM);
      myvec->push_back(BCHANNEL);
      myvec->push_back(CYCLIC);
      myvec->push_back(FRONTP0);
      myvec->push_back(FRONTP1);
      myvec->push_back(INTERNAL);
    }
    return (*myvec);
  }

  //----------------------------------------

  else if ((Name == string("Clock")) || (Name == string("clock")) || (Name == string("CLOCK")))
  {
    std::vector<ttc::ExternalInterface>* myvec = &clockSources;
    if (myvec->size() == 0)
    {
      myvec->push_back(CTC);
      myvec->push_back(LTCIN);
      myvec->push_back(TTC);
      myvec->push_back(INTERNAL);
    }
    return (*myvec);
  }
  else if ((Name == string("BGO")) || (Name == string("bgo")) || (Name == string("BGo")) || (Name == string("Bgo")))
  {
    std::vector<ttc::ExternalInterface>* myvec = &bgoSources;
    if (myvec->size() == 0)
    {
      myvec->push_back(CTC);
      myvec->push_back(LTCIN);
      myvec->push_back(VME);
      myvec->push_back(CYCLIC);
    }
    return (*myvec);
  }
  else if ((Name == string("Orbit")) || (Name == string("orbit")) || (Name == string("ORBIT")))
  {
    std::vector<ttc::ExternalInterface>* myvec = &orbitSources;
    if (myvec->size() == 0)
    {
      myvec->push_back(CTC);
      myvec->push_back(LTCIN);
      myvec->push_back(TTC);
      myvec->push_back(INTERNAL);
    }
    return (*myvec);
  }
  else
  {
    LOG4CPLUS_ERROR(logger_, "TTCci::GetSourceList(Name='" << Name << "'): "
    "Invalid name");
    std::vector<ttc::ExternalInterface> dummy;
    return dummy;
  }
}


std::vector<string> ttc::TTCci::GetSourceListNames(const std::string& Name) const
{
  std::vector<ttc::ExternalInterface> interfaces = GetSourceList(Name);
  vector<string> myvec;
  for (size_t i = 0; i < interfaces.size(); ++i)
  {
    myvec.push_back(INAME(interfaces[i]));
  }
  return myvec;
}


uint32_t ttc::TTCci::BoardStatus(bool getPrevious) const
{
  uint32_t boardStatus = getPrevious ? boardStatusSaved_ : Read(TTCciAdd::STATUS);
  return boardStatus;
}


void ttc::TTCci::PrintBoardStatus() const
{
  std::stringstream msg;
  uint32_t boardStatus = BoardStatus();
  msg << "Status: 0x" << hex << boardStatus << dec << " i.e.:" << endl << "Laser: \t\t\t\t"
      << (IsLaserOn() ? "ON" : "OFF") << endl << "Cancelled B-Data? \t\t" << (IsBDataCancelled() ? "YES" : "NONE")
      << endl << "Cancelled B-Data? \t\t" << (IsBDataCancelled_Latched() ? "YES" : "NONE") << "\t(Latched)" << endl
      << "Clock status? \t\t\t" << (ClockInverted() ? "INVERTED" : "NOT INVERTED") << endl << "Clock: \t\t\t\t"
      << (ClockLocked() ? "LOCKED" : "FREE (UNLOCKED)") << endl << "Dat/Clk sync error? \t\t"
      << (IsDataClkError() ? "YES" : "NONE") << endl << "Dat/Clk sync error? \t\t"
      << (IsDataClkError_Latched() ? "YES" : "NONE") << "\t(Latched)" << endl << "Clock single-evt. upset? \t"
      << (IsClkSingleEvtUpset() ? "YES" : "NONE") << endl << "Clock single-evt. upset? \t"
      << (IsClkSingleEvtUpset_Latched() ? "YES" : "NONE") << "\t(Latched)" << endl << "L1A: \t\t\t\t"
      << (MissedL1A() ? "MISSED by 40MHz Clock" : "O.K.") << endl << "L1A: \t\t\t\t"
      << (MissedL1A_Latched() ? "MISSED by 40MHz Clock" : "O.K.") << "\t(Latched)" << endl << "Double-L1A @ 40MHz? \t\t"
      << (DoubleL1Aat40MHz() ? "YES (ERROR)" : "NONE (OK)") << endl << "Double-L1A @ 40MHz? \t\t"
      << (DoubleL1Aat40MHz_Latched() ? "YES (ERROR)" : "NONE (OK)") << "\t(Latched)" << endl << "OSYNC? \t\t\t\t"
      << (OrbitSyncError() ? "YES" : "NONE") << endl << "OSYNC? \t\t\t\t" << (OrbitSyncError_Latched() ? "YES" : "NONE")
      << "\t(Latched)" << endl;
  LOG4CPLUS_INFO(logger_, msg.str());
}


bool ttc::TTCci::IsLaserOn() const
{
  return (((BoardStatus() >> 0) & 1) ? false : true);
}


bool ttc::TTCci::ClockInverted() const
{
  return (((BoardStatus() >> 2) & 1) ? true : false);
}


bool ttc::TTCci::ClockLocked() const
{
  return (((BoardStatus() >> 5) & 1) ? false : true);
}


bool ttc::TTCci::ClockLocked_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 21) & 1) ? false : true);
}


bool ttc::TTCci::IsBDataCancelled() const
{
  return (((BoardStatus() >> 1) & 1) ? true : false);
}


bool ttc::TTCci::IsBDataCancelled_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 17) & 1) ? true : false);
}


bool ttc::TTCci::IsDataClkError() const
{
  return (((BoardStatus() >> 3) & 1) ? true : false);
}


bool ttc::TTCci::IsDataClkError_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 19) & 1) ? true : false);
}


bool ttc::TTCci::IsClkSingleEvtUpset() const
{
  return (((BoardStatus() >> 4) & 1) ? true : false);
}


bool ttc::TTCci::IsClkSingleEvtUpset_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 20) & 1) ? true : false);
}


bool ttc::TTCci::MissedL1A() const
{
  return (((BoardStatus() >> 6) & 1) ? true : false);
}


bool ttc::TTCci::MissedL1A_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 22) & 1) ? true : false);
}


bool ttc::TTCci::DoubleL1Aat40MHz() const
{
  return (((BoardStatus() >> 7) & 1) ? true : false);
}


bool ttc::TTCci::DoubleL1Aat40MHz_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 23) & 1) ? true : false);
}


bool ttc::TTCci::OrbitSyncError() const
{
  return (((BoardStatus() >> 8) & 1) ? true : false);
}


bool ttc::TTCci::OrbitSyncError_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 24) & 1) ? true : false);
}


bool ttc::TTCci::TriggerSuppressed() const
{
  return (((BoardStatus() >> 9) & 1) ? true : false);
}


bool ttc::TTCci::TriggerSuppressed_Latched(bool getPrevious) const
{
  return (((BoardStatus(getPrevious) >> 25) & 1) ? true : false);
}


void ttc::TTCci::EnableQPLL(bool enable)
{
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  SetBit(qpll_register, QPLLENABLE_BIT, !enable);
  Write(TTCciAdd::QPLLCTRL, qpll_register, "(from EnableQPLL())");
}


void ttc::TTCci::DisableQPLL()
{
  EnableQPLL(false);
}


void ttc::TTCci::ResetQPLL(bool doReset)
{
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  SetBit(qpll_register, QPLLRESET_BIT, !doReset);
  Write(TTCciAdd::QPLLCTRL, qpll_register, "(from ResetQPLL())");
}


void ttc::TTCci::AutoRestartQPLL(bool enable)
{
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  SetBit(qpll_register, QPLLAUTORESTART_BIT, enable);
  Write(TTCciAdd::QPLLCTRL, qpll_register, "(from AutoRestartQPLL() disable)");
}


void ttc::TTCci::SetQPLLFrequencyBits(uint32_t freq, bool only4LSBs)
{
  // NOTE: For the internal clock all six bits are used as QPLL
  // frequency bits. For the external clock only the four LSB.
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  for (size_t i = 0; i < (only4LSBs ? 4 : 6); ++i)
  {
    SetBit_off(qpll_register, i);
  }

  qpll_register |= ((freq) & (only4LSBs ? 0xf : 0x3f));
  Write(TTCciAdd::QPLLCTRL, qpll_register, "(from SetQPLLFrequencyBits())");
}


uint32_t ttc::TTCci::GetQPLLFrequencyBits(bool only4LSBs) const
{
  // NOTE: For the internal clock all six bits are used as QPLL
  // frequency bits. For the external clock only the four LSB.
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  return (qpll_register & (only4LSBs ? 0xf : 0x3f));
}


void ttc::TTCci::SetExtTrigInputDelay(unsigned input_num, uint32_t delay)
{
  if (delay > 255)
  {
    throw std::invalid_argument("TTCci::SetExtTrig1InputDelay(): "
        "delay must be in [0, 255]");
  }

  unsigned mask_width, mask_offset;
  GetExtTrigInputDelayMask(input_num, mask_width, mask_offset);

  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  qpll_register = MaskIn(qpll_register, delay & 0xFF, mask_width, mask_offset);

  Write(TTCciAdd::QPLLCTRL, qpll_register, "(from SetExtTrigInputDelay())");
}


uint32_t ttc::TTCci::GetExtTrigInputDelay(unsigned input_num) const
{
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);

  unsigned mask_width, mask_offset;

  try {
    GetExtTrigInputDelayMask(input_num, mask_width, mask_offset);
  }
  catch(std::invalid_argument& e)
  {
    // this firmware version can't delay , i.e. delay = 0;
    return 0;
  }

  return MaskOut(qpll_register, mask_width, mask_offset);
}


void ttc::TTCci::ResetAllBGODataOnTTCci()
{
  // Loop over all BGO Channels.
  for (size_t i = 0; i < bgo.size(); ++i)
  {
    bgo[i].Reset();
    Address myaddD, myaddP;
    GetBGODataRegisterAddresses(i, myaddD, myaddP);

    Write(TTCciAdd::CHIHB, i, bgo[i].InhibitDelayWord_corr(), "(inhibit/delay)");
    Write(TTCciAdd::CHPRESC, i, bgo[i].PrescaleWord(), "(prescale)");
    Write(TTCciAdd::CHPOSTSC, i, bgo[i].PostscaleWord(), "(postscale)");

    for (size_t n = 0; n < BGOChannel::maxcommands; ++n)
    {
      // NOTE: This explicitly writes zeroes into the DPRAM! This is
      // not the same as setting all 'last-word' and
      // 'do-no-transmit' bits!
      Write(myaddD, n, 0, "(D Data)");
      Write(myaddP, n, 0, "(P data)");
    }
  }
}


void ttc::TTCci::WriteBGODataToTTCci(const size_t channel)
{
  Address myaddD, myaddP;
  const size_t i = channel;

  GetBGODataRegisterAddresses(i, myaddD, myaddP);

  for (size_t n = 0; n < bgo[i].NWords(); ++n)
  {
    uint32_t dummy;
    Write(myaddD, n, bgo[i].DataWord_D(n), "(WriteBGOData-D)");

    // Read back and verify that the value is the same.
    dummy = Read(myaddD, n);
    if (dummy != bgo[i].DataWord_D(n))
    {
      LOG4CPLUS_ERROR(
          logger_,
          "TTCci::WriteBGODataToTTCci(): Reading 0x" << hex << dummy << " while expecting 0x" << bgo[i].DataWord_D(n) << dec << " on ch# " << i << " word " << n << " (Data_D)");
    }

    Write(myaddP, n, bgo[i].DataWord_P(n), "(WriteBGOData-P)");

    // Read back and verify that the value is the same.
    dummy = Read(myaddP, n);
    if (dummy != bgo[i].DataWord_P(n))
    {
      LOG4CPLUS_ERROR(
          logger_,
          "TTCci::WriteBGODataToTTCci(): Reading 0x" << hex << dummy << " while expecting 0x" << bgo[i].DataWord_P(n) << dec << " on ch# " << i << " word " << n << " (Data_P)");
    }
  }

  Write(TTCciAdd::CHIHB, i, bgo[i].InhibitDelayWord_corr(), "(inhib/del word)");
  Write(TTCciAdd::CHPRESC, i, bgo[i].PrescaleWord(), "(prescale)");
  Write(TTCciAdd::CHPOSTSC, i, bgo[i].PostscaleWord(), "(postscale)");
}


void ttc::TTCci::WriteBGODataToTTCci()
{
  // Loop over all BGO channels.
  for (size_t i = 0; i < bgo.size(); ++i)
  {
    Address myaddD, myaddP;
    GetBGODataRegisterAddresses(i, myaddD, myaddP);

    for (size_t n = 0; n < bgo[i].NWords(); ++n)
    {
      uint32_t dummy;
      Write(myaddD, n, bgo[i].DataWord_D(n), "(WriteBGOData-D)");
      dummy = Read(myaddD, n, "(ReadBGOData-D)");
      if (dummy != bgo[i].DataWord_D(n))
      {
        LOG4CPLUS_ERROR(
            logger_,
            "TTCci::WriteBGODataToTTCci(): Reading 0x" << hex << dummy << " while expecting 0x" << bgo[i].DataWord_D(n) << dec << " on ch# " << i << " word " << n << " (Data_D)");
        ;
      }

      Write(myaddP, n, bgo[i].DataWord_P(n), "(WriteBGOData-P)");
      dummy = Read(myaddP, n, "(ReadBGOData-P)");
      if (dummy != bgo[i].DataWord_P(n))
      {
        LOG4CPLUS_ERROR(
            logger_,
            "TTCci::WriteBGODataToTTCci(): Reading 0x" << hex << dummy << " while expecting 0x" << bgo[i].DataWord_P(n) << dec << " on ch# " << i << " word " << n << " (Data_P)");
      }
    } // loop over all words (B frames) of this BGO command

    Write(TTCciAdd::CHIHB, i, bgo[i].InhibitDelayWord_corr(), "(inhib/del word)");
    Write(TTCciAdd::CHPRESC, i, bgo[i].PrescaleWord(), "(prescale)");
    Write(TTCciAdd::CHPOSTSC, i, bgo[i].PostscaleWord(), "(postscale)");
  }
}


void ttc::TTCci::ReadBGODataFromTTCci(const size_t nwords)
{
  // Loop over all 16 BGO channels.
  for (size_t i = 0; i < bgo.size(); ++i)
  {
    Address myaddD, myaddP;
    GetBGODataRegisterAddresses(i, myaddD, myaddP);

    bgo[i].SetInhibitDelayWord(Read(TTCciAdd::CHIHB, i));
    bgo[i].ClearWords();

    for (size_t n = 0; n < nwords; ++n)
    {
      const uint32_t d = Read(myaddD, n);
      const uint32_t p = Read(myaddP, n);
      bgo[i].PushBackDataWord_D(d);
      bgo[i].PushBackDataWord_P(p);
      if (bgo[i].IsLastWord(bgo[i].NWords() - 1) || (d == 0 && p == 0))
      {
        break;
      }
    }

    if (!bgo[i].IsOK())
    {
      stringstream my;
      my << "TTCci::ReadBGODataFromTTCci(): " << "Inconsistency in bgo[ichannel=" << dec << i << "]!";
      LOG4CPLUS_ERROR(logger_, my.str());
      throw std::invalid_argument(my.str());
    }
  }

  for (unsigned int i = 0; i < NCyclicTrigger(); ++i)
  {
    TTCci::ReadCyclicGeneratorFromTTCci(true, i);
  }
  for (unsigned int i = 0; i < NCyclicBGO(); ++i)
  {
    TTCci::ReadCyclicGeneratorFromTTCci(false, i);
  }
}


void ttc::TTCci::GetBGODataRegisterAddresses(const size_t channel, ttc::Address& addressD, ttc::Address& addressP)
{
  switch (channel)
  {
    case 0:
      addressD = TTCciAdd::CH00D;
      addressP = TTCciAdd::CH00P;
      break;
    case 1:
      addressD = TTCciAdd::CH01D;
      addressP = TTCciAdd::CH01P;
      break;
    case 2:
      addressD = TTCciAdd::CH02D;
      addressP = TTCciAdd::CH02P;
      break;
    case 3:
      addressD = TTCciAdd::CH03D;
      addressP = TTCciAdd::CH03P;
      break;
    case 4:
      addressD = TTCciAdd::CH04D;
      addressP = TTCciAdd::CH04P;
      break;
    case 5:
      addressD = TTCciAdd::CH05D;
      addressP = TTCciAdd::CH05P;
      break;
    case 6:
      addressD = TTCciAdd::CH06D;
      addressP = TTCciAdd::CH06P;
      break;
    case 7:
      addressD = TTCciAdd::CH07D;
      addressP = TTCciAdd::CH07P;
      break;
    case 8:
      addressD = TTCciAdd::CH08D;
      addressP = TTCciAdd::CH08P;
      break;
    case 9:
      addressD = TTCciAdd::CH09D;
      addressP = TTCciAdd::CH09P;
      break;
    case 10:
      addressD = TTCciAdd::CH10D;
      addressP = TTCciAdd::CH10P;
      break;
    case 11:
      addressD = TTCciAdd::CH11D;
      addressP = TTCciAdd::CH11P;
      break;
    case 12:
      addressD = TTCciAdd::CH12D;
      addressP = TTCciAdd::CH12P;
      break;
    case 13:
      addressD = TTCciAdd::CH13D;
      addressP = TTCciAdd::CH13P;
      break;
    case 14:
      addressD = TTCciAdd::CH14D;
      addressP = TTCciAdd::CH14P;
      break;
    case 15:
      addressD = TTCciAdd::CH15D;
      addressP = TTCciAdd::CH15P;
      break;
    default:
      stringstream my;
      my << "TTCci::GetBGODataRegisterAddresses(): " << "Invalid B channel " << channel << "!";
      throw std::invalid_argument(my.str());
  }
}


void ttc::TTCci::ReadChannelStatus(
    const size_t channel,
    uint32_t& signalcounter,
    bool& anycancelled,
    uint32_t& cancelcounter,
    bool& ramempty) const
{
  if (channel >= NChannels())
  {
    stringstream my;
    my << "TTCci::ReadChannelStatus(channel=" << dec << channel << "): Invalid Channel (should be 0...15)!";
    throw std::invalid_argument(my.str());
  }
  uint32_t stat = Read(TTCciAdd::CHRMC, channel, "(CHRMC)");
  signalcounter = BGORequestCounter(stat);
  cancelcounter = BGOCancelledCounter(stat);
  anycancelled = HasCancelledBData(stat);
  ramempty = IsRamBufferAtEnd(stat);
}


uint32_t ttc::TTCci::ReadChannelSignalCounter(size_t channel) const
{
  uint32_t signalcounter, cancelcounter;
  bool anycancelled, ramempty;

  ReadChannelStatus(channel, signalcounter, anycancelled, cancelcounter, ramempty);
  return signalcounter;
}


bool ttc::TTCci::CheckAllChannelStatus(
    std::vector<uint32_t>& Ncancelled,
    std::vector<uint32_t>& Nsignal,
    std::vector<bool>& RAMIsEmpty) const
{
  Ncancelled.clear();
  Nsignal.clear();
  RAMIsEmpty.clear();
  bool allok = true;
  for (size_t ich = 0; ich < NChannels(); ++ich)
  {
    uint32_t nsignal, ncancel;
    bool cancelled, ramempty;
    ReadChannelStatus(ich, nsignal, cancelled, ncancel, ramempty);
    if ((cancelled && ncancel == 0) || (!cancelled && ncancel > 0))
    {
      stringstream my;
      my << "TTCci::CheckAllChannelStatus(channel=" << dec << ich << "): Inconsistency: N(cancelled)=" << dec << ncancel
          << " although cancel flag=" << (cancelled ? 1 : 0);
      LOG4CPLUS_ERROR(logger_, my.str());
      throw std::invalid_argument(my.str());
    }
    Ncancelled.push_back(ncancel);
    Nsignal.push_back(nsignal);
    RAMIsEmpty.push_back(ramempty);

    std::stringstream msg;
    msg << "Status ch# " << ich << ":\t";
    msg << " N(signal) = " << dec << nsignal << "(0x" << hex << nsignal << dec << ")\t";
    msg << ncancel << " cancelled (" << cancelled << ")";
    if (cancelled)
    {
      msg << "!!!";
    }
    msg << "\tRAM is " << (ramempty ? "EMPTY" : "NOT empty");
    LOG4CPLUS_INFO(logger_, msg.str());
  }
  return allok;
}


bool ttc::TTCci::IsQPLLEnabled() const
{
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  return (((qpll_register >> QPLLENABLE_BIT) & 1) == 0);
}


bool ttc::TTCci::Is_ResetQPLL() const
{
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  return (((qpll_register >> QPLLRESET_BIT) & 1) == 0);
}


bool ttc::TTCci::Is_AutoRestartQPLL() const
{
  uint32_t qpll_register = Read(TTCciAdd::QPLLCTRL);
  return (((qpll_register >> QPLLAUTORESTART_BIT) & 1) == 1);
}


void ttc::TTCci::ExecuteVMEBGO(const unsigned int ibgo, const bool EnableVMEFirst)
{
  // Complain if the BGO command number is too high.
  if (ibgo > 0xf)
  {
    std::stringstream msg;
    msg << "TTCci::ExecuteVMEBGO(): " << "argument ibgo=" << ibgo << " out of range! (range is [0, 15])";
    LOG4CPLUS_ERROR(logger_, msg.str());
    std::out_of_range(msg.str());
  }

  vector<ttc::ExternalInterface> currentsource = CheckBGOSource();
  if (currentsource.size() == 0)
  {
    LOG4CPLUS_ERROR(logger_, "TTCci::ExecuteVMEBGO(): "
    "CheckBGOSource() returned vector of size=0");
    currentsource.push_back(VME);
  }
  bool switchback = false;
  bool vmeenabled = (find(currentsource.begin(), currentsource.end(), VME) != currentsource.end());
  // Switch temprarily to VME as BGO Source.
  if (EnableVMEFirst)
  {
    if (!vmeenabled)
    {
      if ((currentsource[0] == CTC) || (currentsource[0] == LTCIN))
      {
        switchback = true;
        vector<ttc::ExternalInterface> temp;
        temp.push_back(VME);
        SelectBGOSource(temp);
      }
      else
      {
        currentsource.push_back(VME);
        SelectBGOSource(currentsource);
      }
    }
  }
  else if (!vmeenabled)
  {
    std::stringstream msg;
    msg << "TTCci::ExecuteVMEBGO(): "
        "Unable to send VME BGO, since << ";
    for (size_t k = 0; k < currentsource.size(); ++k)
      msg << " " << INAME(currentsource[k]);
    msg << " >> are selected as BGO source and EnableVMEFirst = " << (EnableVMEFirst ? "true" : "false") << endl;
    LOG4CPLUS_WARN(logger_, msg.str());
  }

  // Now send the actual BGO.
  Write(TTCciAdd::VMEBGO, (ibgo & 0xf), "(VME-BGO)");

  if (EnableVMEFirst && switchback)
  {
    SelectBGOSource(currentsource);
  }
}


void ttc::TTCci::ConfigureBData(const bool Enable, const bool shortword, const uint32_t data)
{
  uint32_t bdatacfg = 0;
  if (Enable)
    bdatacfg |= ((1) << 1);
  if (!shortword)
    bdatacfg |= ((1) << 0);
  bdatacfg |= ((data & 0xffffff) << 8);
  Write(TTCciAdd::BDAT_CFG, bdatacfg, "(BData config)");
}


void ttc::TTCci::ReadBDataConfiguration(bool& IsEnabled, bool& IsShortWord, uint32_t& data) const
{
  uint32_t bdatacfg = Read(TTCciAdd::BDAT_CFG);
  IsEnabled = ((bdatacfg >> 1) & 0x1 ? true : false);
  IsShortWord = ((bdatacfg >> 0) & 0x1 ? false : true);
  data = ((bdatacfg >> 8) & 0xffffff);
}


void ttc::TTCci::Configure(std::istream& in)
{
  TTCciConfiguration config(*this);

  try {
    config.Configure(in);
  }
  catch (xcept::Exception& e)
  {
    XCEPT_RETHROW(
        xcept::Exception,
        "TTCciConfiguration::Configure failed", e);
  }
}


void ttc::TTCci::WriteConfiguration(std::ostream& out, const std::string& comment)
{
  out << "###########################################################" << endl;
  out << "#" << endl;
  out << "# TTCci configuration." << endl;
  out << "#" << endl;
  if (comment.size() > 0)
  {
    out << "# " << comment << endl;
    out << "#" << endl;
  }
  out << "#" << endl;
  out << "# Automatically created with TTCci::WriteConfiguration() " << endl;
  out << "# from current configuration on " << CurrentTime() << endl;
  out << "#" << endl;
  out << "# (Comments (will be ignored) are indicated by \"#\". Lines" << endl
      << "#  can be split using the '\\' character at end of 1st line.)" << endl;
  out << "#" << endl;
  out << "###########################################################" << endl;
  out << endl;
  out << "# THE SOURCES (What should the TTCci be listening to?):" << endl;
  out << "# options: " << "TTC, LTC, CTC, VME, INTERNAL, FRONTPANEL0|1, CYCLIC, or BCHANNEL" << endl;
  string dum;
  vector<string> nm;

  nm = GetSourceListNames("Trigger");
  std::vector<ttc::ExternalInterface> itf = getSelectedTriggerSources();
  out << WORD_TRIGGERSOURCE;
  for (size_t j = 0; j < itf.size(); ++j)
    out << " " << INAME(itf[j]);
  out << " #";
  for (size_t k = 0; k < nm.size(); ++k)
  {
    if (k > 0)
      out << ",";
    out << " " << nm[k];
  }
  out << endl;

  nm = GetSourceListNames("BGO");
  itf = CheckBGOSource();
  out << WORD_BGOSOURCE;
  for (size_t j = 0; j < itf.size(); ++j)
    out << " " << INAME(itf[j]);
  out << " #";
  for (size_t k = 0; k < nm.size(); ++k)
  {
    if (k > 0)
      out << ",";
    out << " " << nm[k];
  }
  out << endl;

  nm = GetSourceListNames("Orbit");
  out << WORD_ORBITSOURCE << "   " << (dum = INAME(getSelectedOrbitSource()));
  out << string(13 - dum.size(), ' ') << "#";
  for (size_t k = 0; k < nm.size(); ++k)
  {
    if (k > 0)
      out << " |";
    out << " " << nm[k];
  }
  out << endl << endl;

  nm = GetSourceListNames("Clock");
  out << "# CLOCK: possible interfaces: ";
  for (size_t k = 0; k < nm.size(); ++k)
  {
    if (k > 0)
      out << " |";
    out << " " << nm[k];
  }
  out << endl;
  out << "# parameters: " << WORD_QPLLRESET << "YES|NO       (ignored if not " << INAME(INTERNAL) << ")" << endl;
  out << "#             " << WORD_QPLLAUTORESTART << "YES|NO (ignored if not " << INAME(INTERNAL) << ")" << endl;
  out << "#             " << WORD_QPLLFREQBITS << "=0x??" << endl;
  ExternalInterface clockif = getSelectedClockSource();
  dum = INAME(clockif);
  if (clockif != INTERNAL)
  {
    out << "# " << WORD_CLOCKSOURCE << " " << INAME(INTERNAL) << " " << WORD_QPLLFREQBITS << "0x" << hex
        << GetQPLLFrequencyBits(false) << dec << "# ";
    for (size_t k = 0; k < nm.size(); ++k)
    {
      if (k > 0)
        out << " |";
      out << " " << nm[k];
    }
    out << endl;
    out << WORD_CLOCKSOURCE << " " << dum << " " << WORD_QPLLRESET << (configuringResetsQPLL_ ? "YES" : "NO") << " "
        << WORD_QPLLAUTORESTART << (Is_AutoRestartQPLL() ? "YES" : "NO") << " " << WORD_QPLLFREQBITS << "0x" << hex
        << GetQPLLFrequencyBits(true) << dec << endl;
  }
  else
  {
    out << "# " << WORD_CLOCKSOURCE << " " << INAME(CTC) << " " << WORD_QPLLRESET << "NO" << " " << WORD_QPLLAUTORESTART
        << (Is_AutoRestartQPLL() ? "YES" : "NO") << " " << WORD_QPLLFREQBITS << "0x" << hex
        << GetQPLLFrequencyBits(true) << dec << endl;
    out << WORD_CLOCKSOURCE << " " << dum << " " << WORD_QPLLFREQBITS << "0x" << hex << GetQPLLFrequencyBits(false)
        << dec << " # ";
    for (size_t k = 0; k < nm.size(); ++k)
    {
      if (k > 0)
        out << " |";
      out << " " << nm[k];
    }
    out << endl;
  }
  out << "" << endl;

  // Trigger sequences.
  out << "# TRIGGER SECTION (ignored if " << WORD_TRIGGERSOURCE << " is not set to " << INAME(INTERNAL)<< ")" << endl;
  out << "# Set (last) trigger to 1 to stop trigger sequence." << endl;
  out << "# (last) trigger = 0 ==> repetitive sequence (default)" << endl;
  out << "# e.g. with N>0 lines like: '" << WORD_TRIGGER_INTERVAL << " 0x20'" << endl;
  out << "# or with '" << WORD_TRIGGER_FREQUENCY << " 1.5 " << WORD_TRIGGER_FREQUENCY_MODE << "EQUI'" << endl;
  out << "# where 'MODE' can be 'EQUI' (equidistant) or 'RANDOM'" << endl;

  if (ram_triggers->triggerFrequency < 0.0)
  {
    out << "# " << WORD_TRIGGER_FREQUENCY << " 10000" << " " << WORD_TRIGGER_FREQUENCY_MODE << "EQUI" << "   # in Hz, "
        << WORD_TRIGGER_FREQUENCY_MODE << "EQUI or " << WORD_TRIGGER_FREQUENCY_MODE << "RANDOM" << endl;
    for (size_t i = 0; i < ram_triggers->triggerdelay.size(); ++i)
    {
      out << WORD_TRIGGER_INTERVAL << "  0x" << hex << ram_triggers->triggerdelay[i] << dec << endl;
    }
  }
  else
  {
    out << "# " << WORD_TRIGGER_INTERVAL << " 0x5784" << endl;
    out << WORD_TRIGGER_FREQUENCY << " " << ram_triggers->triggerFrequency << " " << WORD_TRIGGER_FREQUENCY_MODE
        << (ram_triggers->randomTrigger ? "RANDOM" : "EQUI") << "   # in Hz, " << WORD_TRIGGER_FREQUENCY_MODE
        << "EQUI or " << WORD_TRIGGER_FREQUENCY_MODE << "RANDOM" << endl;
  }
  out << "" << endl;
  out << "# Set The BGO Channels, e.g. for channel 9 (=Start) with : " << endl;
  out << "#   " << WORD_BGOCHANNEL_SETUP << " 9 " << WORD_BGOCHANNEL_SETUP_L << "SINGLE|DOUBLE|BLOCK "
      << WORD_BGOCHANNEL_SETUP_MODE << "YES|NO \\" << endl;
  out << "#     "/*<< WORD_BGOCHANNEL_SETUP_INVOKE<< "STROBE|ORBIT "*/
  << WORD_BGOCHANNEL_SETUP_DELAY1 << "255 " << WORD_BGOCHANNEL_SETUP_DELAY2 << "3 " << WORD_BGOCHANNEL_SETUP_PRESCALE
      << "10 " << WORD_BGOCHANNEL_SETUP_POSTSCALE << "20" << endl;
  out << "#   " << WORD_BGOCHANNEL_DATA << " 9 " << WORD_BGOCHANNEL_DATA_DATA << "0x1234 " << WORD_BGOCHANNEL_DATA_MODE
      << "S|L|A  # S=SHORT, L=LONG, A=A-Command" << endl;
  out << "# The first integer is the bgo channel number (0..15). " << endl
      << "# Alternatively, one can use the channel name instead of the " << endl
      << "# number. The channels 1 to 10 are reserved for BC0, TestEnable, " << endl
      << "# PrivateGap, PrivateOrbit, Resynch, HardReset, EC0, OC0, Start," << endl << "# and Stop, respectively."
      << endl << "#" << endl << "# In order to use per word (individual) postscale values, add a parameter" << endl
      << "# " << WORD_BGOCHANNEL_DATA_POSTSCALE << "=xy to each " << WORD_BGOCHANNEL_DATA << " command. Note that"
      << endl << "# you'll need a firmware version that supports it. The TTCci XDAQ application" << endl
      << "# will show you the corresponding fields in the 'BGO Configuration' page if" << endl << "# it does." << endl
      << endl;

  for (size_t i = 0; i < bgo.size(); ++i)
  {
    if (bgo[i].NWords() > 0 && !bgo[i].IsLastWord(0))
    {
      out << endl;
      string chan;
      {
        chan = bgo[i].GetName();
        if (chan[0] == 'C' && chan[1] == 'h' && chan[2] == 'a')
        {
          stringstream s;
          s << i;
          chan = s.str();
        }
      }
      out << "# Channel # " << dec << i << " (" << chan << "):" << endl;

      out << WORD_BGOCHANNEL_SETUP << " ";
      out << dec << chan;
      out << " " << WORD_BGOCHANNEL_SETUP_L
          << (bgo[i].IsSingleCommand() ? "SINGLE" : (bgo[i].IsDoubleCommand() ? "DOUBLE" : "BLOCK")) << " "
          << WORD_BGOCHANNEL_SETUP_MODE << (bgo[i].IsRepetitive() ? "YES" : "NO") << " "
          << WORD_BGOCHANNEL_SETUP_INITPRESCALE << bgo[i].GetInitialPrescale();

      if (bgo[i].GetDelayTime1() > 0)
        out << " " << WORD_BGOCHANNEL_SETUP_DELAY1 << bgo[i].GetDelayTime1();

      if (bgo[i].GetDelayTime2() > 0)
        out << " " << WORD_BGOCHANNEL_SETUP_DELAY2 << bgo[i].GetDelayTime2();

      if (bgo[i].GetPrescale() > 0)
        out << " " << WORD_BGOCHANNEL_SETUP_PRESCALE << bgo[i].GetPrescale();

      if (bgo[i].GetPostscale() > 0)
        out << " " << WORD_BGOCHANNEL_SETUP_POSTSCALE << bgo[i].GetPostscale();
      out << endl;

      for (size_t n = 0; n < bgo[i].NWords(); ++n)
      {
        if (bgo[i].IsLastWord(n))
          break;

        out << WORD_BGOCHANNEL_DATA << " " << chan << " " << WORD_BGOCHANNEL_DATA_DATA << "0x" << hex
            << bgo[i].DataWord_D(n) << dec << " " << WORD_BGOCHANNEL_DATA_MODE
            << (bgo[i].IsACommand(n) ? "A" : (bgo[i].IsShortWord(n) ? "S" : "L"));

        // we do not check here whether the firmware supports
        // individual postscale values or not. We just
        // write them to the configuration file if they
        // were configured. If necessary, they are ignored
        // at configuration loading time.
        unsigned post_scale_value = bgo[i].GetIndividualPostScaleValue(n);
        if (post_scale_value > 0)
          out << " " << WORD_BGOCHANNEL_DATA_POSTSCALE << post_scale_value;

        out << endl;

      } // loop over all words of this BGO channel
    } // if this BGO channel is not empty
  } // loop over all BGO channels
  out << endl;

  { // Sequences
    out << "################################################################" << endl;
    out << "# Configuration of sequences. Individual sequences can be " << "defined for " << endl
        << "# 'coldReset', 'configure', 'enable', 'stop', 'suspend', " << "'periodic' " << endl
        << "# and 'user' (predefined)" << endl;
    out << "# or for any user-defined sequence (use \"" << WORD_SEQUENCE_ADDNEW << " ChooseName\" first)" << endl;
    out << "# Example: " << endl << "#   " << WORD_SEQUENCE_BEGIN << " enable \t# or configure, suspend, ..." << endl
        << "#   ResetCounters \t\t# resets evt+orb cntrs. (on TTCci only!)" << endl
        << "#   Sleep 5 \t\t\t# sleep for 5 sec" << endl << "#   mSleep 10 \t\t\t# sleep for 10 ms" << endl
        << "#   uSleep 100\t\t\t# sleep for 10 us" << endl
        << "#   EnableL1A \t\t\t# or DisableL1A e.g. for 'Suspend' Sequence" << endl
        << "#   BGO 5 \t\t\t# activates BGO-Channel 5 through VME\t" << endl
        << "#   BGO Start \t\t\t# activates BGO-Channel 9 through VME\t" << endl
        << "#   Periodic On \t\t# (On|Off) Use Periodic seq. with care!!!" << endl
        << "#   Periodic 60  \t\t# periodicity of Periodic sequence: 60 sec" << endl
        << "#   SendBST \t\t\t# Take network time and send it as BST (to emulate BST)" << endl
        << "#   ResumeCyclicAtStart yes|no\t# Do|don't resume from where you left of" << endl
        << "#   SendShortBDATA 0x5 \t\t# Send short BDATA" << endl << "#   SendLongBDATA 0x555 \t# Send long BDATA"
        << endl << "#   BGOSource CTC|LTC|VME|CYCLIC# To change the BGO source (VME ~= none)" << endl
        << "#   Write [i] ADDRESS 0xfff \t# Write 0xffff to reg. ADDRESS (offset i)" << endl
        << "#   Read [i] ADDRESS \t\t# Read from reg. ADDRESS (offset i)" << endl << "#   " << WORD_SEQUENCE_END
        << "\t\t\t# closes this sequence" << endl;

    const vector<string> seqs = GetSequenceNames();

    for (size_t k = 0; k < seqs.size(); ++k)
    {
      string sname = seqs[k];

      Sequence* seq = 0;
      try {
        seq = GetSequence(sname);
      }
      catch(ttc::exception::UndeclaredSequence& e)
      {
        continue;
      }

      out << endl;

      if (!seq->IsPermanent())
        out << WORD_SEQUENCE_ADDNEW << " " << seq->GetName() << endl;

      out << WORD_SEQUENCE_BEGIN << " " << sname << endl;
      for (size_t j = 0; j < seq->N(); ++j)
        out << "  " << seq->Get(j) << endl;
      out << WORD_SEQUENCE_END << endl;
    }

    out << endl;
  }

  {
    out << "################################################################" << endl;
    out << "# Cyclic Trigger and BGO Generators" << endl;
    out << "# Usage: " << WORD_CYCLICGEN_TRIGGER << " id [arguments]" << endl;
    out << "#    or: " << WORD_CYCLICGEN_BGO << " id [arguments]" << endl;
    out << "# where id denotes the generator, i.e. 0..2 for TRIGGER and " << endl << "# 0..4 for BGO generators."
        << endl;
    out << "# The following arguments can be appended to these commands:";
    out << endl << "#   " << WORD_CYCLICGEN_STARTBX << "i \t\t# i=Offset in BX" << endl << "#   "
        << WORD_CYCLICGEN_PRESCALE << "i \t\t# i=prescale" << endl << "#   " << WORD_CYCLICGEN_POSTSCALE
        << "i \t\t# i=postscale (# of times)" << endl << "#   " << WORD_CYCLICGEN_INITPRECALE
        << "i \t\t# i=initial orbits to wait" << endl << "#   " << WORD_CYCLICGEN_REPETITIVE
        << "y|n \t# repeat sequence" << endl << "#   " << WORD_CYCLICGEN_PAUSE << "i \t\t# pause i orbits (for "
        << WORD_CYCLICGEN_REPETITIVE << "y)" << endl << "#   " << WORD_CYCLICGEN_PERMANENT
        << "y|n \t# don't listen to BGO Start/Stop" << endl << "#   " << WORD_CYCLICGEN_CHANNEL
        << "i|Name \t\t# BGO channel or name. For BGO only!" << endl
        << "# The argument CH denotes the BGO channel to be requested." << endl
        << "# Its value can either be a number (0..15) or the channel " << endl
        << "# name, e.g. Resynch, HardReset, EC0, OC0, Start, Stop, TestEnable, " << endl
        << "# PrivateGap, or PrivateOrbit" << endl << "#" << endl;
    for (size_t i = 0; i < 2; ++i)
    {
      const bool trigger = (i == 0);
      for (size_t j = 0; j < (trigger ? NCyclicTrigger() : NCyclicBGO()); ++j)
      {
        CyclicTriggerOrBGO* cycl = GetCyclic(trigger, j);
        if (!cycl->Changed())
          continue;
        out << (trigger ? WORD_CYCLICGEN_TRIGGER : WORD_CYCLICGEN_BGO) << " " << dec << j;
        if (!trigger)
        {
          const size_t ch = cycl->GetBChannel();
          string name = bgo[ch].GetName();
          if (name[0] == 'C' && name[1] == 'h' && name[2] == 'a')
          {
            stringstream s;
            s << ch;
            name = s.str();
          }
          out << " " << WORD_CYCLICGEN_CHANNEL << name << dec;
        }
        if (cycl->GetStartBX() != 0)
          out << " " << WORD_CYCLICGEN_STARTBX << dec << cycl->GetStartBX();
        if (cycl->GetPrescale() != 0)
          out << " " << WORD_CYCLICGEN_PRESCALE << cycl->GetPrescale();
        if (cycl->GetPostscale() != 0)
          out << " " << WORD_CYCLICGEN_POSTSCALE << cycl->GetPostscale();
        if (cycl->GetInitialPrescale() != 0)
          out << " " << WORD_CYCLICGEN_INITPRECALE << cycl->GetInitialPrescale();
        if (cycl->GetPause() != 0)
          out << " " << WORD_CYCLICGEN_PAUSE << cycl->GetPause();
        //if (!cycl->IsRepetitive())
        out << " " << WORD_CYCLICGEN_REPETITIVE << (cycl->IsRepetitive() ? "y" : "n");
        if (1 || !cycl->IsPermanent())
          out << " " << WORD_CYCLICGEN_PERMANENT << (cycl->IsPermanent() ? "y" : "n");
        if (!cycl->IsEnabled())
          out << " DISABLE ";
        out << endl;
      }
    }

    out << endl;
  }

  {
    // Trigger rules:
    out
        << endl
        << "# TRIGGER RULE SETTINGS ###################################" << endl
        << "# There are " << trigrules.size() - FirstTriggerRule() << " that can be changed using e.g.:" << endl
        << "# " << WORD_TRIGRULE << " i N_BX   # i = " << FirstTriggerRule() << "..." << trigrules.size() - 1 << ", "
        << "N_BX = Min no. of clocks for i triggers" << endl
        << endl;

    for (size_t i = FirstTriggerRule(); i < TriggerRuleSize(); ++i)
    {
      out
          << WORD_TRIGRULE << " " << i << " " << GetTriggerRule(i)
          << "  \t# " << "No more than i trigs in j BX"
          << endl;
    }
  }
  out << endl;

  // External trigger1 delay.

  vector<unsigned> delayable_ext_trig_inputs = GetDelayableExternalTrigInputs();

  out << "# EXTERNAL TRIGGER INPUT(S) DELAY(S) ##########################" << endl << "#" << endl
      << "# This value is used to set the external trigger 1 and or 0 input" << endl
      << "# delay in units of bunch crossings." << endl << "# valid values are from 0 to 255" << endl;

  for (unsigned j = 0; j < delayable_ext_trig_inputs.size(); ++j)
  {
    unsigned trig_num = delayable_ext_trig_inputs[j];
    out << TTCciConfiguration::GetExtTrigDelayKeyWord(trig_num) << " " << GetExtTrigInputDelay(trig_num) << endl;
  }

  out << "########### CONFIGURATION END #############################" << endl;
}


void ttc::TTCci::ExecuteSequenceLine(const std::string& line)
{
  string string_val = "";
  uint32_t ulong_val = 0;
  double double_val = 0;
  string command, parname;

  if (FindString(line, (command = "Periodic"), string_val))
  {
    if (GetDouble(line, command, double_val))
    {
      PeriodicSeqParameters.newSetup = true;
      PeriodicSeqParameters.waitNsec = double_val;
    }
    else
    {
      if ((string_val == "ON") || (string_val == "On") || (string_val == "on"))
      {
        PeriodicSeqParameters.newSetup = true;
        PeriodicSeqParameters.enabled = true;
      }
      else if ((string_val == "OFF") || (string_val == "Off") || (string_val == "off"))
      {
        PeriodicSeqParameters.newSetup = true;
        PeriodicSeqParameters.enabled = false;
      }
      else
      {
        failParseSequenceLineInvalidParam(line, command, string_val);
      }
    }
  }

  else if (FindString(line, (command = "SendLongBDATA"), string_val))
  {
    if (GetUnsignedLong(line, command, ulong_val))
    {
      Write(TTCciAdd::VMEDATL, ulong_val, "(SendLongBDATA)");
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(line, (command = "SendShortBDATA"), string_val))
  {
    if (GetUnsignedLong(line, command, ulong_val))
    {
      Write(TTCciAdd::VMEDATS, ulong_val, "(SendShortBDATA)");
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(line, (command = "BGOSource"), string_val))
  {
    const vector<ttc::ExternalInterface> interfaces = GetSourceList("BGO");
    bool valid = false;
    for (vector<ttc::ExternalInterface>::const_iterator it = interfaces.begin(); it != interfaces.end(); ++it)
    {
      if (string_val == INAME((*it)))
      {
        valid = true;
        std::vector<ttc::ExternalInterface> vec;
        vec.push_back((*it));
        SelectBGOSource(vec);
        break;
      }
    }
    if (valid)
    {
      LOG4CPLUS_INFO(logger_, "TTCci: Setting BGO source to '" << string_val << "'");
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(line, (command = "uSleep"), string_val))
  {
    if (GetUnsignedLong(line, command, ulong_val))
    {
      usleep(ulong_val);
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(line, (command = "mSleep"), string_val))
  {
    if (GetUnsignedLong(line, command, ulong_val))
    {
      usleep(ulong_val * 1000);
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(line, (command = "Sleep"), string_val))
  {
    if (GetUnsignedLong(line, command, ulong_val))
    {
      sleep(ulong_val);
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }
  }

  else if (FindString(line, (command = "EnableL1A"), string_val))
  {
    EnableL1A();
  }

  else if (FindString(line, (command = "DisableL1A"), string_val))
  {
    DisableL1A();
  }

  else if (FindString(line, (command = "ResumeCyclicAtStart"), string_val))
  {
    if (string_val == "Yes" || string_val == "YES" || string_val == "yes")
    {
      ResumeCylicGenAtNextStart();
    }
    else if (string_val == "No" || string_val == "NO" || string_val == "no")
    {
      ResumeCylicGenAtNextStart(false);
    }
    else
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }

    LOG4CPLUS_INFO(
        logger_,
        "TTCci: Cyclic generators will " << (_ResumeCylic?"RESUME":"START FROM BEGINNING") << " after next 'Start' BGO.");
  }

  else if (FindString(line, (command = "BGO"), string_val))
  {
    bool foundch = false;
    if (GetUnsignedLong(line, command, ulong_val))
    {
      foundch = true;
    }
    else if (FindString(line, command, string_val))
    {
      for (size_t ib = 0; ib < NChannels(); ++ib)
      {
        if (bgo[ib].MatchesNameOrAlternative(string_val, false))
        {
          foundch = true;
          ulong_val = ib;
          break;
        }
      }
    }

    if (!foundch)
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }

    if (ulong_val >= 16)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "BGO Channel number must be in range [0, 15] in line '" + line + "'");
    }

    ExecuteVMEBGO(ulong_val, true);
  }

  else if (FindString(line, (command = "ResetCounters"), string_val))
  {
    if (L1Aenabled_)
    {
      XCEPT_RAISE(
          xcept::Exception,
          "Invalid request to reset counters while L1A is enabled, in sequence line '" + line + "'");
    }
    ResetCounters();
  }

  else if (FindString(line, (command = "ResetStatus"), string_val))
  {
    LOG4CPLUS_INFO(
        logger_,
        "TTCci::ExecuteSequenceLine(): Resetting TTCci board status");

    ClearLatchedStatus();
  }

  else if (FindString(line, (command = "Write"), string_val))
  {
    bool addrarray = false;
    uint32_t addroffset = 0;

    if (GetUnsignedLong(line, command, ulong_val))
    {
      addrarray = true;
      addroffset = ulong_val;
      string_val = GetNthWord(2, line);
    }
    else
    {
      string_val = GetNthWord(1, line);
    }

    if (!GetUnsignedLong(line, string_val, ulong_val))
    {
      failParseSequenceLineInvalidParam(line, command, string_val);
    }

    if (!addrarray)
    {
      Write(string_val, ulong_val, "(Exec.Seq.)");
    }
    else
    {
      Write(string_val, addroffset, ulong_val, "(Exec.Seq.)");
    }
  }

  else if (FindString(line, (command = "Read"), string_val))
  {
    bool addrarray = false;
    uint32_t addroffset = 0;
    if (GetUnsignedLong(line, command, ulong_val))
    {
      addrarray = true;
      addroffset = ulong_val;
      string_val = GetNthWord(2, line);
    }
    else
    {
      string_val = GetNthWord(1, line);
    }
    if (!addrarray)
    {
      Read(string_val, "(Exec.Seq.)");
    }
    else
    {
      Read(string_val, addroffset, "(Exec.Seq.)");
    }
  }

  else if (FindString(line, (command = "SendBST"), string_val))
  {
    SendBST();
  }

  else
  {
    XCEPT_RAISE(
        xcept::Exception,
        "Unknown command in sequence line '" + line + "'");
  }
}


void ttc::TTCci::failParseSequenceLineInvalidParam(
    const string& line,
    const string& command,
    const string& param)
{
  XCEPT_RAISE(
      xcept::Exception,
      "Unknown value '" + param + "' "
      "for command '" + command + "' "
      "in sequence line '" + line + "'");
}


bool ttc::TTCci::PeriodicSequenceEnabled() const
{
  return PeriodicSeqParameters.enabled;
}


double ttc::TTCci::Periodicity() const
{
  return PeriodicSeqParameters.waitNsec;
}


size_t ttc::TTCci::NCyclicTrigger() const
{
  return _n_ctg;
}


size_t ttc::TTCci::NCyclicBGO() const
{
  return _n_cbg;
}


ttc::CyclicTriggerOrBGO*
ttc::TTCci::GetCyclic(const bool trigger, const size_t i)
{
  if (trigger)
  {
    if (i > NCyclicTrigger())
    {
      LOG4CPLUS_ERROR(
          logger_,
          "TTCci::GetCyclic(trigger=" << trigger << ", i=" << i << "): " << "i > NCyclicTrigger()=" << NCyclicTrigger());
      return (CyclicTriggerOrBGO*) 0;
    }
    return &(_ctg[i]);
  }
  else
  {
    if (i > NCyclicBGO())
    {
      LOG4CPLUS_ERROR(logger_,
          "TTCci::GetCyclic(trigger=" << trigger << ", i=" << i << "): " << "i > NCyclicBGO()=" << NCyclicBGO());
      return (CyclicTriggerOrBGO*) 0;
    }
    return &(_cbg[i]);
  }
}


const ttc::CyclicTriggerOrBGO*
ttc::TTCci::GetCyclic(const bool trigger, const size_t i) const
{
  if (trigger)
  {
    if (i > NCyclicTrigger())
    {
      LOG4CPLUS_ERROR(
          logger_,
          "TTCci::GetCyclic(trigger=" << trigger << ", i=" << i << "): " << "i > NCyclicTrigger()=" << NCyclicTrigger());
      return (const CyclicTriggerOrBGO*) 0;
    }
    return ((const CyclicTriggerOrBGO*) &(_ctg[i]));
  }
  else
  {
    if (i > NCyclicBGO())
    {
      LOG4CPLUS_ERROR(logger_,
          "TTCci::GetCyclic(trigger=" << trigger << ", i=" << i << "): " << "i > NCyclicBGO()=" << NCyclicBGO());
      return (const CyclicTriggerOrBGO*) 0;
    }
    return ((const CyclicTriggerOrBGO*) &(_cbg[i]));
  }
}


ttc::CyclicTriggerOrBGO*
ttc::TTCci::ReadCyclicGeneratorFromTTCci(const bool trigger, const size_t i)
{
  CyclicTriggerOrBGO* cycl = GetCyclic(trigger, i);
  if (!cycl)
  {
    LOG4CPLUS_ERROR(
        logger_,
        "TTCci::ReadCyclicGeneratorFromTTCci(trigger=" << trigger << ", i=" << i << "): " << "GetCyclic(trigger,i) returns 0");
    return cycl;
  }
  // Address offset.
  const size_t idx = i + (trigger ? 0 : _n_ctg);
  string comment = string("(Cyclic ") + (trigger ? "Trig" : "BGO") + ")";
  uint32_t ihb = Read(TTCciAdd::CTBG_INH, idx, comment);
  uint32_t ipres = Read(TTCciAdd::CTBG_IPRESC, idx, comment);
  uint32_t pres = Read(TTCciAdd::CTBG_PRESC, idx, comment);
  uint32_t post = Read(TTCciAdd::CTBG_POSTSC, idx, comment);
  uint32_t pause = Read(TTCciAdd::CTBG_PAUSE, idx, comment);
  uint32_t type = Read(TTCciAdd::CTBG_TYPE, idx, comment);
  if (trigger)
  {
    if (type != 0x1)
    {
      LOG4CPLUS_WARN(
          logger_,
          "TTCci::ReadCyclicGeneratorFromTTCci(trigger=" << trigger << ", i=" << i << "): " << "read type=0x" << hex << type << dec << "! --> setting type to 1" << endl << "(N.B.: type=0 is o.k. for DUMMY(64X) VME bus " << "adapter.)");
      type = 1;
    }
  }
  else
  {
    if ((type & 0x10) != 0x10)
    {
      LOG4CPLUS_WARN(
          logger_,
          "TTCci::ReadCyclicGeneratorFromTTCci(trigger=" << trigger << ", i=" << i << "): " << "read type=0x" << hex << type << dec << "! --> setting type to 0x10" << endl << "(N.B.: type=0 is o.k. for DUMMY(64X) VME bus " << "adapter.)");
      type = 0x10;
    }
  }
  cycl->SetPrescaleWd(pres);
  cycl->SetPostscaleWd(post);
  cycl->SetPauseWd(pause);
  cycl->SetTypeWd(type);
  cycl->SetInhibitWd(ihb);
  cycl->SetInitPrescaleWd(ipres);
  return cycl;
}


void ttc::TTCci::WriteCyclicGeneratorToTTCci(const bool trigger, const size_t i)
{
  const CyclicTriggerOrBGO* cycl = GetCyclic(trigger, i);
  if (!cycl)
  {
    LOG4CPLUS_ERROR(
        logger_,
        "TTCci::WriteCyclicGeneratorToTTCci(trigger=" << trigger << ", i=" << i << "): " << "GetCyclic(trigger,i) returns 0");
    return;
  }
  // Determine address offset.
  const size_t idx = i + (trigger ? 0 : _n_ctg);
  string comment = string("(Cyclic ") + (trigger ? "Trig" : "BGO") + ")";
  Write(TTCciAdd::CTBG_INH, idx, cycl->GetInhibitWd(), comment);
  Write(TTCciAdd::CTBG_IPRESC, idx, cycl->GetInitPrescaleWd(), comment);
  Write(TTCciAdd::CTBG_PRESC, idx, cycl->GetPrescaleWd(), comment);
  Write(TTCciAdd::CTBG_POSTSC, idx, cycl->GetPostscaleWd(), comment);
  Write(TTCciAdd::CTBG_PAUSE, idx, cycl->GetPauseWd(), comment);
  Write(TTCciAdd::CTBG_TYPE, idx, cycl->GetTypeWd(), comment);
}


void ttc::TTCci::SetTriggerRule(size_t ntrig, uint32_t minDeltaBX)
{
  if ((ntrig < FirstTriggerRule()) || (ntrig > trigrules.size()))
  {
    LOG4CPLUS_ERROR(
        logger_,
        "TTCci::SetTriggerRule(ntrig=" << ntrig << ", minDeltaBX=" << minDeltaBX << "): "
        "Invalid value for arg. ntrig! [" << FirstTriggerRule() << "," << trigrules.size() << "]");
    return;
  }

  uint32_t value = (minDeltaBX > 0 ? minDeltaBX - 1 : 0);
  if (trigrules[ntrig] != value)
  {
    trigrules[ntrig] = value;
    Write(TTCciAdd::TRR_nT, ntrig - FirstTriggerRule(), trigrules[ntrig], "(SetTriggerRule())");
  }

  uint32_t dum = Read(TTCciAdd::TRR_nT, ntrig - FirstTriggerRule(), "(SetTriggerRule(): check)");
  if (dum != trigrules[ntrig])
  {
    LOG4CPLUS_WARN(
        logger_,
        "TTCci::SetTriggerRule(): "
        "Setting trigger rule " << ntrig << " to " << trigrules[ntrig] << " failed! Reading back " << dum << "!" << " (o.k. if your are using a DUMMY(64X) bus adapter)");
  }
}


uint32_t ttc::TTCci::GetTriggerRule(size_t ntrig, bool readfromboard) const
{
  if ((ntrig < FirstTriggerRule()) || ntrig > trigrules.size())
  {
    LOG4CPLUS_ERROR(
        logger_,
        "TTCci::GetTriggerRule(ntrig=" << ntrig << "): " << "Invalid value for arg. ntrig! [" << FirstTriggerRule() << "," << trigrules.size() << "]");
    return 0;
  }

  if (readfromboard)
  {
    trigrules[ntrig] = Read(TTCciAdd::TRR_nT, ntrig - FirstTriggerRule(), "(GetTriggerRule())");
  }

  return (trigrules[ntrig] > 0 ? trigrules[ntrig] + 1 : 0);
}


size_t ttc::TTCci::FirstTriggerRule() const
{
  return TTCCI_NTRR_OFFSET;
}


size_t ttc::TTCci::TriggerRuleSize() const
{
  return trigrules.size();
}


void ttc::TTCci::ResetCyclicGenerators(const bool trigger, const bool bgo)
{
  if (trigger)
  {
    for (size_t i = 0; i < NCyclicTrigger(); ++i)
    {
      GetCyclic(true, i)->Reset();
      WriteCyclicGeneratorToTTCci(true, i);
    }
  }
  if (bgo)
  {
    for (size_t i = 0; i < NCyclicBGO(); ++i)
    {
      GetCyclic(false, i)->Reset();
      WriteCyclicGeneratorToTTCci(false, i);
    }
  }
}


void ttc::TTCci::SendBST(int address)
{
  address &= 0x3fff;
  address <<= 18;
  timeval tv;
  gettimeofday(&tv, 0);
  uint64_t t = tv.tv_sec * 1000000ULL + tv.tv_usec;
  for (int subaddr = 7; subaddr <= 14; ++subaddr)
  {
    SendLongBGODataFromVME(0x30000 | address | (subaddr << 8) | (t & 0xff));
    t >>= 8;
  }
}


bool ttc::TTCci::HasCancelledBData(uint32_t rmc_register_content) const
{
  if (GetFirmwareDate() < 0x070622)
    return ((rmc_register_content >> 22) & 1) != 0;
  else
    // This is actually an OR of 'bgo_when_busy' and
    // 'bgo_cancelled'. Not clear to me what the difference is. In
    // the worst case we could just check whether the number of
    // cancelled BGOs is nonzero...
    return ((rmc_register_content >> 27) & 1) != 0;
}


unsigned ttc::TTCci::BGORequestCounter(uint32_t rmc_register_content) const
{
  if (GetFirmwareDate() < 0x070622)
  {
    // 16 Bit in older firmware versions.
    return rmc_register_content & 0xffff;
  }
  else
  {
    // 24 Bit in newer firmware versions.
    return rmc_register_content & 0xffffff;
  }
}


unsigned ttc::TTCci::BGOCancelledCounter(uint32_t rmc_register_content) const
{
  if (GetFirmwareDate() < 0x070622)
  {
    // 6 Bit in older firmware versions.
    return (rmc_register_content >> 16) & 0x3f;
  }
  else
  {
    // 3 Bit in newer firmware versions.
    return (rmc_register_content >> 24) & 0x7;
  }
}


ttc::RAMTriggers*
ttc::TTCci::GetRAMTriggers()
{
  return ram_triggers;
}


ttc::BGOChannel*
ttc::TTCci::GetBGOChannel(const size_t channel)
{
  if (channel >= NChannels())
  {
    // Error!
    stringstream my;
    my << "TTCci::GetBGOChannel(channel='" << channel << "'): Invalid argument!";
    LOG4CPLUS_ERROR(logger_, my.str());
    throw std::invalid_argument(my.str());
  }
  return &(bgo[channel]);
}


ttc::TTCci::~TTCci()
{
  stop_thread_flag = true;
  void* dummy;
  pthread_join(thethread, &dummy);

  if (ram_triggers)
  {
    delete ram_triggers;
  }
  ram_triggers = 0;

  if (bgo_map)
  {
    delete bgo_map;
  }
  bgo_map = 0;
}


unsigned int ttc::TTCci::Get_Resynch_BGOCh() const
{
  assert(bgo_map);
  return bgo_map->Get_Resynch_BGOCh();
}


unsigned int ttc::TTCci::Get_HardReset_BGOCh() const
{
  assert(bgo_map);
  return bgo_map->Get_HardReset_BGOCh();
}


unsigned int ttc::TTCci::Get_ECntReset_BGOCh() const
{
  assert(bgo_map);
  return bgo_map->Get_ECntReset_BGOCh();
}


unsigned int ttc::TTCci::Get_OCntReset_BGOCh() const
{
  assert(bgo_map);
  return bgo_map->Get_OCntReset_BGOCh();
}


unsigned int ttc::TTCci::Get_Start_BGOCh() const
{
  assert(bgo_map);
  return bgo_map->Get_Start_BGOCh();
}


unsigned int ttc::TTCci::Get_Stop_BGOCh() const
{
  assert(bgo_map);
  return bgo_map->Get_Stop_BGOCh();
}


unsigned ttc::TTCci::GetNumBgosRequested(unsigned channel)
{
  const uint32_t rmc = Read(TTCciAdd::CHRMC, channel, "(GetNumBgosCancelled)");
  return BGORequestCounter(rmc);
}


unsigned ttc::TTCci::GetNumBgosCancelled(unsigned channel)
{
  const uint32_t rmc = Read(TTCciAdd::CHRMC, channel, "(GetNumBgosCancelled)");
  return BGOCancelledCounter(rmc);
}


bool ttc::TTCci::HasLongBGOCountersPerChannel() const
{
  return GetFirmwareDate() >= 0x070622;
}


bool ttc::TTCci::HasIndividualBPostScaleNumbers() const
{
  return GetFirmwareDate() >= 0x070622;
}


bool ttc::TTCci::CanDelayExtTrigger0Input() const
{
  return GetFirmwareDate() >= 0x071102;
}


std::vector<unsigned>
ttc::TTCci::GetDelayableExternalTrigInputs() const
{
  std::vector<unsigned> retval;

  if (CanDelayExtTrigger0Input())
    retval.push_back(0);

  retval.push_back(1);

  return retval;
}


void ttc::TTCci::GetExtTrigInputDelayMask(unsigned input_num, unsigned& mask_width, unsigned& mask_offset) const
{
  mask_width = 8;

  if (input_num == 1)
  {
    mask_offset = 16;
  }
  else if (input_num == 0)
  {
    // Cross check whether the firmware actually supports it.
    if (!CanDelayExtTrigger0Input())
    {
      throw std::invalid_argument("TTCci::GetExtTrigInputDelayMask(): "
          "This firmware version does not "
          "support delaying the external "
          "trigger input 0");
    }
    else
    {
      mask_offset = 8;
    }
  }
  else
  {
    throw std::invalid_argument(
        "TTCci::GetExtTrigInputDelayMask(): "
            "Invalid external trigger input number: " + boost::lexical_cast < string
            > (input_num) + ". Valid inputs are 0 and 1.");
  }
}


// non-member functions in namespace ttc

void*
ttc::Thread4PeriodicSequence(void* arg)
{
  log4cplus::Logger logger = log4cplus::Logger::getInstance("ttc-unknown-host.TTCci.Thread4PeriodicSequence");
  ttc::TTCciThreadPars* pars = (ttc::TTCciThreadPars*) arg;
  const string name = "periodic";
  ttc::TTCci* myCi = pars->myTTCci;

  if (!myCi)
    {
      LOG4CPLUS_ERROR(logger, "ttc::Thread4PeriodicSequence(): myTTCci=0 !!!");
    }
  else
    {
      double twait = pars->waitNsec;
      const double tstep = 1.0; // in sec
      time_t t0, t1;
      time(&t0);
      t1 = t0;
      do
        {
          usleep(int(tstep * 1000.0));
          time(&t1);
          if (pars->newSetup)
            {
              // new setup:
              pars->newSetup = false;
              twait = pars->waitNsec;
              time(&t0);
              t1 = t0;
              continue;
            }
          if ((twait > 0.0) && (difftime(t1, t0) > twait))
            {
              time(&t0);
              t1 = t0;
              if (!pars->enabled)
                {
                  continue;
                }

              {
                try
                  {
                    myCi->ExecuteSequence(name);
                  }
                catch (xcept::Exception& e)
                  {
                    LOG4CPLUS_ERROR(logger,
                                    "Exception caught while executing sequence: " << xcept::stdformat_exception_history(e));
                  }
                catch (std::exception& e)
                  {
                    LOG4CPLUS_ERROR(logger, "Exception caught while executing sequence: " << e.what());
                  }
                catch (...)
                  {
                    LOG4CPLUS_ERROR(logger, "Unknown exception caught while executing sequence");
                  }
              }

            }
        } while (!myCi->stop_thread_flag);
    }
  return ((void*) 0);
}
