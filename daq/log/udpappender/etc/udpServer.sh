#!/bin/sh

if test ${XDAQ_ROOT}o = "o"; then
	echo "Error: XDAQ_ROOT environment variable not set"
	exit 1
fi

export LD_LIBRARY_PATH
export LD_LIBRARY_PATH="$XDAQ_ROOT/daq/extern/log4cplus/linuxx86/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH="$XDAQ_ROOT/daq/extern/log4cplus/udpappender/lib/linux/x86:$LD_LIBRARY_PATH"

echo Run udpServer with options $@
${XDAQ_ROOT}/daq/extern/log4cplus/udpappender/test/linux/x86/udpServer.exe $@
