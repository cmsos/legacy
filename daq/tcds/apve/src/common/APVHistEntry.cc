#include "tcds/apve/APVHistEntry.h"

tcds::apve::APVHistEntry::APVHistEntry(uint32_t const dataIn)
{
  // Store the raw data word.
  rawData_ = dataIn;

  // Carve the pieces that we want from the raw data:
  // - bits [7:0] -> APV pipeline address,
  pipelineAddress_ = (rawData_ & 0x000000ff);
  // - bits [31:8] -> event number bits.
  eventNumber_ = (rawData_ & 0xffffffff00) >> 8;
}

uint32_t
tcds::apve::APVHistEntry::eventNumber() const
{
  return eventNumber_;
}

uint8_t
tcds::apve::APVHistEntry::pipelineAddress() const
{
  return pipelineAddress_;
}
