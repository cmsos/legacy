package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

public class TimeVal extends SimpleItem {

	private java.util.Date value;
	private static DateFormat FORMAT;
	
	static {
		FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'");
		FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	@Override
	public java.util.Date getValue() {
		return value;
	}

	@Override
	public void setValue(Object value) {
		this.value = (java.util.Date)value;
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		String.writeEXDRString(FORMAT.format(value), out);		
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		java.lang.String line = String.readEXDRString(in);
		
		try {
			value = FORMAT.parse(line);
		} catch (ParseException e) {
			throw new IOException("failed to parse the date. Parse exception:" + e.getMessage() + "\nData:" + line);
		} catch (NumberFormatException e) {
			throw new IOException("failed to parse the date. Number format exception:" + e.getMessage() + "\nData:" + line);
		}
		
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.TIME_VAL.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
