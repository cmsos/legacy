#ifndef _tcds_ttcspy_TTCSpyInfoSpaceHandler_h_
#define _tcds_ttcspy_TTCSpyInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace ttcspy {

    /**
     * This class holds and handles all monitoring items for a TTCSpy
     * hardware object.
     */
    class TTCSpyInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      TTCSpyInfoSpaceHandler(xdaq::Application& xdaqApp,
                             tcds::utils::InfoSpaceUpdater* const updater);

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace ttcspy
} // namespace tcds

#endif // _tcds_ttcspy_TTCSpyInfoSpaceHandler_h_
