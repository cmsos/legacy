#ifndef _tcds_pi_TTSLogFile_h_
#define _tcds_pi_TTSLogFile_h_

#include <fstream>
#include <stdint.h>
#include <string>
#include <map>

#include "tcds/pi/Definitions.h"

namespace tcds {
  namespace pi {

    class TTSLogEntry;

    class TTSLogFile
    {
      // NOTE: Calls writing to file/stream should use '\n' instead of
      // std::endl in order to avoid unnecessary flushing. Then use
      // log_.flush() to coordinate flushing when required.

    public:
      TTSLogFile(std::string const& softwareVersion,
                 std::string const& firmwareVersion,
                 std::string const& serviceName,
                 uint32_t const runNumber,
                 std::map<tcds::definitions::TTS_SOURCE, std::string> const& ttsChannelLabels,
                 std::string const& logFilePath,
                 std::string const& logFileName,
                 bool const isContinuation);
      ~TTSLogFile();

      // Logging manipulation.
      void openLog();
      void closeLog(bool const isToBeContinued=false);
      void addEntry(tcds::pi::TTSLogEntry const& logEntry);

      int numEntries() const;

    private:
      static char const kCommentChar = '-';

      std::string const sepLine_;

      std::string softwareVersion_;
      std::string firmwareVersion_;

      std::string const serviceName_;
      uint32_t const runNumber_;
      std::map<tcds::definitions::TTS_SOURCE, std::string> const ttsChannelLabels_;
      std::string const logFilePath_;
      std::string const logFileName_;
      std::string logFileFullName_;
      std::string lockFileFullName_;
      bool const isContinuation_;

      mutable std::ofstream log_;
      mutable std::ofstream lock_;

      // The number of entries added to the current file.
      int numEntries_;

      void openLogFile() const;
      void closeLogFile() const;

      void logBegin(bool const isContinuation) const;
      void logHeader() const;
      void logEnd(bool const isToBeContinued) const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTSLogFile_h_
