#ifndef _interface_bril_CalibFunctors_h_
#define _interface_bril_CalibFunctors_h_
#include <map>
#include <string>
#include <cstdio>
#include <memory>
#include "interface/bril/CalibData.h"

namespace interface{ namespace bril{
    class CalibFunctor{
    public:
      CalibFunctor(){}
      virtual void apply(const CalibData& calibdata, float ival, unsigned int ncollidingbx, float& oval) = 0;
      virtual void apply_n(const CalibData& calibdata, float* inputbxlumi, unsigned int ncollidingbx, float* outputbxlumi, size_t nbx=3564) = 0; 
      virtual ~CalibFunctor(){}
    };// CalibFunctor

    template<typename T> CalibFunctor* createT(){
      return new T;
    }
    
    typedef std::map<std::string, CalibFunctor*(*)() > map_type;
    static std::auto_ptr< map_type > s_registry(0);    

    class CalibFunctorFactory{
    public:
      CalibFunctorFactory(){}
      static CalibFunctor* createInstance(const std::string& s){
	if( !s_registry.get() ){ 
	  s_registry.reset( new map_type ); 
	}
	map_type::iterator it = s_registry.get()->find(s);
	if(it == s_registry.get()->end()) return 0;
	return it->second();
      }                
    private:
      CalibFunctorFactory(const CalibFunctorFactory&);
    };// cl CalibFunctorFactory
    
    template<typename T>
      class ImplCalibFunctorFactory : CalibFunctorFactory{
    public:
      ImplCalibFunctorFactory(const std::string& s){
	if( !s_registry.get() ){ 
	  s_registry.reset( new map_type ); 
	}	
	s_registry.get()->insert(std::make_pair(s,&createT<T>));
      }      
    };//cl ImplCalibFactory  

    class poly1d:public CalibFunctor{
      /**
	 A one-dimensional polynomial class
	 The highest power is the length of the coefs array-1
	 e.g. coefs[len-1]*x^2+coefs[len-2]*x+coefs[len-3]
      */
    public:
      poly1d(){}     
      virtual void apply(const CalibData& calibdata, float inval, unsigned int ncollidingbx, float& outval){
	size_t nsize = calibdata.nCoefs();
	if( nsize == 0 ){
	  outval=0;
	  return;
	}
	float result = 0;
	const float* c = calibdata.getCoefs();
	for(size_t myindex=0; myindex<nsize; ++myindex){    
	  float npower = nsize-1-myindex;
	  result = result + c[myindex]*pow(inval, npower);
	}
	outval = result;
	return ;
      }      
      virtual void apply_n(const CalibData& calibdata, float* invals, unsigned int ncollidingbx, float* outvals, size_t nbx=3564 ){
	for(size_t i=0; i<nbx; ++i){
	  apply(calibdata,invals[i],0,outvals[i]);
	}
      }           
    };//poly1d
    
    class poly1dWafterglow:public CalibFunctor{
      /**
	 Product one-dimensional polynomial correction and afterglow correction
	 The highest power is the length of the coefs array-1
	 e.g. coefs[len-1]*x^2+coefs[len-2]*x+coefs[len-3]
	 Afterglow correction :
	 If ncollidingbx>=first_bx_threshold: apply that factor
      */
    public:
      poly1dWafterglow(){}
      virtual void apply(const CalibData& calibdata, float inval, unsigned int ncollidingbx, float& outval){
	size_t nsize = calibdata.nCoefs();
	if( nsize == 0 ){
	  outval=0;
	  return;
	}
	float result = 0;
	const float* c = calibdata.getCoefs();
	for(size_t myindex=0; myindex<nsize; ++myindex){	  
	  float npower = nsize-1-myindex;
	  result = result + c[myindex]*pow(inval, npower);
	}
	size_t nafterglows = calibdata.nAfterglowThresholds();
	if( nafterglows!=0 ){
	  float factor = 1.;
	  const AfterglowThreshold* a = calibdata.getAfterglowThreshold();
	  for( size_t i = nafterglows; i>0; --i){
	    size_t idx = i-1;
	    unsigned int bxthreshold = a[idx].first;
	    factor = a[idx].second;
	    if( ncollidingbx>= bxthreshold ){
	      result = result*factor;
	      break;
	    }
	  }
	}
	outval = result;
	return ;
      }      
      virtual void apply_n(const CalibData& calibdata,float* invals, unsigned int ncollidingbx, float* outvals, size_t nbx=3564 ){
	for(size_t i=0; i<nbx; ++i){
	  apply(calibdata,invals[i],ncollidingbx,outvals[i]);
	}
      }
    }; //poly1dWafterglow
    }}

static interface::bril::ImplCalibFunctorFactory<interface::bril::poly1d> reg1("poly1d");

static interface::bril::ImplCalibFunctorFactory<interface::bril::poly1dWafterglow> reg2("poly1dWafterglow");

#endif
