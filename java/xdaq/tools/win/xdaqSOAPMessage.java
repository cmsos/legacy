package xdaq.tools.win;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;

import javax.xml.parsers.*;

import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.soap.Name;
import xdaq.tools.win.xdaqSOAPException;
// use of URN
public class xdaqSOAPMessage
{
	String targetAddr;
	String command;
	SOAPBodyElement bodyElement_;
	SOAPMessage msg_;
	SOAPEnvelope envelope_;
	String nameSpace_;
	String nameSpaceUrl_;
	
	public xdaqSOAPMessage(SOAPMessage msg) {
		msg_ = msg;						
	}
	
	public xdaqSOAPMessage(String command, String targetAddr ) throws xdaqSOAPException {
		nameSpace_ = new String("xdaq");
		nameSpaceUrl_ = new String("urn:xdaq-soap:3.0");
	
		this.command = command;
		this.targetAddr = targetAddr;
		
		try {
			MessageFactory mf = MessageFactory.newInstance();
			msg_ = mf.createMessage();
			
			SOAPPart sp = msg_.getSOAPPart();
			envelope_ = sp.getEnvelope();
			envelope_.addNamespaceDeclaration("xsi","http://www.w3.org/2001/XMLSchema-instance");
			envelope_.addNamespaceDeclaration("soapenc","http://schemas.xmlsoap.org/soap/encoding/");
			SOAPHeader header = envelope_.getHeader();
			SOAPBody body = envelope_.getBody();
		
			javax.xml.soap.Name commandName = envelope_.createName (command, nameSpace_, nameSpaceUrl_ );
			bodyElement_ = body.addBodyElement ( commandName );
			String urn = "urn:xdaq-application:lid=";
			urn += targetAddr;
			msg_.getMimeHeaders().addHeader ("SOAPAction", urn);
		} catch (Exception e) {
			throw new xdaqSOAPException (e.getMessage() );
		}		
	}
	
	public void addParameter (String name, String type, String value) {
		try {
			SOAPElement element = bodyElement_.addChildElement ( 
					envelope_.createName (
						"Parameter", 
						"",
						""));
						
			element.addAttribute ( envelope_.createName ( "name" ), name);
			element.addAttribute ( envelope_.createName ( "type" ), type);
			element.addTextNode ( value );
		} catch (SOAPException e) {
			System.out.println (e);
		}
	}
	
    public void detachElementsByAttribute (String nameString, String value) {
	    try {
		javax.xml.soap.Name name = envelope_.createName(nameString);
		detachElementsByAttributeRecursion ((SOAPElement) bodyElement_, name, value);
	    } catch (javax.xml.soap.SOAPException ex) {
		System.out.println (ex.toString());
	    }
	}
	
    private void detachElementsByAttributeRecursion 
    	(SOAPElement element, javax.xml.soap.Name name, String value) {
	    java.util.Iterator i = element.getChildElements();
	    Vector v = new Vector();

	    while (i.hasNext()) {
		java.lang.Object obj = i.next();
		if (obj instanceof SOAPElement) {
		    SOAPElement e = (SOAPElement) obj;
		    if (e.getAttributeValue ( name ) != null && e.getAttributeValue ( name ).equals (value) ) {
			v.add(e);
		    } else {
			detachElementsByAttributeRecursion (e, name, value);
		    }
		}
	    }

	    for (int j = 0; j < v.size(); j++) {
		((SOAPElement) v.elementAt(j)).detachNode();
	    }
	}
	
	public Document getDOM() {
		try {
			ByteArrayOutputStream outBuffer = new ByteArrayOutputStream();
			msg_.writeTo ( outBuffer );			
			ByteArrayInputStream inBuffer = new ByteArrayInputStream ( outBuffer.toByteArray() ); 	
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			if (builder.isNamespaceAware() == false) {
				System.out.println ("WARNING: DOM documents do not have namespaces in this implementation.");
			}
	
			return builder.parse(inBuffer);
		}
		catch(SOAPException e ) {
			System.out.println(e);
			return null;
		}
		catch(SAXException e) {
			System.out.println(e);
			return null;		
		}	
		catch(IOException e) {		
			System.out.println(e);
			return null;
		}
		catch (ParserConfigurationException e ) {
			System.out.println(e);
			return null;
		}	
	}
	
	public void setNameSpace (String name, String url) {
		nameSpace_ = name;
		nameSpaceUrl_ = url;
	}
	
	public SOAPMessage getSOAPMessage() {
		return msg_;
	}

	
	public void setParameters(org.w3c.dom.Node node) {
		setParameters ( node, bodyElement_ );
	}
	
	public void addAttribute (String name, String value) {
		try {
			javax.xml.soap.Name aName = envelope_.createName (name);
			bodyElement_.addAttribute (aName, value);
		} catch (SOAPException e) {
			//
	   }
	}
	
	
	public void setParameters(org.w3c.dom.Node node, SOAPElement current) {
		try {
			if (node.getNodeType() == org.w3c.dom.Node.TEXT_NODE) {
				current.addTextNode ( node.getNodeValue() );
			}
			else if (node.getNodeType() == org.w3c.dom.Node.ELEMENT_NODE) {
				String name = null;
				if (node.getPrefix() == null) {
					name = node.getNodeName();
				} else {
					name = node.getLocalName();
				}
			
				SOAPElement element = current.addChildElement ( 
					envelope_.createName (
						name, 
						node.getPrefix(),
						node.getNamespaceURI()));

				NamedNodeMap attributes = node.getAttributes();

				for (int k = 0; k < attributes.getLength(); k++) {
					if ( ( attributes.item(k).getNodeValue().equals(node.getNamespaceURI()) ) &&
   			        ( attributes.item(k).getNodeName().startsWith("xmlns") ) ) {
						//System.out.println("skipping namespace attribute" + node.getNamespaceURI());
						continue;
					} else {
						//System.out.println("set attribute" + attributes.item(k).getNodeValue());
						element.addAttribute ( 
						envelope_.createName ( attributes.item(k).getNodeName() ),
						attributes.item(k).getNodeValue()
						);
					}	
				}

				NodeList children = node.getChildNodes();
				for (int i = 0; i < children.getLength(); i++) {
					setParameters ( children.item(i), element );
				}
			}
		} catch (Throwable t) {
			System.out.println (t);
		}	
	}
}

