// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _sentinel_arc_HyperDAQ_h_
#define _sentinel_arc_HyperDAQ_h_

#include "xgi/framework/UIManager.h"
#include "sentinel/arc/AbstractHyperDAQ.h"
#include "sentinel/arc/utils/Node.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xgi/Utils.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "jansson.h"

namespace sentinel
{
	namespace arc
	{

		class Application;

		class HyperDAQ : public xgi::framework::UIManager, public AbstractHyperDAQ
		{
			public:

				HyperDAQ (sentinel::arc::Application * app);

				void enable();

				void Default (xgi::Input * in, xgi::Output * out) ;
				void loading (xgi::Input * in, xgi::Output * out) ;

				void modelTree (xgi::Input * in, xgi::Output * out) ;
				void modelTabPage (xgi::Input * in, xgi::Output * out) ;

				void shelfRulesTable (xgi::Input * in, xgi::Output * out) ;
				void clearRulesTable (xgi::Input * in, xgi::Output * out) ;
				void debounceRulesTable (xgi::Input * in, xgi::Output * out) ;

				void exceptionsTable (xgi::Input * in, xgi::Output * out) ;

				void viewExceptionTable (xgi::Input * in, xgi::Output * out) ;
				//void viewExceptionProperties (xgi::Input * in, xgi::Output * out) ;

				///////////
				void firealarm (xgi::Input * in, xgi::Output * out) ;
				void revokealarm (xgi::Input * in, xgi::Output * out) ;

				//////////

				void printAllExceptions (xgi::Input * in, xgi::Output * out) ;

				std::string htmlencode (const std::string& src);

				// restful
				void getNode (xgi::Input * in, xgi::Output * out) ;
				void getException (xgi::Input * in, xgi::Output * out) ;
				void getExceptionList (xgi::Input * in, xgi::Output * out) ;
				void getModelTree (xgi::Input * in, xgi::Output * out) ;

			protected:

				//////////
				void modelTreeRow (xgi::Output * out, sentinel::arc::utils::Node * n, size_t depth) ;
				//////////

				void statisticsTabPage (xgi::Output * out) ;

				void shelfRulesTabPage (xgi::Output * out) ;
				void clearRulesTabPage (xgi::Output * out) ;
				void debounceRulesTabPage (xgi::Output * out) ;

				void getExceptionList (json_t * jsonvector, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap, const std::string & exceptionState);
				json_t * getNode (int & totalCount, sentinel::arc::utils::Node::ExceptionMap & exceptionsMap);
				json_t * getModelTree ( sentinel::arc::utils::Node * node);

				//void exceptionsTabPage (xgi::Output * out) ;

				sentinel::arc::Application * application_;

		};
	}
}
#endif
