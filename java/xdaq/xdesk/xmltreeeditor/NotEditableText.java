package xmltreeeditor;

public class NotEditableText {
  private String text;
  public NotEditableText(String str) {
    text = str;
  }

  public String toString() {
    return text;
  }
}
