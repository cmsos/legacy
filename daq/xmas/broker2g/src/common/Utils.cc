/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/Utils.h"

#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "xoap/MessageFactory.h"
#include "xoap/domutils.h"

#include <sstream>

DOMDocument* xmas::broker2g::loadDOM( const std::string& pathname) 
{
	std::string filename = pathname;
	if ((filename.find("file:/") == std::string::npos) && (filename.find("http://")))
	{   
		filename.insert(0, "file:");
	} 
      
	XMLURL* source = 0;
	try
	{ 
		source = new XMLURL(filename.c_str());
	}
	catch (...)
	{
		// using ... because XMLURL calls "throw;" without any exception       
		XCEPT_RAISE (xmas::broker2g::exception::ParserException, "Failed to load XML from " + filename);
	}

	try
	{
		xoap::DOMParser* p = xoap::getDOMParserFactory()->get("configure");
		DOMDocument* doc = p->parse(*source);
		delete source;
		return doc;
	}
	catch (xoap::exception::Exception& xe)
	{
		delete source;
		XCEPT_RETHROW (xmas::broker2g::exception::ParserException, "Cannot parse XML loaded from " + filename, xe);
	}
}

