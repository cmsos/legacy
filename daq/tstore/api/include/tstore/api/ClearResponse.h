// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_api_ClearResponse_h_
#define _tstore_api_ClearResponse_h_

#include <string>
#include "xoap/MessageReference.h"
#include "ws/addressing/Headers.h"
#include "tstore/api/exception/Exception.h"
#include "tstore/api/Request.h"

namespace tstore
{
namespace api
{
	class ClearResponse: public tstore::api::Request
	{
		public:
		
		ClearResponse();
		
		ClearResponse(xoap::MessageReference& msg) ;
        
		virtual ~ClearResponse();
				
		xoap::MessageReference toSOAP();		
	};
}}

#endif
