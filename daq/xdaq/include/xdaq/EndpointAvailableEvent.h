// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdaq_EndpointAvailableEvent_h_
#define _xdaq_EndpointAvailableEvent_h_

#include <string>
#include "xdaq/Event.h"
#include "toolbox/mem/CountingPtr.h"
#include "toolbox/mem/ThreadSafeReferenceCount.h"
#include "toolbox/mem/StandardObjectPolicy.h"
#include "xdaq/Endpoint.h"
#include "xdaq/Network.h"

namespace xdaq 
{

	class EndpointAvailableEvent: public xdaq::Event
	{
		public:

		// typedef toolbox::mem::CountingPtr<EndpointAvailableEvent, toolbox::mem::ThreadSafeReferenceCount, toolbox::mem::StandardObjectPolicy> Reference;

		EndpointAvailableEvent(const xdaq::Endpoint* endpoint, const xdaq::Network* network)
			: xdaq::Event("xdaq::EndpointAvailableEvent", 0)
		{
			endpoint_ = endpoint;
			network_ = network;
		}

		EndpointAvailableEvent(const xdaq::Endpoint* endpoint, const xdaq::Network* network, xdaq::Application* originator)
			: xdaq::Event("xdaq::EndpointAvailableEvent", originator)
		{
			endpoint_ = endpoint;
			network_ = network;
		}

		const xdaq::Endpoint* getEndpoint() const
		{
			return endpoint_;
		}
		
		const xdaq::Network* getNetwork() const
		{
			return network_;
		}

		protected:

		const xdaq::Endpoint* endpoint_;
		const xdaq::Network* network_;

	};

}

#endif
