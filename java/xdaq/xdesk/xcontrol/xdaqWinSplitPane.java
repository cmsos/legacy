package xdaq.tools.win;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import javax.swing.JSplitPane;

import org.w3c.dom.*;

// This class defines three panes, the left one is
// dedicated to display the XDAQ configuration tree.
// The right one is split into two halfs. The upper pane
// can display a list of daqlets, the lower one displays
// a selected Daqlet
//


public class xdaqWinSplitPane extends JPanel
{
	JSplitPane daqletViewSplit_;
	private JSplitPane treeViewSplit_;
	JLabel empty_ = new JLabel ("No Daqlets");
		
	public xdaqWinSplitPane()
	{
		createLayout();
	}
	
	
	private void createLayout()
	{
		setLayout(new GridLayout(1, 1));

		daqletViewSplit_ = new JSplitPane
		(
			JSplitPane.VERTICAL_SPLIT,
			false,
			null,
			null
		);
		
		daqletViewSplit_.setDividerLocation(150);
		
		treeViewSplit_ = new JSplitPane
		(
			JSplitPane.HORIZONTAL_SPLIT,
			false,
			null,
			daqletViewSplit_
		);
		
		treeViewSplit_.setDividerLocation(100);
		
		add(treeViewSplit_);
	}
		
	public void addTreePane ( JPanel dummytree )
	{
		treeViewSplit_.setLeftComponent (new JScrollPane (dummytree));
	}
	
	public void addTreePane ( JTree tree )
	{
		treeViewSplit_.setLeftComponent (new JScrollPane (tree));
	}
	
	public void setEmptyDaqletList ()
	{
		// Must force display of new JScrollPane, otherwise using
		// an instance variable will keep the component in the split
		// pane and partially hide other added components, like the
		// Daqlet table. This is a Java "feature"
		daqletViewSplit_.setLeftComponent ( new JScrollPane (empty_) );
		daqletViewSplit_.setRightComponent ( new JScrollPane (empty_));
	}
	
	
	public void setDaqletList ( java.awt.Container container)
	{
		daqletViewSplit_.setLeftComponent (new JScrollPane (container));
		daqletViewSplit_.setDividerLocation(150);
	}
		
	public JSplitPane getDaqletPane ()
	{
		return daqletViewSplit_;
	}
}
