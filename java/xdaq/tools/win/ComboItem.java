package xdaq.tools.win;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import xdaq.tools.win.CanEnable;

public class ComboItem implements CanEnable 
{
    Object  obj;
    boolean isEnable;
    
    ComboItem(Object obj,boolean isEnable) {
      this.obj      = obj;
      this.isEnable = isEnable;
    }
    
    ComboItem(Object obj) {
      this(obj, true);
    }
    
    public boolean isEnabled() {
      return isEnable;
    }
    
    public void setEnabled(boolean isEnable) {
      this.isEnable = isEnable;
    }
    
    public String toString() {
      return obj.toString();
    }
}
