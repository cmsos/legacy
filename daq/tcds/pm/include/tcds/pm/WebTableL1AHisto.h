#ifndef _tcds_pm_WebTableL1AHisto_h
#define _tcds_pm_WebTableL1AHisto_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace pm {

    class WebTableL1AHisto : public tcds::utils::WebObject
    {

    public:
      WebTableL1AHisto(std::string const& name,
                       std::string const& description,
                       tcds::utils::Monitor const& monitor,
                       std::string const& itemSetName,
                       std::string const& tabName,
                       size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_WebTableL1AHisto_h
