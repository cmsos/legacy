// needs xml-apis.jar and xercesImpl.jar from TriDAS/java/signed

import java.net.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import java.io.*;
import org.w3c.dom.*;
import org.apache.xml.serialize.*;

public class Adapter {

static public void main(String args[] )
{
	String state = Adapter.getState("http://lxcmd101:1972","urn:xdaq-application:lid=0", "SOAPStateMachine");
	System.err.println(state);
}

static public String getState(String uri, String urn, String className)
{
	URL url = null;
	try  {
		url = new URL(uri+"/"+urn);
	} catch (MalformedURLException mue) {
		System.err.println(mue.toString());
		return "";
	} catch (java.io.IOException ioe) {
		System.err.println(ioe.toString());
		return "";
	}
	try {
		//Prepare Message
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document document = builder.newDocument();
		Element envelope = document.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "soap-env:Envelope");
		envelope.setAttribute("soap-env:encodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
		envelope.setAttribute("xmlns:soap-enc","http://schemas.xmlsoap.org/soap/encoding/");
		envelope.setAttribute("xmlns:soap-env","http://schemas.xmlsoap.org/soap/envelope/");
		envelope.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
		envelope.setAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
		document.appendChild(envelope);
		Element body = document.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "soap-env:Body");
		envelope.appendChild(body);
		Element commandElement = document.createElementNS(urn, "xdaq:"+ "ParameterGet");
		commandElement.setAttribute("xmlns:xdaq","urn:xdaq-soap:3.0");
		body.appendChild(commandElement);
		Element properties = document.createElementNS("urn:xdaq-application:"+className, "p:properties");
		properties.setAttribute("xsi:type","soapenc:Struct");
		properties.setAttribute("xmlns:p","urn:xdaq-application:"+className);
		Element state = document.createElementNS("urn:xdaq-application:"+className, "p:State");
		state.setAttribute("xsi:type","xsd:string");
		commandElement.appendChild(properties);
		properties.appendChild(state);
		
		// Connect and send
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setUseCaches(false);
		connection.setDoOutput(true);
		connection.setDoInput(true);		
		connection.setRequestMethod("POST");
		String soapRequest = serialize(document);
		System.out.println(soapRequest);
		connection.setRequestProperty("Content-length", Integer.toString(soapRequest.length()));
		connection.setRequestProperty("SOAPAction",urn);
		connection.connect();
		OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
		writer.write(soapRequest);
		writer.flush();
		writer.close();

		// Get the response
		InputStream istream = connection.getInputStream();			
		BufferedReader ireader = new BufferedReader( new InputStreamReader(istream));
		String inputLine;
		String soapResponse = "";
		while ( ( inputLine = ireader.readLine()) != null ) {	
					soapResponse = soapResponse.concat(inputLine);
		}
		istream.close();
		
		// Parse response
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder responseBuilder = factory.newDocumentBuilder();
		ByteArrayInputStream inBuffer = new ByteArrayInputStream ( soapResponse.getBytes() );			
		Document responseDoc = responseBuilder.parse ( inBuffer );
		NodeList faultList = responseDoc.getElementsByTagNameNS ("*", "faultstring");
		if (faultList.getLength() == 1)	{
			return faultList.item(0).getFirstChild().getNodeValue();
		} else {

			NodeList l = responseDoc.getElementsByTagNameNS ("*", "State");
			if (l.getLength() == 1) {
				return l.item(0).getFirstChild().getNodeValue();
			} else {
				return "";
			} 
		}
	} catch (SAXParseException spe) {
		System.err.println(spe.toString());
		return "";
	} catch (SAXException sxe) {
		System.err.println(sxe.toString());
		return "";
	} catch (ParserConfigurationException pce) {
		System.err.println(pce.toString());
		return "";
	} catch (IOException ioe) {
		System.err.println(ioe.toString());
		return "";
	}
}

public static String serialize ( Document doc )
{
	try {
		ByteArrayOutputStream ostream = new ByteArrayOutputStream();
		OutputFormat oformat = new OutputFormat("XML","ISO-8859-1",true);
		oformat.setIndent(1);
		oformat.setIndenting(true);
		XMLSerializer serializer = new XMLSerializer(ostream,oformat);
		serializer.asDOMSerializer();
		serializer.serialize( doc.getDocumentElement());
		return ostream.toString();
	} catch (java.io.IOException ex) {
		return ex.toString();
	}
}

};
