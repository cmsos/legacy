package xmltreeeditor;

import java.awt.event.MouseAdapter;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.util.*;

public class PopupTrigger
    extends MouseAdapter {

  JPopupMenu popup_menu = null;
  Hashtable icons;
  XMLTree tree;
  DefaultMutableTreeNode currentNode;
  int index, childCount;
  DefaultMutableTreeNode parentNode;
  TreePath currentPath;
  int x, y;

  public PopupTrigger() {
    icons = new Hashtable();
    icons.put("Collapse", new ImageIcon("CollapseAll16.gif"));
    icons.put("Expand", new ImageIcon("ExpandAll16.gif"));
    icons.put("Delete", new ImageIcon("remove16.gif"));
    icons.put("InsertBefore", new ImageIcon("InsertBefore16.gif"));
    icons.put("InsertAfter", new ImageIcon("InsertAfter16.gif"));
    icons.put("Add", new ImageIcon("Add16.gif"));
    icons.put("Element", new ImageIcon("Element16.gif"));
    icons.put("Attribute", new ImageIcon("Attribute16.gif"));
    icons.put("Text", new ImageIcon("Text16.gif"));
    icons.put("CData", new ImageIcon("CDATA16.gif"));
    icons.put("PInstruction", new ImageIcon("Processing16.gif"));
    icons.put("Comment", new ImageIcon("Comment16.gif"));
    icons.put("MoveUp", new ImageIcon("MoveUp16.gif"));
    icons.put("MoveDown", new ImageIcon("MoveDown16.gif"));
  }

// -------------------------------------------------------------------------------
  public void mouseReleased(MouseEvent e) {

    if (!e.isPopupTrigger()) {
      return;
    }

    x = e.getX();
    y = e.getY();
    tree = (XMLTree) e.getSource();
    currentPath = tree.getPathForLocation(x, y);

    if (currentPath == null) {
      return;
    }

    currentNode = (DefaultMutableTreeNode) currentPath.getLastPathComponent();
    parentNode = (DefaultMutableTreeNode) currentNode.getParent();
    try {
      index = parentNode.getIndex(currentNode);
      childCount = parentNode.getChildCount();
    }
    catch (NullPointerException ex) {
      index = 0;
      childCount = 0;
    }

    tree.setSelectionPath(currentPath);
    currentNode = (DefaultMutableTreeNode) currentPath.
        getLastPathComponent();
    popup_menu = new JPopupMenu();
    buildPopup();

    if (popup_menu.getComponentCount() > 0) {
      popup_menu.show(tree, x, y);
    }

  }

  private void buildPopup() {
//----
    if (!currentNode.isLeaf()) {
      if (tree.isExpanded(currentPath)) {
        popup_menu.add(new PopupAction("Collapse",
                                       (ImageIcon) icons.get("Collapse"),
                                       tree));
      }
      else {
        popup_menu.add(new PopupAction("Expand", (ImageIcon) icons.get("Expand"),
                                       tree));
      }
      popup_menu.addSeparator();
    }
//---
    if (isAllowInsertElementBefore()) {
      popup_menu.add(new PopupAction("Insert Element Before",
                                     (ImageIcon) icons.get("InsertBefore"),
                                     tree));
    }
    if (isAllowInsertElementAfter()) {
      popup_menu.add(new PopupAction("Insert Element After",
                                     (ImageIcon) icons.get("InsertAfter"),
                                     tree));
    }
    JMenu addMenu = new JMenu(new PopupAction("Add..",
                                              (ImageIcon) icons.get("Add"),
                                              tree));
    if (isAllowElement()) {
      addMenu.add(new PopupAction("Element", (ImageIcon) icons.get("Element"),
                                  tree));
    }
    if (isAllowAttributes()) {
      addMenu.add(new PopupAction("Attribute",
                                  (ImageIcon) icons.get("Attribute"),
                                  tree));
    }
    addMenu.add(new PopupAction("Text", (ImageIcon) icons.get("Text"), tree));
    addMenu.add(new PopupAction("Comment", (ImageIcon) icons.get("Comment"),
                                tree));
    addMenu.add(new PopupAction("CData", (ImageIcon) icons.get("CData"),
                                tree));
    addMenu.add(new PopupAction("Processing Instruction",
                                (ImageIcon) icons.get("PInstruction"), tree));
    popup_menu.add(addMenu);
    if (isMoveUpAllowed()) {
      popup_menu.add(new PopupAction("Move Up", (ImageIcon) icons.get("MoveUp"),
                                     tree));
    }
    if (isMoveDownAllowed()) {
      popup_menu.add(new PopupAction("Move Down",
                                     (ImageIcon) icons.get("MoveDown"), tree));
    }

    if (isDeleteAllowed()) {
      popup_menu.addSeparator();
      popup_menu.add(new PopupAction("Delete", (ImageIcon) icons.get("Delete"),
                                     tree));
    }

    popup_menu.add(new PopupAction("Normalize tree",null,tree));
    tree.add(popup_menu);

  }

//return true if current selected node is not a root
//and not the last element in parent's child collections
  boolean isMoveDownAllowed() {
    //System.out.println("Index : " + index + " Count : " + childCount );
    boolean returnValue = true;
    if ( (currentNode.isRoot()) || (index == (childCount - 1))) {
      returnValue = false;
    }
    return returnValue;
  }

//return true if current selected node is not a root
//and not the top element in parent's child collections

  boolean isMoveUpAllowed() {
    boolean returnValue = true;
    if ( (currentNode.isRoot()) || (index == 0)) {
      returnValue = false;
    }
    return returnValue;
  }

//return true if current selected node is not root
  boolean isDeleteAllowed() {
    return! (currentNode.isRoot());
  }

  boolean isAllowInsertElementBefore() {
    XMLTreeNodeUserObject userObject = (XMLTreeNodeUserObject) currentNode.
        getUserObject();
    boolean returnValue = true;
    if (currentNode.isRoot() || userObject.getType() != 1) {
      returnValue = false;
    }
    return returnValue;
  }

  boolean isAllowInsertElementAfter() {
    return isAllowInsertElementBefore();
  }

  boolean isAllowAttributes() {
    XMLTreeNodeUserObject userObject = (XMLTreeNodeUserObject) currentNode.
        getUserObject();
    return (userObject.getType() == 1);
  }

  boolean isAllowElement() {
    XMLTreeNodeUserObject userObject = (XMLTreeNodeUserObject) currentNode.
        getUserObject();
    return (userObject.getType() == 1);
  }
}



