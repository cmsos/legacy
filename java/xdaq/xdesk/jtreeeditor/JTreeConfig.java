package xdaq.xdesk.jtreeeditor;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.xpath.XPathAPI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class JTreeConfig {
//---------------------------Variables Definition-------------------------------
  private Hashtable treeNodeIcons;

  private Hashtable popupIcons;
  private Hashtable generalValues;
  private Document document;
  private Class currentClass;
  private DocumentBuilderFactory documentBuilderFactory;
  private DocumentBuilder documentBuilder;

//------------------Constructors------------------------------------------------

  /**An empty constructor*/
  public JTreeConfig() {
    treeNodeIcons = new Hashtable();
    popupIcons = new Hashtable();
    generalValues = new Hashtable();
    document = null;
    currentClass = this.getClass();
    initializeTables();
  }

  /**Constructor by DOM Document*/
  public JTreeConfig(Document doc) throws IOException,
      ParserConfigurationException, TransformerConfigurationException,
      SAXException, TransformerException {
    this();
    this.setDocument(doc);
  }

  /**Constructor by XML file*/
  public JTreeConfig(File file) throws IOException,
      ParserConfigurationException, TransformerConfigurationException,
      SAXException, TransformerException {
    this();
    Document doc_ = null;
    if (file.isFile()) {
      javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.
          DocumentBuilderFactory.newInstance();
      javax.xml.parsers.DocumentBuilder builder = factory.newDocumentBuilder();
      doc_ = builder.parse(file);
      this.setDocument(doc_);
    }
  }

//------------CREATE DEFAULT VALUES TABLE---------------------------------------
  //**********************************************************************
   //All the icons used as default icons are precompiled to the
   //jexplorerconfig.jar/icons
   //**********************************************************************
    /**Sets default values at configuration tables*/
    private void initializeTables() {
      //initialize generalValues

      String resoursePath = "/xdaq/xdesk/jtreeeditor/icons/";
      generalValues.put("treemultiselection", "true");
      generalValues.put("treeRowHeight", "16");

      //initialize treeNodesIcons

      //initialize default node icons
      treeNodeIcons.put("default_Element", resoursePath + "Element16.gif");
      treeNodeIcons.put("default_Attribute", resoursePath + "Attribute16.gif");
      treeNodeIcons.put("default_Text", resoursePath + "Text16.gif");
      treeNodeIcons.put("default_Cdata", resoursePath + "Cdata16.gif");
      treeNodeIcons.put("default_Comment", resoursePath + "Comment16.gif");
      treeNodeIcons.put("default_Processing", resoursePath + "Processing16.gif");

      //add default presentation node icons
      treeNodeIcons.put("Partition", resoursePath + "partition16x16.png");
      treeNodeIcons.put("Host", resoursePath + "host.png");
      treeNodeIcons.put("Script", resoursePath + "Script.gif");
      treeNodeIcons.put("Transport", resoursePath + "transport.gif");
      treeNodeIcons.put("Application", resoursePath + "application16x16.png");
      treeNodeIcons.put("default_Processing", resoursePath + "Processing16.gif");
      treeNodeIcons.put("Parameter", resoursePath + "Parameter.gif");
      treeNodeIcons.put("ClassDef", resoursePath + "ClassDef.gif");
      treeNodeIcons.put("TransportDef", resoursePath + "TransportDef.gif");
      treeNodeIcons.put("urlApplication", resoursePath + "urlApplication.gif");
      treeNodeIcons.put("urlTransport", resoursePath + "urlTransport.gif");
      treeNodeIcons.put("Address", resoursePath + "address.gif");

      //initialize popup icons
      popupIcons.put("Collapse", resoursePath + "CollapseAll16.gif");
      popupIcons.put("Expand", resoursePath + "ExpandAll16.gif");
      popupIcons.put("Delete", resoursePath + "Remove16.gif");
      popupIcons.put("InsertElementBefore", resoursePath + "InsertBefore16.gif");
      popupIcons.put("InsertElementAfter", resoursePath + "InsertAfter16.gif");
      popupIcons.put("Add", resoursePath + "Add16.gif");
      popupIcons.put("Element", resoursePath + "Element16.gif");
      popupIcons.put("Attribute", resoursePath + "Attribute16.gif");
      popupIcons.put("Text", resoursePath + "Text16.gif");
      popupIcons.put("CData", resoursePath + "Cdata16.gif");
      popupIcons.put("ProcessingInstruction", resoursePath + "Processing16.gif");
      popupIcons.put("Comment", resoursePath + "Comment16.gif");
      popupIcons.put("MoveUp", resoursePath + "MoveUp16.gif");
      popupIcons.put("MoveDown", resoursePath + "MoveDown16.gif");
    }

//*****************************************************************************
//                 CHANGE A DEFAULT VALUES AT TABLE
//            TO THE VALUES FROM CONFIGURATION DOMDocument
   /**Sets values at configuration tables accordin to DOM document*/
   private void setDocument(Document doc) throws IOException,
       ParserConfigurationException, TransformerConfigurationException,
       SAXException, TransformerException {
     document = doc;
     document.normalize();
//fill the generalValues by configuration files values

     String xpath = "//GENERAL/generalitem";

     NodeList generalNodes = XPathAPI.selectNodeList(doc, xpath);
     if (generalNodes != null) {
       for (int i = 0; i < generalNodes.getLength(); i++) {
         generalValues.remove(generalNodes.item(i).getAttributes().item(0).
                              getNodeValue());
         generalValues.put(generalNodes.item(i).getAttributes().item(0).
                           getNodeValue().toString(),
                           generalNodes.item(i).getAttributes().item(1).
                           getNodeValue().toString());
       }
     }

     //fill the popupIcons HashTable
     //Select all tags nammed "item" under the "POPUPICONS" tag
     xpath = "//POPUPICONS/item";
     NodeList popupIconsList = XPathAPI.selectNodeList(document, xpath);
     for (int i = 0; i < popupIconsList.getLength(); i++) {
       //Attributes item[0] this is a Name of JMenuItem in popupMenu
       String name = popupIconsList.item(i).getAttributes().item(0).
           getNodeValue();
       //Attributes item[1] this is an URL to the JMenuItem icon
       String path = popupIconsList.item(i).getAttributes().item(1).
           getNodeValue();
       popupIcons.remove(name);
       popupIcons.put(name, path);
     }
   }

//*****************************************************************************
   /**Returns ImageIcon for popupMenu*/
   public ImageIcon getPupupIcon(String key) {
     ImageIcon icon = null;
     String path = "";
     URL url = null;
     if (popupIcons.containsKey(key)) {
       path = popupIcons.get(key).toString();
     }
     try {
       url = new URL(path);
     }
     catch (MalformedURLException ex) {
       url = currentClass.getResource(path);
     }
     if (url != null) {
       icon = new ImageIcon(url);
     }
     if (url == null && path.length() > 0) {
       System.out.println("File " + path + " not found");
     }
     return icon;
   }

//*****************************************************************************
   /**Returns ImageIcon for JTreeEditor nodes*/
   public ImageIcon getIcon(JTreeUserObject nodeobject,
                            boolean isTreeEditable) {
     String name = nodeobject.getName();
     String path = "";
     URL url = null;
     ImageIcon icon = null;
     short type = nodeobject.getType();
     if (!isTreeEditable) {
       path = (treeNodeIcons.containsKey(name)) ?
           treeNodeIcons.get(name).toString() :
           treeNodeIcons.get(getDefaultNodeName(type)).toString();
     }
     else {
       path = treeNodeIcons.get(getDefaultNodeName(type)).toString();
     }

     try {
       url = new URL(path);
     }
     catch (MalformedURLException ex) {
       url = currentClass.getResource(path);
     }

     if (url == null && path.length() > 0) {
       System.out.println("File " + path +
                          " not found. Trying to create a default icon");
       url = currentClass.getResource(treeNodeIcons.get(getDefaultNodeName(type)).
                                      toString());
     }
     icon = new ImageIcon(url);
     return icon;
   }

//*****************************************************************************
   /**Returns true if multiselection is allowed*/
   public boolean isTreeMultiselectionAllowed() {
     if (generalValues.containsKey("treemultiselection")) {
       return (generalValues.get("treemultiselection").toString().compareTo(
           "true") == 0);
     }
     else {
       return true;
     }
   }

//******************************************************************************
   /**Sets multiselection state*/
   public void setTreeMultiSeectionAllowed(boolean isAllowed) {
     generalValues.remove("treemultiselection");
     generalValues.put("treemultiselection", new Boolean(isAllowed));
   }

//******************************************************************************
   /**Returns height of row of the tree*/
   public int getRowHeight() {
     int rowHeight = 16;
     if (generalValues.containsKey("treeRowHeight")) {
       String value = generalValues.get("treeRowHeight").toString();
       rowHeight = Integer.parseInt(value);
     }
     return rowHeight;
   }

//******************************************************************************
   /**Returns a key of treeNodeIcons according to treenode type
    *In the case of unknown type - returns default_element
    */
   private String getDefaultNodeName(short type) {
     short index = 0;
     String[] defaultValues = new String[] {
         "default_Element", "default_Element",
         "default_Attribute", "default_Text",
         "default_Cdata", "default_Element", "default_Element",
         "default_Comment", "default_Processing"};
     index = (type < defaultValues.length) ? type : 0;
     return defaultValues[index];
   }

//******************************************************************************
   /**Creates and returns a new DOM document by configuration tables content*/
   public Document getDocument() throws TransformerException,
       ParserConfigurationException {
     documentBuilderFactory = DocumentBuilderFactory.newInstance();
     documentBuilder = documentBuilderFactory.newDocumentBuilder();
     Document doc = documentBuilder.newDocument();
     Element root = doc.createElement("CONFIGURATION");
     root.appendChild(createGeneralElement(doc));
     root.appendChild(createMainIconsElement(doc));
     root.appendChild(createPopupIconsElement(doc));
     doc.appendChild(root);
     doc.normalize();
     return doc;
   }

//******************************************************************************
   /**Creates MAINICONS tag and attachs a mainicon nodes with 4 sizes of icon*/
   private Element createMainIconsElement(Document doc) throws
       TransformerException {
     Element mainIconsElement = doc.createElement("MAINICONS");
     String name;
     String key;
     Vector v = new Vector(treeNodeIcons.keySet());
     Collections.sort(v);

     for (Enumeration e = v.elements(); e.hasMoreElements(); ) {
       name = e.nextElement().toString();
       Element mainIconNode = doc.createElement("mainicon");
       mainIconNode.setAttribute("name", name);
       mainIconNode.appendChild(geticonElement(name, "tiny", doc));
       mainIconNode.appendChild(geticonElement(name, "small", doc));
       mainIconNode.appendChild(geticonElement(name, "medium", doc));
       mainIconNode.appendChild(geticonElement(name, "large", doc));
       mainIconsElement.appendChild(mainIconNode);
     }

     return mainIconsElement;
   }

//******************************************************************************
   /**Returns an element according to both JTreeEditor node type and icon size*/
   private Element geticonElement(String nodeName, String iconType,
                                  Document doc) throws
       TransformerException {
     Element iconElement = doc.createElement(iconType);
     String path = "";
     String xpath = "";
     //"tiny" is a default size for node icon
     if (iconType == "tiny" && treeNodeIcons.containsKey(nodeName)) {
       path = treeNodeIcons.get(nodeName).toString();
     }
     else {
       xpath = "//mainicon[contains(@name,'" + nodeName + "')]/" + iconType;
       Node nd = XPathAPI.selectSingleNode(doc, xpath);
       if (nd != null) {
         path = nd.getAttributes().item(0).getNodeValue();
       }
     }
     iconElement.setAttribute("url", path);
     return iconElement;
   }

//******************************************************************************
   /**Creates a POPUPICONS tag and attachs childrens
    * from popupIcons configuration table
    */
   private Element createPopupIconsElement(Document doc) {
     Element POPUPICONS = doc.createElement("POPUPICONS");
     for (Enumeration e = popupIcons.keys(); e.hasMoreElements(); ) {
       Element node = doc.createElement("item");
       String name = e.nextElement().toString();
       String url = popupIcons.get(name).toString();
       node.setAttribute("name", name);
       node.setAttribute("url", url);
       POPUPICONS.appendChild(node);
     }
     return POPUPICONS;
   }

//******************************************************************************
   /**Creates GENERAL tag and attachs values from general values configuration table*/
   private Element createGeneralElement(Document doc) {
     Element generalElement = doc.createElement("GENERAL");
     for (Enumeration e = generalValues.keys(); e.hasMoreElements(); ) {
       Element node = doc.createElement("generalitem");
       String name = e.nextElement().toString();
       String url = generalValues.get(name).toString();
       node.setAttribute("key", name);
       node.setAttribute("value", url);
       generalElement.appendChild(node);
     }
     return generalElement;
   }

//******************************************************************************
   /**Save DOM document*/
   public void save() {
     try {
       Document doc = this.getDocument();
       String startDir = System.getProperty("user.dir");
       JFileChooser fc_ = new JFileChooser(new File(startDir));
       JTreeFileFilter filter = new JTreeFileFilter();
       fc_.setFileFilter(filter);
       fc_.setAcceptAllFileFilterUsed(false); //dont accept All Files option
       fc_.setMultiSelectionEnabled(false); //multyselection not accepted
       File file = null;
       int returnVal = fc_.showSaveDialog(new JFrame());

       if (returnVal == JFileChooser.APPROVE_OPTION) {
         file = fc_.getSelectedFile();
         if (!file.getPath().endsWith(".xml")) { //must be an xml file
           file = new File(file.getPath() + ".xml");
         }
         boolean isSaveAccepted = false;
         if (file.exists()) { //have to confirm an overwriting
           int retVal = JOptionPane.showConfirmDialog(new JFrame(),
               "File " + file.getName() +
               " already exists. Do you want to overwrite?",
               "File already exists", JOptionPane.YES_NO_OPTION);
           isSaveAccepted = (retVal == 0); //yes option equals 0
         }
         else { //confirm creation of new file at directory
           int retVal = JOptionPane.showConfirmDialog(new JFrame(),
               "Do you want to create file " + file.getName() + " at " +
               file.getPath().substring(0, file.getAbsolutePath().length() -
                                        (file.getName().length() + 1)),
               "File creation confirmation", JOptionPane.YES_NO_OPTION);
           isSaveAccepted = (retVal == 0); //yes option equals 0
         }
         if (isSaveAccepted) {
           StreamResult result = new StreamResult(file);
           DOMSource sourse = new DOMSource(this.getDocument());
           Transformer transformer = TransformerFactory.newInstance().
               newTransformer();
           transformer.setOutputProperty("indent", "yes");
           transformer.transform(sourse, result);
         }
       }
     }
     catch (Exception e) {
       System.out.println("Saving GUI configuration error " + e.getMessage());
     }
   }
}
