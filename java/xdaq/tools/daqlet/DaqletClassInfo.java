package xdaq.tools.daqlet;

import java.util.*;

public class DaqletClassInfo
{
	HashMap associations_;
	
	public DaqletClassInfo()
	{
		associations_ = new HashMap();
	}
	
	public void put (Properties properties)
	{
		String association = properties.getProperty ("association");
		Vector v = (Vector) associations_.get(association);
		if (v == null)
		{
			v = new Vector();
			v.addElement (properties);
			associations_.put (association, v);
		} else {
			v.addElement (properties);
		}
	}
	
	// Get All Daqlets that are associated with association
	public Vector get (String association)
	{
		return  (Vector) associations_.get(association);
	}
	
	// Get a specific Daqlet from the list of associated onesw
	public Properties get (String association, int selectedClass)
	{
		Vector v = (Vector) associations_.get(association);
		if (v == null) return null;
		return (Properties) v.elementAt(selectedClass);
	}
}
