package xdaq.tools.win;

import tcl.lang.*;
import xdaq.tools.win.*;
import org.w3c.dom.*;
import java.io.*;
import java.util.*;
import javax.xml.parsers.*;
import org.xml.sax.*;
import xdaq.tools.win.*;
/**
 * This class implements the "Configure" command in XdaqPackage.
 */

class TclXdaqCommand implements Command {

	xdaqWin win;
	
	public TclXdaqCommand (xdaqWin win) {
		this.win = win;
	}
    
	    //
	// Parameters may be:
	// 	host id
	// 	class class_name instance instance_number
	// 	target id
	// Instead of number or name one can put "any"
	//
    	public void cmdProc(Interp interp, TclObject argv[]) throws TclException {
		String command = argv[0].toString(); // argv[0] is the command name
		
	   if (argv.length < 3) {
			throw new TclException(interp,command + " error: missing arguments.");
		}			
			
		String args="";
		String host="Any";
		String instance="Any";
		String classname="Any";
		String target="Any";
		String transport="Any";
		String usercommand="";
		
		boolean argsSelected = false;
		boolean hostSelected= false;
		boolean instanceSelected= false;
		boolean classnameSelected= false;
		boolean targetSelected= false;
		boolean parametersSelected=false;
		boolean usercommandSelected = false;
		
		try {
			for (int i = 1; i < argv.length; i++) {			
				String arg = argv[i].toString();
				if (arg.equals("args")) {
					i++;
					args = argv[i].toString();
					argsSelected = true;
				}
				else if (arg.equals("host")) {
					i++;
					host = argv[i].toString();
					hostSelected = true;
				}
				else if (arg.equals("instance")) {
					i++;
					instance = argv[i].toString();
					instanceSelected = true;
				}
				else if (arg.equals("class")) {
					i++;
					classname = argv[i].toString();
					classnameSelected = true;
				}
				else if (arg.equals("id")) {
					i++;
					target = argv[i].toString();
					targetSelected = true;
				}
				else if (arg.equals("command")) {
					i++;
					usercommand = argv[i].toString();
					System.out.println ("User command selected: " + usercommand);
					usercommandSelected = true;
				}
				else if (arg.equals("target")) {
					throw new TclException(interp," error: target no longer supported. Use id instead.");
				}
			}
		} catch (java.lang.NullPointerException e) {
			throw new TclException(interp," error: Missing argument after " + argv[argv.length-1]);
		}
		
		if (targetSelected && ( classnameSelected || instanceSelected || hostSelected )) {
	       		throw new TclException(interp," error: invalid argument" + command);
	        } else if ( classnameSelected  && ( targetSelected  || hostSelected )) {
			throw new TclException(interp," error: invalid argument" + command);
		} else if ( instanceSelected  &&  ! classnameSelected ) {
			throw new TclException(interp," error: missing class argument" + command);
		} else if ( hostSelected && ( classnameSelected || instanceSelected || targetSelected )) {
			throw new TclException(interp," error: invalid argument" + command);
	        }  else if (usercommandSelected && !command.equals("UserCommand") ) {
			throw new TclException(interp," error: Missing user command");
		}			
				
		if ( hostSelected ) {
			win.getTree().selectHost ( host, "Any" );
		} else {
			win.getTree().selectApplication ( classname,  instance, target, "Any" );
		}
					
		if ((command.equals ("Configure")||
		command.equals ("Enable")||
		command.equals ("Disable") ||
		command.equals ("Suspend") ||
		command.equals ("Resume") ||
		command.equals ("Halt") || 
		command.equals ("Reset") ||
		command.equals ("Clear"))) {
			try {
				win.executeCommandOnSelection(command, null, win.getPaths() );
			}
			catch (xdaqWinException e) {
				interp.interruptEval();
			}				
		}
		 else if (command.equals ("UserCommand")) {
			// prepare input DOM
		Document document;
			Vector resultVector = null;
			
			if ( args.equals("") ) { // no args then call with null parameters
			if ( usercommandSelected ) {
					command = usercommand;
				    }
		     		    try {
					resultVector = win.executeCommandOnSelection(command, null, win.getPaths() );
				    }
				    catch (xdaqWinException e) {
					interp.interruptEval();
				    }
			} else {				
				document = StringToDOM.dom(args);
				Node node = null;
				if ( document != null ) {
					node = document.getDocumentElement();
				} else {
					throw new TclException(interp," error: malformed xml args");
				}
				// if a user command then override with the provided command	
			if ( usercommandSelected ) {
					command = usercommand;
				    }
					
				    try {
					resultVector = win.executeCommandOnSelection(command, node, win.getPaths() );
				    } catch (xdaqWinException e) {
					interp.interruptEval();
				    }
			}
			
			TclObject resultList = TclList.newInstance();
			for (int i=0; i < resultVector.size(); i++ ) {
				String resultString = fromDOM( (Node)resultVector.elementAt(i));
				TclObject resultElement = TclString.newInstance(resultString);
				TclList.append(interp,resultList,resultElement);
			}
			interp.setResult(resultList);				
		} 	
		else {
			throw new TclException(interp," error: Command not supported. Use UserCommand with args instead.");
		}	
    	}
    
    
    String fromDOM(Node node) {    
    	String xmlString = DOMtoString.serialize ( node );
	return xmlString;
    }
}
