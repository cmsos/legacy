/**
*      @file FMMDeadTimeMonitor.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.15 $
*     $Date: 2007/03/28 12:06:23 $
*
*
**/
#include "tts/fmm/FMMDeadTimeMonitor.hh"
#include "tts/fmm/FMMTimer.hh"

#include "xcept/Exception.h"

#include <iostream>
#include <sstream>


#define CONST_64(hi,lo) ( ( ((uint64_t) hi) << 32 ) | ( (uint64_t) lo ) )

tts::FMMDeadTimeMonitor::FMMDeadTimeMonitor( tts::FMMCard& fmm) 
  : _fmm(fmm), 
    _running(false),
    _t_reset(0), 
    _t_lastread(0), 
    _timetag(0),
    _timestamp(toolbox::TimeVal::gettimeofday()),
    _counters(NumCounters),
    _t_lastread_old(0), 
    _timetag_old(0),
    _t_interval(1),
    _counters_old(NumCounters),
    _update_semaphore(toolbox::BSem::FULL, true) {

};

tts::FMMDeadTimeMonitor::~FMMDeadTimeMonitor() {

};


void tts::FMMDeadTimeMonitor::start( bool resetHW ) 
  throw (HAL::HardwareAccessException) {

  _update_semaphore.take();

  try {
    doReset( resetHW );
    _running = true;
  }
  catch (...) {
    _update_semaphore.give();
    throw;
  }

  _update_semaphore.give();
}

void tts::FMMDeadTimeMonitor::stop() 
  throw (HAL::HardwareAccessException, xcept::Exception) {

  _update_semaphore.take();

  if (_running) {
    try {
      doUpdateCounters();
    } catch(...) {
      _update_semaphore.give();
      throw;
    }
  }
  _running = false;

  _update_semaphore.give();

}

void tts::FMMDeadTimeMonitor::doReset( bool resetHW ) 
  throw (HAL::HardwareAccessException) {

  tts::FMMTimer timer;
  _t_reset = _t_lastread = _t_lastread_old = timer.getMicroTime();

  _timetag = _timetag_old = 0;
  _t_interval = 1;
  for (int i=0; i<NumCounters; i++) {
    _counters[i].tlow  = 0;
    _counters[i].thigh = 0;
    _counters_old[i].tlow  = 0;
    _counters_old[i].thigh = 0;
  }

  if ( resetHW ) { 
    _fmm.resetTimeTag();
    _fmm.resetDeadTimeMonitors();
  }
};


void tts::FMMDeadTimeMonitor::updateCounters() 
  throw (HAL::HardwareAccessException, xcept::Exception) {

  _update_semaphore.take();

  try {
    doUpdateCounters();
  } catch(...) {
    _update_semaphore.give();
    throw;
  }

  _update_semaphore.give();
}


void tts::FMMDeadTimeMonitor::readCounters(std::vector<tts::FMMDeadTime> & counters, uint64_t& timetag, bool doUpdate)
  throw (HAL::HardwareAccessException, xcept::Exception) {
  
  _update_semaphore.take();

  if (doUpdate) {
    try {
      doUpdateCounters();
    } catch(...) {
      _update_semaphore.give();
      throw;
    }
  }

  counters.resize( NumCounters );
  for (int i=0; i<NumCounters; i++)
    counters[i] = _counters[i];

  timetag = _timetag;

  _update_semaphore.give();
}


void tts::FMMDeadTimeMonitor::readDeadTimeFractions( std::vector<double> & deadTimeFractions, bool doUpdate)   
  throw (HAL::HardwareAccessException, xcept::Exception) {

  _update_semaphore.take();

  if (doUpdate) {
    try {
      doUpdateCounters();
    } catch(...) {
      _update_semaphore.give();
      throw;
    }
  }

  uint64_t t_interval;
  if (_timetag != 0) // take from hardware if available
    t_interval = _timetag / 40l;
  else { // FIXME: remove this when all FMMs upgraded to new firmware
    t_interval = _t_lastread - _t_reset;
  }

  if (t_interval == 0) t_interval = 1; // protection if reset was less than 1 microsecond before

  deadTimeFractions.resize( NumCounters );

  for (int i=0; i<NumCounters; i++)
    deadTimeFractions[i] = _counters[i].getSeconds() * 1.e6 / (double) t_interval;

  _update_semaphore.give(); 
}



void tts::FMMDeadTimeMonitor::readDeadTimeFractionsLastInterval( std::vector<double> & deadTimeFractions)   
  throw (HAL::HardwareAccessException, xcept::Exception) {

  _update_semaphore.take();

  deadTimeFractions.resize( NumCounters );

  for (int i=0; i<NumCounters; i++)
    if (_counters[i].getTimeBX() == _counters_old[i].getTimeBX()) // avoid some rounding errors
      deadTimeFractions[i]=0.;
    else
      deadTimeFractions[i] = (_counters[i].getSeconds() - _counters_old[i].getSeconds()) * 1.e6 / (double) _t_interval;

  _update_semaphore.give(); 
}


void tts::FMMDeadTimeMonitor::lock() {
  _update_semaphore.take();
}

void tts::FMMDeadTimeMonitor::unlock() {
  _update_semaphore.give();
}




void tts::FMMDeadTimeMonitor::doUpdateCounters() 
  throw (HAL::HardwareAccessException, xcept::Exception) {

  if (!_running) return;

  tts::FMMTimer timer;

  uint64_t t_before = timer.getMicroTime();


  // save old timetag and counter values

  _timetag_old = _timetag;
  _counters_old = _counters;
  _t_lastread_old = _t_lastread;

  // read timetag and counter values
  std::vector<uint32_t> counterValues;

  uint64_t tt_high =    (_timetag & CONST_64(0xffffff00, 0x00000000) ) >> 40;
  uint64_t timetag_old = _timetag & CONST_64(0x000000ff, 0xffffffff);
  _fmm.readDeadTimeCountersAndTimeTag(counterValues, _timetag);

  if (_timetag < timetag_old) tt_high++;
  _timetag |= tt_high << 40;

  for (uint32_t i=0; i<NumCounters; i++) {

    // check if the counter has wrapped around
    if ( counterValues[i] <  _counters[i].tlow ) {
      _counters[i].thigh++;
    }
    _counters[i].tlow = counterValues[i];
  }

  uint64_t t_after = timer.getMicroTime();
  _timestamp = toolbox::TimeVal::gettimeofday();

  uint64_t t_sincelastread = t_after - _t_lastread;
  _t_lastread = t_before;


  if (_timetag != 0) // take from hardware if available
    _t_interval = (_timetag - _timetag_old) / 40l;
  else { // FIXME: remove this when all FMMs upgraded to new firmware
    _t_interval = _t_lastread - _t_lastread_old;
  }

  if (_t_interval == 0) _t_interval = 1; // protection if reset was less than 1 microsecond before




  if (t_sincelastread > 107000000) {
    std::stringstream msg;

    msg << "FMM dead time counters have to be polled every 107 seconds. Time elapsed since last poll is: " 
	<< ((double) t_sincelastread / 1000000.) << " s."
	<< " hardware counter wraparound may have been undetected." ;

    XCEPT_RAISE( xcept::Exception, msg.str() ); 
  }

}; 

