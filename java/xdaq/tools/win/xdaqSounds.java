package xdaq.tools.win;

import xdaq.tools.win.SoundFile;

public class xdaqSounds
{
	public xdaqSounds()
	{
		menuDown_ = new SoundFile ("./audio/menuDown.wav");
		slide_ = new SoundFile ("./audio/slide.wav");
	}
	
	public void menuDown()
	{
		menuDown_.play();
	}
	
	public void slide()
	{
		slide_.play();
	}
	
	private SoundFile menuDown_;
	private SoundFile slide_;
}
