// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "benchmark/justio/Client.h"

#include "benchmark/justio/i2oStreamIOMsg.h"
#include "toolbox/BSem.h"
#include "toolbox/Task.h"

#include "toolbox/rlist.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "xoap/Method.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/task/WorkLoopFactory.h"

Client::Client (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this)
{

	xgi::bind(this, &Client::start, "start");


	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);



	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");


	process_ = toolbox::task::bind(this, &Client::process, "process");


}

Client::~Client()
{
}

void Client::actionPerformed (xdata::Event& event)
{

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		try
		{
			destination_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("Server");
			int counter = 0;
			servers_ = new xdaq::ApplicationDescriptor*[destination_.size()];
			std::set<const xdaq::ApplicationDescriptor*>::iterator iter;
			for (iter = destination_.begin(); iter != destination_.end(); iter++)
			{
				servers_[counter] = const_cast<xdaq::ApplicationDescriptor*>(*iter);
				counter++;
			}

			LOG4CPLUS_INFO(getApplicationLogger(), "Found " << destination_.size() << " servers");
		}
		catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
			XCEPT_RAISE(xcept::Exception, "No Server application instance found. Client cannot be configured.");
		}


		try
		{
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			toolbox::net::URN urn("toolbox-mem-pool", "justio");
			pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

			toolbox::mem::Allocator* ca = dynamic_cast<toolbox::mem::Allocator *>(pool_->getAllocator());
			if (ca->isCommittedSizeSupported())
			{
				size_t committedSize = ca->getCommittedSize();
				pool_->setHighThreshold((unsigned long) (committedSize * 0.9));
				pool_->setLowThreshold((unsigned long) (committedSize * 0.7));
			}
			else
			{
				LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to set threshold on pool. Pool is not a supported pool type");
			}

		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
			return;
		}


	}
}

void Client::start(xgi::Input *in, xgi::Output *out) throw (xgi::exception::Exception)
{
	try
	{
		std::stringstream wlss;
		wlss << "justio-" << getApplicationDescriptor()->getInstance();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->activate();
		toolbox::task::getWorkLoopFactory()->getWorkLoop(wlss.str(), "waiting")->submit(process_);
	}
	catch ( toolbox::task::exception::Exception& e )
	{
		std::stringstream ss;
		ss << "Failed to submit workloop ";

		std::cerr << ss.str() << std::endl;
	}
	catch ( std::exception& se )
	{
		std::stringstream ss;
		ss << "Failed to submit notification to worker thread, caught standard exception '";
		ss << se.what() << "'";
		std::cerr << ss.str() << std::endl;
	}
	catch ( ... )
	{
		std::cerr << "Failed to submit notification to worker pool, caught unknown exception" << std::endl;
	}
}


bool Client::process ( toolbox::task::WorkLoop* wl )
{
	//LOG4CPLUS_DEBUG(getApplicationLogger(), "starting send loop...");

	for (;;)
	{

			if (!pool_->isHighThresholdExceeded())
			{
				//::sleep(1);
				// Stop if there is an error in sending
				LOG4CPLUS_INFO(getApplicationLogger(), toolbox::toString("going to send one message"));

				if (this->sendMessage() == 1)
				{

					//LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Error in frameSend. Stopping client"));
					//return false;

					LOG4CPLUS_ERROR(getApplicationLogger(), toolbox::toString("Error in frameSend. retry.."));

					continue;
				}

			}
			else
			{

				LOG4CPLUS_DEBUG(getApplicationLogger(), "high threshold is exceeded");
				while (pool_->isLowThresholdExceeded())
				{
					LOG4CPLUS_DEBUG(getApplicationLogger(), "yield till low threshold reached");
					//this->yield(1);
					::sched_yield();
				}
			}

	}

	return true;
}



// Send an I2O token message to all servers
// If last is 1, the flag 'last' in the
// message is set to one. Otherwise it
// is set to 0
//
int Client::sendMessage ()
{

	//char c;
	//std::cout << "enter char to send message of size (" << currentSize_ << "):";
	//std::cin >> c;

	std::set<xdaq::ApplicationDescriptor*>::iterator iter;
	LOG4CPLUS_DEBUG(getApplicationLogger(), "send message on  " << destination_.size() << " destinations");
	for (unsigned int i = 0; i < destination_.size(); i++)
	{


		toolbox::mem::Reference * ref = 0;
		try
		{
			ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, 4096);

			PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation();

			U32 totalSize = 0;


			totalSize = 1024;


			frame->PvtMessageFrame.StdMessageFrame.MsgFlags = 0;
			frame->PvtMessageFrame.StdMessageFrame.VersionOffset = 0;
			//frame->PvtMessageFrame.StdMessageFrame.TargetAddress    = i2o::utils::getAddressMap()->getTid(destination_[i]);
			frame->PvtMessageFrame.StdMessageFrame.TargetAddress = i2o::utils::getAddressMap()->getTid(servers_[i]);
			frame->PvtMessageFrame.StdMessageFrame.InitiatorAddress = i2o::utils::getAddressMap()->getTid(this->getApplicationDescriptor());
			frame->PvtMessageFrame.StdMessageFrame.MessageSize = (totalSize + sizeof(I2O_TOKEN_MESSAGE_FRAME)) >> 2;

			frame->PvtMessageFrame.StdMessageFrame.Function = I2O_PRIVATE_MESSAGE;
			frame->PvtMessageFrame.XFunctionCode = I2O_TOKEN_CODE;
			frame->PvtMessageFrame.OrganizationID = XDAQ_ORGANIZATION_ID;

			ref->setDataSize(frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2);
			LOG4CPLUS_DEBUG(getApplicationLogger(), "sending to tid: " << frame->PvtMessageFrame.StdMessageFrame.TargetAddress);

			//std::cout << "Message size (words)" << frame->PvtMessageFrame.StdMessageFrame.MessageSize  << " Ref size (bytes)" << ref->getDataSize() << std::endl;
			getApplicationContext()->postFrame(ref, this->getApplicationDescriptor(), servers_[i]);

		}
		catch (toolbox::mem::exception::Exception & me)
		{

			LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(me));
			ref->release();
			return 1; // error
		}
		catch (xdaq::exception::Exception & e)
		{

			LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e));
			ref->release();
			return 1;

		}
		catch (...)
		{
		}
	}

	return 0; // o.k.
}
