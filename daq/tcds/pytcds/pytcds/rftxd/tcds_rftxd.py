###############################################################################
## Utilities related to the RFTXD VME board.
###############################################################################

from pytcds.rftxd.tcds_rftxd_constants import ADDRESS_TABLE_FILE_NAME_RFTXD
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_constants import BOARD_TYPE_RFTXD
from pytcds.utils.tcds_vmeboard import TCDSVME
from pytcds.utils.tcds_utils_vme import get_hw_vme

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class RFTXD(TCDSVME):

    @staticmethod
    def get_base_address(slot_number):
        res = (slot_number << 20)
        # End of get_base_address().
        return res

    def __init__(self, board_type, vme_chain, vme_unit, slot_number, nodes, verbose=False):
        base_address = RFTXD.get_base_address(slot_number)
        super(RFTXD, self).__init__(board_type, vme_chain, vme_unit, slot_number, base_address, nodes, verbose)
        self.slot_number = slot_number
        # End of __init__().

    def get_firmware_version(self):
        # NOTE: The decimal (!) printed format of the firmware version
        # spells the firmware date as yyyymmdd.
        tmp_lo = self.read("firmware_id_lo")
        tmp_hi = self.read("firmware_id_hi")
        tmp = (tmp_hi << 16) + tmp_lo;
        res = "{0:d}".format(tmp)
        # End of get_firmware_version()
        return res

    def get_user_system_id(self):
        res = board_names[self.board_type]
        # End of get_system_id().
        return res

    # End of class RFTXD.

###############################################################################

def get_rftxd_hw(vme_chain,
                 vme_unit,
                 vme_slot,
                 verbose=False):

    rftxd = get_hw_vme(BOARD_TYPE_RFTXD,
                       ADDRESS_TABLE_FILE_NAME_RFTXD,
                       vme_slot,
                       vme_chain,
                       vme_unit,
                       verbose,
                       RFTXD)

    return rftxd

###############################################################################
