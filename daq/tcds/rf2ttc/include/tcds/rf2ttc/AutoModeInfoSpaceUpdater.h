#ifndef _tcds_rf2ttc_AutoModeInfoSpaceUpdater_h_
#define _tcds_rf2ttc_AutoModeInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayervme {
    class VMEDeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
  }
}

namespace tcds {
  namespace rf2ttc {

    class AutoModeInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      AutoModeInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::hwlayervme::VMEDeviceBase const& hw);
      virtual ~AutoModeInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayervme::VMEDeviceBase const& getHw() const;

    };

  } // namespace rf2ttc
} // namespace tcds

#endif // _tcds_rf2ttc_AutoModeInfoSpaceUpdater_h_
