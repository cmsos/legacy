#ifndef _bril_dip_Application_h_
#define _bril_dip_Application_h_
#include <string>
#include <map>
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "xdata/InfoSpace.h"
#include "xdata/Boolean.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/String.h"
#include "xdata/Properties.h"
#include "xdata/UnsignedInteger.h"
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/BSem.h"


namespace bril{
  namespace dip{
    class DipAnalyzer;

    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener,public toolbox::task::TimerListener{
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
      // destructor
      ~Application ();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in message callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception);
      // timer callback
      void timeExpired( toolbox::task::TimerEvent& e );
    protected:
      
      std::vector<DipAnalyzer*> m_analyzers;
      xdata::UnsignedInteger m_fillnum;
      xdata::UnsignedInteger m_runnum;
      xdata::UnsignedInteger m_lsnum;
      xdata::UnsignedInteger m_nbnum;
      xdata::String m_busName;
      xdata::String m_dipRoot;
      xdata::String m_dipServiceName;
      xdata::Float m_bkgd1;
      xdata::Float m_bkgd2;
      xdata::Float m_bkgd3;
      xdata::Float m_bkgd4;
      xdata::Float m_bkgd5;
      xdata::Float m_bkgd6;
      xdata::Float m_bkgd7;
      xdata::Float m_bkgd8;
      xdata::Float m_bkgd9;
      unsigned int m_lastreceive_bkgd12_sec;
    private:
      // members
      // some information about this application
      xdata::InfoSpace *m_appInfoSpace;
      xdaq::ApplicationDescriptor *m_appDescriptor;
  
      // configuration parameters
      xdata::String m_signalTopic;
      xdata::Vector<xdata::Properties> m_analyzerconf;

      toolbox::BSem m_applock;
      bool m_dipconnected;
      void createAnalyzer(const std::string& analyzername,unsigned int dipfrequency,
        const std::string& dipServiceName);
      void destroyAnalyzers();

      // nocopy protection
      Application(const Application&);
      Application& operator=(const Application&);
    };
  }
}
#endif
