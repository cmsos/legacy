package xdaq.tools.win;

public class xdaqDOMDocumentException extends Exception
{
	xdaqDOMDocumentException () { super(); }

	xdaqDOMDocumentException ( String msg )
	{
		super (msg);
	}
}
