#ifndef _bril_lumistore_hdf5_ComplexTable_h
#define _bril_lumistore_hdf5_ComplexTable_h
//#include "interface/bril/DataFormat.h"
#include "bril/lumistore/hdf5/Node.h"
#include "hdf5.h"
#include <string>

namespace bril{ namespace lumistore { namespace hdf5{
 class H5RowDef;
 class ComplexTable : public Node{
   public:
   ComplexTable(const std::string& parentpath, hid_t parentid, const std::string& name,File* file,const std::string& dictionary,size_t nrows, bool shrink=false);  
     ~ComplexTable();
     void append(const void* payloadaddress);
     virtual void close();
   private:
     hid_t create_table();
     void flush();
   private:
     H5RowDef* m_rowdef;
     size_t m_nrows;
     bool m_shrink;
     hid_t m_rowtype;
     void* m_wbuf; 
     char* m_current;
     size_t m_rowsinbuffer;
     size_t m_committedrows;
     size_t m_committedtimes;
     size_t m_allocatedrows;
     hid_t m_tableid;
 };

}}}

#endif
