// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_PolicyFactory_h_
#define _toolbox_PolicyFactory_h_

#include <string>
#include <list>
#include <map>
#include "toolbox/net/URN.h"
#include "toolbox/Policy.h"
#include "toolbox/exception/Exception.h"

namespace toolbox
{
	class PolicyFactory
	{
		public:
			//<policy element="WorkLoop" type="thread" package="POSIX" affinity="1" />
			//Policy* getPolicy(const std::string & type, const std::string & package) ;

			//Policy* getPolicy(toolbox::net::URN & urn, const std::string & type, const std::string & package) ;
			Policy* createPolicy(const std::string & regex, const std::string & type, const std::string & package) ;

			Policy* getPolicy(toolbox::net::URN & urn, const std::string & type) ;
			
			void removePolicy (toolbox::Policy * policy) ;

			//void removePolicy(toolbox::net::URN & urn) ;

			std::list<Policy*> getPolicies();
			
			std::list<Policy*> getPolicies(const std::string & type);

			//! Retrieve a pointer to the toolbox::task::WorkLoopFactory singleton
			//
			static PolicyFactory* getInstance();

			//! Destoy the factory and all associated work loops
			//
			static void destroyInstance();

			private:

			static PolicyFactory* instance_;

			std::map<std::string, toolbox::Policy*, std::less<std::string> > policies_;

	};

	//! Retrieve a pointer to the toolbox::task::WorkLoopFactory singleton
	//
	PolicyFactory* getPolicyFactory();
}

#endif
