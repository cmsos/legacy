#ifndef _tcds_hwutilstca_HwIDInfoSpaceUpdaterTCA_h_
#define _tcds_hwutilstca_HwIDInfoSpaceUpdaterTCA_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilstca {

    class HwIDInfoSpaceUpdaterTCA : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwIDInfoSpaceUpdaterTCA(tcds::utils::XDAQAppBase& xdaqApp,
                              tcds::hwlayertca::TCADeviceBase const& hw);
      virtual ~HwIDInfoSpaceUpdaterTCA();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayertca::TCADeviceBase const& getHw() const;

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_HwIDInfoSpaceUpdaterTCA_h_
