#ifndef __FerolHardwareLocker
#define __FerolHardwareLocker
#include "d2s/utils/HardwareLocker.hh"
#include "ferol/ApplicationInfoSpaceHandler.hh"
#include "ferol/StatusInfoSpaceHandler.hh"

namespace ferol
{
    class HardwareLocker : public utils::HardwareLocker {

        public:

            HardwareLocker( Logger &logger,
                    ApplicationInfoSpaceHandler &appIS,
                    StatusInfoSpaceHandler &statusIS );

            bool lock();
    };
}

#endif /* __HardwareLocker */
