package xdaq.tools.benchmarks;

import java.awt.*;
import java.net.*;
import java.security.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import org.w3c.dom.*;
import java.util.*;

import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import org.jfree.data.xy.*;

public class Plot extends JApplet
{
	URL url;

	
	public void init()
	{	
		
		ChartPanel b = new ChartPanel(this.createChart("bandwidth","Bytes", "MB/s"), true, true, true, true, true);
		ChartPanel l = new ChartPanel(this.createChart("latency","Bytes","usecs"), true, true, true, true, true);
		ChartPanel r = new ChartPanel(this.createChart("rate","Bytes","Hz"), true, true, true, true, true);
		
		Box parameterBox = Box.createVerticalBox();
		parameterBox.add(b);
		parameterBox.add(Box.createRigidArea(new Dimension(2,25)));
		parameterBox.add(l);
		parameterBox.add(Box.createRigidArea(new Dimension(2,25)));
		parameterBox.add(r);
		//BufferedImage  image = chart.createBufferedImage(200,200);
		//JLabel plot = new JLabel();
		//plot.setIcon(new ImageIcon(image));	
		this.getContentPane().setBackground(Color.white);					
		this.getContentPane().add (parameterBox, BorderLayout.CENTER);
	
	}
	
	JFreeChart createChart(String name, String xlabel, String ylabel)
	{
		StringTokenizer st = new StringTokenizer(getParameter(name),",");
		XYSeries bandwidth = new XYSeries(name);
		while ( st.hasMoreTokens() ) {			
			bandwidth.add( Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()) );
			
		}

		XYDataset xyDataset = new XYSeriesCollection(bandwidth);

		JFreeChart chart = ChartFactory.createXYLineChart (name,// Title
								   xlabel, // X-Axis label
								   ylabel, // Y-Axis label
								    xyDataset, // Dataset
								    PlotOrientation.VERTICAL,
								    true, // Show legend
 								    true,
								    true	
								);
								
								
		return chart;
	}
	
	

	
	
} // end of class
