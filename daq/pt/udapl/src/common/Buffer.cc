// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pt/udapl/Buffer.h"

pt::udapl::Buffer::Buffer (pt::udapl::dat::MemoryRegion * mr, toolbox::mem::Pool * pool, size_t size, void* address)
	: toolbox::mem::Buffer(pool, size, address), mr_(mr)
{
	// address_ = address;

	// mr is accessed by the allocator (created and destroyed), this is an arbitrary design choice
}

