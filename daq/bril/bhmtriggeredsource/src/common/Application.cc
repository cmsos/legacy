#include <algorithm>
#include <functional>
#include <initializer_list>
#include <numeric>
#include <cmath>

#include "b2in/nub/Method.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xcept/tools.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"
#include "xgi/framework/Method.h"
#include "xgi/framework/UIManager.h"

#include "xoap/Method.h"
#include "xoap/MessageFactory.h"

#include "bril/bhmtriggeredsource/Application.h"
#include "bril/webutils/WebUtils.h"
#include "bril/bhmtriggeredsource/exception/Exception.h"
#include "bril/bhmtriggeredsource/ReadoutCore.h"

/**
 * @note This file must be built in C++0x mode. Make sure to port hacks to real C++14 when compiler support is available (gcc 5.x)
 */

namespace
 {

  struct SafeLocker // Exception safe RAII style mutex
   {
    SafeLocker( toolbox::BSem& lock ) : m_lock( lock ) { m_lock.take(); }
    ~SafeLocker() { m_lock.give(); }
    void unlockedwait( long us ) { m_lock.give(); toolbox::u_sleep( us ); m_lock.take(); }
    toolbox::BSem& m_lock;
   };

  struct RefDestroyer  ////RAII exception safe guard object
   {
    RefDestroyer( toolbox::mem::Reference* ref ) : m_ref( ref ){}
    ~RefDestroyer(){ if ( m_ref ) m_ref->release(); }
    toolbox::mem::Reference* m_ref;
   };

 }

XDAQ_INSTANTIATOR_IMPL( bril::bhmtriggeredsource::Application )

namespace bril
 {

  namespace bhmtriggeredsource
   {

    const int Application::s_nchannels;

/**
 * @brief Constructor
 *
 * The constructor registers all the callbacks, initializes data structures and monitoring parameters.
 */
    Application::Application( xdaq::ApplicationStub* s ) throw ( xdaq::exception::Exception ) :
     xdaq::Application( s ),
     xgi::framework::UIManager( this ),
     eventing::api::Member( this ),
     m_applock( toolbox::BSem::FULL ),
     m_shutdown( false ),
     m_core(0),
     m_peak_spectrum( 0 ),
     m_average_wf( 0 )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      try
       {
        std::string classname = getApplicationDescriptor()->getClassName();
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Initializing application " << classname );
        toolbox::net::URN memurn( "toolbox-mem-pool", "mem-pool_" + getApplicationDescriptor()->getURN() );
        m_pool_factory = toolbox::mem::getMemoryPoolFactory();
        m_mem_pool = m_pool_factory->createPool( memurn, new toolbox::mem::HeapAllocator() );
        m_timer = toolbox::task::getTimerFactory()->createTimer( "PeriodicDiagnostic_" + getApplicationDescriptor()->getURN() );
        setupConfiguration();
        setupMonitoring();
        xgi::framework::deferredbind( this, this, &Application::Default, "Default" );
        b2in::nub::bind( this, &Application::onMessage );
        toolbox::getRuntime()->addShutdownListener( this );
       }
      catch( xcept::Exception& e )
       {
        notifyQualified( "fatal", e );
        throw; //Errors in the constructor should not be recoverable
       }
     }

    Application::~Application()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      delete m_publish_wl;
      if (m_core!=0) delete m_core;
     }

    void Application::setupConfiguration()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      auto infospace = getApplicationInfoSpace();
      infospace->fireItemAvailable( "eventinginput", &m_cfg_input_topics );
      infospace->fireItemAvailable( "eventingoutput", &m_cfg_output_topics );
      infospace->fireItemAvailable( "placeholder", &m_cfg_placeholder );
      infospace->fireItemAvailable( "connectionFile", &m_cfg_connectionFile );
      infospace->fireItemAvailable( "pipelineLength", &m_cfg_pipeline );
      infospace->fireItemAvailable( "samples", &m_cfg_samples );
      infospace->fireItemAvailable( "slots", &m_cfg_slots );
      infospace->fireItemAvailable( "zsThreshold", &m_cfg_zsThreshold );
      m_cfg_coinc_all=31;  infospace->fireItemAvailable( "coincAny",&m_cfg_coinc_all);
      for (int itdc=0; itdc<4; itdc++) {
	m_cfg_coinc_tdc[itdc]=31;  infospace->fireItemAvailable( ::toolbox::toString("coincTDCbin%d",itdc+1),&m_cfg_coinc_tdc[itdc]);
      }
      
      infospace->addListener( this, "urn:xdaq-event:setDefaultValues" );
      //Changing the parameters below will invoke an action, other parameters should NOT be changed at runtime
      //The possibility of changing these online should only be used for testing hardware configurations, not in production
      infospace->addItemChangedListener( "placeholder", this );
     }

    void Application::setupEventing()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      for ( size_t i = 0; i != m_cfg_input_topics.size(); ++i )
       {
        std::string databus = m_cfg_input_topics[i].getProperty( "bus" );
        std::string topics = m_cfg_input_topics[i].getProperty( "topics" );
        auto topicset = toolbox::parseTokenSet( topics, "," );
        for ( auto it = topicset.begin(); it != topicset.end(); ++it )
         {
          getEventingBus( databus ).subscribe( *it );
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Subscribing to " << databus << "/" << *it );
         }
       }
      m_output_topics_map.clear();
      m_unready_buses.clear();
      for ( size_t i = 0; i != m_cfg_output_topics.size(); ++i )
       {
        std::string topic = m_cfg_output_topics[i].getProperty( "topic" );
        std::string buses = m_cfg_output_topics[i].getProperty( "buses" );
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Publishing topic " << topic << " on buses " << buses );
        BusNameSet set = toolbox::parseTokenSet( buses, "," );
        m_output_topics_map[topic] = set;
        m_unready_buses.insert( set.begin(), set.end() );
       }
      for ( auto it = m_unready_buses.begin(); it != m_unready_buses.end(); ++it )
       {
        LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Waiting for bus \"" << *it << "\" to be ready." );
        getEventingBus( *it ).addActionListener( this );
       }
     }

    void Application::setupHardwareConfiguration()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      if (m_core==0) m_core=new ReadoutCore(getApplicationLogger());
      else m_core->reset();
      
      m_core->setup(m_cfg_connectionFile.value_,
		    m_cfg_slots.value_,
		    m_cfg_pipeline.value_,
		    m_cfg_samples.value_,
		    m_cfg_zsThreshold.value_
		    );
      m_core->setupCoincLevel(0,m_cfg_coinc_all.value_);
      for (int itdc=0; itdc<4; itdc++)
        m_core->setupCoincLevel(itdc+1,m_cfg_coinc_tdc[itdc].value_);
      m_core->configure();
     }

    void Application::setupMonitoring()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      std::string monurn = createQualifiedInfoSpace( "bhmtriggeredsourceMon" ).toString();
      m_mon_infospace = xdata::getInfoSpaceFactory()->get( monurn );
      const int nvars = 5;
      const char* names[nvars] = { "fill", "run", "ls", "nb", "timestamp", };
      xdata::Serializable* vars[nvars] = { &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp };
      for ( int i = 0; i != nvars; ++i )
       {
        m_mon_varlist.push_back( names[i] );
        m_mon_vars.push_back( vars[i] );
        m_mon_infospace->fireItemAvailable( names[i], vars[i] );
       }
     }

    void Application::setupWorkloop()
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      m_publish_wl = toolbox::task::getWorkLoopFactory()->getWorkLoop( getApplicationDescriptor()->getURN() + "_publish", "waiting" );
      toolbox::task::ActionSignature* as_publishing = toolbox::task::bind( this, &Application::publishMain, "publish" );
      m_publish_wl->activate();
      m_publish_wl->submit( as_publishing );
     }

    void Application::Default( xgi::Input* in, xgi::Output* out ) throw ( xgi::exception::Exception )
     {
      using namespace cgicc;
      using std::endl;
      cgicc::Cgicc unpacker(in);

      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      *out << h1() << "BHM Triggered Source" << h1() << endl;
      *out << br() << hr() << br() << endl;

      cgicc::form_iterator tool;
      std::string action;
      std::string myurl=getApplicationContext()->getContextDescriptor()->getURL()+"/" +	getApplicationDescriptor()->getURN()+"/Default";
      tool=unpacker.getElement("action");
      if (tool!=(*unpacker).end()) {
	action=(*(*tool));
	if (action=="start") m_core->start();
	if (action=="stop") m_core->stop();
	if (action=="pause") m_core->pause();
	if (action=="resume") m_core->resume();
    LOG4CPLUS_WARN( getApplicationLogger(), __func__ << ' ' << action );

	out->getHTTPResponseHeader().getStatusCode(301);
        out->getHTTPResponseHeader().addHeader("Location",myurl);
	return;
      }
      
      std::string configurl = "/urn:xdaq-application:service=hyperdaq/editProperties?lid=" + webutils::to_string( getApplicationDescriptor()->getLocalId() );
      *out << h3() << "Current configuration -- " << a( "CHANGE" ).set( "href", configurl ) << h3() << br() << endl;

      *out << "Events: " << m_core->events() << "<p>";
      
      *out << "<form method=get action='" << myurl << "'>\n";

      if (m_core->getState()==ReadoutCore::st_Ready) {
	*out << "<h3>READY</h3>";
	*out << "<button name=action type=submit value=start>Start</button>\n";
      }
      if (m_core->getState()==ReadoutCore::st_Active) {
	*out << "<h3>ACTIVE</h3>";
	*out << "<button name=action type=submit value=stop>Stop</button>\n";
	*out << "<button name=action type=submit value=pause>Pause</button>\n";
      }
      if (m_core->getState()==ReadoutCore::st_Paused) {
	*out << "<h3>PAUSED</h3>";
	*out << "<button name=action type=submit value=stop>Stop</button>\n";
	*out << "<button name=action type=submit value=pause>Resume</button>\n";
      }
      *out << "<button name=action type=submit value=reset>Reset</button>\n";

      *out << "</form>";
      
      *out << br() << hr() << endl;
        auto evts = m_core->event_data();
        *out << "<h3>Collected: " << evts.size() << " events</h3>" << endl;

     }

    void Application::actionPerformed( xdata::Event& e )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received xdata event " << e.type() );
      try
       {
        if ( e.type() == "urn:xdaq-event:setDefaultValues" ) //this is used as an initialization signal
         {
          setupEventing();
          setupHardwareConfiguration();
          //start monitoring timer
          toolbox::TimeInterval interval( 30, 0 );
          toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
          m_timer->scheduleAtFixedRate( start, this, interval,  0, "mon_timer" );
         }
        else if ( e.type() == "ItemChangedEvent" )
         {
          std::string item = static_cast<xdata::ItemEvent&>( e ).itemName();
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Parameter " << item << " changed." );
         }
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

    void Application::actionPerformed( toolbox::Event& e )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": received toolbox event " << e.type() );
      try
       {
        if ( e.type() == "eventing::api::BusReadyToPublish" )
         {
          std::string busname = static_cast<eventing::api::Bus*>( e.originator() )->getBusName();
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Bus \"" << busname << "\" is ready." );
          m_unready_buses.erase( busname );
          if ( !m_unready_buses.empty() )
            return; //wait until all buses are ready
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << " All buses ready for publishing" );
          setupWorkloop();
         }
        if ( e.type() == "Shutdown" )
         {
          LOG4CPLUS_INFO( getApplicationLogger(), __func__ << ": Shutting down" );
          m_shutdown = true;
          toolbox::u_sleep( 500000 );
         }
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

/**
 * @brief XDAQ Eventing receiver callback
 *
 * Receives the three types of data that are used by this application: the BHM amplitude and occupancy histograms, and the beam information.
 */
    void Application::onMessage( toolbox::mem::Reference* ref, xdata::Properties& plist ) throw ( b2in::nub::exception::Exception )
     {
      try
       {
        RefDestroyer rd( ref );
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
        std::string action = plist.getProperty( "urn:b2in-eventing:action" );
        if ( action != "notify" )
          return;
        std::string version = plist.getProperty( "DATA_VERSION" );
        if( version.empty() || version != interface::bril::DATA_VERSION )
          XCEPT_RAISE( exception::EventingError, "Received data with missing or wrong version header (" + version + ")." );
        TopicName topic = plist.getProperty( "urn:b2in-eventing:topic" );
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Received data from " << topic );
        if ( topic == "beam" )
         {
          std::string payloaddict = plist.getProperty( "PAYLOAD_DICT" );
          if ( payloaddict.empty() )
            XCEPT_RAISE( exception::EventingError, "Received data with missing dictionary." );
          interface::bril::beamT* incoming = static_cast<interface::bril::beamT*>( ref->getDataLocation() );
          interface::bril::CompoundDataStreamer cds( payloaddict );
          char status[30];
          status[29] = '\0';
          if ( cds.extract_field( status, "status", incoming->payloadanchor ) )
           {
            LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": Beam mode " << status );
            SafeLocker locker( m_applock );
            m_mon_beammode = status;
           }
          else
            XCEPT_RAISE( exception::EventingError, " Could not unpack information from Beam Data." );
         }
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

/**
 * Collects monitoring information and pushes it to the infospace for collection.
 *
 * @note This function executes in a different thread, all write access to member variables should be locked.
 */
    void Application::timeExpired( toolbox::task::TimerEvent& e )
     {
      LOG4CPLUS_INFO( getApplicationLogger(), __func__ );
      if ( e.originator() != m_timer )
        return;
      try
       {
        toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
        m_mon_timestamp = t.sec();
       }
      catch ( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
     }

/**
 * @note This function executes in a different thread, all write access to member variables should be locked.
 *
 * @returns True to continue processing, false otherwise
 */
    bool Application::publishMain( toolbox::task::WorkLoop* wl )
     {
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
      try
       {
       }
      catch( xcept::Exception& e )
       {
        notifyQualified( "error", e );
       }
      toolbox::u_sleep( 100000 );
      return !m_shutdown;
     }

/**
 * @note This function executes in a different thread, all write access to member variables should be locked.
 */
    template <typename T>
    std::pair<toolbox::mem::Reference*, T*> Application::newPub( unsigned int algoid, unsigned int channelid, unsigned int storage_type )
     {
      toolbox::mem::Reference* memref = 0;
      memref = m_pool_factory->getFrame( m_mem_pool, T::maxsize() );
      memref->setDataSize( T::maxsize() );
      T* datum = static_cast<T*>( memref->getDataLocation() );
      datum->setTotalsize( T::maxsize() );
      datum->setResource( interface::bril::DataSource::BHM, algoid, channelid, storage_type );
      LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << " Created message of size " << memref->getDataSize() );
      return std::make_pair( memref, datum );
     }

/**
 * @note This function executes in a different thread, all write access to member variables should be locked.
 */
    void Application::doPublish( const TopicName& topic, toolbox::mem::Reference* memref, const std::string& payloaddict )
     {
      RefDestroyer rd( memref );
      xdata::Properties plist;
      plist.setProperty( "DATA_VERSION", interface::bril::DATA_VERSION );
      if ( !payloaddict.empty() )
        plist.setProperty( "PAYLOAD_DICT", payloaddict );
      if ( m_mon_beammode == "SETUP" || m_mon_beammode == "ABORT" || m_mon_beammode == "BEAM DUMP" || m_mon_beammode == "RAMP DOWN" ||
           m_mon_beammode == "CYCLING" || m_mon_beammode == "RECOVERY" || m_mon_beammode == "NO BEAM" )
        plist.setProperty( "NOSTORE", "1" );
      auto iset = m_output_topics_map.find( topic );
      if ( iset == m_output_topics_map.end() )
        XCEPT_RAISE( exception::EventingError, "Topic \"" + topic + "\" is not in the list of known topics" );
      for ( auto it = iset->second.begin(); it != iset->second.end(); ++it )
       {
        BusName bus = *it;
        LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ << ": publishing to " << bus << '/' << topic << " a message of size " << memref->getDataSize() );
        toolbox::mem::Reference* ref = memref->duplicate(); //Having to do this manually is *awful*, xdaq memory allocation *sucks* hardcore. The dupe reference ends up leaked ofc.
        getEventingBus( bus ).publish( topic, ref, plist );
       } //the last duplicate is cleaned up by the rd.
     }

   }

 }
