#ifndef _tcds_utils_SOAPCmdResetTTCSpyLog_h_
#define _tcds_utils_SOAPCmdResetTTCSpyLog_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdResetTTCSpyLog : public SOAPCmdBase<T>
    {

    public:
      SOAPCmdResetTTCSpyLog(T& controller);

      /**
       * Resets (i.e., empties) the TTCSpy log. Bound to the SOAP
       * command ResetTTCSpyLog.
       * - SOAP command parameters: none.
       * - SOAP command return value: none.
       */
      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdResetTTCSpyLog.hxx"

#endif // _tcds_utils_SOAPCmdResetTTCSpyLog_h_
