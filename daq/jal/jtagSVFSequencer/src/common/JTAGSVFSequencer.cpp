#include "jal/jtagSVFSequencer/JTAGSVFSequencer.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommand.h"

#include "jal/jtagSVFSequencer/JTAGSVFCommandFrequency.h"

#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagChain/JTAGState.h"

#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagSVFSequencer/JTAGSVFChain.h"
#include "jal/jtagSVFSequencer/JTAGSVFData.h"

#include "jal/jtagSVFSequencer/JTAGSVFFileReader.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommand.h"
#include "jal/jtagSVFSequencer/JTAGSVFCommandCreator.h"

using namespace std;


bool jal::JTAGSVFSequencer::loadSVFFile(const char *fn, double default_frequency) 
  throw (jal::SVFSyntaxException,
	 jal::FileOpenException) {

  jal::JTAGSVFFileReader reader(fn);
  jal::JTAGSVFCommandCreator fab;

  if (default_frequency != -1.)
    _cmds.push_back ( (jal::JTAGSVFCommand*) new jal::JTAGSVFCommandFrequency(default_frequency) );

  vector<string> args;
  while ( reader.nextCommand( args ) ) {

    jal::JTAGSVFCommand* cmd = fab.create( args );

    if (cmd != 0) {
      _cmds.push_back ( cmd );
    }
  }
  _cmds.push_back ( (jal::JTAGSVFCommand*) new jal::JTAGSVFCommandFrequency(-1.) );

  cout << "commands loaded = " << _cmds.size() << endl;

  return true;
}

jal::JTAGSVFSequencer::~JTAGSVFSequencer() {

  vector<jal::JTAGSVFCommand *>::const_iterator it = _cmds.begin();

  for (;it != _cmds.end(); it++)
    delete (*it);
}

bool jal::JTAGSVFSequencer::execute(JTAGChain& ch) 
  throw (jal::HardwareException,
	 jal::TimeoutException,
	 jal::OutOfRangeException,
	 jal::SVFSyntaxException) {

  _progress = 0;
  jal::JTAGSVFChain svf_ch(ch);
  svf_ch.lock();
  svf_ch.resetTAP();

  bool ok = true;
  vector<jal::JTAGSVFCommand *>::const_iterator it = _cmds.begin();

  cout << "going to execute " << dec << _cmds.size() << " commands" << endl;
  int i = 0;

  for (;it != _cmds.end(); it++) {
    uint32_t current_progress = (uint32_t) (  (100l*(uint32_t)(i+1)) /_cmds.size()  );
    if (i==0 || current_progress != _progress) {
      _progress = current_progress;
      cout << "\rProgress = " << dec << setfill(' ') << setw(3) << _progress << " %" << flush;
    }
    i++;
    if ( (*it)->execute(svf_ch) == false) {
      ok=false;
      cout << "!!! error executing command at command nr " << i << endl;
      cout << "command that caused the error was: " << *(*it) << endl; 
      if (!_override_error) break;
    }    
  }
  cout << endl;
  svf_ch.unlock();
  return ok;
}

bool jal::JTAGSVFSequencer::executeParallel(std::vector<jal::JTAGChain*> chains)  // leave this original implementation for backwards compatibility
  throw (jal::HardwareException,
   jal::TimeoutException,
   jal::OutOfRangeException,
   jal::SVFSyntaxException) {


  if (chains.size() < 1) 
    XCEPT_RAISE(jal::OutOfRangeException, "chains argument has to contain at least one chain.");

  _progress = 0;

  vector<jal::JTAGSVFChain*> svf_chains;

  for (vector<jal::JTAGChain*>::iterator ch_it = chains.begin(); ch_it != chains.end(); ch_it++) {
    jal::JTAGSVFChain* svf_ch = new jal::JTAGSVFChain( *(*ch_it) );
    svf_ch->lock();
    svf_ch->resetTAP();
    svf_chains.push_back( svf_ch );
  }

  bool ok = true;
  vector<jal::JTAGSVFCommand *>::const_iterator it = _cmds.begin();

  cout << "going to execute " << dec << _cmds.size() << " commands" << endl;
  int i = 0;

  for (;it != _cmds.end(); it++) {
    uint32_t current_progress = (uint32_t) (  (100l*(uint32_t)(i+1)) /_cmds.size()  );
    if (i==0 || current_progress != _progress) {
      _progress = current_progress;
      cout << "\rProgress = " << dec << setfill(' ') << setw(3) << _progress << " %" << flush;
    }
    i++;

    // execute PRE stage on all chains
    int ich=0;
    for (vector<jal::JTAGSVFChain*>::iterator svfch_it = svf_chains.begin(); svfch_it != svf_chains.end(); svfch_it++, ich++) {
      
      if ( (*it)->executeStage( *(*svfch_it), jal::CMDSTAGE_PRE ) == false ) {
  ok=false;
  cout << "!!! error executing command stage PRE at command nr " << i << " on device number " << ich << " (counting from 0)" << endl;
  cout << "command that caused the error was: " << *(*it) << endl; 
      };

    }   
    if (!ok && !_override_error) break;
    
    // execute PAUSE stage only on first chain

    if ( (*it)->executeStage( *(svf_chains[0]), jal::CMDSTAGE_PAUSE ) == false ) {
  ok=false;
  cout << "!!! error executing command stage PAUSE at command nr " << i << " (thsi stage only executed on device number 0)" << endl;
  cout << "command that caused the error was: " << *(*it) << endl; 
  if (!_override_error) break;
    }

    // execute POST stage on all chains
    ich=0;
    for (vector<jal::JTAGSVFChain*>::iterator svfch_it = svf_chains.begin(); svfch_it != svf_chains.end(); svfch_it++, ich++) {
      
      if ( (*it)->executeStage( *(*svfch_it), jal::CMDSTAGE_POST ) == false ) {
  ok=false;
  cout << "!!! error executing command stage POST at command nr " << i  << " on device number " << ich << " (counting from 0)" << endl;
  cout << "command that caused the error was: " << *(*it) << endl; 
      };

    }   
    if (!ok && !_override_error) break;
  }

  cout << endl;


  for (vector<jal::JTAGSVFChain*>::iterator svfch_it = svf_chains.begin(); svfch_it != svf_chains.end(); svfch_it++) {
    (*svfch_it)->unlock();
    delete *svfch_it;
  }

  return ok;
}



bool jal::JTAGSVFSequencer::anyOk(vector<bool> v) const {
  bool anyOk = false;
  for (vector<bool>::iterator v_it = v.begin(); v_it != v.end(); v_it++)
    if ( (*v_it)==true )
      anyOk = true;
  return anyOk; 
}

std::vector<bool> jal::JTAGSVFSequencer::executeParallelNoStopOnError(std::vector<jal::JTAGChain*> chains) // new implementation that continues on remaining chains in case of errors
  throw (jal::HardwareException,
	 jal::TimeoutException,
	 jal::OutOfRangeException,
	 jal::SVFSyntaxException) {


  if (chains.size() < 1) 
    XCEPT_RAISE(jal::OutOfRangeException, "chains argument has to contain at least one chain.");

  _progress = 0;

  vector<jal::JTAGSVFChain*> svf_chains;
  vector<bool> chain_ok;

    for (vector<jal::JTAGChain*>::iterator ch_it = chains.begin(); ch_it != chains.end(); ch_it++) {
    jal::JTAGSVFChain* svf_ch = new jal::JTAGSVFChain( *(*ch_it) );
    svf_ch->lock();
    svf_ch->resetTAP();
    svf_chains.push_back( svf_ch );
    chain_ok.push_back( true );
  }

  bool ok = true;
  vector<jal::JTAGSVFCommand *>::const_iterator it = _cmds.begin();

  cout << "going to execute " << dec << _cmds.size() << " commands" << endl;
  int i = 0;

  for (;it != _cmds.end(); it++) {
    uint32_t current_progress = (uint32_t) (  (100l*(uint32_t)(i+1)) /_cmds.size()  );
    if (i==0 || current_progress != _progress) {
      _progress = current_progress;
      cout << "\rProgress = " << dec << setfill(' ') << setw(3) << _progress << " %" << flush;
    }
    i++;

    // execute PRE stage on all chains
    int ich=0;
    for (vector<jal::JTAGSVFChain*>::iterator svfch_it = svf_chains.begin(); svfch_it != svf_chains.end(); svfch_it++, ich++) {
      
      if (chain_ok[ich] || _override_error) {
        if ( (*it)->executeStage( *(*svfch_it), jal::CMDSTAGE_PRE ) == false ) {
          chain_ok[ich]=false;
          ok=anyOk(chain_ok);
          cout << "!!! error executing command stage PRE at command nr " << i << " on device number " << ich << " (counting from 0)" << endl;
          cout << "command that caused the error was: " << *(*it) << endl; 
          cout << "STOPPING SVF sequence on device " << ich << endl;
          if (ok) {
            cout << "CONTINUING SVF sequence on devices ";
            int j = 0;
            for (vector<bool>::iterator v_it = chain_ok.begin(); v_it != chain_ok.end(); v_it++, j++)
              if ( (*v_it)==true )
                cout << j << "; ";
            cout << endl;  
          }
          else 
            cout << "ABORTING SVF SEQUENCE since all devices failed." << endl;
        }
      }
    }   
    if (!ok && !_override_error) break;
    
    // execute PAUSE stage only on first chain

    if ( (*it)->executeStage( *(svf_chains[0]), jal::CMDSTAGE_PAUSE ) == false ) {
      // the pause stage should never fail; therfore assume that we can continue using device 0 even if it has failed once
      chain_ok[0]=false;
      ok=anyOk(chain_ok);
      cout << "!!! error executing command stage PAUSE at command nr " << i << " (this stage only executed on device number 0)" << endl;
      cout << "command that caused the error was: " << *(*it) << endl; 
    }
    if (!ok && !_override_error) break;

    // execute POST stage on all chains
    ich=0;
    for (vector<jal::JTAGSVFChain*>::iterator svfch_it = svf_chains.begin(); svfch_it != svf_chains.end(); svfch_it++, ich++) {
      
      if (chain_ok[ich] || _override_error) {
        if ( (*it)->executeStage( *(*svfch_it), jal::CMDSTAGE_POST ) == false ) {
          chain_ok[ich]=false;
          ok=anyOk(chain_ok);
          cout << "!!! error executing command stage POST at command nr " << i  << " on device number " << ich << " (counting from 0)" << endl;
          cout << "command that caused the error was: " << *(*it) << endl; 
          cout << "STOPPING SVF sequence on device " << ich << endl;
          if (ok) {
            cout << "CONTINUING SVF sequence on devices ";
            int j = 0;
            for (vector<bool>::iterator v_it = chain_ok.begin(); v_it != chain_ok.end(); v_it++, j++) 
              if ( (*v_it)==true )
                cout << j << "; ";
            cout << endl;  
          }
          else 
            cout << "ABORTING SVF SEQUENCE since all devices failed." << endl;
        }
      }
    }   
    if (!ok && !_override_error) break;
  }

  cout << endl;


  for (vector<jal::JTAGSVFChain*>::iterator svfch_it = svf_chains.begin(); svfch_it != svf_chains.end(); svfch_it++) {
    (*svfch_it)->unlock();
    delete *svfch_it;
  }

  return chain_ok;
}


bool jal::JTAGSVFSequencer::execute_from_file(char *fn, JTAGChain& ch) const 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException,
	jal::SVFSyntaxException,
	jal::FileOpenException) {

  jal::JTAGSVFFileReader reader(fn);
  jal::JTAGSVFCommandCreator fab;
  
  jal::JTAGSVFChain svf_ch(ch);
  svf_ch.lock();
  svf_ch.resetTAP();

  bool ok=true;
  vector<string> args;
  while ( reader.nextCommand( args ) ) {

    jal::JTAGSVFCommand* cmd = fab.create( args );

    if (cmd != 0) {
      if (cmd->execute(svf_ch) == false) {
	cout << "!!! error executing command." << endl;
	ok=false;
      }
      delete cmd;
      if ( (!ok) && (!_override_error) ) break;
    }
  }
  
  // reset frequency
  jal::JTAGSVFCommand* cmd = (jal::JTAGSVFCommand*) new jal::JTAGSVFCommandFrequency(-1.);
  cmd->execute(svf_ch);
  delete cmd;

  svf_ch.unlock();
  return ok;
}



void jal::JTAGSVFSequencer::print() const {

  cout << "number of commands = " << dec << _cmds.size() << endl;
  vector<jal::JTAGSVFCommand *>::const_iterator it = _cmds.begin();

  for (;it != _cmds.end(); it++) {

    cout << *(*it) << endl;

  }
}


