<!DOCTYPE html>
<html lang="en">
<head>
<title>Smarthub5</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- jQuery -->
<script src="../utils/js/jquery-3.2.1.min.js"></script>

<!-- Bootstrap -->
<link rel="stylesheet" href="../utils/bootstrap-3.3.7-dist/css/bootstrap.min.css">
<script src="../utils/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

<style>
.navbar{
	margin-bottom:2px;
}
.navbar-nav > li > a, .navbar-brand {
	padding-top:5px !important; padding-bottom:0px !important;
	height: 30px;
}
.navbar {min-height:30px !important;}

.fullview {
	height: 100%;
}

html, body {
	height: 100%;
}

.container-fluid {
	height: 100%;
	overflow-y: scroll; /* don't show content that exceeds my height */
}
</style>

</head>

<body>

	<div class="container-fluid fullview">
		<nav class="navbar navbar-default navbar-xs">
			<div class="navbar-header" align="left">
				<a class="navbar-brand" href="#">
						<div align="bottom">
							<img alt="Smarthub5" src="images/smarthub_32.png" height="24px"> <img alt="Smarthub5">
						</div>
					</a>
			</div>
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" id="zonename" style="color: green;"></a></li>
				</ul>
			</div>
		</nav>

<?php
include_once ('config.php');
include_once ('smarthub.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );

echo '<table class="table table-striped">';
echo '<thead>';
echo '<tr>';
echo '<th>Zone</th>';
//echo '<th>Speedial</th>';
echo '<th>Speedial5</th>';
echo '<th>Hotspot5</th>';
echo '</tr>';
echo '</thead>';

echo '<tbody>';

$zones = $zones_;

$dynamiczones = findAllZones($smarthuburl_);

if ( is_array($dynamiczones) )
{
	$zones = array_merge($zones_, $dynamiczones ); // find all zones dynamically
}

foreach ( $zones as $key => $val ) {
	$zonename = $key;
	echo '<tr>';
	echo '<td>';
	echo $zonename;
	echo '</td>';
//	echo '<td>';
//	echo '<a href="' . 'https://xdaq.web.cern.ch/xdaq/xmas/14/speedial/speedial.swf?heartbeatsurl=' . $val . '&zonename=' . $zonename . '">' . 'Go to Speedial</a>';
//	echo '</td>';
	echo '<td>';
	echo '<a href="' . '../speedial5/speedial5.php?zone='. $zonename . '&heartbeaturl=' . $val . '">' . 'Go to Speedial5</a>';
	echo '</td>';
	echo '<td>';
	$arcs = searchARCServices ( $val );
	if (isset ( $arcs ["table"] ['rows'] ) && count ( $arcs ["table"] ['rows'] ) > 0) {
		foreach ( $arcs ['table'] ['rows'] as $row ) {
			$context = $row ['context'];
			$lid = $row ['id'];
			$clasname = $row ['class'];
			$age = floatval ( $row ['age'] );
			
			$arcurl = $context . "/" . "urn:xdaq-application:lid=" . $lid;
			
			if ($age < 1.0) {
				echo '<a href="' . '../hotspot5/hotspot5.php?zone=' . $zonename . '&arcurl=' . urlencode ( $arcurl ) . '">' . 'Go to Hotspot5</a>';
			} else {
				echo 'Not ready';
			}
		}
	} else {
		echo 'Not available';
	}
	echo '</td>';
	echo '</tr>';
}
echo '</tbody>';
echo '</table>';

?>

</div>

</body>
</html>
