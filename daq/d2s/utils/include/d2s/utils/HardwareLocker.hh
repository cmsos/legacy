#ifndef __HardwareLocker
#define __HardwareLocker
#include "tts/ipcutils/SemaphoreArray.hh"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "log4cplus/logger.h"

namespace utils
{

    class Locker {

        public:

            Locker() ; //JRF this constructor is only called from the more specific derived class HardwareLocker() 
                                
            Locker(  const char * lockfile,
                    const char lockprojectid,
                    uint32_t slotUnit); //JRC this constructor is called directly from the Python wrapper for use in scripts. 

            bool lock();
            bool unlock();
            bool lockedByUs();
            virtual std::string updateLockStatus();

        private:
            virtual uint32_t getSlotUnit();

        protected:

            ipcutils::SemaphoreArray *hwLock_P;

            std::string lockStatus_;

            uint32_t slotunit_;

    };



    class HardwareLocker : public Locker {

        public:

            HardwareLocker( Logger &logger,
                    InfoSpaceHandler &appIS,
                    InfoSpaceHandler &statusIS,
                    const char *,
                    const char);
       
            virtual std::string updateLockStatus();
        private:

            virtual uint32_t getSlotUnit();


        protected:

            Logger logger_;
            utils::InfoSpaceHandler &appIS_;
            utils::InfoSpaceHandler &statusIS_;

    };
}

#endif /* __HardwareLocker */
