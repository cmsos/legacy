#include "tcds/apve/HwStatusInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::apve::HwStatusInfoSpaceHandler::HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                               tcds::utils::InfoSpaceUpdater* updater) :
  HwStatusInfoSpaceHandlerTCA(xdaqApp, "tcds-hw-status-apve", updater)
{
}

tcds::apve::HwStatusInfoSpaceHandler::~HwStatusInfoSpaceHandler()
{
}
