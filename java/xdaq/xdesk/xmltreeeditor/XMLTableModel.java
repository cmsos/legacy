package xmltreeeditor;

import java.util.*;

import javax.swing.*;
import javax.swing.table.*;

public class XMLTableModel
    extends AbstractTableModel {
  short nodeType;
  Vector data = null;
  Vector names = null;
  XMLTreeNodeUserObject n_Object = null;
  XMLTree xml_tree;

  public XMLTableModel(XMLTreeNodeUserObject nodeUserObject) {

    nodeType = nodeUserObject.getType();
    n_Object = nodeUserObject;
    initializeDataVector();
  }

  private void initializeDataVector() {
    data = new Vector();

    switch (nodeType) {
      case 1:
        data.addElement( (ImageIcon) n_Object.getIcon());
        data.addElement(new EditableText(n_Object.getName()));
        if (n_Object.getAttrList() != "") {
          data.addElement(new NotEditableText(n_Object.getAttrList()));
        }
        break;
      case 2:

      case 7:
        data.addElement( (ImageIcon) n_Object.getIcon());
        data.addElement(new EditableText(n_Object.getName()));
        data.addElement(new EditableText(n_Object.getValue()));
        break;

      case 3:
        data.addElement(new IconWithText(n_Object.getIcon(), "#text"));
        data.addElement(new EditableText(n_Object.getValue()));
        break;
      case 4:
        data.addElement(new IconWithText(n_Object.getIcon(), "#cdata"));
        data.addElement(new EditableText(new String(n_Object.
            getValue())));
        break;

      case 8:
        data.addElement(new IconWithText(n_Object.getIcon(), "#comment"));
        data.addElement(new EditableText(n_Object.getValue()));
        break;
    }

  }

  public short get_NodeType() {
    return nodeType;
  }

  public Class getColumnClass(int col) {
    return data.elementAt(col).getClass();
  }

  public int getRowCount() {
    return data == null ? 0 : 1;
  }

  public int getColumnCount() {
    return data == null ? 0 : data.size();
  }

  public Object getValueAt(int row, int column) {
    return data.elementAt(column);
  }

  public void setValueAt(Object value, int row, int col) {

    if (value instanceof EditableText) {
      data.setElementAt(value, col);
      fireTableCellUpdated(row, col);
    }
  }

  public boolean isCellEditable(int rowIndex, int columnIndex) {

    return ( (data.elementAt(columnIndex)instanceof EditableText)) ? true : false;

  }

}
