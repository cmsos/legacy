package xdaq.tools.dstorebrowser;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.event.*;

import javax.xml.parsers.*;


import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;


import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;

import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.xdaqSOAPException;

public class ConnectListener implements ActionListener
{
    dstorebrowser mainWindow;

    ConnectListener (dstorebrowser mainWindow_)
	{
	    mainWindow =  mainWindow_;
	}

    public void actionPerformed (ActionEvent e)
	{
	    String buttonName = e.getActionCommand();

	    if (buttonName.startsWith("Commit")) mainWindow.writeTree();
	    else
	    {
		mainWindow.setConnectionParameters();
		SOAPMessage retVal = mainWindow.requestData(null,"");
		if (retVal == null)
		{
		    System.out.println ("Error. getData failed.");
		}
	    }
	} 


};
