// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield							 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_Address_h
#define _tcpla_Address_h

#include <map>

#include "pt/Address.h"

#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/Properties.h"

#include "tcpla/exception/Exception.h"

namespace tcpla
{
	class Address : public pt::Address, public toolbox::Properties
	{
		public:

			//! Create address from url
			Address (std::map<std::string, std::string, std::less<std::string> >& address);

			virtual ~Address ();

			//struct sockaddr_in getSocketAddress ();

			//std::string getIPAddress ();

			//! Get the URL protocol, e.g. ptdapl
			std::string getProtocol ();

			//! Get the URL service if it exists, e.g. i2o
			std::string getService ();

			//! Get additional parameters at the end of the URL
			std::string getServiceParameters ();

			//! Return the URL in string form
			std::string toString ();

			std::string toURL ();

			std::string getHost ();

			std::string getConfigurePort ();

			//! Compare with another address
			bool equals (pt::Address::Reference address);
			std::map<std::string, std::string> getAffinity ();
	};
}

#endif
