#include "tcds/hwutilstca/Utils.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"

void
tcds::hwutilstca::tcaDeviceHwConnectImpl(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                         tcds::hwlayertca::TCADeviceBase& hw)
{
  std::string const connectionsFileName = cfgInfoSpace.getString("ipbusConnectionsFile");
  std::string const connectionName = cfgInfoSpace.getString("ipbusConnection");

  hw.hwConnect(connectionsFileName, connectionName);
}

std::string
tcds::hwutilstca::cyclicGenModeToString(tcds::definitions::CYCLIC_GEN_MODE const mode)
{
  std::string res = "unknown";

  switch (mode)
    {
    case tcds::definitions::CYCLIC_GEN_OFF:
      res = "off";
      break;
    case tcds::definitions::CYCLIC_GEN_MISCONFIGURED:
      res = "misconfigured";
      break;
    case tcds::definitions::CYCLIC_GEN_L1A:
      res = "fire L1A";
      break;
    case tcds::definitions::CYCLIC_GEN_BGO:
      res = "fire B-go";
      break;
    case tcds::definitions::CYCLIC_GEN_SEQUENCE:
      res = "fire sequence";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid cyclic generator operating mode.",
                                    mode));
      break;
    }
  return res;
}

std::string
tcds::hwutilstca::cyclicGenSyncModeToString(tcds::definitions::CYCLIC_GEN_SYNC_MODE const mode)
{
  std::string res = "unknown";

  switch (mode)
    {
    case tcds::definitions::CYCLIC_GEN_SYNC_FREE:
      res = "free-running";
      break;
    case tcds::definitions::CYCLIC_GEN_SYNC_BGO:
      res = "aligned to B-go";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid cyclic generator synchronisation mode.",
                                    mode));
      break;
    }
  return res;
}

std::string
tcds::hwutilstca::cyclicGenPauseBehaviourToString(tcds::definitions::CYCLIC_GEN_PAUSE_BEHAVIOUR const mode)
{
  std::string res = "unknown";

  switch (mode)
    {
    case tcds::definitions::CYCLIC_GEN_PAUSE_FREEZE:
      res = "freeze generator";
      break;
    case tcds::definitions::CYCLIC_GEN_PAUSE_CANCEL:
      res = "cancel output";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid cyclic generator pause behaviour.",
                                    mode));
      break;
    }
  return res;
}
