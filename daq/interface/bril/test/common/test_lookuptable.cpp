#include "bril/BCM1FTopics.hh"
#include <iostream>
/**
 * unit tests of some helpers defined in the interface header files
 */
using namespace interface::bril;
int main(int argc, char** argv){
  std::cout<<IdByName(DataSource::lookuptable,"DIP")<<std::endl;
  std::cout<<IdByName(BCM1FHistAlgos::lookuptable,"OC")<<std::endl;
  std::cout<<NameById(BCM1FHistAlgos::lookuptable,1)<<std::endl;
  std::cout<<NameById(BCM1FHistAlgos::lookuptable,2)<<std::endl;
  std::cout<<StorageType::UINT32<<std::endl;
}
