package
{
	public class MultiRegex
	{
		// comma separated list of regexp 
		public function MultiRegex(patternList:String)
		{
			var t:Array = patternList.split(",");
			for (var i:int = 0; i < t.length; i++) 
			{
				patternList_.push(new RegExp(t[i],"i"))
			}	
		}
		
		public function  test(text:String):Boolean {
			for (var i:int = 0; i < patternList_.length; i++) 
			{
				if ( ! patternList_[i].test(text) )
					return false;
			}	
			return true;
		}
		
		private var patternList_:Array= new Array;
	}
}