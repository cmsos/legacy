/*
 * XDAQMessageException
 *
 * $Id: XDAQMessageException.java,v 4.2 2007/11/13 10:52:27 aohcms Exp $
 */

package net.hep.cms.xdaqctl;

/**
 * Exception caused by XDAQMessage
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XDAQMessageException extends XDAQException
{
	public XDAQMessageException() {}
	public XDAQMessageException(String message) { super(message); }
	public XDAQMessageException(Throwable cause) { super(cause); }
	public XDAQMessageException(String message, Throwable cause) {
		super(message, cause);
	}
}

// End of file
// vim: set ts=4 sw=4:
