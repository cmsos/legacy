#include <sstream>
#include <vector>
#include "amc13controller/AMC13DataSource.hh"
#include "d2s/utils/Exception.hh"
#include "amc13controller/amc13Constants.h"
#include "d2s/utils/loggerMacros.h"

amc13controller::AMC13DataSource::AMC13DataSource(HAL::HardwareDeviceInterface *device_P,
					      utils::InfoSpaceHandler &appIS,
					      Logger logger)
    : OpticalDataSource(device_P, appIS, logger)
{
}

void amc13controller::AMC13DataSource::setDataSource() const
{
    //JRF Loop over all streams
    uint32_t stream_offset(0), index(0);
    for (std::vector<bool>::const_iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    {
	index = streams_it - streams_.begin();
	stream_offset = (index)*0x4000;

	if (dataSource_[index] == GENERATOR_SOURCE) //JRF TODO do we want to be able to mix modes? i.e. use one stream in generator and one in real? If yes, then we need a vector of dataSources_
	{
	    //JRF TODO put this in a loop. We need to have info about which streams are enabled and which are not.
	    device_P->setBit("select_emulator", HAL::HAL_NO_VERIFY, stream_offset);

	    DEBUG("Setting select_emulator to Emulator for AMC13-Generator source operation");
	}
	else if ((dataSource_[index] == L10G_SOURCE) || (dataSource_[index] == L10G_CORE_GENERATOR_SOURCE))
	{
	    device_P->resetBit("select_emulator", HAL::HAL_NO_VERIFY, stream_offset);
	    DEBUG("Setting select_emulator to 0 for 10G-input operation");

	    this->setup10GInput(index); //this method this also resyncs i.e. cleans the input fifos.
	}
	else
	{
	    std::stringstream msg;
	    msg << "Encountered illegal DataSource for operationMode \""
		<< operationMode_
		<< "\" : \""
		<< dataSource_[index]
		<< "\". Cannot continue!";
	    ERROR(msg);
	    XCEPT_RAISE(utils::exception::AMC13Exception, msg.str());
	}
    } //end for loop
}

amc13controller::AMC13DataSource::~AMC13DataSource()
{
}
