/**
*      @file GTPeWebInterface.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.18 $
*     $Date: 2008/05/26 09:43:48 $
*
*
**/
#include "d2s/gtpecontroller/GTPeWebInterface.hh"

#include "log4cplus/helpers/sleep.h"

// for web callback binding and HTML formatting
#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "cgicc/HTMLClasses.h"

// to access URL and URN of owner
#include "xdaq/Application.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ContextDescriptor.h"

#include "d2s/gtpecontroller/GTPeMonitor.hh"

// for access to GTPe Hardware
#include "d2s/gtpe/GTPeCard.hh"
#include "d2s/gtpe/GTPeCrate.hh"
#include "d2s/gtpe/GTPeStatus.hh"
#include "tts/ttsbase/TTSState.hh"


#include "toolbox/string.h"

#include <stdint.h>
#include <time.h>
#include <fstream>

d2s::GTPeWebInterface::GTPeWebInterface(xdaq::Application* owner,
					d2s::GTPeParameters& config,
					xdata::UnsignedShort& daqPartitionId,
					d2s::GTPeCrate* gtpecrate, 
					d2s::GTPeMonitor* gtpeM,
					d2s::GTPeMonitor* gtpePartitionM,
					log4cplus::Logger& logger) 
  : xdaq::Object(owner),
    _config(config), 
    _daqPartitionId(daqPartitionId),
    _gtpecrate(gtpecrate), 
    _gtpeM(gtpeM),
    _gtpePartitionM(gtpePartitionM),
    _logger(logger),
    _autoupdate(false),
    _resyncCount(0){
}	

d2s::GTPeWebInterface::~GTPeWebInterface() {
}

void d2s::GTPeWebInterface::displayDefaultWebPage(xgi::Input * in, xgi::Output * out, std::string const& statename ) 
   {


#ifdef GTPE_WITHMONITOR
  if (_gtpeM)
    _gtpeM->generateHistoryPlot("/tmp/x.gif");
#endif

  /// check if we have parameters
  bool formUsed = false;
  try {
    cgicc::Cgicc cgi(in);    
    if ( xgi::Utils::hasFormElement(cgi,"autoUpd") ) {
      formUsed = true;
      _autoupdate = xgi::Utils::getFormElement(cgi, "autoUpd")->getIntegerValue()  != 0;
    }   

    if ( xgi::Utils::hasFormElement(cgi,"resyncDAQPart") ) {
      formUsed = true;
      int daqPartIdx = xgi::Utils::getFormElement(cgi, "resyncDAQPart")->getIntegerValue();
      // do the resync
      if (daqPartIdx == _config.daqPartitionId.value_) {
	uint32_t rateBitsBeforePause = _gtpecrate->getGTPe().readbackRate( _config.daqPartitionId.value_ );
	_gtpecrate->getGTPe().setPartitionDef( _config.daqPartitionId.value_, 0, 0.);
	log4cplus::helpers::sleepmillis(1000);    
	_gtpecrate->getGTPe().resyncDetPartitions( _config.detPartitionEnableMask.value_ );
	_gtpecrate->getGTPe().setPartitionDef(  _config.daqPartitionId.value_, _config.detPartitionEnableMask, rateBitsBeforePause);
	++_resyncCount;
      }
    }
    
    // check rate updates
    for (int i=0;i<7;i++) {

      if ( xgi::Utils::hasFormElement(cgi,"rate"+toolbox::toString("%d",i)) ) {
	formUsed = true;
	double rate = xgi::Utils::getFormElement(cgi, "rate"+toolbox::toString("%d",i))->getDoubleValue();
	
	// update rate
	_gtpecrate->getGTPe().setPartitionDef(i, _gtpecrate->getGTPe().readbackSelectedPartitions(i) ,rate);	
      }
    }


  }
  catch (const std::exception & e) {
    // don't care if it did not work ...
  }

  try {
    if (formUsed) {
      out->setHTTPResponseHeader( cgicc::HTTPResponseHeader(out->getHTTPResponseHeader().getHTTPVersion(), 303, std::string("redirect") ) ); 
      // constructing a cgicc::HTTPResponseHeader on the stack is ok, since setHTTPResponseHeader will make a copy     
      out->getHTTPResponseHeader().addHeader("Location", 
					     this->getOwnerApplication()->getApplicationDescriptor()->getContextDescriptor()->getURL() + std::string("/") + 
					     this->getOwnerApplication()->getApplicationDescriptor()->getURN() );
      return;
    }


    out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");

    *out << cgicc::HTMLDoctype(cgicc::HTMLDoctype::eStrict) << std::endl;
    *out << cgicc::html().set("lang", "en").set("dir","ltr") << std::endl;


    //--------------------------------------------------------------------------------
    // HTML Header
    //--------------------------------------------------------------------------------
    std::string title = "GTPe Controller";
    char * hostname = getenv("HOSTNAME");
    if (hostname != 0) title += std::string(" on host") + hostname;

    *out << cgicc::head() 
	 << cgicc::title( title );
    if (_autoupdate) {
      *out << "<meta http-equiv=\"refresh\" content=\"2\">";
    }
    *out << cgicc::head() << std::endl;
  
    //--------------------------------------------------------------------------------
    // HTML Body
    //--------------------------------------------------------------------------------
    *out << cgicc::body() << std::endl;

#ifdef GTPE_WITHMONITOR
    if (_gtpeM)
      *out << "<img src=/tmp/x.gif></img>" << std::endl;
#endif

    time_t now;
    now = time(NULL);

    *out << cgicc::table() << cgicc::tr();

    *out << cgicc::td() << cgicc::form().set("method","POST").set("action", std::string("/") + this->getOwnerApplication()->getApplicationDescriptor()->getURN() + "/Default") << std::endl;
    *out << cgicc::input().set("type","hidden").set("name","autoUpd").set("value", _autoupdate?"0":"1") << std::endl;
    *out << cgicc::input().set("type","submit").set("value",_autoupdate?"Toggle autoUpdate off":"Toggle autoUpdate on")  << std::endl;
    *out << cgicc::form() << std::endl;
    *out << cgicc::td();

    *out << cgicc::td() << ctime(&now) << "GTPeController currently in state " << statename << cgicc::td() << std::endl;

    *out <<  cgicc::tr() << cgicc::table();

    // read the GTPe Status (latches the counters)
    GTPeStatus status = _gtpecrate->getGTPe().readStatusSnapshot();
    // latch all GTPe counters
    _gtpecrate->getGTPe().latchCounters();

    uint64_t cntAccepted =  _gtpecrate->getGTPe().readCounterAcceptedTriggers();
    uint64_t cntGenerated = _gtpecrate->getGTPe().readCounterGeneratedTriggers();
    *out << cgicc::table().set("border","1") << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("Counter of distributed triggers") 
	 << cgicc::td() << std::dec << cntAccepted;

    if (cntGenerated != (uint64_t) 0)
      *out << "  /  " << toolbox::toString("%4.1lf", 100. * (double) cntAccepted / (double) cntGenerated) << " % (average since enable)";

    *out << cgicc::td() << cgicc::tr() << std::endl;

    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("Total trigger rate in last second (if enabled/paused)") 
	 << cgicc::td() << toolbox::toString("%10.2lf",_gtpeM->getLastComputedRate().second) << " Hz"<< cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("Counter of all internally generated triggers") 
	 << cgicc::td() << std::dec << cntGenerated << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("Counter of triggers lost because internal buffer full") 
	 << cgicc::td() << std::dec << _gtpecrate->getGTPe().readLostEventsIntBuf() << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("Counter of triggers lost due to SLINK backpressure (LFF)") 
	 << cgicc::td() << std::dec << _gtpecrate->getGTPe().readLostEventsSLINK()  << cgicc::td() << cgicc::tr() << std::endl;

    // readback command reg directly from GTPe //fixme: need semaphore?
    *out << cgicc::tr().set("bgcolor", "#CCFFCC") 
	 << cgicc::td("GTPeStatus (CMDReg)") 
	 << cgicc::td() 
	 << "ClockedMode=" << ( _gtpecrate->getGTPe().isClockedMode() ? "true" : "false") 
	 << "; Paused=" << ( _gtpecrate->getGTPe().isPaused() ? "true" : "false") 
	 << "; Running=" << ( _gtpecrate->getGTPe().isRunning() ? "true" : "false") << cgicc::td() << cgicc::tr() << std::endl;

    // SLINK + Buffer status
    *out << cgicc::tr().set("bgcolor", "#CCFFCC") 
	 << cgicc::td("SLINK + Buffer status") 
	 << cgicc::td()
	 << "IntBufOvflw=" << ( status.isBufferFull() ? "true" : "false") 
	 << "; LinkDown=" << ( status.isLinkDown() ? "true" : "false") 
	 << "; LFF=" << ( status.isLinkFull() ?"true" : "false") 
	 << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::table() << std::endl;

    /// DAQ partition status table
    *out << cgicc::table().set("border","1") << std::endl;
    *out << cgicc::tr() << cgicc::td("DAQ Partition");
    for (int i=7;i>=0;i--)
	  *out << cgicc::td() << i << cgicc::td();
    *out << cgicc::tr() << std::endl;


    *out << cgicc::tr() << cgicc::td("");
    for (int i=7;i>=0;i--)
	if ( ! (i == _config.daqPartitionId.value_) ) 
	  *out << cgicc::td("-");
	else {
	  *out << cgicc::td() << cgicc::form().set("method","POST").set("action", std::string("/") + this->getOwnerApplication()->getApplicationDescriptor()->getURN() + "/Default") << std::endl;
	  *out << cgicc::input().set("type","hidden").set("name","resyncDAQPart").set("value", toolbox::toString("%d",i)) << std::endl;
	  *out << cgicc::input().set("type","submit").set("value", "ReSync")  << std::endl;
	  *out << "cnt=" << _resyncCount;
	  *out << cgicc::form() << std::endl;
	  *out << cgicc::td() << std::endl;;
	}
    *out << cgicc::tr() << std::endl;

    *out << cgicc::tr() << cgicc::td("Trigger Rate / Hz");
    for (int i=7;i>=0;i--) {

      if (_config.clockedMode) {
	// count number of selected DAQ partitions
	uint32_t num_selected = 0;
	for (uint32_t ii=0;ii<8;ii++) 
	  if ( status.isTCSPartitionActive(ii) ) num_selected++;

	double rate_per_partition = _config.clockedModeFrequency / (double) num_selected;

	if ( status.isTCSPartitionActive(i) ) 
	  *out << cgicc::td() << rate_per_partition << cgicc::td();
	else
	  *out << cgicc::td() << "-" << cgicc::td();
      }
      else {

	if (! status.isTCSPartitionActive(i) ) 
	  *out << cgicc::td() << "-" << cgicc::td();
	else {
 
	  double frequ = _gtpecrate->getGTPe().readbackRateHz(i);

	  *out << cgicc::td();

	  *out << cgicc::form().set("method","POST").set("action", std::string("/") + this->getOwnerApplication()->getApplicationDescriptor()->getURN() + "/Default") << std::endl;
 
	  *out << cgicc::input().set("type","text").set("size","6").set("name","rate"+toolbox::toString("%d",i)).set("value", toolbox::toString("%6.0lf",frequ))   << std::endl;
	  *out << cgicc::input().set("type","submit").set("value","Apply")  << std::endl;

	  *out << cgicc::form() << std::endl;
	
	  *out << cgicc::td() << std::endl;;
	}      

      }

    }
    *out << cgicc::tr() << std::endl;


    // selected or not
    *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td("selected");
    for (int i=7;i>=0;i--)
      *out << cgicc::td().set("bgcolor", status.isTCSPartitionActive(i)? "#F2F458" : "#CFCFCF") << ( status.isTCSPartitionActive(i) ? "Y" : "-") << cgicc::td();
    *out << cgicc::tr();

    // TCS partition state
    *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td("Part. Status");
    for (int i=7;i>=0;i--) 
      *out << stateWebTableEntry( status.getTCSPartitionState(i), status.isTCSPartitionActive(i) );
    *out << cgicc::tr();

    // event counter
    *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td("event counter");
    for (int i=7;i>=0;i--)
      if ( status.isTCSPartitionActive(i) )
	*out << cgicc::td() << std::dec << _gtpecrate->getGTPe().readCounterPartitionAcceptedTriggers(i) << cgicc::td();
      else       
	*out << cgicc::td().set("bgcolor", "#CFCFCF") << "-" << cgicc::td();
    *out << cgicc::tr();
  
    // partition trigger rate
    *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td("partition trigger rate");
    for (int i=7;i>=0;i--)
      if ( i == _daqPartitionId.value_  )
	*out << cgicc::td() << _gtpePartitionM->getLastComputedRate().second << " Hz" << cgicc::td();
      else       
	*out << cgicc::td().set("bgcolor", "#CFCFCF") << "-" << cgicc::td();
    *out << cgicc::tr();
  
    // aTTS input
    *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td("aTTS input status (*)");
    uint32_t aTTSmask = _gtpecrate->getGTPe().readbackATTSInputMask();
    for (int i=7;i>=0;i--) 
      *out << stateWebTableEntry( status.getDAQPartitionInputState(i), status.isTCSPartitionActive(i), (aTTSmask&(1<<i)) == 0 );
    *out << cgicc::tr();

    // events lost due to aTTS
    *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td("events lost due to aTTS");
    for (int i=7;i>=0;i--)
      if ( status.isTCSPartitionActive(i) )
	*out << cgicc::td() << std::dec << _gtpecrate->getGTPe().readLostEventsATTS(i) << cgicc::td();
      else       
	*out << cgicc::td().set("bgcolor", "#CFCFCF") << "-" << cgicc::td();
    *out << cgicc::tr();

    // events lost due to 1 empty bx trigger rule
    *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td("events lost due to trigger rule 1 empty bx (all subdets)");
    for (int i=7;i>=0;i--)
      if ( status.isTCSPartitionActive(i) )
	*out << cgicc::td() << std::dec << _gtpecrate->getGTPe().readLostEventsTriggerRule(7) << cgicc::td(); // det 7 has only 1 ebx rule
      else       
	*out << cgicc::td().set("bgcolor", "#CFCFCF") << "-" << cgicc::td();
    *out << cgicc::tr();
    
    uint32_t sTTSmask = _gtpecrate->getGTPe().readbackSTTSInputMask();

    for (int i_det=0;i_det<8;i_det++) {
      *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td() << "Detector partiton " << i_det << " Status (*)" << cgicc::td();

      for (int i_daq=7;i_daq>=0;i_daq--) {      
	uint32_t selected_parts = _gtpecrate->getGTPe().readbackSelectedPartitions(i_daq);
	*out << stateWebTableEntry( status.getDetPartitionInputState(i_det), selected_parts&(1<<i_det), (sTTSmask&(1<<i_det)) == 0 );
      }
      *out << cgicc::tr();

      *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td() << "Detector partiton " << i_det << " Events Lost due to sTTS" << cgicc::td();

      for (int i_daq=7; i_daq>=0; i_daq--) {      

	uint32_t selected_parts = _gtpecrate->getGTPe().readbackSelectedPartitions(i_daq);
	*out << cgicc::td().set("bgcolor",  (selected_parts&(1<<i_det))? "#F2F458" : "#CFCFCF");

	if (selected_parts&(1<<i_det))
	  *out << std::dec << _gtpecrate->getGTPe().readLostEventsSTTS(i_det) << cgicc::td();
	else 
	  *out << "-" << cgicc::td();
      }
      *out << cgicc::tr();

      *out << cgicc::tr().set("bgcolor", "#F2F458")  << cgicc::td() << "Detector partiton " << i_det 
	   << " Events Lost due to trigger rule " <<  _gtpecrate->getGTPe().getTriggerRuleName(i_det) << cgicc::td();

      for (int i_daq=7; i_daq>=0; i_daq--) {      

	uint32_t selected_parts = _gtpecrate->getGTPe().readbackSelectedPartitions(i_daq);
	*out << cgicc::td().set("bgcolor",  (selected_parts&(1<<i_det))? "#F2F458" : "#CFCFCF");

	if (selected_parts&(1<<i_det))
	  *out << std::dec << _gtpecrate->getGTPe().readLostEventsTriggerRule(i_det) << cgicc::td();
	else 
	  *out << "-" << cgicc::td();
      }
      *out << cgicc::tr();

    }


    *out << cgicc::table() << std::endl;

    *out << "<p>* aTTS and sTTS inputs are: (masked inputs in square brackets) " << std::endl;
    *out << "<ul><li>Disconnected : for input 0x0 (DISCONNECTED) and 0xf (DISCONNECTED)</li>" << std::endl;
    *out << "<li>Warning : for input 0x1 (WARNING)</li>" << std::endl;
    *out << "<li>Ready : for input 0x8 (READY), input 0x5 (Illegal) and physically disconnected cable</li>" << std::endl;
    *out << "<li>Busy : for all other input values including 0x4 (BUSY)</li></ul>" << std::endl;

    *out << "<p><p>GTPe firmware revision: " << _gtpecrate->getGTPe().readFirmwareRevisison() << "<p>" << std::endl;
    

    *out << "GTPe Configuration as from XDAQ parameters:<p>" << std::endl;
  
    *out << cgicc::table().set("border","1") << std::endl;

    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("daqPartitionId") 
	 << cgicc::td().set("colspan","2") << std::dec << _config.daqPartitionId << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("detPartitionEnableMask") 
	 << cgicc::td().set("colspan","2") << "0x" << std::hex <<_config.detPartitionEnableMask << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("triggerRate") 
	 << cgicc::td().set("colspan","2") << _config.triggerRate << " Hz" << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("autoDAQPartitionId") 
	 << cgicc::td().set("colspan","2") << (_config.autoDAQPartitionId?"true":"false") << cgicc::td() << cgicc::tr() << std::endl;
    
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("enableATTSInput") 
	 << cgicc::td().set("colspan","2") << (_config.enableATTSInput?"true":"false") << cgicc::td() << cgicc::tr() << std::endl;
    

    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("ClockedMode") 
	 << cgicc::td().set("colspan","2") << (_config.clockedMode?"true":"false") << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("ClockedModeFrequency") 
	 << cgicc::td().set("bgcolor", _config.clockedMode? "#F2F458" : "#CFCFCF").set("colspan","2") 
	 << _config.clockedModeFrequency << " Hz" << cgicc::td() << cgicc::tr() << std::endl;
    *out << cgicc::tr().set("bgcolor", "#F2F458") 
	 << cgicc::td("SLINK Required") 
	 << cgicc::td().set("colspan","2") << (_config.SLINKRequired?"true":"false") << cgicc::td() << cgicc::tr() << std::endl;
 
    *out << cgicc::table() << std::endl;

    *out << cgicc::body() << cgicc::html();


  }
  catch (xcept::Exception &e) {
    XCEPT_RETHROW( xgi::exception::Exception, "cannot create web page", e);
  }
  catch (std::exception &e) { // is there still anybody throwing a std::exception??
    XCEPT_RAISE( xgi::exception::Exception, std::string("cannot create web page. reason:") + e.what());
  }
}


std::string d2s::GTPeWebInterface::stateWebTableEntry(tts::TTSState const& state, bool selected, bool masked) const {

  const char* colour;
  
  switch (state) {
  case tts::TTSState::READY   : colour = "#F2F458"; break; //yellow
  case tts::TTSState::BUSY    : colour = "#ff3333"; break; //red
  case tts::TTSState::WARNING : colour = "#ff6600"; break; //orange
  case tts::TTSState::DISCONNECT1 : colour = "#33ccff"; break; //blue
  case tts::TTSState::DISCONNECT2 : colour = "#33ccff"; break; //blue
  default : colour = "#33ccff"; break; //blue
  }

  if (!selected) colour = "#CFCFCF"; // grey 

  std::stringstream ss;
  ss << cgicc::td().set("bgcolor", colour);
  if (selected) {
    if (masked) ss << "[";
    ss << state.getName();
    if (masked) ss << "]";
  }
  else {
    ss << "-";
  }
  ss << cgicc::td();

  return ss.str();
}
