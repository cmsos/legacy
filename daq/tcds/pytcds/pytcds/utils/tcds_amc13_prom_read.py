#!/usr/bin/env python

###############################################################################
## Checks the PROM contents on an AMC13/CPM.
###############################################################################

import sys

from pytcds.utils.tcds_amc13_flasher import AMC13Flasher
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_hw_connect import get_amc13t2_hw
from pytcds.utils.tcds_utils_misc import chunkify
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PROMReader(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        print "Reading back PROM on '{0:s}'.".format(self.target)
        amc13_t2 = get_amc13t2_hw(network_address, controlhub_address, verbose=self.verbose)

        flasher = AMC13Flasher(amc13_t2)
        data = flasher.read_prom()
        for (prom_num, prom_data) in data.iteritems():
            print "PROM {0:d}".format(prom_num)
            for chunk in chunkify(prom_data, 8):
                tmp = ["0x{0:08x}".format(i) for i in chunk]
                print "    {}".format(", ".join(tmp))

        # End of main().

    # End of class PROMReader.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to read back the PROM contents on an AMC13/CPM."

    res = PROMReader(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
