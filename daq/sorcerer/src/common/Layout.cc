// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini                                                     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "sorcerer/Layout.h"

#include "xdaq/ApplicationRegistry.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xcept/tools.h"

using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::Array;
using v8::String;
using v8::Value;
using v8::Handle;
using v8::HandleScope;
using v8::Context;

sorcerer::Layout::Layout ()
{

}

void sorcerer::Layout::resetCGICC ()
{
	// Reset all the HTML elements that might have been used to their initial state so we get valid output
	cgicc::html::reset();
	cgicc::head::reset();
	cgicc::body::reset();
	cgicc::title::reset();
	cgicc::h1::reset();
	cgicc::h2::reset();
	cgicc::h3::reset();
	cgicc::h4::reset();
	cgicc::h5::reset();
	cgicc::h6::reset();
	cgicc::div::reset();
	cgicc::p::reset();
	cgicc::table::reset();
	cgicc::thead::reset();
	cgicc::tbody::reset();
	cgicc::tfoot::reset();
	cgicc::tr::reset();
	cgicc::td::reset();
	cgicc::comment::reset();
	cgicc::a::reset();
	cgicc::colgroup::reset();
	cgicc::caption::reset();
	cgicc::form::reset();
	cgicc::legend::reset();
}

void sorcerer::Layout::getHTMLHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out)
{
	std::string path = "sorcerer::Layout::getHTMLHeader";

	Isolate * isolate = Isolate::GetCurrent();
	const unsigned argc = 1;
	Local<Value> argv[argc];
	argv[0]= { String::NewFromUtf8(isolate, path.c_str()) };
	//argv[1] = { String::NewFromUtf8(isolate, d.getHost().c_str()) };
	//argv[2] = { String::NewFromUtf8(isolate, d.getPort().c_str()) };
    //
	//Local<Object> headersObject = Object::New(isolate);
    //
	//std::multimap<std::string, std::string, std::less<std::string> >& fullheadersrmap = httpMimeHeadersRequest.getAllHeaders();
	//std::multimap<std::string, std::string, std::less<std::string> >::iterator h;
	//int n = 0;
	//for (h = fullheadersrmap.begin(); h != fullheadersrmap.end(); h++)
	//{
	//
	//	headersObject->Set(String::NewFromUtf8(isolate, (*h).first.c_str()), String::NewFromUtf8(isolate,(*h).second.c_str()));
	//}
	//argv[3] = headersObject;
	//argv[4] = { v8::String::NewFromUtf8	(isolate, requestBuffer.c_str(), v8::String::NewStringType::kNormalString, size) };


	v8::Handle<v8::Value> fnResult;

	fnResult = Local<Function>::New(isolate, this->transmute_)->Call(isolate->GetCurrentContext()->Global(), argc, argv);


    out->getHTTPResponseHeader().addHeader("Server", "sorcerer/layout");

}

void sorcerer::Layout::getHTMLFooter (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out)
{
}

/*
 * Error page
 */
void sorcerer::Layout::errorPage (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out, xcept::Exception& e)
{
	// Log error to output
	LOG4CPLUS_ERROR(manager->getApplication()->getApplicationLogger(), stdformat_exception_history(e));

	out->getHTTPResponseHeader().getStatusCode(400); // bad request
	out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(400));
	this->getHTMLHeader(manager, in, out);

	*out << "	<link href=\"/hyperdaq/html/css/xdaq-exception-viewer.css\" rel=\"stylesheet\" />" << std::endl;

	*out << cgicc::h3("A server side error has occurred") << std::endl;

	*out << "<textarea style=\"display:none;\" id=\"dlContent\">" << stdformat_exception_history(e) << "</textarea>" << std::endl;
	*out << "<input value='Download TXT' type='button' onclick='downloadExceptionHistory(document.getElementById(\"dlContent\").value)' /><br /><br />" << std::endl;

	*out << cgicc::h4("Active Log Appenders") << std::endl;
	// List all log appenders
	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Appender") << std::endl;
	*out << cgicc::th("Level") << std::endl;
	*out << cgicc::th("Output") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;

	SharedAppenderPtrList appenderList = Logger::getRoot().getAllAppenders();
	for (SharedAppenderPtrList::iterator al = appenderList.begin(); al != appenderList.end(); ++al)
	{
		*out << cgicc::tr() << std::endl;

		*out << cgicc::td((*al)->getName()) << std::endl;

		*out << cgicc::td(getLogLevelManager().toString((*al)->getThreshold())) << std::endl;

		// Output log url
		//
		if ((*al)->getName().find("file") == 0)
		{
			std::stringstream link;
			link << "<a href=\"logContentPage?url=" << cgicc::form_urlencode((*al)->getName());
			link << "&tail=1000\">view</a>";
			*out << cgicc::td(link.str()) << std::endl;
		}
		else
		{
			*out << cgicc::td("n/a") << std::endl;
		}

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	//////////
	*out << "<br />         " << std::endl;
	*out << cgicc::h4("Exception History") << std::endl;

	*out << "<table id=\"hyperdaq-exception-viewer-table-wrapper\">          " << std::endl;
	*out << "<tbody>          " << std::endl;
	*out << "<tr>          " << std::endl;
	*out << "<td style=\"vertical-align:top;\" id=\"hyperdaq-exception-viewer-td-tree\">          " << std::endl;

	//////////

	*out << "<table id=\"hyperdaq-exception-viewer-tree\" class=\"xdaq-table-tree\" data-expand=\"true\" data-allow-refresh=\"false\">          " << std::endl;
	*out << "    <thead>                                                 " << std::endl;
	*out << "        <tr>                                                " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Exception Stack </th>      " << std::endl;
	*out << "        </tr>                                               " << std::endl;
	*out << "    </thead>                                                " << std::endl;
	*out << "    <tbody>                                                " << std::endl;

	size_t treeDepth = 0;

	std::vector<xcept::ExceptionInformation> & history = e.getHistory();
	std::vector<xcept::ExceptionInformation>::reverse_iterator i = history.rbegin();
	if (i != history.rend())
	{
		if (treeDepth == 0)
		{
			*out << "<tr class=\"xdaq-table-tree-noclick hyperdaq-exceptions-table-selected\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-label=\"" << (*i).getProperty("identifier") << "\"><td>" << (*i).getProperty("identifier") << "</td></tr>" << std::endl;
		}
		else
		{
			*out << "<tr class=\"xdaq-table-tree-noclick\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-label=\"" << (*i).getProperty("identifier") << "\"><td>" << (*i).getProperty("identifier") << "</td></tr>" << std::endl;
		}
		treeDepth++;
		i++;
		while (i != history.rend())
		{
			*out << "<tr class=\"xdaq-table-tree-noclick\" data-label=\"" << (*i).getProperty("identifier") << "\" data-treeid=\"" << treeDepth << "\" data-tree-depth=\"" << treeDepth << "\" data-treeparent=\"" << (treeDepth - 1) << "\"><td>" << (*i).getProperty("identifier") << "</td></tr>" << std::endl;
			treeDepth++;
			i++;
		}
	}

	*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	//////////
	*out << "</td><td style=\"vertical-align:top;\" id=\"hyperdaq-exception-viewer-td-table\">          " << std::endl;
	//////////

	*out << "<table id=\"hyperdaq-exceptions-properties\" class=\"xdaq-table\">          " << std::endl;
	*out << "    <thead>                                                 " << std::endl;
	*out << "        <tr>                                                " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Name</th>      " << std::endl;
	*out << "            <th style=\"min-width: 100px;\">Value</th>      " << std::endl;
	*out << "        </tr>                                               " << std::endl;
	*out << "    </thead>                                                " << std::endl;

	treeDepth = 0;

	i = history.rbegin();
	while (i != history.rend())
	{


		if (treeDepth == 0)
		{
			*out << "    <tbody id=\"hyperdaq-exception-view-" << "" << treeDepth << "\">" << std::endl;
		}
		else
		{
			*out << "    <tbody id=\"hyperdaq-exception-view-" << "" << treeDepth << "\" class=\"hyperdaq-exception-viewer-hidden\">" << std::endl;
		}

		std::map<std::string, std::string, std::less<std::string> > properties = (*i).getProperties();
		for ( std::map<std::string, std::string, std::less<std::string> >::iterator j = properties.begin(); j != properties.end(); j++)
		{
			if ((*j).first != "identifier")
			{
				*out << "	 	<tr><td>" << (*j).first << "</td><td>" << (*j).second << "</td></tr>";
			}
		}
		*out << "    </tbody >                                                " << std::endl;


		treeDepth++;
		i++;
	}

	//*out << "    </tbody>                                                " << std::endl;
	*out << "</table>                                                    " << std::endl;

	//////////
	*out << "</td>          " << std::endl;
	*out << "</tr>          " << std::endl;
	*out << "</tbody>          " << std::endl;
	*out << "</table>         " << std::endl;

	*out << "	<script type=\"text/javascript\" src=\"/hyperdaq/html/js/xdaq-exception-viewer.js\"></script>" 					<< std::endl;

	//////////
	this->getHTMLFooter(manager, in, out);
}
void sorcerer::Layout::noCallbackFound (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out)
{
	std::string className = manager->getApplication()->getApplicationDescriptor()->getClassName();

	out->getHTTPResponseHeader().getStatusCode(404);
	out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(404));

	*out << "<div id=\"xdaq-404\">" << std::endl;

	*out << "	<img src=\"/hyperdaq/images/framework/xdaq-404-crack.png\" id=\"xdaq-404-crack\"><br /><br />" << std::endl;
	*out << "	We are sorry, but something has happened! <br />" << std::endl;
	*out << "	You have navigated to a page that doesn't exist! <br />" << std::endl;
	*out << "	If this is unexpected, please contact the creators of the application. <br />" << std::endl;
	*out << "	For XDAQ application's, please go <a href=\"https://svnweb.cern.ch/trac/cmsos/\" target=\"_newtab\">HERE</a> to submit a support ticket. <br />" << std::endl;
	*out << "	<br />" << std::endl;
	*out << "	Application Information : <br /><br />" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-vertical").set("style", "display: inline-block;");

	*out << cgicc::tbody();

	*out << cgicc::tr().set("class", "xdaq-table-nohover");
	*out << cgicc::th("Name") << cgicc::td(className) << std::endl;
	*out << cgicc::tr();

	/*
	*out << cgicc::tr().set("class", "xdaq-table-nohover");
	std::stringstream requestHeader;
	requestHeader << out->getHTTPResponseHeader();
	*out << cgicc::th("Request Header") << cgicc::td(requestHeader.str()) << std::endl;
	*out << cgicc::tr();
	*/

	*out << cgicc::tbody();

	*out << cgicc::table();

	*out << "</div>" << std::endl;
}

void sorcerer::Layout::Default (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out)
{
	std::string className = manager->getApplication()->getApplicationDescriptor()->getClassName();

	*out << "<div id=\"xdaq-default-default\">" << std::endl;

	*out << "	" << className << " doesn't have a HyperDAQ page! <br />" << std::endl;
	*out << "	If this is unexpected, please contact the creators of the application. <br />" << std::endl;
	*out << "	For XDAQ application's, please go <a href=\"https://svnweb.cern.ch/trac/cmsos/\" target=\"_newtab\">HERE</a> to submit a support ticket. <br />" << std::endl;

	*out << "</div>" << std::endl;
}
