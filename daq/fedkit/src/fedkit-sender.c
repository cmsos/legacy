/* fedkit documentatio is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: fedkit-sender.c,v 1.1 2007/10/09 08:19:01 cano Exp $
*/
static char *rcsid_sender = "@(#) $Id: fedkit-sender.c,v 1.1 2007/10/09 08:19:01 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_sender);
#endif
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "fedkit.h"
#include "fedkit-sender.h"
#include "fedkit-private.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>


/**
 * @file This file contains driver for the fedkit sender board (userland)
 */

/* ---------------------------------------------------------------------- */
 
/**
 * @function _fedkit_sender_error
 * @param code the error code
 * @param status pointer to the status (could nbe NULL)
 * this function is private to the imlementation of fedkit
 */
inline void _fedkit_sender_error (int code, int * status)
{
	if (status)
		*status = code;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_sender_open : opens, reservses and returns fedkit_sender strucuture
 * @return pointer to the fedkit_sender strucuture on success or NULL on failure
 * @param index index to the required sender board
 * @param status pointer to the integer holding status information
 */
struct fedkit_sender * fedkit_sender_open (int index, int * status) /* native version */
{
    struct fedkit_sender *ret = NULL;
    char filename [100];
    struct _fedkit_board_addresses addresses;
#ifndef FEDKIT_NO_MASTER_INTS
    struct _fedkit_sender_memory_blocks memory_blocks;
#endif /* ndef FEDKIT_NO_MASTER_INTS*/

	/* first establish contact with the kernel driver */
    ret = malloc (sizeof (struct fedkit_sender));
    if (ret == NULL) {
        edprintf ("Could not allocate fedkit_sender structure\n");
        _fedkit_sender_error (FK_out_of_memory, status);
        goto FKSO_alloc;
    }
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_init (&(ret->global_mutex), NULL);
    pthread_mutex_lock (&(ret->global_mutex));
#endif
    sprintf (filename, "/dev/fedkit_sender%d", index);
    ret->fd = open(filename, O_RDWR);
    /*idprintf ("\n");*/
    if (ret->fd == -1) {
        edprintf ("Could not open fedkit sender special file\n");
        perror (filename);
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_open;
    }

    /* Get the base addresses of the board */
    if (-1 == ioctl (ret->fd, FEDKIT_GET_BOARD_ADDRESSES, &addresses)) {
        edprintf ("Could not get address of sender\n");
        perror (filename);
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_addresses;
    }
    if (addresses.base_address_0 == 0 ||addresses.base_address_1 == 0 
        || addresses.base_address_2 == 0 ) {
        edprintf ("Error in fedkit_sender_open, one or more base adresses null\n");
        _fedkit_sender_error (FK_IOerror, status); 
        goto FKSO_addresses; 
    }
    ret->index = index;
    ret->base_address0 = addresses.base_address_0;
    ret->base_address1 = addresses.base_address_1;
    ret->base_address2 = addresses.base_address_2;
    ret->FPGA_version = addresses.FPGA_version;

    /* mmap the base addresses */
    ret->map0 = mmap (NULL, _FK_sender_BAR0_range, PROT_READ| PROT_WRITE, MAP_SHARED, 
                      ret->fd, (__off_t) ret->base_address0);
    if (ret->map0 == MAP_FAILED) {
        edprintf ("Could not map base address 0");
        perror (filename);
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_mmap;
    }
    ret->map1 = mmap (NULL, _FK_sender_BAR1_range, PROT_READ| PROT_WRITE, MAP_SHARED, 
                      ret->fd, (__off_t) ret->base_address1);
    if (ret->map1 == MAP_FAILED) {
        edprintf ("Could not map base address 1");
        perror (filename);
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_mmap;
    }
    ret->map2 = mmap (NULL, _FK_sender_BAR2_range, PROT_READ| PROT_WRITE, MAP_SHARED, 
                      ret->fd, (__off_t) ret->base_address2);
    if (ret->map2 == MAP_FAILED) {
        edprintf ("Could not map base address 2");
        perror (filename);
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_mmap;
    }

#ifndef FEDKIT_NO_MASTER_INTS
    /* get the master send fifo address */
    if (-1 == ioctl (ret->fd, FEDKIT_GET_SENDER_MEMBLOCKS, &memory_blocks)) {
        edprintf ("Could not get master blocks FIFO of sender\n");
        perror (filename);
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_mmap_fifo;
    }

    /* Check the address returned */
    if (NULL == memory_blocks.master_fifo_physical) {
        edprintf ("Could not get master blocks FIFO of sender\n");
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_get_fifo;
    }

    /* mmap the sender FIFO address */
    ret->master_fifo = mmap (NULL, sizeof(struct _fedkit_sender_master_fifo_t), 
                             PROT_READ | PROT_WRITE, MAP_SHARED, ret->fd, (__off_t) memory_blocks.master_fifo_physical);

    if (ret->master_fifo == MAP_FAILED) {
        edprintf ("Count not mmap the master fifo\n");
        _fedkit_sender_error (FK_IOerror, status);
        goto FKSO_get_fifo;
    }    
#endif /* ndef FEDKIT_NO_MASTER_INTS */

    /* Software reset */
    ret->map0[_FK_SENDER_MCTRL/4] |= _FK_SENDER_SOFT_RESET;
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(ret->global_mutex));
#endif
    return ret;
	
    /* error conditions unpiling */
#ifndef FEDKIT_NO_MASTER_INTS
FKSO_mmap_fifo:
FKSO_get_fifo:
#endif /* ndef FEDKIT_NO_MASTER_INTS */
FKSO_mmap:
    if (MAP_FAILED != ret->map0) munmap ((void *)ret->map0, _FK_sender_BAR0_range);
    if (MAP_FAILED != ret->map1) munmap ((void *)ret->map1, _FK_sender_BAR1_range);
    if (MAP_FAILED != ret->map2) munmap ((void *)ret->map2, _FK_sender_BAR2_range);
FKSO_addresses:
    close (ret->fd);
FKSO_open:
    free (ret);
FKSO_alloc: 
    return NULL;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_send_generated (xxx word_count, uint16_t seed, xxx event_trigger_number);
 * @return status information
 * @param sender pointer to the sender structure (returned by fedkit_sender_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_sender_open
 */
int fedkit_send_generated (struct fedkit_sender * sender, uint16_t word_count, uint16_t seed, uint32_t event_trigger_number)
{
	int ret = FK_OK;
	
	if (sender->map2[_FK_SENDER_GCTRL/4] & _FK_SENDER_FIFO_FULL) {
		return FK_overflow;
	}
	sender->map2[_FK_SENDER_GCTRL/4] |= _FK_SENDER_GENERATOR_MODE;
	sender->map2[_FK_SENDER_GEVENT/4] = event_trigger_number;
	sender->map2[_FK_SENDER_GEVENT/4] = word_count;
	sender->map2[_FK_SENDER_GEVENT/4] = seed;
	return ret;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_send_generated_fed_id (xxx word_count, uint16_t seed, uint16_t fed_id xxx event_trigger_number);
 * @return status information
 * @param sender pointer to the sender structure (returned by fedkit_sender_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param fedID number for the fedID to be put into the header
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_sender_open
 */
int fedkit_send_generated_fed_id (struct fedkit_sender * sender, uint16_t word_count, uint16_t seed, 
    uint16_t fed_id, uint32_t event_trigger_number)
{
	int ret = FK_OK;
	
	if (sender->map2[_FK_SENDER_GCTRL/4] & _FK_SENDER_FIFO_FULL) {
		return FK_overflow;
	}
	sender->map2[_FK_SENDER_GCTRL/4] |= _FK_SENDER_GENERATOR_MODE;
	sender->map2[_FK_SENDER_GEVENT/4] = event_trigger_number;
	sender->map2[_FK_SENDER_GEVENT/4] = word_count;
	sender->map2[_FK_SENDER_GEVENT/4] = seed | (fed_id << _FK_SENDER_FEDID_SHIFT);
	return ret;
}

/* ---------------------------------------------------------------------- */

/**
 * @fn fedkit_send_generated_u32 (xxx word_count, uint16_t seed, xxx event_trigger_number);
 * @return status information
 * @param sender pointer to the sender structure (returned by fedkit_sender_open)
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated) (uint32_t)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * Special version for use in labview VIs (not documented in the paper doc)
 * @see fedkit_sender_open
 */
int fedkit_send_generated_u32 (struct fedkit_sender * sender, uint32_t word_count, uint16_t seed, uint32_t event_trigger_number)
{
	int ret = FK_OK;
	
	if (sender->map2[_FK_SENDER_GCTRL/4] & _FK_SENDER_FIFO_FULL) {
		return FK_overflow;
	}
	sender->map2[_FK_SENDER_GCTRL/4] |= _FK_SENDER_GENERATOR_MODE;
	sender->map2[_FK_SENDER_GEVENT/4] = event_trigger_number;
	sender->map2[_FK_SENDER_GEVENT/4] = word_count;
	sender->map2[_FK_SENDER_GEVENT/4] = seed;
	return ret;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_sender_close
 * @param sender poiter to the sender structure returned fy fedkit_sender_open
 */
void fedkit_sender_close (struct fedkit_sender *sender) /* native version */
{
    int loop_count = 1000;
    if (sender == NULL) return;

    if ((((size_t)(sender->map0)) & 0xFFFFFFFF) == 0xECECECEC) {
        eprintf ("WARNING: probably trying to close an already closed sender\n");
    }
    /* Wait for a possible DMA in progress to finish */
    while ((sender->map0[_FK_SENDER_MCTRL/4] & _FK_SENDER_DMA_IN_PROGRESS) && (loop_count-- > 0)) {
        /* sleep 10 microseconds */

        /** keep this (seemingly unused) variable to force a read from the fedkit hardware) */
        uint32_t csr __attribute__ ((unused));
        csr = sender->map0[_FK_SENDER_MCTRL/4];
        usleep (10000);
    }
    /* reset sender */
    sender->map0[_FK_SENDER_MCTRL/4] |= _FK_SENDER_SOFT_RESET;
    /* unmap everything */
    if (MAP_FAILED != sender->map0) munmap ((void *)sender->map0, _FK_sender_BAR0_range);
    if (MAP_FAILED != sender->map1) munmap ((void *)sender->map1, _FK_sender_BAR1_range);
    if (MAP_FAILED != sender->map2) munmap ((void *)sender->map2, _FK_sender_BAR2_range);    
#ifndef FEDKIT_NO_MASTER_INTS
    if (MAP_FAILED != sender->master_fifo) munmap ((void *)sender->master_fifo, 
                                                   sizeof(struct _fedkit_sender_master_fifo_t));
#endif /* ndef FEDKIT_NO_MASTER_INTS */
    close (sender->fd);
    memset (sender, 0xEC, sizeof (struct fedkit_sender));
    free (sender);
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_sender_slave_bus_address returns the PCI address of the slave BAR area
 * of the sender board
 * @param sender pointer to the sender structure
 */
void * fedkit_sender_slave_bus_address (struct fedkit_sender * sender)
{
        // TODO: describe why this double cast is correct
        return (void *) (size_t) sender->base_address1;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_sender_slave_user_address return the userland address pointing to the BAR of the slave function
 * @param sender pointer to the sender structure
 */
void * fedkit_sender_slave_user_address (struct fedkit_sender * sender)
{
	return (void *) sender->map1;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_sender_enable_slave enable slave (or master) operations on the sender board. This function has to be called
 * prior to make any access on the slave areas.
 * @param sender pointer to the sender structure
 */
void fedkit_sender_enable_slave (struct fedkit_sender * sender)
{
	sender->map0[_FK_SENDER_MCTRL/4] &= ~_FK_SENDER_GENERATOR_MODE;	
}

/* ---------------------------------------------------------------------- */
 
/**
 * @function fedkit_master_send (struct fedkit_sender * sender, uint32_t pciaddress, uint16_t word_count)
 * @param sender pointer to the sender structure
 * @param pciaddress PCI (bus) address of the buffer to send over the link
 * @param word_count size of the buffer in 64bits words, including the FED header and trailer
 */
int fedkit_master_send (struct fedkit_sender * sender, uint32_t pciaddress, uint16_t word_count)
{
#if !defined (FEDKIT_NO_MASTER_INTS)
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(sender->global_mutex));
#endif
    if (sender->master_fifo->write + 1 % sender->master_fifo->size == sender->master_fifo->read) {
#ifndef NO_FEDKIT_PTHREAD
        pthread_mutex_unlock (&(sender->global_mutex));
#endif
        return FK_overflow;
    }
    if (!sender->master_fifo->first_dma_done) {
        /* We have to start the first DMA from here (and only the
           first) and to enable the interrupt */
        uint32_t csr = sender->map0[_FK_SENDER_MCTRL/4];
        if (csr & _FK_SENDER_DMA_IN_PROGRESS) {
            eprintf ("Internal error : sender->master_fifo->first_dma_done false and hardware reports a DMA in progress\n");
#ifndef NO_FEDKIT_PTHREAD
            pthread_mutex_unlock (&(sender->global_mutex));
#endif
            return FK_error;
        }
        /* We want to be able to track what's going on */
	// TODO: describe why this double cast is correct
        sender->master_fifo->inprogress = (void *)(size_t)pciaddress;
        /* Check the mode of the sender */
        if (csr & _FK_SENDER_GENERATOR_MODE || !(csr & _FK_SENDER_ENABLE_FIFO_DMA_DONE)) {
            sender->map0[_FK_SENDER_MCTRL/4] = (csr & ~_FK_SENDER_GENERATOR_MODE) | _FK_SENDER_ENABLE_FIFO_DMA_DONE;
        }
        sender->master_fifo->first_dma_done = 1;
        sender->map0[_FK_SENDER_MEVTADDR/4] = pciaddress;
        sender->map0[_FK_SENDER_MEVTSZ/4] = word_count;
#ifndef NO_FEDKIT_PTHREAD
        pthread_mutex_unlock (&(sender->global_mutex));
#endif
        return FK_OK;
    } else {
        /* Put the data in the fifo */
        // TODO: describe why this double cast is correct
        sender->master_fifo->data[sender->master_fifo->write].address = (void *)(size_t)pciaddress;

        sender->master_fifo->data[sender->master_fifo->write].word_count = word_count;
        sender->master_fifo->write  = (sender->master_fifo->write + 1) % sender->master_fifo->size;
        /* If tell the board to re-enable interrupt (if required) to wake up the interrupt */
        if (sender->master_fifo->int_disabled) {
            sender->master_fifo->int_disabled = 0;
            sender->map0[_FK_SENDER_MCTRL/4] |= _FK_SENDER_ENABLE_FIFO_DMA_DONE;
        }
    }
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(sender->global_mutex));
#endif
    return FK_OK;
#else /* !def FEDKIT_NO_MASTER_INTS */
    /* int fedkit_master_send (struct fedkit_sender * sender, uint32_t pciaddress, uint16_t word_count) (non master ints version) */
    uint32_t csr;
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(sender->global_mutex));
#endif
    csr = sender->map0 [_FK_SENDER_MCTRL/4];
	if (csr & _FK_SENDER_DMA_IN_PROGRESS) {
#ifndef NO_FEDKIT_PTHREAD
        pthread_mutex_unlock (&(sender->global_mutex));
#endif
		return FK_overflow; 
	}
	sender->map0[_FK_SENDER_MCTRL/4] = csr & ~_FK_SENDER_GENERATOR_MODE;
	sender->map0[_FK_SENDER_MEVTADDR/4] = pciaddress;
	sender->map0[_FK_SENDER_MEVTSZ/4] = word_count;
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(sender->global_mutex));
#endif	
	return FK_OK;
#endif /* else !def FEDKIT_NO_MASTER_INTS */
}

/* ---------------------------------------------------------------------- */

#if !defined FEDKIT_NO_MASTER_INTS
/** 
    @function fedkit_master_send_complete returns non zero if block
    is not anymore in the send list or in the inprogess value */
int fedkit_master_send_complete (struct fedkit_sender * sender, uint32_t pciaddress) 
{
    int ret = 1;
    int current_read, current_write;
    void * current_inprogress;
    int i;
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_lock (&(sender->global_mutex));
#endif
    current_read = sender->master_fifo->read;
    current_write = sender->master_fifo->write;
    current_inprogress = sender->master_fifo->inprogress;
/*    iprintf ("In progress : 0x%08x for 0x%08x\n", (int) current_inprogress, pciaddress);*/
    // TODO: describe why this double cast is correct
    if (current_inprogress == (void*)(size_t)pciaddress) {
        ret = 0;
        /*DEBUG */
/*        sender->map0[0] |= 0x80000000;
          sender->map0[0] &= ~0x80000000;*/
    }
    else for (i=current_read; i != current_write; i= (i+1)%sender->master_fifo->size) {
/*        iprintf ("Still to send : 0x%08x\n", (int)sender->master_fifo->data[i].address);*/
      // TODO: describe why this double cast is correct
      if (sender->master_fifo->data[i].address == (void*)(size_t)pciaddress) {
/*            sender->map0[0] |= 0x40000000;
              sender->map0[0] &= ~0x40000000;*/
            ret = 0;
            break;
        }
    }
#ifndef NO_FEDKIT_PTHREAD
    pthread_mutex_unlock (&(sender->global_mutex));
#endif   
    return ret;
}
#else /* ndef FEDKIT_NO_MASTER_INTS */
/** 
    @function fedkit_master_send_complete returns non zero if block
    is not anymore in the send list or in the inprogess value */
int fedkit_master_send_complete (struct fedkit_sender * sender, uint32_t pciaddress) 
{
    return 1;
}
#endif /* else ndef FEDKIT_NO_MASTER_INTS */

/* ---------------------------------------------------------------------- */

/**
 * @function _fedkit_sender_next_seed
 *
 */
uint16_t _fedkit_sender_next_seed (uint16_t seed)
{
	seed &= 0xFF;
	seed *= 5;
	seed += 3;
	return (seed & 0xFF);
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_generate_buffer this function generated the same event as the board
 * in a buffer. In conjunction with fedkit_frag_check, we can test the link.
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @see fedkit_frag_check
 */
void * fedkit_generate_buffer (uint16_t word_count, uint16_t seed, uint32_t event_trigger_number)
{
	int i,j;
	unsigned char * ret = (unsigned char *)malloc ((word_count + 3) * 8);
	if (ret != NULL) {
		seed &= 0xFF;
		seed *= 5;
		seed &= 0xFF;
		seed +=0x03;
		seed &= 0xFF;
		for (i=0; i< word_count; i++) {
			for (j=0; j<8; j++)
				ret[2 * 8 + 8 * i + j] = seed;
			seed = _fedkit_sender_next_seed (seed);
		}
		/* now fill the headers... PC is unfortunately little endian... */
		for (j=0; j<16; j++) {
			ret[j] = 0;
		}
		for (j=0; j<8; j++) {
			ret[8 * (word_count + 2) + j] = 0;
		}
		ret [7] = 0x50;
		ret [4] = 0xFF & event_trigger_number;
		ret [5] = 0xFF & (event_trigger_number >> 8);
		ret [6] = 0xFF & (event_trigger_number >> 16);
		ret [15] = 0x90;
		ret [12] = seed & 0xFF;
		ret [13] = (seed >> 8) & 0xFF; 
		ret [8 * (2 + word_count) + 7] = 0xA0;
	}
	return ret;
}

/* ---------------------------------------------------------------------- */

/**
 *	@function fedkit_check_generated_event 
 *  Faster specialised check that checks the fragment in place. It skips the expected fragment
 * in a buffer step.
 * @param fragment pointer to the fragment to check
 * @param word_count size of the event in 64 bits words, daq header and trailer not 
 *			included (but header and trailer are generated)
 * @param seed random seed used to initalise the generator process 
 * @param event_trigger_number trigger number to be included in the DAQ header of the event 
 * 			fragment generated
 * @param error_position placeholder for position of non-matching byte
 * @return error code corresponding to the found error
 */
int fedkit_check_generated_event (struct fedkit_fragment * fragment, uint16_t word_count, uint16_t seed, 
	uint32_t event_trigger_number, int *error_position)
{
	int i;
	int fragment_index =0;
	unsigned char * fragment_block;
	int fragment_block_offset;
	int expected_size = (word_count + 3) * 8;
	unsigned char header [16] = {0,0,0,0,  0xFF & event_trigger_number,
												0xFF & (event_trigger_number >> 8),
													0xFF & (event_trigger_number >> 16),
														0x50,    
								 0,0,0,0,  seed &0xFF,
												 (seed >> 8) & 0xFF
													 ,0,0x90};
													 
	unsigned char trailer [8] = {0,0,0,0,  0,0,0,0xA0};
	
	if (expected_size < fedkit_frag_size (fragment)) {
		*error_position = -1;
		return FK_size_too_big;
	} else if (expected_size > fedkit_frag_size (fragment)) {
		*error_position = -1;
		return FK_size_too_small;
	} else {	
		fragment_block = (unsigned char *)fragment->data_blocks[fragment_index++]->user_address;
		fragment_block_offset = fragment->header_size;
		/* check the header */
		for (i=0; i<2 * 8; i++) {
			if (fragment_block[fragment_block_offset++] != header [i]) {
				*error_position = i;
				return FK_data_mismatch;
			} else {
				if (i % 8 == 7) {
					if (fragment_block_offset >= fragment->block_size) {
						fragment_block = (unsigned char *)fragment->data_blocks[fragment_index++]->user_address;
						fragment_block_offset = fragment->header_size;
					}
				}
			}
		}
		
		/* check the data */
		for (i=0; i<word_count * 8; i++ ) {
			if (fragment_block[fragment_block_offset++] != header [i]) {
				*error_position = i;
				return FK_data_mismatch;
			} else {
				if (i % 8 == 7) {
					if (fragment_block_offset >= fragment->block_size) {
						fragment_block = (unsigned char *)fragment->data_blocks[fragment_index++]->user_address;
						fragment_block_offset = fragment->header_size;
					}
				}
			}
		}
		
		/* check the trailer */
		for (i=0; i<8; i++) {
			if (fragment_block[fragment_block_offset++] != trailer [i]) {
				*error_position = i;
				return FK_data_mismatch;
			}
		}
	}
	return FK_OK;
}

/* ---------------------------------------------------------------------- */

/**
 * @function fedkit_sender_get_FPGA_version
 * @parameter sender pointer to the sender structure
 * @return The sender's FPGA version
 */

uint32_t fedkit_sender_get_FPGA_version (struct fedkit_sender * sender )
{
    if (sender != NULL) {
        return sender->FPGA_version;
    } else {
        return 0xFFFFFFFF;
    }
}

/* ---------------------------------------------------------------------- */
