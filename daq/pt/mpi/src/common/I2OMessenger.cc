// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/mpi/I2OMessenger.h"
#include "pt/mpi/PeerTransport.h"
#include "pt/mpi/Address.h"

#include "toolbox/hexdump.h"

#include "toolbox/utils.h"

#include "i2o/i2o.h"

#include "xcept/tools.h"

pt::mpi::I2OMessenger::I2OMessenger (pt::mpi::PeerTransport * pt, mpila::QueuePair * qp, pt::Address::Reference local, pt::Address::Reference destination)
: pt_(pt), qp_(qp), local_(local), destination_(destination), mutex_(toolbox::BSem::FULL, true)
{

	//pt::mpi::Address & l = dynamic_cast<pt::mpi::Address &>(*local);

}

pt::mpi::I2OMessenger::~I2OMessenger ()
{
}


void pt::mpi::I2OMessenger::send (toolbox::mem::Reference * reference, toolbox::exception::HandlerSignature * handler, void * context)
{
	mutex_.take();


	pt::mpi::Address & destination = dynamic_cast<pt::mpi::Address &>(*destination_);
	//pt::mpi::Address & local = dynamic_cast<pt::mpi::Address &>(*local_);

	toolbox::mem::Reference * ref = reference;
	while (ref != 0)
	{

		size_t size = ref->getDataSize();
		PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) ref->getDataLocation();
		if (size == (size_t)(frame->MessageSize << 2))
		{
			void * cookie = 0;
			if (ref->getNextReference() == 0)
			{
				cookie = reference;
			}

			mpila::WorkRequest sendRequest;
			sendRequest.memoryBuffer =  (mpila::memoryBuffer_t*) ref->getDataLocation();
			sendRequest.cookie = cookie;
			sendRequest.size = size;
			sendRequest.tag = 0;
			sendRequest.workRequestType = mpila::WorkRequestType::SEND;
			sendRequest.context = qp_;
			qp_->post(sendRequest);

			//toolbox::hexdump ((void*) frame, 256);

		}
		else
		{
			std::stringstream ss;
			ss << "Message frame size " << (frame->MessageSize << 2) << " does not match reference data size " << size << ", cannot post to destination address " << destination.toString();
			reference->release();
			mutex_.give();
			XCEPT_RAISE(pt::mpi::exception::Exception, ss.str());
		}

		ref = ref->getNextReference();
	}

	mutex_.give();
}


pt::Address::Reference pt::mpi::I2OMessenger::getLocalAddress ()
{
	return local_;
}

pt::Address::Reference pt::mpi::I2OMessenger::getDestinationAddress ()
{
	return destination_;
}
