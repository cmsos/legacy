package xdaq.xdesk.xcontrol;

import java.util.*;
import javax.swing.tree.*;
import javax.infobus.*;
import org.w3c.dom.*;

public class SelectionRowSet implements RowsetAccess, DataItem
{

	TreePath[] paths_; // selected paths in configuration tree
	int currentRow_;
	String command_; // command for the currently selected row
	String url_; // URL for the currently selected row
	String destinationTid_; // destination Tid for currently selected row
	String originatorTid_; // originator Tid for currently selected row
	String id_;
	org.w3c.dom.Node content_; // DOM content for currently selected row
	boolean released_; // indicates to producer if consumer has finished accessing the selection
	
	/*! Pass a JTree selection and a command. 
		Currently only a single command is allowed that will be used
		for all items selected in the tree
	  */
	public SelectionRowSet(TreePath[] paths, String command)
	{
		released_ = false;
		paths_ = paths;
		command_ = command;
		id_ = null;
		currentRow_ = -1; // next must be called before accessing the first row
	}

	// --- Data item interfaces
	
	//! not implemented
	public Object getProperty (String propertyName)
	{
		return null;
	}
	
	public InfoBusEventListener getSource()
	{
		return null;
	}
	
	//! Allows a producer to know when the consumer has finished using the
	//! data item.
	public void release()
	{
		released_ = true;
	}
	
	// private to the producer for checking if he can
	// change the selection
	public boolean isReleased()
	{
		return released_;
	}
	
	// --- RowsetAccess interfaces
	
	public boolean canDelete()
	{
		return false;
	}
	
	public boolean canInsert()
	{
		return false;
	}
	
	public boolean canUpdate()
	{
		return false;
	}
	
	public boolean canUpdate(int columnNumber)
	{
		return false;
	}
	
	public boolean canUpdate (String columnName)
	{
		return false;
	}
	
	public void deleteRow ()
	{

	}
	
	public void flush()
	{
	
	}
	
	/*! There are 5 columns:
		0: command, 1: URL, 2: destinationTid, 3: originatorTid, 
		4: id, 5: content
	*/
	public int getColumnCount()
	{
		return 6;
	}
	
	public String getColumnDatatypeName (int columnIndex)
	{
		return "java.sql.Types.JAVA_OBJECT";
	}
	
	public int getColumnDatatypeNumber (int columnIndex)
	{
		return java.sql.Types.JAVA_OBJECT;
	}
	
	public Object getColumnItem (int columnIndex)
	{
		// Here, for all selected configuration items, the
		// same command is returned;

		
		switch (columnIndex)
		{
			case 0: return command_;  
			case 1: return url_; 
			case 2: return destinationTid_; 
			case 3: return originatorTid_; 
			case 4: return id_; 
			case 5: return content_; 
			default: return null;
		}
		
	}
	
	public Object getColumnItem (String columnName)
	{
		if (columnName.equals ("command")) return command_;
		if (columnName.equals ("url")) return url_;
		if (columnName.equals ("destinationTid")) return destinationTid_;
		if (columnName.equals ("originatorTid")) return originatorTid_;
		if (columnName.equals ("id")) return id_;		
		if (columnName.equals ("content")) return content_;
		
		// not found
		return null;
	}
	
	public String getColumnName (int columnIndex)
	{
		switch (columnIndex)
		{
			case 0: return new String("command");
			case 1: return new String("url");
			case 2: return new String("destinationTid");
			case 3: return new String("originatorTid");
			case 4: return new String("id");
			case 5: return new String("content");
			default: return null;
		}
	}
	
	//! Not implemented
	public DbAccess getDb()
	{
		return null;
	}
	
	public int getHighWaterMark()
	{
		return paths_.length;
	}
	
	public boolean hasMoreRows()
	{
		if (currentRow_ < (paths_.length - 1) ) return true;
		else return false;
	}
	
	//! Not implemented
	public void lockRow()
	{
	
	}
	
	//! not implemented
	public void newRow()
	{
		
	}
	
	public boolean next()
	{
		if (currentRow_ == (paths_.length - 1 ) ) return false;
		System.out.println("Move to next row");
		currentRow_++;
		setRowContent();
		return true;
	}
	
	//! not implemented
	public void setColumnValue ( int columnIndex, Object object)
	{
		
	}
	
	//! not implemented
	public void setColumnValue (String columnName, Object object)
	{
			
	}
	
	// Helper function to set content variables for current row;
	protected void setRowContent ()
	{
		System.out.println("setRowContent for row:" + currentRow_);
		TreePath p = paths_[currentRow_];
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) p.getLastPathComponent();
		XTreeNode aNode = (XTreeNode) node.getUserObject();
		
		if (node.toString().equals("Host")) 
		{
			url_ = aNode.getAttribute ("url");
			
			System.out.println("Url for host selection:" + url_);
			
			destinationTid_ = "0"; // executive tid
			originatorTid_ = "0"; // dummy
			
			// Pass whole configuration when message goes to executive and command is configure
			if ( command_.equals("Configure"))
				content_ = aNode.domNode.getOwnerDocument().getDocumentElement();
			else
				content_ = null; // no content for other messages than configure
			
			// Pass the host id only if a host is selected
			id_ = aNode.getAttribute ("id");
			
			System.out.println("all has been assigned for" + url_);			
		} 
		else if (node.toString().equals ("Application") || node.toString().equals ("Transport"))
		{
			// Find out the closest encapsulating <Host> tag for the url
			XTreeNode father = ((XTreeNode) node.getUserObject()).getParent();
			url_ = father.getAttribute ("url");
			System.out.println("Url for application selection:" + url_);
			destinationTid_ = aNode.getAttribute ("targetAddr");
			originatorTid_ = "0"; // dummy
			content_ = null; // no content
			
			// no host id for applications or transports
			id_ = null;
			
			System.out.println("all has been assigned for" + url_);	
		}
	}
}
