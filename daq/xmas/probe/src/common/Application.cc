// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h"
#include "xcept/tools.h"

#include "xmas/probe/Application.h"
#include "xmas/probe/FlashListMonitor.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xmas/probe/MonitorReportEvent.h"
#include "xmas/xmas.h"
#include "xmas/probe/Sampler.h"
#include "xmas/probe/exception/MissingInfoSpaceDeclaration.h"
#include "xmas/probe/exception/EvaluationFailed.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "xoap/DOMParserFactory.h"

#include "b2in/utils/exception/SendFailure.h"
#include "b2in/utils/exception/CheckConnectionFailure.h"

XDAQ_INSTANTIATOR_IMPL (xmas::probe::Application);

xmas::probe::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), flashListMonitorRegistry_(this), scheduler_(&flashListMonitorRegistry_), dispatcher_("urn:xdaq-workloop:xmasprobe", "waiting", 0.8)
{
	//outgoingHeartbeatCounter_ = 0;
	//outgoingHeartbeatLostCounter_ = 0;
	outgoingReportLostCounter_ = 0;
	outgoingReportCounter_ = 0;

	pool_ = 0;
	deadBand_ = false;
	sensorMessengerCache_ = 0;

	sensordURL_ = "";

	committedPoolSize_ = 0x100000 * 5; // 5 MB
	highThreshold_ = 0.9;
	lowThreshold_ = 0.8;

	this->getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("lowThreshold", &lowThreshold_);
	this->getApplicationInfoSpace()->fireItemAvailable("highThreshold", &highThreshold_);

	maxReportMessageSize_ = 0x10000; // 64KB
	this->getApplicationInfoSpace()->fireItemAvailable("maxReportMessageSize", &maxReportMessageSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("sensordURL", &sensordURL_);

	monitorableSearchModes_.push_back("starts with");
	monitorableSearchModes_.push_back("ends with");
	monitorableSearchModes_.push_back("contains");
	selectedInfoSpace_ = "Any";
	selectedFlashList_ = "Any";
	selectedMonitorableSearchMode_ = "starts with";
	selectedMonitorableSearchString_ = "";

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/xmas/probe/images/xmas-probe-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/probe/images/xmas-probe-icon.png");

	// This application is an asynchronous event processor
	dispatcher_.addActionListener(this);

	// Activates work loop for sensor asynchronous operations
	(void) toolbox::task::getWorkLoopFactory()->getWorkLoop("urn:xdaq-workloop:xmasprobe", "waiting")->activate();

	autoConfigure_ = false;
	autoConfSearchPath_ = "/tmp";

	this->getApplicationInfoSpace()->fireItemAvailable("autoConfigure", &autoConfigure_);

	// heartbeat report interval
	//heartbeatWatchdog_ = "PT30S";
	//this->getApplicationInfoSpace()->fireItemAvailable("heartbeatWatchdog", &heartbeatWatchdog_);

	this->getApplicationInfoSpace()->fireItemAvailable("url", &settingsURLs_);
	this->getApplicationInfoSpace()->fireItemAvailable("autoConfSearchPath", &autoConfSearchPath_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &xmas::probe::Application::Default, "Default");
	xgi::bind(this, &xmas::probe::Application::getInfoSpaceTable, "getInfoSpaceTable");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);

	// Infosacpe creation 
	xdata::getInfoSpaceFactory()->addItemAvailableListener(this);

}

xmas::probe::Application::~Application ()
{

}

void xmas::probe::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string timerTaskName = e.getTimerTask()->name;
	if (timerTaskName == "xmasprobe:scan")
	{
		/*
		 try
		 {

		 this->heartbeat();
		 }
		 catch (b2in::utils::exception::Exception& f)
		 {
		 std::stringstream msg;
		 msg << "Failed to scan b2in-eventing (for heartbeat) services";
		 XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, e, msg.str(), f);
		 this->notifyQualified("fatal", e);
		 }
		 */
	}
	else // pulsing algorithm
	{

		toolbox::net::UUID uuid(timerTaskName.c_str());

		xmas::PulserEvent * event;

		try
		{
			pulserSettings_.lock();
			event = pulserSettings_.getPulserEvent(uuid);

			// Send pulse only if event is enabled
			if (event->getProperty("enabled") == "false")
			{
				pulserSettings_.unlock();
			}
			else
			{
				// Update pulse count and last pulser time
				event->update();
				pulserSettings_.unlock();

				std::map < std::string, std::string > &samples = event->getSamples();
				
				for (std::map<std::string, std::string>::iterator i = samples.begin(); i != samples.end(); i++)
				{
					this->processPulse((*i).first, (*i).second);
				}
			}
		}
		catch (xmas::exception::Exception & e)
		{
			pulserSettings_.unlock();
			//std::cout << stdformat_exception_history(ex) << std::endl;
			LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			this->notifyQualified("error", e);
			return;
		}
	}

	// check connection to sensord

	if (sensorMessengerCache_ == 0)
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Messenger cache not ready");
		return;
	}

	if (!sensorMessengerCache_->hasMessenger(sensordURL_))
	{
		try
		{
			//std::cout << "create messenger " << std::endl;
			sensorMessengerCache_->createMessenger(sensordURL_); // trigger  caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << sensordURL_.toString() << ", " << e.what());
			this->notifyQualified("error", e);
			return;
		}
	}

}

void xmas::probe::Application::actionPerformed (xdata::Event& event)
{

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		// 	
		// use to remember already configured services
		//
		configuredServices_.clear();
		try
		{
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			toolbox::net::URN urn("b2in", "sensor");
			pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
			pool_->setHighThreshold((unsigned long) (committedPoolSize_ * highThreshold_));
			pool_->setLowThreshold((unsigned long) (committedPoolSize_ * lowThreshold_));

		}
		catch (toolbox::mem::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to create b2in/sensor memory pool for size " << committedPoolSize_.toString();
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);

		}

		if (autoConfigure_)
		{
			std::set < std::string > services = this->scanLocalServices();
			for (std::set<std::string>::iterator i = services.begin(); i != services.end(); i++)
			{
				if ( configuredServices_.find(*i) != configuredServices_.end() )
				{
					configuredServices_[(*i)]++;
				}
				else
				{
					std::stringstream path;
					path << autoConfSearchPath_.toString() << "/" << *i << ".sensor";
					this->applySensorSettings(path.str());
	
					std::stringstream pulserPath;
					pulserPath << autoConfSearchPath_.toString() << "/" << *i << ".pulser";
					try
					{
						this->loadPulserEvents(pulserPath.str());
					}
					catch (xmas::probe::exception::Exception& e)
					{
						std::stringstream msg;
						msg << "Failed to import pulser settings for '" << *i << "' from '" << pulserPath.str() << "', ";
						XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
						this->notifyQualified("fatal", q);
					}

					configuredServices_[(*i)] = 1;
				}

			}
		}
		else // use stated configuration
		{
			// import monitor seetings and flash list definitions from URL
			//
			for (xdata::Vector<xdata::String>::iterator i = settingsURLs_.begin(); i != settingsURLs_.end(); i++)
			{
				this->applySensorSettings(*i);
			}
		}

		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:probe-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:probe-timer");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:probe-timer");
		}
		// submit scan task
		toolbox::TimeInterval interval;

		int delay = (rand() % 14) + 1;
		toolbox::TimeVal start(toolbox::TimeVal::gettimeofday().sec() + delay);
		//interval.fromString(heartbeatWatchdog_);
		interval.fromString("PT10S");

		timer->scheduleAtFixedRate(start, this, interval, 0, "xmasprobe:scan");

		this->schedulePulseEvents(timer);
	}
	else if (event.type() == "ItemAvailableEvent") // New infospace created threfore update flashlist monitors
	{
		xdata::ItemAvailableEvent & e = dynamic_cast<xdata::ItemAvailableEvent&>(event);
		xdata::Serializable * s = e.item();

		if (s->type() == "infospace")
		{
			flashListMonitorRegistry_.lock();

			xdata::InfoSpace * is = static_cast<xdata::InfoSpace*>(s);

			std::vector<xmas::probe::FlashListMonitor*> v = flashListMonitorRegistry_.getFlashListMonitors(is->name());
			std::vector<xmas::probe::FlashListMonitor*>::iterator i;
			for (i = v.begin(); i != v.end(); i++)
			{
				(*i)->attachToInfospace(is);
			}

			flashListMonitorRegistry_.unlock();

		}
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(xmas::probe::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void xmas::probe::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "urn:xmas-probe-event:MonitorReport") // New infospace created threfore update flashlist monitors
	{
		xmas::probe::MonitorReportEvent & report = dynamic_cast<xmas::probe::MonitorReportEvent&>(event);
		this->publishReport(report);
	}
	else if (event.type() == "xdaq::EndpointAvailableEvent")
	{
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(event);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName() << " on application network " << networkName);

		if (network->getName() == networkName)
		{
			try
			{
				sensorMessengerCache_ = new b2in::utils::MessengerCache(this->getApplicationContext(), networkName, this);
			}
			catch (b2in::utils::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to create b2in messenger cache on network '" << network << "'";
				LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
				LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
				return;
			}
			try
			{
				sensorMessengerCache_->createMessenger(sensordURL_); // trigger  caching of messenger for the remote endpoint
			}
			catch (b2in::utils::exception::Exception& e)
			{
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << sensordURL_.toString() << ", " << e.what());
				this->notifyQualified("error", e);
				return;
			}
		}
		else
		{
			std::stringstream ss;
			ss << "No match found for network name, not creating messenger cache : configured network name = '" << networkName << "', available = '" << network->getName() << "'";
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), ss.str());
			return;
		}

	}

	else if (event.type() == "urn:xdaq-event:InstantiateApplication")
	{
		if (autoConfigure_)
		{
			xdaq::InstantiateApplicationEvent& de = dynamic_cast<xdaq::InstantiateApplicationEvent&>(event);
			std::string service = de.getApplicationDescriptor()->getAttribute("service");

			if (service != "")
			{
				// Protect against instantiation event of the own application
				if (service != this->getApplicationDescriptor()->getAttribute("service") )
				{
					if ( configuredServices_.find(service) != configuredServices_.end()) 
					{
						configuredServices_[service]++;

					}
					else
					{
						std::stringstream path;
						path << autoConfSearchPath_.toString() << "/" << service << ".sensor";
						this->applySensorSettings(path.str());
	
						std::stringstream pulserPath;
						pulserPath << autoConfSearchPath_.toString() << "/" << service << ".pulser";
						try
						{
							this->loadPulserEvents(pulserPath.str());
						}
						catch (xmas::probe::exception::Exception& e)
						{
							std::stringstream msg;
					 		msg << "Failed to import pulser settings for '" << service << "' from '" << pulserPath.str() << "', ";
							XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
							this->notifyQualified("fatal", q);
						}
	
						toolbox::task::Timer * timer = 0;
						// Create timer for refreshing subscriptions
						if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:probe-timer"))
						{
							timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:probe-timer");
	
						}
						else
						{
							timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:probe-timer");
						}
						// re-schedule pulser for new events
						this->schedulePulseEvents(timer);

						configuredServices_[service] = 1;
					}
				}
			}
			else
			{
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "No service attribute provided for application class '" << de.getApplicationDescriptor()->getAttribute("class") << "', cannot load sensor settings");

			}
		}
	}
}

std::set<std::string> xmas::probe::Application::scanLocalServices ()
{
	std::set < std::string > services;

	const xdaq::Zone* zone = getApplicationContext()->getDefaultZone();
	std::set<const xdaq::ApplicationDescriptor*> local = zone->getApplicationDescriptors(this->getApplicationContext()->getContextDescriptor());
	for (std::set<const xdaq::ApplicationDescriptor*>::iterator vi = local.begin(); vi != local.end(); ++vi)
	{
		if ((*vi)->getAttribute("service") != "")
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Found local service:" << (*vi)->getAttribute("service"));
			services.insert((*vi)->getAttribute("service"));
		}
		else
		{

			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "No service attribute provided for application class '" << (*vi)->getAttribute("class") << "', cannot load sensor settings");

		}

	}
	return services;
}

void xmas::probe::Application::loadPulserEvents (const std::string& path) 
{
	// import all flash list definitions 
	// The path may be a pattern, so expand it before
	std::vector < std::string > files;
	std::vector<xmas::PulserSettings*> settings;
	try
	{
		files = toolbox::getRuntime()->expandPathName(path);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to import pulser settings from '" << path << "', ";
		XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
	}

	pulserSettings_.lock();
	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		try
		{
			DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(*j);
			pulserSettings_.addEvents(doc);
			doc->release();
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Loaded pulser events from '" << (*j) << "'");
		}
		catch (xoap::exception::Exception& e)
		{
			pulserSettings_.unlock();
			std::stringstream msg;
			msg << "Failed to load configuration file from '" << (*j) << "'";
			XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
		}
		catch (xmas::exception::Exception& e)
		{
			pulserSettings_.unlock();
			std::stringstream msg;
			msg << "Failed to parse configuration from '" << (*j) << "'";
			XCEPT_RETHROW(xmas::probe::exception::Exception, msg.str(), e);
		}
	}

	pulserSettings_.unlock();

	return;
}

void xmas::probe::Application::schedulePulseEvents (toolbox::task::Timer * timer)
{
	pulserSettings_.lock();
	std::map<toolbox::net::UUID, xmas::PulserEvent*>& events = pulserSettings_.getPulserEvents();
	std::map<toolbox::net::UUID, xmas::PulserEvent*>::iterator ei;
	for (ei = events.begin(); ei != events.end(); ++ei)
	{

		xmas::PulserEvent* pe = (*ei).second;

		if (pe->getProperty("scheduled") == "true")
		{
			continue; // already scheduled ignore
		}

		toolbox::TimeVal start;
		try
		{
			start = pe->getStartTime();
		}
		catch (xmas::exception::Exception & e)
		{
			pulserSettings_.unlock();
			std::stringstream msg;
			msg << "Failed to retrieve start time for pulser event";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
			return;
		}

		toolbox::net::UUID id((*ei).first);
		if (pe->hasPeriod())
		{
			// Pass UUID as cookie
			try
			{
				toolbox::TimeInterval interval = pe->getPeriod();
				timer->scheduleAtFixedRate(start, this, interval, 0, id.toString());
				pe->setProperty("scheduled", "true");
			}
			catch (toolbox::task::exception::InvalidListener & e)
			{
				pulserSettings_.unlock();
				std::stringstream msg;
				msg << "Failed to schedule pulser event";
				XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}
			catch (toolbox::task::exception::InvalidSubmission & e)
			{
				pulserSettings_.unlock();
				std::stringstream msg;
				msg << "Failed to schedule pulser event";
				XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}
			catch (toolbox::task::exception::NotActive & e)
			{
				pulserSettings_.unlock();
				std::stringstream msg;
				msg << "Failed to schedule pulser event";
				XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}
			catch (xmas::exception::Exception & e)
			{
				pulserSettings_.unlock();
				std::stringstream msg;
				msg << "Failed to retrieve interval";
				XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}
		}
		else
		{
			try
			{
				timer->schedule(this, start, 0, id.toString());
				pe->setProperty("scheduled", "true");
			}
			catch (toolbox::task::exception::InvalidListener & e)
			{
				pulserSettings_.unlock();
				std::stringstream msg;
				msg << "Failed to schedule pulser event";
				XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}
			catch (toolbox::task::exception::InvalidSubmission & e)
			{
				pulserSettings_.unlock();
				std::stringstream msg;
				msg << "Failed to schedule pulser event";
				XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}
			catch (toolbox::task::exception::NotActive & e)
			{
				pulserSettings_.unlock();
				std::stringstream msg;
				msg << "Failed to schedule pulser event";
				XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}
		}
	}
	pulserSettings_.unlock();
}

void xmas::probe::Application::applySensorSettings (const std::string & path)
{

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Apply sensor settings:" << path);

	// The profile may be a pattern, so expand it before
	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(path);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to import flashlist definitions from '" << path << "', ";
		XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
		this->notifyQualified("fatal", q);
		return;

	}

	flashListMonitorRegistry_.lock();

	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(*j);
			std::vector<xmas::MonitorSettings*> settings = xmas::MonitorSettingsFactory::create(doc);
			std::vector<xmas::MonitorSettings *>::iterator k;
			for (k = settings.begin(); k != settings.end(); k++)
			{
				// Only install a FlashListMonitor object if it is not already instantiated in the registry
				//
				std::string name = (*k)->getFlashListDefinition()->getProperty("name");
				if (!flashListMonitorRegistry_.exists(name))
				{
					LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Applying xmas sensor settings for flashlist '" << name << "', creating monitor object");
					xmas::probe::FlashListMonitor * monitor = flashListMonitorRegistry_.install(*k, &scheduler_);
					std::list<xmas::probe::Sampler*> samplers = monitor->getSamplers();
					for (std::list<xmas::probe::Sampler*>::iterator s = samplers.begin(); s != samplers.end(); s++)
					{
#warning "the sampler is already active and could already receive data and interfere with this add listener call"
						(*s)->addActionListener(this);
					}

				}
				else
				{
					delete (*k); // the vector goes out of scope at the end of the function
				}
			}
			doc->release();
			doc = 0;
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Loaded sensor settings '" << (*j) << "'");
		}
		catch (xmas::probe::exception::FlashListMonitorCreationFailed& e)
		{
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to install monitor settings for flashlist from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
			flashListMonitorRegistry_.unlock();
			return;
		}
		catch (xoap::exception::Exception& e)
		{
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to load configuration file from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
			flashListMonitorRegistry_.unlock();
			return;
		}
		catch (xmas::exception::Exception& e)
		{
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to parse configuration from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
			flashListMonitorRegistry_.unlock();
			return;
		}
	}

	flashListMonitorRegistry_.unlock();

}

void xmas::probe::Application::publishReport (xmas::probe::MonitorReportEvent & report)
{
	xmas::FlashListDefinition * flashListDefinition = report.getFlashListDefinition();

	flashListMonitorRegistry_.lock();

	xmas::probe::FlashListMonitor * monitor;
	try
	{
		monitor = flashListMonitorRegistry_.get(flashListDefinition->getProperty("name"));
	}
	catch (xmas::probe::exception::FlashListMonitorNotFound & e)
	{
		flashListMonitorRegistry_.unlock();
		return;
	}
	flashListMonitorRegistry_.unlock();

// LO new
	if (sensorMessengerCache_ == 0)
	{
		monitor->incrementCommunicationLossCounter();
		return;
	}
// LO new
#ifdef CHANGING
	if ( dialup_ == 0 )
	{
		outgoingReportLostCounter_++;
		return;
	}
#endif

	if (deadBand_)
	{
		if (!pool_->isLowThresholdExceeded())
		{
			deadBand_ = false;
			LOG4CPLUS_WARN(this->getApplicationLogger(), "exit dead band ( start receiving)");
		}
		else
		{
			// still in dead band, therefore cannot report metrics, lost
			monitor->incrementMemoryLossCounter();
			return;
		}
	}
	else if (pool_->isHighThresholdExceeded())
	{
		LOG4CPLUS_WARN(this->getApplicationLogger(), "enter dead band (start discarding)");
		deadBand_ = true;
		// cannot allocate buffers, therefore I return and the monitor report is lost
		monitor->incrementMemoryLossCounter();
		return;
	}

	// Prepare report message
	//
	toolbox::net::URL at(getApplicationContext()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN());
	//report.setProperty("originator",at.toString());

	xdata::Properties plist;
	plist.setProperty("urn:b2in-protocol:service", "sensord");
	plist.setProperty("urn:b2in-protocol:action", "flashlist");

	plist.setProperty("urn:xmas-flashlist:name", flashListDefinition->getProperty("name"));
	plist.setProperty("urn:xmas-flashlist:version", flashListDefinition->getProperty("version"));
	plist.setProperty("urn:xmas-flashlist:tag", report.getProperty("tag"));
	plist.setProperty("urn:xmas-flashlist:originator", at.toString());

	std::map < std::string, xdata::Table::Reference > metrics = report.getMetrics();
	//std::cout << "Found metrics " << metrics.size() << std::endl;

	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxReportMessageSize_);
	}
	catch (toolbox::mem::exception::Exception & ex)
	{
		monitor->incrementMemoryLossCounter();
		XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, e, "Failed to allocate messaage for monitor report", ex);
		this->notifyQualified("error", e);
		return;
	}

	for (std::map<std::string, xdata::Table::Reference>::iterator i = metrics.begin(); i != metrics.end(); i++)
	{
		//std::cout << "Add attachment "  << std::endl;
		try
		{

			xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) ref->getDataLocation(), maxReportMessageSize_);
			serializer_.exportAll(&(*((*i).second)), &outBuffer);

			ref->setDataSize(outBuffer.tellp());

			// Only one table  accepted 
			break;

		}
		catch (xdata::exception::Exception & e)
		{
			monitor->incrementMemoryLossCounter();

			ref->release();
			std::stringstream msg;
			msg << "failed to serialize monitor report for table defined as '(";
			const std::map < std::string, std::string, xdata::Table::ci_less > &definitions = (*i).second->getTableDefinition();
			for (std::map<std::string, std::string, xdata::Table::ci_less>::const_iterator k = definitions.begin(); k != definitions.end(); k++)
			{
				msg << "(" << (*k).first << "," << (*k).second << ")";
			}
			msg << ")' with '" << (*i).second->getRowCount() << "'";

			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, ex, msg.str(), e);
			this->notifyQualified("fatal", ex);
			return;

		}
	}

	if (!sensorMessengerCache_->hasMessenger(sensordURL_))
	{
		try
		{
			sensorMessengerCache_->createMessenger(sensordURL_); // trigger caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			ref->release();
			std::stringstream ss;
			ss << "Failed to create messenger to " << sensordURL_.toString();
			LOG4CPLUS_FATAL(this->getApplicationLogger(), ss.str() << " : " << xcept::stdformat_exception_history(e));
			XCEPT_RETHROW(xmas::probe::exception::Exception, ss.str(), e);
			return;
		}
	}

	try
	{
		sensorMessengerCache_->send(sensordURL_, ref, plist);
		outgoingReportCounter_++;
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "failed to send flashlist to sensord: " << xcept::stdformat_exception_history(e));
		ref->release();
		monitor->incrementCommunicationLossCounter();
	}
	catch (b2in::nub::exception::QueueFull & e)
	{
		// ignore
		ref->release();
		monitor->incrementCommunicationLossCounter();
		LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to send flashlist to sensord: " << xcept::stdformat_exception_history(e));
	}
	catch (b2in::nub::exception::OverThreshold & e)
	{
		// ignore
		ref->release();
		monitor->incrementCommunicationLossCounter();
		LOG4CPLUS_WARN(this->getApplicationLogger(), "failed to send flashlist to sensord: " <<  xcept::stdformat_exception_history(e));
	}

}

void xmas::probe::Application::asynchronousExceptionNotification (xcept::Exception& ex)
{
	LOG4CPLUS_TRACE(this->getApplicationLogger(), "Sending a flashlist or messenger cache check connection failed, " << ex.what());

	if (sensordURL_ != "")
	{
		if (!sensorMessengerCache_->hasMessenger(sensordURL_))
		{
			try
			{
				sensorMessengerCache_->createMessenger(sensordURL_); // trigger caching of messenger for the remote endpoint
			}
			catch (b2in::utils::exception::Exception& e)
			{
				LOG4CPLUS_FATAL(this->getApplicationLogger(), "Failed to create messenger on to " << sensordURL_.toString() << ", " << e.what());
				this->notifyQualified("error", e);
				return;
			}
		}
		else
		{
			LOG4CPLUS_TRACE(this->getApplicationLogger(), "Network exception, messenger already exists");
		}
	}

	if (ex.name() == "b2in::utils::exception::SendFailure")
	{
		outgoingReportLostCounter_++;
	}
}

void xmas::probe::Application::processPulse (const std::string& flashListName, const std::string& tagName) 
{
	flashListMonitorRegistry_.lock();

	xmas::probe::FlashListMonitor * monitor;
	try
	{
		monitor = flashListMonitorRegistry_.get(flashListName);
	}
	catch (xmas::probe::exception::FlashListMonitorNotFound & e)
	{
		flashListMonitorRegistry_.unlock();
		return;
	}

	///////////////////////////////////////
	//LOLO at this point the monitor object for this flashlist is retrieved, so we can free the registry to avoid deadlock situations
	//LOLO Let's remind that this is all right with the condition that Monitor objects are never removed!!!!!
	flashListMonitorRegistry_.unlock();
	///////////////////////////////////////

        LOG4CPLUS_DEBUG(this->getApplicationLogger(), "processing pulse for flashlist: " << flashListName << " and tag: " << tagName);

	// resolve flash list
	//
	std::map<std::string, std::vector<xmas::ItemDefinition*> > items;
	std::map<std::string, std::vector<xdata::InfoSpace*> > infospaces;

	xdata::getInfoSpaceFactory()->lock();
	monitor->incrementPulseCounter();
	if (monitor->resolveMultipleMetrics(items, infospaces))
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "resolved metrics #items: " << items.size() << " and #infospaces: " << infospaces.size());
		// 
		// Detected infospace, try to retrieve
		//

		xdata::Table::Reference t;
		try
		{
			t = monitor->retrieveMultipleMetrics(items, infospaces);
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "retrieved metrics #rows: " << t->getRowCount());
		}
		catch (xmas::probe::exception::Exception & e)
		{
			xdata::getInfoSpaceFactory()->unlock();
			std::stringstream msg;
			msg << "Failed to process user infospace change event for flashlist '" << flashListName << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);

			///////////////////////////////////////
			//flashListMonitorRegistry_.unlock();
			///////////////////////////////////////
			return;
		}

		if (t.isNull())
		{
			xdata::getInfoSpaceFactory()->unlock();
			std::stringstream msg;
			msg << "At least one infosapce item is required for flashlist: '" << flashListName << "'";
			XCEPT_DECLARE(xmas::probe::exception::MissingInfoSpaceDeclaration, e, msg.str());
			this->notifyQualified("fatal", e);

			///////////////////////////////////////
			//flashListMonitorRegistry_.unlock();
			///////////////////////////////////////
			return;
		}

		try
		{
			monitor->addFunctionMetrics(t);
		}
		catch (xmas::probe::exception::Exception & e)
		{
			xdata::getInfoSpaceFactory()->unlock();

			std::stringstream msg;
			msg << "Cannot evaluate functions for flashlist '" << flashListName << "'";

			XCEPT_DECLARE_NESTED(xmas::probe::exception::EvaluationFailed, ex, msg.str(),e);
			this->notifyQualified("fatal", ex);

			///////////////////////////////////////
			//flashListMonitorRegistry_.unlock();
			///////////////////////////////////////
			return;
		}

		xdata::getInfoSpaceFactory()->unlock();

		xmas::probe::MonitorReportEvent * ep = new xmas::probe::MonitorReportEvent(monitor->getFlashListDefinition());
		ep->addMetrics(toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt), t);
		ep->setProperty("tag", tagName);
		toolbox::task::EventReference e(ep);

		try
		{
			// deliver to samplers
			scheduler_.fireEvent(e);
		}
		catch (toolbox::task::exception::Overflow & e)
		{
			monitor->incrementInternalLossCounter();

			std::stringstream msg;
			msg << "Failed to dispatch monitor report for flashlist '" << flashListName << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("warning", q);
		}
		catch (toolbox::task::exception::OverThreshold & e)
		{
			// report is lost, keep counting
			monitor->incrementInternalLossCounter();
		}
		catch (toolbox::task::exception::InternalError & e)
		{
			// dead band ignore
			monitor->incrementInternalLossCounter();

			std::stringstream msg;
			msg << "Failed to dispatch monitor report for flashlist '" << flashListName << "'";
			XCEPT_DECLARE_NESTED(xmas::probe::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
		}
	}
	else
	{
		// ignores, no metrics found nothing to report
		xdata::getInfoSpaceFactory()->unlock();
	}

	///////////////////////////////////////
	//flashListMonitorRegistry_.unlock();
	///////////////////////////////////////
}

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------


void xmas::probe::Application::InfospacesTabPage (xgi::Output * out)
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	xdata::getInfoSpaceFactory()->lock();

	std::string browse = url;
	browse += "/Default";

	std::stringstream jsURL;
	jsURL << getApplicationContext()->getContextDescriptor()->getURL() << "/" << getApplicationDescriptor()->getURN();

	try
	{
		*out << cgicc::form().set("method", "GET").set("action", browse).set("data-url", jsURL.str()) << std::endl;
		std::vector<xdata::InfoSpace*> spaces;

		std::map<std::string, xdata::Serializable *, std::less<std::string> > iss = xdata::getInfoSpaceFactory()->match("(.*)");
		for (std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator it = iss.begin(); it != iss.end(); ++it)
		{
			spaces.push_back(dynamic_cast<xdata::InfoSpace *>((*it).second));
		}

		*out << "InfoSpace" << std::endl;
		*out << cgicc::select().set("name", "infospace").set("id", "infospace") << std::endl;
		if ("Any" == selectedInfoSpace_)
		{
			*out << cgicc::option("Any").set("selected") << std::endl;
		}
		else
		{
			*out << cgicc::option("Any") << std::endl;
		}
		for (std::vector<xdata::InfoSpace*>::size_type j = 0; j < spaces.size(); j++)
		{
			if (selectedInfoSpace_ == spaces[j]->name())
			{
				*out << cgicc::option(spaces[j]->name()).set("selected") << std::endl;
			}
			else
			{
				*out << cgicc::option(spaces[j]->name()) << std::endl;
			}
		}
		*out << cgicc::select() << std::endl;

		*out << " | Item" << std::endl;
		*out << cgicc::select().set("name", "mode").set("id", "mode") << std::endl;
		for (std::vector<std::string>::size_type j = 0; j < monitorableSearchModes_.size(); j++)
		{
			if (monitorableSearchModes_[j] == selectedMonitorableSearchMode_)
			{
				*out << cgicc::option(monitorableSearchModes_[j]).set("selected") << std::endl;
			}
			else
			{
				*out << cgicc::option(monitorableSearchModes_[j]) << std::endl;
			}
		}

		*out << cgicc::select() << std::endl;

		*out << cgicc::input().set("type", "text").set("name", "search").set("id", "search").set("value", selectedMonitorableSearchString_) << std::endl;
		//

		std::string regexpr = "(.*)";

		if (selectedMonitorableSearchString_ != "")
		{
			if (selectedMonitorableSearchMode_ == "starts with")
			{
				regexpr = "^";
				regexpr += selectedMonitorableSearchString_;
			}
			else if (selectedMonitorableSearchMode_ == "ends with")
			{

				regexpr = selectedMonitorableSearchString_;
				regexpr += "$";
			}
			else if (selectedMonitorableSearchMode_ == "contains")
			{
				regexpr += selectedMonitorableSearchString_;
				regexpr += "(.*)";
			}

		}

		//
		*out << cgicc::input().set("id", "xdaq-xmas-probe-infospace-submit").set("type", "submit").set("value", "Browse") << std::endl;
		*out << cgicc::form();
		*out << cgicc::br();

		*out << cgicc::br();

		*out << cgicc::div().set("id", "xdaq-xmas-probe-infospaces-content");

		this->getInfoSpaceTable(0, out);

		*out << cgicc::div();

		*out << "	<script type=\"text/javascript\" src=\"/xmas/probe/html/js/xdaq-xmas-probe.js\"></script>" 					<< std::endl;

	}
	catch (xdata::exception::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "failed to search infospaces", e);
	}
	catch (xcept::Exception& e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Unexpected XDAQ exception raised in user code", e);
	}
	catch (std::exception& e)
	{
		std::string msg = "Unexpected C++ standard exception raised in user code: ";
		msg += e.what();
		XCEPT_RAISE(xgi::exception::Exception, msg);
	}
	catch (...)
	{
		std::string msg = "Unexpected unknown raised in user code. No further information available, please check user code";
		XCEPT_RAISE(xgi::exception::Exception, msg);
	}

	//*out << cgicc::tr();
	//*out << cgicc::td("TBD").set("style","vertical-align: top; font-weight: normal; background-color: rgb(255,255,255);") << std::endl;
	//*out << cgicc::tr() << std::endl;

	xdata::getInfoSpaceFactory()->unlock();

}

void xmas::probe::Application::getInfoSpaceTable (xgi::Input * in, xgi::Output * out) 
{
	/* Just in case */
	cgicc::table::reset();
	cgicc::thead::reset();
	cgicc::tbody::reset();
	cgicc::tfoot::reset();
	cgicc::tr::reset();
	cgicc::td::reset();

	if (in != 0)
	{
		cgicc::Cgicc cgi(in);
		cgicc::form_iterator rl;

		rl = cgi.getElement("mode");
		if (rl != cgi.getElements().end())
		{
			selectedMonitorableSearchMode_ = cgi["mode"]->getValue();
		}

		rl = cgi.getElement("infospace");
		if (rl != cgi.getElements().end())
		{
			selectedInfoSpace_ = cgi["infospace"]->getValue();
		}

		rl = cgi.getElement("search");
		if (rl != cgi.getElements().end())
		{
			selectedMonitorableSearchString_ = cgi["search"]->getValue();
		}
	}

	xdata::getInfoSpaceFactory()->lock();

	std::vector<xdata::InfoSpace*> spaces;

	std::map<std::string, xdata::Serializable *, std::less<std::string> > iss = xdata::getInfoSpaceFactory()->match("(.*)");
	for (std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator it = iss.begin(); it != iss.end(); ++it)
	{
		spaces.push_back(dynamic_cast<xdata::InfoSpace *>((*it).second));
	}

	std::string regexpr = "(.*)";

	if (selectedMonitorableSearchString_ != "")
	{
		if (selectedMonitorableSearchMode_ == "starts with")
		{
			regexpr = "^";
			regexpr += selectedMonitorableSearchString_;
		}
		else if (selectedMonitorableSearchMode_ == "ends with")
		{

			regexpr = selectedMonitorableSearchString_;
			regexpr += "$";
		}
		else if (selectedMonitorableSearchMode_ == "contains")
		{
			regexpr += selectedMonitorableSearchString_;
			regexpr += "(.*)";
		}

	}

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Infospace").set("class", "xdaq-sortable").set("style", "min-width: 400px;");
	*out << cgicc::th("Item").set("class", "xdaq-sortable");
	*out << cgicc::th("Type").set("style", "min-width: 10%;");
	*out << cgicc::th("Value");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	try
	{

		if (selectedFlashList_ == "Any")
		{
			for (std::vector<xdata::InfoSpace*>::size_type i = 0; i < spaces.size(); i++)
			{

				if ((selectedInfoSpace_ == "Any") || (spaces[i]->name() == selectedInfoSpace_))
				{
					std::map<std::string, xdata::Serializable *, std::less<std::string> > serializables = spaces[i]->match(regexpr);

					for (std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator j = serializables.begin(); j != serializables.end(); j++)
					{

						*out << cgicc::tr();
						*out << cgicc::td(spaces[i]->name()) << std::endl;
						*out << cgicc::td((*j).first) << std::endl;
						*out << cgicc::td((*j).second->type()) << std::endl;
						if (((*j).second->type() != "properties") && ((*j).second->type() != "table") && ((*j).second->type() != "mime") && ((*j).second->type() != "vector") && ((*j).second->type() != "bag"))
						{
							*out << cgicc::td((*j).second->toString()) << std::endl;

						}
						else
						{

							*out << cgicc::td("complex value") << std::endl;

						}
						*out << cgicc::tr() << std::endl;
					}
				}

			}
		}
	}
	catch (xdata::exception::Exception& e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Failed to get infospace table", e);
	}

	*out << cgicc::tbody();
	*out << cgicc::table();

	xdata::getInfoSpaceFactory()->unlock();
}

void xmas::probe::Application::FlashlistsTabPage (xgi::Output * out)
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable");
	*out << cgicc::th("Version");
	*out << cgicc::th("Matching Infospaces");
#ifdef CHANGING
	*out << cgicc::th("Topic").set("class", "xdaq-sortable");
#endif
	*out << cgicc::th("More Info");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	flashListMonitorRegistry_.lock();
	xdata::getInfoSpaceFactory()->lock();

	std::vector<xmas::probe::FlashListMonitor*> monitors = flashListMonitorRegistry_.getFlashListMonitors();
	std::vector<xmas::probe::FlashListMonitor*>::iterator i;
	for (i = monitors.begin(); i != monitors.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td((*i)->getFlashListName())  << std::endl;
		std::string version = (*i)->getFlashListDefinition()->getProperty("version");
		std::string name = (*i)->getFlashListDefinition()->getProperty("name");
#ifdef CHANGING
		std::string topic = splitup_->getTopic(name);
#endif
		*out << cgicc::td(version)  << std::endl;
		std::map<std::string, std::vector<xmas::ItemDefinition*> > items;
		std::map<std::string, std::vector<xdata::InfoSpace*> > infospaces;

		//std::cout << "GOING TO RESOLVE MULTIPLE METRICS FOR -->" << (*i)->getFlashListName() << std::endl;
		size_t rows = 0;
		if ((*i)->resolveMultipleMetrics(items, infospaces))
		{
			// calculate rows (cartesian product)
			rows = 1;
			for (std::map<std::string, std::vector<xdata::InfoSpace*> >::iterator j = infospaces.begin(); j != infospaces.end(); j++)
			{
				rows *= (*j).second.size();
			}
		}
		else
		{
			rows = 0;
		}

		std::stringstream msg;
		try
		{
			(void) (*i)->retrieveMultipleMetrics(items, infospaces);
		}
		catch (xmas::probe::exception::Exception & e)
		{
			msg << xcept::htmlformat_exception_history(e);
		}

		if (rows == 0)
		{
			*out << cgicc::td("0").set("class", "xdaq-color-red") << std::endl;
		}
		else
		{
			std::stringstream entry;
			entry << rows;
			*out << cgicc::td(entry.str()).set("class", "xdaq-color-green") << std::endl;
		}
#ifdef CHANGING
		*out << cgicc::td(topic) << std::endl;
#endif
		*out << cgicc::td(msg.str()) << std::endl;

		*out << cgicc::tr() << std::endl;
	}

	xdata::getInfoSpaceFactory()->unlock();
	flashListMonitorRegistry_.unlock();

	*out << cgicc::tbody();
	*out << cgicc::table();

}

void xmas::probe::Application::StatisticsTabPage (xgi::Output * out)
{


	*out << cgicc::table().set("class","xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Network Status");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td();
	if (sensorMessengerCache_ == 0)
	{
		*out << "idle";
	}
	else
	{
		if (!sensorMessengerCache_->hasMessenger(sensordURL_))
		{
			*out << "no messenger";

		}
		else
		{
			*out << "ready";
		}
	}

	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Network Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Network Loss Counter";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingReportLostCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	/*
	 *out << cgicc::tr().set("id", "properties");
	 *out << cgicc::td().set("id", "properties_name");
	 *out << "Heartbeat Loss Counter";
	 *out << cgicc::td();
	 *out << cgicc::td().set("id", "properties_value");
	 *out << outgoingHeartbeatLostCounter_;
	 *out << cgicc::td();
	 *out << cgicc::tr() << std::endl;
	 */

	// Total Report Enqueued 
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total Enqueued Reports";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << outgoingReportCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Total Heartbeat Enqueued 
	/*
	 *out << cgicc::tr().set("id", "properties");
	 *out << cgicc::td().set("id", "properties_name");
	 *out << "Total Enqueued Heartbeat";
	 *out << cgicc::td();
	 *out << cgicc::td().set("id", "properties_value");
	 *out << outgoingHeartbeatCounter_;
	 *out << cgicc::td();
	 *out << cgicc::tr() << std::endl;
	 */

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	// Per flashlist loss of reports

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");
	*out << cgicc::caption("Flashlist");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist");
	*out << cgicc::th("Fire Counter");
	*out << cgicc::th("Pulse Counter");
	*out << cgicc::th("Internal Loss");
	*out << cgicc::th("Enqueuing Loss");
	*out << cgicc::th("Out of Memory Loss");
	*out << cgicc::th("No Topic Loss");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	flashListMonitorRegistry_.lock();

	xdata::UnsignedInteger64 pulseCounter = 0;
	xdata::UnsignedInteger64 fireCounter = 0;
	xdata::UnsignedInteger64 internalLossCounter = 0;
	xdata::UnsignedInteger64 communicationLossCounter = 0;
	xdata::UnsignedInteger64 memoryLossCounter = 0;
	xdata::UnsignedInteger64 unassignedLossCounter = 0;

	std::vector<xmas::probe::FlashListMonitor*> monitors = flashListMonitorRegistry_.getFlashListMonitors();
	std::vector<xmas::probe::FlashListMonitor*>::iterator i;
	for (i = monitors.begin(); i != monitors.end(); i++)
	{
		std::list<xmas::probe::Sampler*> samplers = (*i)->getSamplers();
		xdata::UnsignedInteger32 rows = samplers.size();

		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i)->getFlashListName()).set("rowspan", rows.toString()) << std::endl;

		xdata::UnsignedInteger64 num;
		num = (*i)->getFireCounter();
		fireCounter = fireCounter + num;
		*out << cgicc::td(num.toString()).set("rowspan", rows.toString()) << std::endl;

		num = (*i)->getPulseCounter();
		pulseCounter = pulseCounter + num;
		*out << cgicc::td(num.toString()).set("rowspan", rows.toString()) << std::endl;

		num = (*i)->getInternalLossCounter();
		internalLossCounter = internalLossCounter + num;
		*out << cgicc::td(num.toString()).set("rowspan", rows.toString()) << std::endl;

		num = (*i)->getCommunicationLossCounter();
		communicationLossCounter = communicationLossCounter + num;
		*out << cgicc::td(num.toString()).set("rowspan", rows.toString()) << std::endl;

		num = (*i)->getMemoryLossCounter();
		memoryLossCounter = memoryLossCounter + num;
		*out << cgicc::td(num.toString()).set("rowspan", rows.toString()) << std::endl;

		num = (*i)->getUnassignedLossCounter();
		unassignedLossCounter = unassignedLossCounter + num;
		*out << cgicc::td(num.toString()).set("rowspan", rows.toString()) << std::endl;

		*out << cgicc::tr() << std::endl;
	}

	flashListMonitorRegistry_.unlock();

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Total") << std::endl;

	*out << cgicc::td(fireCounter.toString()) << std::endl;

	*out << cgicc::td(pulseCounter.toString()) << std::endl;

	*out << cgicc::td(internalLossCounter.toString()) << std::endl;

	*out << cgicc::td(communicationLossCounter.toString()) << std::endl;

	*out << cgicc::td(memoryLossCounter.toString()) << std::endl;

	*out << cgicc::td(unassignedLossCounter.toString()) << std::endl;

	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void xmas::probe::Application::ServicesTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");
	*out << cgicc::caption("Scanned services");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Service");
	*out << cgicc::th("Scanned");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();
	for ( std::map<std::string,unsigned int>::iterator i = configuredServices_.begin(); i != configuredServices_.end(); i++)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).first) << std::endl;
		*out << cgicc::td() << (*i).second << cgicc::td() << std::endl;
		*out << cgicc::tr() << std::endl;
	}
	*out << cgicc::tbody();
	*out << cgicc::table();
}

void xmas::probe::Application::Default (xgi::Input * in, xgi::Output * out) 
{
	try
	{
		cgicc::Cgicc cgi(in);
		cgicc::form_iterator rl;

		rl = cgi.getElement("mode");
		if (rl != cgi.getElements().end())
		{
			selectedMonitorableSearchMode_ = cgi["mode"]->getValue();
		}

		rl = cgi.getElement("infospace");
		if (rl != cgi.getElements().end())
		{
			selectedInfoSpace_ = cgi["infospace"]->getValue();
		}

		rl = cgi.getElement("search");
		if (rl != cgi.getElements().end())
		{
			selectedMonitorableSearchString_ = cgi["search"]->getValue();
		}

		*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

		// Tabbed pages

		*out << "<div class=\"xdaq-tab\" title=\"Flashlists\">" << std::endl;
		this->FlashlistsTabPage(out);
		*out << "</div>";

		*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
		this->StatisticsTabPage(out);
		*out << "</div>";

		*out << "<div class=\"xdaq-tab\" title=\"Infospaces\">" << std::endl;
		this->InfospacesTabPage(out);
		*out << "</div>";

		*out << "<div class=\"xdaq-tab\" title=\"Services\">" << std::endl;
		this->ServicesTabPage(out);
		*out << "</div>";

		*out << "</div>";
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}
