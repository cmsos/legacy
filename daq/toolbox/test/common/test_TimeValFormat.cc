#include <iostream>
#include <string>
#include "toolbox/TimeVal.h"

int main() 
{ 
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

	std::cout << "Now in GMT: " << now.toString("", toolbox::TimeVal::gmt) << std::endl;
	std::cout << "Now in loc: " << now.toString("", toolbox::TimeVal::loc) << std::endl;
	std::cout << std::endl;

	std::string timeUTC = "2006-06-01T15:15:15Z";
	toolbox::TimeVal parsedTimeUTC;
	parsedTimeUTC.fromString(timeUTC, "", toolbox::TimeVal::gmt);
	std::cout << "Read UTC time: " << timeUTC << std::endl;
	std::cout << "Parsed in UTC: " << parsedTimeUTC.toString("", toolbox::TimeVal::gmt) << std::endl;
	std::cout << "Parsed in loc: " << parsedTimeUTC.toString("", toolbox::TimeVal::loc) << std::endl;
	std::cout << std::endl;

	toolbox::TimeVal parsedTimeLOC;
	std::string timeLOC = "2006-06-01T17:15:15-02:00";
	parsedTimeLOC.fromString(timeLOC, "", toolbox::TimeVal::gmt);
	std::cout << "Read LOC time: " << timeLOC << std::endl;
	std::cout << "Parsed in UTC: " << parsedTimeLOC.toString("", toolbox::TimeVal::gmt) << std::endl;
	std::cout << "Parsed in loc: " << parsedTimeLOC.toString("", toolbox::TimeVal::loc) << std::endl;
	return 0 ; 
} 
