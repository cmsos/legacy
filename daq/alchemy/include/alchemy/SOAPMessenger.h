// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _nodejs_SOAPMessenger_h
#define _nodejs_SOAPMessenger_h


#include "xoap/MessageReference.h"
#include "pt/SOAPMessenger.h"
#include "pt/exception/Exception.h"
#include "pt/Address.h"
#include "toolbox/BSem.h"

//#include "Channel.h"

#include "log4cplus/logger.h"

using namespace log4cplus;

namespace nodejs
{

	class HTTPSender;

	class SOAPMessenger: public pt::SOAPMessenger
	{
		public:
			
			SOAPMessenger(Logger & logger, pt::Address::Reference destination, pt::Address::Reference local, HTTPSender * sender )
			    throw (pt::exception::Exception);
			
			//! Destructor only deletes channel object
			//
			virtual ~SOAPMessenger();
			
			pt::Address::Reference getLocalAddress();
			pt::Address::Reference getDestinationAddress();
			
			xoap::MessageReference send (xoap::MessageReference message) throw (pt::exception::Exception);
			
		private:
				
			//Channel* channel_;
			pt::Address::Reference local_;
			pt::Address::Reference destination_;
			Logger logger_;
		public:
			HTTPSender * sender_;
			toolbox::BSem mutex_;


	};


}

#endif
