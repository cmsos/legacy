/*
  frl.c
 
 Main source for frl library.

 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: frl.c,v 1.4 2004/11/04 09:36:51 cschwick Exp $

*/
static char *rcsid_frl_c = "@(#) $Id: frl.c,v 1.4 2004/11/04 09:36:51 cschwick Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_frl_c);
#endif
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "frl.h"
#include "frl-private.h"
#include "frl_data_fifo.h"
#include "frl_wc_fifo.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>

/*** debug purposes */
#include <syslog.h>
#include <unistd.h>
//#include <kalloc_tracker.h>

#ifndef NO_FRL_PTHREAD
#ifndef __KERNEL__
#include <pthread.h>
#endif
#endif


/**
 * @function _frl_apply_to_mask
 *  
 * This function shifts a data word to a bitfield indicated by mask.
 *
 * @param mask a mask
 * @param *data a pointer to data which has to be shifted into mask
 * @return -1 in case bits are set outside the mask, 0 otherwise
 */
int _frl_apply_to_mask ( U32 mask, U32 *data ) {

  U32 help, ldata;
  ldata = *data;

  // trivial case:
  if ( mask == 0 ) {
    *data = 0;
    return 0;
  }

  // shifting
  help = mask;
  while ( (help & 0x00000001) == 0 ) {
      help  >>= 1;
      ldata <<= 1;
  }

  // bits set out of the mask
  if ( (~mask & ldata) != 0 ) {
    return -1;
  }

  *data = ldata;
  return 0;
}

/**
 * @function _frl_error
 * @param code the error code
 * @param status pointer to the status (could nbe NULL)
 * this function is private to the imlementation of frl
 */
inline void _frl_error (int code, int * status)
{
    if (status)
        *status = code;
}

/**
 * @function _frl_enable_link
 * @param rec : the frl receiver strucuture
 */
inline void _frl_enable_link (struct frl_receiver * rec)
{
    rec->map[_FRL1_CSR/4] = (rec->map[_FRL1_CSR/4] & ~_FRL1_LINK_CTRL) | rec->link_enable_mask;
    rec->link_enabled = rec->link_enable_mask;
}

/**
 * @function _frl_disable_link
 * @param rec : the frl receiver strucuture
 */
inline void _frl_disable_link (struct frl_receiver * rec)
{
    rec->map[_FRL1_CSR/4] &= ~_FRL1_LINK_CTRL;
    rec->link_enabled = 0;
}

/** 
 * @function _frl_set_board_block_sizes
 */
 
inline void _frl_set_board_block_sizes (struct frl_receiver * rec)
{
    rec->map[_FRL_BKSZ_OFFSET/4] = rec->block_size - rec->header_size;
}

/**
 * @function _frl_free_frag
 * frees the fragment strucuture itself. This function suposes the data blocks
 * are already freed. This will just free the data table and return the fragment 
 * structure in the free fragment pool of the receiver structure.
 * @param frag the fragment
 * @param receiver the receiver
 */
inline void _frl_free_frag (struct frl_fragment * frag, struct frl_receiver * receiver)
{
    idprintf ("Releasing fragment 0x%08x\n", (int)frag);
    if (frag->data_blocks != NULL) {
        free (frag->data_blocks);
        frag->data_blocks = NULL;
    }
    frag->block_number = -1;
#ifndef NO_FRL_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    frag->next = receiver->free_fragments;
    receiver->free_fragments = frag;
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
}

/**
 * @function _frl_alloc_frag
 * @param block_number : the number of blocks the framgnet will have to hold
 * @param receiver pointer to the receiver structure
 */
inline struct frl_fragment * _frl_alloc_frag (struct frl_receiver * receiver, int block_number)
{
    struct frl_fragment * ret = NULL;
	
    /* first find a structure */
#ifndef NO_FRL_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    if (receiver->free_fragments != NULL) {
        ret = receiver->free_fragments;
        receiver->free_fragments = ret->next;
        ret->next=NULL;
        idprintf ("Recycling fragment 0x%08x\n", (int) ret);
    } else {
        ret = (struct frl_fragment *)malloc(sizeof(struct frl_fragment)); 
        idprintf ("New fragment in game 0x%08x\n", (int) ret);
    }
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
	
    /* now allocate space to hold the pointers to data buffers (malloc all the time... improvable?) */
    if (ret != NULL) {
        ret->data_blocks = (struct _frl_data_block **) malloc (sizeof (struct _frl_data_block *[block_number]));
        if (ret->data_blocks == NULL) {
            _frl_free_frag (ret, receiver);
            ret = NULL;
        }
    }
    return ret;
}


/**
 * @function frl_open
 * @return pointer to the frl struct or NULL in case of failure
 * @param index index to the board
 * @param status pointer to the integer holding status information
 */
struct frl_receiver * frl_open (int index, int * status) /* native version */
{
    struct frl_receiver * rec;
    char filename [100];
    struct _frl_board_addresses addresses;
	
    /* First, allocation */
    rec = malloc (sizeof (struct frl_receiver));
#ifndef NO_FRL_PTHREAD
    pthread_mutex_init (&(rec->global_mutex), NULL);
    pthread_mutex_lock (&(rec->global_mutex));
#endif
    if (rec == NULL) {
        eprintf ("Could not allocate frl_receiver structure\n");
        _frl_error (FRL_out_of_memory, status);
        goto FRLO_alloc;
    }
    
    /* establish contact with the driver */
    sprintf (filename, "/dev/frl%d", index);
    rec->fd = open (filename, O_RDWR);
    if (rec->fd == -1) {
        eprintf ("Could not open frl-receiver special file\n");
        perror (filename);
        _frl_error (FRL_IOerror, status);
        goto FRLO_open;
    }
    /* The fact of opening the file succesfully means that the
       board is present, and now reserved */
    
    /* we now have to get the base addresses and to map them to the userspace (open step) */
    /* get base addresses */
    if (-1 == ioctl (rec->fd, FRL_GET_BOARD_ADDRESSES, &addresses)) {
        eprintf ("Could not get adresses of receiver\n");
        perror (filename);
        _frl_error (FRL_IOerror, status);
        goto FRLO_addresses;
    }
    rec->base_address  = addresses.base_address_0;
    iprintf( " rec->base_address is %x\n",  rec->base_address);
    rec->base1_address = addresses.base1_address_0;
    rec->FPGA_version  = addresses.FPGA_version;
    rec->FPGA1_version  = addresses.FPGA1_version;
    rec->index = index;
    rec->suspended = 0;
    rec->blocksInSpy = 0;
    /* mmap the base addresses */
    rec->map = (U32*)mmap (NULL, _FRL_BAR0_range, PROT_READ | PROT_WRITE, MAP_SHARED, rec->fd, 
                           (__off_t) rec->base_address);
    if (rec->map == MAP_FAILED) {
        eprintf ("Could not map base address0 of bridge fpga\n");
        perror (filename);
        _frl_error (FRL_IOerror, status);
        goto FRLO_mmap;        
    }
        
    rec->map1 = (U32*)mmap (NULL, _FRL1_BAR0_range, PROT_READ | PROT_WRITE, MAP_SHARED, 
			   rec->fd, (__off_t) rec->base1_address);
    if (rec->map == MAP_FAILED) {
      eprintf ("Could not map base address0 of FRL fpga\n");
        perror (filename);
        _frl_error (FRL_IOerror, status);
        goto FRLO_mmap1;        
    }
        
    /* first operation, software reset */
    idprintf ("Will reset\n");
    rec->map[_FRL_CSR_OFFSET/4] |= _FRL_CSR_SOFT_RESET;
    rec->map1[_FRL1_CSR/4] |= _FRL1_SOFT_RESET;
    idprintf ("Have issued reset to both FPGAs\n");

    /* block allocation et al. will be done in frl_start  after parameter setting by user */
	
    rec->block_size = FRL_DEFAULT_BLOCK_SIZE;
    rec->block_number = FRL_DEFAULT_BLOCK_NUMBER;
    rec->header_size = FRL_DELAULT_HEADER_SIZE;
    rec->started = 0;
	
    rec->free_fragments = NULL;
	
    rec->link_enabled = 0;
	
    // set a default receive pattern (link 0)
    frl_set_receive_pattern ( rec, 0x01 );

    rec->noalloc = 0;
	
    rec->free_data_block_descriptors = NULL;
    rec->allocated_data_blocks = NULL;
    rec->data_blocks_in_board = NULL;
    rec->data_blocks_physical = NULL;
    rec->data_fifo_physical = NULL;
    rec->data_fifo_user = MAP_FAILED;
    rec->data_blocks_user = MAP_FAILED;
    rec->wc_fifo_physical = NULL;
    
    /* now we initialise the headers */
    rec->data_blocks_in_board_insert = &(rec->data_blocks_in_board);
	
    rec->wc_fifo = MAP_FAILED;
    rec->data_fifo = NULL;
    rec->data_fifo_tab = NULL;
    /*rec->pmap_list = NULL;*/
    rec->data_list =NULL;
    
    idprintf ("\n");
	
#ifdef DEBUG_DATA_POINTERS
    rec->last_pointer_in = 0xFFFFFFFF;
    rec->min_pointer_in = 0xFFFFFFFF;
    rec->max_pointer_in = 0xFFFFFFFF;
    rec->last_pointer_out = 0xFFFFFFFF;
    rec->pointer_in_count = 0;
    rec->pointer_out_count = 0;
    iprintf ("Just initialised the pointer checking structure\n");
#endif
	
    rec->frag_header_size = FRL_HEADER_SIZE;
    rec->frag_trailer_size = FRL_TRAILER_SIZE;
	
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(rec->global_mutex));
#endif
    return rec;
    
    /* error conditions unpiling */
FRLO_mmap1:
    /* unmap the first base address */
    if (rec->map != MAP_FAILED) munmap ((void *)rec->map, _FRL_BAR0_range);
FRLO_mmap:
FRLO_addresses:
    close (rec->fd);
FRLO_open:
    memset (rec, 0xEC, sizeof (struct frl_receiver));
    /* no need to unlock mutex since it is destroyed in the free  */
    free (rec);
FRLO_alloc:
    return NULL;
}



/**
 * @function _frl_alloc_data_fifo allocates the data FIFO
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int _frl_handle_data_fifo (struct frl_receiver *receiver)
{
    U32 shared_fifo_size = 0;
    int ret = FRL_OK;

    shared_fifo_size = (receiver->block_number +1) * sizeof (void *) + sizeof (struct _FRL_data_fifo);

    if (receiver->data_fifo_user == NULL || receiver->data_fifo_user == MAP_FAILED) {
        eprintf ("in  _frl_handle_data_fifo data fifo was not allocated\n");
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLHDF_notalloc;
    }
	
    /* fifo allocated in the dbuff we have to pass its address to the driver.
     * we will comunicate with interrupt service routine through this struct.
     * the struct is located at the end of the data blocks
     */
    receiver->data_fifo = (struct _FRL_data_fifo *) (receiver->data_fifo_user +
                                                    (receiver->block_number + 1) * sizeof (void *));
    receiver->data_fifo->read = 0;
    receiver->data_fifo->write = 0;
    receiver->data_fifo->done_read = 0;
    receiver->data_fifo->done_write = 0;
    receiver->data_fifo->int_disabled = 1;
    receiver->data_fifo->size = (receiver->block_number + 1);
    receiver->data_fifo_tab = receiver->data_fifo_user;
    return 0;
FRLHDF_notalloc:
    return ret;
}

/**
 * @function _frl_alloc_data_blocks allocates the free blocks and feeds them to the FIFO
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 * This function is not called when using the noalloc version of frl_start
 */
int _frl_handle_data_blocks (struct frl_receiver *receiver)
{
    U32 block_area_size = 0;
    int ret = 0;
    int i;
	
    block_area_size = receiver->block_size * receiver->block_number;
	
    /* check that data zone was allocated */
    if (receiver->data_blocks_user == NULL || receiver->data_blocks_user == MAP_FAILED) {
        eprintf ("in _frl_handle_data_blocks : data buffers were not allocated\n");
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLHDB_free_FIFO;
    }
    for (i=0; i< receiver->block_number; i++) {
        struct _frl_data_list * temp_list;
        struct _frl_data_block * temp_block;
		
        /* allocate the data block structure */
        temp_block = *(receiver->data_blocks_in_board_insert) = malloc (sizeof (struct _frl_data_block));
        if (*(receiver->data_blocks_in_board_insert) == NULL) {
            eprintf ("frl failure while allocating data block structures\n");
            _frl_error (FRL_out_of_memory, &ret);
            goto FRLHDB_free_data_blocks;
        }
		
        /* store all information in block */
        temp_block->user_address = (U32 *)((int)receiver->data_blocks_user + i * receiver->block_size);
        /* bus address if for board use only. it points to data zone, after header */
        temp_block->bus_address = (void *)((int)receiver->data_blocks_physical + i * receiver->block_size); 
        /* kernel address no used in frl -> don't care */
        temp_block->kernel_address = NULL;
        /* unused user_handle (this is for noalloc scheme) */
        temp_block->user_handle = NULL;
		
        /* add the block struct in the list - list is only used at deallocation time */
        temp_list = malloc (sizeof (struct _frl_data_list));
        if (temp_list == NULL) {
            free (temp_block);
            eprintf ("frl failure while allocating data block list structures\n");
            _frl_error (FRL_out_of_memory, &ret);
            goto FRLHDB_free_data_blocks;
        }
        temp_list->next = receiver->allocated_data_blocks;
        temp_list->block = temp_block;
        receiver->allocated_data_blocks = temp_list;
		
        /* push the block in the free fifo list (this should not fail) */
        if (_frl_feed_shared_fifo(receiver, (void *)((int)temp_block->bus_address + receiver->header_size))) {
            eprintf ("frl failure while filling the free FIFO (unexpected failure)\n");
            _frl_error (FRL_out_of_memory, &ret);
            goto FRLHDB_free_data_blocks;
        }
		
        /* still have to reference the block in our own queue */
        *(receiver->data_blocks_in_board_insert) = temp_block;
        temp_block->next = NULL;
        receiver->data_blocks_in_board_insert = &(temp_block->next);

    }
#ifdef DEBUG_DATA_POINTERS
    _frl_pointer_ubercheck (receiver, "_frl_alloc_data_blocks");
#endif
    return 0;
	
FRLHDB_free_data_blocks:
    /*_frl_dealloc_data_blocks (receiver);*/
FRLHDB_free_FIFO:
    return -1;
}



/**
 * @function _frl_alloc_datablock_descriptor recycles or allocates a data block descriptor
 * @param receiver pointer to the reciever structure
 * @returns pointer to struct _frl_data_block
 */
 
struct _frl_data_block * _frl_alloc_datablock_descriptor (struct frl_receiver * receiver)
{
    struct _frl_data_block * ret;
    if (receiver->free_data_block_descriptors) {
        ret = receiver->free_data_block_descriptors;
        receiver->free_data_block_descriptors = ret->next;
    } else {
        ret = (struct _frl_data_block * )malloc (sizeof (struct _frl_data_block));
    }
    return ret;
} 


/**
 * @function frl_start
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int frl_start (struct frl_receiver *receiver) /* native version */
{
    int ret = 0;
    struct _frl_receiver_parameters params;
    struct _frl_memory_blocks mem_blocks;
    
    if (receiver == NULL) {
        eprintf ("NULL receiver\n");
        _frl_error (FRL_IOerror, &ret);
        goto FRLS_open;
    }
#ifndef NO_FRL_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    idprintf ("\n");
    if (receiver->started != 0) {
        eprintf ("frl receiver already started\n");
        _frl_error (FRL_IOerror, &ret);
        goto FRLS_lock;
    }
    /* we start ! no subsequent changes on the block sizes */
    idprintf ("\n");
    receiver->started = 1;
    receiver->noalloc = 0;
	
    /* we set the block size in the board (block size - header size) */
    idprintf ("will set block sizes\n");
    /*sleep (1);*/
    _frl_set_board_block_sizes (receiver);
    /* Pass the parameters to the driver */
    params.block_number = receiver->block_number;
    params.block_size = receiver->block_size - receiver->header_size;
    params.do_allocate_blocks = 1;
    if (ioctl (receiver->fd, FRL_SETGET_RECEIVER_PARAMS, &params)) {
        _frl_error (FRL_invalid_parameter, &ret);
        goto FRLS_params;
    }
    /* we ask the driver to allocate the blocks in memory for us */
    if (ioctl (receiver->fd, FRL_GET_RECEIVER_MEMBLOCKS, &mem_blocks)) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLS_nomem;
    }
    /* now memory map and take note of the result (we are in alloc mode)*/
    /* sanity check */
    if (mem_blocks.wc_fifo_physical == NULL 
        || mem_blocks.free_blocks_fifo_physical == NULL
        || mem_blocks.data_blocks_physical == NULL) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLS_nomem;
    }
    /* wc_fifo */
    receiver->wc_fifo_physical = mem_blocks.wc_fifo_physical;
    receiver->wc_fifo = mmap (NULL, sizeof (struct _FRL_wc_fifo), PROT_READ | PROT_WRITE,
                              MAP_SHARED, receiver->fd, (__off_t) receiver->wc_fifo_physical);
    if (receiver->wc_fifo == MAP_FAILED) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLS_mmap;
    }
    /* data_fifo the data fifo is prepared by the driver */
    receiver->data_fifo_physical = mem_blocks.free_blocks_fifo_physical;
    receiver->data_fifo_user = mmap (NULL, sizeof (struct _FRL_data_fifo) +
                                     receiver->block_number * sizeof (void *), 
                                     PROT_READ | PROT_WRITE,
                                     MAP_SHARED, receiver->fd, 
                                     (__off_t) receiver->data_fifo_physical);
    if (receiver->data_fifo_user == MAP_FAILED) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLS_mmap;
    }
    receiver->data_fifo = (struct _FRL_data_fifo *) ((void *)receiver->data_fifo_user + 
                                                    (receiver->block_number + 1) * sizeof (void *));
    receiver->data_fifo_tab  = receiver->data_fifo_user;
    /* data_blocks */
    receiver->data_blocks_physical = mem_blocks.data_blocks_physical;
    receiver->data_blocks_user = mmap (NULL, receiver->block_size * receiver->block_number, 
                                       PROT_READ | PROT_WRITE,
                                       MAP_SHARED, receiver->fd, 
                                       (__off_t) receiver->data_blocks_physical);
    if (receiver->data_blocks_user == MAP_FAILED) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLS_mmap;
    }

    if (_frl_handle_data_fifo (receiver)) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLS_data_fifo;
    }

    if (_frl_handle_data_blocks (receiver)) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLS_data_blocks;
    }
    /* data fifo was already registered automatically */
	
    /* take care of the wc fifo */
    receiver->wc_fifo->read = _FRL_wc_fifo_mask;
    receiver->wc_fifo->write = _FRL_wc_fifo_mask;
    /* give address of wc FIFO to board */
    _frl_send_wc_fifo_params_to_board (receiver);
	
    /* we just have to enable the interrupts and the board will start to ask for data blocks */
#ifndef FRL_NO_INTERRUPT
    idprintf ("enabling interrupts\n");
    _frl_enable_FIFO_interrupts (receiver);    
#else
    idprintf ("handling blocks (noint handle blocks)\n");
    _frl_noint_handle_blocks (receiver);
#endif

    // not here but in the other FPGA /* enable the link */
    // _frl_enable_link (receiver);
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
    return ret; /* happy */
	
FRLS_data_blocks:
FRLS_data_fifo:
FRLS_mmap:
    /* something went wrong so we unmap everything that can be unmapped */
    if (receiver->wc_fifo != MAP_FAILED) munmap (receiver->wc_fifo, sizeof (struct _FRL_wc_fifo));
    if (receiver->data_fifo_user != MAP_FAILED) munmap (receiver->data_fifo_user, sizeof (struct _FRL_data_fifo) +
                                                  receiver->block_number * sizeof (void *));
    if (receiver->data_blocks_user != MAP_FAILED) munmap (receiver->data_blocks_user, receiver->block_size * receiver->block_number);
FRLS_nomem:
FRLS_params:
FRLS_lock:
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
FRLS_open:
    return ret;
}

/**
 * @function frl_start_noalloc
 * @return zero on success or negative error code on failure
 * @param receiver the receiver structure used
 */
int frl_start_noalloc (struct frl_receiver *receiver) /* native version */
{
    int ret = 0;
    struct _frl_receiver_parameters params;
    struct _frl_memory_blocks mem_blocks;
    
    if (receiver == NULL) {
        eprintf ("NULL receiver\n");
        _frl_error (FRL_IOerror, &ret);
        goto FRLSNA_open;
    }
#ifndef NO_FRL_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    idprintf ("\n");
    if (receiver->started != 0) {
        eprintf ("frl receiver already started\n");
        _frl_error (FRL_IOerror, &ret);
        goto FRLSNA_lock;
    }
    /* we start ! no subsequent changes on the block sizes */
    idprintf ("\n");
    receiver->started = 1;
    receiver->noalloc = 1;
	
    /* we set the block size in the board (block size - header size) */
    idprintf ("will set block sizes\n");
    /*sleep (1);*/
    _frl_set_board_block_sizes (receiver);
    /* Pass the parameters to the driver */
    params.block_number = receiver->block_number;
    params.block_size = receiver->block_size - receiver->header_size;
    params.do_allocate_blocks = 0;
    if (ioctl (receiver->fd, FRL_SETGET_RECEIVER_PARAMS, &params)) {
        _frl_error (FRL_invalid_parameter, &ret);
        goto FRLSNA_params;
    }
    /* we ask the driver to allocate the blocks in memory for us */
    if (ioctl (receiver->fd, FRL_GET_RECEIVER_MEMBLOCKS, &mem_blocks)) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLSNA_nomem;
    }
    /* now memory map and take note of the result (we are in alloc mode)*/
    /* sanity check */
    if (mem_blocks.wc_fifo_physical == NULL 
        || mem_blocks.free_blocks_fifo_physical == NULL) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLSNA_nomem;
    }
    /* wc_fifo */
    receiver->wc_fifo_physical = mem_blocks.wc_fifo_physical;
    receiver->wc_fifo = mmap (NULL, sizeof (struct _FRL_wc_fifo), PROT_READ | PROT_WRITE,
                              MAP_SHARED, receiver->fd, (__off_t) receiver->wc_fifo_physical);
    if (receiver->wc_fifo == MAP_FAILED) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLSNA_mmap;
    }
    /* data_fifo don't take care of the dat blocks. Nothing allocated */
    /* receiver->data_blocks_physical = mem_blocks.data_blocks_physical; */
    receiver->data_blocks_physical = NULL;
    receiver->data_blocks_user = MAP_FAILED;
    
    /* Take care of the data blocks FIFO */
    receiver->data_fifo_physical = mem_blocks.free_blocks_fifo_physical;
    receiver->data_fifo_user = mmap (NULL, sizeof (struct _FRL_data_fifo) +
                                     receiver->block_number * sizeof (void *), 
                                     PROT_READ | PROT_WRITE,
                                     MAP_SHARED, receiver->fd, 
                                     (__off_t) receiver->data_fifo_physical);
    if (receiver->data_fifo_user == MAP_FAILED) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLSNA_mmap;
    }

    if (_frl_handle_data_fifo (receiver)) {
        _frl_error (FRL_out_of_DMA_memory, &ret);
        goto FRLSNA_data_fifo;
    }

    /* data fifo was already registered automatically */
	
    /* take care of the wc fifo */
    receiver->wc_fifo->read = _FRL_wc_fifo_mask;
    receiver->wc_fifo->write = _FRL_wc_fifo_mask;
    /* give address of wc FIFO to board */
    _frl_send_wc_fifo_params_to_board (receiver);
	
    /* we just have to enable the interrupts and the board will start to ask for data blocks */
#ifndef FRL_NO_INTERRUPT
    _frl_enable_FIFO_interrupts (receiver);    
#else
    _frl_noint_handle_blocks (receiver);
#endif

    //    not here anymore /* enable the link */
    //    _frl_enable_link (receiver);
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
    return ret; /* happy */
	
FRLSNA_data_fifo:
FRLSNA_mmap:
    /* something went wrong so we unmap everything that can be unmapped */
    if (receiver->wc_fifo != MAP_FAILED) munmap (receiver->wc_fifo, sizeof (struct _FRL_wc_fifo));
    if (receiver->data_fifo_user != MAP_FAILED) munmap (receiver->data_fifo_user, sizeof (struct _FRL_data_fifo) +
                                                  receiver->block_number * sizeof (void *));
    if (receiver->data_blocks_user != MAP_FAILED) munmap (receiver->data_blocks_user, receiver->block_size * receiver->block_number);
FRLSNA_nomem:
FRLSNA_params:
    receiver->started = 0;
FRLSNA_lock:
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
FRLSNA_open:
    return ret;
}




/**
 * @function frl_provide_block
 * @return zero on success or negative error code on failure. 
 * Block is NOT taken care by receiver on failure, user has to 
 * deallocate it or do something with it.
 * @param receiver the receiver strucuture
 * @param user_address pointer to the block in user space
 * @param kernel_address pointer to the block in kernel space
 * @param bus_address pointer to the block as seen from PCI
 * @param user handle free pointer for the user to store his data
 */
int frl_provide_block (struct frl_receiver * receiver, void *user_address, 
                          void *kernel_address, void *bus_address, void *user_handle)
{
    struct _frl_data_block * temp_block;
    int ret = 0;
	
    if (receiver == NULL) {
        eprintf ("Trying to provide blocks to NULL receiver in frl_provide_block\n");
        return FRL_error;
    }

    if (receiver->suspended) {
        eprintf ("Trying to provide blocks to a suspended receiver. Blocks not provided\n");
        return FRL_suspended;
    }
	
    if (receiver->noalloc == 0)
        goto FRLPB_no_noalloc;

#ifndef NO_FRL_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif
    /* allocate the data block structure */
    temp_block = _frl_alloc_datablock_descriptor (receiver);
    if (temp_block == NULL) {
        eprintf ("frl failure while allocating data block structures\n");
        _frl_error (FRL_out_of_memory, &ret);
        goto FRLPB_block_alloc;
    }
	
    /* store all information in block */
    temp_block->user_address = user_address;
    /* bus address if for board use only. it points to data zone, after header */
    temp_block->bus_address = bus_address; 
    /* kernel address */
    temp_block->kernel_address =  kernel_address;
    /* unused user_handle (this is for noalloc scheme) */
    temp_block->user_handle = user_handle;

    /* push the block in the free fifo list (this should not fail) */
    if (_frl_feed_shared_fifo(receiver, (void *)((int)temp_block->bus_address + receiver->header_size))) {
        eprintf ("frl failure while filling the free FIFO in frl_provide_block\n");
        _frl_error (FRL_out_of_memory, &ret);
        free (temp_block);
        goto FRLPB_fifo_full;
    }

    /* still have to reference the block in our own queue */
    *(receiver->data_blocks_in_board_insert) = temp_block;
    temp_block->next = NULL;
    receiver->data_blocks_in_board_insert = &(temp_block->next);
	
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif

#ifdef DEBUG_DATA_POINTERS
    _frl_pointer_ubercheck (receiver, "frl_provide_block");
#endif
#ifndef FRL_NO_INTERRUPT
    _frl_enable_FIFO_interrupts (receiver);
#else
    _frl_noint_handle_blocks (receiver);
#endif
	
FRLPB_no_noalloc:
FRLPB_block_alloc:
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
#endif
FRLPB_fifo_full:
    return ret;
}
						 
/**
 * @function frl_frag_user_handle
 * @return pointer provided by the user for this block or NULL if no handle was provided/index is out of range
 * @param fragment pointer to the framgent
 * @param index index to the block for which the user handle is wanted
 */
void * frl_frag_user_handle (struct frl_fragment * frag, int index)
{
    if (frag == NULL) return NULL;
    if ((index < 0) || (index >= frag->block_number)) return NULL;
    if (frag->data_blocks == NULL) {
        eprintf ("Got NULL pointer as frag->block_number in frl_frag_user_handle.\n");
        return NULL;
    }
    return frag->data_blocks[index]->user_handle;
}

/**
 * @function frl_frag_release_norecycle
 * @param fragment pointer to the fragment structure
 */
void frl_frag_release_norecycle (struct frl_fragment * fragment)
{
    int i;

    idprintf ("frl frag release\n");
    if (fragment == NULL) {
        return;
    } else if (fragment->block_number == -1) {
        eprintf ("Got -1 as block number in frl_frag_release_norecycle. Perhaps a double release of the fragment?\n");
        return;
    } else if (fragment->data_blocks == NULL) {
        eprintf ("Got NULL pointer as frag->block_number in frl_frag_release_norecycle.\n");
        return;
    } else if (fragment->receiver == NULL){
        eprintf ("Got NULL pointer as frag->receiver in frl_frag_release_norecycle.\n");
        return;
    } else {
        idprintf ("bn= %d\n", fragment->block_number);
#ifndef NO_FRL_PTHREAD
        pthread_mutex_lock (&(fragment->receiver->global_mutex));
#endif
        for (i=0;i< fragment->block_number; i++) {
            _frl_free_datablock_descriptor (fragment->receiver, fragment->data_blocks[i]);
        }
#ifndef NO_FRL_PTHREAD
        pthread_mutex_unlock (&(fragment->receiver->global_mutex));
#endif
        _frl_free_frag (fragment, fragment->receiver);
    }
}

/**
 * @function frl_set_block_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_size the block size in bytes (has to be 64 bits aligned)
 */
int frl_set_block_size (struct frl_receiver * receiver, int block_size) {
    if (receiver->started != 0) {
        eprintf ("frl_set_block_size : board already started\n");
        return FRL_board_already_started;
    } else if (block_size > FRL_MAX_BLOCK_SIZE) {
        eprintf ("frl_set_block_size : size too big\n");
        return FRL_size_too_big;
    } else if (block_size < FRL_MIN_BLOCK_SIZE || receiver->header_size >= block_size) {
        eprintf ("frl_set_block_size : size too small\n");
        return FRL_size_too_small;
    } else if ((block_size & ~FRL_SIZE_ALIGN_MASK) != 0) {
        eprintf ("frl_set_block_size : size not aligned\n");
        return FRL_size_not_aligned;
    }
    receiver->block_size = block_size;
    return 0;
}

/**
 * @function frl_get_block_size
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int frl_get_block_size (struct frl_receiver * receiver)
{
    return receiver->block_size;
}

/**
 * @function frl_set_block_number
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param block_number the block number to be allocated
 */
int frl_set_block_number (struct frl_receiver * receiver, int block_number)
{
    if (receiver->started != 0) {
        eprintf ("frl_set_block_number : board already started\n");
        return FRL_board_already_started;
//    } else if (block_number < 2 * _FRL_free_data_fifo_depth) {
//        eprintf ("frl_set_block_number : number too small (%d at least)\n", _FRL_free_data_fifo_depth * 2);
//        return FRL_block_number_too_small;
    } else if (block_number > _FRL_free_data_fifo_depth) {
        eprintf ("frl_set_block_number : number too large (%d at max)\n", _FRL_free_data_fifo_depth );
        return FRL_block_number_too_large;
    }
    receiver->block_number = block_number;
    return 0;
}

/**
 * @function frl_get_block_number
 * @return positive block size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int frl_get_block_number (struct frl_receiver * receiver)
{
    return receiver->block_number;
}

/**
 * @function frl_set_header_size
 * @return zero on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int frl_set_header_size (struct frl_receiver * receiver, int header_size)
{
    if (receiver->started != 0) {
        eprintf ("frl_set_header_size : board already started\n");
        return FRL_board_already_started;
    } else if (header_size > FRL_MAX_BLOCK_SIZE || header_size >= receiver->block_size) {
        eprintf ("frl_set_header_size : size too big\n");
        return FRL_size_too_small;
    } else if ((header_size & ~FRL_SIZE_ALIGN_MASK) != 0) {
        eprintf ("frl_set_header_size : size not aligned\n");
        return FRL_size_not_aligned;
    }
    receiver->header_size = header_size;
    return 0;
}

/**
 * @function frl_get_header_size
 * @return positive header size on success, negative error code on failure
 * @param receiver pointer to the receiver structure
 */
int frl_get_header_size (struct frl_receiver * receiver)
{
    return receiver->header_size;
}


/**
 * @function frl_set_receive_pattern actually tries to put the pattern in the hardware
 * @return zero on success (FRL_OK), negative error code on failure
 * @param receiver pointer to the receiver structure
 * @param header_size the header size in bytes (has to be 64 bits aligned)
 */
int frl_set_receive_pattern (struct frl_receiver * receiver, int receive_pattern)
{
    if (receiver == NULL) {
        return FRL_error;
    }
    if ( receiver->link_enabled ) {
      /* user tries to set a pattern when link is enabled */
      return FRL_not_allowed;
    }
    
    if (_frl_apply_to_mask( _FRL1_LINK_CTRL, &receive_pattern )) {
      return FRL_invalid_pattern;
    }
	
    receiver->link_enable_mask = receive_pattern;
    return FRL_OK;
}

/**
 * @function frl_get_fragment 
 * @returns pointer to the fragment structure or NULL on error
 * @param receiver pointer to the frl receiver structure
 * @param status pointer to the integer holding status information
 */
struct frl_fragment * frl_frag_get (struct frl_receiver * receiver, int * status)
{
    struct _FRL_wc_fifo_element wc_element;
    struct frl_fragment * ret = NULL;
    int i;
	
    if (receiver==NULL) {
        eprintf ("Trying to frag get on a NULL receiver.");
        return NULL; 
    }
    if (receiver->suspended) {
        eprintf ("Trying to get fragment from a suspended receiver. Not fragment provided\n");
        _frl_error (FRL_suspended, status);
        return NULL;
    } 

#ifdef FRL_NO_INTERRUPT
    _frl_noint_handle_blocks (receiver);
#endif
    wc_element = _frl_pop_wc (receiver);
    idprintf ("Popped word count : %d\n", wc_element.wc);
    if (wc_element.no_fragment) {
        eprintf ("frl get fragment : word count FIFO empty\n");
        _frl_error (FRL_no_event_available, status);
    } else {
        int block_number = (wc_element.wc -1) / (receiver->block_size - receiver->header_size) + 1;
        receiver->blocksInSpy -= block_number;
        idprintf ("Popped word count : %d\n", wc_element.wc);
        if (NULL == (ret = _frl_alloc_frag (receiver, block_number))) {
            eprintf ("frl get fragment : word count FIFO empty\n");
            _frl_error (FRL_no_event_available, status);
        } else {
            receiver->blocksInSpy -= block_number;
            idprintf("Alloc OK %d blocks   0x%x blocks in spy \n", block_number, receiver->blocksInSpy);
            ret->FED_number = FRL_FRAGMENT_UNANALYSED;
            ret->receiver = receiver;
            ret->block_size = receiver->block_size;
            ret->header_size = receiver->header_size;
            ret->wc = wc_element.wc;
            ret->block_number = block_number;
            ret->truncated = wc_element.truncated;
            ret->CRC_error = wc_element.CRC_error;
            idprintf ("frag : bs=%d, hs=%d, wc=%d, bn=%d\n",
                      ret->block_size, ret->header_size, ret->wc, ret->block_number);
            for (i=0; i<block_number; i++) {
                ret->data_blocks[i] = _frl_pop_block_in_board (receiver);
                idprintf ("block = 0x%08x\n", (int)ret->data_blocks[i]->bus_address);
                if (ret->data_blocks[i] == NULL) { /* big problem, we should have a block available... 
                                                      everything is likely to be broken */
                    int j;
					
                    eprintf ("Major internal error : blocks registered as in board are less than expected\n");
                    eprintf ("Major internal error : blocks registered as in board are less than expected\n");
                    frl_dump_info (receiver);
                    _frl_error (FRL_internal_error, status);
                    for (j=i-1; j>=0; j--) {
                        _frl_free_data_block (receiver, ret->data_blocks[j]);
                    }
                    _frl_free_frag (ret, receiver);
                    ret = NULL;
                    break;
                }
#ifdef DEBUG_DATA_POINTERS
                {
                    U32 pointer = (U32)ret->data_blocks[i]->bus_address + ret->receiver->header_size;
					
                    ret->receiver->map[0x14/4] = pointer;

                    if (ret->receiver->min_pointer_in > pointer) {
                        eprintf ("POINTER_DEBUG : pointer_out out of range min=%x, now=%x\n", 
                                 ret->receiver->min_pointer_in, pointer);
                    }
                    if (ret->receiver->max_pointer_in < pointer) {
                        eprintf ("POINTER_DEBUG : pointer_out out of range max=%x, now=%x\n", 
                                 ret->receiver->max_pointer_in, pointer);
                    }
                    if (ret->receiver->last_pointer_out == 0xFFFFFFFF) {
                        ret->receiver->last_pointer_out = pointer;
                    } else if (ret->receiver->last_pointer_out + ret->receiver->block_size + 
                               (ret->receiver->noalloc?4096:0)!= pointer) {
                        if ((ret->receiver->last_pointer_out == ret->receiver->max_pointer_in) && 
                            (ret->receiver->min_pointer_in == pointer)) {
                            /* normal case, happy */
                        } else {
                            eprintf ("POINTER_DEBUG : unexpected pointer_out min=%x, max=%x, last=%x, expected=%x, block=%x had=%x\n", 
                                     ret->receiver->min_pointer_in, ret->receiver->max_pointer_in, ret->receiver->last_pointer_out, 
                                     ret->receiver->last_pointer_out + ret->receiver->block_size+ (ret->receiver->noalloc?4096:0),
                                     ret->receiver->block_size, pointer);
                        }
                    }
                    ret->receiver->last_pointer_out = pointer;
                    ret->receiver->pointer_out_count++;
                    if (ret->receiver->pointer_out_count > ret->receiver->pointer_in_count) {
                        eprintf ("POINTER_DEBUG : pointer out count> pointer in count: out: %x in: %x\n", 
                                 ret->receiver->pointer_out_count, ret->receiver->pointer_in_count);
                    }
                }
                _frl_pointer_ubercheck (ret->receiver, "frl_frag_get");
#endif

            }
        }
    }
    return ret;
}



/**
 * @function frl_frag_release
 * @param fragment the framgment to be freed
 * frees the fragment space and data blocks to the right receiver (original receiver)
 */
void frl_frag_release (struct frl_fragment * fragment)
{
    int i;
	
    if (fragment->receiver->suspended) {
        eprintf ("Trying to release a fragment from a suspended block. No block recycled\n");
        return;
    }

    idprintf ("frl frag release\n");
    if (fragment == NULL) {
        return;
    } else if (fragment->block_number == -1) {
        eprintf ("Got -1 as block number in frl_frag_release. Perhaps a double release of the fragment?\n");
        return;
    } else if (fragment->data_blocks == NULL) {
        eprintf ("Got NULL pointer as frag->block_number in frl_frag_release.\n");
        return;
    } else {
        idprintf ("bn= %d\n", fragment->block_number);
        for (i=0;i< fragment->block_number; i++) {
            /* push the block in the free fifo list (this should not fail) */
            _frl_free_data_block (fragment->receiver, fragment->data_blocks[i]);
            /*if (_frl_feed_shared_fifo(fragment->receiver, fragment->data_blocks[i]->bus_address)) {
              eprintf ("puting event back in the free FIFO (unexpected failure)\n");
              eprintf ("puting event back in the free FIFO (unexpected failure)\n");*/
				/* the block is lost until we close receiver */
            /*} else {
              idprintf ("Pushing back block 0x%08x\n", (int)fragment->data_blocks[i]);*/
				/* still have to reference the block in our own queue */
				/**(fragment->receiver->data_blocks_in_board_insert) = fragment->data_blocks[i];
                                   fragment->data_blocks[i]->next = NULL;
                                   fragment->receiver->data_blocks_in_board_insert = &(fragment->data_blocks[i]->next);
                                   fragment->data_blocks[i] = NULL;
                                   }*/
            fragment->data_blocks[i] = NULL;
        }
        idprintf ("\n");
#ifndef FRL_NO_INTERRUPT
        _frl_enable_FIFO_interrupts (fragment->receiver); /* RE-enable the interrupt, in case of... */   
#else
        _frl_noint_handle_blocks (fragment->receiver);
#endif
        _frl_free_frag (fragment, fragment->receiver);
    }
}


/**
 * @function frl_close
 * @param receiver the receiver relese
 * stop activity from the board. frees up the memory. This function is silent on errors because
 * they are unrecoverable and only imply failing feeing of resources.
 */
void frl_close (struct frl_receiver *receiver) /* native version */
{
    struct _frl_data_block * db;
	
    idprintf ("frl close 1\n");
    if (receiver == NULL) return;
    if (((int)receiver->map) == 0xECECECEC) {
        eprintf ("WARNING: probably trying to close an already closed receiver. This program might segfault.\n");
    }
#ifndef NO_FRL_PTHREAD
    pthread_mutex_lock (&(receiver->global_mutex));
#endif

    idprintf ("frl close 2\n");
    receiver->map[_FRL_CSR_OFFSET/4] &= ~(_FRL_CSR_HALF_EMPTY_INT_ENABLE | _FRL_CSR_EMPTY_INT_ENABLE);
#ifndef FRL_NO_RESET
    receiver->map[_FRL_CSR_OFFSET/4] |= _FRL_CSR_SOFT_RESET;
#endif
	
    while (receiver->allocated_data_blocks != NULL) {
        struct _frl_data_list * temp_list = receiver->allocated_data_blocks;
        free (temp_list->block);
        receiver->allocated_data_blocks = temp_list->next;
        free (temp_list);
    }
    idprintf ("frl close 4\n");
    while (receiver->free_fragments != NULL) {
        struct frl_fragment * temp = receiver->free_fragments;
        receiver->free_fragments = receiver->free_fragments->next;
        free (temp);
    }
	
    /* cleanup free data block descriptors */
    while (receiver->free_data_block_descriptors != NULL) {
        db = receiver->free_data_block_descriptors;
        receiver->free_data_block_descriptors = db->next;
        free (db);
    }
	
    /* cleanup data blocks in board in case of noalloc scheme */
    if (receiver->noalloc) {
        while (receiver->data_blocks_in_board != NULL) {
            db = receiver->data_blocks_in_board;
            receiver->data_blocks_in_board = db->next;
            free (db);
        }
    }

    /* unmap all the memory that used to be maped */
    if (receiver->wc_fifo != MAP_FAILED) munmap (receiver->wc_fifo, sizeof (struct _FRL_wc_fifo));
    if (receiver->data_fifo_user != MAP_FAILED) munmap (receiver->data_fifo_user, sizeof (struct _FRL_data_fifo) +
                                                  receiver->block_number * sizeof (void *));
    if (receiver->data_blocks_user != MAP_FAILED) munmap (receiver->data_blocks_user, receiver->block_size * receiver->block_number);
    if (receiver->map != MAP_FAILED) munmap ((void *)receiver->map, _FRL_BAR0_range);
	
    idprintf ("frl close 5\n");
    close (receiver->fd);
#ifndef NO_FRL_PTHREAD
    pthread_mutex_unlock (&(receiver->global_mutex));
    pthread_mutex_destroy (&(receiver->global_mutex));
#endif
    memset (receiver, 0xEC, sizeof (struct frl_receiver));
    free (receiver);
    idprintf ("frl close finished\n");
}



/**
 * Comparson is done on a byte-by-byte basis
 * @param fragment the fragment to compare with the expected data
 * @param expected_data a pointer to the buffer containing expected data (DAQ headers
 * are part of the expected data
 * @param expected_size size of the data in the expected_buffer, headers included 
 * (so expected_size = fragment_size + 3*8)
 * @param error_position placeholder for position of non-matching byte
 * @return error code corresponding to the found error
 */
int frl_frag_check (struct frl_fragment * fragment, void * expected_data, int expected_size, int *error_position)
{
    int i=0;

    if (expected_size < frl_frag_size (fragment)) {
        *error_position = -1;
        return FRL_size_too_big;
    } else if (expected_size > frl_frag_size (fragment)) {
        *error_position = -1;
        return FRL_size_too_small;
    } else {
        for (i=0; i<(frl_frag_size (fragment) + 3*8); i++) {
            if (((char *)(fragment->data_blocks[i/fragment->block_size]->user_address))[i%fragment->block_size]
                != ((char *)expected_data)[i]) {
                *error_position = i;
                return FRL_data_mismatch;
            }
        }
    }
    *error_position = -1;
    return FRL_OK;
}

/**
 * @function frl dumps various receiver information 
 * @param receiver pointer to the receiver structure
 */
void frl_dump_info (struct frl_receiver * rec)
{
    int prev_suspend;
	
    if (rec == NULL) {
        iprintf ("*** frl receiver dump  : NULL receiver\n");
        return;
    } else if (!(rec->started)) {
        iprintf ("*** frl receiver dump  : not started\n");
        return;
    }
    if (rec->wc_fifo != NULL && rec->wc_fifo != MAP_FAILED) {
        iprintf ("*** frl receiver dump  : \n"
                 " - wc FIFO read=0x%08x, write=0x%08x\n",
                 rec->wc_fifo->read, rec->wc_fifo->write);
    }
    if (rec->data_fifo != NULL) {
        iprintf (" - frl DATA fifo dump : \n"
                 "    read       = 0x%08x\n"
                 "    write      = 0x%08x\n"
                 "    size       = 0x%08x\n"
                 "    done_read  = 0x%08x\n"
                 "    done_write = 0x%08x\n",
                 rec->data_fifo->read,
                 rec->data_fifo->write,
                 rec->data_fifo->size,
                 rec->data_fifo->done_read,
                 rec->data_fifo->done_write);
        iprintf ("frl block fifo free slot : %d\n", 
                 _frl_fifo_free_slot_count(rec->data_fifo->write, rec->data_fifo->read, rec->data_fifo->size));
    }
#ifdef DEBUG_DATA_POINTERS
    iprintf ("in m/l/M %08x/%08x/%08x out l %08x\n",
             rec->min_pointer_in, rec->last_pointer_in, rec->max_pointer_in,
             rec->last_pointer_out);
    iprintf ("incount = %x, out_count = %x\n",
             rec->pointer_in_count, rec->pointer_out_count);
#endif
    prev_suspend = rec->suspended;
    rec->suspended = 1;
    iprintf ("pointers in frl = %d\n", frl_susp_get_handle_number(rec));
    rec->suspended = prev_suspend;
    iprintf ("\n\n");
}


/**
 * @function frl_set_error_string returns a const char * containting the representation of the error.
 * @param error_number error code to translate to string
 */
/* my perl command to extract this from frl.h's enum definition:
perl -e 'for (<>) { if (/(FRL_[a-zA-Z_]*)/) {print "case $1:\nreturn \"$1\";\nbreak;\n";}}'
*/
const char * frl_get_error_string (int error_number)
{
    switch (error_number) {
    case FRL_OK:
        return "FRL_OK";
        break;
    case FRL_IOerror:
        return "FRL_IOerror";
        break;
    case FRL_out_of_memory:
        return "FRL_out_of_memory";
        break;
    case FRL_size_too_small:
        return "FRL_size_too_small";
        break;
    case FRL_size_too_big:
        return "FRL_size_too_big";
        break;
    case FRL_block_number_too_small:
        return "FRL_block_number_too_small";
        break;
    case FRL_size_not_aligned:
        return "FRL_size_not_aligned";
        break;
    case FRL_board_already_started:
        return "FRL_board_already_started";
        break;
    case FRL_out_of_DMA_memory:
        return "FRL_out_of_DMA_memory";
        break;
    case FRL_no_event_available:
        return "FRL_no_event_available";
        break;
    case FRL_internal_error:
        return "FRL_internal_error";
        break;
    case FRL_data_mismatch:
        return "FRL_data_mismatch";
        break;
    case FRL_overflow:
        return "FRL_overflow";
        break;
    case FRL_suspended:
        return "FRL_suspended";
        break;
    case FRL_size_mismatch:
        return "FRL_size_mismatch";
        break;
    case FRL_error:
        return "FRL_error";
        break;
    case FRL_invalid_pattern:
        return "FRL_invalid_pattern";
        break;
    case FRL_invalid_parameter:
        return "FRL_invalid_parameter";
        break;
    case FRL_empty:
        return "FRL_empty";
        break;
    case FRL_not_allowed:
        return "FRL_not_allowed";
	break;
    case FRL_block_number_too_large:
        return "FRL_block_number_too_large";
        break;
    default:
        return "Unknown error";
        break;
    }
    return "internal error in frl_get_error_string, you're in trouble...";
}

/**
 * @function frl_suspend suspends operations on a frl receiver
 * @param receiver pointer to the receiver structure
 */
void frl_suspend (struct frl_receiver * receiver)
{
    receiver->suspended = 1;
}

/**
 * @function frl_resume 
 * @param receiver pointer to the receiver structure
 */
void frl_resume (struct frl_receiver * receiver)
{
    receiver->suspended =0;
}

void frl_set_localdaq( struct frl_receiver * receiver ) 
{
    receiver->map[ _FRL_CSR_OFFSET/4 ] |= _FRL_CSR_SPY_ENABLE;
    U32 ormask = (_FRL1_SPY_ENABLE | _FRL1_SPY_ALL);
    U32 newval = receiver->map1[ _FRL1_CSR/4 ] | ormask;
    receiver->map1[ _FRL1_CSR/4 ] |= ormask;
    // read back for debugging
    U32 csr_map0 = receiver->map[ _FRL_CSR_OFFSET/4 ];
    U32 csr_map1 = receiver->map1[ _FRL1_CSR/4 ];
    idprintf( "PCI FPGA CSR : 0x%x  FRL FPGA CSR 0x%x\n", csr_map0, csr_map1 );
}

/**
 * @function frl_susp_get_handle_number
 * @param receiver pointer to the receiver structure
 * @return number of handles or -1 if receiver is not suspended
 */
int frl_susp_get_handle_number (struct frl_receiver * receiver)
{
    struct _frl_data_block *db = receiver->data_blocks_in_board;
    int count = 0;
    if (!receiver->suspended) return -1;
    while (db != NULL) {
        count ++;
        db=db->next;
    }
    return count;
}

/**
 * @function frl_susp_get_handle
 * @param receiver pointer to the receiver structure
 * @param index index of the handle to retrieve
 * @return the handle pointer provided with the clock when registered (possibly NULL) or NULL if the 
 * receiver is not suspended or if index is out of range
 */
void * frl_susp_get_handle (struct frl_receiver * receiver, int index)
{
    struct _frl_data_block *db = receiver->data_blocks_in_board;
    if (!receiver->suspended) return NULL;
    if (index < 0) return NULL;
    while (db != NULL && index-- > 0) {
        db=db->next;
    }
    if (db == NULL) return NULL;
    return db->user_handle;
}

/**
 * @function frl_get_FPGA_version
 * @param receiver pointer to the receiver structure
 * @return version of the receiver's FPGA
 */
U32 frl_get_FPGA_version (struct frl_receiver * receiver)
{
    if (receiver != NULL) {
        return receiver->FPGA_version;
    } else {
        return 0xFFFFFFFF;
    }
}

// /**
//  * Put the receiver in link dump mode
//  * @param receiver pointer to the receiver
//  * @return FRL_OK in case of success
//  */
// int frl_enable_link_dump (struct frl_receiver * receiver)
// {
//     U32 csr;
//     if (receiver == NULL) {
//         eprintf ("frl_enable_link_dump: Null receiver\n");
//         return FRL_IOerror;
//     }
//     csr = receiver->map[_FRL_CSR_OFFSET/4];
//     if ((csr & _FRL_CSR_ALL_LINKS_ENABLE) == _FRL_CSR_ALL_LINKS_ENABLE) {
//         eprintf ("frl_enable_link_dump: both links can't be enabled\n");
//         return FRL_IOerror;
//     }
//     receiver->map[_FRL_CSR_OFFSET/4] |= _FRL_CSR_LINK_DEBUG;
//     return FRL_OK;
// }
// 
// 
// 
// /**
//  * Dump one link word plus status
//  * @param receiver pointer to the receiver structure
//  * @param lsw pointer where least significant 32 bits word goes
//  * @param msw pointer where most significant 32 bits word goes
//  * @param control is set to true iff this is a contorl word
//  * @return FRL_OK in case of success, FRL_empty if nothing to read, other incase of error
//  */ 
// int frl_link_dump (struct frl_receiver * receiver, U32 *lsw, U32 *msw, int * control)
// {
//     U32 dummy;
//     if (receiver == NULL) {
//         eprintf ("frl_link_dump: Null receiver\n");
//         return FRL_IOerror;
//     }
//     if (0 == (receiver->map[_FRL_CSR_OFFSET/4] & _FRL_CSR_LINK_DEBUG)){
//         eprintf ("frl_link_dump: link dump mode not enabled\n");
//         return FRL_IOerror;
//     }
//     if (0 == (receiver->map[_FRL_FIFOSTAT_OFFSET/4] & _FRL_FIFOSTAT_PCI)) {
//         return FRL_empty;
//     } else {
//         *lsw = receiver->map[_FRL_DUMPDATA0_OFFSET/4];
//         *msw = receiver->map[_FRL_DUMPDATA1_OFFSET/4];
//         *control = receiver->map[_FRL_DUMPCTRL0_OFFSET/4] & _FRL_DUMPCTRL0_K;
//         dummy = receiver->map[_FRL_DUMPCTRL1_OFFSET/4];
//     }
//     return FRL_OK;
// }
// 
// /**
//  * Restores normal operations after an link dump session
//  * @param receiver pointer to the receiver structure.
//  */
// int frl_disable_link_dump (struct frl_receiver * receiver)
// {
//     if (receiver == NULL) {
//         eprintf ("frl_disable_link_dump: Null receiver\n");
//         return FRL_IOerror;
//     }
//     receiver->map[_FRL_CSR_OFFSET/4] &= ~_FRL_CSR_LINK_DEBUG;
//     return FRL_OK;
// }

