#!/usr/bin/env python

###############################################################################
## Helper to scan the input alignment settings (i.e., idelay and
## bitslip) on a PI TTS input into an LPM.
## NOTE: This is for debugging/diagnostic purposes only. The LPM TTS
## inputs are supposed to auto-align.
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.pi.tcds_pi_scan_fed_tts_input_alignment import calc_window_midpoint
from pytcds.pi.tcds_pi_scan_fed_tts_input_alignment import run_scan
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

SLEEP_TIME = 0.1
NUM_POLL_ITER = 1

IDELAY_MIN = 0
IDELAY_MAX = 31
NUM_IDELAY = IDELAY_MAX - IDELAY_MIN + 1
BITSLIP_MIN = 0
BITSLIP_MAX = 4

NUM_PIS_PER_LPM = 10

###############################################################################

def check_value(val_str):
    value = int(val_str)
    if value < 1 or value > NUM_PIS_PER_LPM:
        msg_base = "Sorry, but {0:d} as TTS/PI input number" \
                   " does not make sense."
        raise argparse.ArgumentTypeError(msg_base.format(value))
    return value

###############################################################################

class LPMTTSScan(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(LPMTTSScan, self).__init__(description, usage, epilog)
        self.pi_num = None
        # End of pre_hook().

    def setup_parser_custom(self):
        super(LPMTTSScan, self).setup_parser_custom()
        parser = self.parser

        # Add the choice of PI I/O channel.
        help_str = "PI TTS input number to scan. " \
                   "Default: 1."
        parser.add_argument("pi_number",
                            metavar="pi-number",
                            type=check_value,
                            help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        super(LPMTTSScan, self).handle_args()
        self.pi_num = int(self.args.pi_number)
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        lpm = get_lpm_hw(network_address, controlhub_address, verbose=self.verbose)

        if not lpm:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        reg_name_alignment_mode = \
            "lpm_main.tts_serdes_ctrl.align_mode"
        reg_name_serdes_reset = \
            "lpm_main.tts_serdes_ctrl.reset.pi{0:d}".format(self.pi_num)
        reg_name_idelay_ctrl = \
            "lpm_main.tts_idelay_ctrl.tap_ctrl.pi{0:d}".format(self.pi_num)
        reg_name_idelay_load = \
            "lpm_main.tts_idelay_ctrl.tap_ld.pi{0:d}".format(self.pi_num)
        reg_name_bitslip_strobe = \
            "lpm_main.tts_bitslip_ctrl.bitslip_strobe.pi{0:d}".format(self.pi_num)
        reg_name_aligned = \
            "lpm_main.tts_link_status.pi{0:d}.aligned".format(self.pi_num)
        reg_name_window = \
            "lpm_main.tts_alignment_monitor.scan_window.pi{0:d}".format(self.pi_num)
        reg_name_midpoint = \
            "lpm_main.tts_alignment_monitor.scan_midpoint.pi{0:d}".format(self.pi_num)
        reg_name_scan_success = \
            "lpm_main.tts_alignment_monitor.scan_success.pi{0:d}".format(self.pi_num)

        #----------

        window_scan = run_scan(lpm,
                               reg_name_alignment_mode,
                               reg_name_serdes_reset,
                               reg_name_idelay_ctrl,
                               reg_name_idelay_load,
                               reg_name_bitslip_strobe,
                               reg_name_aligned)
        midpoint_scan = calc_window_midpoint(window_scan)

        #----------

        # By now the firmware should have taken over the alignment
        # procedure again, and auto-aligned the link.
        time.sleep(1)
        tmp = lpm.read(reg_name_scan_success)
        if tmp != 0x1:
            print "ARGH! Scan did not finish successfully!"
        window_fw = lpm.read(reg_name_window)
        midpoint_fw = lpm.read(reg_name_midpoint)

        #----------

        print self.sep_line
        print "PI I/O number {0:d} idelay window:".format(self.pi_num)
        print "  according to scan script:"
        print "    0x{0:08x}".format(window_scan)
        print "    0b{0:032b}".format(window_scan)
        print "      {0:s}{1:s}".format(" " * (NUM_IDELAY - midpoint_scan - 1), "^")
        print "    --> midpoint = {0:d}".format(midpoint_scan)
        print "  according to firmware:"
        print "    0x{0:08x}".format(window_fw)
        print "    0b{0:032b}".format(window_fw)
        print "      {0:s}{1:s}".format(" " * (NUM_IDELAY - midpoint_fw - 1), "^")
        print "    --> midpoint = {0:d}".format(midpoint_fw)
        print self.sep_line

        #----------

        # End of main().

    # End of class LPMTTSScan.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper to scan the input alignment settings" \
        " (i.e., idelay, bitslip, etc.) on a PI TTS input into an LPM." \
        "  NOTE: This is for debugging/diagnostic purposes only." \
        " The LPM TTS inputs are supposed to auto-align."

    res = LPMTTSScan(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
