package xdaq.xdesk.xcontrol;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import xdaq.xdesk.xtree.*;

public class XControlTreeSelectionListener implements TreeSelectionListener
{
	XControlViewer viewer_;
	
	XControlTreeSelectionListener(XControlViewer viewer)
	{
		viewer_ = viewer;
	}
	
	public void valueChanged (TreeSelectionEvent e)
	{
		XTree tree = (XTree) e.getSource();
		TreePath[] paths = tree.getSelectionModel().getSelectionPaths();
		
		// Protect against empty selection
		if (paths == null) return;
		
		String association = "";
		String instance = "";
		
		// consistency check
		// remember the last item of the first selection.
		// all other selected items must be the same
		
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
		XTreeNode adapterNode = (XTreeNode) node.getUserObject();
		String lastElement = adapterNode.getNodeName();
		
		for (int i = 1; i < paths.length; i++) 
		{
			node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			
			String currentElement =  ((XTreeNode) node.getUserObject()).getNodeName();
			
			// Transport and Application are equivalent
			if (  currentElement.startsWith("Transport") ) currentElement = "Application";
			
			System.out.println (lastElement + ", " + currentElement);
			
			if (!lastElement.startsWith ( currentElement ))
			{
					JOptionPane.showMessageDialog (null,
				 		"Must select nodes of the same type",
                                       		 "Selection failed", JOptionPane.ERROR_MESSAGE);

					tree.getSelectionModel().removeSelectionPath (paths[i]);	
			}
		}
		
		if (lastElement.startsWith("Host"))
		{
			((XControlMenuBar) viewer_.getJMenuBar()).setHostMenu();
			((XControlToolBar) viewer_.getJToolBar()).showHostButtons();
			
			association = "Host";
			instance = adapterNode.getAttribute ("id");
			
		}
		else if (lastElement.startsWith("Application") || lastElement.startsWith("Transport"))
		{
			// Check for plugin
			association = adapterNode.getAttribute ("class");
			instance = adapterNode.getAttribute ("instance");
			
			((XControlMenuBar) viewer_.getJMenuBar()).setApplicationMenu();
			((XControlToolBar) viewer_.getJToolBar()).showApplicationButtons();
			
		} else if (lastElement.startsWith("Partition")) 
		{
			//((XControlMenuBar) control_.getJMenuBar()).setPartitionMenu();
			association = "Partition";
			instance = "";
		
		} else if (lastElement.startsWith("Script")) 
		{
			//win_.getMenu().setScriptMenu();
			//((XControlMenuBar) control_.getJMenuBar()).setScriptMenu();
			
			association = "Script";
			instance = "";
		} else 
		{
			association = adapterNode.getNodeName();
			instance = "";
			((XControlMenuBar) viewer_.getJMenuBar()).setEmptyMenu();
			((XControlToolBar) viewer_.getJToolBar()).removeAll();
		}		
	}
}
