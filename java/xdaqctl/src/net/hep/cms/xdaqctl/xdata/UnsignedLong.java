package net.hep.cms.xdaqctl.xdata;

public class UnsignedLong extends UnsignedInteger64 {

	@Override
	int getEXDRCode() {
		return XDataType.UNSIGNED_LONG.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
