// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_mpi_Address_h
#define _pt_mpi_Address_h

#include <map>

#include "pt/Address.h"

#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/Properties.h"


namespace pt
{

namespace mpi
{
	class Address : public pt::Address, public toolbox::Properties
	{
		public:

			//! Create address from url
			Address (std::map<std::string, std::string, std::less<std::string> >& address);

			virtual ~Address ();

			//struct sockaddr_in getSocketAddress ();

			//std::string getIPAddress ();

			//! Get the URL protocol, e.g. ptdapl
			std::string getProtocol ();

			//! Get the URL service if it exists, e.g. i2o
			std::string getService ();

			//! Get additional parameters at the end of the URL
			std::string getServiceParameters ();

			//! Return the URL in string form
			std::string toString ();

			std::string toURL ();

			std::string getRank ();

			//! Compare with another address
			bool equals (pt::Address::Reference address);
	};
}
}

#endif
