<?php

include_once ('config.php');
include_once ('tools.php');

echo '<div class="header">' . PHP_EOL;
echo '<img src="images/flashweb.png">' . PHP_EOL;
echo '<h2 class="title">' . $headerTitle . '</h2>' . PHP_EOL;
echo '</div>' . PHP_EOL;

$health = getClusterHealth($config ['elasticsearchurl']);

echo '<p>' . PHP_EOL;
echo '<button type="button" class="statusButton" onclick="location.href=\'view_cluster_health.php\'" style="border: 4px solid ' . $health['status'] . ';">' . $health['cluster_name']. '</button>' . PHP_EOL;
echo '<button type="button" class="statusButton" onclick="location.href=\'view_connections.php\'" style="border: 2px solid grey;">Connections</button>' . PHP_EOL;
echo '<button type="button" class="statusButton" onclick="location.href=\'view_nodes.php\'" style="border: 2px solid grey;">Nodes</button>' . PHP_EOL;
echo '</p>' . PHP_EOL;

?>
