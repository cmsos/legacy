// TestParam.cc

#include "TestParam.h"

#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"  // createMessage()

#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"  // XMLCh2String()

XDAQ_INSTANTIATOR_IMPL(TestParam);

void TestParam::InnerParam::registerFields(xdata::Bag<InnerParam> *bag)
{
	double_ = 0.1;
	vector_.resize(2);
	vector_[0] = false;
	vector_[1] = true;

	bag->addField("d", &double_);
	bag->addField("v", &vector_);
}

TestParam::TestParam(xdaq::ApplicationStub *stub) : xdaq::Application(stub)
{	
	string_ = "String";
	ulong_  = 0xa5a5a5a5l;
	int_    = -255;
	bool_   = true;
	double_ = 1.1e24;
	float_  = 1.2e-3;

	vector_.resize(3);
	for (unsigned int i = 0; i < vector_.size(); ++i) {
		vector_[i] = i + 1;
	}

	getApplicationInfoSpace()->fireItemAvailable("string", &string_);
	getApplicationInfoSpace()->fireItemAvailable("ulong",  &ulong_);
	getApplicationInfoSpace()->fireItemAvailable("int",    &int_);
	getApplicationInfoSpace()->fireItemAvailable("bool",   &bool_);
	getApplicationInfoSpace()->fireItemAvailable("double", &double_);
	getApplicationInfoSpace()->fireItemAvailable("float",  &float_);
	getApplicationInfoSpace()->fireItemAvailable("vector", &vector_);
	getApplicationInfoSpace()->fireItemAvailable("bag",    &bag_);
}

// End of file
// vim: set ai sw=4 ts=4:
