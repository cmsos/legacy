package xdaq.tools.dstorebrowser;

import xdaq.tools.dstorebrowser.dstorebrowser;

public class dstoreMain
{
	public static void main (String args[])
	{

		if ((args.length < 4) || (args.length > 5))
		{
			System.out.println ("Wrong number of arguments.");
			System.out.println ("1) URL of the XDAQ executive running the XDAQDataStore application module,");
			System.out.println ("2) TargetAddr of the XDAQDataStore application module,");
			System.out.println ("3) TargetAddr of the dstorebrowser java GUI application,");
			System.out.println ("4) Port number for the dstorebrowser java GUI application,");
			System.out.println ("5) (Optional) URL of data srouce to get data from.");
			return;
		}
/*
Arguments to pass to dstorebrowser:
  1) URL of the XDAQ executive running the XDAQDataStore
  2) TID of the XDAQDataStore
  3) TID of the dstorebrowser java GUI
  4) Port number of the dstorebrowser java GUI
  5) URL of data source to get data from
*/
	    dstorebrowser t;
	    System.out.println(args.length);
	    if (args.length == 5) 
	    {
  	         t = new dstorebrowser (args[0],Integer.parseInt(args[1]),Integer.parseInt(args[2]),Integer.parseInt(args[3]),args[4]);
		 t.show();
	    } else if (args.length == 4)
	    {
  	         t = new dstorebrowser (args[0],Integer.parseInt(args[1]),Integer.parseInt(args[2]),Integer.parseInt(args[3]),"");
		 t.show();
	    }
	}
}	
