package net.hep.cms.xdaqctl.xdata;

import junit.framework.TestCase;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import java.lang.Boolean;
import java.lang.String;
import java.lang.Integer;
import java.util.Vector;

public class XDataItemTest extends TestCase {

	public void testBoolean() throws Exception {

		SimpleItem xItem = new net.hep.cms.xdaqctl.xdata.Boolean();

		// boolean literal
		xItem.setValue(true);
		assertEquals(true, xItem.getValue());

		// string
		xItem.setValue("false");
		assertEquals(false, xItem.getValue());
		
		// boolean object
		Boolean value = new Boolean(true);
		
		// changing outer Object value doesn't affect XDataItem
		xItem.setValue(value);
		value = false;
		assertEquals(true, xItem.getValue());
				
		// changing inner XDataItem value doesn't affect Object
		value = (Boolean)xItem.getValue();
		assertEquals(true, value.booleanValue());
		xItem.setValue(false);
		assertEquals(true, value.booleanValue());
	}

	public void testNumeric() throws Exception {
		BufferedReader in = new BufferedReader(
				new FileReader("test/xdaq/numeric-ctor.test"));

		String line;
		while ((line = in.readLine()) != null) {
			if (line.length() == 0) { continue; }
			if (line.indexOf("#") == 0) { continue; }

			String[] tokens = line.split("\\s+");
			System.out.println("==== testing: " + line);

			String klass = tokens[0];
			String inputType = tokens[1];
			String inputValue = tokens[2];
			String outputType = tokens[3];
			String outputValue = tokens[4];

			if (inputValue.indexOf("0x") >= 0) {
				inputValue = Long.valueOf(inputValue.replace("0x", ""), 16)
				.toString();
			}
			if (outputValue.indexOf("0x") >= 0) {
				outputValue = Long.valueOf(outputValue.replace("0x", ""), 16)
				.toString();
			}

			SimpleItem item = (SimpleItem)Class.forName(
					"net.hep.cms.xdaqctl.xdata." + klass).newInstance();

			assertNotNull(item);
			assertNull(item.getValue());

			Class c;

			c = Class.forName("java.lang." + inputType);
			Object input = c.getMethod("valueOf", new Class[] { String.class })
			.invoke(null, inputValue);

			c = Class.forName("java.lang." + outputType);
			Object output = c.getMethod("valueOf", new Class[] { String.class })
			.invoke(null, outputValue);

			item.setValue(input);
			assertEquals("class: " + klass, output, item.getValue());
		}
	}

	public void testSerializeNumeric() throws Exception {
		BufferedReader file = new BufferedReader(
				new FileReader("test/xdaq/numeric-serialize.test"));

		String line;
		while ((line = file.readLine()) != null) {
			if (line.length() == 0) { continue; }
			if (line.indexOf("#") == 0) { continue; }

			String[] tokens = line.split("\\s+");

			String klass = tokens[0];
			String inputType = tokens[1];
			String inputValue = tokens[2];
			int exdr = Integer.parseInt(tokens[3]);
			int dataSize = Integer.parseInt(tokens[4]);

			if (inputValue.indexOf("0x") >= 0) {
				inputValue = Long.valueOf(inputValue.replace("0x", ""), 16)
				.toString();
			}

			SimpleItem item = (SimpleItem)Class.forName(
					"net.hep.cms.xdaqctl.xdata." + klass).newInstance();

			Class c = Class.forName("java.lang." + inputType);
			Object input = c.getMethod("valueOf", new Class[] { String.class })
			.invoke(null, inputValue);

			item.setValue(input);

			DataInputStream in = getEncodedStream(item);

			assertEquals(4 + dataSize, in.available());
			assertEquals("EXDR code for class " + klass, exdr, in.readInt());

			for (int i = 0; i < dataSize; ++i) {
				int expected = Integer.parseInt(tokens[5 + i], 16);
				byte b = in.readByte();
				int result = (b >= 0) ? b : 0x80 | b & 0x7f;
				assertEquals("Class " + klass + " byte at " + i + ":",
						expected, result);
			}		
		}
	}
	
	public void testDeserializeNumeric() throws Exception {
		BufferedReader file = new BufferedReader(
				new FileReader("test/xdaq/numeric-serialize.test"));
	
		String line;
		while ((line = file.readLine()) != null) {
			if (line.length() == 0) { continue; }
			if (line.indexOf("#") == 0) { continue; }

			System.out.println("==== testing (deserialize): " + line);
			
			String[] tokens = line.split("\\s+");

			String klass = tokens[0];
			String inputType = tokens[1];
			String inputValue = tokens[2];
			int exdr = Integer.parseInt(tokens[3]);
			int dataSize = Integer.parseInt(tokens[4]);

			if (inputValue.indexOf("0x") >= 0) {
				inputValue = Long.valueOf(inputValue.replace("0x", ""), 16)
						.toString();
			}
			
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			out.write(new byte[] { 0, 0, 0, (byte)exdr });
			for (int i = 0; i < dataSize; ++i) {
				out.write(Integer.parseInt(tokens[5 + i], 16));
			}
			ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
			
			SimpleItem item = (SimpleItem)Class.forName(
					"net.hep.cms.xdaqctl.xdata." + klass).newInstance();
			
			item.readEXDR(new DataInputStream(in));
			
			Class c = Class.forName("java.lang." + inputType);
			Object input = c.getMethod("valueOf", new Class[] { String.class })
					.invoke(null, inputValue);

			assertEquals("failed for " + klass + " on " + inputValue, input, item.getValue());
		}
	}

	public void testString() throws Exception {

		String value = "a value";

		SimpleItem xItem = new net.hep.cms.xdaqctl.xdata.String();
		assertNotNull(xItem);
		assertNull(xItem.getValue());

		xItem.setValue(value);	
		assertEquals(value, xItem.getValue());
	}

	public void testSerializeString() throws Exception {

		String value = "a value";

		SimpleItem xItem = new net.hep.cms.xdaqctl.xdata.String();
		xItem.setValue(value);

		DataInputStream in = getEncodedStream(xItem);

		assertEquals(16, in.available());
		assertEquals(xItem.getEXDRCode(), in.readInt());
		assertEquals(value.length(), in.readInt());

		byte[] buf = new byte[7];
		in.read(buf);
		assertEquals(value, new String(buf));
		assertEquals(0, in.readByte());
	}
	
	public void testDeserializeString() throws Exception {
		ByteArrayInputStream in = new ByteArrayInputStream(
				new byte[] { 0, 0, 0, 4, 0, 0, 0, 6, 's', 't', 'r', 'i', 'n', 'g', 0, 0 });
		
		SimpleItem xItem = new net.hep.cms.xdaqctl.xdata.String();
		xItem.readEXDR(new DataInputStream(in));
		
		assertEquals("string", xItem.getValue());
	}

	public void testTimeVal() throws Exception {

		Date value = new Date();

		SimpleItem xItem = new net.hep.cms.xdaqctl.xdata.TimeVal();
		assertNotNull(xItem);
		assertNull(xItem.getValue());

		xItem.setValue(value);	
		assertEquals(value, xItem.getValue());
	}

	public void testSerializeTimeVal() throws Exception {

		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("GMT"));
		cal.set(2007, Calendar.OCTOBER, 10, 10, 10, 10);
		long l = cal.getTimeInMillis();
		l = l - l % 1000 + 7;
		cal.setTimeInMillis(l);
		Date value = cal.getTime();

		SimpleItem xItem = new net.hep.cms.xdaqctl.xdata.TimeVal();
		xItem.setValue(value);
		String s = "2007-10-10T10:10:10.000007Z";

		DataInputStream in = getEncodedStream(xItem);

		assertEquals(36, in.available());
		assertEquals(xItem.getEXDRCode(), in.readInt());
		assertEquals(s.length(), in.readInt());

		byte[] buf = new byte[s.length()];
		in.read(buf);
		assertEquals(s, new String(buf));
	}
	
	public void testDeserializeTimeVal() throws Exception {
		
		String dateStr = "2007-10-10T10:10:10.008000Z0"; // trailing 0 for padding
		dateStr = "2008-03-17T12:38:53.172660Z ";
		if (dateStr.length() % 4 != 0) {
			System.out.println("==== !! length is not even for %4 !! " + dateStr);
		}
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		out.write(new byte[] { 0, 0, 0, 18, 0, 0, 0, (byte)dateStr.length() });
		out.write(dateStr.getBytes());
		
		ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());
		
		SimpleItem xItem = new net.hep.cms.xdaqctl.xdata.TimeVal();
		xItem.readEXDR(new DataInputStream(in));
		
		DateFormat df =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date exp = df.parse(dateStr);
		
		assertEquals(exp, xItem.getValue());
	}
	
	public void testVector() throws Exception {

		net.hep.cms.xdaqctl.xdata.Vector item = new net.hep.cms.xdaqctl.xdata.Vector();

		assertNotNull(item);
		boolean thrown = false;
		try {
			item.get(0);
		} catch (ArrayIndexOutOfBoundsException e) {
			thrown = true;
		}
		assertTrue(thrown);

		for (int i = 0; i < 3; i++) {
			net.hep.cms.xdaqctl.xdata.Integer value =
				new net.hep.cms.xdaqctl.xdata.Integer();
			value.setValue(i);
			item.add(value);
		}

		for (int i = 0; i < 3; i++) {
			assertEquals(i, ((SimpleItem)item.get(i)).getValue());
		}

		int[] expected = { 0, 1, 2 };

		DataInputStream in = getEncodedStream(item);

		assertEquals((2 + expected.length * 2) * 4, in.available());
		assertEquals("EXDR code for class Vector", item.getEXDRCode(), in.readInt());

		int size = in.readInt();
		assertEquals(expected.length, size);

		for (int i = 0; i < size; i++) {
			assertEquals("EXDR code for class Integer",
					XDataType.INTEGER.getEXDRCode(), in.readInt());
			assertEquals(i, in.readInt()	);
		}
	}
	
	public void testDeserializeVector() throws Exception {
		net.hep.cms.xdaqctl.xdata.Vector item = new net.hep.cms.xdaqctl.xdata.Vector();

		for (int i = 0; i < 3; i++) {
			net.hep.cms.xdaqctl.xdata.Integer value =
				new net.hep.cms.xdaqctl.xdata.Integer();
			value.setValue(i);
			item.add(value);
		}

		DataInputStream in = getEncodedStream(item);

		net.hep.cms.xdaqctl.xdata.Vector itemDes = new net.hep.cms.xdaqctl.xdata.Vector();
		itemDes.readEXDR(in);
		
	    for (int i=0; i<item.size();i++) {
		    net.hep.cms.xdaqctl.xdata.Integer exp = (net.hep.cms.xdaqctl.xdata.Integer) item.get(i);
		    net.hep.cms.xdaqctl.xdata.Integer res = (net.hep.cms.xdaqctl.xdata.Integer) itemDes.get(i);
	    	assertEquals(exp.getValue(), res.getValue());
	    }
	}

	public void testTable() throws Exception {
		
		net.hep.cms.xdaqctl.xdata.Table item = new net.hep.cms.xdaqctl.xdata.Table();
		
		assertNotNull(item);

		// first column, unsigned long
		item.addColumn("longg", XDataType.UNSIGNED_LONG);
		item.addColumn("string", XDataType.STRING);
		
		Vector<Object> row = new Vector<Object>();

		row.clear();
		row.add(0x80000000);
		row.add("the first");
		item.add(row);
		
		row.clear();
		row.add(0x7fffffff);
		row.add("the second");
		item.add(row);
		
		assertEquals(2, item.columns());
		assertEquals(2, item.rows());
		
		Vector<String> names = item.getNames();
		assertEquals(2, names.size());
		assertEquals("string", names.get(1));
		assertEquals(XDataType.UNSIGNED_LONG, item.getType(names.get(0)));
		assertEquals(XDataType.STRING, item.getType(names.get(1)));
		assertNull(item.getType("not-exist"));
		
		row = item.getRow(0);
		assertEquals(new Long(0x80000000), row.get(0));
		assertEquals("the first", row.get(1));

		DataInputStream in = getEncodedStream(item);

		assertEquals(148, in.available());
		assertEquals("EXDR code for class Vector", item.getEXDRCode(), in.readInt());

		in.readInt();
		assertEquals(item.columns(), in.readInt());
		in.readInt();
		assertEquals(item.rows(), in.readInt());		
	}

	public void testDeserializeTable() throws Exception {
		net.hep.cms.xdaqctl.xdata.Table item = new net.hep.cms.xdaqctl.xdata.Table();

		// first column, unsigned long
		item.addColumn("longg", XDataType.INTEGER);
		item.addColumn("string", XDataType.STRING);
		
		Vector<Object> row = new Vector<Object>();

		row.clear();
		row.add(0x80000000);
		row.add("the first");
		item.add(row);
		
		row.clear();
		row.add(0x7fffffff);
		row.add("the second");
		item.add(row);
		
		DataInputStream in = getEncodedStream(item);

		net.hep.cms.xdaqctl.xdata.Table itemDes = new net.hep.cms.xdaqctl.xdata.Table();
		itemDes.readEXDR(in);
		
		for (int i=0; i<item.rows();i++) {
			
			row = item.getRow(i);
			Vector<Object> rowDes = itemDes.getRow(i);
			
			for (int j = 0; j < row.size(); ++j) {
				assertEquals(row.get(j), rowDes.get(j));
			}
		}
	}
	
	private DataInputStream getEncodedStream(XDataItem xItem) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		xItem.writeEXDR(new DataOutputStream(out));
		out.flush();

		DataInputStream in = new DataInputStream(
				new ByteArrayInputStream(out.toByteArray()));

		return in;
	}
}

//End of file
//vim: set sw=4 ts=4:
