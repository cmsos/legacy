package xdaq.tools.controller;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.awt.FlowLayout;
import java.awt.Dimension;

//public class TableSelectionBox extends JPanel implements ActionListener 
public class TableSelectionBox extends JPanel implements CaretListener 
{
	JComboBox columnsSelection;
	JComboBox selectionMethod;
	JTextField inputField;
	JTable table;
	ListSelectionModel tableSelectionModel;
	
	public TableSelectionBox(JTable t) {
		this.setLayout(new FlowLayout(FlowLayout.RIGHT));
		this.setPreferredSize(new Dimension(300,5));
		table = t;
		tableSelectionModel = table.getSelectionModel();
		
		columnsSelection = new JComboBox();
		TableModel model = table.getModel();
		
		for (int c = 0; c < model.getColumnCount(); c++)
	  {
			columnsSelection.addItem(model.getColumnName(c));	
		}

		selectionMethod = new JComboBox();
		selectionMethod.addItem("starts with");
		selectionMethod.addItem("contains");
		selectionMethod.addItem("ends with");				
		
		inputField = new JTextField(10);
		
		//inputField.addActionListener(this);
	   inputField.addCaretListener(this);
		
		this.add(new JLabel("Select where"));
		this.add(columnsSelection);
		this.add(selectionMethod);
		this.add(inputField);
	}
	
	//public void actionPerformed(ActionEvent e) 
	public void caretUpdate(CaretEvent e)
	{
		String filterValue = inputField.getText();
		if (filterValue.equals("")) return;
		
		tableSelectionModel.clearSelection();
		
		int c = columnsSelection.getSelectedIndex();
		
		for (int r = 0; r < table.getRowCount(); r++) 
		{
			Object cellContentObject = table.getValueAt(r,c);
			String cellContent;
			if (cellContentObject.getClass() == String.class) {
				cellContent = (String) cellContentObject;
			} else if (cellContentObject.getClass() == java.lang.Integer.class) {
				cellContent = cellContentObject.toString();
			} else {
				// Cannot filter on this
			return;
			}		
			
			int sm = selectionMethod.getSelectedIndex();
			if (sm == 0)
		{
				if (cellContent.startsWith(filterValue)) {
					tableSelectionModel.addSelectionInterval(r,r);
				}
			} else if (sm == 1){
				if (cellContent.indexOf(filterValue) != -1) {
					tableSelectionModel.addSelectionInterval(r,r);
				}
			} else if (sm == 2){
				if (cellContent.endsWith(filterValue)) {
					tableSelectionModel.addSelectionInterval(r,r);
				}
			}
		}		
	}
}
