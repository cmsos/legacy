#include "d2s/utils/Monitor.hh"
#include "d2s/utils/Exception.hh"
#include "d2s/utils/loggerMacros.h"
#include "d2s/utils/StreamInfoSpaceHandler.hh"

utils::Monitor::Monitor( Logger logger,
        utils::InfoSpaceHandler &appIS, 
        xdaq::Application *xdaq, 
        ApplicationStateMachineIF &fsm,
        std::string workloopID)
: logger_ ( logger ),
    appIS_( appIS ),
    fsm_(fsm)
{
    stopMonitoring_ = false;
    monitoringActive_ = false;
    monLoopCnt_ = 0;

    this->addInfoSpace( &appIS_ );

    std::stringstream workLoopString;
    uint32_t instance = xdaq->getApplicationDescriptor()->getInstance();
    workLoopString << workloopID << instance;
    monitoringThread_ = toolbox::task::getWorkLoopFactory()->getWorkLoop(workLoopString.str(), "waiting");
    toolbox::task::ActionSignature *monitoringPoll = toolbox::task::bind( this, &utils::Monitor::monitoringThread, "poll");

    monitoringThread_->submit( monitoringPoll );
}

    void
utils::Monitor::startMonitoring()
{
    try
    {

        DEBUG( "Activting Monitoring thread.");
        monitoringThread_->activate();
        DEBUG( "Montoring thread activated.");
    }
    catch( xcept::Exception &e )
    {
        std::string err = "Cannot activate monitoring thread.";
        ERROR( err );
        XCEPT_DECLARE_NESTED( utils::exception::SoftwareProblem, top, err, e );
        //this->notifyQualified( "fatal", top );
    }
}

    bool
utils::Monitor::monitoringThread (toolbox::task::WorkLoop *wl) 
{

    // we divide the monitoring loop into 100ms slices so that the canceling of the workloop
    // is not blocked due to a long deltaTMon_ setting.
    if ( monLoopCnt_ != 0 ) {
        monLoopCnt_ -= 1;
        ::usleep(100000);
        return true;
    }


    monLoopCnt_ =  appIS_.getuint32("deltaTMonMs") / 100;
    updateAllInfospaces();

    // We should do fireItemGroupChanged( names, this ) on the infospaces.

    if ( stopMonitoring_ )
    {
        INFO("Monitoring is being stopped.");
        monitoringActive_ = false;
        stopMonitoring_ = false;
        return false;
    } 
    else 
    {
        return true;
    }
}

//JRF TODO herein lies the problem. Do we want to do this, if you update all infospaces, how do you know for which stream you are updating the stream info spaces?
//surely in order to use the stream infospaces, we only need to update them with the stream each time we want to use them... 
    void 
utils::Monitor::updateAllInfospaces()
{
    try
    {
        std::tr1::unordered_map< std::string, utils::InfoSpaceHandler *>::iterator it;
        for ( it = infoSpaceMap_.begin(); it != infoSpaceMap_.end(); it++ )
        {

            utils::InfoSpaceHandler* infosp = (*it).second;
            infosp->update();
        }
    }
    catch ( utils::exception::Exception &e )
    {
        fsm_.gotoFailedAsynchronously( e );
    }
    catch ( xdata::exception::Exception &e )
    {
        fsm_.gotoFailedAsynchronously( e );
    }
}

    void 
utils::Monitor::addInfoSpace( utils::InfoSpaceHandler *is )
{
    std::tr1::unordered_map< std::string, utils::InfoSpaceHandler *>::iterator it;
    it = infoSpaceMap_.find( is->name() );
    if ( it == infoSpaceMap_.end() ) 
    {
        DEBUG( "Monitor: adding infospace " << is->name() );
        infoSpaceMap_.insert( std::pair<std::string, utils::InfoSpaceHandler *>( is->name(), is ) );
    }

}

    std::string 
utils::Monitor::dumpInfoSpaces()
{
    std::stringstream res;
    std::tr1::unordered_map< std::string, utils::InfoSpaceHandler *>::iterator it;
    for( it = infoSpaceMap_.begin(); it != infoSpaceMap_.end(); it++ )
    {
        res << "name: " << (*it).first << " <br>\n";
    }
    return res.str();
}

    utils::InfoSpaceHandler *
utils::Monitor::getInfoSpaceHandler( std::string name )
{
    std::tr1::unordered_map< std::string,utils::InfoSpaceHandler * >::iterator it = infoSpaceMap_.find( name );
    if ( it == infoSpaceMap_.end() ) {
        FATAL( "Software bug: no such infospace : " << name << " in Monitor registered. Bailing out...");
        exit(-1);
    }
    return (*it).second;
}

    void
utils::Monitor::newItemSet( const std::string setName, const uint32_t level )
{
    // get item set
    if ( itemSets_.find( setName ) != itemSets_.end() ) 
    {
        ERROR( "Software Bug: Item Set \"" + setName + "\" exists already! Item sets must have unique names." );
        return;
    }
    if ( setLevels_.find(setName) != setLevels_.end() )
    {
        ERROR( "Software Bug: Item Set \"" + setName + "\" has a pre-existing level.");
        return;
    }
    std::list< std::pair< std::string, Item > > emptySet;
    itemSets_.insert( std::pair< std::string, std::list< std::pair< std::string, utils::Monitor::Item > > >( setName, emptySet ));
    setLevels_.insert( std::pair< std::string, uint16_t >(setName, level));
}

    void
utils::Monitor::setItemSetLevel( const std::string setName, const uint32_t level )
{
    // get item set
    if ( itemSets_.find( setName ) == itemSets_.end() ) 
    {
        ERROR( "Software Bug: Item Set \"" + setName + "\" does not exist." );
        return;
    }
    if ( setLevels_.find(setName) == setLevels_.end() )
    {
        ERROR( "Software Bug: Item Set \"" + setName + "\" exists but does not have an assigned level.");
        return;
    }
    setLevels_.find( setName )->second = level;
}

    uint16_t
utils::Monitor::getItemSetLevel(const std::string setName)
{
    if ( itemSets_.find( setName ) == itemSets_.end() )
    {
        ERROR( "Software Bug: Item Set \"" + setName + "\" does not exist. (Monitor::getItemSetLevel)" );
        return 0;
    }
    if ( setLevels_.find(setName) == setLevels_.end() )
    {
        ERROR( "Software Bug: Item Set \"" + setName + "\" exists but does not have an assigned level.");
        return 0;
    }
    return setLevels_.find( setName )->second;
}

    void
utils::Monitor::addItem( const std::string setName,
        const std::string item, 
        utils::InfoSpaceHandler * is,
        std::string format,
        int32_t streamNo )
{
    // need to find the itemSet
    std::tr1::unordered_map< std::string, std::list< std::pair< std::string, utils::Monitor::Item > > >::iterator set;

    set = itemSets_.find( setName );

    if (set == itemSets_.end() ) 
    {
        ERROR( "Software Bug: the item set \"" << setName << "\" does not exist! ");
        return;
    }

    if ( is->exists(item) ) 
    {
        //DEBUG("insert item " << item << " with streamno " << streamNo );
        utils::Monitor::Item nit = {item, is, format, streamNo };
        (*set).second.push_back( std::pair< std::string, utils::Monitor::Item>( item, nit) );
        //Tell the ISItem about the set which the item is contained in.
        //This is necessary for getting the level of an item set in the update loop.
        is->getItem(item)->addToSet(setName,streamNo);
    }
    else 
    {
        ERROR( "Software bug: the item " << item << " has not been found in Infospace " << is->name() );
    }

    this->addInfoSpace( is );
}



    std::list< std::vector<std::string> > 
utils::Monitor::getFormattedItemSet( std::string setName )
{
    std::list< std::vector<std::string> > result;

    // get item set
    if ( itemSets_.find( setName ) == itemSets_.end() ) 
    {
        ERROR( "Software Bug: Item Set \"" << setName << "\" does not exist (getFormattedItemSet)" );
        return result;
    }
    uint16_t setLevel = this->getItemSetLevel(setName);

    std::list< std::pair< std::string, utils::Monitor::Item > > list;
    list = (*(itemSets_.find( setName ))).second;

    std::list< std::pair< std::string, utils::Monitor::Item > >::const_iterator it;
    for (it = list.begin(); it != list.end(); it++ )
    {
        utils::Monitor::Item item = (*it).second;

        std::string value;
        //DEBUG( "item streamno " << item.streamNo );
        if ( item.streamNo == -1 )
            value = item.is->getFormatted( item.name, item.format );
        else
        {
            //DEBUG(" dynamic casting of " << item.is->name());
            try {
                StreamInfoSpaceHandler *sifh =  dynamic_cast< StreamInfoSpaceHandler *>(item.is);
                //DEBUG( "Dyn cast worked");
                value = sifh->getFormatted( item.name, item.streamNo, item.format );
            } catch (...) {
                FATAL( "Software bug if we come here: dyn cast in Monitor did not work...");
                exit(-1);
            }
        }
        std::string doc = item.is->getItemDoc( item.name );

        uint16_t itemLevel = item.is->getItem(item.name)->level;
        std::string isHidden = (setLevel >= itemLevel) ? "show" : "hide"; //To determine if hidden

        std::stringstream strLevel;
        strLevel << itemLevel;
        isHidden += strLevel.str(); 
        //isHidden += std::to_string(itemLevel);//(itemLevel == 1) ? "*" : "." ; //To check if dropdown necessary

        //Remove wildcards from name for display
        std::string displayName = item.name;
        displayName.erase( std::remove(displayName.begin(), displayName.end(), '*'),
                displayName.end() );

        std::vector<std::string> itl;
        itl.push_back( displayName );
        itl.push_back( value );
        itl.push_back( doc );            
        itl.push_back( isHidden ); //necessary for hiding items with too high a level
        result.push_back( itl );

    }
    return result;
}

    const std::tr1::unordered_map<std::string, utils::InfoSpaceHandler *> &
utils::Monitor::getInfoSpaceMap() 
{
    return infoSpaceMap_;
}

    std::list< std::pair<std::string, utils::Monitor::Item> > 
utils::Monitor::getItems(std::string set)
{
    std::list< std::pair<std::string, Item> > items;
    if ( itemSets_.find(set) == itemSets_.end() )
    {
        ERROR("Item set " << set << " does not exist. (Monitor::getItems)");
    }
    else
    {
        items = itemSets_[set];
    }
    return items;
}
