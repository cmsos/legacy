package xdaq.xdesk.tools;

import java.util.*;
import javax.infobus.*;
import java.beans.*;

import org.apache.log4j.*;

public class InfoBusSubscriber implements  InfoBusDataConsumer
{
	Logger logger = Logger.getLogger (InfoBusSubscriber.class);

	InfoBusMemberSupport infobusSupport_;
	HashMap listeners_; // this belongs only to subscribers
	HashMap dataListeners_;




	// Inner class for listening to InfoBus xreply events
       	//
	class DataListener extends DataItemChangeListenerSupport implements DataItemChangeListener
  	{
		InfoBusSubscriberListener l_;
		int refCount_ = 0;

		DataListener (InfoBusSubscriberListener l, int refCount)
		{
			l_ = l;
			refCount_ = refCount;
		}

		// Thread safe callback, because multiple publishers may change a value
		// concurrently and the same listener callback will be invoked.
		//
  		public synchronized void dataItemValueChanged (DataItemValueChangedEvent e)
		{
			Object o = e.getChangedItem();
			l_.update(o, e.getSource() );
		}

		// Decrement listener reference counter and return new value;
		public int decRefCount()
		{
			refCount_--;
			return refCount_;
		}

		// Increment listener reference counter and return new value
		public int incRefCount()
		{
			refCount_++;
			return refCount_;
		}
  	};
	//
	// end of inner class for listening to xreply events

	// command, e.g.: xcommand
	public InfoBusSubscriber (String infobus)
	{
		Logger logger = Logger.getLogger (InfoBusSubscriber.class);

		listeners_ = new HashMap();
		dataListeners_ = new HashMap();
		infobusSupport_ = new InfoBusMemberSupport (null);

		try
		{
			infobusSupport_.joinInfoBus(infobus);
			infobusSupport_.getInfoBus().addDataConsumer (this);
		} catch (Exception e)
		{
			logger.error ("Cannot access infobus " + infobus + ": " + e.toString());
		}
	}


	// command: the name of the expected incoming command
	// source: The listener to be called back (type DataItemChangeListener)
	public void subscribeItem (String item, InfoBusSubscriberListener listener)
	{
		if (listeners_.get(item) != null)
		{
			logger.warn ("Trying to attach more than one subscriber to infobus item " + item);
			return;
		}

		if (infobusSupport_ != null)
		{
			try {
				// subscribe to infobus events
				// If there are multiple sources, subscribe the same listener to all sources
				// with the same name
				//
				Object[] dataItems = infobusSupport_.getInfoBus().findMultipleDataItems(item, null, this);
				if (dataItems != null)
				{
					DataListener dataListener = new DataListener(listener, dataItems.length);
					dataListeners_.put( listener, dataListener);

					for (int i = 0; i < dataItems.length; i++)
					{
						Object dataItem = dataItems[i];
       						if (dataItem != null && dataItem instanceof DataItemChangeManagerSupport)
						{

      							((DataItemChangeManagerSupport)dataItem).addDataItemChangeListener(dataListener);
						}
					}
				}

			} catch (Exception e)
			{
				logger.error ("Cannot subscribe to infobus item " + item + ": " + e.toString());
			}

			// In any case add command and listener to hashmap
			listeners_.put (item, listener);
		} else
		{
			logger.error ("Cannot subscribe to infobus item " + item + ": infobus unavailable");
		}
	}


	public void unsubscribeItem (String item)
	{



		InfoBusSubscriberListener l = (InfoBusSubscriberListener) listeners_.get (item);
		if (l != null)
		{
			try
			{
				// subscribe to xreply events
				Object dataItem = (Object) infobusSupport_.getInfoBus().findDataItem(
											item, null, this);
				DataItemChangeListener dataListener = (DataItemChangeListener) dataListeners_.get(l);
				if (dataListener != null)
				{
       					if (dataItem != null && dataItem instanceof DataItemChangeManagerSupport)
      						((DataItemChangeManagerSupport)dataItem).removeDataItemChangeListener(dataListener);

					dataListeners_.remove(l);
				}
			} catch (Exception e)
			{
				logger.error ("Cannot unsubscribe infobus item " + item + ": " + e.toString());
			}

			listeners_.remove(item);
		} else
		{
			logger.error ("Cannot unsubscribe infobus item " + item + ": item does not exist");
		}
	}





	public void dataItemAvailable ( InfoBusItemAvailableEvent e )
	{
		String item = e.getDataItemName();
		InfoBusSubscriberListener l = (InfoBusSubscriberListener) listeners_.get ( item );

		if (l != null)
		{
			DataListener dataListener = (DataListener) dataListeners_.get (l);
			if (dataListener == null)
			{
				System.out.println("Adding DataListener: " + item );
				dataListener = new DataListener(l,1); // refCount 1 means 1 source is there
				dataListeners_.put (l, dataListener);
			} else
			{
				dataListener.incRefCount(); // another source publishes the same event
			}

			// DataITemChangeManagerSupport object is provided by the publisher.
			// The subscriber only adds his listener object to this change manager.
			//
			DataItemChangeManagerSupport cms = (DataItemChangeManagerSupport) e.requestDataItem (this, null);
			cms.addDataItemChangeListener( dataListener );
		}
	}

	public void dataItemRevoked ( InfoBusItemRevokedEvent e )
	{


		String item = e.getDataItemName();
		InfoBusSubscriberListener l = (InfoBusSubscriberListener) listeners_.get (item);
		if (l != null)
		{
			// call user listener callback
			DataListener dl = (DataListener) dataListeners_.get(l);
			if (dl.decRefCount() == 0)
			{
				System.out.println("Removing DataListener: " + item );

				//l.revoked(item, e.getSource());
				dataListeners_.remove(l);
				//listeners_.remove(item);
			}
		}
	}

	public void propertyChange (PropertyChangeEvent e)
	{

	}


	public void shutdown()
	{
		Set keySet = listeners_.keySet();
		Object [] names = keySet.toArray(new Object [] {} );
		for ( int i=0; i < names.length; i++ )
		{
			this.unsubscribeItem((String)names[i]);
		}

		try {
			infobusSupport_.getInfoBus().removeDataConsumer (this);
			infobusSupport_.leaveInfoBus();
		}
		catch (Exception e )
		{
			logger.error(e.toString());

		}
	}
}
