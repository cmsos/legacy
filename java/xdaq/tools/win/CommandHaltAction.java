package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CommandHaltAction extends AbstractAction
{
	xdaqWin win_;
	
	public CommandHaltAction (xdaqWin win)
	{
		super ("Halt");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {	
			win_.executeCommandOnSelection ("Halt", null, win_.getPaths());
		}
		catch(xdaqWinException ex )
		{
		
		}

	}
}
