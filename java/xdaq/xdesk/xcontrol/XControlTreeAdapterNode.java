package xdaq.xdesk.xcontrol;

import org.w3c.dom.*;
import org.w3c.dom.Document;
import org.w3c.dom.DOMException;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import javax.swing.tree.*;

import java.util.*;

import java.awt.*;
import javax.swing.*;



// This class wraps a DOM node and returns the text we want to
// display in the tree. It also returns children, index values,
// and child counts. This is why we can dynamically change the
// JTree just with changing the DOM. Clicking on a node will
// invoke this class and traverse all new children which causes
// them to be displayed.
//
public class XControlTreeAdapterNode
{ 
    boolean compress = true; // display the tree compressed?
   
    XControlTreeAdapterNode parent_;
	
    public org.w3c.dom.Node domNode;

    // An array of names for DOM node-types
    static final int ELEMENT_TYPE 	=   	1;
    static final int ATTR_TYPE		=	2;
    static final int TEXT_TYPE		=	3;
    static final int CDATA_TYPE		=	4;
    static final int ENTITYREF_TYPE 	=	5;
    static final int ENTITY_TYPE	=	6;
    static final int PROCINSTR_TYPE 	=	7;
    static final int COMMENT_TYPE	=	8;
    static final int DOCUMENT_TYPE  	=	9;
    static final int DOCTYPE_TYPE	=	10;
    static final int DOCFRAG_TYPE 	=	11;
    static final int NOTATION_TYPE	=	12;
	 
    static final String[] typeName = {
        "none",
        "Element",
        "Attr",
        "Text",
        "CDATA",
        "EntityRef",
        "Entity",
        "ProcInstr",
        "Comment",
        "Document",
        "DocType",
        "DocFragment",
        "Notation",
    };
	
    protected boolean update;
	
     
    public int getChildCount() 
    {
        return childCount();
    }
	  
    
    public int getIndex (TreeNode node) 
    {
        return index((XControlTreeAdapterNode) node);
    }
	
   
    public boolean getAllowsChildren() { return true; }
	
 
 
    public boolean treeElement(String elementName) 
    {
        return true;
    }
    
    public XControlTreeAdapterNode getParent()
    {
    	return parent_;
    }	

    // Construct an Adapter node from a DOM node
    public XControlTreeAdapterNode (org.w3c.dom.Node node, XControlTreeAdapterNode parent) {
        parent_ = parent;
        domNode = node;
        update = false;
    }
  
    public String getNodeName()
    {
    	// Here, extract maybe attributes like class name, instance
	// for displaying...
	
        return domNode.getNodeName();
    }
  
    public String getAttribute (String name)
    {
        String s = "";
	
        NamedNodeMap attributes = domNode.getAttributes();
        if (attributes.getLength() > 0) {
            for (int j = 0; j < attributes.getLength(); j++) {
                Node attribute = attributes.item(j);
                if (attribute.getNodeName().equals(name)) {
                    s = attribute.getNodeValue();
                    break;
                }
            }
        }
	
        return s;
    }

    // Return a string that identifies this node in the tree
    // *** Refer to table at top of org.w3c.dom.Node ***
    public String toString() 
    {
        String s = "";
	
        String nodeName = domNode.getNodeName();
        if (! nodeName.startsWith("#")) {
            s += nodeName;
        }
        if (domNode.getNodeValue() != null) {
            if (s.startsWith("ProcInstr")) 
                s += ", "; 
            else 
                s += ": ";
            // Trim the value to get rid of NL's at the front
            String t = domNode.getNodeValue().trim();
            int x = t.indexOf("\n");
            if (x >= 0) t = t.substring(0, x);
            s += t;
        }
        return s;
    }
  
    //
    // Return the textual representation of a tree node
    //
    public String content()
    {
        String s = "";
	
        // Check for attributes, whatever node
        // it is. Those are displayed in the html panel
        // together with its child nodes.
        //
	
        NamedNodeMap attributes = domNode.getAttributes();
        if (attributes.getLength() > 0) {
            for (int j = 0; j < attributes.getLength(); j++) {
                Node attribute = attributes.item(j);
                s += "<I>"+attribute.getNodeName()+": </I>";
                s += attribute.getNodeValue()+ " ";
            }
            s += "<BR>";
        }
	
        org.w3c.dom.NodeList nodeList = domNode.getChildNodes();
	
        for (int i = 0; i < nodeList.getLength(); i++) {
            org.w3c.dom.Node node = nodeList.item(i);
            XControlTreeAdapterNode adpNode = new XControlTreeAdapterNode(node, this);

            int type = node.getNodeType();		
            if (type == ELEMENT_TYPE) {
				//
				// Would need to check the schema here in order
				// to drive the correct html display
				// Don't print the value now. Only if it is a leaf.

				//
				// If the child is a leaf, print its content,
				// otherwise omit it, to prevent extensive
				// subtree traversal.
			
                if (node.getFirstChild() != null) {
			
                    if (adpNode.isLeaf()) {
                        s += "<I>" + node.getNodeName() + ": </I>";
                        s += adpNode.content(); // print contents of children
                        s += "<BR>";
                    }
                } else {
                    System.out.println ("This element has no child nodes");
                }
            } 
            else if ( type == TEXT_TYPE) {
                String tmp = node.getNodeValue();
			
				// Check if the string can be printed in html
                if (tmp.startsWith("/")) {
                    s += "<PRE>"+tmp+"</PRE>";
                }
                else
                    s += node.getNodeValue();
            }
            else if ( type == ENTITYREF_TYPE ) {
				// The content is in the TEXT node under it
                s += adpNode.content();
            }
            else if ( type == CDATA_TYPE ) {
                StringBuffer sb = new StringBuffer ( node.getNodeValue() );
                for (int j = 0; j < sb.length(); j++) {
                    if (sb.charAt(j) == '<') {
                        sb.setCharAt(j, '&');
                        sb.insert (j+1, "lt;");
                        j += 3;
                    }
                    else if (sb.charAt(j) == '&') {
                        sb.setCharAt (j, '&');
                        sb.insert (j+1, "amp;");
                        j += 4;
                    }
                }
                s += "<pre>" + sb + "\n</pre>";
            }
        }
        return s;
    }


    /*
     * Return children, index, and count values
     */
    public int index(XControlTreeAdapterNode child) 
    {
        int count = childCount();
        for (int i=0; i<count; i++) {
            XControlTreeAdapterNode n = this.child(i);
            if (child.domNode == n.domNode) return i;
        }
        return -1; // Should never get here.
    }
  
    //
    // Retrieve the first child node that matches the <name> tag.
    //
    public XControlTreeAdapterNode firstChild(String name)
    {
        org.w3c.dom.NodeList nodeList = domNode.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeName().equals (name)) {
                return new XControlTreeAdapterNode(nodeList.item(i), this);
            }
        }
        return new XControlTreeAdapterNode(null, this);
    }

    public XControlTreeAdapterNode child(int searchIndex) {
        //Note: JTree index is zero-based. 
        org.w3c.dom.Node node = 
            domNode.getChildNodes().item(searchIndex);
        if (compress) 
	{
            // Return Nth displayable node
            int elementNodeIndex = 0;
            for (int i=0; i<domNode.getChildNodes().getLength(); i++) 
	    {
                node = domNode.getChildNodes().item(i);
		
                if (node.getNodeType() == ELEMENT_TYPE 
                    && treeElement( node.getNodeName() )
                    && elementNodeIndex++ == searchIndex) {
                    break; 
                }
            }
        }
        return new XControlTreeAdapterNode(node, this); 
    }

    public int childCount() {
        if (!compress) {
            // not compress -> return all nodes, included empty #text
            return domNode.getChildNodes().getLength();  
        } 
        int count = 0;
        for (int i=0; i<domNode.getChildNodes().getLength(); i++) 
	{
            org.w3c.dom.Node node = domNode.getChildNodes().item(i); 
	
	    if (node.getNodeType() == ELEMENT_TYPE
                && treeElement( node.getNodeName() )) 
                {
                    // Note: 
                    //   Have to check for proper type. 
                    //   The DOCTYPE element also has the right name
                    ++count;
                }
	   
            // If I wanted to include text node, I'd have to
            // insert this here:
            // if (node.getNodeType() == TEXT_TYPE) { ++count; }
        }
        return count;
    }
  
    public boolean isLeaf()
    {
  
        // a node is not a leaf, if it hase more than one child.
        // If it has exactly one child, we must check if this
        // child is a text node. If not, then it is also not a leaf.
        //
        if (domNode.getChildNodes().getLength() > 1) return false;
        else if (domNode.getChildNodes().getLength() == 1){
            if (domNode.getFirstChild().getNodeType() == TEXT_TYPE) return true;
            else return false;
        }
        else return true;
  
    }

    /**
     * A function which changes the value of a node in the Tree. 
     * This can be used in order to update Parameters with a 
     * subsequent call to UtilParamsSet.
	 * The routine only searches the first parameter found.
	 * @param element is the element to search for.
	 * @param value contains the new value of the element.
     */
    public int changeTextElement(String element, String value) 
    {
        NodeList found = domNode.getOwnerDocument().getElementsByTagName( element );
        if (found.getLength() == 0 ) 
	{
            System.out.println("Warning : cannot find element " +
                               element + " (XControlTreeAdapterNode::changeTextElement)");
            return -1;
        } else if ( found.getLength() > 1 ) {
            System.out.println("Warning : found " + found.getLength() + 
                               "elements  (XControlTreeAdapterNode::changeTextElement)");
        }
        Node valueText = (found.item(0)).getFirstChild();
        System.out.println("valuetext"+valueText + " " + valueText.getNodeValue());
        valueText.setNodeValue( value );
        return 0;
    } 
}
