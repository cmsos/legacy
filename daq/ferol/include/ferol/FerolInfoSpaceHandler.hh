#ifndef __FerolInfoSpaceHandler
#define __FerolInfoSpaceHandler

#include "xdaq/Application.h"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
#include "d2s/utils/DataTracker.hh"

namespace ferol {

    class FerolInfoSpaceHandler : public utils::InfoSpaceHandler 
    {
    public:
        FerolInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater );
        void registerTrackerItems( utils::DataTracker &dataTracker );
    };
}

#endif /* __FerolInfoSpaceHandler */
