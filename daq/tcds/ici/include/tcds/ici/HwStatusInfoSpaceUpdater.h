#ifndef _tcds_ici_HwStatusInfoSpaceUpdater_h_
#define _tcds_ici_HwStatusInfoSpaceUpdater_h_

#include "tcds/hwutilstca/HwStatusInfoSpaceUpdaterTCA.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace ici {

    class TCADeviceICI;

    class HwStatusInfoSpaceUpdater : public tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::ici::TCADeviceICI const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

    private:
      tcds::ici::TCADeviceICI const& getHw() const;

      tcds::ici::TCADeviceICI const& hw_;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_HwStatusInfoSpaceUpdater_h_
