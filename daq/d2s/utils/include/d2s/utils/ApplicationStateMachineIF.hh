#ifndef __ApplicationStateMachineIF
#define __ApplicationStateMachineIF

#include <string>
#include "xcept/Exception.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/fsm/AsynchronousFiniteStateMachine.h"

namespace utils 
{
    class ApplicationStateMachineIF 
    {
    public: 
        virtual void gotoFailedAsynchronously( xcept::Exception &e ) = 0;
        virtual ~ApplicationStateMachineIF() {};
    };
}    
#endif /* __ApplicationStateMachineIF */
