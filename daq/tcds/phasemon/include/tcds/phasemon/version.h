#ifndef _tcdsphasemon_version_h_
#define _tcdsphasemon_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSPHASEMON_VERSION_MAJOR 3
#define TCDSPHASEMON_VERSION_MINOR 13
#define TCDSPHASEMON_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSPHASEMON_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSPHASEMON_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSPHASEMON_VERSION_CODE PACKAGE_VERSION_CODE(TCDSPHASEMON_VERSION_MAJOR,TCDSPHASEMON_VERSION_MINOR,TCDSPHASEMON_VERSION_PATCH)
#ifndef TCDSPHASEMON_PREVIOUS_VERSIONS
#define TCDSPHASEMON_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSPHASEMON_VERSION_MAJOR,TCDSPHASEMON_VERSION_MINOR,TCDSPHASEMON_VERSION_PATCH)
#else
#define TCDSPHASEMON_FULL_VERSION_LIST TCDSPHASEMON_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSPHASEMON_VERSION_MAJOR,TCDSPHASEMON_VERSION_MINOR,TCDSPHASEMON_VERSION_PATCH)
#endif

namespace tcdsphasemon
{
  const std::string package = "tcdsphasemon";
  const std::string versions = TCDSPHASEMON_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS TTC-stream phase monitoring.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS TTC-stream phase monitoring.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
