#include "RAMix139.hh"
#include "VMEDevice.hh"
#include "VMEAddressTable.hh"
#include "VMEAddressTableASCIIReader.hh"
//#include "SBS620x86LinuxBusAdapter.hh"
#include "CAENLinuxBusAdapter.hh"
#include "CommandSequencer.hh"
#include "CommandSequenceASCIIReader.hh"
#include <sys/timeb.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sstream>
#include <iostream>
#include <unistd.h>

#define VMERAMADDRESSTABLE "VMERAMAddressTable.dat"
#define RAMixADDRESSTABLE "RAMix139AddressTable.dat"
#define SEQUENCESETTINGS "Sequences.conf"

#define VMERAMLOW 0x00000000
#define VMERAMHIGH 0x02000000
#define MB * 1024 * 1024

using std::ofstream;
using std::cin;
using std::exception;
using namespace HAL;

int seed = 12345;

//int nvol = 18;
//int volumes[] = { 256, 1024, 4096, 16384, 32768, 65536, 262144, 524288, 1048576, 2097152, 4194304,
// 8388608, 12582912, 16777216, 20971520, 25165824, 29360128, 33554432 };
int nmeasure = 3;
//int nmeasure = 100;
//int nmeasure = 10;
//int nvol = 1;
int nvol = 13;
//int volumes[] = { 4194304 };
//int volumes[] = { 256 };
int volumes[] = { 256, 1024, 4096, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 12582912, 16777216, 20971520, 25165824, 29360128, 33554432 };

/*************************************************************************
 *
 * stolen from frans
 *
 * modified by Sheila 26/6/2006
 *
 *************************************************************************/

long 
subtractTime(struct timeval *t, struct timeval *sub)
{
  signed long sec, usec, rsec, rusec;
  sec = t->tv_sec - sub->tv_sec;
  usec = t->tv_usec - sub->tv_usec;
  if (usec < 0) {
    sec--;
    usec += 1000000;
  }
  if (sec < 0) {
    rsec = 0;
    rusec = 0;
  }
  else {
    rsec = (unsigned long) sec;
    rusec = (unsigned long) usec;
  }
  return (rsec * 1000000 + rusec);
}


int main(int argc, char** argv) 
{
  
  unsigned long adapterNo;
  adapterNo = 0;
  // see if there are arguments
  if ( argc == 2 ) 
  {
    sscanf( argv[1], "%lu", &adapterNo );
  }
  printf( "Try to open adapter number %lu\n", adapterNo );

    #ifdef SBS
    SBS620x86LinuxBusAdapter busAdapter(0, false );
    #else
    VMEBusAdapterInterface* dada = new CAENLinuxBusAdapter( CAENLinuxBusAdapter::V2718, 0);
    delete dada;
    ::sleep(1);
    CAENLinuxBusAdapter busAdapter( CAENLinuxBusAdapter::V2718, adapterNo);


    busAdapter.readVersions( cout );
    //CAENLinuxBusAdapter busAdapter2( CAENLinuxBusAdapter::V2718, 0);
    #endif
    VMEAddressTableASCIIReader addressTableReader( RAMixADDRESSTABLE );
    VMEAddressTable addressTable( "Test address table", addressTableReader );

    RAMix139 RAMix(addressTable, busAdapter, 0x0100);

    VMEAddressTableASCIIReader ramAddressTableReader( VMERAMADDRESSTABLE );
    VMEAddressTable ramAddressTable( "Address table for VME RAM", ramAddressTableReader );

    VMEDevice* VMERAM = new VMEDevice(ramAddressTable,
                                      busAdapter,
                                      VMERAMLOW);


    bool stop;
    string item, answer;
    unsigned long size, verror;
    unsigned long ic1, ic2, value;
    unsigned long dtw, dtr;
    unsigned long *ptr;
    double  mbs, tputr, tputw;
    char *source, *readBack;
    vector<string> names;
    struct timeval tstart,tstop;



    
    //Initialize RAMix

    RAMix.initialize( VMERAMLOW, VMERAMHIGH );
    
    //Dump status Registers and testing if the conexion is ok

    cout << "Status Registers";
    
    RAMix.read("VMELowLimit", &value);
    cout << "VMELowLimit   : " << hex << value << endl;
    if (value != 0){
      cout << "Error";
      return 0;
    }
    RAMix.read("VMEHighLimit", &value);
    cout << "VMEHighLimit  : " << hex << value << endl;
    RAMix.read("A24D32AM", &value);
    cout << "A24D32AM      : " << hex << value << endl;
    if (value != 0x39){
      cout << "Error";
      return 0;
    }
    RAMix.read("A24D32BlockAM", &value);
    cout << "A24D32BlockAM : " << hex << value << endl;
    if (value != 0x3b){
      cout << "Error";
      return 0;
    }
    RAMix.read("A24D64BlockAM", &value);
    cout << "A24D64BlockAM : " << hex << value << endl;
    if (value != 0x38){
      cout << "Error";
      return 0;
    }
    RAMix.read("A32D32AM", &value);
    cout << "A32D32AM      : " << hex << value << endl;
    if (value != 0x09){
      cout << "Error";
      return 0;
    }
    RAMix.read("A32D32BlockAM", &value);
    cout << "A32D32BlockAM : " << hex << value << endl;
    if (value != 0x0b){
      cout << "Error";
      return 0;
    }
    RAMix.read("A32D64BlockAM", &value);
    cout << "A32D64BlockAM : " << hex << value << endl;
    if (value != 0x08){
      cout << "Error";
      return 0;
    }
    RAMix.read("ParityEnable", &value);


//Do the grand Measure

    cout << "Measuring..." ;
   
        verror = 0;
        stop = false;

        cout << "\n   read: t   MB/s    write: t   MB/s\n" << endl;
        
        for (int ivol=0; ivol<nvol; ivol++ ) {
          size = volumes[ivol];
          cout << "#" << setw(9) << "measurement ID"
               << setw(15) <<  "Size [B]"
               << setw(15) << "Size [MB]"
               << setw(15) << "rdTime [us]"
               << setw(15) << "rd [MB/s]"
               << setw(15) << "wrTime [us]"
               << setw(15) << "wr [MB/s]" << endl;
          mbs =( double)(size) / (double) (1024*1024);
          source = new char[size];
          readBack = new char[size];
          for( int imeas = 0; imeas < nmeasure; imeas++ ) {
            srand( seed );
            for (ic1 = 0; ic1<size/4; ic1++) {
              ptr = (unsigned long*)(&(source[(ic1*4)]));
              *ptr = rand();
            }

           // write
            gettimeofday(&tstart,NULL);
            VMERAM->writeBlock( "RAMLow", size, source, HAL_NO_VERIFY, HAL_DO_INCREMENT );
            gettimeofday(&tstop,NULL);
            dtw = subtractTime( &tstop, &tstart);
            tputw = (double)size / (double)dtw;
                                                                                                                                      
            // read back
            gettimeofday(&tstart,NULL);
            VMERAM->readBlock( "RAMLow", size, readBack, HAL_DO_INCREMENT);
            gettimeofday(&tstop,NULL);
            dtr = subtractTime( &tstop, &tstart);
            tputr = (double)size / (double)dtr;
                                                                                                                                      
            // compare for errors
            for ( ic2=0; ic2<size; ic2+=4 ) {
              unsigned long* pts = (unsigned long*)&source[ic2];
              unsigned long* ptr = (unsigned long*)&readBack[ic2];
              if ( *pts != *ptr ) {
                cout << "verifyError " << setw(4) << setfill(' ') << dec << verror
                     << "   at 0x" << hex << ic2
                     << " : 0x" << hex << setfill('0')<< setw(8) << *pts
                     << " != " <<  setw(8) << *ptr << endl;
                verror++;
                if ( verror >= 50 ) {
                  stop = true;
                  break;
                }
              }
            }
                                                                                                                                      
            // write out output:
                                                                                                                                      
            cout << dec << setfill(' ') << setw(15) << imeas
                 << setw(15) <<  volumes[ivol]
                 << setw(15) << mbs
                 << setw(15) << dtr
                 << setw(15) << tputr
                 << setw(15) << dtw
                 << setw(15) << tputw << endl;
            if ( stop ) break;
          }
          delete source;
          delete readBack;
          if ( stop ) break;
        }

}


