package xdaq.xdesk.jfinder;

import javax.swing.*;

public class FinderCell
extends JLabel {
	FinderTreeNode node_;
	boolean selected_;
	
	public FinderCell(FinderTreeNode node) {
		node_ = node;
		selected_ = false;
	}
	
	/*
		public void setSelected (boolean selected)
	 {
			selected_ = selected;
	 }
	 
	 public boolean getSelected ()
	 {
		 return selected_;
	 }
	 */
	
	public FinderTreeNode getFinderTreeNode() {
		return node_;
	}
	
}
