package xdaq.xdesk.xcontrol;

import javax.swing.*;
import javax.swing.tree.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.applet.*;
import org.apache.log4j.*;

import xdaq.xdesk.tools.*;


public class XControl extends JApplet implements MenuBarListener 
{
	protected XControl controller_ = this;
	Logger logger = Logger.getLogger (XControl.class);	
	Vector windows_ = new Vector();
	
	HashMap openPartitions_ = new HashMap();
	InfoBusPublisher frameworkCommandPublisher_ = new InfoBusPublisher("framework");
	// Window close listener
	class XControlWindowListener extends WindowAdapter
	{
		XControlViewer viewer_;
		
		public XControlWindowListener ( XControlViewer viewer)
		{
			viewer_ = viewer;
		}
	
		// window adapter overriding method
		public void windowClosing (WindowEvent e) 
		{
			
			controller_.closeWindow(viewer_);			
		}
	}

	// these methods are used for checking alreaady opened partitions
	public void addPartition (String partition, XControlViewer viewer)
	{
		openPartitions_.put (partition, viewer);
	}
	
	public void removePartition (String partition)
	{
		openPartitions_.remove (partition);
	}
	
	public XControlViewer getPartition (String partition)
	{
		return (XControlViewer) openPartitions_.get (partition);
	}
        
        public void init()
	{	
		frameworkCommandPublisher_.publishItem ("command");
		AppletContext c = this.getAppletContext();
		if (c == null)
		{
			logger.error ("Applet context could not be retrieved");
		}
		
		//container_ = super.getContentPane();
		//window_ = new XControlViewer(this);
		//this.setJMenuBar (window_.getJMenuBar());
		//this.setJMenuBar (new XControlWindowMenuBar (window_));
		
		//menuBar_ = new XControlMenuBar (this);
		//setJMenuBar (menuBar_);		
	}
	
	// This start function is the same for all multiple windows applets
	//
	public void start()
	{
	    if (windows_.size() > 0)
	    {
	    	for (int i = 0; i < windows_.size(); i++)
		{
	    		JFrame f = (JFrame) windows_.elementAt (i);
			f.setVisible(true);
		}
	    } else 
	    {
	    	this.newWindow();	
	    }
	    
	}
	
	public void destroy()
	{
		// unpublish from InfoBus	
		frameworkCommandPublisher_.shutdown();
	
		// remove all windows
		for (int i = 0; i < windows_.size(); i++)
		{
			JFrame f = (JFrame) windows_.elementAt (i);
			f.setVisible(false);
			f.dispose();
		}

		// shutdown all viewers
		Collection c = openPartitions_.values();
		Object [] v = c.toArray(new Object[] {});
		for (int j = 0; j < v.length; j++)
		{
			((XControlViewer)v[j]).shutdown();
		}
	}
	
	
	public void newWindow()
	{
		//XControlWindow window = new XControlWindow();		
		//Container c = frame.getContentPane();
		JFrame frame = new JFrame();
		XControlViewer viewer = new XControlViewer( frame, this);
		XControlMenuBar menuBar = new XControlMenuBar(viewer);

		frame.setJMenuBar(menuBar);
		frame.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener (new XControlWindowListener( viewer));
		//frame.setJMenuBar(w.getJMenuBar());
		frame.getContentPane().add(viewer);
		frame.setSize (this.getWidth(), this.getHeight());
                frame.setTitle("XControl");
		frame.setVisible(true);	
		windows_.add(frame);
	}
	
	public void closeWindow(XControlViewer viewer)
	{
		viewer.shutdown();
		JFrame frame = viewer.getFrame();
						
		if (windows_.contains(frame))
		{
			windows_.remove(frame);
			frame.setVisible(false);
			frame.dispose();
		}	
	
	}
	
	
	public void stop()
	{
	}
	
	
	// Implements MenuBar Listeners	
	
	public void aboutAction()
	{
		System.out.println("About for this Daqlet");
	}
	
	public void switchAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("tasks",this) );
	}
	
	public void exitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("exit",this) );
	}
	public void quitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("quit",this) );
	}
	
	
	
}	
