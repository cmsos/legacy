#!/usr/bin/python
"""
This script is meant to restart any dead triggerd service. By dead it means that:
* The service is include in the xdaqd.<hostname>.conf STARTUP_LIST
* The service pid file exist
* The /proc/<pid> directory does not exist

In that case the service is restarted automatically using the triggerd daemon.

All the log information is appended in the /var/log/auto_restart.py.log file with the following format:
date time: auto_restart : <log>
"""

ZONE_FILE='/etc/subsystem.zone'
XDAQ_SETUP_ROOT='/opt/xdaq/share'
DAEMON='triggerd'
STATE_FILE="/var/log/auto_restart.py.%s.pckl" % DAEMON
MIN_GB_QUOTA=20
MIN_SEC_BETWEEN_CRASHES = 60*60

import logging
import sys
import statvfs
import os
import platform
import re
import pickle
import time

def execute_system(cmd):
    logger.debug(cmd)
    fin,fout = os.popen4(cmd)
    content = fout.read()
    logger.debug(content)
    os.wait()
    fin.close()
    fout.close()
    

def initialize_logger():
    global logger
    logger = logging.getLogger("autorestart.py")
    logger.setLevel(logging.DEBUG)
    
    h = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter("%(asctime)s %(levelname)s %(name)s - %(message)s")
    h.setFormatter(formatter);
    logger.addHandler(h)

def get_startup_list(fn):
    r_sl = re.compile("(^\s*STARTUP_LIST\s*=\s*\")(.*?)\"",re.DOTALL | re.MULTILINE)

    s = open(fn).read()
    m = r_sl.search(s)
    services =[]
    if m:
        services = m.group(2).split()

    return services

def get_services():
    xdaq_zone = open(ZONE_FILE).read().rstrip().lstrip()
    conf_dir = os.path.join(XDAQ_SETUP_ROOT,os.path.join(xdaq_zone,"conf"))

    hostname = platform.node()
    if os.path.isfile(os.path.join(conf_dir,"xdaqd.%s.conf" % hostname)):
        s = get_startup_list(os.path.join(conf_dir,"xdaqd.%s.conf" % hostname))
    else:
        s = get_startup_list(os.path.join(conf_dir,"xdaqd.conf"))

    return s

def is_service_dead(service):
    pid_file = os.path.join("/var/run","%s.pid" % service)
    if os.path.isfile(pid_file):
        pid = open(pid_file).read().rstrip().lstrip()
        proc_dir = os.path.join("/proc",pid)
        if not os.path.isdir(proc_dir):
            logger.info("pid file '%s' exists but process '%s' is dead"
                        % (pid_file,pid))
            return True

    return False

def restart(service):
    stmt = "/sbin/service %s restart %s" % (DAEMON,service)
    execute_system(stmt)

def enough_quota():
    stats = os.statvfs('/tmp')
    avail = stats[statvfs.F_BSIZE] * stats[statvfs.F_BAVAIL]/(1024**3)
    if avail > MIN_GB_QUOTA:
        return True
    else:
        logger.warning("Current disk quota below %s GB. Current quota= %s GB. The services won't be restarted automatically to avoid filling the possibility of filling the disk with core dumps from failing processes" % (MIN_GB_QUOTA,avail))
        return False

def load_service_history(services):
    try:
        try:
            f = open(STATE_FILE)
            return pickle.load(f)
        finally:
            f.close()
    except Exception,e:
        return dict()
    
def is_faulty(service,s_history):
    if not s_history.has_key(service):
        return False

    #data format changed
    if not isinstance(s_history[service],float):
        return False
    
    if (time.time()-s_history[service])> MIN_SEC_BETWEEN_CRASHES:
        return False
    else:
        logger.warning("XDAQ service '%s' has twice in less than %s seconds. It is faulty and needs debugging" % (service,MIN_SEC_BETWEEN_CRASHES))
        return True

def add_fault(service,s_history):
    #What happens if format has changed?
    s_history[service] = time.time()

def save_service_history(s_history):
    try:
        try:
            f = open(STATE_FILE,"w")
            pickle.dump(s_history,f)
        finally:
            f.close()
    except Exception,e:
        pass
    
def main():
    logger.debug("Checking service status...")
    if enough_quota():
        services = get_services()
        s_history = load_service_history(services)
        for s in services:
            if is_service_dead(s) and not is_faulty(s,s_history):
                add_fault(s,s_history)
                restart(s)

        save_service_history(s_history)
        
if __name__ == "__main__":
    initialize_logger()
    main()


