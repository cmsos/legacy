#ifndef __InputStreamInfoSpaceHandler
#define __InputStreamInfoSpaceHandler

#include "amc13controller/AMC13StreamInfoSpaceHandler.hh"
//#include "amc13controller/DataTracker.hh"

namespace amc13controller
{
    class InputStreamInfoSpaceHandler:public utils::StreamInfoSpaceHandler { //AMC13StreamInfoSpaceHandler {
    	public:
        	InputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS );
    		//void registerTrackerItems( DataTracker &tracker );
    };
}

#endif /* __InputStreamInfoSpaceHandler */
