#ifndef _tcds_bobr_BSTReceiverInfoSpaceUpdater_h_
#define _tcds_bobr_BSTReceiverInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace bobr {

    class VMEDeviceBOBR;

    class BSTReceiverInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      BSTReceiverInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::bobr::VMEDeviceBOBR const& hw,
                                  unsigned int const bstrNumber);
      virtual ~BSTReceiverInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::bobr::VMEDeviceBOBR const& getHw() const;

      // The number of the BST receiver to use: 1 or 2.
      unsigned int const bstrNumber_;

    };

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_BSTReceiverInfoSpaceUpdater_h_
