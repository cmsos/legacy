#ifndef _bril_dipprocessor_VacSummary_h_
#define _bril_dipprocessor_VacSummary_h_

#include <map>
#include "xdata/Float.h"
#include "xdata/String.h"
#include "bril/dipprocessor/DipAnalyzer.h"

namespace bril{
  namespace dip{

    class VacSummary: public DipAnalyzer{
    public:
      VacSummary(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName, xdaq::Application* owner);
      // destructor
      ~VacSummary();
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // concrete implementation
      virtual void handleMessage(DipSubscription* dipsub, DipData& message);      
    private:
      //Variables associated with the VacSummary task
      std::string m_dipRoot;
      std::map< std::string,float > m_gaugeValues;
      std::map< std::string,float > m_gaugePositions;
      std::string m_dipHeader;
      std::string m_dipTrailer;
      
      
    
  
    private:    
      void publish_to_dip();
      // nodefaut constructor
      VacSummary();
      // nocopy protection
      VacSummary(const VacSummary&);
      VacSummary& operator=(const VacSummary&);
    };
  }
}
#endif
