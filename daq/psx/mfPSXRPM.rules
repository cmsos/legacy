
rpm: $(PackageListLoop)

installrpm: $(PackageListLoop)

cleanrpm: $(PackageListLoop)

changelog: $(PackageListLoop)

ifndef INSTALL_PATH
INSTALL_PATH = /opt/xdaq
endif

PackagePath  = $(shell cd $(BUILD_HOME)/$(Project)/$(Package);pwd)
PWD          = $(shell pwd)

ifdef Package

ifndef PackageName
PackageName=$(shell awk -F'"' 'BEGIN{IGNORECASE=1} /package[ \t\r\f\v]*=/ {print $$2;}' $(PackagePath)/include/$(Package)/version.h)
endif

ifndef PACKAGE_VER_MAJOR
PACKAGE_VER_MAJOR=$(shell awk 'BEGIN{IGNORECASE=1} /define $(PackageName)_VERSION_MAJOR/ {print $$3;}' $(PackagePath)/include/$(Package)/version.h)
endif

ifndef PACKAGE_VER_MINOR
PACKAGE_VER_MINOR=$(shell awk 'BEGIN{IGNORECASE=1} /define $(PackageName)_VERSION_MINOR/ {print $$3;}' $(PackagePath)/include/$(Package)/version.h)
endif

ifndef PACKAGE_VER_PATCH
PACKAGE_VER_PATCH=$(shell awk 'BEGIN{IGNORECASE=1} /define $(PackageName)_VERSION_PATCH/ {print $$3;}' $(PackagePath)/include/$(Package)/version.h)
endif

ifndef PACKAGE_RELEASE
include  $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.release
endif

ifndef BUILD_VERSION
BUILD_VERSION=1
endif

ifndef BUILD_DISTRIBUTION
BUILD_DISTRIBUTION := $(shell $(XDAQ_ROOT)/$(BUILD_SUPPORT)/checkos.sh)
endif

ifndef BUILD_COMPILER
BUILD_COMPILER :=$(CC)$(shell $(CC) -dumpversion | sed -e 's/\./_/g')
endif


VER_EXISTS=no
ifneq ($(PACKAGE_VER_MAJOR),)
ifneq ($(PACKAGE_VER_MINOR),)
ifneq ($(PACKAGE_VER_PATCH),)
VER_EXISTS=yes
endif
endif
endif

REQUIRES_LIST=0
ifneq ($(PACKAGE_REQUIRED_PACKAGE_LIST),)
REQUIRES_LIST=1
endif

#
# Extract summary, description and authors
#
ifndef Description
Description = $(shell awk -F'"' 'BEGIN{IGNORECASE=1} /description[ \t\r\f\v]*=/ {print $$2;}' $(PackagePath)/include/$(Package)/version.h)
endif

ifndef Summary
Summary = $(shell awk -F'"' 'BEGIN{IGNORECASE=1} /summary[ \t\r\f\v]*=/ {print $$2;}' $(PackagePath)/include/$(Package)/version.h)
endif

ifndef Authors
Authors = $(shell awk -F'"' 'BEGIN{IGNORECASE=1} /authors[ \t\r\f\v]*=/ {print $$2;}' $(PackagePath)/include/$(Package)/version.h)
endif

ifndef Link
Link = $(shell awk -F'"' 'BEGIN{IGNORECASE=1} /link[ \t\r\f\v]*=/ {print $$2;}' $(PackagePath)/include/$(Package)/version.h)
endif



.PHONY: _rpmall

ifeq ($(VER_EXISTS),no)
_rpmall: fail
fail:
	$(error Error could not find a valid version.h in package '$(Package)')
else
_rpmall: spec_update makerpm
endif


# if for package makefile only if endif
#else
#_rpmall:
#	@echo "*** "$(Package) I am in the elese
endif

# 	rpmbuild  -bb -bl --target $(XDAQ_PLATFORM) --define  "_topdir $(PackagePath)/rpm/RPMBUILD"  $(PackagePath)/rpm/$(PackageName).spec


.PHONY: makerpm
makerpm:
	cd $(PackagePath)/projects; tar -xf psx-$(XDAQ_PLATFORM).tar
	mkdir -p $(PackagePath)/rpm/RPMBUILD/{RPMS/{$(XDAQ_PLATFORM)},SPECS,BUILD,SOURCES,SRPMS}
	tar -P -X $(XDAQ_ROOT)/$(BUILD_SUPPORT)/src.exclude -zcf  $(PackagePath)/rpm/RPMBUILD/SOURCES/$(PackageName)-$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)-$(BUILD_VERSION).$(PACKAGE_RELEASE).$(BUILD_DISTRIBUTION).$(BUILD_COMPILER).tgz $(PackagePath)
	rpmbuild  --quiet -bb -bl --define  "_topdir $(PackagePath)/rpm/RPMBUILD"  $(PackagePath)/rpm/$(PackageName).spec
	find  $(PackagePath)/rpm/RPMBUILD -name "*.rpm" -exec mv {} $(PackagePath)/rpm \;
	
.PHONY: spec_update
spec_update: 
	mkdir -p $(PackagePath)/rpm
	cp $(BUILD_HOME)/$(Project)/$(Package)/PSXSpec.template $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__release__#$(BUILD_VERSION).$(PACKAGE_RELEASE).$(BUILD_DISTRIBUTION).$(BUILD_COMPILER)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__version__#$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__prefix__#$(INSTALL_PATH)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__requires__#$(PACKAGE_REQUIRED_PACKAGE_LIST)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__package__#$(Package)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__packagedir__#$(PackagePath)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__packagename__#$(PackageName)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__os__#$(XDAQ_OS)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__platform__#$(XDAQ_PLATFORM)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__project__#$(PROJECT_NAMESPACE)$(Project)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__author__#$(Authors)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__summary__#$(Summary)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__description__#$(Description)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__url__#$(Link)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__buildarch__#$(XDAQ_PLATFORM)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__includedirs__#$(IncludeDirs)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__winccoaroot__#$(PVSS_II_HOME)#' $(PackagePath)/rpm/$(PackageName).spec


.PHONY: _cleanrpmall
_cleanrpmall:
	-rm -rf $(PackagePath)/rpm
	
	
.PHONY: _installrpmall
_installrpmall:
	mkdir -p $(INSTALL_PREFIX)/rpm
	cp $(PackagePath)/rpm/*.rpm $(INSTALL_PREFIX)/rpm
 

.PHONY: _changelogall
_changelogall:
	cd $(PackagePath);\
	svn --verbose log > ChangeLog
 





