#!/usr/bin/env python

###############################################################################
## Helper to scan the input alignment settings (i.e., idelay and
## bitslip) on a FED TTS input into a PI.
## NOTE: This is for debugging/diagnostic purposes only. The PI TTS
## inputs are supposed to auto-align.
###############################################################################

import sys
import time

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

SLEEP_TIME = 0.1
NUM_POLL_ITER = 1

IDELAY_MIN = 0
IDELAY_MAX = 31
NUM_IDELAY = IDELAY_MAX - IDELAY_MIN + 1
BITSLIP_MIN = 0
BITSLIP_MAX = 4

NUM_FEDS_PER_PI = 10

###############################################################################

def check_value(val_str):
    value = int(val_str)
    if value < 1 or value > NUM_FEDS_PER_PI:
        msg_base = "Sorry, but {0:d} as TTS/FED input number" \
                   " does not make sense."
        raise argparse.ArgumentTypeError(msg_base.format(value))
    return value

###############################################################################

def run_scan(hw,
             reg_name_alignment_mode,
             reg_name_serdes_reset,
             reg_name_idelay_ctrl,
             reg_name_idelay_load,
             reg_name_bitslip_strobe,
             reg_name_aligned):

    # Switch to manual alignment mode.
    print "Switching to manual alignment mode"
    hw.write(reg_name_alignment_mode, 0x1)

    #----------

    # Loop over all idelay settings, bit-slipping for each one of
    # them.
    print "Scanning input alignment settings"
    hw.write(reg_name_serdes_reset, 0x0)
    hw.write(reg_name_idelay_load, 0x0)
    hw.write(reg_name_bitslip_strobe, 0x0)
    window = 0x0
    last_good_bitslip = None
    for (num_iter, idelay) in enumerate(xrange(IDELAY_MIN, IDELAY_MAX + 1)):

        # Reset SerDes to start with a clean slate.
        hw.write(reg_name_serdes_reset, 0x1)
        hw.write(reg_name_serdes_reset, 0x0)

        # Apply idelay setting.
        hw.write(reg_name_idelay_ctrl, idelay)
        hw.write(reg_name_idelay_load, 0x1)
        hw.write(reg_name_idelay_load, 0x0)

        # Scan bitslips: five steps, each (internally) with two
        # possible word alignment settings.
        found_alignment = False
        found_jump = False
        # Strobe to trigger a bit slip.
        for bitslip in xrange(BITSLIP_MIN, BITSLIP_MAX + 1):
            hw.write(reg_name_bitslip_strobe, 0x1)
            hw.write(reg_name_bitslip_strobe, 0x0)

            # Check for alignment.
            poll_results = []
            for i in xrange(NUM_POLL_ITER):
                tmp = hw.read(reg_name_aligned)
                is_aligned = (tmp == 0x1)
                poll_results.append(is_aligned)

            poll_shows_alignment = False
            if list(set(poll_results)) == [True]:
                poll_shows_alignment = True
                # break

            made_jump = False
            if poll_shows_alignment:
                if ((bitslip != last_good_bitslip)) \
                   and (not last_good_bitslip is None):
                    made_jump = True
                    found_jump = True
                last_good_bitslip = bitslip
                found_alignment = True

            jump_str = ""
            if made_jump:
                jump_str = " !!! Jumped to new alignment settings !!!"

            if poll_shows_alignment or True:
                print "idelay = {0:2d}" \
                    ", bitslip = {1:d}" \
                    ": aligned = {2:d}{3:s}".format(
                        idelay,
                        bitslip,
                        poll_shows_alignment,
                        jump_str
                    )

        window |= ((found_alignment and not found_jump) << num_iter)
        print "DEBUG JGH window = 0b{0:0{1:d}b}".format(window, idelay - IDELAY_MIN + 1)

        # Don't be too rough...
        time.sleep(SLEEP_TIME)

    # Switch back to auto-align mode.
    print "Switching back to auto-alignment mode"
    hw.write(reg_name_alignment_mode, 0x0)

    # End of run_scan().
    return window

###############################################################################

def calc_window_midpoint(window):
    # NOTE: This comes almost straight from the VHDL implementation.
    if window == 0xffffffff:
        midpoint = 16
    else:
        sreg = (window << 32) + window
        r_edge_found = False
        f_edge_found = False
        r_edge = 0
        f_edge = 0
        cnt = 0
        prev = 0b0000
        if (window & 0x1):
            prev = 0b1111
        while cnt < 63:
            if (prev == 0b0111) and (sreg & 0x1) and not r_edge_found:
                r_edge = cnt
                r_edge_found = True
                # print "DEBUG JGH --> found rising edge"
            elif (prev == 0b1111) and not (sreg & 0x1) and r_edge_found and not f_edge_found:
                f_edge = cnt
                f_edge_found = True
                # print "DEBUG JGH --> found falling edge"
            prev = ((prev & 0b111) << 1) + (sreg & 0x1)
            sreg = (sreg >> 1)
            cnt += 1
            # print "DEBUG JGH prev = {0:04b}".format(prev)
            # print "DEBUG JGH   (sreg & 0x1) = {0:d}".format(sreg & 0x1)
        diff = f_edge - r_edge
        midpoint_6b = r_edge + ((diff & 0b111110) >> 1)
        midpoint = (midpoint_6b & 0b11111)
        # print "DEBUG JGH r_edge_found = {0:b}".format(r_edge_found)
        # print "DEBUG JGH   r_edge = {0:d}".format(r_edge)
        # print "DEBUG JGH f_edge_found = {0:b}".format(f_edge_found)
        # print "DEBUG JGH   f_edge = {0:d}".format(f_edge)
        # print "DEBUG JGH diff = {0:d}".format(diff)
        # print "DEBUG JGH midpoint_6b = {0:d}".format(midpoint_6b)
        # print "DEBUG JGH midpoint = {0:d}".format(midpoint)
    # End of calc_window_midpoint().
    return midpoint

###############################################################################

class PITTSScan(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(PITTSScan, self).__init__(description, usage, epilog)
        self.fed_num = None
        # End of pre_hook().

    def setup_parser_custom(self):
        super(PITTSScan, self).setup_parser_custom()
        parser = self.parser

        # Add the choice of FED I/O channel.
        help_str = "FED TTS input number to scan. " \
                   "Default: 1."
        parser.add_argument("fed_number",
                            metavar="fed-number",
                            type=check_value,
                            help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        super(PITTSScan, self).handle_args()
        self.fed_num = int(self.args.fed_number)
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        pi = get_pi_hw(network_address, controlhub_address, verbose=self.verbose)

        if not pi:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        reg_name_alignment_mode = \
            "pi.tts_serdes_ctrl.align_mode"
        reg_name_serdes_reset = \
            "pi.tts_serdes_ctrl.reset.fed{0:d}".format(self.fed_num)
        reg_name_idelay_ctrl = \
            "pi.tts_idelay_ctrl.tap_ctrl.fed{0:d}".format(self.fed_num)
        reg_name_idelay_load = \
            "pi.tts_idelay_ctrl.tap_ld.fed{0:d}".format(self.fed_num)
        reg_name_bitslip_strobe = \
            "pi.tts_bitslip_ctrl.bitslip_strobe.fed{0:d}".format(self.fed_num)
        reg_name_aligned = \
            "pi.tts_link_status.fed{0:d}.aligned".format(self.fed_num)
        reg_name_window = \
            "pi.tts_alignment_monitor.scan_window.fed{0:d}".format(self.fed_num)
        reg_name_midpoint = \
            "pi.tts_alignment_monitor.scan_midpoint.fed{0:d}".format(self.fed_num)
        reg_name_scan_success = \
            "pi.tts_alignment_monitor.scan_success.fed{0:d}".format(self.fed_num)

        #----------

        window_scan = run_scan(pi,
                               reg_name_alignment_mode,
                               reg_name_serdes_reset,
                               reg_name_idelay_ctrl,
                               reg_name_idelay_load,
                               reg_name_bitslip_strobe,
                               reg_name_aligned)
        midpoint_scan = calc_window_midpoint(window_scan)

        #----------

        # By now the firmware should have taken over the alignment
        # procedure again, and auto-aligned the link.
        time.sleep(1)
        tmp = pi.read(reg_name_scan_success)
        if tmp != 0x1:
            print "ARGH! Scan did not finish successfully!"
        window_fw = pi.read(reg_name_window)
        midpoint_fw = pi.read(reg_name_midpoint)

        #----------

        print self.sep_line
        print "FED I/O number {0:d} idelay window:".format(self.fed_num)
        print "  according to scan script:"
        print "    0x{0:08x}".format(window_scan)
        print "    0b{0:032b}".format(window_scan)
        print "      {0:s}{1:s}".format(" " * (NUM_IDELAY - midpoint_scan - 1), "^")
        print "    --> midpoint = {0:d}".format(midpoint_scan)
        print "  according to firmware:"
        print "    0x{0:08x}".format(window_fw)
        print "    0b{0:032b}".format(window_fw)
        print "      {0:s}{1:s}".format(" " * (NUM_IDELAY - midpoint_fw - 1), "^")
        print "    --> midpoint = {0:d}".format(midpoint_fw)
        print self.sep_line

        #----------

        # End of main().

    # End of class PITTSScan.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper to scan the input alignment settings" \
        " (i.e., idelay, bitslip, etc.) on a FED TTS input into a PI." \
        "  NOTE: This is for debugging/diagnostic purposes only." \
        " The PI TTS inputs are supposed to auto-align."

    res = PITTSScan(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
