package xdaq.tools.daqlet;

import xdaq.tools.daqlet.DaqletTableModel;
import xdaq.tools.daqlet.Daqlet;
import java.util.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

public class DaqletList extends JTable
{
	DaqletTableModel model;
	JSplitPane daqletListPane_; // where to display daqlet content
	Daqlet currentlyShown_ = null;
	int currentRow_ = -1; // force a change in row with first mouse click (row > 0)
	JLabel emptyPane_ = new JLabel("No Daqlet selected");
	public DaqletList (JSplitPane daqletListPane)
	{
		daqletListPane_ = daqletListPane;
		model = new DaqletTableModel();
		//this.setAutoCreateColumnsFromModel (false);
		this.setModel ( model );
		this.setShowHorizontalLines (false);
		this.setShowVerticalLines (false);
		this.setShowGrid(false);
                
                // Get default selection mode
                int selMode = getSelectionModel().getSelectionMode();
    
    
                // Allow only single a selection
                setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		/*
	
		for (int k = 0; k < model.getColumnCount(); k++)
		{
			DefaultTableCellRenderer r = new DaqletTableCellRenderer();
			TableColumn c = new TableColumn (k, 100, r, null);
			addColumn (c);
		}
                */
	
		//this.setDefaultRenderer(String.class, new DaqletTableCellRenderer());
		
		// Add selection listener
		this.addMouseListener ( new MouseAdapter() {
			public void mouseClicked (MouseEvent e) {
				int row = getSelectedRow();
				int column = getSelectedColumn();
				if ((row == -1) || (column == -1)) return;
				else displayDaqlet (row);
			}
		} );
	}
	
	
	public void displayCurrentDaqlet ()
	{
		JScrollPane p = null;
		
		if (currentlyShown_ != null)
		{
			p = new JScrollPane(currentlyShown_.getContentPane());
			
		} else {
			p = new JScrollPane( emptyPane_ );
		}
		
		daqletListPane_.setRightComponent ( p );
		daqletListPane_.setDividerLocation ( 150 );		
	}
	
	public Daqlet currentlyDisplayedDaqlet()
	{
		return currentlyShown_;
	}
	
	public void stopCurrentDaqlet()
	{
		if (currentlyShown_ != null) currentlyShown_.stop();
	}
	
	public void startCurrentDaqlet()
	{
		if (currentlyShown_ != null) currentlyShown_.start();
	}
	
	protected void displayDaqlet (int row)
	{
		// if the clicked row has not changed since 
		// the last mouse click - do nothing.
		if (currentRow_ == row) return;
		
		currentRow_ = row;
	
		Daqlet d = model.getDaqlet(row);
		
		// Stop currently shown Daqlet
		stopCurrentDaqlet();
			
		// Start new applet
		currentlyShown_ = d;
		startCurrentDaqlet();
		
		displayCurrentDaqlet();
	}
	
	public void add (Daqlet d, Properties p)
	{
		model.add (d, p);
	}	
}
