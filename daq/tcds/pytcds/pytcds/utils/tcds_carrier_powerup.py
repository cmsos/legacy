#!/usr/bin/env python

###############################################################################
## Power-up the carrier. The underlying procedure is different for
## different carriers: for the GLIB only the SFPs need to be enabled,
## while the FC7 first requires the FMC powering to be configured.
###############################################################################

import sys

from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class CarrierPowerUp(CmdLineBase):

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Powering up carrier."

        carrier.perform_powerup_sequence()
        # End of main().

    # End of class CarrierPowerUp.

###############################################################################

if __name__ == "__main__":

    res = CarrierPowerUp().run()
    print "Done"
    sys.exit(res)

###############################################################################
