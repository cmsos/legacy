#ifndef _tcds_pi_PITTCInfoSpaceHandler_h_
#define _tcds_pi_PITTCInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace pi {

    class PITTCInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      PITTCInfoSpaceHandler(xdaq::Application& xdaqApp,
                            tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PITTCInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PITTCInfoSpaceHandler_h_
