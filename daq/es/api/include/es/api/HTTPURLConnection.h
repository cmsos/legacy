// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef es_api_HTTPURLConnection_h_
#define es_api_HTTPURLConnection_h_

#include "es/api/exception/Exception.h"
#include <string>

namespace es
{

	namespace api
	{

		class HTTPURLConnection
		{
			public:

				HTTPURLConnection() ;
				~HTTPURLConnection();

				//! receive  from established HTTP connection and returns data into a BufRef
				std::string receiveFrom() ;

				//! send data to an established HTTP connection
				void sendTo
				(
						char * path,
						char * host,
						unsigned int port,
						const char * buf,
						size_t len
				)
				;

				//! send data to an established HTTP connection
				void sendTo
				(
						char * path,
						char * host,
						unsigned int port,
						const char * buf,
						size_t len,
						const char* soapAction
				)
				;


				//! close connection
				void close();

				//! connect to URL
				void connect(const std::string& host, unsigned int port) ;

				//! send buffer of given lenght
				void send(const char * buf, size_t len)  ;

			protected:

				//! receive len characters into buf
				ssize_t receive(char * buf , size_t len )  ;


				//! Helper to re-creating a socket
				void open() ;

				std::string extractMIMEBoundary(const char * buf , size_t size) ;

				void writeHttpPostMIMEHeader
				(
						char * path,
						char * host,
						unsigned int port,
						std::string & boundaryStr,
						size_t len
				)
				;

				void writeHttpPostMIMEHeader
				(
						char * path,
						char * host,
						unsigned int port,
						std::string & boundaryStr,
						size_t len,
						const char* soapAction
				)
				;

				void writeHttpPostHeader
				(
						char * path,
						char * host,
						unsigned int port,
						size_t len,
						const char* soapAction
				)
				;

				void writeHttpPostHeader
				(
						char * path,
						char * host,
						unsigned int port,
						size_t len
				)
				;

				int socket_;
		};

	}}
#endif
