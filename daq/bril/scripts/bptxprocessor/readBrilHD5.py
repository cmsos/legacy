#! /nfshome0/lumipro/brilconda/bin/python
import numpy as np
import tables as tb

maskB1 = np.zeros(3564)
maskB1[1] = 1

maskB1[666] = 1

#f=tb.open_file('//brildata/16/4879/4879_272018_1604290128_1604290137.hd5','r')
#f=tb.open_file('//brildata/16/4910/4910_273017_1605102130_1605110450.hd5','r')
f=tb.open_file('//brildata/16/4937/4937_273537_1605171616_1605172125.hd5','r')

s=f.getNode('/ScopeData')

for name in s.colnames:
    print(name, ':= %s, %s' % (s.coldtypes[name], s.coldtypes[name].shape))

for i in s.iterrows():
    # print i['runnum'],i['lsnum'],i['nbnum'],i['TOT_INT_B1'],i['TOT_INT_B2']

    # print "length of INT array =", len(i['INT_B1'])

    nBX=0
    for x in xrange(len(i['INT_B1'])):
        #      if maskB1[x] == 1:
        if i['INT_B1'][x]> 0:
            nBX+=1
            print "BX=", x, "INT=", i['INT_B1'][x], "LEN = ", float(i['LEN_B1'][x])/1000

    # print i['nCol']
    
    if nBX != i['nB1']:
        print "Total number of good bunches = ", nBX, 'Compare with nB1', i['nB1']

f.close()
