#!/bin/sh

curl --stderr /dev/null \
-H "SOAPAction: urn:xdaq-application:class=BU,instance=0" \
-d "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" 
  xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" 
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" 
  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
	<psx:dpSet xmlns:psx=\"http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd\">
		<psx:dp name=\"MyRawHtChannel1.actualVoltate:_online.._value\">333.456</psx:dp>
		<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\">444.444</psx:dp>
        </psx:dpSet>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>" http://lxcmd101.cern.ch:1964/pvss

echo ""
