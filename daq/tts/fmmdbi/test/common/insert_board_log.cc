#include "tts/fmmdbi/FMMDBI.hh"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdlib.h>


namespace tts {
  namespace fmmdbi {
    tts::FMMDBI* getOracleDBI(std::string user, std::string pwd, std::string db);
  }
}

int main(int argc, char **argv) {

  if (argc != 3) {
    std::cout << "usage: insert_board_log.exe Boardnr logstring" << std::endl;
    std::cout << "Boardnr = last two digits of SN" << std::endl;
    exit (-1);
  }

  tts::FMMDBI *dbi = tts::fmmdbi::getOracleDBI("CMS_DAQ_HSAKULIN", "a3a3a3a3", "devdb10");

  std::stringstream ss;
  ss << "31001020100402000" << std::setw(2) << argv[1];
  dbi->insertBoardLog(ss.str(), getenv("LOGNAME"), argv[2]);
}
