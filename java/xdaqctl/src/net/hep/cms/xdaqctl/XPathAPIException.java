/*
 * XPathAPIException
 *
 * $Id: XPathAPIException.java,v 4.1 2007/03/26 14:40:24 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

/**
 * Exception wrappter for XPathAPI
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XPathAPIException extends Exception
{
	public XPathAPIException() {}
	public XPathAPIException(String message) { super(message); }
	public XPathAPIException(Throwable cause) { super(cause); }
	public XPathAPIException(String message, Throwable cause) {
		super(message, cause);
	}
}

// End of file
// vim: set ts=4 sw=4:
