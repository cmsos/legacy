#include "tcds/rfdippub/RFRXDDataInfoSpaceHandler.h"

// #include "tcds/rfdippub/WebTableRFRXDDataInfo.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::rfdippub::RFRXDDataInfoSpaceHandler::RFRXDDataInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                                     tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-rfdippub-rfrxd", updater)
{
}

tcds::rfdippub::RFRXDDataInfoSpaceHandler::~RFRXDDataInfoSpaceHandler()
{
}

void
tcds::rfdippub::RFRXDDataInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  monitor.newItemSet("itemset-rfdippub-rfrxd");
}

void
tcds::rfdippub::RFRXDDataInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                         tcds::utils::Monitor& monitor,
                                                                         std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "RFRXD info" : forceTabName;

  webServer.registerTab(tabName,
                        "Information from the RFRXDController flashlists",
                        1);
}

// std::string
// tcds::rfdippub::RFRXDDataInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
// {
//   // Specialized formatting rules for the TTCSpy logging settings enums.
//   std::string res = tcds::utils::escapeAsJSONString(kInvalidItemString_);
//   if (item->isValid())
//     {
//       std::string name = item->name();
//       if (name == "apps_info")
//         {
//           // Special in the sense that this is something that needs to
//           // be interpreted as a proper JavaScript object, so let's
//           // not add any more double quotes.
//           res = getString(name);
//         }
//       else
//         {
//           // For everything else simply call the generic formatter.
//           res = InfoSpaceHandler::formatItem(item);
//         }
//     }
//   return res;
// }
