#ifndef __FedkitDataSource
#define __FedkitDataSource

#include "ferol40/OpticalDataSource.hh"

namespace ferol40
{
    class FedkitDataSource : public OpticalDataSource 
    {
    public:
        FedkitDataSource( HAL::HardwareDeviceInterface *device_P,
                          utils::InfoSpaceHandler &appIS,
                          Logger logger );
        void setDataSource()  const;
    };
}

#endif /* __FedkitDataSource */
