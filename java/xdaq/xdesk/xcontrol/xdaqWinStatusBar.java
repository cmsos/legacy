package xdaq.tools.win;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

public class xdaqWinStatusBar extends JPanel
{
	JProgressBar 	bar_;
	JLabel		info_;
	int lastValue_;

	public xdaqWinStatusBar()
	{
		BorderLayout layout = new BorderLayout();
		//layout.setVgap (5);
		//layout.setHgap (5);
		
		this.setLayout ( layout );
		//this.setBorder ( new EtchedBorder ( EtchedBorder.RAISED) );
		
		//Border etched = BorderFactory.createEtchedBorder (); 
		Border paneEtched = BorderFactory.createEmptyBorder (5, 5, 5, 5);
		this.setBorder ( BorderFactory.createCompoundBorder ( new EtchedBorder ( EtchedBorder.RAISED), paneEtched ) );
		
		bar_ = new JProgressBar();
		bar_.setBorder ( new EtchedBorder ( EtchedBorder.RAISED) );
		
		lastValue_ = 0;
		info_ = new JLabel ("");
		
		
		this.add ( info_, BorderLayout.WEST );
		
		this.add ( bar_, BorderLayout.EAST );	
	}
	
	// Sets the total number of items for
	// which the progress bar indicates progress
	//
	public void setProgressItems (int items)
	{
		bar_.setMinimum (0);
		bar_.setMaximum (items);
		//bar_.setStringPainted (true);
		resetProgressBar();
	}
	
	// Resets the progress bar to zero
	//
	public void resetProgressBar()
	{
		lastValue_ = 0;
		bar_.setValue (lastValue_);
	}
	
	// Returns the current item in progress
	// after having advances the bar
	public int advanceProgressBar(int items)
	{
		lastValue_+= items;
		bar_.setValue (lastValue_ );
		return lastValue_;
	}
	
	// Displays information in the status bar
	// type one of "ERROR", "INFORMATION", "WARNING", "NONE"
	//
	public void setStatus (String info, String type)
	{
		info_.setText ( info );
	}
}
