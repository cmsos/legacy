#ifndef _interface_bril_CompoundDataStreamer_h
#define _interface_bril_CompoundDataStreamer_h
#include <string.h>
#include <string>
#include <vector>
#include <stdexcept>
#include <cstdlib>
#include <iostream>
#include "interface/bril/Utils.h"
namespace interface{ namespace bril{     
   
class CompoundDataStreamer{
 public:
  struct Column{
    std::string name;
    std::string type;
    int size;
    int nitems;
  };
  explicit CompoundDataStreamer(const std::string& rowdict):m_rowsize(0){
    std::vector<std::string> fieldstrs;
    fieldstrs = Utils::splitstring(rowdict,' ');
    for( std::vector<std::string>::iterator it=fieldstrs.begin(); it!=fieldstrs.end(); ++it ){
      std::string fieldstr = *it;
      std::vector<std::string> columndef = Utils::splitstring(fieldstr.c_str(),':');
      if(columndef.size()!=3){
	throw std::range_error("size of column definition out of range");
      }
      Column col;
      col.name = columndef[0];
      col.type = columndef[1];
      col.nitems = atoi(columndef[2].c_str());
      if(col.type=="uint32"||col.type=="int32"||col.type=="float"){
	col.size = col.nitems*4;
      }else if(col.type=="uint64"||col.type=="int64"||col.type=="double"){
	col.size = col.nitems*8;
      }else if(col.type=="uint8"||col.type=="int8"||col.type=="bool"){
	col.size = col.nitems;
      }else if(col.type=="uint16"||col.type=="int16"){
	col.size = col.nitems*2;
      }else if(col.type.substr(0,3)=="str"){
	std::string strmaxlen = col.type.substr(3,col.type.size()-3);
	int maxlen = atoi( strmaxlen.c_str() );
	col.size = col.nitems*(maxlen);
      }else{
	throw std::runtime_error("unrecognized column type "+col.type);
      }
      m_rowsize += col.size;
      m_columns.push_back(col);
    }
  }

  inline size_t datasize() const{ return m_rowsize;}

  inline size_t get_offset(int column_index) const{ size_t offset = 0; for(int i=0;i<column_index;++i)	offset += m_columns[i].size;
    return offset;}

  inline size_t get_fieldsize(int column_index)const{ return m_columns[column_index].size; }

  inline size_t ncolumns()const{ return m_columns.size(); }

  inline const Column& column(int column_index) const{ return m_columns[column_index]; }
  bool insert_field( unsigned char* bufref,  const std::string& column_name, const void* fielddata){
    int column_index = -1;
    for(int i=0;i<(int)m_columns.size();++i){
      if(m_columns[i].name == column_name){
	column_index = i; break;
      }
    }
    if(column_index<0) return false;    
    return insert_field(bufref, column_index, fielddata);
  }

  bool insert_field( unsigned char* bufref, int column_index, const void* fielddata){
    if((size_t)column_index<m_columns.size()){
      if(m_columns[column_index].type.substr(0,3)=="str"){
	memset( bufref+get_offset(column_index), 0 , get_fieldsize(column_index) );
	memcpy( bufref+get_offset(column_index), fielddata, strlen((char*)fielddata) );   
      }else{
	memcpy( bufref+get_offset(column_index), fielddata, get_fieldsize(column_index) );      
      }
      return true;
    }
    return false;
  }

  bool extract_field( void* fielddata, int column_index, unsigned char* bufref){
    if((size_t)column_index<m_columns.size()){
      memcpy( fielddata, bufref+get_offset(column_index), get_fieldsize(column_index) );
      return true;
    }
    return false;
  }

  bool extract_field( void* fielddata, const std::string& column_name, unsigned char* bufref){
    int column_index = -1;
    for(int i=0;i<(int)m_columns.size();++i){
      if(m_columns[i].name == column_name){
	column_index = i; break;
      }
    }
    if(column_index<0) return false;
    return extract_field(fielddata,column_index,bufref);
  }

private:
  size_t m_rowsize;
  std::vector<Column> m_columns;
};

  }}
#endif
