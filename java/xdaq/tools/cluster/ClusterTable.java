package xdaq.tools.cluster;

import javax.swing.*;
import java.awt.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.util.*;
import java.net.*;
import java.io.*;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import org.xml.sax.*;

class ClusterTableException extends Exception
{
	ClusterTableException () { super(); }
	ClusterTableException ( String s ) { super(s); }
}

class ClusterTable extends AbstractTableModel
{
	private ImageIcon readyImage_;
	private ImageIcon runningImage_;
	private ImageIcon failedImage_;

	public ClusterTable (String url) throws ClusterTableException
	{
		readyImage_ = new ImageIcon ("./icons/bulb-off30.gif");
		runningImage_ = new ImageIcon ("./icons/bulb-on30.gif");
		failedImage_ = new ImageIcon ("./icons/bulb-error30.gif");
	
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse ( url );
			NodeList clusterTag = document.getElementsByTagName ( "Cluster" );
			NamedNodeMap clusterAttributes = clusterTag.item(0).getAttributes();
			name_ = clusterAttributes.getNamedItem ("name").getNodeValue();
			
			NodeList list = document.getElementsByTagName ( "Node" );
			rows_ = list.getLength();
			columns_ = 6;
			
			clusterTable_ = new Object[rows_][columns_];
			
			for (int i = 0; i < list.getLength(); i++) {
				NamedNodeMap attributes = list.item(i).getAttributes();
				String enabled = attributes.getNamedItem ("enabled").getNodeValue();
				if (enabled.equals("true")) 
					clusterTable_[i][0] = Boolean.TRUE;
				else
					clusterTable_[i][0] = Boolean.FALSE;
					
				clusterTable_[i][1] = attributes.getNamedItem ("host").getNodeValue();
				clusterTable_[i][2] = attributes.getNamedItem ("architecture").getNodeValue();
				clusterTable_[i][3] = attributes.getNamedItem ("protocol").getNodeValue();
				
				NodeList commands = list.item(i).getChildNodes();
				String commandV = new String();
				for (int j = 0; j < commands.getLength(); j++) {
					if (commands.item(j).getNodeType() != Node.TEXT_NODE) {
						commandV += commands.item(j).getFirstChild().getNodeValue();
					
						if ( j < commands.getLength()-2 ) {
							commandV += ", ";
						}
					}
				}
				
				clusterTable_[i][4] = commandV;
				
				// place the image for ready state
				clusterTable_[i][5] = readyImage_;
			}	
		} catch (SAXException e) {
			// Error during parsing.
			Exception x = e;
			if (e.getException() != null)
				x = e.getException();
			x.printStackTrace();
			throw new ClusterTableException (e.toString());
		} catch (ParserConfigurationException e) {
			// Parser can't be built
			e.printStackTrace();
			throw new ClusterTableException (e.toString());
		} catch (IOException e) {
			e.printStackTrace();
			throw new ClusterTableException (e.toString());
		}
	}
	
	public Vector getCommands (int node)
	{
		Vector v = new Vector();
		String commands = (String) clusterTable_[node][4];
		int from = 0;
		int to = 0;
		while ( to < commands.length() ) {
			to = commands.indexOf (", ", from);
			if (to == -1) to = commands.length();
			v.addElement ( commands.substring (from, to) );
			// Advance two characters for command and space: ", "
			from = to+2;
		}
		
		//System.out.println ("Vector: " + v.toString());
		
		return v;
	}
	
	public int getNumNodes ()
	{
		return rows_;
	}
	
	public String getArchitecture ( int node )
	{
		return (String) clusterTable_[node][2];
	}
	
	public String getHostname ( int node )
	{
		return (String) clusterTable_[node][1];
	}
	
	public String getProtocol ( int node )
	{
		return (String) clusterTable_[node][3];
	}
	
	
	public Boolean enabled ( int node )
	{
		return (Boolean) clusterTable_[node][0];
	}
	
	public void enable ( int node, Boolean state )
	{
		clusterTable_[node][0] = state;
	}
	
	public void setState ( int node, String runState)
	{
		if (runState.equals ("ready")) clusterTable_[node][5] = readyImage_;
		if (runState.equals ("running")) clusterTable_[node][5] = runningImage_;
		if (runState.equals ("failed")) clusterTable_[node][5] = failedImage_;
		fireTableCellUpdated ( node, 5);
		
	}
	
	// ----------------------------------
	// Interface for rendering methods
	// ----------------------------------
	
	
	
	public int getRowCount ()
	{
		return rows_;
	}
	
	public int getColumnCount ()
	{
		return columns_;
	}
	
	public Object getValueAt ( int r, int c )
	{
		return clusterTable_[r][c];
	}
	
	public String getColumnName ( int c )
	{
		return names_[c];
	}
	
	public Class getColumnClass ( int c )
	{
		return (clusterTable_[0][c]).getClass();
	}
	
	public boolean isCellEditable ( int r, int c )
	{
		return true;
	}
	
	public void setValueAt ( Object value, int r, int c )
	{
		clusterTable_[r][c] = value;
		fireTableCellUpdated ( r, c);
	} 
	
	public String getName ()
	{
		return name_;
	}
	
	
	private Object clusterTable_[][];
	private int rows_;
	private int columns_;
	private String name_;
	private String names_[] = {"Enabled", "Hostname", "Architecture", "Protocol", "Command", "State"};
};
