#ifndef _tcds_phasemon_HwStatusInfoSpaceUpdater_h_
#define _tcds_phasemon_HwStatusInfoSpaceUpdater_h_

#include "tcds/hwutilstca/HwStatusInfoSpaceUpdaterTCA.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace phasemon {

    class TCADevicePhaseMon;

    class HwStatusInfoSpaceUpdater : public tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::phasemon::TCADevicePhaseMon const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

    private:
      tcds::phasemon::TCADevicePhaseMon const& getHw() const;

      tcds::phasemon::TCADevicePhaseMon const& hw_;

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_HwStatusInfoSpaceUpdater_h_
