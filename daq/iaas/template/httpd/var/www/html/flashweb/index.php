<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
<script type="text/javascript" src="js/toggle.js"></script>
</head>

<body>

<?php
$headerTitle='CMS Flashweb';
include_once ('header.php');
include_once ('config.php');
include_once ('tools.php');

ini_set ( 'display_errors', 'On' );
error_reporting ( E_ALL | E_STRICT );

echo '<form id="zoneListForm" action="" method="post">';
echo '<table>';
echo '<thead>';
echo '<tr>';
echo '<th><input type="checkbox" onClick="toggle(this)" value="all"/></th>';
echo '<th>Zone</th>';
echo '<th>Flashlist count</th>';
echo '<th>Last update</th>';
echo '</tr>';
echo '</thead>';

echo '<tbody>';

$templates = retrieveTemplates ( $config ['elasticsearchurl'], '*-template' );

$zones = array ();
foreach ( $templates as $key => $val ) {
	$mappings = $val ["mappings"];
	$flashlist = key ( $mappings );
	if (validateSignature ( $flashlist, $mappings, $config ['signature'] )) {
		$zone = $mappings [$flashlist] ["_meta"] ["zone"];
		if (! array_key_exists ( $zone, $zones )) {
			$zones [$zone] = 1;
		} else {
			$zones [$zone] ++;
		}
	}
}

foreach ( $zones as $key => $val ) {
	$zonename = $key;
	echo '<tr>';
	echo '<td>';
	echo '<input type="checkbox" name="chk_group[]" value="' . $zonename . '"/>';
	echo '</td>';
	echo '<td>';
	echo '<a href="types.php?zone=' . $zonename . '">' . $zonename . '</a>';
	echo '</td>';
	echo '<td>';
	echo $val;
	echo '</td>';
	echo '<td>';
	echo lastUpdate($config ['elasticsearchurl'], $zonename . '-all');
	echo '</td>';
	echo '</tr>';
}
echo '</tbody>';
echo '</table>';

echo '<br>';

echo '<input type="hidden" name="elasticsearchurl" value="' . $config ['elasticsearchurl'] . '"/>';
echo '<input type="submit" name="delete" value="Delete selection" onclick="doDeleteSelection()"/>';
echo '<input type="submit" name="rotate" value="Rotate selection" onclick="doRotateSelection()"/>';
echo '</form>';
?>

<script>
form = document.getElementById("zoneListForm");

function doDeleteSelection() {
	form.action="delete_zones.php";
	form.submit();
}

function doRotateSelection() {
	form.action="force_rotate.php";
	form.submit();
}
</script>

<div>

<footer>Copyright © XDAQ 2016</footer>

</body>
</html>
