#include "bril/lumistore/hdf5/H5Utils.h"
#include "bril/lumistore/hdf5/Row.h"
#include "hdf5.h"
#include "hdf5_hl.h"
#include <stdlib.h>
#include <string.h>
#include <iostream>

using namespace bril::lumistore::hdf5;

const size_t ndata = 3564;
const size_t NUMBER_OF_BLOCKS = 10;
const size_t ROWS_PER_BLOCK = 10;
const unsigned int CHUNK_DIMS = 10;
//const unsigned int CHUNK_DIMS = 2024;

//typedef bril::lumistore::hdf5::Row<float,3564> rowdata;

int main(){
  size_t rowsize = sizeof(Row)-sizeof(char)*4+sizeof(float)*ndata; 
  //size_t rowsize = sizeof(Row);
  std::cout<<"sizeofrow "<<sizeof(Row)<<std::endl;
  std::cout<<"rowsize "<<rowsize<<std::endl;
  //std::cout<<"sizeof(float)*ndata "<<sizeof(float)*ndata<<std::endl;
  void* item = malloc(rowsize* ROWS_PER_BLOCK);
  herr_t status;
  const char* filename="test_TableRow.hd5";
  
  hid_t file = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

  if(file<0) std::cout<<"failed to create file"<<std::endl;
  
  status = H5LTset_attribute_string(file,"/","CLASS","GROUP");
  status = H5LTset_attribute_string(file,"/","PYTABLES_FORMAT_VERSION","2.1");
  status = H5LTset_attribute_string(file,"/","TITLE","");
  status = H5LTset_attribute_string(file,"/","VERSION","1.0");
  //std::cout<<sizeof(row)<<std::endl;
  
  hid_t rowType = H5Tcreate(H5T_COMPOUND,rowsize);
 
  size_t offset = 0;
  size_t fieldsize = 0;
  
  //H5Tinsert(rowType, "fillnum",offset, H5T_NATIVE_UINT);
  H5Tinsert(rowType, "fillnum", HOFFSET(Row,fillnum), H5T_NATIVE_UINT);  
  
  //H5Tinsert(rowType,"runnum",offset,H5T_NATIVE_UINT);
  H5Tinsert(rowType,"runnum",HOFFSET(Row,runnum), H5T_NATIVE_UINT);
  fieldsize = sizeof(unsigned int);
  offset += fieldsize; 

  //H5Tinsert(rowType,"lsnum",offset, H5T_NATIVE_UINT);
  H5Tinsert(rowType,"lsnum",HOFFSET(Row,lsnum), H5T_NATIVE_UINT);
  fieldsize = sizeof(unsigned int);
  offset += fieldsize;     

  //H5Tinsert(rowType, "nbnum", HOFFSET(rowdata,nbnum), H5T_NATIVE_UINT);
  H5Tinsert(rowType, "nbnum", HOFFSET(Row,nbnum), H5T_NATIVE_UINT);
  fieldsize = sizeof(unsigned int);
  offset += fieldsize; 

  //H5Tinsert(rowType, "timestampsec", offset, H5T_NATIVE_UINT);
  H5Tinsert(rowType, "timestampsec", HOFFSET(Row,timestampsec), H5T_NATIVE_UINT);
  fieldsize = sizeof(unsigned int);
  offset += fieldsize; 

  //H5Tinsert(rowType, "timestampmsec", offset, H5T_NATIVE_UINT);
  H5Tinsert(rowType, "timestampmsec",HOFFSET(Row,timestampmsec), H5T_NATIVE_UINT);
  fieldsize = sizeof(unsigned int);
  offset += fieldsize; 

  //H5Tinsert(rowType,"publishnnb",offset, H5T_NATIVE_UCHAR);
  H5Tinsert(rowType,"publishnnb",HOFFSET(Row,publishnnb), H5T_NATIVE_UCHAR);
  fieldsize = sizeof(unsigned char);
  offset += fieldsize;   

  //H5Tinsert(rowType,"datasourceid",offset, H5T_NATIVE_UCHAR);
  H5Tinsert(rowType,"datasourceid",HOFFSET(Row,datasourceid), H5T_NATIVE_UCHAR);
  fieldsize = sizeof(unsigned char);
  offset += fieldsize;  

  //H5Tinsert(rowType, "algoid", offset, H5T_NATIVE_UCHAR);
  H5Tinsert(rowType, "algoid",HOFFSET(Row,algoid), H5T_NATIVE_UCHAR);
  fieldsize = sizeof(unsigned char);
  offset += fieldsize; 

  //H5Tinsert(rowType, "channelid", offset, H5T_NATIVE_UINT);
  H5Tinsert(rowType, "channelid", HOFFSET(Row,channelid), H5T_NATIVE_UCHAR);
  fieldsize = sizeof(unsigned char);
  offset += fieldsize; 

  hsize_t arraydims[1]; arraydims[0]=ndata;
  hid_t dataid = H5Tarray_create(H5T_NATIVE_FLOAT,1,arraydims);
  H5Tinsert(rowType, "data", HOFFSET(Row, data), dataid);
  //H5Tinsert(rowType, "data", offset, dataid);
  H5Tclose(dataid); 

  hid_t tabid = H5TBOmake_table(file,"datatable",rowType,0,ROWS_PER_BLOCK,0,0);
  if(tabid <0 ) std::cout<<"failed to create datatable"<<std::endl;

  status = H5LTset_attribute_string(tabid,"/datatable","CLASS","TABLE");
  status = H5LTset_attribute_string(tabid,"/datatable","TITLE","datatable");
  status = H5LTset_attribute_string(tabid,"/datatable","VERSION","2.7");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_0_NAME","fillnum");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_1_NAME","runnum");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_2_NAME","lsnum");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_3_NAME","nbnum");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_4_NAME","timestampsec");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_5_NAME","timestampmsec");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_6_NAME","publishnnb");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_7_NAME","datasourceid");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_8_NAME","algoid");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_9_NAME","channelid");
  status = H5LTset_attribute_string(tabid,"/datatable","FIELD_10_NAME","data");
  status = H5LTset_attribute_string(tabid,"/datatable","UNIT","1/pb");
  status = H5LTset_attribute_string(tabid,"/datatable","DESC","testtest");
  Row* current=0;
  memset(item, 0xff, sizeof(item));
  static unsigned int nbnum = 0;
  char* buffc = (char*)item;
  float payload[ndata];
  for(size_t l = 0; l<ndata; ++l){ 
    payload[l]=0.5*float(l);
  }
  for(size_t b = 0; b<NUMBER_OF_BLOCKS; ++b){
    for (size_t i = 0; i < ROWS_PER_BLOCK; ++i){
      nbnum = i+1;
      current = (Row*)(buffc+rowsize*i);
      current->fillnum = 2322;
      current->runnum = (unsigned int)(nbnum/10) + 1;
      current->lsnum = 1;
      current->nbnum = nbnum;
      current->timestampsec = 1;
      current->timestampmsec = 5;
      current->algoid = 1;
      current->channelid = 12;     
      memcpy(current->data,payload,sizeof(payload));      
    }
    status = H5TBOappend_records(tabid,rowType,ROWS_PER_BLOCK,b*ROWS_PER_BLOCK,item);
  }
  H5Dclose(tabid);
  H5Fclose(file);
  free (item);  
}
//hdf5 12.1 s ,    1463509624 bytes, 115MB/s
//hdf5 2m14.300 s, 14635050360 bytes, 104MB/s
//As of 2010, ATA/SATA 7200 RPM desktop  internal data transfer rate up to 1030 Mbit/s, 128MB/s
