
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_DiscoveryListener_h
#define _jxta_DiscoveryListener_h

#include "jxta/AdvertisementList.h"

namespace jxta {

//! This abstract class represents a discovery listener.
//##ModelId=416A98F1014E
class DiscoveryListener 
{
	public:
	//! Callback for a discovery event. 
	//! The Discovery Service calls back this function whenever an asynchronous discovery event happens.
	//! @param AdvertisementList::Reference advList the list of newly discovered advertisements.
	//##ModelId=416A98F20073
    virtual void discoveryEvent(jxta::AdvertisementList::Reference advList) = 0;
};


}

#endif
