#ifndef _tcds_hwutilstca_Definitions_h_
#define _tcds_hwutilstca_Definitions_h_

#include <stdint.h>

namespace tcds {
  namespace definitions {

    // The different cyclic generator operating modes.
    enum CYCLIC_GEN_MODE {
      CYCLIC_GEN_OFF,
      CYCLIC_GEN_MISCONFIGURED,
      CYCLIC_GEN_L1A,
      CYCLIC_GEN_BGO,
      CYCLIC_GEN_SEQUENCE
    };

    // The different cyclic generator synchronisation modes.
    enum CYCLIC_GEN_SYNC_MODE {
      CYCLIC_GEN_SYNC_FREE = 0,
      CYCLIC_GEN_SYNC_BGO = 1
    };

    // The different cyclic generator behaviours during system pause.
    enum CYCLIC_GEN_PAUSE_BEHAVIOUR {
      CYCLIC_GEN_PAUSE_FREEZE = 0,
      CYCLIC_GEN_PAUSE_CANCEL = 1
    };

  } // namespace definitions
} // namespace tcds

#endif // _tcds_hwutilstca_Definitions_h_
