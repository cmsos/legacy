#include "hdf5.h"
#include <stdio.h>
#include <stdlib.h>

#define DATASET         "DS1"
#define DIM0            4

int main(){
  hid_t file, datatype, memtype, space, dset;
  herr_t status;
  hsize_t dims[1] = {4};
  const char* wdata[4] =  {"Parting", "is such", "sweet", "sorrow."};
  char** rdata;
  int ndims, i;

   file = H5Fcreate ("h5vlstring.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
   datatype =  H5Tcopy (H5T_C_S1);
   H5Tset_size (datatype, H5T_VARIABLE);
   memtype = H5Tcopy (H5T_C_S1);
   H5Tset_size (memtype, H5T_VARIABLE);
   /*
    * Create dataspace.  Setting maximum size to NULL sets the maximum
    * size to be the current size.
    */
   space = H5Screate_simple (1, dims, NULL);
   dset = H5Dcreate (file, DATASET, datatype, space, H5P_DEFAULT, H5P_DEFAULT,H5P_DEFAULT);
   H5Dwrite (dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, wdata);
   status = H5Dclose (dset);
   status = H5Sclose (space);
   status = H5Tclose (datatype);
   status = H5Tclose (memtype);
   status = H5Fclose (file);
   // reading back
   file = H5Fopen ("h5vlstring.h5", H5F_ACC_RDONLY, H5P_DEFAULT);
   dset = H5Dopen (file, DATASET, H5P_DEFAULT);
   datatype = H5Dget_type (dset);
   space = H5Dget_space (dset);
   ndims = H5Sget_simple_extent_dims (space, dims, NULL);
   rdata = (char **) malloc (dims[0] * sizeof (char *));
   memtype = H5Tcopy (H5T_C_S1);
   H5Tset_size (memtype, H5T_VARIABLE);
   H5Dread (dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, rdata);
   for (i=0; i<dims[0]; i++)
     printf ("%s[%d]: %s\n", DATASET, i, rdata[i]);
   /*
    * Close and release resources.  Note that H5Dvlen_reclaim works
    * for variable-length strings as well as variable-length arrays.
    * Also note that we must still free the array of pointers stored
    * in rdata, as H5Tvlen_reclaim only frees the data these point to.
    */
   status = H5Dvlen_reclaim (memtype, space, H5P_DEFAULT, rdata);
   free (rdata);
   status = H5Dclose (dset);
   status = H5Sclose (space);
   status = H5Tclose (datatype);
   status = H5Tclose (memtype);
   status = H5Fclose (file);
   
   return 0;
}
