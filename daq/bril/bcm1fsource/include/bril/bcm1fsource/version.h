// $Id$
#ifndef _bril_bcm1fsource_version_h_
#define _bril_bcm1fsource_version_h_
#include "config/PackageInfo.h"
#define BRILBCM1FSOURCE_VERSION_MAJOR 1
#define BRILBCM1FSOURCE_VERSION_MINOR 5
#define BRILBCM1FSOURCE_VERSION_PATCH 1
#define BRILBCM1FSOURCE_PREVIOUS_VERSIONS

// Template macros
//
#define BRILBCM1FSOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRILBCM1FSOURCE_VERSION_MAJOR,BRILBCM1FSOURCE_VERSION_MINOR,BRILBCM1FSOURCE_VERSION_PATCH)
#ifndef BRILBCM1FSOURCE_PREVIOUS_VERSIONS
#define BRILBCM1FSOURCE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBCM1FSOURCE_VERSION_MAJOR,BRILBCM1FSOURCE_VERSION_MINOR,BRILBCM1FSOURCE_VERSION_PATCH)
#else
#define BRILBCM1FSOURCE_FULL_VERSION_LIST  BRILBCM1FSOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBCM1FSOURCE_VERSION_MAJOR,BRILBCM1FSOURCE_VERSION_MINOR,BRILBCM1FSOURCE_VERSION_PATCH)
#endif
namespace brilbcm1fsource{
  const std::string package = "brilbcm1fsource";
  const std::string versions = BRILBCM1FSOURCE_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bcm1fsource";
  const std::string description = "bril bcm1f readout";
  const std::string authors = "J. Leonard";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
