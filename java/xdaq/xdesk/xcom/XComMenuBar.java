package xdaq.xdesk.xcom;

import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import org.w3c.dom.*;
import java.awt.*;
import java.io.*;
import java.applet.*;
//import javax.help.*;

import org.apache.log4j.*;
import java.awt.event.*;

import xdaq.xdesk.tools.*;
// extends Standard menu bar from tools  with Desklet specific menubar. 
//In combination with Desklet specific listener
public class XComMenuBar extends xdaq.xdesk.tools.MenuBar 
{
	static Logger logger = Logger.getLogger (XComMenuBar.class);

	XComMenuBarListener l_;
	
	public XComMenuBar( XComMenuBarListener l)
	{
		super ("XCom", (MenuBarListener) l);
		
		l_ = l;
		JMenu menu;
		JMenuItem item;
		menu = new JMenu ("File");
		
		// About action
		item = new JMenuItem (  new AbstractAction("Close Window") {
						public void actionPerformed (ActionEvent e) {
							l_.closeAction();
						}
					}
				      );
		menu.add (item);
		this.add(menu);
		
	}		
	
	
}
