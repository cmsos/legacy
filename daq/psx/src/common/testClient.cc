#include <sys/timex.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <vector>

// XOAP
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/SOAPConnection.h"
#include "xoap/URLEndpoint.h"
#include "xoap/exception/Exception.h"


using namespace xoap;
using namespace std;

int main(int argc, char *argv[]) {

  string s;
  string msgStr;
  string mode;


  XMLPlatformUtils::Initialize();  // Xerces init

  SOAPMessage *msg;
  SOAPMessage *rep;

  SOAPConnection *conn;


  // customize: PSI URL - hostname of the PVSS machine, PSI standard port
  URLEndpoint *dest = new URLEndpoint("http://lxcmddemo.cern.ch:1964/psx");



  msg = new SOAPMessage();

  // customize: PSI message

  /*
  // handwritten - in a string
  msgStr = "<soap-env:Envelope soap-env:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:soap-env=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap-env:Header/><soap-env:Body><dpSet originator=\"cms\"><dpValue dpName=\"System1:ExampleDP_SumAlert.:original..value\">whatever possible</dpValue><dpValue dpName=\"System1:stateDCS.:original..value\">whatever possible</dpValue></dpSet></soap-env:Body></soap-env:Envelope>";
  msg->parseFrom(msgStr.c_str(),msgStr.length());
  // end of handwritten message
 */


  // constructed using XOAP
  SOAPEnvelope env = msg->getEnvelope();
  SOAPBody body = env.getBody();
  SOAPName cmdName = env.createName("dpSet","","");
  SOAPBodyElement bodyElem = body.addBodyElement(cmdName);

  SOAPName cmdAttrName = env.createName("originator","","");
  bodyElem.addAttribute(cmdAttrName, "cms");

  SOAPName parName =  env.createName("dpValue","","");
  SOAPElement elem = bodyElem.addChildElement(parName);
  elem.addTextNode("whatever possible");
  SOAPName elemAttrName = env.createName("dpName","","");
  elem.addAttribute(elemAttrName, "System1:ExampleDP_SumAlert.:original..value");

  msg->writeTo(msgStr);
  // end of XOAP message



  printf("message:\n%s\n\n",msgStr.c_str());



  rep = new SOAPMessage();
  conn = new SOAPConnection();
  
  //--- send msg ---
  *rep = conn->call(*msg, *dest);
  
  
  //--- print the response ---
  string repStr;
  rep->writeTo(repStr);
  printf("reply:\n%s\n\n",repStr.c_str());

/*
// decode the reply using XOAP
   SOAPEnvelope env_rep = rep->getEnvelope();
   SOAPBody body_rep = env_rep.getBody();
   vector<SOAPElement> elemList_rep = body_rep.getChildElements();
// ...
 */

    delete conn;
    delete msg;
    delete rep;


  return 0;
}

