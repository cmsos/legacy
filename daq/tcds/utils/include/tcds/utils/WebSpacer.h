#ifndef _tcds_utils_WebSpacer_h
#define _tcds_utils_WebSpacer_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {

    class Monitor;

    class WebSpacer : public WebObject
    {

    public:
      WebSpacer(std::string const& name,
                std::string const& description,
                Monitor const& monitor,
                std::string const& itemSetName,
                std::string const& tabName,
                size_t const colSpan);

      virtual std::string getHTMLString() const;
      virtual std::string getJSONString() const;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_WebSpacer_h
