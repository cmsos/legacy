// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_QueuePair_h
#define _ibvla_QueuePair_h

#include <infiniband/verbs.h>

#include "ibvla/exception/Exception.h"
#include "ibvla/exception/InternalError.h"
#include "ibvla/exception/QueueFull.h"

#include "ibvla/ProtectionDomain.h"

namespace ibvla
{
	class QueuePair
	{
		public:

			ProtectionDomain pd_;
			ibv_qp *qp_;

			QueuePair ()
			{
				qp_ = 0;
			}

			QueuePair (ProtectionDomain &pd, ibv_qp * qp);

			~QueuePair ();

			void modify (ibv_qp_attr &attr, int attr_mask) ;

			void query (int attr_mask, ibv_qp_attr &attr, ibv_qp_init_attr &init_attr) ;

			void postSend (ibv_send_wr &wr) ;

			void postRecv (ibv_recv_wr &wr) ;

			uint32_t getNum ();
			enum ibv_qp_state getState ();
			void* getContext();

			//std::string toString ();

			friend std::ostream& operator<< ( std::ostream &ss, QueuePair & qp );

			// can only be used with UD QP's
			//void attach_mcast (ibv_gid *gid, uint16_t lid)
			//{
			//	int result;
			//	if ((result = ibv_attach_mcast(qp_, gid, lid))) throw error("ibv_attach_mcast", result);
			//}

			//void detach_mcast (ibv_gid *gid, uint16_t lid)
			//{
			//	int result;
			//	if ((result = ibv_detach_mcast(qp_, gid, lid))) throw error("ibv_detach_mcast", result);
			//}
	};

	// This is for if you only have the ibv_qp struct
	//std::string toString (ibv_qp *qp);
}

#endif
