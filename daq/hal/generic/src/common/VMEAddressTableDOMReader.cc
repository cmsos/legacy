#include "hal/VMEAddressTableDOMReader.hh"

HAL::VMEAddressTableDOMReader::VMEAddressTableDOMReader( XERCES_CPP_NAMESPACE::DOMDocument* doc ) {

  HAL::DOMToVMEMapConverter::convert( doc, itemPointerList );
}
