#ifndef _tcds_pm_WebTableSequenceRAM_h
#define _tcds_pm_WebTableSequenceRAM_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace pm {

    class WebTableSequenceRAM : public tcds::utils::WebObject
    {

    public:
      WebTableSequenceRAM(std::string const& name,
                          std::string const& description,
                          tcds::utils::Monitor const& monitor,
                          std::string const& itemSetName,
                          std::string const& tabName,
                          size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_WebTableSequenceRAM_h
