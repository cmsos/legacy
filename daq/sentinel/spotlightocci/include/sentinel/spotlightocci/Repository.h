// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_spotlightocci_Repository_h_
#define _sentinel_spotlightocci_Repository_h_

#include <string>
#include <map>
//#include <ttclasses/TTInclude.h>
#include <sqlite3.h>

#include "xgi/Output.h"
#include "sentinel/spotlightocci/exception/FailedToStore.h"
#include "sentinel/spotlightocci/exception/FailedToRead.h"
#include "sentinel/spotlightocci/exception/FailedToOpen.h"
#include "sentinel/spotlightocci/exception/FailedToClose.h"
#include "sentinel/spotlightocci/exception/NotFound.h"

#include "sentinel/spotlightocci/DataBase.h"

#include "xdaq/Object.h"


namespace sentinel 
{
	namespace spotlightocci 
	{
		class Repository: public xdaq::Object
		{
			public:

			Repository(xdaq::Application * owner, const std::string& username, const std::string& password, const std::string& tnsname, unsigned int scatterReadNum)
				;
			~Repository();
			
			/*sentinel::spotlightocci::DataBase * getArchive(toolbox::TimeVal & date)
				;;*/

					
			/*! Store an exception 
				This function is thread safe (read/write lock)
			*/
			void store (xcept::Exception& ex)
				;
			
			void rearm (const std::string & exception,const std::string & source )
				;
			
			void revoke (xcept::Exception& ex)
				;

			
			/*! Retrieve a single exception by uuid in the format it has been stored.
				The name of the format is returned in \param format.
				This function is thread safe (read lock)
			*/
			void retrieve (const std::string& uuid, const std::string& datetime, const std::string& format, xgi::Output* out) 
				;

			/*! Return a list of stored exceptions in the \param ex set that match the search query 
				This function is thread safe (read lock)
			*/
			void catalog (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal &  end, const std::string& format)
				;
			
			void events (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal &  end, const std::string& format)
				;

			void query ( xgi::Output* out, const std::string& query )
				;
				
			void lastStoredEvents (xgi::Output* out, toolbox::TimeVal & since, const std::string& format)
				;
				
			/*! Retrieve the number of exceptions stored */
			std::string rerieveExceptionCount();
			
			/*! Retrieve the time at which the last exception has been stored */
			toolbox::TimeVal& lastExceptionTime();
			
			/* Retrieve all "spotlightocci*.db" files in the path */
			/*
			std::vector<std::string> getFiles()
				;
			*/
			
			sentinel::spotlightocci::DataBase * getCurrentDataBase();
	
			/*void archive (toolbox::TimeInterval & window)
					;*/

			std::list<toolbox::Properties> getOpenArchivesInfo();

			/*! Returns the average time it takes to archive data from current.db */
			/*double getAverageArchiveTime();*/
			
			// Perform cache load from Oracle to TimesTen
			//
			void reset();
	
			private:

			toolbox::BSem repositoryLock_; // used for db management operations
			toolbox::TimeVal lastExceptionTime_;
			sentinel::spotlightocci::DataBase * currentDataBase_;
			std::map<std::string,sentinel::spotlightocci::DataBase *> archiveDataBases_;
		};
	}
}
#endif
