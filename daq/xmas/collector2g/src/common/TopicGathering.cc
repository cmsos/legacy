// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/collector2g/TopicGathering.h"

#include "xcept/tools.h"
#include "toolbox/stl.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/exception/InvalidListener.h"
#include "toolbox/task/exception/NotActive.h"
#include "toolbox/task/exception/InvalidSubmission.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/Event.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "xdata/Boolean.h"


xmas::collector2g::TopicGathering::TopicGathering(xdaq::Application * owner, const std::string & brokerURL, const std::string & networkName, toolbox::TimeInterval interval)
	
	:  xdaq::Object(owner),	topicMutex_(toolbox::BSem::FULL), scanPeriod_(interval), brokerURL_(brokerURL), topic_(0)
{
	if (brokerURL_ == "")
	{
		XCEPT_RAISE(xmas::collector2g::exception::Exception, "missing broker url");
	}
	// Initialize message counters
	brokerCounter_     = 0;
	brokerLostCounter_ = 0;

	try
	{
		brokerMessengerCache_ = new b2in::utils::MessengerCache( this->getOwnerApplication()->getApplicationContext(), networkName, this);
	}
	catch (b2in::utils::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create xmas messenger cache on network '";
		msg << this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("network") << "'";
		XCEPT_RETHROW(xmas::collector2g::exception::Exception, msg.str(), e );
	}
}

xmas::collector2g::TopicGathering::~TopicGathering()
	
{
}


void xmas::collector2g::TopicGathering::timeExpired(toolbox::task::TimerEvent& e)
	
{
 	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "broker-staging time elapsed");

	std::string name = e.getTimerTask()->name;

	if ( name == "broker-staging" )
	{
		this->requesting();
	}
}

void xmas::collector2g::TopicGathering::update (xdata::Properties & plist)
	
{
	std::string action = plist.getProperty("urn:xmasbroker2g:action");
	std::string model = plist.getProperty("urn:xmasbroker2g:model");
	if ( action == "allocate" && model == "collector")
	{
		// Extract all flashlist topics and set the topic name in the topic map ( empty topic means no publish )
		//
		topicMutex_.take();

		if ( topic_ != 0 ) 
		{
			delete topic_;
			topic_ = 0;
		}

		topic_ = new xmas::collector2g::Topic();
		topic_->flashlistName_ = plist.getProperty ("urn:xmasbroker2g:flashlist");	
		topic_->subscribeTopicName_ = plist.getProperty ("urn:xmasbroker2g:subscribe");
		topic_->publishTopicName_ = plist.getProperty ("urn:xmasbroker2g:publish");
		topic_->hashKey_ = toolbox::parseTokenSet(plist.getProperty ("urn:xmasbroker2g:hashkey"),",");

		try
		{
			xdata::Boolean b;
			b.fromString(plist.getProperty ("urn:xmasbroker2g:clear"));
			topic_->autoClear_ = (bool)b;
		}
		catch(xdata::exception::Exception & e)
		{
			delete topic_;
			topic_ = 0;
	        	LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "failed to parse autoclear from broker, " << e.what());
			return;
		}


		try
		{
			topic_->stagingPeriod_.fromString(plist.getProperty ("urn:xmasbroker2g:flush"));
		}
		catch(toolbox::exception::Exception & e)
		{
			 delete topic_;
                        topic_ = 0;
	        	LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "failed to parse staging time from broker, " << e.what());
			return;

		}
		topicMutex_.give();
        }
}


// Finite State Machine
void xmas::collector2g::TopicGathering::requesting () 
{
 	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "requesting topic to broker  for collection");

	// if it has a messenger already this is a re-dial
	if ( ! brokerMessengerCache_->hasMessenger(brokerURL_) )
	{
		try
		{
			brokerMessengerCache_->createMessenger(brokerURL_); // trigger  caching of messenger for the remote endpoint
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), "Failed to create messenger on to broker" << brokerURL_ << ", " << e.what());
			return;
		}
	}

	xdata::Properties plist;
		
	plist.setProperty ("urn:b2in-protocol:service", "xmasbroker2g");
	plist.setProperty ("urn:b2in-protocol-tcp:connection", "close");

	plist.setProperty ("urn:xmasbroker2g:action", "query");
	plist.setProperty ("urn:xmasbroker2g:model", "collector");
	plist.setProperty ("urn:xmasbroker2g:url", brokerMessengerCache_->getLocalAddress()->toString());
	plist.setProperty ("urn:xmasbroker2g:originator", this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service"));

		
	// Tell the broker all the flashlists we can report
	//
	topicMutex_.take();
	if ( topic_ != 0 )
	{
                 plist.setProperty ("urn:xmasbroker2g:subscribe", topic_->getSubscribeTopic());
	}
	else
	{
		 // Those fields will be filled by the broker
		plist.setProperty ("urn:xmasbroker2g:subscribe", "");

	}
	topicMutex_.give();

	/* Tell the broker, when my collection will expire, such that if I do not re-query within the
	time, the broker can attribute the flashlist to another, free collector.
	 */
	toolbox::TimeInterval expires;
	expires = scanPeriod_;
	expires.tv_sec = expires.tv_sec * 2; // Now double the time interval

	plist.setProperty ("urn:xmasbroker2g:expires", expires.toString("xs:duration"));

	try
	{
		brokerCounter_++;
		brokerMessengerCache_->send(brokerURL_,0,plist);
	}
	catch (b2in::nub::exception::InternalError & e)
	{
		std::stringstream ss;
		ss << "failed to send query to broker on url " << brokerURL_;
		brokerLostCounter_++;
		XCEPT_RETHROW(xmas::collector2g::exception::Exception, ss.str(), e);
	}
	catch ( b2in::nub::exception::QueueFull & e )
	{
		std::stringstream ss;
		ss << "failed to send query to broker on url " << brokerURL_;
		brokerLostCounter_++;
		XCEPT_RETHROW(xmas::collector2g::exception::Exception, ss.str(), e);
	}
	catch ( b2in::nub::exception::OverThreshold & e)
	{
		std::stringstream ss;
		ss << "failed to send query to broker on url " << brokerURL_;
		brokerLostCounter_++;
		XCEPT_RETHROW(xmas::collector2g::exception::Exception, ss.str(), e);
	}
}

std::string xmas::collector2g::TopicGathering::getBrokerURL()
	
{
	return brokerURL_;
}

xdata::UnsignedInteger64T xmas::collector2g::TopicGathering::getBrokerCounter()
	
{
	return (brokerCounter_-brokerLostCounter_);
}

xdata::UnsignedInteger64T xmas::collector2g::TopicGathering::getBrokerLostCounter()
	
{
	return brokerLostCounter_;
}

std::string xmas::collector2g::TopicGathering::getStateName()
	
{
	if(this->isSynchronized())
	{
		return "assigned";
	}
	else
	{
		return "requesting";
	}
}

bool xmas::collector2g::TopicGathering::isSynchronized()
	
{
	bool hasTopic = false;
	topicMutex_.take();
	if ( topic_ != 0 )
	{
		hasTopic = true;
	}
	topicMutex_.give();
	return hasTopic;
}

xmas::collector2g::Topic  xmas::collector2g::TopicGathering::getAssignedTopic()
	
{

	xmas::collector2g::Topic  t;

	topicMutex_.take();
	if ( topic_ != 0 )
	{
		t = *topic_;
		topicMutex_.give();
	}
	else
	{
		topicMutex_.give();
		XCEPT_RAISE(xmas::collector2g::exception::Exception, "itopic not yet assigned");
	}

	return t;
}

void xmas::collector2g::TopicGathering::asynchronousExceptionNotification(xcept::Exception& ex)
	
{
        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Sending a notification failed, " << ex.what());
        brokerLostCounter_++;
}

