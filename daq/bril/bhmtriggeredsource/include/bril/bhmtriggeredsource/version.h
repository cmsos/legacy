// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_bhmtriggeredsource_version_h_
#define _bril_bhmtriggeredsource_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILBHMTRIGGEREDSOURCE_VERSION_MAJOR 1
#define BRILBHMTRIGGEREDSOURCE_VERSION_MINOR 2
#define BRILBHMTRIGGEREDSOURCE_VERSION_PATCH 5
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILBHMTRIGGEREDSOURCE_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILBHMTRIGGEREDSOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRILBHMTRIGGEREDSOURCE_VERSION_MAJOR,BRILBHMTRIGGEREDSOURCE_VERSION_MINOR,BRILBHMTRIGGEREDSOURCE_VERSION_PATCH)
#ifndef BRILBHMTRIGGEREDSOURCE_PREVIOUS_VERSIONS
#define BRILBHMTRIGGEREDSOURCE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBHMTRIGGEREDSOURCE_VERSION_MAJOR,BRILBHMTRIGGEREDSOURCE_VERSION_MINOR,BRILBHMTRIGGEREDSOURCE_VERSION_PATCH)
#else
#define BRILBHMTRIGGEREDSOURCE_FULL_VERSION_LIST  BRILBHMTRIGGEREDSOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBHMTRIGGEREDSOURCE_VERSION_MAJOR,BRILBHMTRIGGEREDSOURCE_VERSION_MINOR,BRILBHMTRIGGEREDSOURCE_VERSION_PATCH)
#endif

namespace brilbhmtriggeredsource
{
	const std::string package = "brilbhmtriggeredsource";
	const std::string versions = BRILBHMTRIGGEREDSOURCE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ bhmsource";
	const std::string description = "bril bhm triggered readout";
	const std::string authors = "N.Tosi J.Mans";
	const std::string link = "";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
