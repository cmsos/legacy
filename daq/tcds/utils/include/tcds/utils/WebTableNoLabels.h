#ifndef _tcds_utils_WebTableNoLabels_h
#define _tcds_utils_WebTableNoLabels_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {

    class Monitor;

    class WebTableNoLabels : public WebObject
    {

    public:
      WebTableNoLabels(std::string const& name,
                       std::string const& description,
                       Monitor const& monitor,
                       std::string const& itemSetName,
                       std::string const& tabName,
                       size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_WebTableNoLabels_h
