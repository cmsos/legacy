// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tstore/SQLView.h"
#include "xoap/domutils.h"
#include "xercesc/dom/DOMNode.hpp"	
#include "tstore/client/LoadDOM.h"

bool tstore::SQLView::subviewExists(const std::string &name) {
	std::string upperName=toolbox::toupper(name);
	return (subViews_.count(upperName)!=0);
}

tstore::SQLViewInternal &tstore::SQLView::getSubviewWithName(const std::string &name)  {
	std::string upperName=toolbox::toupper(name);
	if (subviewExists(upperName)) {
		return subViews_[upperName];
	} else {
		XCEPT_RAISE(tstore::exception::InvalidView, "No such table: "+upperName);
	}
}

//creates a subview with the given name, or returns it if one with that name already exists
tstore::SQLViewInternal &tstore::SQLView::createSubviewWithName(const std::string &name) {
	std::string upperName=toolbox::toupper(name);
	//std::cout << "adding subview: " << upperName << std::endl;
	if (!subviewExists(upperName)) {
		SQLViewInternal newView(upperName);
		subViews_[upperName]=newView;
	}
	return subViews_[upperName];
}

//removes the subview from memory
void tstore::SQLView::removeSubviewWithName(const std::string &name) {
	std::string upperName(toolbox::toupper(name));
	if (subviewExists(upperName)) {
		subViews_.erase(upperName);
	}
}

//removes the subview from the DOM tree
//this has a lot of code from the constructor
void tstore::SQLView::removeSubviewWithName(DOMNode *viewNode,const std::string &name)  {
	std::vector<DOMNode *> sqlviewNodes(tstoreclient::nodesWithPrefix(viewNode,namespaceURI()));
	bool removed=false;
	for (std::vector<DOMNode *>::iterator child=sqlviewNodes.begin();child!=sqlviewNodes.end();++child) {
		try {
			std::string sectionType=xoap::XMLCh2String((*child)->getLocalName());
			std::string subViewName=xoap::getNodeAttribute(*child, "name");
			if (toolbox::toupper(name)==toolbox::toupper(subViewName)) {
				//std::cout << "removing " << sectionType << " section from " << subViewName << std::endl;
				removed=true;
				viewNode->removeChild(*child);
			}
		} catch (xcept::Exception &e) {
			XCEPT_RETHROW(tstore::exception::Exception,"Could not remove subview with name "+name+" from the configuration file.",e);
		}
	}

	if (!removed) {
		XCEPT_RAISE(tstore::exception::Exception,"There is no subview with name "+name+" in the configuration file.");
	}
}

tstore::SQLView::SQLView(std::string configurationPath,std::string name)  : View(configurationPath,name) {
	DOMNode *config=getConfiguration(configurationPath);
	if (config) {
		std::vector<DOMNode *> sqlviewNodes(tstoreclient::nodesWithPrefix(config,namespaceURI()));
		for (std::vector<DOMNode *>::iterator child=sqlviewNodes.begin();child!=sqlviewNodes.end();++child) {
			std::string name=xoap::XMLCh2String((*child)->getLocalName());
			std::string nodePrefix=xoap::XMLCh2String((*child)->getNamespaceURI());
			try {
				std::string subViewName=xoap::getNodeAttribute(*child, "name");	
				if (!subViewName.empty()) {
					tstore::SQLViewInternal &subView=createSubviewWithName(subViewName);
					subView.addConfiguration(name,*child);
				} else {
					XCEPT_RAISE(tstore::exception::Exception,"No name specified for "+name);
				}
			} catch (xcept::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,e.what(),e);
			}
		}
		//[ 1775420 ] TStore addView message should allow adding empty SQLViews
		//only throw an error if there is configuration with an error, not if there is no configuration
		/*if (!configurationFound) {
			XCEPT_RAISE(tstore::exception::Exception,"No query, insert, or update configuration was found. Check that your configuration is using the correct namespace ("+namespaceURI()+")");
		}*/
	}
	else XCEPT_RAISE(tstore::exception::Exception,"Could not find configuration.");
}

//As we are no longer using View::insert etc, these can... functions are only used to display on the interface
//of the TStore application. So they should return whether any of the internal views can perform the operation
bool tstore::SQLView::canQuery() const {
	for (SubviewList::const_iterator subviewIterator=subViews_.begin();subviewIterator!=subViews_.end();++subviewIterator) {
		if ((*subviewIterator).second.canQuery()) return true;
	}
	return false;
}

bool tstore::SQLView::canUpdate() const {
	for (SubviewList::const_iterator subviewIterator=subViews_.begin();subviewIterator!=subViews_.end();++subviewIterator) {
		if ((*subviewIterator).second.canUpdate()) return true;
	}
	return false;
}

bool tstore::SQLView::canInsert() const {
	for (SubviewList::const_iterator subviewIterator=subViews_.begin();subviewIterator!=subViews_.end();++subviewIterator) {
		if ((*subviewIterator).second.canInsert()) return true;
	}
	return false;
}

bool tstore::SQLView::canCreate() const {
	for (SubviewList::const_iterator subviewIterator=subViews_.begin();subviewIterator!=subViews_.end();++subviewIterator) {
		if ((*subviewIterator).second.canCreate()) return true;
	}
	return false;
}

bool tstore::SQLView::canRemove() const {
	for (SubviewList::const_iterator subviewIterator=subViews_.begin();subviewIterator!=subViews_.end();++subviewIterator) {
		if ((*subviewIterator).second.canRemove()) return true;
	}
	return false;
}

bool tstore::SQLView::canDestroy() const {
	for (SubviewList::const_iterator subviewIterator=subViews_.begin();subviewIterator!=subViews_.end();++subviewIterator) {
		if ((*subviewIterator).second.canDestroy()) return true;
	}
	return false;
}

void tstore::SQLView::throwOperationNotSupported(const std::string &operation,const std::string &tableName) 
	 
{
	XCEPT_RAISE(tstore::exception::InvalidView, "Table '"+tableName+"' of view '"+name()+"' does not support "+operation);
}

tstore::SQLViewInternal &tstore::SQLView::getSubview()  {
	if (!currentSubview_) XCEPT_RAISE(tstore::exception::Exception, "No table specified for view '"+name()+"'");
	currentSubview_->resetParameters();
	std::map<const std::string,std::string>::iterator parameterIterator;
	for (parameterIterator=parameters_.begin();parameterIterator!=parameters_.end();++parameterIterator) {
		currentSubview_->setParameter((*parameterIterator).first,(*parameterIterator).second);
	}
	return *currentSubview_;
}

void tstore::SQLView::create(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	if (!currentSubview.canCreate()) throwOperationNotSupported("create",currentSubview.name());
	return currentSubview.create(API);
}

void tstore::SQLView::query(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	if (!currentSubview.canQuery()) throwOperationNotSupported("query",currentSubview.name());
	return currentSubview.query(API);
}

void tstore::SQLView::update(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	if (!currentSubview.canUpdate()) throwOperationNotSupported("update",currentSubview.name());
	return currentSubview.update(API);
}

void tstore::SQLView::insert(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	if (!currentSubview.canInsert()) throwOperationNotSupported("insert",currentSubview.name());
	return currentSubview.insert(API);
}

void tstore::SQLView::definition(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	if (!currentSubview.canInsert()) throwOperationNotSupported("definition",currentSubview.name());
	return currentSubview.definition(API);
}

void tstore::SQLView::clear(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	if (!currentSubview.canRemove()) throwOperationNotSupported("clear",currentSubview.name());
	currentSubview.clear(API);
}

void tstore::SQLView::remove(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	if (!currentSubview.canRemove()) throwOperationNotSupported("delete",currentSubview.name());
	currentSubview.remove(API);
	//XCEPT_RAISE(tstore::exception::InvalidViewException, "View '"+name()+"' does not support remove.");
}

/*
Add the configuration for the tables... just allow querying, updating and inserting all columns in all columns.
I am not sure what to do here when there is more than one table. The options are: 
- Create a new subview for each table, which would mean naming the subviews after tables, or adding view parameters to the table tags 
	themselves to say what the names should be.
- Create one subview with all the tables, using the name parameter as the subview name. This would be easy for update and insert, but
	we would not know the join conditions. In any case this is supposed to be a way to store existing xdata tables and it is unlikely that they
	would send two tables that they intended to query/update/insert as one table.
	
For now I will go with the first option, using the table names as subview names, and if anyone complains I will think of adding view parameters
to the table tags.
*/
void tstore::SQLView::addTables(TStoreAPI *API)  {
	std::vector<std::string> subviewsAdded;
	try {
		if (configure_) { //only add configuration if they send the parameter sql:configure="yes", since they might just want to create tables for an existing config.
			std::map<const std::string,std::string> tableNames;
			API->getTableNames(tableNames);
			std::map<const std::string,std::string>::const_iterator tableName;
			for (tableName=tableNames.begin();tableName!=tableNames.end();++tableName) {
				if (!subviewExists((*tableName).first)) {
					tstore::SQLViewInternal &subView=createSubviewWithName((*tableName).first);
					subviewsAdded.push_back((*tableName).first);
					xdata::Table definition;
					DOMNode *node=API->getConfiguration();
					API->getTable(definition,(*tableName).first);
					std::vector<std::string> columns=definition.getColumns();
					subView.addConfiguration(node,columns,(*tableName).first);
				} else {
					XCEPT_RAISE(tstore::exception::Exception,"A subview named "+(*tableName).first+" has already been configured.");
				}
			}
		}
		tstore::View::addTables(API); //add tables to the database. Do this last, so that we don't have to drop them if something else fails.
	} catch (tstore::exception::Exception) {
		//remove the configuration we added in memory
		for (std::vector<std::string>::iterator addedSubview=subviewsAdded.begin();addedSubview!=subviewsAdded.end();++addedSubview) {
			removeSubviewWithName(*addedSubview);
		}
		throw;
	}
}

/*It would not make sense to remove configuration for certain tables within a multiple-table subview, because then the subview might not work any more.
There are two possibilities for removing configuration:
	- Remove the configuration for subviews which only use tables which are in the list of tables to be deleted. Throw an exception if there are subviews
		which use some of the tables to be deleted plus also other tables (since they won't work if one of the tables is deleted)
	- Only remove the configuration for subviews configured using addTable, that is, subviews with only one table, where the table name is the
		same as the subview name. In this case, removeTables works the same way as destroy, but several can be removed in the one message. Throw
		an exception if the table is in any other kind of subview.
Removing configuration should only happen if the configure parameter is yes, because I don't want to 'break' the whole addTable/removeTable
mechanism for people who want this functionality to work differently.*/

namespace tstore 
{
inline bool lt_nocase(const std::string &c1, const std::string &c2) { return toolbox::toupper(c1) < toolbox::toupper(c2); }
}

void tstore::SQLView::removeTables(TStoreAPI *API)  {
	if (configure_) {
		std::vector<std::string> tableNames;
		API->getTableNames(tableNames);
		DOMNode *node=API->getConfiguration();
		for (tstore::SubviewList::iterator subview=subViews_.begin();subview!=subViews_.end();) {
			std::vector<std::string> allTables((*subview).second.allTables());
			std::vector<std::string> intersection;
			std::sort(tableNames.begin(), tableNames.end(),tstore::lt_nocase);
			std::sort(allTables.begin(), allTables.end(),tstore::lt_nocase);  
			std::set_intersection(allTables.begin(),allTables.end(),tableNames.begin(),tableNames.end(),std::back_inserter(intersection),tstore::lt_nocase);
			if (!intersection.empty()) {
				if (intersection.size()<allTables.size()) { //if this view depends on some of the tables we are deleting, but also depends on other tables
					XCEPT_RAISE(tstore::exception::Exception,"The subview "+(*subview).second.name()+" depends partially on the tables you are deleting.");
				}
				else if (intersection.size()==allTables.size()) { //if this view depends only on the tables we are deleting
					std::string name=(*subview).second.name();
					removeSubviewWithName(node,name);
					++subview;
					/*Map has the important property that inserting a new element into a map does not invalidate iterators that point to existing elements. Erasing an element from a map also does not invalidate any iterators, except, of course, for iterators that actually point to the element that is being erased.*/
					removeSubviewWithName(name);
				}
			}
		}
	}
	tstore::View::removeTables(API); //remove tables from the database
}

std::vector<std::string> tstore::SQLView::tablesToDestroy() {
	return getSubview().allTables();
}

//this one should remove an entire subview and all associated tables.
void tstore::SQLView::destroy(TStoreAPI *API)  {
	SQLViewInternal &currentSubview=getSubview();
	currentSubview.destroy(API); //current subview should remove from the database all the tables it uses
	if (configure_) {
		DOMNode *node=API->getConfiguration();
		removeSubviewWithName(node,currentSubview.name());
		removeSubviewWithName(currentSubview.name());
	}
}

//tstore::exception::InvalidView is really the wrong name for the exception thrown here, this should eventually be fixed in View.h
void tstore::SQLView::setParameter(const std::string &parameterName,const std::string &value)  {
	if (parameterName=="name") currentSubview_=&getSubviewWithName(value);
	else if (parameterName=="configure") {
		if (value=="yes") configure_=true;
		else if (value=="no") configure_=false;
		else {
			XCEPT_RAISE(tstore::exception::InvalidView,"The 'configure' parameter can only be 'yes' or 'no' (default is no.)");
		}
	}
	else parameters_[parameterName]=value; //since the subview name might not be the first parameter we get, store these and set them in the subview later
}

void tstore::SQLView::resetParameters() {
	parameters_.clear();
	currentSubview_=NULL;
	configure_=false;
}

std::string tstore::SQLView::namespaceURI() {
	return "urn:tstore-view-SQL";
}
