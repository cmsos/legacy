#ifndef _tcdsrf2ttc_version_h_
#define _tcdsrf2ttc_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSRF2TTC_VERSION_MAJOR 3
#define TCDSRF2TTC_VERSION_MINOR 13
#define TCDSRF2TTC_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSRF2TTC_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSRF2TTC_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSRF2TTC_VERSION_CODE PACKAGE_VERSION_CODE(TCDSRF2TTC_VERSION_MAJOR,TCDSRF2TTC_VERSION_MINOR,TCDSRF2TTC_VERSION_PATCH)
#ifndef TCDSRF2TTC_PREVIOUS_VERSIONS
#define TCDSRF2TTC_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSRF2TTC_VERSION_MAJOR,TCDSRF2TTC_VERSION_MINOR,TCDSRF2TTC_VERSION_PATCH)
#else
#define TCDSRF2TTC_FULL_VERSION_LIST TCDSRF2TTC_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSRF2TTC_VERSION_MAJOR,TCDSRF2TTC_VERSION_MINOR,TCDSRF2TTC_VERSION_PATCH)
#endif

namespace tcdsrf2ttc
{
  const std::string package = "tcdsrf2ttc";
  const std::string versions = TCDSRF2TTC_FULL_VERSION_LIST;
  const std::string description = "CMS software for the RF2TTC.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the RF2TTC.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
