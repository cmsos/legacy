package xdaq.xdesk.jfinder;

import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

public class FinderModel
implements TreeModel {
	EventListenerList listenerList_ = new EventListenerList();
	FinderTreeNode root_;
	
	public FinderModel(FinderTreeNode root) {
		root_ = root;
	}
	
	public void addTreeModelListener(TreeModelListener l) {
		listenerList_.add(TreeModelListener.class, l);
	}
	
	public Object getChild(Object parent, int index) {
		return ( (FinderTreeNode) parent).getChildAt(index);
	}
	
	public int getChildCount(Object parent) {
		return ( (FinderTreeNode) parent).getChildCount();
	}
	
	public int getIndexOfChild(Object parent, Object child) {
		if ( (parent == null) || (child == null)) {
			return -1;
		}
		
		return ( (FinderTreeNode) parent).getIndex( (FinderTreeNode) child);
	}
	
	public Object getRoot() {
		return root_;
	}
	
	public boolean isLeaf(Object node) {
		return ( (FinderTreeNode) node).isLeaf();
	}
	
	public void removeTreeModelListener(TreeModelListener l) {
		listenerList_.remove(TreeModelListener.class, l);
	}
	
	public void valueForPathChanged(TreePath path, Object newValue) {
		// Not sure, if a new value can actually be provided, since the
		// FinderTreeNode reflects the file system and changes are done
		// in the file system itself. The tree is then rescanned.
		
		FinderTreeNode aNode = (FinderTreeNode) path.getLastPathComponent();
		
		aNode.invalidate();
		
		// ???? Assign newValue to what ????
		
		this.nodeChanged(aNode);
	}
	
	// ---------------------------------------------------------------------------------------------
	// Extended interface to notify a change of a node
	
	public void nodeChanged(FinderTreeNode node) {
		if (listenerList_ != null && node != null) {
			/*
			Original code would trigger nodesChanged on all
			 nodes along the path up to the root.
			 Here we don't do that, since JFinder always displays
			 a single level of a directory tree.
			 FinderTreeNode parent = (FinderTreeNode) node.getParent();
			 if (parent != null)
			 {
				 int anIndex = parent.getIndex(node);
				 if (anIndex != -1)
				 {
					 // Get the index of the changed node
					 int[] cIndeces = new int[1];
					 cIndeces[0] = anIndex;
					 this.nodesChanged (parent, cIndeces);
				 }
			 }
			 else if (node == getRoot())
			 {
				 this.nodesChanged (node, null);
			 }
			 */
			
			this.fireTreeNodesChanged(this, this.getPathToRoot(node), null, null);
		}
	}
	
	/**
		* Invoke this method after change of the children identified by
		* childIndices are to be represented in the tree.
		*/
	public void nodesChanged(FinderTreeNode node, int[] childIndices) {
		if (node != null) {
			if (childIndices != null) {
				int cCount = childIndices.length;
				if (cCount > 0) {
					Object[] cChildren = new Object[cCount];
					for (int i = 0; i < cCount; i++) {
						cChildren[i] = node.getChildAt(childIndices[i]);
					}
					this.fireTreeNodesChanged(this, this.getPathToRoot(node),
											  childIndices, cChildren);
				}
			}
			else if (node == this.getRoot()) {
				this.fireTreeNodesChanged(this, this.getPathToRoot(node), null, null);
			}
		}
	}
	
	protected void fireTreeNodesChanged(Object source, Object[] path,
										int[] childIndices, Object[] children) {
		Object[] listeners = listenerList_.getListenerList();
		TreeModelEvent e = null;
		for (int i = listeners.length - 2; i >= 0; i -= 2) {
			if (listeners[i] == TreeModelListener.class) {
				if (e == null) {
					e = new TreeModelEvent(source, path, childIndices, children);
				}
				( (TreeModelListener) listeners[i + 1]).treeNodesChanged(e);
			}
		}
	}
	
	// Return a list of all nodes between this one and the root node
	public TreeNode[] getPathToRoot(TreeNode node) {
		return this.getPathToRoot(node, 0);
	}
	
	public TreeNode[] getPathToRoot(TreeNode node, int depth) {
		TreeNode[] retNodes;
		
		if (node == null) {
			if (depth == 0) {
				return null;
			}
			else {
				retNodes = new TreeNode[depth];
			}
		}
		else {
			depth++;
			if (node == root_) {
				retNodes = new TreeNode[depth];
			}
			else {
				retNodes = getPathToRoot(node.getParent(), depth);
			}
			retNodes[retNodes.length - depth] = node;
		}
		return retNodes;
	}
	
}
