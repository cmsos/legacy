###############################################################################
## Various constants used in the TCDS tests in the lab.
###############################################################################

# The addresses of the boards/applications to connect to.
LPM = "lpm1"
ICIS = ["ici11", "ici12", "ici13", "ici14", "ici15", "ici16", "ici17", "ici18"]
APVES = ["apve11", "apve12", "apve13", "apve14"]
PIS = ["pi11", "pi12", "pi13", "pi14", "pi15", "pi16", "pi17", "pi18"]

###############################################################################
