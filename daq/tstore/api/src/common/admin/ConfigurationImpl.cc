// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/api/admin/ConfigurationImpl.h"
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "xoap/memSearch.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPSerializer.h"
#include "xoap/ErrorHandler.h"
#include "xoap/exception/Exception.h"
#include "xoap/domutils.h"
#include "toolbox/string.h"
#include <xercesc/dom/DOM.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/SAXException.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOMException.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>
#include <xercesc/dom/DOMError.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include "tstore/api/NS.h"

tstore::api::admin::ConfigurationImpl::~ConfigurationImpl()
{
	document_->release();
}

tstore::api::admin::ConfigurationImpl::ConfigurationImpl() 	
{
	//
	// create empty configuration
	//
	try
	{
		DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation ( xoap::XStr ("Core") );

		document_ = impl->createDocument(
		       xoap::XStr(tstore::api::admin::NamespaceUri),		// root element namespace URI.
		       xoap::XStr(tstore::api::admin::NamespacePrefix + ":configuration"),    		// root element name
		       0);    						// document type object (DTD).

		DOMElement * element = document_->getDocumentElement();
		
		element->setAttribute ( xoap::XStr("xmlns:" + tstore::api::admin::NamespacePrefix), xoap::XStr(tstore::api::admin::NamespaceUri) );
		element->setAttribute ( xoap::XStr("xmlns:" + tstore::api::sql::NamespacePrefix), xoap::XStr(tstore::api::sql::NamespaceUri) );
		element->setAttribute ( xoap::XStr("xmlns:" + tstore::api::nested::NamespacePrefix), xoap::XStr(tstore::api::nested::NamespaceUri) );
	}
	catch ( const XMLException& xmle )
	{
		XCEPT_RAISE (tstore::api::admin::exception::Exception, xoap::XMLCh2String(xmle.getMessage()));
	}
	catch ( const DOMException& de )
	{
		// Xerces 2.6: de.getMessage()
		XCEPT_RAISE (tstore::api::admin::exception::Exception, xoap::XMLCh2String(de.msg));
	}	
}

DOMDocument * tstore::api::admin::ConfigurationImpl::getDocument() 	
{
	return document_;
}

/*
tstore::api::admin::ConfigurationImpl::ConfigurationImpl(DOMDocument * document) 
{
	// import full document
	document_ = (DOMDocument*)document->cloneNode(true);
}
*/

bool tstore::api::admin::ConfigurationImpl::hasTableDefinition(const std::string & name ) 
{
	DOMNodeList * tables = document_->getElementsByTagNameNS( xoap::XStr(tstore::api::admin::NamespaceUri), xoap::XStr("table"));
	for (XMLSize_t i = 0; i < tables->getLength(); i++ )
	{
		DOMNode * item = tables->item(i);
		std::string nameAttributeValue = toolbox::toupper(xoap::getNodeAttribute(item, "name"));
		if ( nameAttributeValue == toolbox::toupper(name) )
		{
			return true;
		}	
	}
	return false;
}



void tstore::api::admin::ConfigurationImpl::removeTableDefinition(const std::string & name ) 
{
	DOMNodeList * tables = document_->getElementsByTagNameNS( xoap::XStr(tstore::api::admin::NamespaceUri), xoap::XStr("table"));
	for (XMLSize_t i = 0; i < tables->getLength(); i++ )
	{
		DOMNode * item = tables->item(i);
		std::string nameAttributeValue = toolbox::toupper(xoap::getNodeAttribute(item, "name"));
		if ( nameAttributeValue == toolbox::toupper(name) )
		{
			DOMNode * father = item->getParentNode();
			father->removeChild (item);
			item->release();
			
			// then all associated tables
			DOMNodeList * foreignkeys = document_->getElementsByTagNameNS( xoap::XStr(tstore::api::admin::NamespaceUri), xoap::XStr("foreignkey"));
			for (XMLSize_t i = 0; i < foreignkeys->getLength(); i++ )
			{
				DOMNode * item = foreignkeys->item(i);
				if ( toolbox::toupper(xoap::getNodeAttribute(item, "references")) == toolbox::toupper(name) )
				{
					DOMNode * tableNode = item->getParentNode();
					DOMNode * viewNode = tableNode->getParentNode();
					viewNode->removeChild (tableNode);
					tableNode->release();
				}
			}
			
			return;	
		}
	}
	
	XCEPT_RAISE (tstore::api::admin::exception::Exception, "Cannot find table definition for name '" + name + "'");
}

void tstore::api::admin::ConfigurationImpl::removeInsertStatement (const std::string & name) 
	
{
	DOMNodeList * tables = document_->getElementsByTagNameNS( xoap::XStr(tstore::api::sql::NamespaceUri), xoap::XStr("insert"));
	for (XMLSize_t i = 0; i < tables->getLength(); i++ )
	{
		DOMNode * item = tables->item(i);
		std::string nameAttributeValue = toolbox::toupper(xoap::getNodeAttribute(item, "name"));
		if ( nameAttributeValue == toolbox::toupper(name) )
		{
			DOMNode * father = item->getParentNode();
			father->removeChild (item);
			item->release();
			return;
		}	
	}
	XCEPT_RAISE (tstore::api::admin::exception::Exception, "Cannot find insert statement for name '" + name + "'");
}

void tstore::api::admin::ConfigurationImpl::addInsertStatement
(
	const std::string & name,
	const std::string & tableName, 
	std::vector<std::string> & columns
) 

{
	std::string insertNodeName = tstore::api::sql::NamespacePrefix + ":insert";
	DOMElement * insertNode = document_->createElementNS( xoap::XStr(tstore::api::sql::NamespaceUri), xoap::XStr(insertNodeName) );
	insertNode->setAttribute ( xoap::XStr("name"), xoap::XStr(name) );

	DOMElement * element = document_->getDocumentElement();
	DOMNode * insertElement = element->appendChild(insertNode);
	
	std::string tableNodeName = tstore::api::sql::NamespacePrefix + ":table";
	DOMElement * tableNode = document_->createElementNS( xoap::XStr(tstore::api::sql::NamespaceUri), xoap::XStr(tableNodeName) );
	tableNode->setAttribute ( xoap::XStr("name"), xoap::XStr(tableName) );
	
	DOMNode* tableElement = insertElement->appendChild(tableNode);
		
	std::string tableColumnName = tstore::api::sql::NamespacePrefix + ":column";
	for (std::vector<std::string>::iterator column = columns.begin(); column != columns.end(); ++column) 
	{
		DOMElement *columnNode=document_->createElementNS(xoap::XStr(tstore::api::sql::NamespaceUri),xoap::XStr(tableColumnName));
		columnNode->setAttribute(xoap::XStr("name"),xoap::XStr(*column));
		tableElement->appendChild(columnNode);			
	}													
}

xdata::Table::Reference tstore::api::admin::ConfigurationImpl::getTableDefinition(const std::string & name) 
{

	#warning "TBD xdata::Table::Reference getTable"
	
	//
	// Scan document and rebuild table structure
	//
	xdata::Table::Reference table;
	return table;
}


tstore::api::admin::ConfigurationImpl & tstore::api::admin::ConfigurationImpl::import( tstore::api::admin::ConfigurationImpl & configuration)
{
	// import full document
	document_->release();
	document_ = (DOMDocument*)(configuration.getDocument()->cloneNode(true));
        return *this;
}

void tstore::api::admin::ConfigurationImpl::toSOAP(xoap::SOAPElement& element, xoap::SOAPEnvelope& envelope)
{
	// Add all namespace declarations from the document element into the element
	DOMElement* docElement = document_->getDocumentElement();
	DOMNamedNodeMap* attributes = docElement->getAttributes();
	for (XMLSize_t a = 0; a < attributes->getLength(); ++a)
	{
		DOMNode* node = attributes->item(a);
		if (xoap::XMLCh2String(node->getPrefix()) == "xmlns")
		{
                	try
                	{
                        	((DOMElement*) element.getDOM())->setAttributeNS(
					node->getNamespaceURI(),
					node->getNodeName(),
					node->getTextContent()
				);
                	} 
			catch (DOMException& de)
                	{
				// THIS FUNCTION MUST NOT FAIL!!!
				std::cout << "FATAL:" << xoap::XMLCh2String(de.msg) << std::endl;
                        	// Xerces 2.6: de.getMessage()
                        	// XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));				
                	}
		}
	}

	DOMNodeList* nodeList  = document_->getDocumentElement()->getChildNodes();
	for (XMLSize_t itemIndex = 0; itemIndex < nodeList->getLength(); itemIndex++) 
	{
		// Before adding the element, check if the namespace definition exists
		// already and if not, add it.
		
		element.addChildElement(nodeList->item(itemIndex));
	}			
}
