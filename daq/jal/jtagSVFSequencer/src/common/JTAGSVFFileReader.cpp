#include "jal/jtagSVFSequencer/JTAGSVFFileReader.h"

#include <string>
#include <ctype.h>

using namespace std;

jal::JTAGSVFFileReader::JTAGSVFFileReader (string filename) 
 : _filestream ( filename.c_str() ) {
}

jal::JTAGSVFFileReader::~JTAGSVFFileReader () {
  _filestream.close();
}

bool jal::JTAGSVFFileReader::nextCommand (vector <string> & args) 
  throw (jal::FileOpenException) {

  if (! _filestream ) 
    XCEPT_RAISE(jal::FileOpenException, "cannot open SVF file.");

  args.clear();

  // the max. allowed length in an SVF file is 256 chars according to the standard
  int MAXLENGTH = 300;
  char buffer[MAXLENGTH];

  
  // check if there is already a semicolon
  bool semicolon_found = _commandline.find(";") != string::npos;

  // read until there is a ";"
  while ( ! semicolon_found ) {
    
    if ( _filestream.eof() ) 
      return false;

    _filestream.getline(buffer, MAXLENGTH);

    string line(buffer);

    // comments start after "//" or "!" for the rest of the line
    if (line.find("//") != string::npos) line.erase( line.find("//") );
    if (line.find("!") != string::npos) line.erase( line.find("!") );


    if (line.find(";") != string::npos) semicolon_found = true;

    _commandline += line;
    
    
  }

  // cout << "(1)read up to ;" << endl;

  // get part of command line before the ";"
  string command( _commandline, 0, _commandline.find(";") );

  // and remove the command from the command line 
  _commandline.erase ( 0, _commandline.find(";")+1 );

  // cout << "(2)copied ;" << endl;

  //  cout << "have read lines until finding a ';'. command is \"" << command << "\"" << endl;

  // cout << "remaing commandline is  \"" << _commandline << "\"" << endl;

  replace(command, "\r", "", false);  // kill carrige return characters
  replace(command, "\t", " ", false); // convert tabs to spaces
  replace(command, "  ", " ", true);  // skip multiple spaces

  command.erase(0, command.find_first_not_of(" ") );  // skip leading spaces
  command.erase( command.find_last_not_of(" ")+1 );   // skip trailing spaces

  // cout << "trimmed command is \"" << command << "\"" << endl;
  
  // cout << "(3)trimmed ;" << endl;

  // skip whitespace inside bracketed expressions
  size_t pos=0; int bracketlevel = 0;
  do {
    pos = command.find_first_of ("() ", pos);
    if (pos == string::npos) break;
    if ( command[pos] == '(' ) bracketlevel++;
    if ( command[pos] == ')' ) bracketlevel--;
    if ( command[pos] == ' ' && bracketlevel>0) command.erase (pos--,1);
  } while (++pos < command.length());
  
  // convert to upper case
  for(size_t i = 0; i < command.length(); i++)
    command[i] = toupper(command[i]);

  // tokenize the command string
  size_t i=0, j=0;
  while( (j=command.find(" ",i))!=string::npos) {
    args.push_back( command.substr(i,j-i));
    i = j+1;
  }
  args.push_back( command.substr(i) );
  
  //cout << "(4)tokenized" << endl;

  return true;
}


//--------------------------------------------------------------------------------
// Replace substring in string

int jal::JTAGSVFFileReader::replace(string & input, const string& gone, const string& it, bool multiple) {
  int n=0;
  size_t i = input.find(gone,0);
  while(i!=string::npos) {
      n++;
      input.replace(i,gone.size(),it);
      i = input.find(gone,i+(multiple ? 0 : it.size()));
  }
  return n;
}





