// $Id: Server.cc,v 1.13 2008/07/18 15:26:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include "benchmark/justio/Server.h"
#include "benchmark/justio/i2oStreamIOMsg.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "toolbox/PerformanceMeter.h"
#include "xdaq/NamespaceURI.h"

#include "toolbox/BSem.h"

#include "toolbox/rlist.h"
#include "toolbox/string.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"

#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPName.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "i2o/Method.h"
#include "xgi/framework/Method.h"
#include "i2o/utils/AddressMap.h"
#include "xgi/Method.h"

Server::Server (xdaq::ApplicationStub* c) throw (xdaq::exception::Exception)
	: xdaq::Application(c), xgi::framework::UIManager(this)
{

	i2o::bind(this, &Server::token, I2O_TOKEN_CODE, XDAQ_ORGANIZATION_ID);


	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

void Server::actionPerformed( xdata::Event& event)
{
	counter_ = 0;


	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{


	}
}

// Callback function invoked for the I2O token message
// that is received from the Clients
//
void Server::token(toolbox::mem::Reference * ref)  throw (i2o::exception::Exception)
{


	counter_ = counter_ + 1;

	LOG4CPLUS_DEBUG (getApplicationLogger(), toolbox::toString("received message number %d", (xdata::UnsignedLongT) counter_));

	//PI2O_TOKEN_MESSAGE_FRAME frame = (PI2O_TOKEN_MESSAGE_FRAME) ref->getDataLocation();   
	//size_t size = frame->PvtMessageFrame.StdMessageFrame.MessageSize << 2;
 	//size -= sizeof(I2O_TOKEN_MESSAGE_FRAME); // want to see infiniband max

	ref->release();


}
