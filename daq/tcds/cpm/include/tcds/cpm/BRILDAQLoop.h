#ifndef _tcds_cpm_BRILDAQLoop_h_
#define _tcds_cpm_BRILDAQLoop_h_

#include <deque>
#include <memory>
#include <stdint.h>
#include <string>
#include <sys/types.h>

#include "toolbox/lang/Class.h"

#include "tcds/cpm/BRILDAQPacket.h"
#include "tcds/cpm/BRILDAQSnapshot.h"
#include "tcds/cpm/BRILDAQStorageInfoSpaceHandler.h"
#include "tcds/cpm/LumiNibble.h"
#include "tcds/cpm/LumiSection.h"
/* #include "tcds/cpm/SectionStorageInfoSpaceHandler.h" */
#include "tcds/utils/Lock.h"

namespace toolbox {
  namespace task {
    class WorkLoop;
  }
}

namespace tcds {
  namespace cpm {
    class BRILDAQPublisher;
    class CPMController;
  }
}

namespace tcds {
  namespace cpm {

    class BRILDAQLoop : public toolbox::lang::Class
    {

    public:
      BRILDAQLoop(tcds::cpm::CPMController& cpmController);
      virtual ~BRILDAQLoop();

      // Acquisition loop (start/stop) control methods.
      void start();
      void stop();

      // Reset the book keeping counters (number of nibbles missed,
      // etc.).
      void resetCounters();

      // Clear the 'recent nibbles/sections' history.
      void clearHistory();

      bool isHwConnected() const;

      bool update(toolbox::task::WorkLoop* workLoop);

      BRILDAQSnapshot snapshot() const;

    private:
      static const useconds_t kLoopRelaxTime = 100000;

      // This is the maximum number of lumi sections we'll keep in our
      // history.
      static size_t const kMaxSectionHistoryLength = 100;

      std::string const nibbleDAQAlarmName_;
      std::string const brilDAQAlarmName_;
      tcds::cpm::CPMController& cpmController_;
      std::auto_ptr<toolbox::task::WorkLoop> daqWorkLoopP_;
      std::string workLoopName_;

      uint32_t numNibblesMissed_;
      uint32_t numNibblesReadTwice_;
      uint32_t numNibblesInvalid_;
      uint32_t numNibblesCorrupted_;
      uint32_t numNibblesAlmostCorrupted_;
      std::deque<tcds::cpm::LumiNibble> nibbles_;
      std::deque<tcds::cpm::LumiSection> sections_;

      mutable tcds::utils::Lock lock_;

      // The thingy that takes care of publishing our data to the
      // BRILDAQ eventing system.
      std::auto_ptr<tcds::cpm::BRILDAQPublisher> brildaqPublisherP_;

      uint32_t swVersion_;
      uint32_t fwVersion_;
      uint16_t fedId_;

      // The InfoSpaceHandler with the rates, deadtimes, etc. that
      // should go straight into the database.
      BRILDAQStorageInfoSpaceHandler brildaqStorage_;

      /* SectionStorageInfoSpaceHandler sectionStorage_; */

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQLoop_h_
