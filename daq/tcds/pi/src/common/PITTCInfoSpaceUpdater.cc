#include "tcds/pi/PITTCInfoSpaceUpdater.h"

#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/pi/TCADevicePI.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/LogMacros.h"

tcds::pi::PITTCInfoSpaceUpdater::PITTCInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                       TCADevicePI const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::pi::PITTCInfoSpaceUpdater::~PITTCInfoSpaceUpdater()
{
}

tcds::pi::TCADevicePI const&
tcds::pi::PITTCInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::pi::TCADevicePI const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
