// addon.cc
#include <node.h>
#include "myobject.h"
#include "ApplicationDescriptor.h"
#include "ApplicationContext.h"
#include "PeerTransportAgent.h"
#include "Listener.h"

//namespace demo {

//using v8::Local;
//using v8::Object;

using namespace v8;

//Isolate* isolate = Isolate::GetCurrent();
 //HandleScope scope(isolate);
void getPeerTransportAgent(const FunctionCallbackInfo<Value>& args) {
	nodejs::PeerTransportAgent::NewInstance(args);
}

void getApplicationContext(const FunctionCallbackInfo<Value>& args) {
	nodejs::ApplicationContext::NewInstance(args);
}


Persistent< Function > percy1;
//Local<Function> callbackFunction = Local<Function>::Cast(args[0]);


void setCallback(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Local<Function> cb = Local<Function>::Cast(args[0]);
  percy1.Reset(isolate, cb);

 // const unsigned argc = 1;
 // Local<Value> argv[argc] = { String::NewFromUtf8(isolate, "ciao sono io") };
 // cb->Call(Null(isolate), argc, argv);
}

void runCallback(const FunctionCallbackInfo<Value>& args) {
  Isolate* isolate = args.GetIsolate();
  //Local<Function> cb = Local<Function>::Cast(args[0]);
 // percy = Persistent<Function>::New(cb);
  const unsigned argc = 1;
  Local<Value> argv[argc] = { String::NewFromUtf8(isolate, "ciao sono io") };
  //percy->Call(Null(isolate), argc, argv);
  Local<Function>::New(isolate, percy1)->Call(isolate->GetCurrentContext()->Global(), argc, argv);
}



void InitAll(Local<Object> exports) {
  demo::MyObject::Init(exports);
  nodejs::ApplicationDescriptor::Init(exports);
  nodejs::ApplicationContext::Init(exports);
  nodejs::PeerTransportAgent::Init(exports);
  nodejs::Listener::Init(exports);

  NODE_SET_METHOD(exports, "getPeerTransportAgent", getPeerTransportAgent);
  NODE_SET_METHOD(exports, "getApplicationContext", getApplicationContext);
  NODE_SET_METHOD(exports, "setCallback", setCallback);
  NODE_SET_METHOD(exports, "runCallback", runCallback);





}

NODE_MODULE(NODE_GYP_MODULE_NAME, InitAll)

//}  // namespace demo




