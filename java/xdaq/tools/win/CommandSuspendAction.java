package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class CommandSuspendAction extends AbstractAction
{
	xdaqWin win_;

	public CommandSuspendAction (xdaqWin win)
	{
		super ("Suspend");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {
			win_.executeCommandOnSelection ("Suspend", null, win_.getPaths());
		}
		catch(xdaqWinException ex )
		{
		
		}
	}
}
