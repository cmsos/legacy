#ifndef _tcds_rfdippub_RF2TTCDataInfoSpaceUpdater_h_
#define _tcds_rfdippub_RF2TTCDataInfoSpaceUpdater_h_

#include <string>

#include "toolbox/net/URL.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Table.h"

#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace rfdippub {

    class RF2TTCDataInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      RF2TTCDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp);
      virtual ~RF2TTCDataInfoSpaceUpdater();

    protected:
      /* tcds::utils::XDAQAppBase& getOwnerApplication() const; */

      /* virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler); */

      /* virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item, */
      /*                                  tcds::utils::InfoSpaceHandler* const infoSpaceHandler); */

    private:

      /* // Helper to sort XDAQ applications by host, port number, and */
      /* // then local-id. */
      /* struct compareApplications { */
      /*   bool operator() (xdaq::ApplicationDescriptor* const& lhs, */
      /*                    xdaq::ApplicationDescriptor* const& rhs) const */
      /*   { */
      /*     toolbox::net::URL const lhu = toolbox::net::URL(lhs->getContextDescriptor()->getURL()); */
      /*     toolbox::net::URL const rhu = toolbox::net::URL(rhs->getContextDescriptor()->getURL()); */
      /*     std::string const lhh = lhu.getHost(); */
      /*     std::string const rhh = rhu.getHost(); */
      /*     if (lhh != rhh) */
      /*       { */
      /*         return (lhh < rhh); */
      /*       } */
      /*     else */
      /*       { */
      /*         unsigned int const lhp = lhu.getPort(); */
      /*         unsigned int const rhp = rhu.getPort(); */
      /*         if (lhp != rhp) */
      /*           { */
      /*             return (lhp < rhp); */
      /*           } */
      /*         else */
      /*           { */
      /*             if (lhs->getLocalId() != rhs->getLocalId()) */
      /*               { */
      /*                 return (lhs->getLocalId() < rhs->getLocalId()); */
      /*               } */
      /*             else */
      /*               { */
      /*                 // If nothing else works: sort the raw pointers */
      /*                 // by value. */
      /*                 return (lhs < rhs); */
      /*               } */
      /*           } */
      /*       } */
      /*   } */
      /* }; */

      /* std::string getRF2TTCDataInfo() const; */
      /* xdata::Table getFlashList(std::string const& lasURL, */
      /*                           std::string const& lasServiceName, */
      /*                           std::string const& flashListName) const; */

      /* tcds::utils::XDAQAppBase& xdaqApp_; */

    };

  } // namespace rfdippub
} // namespace tcds

#endif // _tcds_rfdippub_RF2TTCDataInfoSpaceUpdater_h_
