/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/EventingPoolSubscriberModel.h"
#include "xmas/broker2g/CollectorSettings.h"
#include "xmas/broker2g/Utils.h"
#include <sstream>
#include <set>

#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"

#include "toolbox/string.h"
#include "toolbox/stl.h"
#include "toolbox/Runtime.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

XMAS_BROKER2G_MODEL_INSTANTIATOR_IMPL(xmas::broker2g::EventingPoolSubscriberModel);

xmas::broker2g::EventingPoolSubscriberModel::EventingPoolSubscriberModel(xdaq::Application * owner, const std::string& name) :
	xmas::broker2g::Model(owner, name)
{
	requests_ = 0;
	results_ = 0;
}

xmas::broker2g::EventingPoolSubscriberModel::~EventingPoolSubscriberModel()
{
}

void xmas::broker2g::EventingPoolSubscriberModel::loaded() 
{
}


void xmas::broker2g::EventingPoolSubscriberModel::update(xdata::Table::Reference table) 
{
	try
	{
		xdata::Serializable* value;

		value = table->getValueAt(0, "url");
		xdata::String* url = dynamic_cast<xdata::String*>(value);

		LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "Incoming flashlist " << url->toString());
		services_[(std::string)*url] = table;
	}
	catch(xdata::exception::Exception& e)
	{
		XCEPT_RETHROW(xmas::broker2g::exception::ParserException, "Could not parse flashlist", e);
	}
}

xdata::Properties xmas::broker2g::EventingPoolSubscriberModel::query(xdata::Properties& plist)
{
	requests_++;
	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- IN  ---");
		;
		for(std::map<std::string, std::string>::iterator piter = plist.begin() ; piter != plist.end() ; piter++)
		{
			LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Property [" << (*piter).first << "=" << (*piter).second << "]");
		}
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- IN  ---");
	}

	std::list<std::string> eventings;

	std::map<std::string, xdata::Table::Reference>::iterator iter;
	for(iter = services_.begin() ; iter != services_.end() ; iter++)
	{
		xdata::Table::Reference t = (*iter).second;

		try
		{
			xdata::Serializable* value;

			value = t->getValueAt(0, "classname");
			xdata::String* classname = dynamic_cast<xdata::String*>(value);

			value = t->getValueAt(0, "group");
			xdata::String* group = dynamic_cast<xdata::String*>(value);

			value = t->getValueAt(0, "url");
			xdata::String* url = dynamic_cast<xdata::String*>(value);

			LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "Application (" << classname->toString() << "|" << group->toString() << "|" << url->toString() << ")");
			if(classname == NULL || group == NULL || url == NULL)
			{
				continue;
			}

			std::set<std::string> groups = toolbox::parseTokenSet(*group, ",");
			std::set<std::string> subscribegroups = toolbox::parseTokenSet(plist.getProperty("urn:xmasbroker2g:group"), ",");
			if(*classname == "b2in::eventing::Application" && toolbox::stl::intersection(groups, subscribegroups).size() != 0)
			{
				LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(), "Matched (" << url->toString() << ")");
				eventings.push_back(*url);
			}
		}
		catch(xdata::exception::Exception& e)
		{
			// ignore flashlists with missing properties
		}
	}

	xdata::Properties returnplist;
	returnplist.setProperty("urn:xmasbroker2g:subscribeurl",   toolbox::printTokenList(eventings, ","));

	{
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- OUT ---");
		;
		for(std::map<std::string, std::string>::iterator piter = returnplist.begin() ; piter != returnplist.end() ; piter++)
		{
			LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "Property [" << (*piter).first << "=" << (*piter).second << "]");
		}
		LOG4CPLUS_DEBUG( getOwnerApplication()->getApplicationLogger(), "--- OUT ---");
	}

	results_++;
	return returnplist;
}

void xmas::broker2g::EventingPoolSubscriberModel::view(xgi::Output * out )
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	// Incoming Requests
	// 
	*out << cgicc::tr();
	*out << cgicc::th("Requests");
	*out << cgicc::td().set("style","min-width: 100px;");
	*out << requests_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Found Responses
	// 
	*out << cgicc::tr();
	*out << cgicc::th("Found");
	*out << cgicc::td();
	*out << results_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Lost Responses
	// 
	*out << cgicc::tr();
	*out << cgicc::th("Lost");
	*out << cgicc::td();
	*out << (requests_-results_);
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Number of found services
	// 
	*out << cgicc::tr();
	*out << cgicc::th("Found Services");
	*out << cgicc::td();
	*out << services_.size();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 

	*out << cgicc::br() << std::endl; 

	bool tableInit = false;

	for (std::map<std::string, xdata::Table::Reference>::iterator w = services_.begin(); w != services_.end(); w++)
	{
		xdata::Table::Reference tref;
		
		tref = (*w).second;
		
		if ( tref.isNull() )
		{
			LOG4CPLUS_WARN(getOwnerApplication()->getApplicationLogger(), "denormalized empty tables are skipped, a notification" );
			continue;
		}

		std::vector<std::string> columns = tref->getColumns();
		
		if (!tableInit)
		{
			*out << cgicc::table().set("class", "xdaq-table") << std::endl;

			*out << cgicc::thead();
			*out << cgicc::tr();
			// By default it denormalize table
			for (std::vector<std::string>::size_type i = 0; i < columns.size(); i++ )
			{
				std::string localName = columns[i].substr(columns[i].rfind(":")+1);
				*out << cgicc::th(localName).set("title",columns[i]);
			}
			*out << cgicc::tr() << std::endl;
			*out << cgicc::thead();
	
			*out << cgicc::tbody();

			tableInit = true;
		}

		for ( size_t j = 0; j <  tref->getRowCount(); j++ )
		{
			*out << cgicc::tr() << std::endl;
			for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
			{
				xdata::Serializable * s = tref->getValueAt(j, columns[k]);

				if (s->type() == "mime")
				{

					*out << cgicc::td("MIME");
				}
				else if (s->type() == "table")
				{
					*out << cgicc::td("TABLE");
				}
				else
				{						
					*out << cgicc::td(s->toString());
				}
			}
			*out << cgicc::tr();
		}
	}

	if (tableInit)
	{
		*out << cgicc::tbody();
		*out << cgicc::table();
	}
}

void xmas::broker2g::EventingPoolSubscriberModel::cleanup(toolbox::TimeVal timeval)
{
	std::map<std::string, xdata::Table::Reference>::iterator iter = services_.begin();
	while(iter != services_.end())
	{
		xdata::Table::Reference t = (*iter).second;

		try
		{
			xdata::Serializable* value = t->getValueAt(0, "timestamp");
			xdata::TimeVal* timestamp = dynamic_cast<xdata::TimeVal*>(value);
			if((*timestamp) < xdata::TimeVal(timeval))
			{
				std::map<std::string, xdata::Table::Reference>::iterator deliter = iter;
				iter++;
				services_.erase(deliter);
			}
			else
			{
				iter++;
			}
		}
		catch(xdata::exception::Exception& e)
		{
			// ignore flashlists with missing properties
			iter++;
		}
	}
}

