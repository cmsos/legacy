//
//  DaqletContext.java
//  
//
//  Created by Luciano Orsini on Fri Sep 06 2002.
//  Copyright (c) 2002 __CERN__. All rights reserved.
//
package xdaq.tools.daqlet;

import java.applet.*;
import java.util.*;
import java.awt.*;
import java.net.*;
import java.io.*;


public class DaqletContext implements AppletContext{

	protected Framework framework_ = null;
	
	public DaqletContext(Framework framework)
	{
		framework_ = framework;
	
	}

        // Finds and returns the applet in the document represented by this applet context with the given name.
        // When the shared is set to false the name must be concatenated to the instance number.
	public Applet getApplet(String name) 
        {
		return null;
        }

        //Finds all the applets in the document represented by this applet context.
        public Enumeration getApplets() 
        {
        	return null;
        }
	
        // Creates an audio clip.
        public AudioClip getAudioClip(URL url) 
        {
		return null;
        }

        // Returns an Image object that can then be painted on the screen.
        public Image getImage(URL url) 
        {
		return null;
        }

        // Replaces the Web page currently being viewed with the given URL.
	public void showDocument(URL url) 
        {
        }
 
        // Requests that the browser or applet viewer show the Web page indicated by the url argument.
        public void showDocument(URL url, String target) 
        {
        }
        
        // Requests that the argument string be displayed in the "status window".
        public void showStatus(String status) 
        {
        }
	//Returns the stream to which specified key is associated within this applet context.
	public InputStream	getStream(String key) 
	{
		return null;
	}

	//Finds all the keys of the streams in this applet context.
	public Iterator	getStreamKeys()
	{
		return null;
	} 
	
	//Associates the specified stream with the specified key in this applet context.
	public void	setStream(String key, InputStream stream) 
	{
	}
}

