//test.js
const addon = require('./build/Release/alchemy');

var fs = require('fs');
var parser = require('xmldom').DOMParser;
var minimist = require('minimist');
var os = require("os");
var xml2js = require('xml2js');
var httpcore = require('./httpcore');
var util = require('util');


class Network {
	 constructor() {
	    	this._registeredEndpoints = [];
	    }
	 
	 addEndpoint(e) {
		 	this._registeredEndpoints.push(e)
	 }
	 
	 getEndpoints() {
		 return this._registeredEndpoints;
	 }
	 
}

class InstantiatorRegistry {
	
    constructor() {
    	this._registeredTypes = new Map();
    }
    
    add(name, intantiator) {
        if (! this._registeredTypes.has(name) ) {
        	this._registeredTypes.set(name, intantiator);
        } 
        else 
        {
        	console.log("cannot register class");
        }
    }

    create(name, params, descriptor)
    {
    	if (!this._registeredTypes.has(name)) {
    		console.error("cannot instantiate class");
    		return null;
    	}
    	let instantiator = this._registeredTypes.get(name);
    	let instance =  instantiator.create(params,descriptor);
    	return instance;

    }

}

class ApplicationRegistry {
	 	constructor() {
	    	this._registeredApplications = [];
	    }
	    
	    add(a) {
	    	this._registeredApplications.push(a);
	    }
	    
	    getApplications() {
	    	return this._registeredApplications;
	    }
}

//---------------------------------------------------------------------------------------------------------
// main starts here
//---------------------------------------------------------------------------------------------------------

//Bootstrap sequence in nodejs for XDAQ libraries
//process.argv.forEach(function (val, index, array) {
//	  console.log(index + ': ' + val);
//	});


const args = process.argv; 
//console.log(args.slice(2));

// Create ApplicationContext
var context = addon.getApplicationContext();

//Initialize according to command line arguments
//Callback
//function foo(msg) {
//	  console.log(msg);
//	  console.log("Ciao Ragazzi");
//}
var pta = addon.getPeerTransportAgent();
pta.addSenderSupport ("http", httpcore.callback);
//console.log('done adding callback');

context.init(args.slice(1));

// Re-scan profile for xdaqjs applications

var pargv = minimist(args, opts={});

//Emulate XDAQ default profile scan

var filePath = '';

if ( 'e' in pargv )
{ 
	//console.log(pargv);
	filePath = pargv.e;

}
else
{
	var profiles = [];
	profiles.push("/etc/default.profile");
	profiles.push(process.env.HOME + "/etc/default.profile"); 
	profiles.push(process.env.XDAQ_ROOT + "/share/" + process.env.XDAQ_ROOT + "/profile/default." + os.hostname() + ".profile");
	profiles.push(process.env.XDAQ_ROOT + "/share/" + process.env.XDAQ_ROOT + "/profile/default.profile");
	profiles.push(process.env.XDAQ_ROOT + "/etc/default.profile");
	console.log('scanning file path for profile');
	
	for (let item of profiles) {
		  console.log('try to open ' + item);
		  if (fs.existsSync(item)) {
			  console.log('found profile: ' + item);
			  filePath = item;
			  break;
		  }
	}

	

}


//Prepare Instantator registry of all nodejs modules loadeds through profile
var registry = new InstantiatorRegistry();
var network = new Network();
var instances = new ApplicationRegistry();

var hostname = os.hostname();;
var port= '1972';

if ( 'h' in pargv )
{ 
	hostname = pargv.h;

}

if ( 'p' in pargv )
{ 
	port = pargv.p;

}

endpoint =  { '$':  { protocol: 'http', service: 'cgi', hostname: hostname, port: port } }

network.addEndpoint(endpoint); // command line 


try {
	//var parser = new xml2js.Parser({explicitChildren: true});
	
	var parser = new xml2js.Parser({explicitArray : true, explicitChildren: true});

	fs.readFile(filePath, function(err, data) {
		parser.parseString(data,  function (err, result) {


			console.log("File '" + filePath + " was successfully read.");
			//console.log(util.inspect(result, false, null))
			// Assuming xmlDoc is the XML DOM Document
			//profile = xmlToJson(doc);
			////var jsonText = JSON.stringify(profile);
			////console.log(jsonText);
	
			
	
			var includemodules = result['xp:Profile']['$$']['xj:Module'];
			var applications = result['xp:Profile']['$$']['xj:Application'];
			var endpoints = result['xp:Profile']['$$']['xj:Endpoint'];

			
			//console.log("---includemodules ---"); 
			//console.log(includemodules);

			//console.log("---applications ---"); 
			//console.log(applications);

			//--var express = require('express');
			//--var app = express();
			var app = null;

			// Load xdaqjs module into nodejs runtime
			for (let module of includemodules) {

				var Instantiator = require(module);
				var instantiator = new Instantiator();

				console.log('loaded  module' + module + ' with instantiator for ' + instantiator.type() );

				// add to registry
				registry.add(instantiator.type(),instantiator);


			}


			
			// instantiate all applications according profile
			if (typeof applications !== 'undefined'   ) {

				for (let a of applications) {
					var type = a['$']['class'];
					var descriptor = a['$'];
					//console.log('instantiate application ' + type );
					var properties = a['$$']['properties'][0]['$$'];
					var application = registry.create(type, properties,descriptor);
					application.display();
					instances.add(application);
					// add Facade

					if ( application._descriptor != undefined){
						//console.log("Add Facade descriptor");
				        	context.createFacadeApplication	(application._descriptor);
					}
					//console.log("Done facade descriptor");
					//
					// hooks in XGI express for test , need to be generalized
					if ( application._app != undefined){
						console.log("Got app from application" + type);
						app = application._app;
					}
					if ( application._router != undefined){
						console.log("add route for " + type );
						//app.use('/applications', application._router);
						app.use('/', application._router);
					}
				}
			}

			// hardcoded
			//app.listen(3000, function() {
		//		  console.log('Example app listening on port 3000!');
		//	});

			
			if (typeof endpoints !== 'undefined' ) {
				for (let e of endpoints) {
					network.addEndpoint(e);
				}
			}
			
			// end processing profile
			
			// config peer transports
			var eps = network.getEndpoints();
			var as = instances.getApplications();

			if ( (typeof as !== 'undefined' ) && ( typeof eps !== 'undefined' ) ) {
				for (let e of eps) {
					for (let a of as) {
						if ( typeof a.service === 'function') {
							if ( a.service() == e['$']['service'] ) {
								console.log('going to configure ' + e['$']['service'] +  ' peer transport for ' + e['$']['hostname'] + ' ' + e['$']['port'] ); 
								a.config(e['$']['hostname'],e['$']['port']);

							}
						}
					}
				}
			}



	});
	});


} catch (ex) {
	console.log("Unable to read file '" + filePath + "'.");
    console.log(ex);
}


