#ifndef _tcds_hwutilstca_HwStatusInfoSpaceHandlerTCA_h_
#define _tcds_hwutilstca_HwStatusInfoSpaceHandlerTCA_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilstca {

    // BUG BUG BUG
    // This should maybe be called HwStatusInfoSpaceHandlerTCABase(?).
    // BUG BUG BUG end
    class HwStatusInfoSpaceHandlerTCA : public tcds::utils::InfoSpaceHandler
    {

    public:
      virtual ~HwStatusInfoSpaceHandlerTCA();

    protected:
      HwStatusInfoSpaceHandlerTCA(xdaq::Application& xdaqApp,
                                  std::string const& name,
                                  tcds::utils::InfoSpaceUpdater* updater);

      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_HwStatusInfoSpaceHandlerTCA_h_
