#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xqilla/xqilla-dom3.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/MessageFactory.h"

#include "xoap/domutils.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/Method.h"
#include "xoap/SOAPConstants.h"

using namespace xoap;

void t()
{
	XQillaPlatformUtils::initialize();
	
        //
        // Create SOAP message
	//
	xoap::MessageReference msg = xoap::MessageFactory::getInstance(xoap::SOAPConstants::SOAP_1_1_PROTOCOL)->createMessage();	

	
	xoap::MessageFactory * soapFactoryImpl = msg->getImplementationFactory();

	SOAPPart soap = msg->getSOAPPart();
	SOAPEnvelope envelope = soap.getEnvelope();
	SOAPBody body = envelope.getBody();

 	xoap::SOAPBody b = msg->getSOAPPart().getEnvelope().getBody();
        xoap::SOAPFault f = b.addFault();
	if (soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
	{
	      f.setFaultCode ("Server");
	      f.setFaultString ("this is an error");
	}
	else
	{
	       xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
	       f.setFaultCode (faultCodeQName);
	       f.addFaultReasonText("this is an error",std::locale("en_US"));
	}

	xoap::SOAPElement detail = f.addDetail();
	
        //SOAPName name = envelope.createName("message");
	//SOAPBodyElement bodyElement = body.addBodyElement(name);
	//
	//SOAPName tag = envelope.createName("Tag", "PR", "http://example.com/PR-NS");
	//SOAPElement tagElement = bodyElement.addChildElement(tag);
	//tagElement.addTextNode("This is a SOAP message");

//	SOAPName createdName1 = bodyElement.getElementName();
//	std::cout << "Qualified name: " << createdName1.getQualifiedName() << std::endl;
//	std::cout << "Node name: " << createdName1.getLocalName() << std::endl;
//	std::cout << "Prefix: " << createdName1.getPrefix() << std::endl;
//	std::cout << "URI: " << createdName1.getURI() << std::endl;
	
//	SOAPName createdName2 = tagElement.getElementName();
//	std::cout << "Qualified name: " << createdName2.getQualifiedName() << std::endl;
//	std::cout << "Node name: " << createdName2.getLocalName() << std::endl;
//	std::cout << "Prefix: " << createdName2.getPrefix() << std::endl;
//	std::cout << "URI: " << createdName2.getURI() << std::endl;
//
	msg->writeTo(std::cout);
}

int main (int argc, char** argv)
{
	t();
	return 0;
}

