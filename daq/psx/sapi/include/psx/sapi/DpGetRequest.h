// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpGetRequest_h_
#define _psx_sapi_DpGetRequest_h_

#include <string>
#include <set>

#include "psx/sapi/Request.h"
     
namespace psx {
	namespace sapi {
	
		class ApplicationService;
		
		class DpGetRequest : public psx::sapi::Request
		{
      			public:
			
			DpGetRequest( psx::sapi::ApplicationService * as);
			
			std::set<std::string>  & getDpNames();
			
			void addDpName(std::string & name);

						
			private:
			
			std::set<std::string> dpnames_;
		
		};
	}
}

#endif 

