<?php
require 'vendor/autoload.php';
function getTimestreamStatus($context, $lid) {
	// print ("going to retrieve " . $context . "/" . "urn:xdaq-application:service=hyperdaq/parameterQuery?lid=" . PHP_EOL . "\n") ;
	$response = \Httpful\Request::get ( $context . "/" . "urn:xdaq-application:service=hyperdaq/parameterQuery?lid=" . $lid )->parseWith ( function ($body) {
		return $body;
	} )->send ();
	
	$parser = xml_parser_create ();
	xml_parse_into_struct ( $parser, $response, $vals, $index );
	xml_parser_free ( $parser );
	if (isset ( $index ["ENABLE"] [0] )) {
		$elem = $index ["ENABLE"] [0];
		$value = $vals [$elem] ["value"];
		return (trim ( $value ) == "true");
	} else {
		throw new Exception ( "Status does not exist!" );
	}
}

?>
