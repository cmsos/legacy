package xdaq.xdesk.tools;

import java.util.*;
import javax.infobus.*;
import java.beans.*;

import org.apache.log4j.*;

public class Advertiser 
{
	Logger logger = Logger.getLogger (Advertiser.class);

	InfoBusMemberSupport infobusSupport_;	
	String itemName_;
	HashMap producers_;
	
	class Producer implements InfoBusDataProducer
	{
		AdvertisementManager adv_; // object to be published
		String itemName_;

		public Producer (ImmediateAccess object, String itemName)
		{
			itemName_ = itemName;
			adv_ = new AdvertisementManager (this, object);
		}
		
		// A consumer requests to listen to xcommand. Pass it the change manager for subscription
		public void dataItemRequested (InfoBusItemRequestedEvent e) 
		{
			// only if this request belongs to me!!!
			if (e.getDataItemName().equals (itemName_))
			{
				e.setDataItem(adv_);
			}		
		}

		public void propertyChange (PropertyChangeEvent e)
  		{
  			logger.error ("proprtyChange function not yet implemented");
  		}
	};
	
	class AdvertisementManager extends DataItemChangeManagerSupport implements ImmediateAccess , 
									    DataItem
	{
		ImmediateAccess object_;
		InfoBusEventListener source_;

		public AdvertisementManager(InfoBusDataProducer producer, ImmediateAccess object)
		{
			super (producer);
			object_ = object;
			source_ = producer;
		}
		
	 	// Immediate Access interfaces
		public String getPresentationString (java.util.Locale locale)
		{
			return object_.getValueAsString();
		}

		// Returns the TreeModel of the configuration tree
		public Object getValueAsObject()
		{
			return object_.getValueAsObject();
		}

		// Return name of partition 
		public String getValueAsString()
		{
			
			return object_.getValueAsString();
		}

		public void setValue ( Object newValue ) throws InvalidDataException
		{
			// no alterations allowed by clients
			throw new InvalidDataException ("Object is not editable");
		}

		// DataItem Interfaces
		public Object getProperty (String propertyName)
		{
			// empty, no properties
			return null;
		}

		// Return a pointer to this class instance. It is the source of the publishing events
		public InfoBusEventListener getSource()
		{
			// Return the super class as a source of the event
			return source_;
		}

		public void release()
		{
			// empty, not synchronized
		}

	 
	 }
	// command, e.g.: xcommand
	public Advertiser (String infobus, String itemName)
	{
		producers_ = new HashMap();
		itemName_ = itemName;
		infobusSupport_ = new InfoBusMemberSupport (null);
		
		try 
		{
			infobusSupport_.joinInfoBus(infobus);
			
			
			
		} catch (Exception e) 
		{
			logger.error ("Cannot access infobus " + infobus + ": " + e.toString());
		}
	}
	
	public void advertise(ImmediateAccess object)
	{
		if (infobusSupport_ != null)
		{	
			Producer producer = new Producer (object, itemName_);
			producers_.put (object, producer);
			infobusSupport_.getInfoBus().addDataProducer (producer);
			infobusSupport_.getInfoBus().fireItemAvailable (itemName_, null, producer);
		
			
		} else 
		{
			logger.error ("Cannot publish infobus item " + itemName_ + ": infobus not available");
		}
	
	}
	
	public void leave ()
	{		
		try 
		{
			this.revokeAll();

			infobusSupport_.leaveInfoBus();
		} catch (Exception e) 
		{
			logger.error ("Infobus error " + e.toString());
		}
	}	
	
	
	public void revoke (ImmediateAccess object)
	{
		logger.debug("revoke and remove item " + itemName_ + " object name: " + object.getValueAsString() );
		
		Producer producer = (Producer) producers_.get(object);		
		infobusSupport_.getInfoBus().fireItemRevoked(itemName_, producer);
		infobusSupport_.getInfoBus().removeDataProducer(producer);
		producers_.remove(object);
	}
	
	public void revokeAll()
	{
		Set keys = producers_.keySet();
		Object [] a = keys.toArray(new Object [] {} );
			
		for (int i = 0; i < a.length ; i++ )
		{		
				this.revoke((ImmediateAccess) a[i]);
		}
	
	}
	
}
