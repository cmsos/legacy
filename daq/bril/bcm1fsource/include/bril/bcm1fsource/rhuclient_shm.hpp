/***************************************************************************** 
 * File  rhuclient_shm.hpp
 * created on 13.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/

#ifndef RHUCLIENT_SHM_HPP_
#define RHUCLIENT_SHM_HPP_

//#include <rhu/rhuclient.hpp>
#include "bril/bcm1fsource/rhuclient.hpp"
#include <boost/shared_ptr.hpp>
//#include "shm/rhu_shmclient.hpp"

namespace rhu {

  namespace shm {

    // Class to hold histogram data
    // This class is the owner of the data
    // thus, pointer retrieved by this class will
    // invalidate once the last copy of this class is disposed

    class Event : public IEvent
    {
    private:
      // User should not create this class directly
      Event()
      {}
      Event(PHistogramData ahist, PPostmortemData apostmortem)
	: m_histogram(ahist), m_postmortem(apostmortem)
      {}
    public:
      // fabric method
      static PEvent create(PHistogramData ahist, PPostmortemData apostmortem);

      // get Pointer to HistogramData
      virtual HistogramData const* getHistogramData() const
      {
	return m_histogram.get();
      }
      // get PostmortemData
      virtual PostmortemData const* getPostmortemData() const
      {
	return m_postmortem.get();
      }
    private:
      PHistogramData m_histogram;
      PPostmortemData m_postmortem;
    };

    // Abstract Client Interface to Connect to a RHU Device
    class Client : public IClient
    {
    protected:
      // checks, whether a connection is available
      // if no connection is available, throws an EClientNotConnected exception
      void checkConnection() const;

    private:
      bool dummy_isConnected;
      

    public:
      Client()
	: m_config(*this), m_status(*this), m_daq(*this)
      {}

      // Connection Handling
      void connect();
      // connecto to the rhu-device
      virtual void connect(const std::string& device);
      // Reconnect to the rhu-device
      virtual void reconnect();
      // Disconnect from the rhu-device
      virtual void disconnect();
      // returns true, if connection is active
      virtual bool isConnected() const ;

      // receive data,

      class Config : public IClient::Config
      {
      private:
	Client& m_parent;

	bool dummy_isChannelEnabled[8];
	bool dummy_isSimulatorEnable;
	bool dummy_isSampleModeEdge;
	bool dummy_isPostmortemAutoRestart;
	int  dummy_orbitTriggerDelay;
	int  dummy_beamAbortOrbits;
	int  dummy_beamAbortTimeout;
	int  dummy_histogramOrbitCount;

      public:
	Config(Client& aparent)
	  : m_parent(aparent), 
	    dummy_isSimulatorEnable(false),
	    dummy_isSampleModeEdge(true),
	    dummy_isPostmortemAutoRestart(false),
	    dummy_orbitTriggerDelay(0),
	    dummy_beamAbortOrbits(1),
	    dummy_beamAbortTimeout(9999),
	    dummy_histogramOrbitCount(9999)
	{
	  for (int count = 0; count < 8; count++)
	    {
	      dummy_isChannelEnabled[count] = true;
	    }
	}
	// Disable/Enable Channels
	// Enables or Disables the given Channel
	// Disabled channels are not included in the acquisition
	// Index must be >=0 <10 or an exception is thrown
	virtual void setChannelEnable(int index, bool enable);
	// returns true, if given channel is enabled
	// Index must be >=0 <10 or an exception is thrown
	virtual bool isChannelEnabled(int index) const ;

	// Configures the Orbit Trigger Delay
	// delay >=0
	virtual void setOrbitTriggerDelay(int delay);
	// Returns actual Orbit Trigger Delay
	virtual int getOrbitTriggerDelay() const ;

	// Configures Bunch Trigger Delay
	virtual void setBunchTriggerDelay(int delay);
	// Returns actual Bunch Trigger Delay
	virtual int getBunchTriggerDelay() const ;

	// sets the number of orbits to continue daq after beam abort event
	virtual void setBeamAbortOrbits(int orbits);
	// returns beam abort orbits
	virtual int getBeamAbortOrbits() const;

	// activate beam abort timeout and auto continue
	// if timeout > 0, acquisition will start automatically after N ms
	// if timeout = 0, no auto continuation is active
	// timeout >=0 ; < 65536
	virtual void setBeamAbortTimeout(int timeout);
	// returns the beam abort timeout setting
	virtual int getBeamAbortTimeout() const ;

	// set Histogram Orbit Counter, the number of orbits to fill into a
	// histogram
	// orbits > 500 ; < 65536
	virtual void setHistogramOrbitCount(int orbits);
	// Returns the Number of Orbits filled into a histogram
	virtual int getHistogramOrbitCount() const ;

	virtual bool isSimulatorEnable() const;
	virtual void setSimulatorEnable(bool enable);

	virtual bool isSampleModeEdge() const;
	virtual void setSampleModeEdge(bool enable);

	virtual bool isPostmortemAutoRestart() const;
	virtual void setPostmortemAutoRestart(bool enable);

	// // Returns a Device Config Listener
	// virtual DeviceConfigListener getConfigListener();
      };

      class Status : public IClient::Status
      {
      private:
	Client& m_parent;

	bool           dummy_isOrbitTriggerAvailable;
	bool           dummy_isBunchClockAvailable;
	unsigned short dummy_firmwareRevision;
	unsigned short dummy_temperature;

      public:
	Status(Client& aparent)
	  : m_parent(aparent),
	    dummy_isOrbitTriggerAvailable(true),
	    dummy_isBunchClockAvailable(true),
	    dummy_firmwareRevision(0),
	    dummy_temperature(0)
	{
	}
	// returns the actual orbit counter
	//  - attention, this is only snapshot of a 10KHz Counter
	//    at the moment of reading the value, the counter will have changed
	// virtual u64_t getActualOrbitCounter() const ;
	// // resets the orbit counter to 0
	// virtual void  resetOrbitCounter();

	// // returns the statistic of the module
	// virtual void getStatistic(Statistic& statistic) const ;

	// returns true, if orbit trigger is available
	virtual bool isOrbitTriggerAvailable() const ;
	// returns true, if bunch clock is available
	virtual bool isBunchClockAvailable() const ;

	// returns the actual board firmware revision
	virtual unsigned short getFirmewareRevision() const;

	// returns the actual board firmware revision
	virtual unsigned short getTemperature() const;

	// // Returns a Device Config Listener
	// virtual DeviceStatusListener getStatusListener() ;
      };

      // --- State Control of the Module

      class DAQ : public IClient::DAQ
      {
      private:
	Client& m_parent;

	DeviceMode dummy_operationMode;

      public:
	DAQ(Client& aparent)
	  : m_parent(aparent),
	    dummy_operationMode(RHU_MODE_CONFIGURATION)
	{
	}

	// returns actual operation mode
	virtual DeviceMode getOperationMode() const;
	// sets the operation mode
	virtual void setOperationMode(DeviceMode mode);

	// --- Event Handling

	// Waits for an Event from the rhu-device
	// returns 0 if data is available
	// returns error code on error
	virtual error_code_t waitForEvent(int timeout, PEvent& event);

	// --- Histogramdata Handling

	// --- Postmortem Data Handling

	// Retrieves Postmortem Data
	// index=0, latest Postmortem Buffer
	// index=1, Postmortem Buffer just before latest
	// index = 0..4
	// Returns 0 on success or error code
	virtual error_code_t getPostmortemBuffer(int index);

	// Returns number of existing Postmortem Buffer that are filled
	virtual int getPostmortemBufferCount() const ;
      };


      friend class Config;
      friend class Status;
      friend class DAQ;

      // returns a reference to the Config Object
      virtual IClient::Config& config();
      // returns reference to the status object
      virtual IClient::Status& status();
      // returns reference to the DAQ Object
      virtual IClient::DAQ& daq();

      //      friend class RhuClientDataLock;

    protected:
      //u64_t   m_orbitCounter;
      //bool m_connected;

      //      boost::shared_ptr<RhuClientInterface> m_interface;

    private:
      Config m_config;
      Status m_status;
      DAQ m_daq;
      std::string m_device;
    };

  } // namespace shm

} // namespace rhu {

#endif /* RHUCLIENT_SHM_HPP_ */
