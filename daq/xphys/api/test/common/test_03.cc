#include <vector>
#include "xphys.h"

int main (int argc, char** argv)
{
	cout << "Allocating 10000 pages of bigphys memory" << endl;
	XPhysAllocator allocator(10000);
	
	vector<Buffer*> buffers;
	
	cout << "Allocate blocks of 8192 Bytes until out of memory." << endl;
	
	int i = 0;
	for (;;)
	{
		Buffer* b = allocator.alloc ( 8192 );
		if (b == 0)
		{
			cout << "Out of memory after " << i << " buffers." << endl;
			cout << "Allocated in total: " << i*8192 << " bytes." << endl;
			return 0;
		}
		
		i++;
	}
	
	return 0;
	
}
