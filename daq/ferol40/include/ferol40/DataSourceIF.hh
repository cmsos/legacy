#ifndef __DataSourceIF
#define __DataSourceIF
#include "hal/HardwareDeviceInterface.hh"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "log4cplus/logger.h"

namespace ferol40
{
    class DataSourceIF {
    public:
      DataSourceIF( HAL::HardwareDeviceInterface *device_P,
		    utils::InfoSpaceHandler &appIS,
		    Logger logger );
      virtual void setDataSource() const = 0;
      virtual ~DataSourceIF () = 0;
    protected:
        utils::InfoSpaceHandler &appIS_;
        std::string operationMode_;
        std::vector<std::string> dataSource_;
	std::vector<bool> streams_;
        HAL::HardwareDeviceInterface *device_P;
        Logger logger_;
    };
}

#endif /* __DataSourceIF */
