/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * io.hpp
 *
 *  Created on: Nov 17, 2017
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include <cmath>
#include <iostream>
#include <chrono>
#include <string>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdexcept>

namespace mpila
{

namespace helpers
{
namespace io
{
enum class ApplicationState
{
    INIT,
    START,
    RUN,
    DRAIN,
    CLEAR,
    STOP
};

using timepoint_t = std::chrono::system_clock::time_point;

void setThreadAffinity(size_t cpuId);

inline std::string header(int rank)
{
    std::stringstream ss;
    ss << "\033[" << (31 + rank) << "m";
    ss << "[rank " << rank << "]";
    return ss.str();
}

const std::string end(" \033[0m");

template <class Iter>
void writeMeasurementsToFile(const std::string &filePath, size_t sendSize, Iter begin, Iter end)
{
    if (begin != end)
    {
        std::ofstream file;
        file.open(filePath, std::ios_base::app);
        if (file.is_open())
        {
            file << sendSize << ", ";

            if (std::distance(begin, end) > 1)
            {
                for (auto i = begin; i != end - 1; i++)
                {
                    file << std::setprecision(18) << *i << ", ";
                }
            }
            file << *(end - 1) << std::endl;
            file.close();
        }
        else
        {
            std::stringstream ss;
            ss << "Failed to open file " << filePath;
            throw std::runtime_error(ss.str());
        }
    }
}
} /* namespace io */
} /* namespace helpers */
}
