package xdaq.xdesk.xprop;

import java.lang.*;
import java.lang.*;

public class ParameterTableModelException extends Exception
{
	ParameterTableModelException () { super(); }

	ParameterTableModelException ( String msg )
	{
		super (msg);
	}
}
