<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
</head>

<body>
<?php
include_once ('config.php');
include_once ('las.php');
include_once ('tools.php');

$_zone = $_GET ['zone'];
$_flashlist = $_GET ['flashlist'];
$_index = $_zone ."-". strtolower($_flashlist) . "-flash";

$filter = array ();
$jsonhits = topHits ( $config ['elasticsearchurl'], $_index, $_flashlist, $config ['maxsources'], $filter );
$hitsCount = count ( $jsonhits ['aggregations'] ['group_by_context'] ['buckets'] );

echo 'Top hits count = ' . $hitsCount . PHP_EOL;

echo '<br>';

echo '<form action="types.php">';
echo '<input type="submit" value="Go back" method="get"/>';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '</form>';
?>

</body>
</html>
