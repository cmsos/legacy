#include "bril/bhmtriggeredsource/ReadoutCore.h"
#include "hcal/uhtr/uHTR.hh"
#include "amc13/AMC13.hh"
#include "amc13/Status.hh"
#include "xcept/Exception.h"
#include "toolbox/string.h"
#include "toolbox/TimeVal.h"
#include "log4cplus/loggingmacros.h"

namespace bril {

namespace bhmtriggeredsource {

    class ReadoutTask : public ::toolbox::Task {
    public:
      ReadoutTask(ReadoutCore& c): Task("ReadoutTask"),m_core(c) { }
      virtual int svc() { return m_core.readoutTask(); }
    private:
      ReadoutCore& m_core;
    };

    ReadoutCore::ReadoutCore(log4cplus::Logger l) : m_logger(l), m_task(0) {
      m_state=st_Uninit;
      m_connectionManager=0;
      m_amc13=0;
      m_selfCoincs=std::vector<unsigned int>(5,31);
      m_num_events=0;
    }

    ReadoutCore::~ReadoutCore() {
      reset();
    }
    
    void ReadoutCore::configure() {
      uhal::setLogLevelTo(uhal::ErrorLevel());

      if (m_state!=st_Uninit) {
	LOG4CPLUS_ERROR(m_logger,::toolbox::toString("ReadoutCore: Configure called, but state is not uninit (%d)",m_state));
	return;
      }

      LOG4CPLUS_INFO(m_logger,"ReadoutCore: Start configuration with file: " + m_connectionsFile );
      
      // create the connection manager
      m_connectionManager=new uhal::ConnectionManager("file://"+m_connectionsFile);
      
      // connect to the AMC13
      m_amc13=new amc13::AMC13(m_connectionsFile,"T1","T2");
      
      // AMC13 -> basic setup
      // just T2-related work here.
      m_amc13->reset(amc13::AMC13::T2);

      uint32_t slotMask=m_amc13->parseInputEnableList(m_slotList,true);
      m_amc13->enableAllTTC(); // do this first, as it can interrupt clocks otherwise
      m_amc13->AMCInputEnable(slotMask);

      const int icrate=100;

      // connect to the uHTRs
      for (int slot=1; slot<12; slot++ ) {
	// see if this is enabled in the mask
	if ((slotMask&(1<<(slot-1)))==0) continue;

	// next, connect to the uHTR in question
	char name[120];
	sprintf(name,"uhtr.slot%d",slot);
	uhal::HwInterface hw=m_connectionManager->getDevice(name);
	m_uHTRs.push_back(new hcal::uhtr::uHTR(name,hw));
	hcal::uhtr::uHTR* uhtr=m_uHTRs.back();

	uhtr->setCrateSlot(icrate,slot);
	
        uint8_t flavorF, majorF, minorF, patchF;
        uint8_t flavorB, majorB, minorB, patchB;
        uhtr->firmwareRevision(true, flavorF, majorF, minorF, patchF);
        uhtr->firmwareRevision(false, flavorB, majorB, minorB, patchB);
        // if we read the default values, we could not communicate with the card
        if (flavorB == 0 && majorB == 0 && minorB == 0 && patchB == 0) {
          std::string errorMsg = ::toolbox::toString("Could not read uHTR firmware revision from Back FPGA in %s",uhtr->cname());
          LOG4CPLUS_ERROR(m_logger, errorMsg);
	  m_state=st_Fail;
          XCEPT_RAISE(xcept::Exception, errorMsg);
        } else if (flavorF == 0 && majorF == 0 && minorF == 0 && patchF == 0) {
          std::string errorMsg = ::toolbox::toString("Could not read uHTR firmware revision from Front FPGA in $s",uhtr->cname());
          LOG4CPLUS_ERROR(m_logger, errorMsg);
	  m_state=st_Fail;
          XCEPT_RAISE(xcept::Exception, errorMsg);
        }
        else {
          LOG4CPLUS_INFO(m_logger, ::toolbox::toString("%s using uHTR firmware revisions FRONT=%x.%x.%x, BACK=%x.%x.%x", uhtr->cname(), majorF, minorF, patchF, majorB, minorB, patchB));
	}

      }
                  
      // uHTRs -> setup
      for (std::vector<hcal::uhtr::uHTR*>::iterator ihtr=m_uHTRs.begin(); ihtr!=m_uHTRs.end(); ihtr++) {
	(*ihtr)->daq_path_enable(false);
	(*ihtr)->daq_path_reset();
	(*ihtr)->daq_f2b_links_reset();
	(*ihtr)->daq_path_setup(((*ihtr)->slot()<<8)|icrate,m_bcOffsetDAQ,m_samples,2,0,0,m_pipeline,true);
	std::vector<uint32_t> zsThresholds(24 * (*ihtr)->n_channels_per_fiber(), m_zsThreshold);
	(*ihtr)->daq_zs_setup(false, false, 0x7FFF, false, 0x0, 0x1, zsThresholds, true, true);
	(*ihtr)->daq_link_reset(true,false); // GTX Reset	  
	(*ihtr)->daq_link_reset(false,true); // CORE reset after the GTX reset

	(*ihtr)->bhm_selftrigger_setup(m_selfCoincs);
	
      }
      
      // AMC13 -> prep for DAQ
      m_amc13->daqLinkEnable(false);
      m_amc13->write(amc13::AMC13Simple::T1,"CONF.BCN_OFFSET",0);

      m_amc13->setFEDid(0x123); // get a better fixed value here...
      m_amc13->reset(amc13::AMC13::T1);
      
      // setup the self-trigger
      for (int slot=1; slot<12; slot++ ) {
	bool slotActive=((slotMask&(1<<(slot-1)))!=0);
	if (slotActive) m_amc13->write(amc13::AMC13::T1,::toolbox::toString("CONF.LTRIG.AMC%02d.BIT0.TRIGGER_MASK",slot),0xE0);
	else m_amc13->write(amc13::AMC13::T1,::toolbox::toString("CONF.LTRIG.AMC%02d.BIT0.TRIGGER_MASK",slot),0xFF); 
      }
      m_amc13->write(amc13::AMC13::T1,0x1040,1); // use bit 0
      
      // reset the T1 counters
      m_amc13->resetCounters();

      LOG4CPLUS_INFO(m_logger,"ReadoutCore: Configured");
      m_state=st_Ready;
    }


    void ReadoutCore::start() {
      if (m_state!=st_Ready) return;

      // start the uHTRs
      for (std::vector<hcal::uhtr::uHTR*>::iterator ihtr=m_uHTRs.begin(); ihtr!=m_uHTRs.end(); ihtr++) {
	(*ihtr)->daq_path_reset();
	(*ihtr)->daq_path_enable(true);
      }
      
      // start the AMC13
      m_amc13->reset(amc13::AMC13::T1);
      m_amc13->startRun();
      m_amc13->enableLocalL1A(true);
      m_amc13->startContinuousL1A();
      
      // start the readout thread
      if (m_task!=0) delete m_task;
      m_task=new ReadoutTask(*this);
      m_task->activate();
      LOG4CPLUS_INFO(m_logger,"ReadoutCore: Starting");
      m_state=st_Active;
    }

    void ReadoutCore::stop() {
      if (m_state==st_Fail) return;
      // stop the readout thread
      ThreadCommand cmd(cmd_Stop); // stupid process due to bad coding of squeue
      m_cmdQueue.push(cmd);

      // stop the AMC13
      m_amc13->stopContinuousL1A();
      m_amc13->enableLocalL1A(false);
      m_amc13->endRun();

      // stop the uHTRs
      for (std::vector<hcal::uhtr::uHTR*>::iterator ihtr=m_uHTRs.begin(); ihtr!=m_uHTRs.end(); ihtr++) {
	(*ihtr)->daq_path_enable(false);
      }
      
      m_state=st_Ready;
    }

    void ReadoutCore::pause() {
      if (m_state!=st_Active) return;

      // stop the readout thread
      ThreadCommand cmd(cmd_Pause); // stupid process due to bad coding of squeue
      m_cmdQueue.push(cmd);
      m_amc13->stopContinuousL1A();
      m_state=st_Paused;
    }
    void ReadoutCore::resume() {
      if (m_state!=st_Paused) return;
      
      // stop the readout thread
      ThreadCommand cmd(cmd_Resume); // stupid process due to bad coding of squeue
      m_cmdQueue.push(cmd);
      m_amc13->startContinuousL1A();
      m_state=st_Active;
    }
    
    void ReadoutCore::reset() {      
      if (m_task!=0) {
	ThreadCommand cmd(cmd_Stop); // stupid process due to bad coding of squeue
	m_cmdQueue.push(cmd);
	delete m_task;
      }
      m_task=0;
      if (m_amc13!=0) delete m_amc13;
      m_amc13=0;
      if (m_connectionManager!=0) delete m_connectionManager;
      for (std::vector<hcal::uhtr::uHTR*>::iterator i=m_uHTRs.begin(); i!=m_uHTRs.end(); i++) delete *i;
      m_uHTRs.clear();
      
      m_state=st_Uninit;
    }

    void ReadoutCore::setup(const std::string& connectionsFile,
			    const std::string& slotList,
			    int pipeline, int samples, int zs_threshold) {
      m_pipeline=pipeline;
      m_samples=samples;
      m_zsThreshold=zs_threshold;
      m_connectionsFile=connectionsFile;
      m_slotList=slotList;
      m_bcOffsetDAQ=1;
    }

   }

 }

#include <iostream>
#include <numeric>

namespace bril {

namespace bhmtriggeredsource {



    int ReadoutCore::readoutTask()
     {
      bool readoutActive=true; // used by pause/resume
      int m_num_events=0;
      ::toolbox::TimeVal FileTime;
      FileTime = ::toolbox::TimeVal::gettimeofday();
      const std::string time = FileTime.toString(toolbox::TimeVal::gmt);
      std::string Name;
      Name = time + ".raw";
      FILE * Outfile;
      Outfile = fopen (Name.c_str(),"wb");
      std::map<uint16_t, std::vector<int>> bad_channels;
      bad_channels[0xb64] = { 34 };
      int n_bad_events = 0;
      std::cerr << std::hex;
      for ( ; ; )
       {
        if (!readoutActive)
         {
          ThreadCommand cmd=m_cmdQueue.pop();
	    if (cmd==cmd_Stop) return 0;
	    else if (cmd==cmd_Pause) readoutActive=false; // shouldn't really be possible...
	    else if (cmd==cmd_Resume) readoutActive=true; 
         }
        else
         {
	  // check for data...
	  std::vector<uint64_t> payload=m_amc13->readEvent();
           if (payload.empty())
            {
	    // sleep a bit...
//	    ::usleep(1); 
            }
           else
            {
//	    for (size_t i=0; i<payload.size(); i++) {
//	      printf("%08lx %016lx\n",i,payload[i]);
//	    }
             auto it = payload.begin();
             auto start_of_event = payload.begin();
//             std::cerr << "received payload of size " << payload.size() << std::endl;
             for ( ; ; )
              {
               if ( it + 2 > payload.end() )
                {
//                 std::cerr << "Payload completed or too small for header, skipping!!!!" << std::endl;
                 break;
                }
               it++;
               int NAMC = (*it>>52)&0xF;
               it++;
               if ( it + NAMC > payload.end() )
                {
                 std::cerr << "Payload too small for AMC headers (" << NAMC << "), skipping!!!!" << std::endl;
                 break;
                }


	           std::vector<uint32_t> AMCSizes;
               for(int i=0; i<NAMC; i++)
                {
                 AMCSizes.push_back((*it>>32)&0xFFFFFF);
                 it++;
                }
               uint32_t totalamcsize = std::accumulate( AMCSizes.begin(), AMCSizes.end(), 0u );
               if ( it + totalamcsize > payload.end() )
                {
                 std::cerr << "Payload too small for AMCs (" << totalamcsize << "), skipping!!!!" << std::endl;
                 break;
                }
               bool bad_event = true;
               for(int i=0; i<NAMC; i++)
                {
                 uint32_t evn = (*it>>32)&0xFFFFFF;
                 uint16_t orn = (it[1]>>16)&0xFFFF;
                 uint16_t bcn = (*it>>20)&0xFFF;
                 uint16_t board = it[1] & 0xFFFF;
                 if ( AMCSizes[i] <= 3 )
                  {
                   it+=AMCSizes[i];
                   continue;
                  }
                 channeldata cd( evn, orn, bcn, board, &(*(it+2)));
                 bool badchannel = find(bad_channels[board].begin(),bad_channels[board].end(), ( cd.channel() & 0xFF ) ) != bad_channels[board].end();
                 if ( !badchannel )
                  {
                   bad_event = false;
                   m_events.push_back(channeldata( evn, orn, bcn, board, &(*(it+2))));
                   std::cerr << "event with board " << board << ", channel " << ( cd.channel() & 0xFF ) << std::endl;
                  }
                 it+=AMCSizes[i];
                }
               if ( it + 2 > payload.end() )
                {
                 std::cerr << "Payload too small for trailer, skipping!!!!" << std::endl;
                 break;
                }
               it++;
//               std::cerr << "Got trailer: first four bits = " << ((*it)>>60) << std::endl;
               it++;
//               std::cerr << "End of event: it was " << ( bad_event ? "bad" : "good" ) << std::endl;
               if(!bad_event)
                {
                 printf("N. events: %d\n",m_num_events);
                 m_num_events++;
                 fwrite(&(*start_of_event),sizeof(uint64_t),(it-start_of_event),Outfile);
                }
               start_of_event = it;
              }
            }
	  // check for commands...
          if (!m_cmdQueue.empty())
           {
	    ThreadCommand cmd=m_cmdQueue.pop();
	    if (cmd==cmd_Stop)
          {
           fclose(Outfile);
           printf("N. saved events: %lu\n",m_events.size());
           return 0;
           }
	    else if (cmd==cmd_Pause) readoutActive=false;
	    else if (cmd==cmd_Resume) readoutActive=true; // shouldn't really be possible...
           }
         }
       };
      return 1;
    }
   
    ReadoutCore::channeldata::channeldata( uint32_t evn, uint16_t orn, uint16_t bcn, uint16_t board, const uint64_t* data ) :
     m_evn( evn ), m_orn( orn ), m_bcn( bcn )
     {
      const uint16_t* iter = reinterpret_cast<const uint16_t*>(data);
      m_id = (board << 16) | ((*iter)&0xFF);
      iter++;
      int i(0);
      while(((*iter)&0x8000)==0){
        m_adc[i] = (*iter)&0xFF;
        m_tdc[i] = ((iter[1])&0x3F);
        iter+=2;
        i++;  
      }
      m_samples = i;
     }

   }

 }
