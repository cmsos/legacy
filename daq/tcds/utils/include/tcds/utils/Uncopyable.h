#ifndef _tcds_utils_Uncopyable_h
#define _tcds_utils_Uncopyable_h

namespace tcds {
  namespace utils {

  class Uncopyable
    /**
     * Just our own simple mix-in class to prevent copying.
     */
  {

  protected:
    Uncopyable() {};
    ~Uncopyable() {};

  private:
    Uncopyable(Uncopyable const&);
    Uncopyable& operator=(Uncopyable const&);

  };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_Uncopyable_h
