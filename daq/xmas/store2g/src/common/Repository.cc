// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <set>
#include <vector>
#include <map>
#include <string>
#include "xcept/Exception.h"
#include "xdata/Table.h"
#include "xmas/store2g/Repository.h"
#include "toolbox/TimeVal.h"

xmas::store2g::Repository::Repository()
	: mutex_(toolbox::BSem::FULL)
{
}

xmas::store2g::Repository::~Repository()
{
	this->clear();
}

void xmas::store2g::Repository::add( const std::string& flashlist, const std::string& originator,  xdata::Table::Reference& table ) 
	
{		
	std::map<std::string, xdata::Table::Reference>::iterator k = tables_.find(flashlist);
	if (k == tables_.end())
	{
		// Entry not existing yet - Create it
		tables_[ flashlist ] = table;
	}
	else
	{
		try
		{
			xdata::Table::merge( &(*(*k).second), &(*table) );
		}
		catch (xdata::exception::Exception& e)
		{
			XCEPT_RETHROW (xmas::store2g::exception::FailedToInsert, "Flashlist '" + flashlist + "'", e);
		}		
	}
	
	properties_[flashlist].setProperty("lastupdate", toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt));
	properties_[flashlist].setProperty("originator", originator);
}

void xmas::store2g::Repository::clear( const std::string& flashlist ) 
	
{	
	std::map<std::string, xdata::Table::Reference>::iterator k = tables_.find(flashlist);
	if (k != tables_.end())
	{
		tables_.erase (k);
	}
	else
	{
		XCEPT_RAISE (xmas::store2g::exception::FailedToClear, "Flashlist '" + flashlist + "'");
	}
}				

void xmas::store2g::Repository::clear() 
{
	tables_.clear();
}

xdata::Table::Reference xmas::store2g::Repository::getData ( const std::string& flashlist ) 
	
{
	std::map<std::string, xdata::Table::Reference>::iterator k = tables_.find(flashlist);
	if (k != tables_.end())
	{
		return (*k).second;
	}
	else
	{
		XCEPT_RAISE (xmas::store2g::exception::NoData, "Flashlist '" + flashlist + "'");
	}
}

toolbox::Properties xmas::store2g::Repository::getProperties ( const std::string& flashlist ) 
	
{
	std::map<std::string, toolbox::Properties>::iterator k = properties_.find(flashlist);
	if (k != properties_.end())
	{
		return (*k).second;
	}
	else
	{
		XCEPT_RAISE (xmas::store2g::exception::NoData, "Flashlist '" + flashlist + "'");
	}
}

std::set<std::string> xmas::store2g::Repository::getFlashlists()
{
	std::set<std::string> s;
	
	std::map<std::string, xdata::Table::Reference>::iterator k;
	for (k = tables_.begin(); k != tables_.end(); ++k)
	{
		s.insert ( (*k).first );
	}	
	
	return s;
}

void xmas::store2g::Repository::lock()
{
	mutex_.take();
}

void xmas::store2g::Repository::unlock()
{
	mutex_.give();
}
