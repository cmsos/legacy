// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_net_UUID_h_
#define _toolbox_net_UUID_h_

#include <string>
#include <stdlib.h>
#include <uuid/uuid.h>
#include <iostream>
#include "toolbox/net/exception/Exception.h"

namespace toolbox {
namespace net {

class UUID {

	public:
	
	//
	// construction & destruction 
	//
	
	/*! The standard constructor creates an empty, uninitialized identifier. It need to be set afterwards
	    by calling the function make(type)
	*/
	UUID         () ;                         /* standard constructor */
	UUID         (const UUID   & obj) ;       /* copy     constructor */
	UUID         (const std::string & value) ;       /* import   constructor */
	UUID		 (uuid_t & id);
	virtual ~UUID();                         /* destructor */

	//
	// copying & cloning 
	//
	
	UUID         &operator=    (const UUID   &_obj) ;       /* copy   assignment operator */

	
	//
	// content comparison 
	//
	bool          isnil        (void) const  ;                     /* regular method */
	int           compare      (const UUID &_obj) const  ;         /* regular method */
	int           operator==   (const UUID &_obj) const;         /* comparison operator */
	int           operator!=   (const UUID &_obj) const;         /* comparison operator */
	int           operator<    (const UUID &_obj) const;         /* comparison operator */
	int           operator<=   (const UUID &_obj) const;         /* comparison operator */
	int           operator>    (const UUID &_obj) const;         /* comparison operator */
	int           operator>=   (const UUID &_obj) const;         /* comparison operator */

	std::string toString(void) const ; 
	
	friend std::ostream& operator <<(std::ostream& s, const toolbox::net::UUID& uuid);
	
    private:
        uuid_t id_;         


};

std::ostream& operator <<(std::ostream& s, const toolbox::net::UUID& uuid);

}
}


#endif
