// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"
#include "xoap/DOMParserFactory.h"
#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/store2g/Application.h"
#include "xplore/DiscoveryEvent.h"

#include "xdata/InfoSpaceFactory.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "xdata/exdr/Serializer.h"

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/Table.h"

#include "xdaq/ApplicationDescriptorImpl.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/string.h"
#include "toolbox/stl.h"
#include "toolbox/Runtime.h"
#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"

#include "xoap/Event.h"

#include "xdata/UnsignedInteger64.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"

XDAQ_INSTANTIATOR_IMPL (xmas::store2g::Application);

xmas::store2g::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), descriptorsCache_(this), tstoreProxy_(this, &descriptorsCache_), stager_(this, &repository_, &tstoreProxy_, &storeSettings_), b2inEventingProxy_(0)
{
	s->getDescriptor()->setAttribute("icon", "/xmas/store2g/images/xmas-store2g-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/store2g/images/xmas-store2g-icon.png");
	reportLostCounter_ = 0;

	subscribeGroup_ = "";
	storeView_ = "default";

	period_ = "PT15S";
	subscribeExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("oclFileName", &oclFileName_);
	this->getApplicationInfoSpace()->fireItemAvailable("db", &db_);
	this->getApplicationInfoSpace()->fireItemAvailable("authentication", &authentication_);
	this->getApplicationInfoSpace()->fireItemAvailable("credentials", &credentials_);
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);
	this->getApplicationInfoSpace()->fireItemAvailable("storeGroup", &storeGroup_);
	this->getApplicationInfoSpace()->fireItemAvailable("storeView", &storeView_);
	this->getApplicationInfoSpace()->fireItemAvailable("url", &settingsURLs_);
	this->getApplicationInfoSpace()->fireItemAvailable("period", &period_);
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration", &subscribeExpiration_);

	b2in::nub::bind(this, &xmas::store2g::Application::onMessage);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &xmas::store2g::Application::Default, "Default");
	xgi::framework::deferredbind(this, this, &xmas::store2g::Application::applySettings, "applySettings");
	xgi::framework::deferredbind(this, this, &xmas::store2g::Application::clear, "clear");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	//settings_.setProperty("autorefresh", "0");
}

xmas::store2g::Application::~Application ()
{

}

void xmas::store2g::Application::loadSettings (const std::string& path) 
{
	// import all flash list definitions 
	// The path may be a pattern, so expand it before
	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(path);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to import flashlist definitions from '" << path << "', ";
		XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
	}

	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(*j);
			storeSettings_.add(doc);
			doc->release();
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Loaded store settings from '" << (*j) << "'");
		}
		catch (xoap::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to load store configuration file from '" << (*j) << "'";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}
		catch (xmas::store2g::exception::Exception& e)
		{
			doc->release();
			std::stringstream msg;
			msg << "Failed to parse store configuration from '" << (*j) << "'";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}
	}

	return;
}

void xmas::store2g::Application::asynchronousExceptionNotification (xcept::Exception& e)
{

	std::stringstream msg;
	msg << "Failed to subscribe to b2in eventing";
	XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, ex, msg.str(), e);
	this->notifyQualified("error", ex);

}

void xmas::store2g::Application::subscribeToDiscoveryServices () 
{
	try
	{
		xplore::Interface * interface = dynamic_cast<xplore::Interface*>(getApplicationContext()->getFirstApplication("xplore::Application"));

		std::vector < xplore::Advertisement::Reference > resultSet;
		std::stringstream filter;
		std::set < std::string > groups = getApplicationContext()->getDefaultZone()->getGroupNames(this->getApplicationDescriptor());

		filter << "(&(service=tstore)";
		filter << "(|";

		for (std::set<std::string>::iterator i = groups.begin(); i != groups.end(); ++i)
		{
			filter << "(group=*" << (*i) << "*)";
		}
		filter << "))";

		LOG4CPLUS_DEBUG(getApplicationLogger(), "Discovering services using filter: " << filter.str());
		interface->addListener(this, "peer", filter.str());
	}
	catch (xplore::exception::Exception& e)
	{
		XCEPT_RETHROW(xmas::store2g::exception::Exception, "failed to subscribe to discovery services", e);
	}
	catch (xdaq::exception::Exception& e)
	{
		XCEPT_RETHROW(xmas::store2g::exception::Exception, "cannot find discovery services plugin", e);
	}
}

void xmas::store2g::Application::actionPerformed (toolbox::Event& event)
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << event.type());

	static bool stagerEnabled = false;

	if (event.type() == "xplore::DiscoveryEvent")
	{
		// forward to cache
		descriptorsCache_.actionPerformed(event);

		// activate stager at this point to prevent/reduce warnings due to late discovery of services
		if ( ! stagerEnabled )
		{
			toolbox::TimeInterval interval;
			interval.fromString(period_.toString());
			stager_.enable(interval, storeGroup_.toString(), storeView_.toString(), db_.toString());
			stagerEnabled = true;
		}
	}

	/*
	 if ( e.type() == "xdaq::EndpointAvailableEvent" )
	 {
	 // If the available endpoint is a b2in endpoint we are ready to
	 // discover other b2in endpoints on the network
	 //
	 std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

	 xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
	 // xdaq::Endpoint* endpoint = ie.getEndpoint();
	 xdaq::Network* network = ie.getNetwork();

	 LOG4CPLUS_INFO (this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

	 if (network->getName() == networkName)
	 {
	 try
	 {
	 LOG4CPLUS_INFO (this->getApplicationLogger(), "Discover b2in endpoints on network " << networkName);
	 this->discoverEndpoints();
	 this->refreshSubscriptionsToEventing();
	 }
	 catch (xdaq::exception::Exception& e)
	 {
	 LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	 }
	 }
	 }
	 */
}

//
// Infoapace listener
//
void xmas::store2g::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		b2inEventingProxy_ = new b2in::utils::ServiceProxy(this, "b2in-eventing", subscribeGroup_.toString(), this);

		for (xdata::Vector<xdata::String>::iterator i = settingsURLs_.begin(); i != settingsURLs_.end(); i++)
		{
			try
			{
				this->loadSettings((*i).toString());
			}
			catch (xmas::store2g::exception::Exception& e)
			{
				std::stringstream msg;
				msg << "Failed to import store settings from '" << (*i).toString() << "', ";
				XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, q, msg.str(), e);
				this->notifyQualified("fatal", q);
				return;
			}

		}

		// initialize discovery services
		try
		{
			this->subscribeToDiscoveryServices(); // this function can throw
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Use xmas store with dynamic (discovery service) configuration");
		}
		catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

		if ( oclFileName_.toString() != "" ) // for backward compatibility if paramater not specified
		{
			// makes use of the credential as specified in the OCL file for the current default zone (-z zonename or XDAQ_ZONE or default)
			this->loadOCL(oclFileName_.toString());
		}


		tstoreProxy_.authenticate(authentication_.toString(), credentials_.toString());

		try
		{
			if (!toolbox::task::getTimerFactory()->hasTimer("xmas-store2g"))
			{
				(void) toolbox::task::getTimerFactory()->createTimer("xmas-store2g");
			}
			toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer("xmas-store2g");
			toolbox::TimeVal start;
			start = toolbox::TimeVal::gettimeofday();

			// submit scan task
			toolbox::TimeInterval interval2;
			interval2.fromString(period_);
			LOG4CPLUS_INFO(this->getApplicationLogger(), "Schedule discovery timer for " << interval2.toString());
			timer->scheduleAtFixedRate(start, this, interval2, 0, "discovery-staging");

		}
		catch (toolbox::task::exception::InvalidListener& e)
		{
			XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, q, "cannot set up scan timer for b2in eventing proxy", e);
			this->notifyQualified("fatal", q);
		}
		catch (toolbox::task::exception::InvalidSubmission& e)
		{
			XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, q, "cannot set up scan timer for b2in eventing proxy", e);
			this->notifyQualified("fatal", q);
		}
		catch (toolbox::task::exception::NotActive& e)
		{
			XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, q, "cannot set up scan timer for b2in eventing proxy", e);
			this->notifyQualified("fatal", q);
		}
		catch (toolbox::exception::Exception& e)
		{
			XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, q, "cannot set up scan timer for b2in eventing proxy", e);
			this->notifyQualified("fatal", q);
		}

	}
	else
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}
// Load OCL override credentials

void xmas::store2g::Application::loadOCL(const std::string & fname)
{
	std::vector < std::string > files;
	try
	{
		files = toolbox::getRuntime()->expandPathName(fname);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand OCL filename from '" << fname << "', ";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}

	std::string zone = this->getApplicationContext()->getDefaultZoneName();
 	try
        {
		//LOG4CPLUS_INFO (this->getApplicationLogger(), "Load OCL from '" << files[0]  << "'");
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML( files[0] );
		DOMNodeList* list = doc->getElementsByTagNameNS(xoap::XStr("http://xdaq.web.cern.ch/xdaq/xsd/2016/oraclecl"),xoap::XStr("XaasZone") );

		std::string defautZone = this->getApplicationContext()->getDefaultZoneName();

		for (XMLSize_t j = 0; j < list->getLength(); j++)
		{
			DOMNode* zoneNode = list->item(j);
			std::string zoneName  = xoap::getNodeAttribute (zoneNode, "zone");   // new network name

			if (defautZone == zoneName )
			{
					DOMNodeList* credentials = zoneNode->getChildNodes();
					std::string databaseUser = "";
					std::string databasePassword = "";
					std::string databaseTnsName = "";
					for (unsigned int j = 0; j < credentials->getLength(); j++ )
					{


							DOMNode* c = credentials->item(j);
							if  (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databaseUser" ))
							{
								 databaseUser = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());

							}
							else if  (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databasePassword" ))
							{
								 databasePassword = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());

							}
							else if  (( c->getNodeType() == DOMNode::ELEMENT_NODE ) && ( xoap::XMLCh2String(c->getLocalName()) == "databaseTnsName" ))
							{
								 databaseTnsName = xoap::XMLCh2String(c->getFirstChild()->getNodeValue());

							}
					}
					credentials_ = databaseUser + "/" + databasePassword;
					db_ = databaseTnsName;
					//std::cout << "found zone: " << zoneName << " in OCL"  << std::endl;
					//std::cout << "credentials_: " << credentials_.toString() << " in OCL"  << std::endl;
					//std::cout << "db_: " << db_.toString() << " in OCL"  << std::endl;
					break;
			}


		}
		doc->release();
		//LOG4CPLUS_INFO (this->getApplicationLogger(), "Loaded OCL from '" << fname << "'");
	}
	catch (xoap::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to load OCL file from '" << fname << "'";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));

	}

}

void xmas::store2g::Application::Default (xgi::Input * in, xgi::Output * out) 
{
	this->TabPanel(out);
}

void xmas::store2g::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) 
{
	std::string subscriptionID = plist.getProperty("urn:b2in-eventing:id");
	std::string topic = plist.getProperty("urn:b2in-eventing:topic");

	std::map<std::string, xdata::Properties>::iterator i = subscriptions_.find(topic);
	if (i != subscriptions_.end())
	{
		if ((*i).second.getProperty("urn:b2in-eventing:id") != subscriptionID)
		{
			// duplicate
			if (msg != 0)
			{
				msg->release();
			}
			LOG4CPLUS_WARN(this->getApplicationLogger(), "Mismatch UUID from subscription message, assuming duplicated subscription, discarding message");
			return;
		}
	}
	else
	{
		// invalid topic
		if (msg != 0)
		{
			msg->release();
		}
		LOG4CPLUS_WARN(this->getApplicationLogger(), "Could not find the local subscription record, discarding message");
		return;
	}

	std::string flashListName = plist.getProperty("urn:xmas-flashlist:name");
	std::string tagName = plist.getProperty("urn:xmas-flashlist:tag");
	std::string originator = plist.getProperty("urn:xmas-flashlist:originator");
	std::string version = plist.getProperty("urn:xmas-flashlist:version");

	// check if enable storage, check tag and version
	if (!this->checkFlashlist(flashListName, version))
	{
		return;
	}

	repository_.lock();

	xdata::exdr::Serializer serializer;
	xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());
	xdata::Table * t = new xdata::Table();

	try
	{
		serializer.import(t, &inBuffer);

	}
	catch (xdata::exception::Exception& ec)
	{
		delete t;
		msg->release();
		std::stringstream msg;
		msg << "Failed to deserialize flashlist table '";
		msg << flashListName << "'";
		XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, ex, msg.str(), ec);
		this->notifyQualified("error", ex);
		repository_.unlock();
		return;
	}

	msg->release();

	// statistics
	xmas::store2g::Settings::Properties & properties = storeSettings_.getProperties(flashListName);
	properties.incomingReports++;
	properties.totalRowsReceived += t->getRowCount();
	//

	xdata::Table::Reference table(t);
	try
	{
		repository_.add(flashListName, originator, table);
	}
	catch (xmas::store2g::exception::FailedToInsert & e)
	{
		this->notifyQualified("error", e);
		repository_.unlock();
		return;
	}

	repository_.unlock();
}

bool xmas::store2g::Application::checkFlashlist (const std::string & flashListName, const std::string & version)
{
	xmas::store2g::Settings::Properties properties = storeSettings_.getProperties(flashListName);
	std::string name = properties.definition->getProperty("name");
	std::string cversion = properties.definition->getProperty("version");

	if (name == flashListName)
	{
		if (properties.enable)
		{
			if (cversion == version)
			{
				return true;
			}
			else
			{
				std::stringstream msg;
				msg << "flashlist '" << flashListName << "' version mismatch, expected" << cversion << " received " << version;
				LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str());
				return false;
			}
		}
		else
		{
			std::stringstream msg;
			msg << "flashlist '" << flashListName << "' storage is disabled";
			LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str());
			return false;
		}
	}

	std::stringstream msg;
	msg << "flashlist '" << flashListName << "' not configured for storage";
	LOG4CPLUS_WARN(this->getApplicationLogger(), msg.str());
	return false;
}

void xmas::store2g::Application::refreshSubscriptionsToEventing () 
{
	if (subscriptions_.empty())
	{
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Prepare subscription records");
		const xdaq::Network * network = 0;
	    std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		try
		{
			network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
		}
		catch (xdaq::exception::NoNetwork& nn)
		{
			std::stringstream msg;
			msg << "Failed to access b2in network " << networkName << ", not configured";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), nn);
		}
		pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());
		std::list < std::string > topics = storeSettings_.getFlashlistsNames();
		std::list<std::string>::iterator i;
		for (i = topics.begin(); i != topics.end(); ++i)
		{
			if (subscriptions_.find(*i) == subscriptions_.end())
			{
				xdata::Properties plist;
				toolbox::net::UUID identifier;
				plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

				plist.setProperty("urn:b2in-eventing:action", "subscribe");
				plist.setProperty("urn:b2in-eventing:id", identifier.toString());
				plist.setProperty("urn:b2in-eventing:topic", (*i)); // Subscribe to collected data
				plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
				plist.setProperty("urn:b2in-eventing:subscriberservice", "xmasstore2g");
				plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
				subscriptions_[(*i)] = plist;
			}
		}
	}

	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "subscribe/Renew");
	b2in::utils::MessengerCache * messengerCache = 0;
	try
	{
		messengerCache = b2inEventingProxy_->getMessengerCache();
	}
	catch (b2in::utils::exception::Exception & e)
	{
		XCEPT_RETHROW(xmas::store2g::exception::Exception, "cannot access messenger cache", e);
	}
	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
	// Therefore there is no loss of efficiency.
	//

	std::list < std::string > destinations = messengerCache->getDestinations();

	for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++)
	{
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		std::map<std::string, xdata::Properties>::iterator i;
		for (i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Subscribe to topic " << (*i).first << " on url " << (*j));
				messengerCache->send((*j), 0, (*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				std::stringstream msg;
				msg << "failed to send subscription for topic (internal error) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, ex, msg.str(), e);
				this->notifyQualified("fatal", ex);
				return;
			}
			catch (b2in::nub::exception::QueueFull & e)
			{
				std::stringstream msg;
				msg << "failed to send subscription for topic (queue full) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(xmas::store2g::exception::Exception, ex, msg.str(), e);
				this->notifyQualified("error", ex);
				return;
			}
			catch (b2in::nub::exception::OverThreshold & e)
			{
				// ignore just count to avoid verbosity
				std::stringstream msg;
				msg << "failed to send subscription for topic (over threshold) " << (*i).first << " to url " << (*j);
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg.str());
				return;
			}
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
	}
}

void xmas::store2g::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string name = e.getTimerTask()->name;

	//std::cout << " time expired " << name << std::endl;

	if (name == "discovery-staging")
	{

		try
		{
			b2inEventingProxy_->scan();
		}
		catch (b2in::utils::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

		try
		{
			this->refreshSubscriptionsToEventing();
		}
		catch (xmas::store2g::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

	}
}

// 
// Hyperdaq support
//

void xmas::store2g::Application::TabPanel (xgi::Output * out)
{
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// Tabbed pages
	*out << "<div class=\"xdaq-tab\" title=\"Summary\">" << std::endl;
	this->SummaryTabPage(out);
	*out << "</div>" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Metrics\">" << std::endl;
	this->MetricsTabPage(out);
	*out << "</div>" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Flashlists\">" << std::endl;
	this->FlashlistsTabPage(out);
	*out << "</div>" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Services\">" << std::endl;
	this->ServicesTabPage(out);
	*out << "</div>" << std::endl;

	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
	this->SettingsTabPage(out);
	*out << "</div>" << std::endl;

	*out << "</div>" << std::endl;
}

void xmas::store2g::Application::SettingsTabPage (xgi::Output * out)
{
	std::vector < std::string > names = settings_.propertyNames();
	if (names.size() == 0)
	{
		*out << "There are no settings to configure" << std::endl;
		return;
	}

	std::stringstream method;
	method << "/" << getApplicationDescriptor()->getURN() << "/applySettings";

	*out << cgicc::form().set("method", "GET").set("action", method.str()) << std::endl;

	*out << cgicc::table().set("class", "xdaq-table-vertical");

	for (std::vector<std::string>::iterator i = names.begin(); i != names.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::th(*i);
		*out << cgicc::td();
		*out << cgicc::input().set("type", "text").set("name", (*i)).set("value", settings_.getProperty(*i)) << std::endl;
		*out << cgicc::td();
		//*out << "Configuration type" << std::endl;
		//*out << cgicc::select().set("name","type") << std::endl;
		//*out << cgicc::option("standard")  << std::endl;
		//*out << cgicc::select()  << std::endl;
		*out << cgicc::tr();

	}
	*out << cgicc::tr().set("class", "xdaq-table-nohover");
	*out << cgicc::td("");
	*out << cgicc::td();
	*out << cgicc::input().set("type", "submit").set("value", "Apply") << std::endl;
	*out << cgicc::td();
	*out << cgicc::tr();
	*out << cgicc::table();

	*out << cgicc::form() << std::endl;
}

void xmas::store2g::Application::ServicesTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Service");
	*out << cgicc::th("Host");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	// refresh subscriptions
	std::set < std::string > engaged = toolbox::parseTokenSet(subscribeGroup_, ",");
	std::set < std::string > groups = getApplicationContext()->getDefaultZone()->getGroupNames(this->getApplicationDescriptor());

	try
	{

		std::list < std::string > destinations = b2inEventingProxy_->getMessengerCache()->getDestinations();

		for (std::list<std::string>::iterator i = destinations.begin(); i != destinations.end(); i++)
		{
			*out << cgicc::tr();
			*out << cgicc::td("b2in-eventing") << std::endl;
			*out << cgicc::td(*i) << std::endl;
			*out << cgicc::tr() << std::endl;
		}
	}
	catch (b2in::utils::exception::Exception & e)
	{
		// ignore
	}
	*out << cgicc::tbody();
	*out << cgicc::table();

	/*
	 std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_.getDescriptors(groups, "ws-eventing");
	 std::list<xdaq::ApplicationDescriptorImpl>::iterator i;
	 for (  i = descriptors.begin(); i != descriptors.end(); i++ )
	 {
	 std::string service = (*i).getProperty("service");
	 std::string host = (*i).getContextDescriptor()->getURL();
	 std::set<std::string> appGroups = toolbox::parseTokenSet((*i).getProperty("group"),",");

	 //std::cout << "engaged are " << toolbox::printTokenSet(engaged,",") << std::endl;
	 //std::cout << "application are " << toolbox::printTokenSet(appGroups,",") << std::endl;
	 //std::cout << "intersection are " << toolbox::printTokenSet(toolbox::stl::intersection(engaged, appGroups),",")
	 //<< "size is " << toolbox::stl::intersection(engaged, appGroups).size() << std::endl;

	 *out << cgicc::tr();
	 *out << cgicc::td(service).set("style","vertical-align: top; font-weight: normal; background-color: rgb(255,255,255);") << std::endl;
	 *out << cgicc::td(host).set("style","vertical-align: top; font-weight: normal; background-color: rgb(255,255,255);") << std::endl;
	 if ( toolbox::stl::intersection(engaged, appGroups).size() != 0 )
	 {
	 *out << cgicc::td("yes").set("style","vertical-align: top; font-weight: normal; background-color: rgb(0,139,0);") << std::endl;
	 }
	 else
	 {
	 *out << cgicc::td("no").set("style","vertical-align: top; font-weight: normal; background-color: rgb(255,255,255);") << std::endl;
	 }
	 *out << cgicc::tr() << std::endl;
	 }
	 */

	*out << "<br/>" << std::endl;

	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Service");
	*out << cgicc::th("Host");
	*out << cgicc::th("Engaged");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();
	engaged = toolbox::parseTokenSet(storeGroup_, ",");
	std::list < xdaq::ApplicationDescriptorImpl > descriptors = descriptorsCache_.getDescriptors(groups, "tstore");
	for (std::list<xdaq::ApplicationDescriptorImpl>::iterator j = descriptors.begin(); j != descriptors.end(); j++)
	{
		std::string service = (*j).getProperty("service");
		std::string host = (*j).getContextDescriptor()->getURL();
		std::set < std::string > appGroups = toolbox::parseTokenSet((*j).getProperty("group"), ",");

		//std::cout << "engaged are " << toolbox::printTokenSet(engaged,",") << std::endl;		
		//std::cout << "applications are " << toolbox::printTokenSet(appGroups,",") << std::endl;

		*out << cgicc::tr();
		*out << cgicc::td(service) << std::endl;
		*out << cgicc::td(host) << std::endl;
		if (toolbox::stl::intersection(engaged, appGroups).size() != 0)
		{
			*out << cgicc::td("yes").set("class", "xdaq-color-green") << std::endl;
		}
		else
		{
			*out << cgicc::td("no").set("class", "xdaq-color-red") << std::endl;
		}
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void xmas::store2g::Application::FlashlistsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable");
	*out << cgicc::th("Version");
	*out << cgicc::th("Tag");
	*out << cgicc::th("Mode");
	*out << cgicc::th("Enable");
	*out << cgicc::th("Status");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	std::list < std::string > names = storeSettings_.getFlashlistsNames();
	for (std::list<std::string>::iterator i = names.begin(); i != names.end(); i++)
	{
		xmas::store2g::Settings::Properties properties = storeSettings_.getProperties(*i);

		*out << cgicc::tr();
		*out << cgicc::td(*i) << std::endl;
		*out << cgicc::td(properties.definition->getProperty("version")) << std::endl;
		*out << cgicc::td(properties.tag) << std::endl;
		*out << cgicc::td(properties.mode) << std::endl;
		if (properties.enable)
		{
			*out << cgicc::td("true").set("class", "xdaq-color-green") << std::endl;
		}
		else
		{
			*out << cgicc::td("false").set("class", "xdaq-color-red") << std::endl;

		}

		if (properties.failed)
		{
			*out << cgicc::td("bad").set("class", "xdaq-color-red") << std::endl;
		}
		else
		{
			*out << cgicc::td("ok").set("class", "xdaq-color-green") << std::endl;

		}

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void xmas::store2g::Application::MetricsTabPage (xgi::Output * out)
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	std::stringstream method;
	method << "/" << getApplicationDescriptor()->getURN() << "/clear";
	*out << cgicc::form().set("method", "POST").set("action", method.str()) << std::endl;
	*out << cgicc::input().set("type", "submit").set("value", "Clear").set("class", "xdaq-reset-button") << std::endl;
	*out << cgicc::form() << std::endl;

	*out << cgicc::br();
	*out << cgicc::label("Last update: ");
	*out << cgicc::label(toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::loc));
	*out << cgicc::br();

	repository_.lock();

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable");
	*out << cgicc::th("Last Update");
	*out << cgicc::th("Originator");
	*out << cgicc::th("Number of Rows");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	std::set < std::string > flashlists = repository_.getFlashlists();
	for (std::set<std::string>::iterator i = flashlists.begin(); i != flashlists.end(); i++)
	{

		std::string flashlist = (*i);
		toolbox::Properties properties = repository_.getProperties(flashlist);
		xdata::Table::Reference table = repository_.getData(flashlist);
		std::string lastupdate = properties.getProperty("lastupdate");
		std::string originator = properties.getProperty("originator");
		std::stringstream rowcount;
		rowcount << table->getRowCount();
		*out << cgicc::tr();
		*out << cgicc::td(flashlist) << std::endl;
		*out << cgicc::td(lastupdate) << std::endl;
		*out << cgicc::td(originator) << std::endl;
		*out << cgicc::td(rowcount.str()) << std::endl;
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();

	repository_.unlock();
}

void xmas::store2g::Application::SummaryTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-nohover").set("style", "width: 100%;");
	*out << cgicc::tr();
	*out << cgicc::td();
	*out << cgicc::label("Last update: ");
	*out << cgicc::label(toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::loc));
	*out << cgicc::td();
	*out << cgicc::td();
	*out << cgicc::label("Report Lost: ");
	*out << cgicc::label() << reportLostCounter_ << cgicc::label();
	*out << cgicc::td();
	*out << cgicc::td();
	*out << cgicc::label("Connections: ");
	*out << cgicc::label() << tstoreProxy_.getConnectionCounter() << cgicc::label();
	*out << cgicc::td();
	*out << cgicc::tr();
	*out << cgicc::table();

	*out << cgicc::br();

	*out << cgicc::table().set("class", "xdaq-table").set("style", "width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable");
	*out << cgicc::th("Incoming reports");
	*out << cgicc::th("Insert actions");
	*out << cgicc::th("Total received rows");
	*out << cgicc::th("Total inserted rows");
	*out << cgicc::th("Last insert");
	*out << cgicc::th("Depth");
	*out << cgicc::th("Status");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	std::list < std::string > names = storeSettings_.getFlashlistsNames();
	for (std::list<std::string>::iterator i = names.begin(); i != names.end(); i++)
	{
		xmas::store2g::Settings::Properties properties = storeSettings_.getProperties(*i);

		*out << cgicc::tr();
		*out << cgicc::td(*i) << std::endl;
		*out << cgicc::td() << properties.incomingReports << cgicc::td() << std::endl;
		*out << cgicc::td() << properties.insertActions << cgicc::td() << std::endl;
		*out << cgicc::td() << properties.totalRowsReceived << cgicc::td() << std::endl;
		*out << cgicc::td() << properties.totalRowsInserted << cgicc::td() << std::endl;
		if (toolbox::TimeVal::zero() == properties.lastInsert)
		{
			*out << cgicc::td("n.a.") << std::endl;

		}
		else
		{
			*out << cgicc::td(properties.lastInsert.toString(toolbox::TimeVal::loc)) << std::endl;

		}
		*out << cgicc::td() << properties.depth << cgicc::td() << std::endl;

		if (properties.failed)
		{
			*out << cgicc::td("bad").set("class", "xdaq-color-red") << std::endl;
		}
		else
		{
			*out << cgicc::td("ok").set("class", "xdaq-color-green") << std::endl;

		}
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}
void xmas::store2g::Application::applySettings (xgi::Input * in, xgi::Output * out) 
{
	try
	{
		cgicc::Cgicc cgi(in);
		std::vector < std::string > names = settings_.propertyNames();
		for (std::vector<std::string>::iterator i = names.begin(); i != names.end(); i++)
		{
			if (xgi::Utils::hasFormElement(cgi, (*i)))
			{
				std::string value = xgi::Utils::getFormElement(cgi, (*i))->getValue();
				if (settings_.getProperty(*i) != value)
				{
					if ((*i) == "autorefresh")
					{

					}
					settings_.setProperty((*i), value);
				}
			}
		}
	}
	catch (std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	this->TabPanel(out);
}

void xmas::store2g::Application::clear (xgi::Input * in, xgi::Output * out) 
{
	repository_.lock();
	repository_.clear();
	repository_.unlock();
	this->TabPanel(out);
}

