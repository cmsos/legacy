#!/bin/sh

curl --stderr /dev/null \
     -H "SOAPAction: urn:xdaq-application:class=BU,instance=0" \
     -d "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"
  xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"
  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
<SOAP-ENV:Header>
</SOAP-ENV:Header>
<SOAP-ENV:Body>
        <psx:dpGet xmlns:psx=\"http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd\">
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam1:_online.._value\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam1:_online.._invalid\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam1:_online.._userbit1\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam1:_online.._userbit2\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam1:_online.._userbit3\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam2:_online.._value\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam2:_online.._invalid\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam2:_online.._userbit1\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam2:_online.._userbit2\"/>
                <psx:dp name=\"cms_cen_dcs_2:ExternalApps/Accelerator/LHC.RunControl.CirculatingBunchConfig.Beam2:_online.._userbit3\"/>
        </psx:dpGet>
</SOAP-ENV:Body>
</SOAP-ENV:Envelope>" http://psx-tcds:9929/urn:xdaq-application:lid=60

echo ""
