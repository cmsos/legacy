#ifndef _bril_timesource_exception_Exception_h_
#define _bril_timesource_exception_Exception_h_
#include "xcept/Exception.h"
namespace bril { 
  namespace timesource {
    namespace exception { 
      class Exception: public xcept::Exception{
      public: 
      Exception( std::string name, std::string message, std::string module, int line, std::string function ):xcept::Exception(name, message, module, line, function) {} 
      Exception( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e ):xcept::Exception(name, message, module, line, function, e) {} 
      }; 
    }//ns exception
  }//ns timesource

  XCEPT_DEFINE_EXCEPTION(timesource,Application)

  XCEPT_DEFINE_EXCEPTION(timesource,HWApplication)
  
  XCEPT_DEFINE_EXCEPTION(timesource,NBTimer)

  XCEPT_DEFINE_EXCEPTION(timesource,DataGenerator)

  XCEPT_DEFINE_EXCEPTION(timesource,SystemBoardError)

  XCEPT_DEFINE_EXCEPTION(timesource,TCDSReadError)

  XCEPT_DEFINE_EXCEPTION(timesource,TCPConnectError)
  
  XCEPT_DEFINE_EXCEPTION(timesource,UnknownError)
}//ns bril
#endif
