#include "tcds/bobr/Utils.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"

std::string
tcds::bobr::bstMasterToString(tcds::definitions::BST_MASTER const bstMaster)
{
  std::string res = "";

  switch (bstMaster)
    {
    case tcds::definitions::BST_MASTER_UNKNOWN:
      res = "unknown/none";
      break;
    case tcds::definitions::BST_MASTER_BEAM1:
      res = "beam1";
      break;
    case tcds::definitions::BST_MASTER_BEAM2:
      res = "beam2";
      break;
    default:
      XCEPT_RAISE(tcds::exception::ValueError,
                  toolbox::toString("'%d' is not a valid BST master value.",
                                    bstMaster));
      break;
    }
  return res;
}
