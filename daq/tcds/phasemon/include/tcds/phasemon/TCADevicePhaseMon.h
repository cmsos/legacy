#ifndef _tcds_phasemon_TCADevicePhaseMon_h_
#define _tcds_phasemon_TCADevicePhaseMon_h_

#include <string>
#include <vector>

#include "tcds/hwlayertca/TCADeviceBase.h"
#include "tcds/phasemon/Definitions.h"
#include "tcds/phasemon/TCADevicePhaseMon.h"

namespace tcds {
  namespace phasemon {

    /**
     * Implementation of PhaseMon functionality. The PhaseMon is based
     * on the FC7 FMC carrier board, with a TTC FMC in the L12
     * position and an 8-SFP TCDS FMC in the L8 position.
     */
    class TCADevicePhaseMon : public tcds::hwlayertca::TCADeviceBase
    {

    public:
      TCADevicePhaseMon();
      virtual ~TCADevicePhaseMon();

      // Select the reference clock used as basis for all phase
      // measurements.
      void selectRefClock(tcds::definitions::PHASEMON_REF_CLK_SRC const refClkSrc) const;
      tcds::definitions::PHASEMON_REF_CLK_SRC getSelectedRefClk() const;
      bool isPhaseMonLocked() const;
      void enablePhaseMeasurements(std::vector<tcds::definitions::PHASEMON_INPUT> connected) const;
      void disablePhaseMeasurements() const;
      bool isInputConnected(tcds::definitions::PHASEMON_INPUT const inputNum) const;
      double readPhaseMeasurement(tcds::definitions::PHASEMON_INPUT const inputNum) const;
      tcds::definitions::PHASEMON_INPUT_STATUS getPhaseMeasurementStatus(tcds::definitions::PHASEMON_INPUT const inputNum) const;

    protected:
      virtual std::string regNamePrefixImpl() const;

      virtual bool bootstrapDoneImpl() const;
      virtual void runBootstrapImpl() const;

    private:
      // Select the reference clock used as basis for all phase
      // measurements. The selection is a bit cumbersome, but it
      // matches the underlying firmware. Two switches need to be set:
      // - The master selection between taking the reference clock
      //   from the fabric (i.e, FCLKA if the rest of the software
      //   works well) and taking the reference clock from an external
      //   source.
      // - The 'external' sub-selection between taking the external
      //   reference clock from the L8 SFP8 RX or from the L12
      //   TTC-clock Lemo.
      void selectRefClock(bool const refClkFromFabric,
                          bool const externalRefClkFromLemo) const;

    };

  } // namespace phasemon
} // namespace tcds

#endif // _tcds_phasemon_TCADevicePhaseMon_h_
