package xdaq.xdesk.xtree;

public class XTreeDOMDocumentException extends Exception
{
	XTreeDOMDocumentException () { super(); }

	XTreeDOMDocumentException ( String msg )
	{
		super (msg);
	}
}
