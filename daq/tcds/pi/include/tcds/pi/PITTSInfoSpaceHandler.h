#ifndef _tcds_pi_PITTSInfoSpaceHandler_h_
#define _tcds_pi_PITTSInfoSpaceHandler_h_

#include <map>
#include <stddef.h>
#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pi {

    class PITTSInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      PITTSInfoSpaceHandler(xdaq::Application& xdaqApp,
                            tcds::utils::InfoSpaceUpdater* updater);
      virtual ~PITTSInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      std::vector<std::string> inputNames_;
      std::map<std::string, std::string> inputLabels_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PITTSInfoSpaceHandler_h_
