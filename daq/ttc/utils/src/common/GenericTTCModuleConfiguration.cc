#include "ttc/utils/GenericTTCModuleConfiguration.hh"

#include "ttc/utils/GenericTTCModule.hh"
#include "ttc/utils/ConfigurationItem.hh"
#include "ttc/utils/Utils.hh"

#include <sstream>
#include <stdexcept>
#include <cstring>


using namespace std;


ttc::GenericTTCModuleConfiguration::GenericTTCModuleConfiguration(GenericTTCModule* _ttc_module)
:
    line_number(0),
    ttc_module(_ttc_module),
    logger_(log4cplus::Logger::getInstance("GenericTTCModuleConfiguration"))
{}


ttc::GenericTTCModuleConfiguration::~GenericTTCModuleConfiguration()
{
  for (map<string, ConfigurationItem*>::iterator
      it = configuration_items.begin();
      it != configuration_items.end(); ++it)
  {
    ConfigurationItem* item = it->second;
    delete item;
  }
}


void ttc::GenericTTCModuleConfiguration::readLinesAndJoinContinuedLines(std::istream& in)
{
  size_t i = 0;
  static const size_t linesize = 501;
  char charline[linesize];
  while (in.getline(charline, linesize))
  {
    if (strlen(charline) >= (linesize - 1))
    {
      LOG4CPLUS_ERROR(
          logger_,
          "GenericTTCModuleConfiguration: "
          "Exceeded maximum length (" << dec << linesize << ") in line " << i);
    }

    string line = string(charline);
    if (line.size() > 0)
    {
      size_t begincomment = line.find_first_of('#', 0);
      if (begincomment < line.size())
      {
        line.erase(begincomment);
      }
    }
    config.push_back(line);
  }

  joinContinuedLines();
}


void ttc::GenericTTCModuleConfiguration::joinContinuedLines()
{
  int firstline = -1;
  size_t linebrk = 0;

  for (size_t i = 0; i < config.size(); ++i)
  {
    if (firstline > 0)
    {
      if (firstline != int(i) - 1)
      {
        stringstream myerr;
        myerr << "ERROR: GenericTTCModuleConfiguration: Problem in line " << i << "!";
        throw std::invalid_argument(myerr.str());
      }
      config[firstline].erase(linebrk);
      config[firstline] += (" " + config[i]);
      config[i] = "";
      if (config[firstline].find_first_of('\\', 0) < config[firstline].size())
      {
        stringstream myerr;
        myerr << "ERROR: GenericTTCModuleConfiguration: Second time in a row that a " << endl
            << "line is split using the '\\' character! line " << i;
        throw std::invalid_argument(myerr.str());
      }
      firstline = -1;
    }
    else
    {
      linebrk = config[i].find_first_of('\\', 0);
      if (linebrk < config[i].size())
      {
        firstline = int(i);
      }
    }
  }
}


void ttc::GenericTTCModuleConfiguration::processSingleLine(const string& current_line)
{
  string string_val;

  std::map<std::string, ConfigurationItem*>::iterator it;
  for (it = configuration_items.begin(); it != configuration_items.end(); ++it)
  {
    const string& command_name = it->first;

    if (FindString(current_line, command_name, string_val))
    {
      // We have found the command.
      ConfigurationItem* item = it->second;

      try {
        item->configure(line_number, current_line, command_name, string_val);
      }
      catch(xcept::Exception& e)
      {
        XCEPT_RETHROW(
            xcept::Exception,
            "Failed to configure item '" + command_name + "'", e);
      }

      return;
    }
  }

  // No command was found.
  // If this line contains something other than white space, complain.
  for (size_t i = 0; i < current_line.size(); ++i)
  {
    char c = current_line[i];

    // Blanks and tabs are ok.
    if (c != ' ' && c != '\t')
    {
      XCEPT_RAISE(xcept::Exception, "Failed to interpret configuration line");
    }
  }
}


void ttc::GenericTTCModuleConfiguration::extractSequences()
{
  // First remove all user-defined sequences.
  for (size_t k = 0; k < ttc_module->_sequences.size(); ++k)
  {
    if (!ttc_module->_sequences[k].IsPermanent())
    {
      ttc_module->_sequences.erase(ttc_module->_sequences.begin() + k, ttc_module->_sequences.end());
      break;
    }
  }

  bool inside = false; // inside a sequence?
  string string_val, varname, parname;
  for (size_t i = 0; i < config.size(); ++i)
  {
    if (config[i].size() == 0)
      continue;
    if (FindString(config[i], (varname = WORD_SEQUENCE_ADDNEW), string_val))
    {
      if (inside)
      {
        ostringstream msg;
        msg
            << "command '" << varname << "' defined with a "
            << WORD_SEQUENCE_BEGIN << "..." << WORD_SEQUENCE_END << " sequence in line " << i + 1;

        XCEPT_RAISE(xcept::Exception, msg.str());
      }
      ttc_module->AddSequence(string_val);
      config[i] = "";
    }
    else if (FindString(config[i], (varname = WORD_SEQUENCE_BEGIN), string_val))
    {
      if (inside)
      {
        ostringstream msg;
        msg
            << "Found error in line " << i + 1 << " of configuration! " << endl
            << " New sequence although we are already inside one!";

        XCEPT_RAISE(xcept::Exception, msg.str());
      }

      string name = string_val;
      inside = true;

      Sequence* mySeq = 0;

      try {
        mySeq = ttc_module->GetSequence(string_val);
      }
      catch(ttc::exception::UndeclaredSequence& e)
      {
        ostringstream msg;
        msg << "Found error in line " << i + 1 << " of configuration: "
            << "Definition of sequence '" << string_val << "', but declaration is missing";

        XCEPT_RETHROW(xcept::Exception, msg.str(), e);
      }

      mySeq->Clear();

      config[i] = ""; // remove line from further processing
      for (size_t k = i + 1; k < config.size(); ++k)
      {
        ++i;
        if (config[i].size() == 0)
          continue;
        if (FindString(config[i], (parname = WORD_SEQUENCE_END), string_val))
        {
          inside = false;
          config[i] = "";
          std::stringstream msg;
          msg << "Configured sequence '" << name << "' :" << endl;
          for (size_t ll = 0; ll < mySeq->N(); ++ll)
          {
            msg << "  " << ll << ")\t" << mySeq->Get(ll) << endl;
          }LOG4CPLUS_INFO(logger_, msg.str());
          break;
        }
        else
        {
          mySeq->PushBack(config[i]);
        }
        config[i] = "";
      }
    }
    else if (FindString(config[i], (varname = WORD_SEQUENCE_END), string_val))
    {
      ostringstream msg;
      msg
          << "Found error in line " << i + 1 << " of configuration! " << endl
          << " End sequence although none was open!";

      XCEPT_RAISE(xcept::Exception, msg.str());
    }
  }

  if (inside)
  {
    ostringstream msg;
    msg
        << "Found error in configuration! " << endl
        << " Sequence still open at EOF!";

    XCEPT_RAISE(xcept::Exception, msg.str());
  }
}
