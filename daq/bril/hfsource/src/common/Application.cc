#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"

#include "xdata/InfoSpaceFactory.h"
#include "toolbox/BSem.h"

#include "b2in/nub/Method.h"
#include "bril/hfsource/Application.h"
#include "bril/hfsource/WebUtils.h"
#include "bril/hfsource/MathUtils.h"
#include "bril/hfsource/exception/Exception.h"
#include "toolbox/squeue.h"
#include <unistd.h>
#include "uhal/log/exception.hpp"
#include "uhal/ConnectionManager.hpp"
#include "uhal/HwInterface.hpp"
#include "interface/bril/HFTopics.hh" //kati
//#include "interface/bril/CommonDataFormat.h" //kati
#include <algorithm>
//#include <TFile.h>
//#include <TH1.h>
//#include <TSystem.h>


using namespace std;
#include <iomanip>

XDAQ_INSTANTIATOR_IMPL (bril::hfsource::Application)


bril::hfsource::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::hfsource::Application::Default, "Default");
    b2in::nub::bind(this, &bril::hfsource::Application::onMessage);
    m_appInfoSpace = getApplicationInfoSpace();
    m_appDescriptor= getApplicationDescriptor();
    m_classname    = m_appDescriptor->getClassName();
    m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
    m_instanceid = 0;
    m_norbit = 0;
    //m_crateid.value_ = "100";
    m_currentfill = 0;
    m_currentrun = 0;
    m_currentls = 0;
    m_currentnibble = 0;
    hfPrevStatusTime = 0;
    hfCurrStatus = 8;
    local_flag=false;
    global_flag=false;
    forceReconfigure=false;
    BXOffset=1; //old default before HF firmware upgrade in Oct 2018
    OCBXOffset=-1; //old default before HF firmware upgrade in Oct 2018
    nReconfigure=0;
    if( m_appDescriptor->hasInstanceNumber() ){
        m_instanceid = m_appDescriptor->getInstance();
    }

    uhal::setLogLevelTo( uhal::Warning() );
    
    debugStatement<<"UHTR VERSION:  "
            <<"HCALUHTR_VERSION_MAJOR "<<HCALUHTR_VERSION_MAJOR
            <<"HCALUHTR_VERSION_MINOR "<<HCALUHTR_VERSION_MINOR
            <<"HCALUHTR_VERSION_PATCH "<<HCALUHTR_VERSION_PATCH;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    int maskPerFiber[]={3, 15, 12, 3, 15, 12, 15, 15, 15, 15, 15, 15, 3, 15, 12, 3, 15, 12, 15, 15, 15, 15, 15, 15};
    LOG4CPLUS_DEBUG(getApplicationLogger(), "fiber, chan ");
    for (size_t iFiber = 0; iFiber < nFibers; iFiber++) { // fibers

       //modifying to 4-channels
        for (size_t ch = 0; ch < nChannels; ch++) {// channels
            mask.push_back(maskPerFiber[iFiber] & 1<<ch);
            if( maskPerFiber[iFiber]>15 ) LOG4CPLUS_ERROR(getApplicationLogger(), "Mask is out of scope for fiber "<<iFiber);
            if((maskPerFiber[iFiber] & 1<<ch) == 0) {
                LOG4CPLUS_DEBUG(getApplicationLogger(), iFiber<<", "<<ch);
            }
        }
    }

    defineMonTable();

    toolbox::net::URN memurn("toolbox-mem-pool",m_classname.toString()+"_"+m_instanceid.toString()+"_mem");
    try{
        m_appInfoSpace->fireItemAvailable("signalTopic",&m_signalTopic);
        m_appInfoSpace->fireItemAvailable("uhtrAddress",&uhtrAddress);
        m_appInfoSpace->fireItemAvailable("bus",&m_bus);
        m_appInfoSpace->fireItemAvailable("topics",&m_topics);
        m_appInfoSpace->fireItemAvailable("histsToRead",&histsToRead);
        m_appInfoSpace->fireItemAvailable("orbits_per_nibble",        &orbits_per_nibble);
        m_appInfoSpace->fireItemAvailable("integration_period_nb",    &integration_period_nb);
        m_appInfoSpace->fireItemAvailable("nibbles_per_section",      &nibbles_per_section);
        m_appInfoSpace->fireItemAvailable("cms1_threshold",           &cms1_threshold);
        m_appInfoSpace->fireItemAvailable("cms2_threshold",           &cms2_threshold);
        m_appInfoSpace->fireItemAvailable("BXOffset",                 &BXOffset);
        m_appInfoSpace->fireItemAvailable("OCBXOffset",                 &OCBXOffset);
        m_appInfoSpace->fireItemAvailable("orbit_phase",              &orbit_phase);
        m_appInfoSpace->fireItemAvailable("addressTable",             &addressTable);
        m_appInfoSpace->fireItemAvailable("luts_path",                &luts_path);

        m_appInfoSpace->addListener(this, "urn:xdaq-event:setDefaultValues");

        toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
        m_memPool = m_poolFactory->createPool(memurn,allocator);
        m_appURN.fromString(m_appDescriptor->getURN());

        m_CollectAndPackageData = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_"+m_instanceid.toString()+"_CollectAndPackageData","waiting");
        m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_"+m_instanceid.toString()+"_publishing","waiting");
    }catch(xdata::exception::Exception& e){
        std::string msg("Failed to setup appInfoSpace ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        XCEPT_RETHROW(bril::hfsource::exception::Exception,msg,e);
    }catch(xcept::Exception& e){
        std::string msg("Failed to setup memory pool ");
        LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        XCEPT_RETHROW(bril::hfsource::exception::Exception,msg,e);
    }

    for(std::set<std::string>::iterator histName = histNames.begin();histName!=histNames.end(); histName++){
        m_lumi_hAvailable[*histName]=false;
        m_lumi_hOverflow[*histName]=false;
    }

    // initialize lastPublish with instantiation
    timeNow = toolbox::TimeVal::gettimeofday();
    lastPublish = timeNow.sec();
    lastConfigure = timeNow.sec();
    configuring = false;;
}

bril::hfsource::Application::~Application (){
    QueueStoreIt it;
    for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
        delete it->second;
    }
    m_topicoutqueues.clear();
}

void bril::hfsource::Application::timeExpired(toolbox::task::TimerEvent& e){
    //XDAQ version of multithreading.  Start a timer, when it ends, start the ZMQ listener.
    timeNow = toolbox::TimeVal::gettimeofday();
    if(timeNow.sec()-lastPublish>3){
        debugStatement<<"time in timeExpired "<<timeNow.sec()<<" lastPublish "<<lastPublish<<" diff "<<timeNow.sec()-lastPublish;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
    }


    if(timeNow.sec()-lastPublish>7*integration_period_secs||forceReconfigure){  //no publications in the last 7 integration periods
        Reconfigure();
    }
//  do_zmqclient();
}

void bril::hfsource::Application::defineMonTable(){
}

void bril::hfsource::Application::initializeMonTable(size_t channels){
}


void bril::hfsource::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){
    std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();
    *out << "<h2 align=\"center\"> Monitoring "<<appurl<<"</h2>";
    *out << cgicc::br();
    *out << "<p> Eventing Status</p>";
    *out <<busesToHTML();
    // update monitorables
    // m_monInfoSpacePull->lock();
    // m_monInfoSpacePull->fireItemGroupRetrieve(m_monItemListPull,this);
    // m_monInfoSpacePull->unlock();
    // *out << bril::hfsource::WebUtils::mapToHTML("Configuration",m_monConfig,"",true);
    // *out << bril::hfsource::WebUtils::mapToHTML("Run Status",m_monRunStatus,"",false);
    // *out << "<br>";
    // *out << "<p> Histogram Status</p>";
    // *out << bril::hfsource::WebUtils::xdatatableToHTML(&m_montable);
}



// What does this function do?  Called after constructor.
// Parameters from xml are read-in before this and after constructor.
// Put
void bril::hfsource::Application::actionPerformed(xdata::Event& e){
            std::stringstream message;
            message<<"BXOffset HEREEEEEEEEEEEEEEEEEEEE "<<BXOffset<<" is it ok ?";

    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
    if( e.type() == "urn:xdaq-event:setDefaultValues" ){
      initializeMonTable(1);
      // set up the timer for pushing into monInfoSpacePush
      std::string timername(m_appURN.toString()+"_monpusher");
        try{
            toolbox::TimeInterval monpushInterval(5,0);// check timer every 5 seconds
            toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername);
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate(start, this, monpushInterval, 0, timername);
        }catch(toolbox::exception::Exception& e){
            std::stringstream msg;
            msg << "failed to start timer "<<timername;
            LOG4CPLUS_ERROR(getApplicationLogger(),msg.str()+stdformat_exception_history(e));
            XCEPT_RETHROW(bril::hfsource::exception::Exception,msg.str(),e);
        }
        //
        this->getEventingBus(m_bus.value_).addActionListener(this);
        if(this->getEventingBus(m_bus.value_).canPublish()){
            std::string msg("Eventing bus is ready at setDefaultValues");
            LOG4CPLUS_DEBUG(getApplicationLogger(),msg);
        }
        if(!m_topics.value_.empty()){
            m_topiclist = toolbox::parseTokenSet(m_topics.value_,",");
        }
        if(!histsToRead.value_.empty()){
            histNames = toolbox::parseTokenSet(histsToRead.value_,",");
        }
        debugStatement<<"\nuhtrAddress.value_            "<<uhtrAddress.value_<<"\n";
        debugStatement<<std::dec<<"orbit_phase.value_            "<<orbit_phase.value_<<"\n";
        debugStatement<<"cms2_threshold.value_         "<<cms2_threshold.value_<<"\n";
        debugStatement<<"OCBXOffset.value_         "<<OCBXOffset.value_<<"\n";
        debugStatement<<"integration_period_nb.value_  "<<integration_period_nb.value_<<"\n";
        debugStatement<<"orbits_per_nibble.value_      "<<orbits_per_nibble.value_<<"\n";
        debugStatement<<"integration_period_orbits     "<<integration_period_orbits<<"\n";
        integration_period_orbits=integration_period_nb.value_ * orbits_per_nibble.value_;
        integration_period_secs=integration_period_orbits / 11245.6;
        debugStatement<<"integration_period_orbits     "<<integration_period_orbits<<"\n";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");

        for(std::set<std::string>::iterator it = m_topiclist.begin();it!=m_topiclist.end(); ++it){
            m_topicoutqueues.insert(std::make_pair(*it,new toolbox::squeue<toolbox::mem::Reference*>));
        }
    }

    uhtrShortName.clear();
    uhtrShortName=uhtrAddress.toString();
    uhtrShortName=(uhtrShortName.substr(uhtrShortName.find("hcal-uhtr"),uhtrShortName.find(":50001")));

    debugStatement<<"uhtrShortName "<<uhtrShortName;
    LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");
    crate = atoi((uhtrShortName.substr(10,2)).c_str());
    board = atoi((uhtrShortName.substr(13,2)).c_str());
}



// This is the main function where we connect to the uHTR
// Then it calls CollectAndPackageData to collect the data from the uHTR
// Finally it publishes the data
void bril::hfsource::Application::actionPerformed(toolbox::Event& e){
    LOG4CPLUS_INFO(getApplicationLogger(), "Received toolbox event " << e.type());
    if ( e.type() == "eventing::api::BusReadyToPublish" ){
        std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
        std::stringstream msg;
        msg<< "Eventing bus '" << busname << "' is ready to publish";
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());

        debugStatement<<"Connecting to "<<uhtrAddress.value_;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        Connect();
        debugStatement<<"Connected... checking setup";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        if (!CheckFirmware()){
            debugStatement<<"Could not read fw version";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
        }

        try
        {
            check_configure_uhtr();
        }
        catch(std::runtime_error & ex)
        {
            LOG4CPLUS_INFO(getApplicationLogger(), std::string("Failed load/verify LUT tables. Termination."));

            std::exit(-1);
        }

        LOG4CPLUS_INFO(getApplicationLogger(), std::string("REAL mode, start reading and publishing lumi workloops"));

        toolbox::task::ActionSignature* as_taking =
        toolbox::task::bind(this, &bril::hfsource::Application::CollectAndPackageData, "CollectAndPackageData");
        m_CollectAndPackageData->activate();
        m_CollectAndPackageData->submit(as_taking);

        toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this, &bril::hfsource::Application::publishing,"publishing");
        m_publishing->activate();
        m_publishing->submit(as_publishing);

        // FIXME TURNED OFF FOR TESTING
        //m_monInfoSpacePush->fireItemGroupChanged(m_monItemListPush,this);
    }
}


// fill the header with the lumi nibble identifying information
void bril::hfsource::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception){
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        if(topic == m_signalTopic.toString()){
            std::stringstream msg;
            if(ref!=0){
                //parse timing signal message header to get timing info
                interface::bril::DatumHead* thead = (interface::bril::DatumHead*)(ref->getDataLocation());
                unsigned int fillnum = thead->fillnum;
                unsigned int runnum = thead->runnum;
                unsigned int lsnum = thead->lsnum;
                unsigned int nbnum = thead->nbnum;
                for(std::set<std::string>::iterator it = m_topiclist.begin();it!=m_topiclist.end(); ++it){
                    // FIXME do I want to publish each channel?
                    for(unsigned int ichannel=1;ichannel<=1;++ichannel){
                        toolbox::mem::Reference* outmessage = do_generatedata(*it,fillnum,runnum,lsnum,nbnum,ichannel);
                        msg.str("");
                        msg<<"push data to "<<*it<<" ichannel "<<ichannel;
                        LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
                        m_topicoutqueues[*it]->push(outmessage);
                    }
                }
            }
        }
    }
    if(ref!=0){
        ref->release();
        ref=0;
    }
}


// This is for making fake data
toolbox::mem::Reference* bril::hfsource::Application::do_generatedata(const std::string& topic, unsigned int fillnum, unsigned int runnum,unsigned int lsnum,unsigned int nbnum, unsigned int ichannel){
    std::stringstream msg;
    msg<<"Generate data topic "<<topic<<"  run " << runnum << " ls "<<lsnum<<" nb "<<nbnum<<" channel "<<ichannel;
    LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    toolbox::mem::Reference* bufRef = 0;
    return bufRef;
}



// give the topic and publish its data.
void bril::hfsource::Application::do_publish(const std::string& busname,const std::string& topicname,toolbox::mem::Reference* bufRef){
    std::stringstream msg;
    try{
        xdata::Properties plist;
        plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
        msg<<"Publishing "<<busname<<":"<<topicname;
        LOG4CPLUS_DEBUG(getApplicationLogger(),msg.str());
        this->getEventingBus(busname).publish(topicname,bufRef,plist);
        timeNow = toolbox::TimeVal::gettimeofday();
        lastPublish = timeNow.sec();
    }catch(xcept::Exception& e){
        msg<<"Failed to publish "<<topicname<<" to "<<busname;
        LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
        if(bufRef){
            bufRef->release();
            bufRef = 0;
        }
        XCEPT_DECLARE_NESTED(bril::hfsource::exception::Exception,myerrorobj,msg.str(),e);
        this->notifyQualified("fatal",myerrorobj);
    }
}


// publish all the topics
bool bril::hfsource::Application::publishing(toolbox::task::WorkLoop* wl){
    if(configuring) {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Will not publish because configuring in progress.");
        usleep(100000);
        return true;
    }
    QueueStoreIt it;
    for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
        if(it->second->empty()) continue;
        std::string topicname = it->first;
        toolbox::mem::Reference* data = it->second->pop();
        debugStatement<<"trying to publish";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        do_publish(m_bus.value_,topicname,data);
        debugStatement<<"success!";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
    }
    usleep(100000);
    return true;
}


// collect data
bool bril::hfsource::Application::CollectAndPackageData(toolbox::task::WorkLoop* wl){
    if(configuring) {
        LOG4CPLUS_INFO(getApplicationLogger(), "Will not try to CollectAndPackageData because configuring in progress.");
        usleep(100000);
        return true;
    }

    if(!uHTR){
        LOG4CPLUS_INFO(getApplicationLogger(), "There is a null pointer to the uHTR.  Skipping CollectAndPackageData.");
        usleep(100000);
        return true;
    }

    timeNow = toolbox::TimeVal::gettimeofday();
    HFReadoutStatus();
    // getting occupancy histograms
    for(std::set<std::string>::iterator histName = histNames.begin();histName!=histNames.end(); histName++){
        xdata::Boolean myoverflow(m_lumi_hOverflow[*histName]);
        //m_montable.setValueAt(0,"Overflow",myoverflow);
    }


    int nOverFlowed=0;
    toolbox::mem::Reference* hist_ref = 0;
    toolbox::TimeVal timeVal = toolbox::TimeVal::gettimeofday();
    int topicSize=0;
    std::string topicName;

    unsigned int nAvailable=0;
    for(std::set<std::string>::iterator histName = histNames.begin();histName!=histNames.end(); histName++){
        if(m_lumi_hAvailable[*histName]) nAvailable++;
    }

    if(nAvailable!=0 && nAvailable!=histNames.size()) {
        debugStatement<<"Assumption of all available or none available is wrong:  "<<nAvailable<<" available!";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        HFReadoutStatus();
        nAvailable=0;
        for(std::set<std::string>::iterator histName = histNames.begin();histName!=histNames.end(); histName++){
            if(m_lumi_hAvailable[*histName]) nAvailable++;
        }
        debugStatement<<"After trying again:  "<<nAvailable<<" available!";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");

        if(nAvailable!=histNames.size()) {
            usleep(100000);
            return true;
        }
    }

    memset(occ1Data   ,0,sizeof(occ1Data));
    memset(occ2Data   ,0,sizeof(occ2Data));
    memset(sumETData  ,0,sizeof(sumETData));
    memset(validData  ,0,sizeof(validData));

    unsigned int * thisData;

    for(std::set<std::string>::iterator histName = histNames.begin();histName!=histNames.end(); histName++){
        if(!m_lumi_hAvailable[*histName]) continue;

        debugStatement<<*histName<<" histo available";
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");

        xdata::Boolean myavailable(m_lumi_hAvailable[*histName]);
        //m_montable.setValueAt(0,"Available",myavailable);

        hcal::uhtr::uHTR::LumiHistogram uhtrLumiHist;
        try{
            uHTR->lumi_read_histogram(*histName, uhtrLumiHist);
            debugStatement<<std::dec<<"Fill, Run, LS, Nibble, Crate, Board "<< uhtrLumiHist.lhc_fill
                <<", "<<uhtrLumiHist.cms_run
                <<", "<<uhtrLumiHist.lumi_section
                <<", "<<uhtrLumiHist.lumi_nibble
                <<", "<<crate
                <<", "<<board<<"";
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
        } catch(uhal::exception::exception& e) {
            std::stringstream message;
            message<<"Failed to lumi_read_histogram "<<*histName<<" in "<<crate<<","<<board<<" uHTR... trying again";
            LOG4CPLUS_INFO(getApplicationLogger(),message.str());
            try{
                uHTR->lumi_read_histogram(*histName, uhtrLumiHist);
                debugStatement<<std::dec<<"Fill, Run, LS, Nibble  "<< uhtrLumiHist.lhc_fill
                    <<", "<<uhtrLumiHist.cms_run
                    <<", "<<uhtrLumiHist.lumi_section
                    <<", "<<uhtrLumiHist.lumi_nibble;
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
            } catch(uhal::exception::exception& e) {
                debugStatement<<"Failed to lumi_read_histogram "<<*histName<<" in "<<crate<<","<<board<<" uHTR... twice!";
                LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
                debugStatement.str("");
                continue;
            }
        }



        if(m_lumi_hOverflow[*histName]){
            std::stringstream message;
            message<<"Overflow... skipping..."<<*histName;
            LOG4CPLUS_INFO(getApplicationLogger(),message.str());
            nOverFlowed++;
            continue;
        }

        std::vector<uint32_t> histo = uhtrLumiHist.h;
        //FIXME should I institute some quality checks here?
        // check to see if board out of phase with previous lumi block
        if( ((uhtrLumiHist.lumi_nibble-m_currentnibble)%integration_period_nb.value_)!=0 && m_currentnibble!=0
          && (timeNow.sec()-lastConfigure)>integration_period_secs*8){ // ignore three blocks after reconfiguration
            debugStatement<<"This nib and prev nib are not aligned modulo the integraion period.  "
                <<uhtrLumiHist.lumi_nibble<<" "<<m_currentnibble;
            debugStatement<<"\n integration_period_nb.value_"<<integration_period_nb.value_<<" "<<(uhtrLumiHist.lumi_nibble-m_currentnibble)%integration_period_nb.value_;
            debugStatement<<"\n FORCE RECONFIGURE next timer";
            forceReconfigure=true;
            LOG4CPLUS_INFO(getApplicationLogger(),"");
        }

        m_currentfill   = uhtrLumiHist.lhc_fill;
        m_currentrun    = uhtrLumiHist.cms_run;
        m_currentls     = uhtrLumiHist.lumi_section;
        m_currentnibble = uhtrLumiHist.lumi_nibble;
        m_histotype     = uhtrLumiHist.type;

        if(histo.size()!=(unsigned int)interface::bril::MAX_NBX) {
            std::stringstream message;
            message<<"size of histo "<<histo.size();
            LOG4CPLUS_INFO(getApplicationLogger(),message.str());
        }

        bool sane=SanityChecks();

        if(!sane) {
            timeNow = toolbox::TimeVal::gettimeofday();
            lastPublish=timeNow.sec();
            LOG4CPLUS_INFO(getApplicationLogger(), "Choosing not to publish and advancing the lastPublish time");
            continue;
        }

        if(histo.size()==0){
            continue;
        }

        timeNow = toolbox::TimeVal::gettimeofday();
        if(timeNow.sec()-lastConfigure<integration_period_secs*2){ // ignore two blocks after reconfiguration
            std::stringstream message;
            message<<"Reconfigured a few seconds ago... drop these histograms.";
            LOG4CPLUS_INFO(getApplicationLogger(),message.str());
            continue;
        }

        /// FIXME need to add mask for abort gap
        if(*histName=="CMS1"){
            thisData=occ1Data;
            topicSize=interface::bril::hfCMS1T::maxsize();
            topicName=interface::bril::hfCMS1T::topicname();
        }else if(*histName=="CMS2"){
            thisData=occ2Data;
            topicSize=interface::bril::hfCMS2T::maxsize();
            topicName=interface::bril::hfCMS2T::topicname();
        }else if(*histName=="CMS_ET"){
            thisData=sumETData;
            topicSize=interface::bril::hfCMS_ETT::maxsize();
            topicName=interface::bril::hfCMS_ETT::topicname();
        }else if(*histName=="CMS_VALID"){
            thisData=validData;
            topicSize=interface::bril::hfCMS_VALIDT::maxsize();
            topicName=interface::bril::hfCMS_VALIDT::topicname();
        }else{
            std::stringstream message;
            message<<"Unknown topic:"<<*histName<<"  Skipping...";
            LOG4CPLUS_INFO(getApplicationLogger(),message.str());
            continue;
        }

        hist_ref = m_poolFactory->getFrame(m_memPool,topicSize);
        hist_ref->setDataSize(topicSize);
        interface::bril::DatumHead* datumHead = (interface::bril::DatumHead*)(hist_ref->getDataLocation());
        datumHead->setTime(m_currentfill,m_currentrun,m_currentls,m_currentnibble,timeVal.sec(),timeVal.millisec());
        debugStatement<<"topicName "<<topicName<<" topicSize "<<topicSize;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        datumHead->setTotalsize(topicSize);
        debugStatement<<"crate, board "<<crate<<","<<board;
        LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        datumHead->setResource(crate, 0, board, interface::bril::StorageType::UINT32);
        unsigned int brildaqHist[interface::bril::MAX_NBX]; //need to be unsigned int! because you declared so in the dataformat.
        int length=std::min(interface::bril::MAX_NBX,(int)histo.size());
        int hfetOffset = BXOffset;
        int hfocOffset = OCBXOffset;

        for(int ibx=0; ibx<length; ibx++){
            if(*histName=="CMS_ET"){
                brildaqHist[(ibx+hfetOffset)%3564]=histo[ibx];
                *(thisData+((ibx+hfetOffset)%3564))=histo[ibx];
            } else {
                brildaqHist[ibx]=histo[ibx];
                *(thisData+ibx)=histo[ibx];
            }
        }

        if(length<interface::bril::MAX_NBX){
            for(int ibx=length; ibx<interface::bril::MAX_NBX; ibx++){
                if(*histName=="CMS_ET"){
                    brildaqHist[(ibx+hfetOffset)%3564]=0;
                    *(thisData+((ibx+hfetOffset)%3564))=0;
                } else {
                    brildaqHist[ibx]=0;
                    *(thisData+ibx)=0;
                }
            }
        }

        for(int ibx=0; ibx<length; ibx++){
            if(*histName=="CMS1"){
                brildaqHist[(ibx+hfocOffset)%3564]=histo[ibx];
                *(thisData+((ibx+hfocOffset)%3564))=histo[ibx];
            } else {
                brildaqHist[ibx]=histo[ibx];
                *(thisData+ibx)=histo[ibx];
            }
        }

        if(length<interface::bril::MAX_NBX){
            for(int ibx=length; ibx<interface::bril::MAX_NBX; ibx++){
                if(*histName=="CMS1"){
                    brildaqHist[(ibx+hfocOffset)%3564]=0;
                    *(thisData+((ibx+hfocOffset)%3564))=0;
                } else {
                    brildaqHist[ibx]=0;
                    *(thisData+ibx)=0;
                }
            }
        }

        //std::cout<<"datumHead->payloadsize() "<<datumHead->payloadsize()<<std::endl;

        memcpy(datumHead->payloadanchor,brildaqHist,datumHead->payloadsize());
        m_topicoutqueues[topicName]->push(hist_ref);
    }

    ///toolbox::mem::Reference* rawHistBufRef = 0;
    ///// FOR COMPOUND DATA TYPE
    ///std::string pdict = interface::bril::hfrawT::payloaddict();
    ///xdata::Properties plist;
    ///plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
    ///plist.setProperty("PAYLOAD_DICT",pdict);
    ///// END COMPOUND-SPECIFIC STUFF
    ///
    ///rawHistBufRef = m_poolFactory->getFrame(m_memPool,interface::bril::hfrawT::maxsize());
    ///rawHistBufRef->setDataSize(interface::bril::hfrawT::maxsize());
    ///
    ///// fill, run, ls, ln, sec, msec
    ///interface::bril::DatumHead* rawHistHeader= (interface::bril::DatumHead*)(rawHistBufRef->getDataLocation());
    ///rawHistHeader->setTime(m_currentfill,m_currentrun,m_currentls,m_currentnibble,timeVal.sec(),timeVal.millisec());
    ///rawHistHeader->setFrequency(integration_period_nb.value_);
    ///
    ///// sourceid, algo, channel, payloadtype
    ///rawHistHeader->setResource(interface::bril::DataSource::HF,NULL,NULL,interface::bril::StorageType::COMPOUND);
    ///rawHistHeader->setTotalsize(interface::bril::hfrawT::maxsize());
    ///
    ///// MORE COMPOUND-SPECIFIC STUFF
    ///interface::bril::CompoundDataStreamer tc(pdict);
    ///tc.insert_field( rawHistHeader->payloadanchor, "global",  &global_flag);
    ///tc.insert_field( rawHistHeader->payloadanchor, "local",   &local_flag );
    ///tc.insert_field( rawHistHeader->payloadanchor, "status",  &hfCurrStatus);
    ///tc.insert_field( rawHistHeader->payloadanchor, "crate",   &crate );
    ///tc.insert_field( rawHistHeader->payloadanchor, "board",   &board );
    ///tc.insert_field( rawHistHeader->payloadanchor, "occ1",    occ1Data);
    ///tc.insert_field( rawHistHeader->payloadanchor, "occ2",    occ2Data);
    ///tc.insert_field( rawHistHeader->payloadanchor, "sumet",   sumETData);
    ///tc.insert_field( rawHistHeader->payloadanchor, "valid",   validData);
    ///// END MORE COMPOUND-SPECIFIC STUFF
    ///
    ///topicName=interface::bril::hfrawT::topicname();
    ///m_topicoutqueues[topicName]->push(rawHistBufRef);

    usleep(100000);
    return true;
}


bool bril::hfsource::Application::SanityChecks(){
    bool sane=true;

    std::stringstream message;

    if(m_currentfill<0 || m_currentfill>9999
     || m_currentrun<0 || m_currentrun>999999
     || m_currentls<0 || m_currentls>999999
     || m_currentnibble<0 || m_currentnibble>64){
        sane=false;
        message<<"Data may be corrupted: fill,run,ls,ln,crate,board "<<m_currentfill<<" "<<m_currentrun<<" "<<m_currentls<<" "<<m_currentnibble<<" "<<crate<<" "<<board;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());
    }

    if(hfCurrStatus>3){
        sane=false;
        message<<"In a local run:  hfCurrStatus "<<hfCurrStatus<<" "<<m_currentfill<<" "<<m_currentrun<<" "<<m_currentls<<" "<<m_currentnibble<<" "<<crate<<" "<<board;
        LOG4CPLUS_INFO(getApplicationLogger(), message.str());
    }

    return sane;
}


bool bril::hfsource::Application::Reconfigure(){
    bool linkReset=false;
    bool uHTRSetup=false;

    timeNow = toolbox::TimeVal::gettimeofday();
    if(timeNow.sec()-lastConfigure<integration_period_secs*5){  // need to give it a chance
        debugStatement<<"Don't reconfigure... it has only be "<<timeNow.sec()-lastConfigure<<" seconds.\n";
        LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
        debugStatement.str("");
        return false;
    }

    configuring = true;
    usleep(50000); // wait to reconfigure so that no other process can use uHTR
    if(timeNow.sec()-lastPublish>integration_period_secs*16){  // need to get serious
        try{
            debugStatement<<"Trying deep RECONFIGURE:  Disconnect/Connect "<<crate<<","<<board;
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
            Disconnect();
            Connect(); //includes link reset and setup
            uint32_t status, errors;
            float lumi_bc0, trig_bc0;
            try{
                uHTR->lumi_link_status(0, status, lumi_bc0, trig_bc0, errors); // 0 = HF, 1 = BHM
            } catch(uhal::exception::exception& e) {
                LOG4CPLUS_INFO(getApplicationLogger(),"Failed to lumi_link_status in uHTR");
            }

            if (status == 0x3F) linkReset=true;
            if(uHTR!=0) uHTRSetup=true;

            debugStatement<<"CheckFirmware() "<<CheckFirmware();
            LOG4CPLUS_DEBUG(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
        } catch(uhal::exception::exception& e) {
            debugStatement<<"Failed in connect "<<crate<<","<<board;
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
        }
    } else { // normal re-configure
        try{
            LOG4CPLUS_INFO(getApplicationLogger(), "RECONFIGURE resetting link");
            uHTR->lumi_link_reset();
            usleep(50000); // 50ms
            uint32_t status, errors;
            float lumi_bc0, trig_bc0;
            try{
                uHTR->lumi_link_status(0, status, lumi_bc0, trig_bc0, errors); // 0 = HF, 1 = BHM
            } catch(uhal::exception::exception& e) {
                LOG4CPLUS_INFO(getApplicationLogger(),"Failed to lumi_link_status in uHTR");
            }

            if (status != 0x3F){
                try{
                    //throw std::runtime_error("lumi link not happy");
                    LOG4CPLUS_INFO(getApplicationLogger(),"lumi link not happy--trying again");
                    uHTR->lumi_link_reset();
                    usleep(50000); // 50ms
                    uHTR->lumi_link_status(0, status, lumi_bc0, trig_bc0, errors); // 0 = HF, 1 = BHM
                    if (status != 0x3F){
                        LOG4CPLUS_INFO(getApplicationLogger(),"lumi link STILL not happy");
                    }
                } catch(uhal::exception::exception& e) {
                    LOG4CPLUS_INFO(getApplicationLogger(),"Failed to lumi_link_reset OR lumi_link_status in uHTR AGAIN");
                }
            }else{
                linkReset=true;
            }
        } catch(uhal::exception::exception& e) {
            debugStatement<<"Failed to lumi_link_reset in uHTR "<<crate<<","<<board;
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
        }

        try{
            LOG4CPLUS_INFO(getApplicationLogger(),"RECONFIGURE lumi setup!");
            uHTR->lumi_setup(orbit_phase.value_,
                             integration_period_nb,
                             cms1_threshold.value_,
                             cms1_threshold.value_,
                             cms2_threshold.value_,
                             mask,
                             false); // bool disable_ln_bcast
            uHTRSetup=true;
        } catch(uhal::exception::exception& e) {
            debugStatement<<"Failed to lumi_setup in uHTR "<<crate<<","<<board;
            LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
            debugStatement.str("");
        }
    }
    configuring = false;
    forceReconfigure=false;

    if(!linkReset){
        LOG4CPLUS_INFO(getApplicationLogger(), "Link not reset in Reconfigure()");
    }

    if(!uHTRSetup){
        LOG4CPLUS_INFO(getApplicationLogger(), "uHTR not setup in Reconfigure()");
    }

    nReconfigure++;

    debugStatement<<"nReconfigure "<<nReconfigure
                <<", crate "<<crate
                <<", board "<<board;
    LOG4CPLUS_INFO(getApplicationLogger(), debugStatement.str());
    debugStatement.str("");

    if(linkReset&&uHTRSetup) lastConfigure=timeNow.sec();
    return (linkReset&&uHTRSetup);
}
//
// Connections
//

void bril::hfsource::Application::Connect(){
    uhal::HwInterface hw = uhal::ConnectionManager::getDevice(uhtrShortName, uhtrAddress.value_, addressTable.value_);
    std::stringstream msg;
    try {
        msg<<"Connecting to uHTR";
        uHTR = new hcal::uhtr::uHTR(uhtrShortName,hw);
    } catch(uhal::exception::exception& e) {
        msg<<"Failed to instantiate uHTR at "<<uhtrShortName<<" "<<e.what();
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
        msg.str("");
        unsigned int counter=1;
        while(!uHTR){
            usleep(30000000); //wait 30 seconds
            try{
                uHTR = new hcal::uhtr::uHTR(uhtrShortName,hw);
            } catch(uhal::exception::exception& e) {
                counter++;
                msg<<"Failed to instantiate uHTR for the "<<counter<<" times.";
            }
            LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
            msg.str("");
        }
        LOG4CPLUS_INFO(getApplicationLogger(),"Finally successfully instantiated new uHTR");
    }

    msg<<"Resetting link to uHTR";
    try{
        uHTR->lumi_link_reset();
    } catch(uhal::exception::exception& e) {
        msg<<"Failed to lumi_link_reset in uHTR "<<crate<<","<<board;
    }
    usleep(50000); // 50ms
    try{
        // run setup initially
      uHTR->lumi_setup(orbit_phase.value_,
                       integration_period_nb,
                       cms1_threshold.value_,
                       cms1_threshold.value_,
                       cms2_threshold.value_,
                       mask,
                       false); // bool disable_ln_bcast
    } catch(uhal::exception::exception& e) {
        msg<<"Failed to lumi_setup in uHTR "<<crate<<","<<board;
    }
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
}

void bril::hfsource::Application::Disconnect(){
    try{
        delete uHTR; uHTR=0;
        LOG4CPLUS_INFO(getApplicationLogger(), "Deleted uHTR and set pointer to 0.");
    }catch(uhal::exception::exception& e){
        LOG4CPLUS_INFO(getApplicationLogger(), "Couldn't delete uHTR... setting pointer to 0.");
        uHTR=0;
    }
}


bool bril::hfsource::Application::CheckFirmware(){
    bool front=false;
    bool success=false;
    uint8_t flavorF,majorF,minorF,patchF;
    int major,flavor,minor,patch;
    uHTR->firmwareRevision(front, flavorF,majorF,minorF,patchF);
    flavor = flavorF;
    major = majorF;
    minor = minorF;
    patch = patchF;
    //if you readout 0s that means that we could not communicate with the card
    std::stringstream msg;
    msg<<"CheckFirmware \n";
    if (flavor ==0 && major ==0 && minor ==0 && patch ==0){
        msg<<"did not readout fw => did not communicate with the card \n";
    } else {
        msg<<"I was able to read fw version of the card \n";
        msg<<"flavorF="<<flavor<<"\n";
        msg<<"majorF="<< major <<"\n";
        msg<<"minorF="<< minor <<"\n";
        msg<<"patchF="<< patch ;
        std::stringstream ss;
        ss << flavor<<"."<<major<<"."<<minor<<"."<<patch ;
        m_applock.take();
        m_firmwareversion = ss.str();
        m_applock.give();
        success=true;
    }
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    return success;
}



int bril::hfsource::Application::lumi_programlut(){
    // Sets up lookup tables.
    // Returns number of reprogrammed LUTs.
    std::vector<uint16_t> lut[nFibers][nChannels];

    // LUTs from file vs 1-to-1 LUTs (1 ADC count = 1 energy unit)
    if (luts_path.value_.size() > 0) {
        LOG4CPLUS_DEBUG(getApplicationLogger(),"trying to read in LUTs....");
        if (readlut(lut)) {
            LOG4CPLUS_ERROR(getApplicationLogger()," failed to get LUTs from file...");
            return -1; // failed to get LUTs from file
        }
    } else {
        for (unsigned int iFiber = 0; iFiber < nFibers; ++iFiber)
            for (unsigned int iChan = 0; iChan < nChannels; ++iChan)
                for (uint16_t i = 0; i < nElements; ++i)
                    lut[iFiber][iChan].push_back(i);
    }

    LOG4CPLUS_DEBUG(getApplicationLogger(),"read  LUTs alright....");
    
    // number of reprogrammed LUTs

    int num_prog = 0;

    for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++) { // fibers
        for (unsigned int iChan = 0; iChan < nChannels; iChan++) { // channels
            // get currently used LUT
            bool lut_invalid = false;
            std::vector<uint16_t> curr_lut;
            try{
                uHTR->lumi_read_lut(iFiber, iChan, curr_lut);
            } catch(uhal::exception::exception& e) {
                std::stringstream msg;
                msg<<"Failed to lumi_read_lut "<<crate<<","<<board<<" uHTR\n";
                msg<<"\t\tiFiber, iChan:  "<<iFiber<<", "<<iChan;
                LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
                continue;
            }

            // verify current LUT
            if (curr_lut.size() == lut[iFiber][iChan].size()) {
                for (size_t ilut = 0; ilut < lut[iFiber][iChan].size(); ilut++) {
                    if (curr_lut[ilut] != lut[iFiber][iChan][ilut]) {
                        lut_invalid = true;
                        break;
                    }
                }
            } else {
                lut_invalid = true;
            }

            LOG4CPLUS_DEBUG(getApplicationLogger(),"next we perform LUT reconfiguration....");

            // perform LUT reconfiguration then necessary
            if (lut_invalid) {
                num_prog++;
                try{
                    uHTR->lumi_program_lut(iFiber, iChan, lut[iFiber][iChan]);
                } catch(uhal::exception::exception& e) {
                    std::stringstream msg;
                    msg<<"Failed to lumi_program_lut "<<crate<<","<<board<<" uHTR\n";
                    msg<<"\t\tiFiber, iChan:  "<<iFiber<<", "<<iChan;
                    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
                    continue;
                }


                // re-verify
                curr_lut.clear();
                try{
                    LOG4CPLUS_INFO(getApplicationLogger(),"read luts to verify....");
                    uHTR->lumi_read_lut(iFiber, iChan, curr_lut);
                    LOG4CPLUS_INFO(getApplicationLogger(),"exiting this lumi_read_lut....");

                } catch(uhal::exception::exception& e) {
                    std::stringstream msg;
                    msg<<"Failed to lumi_read_lut2 "<<crate<<","<<board<<" uHTR\n";
                    msg<<"\t\tiFiber, iChan:  "<<iFiber<<", "<<iChan;
                    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
                    continue;
                }
                if (curr_lut.size() != lut[iFiber][iChan].size()) {
                    LOG4CPLUS_ERROR(getApplicationLogger()," problem with the size of curr_lut...");
                    return -1;
                }

                std::stringstream msg;
                
                msg << "PRINTING LUT Crate # " << crate <<" board # " << board << " fiber # " << iFiber << " channel # " << iChan << " : " << std::setw(4);

                for (size_t ilut = 0; ilut < lut[iFiber][iChan].size(); ilut++) {
                    if (curr_lut[ilut] != lut[iFiber][iChan][ilut]) {
                        LOG4CPLUS_ERROR(getApplicationLogger(),"Item in LUT fails verification:  "<<ilut<<" in uHTR "<<curr_lut[ilut]<<"  in hfsource "<<lut[iFiber][iChan][ilut]);
                        return -1;
                    }
                }

                LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
            }
        }
    }
    //m_applock.take();

    std::string mmm; mmm = "LUTs are loaded from the file: " + luts_path.value_ + " verified.";

    LOG4CPLUS_INFO(getApplicationLogger(),mmm);

    //m_lut.value_ = ss.str();
    //m_applock.give();
    return num_prog;
}


void bril::hfsource::Application::HFReadoutStatus(){
    // lumi histogram status
    m_applock.take();
    m_lumi_hAvailable.clear();
    m_lumi_hOverflow.clear();
    m_applock.give();
    timeNow=toolbox::TimeVal::gettimeofday();
    try{
        uHTR->lumi_histo_status(m_lumi_hAvailable,m_lumi_hOverflow);
    }catch(uhal::exception::exception& e) {
        LOG4CPLUS_INFO(getApplicationLogger(),"failed to lumi_histo_status");
        hfPrevStatusTime=timeNow.sec();
        hfCurrStatus=7;
        return;
    }

    // HF status
    local_flag=false;
    global_flag=false;
    bool obtainedStatus=false;

    try{
        uHTR->lumi_histo_status(m_lumi_hAvailable,m_lumi_hOverflow);
        obtainedStatus=true;
    } catch(uhal::exception::exception& e) {
        std::stringstream msg;
        msg<<"Failed to get histo status from uHTR "<<crate<<","<<board<<" uHTR";
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    }

    try{
        if(obtainedStatus){
            uHTR->lumi_run_bit_read(local_flag,global_flag);
            obtainedStatus=true;
        }
    } catch(uhal::exception::exception& e) {
        std::stringstream msg;
        msg<<"Failed to get status flags from uHTR "<<crate<<","<<board<<" uHTR";
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    }

    if(!obtainedStatus){
        hfPrevStatusTime=timeNow.sec();
        hfCurrStatus=6;
        return;
    }


    //change flag when something is different
    // AND time stamp is later
    if(timeNow.sec()>hfPrevStatusTime){
        unsigned short thisStatus=0;
        if(global_flag && !local_flag){
            // everything should be great
            thisStatus=0;
        } else if(!global_flag && local_flag){
            // a local run is going on -- data will be unreliable
            thisStatus=4;
        } else if(global_flag && local_flag){
            thisStatus=5;
            LOG4CPLUS_INFO(getApplicationLogger(),"HF is in global and local at the same time.  Call the HCALDOC ASAP.");
        } else if(!global_flag && !local_flag){
            // this is probably fine...
            // less reliable after a local run
            if(hfCurrStatus<=1){
                thisStatus=1;
            } else if(hfCurrStatus>=3){
                thisStatus=3;
            } else {
            // unknown previous status
                thisStatus=2;
            }
        }

        std::stringstream msg;
        if(thisStatus!=hfCurrStatus){
            hfPrevStatusTime=timeNow.sec();
            hfPrevStatus=hfCurrStatus;
            hfCurrStatus=thisStatus;
            msg<<"HFReadoutStatus():  Status change:  global, local, curStatus, prevStatus, timePrevStatus\n";
            msg<<"HFReadoutStatus():  "<<global_flag<<" "<<local_flag<<" "<<hfCurrStatus<<" "<<hfPrevStatus<<" "<<hfPrevStatusTime;
            if(hfCurrStatus==5) msg<<"\nHF is in global and local at the same time.  Call the HCALDOC ASAP.";
            LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
        }
    }

    if(hfCurrStatus<0 || hfCurrStatus>5){
        std::stringstream msg;
        msg<<"HFReadoutStatus():  Unknown status:  global, local, curStatus, prevStatus, timePrevStatus\n";
        msg<<"HFReadoutStatus():  "<<global_flag<<" "<<local_flag<<" "<<hfCurrStatus<<" "<<hfPrevStatus<<" "<<hfPrevStatusTime;
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
    }
}


//_____________________________________________________________________________________________
bool bril::hfsource::Application::check_configure_uhtr(){
    // Verifies uHTR configuration and reconfigures it if necessary.
    // Returns true if any reconfiguration steps were actually performed.

            
    LOG4CPLUS_INFO(getApplicationLogger()," passed 0 check in check_configure_uhtr....");
    bool checked=false;

    int num_prog = lumi_programlut();
    if (num_prog < 0)
        throw std::runtime_error("LUT programming failed");

    
    // int local_orbit_phase, local_n_lhc_orb, local_n_cms_orb, local_lhc_threshold, local_cms1_threshold, local_cms2_threshold;
    int local_orbit_phase, local_integration_period_nb, local_lhc_threshold, local_cms1_threshold, local_cms2_threshold,BXOffset,OCBXOffset;
    bool local_disable_ln_bcast;
    std::vector<bool> channel_mask;

    try{
        uHTR->lumi_read_setup(local_orbit_phase, local_integration_period_nb, local_lhc_threshold,
                              local_cms1_threshold, local_cms2_threshold, channel_mask, local_disable_ln_bcast);
        checked=true;
    } catch(uhal::exception::exception& e) {
        LOG4CPLUS_INFO(getApplicationLogger(),"Failed to lumi_read_setup from uHTR");
        //Reconfigure();
    }

    std::stringstream msg;
    // msg<<"Pre-setup configuration: orbit_phase="<<local_orbit_phase<<" n_lhc_orb="<<local_n_lhc_orb<<" n_cms_orb="<<
    //     local_n_cms_orb<<" lhc_threshold="<<local_lhc_threshold<<" cms1_threshold="<<local_cms1_threshold<<" cms2_threshold="<<
    //     local_cms2_threshold;
    msg<<"Pre-setup configuration: orbit_phase="<<local_orbit_phase<<" integration_period_nb="<<local_integration_period_nb<<
      " lhc_threshold="<<local_lhc_threshold<<" cms1_threshold="<<local_cms1_threshold<<" cms2_threshold="<<
      local_cms2_threshold;
    LOG4CPLUS_INFO(getApplicationLogger(),msg.str());

    bool setup_valid = false;
    // check current settings
    //   FIXME - NEED TO THINK ABOUT HOW SETTINGS ARE PASSED ALONG
    //      AND HOW TO DEAL WTIH MULTIPLE UHTRs
    // if (    local_n_lhc_orb       == integration_period_orbits &&
    //         local_n_cms_orb       == integration_period_orbits &&
    //         local_lhc_threshold   == cms1_threshold.value_ &&
    //         local_cms1_threshold  == cms1_threshold.value_ &&
    //         local_cms2_threshold  == cms2_threshold.value_ &&
    //         local_orbit_phase          == orbit_phase.value_)
    //  //       channel_mask.size() == self->cfg.mask[uHTR_id].size())
    //     setup_valid = true;

    if (    local_integration_period_nb == integration_period_orbits &&
            local_lhc_threshold         == cms1_threshold.value_ &&
            local_cms1_threshold        == cms1_threshold.value_ &&
            local_cms2_threshold        == cms2_threshold.value_ &&
            local_orbit_phase           == orbit_phase.value_)
      //       channel_mask.size() == self->cfg.mask[uHTR_id].size())
      {
        setup_valid = true;
      }


    //// check masks
    //if (setup_valid) {
    //   for (size_t i = 0; i < channel_mask.size(); i++)
    //      if (channel_mask[i] != self->cfg.mask[uHTR_id][i]) {
    //         setup_valid = false;
    //         break;
    //      }
    // }


    if (!setup_valid) {
        msg<< "Setup invalid:  uHTR->lumi_setup required";
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
        msg.str("");

        try{
          uHTR->lumi_setup(orbit_phase.value_,
                           integration_period_nb,
                           cms1_threshold.value_,
                           cms1_threshold.value_,
                           cms2_threshold.value_,
                           mask,
                           false); // bool disable_ln_bcast
            // msg<<"Post-lumi_setup configuration: orbit_phase="<<local_orbit_phase<<" n_lhc_orb="<<local_n_lhc_orb<<" n_cms_orb="<<
            //     local_n_cms_orb<<" lhc_threshold="<<local_lhc_threshold<<" cms1_threshold="<<local_cms1_threshold<<" cms2_threshold="<<
            //     local_cms2_threshold;
          msg<<"Post-lumi_setup configuration: orbit_phase="<<local_orbit_phase<<
            " integration_period_nb="<<local_integration_period_nb<<
            " lhc_threshold="<<local_lhc_threshold<<" cms1_threshold="<<local_cms1_threshold<<
            " cms2_threshold="<<local_cms2_threshold;
        } catch(uhal::exception::exception& e) {
            msg<<"Failed to lumi_setup in uHTR";
            //Reconfigure();
        }
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
        msg.str("");
    }


    if (num_prog > 0 || !setup_valid) {
        try{
            uHTR->lumi_link_reset();
            usleep(50000); // 50ms
        } catch(uhal::exception::exception& e) {
            LOG4CPLUS_INFO(getApplicationLogger(),"Failed to lumi_link_reset in uHTR");
            //Reconfigure();
        }
    }

    // check whether lumi link is happy
    uint32_t status, errors;
    float lumi_bc0, trig_bc0;
    try{
        uHTR->lumi_link_status(0, status, lumi_bc0, trig_bc0, errors); // 0 = HF, 1 = BHM
    } catch(uhal::exception::exception& e) {
        LOG4CPLUS_INFO(getApplicationLogger(),"Failed to lumi_link_status in uHTR");
    }

    if (status != 0x3F){
        try{
            //throw std::runtime_error("lumi link not happy");
            LOG4CPLUS_INFO(getApplicationLogger(),"lumi link not happy--trying again");
            uHTR->lumi_link_reset();
            usleep(50000); // 50ms
            uHTR->lumi_link_status(0, status, lumi_bc0, trig_bc0, errors); // 0 = HF, 1 = BHM
            if (status != 0x3F){
                LOG4CPLUS_INFO(getApplicationLogger(),"lumi link STILL not happy");
            }
        } catch(uhal::exception::exception& e) {
            LOG4CPLUS_INFO(getApplicationLogger(),"Failed to lumi_link_reset OR lumi_link_status in uHTR AGAIN");
        }
    }
            
    LOG4CPLUS_DEBUG(getApplicationLogger(),"passed all checks in check_configure_uhtr");
    // return false if no reconfiguration was performed
    return (num_prog > 0 || !setup_valid || !checked);
}


bool bril::hfsource::Application::readlut(std::vector<uint16_t> lut[nFibers][nChannels])
{
    // Returns false on success.

    // NOTE: allocate memory on heap since buffer is large (avoids segfaults)
    char* buf = new char[Nbytes_allocated];  // 8 Mb

    // read whole file in one shot
    FILE* f = fopen(luts_path.value_.c_str(), "r");
    if (!f) {
        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file open failed");
        delete buf;
        return true;
    }

    size_t nbytes = fread(buf, 1, Nbytes_allocated, f);

    if (ferror(f) != 0) {
        fclose(f);
        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file read failed");
        delete buf;
        return true;
    }

    if (feof(f) == 0 || nbytes == Nbytes_allocated) {
        fclose(f);
        LOG4CPLUS_ERROR(getApplicationLogger(), "too large LUT file");
        delete buf;
        return true;
    }

    fclose(f);

    // append end of string
    buf[nbytes] = '\0';

    // NOTE: allocate memory on heap since buffer is large (avoids segfaults)
    char* line = new char[Nbytes_allocated];

    char* saveptr_line;
    char* lineptr = strtok_r(buf, "\n", &saveptr_line);

    // loop over lines
    while (lineptr) {
        // make a copy since strtok_r() modifies the original buffer
        strcpy(line, lineptr);

        char* saveptr_lineParser;
        char* lineParser = strtok_r(line, " \t", &saveptr_lineParser);

        /////  stands for the columns in the LUTs file
        int  LineIndex = 0;
        int iFiber=-1;
        int  iChan = -1;

        // loop over lineParser
        while (lineParser) {
            // crateId, slot, fiberIndex, fiberChanId, ieta, iphi, depth, LUT[0], LUT[1], ...

            if ( LineIndex != 4 && LineIndex != 5 && LineIndex != 6) {
                // convert to integer (decimal, octal and hex numbers are accepted)
                char* ptr; // reference to first invalid character

                long int res = strtol(lineParser, &ptr, 0);

                if ( strlen(lineParser) == 1 )
                {
                    if ( lineParser[0] == 0x0A || lineParser[0] == 0x0D ) break;
                }
                if ( strlen(lineParser) == 2 )
                {
                    if ( lineParser[0] == 0x0D && lineParser[1] == 0x0A ) break;
                }
                // validation

                if (res == LONG_MIN || res == LONG_MAX || *ptr != '\0' || res < 0 || res > /*UINT16_MAX*/65535) {
                    LOG4CPLUS_ERROR(getApplicationLogger(), "not a number or wrong value, reading of LUTs failed");
                    delete line;
                    delete buf;
                    return true;
                }

                if ( LineIndex == 0) {
                    // to to next line if this is wrong crate
                    if (crate.value_ != res)
                        break;
                }
                else if (  LineIndex == 1) {
                    // to to next line if this is wrong slot
                    if (board.value_ != res)
                        break;
                }
                else if ( LineIndex == 2) {
                    iFiber = res;
                    if (iFiber < 0 || iFiber > int(nFibers-1)) {
                        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file: wrong iFiber");
                        delete line;
                        delete buf;
                        return true;
                    }
                }
                else if ( LineIndex == 3) {
                    iChan = res;
                    if (iChan < 0 || iChan > int(nChannels-1)) {
                        LOG4CPLUS_ERROR(getApplicationLogger(), "LUT file: wrong iChan ============");
                        delete line;
                        delete buf;
                        return true;
                    }
                }
                else
                lut[iFiber][iChan].push_back((uint16_t)res);
            }

            LineIndex++;
            lineParser = strtok_r(NULL, " \t", &saveptr_lineParser);
        }

        lineptr = strtok_r(NULL, "\n", &saveptr_line);
    }

    delete line;
    delete buf;

    LOG4CPLUS_DEBUG(getApplicationLogger(), "made it so far... will push the 1-to-1 LUT");
    // write 1-to-1 LUTs for those fibers/channels which are missing in the file
    for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++)
        for (unsigned int iChan = 0; iChan < nChannels; iChan++)
            if (lut[iFiber][iChan].size() == 0) {
                LOG4CPLUS_WARN(getApplicationLogger(), "No LUT data for crate # " << crate.value_ << ", slot # " << board.value_ << ", fiber # " << iFiber << ", chanel # " << iChan << " . Using 1-to-1 LUT");

                for (uint16_t i = 0; i < nElements; i++) lut[iFiber][iChan].push_back(i);
            }

bool ret_ = false;
    // verify that sizes of LUTs are correct
    for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++)
        for (unsigned int iChan = 0; iChan < nChannels; iChan++)
            if (lut[iFiber][iChan].size() != nElements) {
                LOG4CPLUS_ERROR(getApplicationLogger(), "invalid LUT size for iFiber=" << iFiber << ", iChan=" << iChan);
                // this used to be an exit, we may change it back in the future
                ret_=true;
            }


    if(compressLUT){
        for (unsigned int iFiber = 0; iFiber < nFibers; iFiber++)
            for (unsigned int iChan = 0; iChan < nChannels; iChan++)
                for (uint16_t i = 0; i < nElements; i++) {
                    lut[iFiber][iChan][i]=int(float(lut[iFiber][iChan][i])/2.+0.5);
                    if (lut[iFiber][iChan][i]>1023) lut[iFiber][iChan][i] = 1023;
                }
    }

    return ret_;
}
