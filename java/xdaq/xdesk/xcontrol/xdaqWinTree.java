package xdaq.tools.win;

import javax.swing.*;
import javax.swing.tree.*;
import org.w3c.dom.*;

import xdaq.tools.win.xdaqWinTreeRenderer;

public class xdaqWinTree extends JTree 
{
	xdaqWinTreeRenderer renderer_;
	Document currentDocument;
	DefaultTreeModel treeModel;
	
	public xdaqWinTree( Document document) 
	{
		currentDocument = document;
		this.putClientProperty ("JTree.lineStyle", "Angled");
		this.getSelectionModel().setSelectionMode 
			(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
			
		this.setEditable(false);
		
		// The following doesn't work in contrary to the documentation
		//this.setRowHeight ( 0 );
		
		xdaqWinTreeAdapterNode adapterNode = new xdaqWinTreeAdapterNode (
			document.getDocumentElement(), null);
		
		DefaultMutableTreeNode rootNode = this.createTree ( adapterNode );
			
		treeModel = new DefaultTreeModel ( rootNode );
		this.setModel ( treeModel );
		
		treeModel.setRoot ( rootNode );
		this.treeDidChange();
		
		// Theese two lines have to be the last ones in the CTOR.
		// Otherwise the JVM on Linux and Sun report an exception.
		//
		renderer_ = new xdaqWinTreeRenderer();
		this.setCellRenderer ( renderer_ );
	}
	
	// Recursivly traverse all DOM nodes and build a tree from DefaultMutableTreeNodes
	//
	public DefaultMutableTreeNode createTree ( xdaqWinTreeAdapterNode adapterNode )
	{
		DefaultMutableTreeNode node = new DefaultMutableTreeNode ( adapterNode );
		
		if ( adapterNode.isLeaf() )
		{
			return node;
		}
		else {
			NodeList children = adapterNode.domNode.getChildNodes();
			
			// use adapterNode::childCount instead of the equivalent DOM
			// node function, since the DOM document also contains empty
			// text nodes.
			//
			for (int k = 0; k < adapterNode.childCount(); k++)
			{
				node.add ( createTree ( adapterNode.child (k) ) );
			}
			return node;
		}
	}
	
	Document getDocument() 
	{
		return currentDocument;
	}
	
	public void selectApplication ( String className, String instance, String targetAddr, String transport )
	{
		DefaultTreeModel model = (DefaultTreeModel) getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
		TreePath mainPath = new TreePath (root);
		
		clearSelection();
		
		xdaqWinTreeAdapterNode n;
		for (int i = 0; i < model.getChildCount (root); i++)
		{
			DefaultMutableTreeNode child = (DefaultMutableTreeNode) model.getChild (root, i);
			n = (xdaqWinTreeAdapterNode) (child.getUserObject());
			String nodeName = n.getNodeName();
			
			// Look for all Host nodes
			//
			if ( nodeName.equals ("Host") ) 
			{
				// Look for all Application nodes under Host
				//
				TreePath path = mainPath.pathByAddingChild(child);
				for (int j = 0; j < model.getChildCount ( child ); j++)
				{
					DefaultMutableTreeNode hostChild = (DefaultMutableTreeNode) model.getChild (child, j);
					xdaqWinTreeAdapterNode m = (xdaqWinTreeAdapterNode) (hostChild.getUserObject());
					
					if (m.getNodeName().equals ("Application") || m.getNodeName().equals ("Transport"))
					{
						String applicationClassName  =  m.getAttribute("class");
						String applicationInstance   =  m.getAttribute("instance");
						String applicationTid        =  m.getAttribute("targetAddr");
						String applicationTransport  =  m.getAttribute("transport");

						if 	( (className.equals("Any")   || className.equals (applicationClassName)) &&
				  	  		(instance.equals ("Any")   || instance.equals (applicationInstance))   &&
					  		(targetAddr.equals ("Any") || targetAddr.equals (applicationTid))      &&
					  		(transport.equals ("Any")  || transport.equals (applicationTransport)) )
						{	
							TreePath ApplicationPath = path.pathByAddingChild ( hostChild );
							addSelectionPath ( ApplicationPath );
						}
					}
				}
			}
		}
	}
	
	public void selectHost ( String id, String url )
	{
		DefaultTreeModel model = (DefaultTreeModel) getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
		TreePath mainPath = new TreePath (root);
		
		clearSelection();
		
		xdaqWinTreeAdapterNode n;
		for (int i = 0; i < model.getChildCount (root); i++)
		{
			DefaultMutableTreeNode child = (DefaultMutableTreeNode) model.getChild (root, i);
			n = (xdaqWinTreeAdapterNode) (child.getUserObject());
			String nodeName = n.getNodeName();
			
			if ( nodeName.equals ("Host") ) 
			{
				String hostId  = n.getAttribute("id");
				String hostUrl =  n.getAttribute("url");
			
				if 	( (id.equals("Any") || id.equals (hostId)) &&
				  	(url.equals ("Any") || url.equals (hostUrl)) )
				{
					TreePath path = mainPath.pathByAddingChild ( child );
					addSelectionPath ( path );
				}
			}
		}
	}
	
	
}
