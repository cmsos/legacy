package xdaq.tools.win;

import org.w3c.dom.*;
import org.w3c.dom.Node.*;
import javax.xml.parsers.*;
import javax.swing.table.DefaultTableModel;
import xdaq.tools.win.TreeTableModelAdapter;
import xdaq.tools.win.DOMtoString;

public class ParameterTableModel extends AbstractTreeTableModel 
                             implements TreeTableModel 
{
    // Names of the columns.
    static protected String[]  cNames = {"Name", "Selection", "Type", "Value"};

    // Types of the columns.
    static protected Class[]  cTypes = {TreeTableModel.class, Boolean.class, String.class, String.class, String.class};
	 TreeTableModelAdapter adapter = null;
  
  	public void setAdapter(TreeTableModelAdapter adapter )
	{
		this.adapter = adapter;
	}
	
    	public ParameterTableModel(Node root) 
	{ 	
		// See what AbstractTreeTableModel wants as an interface
		//
		setRoot(new ParameterNode (root, null, this));
    	}
	
	public ParameterNode[] getPathToRoot(ParameterNode aNode) 
	{
        	return getPathToRoot(aNode, 0);
    	}
 
 	public void nodeStructureChanged(ParameterNode node) 
	{
        	if (node != null) 
		{
			fireTreeStructureChanged(this, getPathToRoot(node), null, null);
        	}
    	}



	protected ParameterNode[] getPathToRoot(ParameterNode aNode, int depth) 
	{
        	ParameterNode[] retNodes;
		// This method recurses, traversing towards the root in order
		// size the array. On the way back, it fills in the nodes,
		// starting from the root and working back to the original node.

        	/* Check for null, in case someone passed in a null node, or
        	   they passed in an element that isn't rooted at root. */
        	if(aNode == null) 
		{
        	    if(depth == 0)
                	return null;
        	    else
                	retNodes = new ParameterNode[depth];
        	}
        	else 
		{
        	    depth++;
        	    if(aNode == root)
                	retNodes = new ParameterNode[depth];
        	    else
                	retNodes = getPathToRoot((ParameterNode)aNode.father, depth);
        	    retNodes[retNodes.length - depth] = aNode;
        	}
        	return retNodes;
    	}

	// update model with incoming DOM
	public void update (Node root) 
	{
		((ParameterNode) this.getRoot()).update(root);
		//nodeStructureChanged((ParameterNode) this.getRoot());
		adapter.fireTableDataChanged();
	}
	
	
	// export deep -> also vector elements
	public Node exportSelection (boolean deep, String nsURI, String nsPrefix)
	{  
		Document document = null;
     		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		
		try {
          		DocumentBuilder builder = factory.newDocumentBuilder();
			DOMImplementation impl = builder.getDOMImplementation();
			// document = impl.createDocument ("urn:xdaq:configuration", "xdaq", null);
		        document = impl.createDocument(null, "xdaq", null);
	
			//Element root = (Element) document.createElementNS("urn:xdaq:configuration","root");
			System.out.println ("nsURI: [" + nsURI +"]");
			System.out.println ("nsURI: [" + nsPrefix +"]");

			Element root = (Element) document.createElementNS(nsURI, nsPrefix+":root");
			
			root.setAttribute("xmlns:soapenc","http://www.w3.org/2001/XMLSchema-instance");
			root.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			
			
	 		((ParameterNode) this.getRoot()).exportSelection(root, deep);
			org.w3c.dom.Element element = (org.w3c.dom.Element)root.getFirstChild();
			element.setAttribute("xmlns:"+nsPrefix, nsURI);
	  		return root;
      		} catch (ParserConfigurationException pce) 
		{
            		// Parser with specified options can't be built
            		pce.printStackTrace();
			return null;
      		}
	}
	

	
	 /** By default, make the column with the Tree in it the only editable one. 
    *  Making this column editable causes the JTable to forward mouse 
    *  and keyboard events in the Tree column to the underlying JTree. 
    */ 
	public boolean isCellEditable(Object node, int column) 
	{
		if ( getColumnClass(column) == TreeTableModel.class )
			return true;
		else 	 
			return  ((ParameterNode) node).isEditable(cNames[column]);
    	}

    //
    // Some convenience methods. 
    //

    	protected Object[] getChildren(Object node) 
	{
		ParameterNode parameterNode = ((ParameterNode)node); 
		return parameterNode.getChildren(); 
    	}

    	//
    	// The TreeModel interface
    	//
    	public void setValueAt(Object aValue, Object node, int column) 
	{
		((ParameterNode) node).setValue (aValue, cNames[column]);

	}
	
    	public int getChildCount(Object node) 
	{ 
		Object[] children = getChildren(node); 
		return (children == null) ? 0 : children.length;
    	}

    	public Object getChild(Object node, int i) 
	{ 
		return getChildren(node)[i]; 
    	}

    	// The superclass's implementation would work, but this is more efficient. 
    	public boolean isLeaf(Object node) { return ((ParameterNode) node).isSimpleType(); }

    	//
    	//  The TreeTableNode interface. 
    	//
    	public int getColumnCount() 
	{
		return cNames.length;
    	}

    	public String getColumnName(int column) 
	{
		return cNames[column];
    	}

    	public Class getColumnClass(int column) 
	{
		return cTypes[column];
    	}
 
    	public Object getValueAt(Object node, int column) 
	{
		ParameterNode n = (ParameterNode) node; 
		
		switch(column) 
		{
		    case 0:
			return n.getName();
			case 1:
			return n.getSelection();
		    case 2:
			return n.getType();
		    case 3:
			return n.getValue();
		}
	
		// Should never happen!
		return null; 
    	}
}

/* A FileNode is a derivative of the File class - though we delegate to 
 * the File object rather than subclassing it. It is used to maintain a 
 * cache of a directory's children and therefore avoid repeated access 
 * to the underlying file system during rendering. 
 */
class ParameterNode 
{  
	Object[] children; 
	Object father;
	ParameterTableModel model = null;
	
	// Parameter is the node <Parameter type="..." ...>
	public void callViewer ( org.w3c.dom.Node parameter )
	{
		Node start = parameter.getFirstChild();
		
		while (start.getNodeType() !=  org.w3c.dom.Node.ELEMENT_NODE)
		{
			System.out.println ("A non Element node under Parameter");
			start = start.getNextSibling();
		}
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			org.w3c.dom.Document plot = builder.newDocument();
			Node n = plot.importNode (start, true);
			plot.appendChild ( n );
			//String s = DOMtoString.serialize (plot.getDocumentElement());
			//System.out.println ("Document received: \n" + s);
		} catch (ParserConfigurationException pce) {
			System.out.println (pce.toString());
		}
	}
	
	//! Spawns a viewer's frame
	public void showViewer()
	{
	}
	
	// build a tree of parameter nodes from an existing
	// DOM tree
	public ParameterNode (Node node, ParameterNode father, ParameterTableModel model )
	{
	    	this.father = father;
		this.model = model;
		children = null;
		index_ = "";
		size_ = "0";
		
		NamedNodeMap attributes = node.getAttributes();
		
		// Must do exception handling here
		//
		try
		{
			type_ = attributes.getNamedItemNS("http://www.w3.org/2001/XMLSchema-instance","type").getNodeValue();
		} catch (java.lang.NullPointerException ex) 
		{
			// is the root therefore is the main bag
			//if ( father == null )
			//{	
			//	type_ = "bag";
			//}
			//else {
				System.out.println("error : no type attribute for node: " + node.getNodeName());
			
			//}	
		}
		//try{
			//name_ = attributes.getNamedItem("name").getNodeValue();
		name_ = node.getLocalName();
		//} catch (java.lang.NullPointerException e) 
		//{
		if ( name_ == "item" )
		{
			try 
			{
				// Check for vector index
				index_ = attributes.getNamedItemNS(" http://schemas.xmlsoap.org/soap/encoding/","position").getNodeValue();
				index_ = index_.substring(index_.indexOf('[')+1, index_.indexOf(']')-1);
				name_ = index_;
			}
			catch(java.lang.NullPointerException ex)
			{
				//if ( father == null ) {
					// is the root therefore is the main bag
				//	name_ = "Parameters";
				//}
				//else {
					System.out.println("error : no name attribute");
			
				//}
			
			}
		
		}	
		
		
		value_ = "-";
			
		//if ( type_.equals ("bag") || type_.equals ("vector") || type_.equals("infospace")  || type_.equals("monitorable") )
		if ( type_.endsWith ("Array") ||  type_.endsWith ("Struct"))
		{
			NodeList list = node.getChildNodes();
			
			// Remember the size of the vector or bag
			Integer sizeInt = new Integer (list.getLength());
			size_ = sizeInt.toString();
			
			if (list.getLength() > 0)
			{
				Object[] tmp = new ParameterNode[list.getLength()];
				int j = 0;
				int i = 0;
				for (i = 0; i < list.getLength(); i++)
				{
					if (list.item(i).getNodeType() ==  org.w3c.dom.Node.ELEMENT_NODE)
					{
						tmp[j] = new ParameterNode (list.item(i), this, model);
						j++;
					}
				}
				children = new ParameterNode[j];
				for (i = 0; i < j; i++) { children[i] = tmp[i]; }
			}
		} else if ( type_.equals ("jasplot"))
		{
			// EXPERIMENTAL SUPPORT FOR JASPLOT
			value_ = "jasplot";
			System.out.println ("Created new plot data");
			callViewer ( node );
		}
		else 
		{
		    //System.out.println("Node name:" +node.getNodeName() +" name:"+name_+" type:"+type_);
			if ( node.hasChildNodes() ) {
				value_ = node.getFirstChild().getNodeValue();
			}
			else {
				value_ = "";
			}	
		}
	}
	
	//! Update a parameter value in the table model
	public void update (Node node)
	{
		//System.out.println("update");
		NamedNodeMap attributes = node.getAttributes();
		//String type = "";
		
		try
		{
			//type = attributes.getNamedItem("type").getNodeValue();
			type_ = attributes.getNamedItemNS("http://www.w3.org/2001/XMLSchema-instance","type").getNodeValue();
		
		
		} catch (java.lang.NullPointerException ex) 
		{
			// is the root therefore is the main bag
			//if ( father == null )
			//{	
			//	type = "infospace";
			//}
			//else {
				System.out.println("error : no type attribute for node: "+ node.getNodeName());
			//}	
		}
		
		NodeList list = node.getChildNodes();
		
		if (type_.endsWith("Array"))
		{

			// Remember the new vector size
			//String size = attributes.getNamedItem("size").getNodeValue();
			String size = attributes.getNamedItemNS(" http://schemas.xmlsoap.org/soap/encoding/","arrayType").getNodeValue();
			size = size.substring(index_.indexOf('[')+1, index_.indexOf(']')-1);

			// If the size has changed, replace all of them, otherwise,
			// update only the received elements.
			if (!size_.equals(size))
			{
				//System.out.println ("Overriding vector. Old Size: " + size_ + ", new size: " + size);
				
				size_ = size;
				// Blindly re-create all vector items
				// Vector size may change dynamically
				//
				//System.out.println("Blindly re-create all vector items");
				Object[] tmp = new ParameterNode[list.getLength()];
				int j =0;
				int i = 0;
				for (i = 0; i < list.getLength(); i++)
				{
					if (list.item(i).getNodeType() ==  org.w3c.dom.Node.ELEMENT_NODE) 
					{
						tmp[j] = new ParameterNode (list.item(i), this, model);
						((ParameterNode)tmp[j]).setSelection(true);
						//System.out.println(" re-create   items"+j);
						j++;
					}	
				}
				//System.out.println(" after re-create  total items"+i);
				if (i == 0) 
				{ // no elements of vector detected
					children = null; // remove all previous children
				} else 
				{	
					children = new ParameterNode[j];
					for (i = 0; i < j; i++) { 
						children[i] = tmp[i];
						//System.out.println(" re-assign node"+i); 
					}
				}

				// Update the graphical TreeTable structure 
				// according to the new model
				//
				model.nodeStructureChanged(this);
			} else
			{
				//System.out.println ("Only update the vector, don't override");
				
				for (int i = 0; i < list.getLength(); i++)
				{
					if (list.item(i).getNodeType() ==  org.w3c.dom.Node.ELEMENT_NODE) 
					{
						// find node to update
						NamedNodeMap attrs = list.item(i).getAttributes();
						//String index = attrs.getNamedItem("index").getNodeValue();
						String index = attributes.getNamedItemNS(" http://schemas.xmlsoap.org/soap/encoding/","position").getNodeValue();
						index = index_.substring(index_.indexOf('[')+1, index_.indexOf(']')-1);
						
						if ( children != null ) 
						{
							for ( int j = 0; j< children.length; j++ ) 
							{
								// INDEX equals NAME for Vectors and can be used for
								// finding the right entry to be updated.
								if (  ((ParameterNode)children[j]).getName().equals(index) ) 
								{
									((ParameterNode) children[j]).update (list.item(i));
								}
							}	

						}
					}
				}
			}

		} //else if ( type.equals ("bag") || type.equals("infospace") || type.equals("monitorable") )
		else if ( type_.endsWith("Struct") )
		{
			//System.out.println("update all bag items");
			for (int i = 0; i < list.getLength(); i++)
			{
				if (list.item(i).getNodeType() ==  org.w3c.dom.Node.ELEMENT_NODE) {
					// find node to update
					//NamedNodeMap attrs = list.item(i).getAttributes();
					//String name = attrs.getNamedItem("name").getNodeValue();
					String name = list.item(i).getLocalName();
					
					if ( children != null ) {
						for ( int j = 0; j< children.length; j++ ) {
							if (  ((ParameterNode)children[j]).getName().equals(name) ) {
								((ParameterNode) children[j]).update (list.item(i));
							}
						}	

					}
				}

			}
		} else if ( type_.equals ("jasplot"))
		{
			// EXPERIMENTAL JASPLOT SUPPORT
			// <Parameter type = "jasplot" ...>
			value_ = "jasplot";
			callViewer ( node ); 
		}
		else 
		{
			//System.out.println ("Update a leaf node with value: " + list.item(0).getNodeValue());
			
			if ( node.hasChildNodes() ) 
			{
				value_ = list.item(0).getNodeValue();
			}
			else 
			{
				value_ = "";
			}	
		}	
	}
	
	// Check which parameters are selected and
	// append to the node parameter
	//
	public void exportSelection (Node node, boolean deep)
	{		
		if (selection_) 
		{
			Document doc = node.getOwnerDocument();
			
			
			//Element param = doc.createElement("Parameter");
			Element param = null;
			
			if ( ! index_.equals("")) 
			{
				//param.setAttribute ("index", index_);
				param = doc.createElementNS(node.getNamespaceURI(),node.getPrefix()+":item");
				param.setAttributeNS("http://schemas.xmlsoap.org/soap/encoding/","soapenc:position", "["+index_+"]");
			}
			else 
			{
				//param.setAttribute ("name", name_);
				param = doc.createElementNS(node.getNamespaceURI(), node.getPrefix()+ ":" + name_);
				param.setAttributeNS("http://www.w3.org/2001/XMLSchema-instance","xsi:type",type_);
			}	
			
			//param.setAttribute ("type", type_);
			node.appendChild (param);

			//if (type_.equals("bag") || type_.equals("infospace") || type_.equals("monitorable")) 
			if (type_.endsWith("Struct") )
			{
				if (children != null) 
				{
					for (int i = 0; i < children.length; i++)
					{	
						((ParameterNode)children[i]).exportSelection (param, deep);
					}
				}
			} 
			//else if (type_.equals("vector") )
			else if (type_.endsWith("Array") )
			{
				// Add the size
				//param.setAttribute ("size", size_);
				param.setAttributeNS("http://schemas.xmlsoap.org/soap/encoding/","soapenc:arrayType", "xsd:ur-type["+size_+"]");
				
				if ((deep) && (children != null)) 
				{
					for (int i = 0; i < children.length; i++)
					{	
						((ParameterNode)children[i]).exportSelection (param, deep);
					}
				}
			} 
			else if (type_.equals("jasplot") )
			{
				// EXPERIMENTAL JASPLOT SUPPORT
				// a jasplot is not sent back to an xdaq executive
				// for the moment. Do nothing here.
			}
			else 
			{
				Text value = doc.createTextNode( value_ );
				param.appendChild ( value );
			}
		}
	}


    	/**
     	* Returns the the string to be used to display this leaf in the JTree.
     	*/
    	public String toString () 
	{ 
		return this.name_;
    	}

    	/**
     	* Loads the children, caching the results in the children ivar.
     	*/
    	protected Object[] getChildren() 
	{
		if (children != null) 
		{
			return children;
		}
		return null;
    	}
	
	public boolean isSimpleType()
	{
		//if ( ( !this.type_.equals("bag") ) && (!this.type_.equals("vector")) && (!this.type_.equals("infospace")) && (!this.type_.equals("monitorable"))) return true;
		if (  (!this.type_.endsWith("Struct") ) && (!this.type_.endsWith("Array")) ) return true;
		return false;
	}
	
	public boolean isEditable ( String field )
	{
		if ( this.isSimpleType() && ((field.equals ("Value") ))) 
		{	
				return true;
		}	
		else if (field.equals ("Selection")) 
		{
			// The Selection check box is always editable -> clickable
			return true;
		}
		return false;
	}
	
	public void setValue (Object aValue, String field)
	{
		if (field.equals("Selection") ) 
		{
			selection_ = ((Boolean) aValue).booleanValue();
			if (children != null) 
			{
				for (int i = 0; i < children.length; i++)
				{
					((ParameterNode)children[i]).setValue (aValue, field);
				}
			}
			
			// Only if it is a selection action check for the father.
			// In case of de-selection, don't call the father nodes.
			//
			if ((father != null) && ((Boolean) aValue).booleanValue())
				((ParameterNode)father).setFatherSelection (selection_);
		}
		
		if (field.equals("Value")) value_ = (String) aValue;
	}
	
	public void setFatherSelection ( boolean selection )
	{
		selection_ = selection;
		if (father != null)
		{
			// Set value and call father	
			((ParameterNode)father).setFatherSelection (selection);
		}
	}
	
	public String getName() { return this.name_; }
	public String getType() { return this.type_; }
	public String getValue() { return this.value_; }
	
	public Boolean getSelection() 
	{ 
		return new Boolean(this.selection_); 
	}
	
	public void setSelection (boolean value)
	{
		selection_ = true;
		if ( children != null ) 
		{
			for ( int j = 0; j< children.length; j++ ) 
			{
				((ParameterNode) children[j]).setSelection(value);
			}
		
		}
	}
	
	private String name_;
	private String type_;
	private String value_;
	private boolean selection_;
	private String index_;
	private String size_;
}

