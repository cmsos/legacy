#include "ferol/FrlFirmwareChecker.hh"
#include "ferol/loggerMacros.h"
#include <sstream>
#include <iomanip>
#include "ferol/Ferol.hh"
#include "ferol/Frl.hh"

ferol::FrlFirmwareChecker::FrlFirmwareChecker( HAL::PCIDevice * frlDevice,
                                               HAL::PCIDevice * bridgeDevice,
                                               ConfigSpaceSaver *cfgfrl,
                                               ConfigSpaceSaver *cfgferol,
                                               bool dontFail )
{

    frlDevice->read( "FirmwareVersion", &frlFwVersion_ );
    frlDevice->read( "FirmwareType", &frlFwType_ );
    frlDevice->read( "HardwareRevision", &frlHwRevision_ );
    bridgeDevice->read( "FirmwareVersion", &bridgeFwVersion_ );
    bridgeDevice->read( "FirmwareType", &bridgeFwType_ );
    bridgeDevice->read( "HardwareRevision", &bridgeHwRevision_ );

    fpgaType_ = FRL;

    bridgeDevice_P = bridgeDevice; // needed to change firmware versions.
    frlDevice_P = frlDevice; // needed to verify the firmware type after a firmware change.

    cfgFrl_P = cfgfrl;
    cfgFerol_P = cfgferol;
    dontFail_ = dontFail;
}

ferol::FrlFirmwareChecker::FrlFirmwareChecker( HAL::PCIDevice * ferolDevice, bool dontFail )
{

    ferolDevice->read( "FirmwareVersion", &ferolFwVersion_ );
    ferolDevice->read( "FirmwareType", &ferolFwType_ );
    ferolDevice->read( "HardwareRevision", &ferolHwRevision_ );

    fpgaType_ = FEROL;
    dontFail_ = dontFail;
}

bool
ferol::FrlFirmwareChecker::checkFirmware( std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw )
{
    bool result = true;
    if ( fpgaType_ == FRL ) 
        {
            result = checkFrlFirmware( mode, dataSource, errorstr, changedFw);
        } 
    else if ( fpgaType_ == FEROL )
        {
            result = checkFerolFirmware( mode, dataSource, errorstr );
        }
    else 
        {
            result = false;
            errorstr = "Illegal fpgaType encountered (probably a software bug)";
        }

    if ( dontFail_ )
        result = true;

    return result;
}
    

bool
ferol::FrlFirmwareChecker::checkFrlFirmware( std::string mode, std::string dataSource, std::string &errorstr, bool *changedFw )
{
    bool ret = true;
    std::stringstream error;

    if ( changedFw ) *changedFw = false;

    // There are two generations of FRLs which are used. 
    // The newer type has bit 0 of the type set to 1
    // whereas the older has this bit set to 0. We accept
    // both versions since they are functionally compatible.

    // check the bridge type:
    if ( bridgeFwType_ != FRL_BRIDGE_FIRMWARE_TYPE ) 
        {
            ret = false;
            error << "Error verifying Bridge firmware type - expected " 
                  << std::hex << FRL_BRIDGE_FIRMWARE_TYPE << " read " 
                  << bridgeFwType_;
        }
  
    // check the bridge version:
    if ( bridgeFwVersion_ < FRL_BRIDGE_FIRMWARE_MIN_VERSION ) 
        {
            ret = false;
            error << "Error verifying Bridge firmware version - expected >= " 
                  << std::hex << FRL_BRIDGE_FIRMWARE_MIN_VERSION << " read " 
                  << bridgeFwVersion_;
        }

    // check the FRL type:
    // only check the firmware of the FRL in the modes where the FRL is actually used:
    if ( mode == FRL_MODE )
        {
            uint32_t requiredFirmwareType    = 0;
            uint32_t requiredFirmwareVersion = 0;
            
            if ( dataSource == SLINK_SOURCE )
                {
                    requiredFirmwareType = FRL_DAQ_FIRMWARE_TYPE;
                    requiredFirmwareVersion = FRL_DAQ_FIRMWARE_MIN_VERSION;
                }
            else if ( dataSource == GENERATOR_SOURCE )
                {
                    requiredFirmwareType = FRL_FEDEMU_FIRMWARE_TYPE;
                    requiredFirmwareVersion = FRL_FEDEMU_FIRMWARE_MIN_VERSION;
                }
	    else 
	      {
		error << "The dataSource \"" << dataSource << "\" is unexpected in FRL Mode. Subsequent tests will fail. Fix your configuration (i.e. probably the DataSource parameter). \n";
	      }

    
            if ( frlFwType_ != requiredFirmwareType ) 
                {
                    error << "Error verifying FRL firmware type - expected " 
                          << std::hex << requiredFirmwareType << " read " 
                          << frlFwType_
                          << ". Trying to change the firmware now.";
                    //std::cout << error << std::endl;
                    if ( ! this->changeFrlFirmware( requiredFirmwareType, error ) ) 
                        {
                            ret = false;
                        }
                    if ( frlFwType_ != requiredFirmwareType ) 
                        {
                            ret = false;
                            error << "Changing of firmware type in frl failed. Firmware in hardware is " << frlFwType_
                                  << ". Giving up at this point...";
                        }
                    else
                        {
                            error << "Changing of firmware was successful.";
                            if ( changedFw ) *changedFw = true;
                        }
               }
            
            if ( frlFwVersion_ < requiredFirmwareVersion ) 
                {
                    ret = false;
                    error << "Error verifying FRL firmware version - expected >= " 
                          << std::hex <<  requiredFirmwareVersion << " read " 
                          << frlFwVersion_;
                } 
            errorstr = error.str();
        }

    return ret;
}

// this is not so easy: first the monitoring loops have to be shut down.
// Actually the FRL monitoring loop does not need to be stopped: it only
// touches the hardware if the flat "hardwareCreated" is set. This is not
// set at the moment this routine is called. 
// No access during this gymnastics! Also the Ferol must not be touched
// since during the procedure the hardware issues a PCI reset to the ferol
// so that also the ferol looses the PCI configuration and all other 
// configuration. Therefore this reload has to be handled differently...
bool
ferol::FrlFirmwareChecker::changeFrlFirmware( uint32_t requiredFirmwareType, std::stringstream &err )
{
    int32_t fwix = -1;
    if( requiredFirmwareType == FRL_DAQ_FIRMWARE_TYPE )
        fwix = 0;
    else if( requiredFirmwareType == FRL_FEDEMU_FIRMWARE_TYPE )
        fwix = 1;
    else {
        err << "Unknown firmware type: " << requiredFirmwareType << ". Cannot change firmware.";
        return false;
    }

    // Avoid any access to the hardware including the Ferol!
    cfgFerol_P->hwlock();
    cfgFrl_P->hwlock();

    //std::cout << "Locked the hardware and got all pci devices. " << std::endl;
    try 
        {
            cfgFrl_P->saveConfigSpace( );
            //std::cout << " saved config space of frl" << std::endl;
            cfgFerol_P->saveConfigSpace( );
            //std::cout << " saved config space of ferol" << std::endl;
            bridgeDevice_P->write( "firmwareIndex", fwix );
            //std::cout << " set firmware index to  " << std::hex << fwix << std::endl;
            ::sleep(5);

            bridgeDevice_P->write( "FRLReload", 1 );
            bridgeDevice_P->write( "FRLReload", 0 );

            ::sleep(10);

            cfgFerol_P->rescanBAbits();
            cfgFrl_P->rescanBAbits();

            cfgFerol_P->writeConfigSpace();
            //std::cout << " wrote back ferol config space" << std::endl;
            cfgFrl_P->writeConfigSpace();
            //std::cout << " wrote back frl config space" << std::endl;
        } 
    catch( HAL::HardwareAccessException &e )
        {
            err << "Could not change firmware : \n " << e.what();
            cfgFrl_P->hwunlock();
            cfgFerol_P->hwunlock();
            return false;
        }

    frlDevice_P->read( "FirmwareVersion", &frlFwVersion_ );
    frlDevice_P->read( "FirmwareType", &frlFwType_ );
    frlDevice_P->read( "HardwareRevision", &frlHwRevision_ );

    // This crashes the computer for some reaons...
//     // We have to reconfigure the Ferol
//     std::cout << "re-configuring ferol after firwmare change" << std::endl;
//     try 
//         {
//             cfgFerol_P->configure();
//         }
//     catch ( utils::exception::FerolException &e )
//         {
//             err << "Problems while re-configuring Ferol after Frl firmware change: \n" << e.what();
//             cfgFrl_P->hwunlock();
//             cfgFerol_P->hwunlock();
//             return false;
//         }
    cfgFrl_P->hwunlock();
    cfgFerol_P->hwunlock();
    //std::cout << "unlocked... all done, vesions are fw: " << std::hex << frlFwVersion_ << " type " << frlFwType_ << " hw " << frlHwRevision_ << std::endl;
    return true;

}


bool
ferol::FrlFirmwareChecker::checkFerolFirmware( std::string mode, std::string dataSource, std::string &errorstr )
{
    bool ret = true;
    std::stringstream error;

    // There are two generations of FRLs which are used. 
    // The newer type has bit 0 of the type set to 1
    // whereas the older has this bit set to 0. We accept
    // both versions since they are functionally compatible.

    // check the FEROL type:
    uint32_t requiredFirmwareType    = 0;
    uint32_t requiredFirmwareVersion = 0;

    if ( mode == FEDKIT_MODE ) 
        {
            requiredFirmwareType = FEROL_FEDKIT_FIRMWARE_TYPE;
            requiredFirmwareVersion = FEROL_FEDKIT_FIRMWARE_MIN_VERSION;
        }
    else if ( dataSource == L6G_SOURCE || dataSource == L6G_CORE_GENERATOR_SOURCE )
        {
            requiredFirmwareType = FEROL_SLINKEXPRESS_FIRMWARE_TYPE;
            requiredFirmwareVersion = FEROL_SLINKEXPRESS_FIRMWARE_MIN_VERSION;
        }
    else 
        {
            requiredFirmwareType = FEROL_DAQ_FIRMWARE_TYPE;
            requiredFirmwareVersion = FEROL_DAQ_FIRMWARE_MIN_VERSION;
        }
  
    if ( ferolFwType_ != requiredFirmwareType ) 
        {
            ret = false;
            error << "Error verifying FEROL firmware type - expected " 
                  << std::hex << requiredFirmwareType << " read " 
                  << ferolFwType_;
        }

    // check the ferol fw version
    if ( ferolFwVersion_ < requiredFirmwareVersion ) 
        {
            ret = false;
            error << "Error verifying FEROL firmware version - expected >= " 
                  << std::hex <<  requiredFirmwareVersion << " read " 
                  << ferolFwVersion_;
        } 
    errorstr = error.str();

    return ret;
}

uint32_t
ferol::FrlFirmwareChecker::getFrlFirmwareVersion() const
{
    return frlFwVersion_;
}

uint32_t
ferol::FrlFirmwareChecker::getFrlFirmwareType() const
{
    return frlFwType_;
}

uint32_t
ferol::FrlFirmwareChecker::getFrlHardwareRevision() const
{
    return frlHwRevision_;
}

uint32_t
ferol::FrlFirmwareChecker::getBridgeFirmwareVersion() const
{
    return bridgeFwVersion_;
}

uint32_t
ferol::FrlFirmwareChecker::getBridgeFirmwareType() const
{
    return bridgeFwType_;
}

uint32_t
ferol::FrlFirmwareChecker::getBridgeHardwareRevision() const
{
    return bridgeHwRevision_;
}
uint32_t

ferol::FrlFirmwareChecker::getFerolFirmwareVersion() const
{
    return ferolFwVersion_;
}

uint32_t
ferol::FrlFirmwareChecker::getFerolFirmwareType() const
{
    return ferolFwType_;
}

uint32_t
ferol::FrlFirmwareChecker::getFerolHardwareRevision() const
{
    return ferolHwRevision_;
}


//void ferol::FrlFirmwareChecker::reloadFrlFirmware( Frl *frl, 
//                                                   Ferol *ferol, 
//                                                   uint32_t firmwareId )
//{
//    return;
//
//    // this is not so easy: first the monitoring loops have to be shut down.
//    // No access during this gymnastics! Also the Ferol must not be touched
//    // since during the procedure the hardware issues a PCI reset to the ferol
//    // so that also the ferol looses the PCI configuration and all other 
//    // configuration. Therefore this reload has to be handled differently...
//
//    HAL::PCIDevice *frlDevice = frl->frlDevice_P;
//    HAL::PCIDevice *bridgeDevice = frl->bridgeDevice_P;
//    HAL::PCIDevice *ferolDevice = ferol->ferolDevice_P;
//    this->saveFrlConfigSpace( frlDevice );
//    this->saveFerolConfigSpace( ferolDevice );
//    uint32_t ctrl = (firmwareId << 16) + 0x04; // firmware Id to bit 16..18, bit 2 set to trigger reload
//    bridgeDevice->write( "firmwareControl", ctrl );
//    ::sleep(4);
//    this->writeBackFrlConfigSpace( frlDevice );
//    this->writeBackFrlConfigSpace( ferolDevice );
//    std::cout << "all done" << std::endl;
//}
