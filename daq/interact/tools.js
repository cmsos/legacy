// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                            *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                *
 *                                                                       *
 * For the licensing terms see LICENSE.		                            *
 * For the list of contributors see CREDITS.   			                *
 *************************************************************************/
var url = require('url');

module.exports = {
		getCallerIP : 	function (request) {
			var ip = request.headers['x-forwarded-for'] ||
			request.connection.remoteAddress ||
			request.socket.remoteAddress ||
			request.connection.socket.remoteAddress ||
			request.ip;
			ip = ip.split(',')[0];
			//console.log(ip);
			ip = (ip.split(':').slice(-1))[0]; //in case the ip returned in a format: "::ffff:146.xxx.xxx.xxx"
			return ip;
		},


		getHost : function getHost(fullhostname) {
			host = fullhostname.split(':')[0]; 
			return host;
		},

		getPort:function (fullhostname) {
			port = fullhostname.split(':')[1]; 
			return port;
		},

		getHostByIP: function (ip) {
			dns.reverse(ip, function(err, domains) {
				if(err) {
					console.log(err.toString());
					return "unknown";
				}
				//console.log(domains);
				return domains[0];
			});
		},

		processEnvironment: function (options) {

			return function(request, response, next) {
				// Implement the middleware function based on the options object

				//
				// Add CGI/XDAQ specific headers 
				//
				request.headers["request_method"] = request.method;
				request.headers["request_uri"] = request.url;
				request.headers["server_protocol"] = 'HTTP/'+ request.httpVersion;
				request.headers["x-xdaq-receivetimestamp"] = new Date().valueOf();
				request.headers["x-xdaq-remote-addr"] = module.exports.getCallerIP(request);
				// request.headers["x-xdaq-remote-host"] = getHostByIP(request.headers["x-xdaq-remote-addr"]); this is asynchronous cannot work
				request.headers["x-xdaq-remote-host"] = request.headers["x-xdaq-remote-addr"];  // let the user to find

				const request_url = url.parse(request.url);
				console.log('---->ENVIRON begin');
				console.log(request_url);
				
                		var script = '';
                		var script_extract = (request_url.pathname.split('/'))[1];
                		if (script_extract)
                		{
                        		script = script_extract;
                		}
                		var path_info = '';
                		var path_info_extract = (request_url.pathname.split('/'))[2];
                		if (path_info_extract)
                		{
                        		path_info = path_info_extract;
                		}
                		var query_string = '';
                		if (request_url.query)
                		{
                        		query_string = request_url.query;
                		}


				var content_type = '';

				if ( request.headers['content-type'] )
				{
					content_type = request.headers['content-type'] ;
				}
				var content_length = '';
				if ( request.headers['content-length'] )
				{
					content_length = request.headers['content-length'] ;
				}

				var accept = '';
				if ( request.headers['accept'] )
				{
					accept = request.headers['accept'] ;
				}

				var user_agent = '';
				if ( request.headers['user-agent'] )
				{
					user_agent = request.headers['user-agent'] ;
				}

				var referer = '';
				if ( request.headers['referer'] )
				{
					referer = request.headers['referer'] ;
				}

				var cookie = '';
				if ( request.headers['cookie'] )
				{
					cookie = request.headers['cookie'] ;
				}

				var auth_type = '';
				var remote_user = '';
				if ( request.headers['authorization'] )
				{
					var authorization = request.headers['authorization'] ;
					var auth_type_extract = (auth_type.split(' '))[1];
					if (auth_type_extract)
					{
						auth_type = auth_type_extract;
					}
					var remote_user_extract = (auth_type.split(' '))[2];
					if (remote_user_extract)
					{
						remote_user = Buffer.from(remote_user_extract, 'base64'); 
					}
				}

				//
				// Add  CGI/XDAQ specific environment 
				//
				//ENVIRONMENT ORIGINAL: SERVER_SOFTWARE=XDAQ/3.0
				//ENVIRONMENT ORIGINAL: SERVER_NAME=kvm-s3562-1-ip151-66.cms
				//ENVIRONMENT ORIGINAL: GATEWAY_INTERFACE=CGI/1.1
				//ENVIRONMENT ORIGINAL: SERVER_PROTOCOL=HTTP/1.1
				//ENVIRONMENT ORIGINAL: SERVER_PORT=1973
				//ENVIRONMENT ORIGINAL: REQUEST_METHOD=GET
				//ENVIRONMENT ORIGINAL: PATH_TRANSLATED=/urn:xdaq-application:service=hyperdaq
				//ENVIRONMENT ORIGINAL: SCRIPT_NAME=urn:xdaq-application:service=hyperdaq
				//ENVIRONMENT ORIGINAL: PATH_INFO=
				//ENVIRONMENT ORIGINAL: QUERY_STRING=
				//ENVIRONMENT ORIGINAL: REMOTE_HOST=kvm-s3562-1-ip150-16.cms
				//ENVIRONMENT ORIGINAL: REMOTE_ADDR=10.176.140.50
				//ENVIRONMENT ORIGINAL: AUTH_TYPE=
				//ENVIRONMENT ORIGINAL: REMOTE_USER=
				//ENVIRONMENT ORIGINAL: REMOTE_IDENT=
				//ENVIRONMENT ORIGINAL: CONTENT_TYPE=
				//ENVIRONMENT ORIGINAL: CONTENT_LENGTH=
				//ENVIRONMENT ORIGINAL: HTTP_ACCEPT=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
				//ENVIRONMENT ORIGINAL: HTTP_USER_AGENT=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0
				//ENVIRONMENT ORIGINAL: REDIRECT_REQUEST=
				//ENVIRONMENT ORIGINAL: REDIRECT_URL=
				//ENVIRONMENT ORIGINAL: REDIRECT_STATUS=
				//ENVIRONMENT ORIGINAL: HTTP_REFERER=
				//ENVIRONMENT ORIGINAL: HTTP_COOKIE=
				//ENVIRONMENT ORIGINAL: ACCEPT_ENCODING=gzip, deflate
				//ENVIRONMENT ORIGINAL: ACCEPT_LANGUAGE=en-US,en;q=0.5

				//
				var environment = {
						'SERVER_SOFTWARE': 'XDAQ/3.0',
						'SERVER_NAME': module.exports.getHost(request.headers['host']),
						'GATEWAY_INTERFACE' : 'CGI/1.1',
						'SERVER_PROTOCOL' : 'HTTP/'+ request.httpVersion,
						'SERVER_PORT': module.exports.getPort(request.headers['host']),
						'REQUEST_METHOD': request.method,
						'PATH_TRANSLATED': request.url,
						'SCRIPT_NAME': script,        // actually the full path to the executing program
						'PATH_INFO': path_info,       // www.cern.ch/pippo.cgi/path -> path
						'QUERY_STRING': query_string,  // everthing after question mark
						'REMOTE_HOST':  request.headers["x-xdaq-remote-addr"],   //it comes from the socket connection
						'REMOTE_ADDR': request.headers["x-xdaq-remote-addr"],  // IP from client (it comes from the socket connection
						'AUTH_TYPE' : auth_type,
						'REMOTE_USER': remote_user,
						'REMOTE_IDENT': '',              // according RFC931
						'CONTENT_TYPE' :   content_type,
						'CONTENT_LENGTH':  content_length,
						'HTTP_ACCEPT':     accept,
						'HTTP_USER_AGENT': user_agent,
						//REDIRECT_REQUEST=
						//REDIRECT_URL=
						//REDIRECT_STATUS=
						'HTTP_REFERER':    referer,
						'HTTP_COOKIE':     cookie,
						'ACCEPT_ENCODING': request.headers["accept-encoding"],
						'ACCEPT_LANGUAGE': request.headers["accept-language"],
				};


				console.log(environment);
				console.log('---->ENVIRON end');
				request.environment = environment;
				next();
			}
		},

		processByClass: function (req, res, next) {
			var script_name = 'urn:xdaq-application:class=' + req.params.type + ',instance=' + req.params.instance;
			var path_translated =  '/' +script_name + '/' + req.params.resource;

			req.environment["PATH_TRANSLATED"] = path_translated;
			req.environment["SCRIPT_NAME"] = script_name ;
			req.environment["PATH_INFO"] = req.params.resource ;

			next();
		} ,

		processByService : function (req, res, next) {
			var script_name = 'urn:xdaq-application:service=' + req.params.service ;
			var path_translated =  '/' +script_name + '/' + req.params.resource;

			req.environment["PATH_TRANSLATED"] = path_translated;
			req.environment["SCRIPT_NAME"] = script_name ;
			req.environment["PATH_INFO"] = req.params.resource ;

			next();
		},

		processByLID: function (req, res, next) {
			
			var script_name = 'urn:xdaq-application:lid=' + req.params.lid;
			var path_translated =  '/' +script_name;

			if (req.params.resource != undefined ) {
				path_translated =  '/' +script_name + '/' + req.params.resource;
			}
			req.environment["PATH_TRANSLATED"] = path_translated;
			req.environment["SCRIPT_NAME"] = script_name ;
			
			if (req.params.resource != undefined ) {
				req.environment["PATH_INFO"] = req.params.resource;
			}

			next();
		} 
};
