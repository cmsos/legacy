#include "InfoSpaceTestSuite.h"
#include "xdata/ItemEvent.h"

CPPUNIT_TEST_SUITE_REGISTRATION( InfoSpaceTestSuite );

/*
class IntegerActionListener: public xdata::ActionListener
{
	void actionPerformed(xdata::Event & e )
	{
		xdata::ItemEvent &ie = dynamic_cast<xdata::ItemEvent&>(e);
		cout <<  endl << "An event of type: " << e.type() << " has occurred" << endl;
		cout <<  "It is an item of type: " << ie.item()->type() << " name:" << ie.itemName() << endl;
		xdata::Integer * i = dynamic_cast<xdata::Integer *>(ie.item());
		cout << "Current value: " << (int)*i  << endl;
		
		if ( e.type() == "ItemRetrieveEvent" )
		{	
			cout << "I am trying to retrieve the item" << endl;
			*i = 444;
		}
	}
	
};
*/

class TestItemActionListener: public xdata::ActionListener
{
	public:

	TestItemActionListener(std::string type, std::string name, xdata::Serializable * s)
	{
		name_ = name;
		type_ = type;
		s_ = s;
		eventReceived_ = false;
	}
	
	void actionPerformed(xdata::Event & received )
	{
		xdata::ItemEvent& e = dynamic_cast<xdata::ItemEvent&>(received);
		
		//cout << "type_ : " << type_ << ", e.type(): " << e.type() << endl;
		//cout << "name_ : " << name_ << ", e.name(): " << e.itemName() << endl;
		//cout << "s_ : " << hex << s_ << dec << ", e.item(): " << hex << e.item() << dec << endl;
		
		eventReceived_ = (( type_ == e.type() ) && (name_ == e.itemName() ) && (s_ == e.item()) );
	}

	bool eventReceived()
	{
		return 	eventReceived_;
	}
	
	protected:
	
	std::string type_;
	std::string name_;
	xdata::Serializable * s_;
	bool eventReceived_;
	

};


void InfoSpaceTestSuite::setUp()
{
/*	testInteger = new xdata::Integer();
	
	xdata::Integer pippo;
		
	*testInteger = 0;
	
	integerListener = new IntegerActionListener();
*/	
}

void InfoSpaceTestSuite::tearDown()
{
/*
	// missing release info space
	
	delete testInteger;
	delete integerListener;
*/	
}


/*
void InfoSpaceTestSuite::InfoSpaceExample()
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("xdaq.cern.ch/infospace/parameters");
	
	// add listeners to data events
	
	is->addItemAvailabledListener(integerListener);
	is->addItemRevokedListener(integerListener);
	
	// fire availability o item
	
	is->fireItemAvailable("testInteger", testInteger, this);
	
	// add listener to value change and retrieve 
	is->addItemChangedListener("testInteger",integerListener);
	is->addItemRetrieveListener("testInteger",integerListener);
	
	
	// going to change value and notify it to listeners
	
	*testInteger = 100;

	is->fireItemValueChanged("testInteger", this);
	is->fireItemValueRetrieve("testInteger", this);
	
	cout << "The retrieved value is:" << (int)*testInteger << endl;
	
	is->fireItemRevoked("testInteger", this);
	is->removeItemAvailabledListener(integerListener);
	is->removeItemRevokedListener(integerListener);
}
*/	

void InfoSpaceTestSuite::ItemAvailableEventNotification()
{ 
	 xdata::InfoSpace * is = xdata::InfoSpace::get("xdaq.cern.ch/infospace/parameters");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemAvailableEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );
	 is->addItemAvailableListener(l);
	 is->fireItemAvailable("testInteger", i, this);
	 bool eventReceived = l->eventReceived();
	 
	 is->removeItemAvailableListener(l);
	 is->fireItemRevoked("testInteger", this);
	 delete i;
	 delete l;
		 
	 CPPUNIT_ASSERT(eventReceived);
 
}

void InfoSpaceTestSuite::ItemRevokedEventNotification()
{
 
	 xdata::InfoSpace * is = xdata::InfoSpace::get("xdaq.cern.ch/infospace/parameters");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemRevokedEvent","testInteger", i );
	 is->addItemRevokedListener(l);
	 is->fireItemAvailable("testInteger", i, this);
	 is->fireItemRevoked("testInteger", this);
	
	 bool eventReceived = l->eventReceived();
	 
	 is->removeItemRevokedListener(l);
	 delete i;
	 delete l;
		 
	 CPPUNIT_ASSERT(eventReceived); 
}


void InfoSpaceTestSuite::GetNewInfoSpace()
{
 	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	CPPUNIT_ASSERT ( is );
	xdata::InfoSpace::remove("NewInfoSpace");
}

void InfoSpaceTestSuite::ItemFind()
{

	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	is->fireItemAvailable("testInteger", i, this);
	
	// find item
	xdata::Serializable *s = is->find("testInteger");
	CPPUNIT_ASSERT_EQUAL (dynamic_cast<xdata::Integer*>(s), i);
	
	delete i;
	xdata::InfoSpace::remove("NewInfoSpace");
}

void InfoSpaceTestSuite::ItemNotFound()
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	is->fireItemAvailable("testInteger", i, this);
	
	// find item
	CPPUNIT_ASSERT_THROW( is->find("unknown"), xdata::exception::Exception);
	
	delete i;
	xdata::InfoSpace::remove("NewInfoSpace");
}

void InfoSpaceTestSuite::GetExistingInfoSpace()
{
	xdata::InfoSpace * is1 = xdata::InfoSpace::get("NewInfoSpace");
	xdata::InfoSpace * is2 = xdata::InfoSpace::get("NewInfoSpace");
	CPPUNIT_ASSERT_EQUAL ( is1, is2 );
	xdata::InfoSpace::remove("NewInfoSpace");
}

void InfoSpaceTestSuite::DestroyInfoSpace()
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	is->fireItemAvailable("testInteger", i, this);
	
	// Destory infospace -> will loose the reference to the item
	xdata::InfoSpace::remove("NewInfoSpace");
	
	// Get infospace -> should be recreated and item should not be inside
	is = xdata::InfoSpace::get("NewInfoSpace");
	CPPUNIT_ASSERT_THROW( is->find("unknown"), xdata::exception::Exception);
	
	xdata::InfoSpace::remove("NewInfoSpace");
	delete i;
}

void InfoSpaceTestSuite::DestroyInfoSpaceWrongName()
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	
	CPPUNIT_ASSERT_THROW( xdata::InfoSpace::remove("unknown"), xdata::exception::Exception);
	xdata::InfoSpace::remove("NewInfoSpace");		
}



// TBD:


void InfoSpaceTestSuite::FireItemAvailableTwice() 
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	is->fireItemAvailable("testInteger", i, this);

	CPPUNIT_ASSERT_THROW( is->fireItemAvailable("testInteger", i, this), xdata::exception::Exception );

	xdata::InfoSpace::remove("NewInfoSpace");	
	delete i;
}


void InfoSpaceTestSuite::FireItemRevoked() 
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	is->fireItemAvailable("testInteger", i, this);

	is->fireItemRevoked("testInteger", this);
	
	CPPUNIT_ASSERT_THROW( is->find("testInteger"), xdata::exception::Exception);

	xdata::InfoSpace::remove("NewInfoSpace");	
	delete i;
}


void InfoSpaceTestSuite::FireItemRevokedTwice() 
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	is->fireItemAvailable("testInteger", i, this);

	is->fireItemRevoked("testInteger", this);
	CPPUNIT_ASSERT_THROW(is->fireItemRevoked("testInteger", this), xdata::exception::Exception);

	xdata::InfoSpace::remove("NewInfoSpace");	
	delete i;
}


void InfoSpaceTestSuite::RemoveAvailableListener() 
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	TestItemActionListener * l  = new TestItemActionListener("ItemAvailableEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );
	is->addItemAvailableListener(l);
	
	is->removeItemAvailableListener(l);
	
	is->fireItemAvailable("testInteger", i, this);
	bool eventReceived = l->eventReceived();

	is->fireItemRevoked("testInteger", this);

	xdata::InfoSpace::remove("NewInfoSpace");

	delete i;
	delete l;

	// Since the listener has been removed, the boolean will remain set false
	CPPUNIT_ASSERT(!eventReceived);
}


void InfoSpaceTestSuite::RemoveAvailableListenerWrongListener() 
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	TestItemActionListener * l  = new TestItemActionListener("ItemAvailableEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );
	is->addItemAvailableListener(l);
	
	// Remove non existing listener
	CPPUNIT_ASSERT_THROW(is->removeItemAvailableListener(0), xdata::exception::Exception);
	
	is->removeItemAvailableListener(l);

	xdata::InfoSpace::remove("NewInfoSpace");

	delete i;
	delete l;
}

void InfoSpaceTestSuite::RemoveRevokedListener() 
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	TestItemActionListener * l  = new TestItemActionListener("ItemRevokedEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );
	is->addItemRevokedListener(l);
	
	is->removeItemRevokedListener(l);
	
	is->fireItemAvailable("testInteger", i, this);
	is->fireItemRevoked("testInteger", this);
	
	bool eventReceived = l->eventReceived();

	xdata::InfoSpace::remove("NewInfoSpace");

	delete i;
	delete l;

	// Since the listener has been removed, the boolean will remain set false
	CPPUNIT_ASSERT(!eventReceived);
}

void InfoSpaceTestSuite::RemoveRevokedListenerWrongListener() 
{
	xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	xdata::Integer * i = new xdata::Integer();
	TestItemActionListener * l  = new TestItemActionListener("ItemAvailableEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );
	is->addItemRevokedListener(l);
	
	// Remove non existing listener
	CPPUNIT_ASSERT_THROW(is->removeItemRevokedListener(0), xdata::exception::Exception);
	
	is->removeItemRevokedListener(l);

	xdata::InfoSpace::remove("NewInfoSpace");

	delete i;
	delete l;
}


void InfoSpaceTestSuite::FireItemValueChanged() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemChangedEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemChangedListener("testInteger", l);
	 
	 // real Test!!!
	 is->fireItemValueChanged("testInteger", this);

	 bool eventReceived = l->eventReceived();
	 
	 is->removeItemChangedListener("testInteger", l);
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
		 
	 CPPUNIT_ASSERT(eventReceived);
}

void InfoSpaceTestSuite::FireItemValueChangedWrongName() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemChangedEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemChangedListener("testInteger", l);
	 
	 // real Test!!!
	 CPPUNIT_ASSERT_THROW( is->fireItemValueChanged("unknown", this), xdata::exception::Exception);
	 
	 is->removeItemChangedListener("testInteger", l);
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
}

void InfoSpaceTestSuite::FireItemValueRetrieve() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemRetrieveEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemRetrieveListener("testInteger", l);
	 
	 // real Test!!!
	 is->fireItemValueRetrieve("testInteger", this);

	 bool eventReceived = l->eventReceived();
	 
	 is->removeItemRetrieveListener("testInteger", l);
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
		 
	 CPPUNIT_ASSERT(eventReceived);
}

void InfoSpaceTestSuite::FireItemValueRetrieveWrongName() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemRetrieveEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemRetrieveListener("testInteger", l);
	 
	 // real Test!!!
	 CPPUNIT_ASSERT_THROW( is->fireItemValueRetrieve("unknown", this), xdata::exception::Exception);
	 
	 is->removeItemRetrieveListener("testInteger", l);
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
}

void InfoSpaceTestSuite::RemoveItemChangeListener() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemChangedEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemChangedListener("testInteger", l);
	 
	 // real Test!!! After remove listener, the callback is
	 // not called anymore.
	 is->removeItemChangedListener("testInteger", l);
	 is->fireItemValueChanged("testInteger", this);

	 bool eventReceived = l->eventReceived();
	 	
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
		 
	 CPPUNIT_ASSERT(!eventReceived);
}

void InfoSpaceTestSuite::RemoveItemChangeListenerWrongName() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemChangedEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemChangedListener("testInteger", l);
	 
	 // real Test!!!
	 CPPUNIT_ASSERT_THROW(  is->removeItemChangedListener("unknown", l), xdata::exception::Exception);
	 // real Test!!! After remove listener, the callback is
	 // not called anymore.
	 is->removeItemChangedListener("testInteger", l);
	
	 	
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
		 
}

void InfoSpaceTestSuite::RemoveItemChangeListenerWrongListener() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemChangedEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemChangedListener("testInteger", l);
	 
	 // real Test!!!
	 CPPUNIT_ASSERT_THROW(  is->removeItemChangedListener("testInteger", 0), xdata::exception::Exception);
	 // real Test!!! After remove listener, the callback is
	 // not called anymore.
	 is->removeItemChangedListener("testInteger", l);
	
	 	
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
}


void InfoSpaceTestSuite::RemoveItemRetrieveListener() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemRetrieveEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemRetrieveListener("testInteger", l);
	 
	 // real Test!!! After remove listener, the callback is
	 // not called anymore.
	 is->removeItemRetrieveListener("testInteger", l);
	 is->fireItemValueRetrieve("testInteger", this);

	 bool eventReceived = l->eventReceived();
	 	
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
		 
	 CPPUNIT_ASSERT(!eventReceived);
}

void InfoSpaceTestSuite::RemoveItemRetrieveListenerWrongName() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemRetrieveEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemRetrieveListener("testInteger", l);
	 
	 // real Test!!!
	 CPPUNIT_ASSERT_THROW(  is->removeItemRetrieveListener("unknown", l), xdata::exception::Exception);
	 // real Test!!! After remove listener, the callback is
	 // not called anymore.
	 is->removeItemRetrieveListener("testInteger", l);
	
	 	
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
}

void InfoSpaceTestSuite::RemoveItemRetrieveListenerWrongListener() 
{
	 xdata::InfoSpace * is = xdata::InfoSpace::get("NewInfoSpace");
	 xdata::Integer * i = new xdata::Integer();
	 TestItemActionListener * l  = new TestItemActionListener("ItemRetrieveEvent","testInteger", dynamic_cast<xdata::Serializable*>(i) );

	 is->fireItemAvailable("testInteger", i, this);	 
	 is->addItemRetrieveListener("testInteger", l);
	 
	 // real Test!!!
	 CPPUNIT_ASSERT_THROW(  is->removeItemRetrieveListener("testInteger", 0), xdata::exception::Exception);
	 // real Test!!! After remove listener, the callback is
	 // not called anymore.
	 is->removeItemRetrieveListener("testInteger", l);
	
	 	
	 is->fireItemRevoked("testInteger", this);
	 xdata::InfoSpace::remove("NewInfoSpace");
	 
	 delete i;
	 delete l;
}

