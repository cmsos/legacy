
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "jxta/AdvertisementListImpl.h"

jxta::AdvertisementListImpl::AdvertisementListImpl(Jxta_vector* advertisements): advertisements_(advertisements)
{
}
	
jxta::AdvertisementListImpl::~AdvertisementListImpl()
{
	if ( advertisements_ != 0 ) {
		JXTA_OBJECT_RELEASE(advertisements_);
	}	
}
		
int jxta::AdvertisementListImpl::getLength()
{
	 if (advertisements_ != 0)
		return jxta_vector_size(advertisements_);
	 else
		return 0; 
}

jxta::Advertisement::Reference jxta::AdvertisementListImpl::getItem(int i)
{
	if ( advertisements_ != 0 ) {
		Jxta_advertisement* adv;
		jxta_vector_get_object_at(advertisements_, (Jxta_object**)&adv, i);
		return Advertisement::Reference(new AdvertisementImpl(adv));
	}
	else
		return Advertisement::Reference(0);
}

void jxta::AdvertisementListImpl::addItem(jxta::Advertisement::Reference adv)
{
	Jxta_advertisement* j_adv = ((AdvertisementImpl&)(*adv)).getJxtaAdv();
	jxta_vector_add_object_last(advertisements_, (Jxta_object*)j_adv);
}

void jxta::AdvertisementListImpl::removeItem(int i)
{
	Jxta_object* adv;
	jxta_vector_remove_object_at(advertisements_, &adv, i);
	//JXTA_OBJECT_RELEASE(adv);    no need to cleanup, the associated Advertisement::Reference object will do it
}

