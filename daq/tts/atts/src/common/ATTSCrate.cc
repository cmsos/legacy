/**
*      @file ATTSCrate.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.10 $
*     $Date: 2007/03/27 07:53:25 $
*
*
**/
#include "tts/atts/ATTSCrate.hh"

#include "tts/atts/ATTSInterfaceCard.hh"
#include "tts/atts/ATTSHardcodedAddressTableReader.hh"

#include "hal/PCILinuxBusAdapter.hh"
#include "hal/PCIDummyBusAdapter.hh"
#include "hal/NoSuchDeviceException.hh"

#include "xcept/Exception.h"

#include <iostream>
#include <sstream>


tts::ATTSCrate* tts::ATTSCrate::_instance = 0;

tts::ATTSCrate::ATTSCrate(bool dummy) 
  throw (HAL::HardwareAccessException, xcept::Exception)
  : _busadapter(0),
    _atts_addresstable(0),
    _theATTSs() {

  try {
    if (dummy)
      _busadapter = new HAL::PCIDummyBusAdapter();
    else
      _busadapter = new HAL::PCILinuxBusAdapter();
  
    //
    // detect and instantiate ATTS Interface Cards
    //
    tts::ATTSHardcodedAddressTableReader reader;
    
    _atts_addresstable = new HAL::PCIAddressTable("ATTS address table", reader);

    uint32_t ATTSVendorID = 0xecd6;
    uint32_t ATTSDeviceID = 0xa115;

    uint32_t index = 0;
    try {
      for (; index < 16; index++) {
	HAL::PCIDevice* dev = new HAL::PCIDevice(*_atts_addresstable, *_busadapter, ATTSVendorID, ATTSDeviceID, index);
	_theATTSs.push_back( new tts::ATTSInterfaceCard(dev) );
      }
    }
    catch(HAL::NoSuchDeviceException & e) {
    }

  }
  catch (...) {
    cleanup();
    throw;   // rethrow any other exception after cleaning up
  }
}


tts::ATTSCrate* tts::ATTSCrate::getInstance() 
    throw (HAL::HardwareAccessException, xcept::Exception) {

  if (_instance == 0)
    _instance = new tts::ATTSCrate();

  return _instance;
}


// clean up all dynamically created objects belonging to ATTSCrate.
// only called upon exception in constructor and in desutuctor
void tts::ATTSCrate::cleanup() {
  
  /// delete ATTS PCI Devices
  std::vector<tts::ATTSInterfaceCard*>::iterator it = _theATTSs.begin();
  for (; it != _theATTSs.end(); it++)
    delete (*it);

  _theATTSs.clear();

  if (_atts_addresstable) {
    delete _atts_addresstable;
    _atts_addresstable = 0;
  }

  if (_busadapter) {
    delete _busadapter;
    _busadapter=0;
  }
}

tts::ATTSCrate::~ATTSCrate() {
  cleanup();
}



tts::ATTSInterfaceCard& tts::ATTSCrate::getATTSInterfaceCard( uint32_t id )     
  throw (xcept::Exception) {

  if ( id >= (uint32_t) _theATTSs.size() )
    XCEPT_RAISE (xcept::Exception, "ATTS with requested ID is not present.");

  return * (_theATTSs[id] );
}

tts::ATTSInterfaceCard& tts::ATTSCrate::getATTSInterfaceCardbySN( std::string const& sn ) 
  throw (xcept::Exception) {

  std::vector<tts::ATTSInterfaceCard *>::iterator it = _theATTSs.begin();
  for (; it != _theATTSs.end(); ++it) 
    if ( (*it)->getSerialNumber() == sn ) 
      return *( *it );

  XCEPT_RAISE (xcept::Exception, "ATTS with serial number '" + sn + "' not found in the crate.");
  
}

tts::ATTSInterfaceCard& tts::ATTSCrate::getATTSInterfaceCardbyGeoSlot( uint32_t geoslot )
  throw (xcept::Exception) {

  checkGeoSlotsUnique();

  std::vector<tts::ATTSInterfaceCard*>::iterator it = _theATTSs.begin();
  for (; it != _theATTSs.end(); ++it)
    if ( (*it)->getGeoSlot() == geoslot ) 
      return *( *it );
  
  std::stringstream msg;
  msg << "ATTS with geoslot number " <<  geoslot << " not found in the crate.";

  XCEPT_RAISE(xcept::Exception, msg.str() );
}

void tts::ATTSCrate::checkGeoSlotsUnique()
  throw (xcept::Exception) {

  // check that geoslots are unique to be sure that we are in a crate with proper backplane

  for (std::vector<tts::ATTSInterfaceCard*>::size_type id1=0; id1<numATTSInterfaceCards(); id1++) {
    for (std::vector<tts::ATTSInterfaceCard*>::size_type id2=0; id2<id1; id2++) {
      if ( _theATTSs[id1]->getGeoSlot() == _theATTSs[id2]->getGeoSlot() ) {
	std::stringstream msg;
	msg << "GeoSlots not unique. ATTSs on PCI index "
	    << id1 << " and " << id2 << " have same geoslot " << _theATTSs[id1]->getGeoSlot() ;

	XCEPT_RAISE(xcept::Exception, msg.str() );
      }
    }
  }

}
