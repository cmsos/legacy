#include "tcds/ici/HwStatusInfoSpaceUpdater.h"

#include "tcds/ici/TCADeviceICI.h"

tcds::ici::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                              tcds::ici::TCADeviceICI const& hw) :
  tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA(xdaqApp, hw),
  hw_(hw)
{
}

tcds::ici::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

tcds::ici::TCADeviceICI const&
tcds::ici::HwStatusInfoSpaceUpdater::getHw() const
{
  return hw_;
}
