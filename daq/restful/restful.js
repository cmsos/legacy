// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/


const addon = require('../alchemy/build/Release/alchemy');

var http = require("http");
var dns = require('dns');
var url = require('url');
const path = require('path');
const fs = require('fs');
var express = require('express');


var querystring = require('querystring');
var bodyParser = require('body-parser');
//
//context.init();

function foo(msg) {
	  console.log(msg);
	  console.log("Ciao Belli");
}

var pta = addon.getPeerTransportAgent();
var xgil = pta.getListener('cgi');
//
function processIncoming (req, res) { 
	var xgiinput = {
			'url':  req.headers["request_uri"], 
			'method' : req.method,
			'headers': req.headers, 
			'environment': req.environment,
			'data' : req.body
	};	
	req.xgiinput = xgiinput;

	console.log(req.xgiinput)
	var xgiout = xgil.processIncoming(req.xgiinput);
	//response.statusCode = xgiout.status;
	//response.statusMessage = xgiout.reason;
	//response.httpVersion = xgiout.version;
	xgiout.headers.forEach(function(value){
		var mimetype = value.split(':');
		//console.log(mimetype);
		//response.setHeader(mimetype[0], mimetype[1]);

	});


	// We access the ID param on the request object
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify(xgiout.data));

}

//
var rootDir = process.env.XDAQ_DOCUMENT_ROOT;
function getCallerIP(request) {
	var ip = request.headers['x-forwarded-for'] ||
	request.connection.remoteAddress ||
	request.socket.remoteAddress ||
	request.connection.socket.remoteAddress ||
	request.ip;
	ip = ip.split(',')[0];
	//console.log(ip);
	ip = (ip.split(':').slice(-1))[0]; //in case the ip returned in a format: "::ffff:146.xxx.xxx.xxx"
	return ip;
}


function getHost(fullhostname) {
	host = fullhostname.split(':')[0]; 
	return host;
}

function getPort(fullhostname) {
	port = fullhostname.split(':')[1]; 
	return port;
}

function getHostByIP(ip) {
	dns.reverse(ip, function(err, domains) {
		if(err) {
			console.log(err.toString());
			return "unknown";
		}
		//console.log(domains);
		return domains[0];
	});
}

function processEnvironment(options) {

	return function(request, response, next) {
		// Implement the middleware function based on the options object

			//
			// Add CGI/XDAQ specific headers 
			//
			request.headers["request_method"] = request.method;
			request.headers["request_uri"] = request.url;
			request.headers["server_protocol"] = 'HTTP/'+ request.httpVersion;
			request.headers["x-xdaq-receivetimestamp"] = new Date().valueOf();
			request.headers["x-xdaq-remote-addr"] = getCallerIP(request);
			// request.headers["x-xdaq-remote-host"] = getHostByIP(request.headers["x-xdaq-remote-addr"]); this is asynchronous cannot work
			request.headers["x-xdaq-remote-host"] = request.headers["x-xdaq-remote-addr"];  // let the user to find

			const request_url = url.parse(request.url);
			//console.log(request_url);
			var script = '';
			var path_info = '';
			var query_string = '';
			
			var content_type = '';

			if ( request.headers['content-type'] )
			{
				content_type = request.headers['content-type'] ;
			}
			var content_length = '';
			if ( request.headers['content-length'] )
			{
				content_length = request.headers['content-length'] ;
			}

			var accept = '';
			if ( request.headers['accept'] )
			{
				accept = request.headers['accept'] ;
			}

			var user_agent = '';
			if ( request.headers['user-agent'] )
			{
				user_agent = request.headers['user-agent'] ;
			}

			var referer = '';
			if ( request.headers['referer'] )
			{
				referer = request.headers['referer'] ;
			}

			var cookie = '';
			if ( request.headers['cookie'] )
			{
				cookie = request.headers['cookie'] ;
			}
			
			var auth_type = '';
			var remote_user = '';
			if ( request.headers['authorization'] )
			{
				var authorization = request.headers['authorization'] ;
				var auth_type_extract = (auth_type.split(' '))[1];
				if (auth_type_extract)
				{
					auth_type = auth_type_extract;
				}
				var remote_user_extract = (auth_type.split(' '))[2];
				if (remote_user_extract)
				{
					remote_user = Buffer.from(remote_user_extract, 'base64'); 
				}
			}

			//
			// Add  CGI/XDAQ specific environment 
			//
			//ENVIRONMENT ORIGINAL: SERVER_SOFTWARE=XDAQ/3.0
			//ENVIRONMENT ORIGINAL: SERVER_NAME=kvm-s3562-1-ip151-66.cms
			//ENVIRONMENT ORIGINAL: GATEWAY_INTERFACE=CGI/1.1
			//ENVIRONMENT ORIGINAL: SERVER_PROTOCOL=HTTP/1.1
			//ENVIRONMENT ORIGINAL: SERVER_PORT=1973
			//ENVIRONMENT ORIGINAL: REQUEST_METHOD=GET
			//ENVIRONMENT ORIGINAL: PATH_TRANSLATED=/urn:xdaq-application:service=hyperdaq
			//ENVIRONMENT ORIGINAL: SCRIPT_NAME=urn:xdaq-application:service=hyperdaq
			//ENVIRONMENT ORIGINAL: PATH_INFO=
			//ENVIRONMENT ORIGINAL: QUERY_STRING=
			//ENVIRONMENT ORIGINAL: REMOTE_HOST=kvm-s3562-1-ip150-16.cms
			//ENVIRONMENT ORIGINAL: REMOTE_ADDR=10.176.140.50
			//ENVIRONMENT ORIGINAL: AUTH_TYPE=
			//ENVIRONMENT ORIGINAL: REMOTE_USER=
			//ENVIRONMENT ORIGINAL: REMOTE_IDENT=
			//ENVIRONMENT ORIGINAL: CONTENT_TYPE=
			//ENVIRONMENT ORIGINAL: CONTENT_LENGTH=
			//ENVIRONMENT ORIGINAL: HTTP_ACCEPT=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
			//ENVIRONMENT ORIGINAL: HTTP_USER_AGENT=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:55.0) Gecko/20100101 Firefox/55.0
			//ENVIRONMENT ORIGINAL: REDIRECT_REQUEST=
			//ENVIRONMENT ORIGINAL: REDIRECT_URL=
			//ENVIRONMENT ORIGINAL: REDIRECT_STATUS=
			//ENVIRONMENT ORIGINAL: HTTP_REFERER=
			//ENVIRONMENT ORIGINAL: HTTP_COOKIE=
			//ENVIRONMENT ORIGINAL: ACCEPT_ENCODING=gzip, deflate
			//ENVIRONMENT ORIGINAL: ACCEPT_LANGUAGE=en-US,en;q=0.5

			//
			var environment = {
					'SERVER_SOFTWARE': 'XDAQ/3.0',
					'SERVER_NAME': getHost(request.headers['host']),
					'GATEWAY_INTERFACE' : 'CGI/1.1',
					'SERVER_PROTOCOL' : 'HTTP/'+ request.httpVersion,
					'SERVER_PORT': getPort(request.headers['host']),
					'REQUEST_METHOD': request.method,
					'PATH_TRANSLATED': request.url,
					'SCRIPT_NAME': script,        // actually the full path to the executing program
					'PATH_INFO': path_info,       // www.cern.ch/pippo.cgi/path -> path
					'QUERY_STRING': query_string,  // everthing after question mark
					'REMOTE_HOST':  request.headers["x-xdaq-remote-addr"],   //it comes from the socket connection
					'REMOTE_ADDR': request.headers["x-xdaq-remote-addr"],  // IP from client (it comes from the socket connection
					'AUTH_TYPE' : auth_type,
					'REMOTE_USER': remote_user,
					'REMOTE_IDENT': '',              // according RFC931
					'CONTENT_TYPE' :   content_type,
					'CONTENT_LENGTH':  content_length,
					'HTTP_ACCEPT':     accept,
					'HTTP_USER_AGENT': user_agent,
					//REDIRECT_REQUEST=
					//REDIRECT_URL=
					//REDIRECT_STATUS=
					'HTTP_REFERER':    referer,
					'HTTP_COOKIE':     cookie,
					'ACCEPT_ENCODING': request.headers["accept-encoding"],
					'ACCEPT_LANGUAGE': request.headers["accept-language"],
			};


		console.log(environment);

		request.environment = environment;
		next();
	}
}

function processByClass(req, res, next) {
	var script_name = 'urn:xdaq-application:class=' + req.params.type + ',instance=' + req.params.instance;
	var path_translated =  '/' +script_name + '/' + req.params.resource;

	req.environment["PATH_TRANSLATED"] = path_translated;
	req.environment["SCRIPT_NAME"] = script_name ;
	req.environment["PATH_INFO"] = req.params.resource ;

	next();
} 

function processByService(req, res, next) {
	var script_name = 'urn:xdaq-application:service=' + req.params.service ;
	var path_translated =  '/' +script_name + '/' + req.params.resource;

	req.environment["PATH_TRANSLATED"] = path_translated;
	req.environment["SCRIPT_NAME"] = script_name ;
	req.environment["PATH_INFO"] = req.params.resource ;

	next();
} 

function processByLID(req, res, next) {
	var script_name = 'urn:xdaq-application:lid=' + req.params.lid;
	var path_translated =  '/' +script_name + '/' + req.params.resource;
	req.environment["PATH_TRANSLATED"] = path_translated;
	req.environment["SCRIPT_NAME"] = script_name ;
	req.environment["PATH_INFO"] = req.params.resource ;

	next();
} 

	
class RESTful {
	   constructor(paramaters) {
	       this.paramaters = paramaters;
	   }

	   display() {
	       console.log(this.paramaters);
	   }
	   
	   // peer transport specific
	   service() {
			return "restful";
		}
		
	   protocol() {
			return "http";
		}
	   
	  

	   
	   config( host, port)
	   {
		   var application = express();
		   application.use(bodyParser.json()); // support json encoded bodies
		   application.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
		   application.use(processEnvironment({}));
		// Create the express router object for Photos
		   var router = express.Router();
		   // A GET to the root of a resource returns a list of that resource
		   router.get('/application/:lid/:resource', processByLID, processIncoming);
		   router.post('/application/:lid/:resource', processByLID, processIncoming);
		   router.get('/service/:service/:resource', processByService, processIncoming);
		   router.post('/service/:service/:resource', processByService, processIncoming);
		   router.get('/class/:type/instance/:instance/:resource', processIncoming);
		   router.post('/class/:type/instance/:instance/:resource', processByClass, processIncoming);


		// Attach the routers for their respective paths
		   application.use('/api', router);
		   
		  /* app.get('/', function (req, res) {
		      res.send('Hello World');
		   })*/

		   var server = application.listen(port, host, function () {
		     // var host = server.address().address
		     // var port = server.address().port
		      
		      console.log("Example app listening at http://%s:%s", host, port)
		   })


	   }
		 
} // end of class

module.exports = class Instantiator {
	constructor() {
	}
	
	create (args) {
		return new RESTful(args)
	}
	
	type() {
		return "RESTful";
	}
	
	
}




