package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Float extends SimpleItem {
	
	private java.lang.Float value;
	
	@Override
	public java.lang.Float getValue() {
		return value;
	}
	
	@Override
	public void setValue(Object value) {
		this.value = ((Number)value).floatValue();
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		out.writeFloat(value);
	}

	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = in.readFloat();
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.FLOAT.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
