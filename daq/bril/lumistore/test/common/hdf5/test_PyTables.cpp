#include "hdf5.h"
#include <string.h>
#include "hdf5_hl.h"

#define NFIELDS  (hsize_t)     5
#define NRECORDS (hsize_t)     0
#define NRECORDS_ADD (hsize_t) 2
#define TABLE_NAME             "table"


int main(){
  typedef struct Particle
  {
    char   name[16];
    int    lati;
    int    longi;
    float  pressure;
    double temperature;
  } Particle;
  
 /* Define an array of Particles */
 Particle  p_data[NRECORDS] = {
   //{"zero",0,0, 0.0f, 0.0},
   //{"one",10,10, 1.0f, 10.0},
   //{"two",  20,20, 2.0f, 20.0},
   //{"three",30,30, 3.0f, 30.0},
   //{"four", 40,40, 4.0f, 40.0},
   //{"five", 50,50, 5.0f, 50.0},
   //{"six",  60,60, 6.0f, 60.0},
   //{"seven",70,70, 7.0f, 70.0}
 };
 
/* Calculate the size and the offsets of our struct members in memory */
 size_t dst_size =  sizeof( Particle );
 size_t dst_offset[NFIELDS] = { HOFFSET( Particle, name ),
                                HOFFSET( Particle, lati ),
                                HOFFSET( Particle, longi ),
                                HOFFSET( Particle, pressure ),
                                HOFFSET( Particle, temperature )};

 // size_t dst_sizes[NFIELDS] = { sizeof( p_data[0].name),
 //                              sizeof( p_data[0].lati),
 //                              sizeof( p_data[0].longi),
 //                              sizeof( p_data[0].pressure),
 //                              sizeof( p_data[0].temperature)};
 size_t dst_sizes[NFIELDS] = { 16,
                               sizeof(int),
                               sizeof(int),
                               sizeof(float),
                               sizeof(double)};

 /* Define field information */
 const char *field_names[NFIELDS] =
 { "Name","Latitude", "Longitude", "Pressure", "Temperature" };
 hid_t      field_type[NFIELDS];
 hid_t      string_type;
 hid_t      file_id;
 hsize_t    chunk_size = 300;
 int        *fill_data = NULL;
 int        compress  = 0;
 herr_t     status;
 int        i;

  /* Append particles */
 Particle particle_in[ NRECORDS_ADD ] =
   {{ "eight",80,80, 8.0f, 80.0},
    {"nine",90,90, 9.0f, 90.0} };
 
 string_type = H5Tcopy( H5T_C_S1 );
 H5Tset_size( string_type, 16 );
 field_type[0] = string_type;
 field_type[1] = H5T_NATIVE_INT;
 field_type[2] = H5T_NATIVE_INT;
 field_type[3] = H5T_NATIVE_FLOAT;
 field_type[4] = H5T_NATIVE_DOUBLE;
 
  hid_t fid; 

  const char* filename="testc.hd5";
  fid = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  status = H5LTset_attribute_string(fid,"/","CLASS","GROUP");
  status = H5LTset_attribute_string(fid,"/","PYTABLES_FORMAT_VERSION","2.1");  
  status = H5LTset_attribute_string(fid,"/","TITLE","");
  status = H5LTset_attribute_string(fid,"/","VERSION","1.0");

  /* make a table */
  //herr_t H5TBmake_table( const char *table_title, hid_t loc_id, const char *dset_name, hsize_t nfields, const hsize_t nrecords, size_t type_size, const char *field_names [ ], const size_t *field_offset, const hid_t *field_types, hsize_t chunk_size, void *fill_data, int compress, const void *data )
  status=H5TBmake_table( "Table Title",fid,TABLE_NAME,NFIELDS,NRECORDS,
			 dst_size, field_names, dst_offset, field_type,
			 chunk_size, fill_data, compress, p_data  );
  status=H5TBappend_records(fid, TABLE_NAME,NRECORDS_ADD, dst_size, dst_offset, dst_sizes,&particle_in );

  H5Tclose( string_type );
  
  status = H5Fclose(fid);
}
