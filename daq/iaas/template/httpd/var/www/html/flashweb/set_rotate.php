<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
</head>

<body>
<?php
include_once ('tools.php');
$_zone = $_POST ['zone'];
if (! empty ( $_POST ['chk_group'] )) {
	// Loop to store and display values of individual checked checkbox.
	foreach ( $_POST ['chk_group'] as $selected ) {
		if (isset ( $_POST ['enable'] )) {
			echo "Enabling rotate for " . $_zone . '-' . strtolower ( $selected ) . '-template';
			$json = setRotate ( $_POST ['elasticsearchurl'], $_zone, $selected, "true" );
		}
		else {
			echo "Disabling rotate for " . $_zone . '-' . strtolower ( $selected ) . '-template';
			$json = setRotate ( $_POST ['elasticsearchurl'], $_zone, $selected, "false" );
		}
	}
}

echo '<br>';

echo '<form action="types.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '</form>'?>

</body>
</html>