// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_PipeConnectionClosedByPeer_h_
#define _pt_blit_PipeConnectionClosedByPeer_h_

#include <string>
#include "tcpla/EndPoint.h"

namespace pt
{
	namespace blit
	{

		class PipeConnectionClosedByPeer
		{

					friend class Dispatcher;
					friend class InputPipe;
					friend class PeerTransport;
			public:
					int getPipeIndex();
					int getSID();
			protected:

					PipeConnectionClosedByPeer(tcpla::EndPoint ep_handle, int index, int sid);
				tcpla::EndPoint getEndPoint();


			private:

				tcpla::EndPoint ep_handle_;
				int index_;
				int sid_;

		};

	}
}

#endif
