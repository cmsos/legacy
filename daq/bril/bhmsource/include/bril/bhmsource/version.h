// $Id$

/*************************************************************************
 * XDAQ Application Template                                             *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_bhmsource_version_h_
#define _bril_bhmsource_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILBHMSOURCE_VERSION_MAJOR 1
#define BRILBHMSOURCE_VERSION_MINOR 3
#define BRILBHMSOURCE_VERSION_PATCH 3
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILBHMSOURCE_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILBHMSOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRILBHMSOURCE_VERSION_MAJOR,BRILBHMSOURCE_VERSION_MINOR,BRILBHMSOURCE_VERSION_PATCH)
#ifndef BRILBHMSOURCE_PREVIOUS_VERSIONS
#define BRILBHMSOURCE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBHMSOURCE_VERSION_MAJOR,BRILBHMSOURCE_VERSION_MINOR,BRILBHMSOURCE_VERSION_PATCH)
#else
#define BRILBHMSOURCE_FULL_VERSION_LIST  BRILBHMSOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBHMSOURCE_VERSION_MAJOR,BRILBHMSOURCE_VERSION_MINOR,BRILBHMSOURCE_VERSION_PATCH)
#endif

namespace brilbhmsource
{
	const std::string package = "brilbhmsource";
	const std::string versions = BRILBHMSOURCE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ bhmsource";
	const std::string description = "bril bhm readout";
	const std::string authors = "N.Tosi S.Orfanelli";
	const std::string link = "";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
