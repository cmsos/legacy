###############################################################################
## Utility class representing a single register.
##
## NOTE: This is not very pretty, no. My defense is that it has to
## span both VME (HAL) and uTCA (uhal) needs.
###############################################################################

import math

import uhal

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class RegisterInfo(object):

    @classmethod
    def create_from_uhal_node(cls, name, hw, read_values=True):
        node = hw.getNode(name)
        address = node.getAddress()
        mask = node.getMask()
        permission = node.getPermission()
        read_write_mode = node.getMode()
        is_readable = (permission in [uhal.NodePermission.READ,
                                      uhal.NodePermission.READWRITE])
        is_writable = (permission in [uhal.NodePermission.WRITE,
                                      uhal.NodePermission.READWRITE])
        is_hierarchical = (read_write_mode == uhal.BlockReadWriteMode.HIERARCHICAL)
        is_block = (read_write_mode != uhal.BlockReadWriteMode.SINGLE)
        is_leaf = (not len(node.getNodes()))
        values = [None]
        if read_values and is_readable:
            if not is_hierarchical:
                if is_block:
                    node_size = node.getSize()
                    try:
                        values = node.readBlock(node_size)
                        hw.dispatch()
                    except Exception, err:
                        values = [None]
                else:
                    try:
                        tmp = node.read()
                        hw.dispatch()
                        values = [tmp.value()]
                    except Exception, err:
                        values = [None]
        res = cls(name, address, mask, is_readable, is_writable, is_block, is_hierarchical, is_leaf, values)
        # End of create_from_uhal_node().
        return res

    @classmethod
    def create_from_hal_node(cls, name, hw, read_values=True):
        node = hw.get_node(name)
        address = node.address
        mask = node.mask
        is_readable = node.readable
        is_writable = node.writable
        is_hierarchical = False
        is_block = False
        is_leaf = False
        values = [None]
        if read_values and is_readable:
            if not is_hierarchical:
                if is_block:
                    node_size = 4#node.getSize()
                    try:
                        values = node.read_block(node_size)
                    except Exception, err:
                        values = [None]
                else:
                    try:
                        tmp = hw.read(name)
                        values = [tmp]
                    except Exception, err:
                        values = [None]
        res = cls(name, address, mask, is_readable, is_writable, is_block, is_hierarchical, is_leaf, values)
        # End of create_from_hal_node().
        return res

    def __init__(self, name, address, mask, is_readable, is_writable, is_block, is_hierarchical, is_leaf, contents):
        self.name = name
        self.address = address
        self.mask = mask
        self.is_readable_ = is_readable
        self.is_writable_ = is_writable
        self.is_block_ = is_block
        self.is_hierarchical_ = is_hierarchical
        self.is_leaf_ = is_leaf
        self.contents = contents
        # End of __init__().

    def is_leaf(self):
        res = self.is_leaf_
        # End of is_leaf().
        return res

    def is_hierarchical(self):
        res = self.is_hierarchical_
        # End of is_hierarchical().
        return res

    def is_block(self):
        res = self.is_block_
        # End of is_block().
        return res

    def is_readable(self):
        res = self.is_readable_
        # End of is_readable().
        return res

    def is_writable(self):
        res = self.is_writable_
        # End of is_writable().
        return res

    def width(self, fmt="d"):
        # This is a bit clumsy, but I saw no other way to figure out
        # the width of the mask in bits.
        tmp_bin_str = bin(self.mask)[2:]
        ind = tmp_bin_str[::-1].find("1")
        tmp = tmp_bin_str[:len(tmp_bin_str) - ind]
        num_bits = len(tmp)
        res = None
        if fmt == "d":
            # Decimal. No clue how to specify the width.
            res = 0
        elif fmt == "b":
            # Binary. Easy.
            res = num_bits
        elif fmt == "h":
            # Hexadecimal. Not too bad either.
            res = int(math.ceil(num_bits / 4.))
        else:
            res = 0
        # End of width().
        return res

    # End of class RegisterInfo.

###############################################################################
