// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "xoap/domutils.h"

#if defined(XERCES_NEW_IOSTREAMS)
#include <iostream>
#else
#include <iostream.h>
#endif

#include "xalanc/Include/XalanMemoryManagement.hpp"
#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/framework/LocalFileInputSource.hpp"
#include <xercesc/parsers/XercesDOMParser.hpp>

#include "xalanc/PlatformSupport/XSLException.hpp"
#include "xalanc/DOMSupport/XalanDocumentPrefixResolver.hpp"
#include "xalanc/XPath/XObject.hpp"
#include "xalanc/XPath/XPathEvaluator.hpp"
#include "xalanc/XalanSourceTree/XalanSourceTreeDOMSupport.hpp"
#include "xalanc/XalanSourceTree/XalanSourceTreeInit.hpp"
#include "xalanc/XalanSourceTree/XalanSourceTreeParserLiaison.hpp"
#include "xalanc/XercesParserLiaison/XercesDocumentWrapper.hpp"

#include "xalanc/XercesParserLiaison/FormatterToXercesDOM.hpp"
#include "xalanc/XercesParserLiaison/XercesDOMFormatterWalker.hpp"
#include "xalanc/XercesParserLiaison/XercesDOMSupport.hpp"
#include "xalanc/XercesParserLiaison/XercesParserLiaison.hpp"
#include "xalanc/XalanTransformer/XercesDOMWrapperParsedSource.hpp"
#include "xalanc/XPath/XObject.hpp"
#include <xalanc/XPath/NodeRefList.hpp>

#include "tstore/DocumentFilter.h"

#include "xoap/SOAPElement.h"

#include "tstore/TStore.h"
#include <cgicc/FormEntry.h>
#include <cgicc/Cgicc.h>
#include "xdata/XMLDOM.h"
#include "xcept/Exception.h"
#include "toolbox/regex.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/NamespaceURI.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPFault.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/filter/MessageFilter.h"
#include "xoap/DOMParserFactory.h"

#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "xgi/framework/Method.h"
#include "xercesc/dom/DOMNode.hpp"

#include "xdaq/ApplicationContext.h"

#include "xdata/TableIterator.h"

#include "tstore/client/LoadDOM.h"
#include "tstore/client/AttachmentUtils.h"

#include <iterator>
#include <stdexcept>

#include "tstore/SQLView.h"
#include "tstore/NestedView.h"
#include "tstore/SyncManager.h"

//#include "tstore/GlobalStatelessConnectionPool.h"
#include "tstore/OraclePoolConnection.h"

XALAN_CPP_NAMESPACE_USE


//
// provides factory method for instantion of TStore application
//
XDAQ_INSTANTIATOR_IMPL(tstore::TStore)

static std::string CONNECTION_TIMEOUT_TIMER="Connection Timeout";

tstore::TStore::TStore(xdaq::ApplicationStub * s) : xdaq::Application(s), xgi::framework::UIManager(this) {
	xdata::InfoSpace* is = this->getApplicationInfoSpace();
	toolbox::task::getTimerFactory()->createTimer(CONNECTION_TIMEOUT_TIMER);
	s->getDescriptor()->setAttribute("icon","/tstore/images/tstore-icon.png");
	is->fireItemAvailable("configurationRootDirectory", &viewDirectory_);
	is->addItemChangedListener("configurationRootDirectory",this);

	//web interface
	xgi::framework::deferredbind(this, this, &TStore::Default, "Default");
	xgi::framework::deferredbind(this, this, &TStore::killConnection, "killConnection");
	
	//standard SOAP interface
	//https://twiki.cern.ch/twiki/bin/view/CMS/TStore_SOAP_Service_Protocol
	xoap::bind(this, &TStore::query, "query", TSTORE_NS_URI );
	xoap::bind(this, &TStore::update, "update", TSTORE_NS_URI );
	xoap::bind(this, &TStore::definition, "definition", TSTORE_NS_URI );
	xoap::bind(this, &TStore::insert, "insert", TSTORE_NS_URI );
	xoap::bind(this, &TStore::add, "add", TSTORE_NS_URI );
	xoap::bind(this, &TStore::connect, "connect", TSTORE_NS_URI );
	xoap::bind(this, &TStore::disconnect, "disconnect", TSTORE_NS_URI );
	xoap::bind(this, &TStore::renew, "renew", TSTORE_NS_URI );
	xoap::bind(this, &TStore::remove, "delete", TSTORE_NS_URI );
	xoap::bind(this, &TStore::clear, "clear", TSTORE_NS_URI );
	
	//administrative SOAP interface
	//https://twiki.cern.ch/twiki/bin/view/CMS/TStore_Administrative_SOAP_Service_Protocol
	xoap::bind(this, &TStore::addTable, "addTable", TSTORE_NS_URI );
	xoap::bind(this, &TStore::removeTable, "removeTable", TSTORE_NS_URI );
	xoap::bind(this, &TStore::destroy, "destroy", TSTORE_NS_URI );
	xoap::bind(this, &TStore::getConfiguration, "getConfiguration", TSTORE_NS_URI );
	xoap::bind(this, &TStore::setConfiguration, "setConfiguration", TSTORE_NS_URI );
	xoap::bind(this, &TStore::sync, "sync", TSTORE_NS_URI );
	xoap::bind(this, &TStore::addView, "addView", TSTORE_NS_URI );
	xoap::bind(this, &TStore::removeView, "removeView", TSTORE_NS_URI );
	xoap::bind(this, &TStore::getViews, "getViews", TSTORE_NS_URI );
}

tstore::TStore::~TStore() {
	for (std::map<std::string,tstore::View *>::iterator viewIterator=views_.begin();viewIterator!=views_.end();viewIterator++) {
		try {
			delete (*viewIterator).second;
			views_.erase(viewIterator);
		} catch(tstore::exception::Exception& e) {
			LOG4CPLUS_ERROR(this->getApplicationLogger(), toolbox::toString("tstore::TStore::~TStore(): %s", e.what()));
		}
	}
	for (tstore::ConnectionList::iterator connection=connections_.begin();connection!=connections_.end();++connection) {
		tstore::Connection *currentConnection=(*connection).first;
		++connection; //increment it now, because afterwards, that iterator will be invalid
		closeConnection(currentConnection);
	}
}

/**
Close all open connections to a view
*/
void tstore::TStore::closeConnectionsToView(const std::string &viewName) {
	LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Connection information for view "+viewName+" has changed. Closing existing connections.");
	for (tstore::ConnectionList::iterator connection=connections_.begin();connection!=connections_.end();) {
		if ((*connection).second==viewName) {
			tstore::Connection *currentConnection=(*connection).first;
			++connection; //increment it now, because afterwards, that iterator will be invalid
			removeConnectionTimerForConnection(currentConnection);
			closeConnection(currentConnection);
		} else {
			++connection;
		}
	}
}

void tstore::TStore::closeConnection(tstore::Connection *connection) {
	LOG4CPLUS_INFO(this->getApplicationLogger(),"Closing connection "+IDFromConnection(connection));
	connection->closeConnection();
	delete connection;
	connections_.erase(connection);
	connectionDates_.erase(connection);
	//connectionUsernames_.erase(connection);
}

/** reads a data type mapping of the form
<tstore:maptype source="xdata type" [table="pattern"] [column="pattern"] [target="sql type"] [format="format string"]/>
from the node \a mappingNode.
This is just for compatibility with old configuration files, people should be using the new way of defining
data types (using <table name="tablename"><column name="columnname" type="xdata type"/>)
See https://twiki.cern.ch/twiki/bin/view/CMS/TStore_View_Configuration_File#4_2_Previous_Version
*/
void tstore::TStore::readOldStyleMapping(tstore::MappingList &mappings, DOMNode *mappingNode) {
	std::string xdataType = xoap::getNodeAttribute(mappingNode, "source");
	std::string table = xoap::getNodeAttribute(mappingNode, "table");
	std::string column = xoap::getNodeAttribute(mappingNode, "column");
	std::string dbType = xoap::getNodeAttribute(mappingNode, "target");
	std::string format = xoap::getNodeAttribute(mappingNode, "format");
	tstore::Mapping mapping(xdataType,column,table,dbType,format);
	if (tstore::OracleConnection::isMappingTypeValid(mapping.dbType())) {
		mappings.push_back(mapping);
	} else {
		LOG4CPLUS_ERROR(this->getApplicationLogger(),"Mapping target '"+mapping.dbType()+"' is not recognised. This mapping will not be used.");
		std::vector<std::string> types(tstore::OracleConnection::mappingTypeList());
		std::ostringstream typeString;
		typeString << "Available mapping types for Oracle are: ";
		std::copy(types.begin(),types.end(),std::ostream_iterator<std::string>(typeString, ","));
		LOG4CPLUS_INFO(this->getApplicationLogger(),typeString.str());
	}
}

void tstore::TStore::addColumnDefinition(tstore::MappingList &mappings, const std::string &xdataType,const std::string &columnName,const std::string &tableName)  {
	tstore::Mapping mapping(xdataType,columnName,tableName);
	mapping.setMatchesExactly();
	mappings.push_back(mapping);
}

/**
Reads a foreign key specification from \a node of the form
<foreignkey table="otherTable">
	<keycolumn column="columnInThisTable" references="columnInOtherTable/>
	<keycolumn column="anotherColumnInThisTable" references="anotherColumnInOtherTable/>
</foreignKey>

See https://twiki.cern.ch/twiki/bin/view/CMS/TStore_View_Configuration_File#5_2_Foreign_key
*/
void tstore::TStore::readForeignKey
(
	std::map<std::string, std::string, xdata::Table::ci_less> &linkedColumns,
	DOMNode *node
) 
 
{
	DOMNodeList* columns = node->getChildNodes();
	unsigned long columnCount=columns->getLength();
	for (unsigned long columnIndex = 0; columnIndex < columnCount; columnIndex++) {
		DOMNode *columnNode = columns->item(columnIndex);
		std::string nodePrefix=xoap::XMLCh2String(columnNode->getNamespaceURI());
		if (nodePrefix=="urn:xdaq-tstore:1.0") {
			std::string tagName=xoap::XMLCh2String(columnNode->getLocalName());
			if (tagName=="keycolumn") {
				std::string columnName = toolbox::toupper(xoap::getNodeAttribute(columnNode, "column"));
				std::string referencedColumn = toolbox::toupper(xoap::getNodeAttribute(columnNode, "references"));
				//std::cout << " column " << columnName << " references " << referencedColumn << std::endl;
				if (columnName.empty() || referencedColumn.empty()) {
					XCEPT_RAISE(tstore::exception::Exception,"Attribute missing in keycolumn tag");
				}
				if (linkedColumns.count(columnName)) {
					XCEPT_RAISE(tstore::exception::Exception,"Foreign key uses the column '"+columnName+"' more than once.");
				}
				linkedColumns[columnName]=referencedColumn;
			} else {
				XCEPT_RAISE(tstore::exception::Exception,"Unknown tag found in definition of foreign key: "+tagName);
			}
		}
	}
}

/**
Reads the new style of mapping, as specified at https://twiki.cern.ch/twiki/bin/view/CMS/TStore_View_Configuration_File#4_Data_type_mappings
*/
void tstore::TStore::readTableMappings(tstore::MappingList &mappings, DOMNode *tableNode,xdata::Table &definition,std::vector<tstore::ForeignKey> &foreignKeys)  {
	try {
		std::string tableName = xoap::getNodeAttribute(tableNode, "name");
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"reading mappings for table "+tableName);
		DOMNodeList* columns = tableNode->getChildNodes();
		unsigned long columnCount=columns->getLength();
		for (unsigned long columnIndex = 0; columnIndex < columnCount; columnIndex++) {
			DOMNode *columnNode = columns->item(columnIndex);
			std::string nodePrefix=xoap::XMLCh2String(columnNode->getNamespaceURI());
			if (nodePrefix=="urn:xdaq-tstore:1.0") {
				std::string tagName=xoap::XMLCh2String(columnNode->getLocalName());
				if (tagName=="column") {
					std::string xdataType = xoap::getNodeAttribute(columnNode, "type");
					std::string columnName = xoap::getNodeAttribute(columnNode, "name");
					LOG4CPLUS_DEBUG(this->getApplicationLogger(),"found column with name: "+columnName+" and type: "+xdataType);
					if (xdataType.empty() || columnName.empty()) {
						XCEPT_RAISE(tstore::exception::Exception,"Attribute missing in column tag");
					}
					addColumnDefinition(mappings,xdataType,columnName,tableName);
					LOG4CPLUS_DEBUG(this->getApplicationLogger(),"about to add column");
					definition.addColumn(toolbox::toupper(columnName),xdataType);
					LOG4CPLUS_DEBUG(this->getApplicationLogger(),"added column");
				} else if (tagName=="foreignkey") {
					tstore::ForeignKey newKey;
					newKey.childTableName=xoap::getNodeAttribute(columnNode, "references");
					if (newKey.childTableName.empty()) {
						XCEPT_RAISE(tstore::exception::Exception,"Foreign key with no table found");
					} 
					readForeignKey(newKey.linkedColumns,columnNode);
					if (newKey.linkedColumns.empty()) {
						XCEPT_RAISE(tstore::exception::Exception,"Foreign key with no columns found");
					} 
					foreignKeys.push_back(newKey);
				} else {
					//this should be an exception instead, for when it is reloaded.
					//on the other hand, this will all be done automatically if I make a schema and validate against it.
					XCEPT_RAISE(tstore::exception::Exception,"Unknown tag found in definition of table '"+tableName+"': "+tagName);
				}
			}
		}
	} catch (xcept::Exception &e) {
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"could not read table mapping: "+(std::string)e.what());
		XCEPT_RETHROW (tstore::exception::Exception, "Could not read table mapping", e);
	}
}

bool tstore::TStore::keysMatch(const std::string &keys1,const std::string &keys2) {
	std::vector<std::string> keySet1,keySet2;
	//use vectors because the order is important
	std::back_insert_iterator<std::vector<std::string> > iterator1(keySet1);
	std::back_insert_iterator<std::vector<std::string> > iterator2(keySet2);
	tstore::separateKeys(iterator1,keys1);
	tstore::separateKeys(iterator2,keys2);
	//std::cout << ((keySet1==keySet2)?"match":"do not match") << std::endl;
	return keySet1==keySet2;
}

void tstore::TStore::readMappings(tstore::MappingList &mappings, tstore::TableList &tables,DOMNode *node)  {
	try {
		std::vector<DOMNode *> tstoreNodes(tstoreclient::nodesWithPrefix(node,"urn:xdaq-tstore:1.0"));
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"got nodes with prefix");
		for (std::vector<DOMNode *>::iterator node=tstoreNodes.begin();node!=tstoreNodes.end();++node) {
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"got a node");
			if (xoap::XMLCh2String((*node)->getLocalName())=="maptype") {
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"reading an old-style mapping node");
				readOldStyleMapping(mappings,(*node));
			} else if (xoap::XMLCh2String((*node)->getLocalName())=="table") {
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"reading a new-style mapping node");
				tstore::TableDefinition definition;
				readTableMappings(mappings,(*node),definition.definition,definition.foreignKeys);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"read table mappings");
				definition.key=toolbox::toupper(xoap::getNodeAttribute(*node, "key"));
				definition.name=xoap::getNodeAttribute(*node, "name");
				//should there be an error here if the name does not match the pattern for that view?
				tables.push_back(definition);
			}
		}
	} catch (xcept::Exception &e) {
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"could not read mappings: "+(std::string)e.what());
		XCEPT_RETHROW (tstore::exception::Exception, "Could not read mappings", e);
	}
}

//reads connection information from the DOMNode given.
//Connection information should be in the view configuration file in the format
//<tstore:connection dbname="name" username="name" [password="name"]/>
//returns whether there was any connection information found.
//[ 1810251 ] Change authentication for connecting to views
//no longer needs a username in the configuration file
bool tstore::TStore::readConnectionParameters(DOMNode *node,std::string &database)  {
	DOMNodeList* connections = node->getChildNodes();
	bool foundConnection=false;
	unsigned long connectionCount=connections->getLength();
	for (unsigned long connectionIndex = 0; connectionIndex < connectionCount;connectionIndex++) {
		DOMNode *connectionNode = connections->item(connectionIndex);
		if (xoap::XMLCh2String(connectionNode->getLocalName())=="connection") {
			if (foundConnection) {
				XCEPT_RAISE(tstore::exception::Exception,"The view has more than one connection tag.");
			} else {
				std::string username;
				foundConnection=true;
				DOMNode* connectionNode = connections->item(connectionIndex);
				database = xoap::getNodeAttribute(connectionNode, "dbname");
				username = xoap::getNodeAttribute(connectionNode, "username");
				if (!username.empty()) {
					LOG4CPLUS_WARN(this->getApplicationLogger(),"Connection parameters include a username. This is no longer supported; the username must be given in the credentials attribute in the connect message.");
				}
			}
		}
	}
	if (!foundConnection) {
		XCEPT_RAISE(tstore::exception::Exception,"The view has no connection tag."); 
		//shouldn't add the view in this case.
		return false;
	}
	return true;
}

tstore::View * tstore::TStore::createView(const std::string &configPath,const std::string &viewName)  {
	std::string className;
	std::string::size_type firstColon=viewName.find(':');
	if (firstColon!=std::string::npos) {
		std::string::size_type secondColon=viewName.find(':',firstColon+1);
		if (secondColon!=std::string::npos) {
			className=viewName.substr(0,secondColon);
		}
	}
	//this will eventually choose from dynamically loaded view classes... but for now it's just SQLView and NestedView
	if (className=="urn:tstore-view-SQL"/*tstore::SQLView::namespaceURI()*/) {
		return new tstore::SQLView(configPath,viewName);
	} else if (className=="urn:tstore-view-Nested"/*tstore::NestedView::namespaceURI()*/) {
		return new tstore::NestedView(configPath,viewName);
	} else {
		XCEPT_RAISE(tstore::exception::InvalidView, "Unknown view class: "+className);
	}
}

bool tstore::TStore::isViewNode(const DOMNode *node) {
	return xoap::XMLCh2String(node->getLocalName())=="view" && xoap::XMLCh2String(node->getNamespaceURI())=="urn:xdaq-tstore:1.0";
}

void tstore::TStore::loadView(DOMNode* viewNode,const std::string &viewPath) {
	try {
		std::string viewName = xoap::getNodeAttribute(viewNode, "id");
		if (!viewName.empty()) {
			std::string database;
			//std::string password;
			try {
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"about to read connection parameters");
				readConnectionParameters(viewNode,database);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"loaded connection parameters");
				tstore::MappingList mappings;
				tstore::TableList tables;
				tstore::View *view=NULL;
				view=createView(viewPath,viewName);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"created view");
				if (!view) XCEPT_RAISE(tstore::exception::Exception,"Could not instantiate view object.");
				readMappings(mappings,tables,viewNode); //this is done here rather than in the view because there may be global mappings as well
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"read mappings");
				addView(view,viewName,mappings,tables,database,viewPath);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"added view");
			} catch (tstore::exception::Exception &e) {
				LOG4CPLUS_ERROR(this->getApplicationLogger(),"Could not load view with ID '"+viewName+"': "+e.what());
			} 
		} else {
			LOG4CPLUS_ERROR(this->getApplicationLogger(),"View with no id found.");
		}
	} catch (xcept::Exception &e) {
		LOG4CPLUS_ERROR(this->getApplicationLogger(),"Could not read view ID: "+(std::string)e.what());
	}
}

void tstore::TStore::loadViewsFromFile(const std::string &viewsPath)  {
	try {
		//xdaq::ApplicationContextImpl *impl=dynamic_cast<xdaq::ApplicationContextImpl*>(this->getApplicationContext());
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Loading views from file: "+viewsPath);
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(tstoreclient::parsePath(viewsPath)); //The parser owns the returned DOMDocument. It will be deleted when the parser is released.
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"parsed file");
		if (doc) {
			DOMNodeList* lists = doc->getElementsByTagNameNS(xoap::XStr("urn:xdaq-tstore:1.0"), xoap::XStr("view"));
			for (unsigned long i = 0; i < lists->getLength(); i++) {
				DOMNode* viewNode = lists->item(i);
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"about to load a view");
				loadView(viewNode,viewsPath);
			}
		}
		viewError_="";
	}	
	catch (xcept::Exception& e) {
		viewError_=e.message();
		std::string msg = "Loading view configuration from URL '";
		msg += viewsPath;
		msg += "' failed";
		XCEPT_RETHROW (tstore::exception::Exception, msg, e);
	}
}

std::string tstore::TStore::pathToViewFile(const std::string &fileName) {
	std::ostringstream path;
	path << (std::string)viewDirectory_ << "/" << fileName << ".view";
	return path.str();
}

std::vector<std::string> tstore::TStore::getViewConfigFileNames() {
	std::string pathExpression=pathToViewFile("*");
	return toolbox::getRuntime()->expandPathName(pathExpression);
}

void tstore::TStore::loadViews()  {
	std::vector<std::string> paths=getViewConfigFileNames();
	for (std::vector<std::string>::iterator path = paths.begin(); path != paths.end(); path++) {
		loadViewsFromFile(*path);
	}
}

//If I squint I can pretend ActionListener is a protocol
void tstore::TStore::actionPerformed(xdata::Event & received ) {
	xdata::ItemEvent& event = dynamic_cast<xdata::ItemEvent&>(received);
	try {
		if (event.itemName()=="configurationRootDirectory") {
			loadViews();
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"loaded views");
			
		}
	} catch (xcept::Exception &e) {
		LOG4CPLUS_ERROR(this->getApplicationLogger(),e.message());
	}
}

std::string tstore::TStore::getNamedAttribute(DOMNamedNodeMap *attributes,const std::string &name) {
	if (attributes) {
		DOMNode *item=attributes->getNamedItemNS(xoap::XStr(TSTORE_NS_URI),xoap::XStr(name));
		if (item) {
			return xoap::XMLCh2String(item->getNodeValue());
		}
	}
	return ""; //perhaps should throw an exception here, but what would we do with it?
}

//this should be in xoap or somewhere
std::string tstore::TStore::readCDATAFromChild(DOMNode *node) {
	DOMNodeList *children=node->getChildNodes();
	int childrenCount=children->getLength();
	int childIndex;
	for (childIndex=0;childIndex<childrenCount;childIndex++) {
		DOMNode *child=children->item(childIndex);
		if (child->getNodeType()==DOMNode::CDATA_SECTION_NODE) {
			return xoap::XMLCh2String(child->getNodeValue()); //let's not trim it, just in case it needs the spaces
		}
	}
	return "";
}

//this version uses attributes in the command (e.g, <query>, <update>) tag like this: view-namespace:parameterName="parameterValue"
void tstore::TStore::setViewParameters(tstore::View &view,DOMNode *command)  {
	view.resetParameters();
	DOMNamedNodeMap *parameterList=command->getAttributes();
	for (unsigned int parameterIndex = 0; parameterIndex < parameterList->getLength(); parameterIndex++) {
		DOMNode* parameterNode=parameterList->item(parameterIndex);
		if (xoap::XMLCh2String(parameterNode->getNamespaceURI())==view.namespaceURI()) {
			std::string parameterName=xoap::XMLCh2String(parameterNode->getLocalName());
			std::string parameterValue=xoap::XMLCh2String(parameterNode->getNodeValue());
			if (parameterName!="") try {
				view.setParameter(parameterName,parameterValue);
			} catch (tstore::exception::Exception &e) {
				XCEPT_RETHROW(tstore::exception::Exception,"Could not add parameter '"+parameterName+"' to view"+view.name()+". "+e.what(),e);
			}
		}
	}
}

/// \return the element of \a msg containing the command, e.g. <tstoresoap:insert...> 
/// throws an exception if there is no such element (in which case no callback should have even been called)
DOMElement* tstore::TStore::getCommand(xoap::MessageReference msg)  {
 	xoap::SOAPEnvelope envelope = msg->getSOAPPart().getEnvelope();
 	xoap::SOAPBody body = envelope.getBody();
 	DOMNode* node = body.getDOMNode();
	DOMNodeList* bodyList = node->getChildNodes();
	for (unsigned int i = 0; i < bodyList->getLength(); i++) {
		DOMNode* command = bodyList->item(i);
		if (command->getNodeType() == DOMNode::ELEMENT_NODE) {
			DOMElement *element=dynamic_cast<DOMElement *>(command);
			if (!element) {
				XCEPT_RAISE(tstore::exception::Exception,"Node is not really an element");
			}
			return element;
		}
	}
	XCEPT_RAISE(tstore::exception::Exception,"No command element");
}

//gets an attribute of the main command in the message
std::string tstore::TStore::getCommandAttribute(xoap::MessageReference msg,const std::string &attributeName)  {
	DOMNode* command = getCommand(msg);
	DOMNamedNodeMap *attributes=command->getAttributes();
	return getNamedAttribute(attributes,attributeName);
}

//gets an attribute of the main command in the message
//throws an exception if it is empty
std::string tstore::TStore::getCompulsoryCommandAttribute(xoap::MessageReference msg,const std::string &attributeName)  {
	std::string attribute=getCommandAttribute(msg,attributeName);
	if (attribute.empty()) {
		XCEPT_RAISE(tstore::exception::InvalidView, "No "+ attributeName+" provided");
	}
	return attribute;
}

//returns the name of the view specified as an attribute of the command
//throws an exception if there is no view specified, or if there is no view by that name.
std::string tstore::TStore::getView(xoap::MessageReference msg)  {
	std::string viewName(getCompulsoryCommandAttribute(msg,"id"));
	if (!views_.count(viewName)) {
		XCEPT_RAISE(tstore::exception::InvalidView, "There is no view named '"+viewName);
	}
	return viewName;
}

tstore::View* tstore::TStore::viewForConnection(tstore::Connection *connection,xoap::MessageReference msg)  {
	tstore::View *view=viewForConnection(connection);
	setViewParameters(*view,getCommand(msg));
	return view;
}

tstore::View* tstore::TStore::viewForConnection(tstore::Connection *connection)  {
	std::string viewName(connections_[connection]);
	if (views_.count(viewName) && views_[viewName]) {
		return views_[viewName];
	} else {
		XCEPT_RAISE(tstore::exception::InvalidView, "The connection ID is associated with view '"+viewName+"' which no longer exists.");
	}
}

tstore::Connection* tstore::TStore::connectionFromMessage(xoap::MessageReference msg)  {
	std::string connectionID=getCompulsoryCommandAttribute(msg,"connectionID");
	return connectionFromID(connectionID);
}

void tstore::TStore::addFault(xcept::Exception &e,const std::string &task)  {
	xcept::ExceptionHistory exHistory(e);
	string msg = "Failed to '" + task + "'";

	while(exHistory.hasMore()) {
		xcept::ExceptionInformation& exInfo = exHistory.getPrevious();

		msg += "<-- " + exInfo.getProperty("message") +
			" raised at " + exInfo.getProperty("function") +
			"(" +  exInfo.getProperty("module") +
			":" + exInfo.getProperty("line") + ")";
	}

	LOG4CPLUS_ERROR(this->getApplicationLogger(), msg);
	XCEPT_RETHROW(xoap::exception::Exception,"Could not "+task,e);
}

xoap::MessageReference tstore::TStore::createResponse (const std::string &name,std::map<std::string,std::string> *attributes)  {
	xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope responseEnvelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = responseEnvelope.createName( name, "tstoresoap", TSTORE_NS_URI);
    xoap::SOAPElement element = responseEnvelope.getBody().addBodyElement ( responseName );
	
	if (attributes) {
		std::map<std::string,std::string>::iterator attribute;
		for (attribute=attributes->begin();attribute!=attributes->end();++attribute) {
			xoap::SOAPName id = responseEnvelope.createName((*attribute).first);
			element.addAttribute(id, (*attribute).second);    
		}
	}
	return reply;
}

tstore::MappingList& tstore::TStore::mappingsForView(const std::string &viewName)  {
	if (viewMappings_.count(viewName)) {
		return viewMappings_[viewName];
	} else {
		XCEPT_RAISE(tstore::exception::Exception, "No mappings for view '"+viewName+"'");
	}
}

tstore::MappingList& tstore::TStore::mappingsForView(tstore::View *view)  {
	if (view) {
		return mappingsForView(view->name());
	} else {
		XCEPT_RAISE(tstore::exception::InvalidView, "No view given");
	}
}

tstore::TableList& tstore::TStore::tablesInView(const std::string &viewName)  {
	if (viewTables_.count(viewName)) {
		return viewTables_[viewName];
	} else {
		XCEPT_RAISE(tstore::exception::Exception, "No tables for view '"+viewName+"'");
	}
}

tstore::TableList& tstore::TStore::tablesInView(tstore::View *view)  {
	if (view) {
		return tablesInView(view->name());
	} else {
		XCEPT_RAISE(tstore::exception::InvalidView, "No view given");
	}
}

xoap::MessageReference tstore::TStore::update (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("updateResponse");
    
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		xdata::Table newData;
		try {
			tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view));
			view->update(&API);
		} catch (tstore::exception::Exception &e) {
			addFault(e,"update view "+view->name());
		}
	} catch (xcept::Exception &e) {
		addFault(e,"update");
	}
	return reply;
}

//get the names of the tables specified in the message and puts them into tableNames. These should be children of the <addTable> tag in the format:
//<table name="ORACLE TABLE NAME" type="xdata type"/>
void tstore::TStore::getTableNames(xoap::MessageReference msg,std::map<const std::string,std::string> &tableNames)  {
 	DOMNode* node = getCommand(msg);
	DOMNodeList* bodyList = node->getChildNodes();
	for (unsigned int i = 0; i < bodyList->getLength(); i++) {
		DOMNode* child = bodyList->item(i);
		if (child->getNodeType() == DOMNode::ELEMENT_NODE) {
			std::string nodeName=xoap::XMLCh2String(child->getLocalName());
			if (nodeName=="table") {
				std::string tableName=xoap::getNodeAttribute(child, "name");
				std::string primaryKey=xoap::getNodeAttribute(child, "key");	
				if (!tableName.empty()) {
					//tableNames.push_back(tableName);
					tableNames[tableName]=primaryKey;
				} else {
					XCEPT_RAISE(tstore::exception::Exception,"Table tag with no 'name' attribute.");
				}
			} else {
				XCEPT_RAISE(tstore::exception::Exception,"Unknown '"+nodeName+"' element in addTable message. TStore only understands 'table' elements.");
			}
		}
	}
	if (tableNames.empty()) {
		XCEPT_RAISE(tstore::exception::Exception,"No table names sent.");
	}
}

std::string tstore::TStore::configurationPathForView(const std::string &viewName)  {
	if (viewConfiguration_.count(viewName)) return viewConfiguration_[viewName];
	XCEPT_RAISE(tstore::exception::Exception,"Could not get path for view '"+viewName+"'");
}

/// \return the node from the configuration which corresponds to the
/// <view id="viewName">...</view> element
DOMNode* tstore::TStore::getNodeForView(DOMDocument* doc,const std::string &viewName)  {
	if (doc) {
		DOMNodeList* lists = doc->getElementsByTagNameNS(xoap::XStr("urn:xdaq-tstore:1.0"), xoap::XStr("view"));
		for (unsigned long i = 0; i < lists->getLength(); i++) {
			DOMNode* viewNode = lists->item(i);
			std::string thisViewName = xoap::getNodeAttribute(viewNode, "id");
			if (viewName==thisViewName) {
				return viewNode;
			}
		}
	}
	//no configuration found for the view, even though there must have been some in the beginning because to get this far we must have the view in memory.
	XCEPT_RAISE(tstore::exception::Exception,"Could not find configuration for view "+viewName+". The configuration file must have been changed since TStore launched.");
}

/// writes a backup of the file at path at path+"~"
/// throws an exception in case of error
void tstore::TStore::writeBackup(const std::string &path)  {
	std::string backupPath=path+"~";
	try {
		std::ifstream oldFile( path.c_str() );
		if (!oldFile) {
			XCEPT_RAISE(tstore::exception::Exception,"Could not back up view configuration file. Could not open file "+path+" for reading.");
		}
		
		std::ofstream backup (backupPath.c_str());
		
		if (!backup) {
			oldFile.close();
			XCEPT_RAISE(tstore::exception::Exception,"Could not back up view configuration file. Could not open file "+backupPath+" for writing.");
		}
		char c;
		while( oldFile.get( c ) ) backup.put(c);
		oldFile.close();
		backup.close();
	} catch (std::exception &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Unknown error writing backup of "+path+" at "+backupPath+": "+e.what());
	}
}

/// creates a backup of the file at \a viewConfigPath and writes \a doc to that path.
void tstore::TStore::writeConfig(DOMDocument* doc,const std::string &viewConfigPath)  {
	std::string config;
	try {
		std::string path=tstoreclient::parseLocalPath(viewConfigPath);
		writeBackup(path);
		//std::cout << writeXML(doc) << std::endl;
		tstoreclient::writeXML(doc,path);
	} catch (xcept::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not write configuration",e);
	}
}

bool tstore::TStore::getDryRun(xoap::MessageReference msg)  {
	return toolbox::toupper(getCommandAttribute(msg,"test"))=="TRUE";
}

xoap::MessageReference tstore::TStore::removeTable(xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("removeTableResponse");
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		std::string viewConfigPath(configurationPathForView(view->name()));
		std::string localPath=tstoreclient::parseLocalPath(viewConfigPath);
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(localPath);
		DOMNode *viewNode=getNodeForView(doc,view->name());
		tstore::MappingList oldMappings=mappingsForView(view);
		try {
			std::map<const std::string,std::string> tableNames;
			bool dryRun=getDryRun(msg);
			//get the table names from the message
			getTableNames(msg,tableNames);
			tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view),dryRun,viewNode,&tableNames);
			view->removeTables(&API);
			if (!dryRun) writeConfig(doc,viewConfigPath);
		} catch (tstore::exception::Exception &e) {
			mappingsForView(view)=oldMappings;
			addFault(e,"remove table from view "+view->name());
		}
	} catch (xcept::Exception &e) {
		addFault(e,"remove table");
	}
	return reply;
}


//adds to the view and the database a table whose definition is specified in the attachment.
xoap::MessageReference tstore::TStore::addTable(xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("addTableResponse");
	std::map<const std::string,std::string> tableNames;
	std::map<const std::string,std::string>::iterator tableName;
	tstore::View *view=NULL;
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		view=viewForConnection(connection,msg);
		std::string viewConfigPath=configurationPathForView(view->name());
		std::string localPath=tstoreclient::parsePath(viewConfigPath);
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(localPath);
		DOMNode *viewNode=getNodeForView(doc,view->name());
		getTableNames(msg,tableNames);
		for (tableName=tableNames.begin();tableName!=tableNames.end();++tableName) {
			xdata::Table table;
			if (!tstoreclient::getAttachmentWithID(msg,table,(*tableName).first)) {
				XCEPT_RAISE(tstore::exception::Exception,"No table was attached for table '"+(*tableName).first+"' (the Content-ID should match the table name.)");
			}
		}
		bool dryRun=getDryRun(msg);
		tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view),dryRun,viewNode,&tableNames);
		view->addTables(&API);
		if (!dryRun) writeConfig(doc,viewConfigPath);
	} catch (xcept::Exception &e) {
		addFault(e,"add table");
	}
	return reply;
}

//creates whatever tables are needed for the view. (Not used or tested)
xoap::MessageReference tstore::TStore::add (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("addResponse");
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		try {
			tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view));
			view->create(&API);
		} catch (tstore::exception::Exception &e) {
			addFault(e,"add view "+view->name());
		}
	} catch (tstore::exception::InvalidView &e) {
		addFault(e,"add");
	} catch (xcept::Exception &e) {
		addFault(e,"add");
	}
	return reply;
}

xoap::MessageReference tstore::TStore::remove (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("deleteResponse");
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		try {
			tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view));
			view->remove(&API);
		} catch (tstore::exception::Exception &e) {
			addFault(e,"delete from view "+view->name());
		}
	} catch (tstore::exception::InvalidView &e) {
		addFault(e,"delete");
	}
	return reply;
}

xoap::MessageReference tstore::TStore::destroy (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("destroyResponse");
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		std::string viewConfigPath(configurationPathForView(view->name()));
		std::string localPath=tstoreclient::parseLocalPath(viewConfigPath);
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(localPath);
		DOMNode *viewNode=getNodeForView(doc,view->name());
		tstore::MappingList oldMappings=mappingsForView(view);
		try {
			bool dryRun=getDryRun(msg);
			tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view),dryRun,viewNode);
			view->destroy(&API);
			if (!dryRun) writeConfig(doc,viewConfigPath);
		} catch (tstore::exception::Exception &e) {
			mappingsForView(view)=oldMappings;
			addFault(e,"destroy view "+view->name());
		}
	} catch (xcept::Exception &e) {
		addFault(e,"delete");
	}
	return reply;
}


xoap::MessageReference tstore::TStore::clear (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("clearResponse");
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		try {
			tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view));
			view->clear(&API);
		} catch (tstore::exception::Exception &e) {
			addFault(e,"clear view "+view->name());
		}
	} catch (tstore::exception::InvalidView &e) {
		addFault(e,"clear");
	}
	return reply;
}

void tstore::TStore::removeConnectionTimerForConnection(tstore::Connection *connection)  {
	std::string name=connectionTimerNameForConnection(connection);
	//remove the timer, otherwise there will be problems if a future connection happens to get the same ID.
	try {
		toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer(CONNECTION_TIMEOUT_TIMER);
		timer->remove(name);
	} catch (toolbox::task::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception,"Could not remove connection timeout timer",e);
	}
}

void tstore::TStore::killConnection(tstore::Connection *connection)  {
	if (connection->isConnected()) {
		try {
			connection->closeConnection();

			if (connections_.count(connection)) {
				connectionErrors_[connections_[connection]]="";
				connections_.erase(connection);
				connectionDates_.erase(connection);
			}

			removeConnectionTimerForConnection(connection);
		} catch(tstore::exception::Exception& e) {
			XCEPT_RETHROW (tstore::exception::Exception,"Error calling: killConnection", e);
		}

		delete connection;
	}
}

/// \return a human-readable string with the details of \a connection
std::string tstore::TStore::connectionInformation(tstore::Connection *connection) {
	std::string database=connection->getProperty(tstore::databaseKey);
	//put the ID first since it is fixed-length, then things will line up nicely
	return "connection ID "+IDFromConnection(connection)+" for view "+connections_[connection]+" ("+database+")";
}

xoap::MessageReference tstore::TStore::disconnect (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("disconnectResponse");
    
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		LOG4CPLUS_INFO(this->getApplicationLogger(),"Disconnect: "+connectionInformation(connection));
		killConnection(connection);
	} catch (tstore::exception::Exception &e) {
		addFault(e,"disconnect");
	}
	return reply;
}

tstore::Connection* tstore::TStore::connectionFromID(const std::string &connectionID)  {
	tstore::Connection *connection;
	std::istringstream stream(connectionID);
	//stream >> std::hex >> (void *)connection;

	// Take the number in the string, assume that it is a pointer to
	// an object and assign it to the connection object.
	//
	size_t ptr;
	stream >> std::hex >> ptr;
	connection = reinterpret_cast<tstore::Connection*>(ptr);
	if (connections_.count(connection)) {
		return connection;
	} else {
		XCEPT_RAISE(tstore::exception::InvalidView, "There is no open connection with ID '"+connectionID+"'");
	}
}

std::string tstore::TStore::IDFromConnection(tstore::Connection *connection)  {
	std::ostringstream connectionID;

	// Put the pointer as a hex number into the string
	if (connection)
		connectionID << std::hex << connection;

	return connectionID.str();
}

tstore::Connection *tstore::TStore::connectWithBasicAuthentication(const std::string &databaseName,const std::string &credentials)  {
	try {
		std::string password;
		std::string username;
		std::string::size_type slash=credentials.find('/');
		if (slash!=std::string::npos) {
			username=credentials.substr(0,slash);
			password=credentials.substr(slash+1);
		} else {
			XCEPT_RAISE(tstore::exception::Exception,"Credentials '"+credentials+"' are not in the correct format for basic authentication. They should be in the form username/password.");

		}

		//tstore::OracleConnection *connection=new tstore::OracleConnection(databaseName,username,password);
		//tstore::OracleConnection *connection = tstore::OracleConnection::create(databaseName, username, password);
		tstore::OracleConnection *connection = tstore::OraclePoolConnection::create(databaseName, username, password, &globalConnPool);
		connection->openConnection();

		return connection;

	} catch (xcept::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception,"Could not connect with basic authentication.",e);
	}
}

std::string tstore::TStore::connectionTimerNameForConnection(tstore::Connection *connection) throw() {
	return "Connection "+IDFromConnection(connection);
}

//do something about exceptions here
void tstore::TStore::scheduleConnectionTimeout(tstore::Connection *connection,const toolbox::TimeInterval &interval)  {
	try {
		toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer(CONNECTION_TIMEOUT_TIMER);
		toolbox::TimeVal timeToExecute= toolbox::TimeVal::gettimeofday()+interval;
		timer->schedule( this, timeToExecute,  connection,connectionTimerNameForConnection(connection) );
	} catch (toolbox::task::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::Exception,"Could not schedule connection timeout",e);
	}
}

void tstore::TStore::getTimeout(DOMNamedNodeMap *attributes,toolbox::TimeInterval &interval)  {
	std::string time=getNamedAttribute(attributes,"timeout");

	try {
		interval.fromString(time);
	} catch (toolbox::exception::Exception &e) {
		XCEPT_RETHROW (tstore::exception::InvalidTimeoutException,"No valid timeout value given. Check the 'timeout' parameter.",e);
	}
}

void tstore::TStore::getTimeout(xoap::MessageReference msg,toolbox::TimeInterval &interval)  {
	DOMNode* command = getCommand(msg);
	DOMNamedNodeMap *attributes=command->getAttributes();
	getTimeout(attributes,interval);
}

xoap::MessageReference tstore::TStore::renew (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("renewResponse");
    
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		LOG4CPLUS_INFO(this->getApplicationLogger(),"Renew: "+connectionInformation(connection));
		toolbox::TimeInterval interval;
		
		getTimeout(msg,interval);
		toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->getTimer(CONNECTION_TIMEOUT_TIMER);
		
		//do nothing if the new timeout is earlier than the old one
		std::string name=connectionTimerNameForConnection(connection);
		std::vector<toolbox::task::TimerTask> tasks=timer->getScheduledTasks();
		for (std::vector<toolbox::task::TimerTask>::iterator task=tasks.begin();task!=tasks.end();++task) {
			if ((*task).name==name) {
				toolbox::TimeVal timeToExecute=toolbox::TimeVal::gettimeofday()+interval;
				if (timeToExecute<(*task).schedule) {
					LOG4CPLUS_INFO(this->getApplicationLogger(),"New timeout "+timeToExecute.toString(toolbox::TimeVal::gmt)+" is before the existing timeout "+(*task).schedule.toString(toolbox::TimeVal::gmt)+" for the connection. Renew will be ignored.");
					return reply;
				}
			}
		}
		timer->remove(name);
		scheduleConnectionTimeout(connection,interval);
	} catch (xcept::Exception &e) {
		addFault(e,"renew");
	}
	return reply;
}

xoap::MessageReference tstore::TStore::connect (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("connectResponse");
    
	try {
		std::string viewName=getView(msg);
		try {
			DOMNode* command = getCommand(msg);
			if (command) {
				DOMNamedNodeMap *attributes=command->getAttributes();
				std::string credentials=getNamedAttribute(attributes,"credentials");
				std::string authenticationMethod=getNamedAttribute(attributes,"authentication");
				
				if (!credentials.empty()) {
					tstore::Connection *connection;
					toolbox::TimeInterval interval;
					getTimeout(attributes,interval);
					if (toolbox::tolower(authenticationMethod)=="basic") {
						connection=connectWithBasicAuthentication(viewDatabases_[viewName],credentials);
					} else {
				 		XCEPT_RAISE(tstore::exception::Exception,"Authentication method '"+authenticationMethod+"' not recognised.");
					}
					
					xdata::TimeVal now(toolbox::TimeVal::gettimeofday());
					std::string timeCreated(now.toString());
					connections_[connection]=viewName;
					connectionDates_[connection]=timeCreated;
					//connectionUsernames_[connection]=username;
					connectionErrors_[viewName]="";
					scheduleConnectionTimeout(connection,interval);


					std::map<std::string,std::string> attributes;
					attributes["connectionID"]=IDFromConnection(connection);
					return createResponse("connectResponse",&attributes);
				} else {
				 	XCEPT_RAISE(tstore::exception::Exception,"No credentials given.");
				}
			}
		} catch (xcept::Exception &e) {
			connectionErrors_[viewName]=e.message();

			//LOG4CPLUS_ERROR(this->getApplicationLogger(),
			//	toolbox::toString("tstore::TStore::connect to view '%s': %s", viewName.c_str(), e.message().c_str()));

			addFault(e,"connect to "+viewName);
		}
	} catch (tstore::exception::Exception &e) {
		//LOG4CPLUS_ERROR(this->getApplicationLogger(), toolbox::toString("tstore::TStore::connect: %s", e.what()));
		addFault(e,"connect");
	}
	return reply;
}

void tstore::TStore::timeExpired (toolbox::task::TimerEvent& e) {
	toolbox::task::TimerTask *task=e.getTimerTask();
	if (!task->context) LOG4CPLUS_WARN(this->getApplicationLogger(),"Timeout called with no connection.");
	tstore::Connection *connection=static_cast<tstore::Connection *>(task->context);
	if (connection) {
		LOG4CPLUS_INFO(this->getApplicationLogger(),"Connection "+IDFromConnection(connection)+" timed out.");
		closeConnection(connection);
	} else {
		LOG4CPLUS_WARN(this->getApplicationLogger(),"Timeout called for invalid connection.");
	}
}	

//this is basically a copy of update for now but eventually there may be more complicated updates.
xoap::MessageReference tstore::TStore::insert (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("insertResponse");
    
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		try {
			tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view));
			view->insert(&API);
		} catch (tstore::exception::Exception &e) {
			addFault(e,"insert into view "+view->name());
		}
	} catch (xcept::Exception &e) {
		addFault(e,"insert");
	}
	return reply;
}

 xoap::MessageReference tstore::TStore::query (xoap::MessageReference msg)  {
 	 xoap::MessageReference reply = createResponse("queryResponse");

	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view));
 		view->query(&API);
	} catch (tstore::exception::InvalidView &e) {
		addFault(e,"query");
	} catch (xcept::Exception &e) {
		addFault(e,"query");
	}
     return reply;
 }

 xoap::MessageReference tstore::TStore::definition (xoap::MessageReference msg)  {
 /* for 'simple' inserts, where all that is specified is the table and columns that can be inserted, we can get the table
 definition by examining database metadata for those columns. Once there are more complicated inserts, we might have to
 have some additional configuration or else do something like running a query and ignoring the data returned. (This is assuming
that the columns in the results of a query are the same as or a superset of what can be inserted, which would make the most sense 
for a user.) */
 	xoap::MessageReference reply = createResponse("definitionResponse");
	try {
 		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view));
		view->definition(&API);
	} catch (tstore::exception::InvalidView &e) {
		addFault(e,"get definition");
	} catch (xcept::Exception &e) {
		addFault(e,"get definition");
	}
     return reply;
 }
 
xoap::MessageReference tstore::TStore::getViews (xoap::MessageReference msg)  {
	xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope responseEnvelope = reply->getSOAPPart().getEnvelope();

	try {
		std::map<std::string,tstore::View *>::iterator view;
		xoap::SOAPName responseElementName = responseEnvelope.createName( "getViewsResponse", "tstoresoap", TSTORE_NS_URI);
		xoap::SOAPElement responseElement = responseEnvelope.getBody().addBodyElement ( responseElementName );
		for (view=views_.begin();view!=views_.end();++view) {
			xoap::SOAPName viewElementName = responseEnvelope.createName( "view", "tstoresoap", TSTORE_NS_URI);
			xoap::SOAPElement element = responseElement.addChildElement ( viewElementName );
			xoap::SOAPName nameAttribute = responseEnvelope.createName("name");
			//maybe add some other attributes if they would be convenient for someone... but they can all be found by using getConfiguration
			element.addAttribute(nameAttribute, (*view).first); 
		}
	} catch (xcept::Exception &e) {
		addFault(e,"get views");
	}
	return reply;
}

/// imports all the nodes in \a children into \a parent's document and adds them to \a parent
void tstore::TStore::addChildElements(DOMNode *parent,DOMNodeList *children) {
	DOMDocument* doc = parent->getOwnerDocument();
	for (unsigned int childIndex = 0; childIndex < children->getLength(); childIndex++) {
		DOMNode * child=children->item(childIndex);
		//LOG4CPLUS_DEBUG(this->getApplicationLogger(),"adding child "+xoap::XMLCh2String(child->getNodeName())+" with value: "+xoap::XMLCh2String(child->getNodeValue()));
		parent->appendChild(doc->importNode(child,true));
	}
}

template <class Iterator>
DOMNode* tstore::TStore::getMutableMatchingNodeWithAncestors(DOMNode *haystack,Iterator ancestor,Iterator end)  {
	if (ancestor==end) return haystack;
	const DOMNode *needle=*ancestor;
	if (needle->getNodeType()==DOMNode::ATTRIBUTE_NODE) {
		DOMNamedNodeMap *attributes=haystack->getAttributes();
		if (attributes) {
			DOMNode *matchingAttribute=attributes->getNamedItemNS(needle->getNamespaceURI(),needle->getLocalName());
			if (matchingAttribute) {
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"found matching attribute");
			} else {
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"no matching attribute with namespace: "+xoap::XMLCh2String(needle->getNamespaceURI())+" localName: "+xoap::XMLCh2String(needle->getLocalName()));
			}
			return matchingAttribute;
		}
	}
	//look through direct children of haystack
	DOMNodeList* children = haystack->getChildNodes();
	//std::cout << "looking for match for " << writeXML((DOMNode *)needle) << std::endl;
	for (unsigned int childIndex = 0; childIndex < children->getLength(); childIndex++) {
		DOMNode *child=children->item(childIndex);
		if (xoap::XMLCh2String(child->getNodeName())==xoap::XMLCh2String(needle->getNodeName())) {
		if (xoap::XMLCh2String(child->getNamespaceURI())==xoap::XMLCh2String(needle->getNamespaceURI())) {
		if (xoap::XMLCh2String(child->getPrefix())==xoap::XMLCh2String(needle->getPrefix())) {
		if (xoap::XMLCh2String(child->getNodeValue())==xoap::XMLCh2String(needle->getNodeValue())) {
		//if (xoap::XMLCh2String(child->getBaseURI())==xoap::XMLCh2String(needle->getBaseURI())) {
		//if (child->isEqualNode(needle)) { //this crashes with 'pure virtual method called' in _ZNK11xercesc_2_716DOMElementNSImpl10getBaseURIEv+0x24
			//LOG4CPLUS_DEBUG(this->getApplicationLogger(),"found matching ancestor "+xoap::XMLCh2String(needle->getNodeName()));
			++ancestor;
			DOMNode *matchingChild=getMutableMatchingNodeWithAncestors(child,ancestor,end);
			if (matchingChild) {
				return matchingChild;
			} else {
				--ancestor;
			}
		} }}}
	}
	LOG4CPLUS_DEBUG(this->getApplicationLogger(),"not found");
	return NULL;
}

/// finds a node matching \a needle as a descendent of \a haystack
/// this is necessary because the results from a filter are not mutable, so we have to find them again in a mutable version
/// of the tree.
/// For a node to match, it must have the same name, namespaceURI, prefix and value as \a needle.
/// each of its ancestors up to \a haystack must also match those of \a needle.
DOMNode* tstore::TStore::getMutableMatchingNode(DOMNode *haystack,const DOMNode *needle)  {
	std::vector<const DOMNode *> ancestors;
	const DOMNode *currentAncestor=needle;
	do {
		ancestors.push_back(currentAncestor);
		if (currentAncestor->getNodeType()==DOMNode::ATTRIBUTE_NODE) {
			const DOMAttr *attribute=dynamic_cast<const DOMAttr *>(currentAncestor);
			if (attribute) {
				currentAncestor=attribute->getOwnerElement();
			} //otherwise we should probably throw an exception
		} else {
			currentAncestor=currentAncestor->getParentNode();
		}
	} while (currentAncestor!=NULL && !isViewNode(currentAncestor));
	return getMutableMatchingNodeWithAncestors(haystack,ancestors.rbegin(),ancestors.rend());
}

std::list<xoap::SOAPElement> tstore::TStore::matchingNodes(DOMNode *node,const std::string &path) 
    
{
	
	//std::cout << "looking for " << path /*<< " in:" << std::endl << writeXML(wrapper->getEnvelope())*/ << std::endl;
	//since the new extract requires a DOMDocument, create one containing just this view node
	DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(xoap::XStr("LS"));
	DOMDocument* newDocument = impl->createDocument(
			   xoap::XStr("urn:xdaq-tstore:1.0"),                    // root element namespace URI.
			   xoap::XStr("tstore:configuration"),         // root element name
			   0);                   // document type object (DTD).
	/*DOMNode *copiedNode=*/newDocument->getDocumentElement()->appendChild(newDocument->importNode(node,true));
	copyNamespaceDeclarations(newDocument->getDocumentElement(),node->getOwnerDocument()->getDocumentElement()); 
	//copyNamespaceDeclarations(newDocument->getDocumentElement(),node);
	//std::cout << "going to extract" << std::endl;
	return extract(path,newDocument);
}

void tstore::TStore::raiseDOMException(const DOMException &e,const std::string &action)  {
	std::ostringstream error;
	error << "Could not " << action << " because of error " << e.code << ": " << xoap::XMLCh2String(e.msg);
	XCEPT_RAISE(tstore::exception::Exception,error.str());
}

/// replaces the descendent node of \a viewNode matching \a foundNode with \a newChildren
void tstore::TStore::replaceSingleNode (DOMNode *viewNode,const DOMNode *foundNode,DOMNodeList* newChildren)  {
	DOMNode *matchingNode=getMutableMatchingNode(viewNode,foundNode);
	DOMNode *parentNode=matchingNode->getParentNode();
	if (parentNode) {
		//remove the existing child and insert the new one(s) in its place
		//this can not be done simply with replaceChild as there may be more than one,
		//but to make sure it is in the right position we could use insertBefore
		for (unsigned int childIndex = 0; childIndex < newChildren->getLength(); childIndex++) {
			DOMNode *newChild=newChildren->item(childIndex);
			DOMDocument *doc2=parentNode->getOwnerDocument();
			DOMNode *importedNode=doc2->importNode(newChild,true);
			parentNode->insertBefore(importedNode,matchingNode);
		}
		parentNode->removeChild(matchingNode);
	} else if (matchingNode->getNodeType()==DOMNode::ATTRIBUTE_NODE) {
		std::ostringstream error;
		DOMAttr *matchingAttribute=dynamic_cast<DOMAttr *>(matchingNode);
		if (newChildren->getLength()>1) {
			error << "XPath points to an attribute but there are " << newChildren->getLength() << " elements in the supplied configuration.";
			XCEPT_RAISE(tstore::exception::Exception,error.str());
		}
		DOMNode *newValue=newChildren->item(0);
		if (newValue->getNodeType()==DOMNode::TEXT_NODE) {
			matchingAttribute->setValue(newValue->getNodeValue());
		} else {
			error << "XPath points to an attribute but the node supplied is not a text node. Node Type: " << newValue->getNodeType();
			XCEPT_RAISE(tstore::exception::Exception,error.str());
		}
	} else {
		XCEPT_RAISE(tstore::exception::Exception,"Existing node has no parent");
	}
}

/// replaces all descendent nodes of \a viewNode with \a newChildren
void tstore::TStore::replaceEntireConfig(DOMNode *viewNode,DOMNodeList* newChildren)  {
	DOMNodeList* oldChildren = viewNode->getChildNodes();
	while (oldChildren->getLength()) {
		//std::cout << "removing child" << std::endl;
		viewNode->removeChild(oldChildren->item(0)); //oldChildren updates live, so this will go through all of them
	}
	addChildElements(viewNode,newChildren);
}

/// removes the direct child nodes of \a viewNode matching \a matchingElements, and adds \a newChildren as children of \a viewNode
/// raises an exception and leaves viewNode in an undefined state if any of the \a matchingElements are not direct children of \a viewNode
void tstore::TStore::replaceRootElements(DOMNode *viewNode,std::list<xoap::SOAPElement> &matchingElements,DOMNodeList* newChildren,const std::string &filterPath)  {
	//remove matching elements
	for (std::list<xoap::SOAPElement>::iterator matchingElement = matchingElements.begin(); matchingElement != matchingElements.end(); ++matchingElement) {
		DOMNode *matchingNode=getMutableMatchingNode(viewNode,(*matchingElement).getDOM());
		if (!matchingNode->getParentNode()->isSameNode(viewNode)) {
			//if they are not all children of the root, then we don't know where to put the replacement XML, so we can't do it
			XCEPT_RAISE(tstore::exception::Exception,"Not all existing nodes matching the filter '"+filterPath+"' are in the root of the view configuration. Could not determine where to put the new configuration.");
		}
		//std::cout << "removing child " << xoap::XMLCh2String(matchingNode->getNodeName()) << " with value: " << xoap::XMLCh2String(matchingNode->getNodeValue()) << std::endl;
		viewNode->removeChild(matchingNode);
	}
	
	//add the new ones
	addChildElements(viewNode,newChildren);
}

bool tstore::TStore::removeFile(const std::string & temporaryViewConfigPath)  {
	return (0==std::remove(temporaryViewConfigPath.c_str()));
}

void tstore::TStore::removeTemporaryFile(const std::string & temporaryViewConfigPath)  {
	if (!removeFile(temporaryViewConfigPath)) {
		LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString("Could not delete temporary view configuration file at %s because of error %d",temporaryViewConfigPath.c_str(),errno));
		perror(NULL);
	}
}

xoap::MessageReference tstore::TStore::setConfiguration (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("insertResponse");
	std::string action="set configuration";
	std::string pathInfo="";
	std::string temporaryViewConfigPath;
	try {
		std::string viewName=getView(msg);
		std::string filterPath=getCommandAttribute(msg,"path");
		pathInfo=" with xpath "+filterPath;
		std::string viewConfigPath(configurationPathForView(viewName));
		std::string localPath=tstoreclient::parseLocalPath(viewConfigPath); //must be a local path or we can't write it
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(localPath);
		DOMNode *viewNode=getNodeForView(doc,viewName);
		
		DOMNode* node = getCommand(msg);
		
		try { 
			tstoreclient::removeBlankTextNodes(node);
			DOMNodeList* newChildren = node->getChildNodes();
			if (filterPath.empty()) {
				action="replace configuration";
				replaceEntireConfig(viewNode,newChildren);
			} else {
				std::list<xoap::SOAPElement> matchingElements;
				matchingElements = matchingNodes(viewNode,filterPath);//filter.extract(config);
				int elementCount=matchingElements.size();
				
				if (elementCount==0) {
					XCEPT_RAISE(tstore::exception::Exception,"No existing nodes match the filter '"+filterPath+"'. Could not determine where to put the new configuration.");
				} else if (elementCount==1) {
					action="replace single node in configuration";
					replaceSingleNode(viewNode,matchingElements.front().getDOM(),newChildren);
				} else {
					action="replace selected root elements in configuration";
					replaceRootElements(viewNode,matchingElements,newChildren,filterPath);
				}
			}

			//we will load the config back from the file in order to reconfigure the view, because
			//re-parsing is the only way to check the document against the schema
			//however, if loading the view fails we must be able to go back to the old configuration
			//otherwise the view can no longer be loaded and therefore its configuration can not be fixed by this interface.
			//So we will first write the configuration to a temporary file
			//(we could just write to the normal file and restore from the backup if necessary, but that would destroy the previous backup)
			std::string database;
			readConnectionParameters(viewNode,database);
			temporaryViewConfigPath=localPath+"_temp";
			tstoreclient::writeXML(doc,temporaryViewConfigPath);
			tstore::View *view=views_[viewName];
			view=createView(temporaryViewConfigPath,viewName);
			std::vector<tstore::Mapping> mappings;
			tstore::TableList tables;
			readMappings(mappings,tables,viewNode); //this is done here rather than in the view because there may be global mappings as well
			writeConfig(doc,viewConfigPath);
			replaceView(view,viewName,mappings,tables,database,viewConfigPath); //this could throw an exception if there is no existing view, but we know there is one or we wouldn't have got this far
			removeTemporaryFile(temporaryViewConfigPath);
		} catch (xoap::exception::Exception &e) {
			XCEPT_RETHROW(tstore::exception::Exception,"Could not "+action,e);
		} catch (DOMException &e) {
			raiseDOMException(e,action);
		} catch (tstore::exception::Exception &e) {
			if (!temporaryViewConfigPath.empty()) removeTemporaryFile(temporaryViewConfigPath);
			XCEPT_RETHROW(tstore::exception::Exception,"The new configuration for view with ID '"+viewName+" is not valid or could not be saved. The configuration will not be changed.",e);
		}
		
	} catch (xcept::Exception &e) {
		addFault(e,"set configuration"+pathInfo);
	}
	return reply;
}

void tstore::TStore::debugAttributes(DOMElement *element) {
	DOMNamedNodeMap *parameterList=element->getAttributes();
	for (unsigned int parameterIndex = 0; parameterIndex < parameterList->getLength(); parameterIndex++) {
		DOMNode* parameterNode=parameterList->item(parameterIndex);
		std::cout << xoap::XMLCh2String(parameterNode->getNamespaceURI()) << " " << xoap::XMLCh2String(parameterNode->getLocalName()) << ": " << xoap::XMLCh2String(parameterNode->getNodeValue()) << std::endl;
	}
}

// LO NEW
//copies the namespace declarations from \a source to \a destination
//only copies the declarations in the nodes themselves, not in any child elements.
void tstore::TStore::copyNamespaceDeclarations(DOMNode *destination ,const DOMNode *source) 
    
{
    DOMNamedNodeMap *attributes=source->getAttributes();
    for (unsigned int attributeIndex = 0; attributeIndex < attributes->getLength(); attributeIndex++) 
    {
        DOMNode *parameterNode=attributes->item(attributeIndex);
        if (xoap::XMLCh2String(parameterNode->getPrefix())=="xmlns") 
        {
            //destination.addNamespaceDeclaration(xoap::XMLCh2String(parameterNode->getLocalName()),xoap::XMLCh2String(parameterNode->getNodeValue()));
            try 
            {
                std::string ns = "xmlns:";
                ns += xoap::XMLCh2String(parameterNode->getLocalName());
                ((DOMElement*)destination)->setAttributeNS(xoap::XStr("http://www.w3.org/2000/xmlns/"),xoap::XStr(ns),parameterNode->getNodeValue());
            }
            catch (DOMException& de) 
            {   
                  // Xerces 2.6: de.getMessage()
                  XCEPT_RAISE (xoap::exception::Exception, xoap::XMLCh2String(de.msg));
            }   

        }
    }
}


xoap::MessageReference tstore::TStore::getConfiguration (xoap::MessageReference msg)  {
	xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope responseEnvelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = responseEnvelope.createName( "getConfigurationResponse", "tstoresoap", TSTORE_NS_URI);
    xoap::SOAPElement configurationResponse = responseEnvelope.getBody().addBodyElement ( responseName );
	try {
		std::string viewName=getView(msg);
		std::string filterPath=getCommandAttribute(msg,"path");
		std::string viewConfigPath(configurationPathForView(viewName));
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(tstoreclient::parsePath(viewConfigPath));
		std::list<xoap::SOAPElement> matchingElements;
		DOMNode *viewNode=getNodeForView(doc,viewName);
			
		if (viewNode) {
			DOMDocument *responseDocument=configurationResponse.getDOM()->getOwnerDocument();
			copyNamespaceDeclarations(getCommand(reply),doc->getDocumentElement());
			try {
				if (!filterPath.empty()) { //if the path is blank then filter.match will crash
					matchingElements = matchingNodes(viewNode,filterPath);//filter.extract(config);
					//std::cout << "found " << matchingElements.size() << " elements" << std::endl;
					//unsigned int elementIndex=0;
					for (std::list<xoap::SOAPElement>::iterator matchingElement = matchingElements.begin(); matchingElement != matchingElements.end(); ++matchingElement) {

						DOMNode *matchingNode=(*matchingElement).getDOM();//responseDocument->importNode((*matchingElement).getDOM(),true);
						if (matchingNode->getNodeType() == DOMNode::ELEMENT_NODE) {
							DOMElement *element=dynamic_cast<DOMElement *>(matchingNode);
							debugAttributes(element);
							//LOG4CPLUS_DEBUG(this->getApplicationLogger(),toolbox::toString("adding element node %d",elementIndex++));
							configurationResponse.addChildElement(matchingNode);
						} else {
							//LOG4CPLUS_DEBUG(this->getApplicationLogger(),toolbox::toString("adding text node %d",elementIndex++));
							//LOG4CPLUS_DEBUG(this->getApplicationLogger(),"with text "+xoap::XMLCh2String(matchingNode->getNodeValue()));
							DOMText *textNode=responseDocument->createTextNode(matchingNode->getNodeValue());
							configurationResponse.addChildElement(textNode);
							//std::cout << matchingNode->getNodeType() << ": " << matchingNode->getNodeValue() << std::endl;
						}
					}
				} else {
					addChildElements(configurationResponse.getDOM(),viewNode->getChildNodes());
				}
			} catch (DOMException &e) {
				raiseDOMException(e,"get configuration");
			}
		}
	} catch (xcept::Exception &e) {
		addFault(e,"get configuration");
	}
	return reply;	
}

/*std::string getTableNamePatternForView(tstore::View *view) {
	std::string abbreviatedName;
	std::string viewName(view->name());
	std::string::size_type firstColon=viewName.find(':');
	if (firstColon!=std::string::npos) {
		std::string::size_type secondColon=viewName.find(':',firstColon+1);
		if (secondColon!=std::string::npos) {
			abbreviatedName=viewName.substr(secondColon+1,5);
		}
	}
	return toolbox::toupper(abbreviatedName)+"@_%"; //have not decided what to actually use for this
}*/

bool tstore::TStore::isColumnTypeCompatible(tstore::Connection *connection,const std::string &typeInDatabase,const std::string &typeInConfig,bool strict) {
	if (typeInDatabase==typeInConfig) return true;
	/*if (strict)*/ return connection->typesAreIdentical(typeInDatabase,typeInConfig);
	//in fact compatibility would be assessed by looking at the type in the database rather than the xdata type guessed from it
	//else return connection->typesAreCompatible(typeInDatabase,typeInConfig);
}

bool tstore::TStore::tableMatchesPattern(const std::string &tableName,const std::string &pattern)  {
	//std::cout << tableName << "  " << pattern << tableName.compare(0,pattern.size(),pattern) << std::endl;
	//return tableName.compare(0,pattern.size()+1,pattern+"_")==0;
	try {
		return toolbox::regx_match_nocase( tableName, pattern );
	} catch (std::exception &e) {
		XCEPT_RAISE(tstore::exception::Exception,"Could not check whether the table "+tableName+" matches the pattern "+e.what());
	}
}

bool tstore::TStore::checkColumnsMatch(tstore::SyncManager *syncManager,tstore::Connection *connection,xdata::Table &definitionInDatabase,xdata::Table &definitionInConfig)  {
	std::map<std::string, std::string, xdata::Table::ci_less> columnsInDatabase=definitionInDatabase.getTableDefinition();
	std::map<std::string, std::string, xdata::Table::ci_less> columnsInConfig=definitionInConfig.getTableDefinition();
	std::map<std::string, std::string, xdata::Table::ci_less>::iterator column;
	for (column=columnsInConfig.begin();column!=columnsInConfig.end();++column) {
		if (columnsInDatabase.count((*column).first)) {
			if (!isColumnTypeCompatible(connection,columnsInDatabase[(*column).first],(*column).second,true)) {
				if (syncManager) {
					syncManager->columnsAreIncompatible((*column).first,columnsInDatabase[(*column).first],(*column).second);
				}
				//if there is no syncManager then an error will be raised later.
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Column "+(*column).first+" has type "+columnsInDatabase[(*column).first]+" in the database but "+(*column).second+ " in the configuration");
				return false;
			}
		} else {
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Column "+(*column).first+" does not exist in the database.");
			return false;
		}
	}
	//we could get rid of this loop and just use count if we removed matching columns as we went.
	//this also might make it easier for a merge of columns but more difficult for a straight add of an entire table
	for (column=columnsInDatabase.begin();column!=columnsInDatabase.end();++column) {
		if (!columnsInConfig.count((*column).first)) {
			LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Column "+(*column).first+" does not exist in the configuration.");
			return false;
		}
	}
	return true;
}

bool tstore::TStore::checkForeignKeysMatch(tstore::SyncManager *syncManager,std::vector<tstore::ForeignKey> &foreignKeysInDatabase,std::vector<tstore::ForeignKey> &foreignKeysInConfig)  {
	for (std::vector<tstore::ForeignKey>::iterator foreignKeyInConfig=foreignKeysInConfig.begin();foreignKeyInConfig!=foreignKeysInConfig.end();++foreignKeyInConfig) {
		bool found=false;
		for (std::vector<tstore::ForeignKey>::iterator foreignKeyInDatabase=foreignKeysInDatabase.begin();foreignKeyInDatabase!=foreignKeysInDatabase.end();++foreignKeyInDatabase) {
			//a table may reference another table more than once, so we can not do anything special
			//if the referenced table is the same but the columns aren't. We can only compare for equality.
			//we can use the normal == operator here because we added the column names in uppercase, and maps are always ordered the same way.
			if (*foreignKeyInConfig==*foreignKeyInDatabase) {
				found=true;
				break;
			}
		}
		if (!found ) {
			//if it doesn't exist in the database, then check whether the referenced table is in the database or in the config
			//tell the syncManager, it might want to throw an exception if the referenced table would not exist.
			//That way we avoid creating a whole lot of tables only to have Oracle throw an exception at the end when we try to create the
			//foreign key constraint.
			//however this might be difficult given the fact that tables are currently removed from the tablesInDatabase list as they are
			// found in the config.
			//syncManager->keyNotInDatabase(*foreignKeyInConfig,tableExistsInConfig,tableExistsInDatabase);
			return false;
		}
	}
	//now check whether there are any in the database which are not in the config
	for (std::vector<tstore::ForeignKey>::iterator foreignKeyInDatabase=foreignKeysInDatabase.begin();foreignKeyInDatabase!=foreignKeysInDatabase.end();++foreignKeyInDatabase) {
		bool found=false;
		for (std::vector<tstore::ForeignKey>::iterator foreignKeyInConfig=foreignKeysInConfig.begin();foreignKeyInConfig!=foreignKeysInConfig.end();++foreignKeyInConfig) {
			if (*foreignKeyInConfig==*foreignKeyInDatabase) {
				found=true;
				break;
			}
			if (!found) {
				return false;
			}		
		}
	}

	return true;
}

void tstore::TStore::sync (tstore::View *view,tstore::Connection *connection,const std::string &tableNamePattern,tstore::TStoreAPI *API,tstore::SyncManager *syncManager)  {
	try {
		tstore::TableList &tablesInConfig=tablesInView(view);
		//we could have a method in OracleConnection which returns a TableList and gets all the information for it in one big query.
		std::set<std::string> tablesInDatabase;
		//std::string tableNamePattern=getTableNamePatternForView(view);
		std::string stateOfTable;
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Getting tables matching pattern: "+tableNamePattern);
		connection->getTablesMatchingPattern(tablesInDatabase,tableNamePattern/*+"@_"*/);
		std::ostringstream matchingTables;
		std::copy(tablesInDatabase.begin(),tablesInDatabase.end(),std::ostream_iterator<std::string>(matchingTables, ","));
		LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Got tables: "+matchingTables.str());
		
		for (tstore::TableList::iterator table=tablesInConfig.begin();table!=tablesInConfig.end();++table) {
			std::string tableName=toolbox::toupper((*table).name);
			if (tableMatchesPattern(tableName,tableNamePattern)) {
				if (tablesInDatabase.count(tableName)) {
					LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Table '"+tableName+"' has the same name as a table in the database");
					std::map<std::string, std::string, xdata::Table::ci_less> columnsInConfig=(*table).definition.getTableDefinition();
					
					//first check if the tables are different
					//pass the database definition and keys into the syncManager
					xdata::Table definitionInDatabase;
					connection->getTableDefinition(tableName,definitionInDatabase);
					bool columnsMatch=checkColumnsMatch(syncManager,connection,definitionInDatabase,(*table).definition);
					
					std::string keysInDatabase;
					connection->getKeysForTable(tableName,keysInDatabase);
					bool keysMatch=this->keysMatch(keysInDatabase,(*table).key);
					
					xdata::Table foreignKeysInDatabase;
					std::vector<tstore::ForeignKey> processedKeys;
					connection->getForeignKeys(tableName,foreignKeysInDatabase);
					processForeignKeys(processedKeys,foreignKeysInDatabase);
					bool foreignKeysMatch=this->checkForeignKeysMatch(syncManager,processedKeys,(*table).foreignKeys);
					
					if (!columnsMatch || !keysMatch || !foreignKeysMatch) {
						std::string thingsChanged;
						if (!columnsMatch) thingsChanged="columns";
						if (!keysMatch) {
							if (!thingsChanged.empty()) thingsChanged+=" and ";
							thingsChanged+="primary key";
						}
						if (!foreignKeysMatch) {
							if (!thingsChanged.empty()) thingsChanged+=" and ";
							thingsChanged+="foreign key constraints";
						}
						std::string stateOfTable="Table '"+tableName+"' has different "+
												thingsChanged+
												" in the database from in the configuration";
						if (syncManager) {
							LOG4CPLUS_DEBUG(this->getApplicationLogger(),stateOfTable);
							syncManager->syncTableInBothPlaces(*API,*table,definitionInDatabase,keysInDatabase,processedKeys);
						} else {
							XCEPT_RAISE(tstore::exception::Exception,stateOfTable);
						}
					} else {
						LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Table in the database is compatible with the one in the configuration");
					}
					//std::cout << "erasing " << tableName << std::endl;
					tablesInDatabase.erase(tableName);
				} else {
					stateOfTable="Table '"+tableName+"' does not match table in database";
					if (syncManager) {
						LOG4CPLUS_DEBUG(this->getApplicationLogger(),stateOfTable);
						syncManager->syncTableInConfig(*API,*table);
					} else {
						XCEPT_RAISE(tstore::exception::Exception,stateOfTable);
					}
				}
			} else {
				;//LOG4CPLUS_DEBUG(this->getApplicationLogger(),"Table "+tableName+" in config does not match the pattern "+tableNamePattern);
			}
		}
		for (std::set<std::string>::iterator table=tablesInDatabase.begin();table!=tablesInDatabase.end();++table) {
			stateOfTable="Table '"+*table+"' in database is not in configuration";
			if (syncManager) {
				LOG4CPLUS_DEBUG(this->getApplicationLogger(),stateOfTable);
				syncManager->syncTableInDatabase(*API,*table);
			} else {
				XCEPT_RAISE(tstore::exception::Exception,stateOfTable);
			}
		}
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception,"Could not get information from the database necessary to sync",e);
	}
}

xoap::MessageReference tstore::TStore::sync (xoap::MessageReference msg)  {
	xoap::MessageReference reply = xoap::createMessage();
    xoap::SOAPEnvelope responseEnvelope = reply->getSOAPPart().getEnvelope();
    xoap::SOAPName responseName = responseEnvelope.createName( "syncResponse", "tstoresoap", TSTORE_NS_URI);
    /*xoap::SOAPElement syncResponse =*/ responseEnvelope.getBody().addBodyElement ( responseName );
	tstore::SyncManager *syncManager=NULL;
	try {
		tstore::Connection *connection=connectionFromMessage(msg);
		tstore::View *view=viewForConnection(connection,msg);
		std::string modeString=getCompulsoryCommandAttribute(msg,"mode");
		//std::string tableNamePattern=getCompulsoryCommandAttribute(msg,"prefix");
		std::string tableNamePattern=getCompulsoryCommandAttribute(msg,"pattern");
		std::string viewConfigPath(configurationPathForView(view->name()));
		std::string parsedPath=tstoreclient::parsePath(viewConfigPath);
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(parsedPath);
		DOMNode *viewNode=getNodeForView(doc,view->name());
		bool dryRun=getDryRun(msg);
		tstore::TStoreAPI API(connection,msg,reply,mappingsForView(view),getDryRun(msg),viewNode);
		//the SyncManager subclasses handle the comparison between the database and the config file
		//and change the appropriate things when differences are found.
		if (modeString=="to database") {
			syncManager=new tstore::SyncToDatabase(this->getApplicationLogger());
		} else if (modeString=="from database") {
			syncManager=new tstore::SyncFromDatabase(this->getApplicationLogger());
		} else if (modeString=="both ways") {
			syncManager=new tstore::SyncBothWays(this->getApplicationLogger());
		} else {
			XCEPT_RAISE(tstore::exception::Exception,"Unknown mode: "+modeString);
		}
		//syncManager->setLogger(this->getApplicationLogger());
		if (syncManager->writesToConfig()) {
			tstoreclient::parseLocalPath(viewConfigPath);
		}
		sync(view,connection,tableNamePattern,&API,syncManager);
		
		if (!dryRun && syncManager->writesToConfig()) {
			std::vector<tstore::Mapping> mappings;
			tstore::TableList tables;
			//we could change TStoreAPI to also change tables, or we could simply reload the mappings from the new config (which could mean the
			//other code to update stuff in TStoreAPI could be removed.)
			//Maybe it would be a good idea to do both while testing, and check that they give the same results.
			readMappings(mappings,tables,viewNode);
			replaceView(view,view->name(),mappings,tables,viewConfigPath); //this could throw an exception if there is no existing view, but we know there is one or we wouldn't have got this far
			writeConfig(doc,viewConfigPath);
		}

	} catch (xcept::Exception &e) {
		//maybe tell the sync object to clean up whatever it has done in the database. But in the case of dropping a table this is impossible..
		delete syncManager;
		addFault(e,"sync");
	}
	delete syncManager;
	return reply;
}

bool tstore::TStore::viewConfigFileExists(const std::string &path)  {
	std::vector<std::string> fileNames=getViewConfigFileNames();
	for (std::vector<std::string>::iterator fileName=fileNames.begin();fileName!=fileNames.end();++fileName) {
		if (path==*fileName) return true;
	}
	return false;
}

DOMNode* tstore::TStore::newViewNode(DOMDocument *doc,const std::string &viewName)  {
	try {
		DOMElement *viewNode=doc->createElementNS(xoap::XStr("urn:xdaq-tstore:1.0"),xoap::XStr("tstore:view"));
		viewNode->setAttribute(xoap::XStr("id"),xoap::XStr(viewName));
		doc->getDocumentElement()->appendChild(viewNode);
		return viewNode;
	} catch (const DOMException& e) {
		raiseDOMException(e,"create new view node for view with id "+viewName);
	}
	return NULL; //this will never actually be reached since raiseDOMException always raises an exception, but it gets rid of a warning
}

//this function has not been tested, since it is not clear how to add a file to the tstore configuration
DOMDocument* tstore::TStore::newEmptyConfigFile()  {
	try {
		DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(xoap::XStr("LS"));
		DOMDocument* doc = impl->createDocument(
			   xoap::XStr("urn:xdaq-tstore:1.0"),                    // root element namespace URI.
			   xoap::XStr("tstore:configuration"),         // root element name
			   0);                   // document type object (DTD).
		return doc;
	} catch (const DOMException& e) {
		raiseDOMException(e,"create new empty view configuration file");
	}
	return NULL; //this will never actually be reached since raiseDOMException always raises an exception, but it gets rid of a warning
}

xoap::MessageReference tstore::TStore::removeView (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("removeViewResponse");
	std::string temporaryViewConfigPath;
	try {
		std::string viewName=getView(msg);
		std::string viewConfigPath(configurationPathForView(viewName));
		std::string localPath=tstoreclient::parseLocalPath(viewConfigPath); //must be a local path or we can't write it
		DOMDocument* doc = xoap::getDOMParserFactory()->get("configure")->loadXML(localPath);
		DOMNode *viewNode=getNodeForView(doc,viewName);
		DOMNode *parentNode=viewNode->getParentNode();
		parentNode->removeChild(viewNode);
		
		//if there are no more views in this file, delete it.
		DOMNodeList* remainingViews = doc->getElementsByTagNameNS(xoap::XStr("urn:xdaq-tstore:1.0"), xoap::XStr("view"));
		if (remainingViews->getLength()) {
			writeConfig(doc,viewConfigPath);
		} else {
			writeBackup(viewConfigPath); //in case the view was removed by accident, leave a backup just as we would in the case where there are other views in the file)
			if (!removeFile(viewConfigPath)) {
				LOG4CPLUS_ERROR(this->getApplicationLogger(),toolbox::toString("Could not delete empty view file at %s because of error %d",viewConfigPath.c_str(),errno));
			}
		}
		removeView(viewName);
	} catch (tstore::exception::Exception &e) {
		XCEPT_RETHROW(xoap::exception::Exception,"Could not remove view",e);
	}
	return reply;
}

xoap::MessageReference tstore::TStore::addView (xoap::MessageReference msg)  {
	xoap::MessageReference reply = createResponse("addViewResponse");
	std::string temporaryViewConfigPath;
	try {
		std::string viewName=getCompulsoryCommandAttribute(msg,"id");
		std::string fileName=getCompulsoryCommandAttribute(msg,"fileName");
		std::string path=pathToViewFile(fileName);
		std::string localPath=tstoreclient::parseLocalPath(path);
		DOMDocument* doc=NULL;
		bool configAlreadyExisted=viewConfigFileExists(localPath);
		if (configAlreadyExisted) {
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(localPath);
		} else {
			doc=newEmptyConfigFile();
		}
		DOMNode *newNode=newViewNode(doc,viewName);
		//add the contents of the addView command as initial configuration for the view
		addChildElements(newNode,getCommand(msg)->getChildNodes());
		
		std::string database;
		readConnectionParameters(newNode,database);
		temporaryViewConfigPath=localPath+"_temp";
		tstoreclient::writeXML(doc,temporaryViewConfigPath);
		tstore::View *view=createView(temporaryViewConfigPath,viewName);
		std::vector<tstore::Mapping> mappings;
		tstore::TableList tables;
		readMappings(mappings,tables,newNode); //this is done here rather than in the view because there may be global mappings as well
		if (configAlreadyExisted) writeBackup(localPath);
		tstoreclient::writeXML(doc,path);
		addView(view,viewName,mappings,tables,database,localPath);
		removeTemporaryFile(temporaryViewConfigPath);
	} catch (tstore::exception::Exception &e) {
		if (!temporaryViewConfigPath.empty()) removeTemporaryFile(temporaryViewConfigPath);
		XCEPT_RETHROW(xoap::exception::Exception,"Could not add view",e);
	}
	return reply;
}

void tstore::TStore::killConnection(xgi::Input * in, xgi::Output * out )  {
	tstore::Connection *connection;
	cgicc::Cgicc cgi(in);
	std::string connectionID=**cgi["connection"];
	try {
		connection=connectionFromID(connectionID);
		LOG4CPLUS_INFO(this->getApplicationLogger(),"Kill: "+connectionInformation(connection));
		killConnection(connection);
	}
	catch (xcept::Exception &e) {
		LOG4CPLUS_WARN(this->getApplicationLogger(),"Could not kill connection '"+connectionID+"'. "+e.message());
	}
	Default(in,out);
}

void tstore::TStore::removeView(const std::string &viewName) {
	LOG4CPLUS_INFO(this->getApplicationLogger(),"Removing view "+viewName);
	//I can't find any documentation about what erase does when the key does not exist
	closeConnectionsToView(viewName);
	views_.erase(viewName);
	viewMappings_.erase(viewName);
	viewTables_.erase(viewName);
	viewDatabases_.erase(viewName);
	viewConfiguration_.erase(viewName);
	LOG4CPLUS_INFO(this->getApplicationLogger(),"Removed view "+viewName);
}

void tstore::TStore::addOrReplaceView(tstore::View *view,const std::string &name,std::vector<tstore::Mapping> &mappings,tstore::TableList &tables,const std::string &database,const std::string &path) {
	LOG4CPLUS_INFO(this->getApplicationLogger(),"Adding view "+name);
	views_[name]=view;
	//maybe the following information should be stored in the view itself, but it is read by TStore
	//and only used by the views indirectly.
	viewMappings_[name]=mappings;
	viewTables_[name]=tables;
	viewDatabases_[name]=database;
	viewConfiguration_[name]=path;
	LOG4CPLUS_INFO(this->getApplicationLogger(),"Added view "+name+" which connects to "+viewDatabases_[name]);
}

void tstore::TStore::addView(tstore::View *view,const std::string &name,tstore::MappingList &mappings,tstore::TableList &tables,const std::string &database,const std::string &path)  {
	if (views_.count(name)) {
		XCEPT_RAISE(tstore::exception::Exception,"Duplicate view named '"+name+"' -- only the first one will be loaded.");
	} else {
		addOrReplaceView(view,name,mappings,tables,database,path);
	}
}

void tstore::TStore::replaceView(tstore::View *view,const std::string &name,tstore::MappingList &mappings,tstore::TableList &tables,const std::string &database,const std::string &path)  {
	if (!views_.count(name)) {
		XCEPT_RAISE(tstore::exception::Exception,"There is no existing view named '"+name+".");
	} else {
		if (viewDatabases_[name]!=database) {
			closeConnectionsToView(name);
			delete views_[name];
		}
		addOrReplaceView(view,name,mappings,tables,database,path);
	}
}

void tstore::TStore::replaceView(tstore::View *view,const std::string &name,tstore::MappingList &mappings,tstore::TableList &tables,const std::string &path)  {
	if (!views_.count(name)) {
		XCEPT_RAISE(tstore::exception::Exception,"There is no existing view named '"+name+".");
	} else {
		std::string database=viewDatabases_[name];
		addOrReplaceView(view,name,mappings,tables,database,path);
	}
}


void tstore::TStore::printError(xgi::Output * out,const std::string &error) {
	if (!error.empty()) {
		//do some nice formatting, like red or something
		*out << cgicc::b() << error << cgicc::b() << cgicc::br();
	}
}

void tstore::TStore::printDatabaseInfo (xgi::Output * out, const std::string &viewName)
{
	// In case of exceptions being thrown, put the HTML into a stringstream and only append once the errors HAVNT happened
	std::stringstream tmpOut;
	try
	{
		tstore::ConnectionList::iterator connection = connections_.begin();

		if (connection == connections_.end())
		{
			return;
		}

		tmpOut << "<table class=\"xdaq-table\">";
		for (connection = connections_.begin(); connection != connections_.end(); ++connection)
		{
			if ((*connection).second == viewName)
			{
				std::string database = (*connection).first->getProperty(tstore::databaseKey);
				std::string username = (*connection).first->getProperty(tstore::userKey);
				std::string connectionID = IDFromConnection((*connection).first);
				tmpOut << cgicc::form().set("method", "GET").set("action", toolbox::toString("/%s/killConnection", getApplicationDescriptor()->getURN().c_str()));
				tmpOut << "<tr><td>";
				tmpOut << connectionID;
				tmpOut << "</td>";
				if ((*connection).first->isConnected())
				{
					tmpOut << "<td class=\"xdaq-green\">" << "Connected to " << username << "@" << database << " since " << connectionDates_[(*connection).first] << "</td>";
				}
				else
				{
					tmpOut << "<td class=\"xdaq-red\">" << "Not Connected";
				}
				tmpOut << cgicc::input().set("type", "hidden").set("value", connectionID).set("name", "connection") << "</td>";
				tmpOut << "<td>";
				tmpOut << cgicc::input().set("type", "submit").set("value", "Kill Connection") << cgicc::form();
				tmpOut << "</td></tr>";
			}
		}
		tmpOut << "</table>";

		*out << tmpOut.str();

		printError(out, connectionErrors_.count(viewName) ? connectionErrors_[viewName] : "");
	}
	catch (std::exception &e)
	{
		*out << e.what();
	}
}

void tstore::TStore::printBooleanCell(xgi::Output * out,bool b) {
	//maybe this will have a tick/cross image or something
	*out << cgicc::td() << (b?(std::string)"Yes":(std::string)"No") << cgicc::td() << std::endl;
}

void tstore::TStore::printViews(xgi::Output * out) {
	printError(out,viewError_);
	*out << cgicc::table().set("class","xdaq-table") << std::endl;
	*out << cgicc::caption() << std::endl;
	*out << views_.size() << " views" << std::endl;
	*out << cgicc::caption() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Name") << std::endl;
	*out << cgicc::th("Query") << std::endl;
	*out << cgicc::th("Update") << std::endl;
	*out << cgicc::th("Insert") << std::endl;
	*out << cgicc::th("Delete") << std::endl;
	*out << cgicc::th("Create") << std::endl;
	*out << cgicc::th("Drop") << std::endl;
	*out << cgicc::th("Connections") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tbody() << std::endl;

	for( std::map<std::string,tstore::View*>::iterator viewIterator=views_.begin(); viewIterator!=views_.end(); ++viewIterator) {
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td() << (*viewIterator).first << cgicc::td() << std::endl;
		printBooleanCell(out,(*viewIterator).second->canQuery());
		printBooleanCell(out,(*viewIterator).second->canUpdate());
		printBooleanCell(out,(*viewIterator).second->canInsert());
		printBooleanCell(out,(*viewIterator).second->canRemove());
		printBooleanCell(out,(*viewIterator).second->canCreate());
		printBooleanCell(out,(*viewIterator).second->canDrop());
		*out << cgicc::td();
		printDatabaseInfo(out,(*viewIterator).first);
		*out << cgicc::td() << std::endl;
		*out << cgicc::tr() << std::endl;
	}
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}

void tstore::TStore::printConnPoolInfo(xgi::Output * out) {
	*out << cgicc::table().set("class","xdaq-table");
	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th() << "Connection Pool DB" << cgicc::th();
	*out << cgicc::th() << toolbox::toString("Busy connections (Max %d)", globalConnPool.getMaxConnections()) << cgicc::th();
	*out << cgicc::th() << "Open Connections" << cgicc::th();
	*out << cgicc::tr();
	*out << cgicc::thead();
	*out << cgicc::tbody();

	for(tstore::StatelessConnPoolMap::const_iterator it = globalConnPool.poolsBegin(); it != globalConnPool.poolsEnd(); it++) {
		try {
			string db = it->first;
			oracle::occi::StatelessConnectionPool* connPool = it->second;

			if (connPool) {
				*out << cgicc::tr();
				*out << cgicc::td() << db << cgicc::td();
				std::string busyConnStyle = (connPool->getBusyConnections() != connPool->getMaxConnections()) ? "" : "xdaq-color-red";
				*out << cgicc::td().set("class", busyConnStyle) << connPool->getBusyConnections() << cgicc::td();
				*out << cgicc::td() << connPool->getOpenConnections() << cgicc::td();
				*out << cgicc::tr();
			}
		} catch(oracle::occi::SQLException& e) {
			XCEPT_RAISE(tstore::exception::Exception,
				toolbox::toString("Oracle returned error: %s", e.what()));
		}
	}

	*out << cgicc::tbody();
	*out << cgicc::table();
}

void tstore::TStore::Default(xgi::Input * in, xgi::Output * out )  {
	try {
		printConnPoolInfo(out);
		*out << "<br /><hr /><br />" << std::endl;
		printViews(out);
	} catch (xcept::Exception &e) {
		*out << e.message();
	}
}



/*
std::list<xoap::SOAPElement> tstore::TStore::extract(const std::string & path, DOMDocument* doc) 
{
	tstore::DocumentFilter fil (path);
    return fil.extract(doc);
}
*/
std::list<xoap::SOAPElement> tstore::TStore::extract(const std::string & path, DOMDocument* doc) 
{
    XALAN_USING_XERCES(XMLPlatformUtils)

    XALAN_USING_XALAN(XPathEvaluator)
    XALAN_USING_XALAN(XalanDocumentPrefixResolver)
    XALAN_USING_XERCES(LocalFileInputSource)
    
    XALAN_USING_XALAN(XalanDocument)
    XALAN_USING_XALAN(XalanDocumentPrefixResolver)
    XALAN_USING_XALAN(XalanDOMString)
    XALAN_USING_XALAN(XalanNode)
    XALAN_USING_XALAN(XalanSourceTreeInit)
    XALAN_USING_XALAN(XalanSourceTreeDOMSupport)
    XALAN_USING_XALAN(XalanSourceTreeParserLiaison)
    XALAN_USING_XALAN(XObjectPtr)
    XALAN_USING_XALAN(NodeRefListBase)
    XALAN_USING_XALAN(NodeRefList)
    XALAN_USING_XALAN(XercesDOMWrapperParsedSource)
    
    XercesParserLiaison liaison;
    XercesDOMSupport support(liaison);
    
    XercesDOMWrapperParsedSource src(doc, liaison, support);
    XalanDocument* xalanDoc = src.getDocument( );
    XPathEvaluator evaluator;
    XalanDocumentPrefixResolver resolver(xalanDoc);
    XalanDOMString xpath(path.c_str());
    
    // To map xalan nodes back to Xerces nodes
    XercesDocumentWrapper* theWrapper = liaison.mapDocumentToWrapper(/*xalanDoc*/src.getDocument( ));
    
    try
    {
    
        // Context node is the view node, for convenience
        // in fact the root node should also be the view node, to prevent people from using paths like /tstore:configuration/tstore:view and then trying to setConfiguration
        // however, it might be useful for people to be able to see the entire thing.
        // I doubt people cared what the context node or root node were before, since they were both a dummy SOAP message
        // most likely people always used //
        /*XalanNode* const theContextNode =
			evaluator.selectSingleNode(
				support,
				xalanDoc,
				XalanDOMString("/tstore:configuration/tstore:view").c_str(),
				resolver
			);*/
		//this way is better, it at least stops it crashing when there are no results
        XalanNode* /*const*/ theContextNode = xalanDoc->getFirstChild()->getLastChild()/*->getFirstChild()*/;//evaluator.selectSingleNode( support, xalanDoc, XalanDOMString("/").c_str(), resolver);
  
        //crashes here (without throwing an exception) with certain paths, e.g. "//"
        //perhaps related to http://mail-archives.apache.org/mod_mbox/xml-xalan-dev/200111.mbox/%3c20011123144905.22396.qmail@nagoya.betaversion.org%3e
        XObjectPtr result = evaluator.evaluate( support, theContextNode, xpath.c_str(),resolver/*,(const XalanElement *)xalanDoc->getFirstChild()->getLastChild()*/);

        const NodeRefListBase& nodeset = result->nodeset( );
        std::list<xoap::SOAPElement> elements;
        for (NodeRefListBase::size_type i = 0; i < nodeset.getLength(); ++i)
        {
            DOMNode * dn = (DOMNode *) theWrapper->mapNode( nodeset.item(i) );
            elements.push_back ( xoap::SOAPElement (dn) );
        }

        return elements;
    }
    catch (std::exception & e)
    {
    	std::cout << e.what() << std::endl;
        XCEPT_RAISE(xoap::exception::Exception, e.what() );
    }
    catch (const XSLException & e)
    {
       std::ostringstream msg;
       std::cout << e.getMessage() << std::endl;
       msg << e.getMessage() << " " <<  e.getType();
       XCEPT_RAISE(xoap::exception::Exception, msg.str() );
    }
}
