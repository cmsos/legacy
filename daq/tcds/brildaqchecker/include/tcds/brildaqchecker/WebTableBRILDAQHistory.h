#ifndef _tcds_brildaqchecker_WebTableBRILDAQHistory_h
#define _tcds_brildaqchecker_WebTableBRILDAQHistory_h

#include <cstddef>
#include <string>

#include "tcds/utils/WebObject.h"

namespace tcds {
  namespace utils {
    class Monitor;
  }
}

namespace tcds {
  namespace brildaqchecker {

    class WebTableBRILDAQHistory : public tcds::utils::WebObject
    {

    public:
      WebTableBRILDAQHistory(std::string const& name,
                             std::string const& description,
                             tcds::utils::Monitor const& monitor,
                             std::string const& itemSetName,
                             std::string const& tabName,
                             size_t const colSpan);

      std::string getHTMLString() const;

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_WebTableBRILDAQHistory_h
