#ifndef _tcds_apve_APVEInfoSpaceUpdater_h_
#define _tcds_apve_APVEInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace apve {

    class TCADeviceAPVE;

    class APVEInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      APVEInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                           TCADeviceAPVE const& hw);
      virtual ~APVEInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      tcds::apve::TCADeviceAPVE const& getHw();

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVEInfoSpaceUpdater_h_
