#! /bin/awk -f
BEGIN {
	IGNORECASE=1;
	FS=":";
	"hostname -s" | getline hostname;
	#print "HOSTNAME IS: [",hostname, "]";
	pattern = "^" hostname;
}

#$1 ~ pattern { 
/^[^#]/ { 
		split(argument, fname, ".");
		filename = fname[1] "." $1 "." fname[2] "." fname[3];
		#print "Service ", fname[1];
		#print "List ", $5;
		split( fname[1],service, "/");

		# split services into an array and check if service to instantiate can be found
		split( $2, services, " ");
		found = 0;
		for ( i in services) {
			print "Service name is ",services[i];
			if( services[i] == service[2]) {
				found = 1;
			}
		}

		if ( (found != 0  ) || (service[2] == "default") || ( service[2] == "xdaqd")) {
			print "Creating ",filename;
			system ("sed 's/%service/" $2 "/g' " argument "> " filename );
		}
		

}

END {
	print "Finished processing";
}

