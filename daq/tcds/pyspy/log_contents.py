#!/usr/bin/env python

import re

from po_boys_uhal import chunkify
from log_entry import FIRST_BX
from log_entry import LAST_BX
from log_entry import LogEntry
from log_line import LogLine

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def fix_bx_number(bx_in):
    num_bx_per_orbit = (LAST_BX - FIRST_BX + 1)
    res = bx_in
    if (res < FIRST_BX):
        res += num_bx_per_orbit
    elif (res > LAST_BX):
        res -= num_bx_per_orbit
    # End of fix_bx_number().
    return res

###############################################################################

class LogContents(object):

    def __init__(self, raw_log_contents, backplane_mode):

        # First step: process the raw entries that come straight from
        # the firmware logger.

        # Split the raw contents into single-entry chunks.
        tmp = chunkify(raw_log_contents, LogEntry.NUM_WORDS_PER_SPY_MEM_ENTRY)

        # Turn them into 'log entry' objects.
        entries = [LogEntry(i[::-1], backplane_mode) for i in tmp]

        #----------

        # Second step: unpack the raw entries (since each one of those
        # can contain both a trigger and a B-command).
        lines = []
        for entry in entries:
            line = None
            if entry.is_l1a():
                # L1A.
                line = LogLine(LogLine.TTC_TRAFFIC_TYPE_L1A,
                               entry.get_timestamp(),
                               entry.get_bx_number(),
                               raw_data=entry.raw_data,
                               tcds_mode=backplane_mode)
            elif entry.is_bcommand_short():
                # Short B-command.
                line = LogLine(LogLine.TTC_TRAFFIC_TYPE_BCOMMAND_SHORT,
                               entry.get_timestamp(),
                               entry.get_bx_number(),
                               entry.get_bcommand_data(),
                               raw_data=entry.raw_data,
                               tcds_mode=backplane_mode)
            elif entry.is_bcommand_long():
                # Long B-command.
                line = LogLine(LogLine.TTC_TRAFFIC_TYPE_BCOMMAND_LONG,
                               entry.get_timestamp(),
                               entry.get_bx_number(),
                               entry.get_bcommand_data(),
                               entry.get_long_bcommand_address(),
                               entry.get_long_bcommand_subaddress(),
                               entry.get_long_bcommand_external_flag(),
                               raw_data=entry.raw_data,
                               tcds_mode=backplane_mode)
            lines.append(line)

        #----------

        if len(lines):

            # Third step: (re)sort in order of ascending arrival time.
            lines.sort();

            #----------

            # Fourth step: subtract the overall offset from all timestamps.
            offset = lines[0].timestamp
            for line in lines:
                line.subtract_offset(offset)

        self.lines = lines
        # End of __init__().

    def get_formatted(self, verbose=False):
        lines = self.lines
        strings = [str(i) for i in lines]
        res = strings
        # Apply some reformatting to align all entries vertically on
        # the screen. Not cheap (computationally) but easier on the
        # eyes.
        regexp_pattern = "^.*?(at .*?),.*?(in.*?):(.*)$"
        regexp = re.compile(regexp_pattern)
        regexp_matches = [regexp.match(i) for i in strings]
        rows = [[j.strip() for j in i.groups()] for i in regexp_matches]
        cols = zip(*rows)
        col_sizes = [max(len(val) for val in col) for col in cols]
        fmt = " ".join(["{{{0:d}:{1:d}s}}".format(i, j) for (i, j) in enumerate(col_sizes)])
        tmp = [fmt.format(*row) for row in rows]
        res = None
        if verbose:
            res = ["{0:<4d} {1:s} {2:s}".format(i, j.raw_data_as_string(), k) \
                   for (i, (j, k)) in enumerate(zip(lines, tmp))]
        else:
            res = ["{0:<4d} {1:s}".format(i, j) \
                   for (i, j) in enumerate(tmp)]
        # End of get_formatted().
        return res

    # End of class LogContents.

###############################################################################
