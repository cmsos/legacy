#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <iomanip>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include "d2s/utils/Exception.hh"
#include "ferol40/Ferol40.hh"
#include "ferol40/loggerMacros.h"
#include "ferol40/ferol40Constants.h"
#include "ferol40/HardwareDebugger.hh"
#include "hal/PCIAddressTableASCIIReader.hh"
#include "toolbox/math/random.h"
#include "toolbox/regex.h"
#include "hal/linux/StopWatch.hh"
#include "d2s/utils/Watchdog.hh"
#include "ferol40/Ferol40EventGenerator.hh"
#include "ferol40/SlinkExpressEventGenerator.hh"
#include "ferol40/DataSourceFactory.hh"
#include "ferol40/Ferol40DataSource.hh"
ferol40::Ferol40::Ferol40(xdaq::Application *xdaq,
        ferol40::ApplicationInfoSpaceHandler &appIS,
        ferol40::StatusInfoSpaceHandler &statusIS,
        ferol40::InputStreamInfoSpaceHandler &inputIS,
        ferol40::Ferol40Monitor &monitor,
        ferol40::HardwareLocker &hwLocker)
    : ConfigSpaceSaver(&(ferol40Device_P)),
    logger_(xdaq->getApplicationLogger()),
    appIS_(appIS),
    statusIS_(statusIS),
    monitor_(monitor),
    hwLocker_(hwLocker),
    ferol40IS_(xdaq, this),
    tcpIS_(xdaq, this, &appIS_),
    //streamConfigIS_( xdaq, this, &appIS_, streams_ ), //JRF TODO these are currently in the Ferol40Controller. Check if that's gonna work
    inputIS_(inputIS),
    busAdapter_(),
    deviceLock_(toolbox::BSem::FULL, true) //JRF note. this is set to recursive, so the same thread can lock it multiple times without causing a deadlock. of course it must unlock it the same number of times.
      //JRF removing this
      //dataTracker_(ferol40IS_, &ferol40Device_P)
      //JRF replaced this with a vector of streamDataTrackers
      //,dataTracker2_( tcpIS_, &ferol40Device_P )
{
    DEBUG("Constructor of Ferol40Controller");
    HAL::StopWatch watch(1);
    watch.reset();
    watch.start();

    slCore_P = NULL;
    //mdioInterface_P = NULL;
    ferol40Device_P = NULL;
    slot_ = 0;
    hwCreated_ = false;
    const utils::DataTracker tcpStreamDataTracker(tcpIS_, &ferol40Device_P);
    const utils::DataTracker inputStreamDataTracker(inputIS_, &ferol40Device_P);
    for (int i = 0; i < NB_STREAMS; ++i)
    {
        streams_.push_back(false); //add one entry for each stream, index is the stream id. this vector is used only to store the enable boolean for that stream
        dataSource_.push_back("n.a.");
        streamBpCounterHigh_.push_back(0);
        streamBpCounterOld_.push_back(0);
        tcpStreamDataTrackers_.push_back(tcpStreamDataTracker);     //JRF add the 4 data trackers we require for the TCP infospace tracking
        inputStreamDataTrackers_.push_back(inputStreamDataTracker); //JRF add the 4 data trackers we require for the inputstream infospace tracking
        tcpIS_.registerTrackerItems(tcpStreamDataTrackers_[i]);     // register the Data Tracker for each stream, passing in the stream id
        inputIS_.registerTrackerItems(inputStreamDataTrackers_[i]);
    }
    operationMode_ = "n.a.";

    //JRF TODO shall we put this back once we've removed all the input and tcp stream items
    //monitor.addInfoSpace( &ferol40IS_ );
    monitor.addInfoSpace(&tcpIS_);
    //JRF NOTE that the inputIS_ is managed by the Ferol40Controller so no need to do this here.

    // The Addresstables need to be created only once
    try
    {
        HAL::PCIAddressTableASCIIReader ferol40Reader(FEROL40_ADDRESSTABLE_FILE);
        ferol40Table_P = new HAL::PCIAddressTable("FEROL40 Addresstable", ferol40Reader);
        HAL::PCIAddressTableASCIIReader sleReader(SLINKEXPRESS_ADDRESSTABLE_FILE);
        slinkExpressTable_P = new HAL::PCIAddressTable("SlinkExpress Addresstable", sleReader);
    }
    catch (HAL::HardwareAccessException &e)
    {
        FATAL("PROGRAM WILL END!!! Could not create Addresstable for Ferol40 in Constructor." << e.what());
        exit(-1);
    }
    watch.stop();
    uint32_t stime = watch.read();
    watch.reset();
    std::stringstream info;
    info << "Ferol40 hardware object constructor took "
        << stime << "us.";
    INFO(info.str());
}

ferol40::Ferol40::~Ferol40()
{

    this->suspendEvents();
    shutdownHwDevice();
}

/**
 * Algorithm: Scan through all possible PCI indices (0-15) and probe if 
 * a FEROL40 is present at this index. If an FEROL40 is found, read out the slot
 * number for the corresponding FEROL40 (the slot number is read from the 
 * backplane: It has some lines which for each slot encodes the slot number.
 * These lines are mapped to a register in the Bridge FPGA). If the read
 * slot number is equal to the slotnumber which has been given to this 
 * program as configuration parameter, we have found the correct ferol40.
 **/
void ferol40::Ferol40::createFerol40Device() throw(utils::exception::Exception)
{

    if (!hwLocker_.lockedByUs())
        return;

    // Scan PCI to find slot
    uint32_t slotread;
    uint64_t readback;
    slot_ = appIS_.getuint32("slotNumber");

    for (uint32_t i = 0; i < 12; i++) //JRF There are only 12 slots on MicroTCA
    {
        try
        {
            HAL::PCIDevice *dev = new HAL::PCIDevice(*ferol40Table_P,
                    busAdapter_,
                    FEROL40_VENDORID,
                    FEROL40_DEVICEID,
                    i,
                    false);

            // JRF TODO, check if we are removing FEDKIT mode...
            /*if ( operationMode_ == FEDKIT_MODE ) 
              {
            // For the fekdit the unit slot number will be the unit-number.
            // We set slotread to i so that the if statement below is 
            // successfull if we probe the correct unit number i.
            slotread = i;
            }
            else*/
            if (operationMode_ == SCAN_CRATE)
            {
                // Scan the crate by just checking for a sensible response from each slot
                //
                dev->read64("Device_Id", &readback);
                std::cout << "Device ID = 0x" << std::hex << readback << " from FEROL in slot " << i << std::endl;
                dev->read64("Vendor_Id", &readback);
                std::cout << "Vendor ID = 0x" << readback << " from FEROL in slot " << i << std::endl;
                dev->read64("serial_number_lower_bit", &readback);
                std::cout << "Serial number lower bits = 0x" << std::hex << readback << " from FEROL in slot " << i << std::endl;
                dev->read64("serial_number_higher_bit", &readback);
                std::cout << "Serial number higher bits = 0x" << readback << " from FEROL in slot " << i << std::endl;

                for (uint32_t index = 0; index < 10000000000000; index++)
                {
                    /*dev->write64("debug_test4",0x55cc55cc11441144);
                      dev->read64("debug_test4",&readback);
                      std::cout << "debug_test4 64 = 0x" << readback << " from FEROL in slot " << i << std::endl;
                      */
                    dev->write("debug_test4", 0x11441144);
                    //dev->write("debug_test4",0x55cc55cc,HAL::HAL_DO_VERIFY,4);
                    dev->read("debug_test4", &slotread);
                    //std::cout << "debug_test4 32 (0) = 0x" << slotread << " from FEROL in slot " << i << std::endl;
                    dev->read("Geographic_Address", &slotread);
                    if (slotread != slot_)
                    {
                        std::cout << "Failed to read Geographical Address, value read back = " << slotread << " from FEROL in slot " << slot_ << std::endl;
                    }
                    if (index % 1000000 == 0)
                        std::cout << "Index = " << std::dec << index << ", slot read back = " << slotread << std::endl;
                    /*dev->read("debug_test4",&slotread,4);
                      std::cout << "debug_test4 32 (4) = 0x" << slotread << " from FEROL in slot " << i << std::endl;
                      */
                    dev->write("eth_10Gb_phy_data", 0x11441144);
                    dev->read("eth_10Gb_phy_data", &slotread);
                    //std::cout << "eth_10Gb_phy_data (0) = 0x" << slotread << " from FEROL in slot " << i << std::endl;
                }

                dev->read("Geographic_Address", &slotread);
                std::cout << "Geographic_Address = 0x" << slotread << " from FEROL in slot " << i << std::endl;
                dev->read64("compilation_version_sel", &readback);
                std::cout << "compilation utils::version = 0x" << readback << " from FEROL in slot " << i << std::dec << std::endl;
            }
            else
            {
                dev->read("Geographic_Address", &slotread);

                INFO("Found Ferol40 for unit ( = pciIndex ) " << i << " and read slot number " << slotread << ". Looking for ferol40 in slot " << slot_);
                if (slotread == 0 || slotread > 12)
                {
                    //JRF Try again...
                    dev->read("Geographic_Address", &slotread);
                    if (slotread == 0 || slotread > 12)
                    {
                        std::ostringstream msg;
                        msg << "Geographic Slot badly read. " << slotread << std::endl;
                        FATAL(msg.str());
                        XCEPT_RAISE(utils::exception::HardwareAccessFailed, msg.str());
                    }
                    else
                    {
                        std::ostringstream msg;
                        msg << "Geographic Slot badly read the first time buy ok the second time!. " << slotread << std::endl;
                        WARN(msg.str());
                    }
                }
            }

            if (slot_ == slotread)
            {
                //JRF TODO put this into an INFO message
                std::cout << "FOUND FEROL40 in slot " << slot_ << " Awesome!!!" << std::endl;
                ferol40Device_P = dev;
                // check the firmware types loaded and the utils::versions:

                //JRF TODO ADD FIRMWARE and HARDWARE CHECK
                fwChecker_P = NULL; //JRF TODO Remove this... it was just to prevent crash on halt...
                fwChecker_P = new ferol40::FirmwareChecker(ferol40Device_P, appIS_.getbool("noFirmwareVersionCheck"));

                statusIS_.setuint32("HardwareRevision", fwChecker_P->getFerol40HardwareRevision());
                statusIS_.setuint32("FirmwareType", fwChecker_P->getFerol40FirmwareType());
                statusIS_.setuint32("FirmwareVersion", fwChecker_P->getFerol40FirmwareVersion());

                std::string errorstr;
                bool changedFW;
                if (!fwChecker_P->checkFirmware(operationMode_, std::string(dataSource_[0]), errorstr, &changedFW))
                {
                    XCEPT_RAISE(utils::exception::Ferol40Exception, errorstr);
                }
                // the bit 7 is set to one in order to end the power on reset of the Vitesse chip.
                // A statemachine in the FPGA is writing 2 64 bit words into the Vitesse afterwards.
                // Therefore one has to wait a bit.
                // This code has to be executed before the hwCreated_ is set to true, since the
                // monitoring thread could otherwise interfere with the automatic configuration
                // accesses of the FPGA. (There is a potential race condition).
                // ferol40Device_P->write( "SERDES_INIT_FEROL40", 0x180 ); // See comment below;
                //::usleep( 500 );

                //JRF TODO understand the Slink Express Core stuff and figure out how to multiply this to 4 streams.
                slCore_P = new SlinkExpressCore(ferol40Device_P, slinkExpressTable_P, dataSource_);
                //mdioInterface_P = new MdioInterface( ferol40Device_P, logger_ );
                hwCreated_ = true;
                //boardType_ = ferol40::Ferol40::FEROL40;

                // read out here the Mac Address of the Ferol40 and check if it is set correctly.
                // This check is based on the fact that the first three bytes of the mac address
                // of each Ferol40 should be set to 08:00:30. (This value is the default of the
                // application infospace value MAC_HI_FEROL40)

                //ferol40Device_P->setBit( "read_SN_MAC" );  // This is done also automatically at
                // power up. Therefore not necessary here.

                uint64_t snlow;
                uint32_t snhi;
                ferol40Device_P->read64("serial_number_lower_bit", &snlow);
                ferol40Device_P->read("serial_number_higher_bit", &snhi);
                ferol40IS_.setuint64("Ferol40SnLow", snlow);
                ferol40IS_.setuint32("Ferol40SnHi", snhi);
                ferol40IS_.setuint32("slotNumber", slotread);
                tcpIS_.setuint32("slotNumber", slotread);

                //JRF TODO we now have one MAC address per Stream so we need to set these in a loop one for each stream
                uint64_t mac;
                uint32_t index(0), stream_offset(0);
                for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
                {
                    index = streams_it - streams_.begin();
                    stream_offset = (index)*0x4000;
                    ferol40Device_P->read64("eth_10gb_ctrl_MAC_def", &mac, stream_offset);

                    //JRF TODO implement the source MAC address for each stream in the infospace. dont' forget to monitor these new values...
                    //ferol40IS_.setuint64( "Source_MAC_EEPROM", (((uint64_t)machi) << 32) + maclo );
                    ferol40IS_.setvectorelementuint64("InputPorts", "TCP_SOURCE_MAC_ADDRESS", mac, index);
                    if (appIS_.getuint32("MAC_HI_FEROL40") != (mac >> 24))
                    { //JRF this check requires that we know that the MAC address always starts with the same 3 bytes...
                        std::stringstream msg;
                        msg << "FEROL40 Mac Address for stream " << index << ", is not set correctly. It should start with "
                            << std::setw(6) << std::setfill('0') << std::hex << appIS_.getuint32("MAC_HI_FEROL40")
                            << " but is read out as " << std::setw(12) << mac;
                        FATAL(msg.str());
                        XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
                    }
                }
                DEBUG("Created Ferol40 Device");
                break;
            }
            else
            {
                delete dev;
            }
        }
        catch (HAL::NoSuchDeviceException &e)
        {
            DEBUG("No Ferol40 card found at index." << i);
        }
        catch (HAL::HardwareAccessException &e)
        {
            FATAL("Hardware Access failed: Could not create Ferol40 PCIDevice and read geo slot." << e.what());
            XCEPT_RETHROW(utils::exception::HardwareAccessFailed,
                    "Failed to create Ferol40 PCIDevice and read the geo-slot.", e);
        }
    }

    // If we have not found the ferol40 there is something wrong...
    if (!hwCreated_)
    {
        std::ostringstream msg;
        msg << "No Ferol40 Card found in geographic slot " << appIS_.getuint32("slotNumber");
        ERROR(msg.str());
        XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
    }
}

void ferol40::Ferol40::shutdownHwDevice() throw(utils::exception::Ferol40Exception)
{

    this->hwlock();

    hwCreated_ = false;

    //boardType_ = ferol40::Ferol40::UNKNOWN;

    if (fwChecker_P)
    {
        delete fwChecker_P;
        fwChecker_P = NULL;
    }

    if (ferol40Device_P)
    {
        delete slCore_P;
        //delete mdioInterface_P;
        slCore_P = NULL;
        //mdioInterface_P = NULL;
        delete ferol40Device_P;
        ferol40Device_P = NULL;
    }
    this->hwunlock();
}

    ferol40::SlinkExpressCore *
ferol40::Ferol40::getSlinkExpressCore()
{
    if (slCore_P)
        return slCore_P;
    else
        return NULL;
}

/////////////////////////////////// state transistions  ////////////////////////////

void ferol40::Ferol40::instantiateHardware() throw(utils::exception::Exception)
{
    operationMode_ = appIS_.getstring("OperationMode");

    //JRF Loop over all streams
    uint32_t index(0), stream_offset(0);

    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    {
        index = streams_it - streams_.begin();
        *streams_it = dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[index].getField("enable"))->value_;
        dataSource_[index] = dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[index].getField("DataSource"))->value_; //appIS_.getstring( "DataSource" );
        //std::cout << "streams_it = " << (*streams_it?"true":"false") << " index number: " << ( streams_it - streams_.begin() ) << std::endl;
    }
    this->createFerol40Device();

    // We have created the Hardware successfully. In case we had been killed
    // previously while transferring data, it could be that the TCP connections
    // are still happily alive and transfer data. We try to shut down the
    // connections here. This should at least guarantee that we do not send
    // data anymore, even if the receiver does not receive our reset.
    // We reset both streams in any case. (It could be that one of the streams
    // is not anymore in the configuraton, but if it was in the previous
    // configuration we want to be sure that no data is flowing from that
    // stream anymore.)
    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    {
        stream_offset = (streams_it - streams_.begin()) * 0x4000;
        try
        {
            ferol40Device_P->setBit("eth_10gb_req_TCP_rst", HAL::HAL_NO_VERIFY, stream_offset);
        }
        catch (HAL::HardwareAccessException &e)
        {
            FATAL("Hardware Access failed.");
            XCEPT_RETHROW(utils::exception::Ferol40Exception,
                    "Hardware access failed.", e);
        }
    } //end for loop
}

//JRF TODO, check with dominique what happens to all things to do with the Vitess chip... I think we don't need it.
/*void 
  ferol40::Ferol40::resetVitesse( uint32_t reset )
  throw( utils::exception::Ferol40Exception )
  {
  if ( ! hwCreated_ ) {
  WARN("No reset performed since no hardware created object yet.");
  return;
  }



//    uint32_t val = 0x180;
//if ( reset == 0 ) val = 0x100;
try
{

if ( reset == 0 ) 
// reset of the XAUI link 
{
ferol40Device_P->setBit( "SERDES_DOWN" );
}
else 
{
// the following command triggers a statemachine executing the following tasks: 
//  - release reset of Vitesse Chip
//  - configure the Vitesse via MDIO accesses
//  - execute the reset procedure of the SERDES (XAUI)
ferol40Device_P->setBit( "SERDES_UP" );
::usleep( 100000 ); // precise duration not known. Dominique to check.

}
}
catch ( HAL::HardwareAccessException &e )
{
ERROR( "A hardware exception occured while resetting the Vitesse : " << e.what() );
XCEPT_RETHROW( utils::exception::Ferol40Exception, "A hardware exception occured.", e );
}
}*/

//////////////////////////////////////////////////
// configure algo:
//     set tcp/ip and other network parameters
//     make arp request
//     setup tcp/ip connection
///////////////////////////////////////////////////
void ferol40::Ferol40::configure() throw(utils::exception::Ferol40Exception)
{

    // At this state we know that there is at least one stream enabled (otherwise
    // the Ferol40Controller would not have called us) and the hardwre has been
    // instantiated. We check once more if the instantiation was successfull:

    if (!hwCreated_)
        return;

    this->hwlock();

    // catch all hardware access related problems:
    try
    {
        try
        {
            // in case we were killed brute force, we need to switch DAQ-OFF here.
	    DEBUG("Switching DAQ-OFF in slink express links which participate in this run");
            for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
            {

                if (*streams_it) {
                    daqOff(streams_it - streams_.begin());
		}


            }
        }
        catch (...)
        {
            //JRF TODO, handle this exception correctly, we should raise an error, allow completion of configure and then go to failed so that the monitoring pages are ok
            ERROR("daqOff Failed!!!");
        }
        // we need to do the same with the 10Gbit link
        uint32_t pollres;
        DEBUG("Doing a software reset of FEROL40");
        HAL::StopWatch watch(1);
        watch.reset();
        watch.start();
        // JRF General Reset of Ferol40.
        ferol40Device_P->setBit("General_reset_bit"); //SOFTWARE_RESET

        try
        {

            ferol40Device_P->pollItem("General_reset_bit", 0, 1000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL); //SOFTWARE_RESET
            watch.stop();
            uint32_t stime = watch.read();
            watch.reset();
            std::stringstream info;
            info << "The software reset took "
                << stime << "us.";
            INFO(info.str());
        }
        catch (HAL::TimeoutException &e)
        {
            watch.stop();
            uint32_t stime = watch.read();
            watch.reset();
            std::stringstream err;
            err << "Timeout while waiting for the end of the reset after "
                << stime << "us.";
            ERROR(err.str());
        }

        //JRF now we reset the QSFP
        ferol40Device_P->setBit("QSFP_reset_r_bit");

        //JRF Now we loop over all streams and if they are enabled we enable the hardware for that stream.
        uint32_t stream_offset(0), index(0);
        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            stream_offset = (streams_it - streams_.begin()) * 0x4000; // this is the HAL address offset to apply for each stream

            if (*streams_it)
            {                                                //if we are using the stream, we enable it.
                ferol40Device_P->setBit("QSFP_reset_r_bit"); //JRF TODO we might want to reset the stream regardless whether it's in or out of the run.
                ferol40Device_P->setBit("enable_FED_link", HAL::HAL_DO_VERIFY, stream_offset);
            }
        }

        //JRF Note. We stop the loop here and do the wait, like that we save a bit of time since we wait once for all 4 streams.

        //JRF We don't unlock the hardware here because we are using a recursive lock.
        ::usleep(1000); // The reset takes 600us

        //uint64_t dest;
        //ferol40Device_P->read64( "FEDx_link_setup", &dest  );
        //std::cout << "FEDx_Link_Setup value " << dest << std::endl;

        //////////////////////////////////////////////////////////////////////////
        //                                                                      //
        // Handle the input side:   Slink Express                               //
        //                                                                      //
        //////////////////////////////////////////////////////////////////////////

        //JRF Note. we do this outside the loop since all the streams are handled inside the method. We should change the name to setDataSources() plural!
        DEBUG("Setting up the Data Source");
        ferol40::DataSourceIF *sourceHandler = DataSourceFactory::createDataSource(ferol40Device_P, appIS_, logger_);
        if (sourceHandler == NULL)
            DEBUG("Wrong DATA SOURCE");
        //JRF TODO handle this error gracefully...
        else
        {
            sourceHandler->setDataSource();
            if (operationMode_ == FEROL40_MODE)
                delete (dynamic_cast<ferol40::Ferol40DataSource *>(sourceHandler));
            else
                DEBUG("Wrong DATA SOURCE");
            //JRF TODO, handle other cases. Note. for the moment we only have this mode.
        }
        //delete(sourceHandler);

        //std::cout << "DEBUG!!!" << std::endl;

        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            index = streams_it - streams_.begin();
            stream_offset = (index)*0x4000;
            if (*streams_it)
            { //JRF  only do anything to the stream if it's being used. TODO check that the hardware locking etc is ok when not using streams.

                DEBUG("reset_status_counters in FEROL40");
                ferol40Device_P->setBit("reset_status_counters", HAL::HAL_NO_VERIFY, stream_offset); //RESET_COUNTERS
                streamBpCounterHigh_[index] = 0;
                streamBpCounterOld_[index] = 0;

                INFO("Suspend Events (stop event generator in case it was not done during a brutal kill of the previous run)");
                this->suspendEvents(); // in case previous run was brute force killed

                DEBUG("Setting FED IDs for streams which participate in this run.");
                ferol40Device_P->write("SLINKXpress_FEDID_set", dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("expectedFedId"))->value_, HAL::HAL_DO_VERIFY, stream_offset); //L10gb_FEDID_SlX0 //JRF NOTE changed the name from streams to InputPorts
                //JRF note we have to mirror this value to the flat application infospace paramter for samim:
                //std::stringstream tmpName;
                //tmpName.str("");
                //tmpName << "expectedFedId_" << index;
                //appIS_.setuint32( tmpName.str(), dynamic_cast<xdata::UnsignedInteger32*>(appIS_.getvector( "InputPorts" )[index].getField("expectedFedId"))->value_ );

                DEBUG("Set the backpressure generation");
                ferol40Device_P->write("Window_trg_stop", appIS_.getuint32("Window_trg_stop"));                                                                                                                        //dynamic_cast<xdata::UnsignedInteger32*>(appIS_.getvector( "InputPorts" )[index].getField("Window_trg_stop"))->value_, HAL::HAL_DO_VERIFY, stream_offset  );//Window_trg_stop
                ferol40Device_P->write("nb_frag_before_BP", dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("nb_frag_before_BP"))->value_, HAL::HAL_DO_VERIFY, stream_offset); //nb_frag_before_BP_L0

                //////////////////////////////////////////////////////////////////////////
                //                                                                      //
                // Setup the 10GB TCP link to the DAQ                                   //
                //                                                                      //
                //////////////////////////////////////////////////////////////////////////

                // JRF check that the links are up.
                DEBUG("Check that the 10Gb DAQ Ethernet link is up"); //JRF TODO, add the stream number to the debug statements.
                try
                {
                    //JRF polling the serdes status we look for bits 0, 2, 3 and 4 to be set.
                    //Bit(0) : Asserted to indicate that the block synchronizer has established synchronization
                    //Bit(1) : Asserted by the BER monitor block to indicate a Sync Header high bit error rate greater than 10-4.
                    //Bit(2) : Asserted when the TX channel is ready to transmit data. Because the readyLatency on this Avalon-ST interface is 0,the MAC may drive tx_ready as soon as it comes out of reset.
                    //Bit(3) : Asserted when the RX reset is complete.
                    //Bit(4) : When asserted, indicates that the PCS is sending data to the MAC. Because the readyLatency on this Avalon-ST interface is 0,the MAC must be ready to receive data whenever this sign
                    //JRF TODO put this back in when dom fixes the firmware...
                    uint32_t value;
                    //ferol40Device_P->pollItem( "eth_10gb_serdes_status", 0x1D, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL , stream_offset);
                    //JRF TODO, we should distinguish between SERDES on Ethernet and SERDES for SLink. make sure we have status params for infospace for both with sensible names
                    ferol40Device_P->read("eth_10gb_serdes_status", &value, stream_offset); //SERDES_STATUS
                    //JRF TODO, store this value in the stream infospace for monitoring... unless we do this exclusively in the monitor.

                    //std::cout << "serdes status: " << value << std::endl;
                }
                catch (HAL::TimeoutException &e)
                {
                    std::stringstream err;
                    err << "Could not poll on DAQ link status. (poll result: " << pollres << " )";
                    ERROR(err.str());
                    //mdioInterface_P->mdiounlock();
                    this->hwunlock();
                    XCEPT_RETHROW(utils::exception::Ferol40Exception, err.str(), e);
                }

                //JRF This has never been used... but we'll add it just in case...
                uint32_t netmask = makeIPNum(appIS_.getstring("IP_NETMASK"));                                       //although we set this for each stream, we will use only a global parameter.
                ferol40Device_P->write("eth_10gb_ctrl_IP_network_def", netmask, HAL::HAL_DO_VERIFY, stream_offset); //IP_NETMASK
                if (netmask)
                {
                    uint32_t gateway = makeIPNum(appIS_.getstring("IP_GATEWAY"));
                    if (gateway == 0)
                    {
                        std::stringstream msg;
                        msg << "The netmask is set to " << appIS_.getstring("IP_NETMASK")
                            << " but the gateway is set to 0. You must set the gateway correctly if you set the netmask, "
                            << "since the Ferol40Controller assumes you want to route your packets over a gateway if you "
                            << "set the netmask. (You can also leave the netmask at 0 and operate the Ferol40 in the ferol40-network "
                            << "without being able to route packets to other networks.)";
                        this->hwunlock();
                        XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
                    }
                    else
                    {
                        ferol40Device_P->write("eth_10gb_ctrl_IP_gateway_def", gateway, HAL::HAL_DO_VERIFY, stream_offset);
                    }
                }

                std::string sourceIPstr = dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[streams_it - streams_.begin()].getField("SourceIP"))->value_; //appIS_.getstring( "SourceIP" );
                uint32_t sourceIp;
                if (sourceIPstr == "auto") //JRF note that this mode is almost never used any more. I leave it in for completeness
                {
                    ferol40Device_P->read("eth_10gb_ctrl_IPS_def", &sourceIp, stream_offset); //IP_SOURCE
                    if (sourceIp == 0)
                    {
                        std::stringstream msg;
                        msg << "The Source IP address for stream " << index << " is set to 0 in the hardware even though it should have been set automatically "
                            << "by some funky script or by DHCP. This automatic mechanism seem to have failed. You need "
                            << "to either debug this, or set the IP_SOURCE parameter to the correct ip address of the Ferol40.";
                        this->hwunlock();
                        XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
                    }
                }
                else //JRF in this case we now check the boolean SourceIpOverride, if it is true, we write the IP if it is false, we check the IP.
                {

                    if (appIS_.getbool("SourceIpOverride"))
                    {
                        sourceIp = makeIPNum(sourceIPstr);
                        // In this case we simply override what is already in the SourceIP from the xml.
                        ferol40Device_P->write("eth_10gb_ctrl_IPS_def", sourceIp, HAL::HAL_DO_VERIFY, stream_offset); //IP_SOURCE
                    }
                    else
                    {
                        // In this case we check what is in the Ferol40 first and throw and error if it is different to the XML
                        ferol40Device_P->read("eth_10gb_ctrl_IPS_def", &sourceIp, stream_offset); //IP_SOURCE
                        if (sourceIp != makeIPNum(sourceIPstr))
                        {
                            std::stringstream err;
                            err << "Source IP on Ferol40 in slot "
                                << getSlot()
                                << ", Input Port "
                                << index
                                << ", differs from that in the configuration. IP on board: "
                                << std::hex << sourceIp << std::dec << ", (" << makeIPString(sourceIp) << ")"
                                << ", Hostname in configuration: "
                                << sourceIPstr
                                << " ("
                                << makeIPString(makeIPNum(sourceIPstr)) << " (" << makeIPNum(sourceIPstr) << ") "
                                << "). Normally the IP address should have been set automatically by the ferolconfig service which runs at PC boot time on the frlpc40 machines."
                                << " If the IP address is ZERO, then you should restart this service to recover the correct IP address. If the IP address is WRONG you should contact an expert. If you ARE the expert, good luck!"
                                << std::endl;
                            ERROR(err.str());

                            XCEPT_DECLARE(utils::exception::Ferol40Exception, top, err.str());
                            throw(top);
                        }
                    }
                }

                // check that our IP address and our MAC addresse are unique on the network.
                doArpProbe(index);

                ferol40Device_P->write("eth_10gb_ctrl_IPD_def",
                        makeIPNum(dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[index].getField("DestinationIP"))->value_),
                        HAL::HAL_DO_VERIFY, stream_offset);                                      //IP_DEST
                ferol40Device_P->write("eth_10gb_ctrl_IP_network_def", 0x0, HAL::HAL_DO_VERIFY, stream_offset); //JRF TODO check if we need to set this at all... //
                ferol40Device_P->write("eth_10gb_ctrl_IP_gateway_def", 0x0, HAL::HAL_DO_VERIFY, stream_offset); //JRF TODO check if we need to set this at all... //
                ferol40Device_P->write("eth_10gb_port_Src_def",
                        dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("TCP_SOURCE_PORT"))->value_,
                        HAL::HAL_DO_VERIFY, stream_offset); //TCP_SOURCE_PORT_FED0
                ferol40Device_P->write("eth_10gb_port_Dest_def",
                        dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("TCP_DESTINATION_PORT"))->value_,
                        HAL::HAL_DO_VERIFY, stream_offset);                  //TCP_DESTINATION_PORT_FED0
                ferol40Device_P->setBit("tx_ready_bit", HAL::HAL_DO_VERIFY, stream_offset); //New
                ferol40Device_P->setBit("rx_ready_bit", HAL::HAL_DO_VERIFY, stream_offset); //New

                //eth_10gb_ctrl_Init_def
                ferol40Device_P->write("eth_10gb_ctrl_Init_def", appIS_.getuint32("TCP_CONFIGURATION"), HAL::HAL_NO_VERIFY, stream_offset); //TCP_CONFIGURATION_FED0
                ferol40Device_P->write("TCP_OPTIONS_MSS_SCALE", appIS_.getuint32("TCP_OPTIONS_MSS_SCALE"));                                 //TCP_OPTIONS_MSS_SCALE_FED0
                ferol40Device_P->write("eth_10gb_Cong_wind_def",
                        dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("TCP_CWND"))->value_,
                        HAL::HAL_DO_VERIFY, stream_offset); //TCP_CWND_FED0 //JRF NOTE, this is the only TCP tuning param which is specific to each stream for the moment.
                ferol40Device_P->write("eth_10gb_ctrl_timer_RTT_def", appIS_.getuint32("TCP_TIMER_RTT"));
                ferol40Device_P->write("eth_10gb_ctrl_timer_RTT_sync_def", appIS_.getuint32("TCP_TIMER_RTT_SYN"));
                ferol40Device_P->write("eth_10gb_ctrl_timer_persist_def", appIS_.getuint32("TCP_TIMER_PERSIST"));
                ferol40Device_P->write("eth_10gb_ctrl_thresold_retrans_def", appIS_.getuint32("TCP_REXMTTHRESH"));
                ferol40Device_P->write("eth_10gb_ctrl_Rexmt_CWND_Sh_def", appIS_.getuint32("TCP_REXMTCWND_SHIFT"));
            } //JRF end of if(streams_)
        }     //JRF End of Loop over steams_

        // Issue an ARP request and wait for the Answer. With a timeout.
        // Since the connection is point to point, we do not need to retry.
        // If ARP packets get lost, there is something strange in our setup.
        uint32_t maxTries = appIS_.getuint32("MAX_ARP_Tries");
        uint32_t arpTimeout = appIS_.getuint32("ARP_Timeout_Ms");
        uint32_t arpok(0);
        uint32_t tries(0);

        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            arpok = 0;
            tries = 0;
            index = streams_it - streams_.begin();
            stream_offset = (index)*0x4000;
            if (*streams_it)
            {
                while ((!arpok) && (tries < maxTries))
                {

                    tries++;
                    ferol40Device_P->setBit("eth_10gb_ARP_req", HAL::HAL_NO_VERIFY, stream_offset);
                    try
                    {
                        ferol40Device_P->pollItem("eth_10gb_MAC_rcv_OK", 1, arpTimeout,
                                &pollres, HAL::HAL_POLL_UNTIL_EQUAL, stream_offset);
                        arpok = 1;
                        DEBUG("ARP reply is ok");
                        //ferol40IS_.setuint32( "TCP_ARP_REPLY_OK", 1 ); //JRF TODO require one for each stream so this must move to the streamInfoSpace.
                        arpok = 1;
                    }
                    catch (HAL::TimeoutException &e)
                    {
                        std::stringstream err;
                        err << "Did not get a response to ARP request for trial " << tries;
                        WARN(err.str());
                    }
                }

                if (!arpok)
                { //JRF TODO fix this error message to loop over the streams.
                    std::stringstream msg;
                    msg << "Could not perform an arp request. Tried " << maxTries << " times. Destination is "
                        << appIS_.getstring("DestinationIP");
                    //JRF since Vitess MDIO is not needed any more, we could replace this code with some code that checks the configuration of the equivalent part of the ferol40 stream.
                    //JRF removing this not needed // mdioInterface_P->mdioRead( 0, 0x8000, &tmp, 1); //JRF TODO what is this vitess stuff do we have 4 of these now?
                    ERROR(msg);
                    this->hwunlock();
                    XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
                }

                // We do this here because it is fixed throughout a run. no need to monitor this more than once.
                // Now read back the Mac addresses of the destination for monitoring purposes.
                uint64_t mac_dest;
                //JRF TODO, rename these registers ... maybe. Note also. The destination is per stream so we need to store this info in the ApplicationInfospace so that the TCPStreamInfoSpace can grab
                //the values from the application infoSpace when monitoring them.
                //ferol40Device_P->read( "MAC_DEST_LO", &mac_dest_lo);
                //ferol40Device_P->read( "MAC_DEST_HI", &mac_dest_hi);
                ferol40Device_P->read64("eth_10gb_Dest_MAC_address", &mac_dest, static_cast<uint64_t>(stream_offset));
                //JRF TODO add a new setuint64 method that just takes a 64 bit number
                //JRF TODO move this item to HardwareStatus...
                ferol40IS_.setuint64("DestinationMACAddress", mac_dest & 0xFFFFFFFF, (mac_dest >> 32) & 0xFFFFFFFF, true);
            }
        } // end for loop

        // Open the TCP connections
        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            index = streams_it - streams_.begin();
            if (*streams_it)
            {
                openConnection(index);
                DEBUG("stream Open"); //JRF TODO add stream number to debug
            }
        }

        //JRF Now we can read back certain montorables which we want to monitor only once after configure._base
        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            index = streams_it - streams_.begin();
            stream_offset = (index)*0x4000;
            if (*streams_it)
            {
                //This loop is to fill those monitor once items on the streams if they exist
            }
        }
        uint32_t value;
        ferol40Device_P->read("eth_10gb_qsfp_setup", &value);
        statusIS_.setbool("QSPF_present", ((value & 0x1) == 0) ? 0 : 1, true);               //QSPF_present
        statusIS_.setbool("QSFP_int", ((value & 0x2) == 0) ? 0 : 1, true);                   //QSFP_int
        statusIS_.setbool("QSFP_low_power_r_bit", ((value & 0x4) == 0) ? 0 : 1, true);       //QSFP_low_power_r_bit
        statusIS_.setbool("DDR3_0_Ready_bit", ((value & 0x8) == 0) ? 0 : 1, true);           //DDR3_0_Ready_bit
        statusIS_.setbool("DDR3_0_Calibration_ok_bit", ((value & 0x10) == 0) ? 0 : 1, true); //DDR3_0_Calibration_ok_bit

        // JRF TODO check how this works and reinstate:
        // ferol40Device_P->write( "ENA_PAUSE_FRAME", appIS_.getbool( "ENA_PAUSE_FRAME" ) );
    }
    catch (HAL::HardwareAccessException &e)
    {
        ERROR("A hardware exception occured : " << e.what());
        //mdioInterface_P->mdiounlock();
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception, "A hardware exception occured.", e);
    }
    catch (utils::exception::Ferol40Exception & e) 
    {
        ERROR("A General Exception occurred : " << e.what());
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception, "A general exception occurred.", e);
    }

    this->hwunlock();
}

void ferol40::Ferol40::openConnection(uint32_t stream)
{
    // JRF TODO, note that input stream value is 0,1,2,3, but remember that we must convert this to base address offsets... 0x4000,0x8000,0xc000
    //

    //std::stringstream item_tcp_open, item_tcp_connection_established, item_tcp_destination_port;
    //item_tcp_open << "TCP_OPEN_FED" << stream;
    //item_tcp_open << "eth_10gb_req_TCP_sync";
    //item_tcp_connection_established << "TCP_CONNECTION_ESTABLISHED_FED" << stream;
    //item_tcp_destination_port << "TCP_DESTINATION_PORT_FED" << stream;

    utils::Watchdog watchdog(appIS_.getuint32("Connection_Timeout_Ms"));
    watchdog.start();
    std::stringstream msg;
    bool connection_established = false;
    ferol40Device_P->setBit("eth_10gb_req_TCP_sync", HAL::HAL_NO_VERIFY, stream * 0x4000);

    uint32_t tcpStatus = 0, oldStatus = 0;
    do
    {
        ::usleep(50000); // 50ms
        if (ferol40Device_P->isSet("TCP_CONNECTION_ESTABLISHED", stream * 0x4000)) //JRF TODO, note that there is a new state requested we can check in this polling for extra security.
        {
            ferol40IS_.setvectorelementuint32("InputPorts", "TCP_CONNECTION_ESTABLISHED", 1, stream, true); //JRF TODO, If this fails, we should get the vector and find the element like that since we get a reference.
            connection_established = true;
            break;
        }

        msg.str("");
        if (catchTcpError(stream, msg, tcpStatus)) //JRF TODO check this...
        {
            if (oldStatus != tcpStatus)
            {
                msg << "...try to establish connection again...\n";
                WARN(msg.str());
                oldStatus = tcpStatus;
            }
            ferol40Device_P->setBit("eth_10gb_req_TCP_sync", HAL::HAL_NO_VERIFY, stream * 0x4000);
        }
    }

    while (!watchdog.timeout());
    if (!connection_established)
    {
        try {
            msg << "Cannot establish connection for Stream " << stream << " (Dst-IP : "
                << dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[stream].getField("DestinationIP"))->value_ //appIS_.getstring( "DestinationIP" )
                << "  Destination Port : "
                << dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[stream].getField("TCP_DESTINATION_PORT"))->value_
                << ")  after " << appIS_.getuint32("Connection_Timeout_Ms") << "ms.";
        }
        catch (std::exception & e) 
        {
            msg << e.what();
        }
        catch (...)
        {    
                std::cout << "unknown exception" << std::endl;
        }
        ERROR(msg.str());
        this->hwunlock();
        XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
    }
}

bool ferol40::Ferol40::catchTcpError(uint32_t stream, std::stringstream &msg, uint32_t &tcpStatus)
{
    std::stringstream item_tcp_stat_connrst;
    item_tcp_stat_connrst << "TCP_STAT_CONNRST_FED" << stream;
    ferol40Device_P->read("eth_10gb_Status_TCP_flags", &tcpStatus, stream * 0x4000);
    //JRF TODO, check how to do this now...

    if (tcpStatus == 0)
        return false;

    if (tcpStatus & 0x01)
        msg << "Connection reset by peer\n";
    else if (tcpStatus & 0x02)
        msg << "Connection closed by Peer. In CMS this should not happen! The RU is not allowed to close the connection. It is the Ferol40 which is supposed to close the connection first!\n";
    else if (tcpStatus & 0x04)
        msg << "Connection refused\n";
    else if (tcpStatus & 0x08)
        msg << "Segment with URG flag received.\n";
    else if (tcpStatus & 0x10)
        msg << "Unexpected squence number received. This probably means that somebody tried to send data to us but the Ferol40 does not support the reception of data via TCP/IP!\n";
    else if (tcpStatus & 0x20)
        msg << "Syn flag received in an established connection...very ugly TCP error...\n";

    return true;
}

void ferol40::Ferol40::stop() throw(utils::exception::Ferol40Exception)
{
    // There is not much to do here. We assume the previous run stopped perfectly and the
    // ferol40 is still up and ready to run. No connections will be closed down. No reset
    // will be done.
    this->hwlock();
    this->suspendEvents();
    this->hwunlock();
    //JRF Adding a check to ensure the BIFI and slink are empty after stopping. This should always be the case.
    if ( ! this->buffersAreEmpty() ) {
        ERROR("Failed to stop as buffers are not empty!!!");
    }
}

bool ferol40::Ferol40::buffersAreEmpty() 
{
    uint32_t index(0);
    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    {
        if (*streams_it)
        {
            index = streams_it - streams_.begin();
            std::cout << index << std::cout;
        }
    }
    if (true) {
        return true;
    } else {
        return false;
    }
}

///////////////////////////////////////////////////////////////////////////
//
// setup event generator if events need to be generated in Ferol40
//
// Logic:
//    o If we are in Ferol40 Data Geenerator mode setup the event generator
//    o If data comes via the optical slink
//      set DAQ-ON on the sender side.
//       - if the data source is the core generator set up that generator.
//
//  The code below is written for clarity and not for optimal compactness.
//
//////////////////////////////////////////////////////////////////////////

void ferol40::Ferol40::enable() throw(utils::exception::Ferol40Exception)
{
    if (!hwCreated_)
        return;

    // Some monitoring counters are reset.
    this->hwlock();
    try
    {
        //JRF TODO loop over all streams....
        ferol40Device_P->setBit("eth_10gb_local_reset", HAL::HAL_NO_VERIFY, 0x0000); //JRF TODO rename to RESET_COUNTERS and put all this in loop over streams." );
        ferol40Device_P->setBit("eth_10gb_local_reset", HAL::HAL_NO_VERIFY, 0x4000);
        ferol40Device_P->setBit("eth_10gb_local_reset", HAL::HAL_NO_VERIFY, 0x8000);
        ferol40Device_P->setBit("eth_10gb_local_reset", HAL::HAL_NO_VERIFY, 0xc000);

        streamBpCounterHigh_[0] = 0; //JRF TODO implement counters for all 4 streams.
        streamBpCounterHigh_[1] = 0;
        streamBpCounterOld_[0] = 0;
        streamBpCounterOld_[1] = 0;

        ferol40Device_P->setBit("reset_status_counters", HAL::HAL_NO_VERIFY, 0x0000);
        ferol40Device_P->setBit("reset_status_counters", HAL::HAL_NO_VERIFY, 0x4000);
        ferol40Device_P->setBit("reset_status_counters", HAL::HAL_NO_VERIFY, 0x8000);
        ferol40Device_P->setBit("reset_status_counters", HAL::HAL_NO_VERIFY, 0xc000);

        // reset sequence number and reset fifo in sender core; needs to be done before using the core.
        ferol40Device_P->writePulse("SLINKXpress_cmd_resync", 0x0000);
        ferol40Device_P->writePulse("SLINKXpress_cmd_resync", 0x4000);
        ferol40Device_P->writePulse("SLINKXpress_cmd_resync", 0x8000);
        ferol40Device_P->writePulse("SLINKXpress_cmd_resync", 0xc000);
    }
    catch (HAL::HardwareAccessException &e)
    {
        ERROR("A hardware exception occured when resetting counters : " << e.what());
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception, "A hardware exception occured.", e);
    }

    //JRF doens't exist any more
    //dataTracker_.reset();
    //JRF reset the used streamDataTrackers in loop
    uint32_t index(0);
    for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    {
        if (*streams_it)
        {
            index = streams_it - streams_.begin();
            tcpStreamDataTrackers_[index].reset(); // register the Data Tracker for each stream, passing in the stream id
            inputStreamDataTrackers_[index].reset();
        }
    }

    //dataTracker2_.reset();

    ////////////////////////////////////////// FEROL40_MODE //////////////////////////////////////////////

    try
    {
        if (operationMode_ == FEROL40_MODE)
        {

            for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
            {
                index = streams_it - streams_.begin();
                uint32_t stream_offset = index * 0x4000;
		std::string triggerMode = appIS_.getstring("Ferol40TriggerMode");
                 
		if (triggerMode == FEROL40_EXTERNAL_TRIGGER_MODE)
                        {
                            ferol40Device_P->setBit("tcds_enable", HAL::HAL_DO_VERIFY);

                        }
		else
                        {
                            ferol40Device_P->resetBit("tcds_enable", HAL::HAL_DO_VERIFY);

                        }

                if (*streams_it)
                {
                    if (dataSource_[index] == GENERATOR_SOURCE) //JRF TODO fix this, should do for all streams.
                    {

                        //JRF Note, this was also not used in the ferol
                        //this->setupEventGen();
                        DEBUG("setting emulator mode in ferol40");
                        Ferol40EventGenerator fevgen(ferol40Device_P, logger_);
                        fevgen.setupEventGenerator(dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_bytes"))->value_,
                                dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Max_bytes"))->value_,
                                dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Stdev_bytes"))->value_,
                                dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_ns"))->value_,
                                dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_Stdev_ns"))->value_,
                                dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("N_Descriptors"))->value_,
                                dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Seed"))->value_,
                                index,
                                dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("expectedFedId"))->value_);

                        //JRF TODO, once we have an external trigger mode, we must test this.
                        if (triggerMode == FEROL40_EXTERNAL_TRIGGER_MODE)
                        {
                            ferol40Device_P->write("thresold_busy", 0x180, HAL::HAL_DO_VERIFY, stream_offset);
                            ferol40Device_P->write("thresold_ready", 0x100, HAL::HAL_DO_VERIFY, stream_offset);
                            ferol40Device_P->setBit("event_ext_trigger", HAL::HAL_DO_VERIFY, stream_offset);

                        }
                        else if (triggerMode == FEROL40_AUTO_TRIGGER_MODE)
                        {
                            //JRF TODO set whatever we have to set for AUTO Triggering.
                            ferol40Device_P->resetBit("event_ext_trigger", HAL::HAL_DO_VERIFY, stream_offset);

                        }
                        //JRF TODO add handling for SOAP Trigger mode.
                        else
                        {
                            WARN("Illegal Ferol40 Trigger mode encounteredL " << triggerMode << ". Using AUTO trigger mode.");
                        }
                        //JRF for now we don't use the loop, we only use the memory.
                        ferol40Device_P->resetBit("event_gen_loop", HAL::HAL_DO_VERIFY, stream_offset);
                        ferol40Device_P->resetBit("event_pci_trigger", HAL::HAL_NO_VERIFY, stream_offset);
                        //ferol40Device_P->write("Event_gen_length", 0x4d0, HAL::HAL_DO_VERIFY, stream_offset; //JRF only used for the loop mode
                        //ferol40Device_P->write("source_value", dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("expectedFedId"))->value_ , HAL::HAL_DO_VERIFY, stream_offset); //JRF only used for the loop mode.
                        //ferol40Device_P->write("event_counter", 1, HAL::HAL_DO_VERIFY, stream_offset); //JRF only used for loop mode.
                        // JRF Now we send one trigger for testing.
                        //ferol40Device_P->setBit( "event_gen_soft_trig", HAL::HAL_DO_VERIFY, 0x0000) ;

                        ::usleep(1000);
                    }

                    else if (dataSource_[index] == L10G_CORE_GENERATOR_SOURCE) //JRF TODO, we must check the dataSource on each stream and configure accordingly, Don't forget to implement the monitoring correctly when different streams use different sources.!
                    {
                        //JRF TODO loop over all streams, remmeber to pass in the stream number for each stream... Of course we have to fix the Slink core and generator classes to work for the ferol40.
                        try
                        {
                            // setting DAQ-OFF in the slink express sender core (Should be already off) and enable internal event generator.
                            daqOff(index, true);

                            SlinkExpressEventGenerator evgeni(ferol40Device_P, slCore_P, logger_); // Paying homage to a great expert of CMS !!! (even though a letter is missing...)
                            evgeni.setupEventGenerator(dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_bytes"))->value_,
                                    dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Max_bytes"))->value_,
                                    dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Length_Stdev_bytes"))->value_,
                                    dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_ns"))->value_,
                                    dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Event_Delay_Stdev_ns"))->value_,
                                    dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("N_Descriptors"))->value_,
                                    dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("Seed"))->value_,
                                    index,
                                    dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[index].getField("expectedFedId"))->value_);
                            // Start the generator
                            uint32_t data = 0x12;
                            slCore_P->write("SlX_GEN_TRIGGER_CONTROL", data, index);
                        }
                        catch (HAL::HardwareAccessException &e)
                        {
                            ERROR("Failed to Configure L10G_CORE_GENERATOR_SOURCE, ");
                            XCEPT_RETHROW(utils::exception::Ferol40Exception,
                                    "Hardware access failed.", e);
                        }
                    }
                    else if (dataSource_[index] == L10G_SOURCE)
                    {
                        // setting DAQ-ON in the slink express sender core
                        DEBUG("switching on DAQ: 10G daqon");
                        daqOn(index); // JRF TODO... again we need to do this on all enabled streams...
                    }
                }
            }
            //JRF call resumeEvents(), this method only resumes events for Generated modes
            this->resumeEvents();
        }
    }
    catch (HAL::HardwareAccessException &e)
    {
        FATAL("Hardware Access failed. " << e.what());
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception,
                "Hardware access failed.", e);
    }

    this->hwunlock();
}

/////////////////////////////////////////////////////
// reset tcp/ip conncetion
// call routine to destroy hardare devices
// reset the data trackers
/////////////////////////////////////////////////////
void ferol40::Ferol40::halt() throw(utils::exception::Ferol40Exception)
{
    if (!hwCreated_)
        return;
    //JRF TODO add all 4 streams and remove handling of 6G links.
    this->hwlock();

    this->suspendEvents();
    try
    {
        uint32_t index(0), stream_offset(0);
        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            if (*streams_it) //JRF if the stream is enabled then we do the business
            {
                index = streams_it - streams_.begin();
                stream_offset = index * 0x4000;

                if (dataSource_[index] == L10G_SOURCE)
                {
                    daqOff(index);
                }
                ferol40Device_P->setBit("eth_10gb_req_TCP_rst", HAL::HAL_NO_VERIFY, stream_offset); // TCP_RESET_FED0
            }
        }
    }
    catch (HAL::HardwareAccessException &e)
    {
        FATAL("Hardware Access failed.");
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception,
                "Hardware access failed.", e);
    }

    // would be better to only update the ferol40 infospace

    // the two functions below also take the hw lock, but they are in the same thread.
    // monitor_.updateAllInfospaces();
    shutdownHwDevice();
    this->hwunlock();
}

void ferol40::Ferol40::suspendEvents() throw(utils::exception::Ferol40Exception)
{
    if (!hwCreated_)
        return;
    if (operationMode_ != FEROL40_MODE)
        return; //JRF TODO, maybe remove this if we only have one operation mode.
    this->hwlock();
    int32_t index(0), stream_offset(0);

    try
    {
        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            if (*streams_it) //JRF if the stream is enabled then we do the business
            {
                index = streams_it - streams_.begin();
                stream_offset = index * 0x4000;

                if (dataSource_[index] == GENERATOR_SOURCE)
                {
                    ferol40Device_P->setBit("event_gen_stop", HAL::HAL_NO_VERIFY, stream_offset); //GEN_TRIGGER_STOP_FED0");
                }
                else if (dataSource_[index] == L10G_CORE_GENERATOR_SOURCE)
                {
                    // Stop the generator
                    uint32_t data = 0x04;
                    slCore_P->write("SlX_GEN_TRIGGER_CONTROL", data, index);
                }
                /*if ( false ) // JRF TODO stream1_ )
                  {
                  if ( dataSource_[0] == GENERATOR_SOURCE )
                  ferol40Device_P->setBit("event_gen_stop",HAL::HAL_DO_VERIFY,stream_offset); //"GEN_TRIGGER_STOP_FED1");
                // In FEDKIT_MODE we never have a core generator connected to the second link. There is 
                // always a sender core connected to this second link and therefore it always plays the 
                // role of a FED. We cannot receive data on the secon input in the fekit. 
                // JRF TODO check if this is true. it could be that we want to be able to do this for FEDKIT too.
                else if ( ( operationMode_ == FEROL40_MODE ) && ( dataSource_[0] == L10G_CORE_GENERATOR_SOURCE ) ) 
                {
                // Stop the generator
                uint32_t data = 0x04;
                slCore_P->write( "SlX_GEN_TRIGGER_CONTROL", data, 1 );
                }
                }*/
            } //end if
        }     // end for loop
    }
    /*    catch (HAL::HardwareAccessException &e)
          {
          FATAL("Hardware Access failed.");
          this->hwunlock();
          XCEPT_RETHROW(utils::exception::Ferol40Exception,
          "Hardware access failed.", e);
          }*/
    catch (utils::exception::Ferol40Exception &e)
    {
        WARN("Communication problem during suspendEvents() with slink no " << index << "  : " << e.what());
    }

    this->hwunlock();
}

void ferol40::Ferol40::resumeEvents() throw(utils::exception::Ferol40Exception)
{
    if (!hwCreated_)
        return;
    if (operationMode_ != FEROL40_MODE)
        return; //JRF TODO, can maybe remove this if we only have one mode.

    this->hwlock();

    try
    {
        uint32_t index(0), stream_offset(0);
        for (std::vector<bool>::iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
        {
            if (*streams_it) //JRF if the stream is enabled then we do the business
            {
                index = streams_it - streams_.begin();
                stream_offset = index * 0x4000;

                if (dataSource_[index] == GENERATOR_SOURCE)

                    ferol40Device_P->setBit("event_gen_start", HAL::HAL_NO_VERIFY, stream_offset); //"GEN_TRIGGER_START_FED0");

                else if (dataSource_[index] == L10G_CORE_GENERATOR_SOURCE)

                {
                    // Start the generator
                    uint32_t data = 0x10;
                    slCore_P->write("SlX_GEN_TRIGGER_CONTROL", data, index);
                }
            } //end if
        }     //end for loop
    }
    catch (HAL::HardwareAccessException &e)
    {
        FATAL("Hardware Access failed.");
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception,
                "Hardware access failed.", e);
    }
    this->hwunlock();
}

void ferol40::Ferol40::hwlock()
{
    deviceLock_.take();
}

void ferol40::Ferol40::hwunlock()
{
    deviceLock_.give();
}

/////////////////////////////////// update monitoring info ////////////////////////////

bool ferol40::Ferol40::updateInfoSpace(utils::InfoSpaceHandler *is, uint32_t streamNo)
{
    this->hwlock();

    if (!hwCreated_ && ( is->name() != "StreamConfig") )//JRF If hwCreated is false, we are not using the hardware, so we should not monitor anything except the enable on the StreamConfig InfoSpace
    {
        this->hwunlock();
        return false; //don't push when we're not monitoring
    }
    std::list<utils::InfoSpaceHandler::ISItem> items = is->getItems();
    std::list<utils::InfoSpaceHandler::ISItem>::const_iterator it;

    //JRF we get here if we are monitoring one of the stream info spaces.
    if (!hwCreated_) //JRF note that !hwCreated_ implies that all streams are disabled. 
    {
        for (it = items.begin(); it != items.end(); it++)
        {
            //JRF First we check all streams are disabled. If they are, we only update the enable flag and then return

            if ((*it).gethwname() == "enable")
            {
                is->setbool((*it).name, dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[streamNo].getField("enable"))->value_);
                break; //JRF we have set the only thing we need to set in this case.
            }
            continue; // skip this item go to the next, we are looking for "enable" only while the stream is disable.
        }
        this->hwunlock();
        is->writeInfoSpace();
        return true; // we have to push in this case for StreamConfig
    }

    //JRF we only get here if we have the hardware lock.

    uint32_t stream_offset = streamNo * 0x4000; //JRF TODO replace 0x4000 by a constant.
    // Later there are checks which should be done only in specific states. Since the state changes
    // happen in a different thread there is a potential race condition which results in hardware register
    // values which were sampled at the previous state. Therefore we first sample the state here and once
    // more at the end of the retrieval  of hardware values and apply the checks only if there is not
    // state change in between.
    std::string state1 = statusIS_.getstring("stateName");

    //JRF TODO, we should do this for each stream...
    try
    {
        //JRF TODO put this back once we have the latch from Dom
        //ferol40Device_P->setBit( "LATCH_REGISTERS" );
        ferol40Device_P->setBit("eth_10gb_latch_values", HAL::HAL_NO_VERIFY, stream_offset);
        ferol40Device_P->setBit("Latch_reg_Bifi", HAL::HAL_NO_VERIFY);

        if (is->name() == "TCPStream")
        {
            //dataTracker2_.update();//JRF replaced this with a vector of stream Data trackers below
            tcpStreamDataTrackers_[streamNo].update(streamNo);
            //std::cout << "just called update() on tcpStreamDataTrackers_[streamNo], streamNo = " << streamNo << std::endl;
        }
        else if (is->name() == "InputStream")
            inputStreamDataTrackers_[streamNo].update(streamNo);
        else
            //JRF doesn't exist any more
            //dataTracker_.update();

            //JRF TODO, add the SFP items to InputStreamInfoSpace...
            try
            {
                //JRF TODO, put this back in when ready. readSFP(streamNo);
            }
        catch (utils::exception::Ferol40Exception &e)
        {
            // the run should continue... we only read mdio for monitoring purposes
            WARN(e.what());
        }

        for (it = items.begin(); it != items.end(); it++)
        {

            /////////////////// items read from hardware registers ///////////////////

            if ((*it).update == utils::InfoSpaceHandler::HW32)
            {

                uint32_t value;
                if (is->name() == "TCPStream" || is->name() == "InputStream" || is->name() == "StreamConfig")
                    ferol40Device_P->read((*it).gethwname(), &value, stream_offset); //JRF Note we are enforcing that all hardware items in stream info spaces require the offset
                //if ((*it).gethwname() == "WrongFEDIdDetected" )
                //    std::cout << "updateInfoSpace, item name = " << (*it).gethwname() << ", Value = " << value << std:: endl;
                else
                    ferol40Device_P->read((*it).gethwname(), &value); //JRF Note we are enforcing that ALL hardware items in non-stream info spaces require no offset.

                if ((*it).type == utils::InfoSpaceHandler::UINT32)
                    is->setuint32((*it).name, value);
                else
                    is->setuint64((*it).name, 0, value); //JRF TODO implement a true 64 bit method
            }
            else if ((*it).update == utils::InfoSpaceHandler::HW64)
            {
                uint64_t value;
                if (is->name() == "TCPStream" || is->name() == "InputStream" || is->name() == "StreamConfig")
                {
                    ferol40Device_P->read64((*it).gethwname(), &value, stream_offset);
                }
                else
                {
                    ferol40Device_P->read64((*it).gethwname(), &value);
                }
                is->setuint64((*it).name, value);
            }

            ////////////////////// items read from the Slink express ////////////////////

            else if ((*it).update == utils::InfoSpaceHandler::SLE) //JRF TODO check the implementation of slink core registers... we need to implement an offset for the read/write to slink
            {
                uint32_t value(0);
                try
                {
                    if (is->name() == "TCPStream" || is->name() == "InputStream" || is->name() == "StreamConfig")
                        //JRF TODO, reinstate this once the Slink Core utils::Monitoring has been fixed and is not timing out all the time.
                        slCore_P->read((*it).gethwname(), &value, streamNo); //JRF TODO implement this method
                    else
                        slCore_P->read((*it).gethwname(), &value);


                    if ((*it).type == utils::InfoSpaceHandler::UINT32)
                        is->setuint32((*it).name, value);
                    else
                        is->setuint64((*it).name, value, 0); //JRF TODO check why the 32 bit value is loaded into the MSBs...
                }
                catch (HAL::HardwareAccessException &e)
                {
                    WARN(e.what());
                }
                catch (utils::exception::Ferol40Exception &e)
                {
                    WARN(e.what());
                }
            }

            ///////////////////////// calculated tracker values /////////////////////////

            else if ((*it).update == utils::InfoSpaceHandler::TRACKER)
            {
                if (is->name() == "TCPStream")
                { //JRF We only have calculated data trackers on the hardware streams, no need on the streamConfig
                    is->setdouble((*it).name, tcpStreamDataTrackers_[streamNo].get((*it).gethwname()));
                }
                else if (is->name() == "InputStream")
                {
                    is->setdouble((*it).name, inputStreamDataTrackers_[streamNo].get((*it).gethwname()));
                }
                else
                {
                    //JRF doesnt exist any more
                    //is->setdouble((*it).name, dataTracker_.get((*it).name));
                }
            }

            ///////////////////////// calculated quantities /////////////////////////

            else if ((*it).update == utils::InfoSpaceHandler::PROCESS)
            {
                //                            if ( is->name() == "TCPStream" )
                //                                {
                //                                    DEBUG( (*it).gethwname() );
                //                                }
                if ((*it).name == "AccSlinkFullSeconds") //InputStreamInfoSpace
                {
                    try
                    {
                        //uint64_t value = is->getuint64( "BackpressureCounter" );
                        //JRF TODO check this calculation is ok.
                        uint32_t value(0);
                        //JRF TODO, put this back in when it's fixed and not timing out in Global DAQ.
                        //slCore_P->read("SlX_BP_counter", &value, streamNo);
                        uint32_t freq = is->getuint32("FEDFrequency");
                        // keep track of overflow since at 100Mhz the 32 bit value is only good for 42 seconds...
                        if (value < streamBpCounterOld_[streamNo])
                        {
                            streamBpCounterHigh_[streamNo] += 1;
                        }
                        streamBpCounterOld_[streamNo] = value;
                        uint64_t bcount = (((uint64_t)streamBpCounterHigh_[streamNo]) << 32) + value;
                        is->setuint64("BackpressureCounter", bcount);
                        // The frequency might not have been set yet if this is the first udate of the monitoring.
                        if (freq > 0)
                        {
                            double acctime = (double)bcount / (1000.0 * (double)freq);
                            is->setdouble("AccSlinkFullSeconds", acctime);
                        }
                        else
                        {
                            is->setdouble("AccSlinkFullSeconds", 0.0);
                        }
                    }
                    catch (HAL::HardwareAccessException &e)
                    {
                        WARN(e.what());
                    }
                    catch (utils::exception::Ferol40Exception &e)
                    {
                        WARN(e.what());
                    }
                }
                else if ((*it).name == "AccBackpressureSeconds")
                {
                    uint64_t value;
                    // This item is a "PROCESS" item: HW64
                    if (dynamic_cast<xdata::String *>( appIS_.getvector("InputPorts")[streamNo].getField("DataSource"))->value_ == "GENERATOR_SOURCE")
                        ferol40Device_P->read64("BackpressureToEMU", &value, stream_offset); //Note the clock has changed to 156.25MHz
                    else
                        ferol40Device_P->read64("BackpressureToSlink", &value, stream_offset); //Note the clock has changed to 156.25MHz
                    // The frequency for the counter comes from a 156.25MHz clock
                    double acctime = (double)value / ((double)156250000.0);
                    is->setdouble("AccBackpressureSeconds", acctime);
                    //is->setdouble("AccBackpressureSecond", acctime); //JRF TODO. Deprecated, remove when not used.
                }
                else if ((*it).name == "AccBIFIBackpressureSeconds")
                {
                    //JRF NOTE this counter runs at 125MHz
                    uint64_t value;
                    ferol40Device_P->read64("BACK_PRESSURE_BIFI", &value, stream_offset);
                    double acctime = (double)value / ((double)175000000.0);
                    is->setdouble("AccBIFIBackpressureSeconds", acctime);
                }
                else if ((*it).name == "LatchedTimeBackendSeconds") //JRF Backend is TCP / ethernet daq link.
                {
                    uint64_t value;
                    ferol40Device_P->read64("eth_10gb_Accu_ack_timer", &value, stream_offset);
                    double acctime = (double)value / ((double)156251244.0);
                    is->setdouble("LatchedTimeBackendSeconds", acctime);
                }
                else if ((*it).name == "LatchedTimeFrontendSeconds") //JRF Frontend is slink and bifi
                {
                    uint64_t value;
                    ferol40Device_P->read64("TimeStamp", &value);
                    double acctime = (double)value / ((double)156251244.0);
                    is->setdouble("LatchedTimeFrontendSeconds", acctime);
                }
                else if ((*it).name == "LatchedSlinkSenderClockSeconds")
                {
                    //JRF TODO, implement this in firmware. Does not exist.
                    is->setdouble("LatchedSlinkSenderClockSeconds", 0.0);
                }
                else if ((*it).gethwname() == "DataSource") //JRF TODO currently only in Ferol40InfoSpace
                {
                    std::string value = dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[streamNo].getField("DataSource"))->value_;
                    is->setstring((*it).name, value);
                }
                else if ((*it).gethwname() == "enable") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setbool((*it).name, dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[streamNo].getField("enable"))->value_);
                }
                else if ((*it).gethwname() == "DestinationIP") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setstring((*it).name, dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[streamNo].getField("DestinationIP"))->value_);
                }
                else if ((*it).gethwname() == "SourceIP") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setstring((*it).name, dynamic_cast<xdata::String *>(appIS_.getvector("InputPorts")[streamNo].getField("SourceIP"))->value_);
                }
                else if ((*it).gethwname() == "TCP_SOURCE_PORT") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("TCP_SOURCE_PORT"))->value_);
                }
                else if ((*it).gethwname() == "TCP_DESTINATION_PORT") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("TCP_DESTINATION_PORT"))->value_);
                }
                else if ((*it).gethwname() == "ENA_PAUSE_FRAME") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setbool((*it).name, dynamic_cast<xdata::Boolean *>(appIS_.getvector("InputPorts")[streamNo].getField("ENA_PAUSE_FRAME"))->value_);
                }
                else if ((*it).gethwname() == "TCP_CWND") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("TCP_CWND"))->value_);
                }
                else if ((*it).gethwname() == "nb_frag_before_BP") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("nb_frag_before_BP"))->value_);
                }
                else if ((*it).gethwname() == "Maximal_fragment_size") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("Maximal_fragment_size"))->value_);
                }
                else if ((*it).gethwname() == "N_Descriptors") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("N_Descriptors"))->value_);
                }
                else if ((*it).gethwname() == "Event_Length_bytes") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("Event_Length_bytes"))->value_);
                }
                else if ((*it).gethwname() == "Event_Length_Stdev_bytes") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("Event_Length_Stdev_bytes"))->value_);
                }
                else if ((*it).gethwname() == "Event_Length_Max_bytes") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("Event_Length_Max_bytes"))->value_);
                }
                else if ((*it).gethwname() == "Event_Delay_ns") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("Event_Delay_ns"))->value_);
                }
                else if ((*it).gethwname() == "Event_Delay_Stdev_ns") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("Event_Delay_Stdev_ns"))->value_);
                }
                else if ((*it).gethwname() == "Seed") //JRF TODO currently only in Ferol40InfoSpace
                {
                    is->setuint32((*it).name, dynamic_cast<xdata::UnsignedInteger32 *>(appIS_.getvector("InputPorts")[streamNo].getField("Seed"))->value_);
                }

                else if ((*it).gethwname() == "PACK_REXMIT") //JRF TODO, currently only in Ferol40Infospace, needs to be moved...
                {
                    uint32_t rexmit, sndpack;
                    ferol40Device_P->read("TCP_STAT_SNDREXMITPACK", &rexmit, stream_offset);
                    ferol40Device_P->read("TCP_STAT_SNDPACK", &sndpack, stream_offset); //JRF TODO change this to the 64 bit register... or add that one...
                    if (sndpack > 0)
                    {
                        is->setdouble((*it).name, ((double)rexmit) / ((double)sndpack));
                    }
                    else
                    {
                        is->setdouble((*it).name, 0.0);
                    }
                }

                else if ((*it).gethwname() == "BIFI") //JRF TODO currently only in Ferol40InfoSpace
                {
                    uint64_t used;
                    ferol40Device_P->read64("BIFI_USED", &used, stream_offset);
                    //std::cout << "StreamID = " << streamNo << "BIFI_USED" << used << std::endl;
                    is->setdouble((*it).name, ((double)used) / ((double)134213632.0)); //524287.0 ) );
                }

                else if ((*it).gethwname() == "BIFI_MAX")
                {
                    uint64_t used;
                    ferol40Device_P->read64("BIFI_USED_MAX", &used, stream_offset);
                    is->setdouble((*it).name, ((double)used) / ((double)134213632.0)); //524287.0 ) );
                    //std::cout << "StreamID = " << streamNo << "BIFI_MAX" << used << std::endl;
                }

                else if ((*it).gethwname() == "RTT")
                {
                    uint32_t rtt;
                    ferol40Device_P->read("TCP_STAT_MEASURE_RTT", &rtt, stream_offset);
                    is->setdouble((*it).name, ((double)rtt) / ((double)156.25));
                }

                else if ((*it).gethwname() == "RTT_MAX")
                {
                    uint32_t rtt;
                    ferol40Device_P->read("TCP_STAT_MEASURE_RTT_MAX", &rtt, stream_offset);
                    is->setdouble((*it).name, ((double)rtt) / ((double)156.25));
                }

                else if ((*it).gethwname() == "RTT_MIN")
                {
                    uint32_t rtt;
                    ferol40Device_P->read("TCP_STAT_MEASURE_RTT_MIN", &rtt, stream_offset);
                    is->setdouble((*it).name, ((double)rtt) / ((double)156.25));
                }

                else if ((*it).gethwname() == "AVG_RTT")
                {
                    uint32_t count;
                    uint64_t sum;
                    ferol40Device_P->read64("TCP_STAT_MEASURE_RTT_SUM", &sum, stream_offset);
                    ferol40Device_P->read("TCP_STAT_MEASURE_RTT_COUNT", &count, stream_offset);
                    if (count > 0)
                    {
                        is->setdouble((*it).name, ((double)sum) / ((double)count) / ((double)156.25));
                    }
                    else
                    {
                        is->setdouble((*it).name, 0);
                    }
                }

                else if ((*it).gethwname() == "ACK_DELAY_MAX")
                {
                    uint32_t delay;
                    ferol40Device_P->read("TCP_STAT_ACK_DELAY_MAX", &delay, stream_offset);
                    is->setdouble((*it).name, ((double)delay) / ((double)156.25));
                }

                else if ((*it).gethwname() == "ACK_DELAY")
                {
                    uint32_t delay;
                    ferol40Device_P->read("TCP_STAT_ACK_DELAY", &delay, stream_offset);
                    is->setdouble((*it).name, ((double)delay) / ((double)156.25));
                }

                else if ((*it).gethwname() == "AVG_ACK_DELAY")
                {
                    uint64_t timer;
                    ferol40Device_P->read64("TCP_STAT_TIMER", &timer, stream_offset);
                    uint32_t ackcnt;
                    ferol40Device_P->read("TCP_STAT_ACK_COUNT", &ackcnt, stream_offset);
                    if (ackcnt > 0)
                    {
                        is->setdouble((*it).name, ((double)timer / (double)ackcnt) / (double)156.25);
                    }
                    else
                    {
                        is->setdouble((*it).name, 0.0);
                    }
                }
            }
        }
    }
    catch (HAL::HardwareAccessException &e)
    {
        ERROR(e.what());
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        ERROR(e.what());
    }
    catch (...)
    {
        ERROR("unknown error occurred when reading registers for monitoring loop");
    }
    // The following lines access the hardware and need to be done here
    // before the hw lock is released. The result is used further below.
    std::stringstream tcpErrorStream0, tcpErrorStream1;
    uint32_t tcpStatusStream0, tcpStatusStream1;
    catchTcpError(0, tcpErrorStream0, tcpStatusStream0);
    catchTcpError(1, tcpErrorStream1, tcpStatusStream1);

    this->hwunlock();

    is->writeInfoSpace();

    // At this point we can do some post processing. Spontaneous state changes
    // which are indications of failures should be detetected here:

    // If we are in the state configured, enabled or paused we should have the
    // connections up for enabled streams
    std::string state2 = statusIS_.getstring("stateName");

    // For the time being the following checks should only be executed on the old FEROL40_IS
    // When the migration of the monitoring is finished this FEROL40_IS dissapears and the
    // checks will be executed on the new Input Info space handler.
    // JRF TODO, fix this and/or remove it.
    if (is->name() == "Ferol40_IS")
    {

        if ((state1 == state2) && (state1 == "Configured" || state1 == "Enabled" || state1 == "Paused"))
        {

            if (appIS_.getvector("InputPorts")[0].getField("enable") && (is->getuint32("TCP_CONNECTION_ESTABLISHED_FED0") == 0))
            {
                std::stringstream msg;
                msg << "Lost TCP/IP connection in stream 0." << tcpErrorStream0.str();
                ERROR(msg.str());
                XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
            }
            if (appIS_.getvector("InputPorts")[1].getField("enable") && (is->getuint32("TCP_CONNECTION_ESTABLISHED_FED1") == 0))
            {
                std::stringstream msg;
                msg << "Lost TCP/IP connection in stream 1." << tcpErrorStream1.str();
                ;
                ERROR(msg.str());
                XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
            }
            if ((appIS_.getvector("InputPorts")[0].getField("enable") || appIS_.getvector("InputPorts")[1].getField("enable")) &&
                    is->getuint32("SERDES_STATUS") == 0)
            {
                std::stringstream msg;
                msg << "Lost SERDES. 10Gb SERDES to DAQ not up any more.";
                ERROR(msg.str());
                XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
            }

            // check if there has appeared a conflict on the network. However, do not send a ARP_PROBE!
            // this is protected with a hwlock internally
            std::cout << "about to doArpProbe in monitor loop" << std::endl;
            this->doArpProbe(0, false); //JRF todo, we must add a loop over streams here... not even sure what this does yet...
        }
    }
    return true;
}

////////////////////////////// utilities ////////////////////////////

    std::string
ferol40::Ferol40::makeIPString(uint32_t ip) throw(utils::exception::Ferol40Exception)
{
    std::string res = "";
    char buf[INET_ADDRSTRLEN];
    //ip = ((ip & 0xff000000) >> 24) + ((ip & 0xff0000) >> 8) + ((ip & 0xff00) << 8) + ((ip & 0xff) << 24);
    const char *tmp = inet_ntop(AF_INET, &ip, buf, INET_ADDRSTRLEN);
    if (tmp == NULL)
    {
        std::string msg = "Could not convert IP to string : " + ip;
        ERROR(msg);
        XCEPT_RAISE(utils::exception::Ferol40Exception, msg);
    }
    else
    {
        res = std::string(tmp);
    }
    return res;
}

    uint32_t
ferol40::Ferol40::makeIPNum(std::string hoststr) throw(utils::exception::Ferol40Exception)
{
    uint32_t dec;
    std::string ipstr;

    // Check if ipstr is an IP or a hostname
    std::string regex = "([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})";
    if (!toolbox::regx_match(hoststr, regex))
    {
        // we have a hostname
        struct hostent res;
        struct hostent *resptr;
        int myerrno;
        char buf[1024];
        // This should be re-tried if it does not work!!!
        int ret = gethostbyname_r(hoststr.c_str(), &res, buf, 1024, &resptr, &myerrno);
        if (ret || !resptr)
        {
            // We have a problem
            char msg[1024];
            char *err;
            if (myerrno == -1)
                err = strerror_r(errno, msg, 1024);
            else
                err = (char *)hstrerror(myerrno);
            FATAL(std::string(err));
            XCEPT_RAISE(utils::exception::Ferol40Exception, std::string(err));
        }
        char *addr = res.h_addr_list[0];
        ipstr = makeIPString(*((uint32_t *)addr));
        DEBUG("Found address " << ipstr << " from hoststring " << hoststr);
    }
    else
    {
        ipstr = hoststr;
    }

    int res = inet_pton(AF_INET, ipstr.c_str(), &dec);
    if (res != 1)
    {
        std::string msg = "Could not convert IP string to number : " + ipstr;
        ERROR(msg);
        XCEPT_RAISE(utils::exception::Ferol40Exception, msg);
    }
    else
    {
        dec = ((dec & 0xff000000) >> 24) + ((dec & 0xff0000) >> 8) + ((dec & 0xff00) << 8) + ((dec & 0xff) << 24);
    }

    return dec;
}

    std::string
ferol40::Ferol40::uint64ToMac(uint64_t mac)
{
    std::stringstream result;
    uint32_t byte1 = mac & 0xffll;
    uint32_t byte2 = (mac & 0xff00ll) >> 8;
    uint32_t byte3 = (mac & 0xff0000ll) >> 16;
    uint32_t byte4 = (mac & 0xff000000ll) >> 24;
    uint32_t byte5 = (mac & 0xff00000000ll) >> 32;
    uint32_t byte6 = (mac & 0xff0000000000ll) >> 40;

    result << std::setw(2) << std::setfill('0') << std::hex << byte6 << ":" << std::setw(2) << byte5 << ":" << std::setw(2) << byte4 << ":" << std::setw(2) << byte3 << ":" << std::setw(2) << byte2 << ":" << std::setw(2) << byte1;

    return result.str();
}

void ferol40::Ferol40::doArpProbe(uint32_t stream_no, bool doProbe)
{
    uint32_t timeout = 1000; // one second
    uint32_t pollres;
    uint32_t stream_offset = stream_no * 0x4000;

    this->hwlock();
    try
    {
        if (doProbe)
        {
            ferol40Device_P->setBit("eth_10gb_probe_ARP", HAL::HAL_NO_VERIFY, stream_offset);
            ferol40Device_P->pollItem("eth_10gb_ARP_done", 1, timeout, &pollres, HAL::HAL_POLL_UNTIL_EQUAL, stream_offset);
            // wait a bit since the 4th arp probe also might trigger a reply.
            ::usleep(100000);
        }
        uint32_t mac_conflict(0), ip_conflict(0);
        uint64_t offendingMac;
        //JRF TODO find out if we still need to do this...
        ferol40Device_P->read("eth_10gb_MAC_Conflict", &mac_conflict, stream_offset); //JRF TODO check that we might need to read64 here if these numbers get really big... not likely.
        ferol40Device_P->read("eth_10gb_IP_Conflict", &ip_conflict, stream_offset);
        std::stringstream msg;
        if (mac_conflict != 0)
        {
            msg << "A mac-address conflict has been detected on the network for stream " << stream_no << ". (This means some device "
                << "answered to an ARP request issued with our IP address. We have a duplicate MAC address "
                << "on the network! THIS IS A DISASTER !!!\n";
        }
        if (ip_conflict != 0)
        {
            msg << "An ip-address conflict has been detected on the network for stream " << stream_no << ". (This means some device "
                << "answered to an ARP request issued with our IP address. We have a duplicate IP address "
                << "on the network! THIS IS A DISASTER !!!\n";
            //JRF TODO, do we need to do this?
            ferol40Device_P->read64("eth_10gb_MAC_WITH_IP_CONFLICT", &offendingMac, stream_offset);
            msg << "The offending device which has our IP address has the MAC address : "
                << uint64ToMac(offendingMac);
        }
        if (ip_conflict || mac_conflict)
        {
            ERROR(msg.str());
            this->hwunlock();
            XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
        }
    }
    catch (HAL::TimeoutException &e)
    {
        std::stringstream err;
        err << "Encountered timeout (1 sec) while waiting for ARP_PROBE to finish.";
        ERROR(err.str());
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception, err.str(), e);
    }
    catch (HAL::HardwareAccessException &e)
    {
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception,
                "Problem with hardware access while doing ARP_PROBE.", e);
    }
    this->hwunlock();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////// a temporary hack to play with the Serdes ///////////////////

void ferol40::Ferol40::controlSerdes(bool on) throw(utils::exception::Exception)
{

    //JRF TODO, check all this, what it's for and edit accordingly
    //
    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
    // this hack is needed to toggle link up and down without having configured the ferol40.   //
    // The hardware device needs to be created to play with this bit. We will use this in    //
    // future again since there are problems in the switch at point 5.                       //
    bool hwhack = false;
    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

    if (!hwCreated_)
    {
        ERROR("Cannot play with the serdes/laser since the HardwareDevice has not been created yet!");

        // return;

        //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

        hwhack = true;

        this->createFerol40Device();
    }
    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////

    try
    {

        if (on)
        {
            this->hwlock();
            uint32_t pollres;
            try
            {
                ferol40Device_P->pollItem("SERDES_STATUS", 1, 5000, &pollres,
                        HAL::HAL_POLL_UNTIL_EQUAL);
                this->hwunlock();
                DEBUG("SERDES should be up now.");
            }
            catch (HAL::TimeoutException &e)
            {
                this->hwunlock();
                std::stringstream err;
                err << "Could not initialize Ferol40 SerDes. (poll result: " << pollres << " )";
                ERROR(err.str());
                XCEPT_RETHROW(utils::exception::Ferol40Exception, err.str(), e);
            }
        }
        //            else
        //                {
        //                    this->hwlock();
        //                    ferol40Device_P->write( "SERDES_INIT_FEROL40", 0x100 );
        //                    this->hwunlock();
        //                    DEBUG( "SERDES should be down now");
        //                }
    }
    catch (HAL::HardwareAccessException &e)
    {
        this->hwunlock();
        XCEPT_RETHROW(utils::exception::Ferol40Exception,
                "Problem with hardware access while playing with 10G SERDES.", e);
    }

    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
    if (hwhack)
    {
        if (ferol40Device_P)
        {
            this->hwlock();
            hwCreated_ = false;
            delete ferol40Device_P;
            ferol40Device_P = NULL;
            this->hwunlock();
            DEBUG("deleted quick and dirty hardware device...");
        }
    }
    //////////////////////////// !!!!!!!! to be removed !!!!!!!!!!!! //////////////////////////
}

void ferol40::Ferol40::readSFP(uint32_t portNo) throw(utils::exception::Ferol40Exception)
{
    //JRF TODO rewrite this to access the sfp through the new address table, rather than through the vitess and MDIO.
    if (ferol40Device_P == NULL)
    {
        std::stringstream err;
        err << "Illegal operation: You must configure the Ferol40Controller before accessing the hardware!";
        XCEPT_DECLARE(utils::exception::Ferol40Exception, top, err.str());
        throw(top);
    }

    uint32_t length = 128;
    char sdata[length];
    u_char *data;
    // remove the resets
    //uint32_t oldReset(0);

    std::stringstream portNoStr;
    portNoStr << portNo;

    //DEBUG( "release the reset of both vitesse chips (VT0/1_RESET_N to '1')" );
    //JRF TODO fix the following line
    //ferol40Device_P->read("SERDES_SETUP_FEROL40", &oldReset );
    //std::cout << " old reset " <<  std::hex << oldReset << std::endl;
    /*if ((portNo == 0) && ((oldReset & 0x80) == 0))
      {
      return;
      }
      else if ((portNo == 1) && ((oldReset & 0x80000000) == 0))
      {
      return;
      }
      else if (portNo > 1)
      {
      return;
      }*/

    // first thing to do is to check if there is a SFP plugged into the cage. This can be
    // done to read the state of the MSTCODE[1] pin which is mapped to bit 14 in register 0xE607 of device 1 (1xE607)
    //
    uint32_t value(0);
    //JRF TODO repalce mdio stuff with direct access...
    //mdioInterface_P->mdioRead( portNo, 0xE607, &value, 1 );
    bool sfpPresent = !(value & 0x4000);

    if (!sfpPresent)
    {
        inputIS_.setstring(std::string("SFP_VENDOR"), "n.a.");
        inputIS_.setstring(std::string("SFP_PARTNO"), "n.a.");
        inputIS_.setstring(std::string("SFP_DATE"), "n.a.");
        inputIS_.setstring(std::string("SFP_SN"), "n.a.");
        inputIS_.setdouble(std::string("SFP_VCC"), 0.0);
        inputIS_.setdouble(std::string("SFP_TEMP"), 0.0);
        inputIS_.setdouble(std::string("SFP_BIAS"), 0.0);
        inputIS_.setdouble(std::string("SFP_TXPWR"), 0.0);
        inputIS_.setdouble(std::string("SFP_RXPWR"), 0.0);
        return;
    }

    // now we reset the two wire interface
    //mdioInterface_P->mdioWrite( portNo, 0xEF01, 0x8000, 1);

    // setup the reading of the conventional SFP memory.
    // This is found at address A0. It needs to be shifted >>1 for the
    // Vitesse.
    //mdioInterface_P->mdioWrite( portNo, 0x8002, 0x0050 );
    //mdioInterface_P->mdioWrite( portNo, 0x8004, 0x1000 );
    try
    {
        //          INFO("first i2c");
        i2cCmd(portNo, 0x0002);
    }
    catch (utils::exception::Ferol40Exception &e)
    {
        return;
        //XCEPT_RETHROW( utils::exception::Ferol40Exception, "", e);
    }

    //mdioInterface_P->mdioReadBlock( portNo, 0x8007, sdata, length );

    std::string vendor(&(sdata[20]), 16);
    std::string partNumber(&(sdata[40]), 20);
    std::string serialNo(&(sdata[68]), 16);
    std::string date(&(sdata[84]), 8);

    // Infospace Items are not pushed through since this function is called in the
    // updateInfospace method, which in the end dows a global push through of all
    // infospace variables.
    inputIS_.setstring(std::string("SFP_VENDOR"), vendor);
    inputIS_.setstring(std::string("SFP_PARTNO"), partNumber);
    inputIS_.setstring(std::string("SFP_DATE"), date);
    inputIS_.setstring(std::string("SFP_SN"), serialNo);

    // Now do the readout of the Enhanced Feature set memory (Address 0xA2).
    // The Vitesse again wants this address to be shifted >> 1 so that it becaomes 0x51.
    //mdioInterface_P->mdioWrite( portNo, 0x8002, 0x0051 );
    //mdioInterface_P->mdioWrite( portNo, 0x8004, 0x1000 );
    //    INFO("Second i2c");
    i2cCmd(portNo, 0x0002);

    //mdioInterface_P->mdioReadBlock( portNo, 0x8007, sdata, length );

    data = (u_char *)sdata;

    double temp = getTemp(&data[96]);
    double Vcc = getVoltage(&data[98]);
    double txBias = getCurrent(&data[100]);
    double txPower = getPower(&data[102]);
    double rxPower = getPower(&data[104]);

    inputIS_.setdouble(std::string("SFP_VCC"), Vcc);
    inputIS_.setdouble(std::string("SFP_TEMP"), temp);
    inputIS_.setdouble(std::string("SFP_BIAS"), txBias);
    inputIS_.setdouble(std::string("SFP_TXPWR"), txPower);
    inputIS_.setdouble(std::string("SFP_RXPWR"), rxPower);

    inputIS_.setdouble(std::string("SFP_VCC_L_ALARM"), getVoltage(&data[10]));
    inputIS_.setdouble(std::string("SFP_VCC_H_ALARM"), getVoltage(&data[8]));
    inputIS_.setdouble(std::string("SFP_VCC_L_WARN"), getVoltage(&data[14]));
    inputIS_.setdouble(std::string("SFP_VCC_H_WARN"), getVoltage(&data[12]));

    inputIS_.setdouble(std::string("SFP_TEMP_L_ALARM"), getTemp(&data[2]));
    inputIS_.setdouble(std::string("SFP_TEMP_H_ALARM"), getTemp(&data[0]));
    inputIS_.setdouble(std::string("SFP_TEMP_L_WARN"), getTemp(&data[6]));
    inputIS_.setdouble(std::string("SFP_TEMP_H_WARN"), getTemp(&data[4]));

    inputIS_.setdouble(std::string("SFP_BIAS_L_ALARM"), getCurrent(&data[18]));
    inputIS_.setdouble(std::string("SFP_BIAS_H_ALARM"), getCurrent(&data[16]));
    inputIS_.setdouble(std::string("SFP_BIAS_L_WARN"), getCurrent(&data[22]));
    inputIS_.setdouble(std::string("SFP_BIAS_H_WARN"), getCurrent(&data[20]));

    inputIS_.setdouble(std::string("SFP_TXPWR_L_ALARM"), getPower(&data[26]));
    inputIS_.setdouble(std::string("SFP_TXPWR_H_ALARM"), getPower(&data[24]));
    inputIS_.setdouble(std::string("SFP_TXPWR_L_WARN"), getPower(&data[30]));
    inputIS_.setdouble(std::string("SFP_TXPWR_H_WARN"), getPower(&data[28]));

    inputIS_.setdouble(std::string("SFP_RXPWR_L_ALARM"), getPower(&data[34]));
    inputIS_.setdouble(std::string("SFP_RXPWR_H_ALARM"), getPower(&data[32]));
    inputIS_.setdouble(std::string("SFP_RXPWR_L_WARN"), getPower(&data[38]));
    inputIS_.setdouble(std::string("SFP_RXPWR_H_WARN"), getPower(&data[36]));
}

double ferol40::Ferol40::getTemp(u_char *data)
{
    return ((double)((int16_t)(256 * (uint32_t)data[0] + (uint32_t)data[1]))) / 256.0;
}

double ferol40::Ferol40::getVoltage(u_char *data)
{
    return ((double)(256 * (uint32_t)data[0] + (uint32_t)data[1])) / 10000.0;
}

double ferol40::Ferol40::getCurrent(u_char *data)
{
    return (256 * (uint32_t)data[0] + (uint32_t)data[1]) * 2 / 1000.;
}

double ferol40::Ferol40::getPower(u_char *data)
{
    return ((double)(256 * (uint32_t)data[0] + (uint32_t)data[1])) / 10.0;
}

void ferol40::Ferol40::i2cCmd(uint32_t portNo, uint32_t value) throw(utils::exception::Ferol40Exception)
{
    std::string i2cState = checkI2CState(portNo);
    //    INFO("before i2c state is " << i2cState);
    if ((i2cState != "idle") && (i2cState != "success"))
    {
        std::stringstream msg;
        msg << "Before write operation I2C bus in state " << i2cState << " for Vitesse No. " << portNo;
        ERROR(msg.str());
        XCEPT_DECLARE(utils::exception::Ferol40Exception, top, msg.str());
        throw(top);
    }
    //mdioInterface_P->mdioWrite( portNo, 0x8000, value );

    i2cState = checkI2CState(portNo);
    //    INFO("after checking i2c state is " << i2cState);

    if ((i2cState != "idle") && (i2cState != "success"))
    {
        std::stringstream msg;
        msg << "After write operation I2C bus in state " << i2cState << " for Vitesse No. " << portNo;
        INFO(msg.str());
        XCEPT_DECLARE(utils::exception::Ferol40Exception, top, msg.str());
        throw(top);
    }
}

std::string ferol40::Ferol40::checkI2CState(uint32_t portNo) throw(utils::exception::Ferol40Exception)
{
    std::string state;
    uint32_t tmp(0);
    uint32_t iloop = 0;
    do
    {

        //mdioInterface_P->mdioRead( portNo, 0x8000, &tmp );

        if ((tmp & 0x0C) == 0x00)
        {
            state = "idle";
        }
        else if ((tmp & 0x0C) == 0x04)
        {
            state = "success";
        }
        else if ((tmp & 0x0C) == 0x08)
        {
            state = "inProgress";
        }
        else
        {
            state = "failed";
        }

        iloop++;
        ::usleep(1000);

    } while ((state == "inProgress") && (iloop <= 1000));

    return state;
}

    int32_t
ferol40::Ferol40::daqOff(uint32_t link, bool generatorOn)
{
    try
    {
        // setting DAQ-OFF in the slink express sender core (Should be already off)
        uint32_t data = 0x00000000; // the second argument of the setupEventGenerator function is a reference !!
        if (generatorOn)
            data = 0x80000000; //JRF TODO must implement this variable.

        slCore_P->write("SlX_DAQ_ON_OFF", data, link);

    }
    catch (utils::exception::Ferol40Exception &e)
    {
        WARN("Communication problem during DAQ-OFF with slink no " << link << "  : " << e.what());

        return -1;
    }
    return 0;
}

    int32_t
ferol40::Ferol40::daqOn(uint32_t link)
{
    try
    {
        // setting DAQ-ON in the slink express sender core
        uint32_t data = 0x40000000; // the second argument of the slinkExpressCommand function is a reference !!
	slCore_P->write("SlX_DAQ_ON_OFF", data, link);

    }
    catch (utils::exception::Ferol40Exception &e)
    {
        WARN("Communication problem during DAQ-ON with slink no " << link << "  : " << e.what());
	return -1;
    }
    return 0;
}

    int32_t
ferol40::Ferol40::resyncSlinkExpress(uint32_t link)
{
    if (ferol40Device_P == NULL)
        return -1;

    std::string cmdstr = "";

    if ((dataSource_[link] == L10G_SOURCE) || (dataSource_[link] == L10G_CORE_GENERATOR_SOURCE))
    {
        cmdstr = "SLINKXpress_cmd_resync";
    }
    else
    {
        ERROR("Wrong dataSource. Cannot resync Slink Express with data Source : " << dataSource_[0]);
        return -1;
    }

    ferol40Device_P->writePulse(cmdstr,link*0x4000);

    return 0;
}

    HAL::HardwareDeviceInterface *
ferol40::Ferol40::getFerol40Device()
{
    return ferol40Device_P;
}

    uint32_t
ferol40::Ferol40::getSlot()
{
    return slot_;
}
