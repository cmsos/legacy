#ifndef _tcds_utils_XDAQObject_h_
#define _tcds_utils_XDAQObject_h_

namespace xdaq {
    class Application;
}

namespace tcds {
  namespace utils {

    class XDAQObject
    {

    public:
      virtual ~XDAQObject();
      xdaq::Application& getOwnerApplication() const;

    protected:
      XDAQObject(xdaq::Application& owner);

    private:
      xdaq::Application& owner_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_XDAQObject_h_
