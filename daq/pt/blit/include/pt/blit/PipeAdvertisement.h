// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_PipeAdvertisement_h_
#define _pt_blit_PipeAdvertisement_h_

#include <string> 

#include "tcpla/PublicServicePoint.h"
#include "tcpla/EndPoint.h"


namespace pt
{
	namespace blit
	{

		class PipeAdvertisement
		{

					friend class EventHandler;
					friend class InputPipe;
					friend class PeerTransport;
			public:
					int getIndex();
					std::string toString();
			protected:

				PipeAdvertisement(tcpla::PublicServicePoint * psp);
				tcpla::PublicServicePoint * getPSP();


			private:

				tcpla::PublicServicePoint * psp_;



		};

	}
}

#endif
