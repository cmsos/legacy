#ifndef _tcds_lpm_HwStatusInfoSpaceUpdater_h_
#define _tcds_lpm_HwStatusInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace lpm {

    class TCADeviceLPM;

    class HwStatusInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::lpm::TCADeviceLPM const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::lpm::TCADeviceLPM const& getHw() const;

      tcds::lpm::TCADeviceLPM const& hw_;

    };

  } // namespace lpm
} // namespace tcds

#endif // _tcds_lpm_HwStatusInfoSpaceUpdater_h_
