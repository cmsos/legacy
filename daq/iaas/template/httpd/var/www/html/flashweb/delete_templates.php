<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
</head>

<body>
<?php
include_once ('tools.php');
$_zone = $_POST ['zone'];
if (isset ( $_POST ['delete'] )) { // to run PHP script on submit
	if (! empty ( $_POST ['chk_group'] )) {
		// Loop to store and display values of individual checked checkbox.
		foreach ( $_POST ['chk_group'] as $selected ) {
			echo "Deleting template " . $_zone . '-' . strtolower ( $selected ) . '-template';
			$json = deleteTemplates ( $_POST ['elasticsearchurl'], $_zone . '-' . strtolower ( $selected ) . '-template' );
			if ($json ["acknowledged"]) {
				echo " OK <br>";
			} else {
				print_r ( $json );
			}
			
			echo "Deleting indices " . $_zone . '-' . strtolower ( $selected ) . '-*';
			$json = deleteIndices ( $_POST ['elasticsearchurl'], $_zone . '-' . strtolower ( $selected ) . '-*');
			if ($json ["acknowledged"]) {
				echo " OK <br>";
			} else {
				print_r ( $json );
			}
		}
	}
}

echo '<br>';

echo '<form action="types.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '<input type="hidden" name="zone" value="' . $_zone . '"/>';
echo '</form>'?>

</body>
</html>