package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Integer32 extends SimpleItem {
	
	private java.lang.Integer value;
	
	@Override
	public java.lang.Integer getValue() {
		return value;
	}
	
	@Override
	public void setValue(Object value) {
		this.value = ((Number)value).intValue();
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		out.writeInt(value);
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = in.readInt();
	}

	@Override
	int getEXDRCode() {
		return XDataType.INTEGER32.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
