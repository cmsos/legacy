// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include "xoap/MessageFactory.h"
#include "tstore/api/admin/AddView.h"
#include "tstore/api/NS.h"
		
tstore::api::admin::AddView::AddView
(
	const std::string & viewId, 
	const std::string & fileName,
	const std::string& database
)
	: viewId_(viewId), fileName_(fileName), database_(database)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/addView");
}

tstore::api::admin::AddView::~AddView()
{
}
		
tstore::api::admin::AddView::AddView(xoap::MessageReference& msg) 
{
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName viewName("addView", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(viewName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::admin::exception::Exception, "Invalid message: missing or bad <addView/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName viewId = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName fileName = envelope.createName("fileName", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
      	viewId_ = bodyElements[0].getAttributeValue(viewId);
	fileName_ = bodyElements[0].getAttributeValue(fileName);
}



xoap::MessageReference tstore::api::admin::AddView::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPHeader header = envelope.getHeader();
	xoap::SOAPName command = envelope.createName
		("addView",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	xoap::SOAPName viewId = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(viewId, viewId_);
	xoap::SOAPName fileName = envelope.createName("fileName", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(fileName, fileName_);
	
	// add connection parameters
	xoap::SOAPName connection = envelope.createName( "connection", "tstore", "urn:xdaq-tstore:1.0");
	xoap::SOAPElement connectionElement = element.addChildElement ( connection );

	// database name (TNS)
	xoap::SOAPName dbname = envelope.createName("dbname", "","");
	connectionElement.addAttribute(dbname, database_);

	// Credentials is currently only 'username'
#warning "TO BE REMOVED"
	xoap::SOAPName username = envelope.createName("username", "","");
	connectionElement.addAttribute(username, "cdaq_monitor");
	
	// TEMPORARY add a fake insert statement to make tstore happy
	/*xoap::SOAPName insert = envelope.createName( "insert", "sql", "urn:tstore-view-SQL");
	xoap::SOAPElement insertElement = element.addChildElement ( insert );
	
	xoap::SOAPName insertName = envelope.createName("name", "", "");
	insertElement.addAttribute(insertName, "bob");
	
	
	xoap::SOAPName table = envelope.createName( "table", "sql", "urn:tstore-view-SQL");
	xoap::SOAPElement tableElement = insertElement.addChildElement ( table );
	
	xoap::SOAPName tableName = envelope.createName("name", "", "");
	tableElement.addAttribute(tableName, "processInfo");
	*/
	// END OF TEMPORARY CODE
	
	
	return request;
}

std::string tstore::api::admin::AddView::getViewId()
{
	return viewId_;
}

std::string tstore::api::admin::AddView::getFileName()
{
	return fileName_;
}

std::string tstore::api::admin::AddView::getDatabase()
{
	return database_;
}

