// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			               	 	 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield, A. Forrest				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                    	     *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _tcpla_InputIndexGenerator_h_
#define _tcpla_InputIndexGenerator_h_

#include "toolbox/BSem.h"

namespace tcpla
{
	class InputIndexGenerator
	{
		public:

			static InputIndexGenerator* getInstance ();

			static void destroyInstance ();

			size_t getIndex ();

		private:

			InputIndexGenerator();

			toolbox::BSem mutex_;

			size_t index_;

			static InputIndexGenerator* instance_;

	};

	InputIndexGenerator* getInputIndexGenerator ();
}

#endif
