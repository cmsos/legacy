#include "bril/lumistore/hdf5/Table.h"
#include "bril/lumistore/hdf5/File.h"
#include "bril/lumistore/hdf5/Group.h"
#include "interface/bril/BCM1FTopics.hh"
#include "hdf5.h"
#include "hdf5_hl.h"
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace interface::bril;
using namespace bril::lumistore::hdf5;
const size_t NUMBER_OF_BLOCKS = 1;
const size_t ROWS_PER_BLOCK = 1024;

int main(){
  size_t totalsize = bcm1fhistT::maxsize();
  void* ref = malloc(totalsize);
  ((DatumHead*)ref)->setTime(2312,123450,1,1,1398344937,0);
  ((DatumHead*)ref)->setResource(DataSource::BCM1F,0,0,StorageType::UINT16);
  ((DatumHead*)ref)->setTotalsize(totalsize);
  ((DatumHead*)ref)->setFrequency(1);
  unsigned short bcm1fhist[MAX_NBX*4];
  for(size_t i=0; i<MAX_NBX*4; ++i) bcm1fhist[i]=i;
  memcpy(((DatumHead*)ref)->payloadanchor,bcm1fhist,sizeof(bcm1fhist));
  const std::string filename="test_Table.hd5";
  File* f = new File(filename);
  size_t datacolumnlen = bcm1fhistT::n();
  std::cout<<"datacolumnlen "<<datacolumnlen<<std::endl;
  std::cout<<"parent path "<<f->get_rootnode()->pathname()<<std::endl;
  Node* t = f->create_table(f->get_rootnode()->pathname(),bcm1fhistT::topicname(),datacolumnlen,H5T_NATIVE_USHORT,ROWS_PER_BLOCK,false);  
  std::cout<<"table pathname "<<t->pathname()<<std::endl;
  f->set_attrstr(t->pathname(),"UNIT",bcm1fhistT::unit());
  f->set_attrstr(t->pathname(),"DESC",bcm1fhistT::description());
  for(size_t i=0; i<NUMBER_OF_BLOCKS*ROWS_PER_BLOCK; ++i){
    (dynamic_cast<Table*>(t))->append( (DatumHead*)ref,((DatumHead*)ref)->payloadanchor );
  }
  t->close();
  f->close();
  delete f;
  free(ref);
}
//tableclass hdf5 2m2.332s 14635050264bytes 
//tableclass hdf5 2m18.095s 14635050360bytes  
//table,group,file class 2m3.252s 14635050360bytes  
//readback one row 0m0.142s
//readback one row 0m1.214s,0m0.143s,0m0.142s,0m0.138s
//readback one row 0m2.331s,0m0.142s,0m0.162s
