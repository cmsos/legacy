// TestXctl.cc

#include "TestXctl.h"

#include "xoap/Method.h"
#include "xdaq/NamespaceURI.h"
#include "xoap/MessageFactory.h"  // createMessage()

#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"  // XMLCh2String()

#include <unistd.h>

XDAQ_INSTANTIATOR_IMPL(TestXctl);

TestXctl::TestXctl(xdaq::ApplicationStub *stub) :
		xdaq::Application(stub), frozen_(false)
{	
	xoap::bind(this, &TestXctl::OnCommand, "Command", XDAQ_NS_URI);
	xoap::bind(this, &TestXctl::OnFreeze,  "Freeze",  XDAQ_NS_URI);

	number_ = 99;
	line_ = "foo";
	nullstr_ = "";

	getApplicationInfoSpace()->fireItemAvailable("number",  &number_);
	getApplicationInfoSpace()->fireItemAvailable("line",    &line_);
	getApplicationInfoSpace()->fireItemAvailable("nullstr", &nullstr_);

	LOG4CPLUS_INFO(getApplicationLogger(), "TestXctl");
}

xoap::MessageReference TestXctl::OnCommand(xoap::MessageReference message)
		throw (xoap::exception::Exception)
{
	Logger l = getApplicationLogger();

	if (frozen_) {
		LOG4CPLUS_INFO(l, "==== TestXctl::OnCommand frozen");
		sleep(5U);
		frozen_ = false;
	}

	DOMNodeList *elements = message->getSOAPPart().getEnvelope().getBody().getDOMNode()->getChildNodes();

	DOMNode *e;
	bool found = false;
	for (unsigned int i = 0; i < elements->getLength(); i++) {
		e = elements->item(i);

		if (e->getNodeType() != DOMNode::ELEMENT_NODE) { continue; }
		if (xoap::XMLCh2String(e->getLocalName()) == "Command") {
			found = true;
			break;
		}
	}

	if (found) {
		LOG4CPLUS_INFO(l, "==== TestXctl::OnCommand");

		elements = e->getChildNodes();

		for (unsigned long i = 0; i < elements->getLength(); i++) {
			e = elements->item(i);

			if (e->getNodeType() != DOMNode::ELEMENT_NODE) { continue; }

			if (xoap::XMLCh2String(e->getLocalName()) == "Name") {
				LOG4CPLUS_INFO(l, "==== " << "Name: " <<
						xoap::XMLCh2String(e->getFirstChild()->getNodeValue()));
			}
			if (xoap::XMLCh2String(e->getLocalName()) == "Value") {
				LOG4CPLUS_INFO(l, "==== " << "Value: " <<
						xoap::XMLCh2String(e->getFirstChild()->getNodeValue()));
			}
		}
	}

	// return empty reply
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName(
			"CommandResponse", "xdaq" , XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);

	return reply;  // Empty reply
}

xoap::MessageReference TestXctl::OnFreeze(xoap::MessageReference message)
		throw (xoap::exception::Exception)
{
	LOG4CPLUS_INFO(getApplicationLogger(), "==== TestXctl::Freeze");
	
	frozen_ = true;

	// return empty reply
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPName responseName = envelope.createName(
			"CommandResponse", "xdaq" , XDAQ_NS_URI);
	envelope.getBody().addBodyElement(responseName);

	return reply;  // Empty reply
}

xoap::MessageReference TestXctl::ParameterQuery(xoap::MessageReference message)
		throw (xoap::exception::Exception)
{
	if (frozen_) {
		LOG4CPLUS_INFO(
				getApplicationLogger(), "==== TestXctl::ParameterQuery frozen");
		sleep(5U);
		frozen_ = false;
	}

	return Application::ParameterQuery(message);
}

xoap::MessageReference TestXctl::ParameterGet(xoap::MessageReference message)
		throw (xoap::exception::Exception)
{
	if (frozen_) {
		LOG4CPLUS_INFO(
				getApplicationLogger(), "==== TestXctl::ParameterGet frozen");
		sleep(5U);
		frozen_ = false;
	}

	return Application::ParameterGet(message);
}

xoap::MessageReference TestXctl::ParameterSet(xoap::MessageReference message)
		throw (xoap::exception::Exception)
{
	if (frozen_) {
		LOG4CPLUS_INFO(
				getApplicationLogger(), "==== TestXctl::ParameterSet frozen");
		sleep(5U);
		frozen_ = false;
	}

	return Application::ParameterSet(message);
}

// End of file
// vim: set ai sw=4 ts=4:
