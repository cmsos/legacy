#include "ttc/utils/YuiSlider.hh"

#include "ttc/utils/JavaScriptUtils.hh"

#include <sstream>
#include <iostream>


using namespace std;


// class ttc::YuiSlider - static methods

string ttc::YuiSlider::getStyleSheet()
{
  ostringstream buf;

  const int slider_height = 20;

  const string slider_bg_color = "#AAAAAA";
  const string slider_handle_color = "#00ff00";

  buf << "<style type=\"text/css\">" << endl
      << "    .slider {" << endl
      << "            background: " << slider_bg_color << ";" << endl
      << "            height: " << slider_height << "px;" << endl
      << "            width: 74px;" << endl
      << "    }" << endl
      << endl << "    .left_fill {"
      << endl << "            height: 10px;" << endl
      << "            background: " << slider_bg_color << ";" << endl
      << "            width: 0px;" << endl
      << "            float: left;" << endl
      << "    }" << endl
      << endl
      << "    .handle {" << endl
      << "            background: " << slider_handle_color << ";" << endl
      << "            width: 10px;" << endl
      << "            height: " << slider_height << "px;" << endl
      << "            float: left;" << endl
      << "            z-index: 100;" << endl
      << "    }" << endl
      << "</style>" << endl;

  return buf.str();
}


string ttc::YuiSlider::getYuiIncludes()
{
  vector<string> vars;
  vars.push_back("yahoo/yahoo");
  vars.push_back("yahoo-dom-event/yahoo-dom-event");
  vars.push_back("dragdrop/dragdrop");
  vars.push_back("slider/slider");
  vars.push_back("connection/connection");
  return YuiWidgetWithPostRequest::getYuiIncludes(vars);
}


string ttc::YuiSlider::getAllSlidersJavaScriptCode(const vector<YuiSlider*>& sliders)
{
  if (sliders.size() == 0)
    return "";

  ostringstream buf;
  buf << getCommonJavaScriptCode() << "<script type=\"text/javascript\">" << endl;

  // Print the variable declarations for the sliders.
  for (size_t i = 0; i < sliders.size(); ++i)
  {
    // Put a 'var' keyword in front of each variable
    // (seems to help to make them global).
    buf << "var " << sliders[i]->getSliderVariableName() << ";" << endl;
  }

  const string init_function_name = "init_sliders";

  // Print the initialization function.
  buf << "function " << init_function_name << "() {" << endl;

  // loop over all sliders
  for (size_t i = 0; i < sliders.size(); ++i)
  {
    buf << "  " << sliders[i]->getSliderVariableName() << " = new MakeSlider(\"" << sliders[i]->name << "\","
        << sliders[i]->initial_value << "," << sliders[i]->min_value << "," << sliders[i]->max_value << ");"
        << endl;

    // Register the callback generating the http request if the slider value is changed.
    if (sliders[i]->value_changed_url != "")
    {
      buf << "  setSliderSubmitCommand(" << sliders[i]->getSliderVariableName() << ",";

      // Generate the function which must return the new value.
      {
        ostringstream buf2;
        // Create a 'lambda' expression function for returning the
        // value when the slider has changed.
        buf2 << "function() { return " << sliders[i]->getSliderVariableName() << ".getValue(); }";

        buf << sliders[i]->makeMakeSubmitCommandInvocationCode(buf2.str());
      }

      // End of 'setSliderSubmitCommand'.
      buf << ");" << endl;
    }
  }

  buf
      << "}" << endl
      << endl
      << "Event.on(window, \"load\", " << init_function_name << ");" << endl
      << "</script>" << endl;

  return buf.str();
}


string ttc::YuiSlider::getCommonJavaScriptCode()
{
  ostringstream buf;

  buf
      << "<script type=\"text/javascript\">" << endl
      << "//----------------------------------------" << endl
      << JavaScriptUtils::makeSubmitCommand()
      << "//----------------------------------------" << endl
      << JavaScriptUtils::MakeSlider()
      << "//----------------------------------------" << endl
      << JavaScriptUtils::setSliderSubmitCommand()
      << "//----------------------------------------" << endl
      << "</script>" << endl;

  return buf.str();
}


// class ttc::YuiSlider - members

ttc::YuiSlider::YuiSlider(
    const string& _name,
    int _initial_value,
    int _min_value,
    int _max_value)
:
    YuiWidgetWithPostRequest(_name),
    initial_value(_initial_value),
    min_value(_min_value),
    max_value(_max_value)
{}


string ttc::YuiSlider::getSliderHtmlCode()
{
  ostringstream buf;
  buf
      << "<div tabindex=\"0\" id=\"slider_" << name << "\" class=\"slider\" style=\"float:left;\">" << endl
      << "    <div id=\"left_fill_" << name << "\" class=\"left_fill\">" << endl
      << "        <div id=\"handle_" << name << "\" class=\"handle\"></div>" << endl
      << "    </div>" << endl
      << "</div>" << endl;

  return buf.str();
}


string ttc::YuiSlider::getHtmlCodeForTextControl()
{
  ostringstream buf;

  buf
      << "<form "
          "onsubmit=\"return " << getSliderVariableName() << ".updateSliderValue();\">" << endl
      << " &nbsp;  "
          "<input "
          "autocomplete=\"off\" "
          "name=\"sliderTextField_" << name << "\" "
          "id=\"sliderTextField_" << name << "\" "
          "type=\"text\" "
          "value=\"0\" "
          "size=\"4\" "
          "maxlength=\"4\" "
          "/>" << endl
      << "</form>" << endl;

  return buf.str();
}


string ttc::YuiSlider::getSliderIncrementButtonCreationCode(
    const string& button_suffix,
    const string& button_label,
    int increment)
{
  string slider_variable_name = getSliderVariableName();
  string button_name = slider_variable_name + "_" + "button" + button_suffix;

  ostringstream buf;
  buf
      << "<span "
          "id=\"" << button_name << "\" "
          "class=\"yui-button yui-push-button\">" << endl
      << "    <span class=\"first-child\">" << endl
      << "        <button type=\"button\">&nbsp;</button>" << endl
      << "    </span>" << endl
      << "</span>" << endl
      << endl
      << "<script type=\"text/javascript\">" << endl
      << "YAHOO.util.Event.on(window, \"load\", function()" << endl
      << "{" << endl
      << "  makeSliderIncrementButton("
      << slider_variable_name << ","
      << "\"" << button_name << "\","
      << "\"" << button_label << "\","
      << increment << ")" << endl
      << "});  " << endl
      << "</script>" << endl;

  return buf.str();
}


string ttc::YuiSlider::getSliderVariableName()
{
  return "slider_var_" + name;
}
