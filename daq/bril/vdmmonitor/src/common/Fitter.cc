#include "bril/vdmmonitor/Fitter.h"
#include <iostream>
//#include <stdio.h>      /* printf, scanf, puts, NULL */

#include <stdlib.h>     /* abs */

//#include <time.h>       /* time */
//#include <cstdlib>

#include "TROOT.h"
#include "TGraphErrors.h"
#include "TGraph.h"
#include "TF1.h"
#include "TH1F.h"
#include "TFitResult.h"

#include "TCanvas.h"
#include "TImage.h"

#include "TStyle.h"

#include <vector>
#include <numeric>

//using namespace std;

void bril::vdmmonitor::fit_vector(const std::vector<float>&  sep, const std::vector< std::vector <float> >&  All_shapes_X, const std::vector< std::vector <float> >&  All_shapes_X_err, const std::vector< std::vector <float> >& b1_currents, const std::vector< std::vector <float> >& b2_currents, std::vector<float>& bx_b1_b2_product, std::vector<float>&  bxwidth, std::vector<float>&  bxwidth_err, std::vector<float>& bxpeak, std::vector<float>& bxpeak_err, std::vector<float>& bxmean, std::vector<float>& bxmean_err, float& avgwidth, float& avgwidth_err, float& avgpeak, float& avgpeak_err, float& avgmean, float& avgmean_err)
{
    
  if (sep.empty()) return;

    TH1F * h_All_shapes_X[3564];
    TH1F * h_All_shapes_err_X[3564];
    TH1F * h_b1_currents[3564];
    TH1F * h_b2_currents[3564];
    TH1F * h_b1_b2_product[3564];

	TGraphErrors * gr_norm_lumi[3564]; 

    int histos_counter = 0;
	int histos_counter0 = 0;
	int histos_counter1 = 0;
	int histos_counter2 = 0;
	int collidig_bx = 0;

	float norm_lumi_val = 0;
	float norm_lumi_err_val = 0;
    std::vector<float>  normalized_lumi;
    std::vector<float>  normalized_lumi_err;

	std::vector<float> sep_err(sep.size(), 0.0);

	float min_sep = sep.front(); // negative
	float max_sep = sep.back();
	float scan_range = std::abs(min_sep) + std::abs(max_sep);
	float bin_widht = scan_range/sep.size();
	float min_sep_bin_mid = sep.front() - bin_widht/2; // negative
	float max_sep_bin_mid = sep.back() + bin_widht/2;

	//TF1 * gaus;
	Double_t par[3];

	//std::cout << " ***** sep.size() =  " << sep.size() << " min_sep  =  " << min_sep << " max_sep  =  " << max_sep << " scan_range  =  " << scan_range << " bin_widht " << bin_widht <<  " min_sep_bin_mid  =  " << min_sep_bin_mid << " max_sep_bin_mid  =  " << max_sep_bin_mid  <<'\n';

	for (int bx = 0; bx <3564 ; ++bx) {
	  h_All_shapes_X[bx] = new TH1F (Form("h_All_shapes_X_%i",bx),"", sep.size(), min_sep_bin_mid,max_sep_bin_mid); 
	  h_All_shapes_err_X[bx] = new TH1F (Form("h_All_shapes_err_X_%i",bx),"", sep.size(), min_sep_bin_mid, max_sep_bin_mid); 
	  h_b1_currents[bx] = new TH1F (Form("h_b1_currents_%i",bx),"", sep.size(), min_sep_bin_mid, max_sep_bin_mid); 
	  h_b2_currents[bx] = new TH1F (Form("h_b2_currents_%i",bx),"", sep.size(), min_sep_bin_mid, max_sep_bin_mid);
	  h_b1_b2_product[bx] = new TH1F (Form("h_b1_b2_product_%i",bx),"", sep.size(), min_sep_bin_mid, max_sep_bin_mid);
	}


	//fill histograms with X or Y shape vectors values
	for(std::vector<std::vector<float> >::const_iterator it =  All_shapes_X.begin(); it !=  All_shapes_X.end(); ++it) { //loop through BCIDs
		int vec_elements_counter = 1;
		for(std::vector<float>::const_iterator jt = it->begin(); jt != it->end(); ++jt) { //loop through the vector elements and fill shape histograms
	        	float bin_content = *jt;
			h_All_shapes_X[histos_counter]-> SetBinContent(vec_elements_counter, bin_content);
			//h_All_shapes_err_X[histos_counter]-> SetBinContent(vec_elements_counter, ((bin_content)*0.05)); // 5% of rate for tests
			++vec_elements_counter;
		  
		}

		++histos_counter;	
	}	

	// std::cout << " ***** Passes filling of the shape histograms from the vectors **** "<<'\n';


	for(std::vector<std::vector<float> >::const_iterator it =  All_shapes_X_err.begin(); it !=  All_shapes_X_err.end(); ++it) { //loop through BCIDs
		int vec_elements_counter = 1;
		for(std::vector<float>::const_iterator jt = it->begin(); jt != it->end(); ++jt) { //loop through the vector elements and fill shape histograms
	        	float bin_content = *jt;
			h_All_shapes_err_X[histos_counter0]-> SetBinContent(vec_elements_counter, bin_content);
			++vec_elements_counter;
		  
		}

		++histos_counter0;	
	}

	//fill histograms with B1 currents vectors values
	for(std::vector<std::vector<float> >::const_iterator it =  b1_currents.begin(); it !=  b1_currents.end(); ++it) { //loop through BCIDs
		int vec_elements_counter = 1;
		for(std::vector<float>::const_iterator jt = it->begin(); jt != it->end(); ++jt) { //loop through the vector elements and fill BC1 histograms
			float bin_content = *jt;
			h_b1_currents[histos_counter1]-> SetBinContent(vec_elements_counter, bin_content);
			h_b1_b2_product[histos_counter1]-> SetBinContent(vec_elements_counter, bin_content);
			++vec_elements_counter;
		  
		}

		++histos_counter1;	
	}	

	//fill histograms with B2 currents vectors values
	for(std::vector<std::vector<float> >::const_iterator it =  b2_currents.begin(); it !=  b2_currents.end(); ++it) { //loop through BCIDs
		int vec_elements_counter = 1;
		for(std::vector<float>::const_iterator jt = it->begin(); jt != it->end(); ++jt) { //loop through the vector elements and fill BC2 histograms
		  	float bin_content = *jt;
			h_b2_currents[histos_counter2]-> SetBinContent(vec_elements_counter, bin_content);
			++vec_elements_counter;
		  
		}
		++histos_counter2;	
	}	

	//std::cout << " ***** Filled histograms and just ready for fitting **** "<<'\n';
	//std::cout << " ***** Bin content before fit loop is **** "<<'\n';
	//std::cout << "  h_All_shapes_X[200] bin 4  = " << h_All_shapes_X[200] -> GetBinContent(4)<< " h_b1_currents[200] bin 4 = "<< h_b1_currents[200] -> GetBinContent(4) <<  " h_b2_currents[200] bin 4 = " << h_b2_currents[200] -> GetBinContent(4) <<  '\n';



        //normalize shape X(Y) histos by beam currents and fit histograms
	for (int bx = 0; bx <3564 ; ++bx) {	     
	       //gaus = new TF1("gaus","gaus", min_sep_bin_mid, max_sep_bin_mid);
	       h_All_shapes_X[bx]->Divide(h_b1_currents[bx]);
	       h_All_shapes_X[bx]->Divide(h_b2_currents[bx]);
	       h_All_shapes_X[bx]->Scale(1e22);

	       h_All_shapes_err_X[bx]->Divide(h_b1_currents[bx]);
	       h_All_shapes_err_X[bx]->Divide(h_b2_currents[bx]);
	       h_All_shapes_err_X[bx]->Scale(1e22);

	       h_b1_b2_product[bx]->Multiply(h_b2_currents[bx]); // was just filled with b1, now *b2
	       h_b1_b2_product[bx]->Scale(1.0/1e22); // to bring it to values of order 1.2-1.3


	       for (int i=0; i<int(sep.size()); ++i) {
	          norm_lumi_val = h_All_shapes_X[bx] -> GetBinContent(i+1);
		      norm_lumi_err_val = h_All_shapes_err_X[bx] -> GetBinContent(i+1);
			 
		      normalized_lumi.push_back(norm_lumi_val);
		      normalized_lumi_err.push_back(norm_lumi_err_val);
	       }

	       gr_norm_lumi[bx] =  new TGraphErrors(int(sep.size()), &sep[0], &normalized_lumi[0], &sep_err[0], &normalized_lumi_err[0]); 
		       
	       normalized_lumi.clear();
	       normalized_lumi_err.clear();
	     
	       /*
	       if(bx==216){
	       		std::cout << " ***** Bin content inside fit loop, after Divide and Scale(10^(22) is **** "<<'\n';
				std::cout << "  ** h_All_shapes_X[0] bin 2  = " << h_All_shapes_X[bx] -> GetBinContent(2) << "  h_All_shapes_err_X[bx] bin 2  = " << h_All_shapes_err_X[bx] -> GetBinContent(2) << " h_b1_currents[bx] bin 6 = "<< h_b1_currents[bx] -> GetBinContent(2) <<  " h_b2_currents[bx] bin 6 = " << h_b2_currents[bx] -> GetBinContent(2) <<  '\n';
				std::cout <<  "  ** h_All_shapes_X[bx] bin 4  = " << h_All_shapes_X[bx] -> GetBinContent(4) <<  "  h_All_shapes_X_err[bx] bin 4  = " << h_All_shapes_err_X[bx] -> GetBinContent(4)<< " h_b1_currents[bx] bin 4 = "<< h_b1_currents[bx] -> GetBinContent(4) <<  " h_b2_currents[bx] bin 4 = " << h_b2_currents[bx] -> GetBinContent(4) <<  '\n';
				std::cout << "  ** h_All_shapes_X[bx] bin 6  = " << h_All_shapes_X[bx] -> GetBinContent(6) << "  h_All_shapes_err_X[bx] bin 6  = " << h_All_shapes_err_X[bx] -> GetBinContent(6) << " h_b1_currents[bx] bin 6 = "<< h_b1_currents[bx] -> GetBinContent(6) <<  " h_b2_currents[bx] bin 6 = " << h_b2_currents[bx] -> GetBinContent(6) << " Mean b1 "<<  h_b1_currents[bx] -> GetMean() << " Mean b2 "<<  h_b2_currents[bx] -> GetMean() <<   '\n';
	       		std::cout <<" h_b1_b2 bin 6 = "<< h_b1_b2_product[bx] -> GetBinContent(6) << " Mean of b1*b2/1e22 = " << h_b1_b2_product[bx]->GetMean() <<'\n';
	       		// Bin content is reasonable, e.g.: h_b1_currents[bx] bin 4 = 1.05295e+11 h_b2_currents[bx] bin 4 = 1.07548e+11, but Mean is not reasonable: b1 -1.20489e-05 Mean b2 -6.75561e-06
	       }
	       */

	       if ( (h_b1_currents[bx]->GetSumOfWeights()!=0) && (h_b2_currents[bx]->GetSumOfWeights()!=0) ) { //check if histograin is not empty
	       		++collidig_bx;
	       		//TF1* gaus=new TF1("gaus","gaus", min_sep_bin_mid, max_sep_bin_mid);
	       		TF1* gaus = new TF1("gaus","gaus", min_sep_bin_mid, max_sep_bin_mid);
	      		//h_All_shapes_X[bx]->Fit(gaus,"RQ","",min_sep_bin_mid, max_sep_bin_mid);
	      		gr_norm_lumi[bx]->Fit(gaus,"RQ","",min_sep_bin_mid, max_sep_bin_mid);
		 		gaus->GetParameters(par);
		 /*
		 if(bx==0){
		 std::cout << "!!! h_b1_currents[bx]->GetSumOfWeights() = " << h_b1_currents[bx]->GetSumOfWeights() << "  h_b2_currents[bx]->GetSumOfWeights() = " << h_b2_currents[bx]->GetSumOfWeights() <<  " HistMean: " << float (h_All_shapes_X[bx]->GetMean(1)) << " HistRMS: " << float (h_All_shapes_X[bx]->GetRMS(1)) << " GaussAmp "<< par[0] << " GausAmpErr "<< gaus->GetParError(0)<< " GaussMean: " << par[1] <<" GaussMeanErr " << gaus->GetParError(1) <<" GaussWidth "<< par[2]<< " WidthErr "<< gaus->GetParError(2)<< '\n';
		 }
		 */

		 	 	bxwidth.push_back(float(par[2]));
		 	 	bxwidth_err.push_back(float(gaus->GetParError(2)));
		 		bxmean.push_back(float(par[1]));
		 		bxmean_err.push_back(float(gaus->GetParError(1)));
		 		bxpeak.push_back(float(par[0]));
		 		bxpeak_err.push_back(float(gaus->GetParError(0)));
	     		bx_b1_b2_product.push_back(float(h_b1_b2_product[bx]-> GetBinContent(4))); // bin 4 is ~Peak, not using mean, as mean gives wired value (-1.88075e-05). 

		 		delete gaus;
		 		gaus = 0;
	     	}
		 	else {
				bxwidth.push_back(0);
		 		bxwidth_err.push_back(0);
		 		bxmean.push_back(0);
		 		bxmean_err.push_back(0);
		 		bxpeak.push_back(0);
		 		bxpeak_err.push_back(0);
		 		bx_b1_b2_product.push_back(0);
		 	}
				   
	} // end bx loop

	// average fit outputs from all BCIDs
	avgwidth = accumulate( bxwidth.begin(), bxwidth.end(), 0.0);
	avgwidth_err = accumulate( bxwidth_err.begin(), bxwidth_err.end(), 0.0);
	avgpeak = accumulate( bxpeak.begin(), bxpeak.end(), 0.0);
	avgpeak_err = accumulate( bxpeak_err.begin(), bxpeak_err.end(), 0.0);
	avgmean = accumulate( bxmean.begin(), bxmean.end(), 0.0);
	avgmean_err = accumulate( bxmean_err.begin(), bxmean_err.end(), 0.0);

	avgwidth = avgwidth/collidig_bx;
	avgwidth_err = avgwidth_err/collidig_bx;
	avgpeak = avgpeak/collidig_bx;
	avgpeak_err = avgpeak_err/collidig_bx;
	avgmean = avgmean/collidig_bx;
	avgmean_err = avgmean_err/collidig_bx;

	//std::cout << " *** Found n_coll_bx = " <<  collidig_bx << " avgwidth: " << avgwidth << " avgwidth_err: " << avgwidth_err<< " avgpeak: " << avgpeak<< " avgpeak_err: " << avgpeak_err << " avgmean: " << avgmean<< " avgmean_err: " << avgmean_err << " bxwidth.size() in fitter"<< bxwidth.size()<<'\n';

	/*
      	TCanvas *c1 = new TCanvas("test","Gr", 400, 600);

	c1 -> Divide(2,2);
	c1 -> cd (1);
	h_All_shapes_X[0]->Draw();
	c1 -> cd (2);
	h_b1_currents[0]->Draw();
	c1 -> cd (3);
	h_b2_currents[0]->Draw();
	c1 -> cd (4);

	gr_norm_lumi[0]->Draw("AP");
	c1->SaveAs("/nfshome0/okarache/testGr_prog_LHCSource.root");
	//c1->SaveAs("/nfshome0/okarache/testGr_prog_fakeSource.root");
	//c1->SaveAs("/nfshome0/okarache/testGr_prog.root");
       
	*/

	for (int bx = 0; bx <3564 ; ++bx) {
	  delete h_All_shapes_X[bx]; 
	  delete h_All_shapes_err_X[bx]; 
	  delete h_b1_currents[bx]; 
	  delete h_b2_currents[bx]; 
	  delete h_b1_b2_product[bx];
	  delete gr_norm_lumi[bx]; 
	}
}
