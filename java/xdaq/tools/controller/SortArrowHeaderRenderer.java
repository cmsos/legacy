package xdaq.tools.controller;

 // This renderer extends a component. It is used each time a
 // cell must be displayed.
import java.awt.*;
import javax.swing.table.*;
import javax.swing.*;
import java.net.*;



public class SortArrowHeaderRenderer implements TableCellRenderer
{
	// The renderer to delegate to
	private TableCellRenderer delegateRenderer;
	
	// Whether we can inject a icon into the delegate renderer or not
	private boolean iconInjection = false;
	
	// The icons to use for indicating the sort order
	private  Icon ascendingIcon = null;
	private  Icon descendingIcon = null;
	
	/* Creates a new SortArrowHeaderRenderer that delegates
	 * most drawing to the tables current header renderer.
	 */
	 public SortArrowHeaderRenderer(JTable table)
	 {
	 	// find the delegate
		this.delegateRenderer = table.getTableHeader().getDefaultRenderer();
		
		// determine if we can inject icons into the delegate
		iconInjection = (delegateRenderer instanceof DefaultTableCellRenderer);
		
		// Load the icons
		
		//URL  url = getClass().getResource("icons/up_arrow.png");
		//System.out.println(url.toString());
	 	ascendingIcon = new ImageIcon(getClass().getResource("icons/up_arrow.png"));
		descendingIcon = new ImageIcon(getClass().getResource("icons/down_arrow.png"));
		//
		
	 }
	 

	 /* Renders the header in the default way
	  * but with the addition of an icon 
	  */
	  public java.awt.Component getTableCellRendererComponent(JTable table, Object value,
	  	boolean isSelected, boolean hasFocus, int row, int column)
		{
			if (iconInjection)
			{
				DefaultTableCellRenderer jLabelRenderer = (DefaultTableCellRenderer) delegateRenderer;
				Icon iconToUse = ascendingIcon;
				GTableModel model = (GTableModel)table.getModel();
				if ( model.getAcendingOrder(column).intValue() == 1 )
					iconToUse = descendingIcon;
				
				jLabelRenderer.setIcon(iconToUse);
				jLabelRenderer.setHorizontalTextPosition(jLabelRenderer.LEADING);
				
				return delegateRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			} else
			{
				return delegateRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			}
		}

}
