#ifndef ReadoutCore_h_included
#define ReadoutCore_h_included

#include <vector>
#include <string>
#include <deque>
#include "bril/webutils/WebUtils.h"
#include "log4cplus/logger.h"
#include "toolbox/squeue.h"
#include "toolbox/Task.h"


namespace hcal {
  namespace uhtr {
    class uHTR;
  }
}

namespace uhal {
  class ConnectionManager;
}

namespace amc13 {
  class AMC13;
}

namespace bril
 {

  using webutils::Range;
  using webutils::make_range;



  namespace bhmtriggeredsource
   {

     class ReadoutCore
      {

     public:
       enum State { st_Uninit, st_Ready, st_Active, st_Paused, st_Fail };
       State getState() const { return m_state; }
       ReadoutCore(log4cplus::Logger l);
       ~ReadoutCore();

       void setup(const std::string& connectionsFile,
		  const std::string& slotList,
		  int pipeline, int samples, int zs_threshold);
       // set itdc=0 for "any" and ncoinc=31 for "disabled"
       void setupCoincLevel(unsigned int itdc,unsigned int ncoinc) { if (itdc<5) m_selfCoincs[itdc]=ncoinc; }

       void configure();
       void start();
       void stop();
       void pause();
       void resume();
       void reset();

       int readoutTask();
       int events() const { return m_num_events; }

       struct Array{
         int array[12];
       };

       class channeldata
        {
         public:
         typedef const uint8_t* const_iterator;
         typedef uint8_t size_type;
         typedef uint32_t channel_id_type;
         static const size_type s_nsamples = 16;

         public:
         channeldata( uint32_t evn, uint16_t orn, uint16_t bcn, uint16_t board, const uint64_t* );

         Range<const_iterator> adc() const
         {
           return make_range( m_adc );
         }

         Range<const_iterator> tdc() const
         {
           return make_range( m_tdc );
         }

         size_type size() const
         {
           return m_samples;
         }

         channel_id_type channel() const
         {
           return m_id;
         }

         private:
         uint32_t m_evn;
         uint16_t m_orn;
         uint16_t m_bcn;
         uint32_t m_id;
         uint8_t m_adc[s_nsamples];
         uint8_t m_tdc[s_nsamples];
         uint8_t m_samples;

       };

      public:
      typedef std::deque<channeldata> event_list;

      event_list event_data() const
       {
        return m_events;
       }

     private:
       log4cplus::Logger m_logger;
       State m_state;

       enum ThreadCommand { cmd_Stop, cmd_Pause, cmd_Resume };
       ::toolbox::squeue<ThreadCommand> m_cmdQueue;
       ::toolbox::Task* m_task;
       
       uhal::ConnectionManager* m_connectionManager;
       amc13::AMC13* m_amc13;
       std::vector<hcal::uhtr::uHTR*> m_uHTRs;
       std::string m_connectionsFile, m_slotList;
       int m_pipeline, m_samples, m_zsThreshold;
       int m_bcOffsetDAQ;
       std::vector<unsigned int> m_selfCoincs;

       int m_num_events;

       std::deque<channeldata> m_events;

     };
  }
}

#endif //  ReadoutCore_h_included
