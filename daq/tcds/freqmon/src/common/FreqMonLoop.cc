#include "tcds/freqmon/FreqMonLoop.h"

#include <cmath>
#include <memory>
#include <sstream>
#include <stdint.h>
#include <string>
#include <unistd.h>

#include "toolbox/BSem.h"
#include "toolbox/string.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/exception/Exception.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/freqmon/ConfigurationInfoSpaceHandler.h"
#include "tcds/freqmon/FreqMon.h"
#include "tcds/utils/LockGuard.h"

tcds::freqmon::FreqMonLoop::FreqMonLoop(tcds::freqmon::FreqMon& controller) :
  alarmName_("FreqMonAlarm"),
  controller_(controller),
  workLoopP_(0),
  workLoopName_(""),
  allOk_(false),
  freqCounterReaderP_(0),
  lock_(toolbox::BSem::FULL),
  freqVal_(0)
{
  std::stringstream workLoopName;
  uint32_t const instanceNumber = controller_.getApplicationDescriptor()->getInstance();
  workLoopName << "FreqMonWorkLoop_" << instanceNumber;
  workLoopName_ = workLoopName.str();
  workLoopP_ = std::auto_ptr<toolbox::task::WorkLoop>(toolbox::task::getWorkLoopFactory()->getWorkLoop(workLoopName_, "waiting"));
  toolbox::task::ActionSignature* const updateAction =
    toolbox::task::bind(this, &tcds::freqmon::FreqMonLoop::update, "updateAction");
  workLoopP_->submit(updateAction);
}

tcds::freqmon::FreqMonLoop::~FreqMonLoop()
{
  stop();
}

void
tcds::freqmon::FreqMonLoop::start()
{
  if (workLoopP_.get())
    {
      try
        {
          workLoopP_->activate();
        }
      catch (toolbox::task::exception::Exception const& err)
        {
          // Raise the alarm.
          std::string const problemDesc =
            toolbox::toString("Failed to start FreqMon workloop: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::FreqMonFailureAlarm, alarmException, problemDesc);
          controller_.raiseAlarm(alarmName_, "error", alarmException);
        }
    }
}

void
tcds::freqmon::FreqMonLoop::stop()
{
  if (workLoopP_.get())
    {
      if (workLoopP_->isActive())
        {
          workLoopP_->cancel();
        }
      workLoopP_.reset();
    }
}

bool
tcds::freqmon::FreqMonLoop::isAllOk() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  return allOk_;
}

bool
tcds::freqmon::FreqMonLoop::update(toolbox::task::WorkLoop* workLoop)
{
  std::string const hostName =
    controller_.getConfigurationInfoSpaceHandler().getString("hostName");
  uint32_t const portNumber =
    controller_.getConfigurationInfoSpaceHandler().getUInt32("portNumber");

  if (freqCounterReaderP_.get() == 0)
    {
      try
        {
          freqCounterReaderP_ =
            std::auto_ptr<tcds::freqmon::FreqCounterReader>(new tcds::freqmon::FreqCounterReader(controller_, hostName, portNumber));

          // If we get here, let's revoke any existing alarm...
          controller_.revokeAlarm(alarmName_);
        }
      catch (tcds::exception::Exception const& err)
        {
          allOk_ = false;
          freqCounterReaderP_.reset();
          // Raise the alarm.
          std::string const problemDesc =
            toolbox::toString("The network connection to the frequency counter failed: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::FreqMonFailureAlarm, alarmException, problemDesc);
          controller_.raiseAlarm(alarmName_, "error", alarmException);
        }
    }

  // Read the (hopefully) new value.
  if (freqCounterReaderP_.get())
    {
      try
        {
          uint32_t const tmp = freqCounterReaderP_->readValue();
          {
            // Kinda clumsy locking, but okay.
            tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
            freqVal_ = tmp;
          }
          allOk_ = true;

          // If we get here, let's revoke any existing alarm...
          controller_.revokeAlarm(alarmName_);
        }
      catch (tcds::exception::Exception const& err)
        {
          allOk_ = false;
          freqCounterReaderP_.reset();
          // Raise the alarm.
          std::string const problemDesc =
            toolbox::toString("At least one update poll of the frequency counter failed: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::FreqMonFailureAlarm, alarmException, problemDesc);
          controller_.raiseAlarm(alarmName_, "error", alarmException);
        }
    }

  //----------

  // Wait a little in order not to completely hog the CPU.
  ::usleep(kLoopRelaxTime);

  // Return true in order to schedule the next iteration.
  return true;
}

double
tcds::freqmon::FreqMonLoop::snapshot() const
{
  // NOTE: Even though the current frequency counter only produces
  // integer measurements with 1 Hz accuracy, the result _has_ to be a
  // double since a float cannot store the required precision.

  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  return double(freqVal_);
}
