// $Id: Server.h,v 1.2 2008/07/18 15:26:40 gutleber Exp $

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Server_h_
#define _Server_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include <set>
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/PerformanceMeter.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"
#include "i2o/Method.h"
#include "toolbox/task/Timer.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"


class Server: public xdaq::Application, public xgi::framework::UIManager, xdata::ActionListener
{	
	public:
		XDAQ_INSTANTIATOR();

		Server(xdaq::ApplicationStub* c) throw(xdaq::exception::Exception);

		// Interface function invoked for the I2O token message
		// that is received from the Client(s)
		//
		void token(  toolbox::mem::Reference * ref)  throw (i2o::exception::Exception);

		void actionPerformed (xdata::Event& e);


	protected:
		size_t counter_;

	
};

#endif
