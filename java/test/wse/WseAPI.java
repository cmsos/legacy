package test.wse;

import wsmg.WseClientAPI;
import wsmg.NotificationHandler;

import java.lang.*;
import java.util.HashMap;
import java.net.InetAddress;

public class WseAPI
{
	/** WS eventing api */
	private WseClientAPI client;
	/** List of RenewThreads */
	private HashMap map;

	public WseAPI()
	{
		client = new WseClientAPI();
		map = new HashMap();
	}

	public void finalize() throws Throwable
	{
		try
		{
			cleanup();
		}
		finally
		{
			super.finalize();
		}
	}

	/**
	 * cleanup resources 
	 */
	public void cleanup()
	{
		client.shutdownConsumerService();
	}

	/**
	 * start a listener on the specified port and call the handler function for incoming messages
	 * 	@param	port	listening port of the handler
	 *	@param	handler	class implementing function for incoming notifications
	 *	@param	returns	the fullly qualified URN of the listener 
	 *	@throws WseException in case the port is already used
	 */
	public String listen(int port, NotificationHandler handler) throws WseException
	{
		try
		{
			client.startConsumerService(port, handler);
		}
		catch(Exception e)
		{
			throw new WseException("Listener could not be started", e);
		}

		try
		{
			InetAddress localMachine = java.net.InetAddress.getLocalHost();	
			return "http://"+localMachine.getHostAddress()+":"+port;
		}
		catch(java.net.UnknownHostException e)
		{
			throw new WseException("Local hostname could not be resolved", e);
		}
	}

	/**
	 * subscribe to a ws eventing service
	 * 	@param	eventingurn	fully qualified urn of the ws eventing service (e.g. http://lxcmd101:1980/urn:xdaq-application:lid=400)
	 *	@param	consumerurn fully qualified urn of the local listener, this is the urn returned by the listen function
	 *	@param	xpath	XPath filter expression sent to the ws eventing service to only receive matching messages
	 *	@param	expires	expiration time in seconds sent to the ws eventing service. If 0 or less subscription will never expire
	 *	@param	renew time in seconds after which the subscription should be renewed. If bigger than 0 automatic Renew messages are sent for this subscription
	 *	@return the unique subscription identifier needed to identify the subscription for further requests (renew, unsubscribe) 
	 *	@throws WseException in case renewal failed due to connection errors
	 */
	public String subscribe(String eventingurn, String consumerurn, String xpath, int expires, int renew) throws WseException
	{
		String subscriptionid;

		try
		{
			if(expires<=0)
			{
				subscriptionid = client.subscribe(eventingurn, consumerurn, null, xpath, null, null);
			}
			else
			{
				subscriptionid = client.subscribe(eventingurn, consumerurn, null, xpath, null, null, "PT"+expires+"S");
				if(renew>0)
				{
					RenewThread rt = new RenewThread(eventingurn, consumerurn, subscriptionid, expires, renew);
					rt.start();
					map.put(subscriptionid, rt);
				}
			}
		}
		catch(Exception e)
		{
			throw new WseException("Subscription failed", e);
		}

		return subscriptionid;
	}
	
	/**
	 * renew a subscription
	 * 	@param	eventingurn	fully qualified urn of the ws eventing service (e.g. http://lxcmd101:1980/urn:xdaq-application:lid=400)
	 *	@param	consumerurn fully qualified urn of the local listener, this is the urn returned by the listen function
	 *	@param	expires	new expiration time in seconds sent to the ws eventing service
	 *	@param	subscriptionid unique identifier for the subscription
	 *	@throws WseException in case renewal failed due to connection errors
	 */
	public void renew(String eventingurn, String consumerurn, int expires, String subscriptionid) throws WseException
	{
		try
		{
			client.renew(eventingurn, consumerurn, null, null, "PT"+expires+"S", subscriptionid);
		}
		catch(Exception e)
		{
			throw new WseException("Renew failed", e);
		}
	}

	/**
	 * unsubscribe the listener from the ws eventing service
	 * 	@param	eventingurn	fully qualified urn of the ws eventing service (e.g. http://lxcmd101:1980/urn:xdaq-application:lid=400)
	 *	@param	subscriptionid unique identifier for the subscription
	 *	@throws WseException in case renewal failed due to connection errors
	 */
	public void unsubscribe(String eventingurn, String subscriptionid) throws WseException
	{
		// halt renew thread
		Object thread=map.get(subscriptionid);
		if(thread!=null)
		{
			System.out.println("halting according renew thread");
			((RenewThread)thread).halt();
		}

		try
		{
			// unsubscribe at server
			client.unSubscribe(eventingurn, subscriptionid);
		}
		catch(Exception e)
		{
			throw new WseException("unsubscribe failed", e);
		}
	}

	private class RenewThread extends Thread
	{
		private String eventingurn;
		private String consumerurn;
		private String subscriptionid;
		private int expires;
		private int renew;

		private boolean stop;

		public RenewThread(String eventingurn, String consumerurn, String subscriptionid, int expires, int renew)
		{
			this.eventingurn = eventingurn;
			this.consumerurn = consumerurn;
			this.subscriptionid = subscriptionid;
			this.expires = expires;
			this.renew = renew;

			this.stop = false;
		}

		public void run()
		{
			while(!stop)
			{
				try
				{
					sleep(renew*1000);
				}
				catch(Exception e)
				{ }

				if(!stop)
				{
					try
					{
						System.out.println("renew");
						renew(eventingurn, consumerurn, expires, subscriptionid);
					}
					catch(WseException e)
					{
						System.err.println(e.getMessage());
					}
				}
			}
		}

		public void halt()
		{
			synchronized(this)
			{
				stop = true;
				notifyAll();
			}
		}
	}
}

