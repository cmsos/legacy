package xmltreeeditor;

import java.util.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

class XMLTreeCellRenderer
    extends JTable
    implements TreeCellRenderer, TreeCellEditor {

  protected XMLTreeNodeUserObject nodeobj;
  protected DefaultMutableTreeNode p_node;
  protected Component returnComponent;
  protected Color m_textSelectionColor;
  protected Color m_textNonSelectionColor;
  protected Color m_bkSelectionColor;
  protected Color m_bkNonSelectionColor;
  protected Color m_borderSelectionColor;
  protected boolean m_selected;
  protected XMLTree m_tree = null;

  public XMLTreeCellRenderer() {
    this.setRowSelectionAllowed(false);
    this.setColumnSelectionAllowed(false);
    this.getSelectionModel().setSelectionMode(ListSelectionModel.
                                              SINGLE_SELECTION);
    XMLTableCellRenderer tableCellRenderer = new XMLTableCellRenderer();
    XMLTableCellEditor tableCellEditor = new XMLTableCellEditor();

    this.setDefaultRenderer(EditableText.class, new XMLTableCellRenderer());
    this.setDefaultRenderer(ImageIcon.class, new XMLTableCellRenderer());
    this.setDefaultRenderer(NotEditableText.class, new XMLTableCellRenderer());
    this.setDefaultRenderer(IconWithText.class, new XMLTableCellRenderer());

    this.setDefaultEditor(EditableText.class, new XMLTableCellEditor());
    this.setDefaultEditor(ImageIcon.class, new XMLTableCellEditor());
    this.setDefaultEditor(NotEditableText.class, new XMLTableCellEditor());
    this.setDefaultEditor(IconWithText.class, new XMLTableCellEditor());
  }

//IMPLEMENT TREECELLEDITOR
  public void removeCellEditorListener(CellEditorListener l) {
  }

  public void addCellEditorListener(CellEditorListener l) {
  }

  public void cancelCellEditing() {

    XMLTableModel m_mod = (XMLTableModel)this.getModel();

    try {
      XMLTableCellEditor ed = (XMLTableCellEditor)this.getCellEditor();

      m_mod.setValueAt(new EditableText(ed.getCellEditorValue().toString()), 0,
                       ed.col);
    }
    catch (Exception e) {

    }

    switch (nodeobj.getType()) {
      case 1:
        nodeobj.setName(m_mod.getValueAt(0, 1).toString());
        break;
      case 2:
        nodeobj.setName(m_mod.getValueAt(0, 1).toString());
        nodeobj.setValue(m_mod.getValueAt(0, 2).toString());

        XMLTree temp_tree = (XMLTree)this.getParent();
        TreePath temp_path = temp_tree.getSelectionPath();
        DefaultMutableTreeNode temp_node = (DefaultMutableTreeNode) temp_path.
            getLastPathComponent();
        DefaultMutableTreeNode temp_parent_node = (DefaultMutableTreeNode)
            temp_node.getParent();
        XMLTreeNodeUserObject parent_user_object = (XMLTreeNodeUserObject)
            temp_parent_node.
            getUserObject();

        String atrrString = "";
        XMLTreeNodeUserObject tmpUserObject;
        for (int i = 0; i < temp_parent_node.getChildCount(); i++) {
          temp_node = (DefaultMutableTreeNode) temp_parent_node.getChildAt(i);
          tmpUserObject = (XMLTreeNodeUserObject) temp_node.getUserObject();
          if (tmpUserObject.getType() == 2) {
            atrrString = atrrString + tmpUserObject.getName() + "=" +
                tmpUserObject.getValue() + ", ";
          }
        }
        atrrString = atrrString.substring(0, atrrString.length() - 2);
        atrrString = "(" + atrrString.trim() + ")";
        parent_user_object.setAttrList(atrrString);
        temp_tree.revalidate();
        temp_tree.repaint();
        break;
      case 3:
        nodeobj.setValue(m_mod.getValueAt(0, 1).toString());
        break;
      case 4:
        nodeobj.setValue(m_mod.getValueAt(0, 1).toString());
        break;
      case 7:
        nodeobj.setName(m_mod.getValueAt(0, 1).toString());
        nodeobj.setValue(m_mod.getValueAt(0, 2).toString());
        break;
      case 8:
        nodeobj.setValue(m_mod.getValueAt(0, 1).toString());
        break;
    }
  }

  public boolean stopCellEditing() {

    return true;
  }

  public XMLTree get_Tree() {
    return m_tree;
  }

  public boolean shouldSelectCell(EventObject anEvent) {

    return true;
  }

  public boolean isCellEditable(EventObject ev) {

    boolean returnValue = false;
    if (ev instanceof MouseEvent) {
      MouseEvent evt = (MouseEvent) ev;
      if (evt.getClickCount() > 1) {
        returnValue = true;
      }
    }
    return returnValue;
  }

  public Component getTreeCellEditorComponent(JTree tree,
                                              Object value,
                                              boolean selected,
                                              boolean expanded,
                                              boolean leaf,
                                              int row
                                              ) {
    m_tree = (XMLTree) tree;
    DefaultMutableTreeNode x = (DefaultMutableTreeNode) value;
    XMLTreeNodeUserObject node = (XMLTreeNodeUserObject) x.getUserObject();
    m_selected = selected;
    nodeobj = node;
    this.setModel(new XMLTableModel(node));
    setSizes();
    setBorders();
    return this;
  }

  public Object getCellEditorValue() {
    return this;
  }

  public Component getTreeCellRendererComponent(JTree tree,
                                                Object value,
                                                boolean selected,
                                                boolean expanded,
                                                boolean leaf,
                                                int row,
                                                boolean hasFocus)

  {
    m_tree = (XMLTree) tree;
    DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
    Object x = node.getUserObject();
    XMLTreeNodeUserObject nobj = (XMLTreeNodeUserObject) x;
    nodeobj = nobj;
    m_selected = selected;
    this.setModel(new XMLTableModel(nobj));
    setSizes();
    setBorders();
    return this;
  }

  void setSizes() {
    this.setRowHeight(25);
    switch (nodeobj.getType()) {
      case 1:
        this.getColumnModel().getColumn(0).setPreferredWidth(30);
        if (nodeobj.getAttrList() != null &&
            nodeobj.getAttrList().trim() != "") {
          this.getColumnModel().getColumn(1).setPreferredWidth(170);
          this.getColumnModel().getColumn(2).setPreferredWidth(300);
        }
        else {
          this.getColumnModel().getColumn(1).setPreferredWidth(470);
        }
        break;
      case 3:
      case 4:
      case 8:
        this.getColumnModel().getColumn(0).setPreferredWidth(100);
        this.getColumnModel().getColumn(1).setPreferredWidth(400);
        break;
      case 2:
      case 7:
        this.getColumnModel().getColumn(0).setPreferredWidth(30);
        this.getColumnModel().getColumn(1).setPreferredWidth(170);
        this.getColumnModel().getColumn(2).setPreferredWidth(300);
        break;
    }
  }

  public void setBorders() {
    if (m_selected) {
      this.setBorder(BorderFactory.createLineBorder(Color.lightGray));
    }
    else {
      this.setBorder(BorderFactory.createLineBorder(Color.white));
    }
    this.setGridColor(Color.white);

  }
}
