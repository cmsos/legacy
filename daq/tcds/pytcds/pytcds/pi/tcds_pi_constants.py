# This is the overall directory for all address table files.
from pytcds.utils.tcds_constants import ADDRESS_TABLE_DIR

# The uhal address table for the PI.
ADDRESS_TABLE_FILE_NAME_PI = "%s/%s" % \
                             (ADDRESS_TABLE_DIR,
                              "device_address_table_fc7_pi.xml")
