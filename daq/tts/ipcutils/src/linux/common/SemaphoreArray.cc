/**
*      @file SemaphoreArray.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.1 $
*     $Date: 2007/03/28 12:06:24 $
*
*
**/
#include "tts/ipcutils/SemaphoreArray.hh"

#include <string>
#include <sstream>

#include <errno.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>



#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
/* union semun is defined by including <sys/sem.h> */
#else
/* according to X/OPEN we have to define it ourselves */
union semun {
  int val;                  /* value for SETVAL */
  struct semid_ds *buf;     /* buffer for IPC_STAT, IPC_SET */
  unsigned short *array;    /* array for GETALL, SETALL */
  /* Linux specific part: */
  struct seminfo *__buf;    /* buffer for IPC_INFO */
};
#endif


ipcutils::SemaphoreArray::SemaphoreArray(const char* keyFile, char projectId, bool doCreate, uint32_t nsem)
  throw (ipcutils::exception::NoExist, 
	 ipcutils::exception::Exception) {

  _sid = getSema(keyFile, projectId, doCreate, nsem);
  _nsem = nsem;

}

ipcutils::SemaphoreArray::~SemaphoreArray() {};


bool ipcutils::SemaphoreArray::isLocked(uint32_t isem)
  throw (ipcutils::exception::OutOfRange,
	 ipcutils::exception::Exception) {
  
  if (isem >= _nsem)
    XCEPT_RAISE(ipcutils::exception::OutOfRange, "isem out of range");

  int semval = semctl(_sid, (int) isem, GETVAL, 0);

  if (semval == -1) {
    std::stringstream msg;
    msg << "Unkown error reading semaphore. errno=" << std::dec << errno;
    XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
  }

  return semval == 0;
}

uint32_t ipcutils::SemaphoreArray::getPID(uint32_t isem)
  throw (ipcutils::exception::OutOfRange,
	 ipcutils::exception::Exception) {
  
  if (isem >= _nsem)
    XCEPT_RAISE(ipcutils::exception::OutOfRange, "isem out of range");

  int pidval = semctl(_sid, (int) isem, GETPID, 0);

  if (pidval == -1) {
    std::stringstream msg;
    msg << "Unkown error reading semaphore PID. errno=" << std::dec << errno;
    XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
  }

  return (uint32_t) pidval;

}


bool ipcutils::SemaphoreArray::takeIfUnlocked(uint32_t isem)
  throw (ipcutils::exception::OutOfRange,
	 ipcutils::exception::Exception) {
  
  if (isem >= _nsem)
    XCEPT_RAISE(ipcutils::exception::OutOfRange, "isem out of range");

  struct sembuf sem_lock={ (unsigned short) isem, -1, IPC_NOWAIT | SEM_UNDO}; 
  bool success = semop(_sid, &sem_lock, 1) != -1; 

  if (!success && errno != EAGAIN) {
    std::stringstream msg;
    msg << "Unkown error locking semaphore. errno=" << std::dec << errno;
    XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
  }

  return success;
}

void ipcutils::SemaphoreArray::take(uint32_t isem)
  throw (ipcutils::exception::OutOfRange,
	 ipcutils::exception::Exception) {
  
  if (isem >= _nsem)
    XCEPT_RAISE(ipcutils::exception::OutOfRange, "isem out of range");

  struct sembuf sem_lock={ (unsigned short) isem, -1, SEM_UNDO};
  bool success = semop(_sid, &sem_lock, 1) != -1; 

  if (!success) {
    std::stringstream msg;
    msg << "Unkown error locking semaphore. errno=" << std::dec << errno;
    XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
  }
}



bool ipcutils::SemaphoreArray::takeTimed(uint32_t isem, uint32_t timeout_sec)
  throw (ipcutils::exception::OutOfRange,
	 ipcutils::exception::Exception) {
  
  if (isem >= _nsem)
    XCEPT_RAISE(ipcutils::exception::OutOfRange, "isem out of range");

  struct timespec ts;
  ts.tv_sec = timeout_sec;
  ts.tv_nsec = 0;

  struct sembuf sem_lock={ (unsigned short) isem, -1, SEM_UNDO};
  bool success = semtimedop(_sid, &sem_lock, 1, &ts) != -1; 

  if (!success && errno != EAGAIN) {
    std::stringstream msg;
    msg << "Unkown error locking semaphore. errno=" << std::dec << errno;
    XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
  }

  return success;
}



void ipcutils::SemaphoreArray::give(uint32_t isem)
  throw (ipcutils::exception::OutOfRange,
	 ipcutils::exception::Exception) {
  
  if (isem >= _nsem)
    XCEPT_RAISE(ipcutils::exception::OutOfRange, "isem out of range");

  struct sembuf sem_lock={ (unsigned short) isem, 1, IPC_NOWAIT | SEM_UNDO};

  bool success = semop(_sid, &sem_lock, 1) != -1; 

  if (!success) {
    std::stringstream msg;
    msg << "Unkown error unlocking semaphore. errno=" << std::dec << errno;
    XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
  }
}



void ipcutils::SemaphoreArray::createLocks(const char* keyFile, char projectId, uint32_t nsem) 
  throw (ipcutils::exception::Exception) {

  bool doCreate = true;
  int sid = getSema(keyFile, projectId, doCreate, nsem);
  resetSemaphores(sid, nsem);
}

void ipcutils::SemaphoreArray::destroyLocks(const char* keyFile, char projectId)
  throw (ipcutils::exception::NoExist,
	 ipcutils::exception::Exception) {

  bool doCreate = false;
  int sid = getSema(keyFile, projectId, doCreate, 0);

  if ( semctl(sid, 0, IPC_RMID) == -1 ) {
    std::stringstream msg;
    msg << "Error removing semaphore set. errno=" << std::dec << errno;
    XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
  }
 
};
  
void ipcutils::SemaphoreArray::resetLocks(const char* keyFile, char projectId, uint32_t nsem) 
  throw (ipcutils::exception::NoExist,
	 ipcutils::exception::Exception) {

  bool doCreate = false;
  int sid = getSema(keyFile, projectId, doCreate, nsem);

  resetSemaphores(sid, nsem);
};




int ipcutils::SemaphoreArray::getSema(const char* keyFile, char projectId, bool doCreate, uint32_t nsem)
  throw (ipcutils::exception::NoExist,
	 ipcutils::exception::Exception) {

  key_t mykey = ftok(keyFile, projectId);
  int semflags = 0666;

  // first try to get an existing semaphore array
  int sid = semget(mykey, nsem, semflags);


  // if it does not exist, create an array and initialize it
  if (sid == -1 && errno == ENOENT && doCreate) {

    semflags |= IPC_CREAT;
    sid = semget(mykey, nsem, semflags);

    if (sid != -1) {
      resetSemaphores(sid, nsem);
    }
  }


  if( sid == -1) {
    
    std::string sem_name = std::string("keyfile=") + keyFile + std::string(", projectId=") + projectId; 

    switch (errno) {

    case ENOENT: 
      XCEPT_RAISE(ipcutils::exception::NoExist, 
		  std::string("Semaphore array ") +  sem_name + "  does not exist on this host. ");
      break;
    case EACCES:  
      XCEPT_RAISE(ipcutils::exception::Exception, 
		  std::string("Insufficient accesss rights accessing semaphore array ") + sem_name + ".");
      break;
    default:
      std::stringstream msg;
      msg << "Unkown error accessing semaphore array " << sem_name << ". errno=" << std::dec << errno;
      XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
      break;
    }
  }

  return sid;
}


void ipcutils::SemaphoreArray::resetSemaphores(int sid, uint32_t nsem) 
  throw (ipcutils::exception::Exception) {

  union semun semopts;
  semopts.val = 1;

  for (uint32_t i=0; i<nsem; i++)
    if ( semctl(sid, i, SETVAL, semopts) == -1 ) {
      std::stringstream msg;
      msg << "Error resetting semaphore " << std::dec << i << "; errno=" << std::dec << errno;
      XCEPT_RAISE(ipcutils::exception::Exception, msg.str() );
    }
};

