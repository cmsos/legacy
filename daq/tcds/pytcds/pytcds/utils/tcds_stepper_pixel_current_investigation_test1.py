#!/usr/bin/env python

###############################################################################
## Script to help track down/diagnose the issue with the varying
## Phase-1 pixel digital curents as a function of CPM (or LPM)
## activity.
###############################################################################

import signal
import sys

from pytcds.cpm.tcds_cpm import CPM
from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.lpm.tcds_lpm import LPM
from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_step_runner import Step
from pytcds.utils.tcds_step_runner import StepPrintMessage
from pytcds.utils.tcds_step_runner import StepRunner
from pytcds.utils.tcds_step_runner import StepSleep
from pytcds.utils.tcds_step_runner import StepWaitForEnter

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

FIVE_MINUTES = 5 * 60
TWO_MINUTES = 2 * 60
STEP_TIME = TWO_MINUTES

# XPM = "cpm-t1"
# XPM = "lpm-pixel-pri"
XPM = "cpm-pri-t1"

###############################################################################

def exit_strategy(signal, frame):
    print
    sys.exit()
    # End of exit_strategy().

###############################################################################

class StepPMBase(Step):

    def __init__(self, description, function, hw):
        # Figure out if we're running on a CPM or an LPM.
        if isinstance(hw, CPM):
            self._mode = "CPM"
            self._prefix = "cpmt1."
        elif isinstance(hw, LPM):
            self._mode = "LPM"
            self._prefix = ""
        else:
            raise ValueError("Class {0:s}" \
                             " expects either a CPM or an LPM".format(self.__class__.__name__))
        super(StepPMBase, self).__init__(description, function)
        self.hw = hw
        # End of __init__().

    # End of class StepPMBase.

###############################################################################

class StepPMDisableTriggerSources(StepPMBase):

    def __init__(self, hw):
        description = "Disabling all trigger sources"
        function = self.tweak_inselect
        super(StepPMDisableTriggerSources, self).__init__(description, function, hw)
        # End of __init__().

    def tweak_inselect(self):
        self.hw.write("{0:s}ipm.main.inselect.external_trigger0_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.external_trigger1_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.external_trigger_from_cpm_l1a_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.random_trigger_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.bunch_mask_trigger_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.cyclic_trigger_enable".format(self._prefix), 0x0)
        # End of tweak_inselect().
        return True

    # End of class StepPMDisableTriggerSources.

###############################################################################

class StepPMDisableBGo(StepPMBase):

    def __init__(self, hw, bgo_number):
        description = "Disabling B-go {0:d}".format(bgo_number)
        function = self.disable_bgo
        super(StepPMDisableBGo, self).__init__(description, function, hw)
        self.bgo_number = bgo_number
        # End of __init__().

    def disable_bgo(self):
        self.hw.write("{0:s}ipm.bgo_channels.bgo_channel{1:d}.firing_bx".format(self._prefix,
                                                                                self.bgo_number), 0x0)
        # End of disable_bgo().
        return True

    # End of class StepPMDisableBGo.

###############################################################################

class StepPMSetRandomTriggerRate(StepPMBase):

    def __init__(self, hw, rate):
        description = "Disabling random triggers"
        if rate > 0:
            description = "Enabling random trigger rate at {0:.1f} Hz".format(rate)
        function = self.set_random_trigger_rate
        super(StepPMSetRandomTriggerRate, self).__init__(description, function, hw)
        self.rate = rate
        # End of __init__().

    def set_random_trigger_rate(self):
        enable = 0
        val = 0
        if self.rate > 0:
            enable = 1
            val = int(self.rate / (3564. * 11246 / 2**32))
        self.hw.write("{0:s}ipm.main.random_trigger_config".format(self._prefix), val)
        self.hw.write("{0:s}ipm.main.inselect.random_trigger_enable".format(self._prefix), enable)
        # End of set_random_trigger_rate().
        return True

    # End of class StepPMSetRandomTriggerRate.

###############################################################################

class StepPMSetROCResetRate(StepPMBase):

    def __init__(self, hw, rate):
        description = "Disabling periodic ROC reset sequence"
        if rate > 0:
            description = "Enabling periodic ROC reset sequence at {0:.1f} Hz".format(rate)
        function = self.set_roc_reset_rate
        super(StepPMSetROCResetRate, self).__init__(description, function, hw)
        self.rate = rate
        # End of __init__().

    def set_roc_reset_rate(self):
        enable = 0
        val = 0
        if self.rate > 0:
            enable = 1
            val = int(round(11246. / self.rate))
        self.hw.write("{0:s}ipm.cyclic_generator6.configuration.prescale".format(self._prefix), val)
        self.hw.write("{0:s}ipm.cyclic_generator6.configuration.enabled".format(self._prefix), enable)
        # End of set_roc_reset_rate().
        return True

    # End of class StepPMSetROCResetRate.

###############################################################################

class Runner(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Runner, self).__init__(description, usage, epilog)
        self.target = XPM
        # End of __init__().

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def handle_args(self):
        self.args.target = self.target
        super(Runner, self).handle_args()
        # End of handle_args().

    def main(self):

        # Validate the target and connect to the hardware.
        print "Validating hardware target, and connecting to the hardware"
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        if self.target.lower().startswith("cpm"):
            f = get_cpm_hw
        if self.target.lower().startswith("lpm"):
            f = get_lpm_hw
        pm = f(network_address, controlhub_address, verbose=self.verbose)
        if not pm:
            self.error("Could not connect to {0:s}".format(self.ip_address))

        #----------

        print "Building procedure steps"
        steps = [
            StepPrintMessage("Starting"),

            StepWaitForEnter("Please 'Configure' a run" \
                             " _with_ pixel included" \
                             " and then press Enter..."),
            StepWaitForEnter("Please 'destroy' pixel" \
                             " and 'Configure' a run" \
                             " _without_ pixel included" \
                             " and then press Enter..."),
            StepPMDisableTriggerSources(pm),
            StepPMSetROCResetRate(pm, 70),
            StepWaitForEnter("Please 'Start' the run" \
                             " and then press Enter..."),
            StepSleep(STEP_TIME),

            StepPMSetROCResetRate(pm, 50),
            StepSleep(STEP_TIME),
            StepPMSetROCResetRate(pm, 30),
            StepSleep(STEP_TIME),
            StepPMSetROCResetRate(pm, 10),
            StepSleep(STEP_TIME),
            StepPMSetROCResetRate(pm, 1),
            StepSleep(STEP_TIME),
            StepPMSetROCResetRate(pm, 0.1),
            StepSleep(STEP_TIME),
            StepPMSetROCResetRate(pm, 0),
            StepSleep(STEP_TIME),

            StepPMSetROCResetRate(pm, 70),
            StepSleep(STEP_TIME),
            # Disable EC0.
            StepPMDisableBGo(pm, 7),
            StepSleep(STEP_TIME),
            # Disable ROC resets.
            StepPMSetROCResetRate(pm, 0),
            StepSleep(STEP_TIME),

            StepPrintMessage("Done")
        ]

        #----------

        print "Running procedure steps"
        stepper = StepRunner(steps)
        stepper.run()

        #----------

        # End of main().

    # End of class Runner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to help track down/diagnose the issue" \
               " with the varying Phase-1 pixel digital curents" \
               " as a function of CPM/LPM activity."

    signal.signal(signal.SIGINT, exit_strategy)
    res = Runner(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
