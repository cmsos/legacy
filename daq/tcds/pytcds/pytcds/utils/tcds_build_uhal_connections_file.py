#!/usr/bin/env python

###############################################################################
## Utility to build a uhal connections (XML) file based on a TCDS
## hardware description (JSON) file.
###############################################################################

import sys

from xml.dom import minidom
from xml.etree import ElementTree as ET

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

address_table_path = "${XDAQ_ROOT}/etc/tcds/addresstables/"
address_table_file_names = {
    "cpm-t1": "device_address_table_amc13_cpmt1.xml",
    "cpm-t2": "device_address_table_amc13_cpmt2.xml",
    "lpm": "device_address_table_fc7_lpm.xml",
    "ici": "device_address_table_fc7_lpm.xml",
    "apve": "device_address_table_fc7_lpm.xml",
    "pi": "device_address_table_fc7_pi.xml",
    "phasemon": "device_address_table_fc7_phasemon.xml"
}

###############################################################################

def prettify(elem):

    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    res = reparsed.toprettyxml(indent="  ", encoding='utf-8')

    # End of prettify().
    return res

###############################################################################

class Builder(CmdLineBase):

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def main(self):

        out_file_name = "tcds_connections.xml"
        sep_line = " " + "+" * 10 + " "

        #----------

        doc = ET.Element('connections')

        doc.append(ET.Comment(sep_line))
        doc.append(ET.Comment("WARNING: This file is auto-generated. Do not edit!"))
        doc.append(ET.Comment(sep_line))

        #----------

        target_types = list(set([i[1].type for i in self.setup.hw_targets.items()]))
        for target_type in target_types:
            targets = [(i, j) for (i, j) in self.setup.hw_targets.iteritems() if j.type == target_type]
            for (target_id, target) in sorted(targets):
                if hasattr(target, 'vme_chain'):
                    # VME stuff does not matter for the UHAL
                    # connections file.
                    continue
                tmp = ET.SubElement(doc, "connection")
                tmp.set("id",
                        target_id)
                board = None
                if target_type in ['ici', 'apve']:
                    board = self.setup.hw_targets[target.id_lpm]
                else:
                    board = target
                tmp.set("uri",
                        "chtcp-2.0://" + board.controlhub + ":10203?target=" + board.dns_alias + ":50001")
                address_table_file_name = address_table_file_names[target_type]
                tmp.set("address_table",
                        "file://" + address_table_path + address_table_file_name)

        #----------

        doc.append(ET.Comment(sep_line))

        #----------

        with open(out_file_name, 'w') as out_file:
            out_file.write(prettify(doc))

        # End of main().

    # End of class Builder.

###############################################################################

if __name__ == "__main__":

    desc_str = "Utility to build a uhal connections (XML) file based on a TCDS " \
               "hardware description (JSON) file."
    res = Builder(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
