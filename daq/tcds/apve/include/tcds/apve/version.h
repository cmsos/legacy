#ifndef _tcds_apve_version_h_
#define _tcds_apve_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSAPVE_VERSION_MAJOR 3
#define TCDSAPVE_VERSION_MINOR 13
#define TCDSAPVE_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSAPVE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSAPVE_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSAPVE_VERSION_CODE PACKAGE_VERSION_CODE(TCDSAPVE_VERSION_MAJOR,TCDSAPVE_VERSION_MINOR,TCDSAPVE_VERSION_PATCH)
#ifndef TCDSAPVE_PREVIOUS_VERSIONS
#define TCDSAPVE_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSAPVE_VERSION_MAJOR,TCDSAPVE_VERSION_MINOR,TCDSAPVE_VERSION_PATCH)
#else
#define TCDSAPVE_FULL_VERSION_LIST TCDSAPVE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSAPVE_VERSION_MAJOR,TCDSAPVE_VERSION_MINOR,TCDSAPVE_VERSION_PATCH)
#endif

namespace tcdsapve
{
  const std::string package = "tcdsapve";
  const std::string versions = TCDSAPVE_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS APVE.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS APVE.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
