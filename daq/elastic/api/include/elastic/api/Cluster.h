// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _elastic_api_EventingBus_h_
#define _elastic_api_EventingBus_h_

#include "xdaq/Object.h"

#include "toolbox/Properties.h"

#include "elastic/api/exception/Exception.h"

#include "elastic/api/Stream.h"


namespace elastic
{
	namespace api
	{
		class Cluster /*: public toolbox::ActionListener, public toolbox::EventDispatcher*/
		{
			public:

				Cluster (xdaq::Application * owner, const std::string & url, toolbox::Properties & p) ;

				std::string getClusterName () ;
				std::string getClusterURL ();

				size_t getNumberOfDocuments() ;

				// insert documents
				json_t * index(const std::string& index, const std::string& type, const std::string& id, const std::string& properties, json_t * json) ;
				/// insert,  a document with automatic id creation
				json_t * index(const std::string& index, const std::string& type, const std::string& properties, json_t * json) ;

				json_t * search(const std::string& index, const std::string& type, const std::string& properties, json_t * json) ;

				json_t * getDocument(const std::string& index, const std::string& type, const std::string& id) ;
				json_t * getIndexStats(const std::string& index, const std::string& item) ;

				json_t * createIndex(const std::string& index, json_t* properties) ;
				json_t * createIndexData(const std::string& index, json_t * properties) ;
				json_t * deleteIndex(const std::string& index) ;

				json_t * createMapping(const std::string& index, const std::string& type, json_t * mapping) ;
				json_t * createTemplate(const std::string & templateName, json_t * templateJSON) ;
				json_t * getMapping(const std::string& index, const std::string& type) ;
				json_t * deleteMapping(const std::string& index, const std::string& type) ;

				//void actionPerformed(toolbox::Event& e);
				bool exists(const std::string& index, const std::string& type ) ;

				json_t * getClusterHealth() ;

				json_t * bulkIndex(const std::string& index, const std::string& properties, std::stringstream & bulk) ;

				void showStats();
			private:

				toolbox::Properties properties_;
				std::string name_;
				std::string url_;
				//elastic::api::Stream stream_;
				std::vector<elastic::api::Stream*> streams_;
				size_t streamNum_;
				size_t numStreams_;

		};
	}
}

#endif
