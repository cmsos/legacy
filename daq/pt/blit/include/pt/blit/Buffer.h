// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_Buffer_h_
#define _pt_blit_Buffer_h_

#include <sys/types.h>
#include <sys/user.h>

#include "toolbox/mem/Buffer.h"
#include "toolbox/mem/Pool.h"
#include "tcpla/EndPoint.h"

namespace pt
{
	namespace blit
	{

	class Buffer : public toolbox::mem::Buffer
	{
		public:


			Buffer (toolbox::mem::Pool * pool, size_t size, void* address);


			// this cookies can be set dynamically to determine the context information

			tcpla::EndPoint ep_handle_;
			int sid_;
			int index_;


		protected:

			Buffer (const Buffer& c)
				: toolbox::mem::Buffer(c)
			{
			}

		public:

	};
}
}

#endif
