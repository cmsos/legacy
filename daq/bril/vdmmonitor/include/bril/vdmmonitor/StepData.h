#ifndef _bril_vdmmonitor_StepData_h_
#define _bril_vdmmonitor_StepData_h_
//#include <string.h>
//#include <stdio.h>
#include <vector>

namespace bril{
  namespace vdmmonitor{
    class StepData{
    public:
    StepData():plane(0),timestampsec(0),sep(0.0),n_rate_messages(0),n_beamintensity_messages(0){}
      int plane; //1: X, 2:Y
      unsigned int timestampsec;
      float sep;
      int n_rate_messages;
      int n_beamintensity_messages;
      std::vector<float> rate;     
      std::vector<float> rateerror;
      std::vector<float> beam1intensity;
      std::vector<float> beam2intensity;
    };
  }//end ns vdmmonitor
}//end ns bril
#endif
