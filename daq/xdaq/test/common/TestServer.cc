#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XFNAdapter.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"


class myTest: public XRUAdapter, public XRCAdapter, public XEVMAdapter
{	 
       void readout (EventIdentifier* eid) {
       	cout << "readout:" << hex << *eid << endl;
       
       }
       void send (EventIdentifier* eid, DestinationCookie* dcookie) {
        cout << "send:" << hex << *eid << dcookie->addr << dcookie->cookie << endl;
       }
       void config (char * str) {
       	cout << "config:" << str << endl;
       }
       
       void start (char * str) {
       cout << "start:" << str << endl;
       }
       
       void stop (char * str) {
       cout << "stop:" << str << endl;
       }
       
       void shutdown (char * str) {
       cout << "shutdown:" << str << endl;
       }
       
       void action (char * str) {
        cout << "action:" << str << endl;
       }
        
       void suspend (char * str) {
        cout << "suspend:" << str << endl;
       
       }
       void resume (char * str) {
         cout << "resume:" << str << endl;
       }
       void load (char * str) {
         cout << "load:" << str << endl;
       }

        void allocate (EventProfile* eprofile, DestinationCookie* dcookie) {
        cout << "allocate1:" << hex << *eprofile << dcookie->addr << dcookie->cookie << endl;
       }
       
       void allocateRetrieve (FragmentSet* fset, EventProfile* eprofile, DestinationCookie* dcookie) {
        cout << "allocateRetrieve:" << hex << *fset << *eprofile << dcookie->addr << dcookie->cookie << endl;
       }
       
       void retrieve (EventIdentifier* eid, FragmentSet* fset, DestinationCookie* dcookie) {
         cout << "retrieve:" << hex << *eid << *fset << dcookie->addr << dcookie->cookie << endl;
       }
       
       void clear (EventIdentifier* eid) {
         cout << "clear:" << hex << *eid << endl;
       }
       

};

class myTest2: public XFUAdapter, public XFNAdapter
{	 
      void confirm (EventIdentifier* eid) {
         cout << "confirm:"  << hex << *eid << endl;
       }
       
       void cache (FragmentIdentifier* fid, EventIdentifier* eid,  DestinationCookie* dcookie, FragmentData* fdata)
       {
       	cout << "cache:" << hex << *fid << *eid << dcookie->addr << dcookie->cookie <<  fdata->length <<
	 (int)fdata->data[0] << endl;
       }
       
       void allocate (EventProfile* eprofile, DestinationCookie* dcookie) {
        cout << "allocate2:" << hex << *eprofile << dcookie->addr << dcookie->cookie << endl;
       }
       
       void collect(EventIdentifier* eid, FragmentSet* fs,
        DestinationCookie* dcookie){
	cout << "collect:" << hex << *eid << *fs << dcookie->addr << dcookie->cookie << endl;
	}
	
       void discard(EventIdentifier* eid){
         cout << "discard:"  << hex << *eid << endl;
	}
	
       void take (EventIdentifier* eid, FragmentDataSet* fds){
         cout << "take:"  << hex << *eid << fds->num << fds->fdata[0]->length <<
	 (int)fds->fdata[0]->data[0] << fds->fdata[1]->length <<
	 (int)fds->fdata[1]->data[0] << endl;
	}
	
};



extern "C" void Main(char * str)
{
  myTest * test = new myTest();
  myTest2 * test2 = new myTest2();
  tcpAddr      addr;
  addr.port = 20200;
  strcpy(addr.host,str);
  vxtcphandle * handle = new vxtcphandle();
  vxtcpiostream * iostream = new vxtcpiostream(handle);
  handle->bind(&addr);
  handle->listen(1);
  XIO * xio = new XIO(iostream);
 
  //XSDispatcherLoop * dloop = new XSDispatcherLoop(xio,0);
  XMDispatcherLoop * dloop = new XMDispatcherLoop(xio);
  //dloop->setListener(test);
  dloop->addListener(test);
  dloop->addListener(test2);
  cout << "Loop forever" << endl;
  
  retry: // if lost connection
  
  Try
  {

  	handle->accept();

  	dloop->foreverloop();
  }
  Catch(vxException e)
  {
  	cout << e;
	goto retry;
  }
  
  
}
//
// The main program
//
#ifndef vxworks
int main(int argc, char* argv[])
{
	Main(argv[1]);
 	
}
#endif
