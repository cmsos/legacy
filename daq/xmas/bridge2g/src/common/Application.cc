// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/bridge2g/Application.h"

#include "xmas/xmas.h"

#include "toolbox/stl.h"
#include "toolbox/string.h"
#include "toolbox/net/URL.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/exception/Processor.h"
#include "toolbox/task/exception/InvalidListener.h"
#include "toolbox/task/exception/NotActive.h"
#include "toolbox/task/exception/InvalidSubmission.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "pt/PeerTransportAgent.h"
#include "pt/SOAPMessenger.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/Event.h"

#include "xcept/tools.h"

#include "xplore/Interface.h"
#include "xplore/DiscoveryEvent.h"
#include "xplore/exception/Exception.h"

#include  "xgi/Input.h"
#include  "xgi/Output.h"
#include  "xgi/Method.h"
#include  "xgi/framework/Method.h"

#include "xgi/Utils.h"
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdata/InfoSpaceFactory.h"

#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"

#include "b2in/nub/Method.h" 

XDAQ_INSTANTIATOR_IMPL(xmas::bridge2g::Application)

xmas::bridge2g::Application::Application(xdaq::ApplicationStub * s)  
	: xdaq::Application(s),xgi::framework::UIManager(this), dialup_(0), splitup_(0)
{
	deadBand_ = false;

	s->getDescriptor()->setAttribute("icon", "/xmas/bridge2g/images/xmas-bridge2g-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/bridge2g/images/xmas-bridge2g-icon.png");
	// override default service name
	//getApplicationDescriptor()->setAttribute("service"this, "xmasbridge2g");

	// Memory pool configuration
	committedPoolSize_ = 0x100000 * 50; // 50 MB
	highThreshold_  = 0.9;
	lowThreshold_ = 0.8;
	maxReportMessageSize_ = 0x10000; // 64KB

	//TBD: remove dead band algorithm for memory allocation?
	this->getApplicationInfoSpace()->fireItemAvailable("committedPoolSize",    &committedPoolSize_);
	this->getApplicationInfoSpace()->fireItemAvailable("lowThreshold",         &lowThreshold_);
	this->getApplicationInfoSpace()->fireItemAvailable("highThreshold",        &highThreshold_);
        this->getApplicationInfoSpace()->fireItemAvailable("maxReportMessageSize",&maxReportMessageSize_);

	// Broker configuration
	brokerWatchdog_ = "PT30S"; // 30 seconds bys default
	publishGroup_ = "eventing";
	brokerURL_ = "";
	eventingURL_ = "";

	this->getApplicationInfoSpace()->fireItemAvailable("brokerWatchdog",          &brokerWatchdog_);
	this->getApplicationInfoSpace()->fireItemAvailable("brokerURL",               &brokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("publishGroup",            &publishGroup_);
	this->getApplicationInfoSpace()->fireItemAvailable("eventingURL",             &eventingURL_);

	this->getApplicationInfoSpace()->fireItemAvailable("flashlists",              &flashlists_);

	xgi::framework::deferredbind(this, this, &xmas::bridge2g::Application::Default, "Default");

	// SOAP binding
	xoap::bind(this, &xmas::bridge2g::Application::report, "report",  xmas::NamespaceUri );

	// B2IN binding
	b2in::nub::bind(this, &xmas::bridge2g::Application::onMessage );

	// Listen to events indicating the setting of the application's default values
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

xmas::bridge2g::Application::~Application()
{
}

void xmas::bridge2g::Application::actionPerformed(toolbox::Event& e)
{
	if ( e.type() == "xdaq::EndpointAvailableEvent" )
	{
		toolbox::TimeInterval interval;
		try
		{
			interval.fromString(brokerWatchdog_.toString()); 
		}		
		catch (toolbox::exception::Exception& e)
		{
			// Failed to parse the watchdog time string
			LOG4CPLUS_FATAL (this->getApplicationLogger(), "Failed to parse brokerWatchdog property '" << brokerWatchdog_.toString() << "'");
		}

		// If the available endpoint is a b2in endpoint we are ready to
		// discover other b2in endpoints on the network
		//
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		
		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
		const xdaq::Network* network = ie.getNetwork();
		
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());
		
		if (network->getName() == networkName)
		{
			dialup_ = new b2in::utils::EventingDialup (this, publishGroup_, brokerURL_, eventingURL_, networkName, interval);
     
	   		std::list<std::string> flashlists = toolbox::parseTokenList(flashlists_, ",");
                        splitup_ = new b2in::utils::TopicSplitup (this, brokerURL_, networkName, interval, flashlists);
		}
	}
}


void xmas::bridge2g::Application::actionPerformed (xdata::Event& e) 
{ 
	if ( e.type() == "urn:xdaq-event:setDefaultValues" )
	{
		try 
		{
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			toolbox::net::URN urn("xmas", "bridge2g");
			pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to create b2in/sensor memory pool for size " << committedPoolSize_.toString();
			LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
		}

		std::list<std::string> flashlists = toolbox::parseTokenList(flashlists_, ",");
		for(std::list<std::string>::iterator iter = flashlists.begin() ; iter != flashlists.end() ; iter++)
		{
			forward_[*iter] = new b2in::utils::Statistics();
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to process unknown event type '" << e.type() << "'";
		LOG4CPLUS_FATAL (this->getApplicationLogger(), msg.str());
	}
}

// Hyperdaq

void xmas::bridge2g::Application::Default(xgi::Input * in, xgi::Output * out ) 
{	
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
	
	// Tabbed pages
	
	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"Diagnostics\">" << std::endl;
	this->DiagnosticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void xmas::bridge2g::Application::StatisticsTabPage(xgi::Output * out) 
{
	*out << cgicc::table().set("class","xdaq-table-vertical")  << std::endl;
	*out << cgicc::caption("Eventing Dialup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getEventingURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total outgoing reports";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 

	//Splitup

	*out << cgicc::table().set("class","xdaq-table-vertical")  << std::endl;
	*out << cgicc::caption("Topic Splitup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td();
	if(splitup_ != 0)
	{
		*out << splitup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	if(splitup_ != 0)
	{
		*out << splitup_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if(splitup_ != 0)
	{
		*out << splitup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if(splitup_ != 0)
	{
		*out << splitup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl; 

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	// Per flashlist loss of reports
		
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");
	*out << cgicc::caption("Flashlist");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable");
	*out << cgicc::th("Fire Counter");
	*out << cgicc::th("Internal Loss");
	*out << cgicc::th("Enqueuing Loss");
	*out << cgicc::th("Out of Memory Loss");
	*out << cgicc::th("No Topic Loss");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();

	xdata::UnsignedInteger64 fireCounter              = 0;
	xdata::UnsignedInteger64 internalLossCounter      = 0;
	xdata::UnsignedInteger64 communicationLossCounter = 0;
	xdata::UnsignedInteger64 memoryLossCounter        = 0;
	xdata::UnsignedInteger64 unassignedLossCounter    = 0;

	for ( std::map<std::string, b2in::utils::Statistics*>::iterator i = forward_.begin() ; i != forward_.end() ; i++)
	{
		*out << cgicc::tr() << std::endl;
		*out << cgicc::td((*i).first) << std::endl;
		
		xdata::UnsignedInteger64 num;
		num = (*i).second->getFireCounter();
		fireCounter = fireCounter + num;
		*out << cgicc::td(num.toString()) << std::endl;

		num = (*i).second->getInternalLossCounter();
		internalLossCounter = internalLossCounter + num;
		*out << cgicc::td(num.toString()) << std::endl;

		num = (*i).second->getCommunicationLossCounter();
		communicationLossCounter = communicationLossCounter + num;
		*out << cgicc::td(num.toString()) << std::endl;

		num = (*i).second->getMemoryLossCounter();
		memoryLossCounter = memoryLossCounter + num;
		*out << cgicc::td(num.toString()) << std::endl;

		num = (*i).second->getUnassignedLossCounter();
		unassignedLossCounter = unassignedLossCounter + num;
		*out << cgicc::td(num.toString())  << std::endl;

		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();

	*out << cgicc::tfoot();

	*out << cgicc::tr() << std::endl;
	*out << cgicc::td("Total") << std::endl;

	*out << cgicc::td(fireCounter.toString()) << std::endl;

	*out << cgicc::td(internalLossCounter.toString()) << std::endl;

	*out << cgicc::td(communicationLossCounter.toString()) << std::endl;

	*out << cgicc::td(memoryLossCounter.toString()) << std::endl;

	*out << cgicc::td(unassignedLossCounter.toString()) << std::endl;

	*out << cgicc::tr() << std::endl;

	*out << cgicc::tfoot();
	*out << cgicc::table();
}

void xmas::bridge2g::Application::DiagnosticsTabPage(xgi::Output * out) 
{
	//TBD
}

void xmas::bridge2g::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	
{
	std::string action = plist.getProperty("urn:xmasbroker2g:action");
	if ( action == "allocate")
	{
		std::string model = plist.getProperty("urn:xmasbroker2g:model");
		if( model == "eventing"  && dialup_ != 0)
		{
			dialup_->onMessage(msg,plist);
		}
		else if (model == "topic" && splitup_ != 0)
		{
			splitup_->onMessage(msg,plist);
		}
		else
		{
			LOG4CPLUS_INFO(this->getApplicationLogger(), "unknown broker response: " << model);
		}
	}	
	if (msg != 0) msg->release();
}

xoap::MessageReference xmas::bridge2g::Application::report (xoap::MessageReference msg) 
{
	// DEBUG
	//	msg->writeTo(std::cout);
	//
	DOMNodeList* bodyList = msg->getSOAPPart().getEnvelope().getBody().getDOMNode()->getChildNodes();
	std::string namespaceURI = "";
	std::string namespacePrefix = "";
	std::string commandName = "";
	for (XMLSize_t i = 0; i < bodyList->getLength(); i++) 
	{
		DOMNode* command = bodyList->item(i);

		if (command->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			namespaceURI = xoap::XMLCh2String (command->getNamespaceURI());
			namespacePrefix = xoap::XMLCh2String (command->getPrefix());
			commandName = xoap::XMLCh2String (command->getLocalName());
			break;
		}
	}

	DOMDocument * document = msg->getSOAPPart().getEnvelope().getDOM()->getOwnerDocument();
	DOMNodeList * list =  document->getElementsByTagNameNS(xoap::XStr(xmas::NamespaceUri), xoap::XStr("sample"));
	if ( list->getLength() == 0)
	{
		XCEPT_RAISE(xoap::exception::Exception, "Could not find sample element");
	}

	std::string flashListName = xoap::getNodeAttribute(list->item(0),"flashlist");
	std::string originator = xoap::getNodeAttribute(list->item(0),"originator");
	std::string tagName = xoap::getNodeAttribute(list->item(0),"tag");
	std::string version = xoap::getNodeAttribute(list->item(0),"version");

	// extract attachment with flaslist data and serialize into a table
	std::list<xoap::AttachmentPart*> attachments = msg->getAttachments();			
	std::list<xoap::AttachmentPart*>::iterator j;
	for ( j = attachments.begin(); j != attachments.end(); j++ )
	{
		if ( (*j)->getSize() == 0 )
		{
			XCEPT_RAISE(xoap::exception::Exception, "empty attachment, cannot forward flashlist");
		}
		this->publishReport((*j)->getContent(),(*j)->getSize(), flashListName, version, tagName, originator);
	}			
	
	xoap::MessageReference reply = xoap::createMessage();
	xoap::SOAPEnvelope envelope = reply->getSOAPPart().getEnvelope();
	xoap::SOAPBody b = envelope.getBody();
	xoap::SOAPName responseName = envelope.createName(commandName + "Response", namespacePrefix, namespaceURI);
	b.addBodyElement ( responseName );
	return reply;
}

void xmas::bridge2g::Application::publishReport(char * buf, size_t size, std::string & flashlistname, std::string & version, std::string & tag, std::string & originator)
{
	if(size > maxReportMessageSize_)
	{
		std::stringstream msg;
		msg << "Monitoring report too big for flashlist='" << flashlistname << "' originator='" << originator << "' version='" << version << "' tag='" << tag << "'";
		XCEPT_DECLARE (xmas::bridge2g::exception::Exception, e, msg.str());
		this->notifyQualified("error", e);	
		return;
	}
	std::map<std::string, b2in::utils::Statistics*>::iterator found = forward_.find(flashlistname);
	if(found == forward_.end())
	{
		// flashlist not registered - silently ignore
		std::stringstream msg;
		msg << "Monitoring report discarded for flashlist='" << flashlistname << "' originator='" << originator << "' version='" << version << "' tag='" << tag << "'";
		LOG4CPLUS_INFO (this->getApplicationLogger(), msg.str());
		return;
	}

	b2in::utils::Statistics* current = (*found).second;

	current->incrementFireCounter();

	if ( dialup_ == 0 )
	{
		current->incrementCommunicationLossCounter();
		return;
	}

	if ( deadBand_ )
	{
		if ( ! pool_->isLowThresholdExceeded() )
		{
			deadBand_ = false;
			LOG4CPLUS_WARN (this->getApplicationLogger(), "exit dead band ( start receiving)");
		}
		else
		{
			// still in dead band, therefore cannot report metrics, lost
			current->incrementMemoryLossCounter();
			return;
		}
	}
	else if ( pool_->isHighThresholdExceeded() )
	{
		LOG4CPLUS_WARN (this->getApplicationLogger(), "enter dead band (start discarding)");
		deadBand_ = true;
		// cannot allocate buffers, therefore I return and the monitor report is lost
		current->incrementMemoryLossCounter();
		return;
	}

	// Prepare report message
	//
	xdata::Properties plist;
	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");

	plist.setProperty("urn:xmas-flashlist:name",       flashlistname);
	plist.setProperty("urn:xmas-flashlist:version",    version);
	plist.setProperty("urn:xmas-flashlist:tag",        tag);
	plist.setProperty("urn:xmas-flashlist:originator", originator);
	
	std::string topic = splitup_->getTopic(flashlistname);
	if ( topic != "" )
	{
		plist.setProperty("urn:b2in-eventing:topic", topic);
	}
	else
	{
		current->incrementUnassignedLossCounter();
		return; // give up, no topic to report
	}	
		
	plist.setProperty("urn:b2in-eventing:action", "notify");

	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxReportMessageSize_);
	}	
	catch (toolbox::mem::exception::Exception & ex )
	{
		current->incrementMemoryLossCounter();
		XCEPT_DECLARE_NESTED (xmas::bridge2g::exception::Exception, e, "Failed to allocate message for monitor report", ex);
		this->notifyQualified("error", e);	
		return;
	}

	
	if ( size > maxReportMessageSize_ )
	{
		std::stringstream msg;
                msg << "Binary message overflows (size =" << maxReportMessageSize_ << " )  to include attachment of size " << size;
		XCEPT_DECLARE (xmas::bridge2g::exception::Exception, e, msg.str());
                this->notifyQualified("error", e);
		current->incrementCommunicationLossCounter();
                ref->release();
		return;
	}

	memcpy((char*)ref->getDataLocation(), buf, size);
	ref->setDataSize(size);

	try
	{
		// LOG4CPLUS_INFO (this->getApplicationLogger(), "Send report for flashlist " << plist.getProperty("urn:xmas-flashlist:name"));
		dialup_->send(ref,plist);
		return;
	}
	catch (b2in::utils::exception::Exception & e)
	{
		current->incrementCommunicationLossCounter();
		ref->release();
	}
}

