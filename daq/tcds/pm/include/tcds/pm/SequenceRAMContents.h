#ifndef _tcds_pm_SequenceRAMContents_h_
#define _tcds_pm_SequenceRAMContents_h_

#include <stdint.h>
#include <string>
#include <vector>

#include "tcds/pm/SequenceRAMEntry.h"

namespace tcds {
  namespace pm {

    class SequenceRAMContents
    {

    public:
      SequenceRAMContents(std::vector<uint32_t> const dataIn);

      std::string getJSONString() const;

    private:
      // This constructor is not supposed to be used.
      SequenceRAMContents();

      std::vector<uint32_t> const rawData_;
      std::vector<SequenceRAMEntry> entries_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_SequenceRAMContents_h_
