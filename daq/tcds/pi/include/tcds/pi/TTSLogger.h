#ifndef _tcds_pi_TTSLogger_h_
#define _tcds_pi_TTSLogger_h_

#include <stdint.h>
#include <string>
#include <vector>

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace pi {

    class TTSLogger
    {

    public:

      enum LOGGING_BUFFER_MODE {LOGGING_BUFFER_MODE_SINGLESHOT = 0,
                                LOGGING_BUFFER_MODE_CIRCULAR = 1};

      TTSLogger(tcds::hwlayertca::TCADeviceBase& hw);
      ~TTSLogger();

      /* void startLogging() const; */
      /* void stopLogging() const; */
      /* void resetLogging() const; */
      /* void restartLogging() const; */
      /* bool isLoggingEnabled() const; */
      bool isLogFull() const;
      void setBufferMode(LOGGING_BUFFER_MODE const val) const;
      /* void setBC0Val(uint8_t const val) const; */
      /* void setBC0BX(uint16_t const val); */
      /* uint16_t bc0BX() const; */
      uint32_t readLogAddressPointer() const;
      std::vector<uint32_t> readLog() const;
      /* std::string readLogFormatted() const; */

    private:
      void zeroMemory() const;
      uint32_t logSize() const;
      uint32_t getNumLogEntries() const;

      tcds::hwlayertca::TCADeviceBase& hw_;

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_TTSLogger_h_
