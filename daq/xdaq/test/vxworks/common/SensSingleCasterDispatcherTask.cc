

#include "vxNet.h"
#include "SensSingleCasterDispatcherTask.h"
int _debug_SensSingleCasterDispatcherTask_ =0;


SensSingleCasterDispatcherTask::SensSingleCasterDispatcherTask( const char* interface,
			  const int unit,
			  const char* protoName,
			  const int type )

{
	status_    = OK;



	::strcpy ( interface_, interface );
	unit_ = unit;
	::strcpy ( protoName_, protoName );
	type_ = type;
	
	muxCookie_ = ::muxBind ( interface_,
			         unit_,
			         ( FUNCPTR ) SensSingleCasterDispatcherTask::wrapRcvRtn,
			         ( FUNCPTR ) SensSingleCasterDispatcherTask::wrapShutdownRtn,
			         ( FUNCPTR ) SensSingleCasterDispatcherTask::wrapTxRestartRtn,
			         SensSingleCasterDispatcherTask::wrapErrorRtn,
			         type_,
			         protoName_,
			         ( void* ) this );

	if ( muxCookie_ == NULL ) {
		status_ = ERROR;
		cout << "vxsenshandle::vxsenshandle:: Error muxCookie_ " << endl;
		return; // THROW
	}

	msgq_ = msgQCreate (8192, sizeof(M_BLK_ID), MSG_Q_FIFO);
}



SensSingleCasterDispatcherTask::~SensSingleCasterDispatcherTask ( )
{
	STATUS s;
	
	if ( muxCookie_ != NULL ) {
		s = ::muxUnbind(muxCookie_, type_, (FUNCPTR)SensSingleCasterDispatcherTask::wrapRcvRtn);
	}

}
BOOL SensSingleCasterDispatcherTask::stackRcvRtn ( long type, M_BLK_ID pNetBuff, LL_HDR_INFO* pLinkHdr)
{
	//::logMsg("stackRcvRtn dump pNetBuff %d\n", (int)pNetBuff, 0, 0, 0, 0, 0);
	msgQSend (msgq_, (char*)&pNetBuff, sizeof(M_BLK_ID), WAIT_FOREVER, MSG_PRI_NORMAL);
	
}


int SensSingleCasterDispatcherTask::svc()
{
	M_BLK_ID pNetBuff;
	
	for(;;) {
		msgQReceive (msgq_, (char*)&pNetBuff, sizeof(M_BLK_ID) , WAIT_FOREVER);	
		char* buf = pNetBuff->mBlkHdr.mData + 14 + 2;
		//::logMsg("svc  pNetBuff %d\n", (int)pNetBuff, 0, 0, 0, 0, 0);

		DAQProtocolHeader* proto;
		proto = (DAQProtocolHeader*) buf;
		
		this->dispatch(ntohs(proto->protocol),buf);
		
		//free packet when handled
		netMblkClChainFree(pNetBuff);
	}

}
void SensSingleCasterDispatcherTask::stackShutdownRtn ( )
{
}

void SensSingleCasterDispatcherTask::stackTxRestartRtn ( )
{

}

void SensSingleCasterDispatcherTask::stackErrorRtn ( END_OBJ* pEnd, END_ERR* pError )
{

}


//
// wrapper members
//

BOOL SensSingleCasterDispatcherTask::wrapRcvRtn ( void* muxCookie, long type, M_BLK_ID pNetBuff, LL_HDR_INFO* pLinkHdr,
			        void* pObj )
{
	//::logMsg("SensSingleCasterDispatcherTask::wrapRcvRtn\n", 0, 0, 0, 0, 0, 0);

	( ( SensSingleCasterDispatcherTask* ) pObj )->stackRcvRtn ( type, pNetBuff, pLinkHdr );
	//Hold packet, therefore free MBLK chain after use
	
	return(TRUE);
}

void SensSingleCasterDispatcherTask::wrapShutdownRtn ( void* muxCookie, void* pObj )
{
	( ( SensSingleCasterDispatcherTask* ) pObj )->stackShutdownRtn ( );
}

void SensSingleCasterDispatcherTask::wrapTxRestartRtn ( void* muxCookie, void* pObj )
{
	( ( SensSingleCasterDispatcherTask* ) pObj )->stackTxRestartRtn ( );
}

void SensSingleCasterDispatcherTask::wrapErrorRtn ( END_OBJ* pEnd, END_ERR* pError, void* pObj )
{
	( ( SensSingleCasterDispatcherTask* ) pObj )->stackErrorRtn( pEnd, pError );
}



