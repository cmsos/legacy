package xdaq.xdesk.xcontrol;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class XControlAction extends AbstractAction
{
	String command_;
	XControlViewer w_;
	public XControlAction (XControlViewer w, String command)
	{
		super(command);
		command_ = command;
		w_ = w;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		w_.publishCommand (command_);
	}
}
