// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_spotlightocci_Application_h_
#define _sentinel_spotlightocci_Application_h_

#include <string>
#include <map>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/AsynchronousEventDispatcher.h"

#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/ApplicationContext.h" 

#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"
#include "xdata/Properties.h"

#include "b2in/nub/Method.h"
#include "b2in/utils/MessengerCache.h"
#include "b2in/utils/ServiceProxy.h"

#include "xgi/Method.h"
#include "xgi/Utils.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "sentinel/exception/Exception.h"

#include "xplore/utils/DescriptorsCache.h"

#include "sentinel/spotlightocci/Repository.h"

namespace sentinel 
{
	namespace spotlightocci 
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager,
			public toolbox::ActionListener, 
			public xdata::ActionListener,
			public toolbox::task::TimerListener,
			public b2in::utils::MessengerCacheListener
		{
			public:

			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub* s) ;
			~Application();

			void actionPerformed ( xdata::Event& e );
			
			void actionPerformed( toolbox::Event& event );
			
			void timeExpired(toolbox::task::TimerEvent& e);

			//
			// XGI Interface
			//
			void Default(xgi::Input * in, xgi::Output * out ) ;
			
			/*! Clear exception history
			 */
			void reset(xgi::Input * in, xgi::Output * out ) ;
			
			/*! View a specific exception referenced by uuid
			 */
			void view(xgi::Input * in, xgi::Output * out ) 
				;
				
			/*! list all available exceptions
			 */
			void catalog(xgi::Input * in, xgi::Output * out ) 
				;
			
			void query(xgi::Input*, xgi::Output*)
				;

			void events(xgi::Input * in, xgi::Output * out ) 
				;		
				
			void lastStoredEvents(xgi::Input * in, xgi::Output * out ) 
				;

			/*! generate a test exception
			 */
			void inject(xgi::Input * in, xgi::Output * out ) 
				;
			void generate(xgi::Input * in, xgi::Output * out ) 
				;
				
			/*! Retrieve list of spotlight database files in the configured path */
			/*
			void files(xgi::Input * in, xgi::Output * out ) 
				;
			*/
				
			/*! Retrieve list of open databases */
			void databases(xgi::Input * in, xgi::Output * out ) 
				;
				
			/*! Attach to an additional database file in the configured directory
				parameters 'dbname' and 'filename' 
			*/
			void attach(xgi::Input * in, xgi::Output * out ) 
				;
				
			/*! Detach from an attached database
				parameter 'dbname'
			*/
			void detach(xgi::Input * in, xgi::Output * out ) 
				;
				
			//
			// B2IN interface
			//
		        void onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) ;

			void asynchronousExceptionNotification(xcept::Exception& e);

			void rearm(xgi::Input * in, xgi::Output * out ) ;

			void statisticsTabPage(xgi::Input * in, xgi::Output * out) ;

			protected:
			
                       void refreshSubscriptionsToEventing() ;

			void loadJEL(const std::string & fname);
			void loadOCL(const std::string & fname);

			bool applyJEL( xcept::Exception & e);
			bool match ( xcept::Exception & e, std::map<std::string, std::string>& filter);
			std::string mapToAttributeName(const std::string & name);

			void mainPage ( xgi::Input * in, xgi::Output * out) ;

			void HotspotTabPage(xgi::Output * out) ;
			void JELTabPage(xgi::Output * out) ;

			
			private:
			
			toolbox::task::AsynchronousEventDispatcher dispatcher_;
			Repository* repository_;							
			xdata::UnsignedInteger64T exceptionsLostCounter_;
			xdata::String topic_;

			xdata::String subscribeGroup_; // one or more comma separated groups hosting a ws-eventing service for exceptions
			xdata::String jelFileName_; // xml file containing junk exception filter rules 
			xdata::String oclFileName_;
			xdata::Boolean dataInputSubscription_; // enable/disable subscription to b2in eventing

			// Connection details for Oracle database
			xdata::String databaseUser_;
			xdata::String databasePassword_;
			xdata::String databaseTnsName_;

			xdata::UnsignedInteger scatterReadNum_;

 			b2in::utils::ServiceProxy* b2inEventingProxy_;
                        xdata::String  scanPeriod_;
                        xdata::String  subscribeExpiration_;

			std::map<std::string, xdata::Properties> subscriptions_; // indexed by topic

			std::list<xdata::Properties> jel_;
			std::map<std::string,std::map<std::string,size_t> > jec_;
		};
	}
}
#endif
