/*
 * i2oXFunctionCodes.h
 */

#ifndef _i2oXFunctionCodes_h_
#define _i2oXFunctionCodes_h_


/////////////////////////////////////////////////////////////
// External EVB I2O function codes                         //
/////////////////////////////////////////////////////////////

const U8 I2O_BU_ALLOCATE      = 0x01;
const U8 I2O_BU_COLLECT       = 0x02;
const U8 I2O_BU_DISCARD       = 0x03;
const U8 I2O_EVM_TRIGGER      = 0x04;
const U8 I2O_EVM_LUMISECTION  = 0x11;
const U8 I2O_FU_TAKE          = 0x05;
const U8 I2O_RU_DATA_READY    = 0x06;
const U8 I2O_EVMRU_DATA_READY = 0x06; // New name
const U8 I2O_TA_CREDIT        = 0x07;


/////////////////////////////////////////////////////////////
// Internal EVB I2O function codes                         //
/////////////////////////////////////////////////////////////

const U8 I2O_EVM_ALLOCATE_CLEAR = 0x08;
const U8 I2O_BU_CONFIRM         = 0x09;
const U8 I2O_RU_READOUT         = 0x0a;
const U8 I2O_RU_SEND            = 0x0b;
const U8 I2O_BU_CACHE           = 0x0c;


/////////////////////////////////////////////////////////////
// External DFC I2O function codes                         //
/////////////////////////////////////////////////////////////

const U8 I2O_RUI_CREDIT       = 0x0d;


/////////////////////////////////////////////////////////////
// Interinal DFC I2O function codes                        //
/////////////////////////////////////////////////////////////

const U8 I2O_DFC_HIGH    = 0x0e;
const U8 I2O_DFC_LOW     = 0x0f;
const U8 I2O_DFC_DISABLE = 0x10;


/////////////////////////////////////////////////////////////
// Filter farm I2O function codes                          //
/////////////////////////////////////////////////////////////

// Filter farm reserves 0x1a to 0x20 //


#endif
