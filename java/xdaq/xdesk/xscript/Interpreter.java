package xdaq.xdesk.xscript;

import java.io.*;
import java.awt.event.*;

public interface Interpreter
{
	// Evaluate a script
	public void eval (String script);
	
	// interrupt corrent eval processing
	public void interrupt ();
		
	// Start the interpreter in a separate thread
	public void start ();
	
	// Stop an interpreter thread
	public void stop ();
	
	// Set a callback listener. 
	// Called for the interrupt, stop and finish of eval functions
	public void setActionListener (ActionListener listener);
	
	// Pass a stream object to which all output is redirected
	// By default System.out
	public void setOutputStream (java.io.InputStream out);
	
	// Pass a stream object from which all input is processed
	// By default System.in
	public void setInputStream (java.io.OutputStream in);
	
	// Return the currently set input stream object
	public java.io.InputStream getInputStream();
	
	// Return the currently set output stream object
	public java.io.OutputStream getOutputStream();
}
