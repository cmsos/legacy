from sqlalchemy import *
import utils

db = create_engine('sqlite:///testiov.db')
metadata = MetaData(db)

iovtags = Table('IOVTAGS', metadata,
                Column('TAGID', BIGINT, primary_key=True),
                Column('TAGNAME', TEXT() ),
                Column('CREATIONUTC', TEXT(), ),
                Column('DATADICT', TEXT() ),
                Column('IOVDATAVERSION', TEXT(),default='1.0.0'),
                Column('DATASOURCE', TEXT() ),
                Column('APPLYTO', TEXT() ),
                Column('ISDEFAULT', SMALLINT,default=0 ),
                Column('COMMENT', TEXT(),default=u'')
)
tagdata = Table('IOVTAGDATA', metadata,
                Column('TAGID', BIGINT, primary_key=True),
                Column('SINCE', INT , primary_key=True),
                Column('PAYLOADID', BIGINT, )                
)

iovp_uint8 = Table('IOVP_UINT8', metadata,
                Column('PAYLOADID', BIGINT, primary_key=True),
                Column('IFIELD', INT, primary_key=True),
                Column('IITEM', INT, primary_key=True),
                Column('IPOS', INT, primary_key=True),
                Column('VAL', SMALLINT, ),
)

iovp_string = Table('IOVP_STRING', metadata,
                Column('PAYLOADID', BIGINT, primary_key=True),
                Column('IFIELD', INT, primary_key=True),
                Column('IITEM', INT, primary_key=True),
                Column('IPOS', INT, primary_key=True),
                Column('VAL', TEXT(), ),
)

connection = db.connect()

# I'm bored. A different way. Use pandas and iterator

import pandas as pd

tagname = 'bcm1fchannelmask_v1'
s = select([iovtags.c.TAGID])
print "get tagid of %s"%tagname
print "query string:\n%s "%s
results = (connection.execution_options(stream_results=True).execute(s))#
dataframe = pd.DataFrame(iter(results)) #The ResultProxy can be used as an iterator to avoid fetching data
# do something with dataframe here blah blah
dataframe.columns = results.keys()
results.close()
print dataframe


# add string folder in the mix
import stringfolder as sf
s = select([iovtags.c.TAGID,iovtags.c.DATADICT])
results = (connection.execution_options(stream_results=True).execute(s))
dataframe = pd.DataFrame(sf.string_folding_wrapper(results))
dataframe.columns = results.keys()
print dataframe
results.close()
datadictStr = dataframe['DATADICT'][0]
print 'data dictionary %s'%datadictStr

print "get since-payloadid map"
s = select([tagdata.c.SINCE,tagdata.c.PAYLOADID]).where(
    (iovtags.c.TAGID == tagdata.c.TAGID)
    &
    (iovtags.c.TAGNAME == bindparam('t'))
)
print "query string:\n%s "%s
results = (connection.execution_options(stream_results=True).execute(s,t=tagname))
dataframe = pd.DataFrame(sf.string_folding_wrapper(results))
dataframe.columns = results.keys()
results.close()
print dataframe

payloadid_1 = dataframe['PAYLOADID'][0]
print 'payloadid_1 ',payloadid_1
datadict = utils.parsedatadict(datadictStr)
payloadtables = {}
payloadtables['IOVP_UINT8'] = iovp_uint8
payloadtables['IOVP_STRING'] = iovp_string

fielddfs = []
for fielddict in datadict:
    ptablename = utils.getPtablename(fielddict[0])
    print ptablename
    varlen = fielddict[1]
    alias = fielddict[2] 
    print payloadid_1,ptablename, varlen, alias
    tb = payloadtables[ptablename]
    print ptablename
    print tb.c
    s = select([tb.c.IITEM,tb.c.IFIELD,tb.c.IPOS,tb.c.VAL]).where(
        (tb.c.PAYLOADID == bindparam('pid'))
    )
    print s
    print payloadid_1
    results = (connection.execution_options(stream_results=True).execute(s,pid=payloadid_1))
    df = pd.DataFrame(sf.string_folding_wrapper(results))
    df.columns = results.keys()
    print df
    fielddfs.append( df )

#concatenated = pd.concat(fielddfs)
#print concatenated
connection.close()
db.dispose()
