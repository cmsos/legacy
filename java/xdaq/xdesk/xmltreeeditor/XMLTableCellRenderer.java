package xmltreeeditor;

import java.util.*;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import javax.swing.border.*;

public class XMLTableCellRenderer
    extends DefaultTableCellRenderer {
  protected static Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

  public void setValue(Object value) {
    this.setBorder(noFocusBorder);
    this.setFont(new Font("Arial", Font.BOLD, 11));

    if (value instanceof ImageIcon) {

      setIcon( (ImageIcon) value);

    }
    else if (value instanceof IconWithText) {
      IconWithText tmpValue = (IconWithText) value;
      setIcon(tmpValue.getIcon());
      setText(tmpValue.toString());
    }
    else {
      setText(value.toString());
    }

    if (value instanceof EditableText) {
      this.setForeground(Color.blue);
    }
    else {
      this.setForeground(Color.black);
    }

  }

}
