#ifndef _bril_timesource_exception_TimeSourceAlarm_h_
#define _bril_timesource_exception_TimeSourceAlarm_h_
#include "xcept/Exception.h"
#include "sentinel/exception/Exception.h"
namespace bril { 
  namespace timesource {
    namespace exception {
      class TimeSourceAlarm : public sentinel::exception::Exception{
      public: 
      TimeSourceAlarm( std::string name, std::string message, std::string module, int line, std::string function ):sentinel::exception::Exception(name, message, module, line, function) {} 
      TimeSourceAlarm( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e ):sentinel::exception::Exception(name, message, module, line, function, e) {} 
      }; 
    }//ns exception
  }//ns timesource
}//ns bril
#endif
