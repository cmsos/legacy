/*
 * XDAQDocument
 *
 * $Id: XDAQDocument.java,v 4.1 2007/03/26 14:40:24 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import java.io.*;
import java.util.Vector;

// DOM
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.w3c.dom.*;
import org.w3c.dom.Node;  // to solve ambiguity with soap.Node

// XML transformer
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

// Remaining exceptions
import java.io.IOException;

/**
 * XDAQ XML configuration document class
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XDAQDocument
{
	protected class CommandResponse
	{
		public String endPoint;
		public String application;
		public int instance;

		public Document reply;
		public String fault;
	}

	private Document domDocument;
	private DefaultNamespaceContext nsContext;

	private int timeout = 0;

	private final static String XDAQ_NS =
			"http://xdaq.web.cern.ch/xdaq/xsd/2004/XMLConfiguration-30";

	private final String CONTEXT_PATH = "//p:Context";
	private final String APPLICATION_PATH = "/p:Application[@id>=10]";

	private Transformer transformer;

	/**
	 * Default constructor, callable only from other constructors
	 */
	protected XDAQDocument()
	{
		// prepare namespace context for XPath queries
		nsContext = new DefaultNamespaceContext();
		nsContext.put("p", XDAQ_NS);

		// Create a transformer
		TransformerFactory tf = TransformerFactory.newInstance();
		try {
			transformer = tf.newTransformer();
		} catch (TransformerConfigurationException e) {
			throw new XDAQRuntimeException(e);
		}
	}

	/**
	 * Constructor from an XML file
	 *
	 * @param filename Name of a XDAQ configuration XML file
	 */
	public XDAQDocument(String filename)
			throws XDAQException
	{
		this();

		FileReader reader;
		try {
			reader = new FileReader(filename);
		} catch (FileNotFoundException e) {
			throw new XDAQException(
					"The XML file, " + filename + " is not found.", e);
		}

		initialize(reader);
	}

	/**
	 * Constructor from an XML string
	 *
	 * @param  stringReader
	 *     StringReader created from the XML configuration string
	 */
	public XDAQDocument(StringReader stringReader)
			throws XDAQException
	{
		this();

		initialize(stringReader);
	}

	/**
	 * Copy constructor
	 *
	 * @param  original Original XDAQDocument instance
	 */
	public XDAQDocument(XDAQDocument original)
			throws XDAQException
	{
		this();

		// Create an instance of Result for a DOM tree
		DOMSource domSource = new DOMSource(original.domDocument);
		DOMResult domResult = new DOMResult();

		// Transform !!
		try {
			transformer.transform(domSource, domResult);
		} catch (TransformerException e) {
			throw new XDAQException("Failed to copy.", e);
		}

		domDocument = (Document)domResult.getNode();
		domDocument.normalize();
	}

	/**
	 * The real content of constructors
	 *
	 * @param input InputSource giving an XDAQ configuration XML string
	 */
	private void initialize(Reader input)
			throws XDAQException
	{
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			DocumentBuilder db = dbf.newDocumentBuilder();
			domDocument = db.parse(new InputSource(input));
			domDocument.normalize();
		} catch (IOException e) {
			throw new XDAQException(e);
		} catch (SAXException e) {
			throw new XDAQException(e);
		} catch (ParserConfigurationException e) { // from newDocumentBuilder()
			throw new XDAQRuntimeException(e);
		}
	}

	/**
	 * Expose the internal DOM document
	 *
	 * @return An org.w3c.dom.Document instance used internally
	 */
	public Document getDOMDocument()
	{
		return domDocument;
	}

	/**
	 * Assign numerical local IDs to 'auto' directives.
	 *
	 * If 'id' attribute is 'auto' in a xc:Application element,
	 * finding out current max number used in the Context and
	 * assign incremental numbers to 'auto' Applications.
	 */
	public void assignLocalID()
			throws XDAQException
	{
		// Loop over all contexts and applications,
		//   index i for context and j for application
		//
		NodeList contextList;
		NodeList applicationList;

		// Loop over all contexts
		try {
			contextList = XPathAPI.selectNodeList(
					domDocument, CONTEXT_PATH, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find all the contexts.", e);
		}

		for (int i = 0; i < contextList.getLength(); ++i) {
			Node context = contextList.item(i);

			int idCounter = 9;  // use IDs 10 and larger

			// 1st path, check max of the existing IDs
			try {
				applicationList = XPathAPI.selectNodeList(
						context, "p:Application[@id!='auto']", nsContext);
			} catch (XPathAPIException e) {
				throw new XDAQException(
						"Failed to find contexts with 'non-auto' id.", e);
			}

			for (int j = 0; j < applicationList.getLength(); ++j) {
				Node application = applicationList.item(j);

				int id = Integer.parseInt(
						((Element)application).getAttribute("id"));
				if (id > idCounter) { idCounter = id; }
			}

			// 2nd path, assign IDs to 'auto' applications
			try {
				applicationList = XPathAPI.selectNodeList(
						context, "p:Application[@id='auto']", nsContext);
			} catch (XPathAPIException e) {
				throw new XDAQException(
						"Failed to find contexts with 'auto' id.", e);
			}

			for (int j = 0; j < applicationList.getLength(); ++j) {
				Node application = applicationList.item(j);

				++idCounter;
				((Element)application).getAttributeNode("id")
						.setValue(Integer.toString(idCounter));
			}
		}
	}

	/**
	 * Configure all the host in the XML configuration
	 *
	 * @return Summary string of configuration result.
	 *     The format is 'ConfigureHost: <endpoint>: <result>\n',
	 *     for each XDAQ context (executive).
	 *     The <result> string is just 'Success' when no <Fault> is
	 *     reported.  When <Fault> happens, <result> string is
	 *     the text of the <faultsting> element.
	 */
	public String configureHost()
			throws XDAQException
	{
		XDAQMessage message;
		try {
			message = new XDAQMessage(
					"Configure", domDocument.getDocumentElement());
		} catch (XDAQMessageException e) {
			throw new XDAQException("Failed to find contexts.", e);
		}
		message.setTimeout(timeout);

		NodeList contextList;
		try {
			contextList = XPathAPI.selectNodeList(
					domDocument, CONTEXT_PATH, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find contexts.", e);
		}

		StringBuffer result = new StringBuffer();

		for (int i = 0; i < contextList.getLength(); ++i) {
			Element context = (Element)contextList.item(i);

			String endPoint = context.getAttribute("url");

			result.append("ConfigureHost: " + endPoint + ": ");
			try {
				message.send(endPoint, 0);
				result.append(getSimpleString(message));
			} catch (XDAQMessageException e) {
				throw new XDAQException(
						"Configure an executive at " + endPoint, e);
			}
			result.append("\n");
		}

		return result.toString();
	}

	/**
	 * Send an XDAQ SOAP command and return a summary result sting.
	 *
	 * Send XDAQ SOAP command to applications.<br/>
	 *
	 * There are 6 similar methods, from combinations of
	 * three types of arguments and two types of return value.<br/>
	 *
	 * For return value,
	 * getResponseString(*) methods sends a command to multiple 
	 * applications and returns summary result as a string.
	 * The getResponseDOM(*) methods sends a command to a single
	 * applications and returns a full DOM structure returned
	 * from the application.<br/>
	 *
	 * There are three ways to select applications to command.
	 * getResponse*(command) selects all application in the configuration.
	 * getResponse*(command, applicationClass) selects only applications of 
	 * <applicationClass> class.
	 * getResponse*(command, applicationClass, instance) selects an application
	 * of the <applicationClass> class and <instance> instance.
	 * For all cases, getResponseDOM(*) pick the first application
	 * when multiple applications are selected.
	 *
	 * @param  command Command string.
	 *    In case of simple command, this is the element tag name
	 *    (e.g. "Configure").
	 *    In case of structured command, this is the full XML string
	 *    (e.g. <pre>
	 *    "&lt;xml version="1.0" encoding="UTF-8"?>
	 *     &lt;xdaq:Command xmlns:xdaq="urn:xdaq-soap:3.0">
	 *       &lt;Name>counter&lt;Name>
	 *       &lt;Value>9&lt;Value>
	 *     &lt;xdaq:Command>"</pre>)
	 *
	 * @return Summary result string of the command
	 *     The format is '<command>: <endpoint>: <result>\n',
	 *     one line for each application.
	 *     See 'Returns' of configureHost().
	 *
	 * @see    #configureHost()
	 */
	public String getResponseString(String command)
			throws XDAQException
	{
		return getResponseString(command, getApplications());
	}

	/**
	 * Send an XDAQ SOAP command and return a summary result sting.
	 *
	 * @param  command Command string.
	 * @param  applicationClass A class name of applications
	 * @return Summary result string of the command
	 *
	 * @see    #getResponseString(String command)
	 */
	public String getResponseString(String command, String applicationClass)
			throws XDAQException
	{
		return getResponseString(command, getApplications(applicationClass));
	}

	/**
	 * Send an XDAQ SOAP command and return a summary result sting.
	 *
	 * @param  command Command string.
	 * @param  applicationClass A class name of the target application
	 * @param  instance Instance number of the target application
	 * @return Summary result string of the command
	 *
	 * @see    #getResponseString(String command)
	 */
	public String getResponseString(
			String command, String applicationClass, int instance)
			throws XDAQException
	{
		return getResponseString(
				command, getApplications(applicationClass, instance));
	}

	/**
	 * Send an XDAQ SOAP command and return a summary result sting.
	 */
	private String getResponseString(String command, NodeList applicationList)
			throws XDAQException
	{
		Vector<CommandResponse> responseList =
				sendCommand(command, applicationList);

		StringBuffer result = new StringBuffer();

		String commandString;
		if (command.indexOf("<") == -1) {  // Simple command
			commandString = command;
		} else {                           // Structured command
			commandString = "Structured command";
		}

		for (CommandResponse response: responseList) {
			result.append(commandString + ": " +
					response.endPoint + ": " + 
					response.application + ": " + 
					response.instance + ": " + 
					(response.fault == null ? "Success" : response.fault) +
					"\n");
		}

		return result.toString();
	}

	/**
	 * Send an XDAQ SOAP command and return a result DOM.
	 *
	 * @param  command Command string.
	 * @return DOM Document returned by an application
	 *
	 * @see    #getResponseString(String command)
	 */
	public Document getResponseDOM(String command)
			throws XDAQException
	{
		return getResponseDOM(command, getApplications());
	}

	/**
	 * Send an XDAQ SOAP command and return a result DOM.
	 *
	 * @param  command Command string.
	 * @param  applicationClass A class name of applications
	 * @return DOM Document returned by an application
	 *
	 * @see    #getResponseString(String command)
	 */
	public Document getResponseDOM(String command, String applicationClass)
			throws XDAQException
	{
		return getResponseDOM(command, getApplications(applicationClass));
	}

	/**
	 * Send an XDAQ SOAP command and return a result DOM.
	 *
	 * @param  command Command string.
	 * @param  applicationClass A class name of the target application
	 * @param  instance Instance number of the target application
	 * @return DOM Document returned by an application
	 *
	 * @see    #getResponseString(String command)
	 */
	public Document getResponseDOM(
			String command, String applicationClass, int instance)
			throws XDAQException
	{
		return getResponseDOM(
				command, getApplications(applicationClass, instance));
	}

	/**
	 * Send command
	 */
	private Document getResponseDOM(String command, NodeList applicationList)
			throws XDAQException
	{
		Vector<CommandResponse> responseList =
				sendCommand(command, applicationList, true);

		return responseList.firstElement().reply;
	}

	/**
	 * Get a list of applications
	 *
	 * @return node list of all the applications
	 */
	protected NodeList getApplications()
			throws XDAQException
	{
		NodeList applications;
		try {
			applications = XPathAPI.selectNodeList(
					domDocument, CONTEXT_PATH + APPLICATION_PATH, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find applications.", e);
		}

		return applications;
	}

	/**
	 * Get a list of applications
	 *
	 * @param  applicationClass A class name of the target application
	 * @return node list containing applications of a class.
	 */
	protected NodeList getApplications(String applicationClass)
			throws XDAQException
	{
		NodeList applications;
		try {
			applications = XPathAPI.selectNodeList(domDocument,
					CONTEXT_PATH + APPLICATION_PATH +
					"[@class='" + applicationClass + "']",
					nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find applications (" +
					applicationClass + ").", e);
		}

		return applications;
	}

	/**
	 * Get a list of applications
	 *
	 * @param  applicationClass A class name of the target application
	 * @param  instance Instance number of the target application
	 * @return node list containing an application with specific class/instance
	 */
	protected NodeList getApplications(String applicationClass, int instance)
			throws XDAQException
	{
		NodeList applications;
		try {
			applications = XPathAPI.selectNodeList(domDocument,
					CONTEXT_PATH + APPLICATION_PATH +
					"[@class='" + applicationClass + "']" +
					"[@instance='" + instance + "']",
					nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find an application (" +
					applicationClass + "," + instance + ").", e);
		}

		return applications;
	}

	/**
	 * Send an XDAQ SOAP command and return a summary result sting.
	 */
	protected Vector<CommandResponse> sendCommand(
			String command, NodeList applicationList, boolean firstOnly)
			throws XDAQException
	{
		Vector<CommandResponse> result = new Vector<CommandResponse>();

		XDAQMessage message = createCommandMessage(command);

		int length = firstOnly ? 1 : applicationList.getLength();

		for (int i = 0; i < length; ++i) {
			Element application = (Element)applicationList.item(i);
			Element context = (Element)application.getParentNode();
			String endPoint = context.getAttribute("url");
			String klass = application.getAttribute("class");
			int instance =
					Integer.parseInt(application.getAttribute("instance"));

			Document reply;
			try {
				reply = message.send(endPoint, klass, instance);
			} catch (XDAQMessageException e) {
				throw new XDAQException(
						endPoint + " " + klass + " " + instance, e);
			}

			CommandResponse commandResponse = new CommandResponse();
			commandResponse.endPoint = endPoint;
			commandResponse.application = klass;
			commandResponse.instance = instance;
			commandResponse.reply = reply;
			commandResponse.fault = message.getFaultString();

			result.add(commandResponse);
		}

		return result;
	}

	protected Vector<CommandResponse> sendCommand(
			String command, NodeList applicationList)
			throws XDAQException
	{
		return sendCommand(command, applicationList, false);
	}

	/**
	 * create XDAQMessage for a command
	 *
	 * @param  command Command string.
	 * @return XDAQMessage instance
	 */
	private XDAQMessage createCommandMessage(String command)
			throws XDAQException
	{
		XDAQMessage message;

		try {
			if (command.indexOf("<") == -1) {  // Simple command
				message = new XDAQMessage(command);
			} else {                           // Structured command
				// Create an instance of Result for a DOM tree
				StreamSource streamSource = new StreamSource(
						new StringReader(command));
				DOMResult domResult = new DOMResult();

				// Transform !!
				transformer.transform(streamSource, domResult);

				message = new XDAQMessage(
						((Document)domResult.getNode()).getDocumentElement());
			}
		} catch (TransformerException e) {
			throw new XDAQException(
					"Failed to convert a command string to DOM: " + command, e);
		} catch (XDAQMessageException e) {
			throw new XDAQException(
					"Failed to create a message: " + command, e);
		}
		message.setTimeout(timeout);

		return message;
	}

	/**
	 * Get a list of XDAQ executive contexts.
	 *
	 * @return A String vector of context's endpoint URLs
	 */
	public Vector<String> getContextList()
			throws XDAQException
	{
		Vector<String> list = new Vector<String>();

		NodeList contextList;

		try {
			contextList = XPathAPI.selectNodeList(
					domDocument, CONTEXT_PATH, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find all the contexts.", e);
		}

		for (int i = 0; i < contextList.getLength(); ++i) {
			Element context = (Element)contextList.item(i);

			String url = context.getAttribute("url");

			if (!list.contains(url)) {
				list.add(url);
			}
		}

		return list;
	}

	/**
	 * Get a list of XDAQ application classes
	 *
	 * @return A String vector of application class names
	 */
	public Vector<String> getApplicationList()
			throws XDAQException
	{
		Vector<String> list = new Vector<String>();

		NodeList applicationList;
		
		try {
			applicationList = XPathAPI.selectNodeList(
					domDocument, CONTEXT_PATH + APPLICATION_PATH, nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find all the applications.", e);
		}

		for (int i = 0; i < applicationList.getLength(); ++i) {
			Element application = (Element)applicationList.item(i);

			String applicationClass = application.getAttribute("class");

			if (!list.contains(applicationClass)) {
				list.add(applicationClass);
			}
		}

		return list;
	}

	/**
	 * Get a list of XDAQ application classes for a given context
	 *
	 * @param  context A context's endpoint URL
	 * @return A String vector of application class names
	 */
	public Vector<String> getApplicationList(String context)
			throws XDAQException
	{
		Vector<String> list = new Vector<String>();

		NodeList applicationList;
		
		try {
			applicationList = XPathAPI.selectNodeList(domDocument,
					CONTEXT_PATH + "[@url='" + context + "']" +
					APPLICATION_PATH,
					nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException(
					"Failed to find the applications in " + context + ".", e);
		}

		for (int i = 0; i < applicationList.getLength(); ++i) {
			Element application = (Element)applicationList.item(i);

			String applicationClass = application.getAttribute("class");

			if (!list.contains(applicationClass)) {
				list.add(applicationClass);
			}
		}

		return list;
	}

	/**
	 * Get a list of instance numbers for a XDAQ application class
	 *
	 * @param  applicationClass An application class name
	 * @return An Integer vector of application instance #s
	 */
	public Vector<Integer> getInstanceList(String applicationClass)
			throws XDAQException
	{
		Vector<Integer> list = new Vector<Integer>();

		NodeList applicationList;
		
		try {
			applicationList = XPathAPI.selectNodeList(domDocument,
					CONTEXT_PATH +
					APPLICATION_PATH + "[@class='" + applicationClass + "']",
					nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find the applications, " +
					applicationClass + ".", e);
		}

		for (int i = 0; i < applicationList.getLength(); ++i) {
			Element application = (Element)applicationList.item(i);

			list.add(new Integer(application.getAttribute("instance")));
		}

		return list;
	}

	/**
	 * Get a list of instance numbers for a XDAQ application class
	 * for a given context
	 *
	 * @param  context A context's endpoint URL
	 * @param  applicationClass An application class name
	 * @return An Integer vector of application instance #s
	 */
	public Vector<Integer> getInstanceList(
			String context, String applicationClass)
			throws XDAQException
	{
		Vector<Integer> list = new Vector<Integer>();

		NodeList applicationList;
		
		try {
			applicationList = XPathAPI.selectNodeList(domDocument,
					CONTEXT_PATH + "[@url='" + context + "']" +
					APPLICATION_PATH + "[@class='" + applicationClass + "']",
					nsContext);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find the instances in " +
					context + " " + applicationClass + ".", e);
		}

		for (int i = 0; i < applicationList.getLength(); ++i) {
			Element application = (Element)applicationList.item(i);

			list.add(new Integer(application.getAttribute("instance")));
		}

		return list;
	}

	/**
	 * Create a XDAQParameter
	 * for a specific application class and an instance #.
	 *
	 * @param  applicationClass An application class name
	 * @param  instance An instance # of the application
	 * @return XDAQParameter instance
	 *
	 * @see    XDAQParameter
	 */
	public XDAQParameter getParameter(String applicationClass, int instance)
			throws XDAQException
	{
		String endPoint;
		
		try {
			endPoint = getEndPoint(applicationClass, instance);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find the endpoint of " +
					applicationClass + " " + instance + ".", e);
		}

		if (endPoint == null) { return null; }

		return new XDAQParameter(endPoint, applicationClass, instance);
	}

	/**
	 * Create a XDAQParameter from XML string
	 * for a specific application class and an instance #.
	 *
	 * @param  applicationClass An application class name
	 * @param  instance An instance # of the application
	 * @param  xmlString Parameter XML string.
	 *    (e.g.<pre>
	 *    &lt;Foo:properties xmlns:Foo="urn:xdaq-application:Foo"
	 *        xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding"
	 *        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	 *        xsi:type="soapenc:Struct"
	 *      &lt;Foo:line xsi:type="xsd:string">foo&lt;Foo:line>
	 *      &lt;Foo:number xsi:type="xsd:unsignedLong">99&lt;Foo:number>
	 *    &lt;Foo:properties></pre>)
	 * @return XDAQParameter instance
	 *
	 * @see    XDAQParameter
	 */
	public XDAQParameter getParameter(
			String applicationClass, int instance, String xmlString)
			throws XDAQException
	{
		String endPoint;
		
		try {
			endPoint = getEndPoint(applicationClass, instance);
		} catch (XPathAPIException e) {
			throw new XDAQException("Failed to find the endpoint of " +
					applicationClass + " " + instance + ".", e);
		}

		if (endPoint == null) { return null; }

		return new XDAQParameter(
				endPoint, applicationClass, instance, xmlString);
	}

	/**
	 * returns a context EndPoint
	 */
	private String getEndPoint(String applicationClass, int instance)
			throws XPathAPIException
	{
		Element application = (Element)XPathAPI.selectSingleNode(domDocument,
				CONTEXT_PATH + APPLICATION_PATH +
				"[@class='" + applicationClass + "']" +
				"[@instance='" + instance + "']",
				nsContext);

		if (application == null) { return null; }

		return ((Element)application.getParentNode()).getAttribute("url");
	}

	/**
	 * Interpret the reply DOM
	 */
	private String getSimpleString(XDAQMessage message)
	{
		String result = message.getFaultString();

		if (result == null) { result = "Success"; }

		return result;
	}

	/**
	 * Get timeout.
	 *
	 * @return timeout value in milliseconds
	 */
	public int getTimeout()
	{
		return timeout;
	}

	/**
	 * Set timeout.
	 * The value set is used in all subsequent commands.
	 *
	 * @param  aTimeout timeout value set, in milliseconds
	 */
	public void setTimeout(int aTimeout)
	{
		timeout = aTimeout;
	}

	/**
	 * Shut down thread pools used in XDAQMessage for time out management.
	 */
	public static void shutdownThreadPool() throws InterruptedException
	{
		XDAQMessage.shutdownThreadPool();
	}
}

// End of file
// vim: set ts=4 sw=4:
