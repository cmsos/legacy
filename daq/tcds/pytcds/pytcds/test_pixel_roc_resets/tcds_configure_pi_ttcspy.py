#!/usr/bin/env python

###############################################################################
## A little script to configure the TTCSpy-in-the-PI for the pixel ROC
## reset tests.
###############################################################################

import sys

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Configurator(CmdLineBase):

    def main(self):

        # Check if the target is valid.
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        # Connect.
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        pi = get_pi_hw(network_address, controlhub_address, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        #----------

        # Configure.
        # - General TTCSpy settings.
        # NOTE: The CPM emits the BC0 in BX 3540, but the pixels delay
        # their BC0 by 23 ticks.
        pi.write('pi.ttcspy.logging_control.bc0_val', 0x01)
        pi.write('pi.ttcspy.logging_control.expected_bc0_bx', 3540 + 23)
        pi.write('pi.ttcspy.logging_control.logging_buffer_mode', 0x0)
        pi.write('pi.ttcspy.logging_control.logging_mode', 0x0)
        pi.write('pi.ttcspy.logging_control.trigger_combination_operator', 0x0)
        # - TTCSpy trigger mask.
        pi.write('pi.ttcspy.trigger_mask.add_all', 0x0)
        pi.write('pi.ttcspy.trigger_mask.adr_zero_data', 0x0)
        pi.write('pi.ttcspy.trigger_mask.brc_all', 0x0)
        pi.write('pi.ttcspy.trigger_mask.brc_bc0', 0x0)
        pi.write('pi.ttcspy.trigger_mask.brc_dddd', 0x0)
        pi.write('pi.ttcspy.trigger_mask.brc_dddd_all', 0x0)
        pi.write('pi.ttcspy.trigger_mask.brc_ec0', 0x0)
        pi.write('pi.ttcspy.trigger_mask.brc_tt', 0x0)
        pi.write('pi.ttcspy.trigger_mask.brc_tt_all', 0x0)
        # NOTE: The pixels emit B-command 0x1c as ROC reset upon B-go
        # 14. In the lab we use 0xe.
        pi.write('pi.ttcspy.trigger_mask.brc_val0', 0x1c)
        # pi.write('pi.ttcspy.trigger_mask.brc_val0', 0xe)
        pi.write('pi.ttcspy.trigger_mask.brc_val1', 0x00)
        pi.write('pi.ttcspy.trigger_mask.brc_val2', 0x00)
        pi.write('pi.ttcspy.trigger_mask.brc_val3', 0x00)
        pi.write('pi.ttcspy.trigger_mask.brc_val4', 0x00)
        pi.write('pi.ttcspy.trigger_mask.brc_val5', 0x00)
        pi.write('pi.ttcspy.trigger_mask.brc_zero_data', 0x0)
        pi.write('pi.ttcspy.trigger_mask.err_com', 0x0)
        pi.write('pi.ttcspy.trigger_mask.l1a', 0x1)

        #----------

        # End of main().

    # End of class Configurator.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to configure the TTCSpy-in-the-PI " \
               "for the pixel ROC reset tests."

    res = Configurator(desc_str).run()

    print "Done"

    sys.exit(res)

###############################################################################
