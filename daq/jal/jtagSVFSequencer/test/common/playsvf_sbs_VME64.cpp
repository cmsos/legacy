#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagChain/JTAGScanData.h"
#include "jal/jtagController/DTTFJTAGController.h"
#include "jal/jtagController/ScanPSC100JTAGController.h"
#include "jal/jtagController/HALScanPSC100Adapter.h"
#include "jal/jtagSVFSequencer/JTAGSVFSequencer.h"


#include "hal/VME64xCrate.hh"
#include "hal/VMEConfigurationSpaceHandler.hh"
#include "hal/VMEConfigurationSpaceAddressReader.hh"
#include "hal/ModuleMapperInterface.hh" 
#include "hal/AddressTableContainerInterface.hh" 
#include "hal/ASCIIFileModuleMapper.hh"
#include "hal/ASCIIFileAddressTableContainer.hh"
#include "hal/StaticVMEConfigurationASCIIFileReader.hh"
#include "hal/VME64xDummyBusAdapter.hh"

#include "hal/VMEDevice.hh"
#include "hal/VMEAddressTable.hh"
#include "hal/VMEAddressTableASCIIReader.hh"
#include "hal/SBS620x86LinuxBusAdapter.hh"
#include "hal/VMEDummyBusAdapter.hh"
#include "hal/linux/StopWatch.hh"

#include <iostream>
#include <iomanip>

#include <ctype.h>
#include <string>


#define CRATE_MAPFILE "VME64xCrateConfigMap.dat"
#define MODULE_MAP_FILE "ModuleMapperFile.dat"
#define ADDERSSTABLE_CONTAINER_FILE "AddressTableMap.dat"
#define STATIC_CONFIGURATION_FILE "StaticConfiguration.dat"

using namespace std;
using namespace HAL;
using namespace jal;



int main(int argc, char **argv) {
  
  // say hello
  cout << endl;
  cout << "==============================================" << endl;
  cout << "This is playsvf_sbs_VME64  ($Revision: 1.1 $)" << endl;
  cout << "==============================================" << endl;
  cout << "A utility to play SVF files to a JTAG chain via HAL and VME (SBS Interface)." << endl;
  cout << "Customized version for Global Trigger Carte and FDL Board of HEPHY Vienna." << endl;
  cout << "Use with care. Incorrect use may damage the hardware. No explicit or implied warranty." << endl;
  cout << endl;

  // the default parameters
  bool check_only = false;
  bool debug_mode = false;
  bool verbose_mode = false;
  bool override_errors = false;
  double SCK_frequency = 10.e6;
  int chain_nr = 0;

  // display help
  if (argc==1) {
    cout << "Usage: " << argv[0] << " [-j=n] [-C] [-D] [-V] [-O] filename.svf" << endl;
    cout << "     -j=n            set JTAG chain nr (0 or 1) (default = " << chain_nr << ")" << endl;          
    cout << "     -C              check only. Only read svf file and check syntax, no hardware access." << endl;
    cout << "     -D              debug only. No hardware access. This will use the dummy bus adapter." << endl;
    cout << "     -V              verbose. display lots of debug output." << endl;
    cout << "     -O              override errors. Continue upon compare error. Only advisable in debug mode." << endl;
    cout << endl;
    exit(0);
  }

  // parse options
  for (int i=1; i<argc;i++) {
    if (argv[i][0] == '-') {
      switch ( toupper( argv[i][1] ) ) {
      case 'J' : 
	if (argv[i][2] == '=') {
	  // quick conversion to hex  
	  int success = sscanf(&(argv[i][3])," %d", &chain_nr);
	  if (!success) { cout << "error parsing JTAG chain nr" << endl; exit(-1);}
	  if (chain_nr <0 || chain_nr >3) { cout << "JTAG chain nr out of range" << endl; exit(-1);}
	}
	else { cout << "error parsing chain number" << endl; exit(-1);}
	break;
      case 'C' : check_only = true; break;
      case 'D' : debug_mode = true; break;
      case 'V' : verbose_mode = true; break;
      case 'O' : override_errors = true; break;
      }
    }
  }

  string svf_filename = argv[argc-1];

  // display options
  cout << "The following settings will be used:" << endl << endl;
  cout << "  JATG Chain Nr:          " << chain_nr << endl;
  cout << "  system clock frequency " << SCK_frequency/1.e6 << " MHz" << endl;
  cout << "  Check only:             " << (check_only?"YES":"NO") << endl;
  cout << "  Debug only:             " << (debug_mode?"YES":"NO") << endl;
  cout << "  Verbose:                " << (verbose_mode?"YES":"NO") << endl;
  cout << "  Override Errors:        " << (override_errors?"YES":"NO") << endl;
  cout << endl;
  cout << "  SVF File: " << svf_filename << endl;
  cout << endl;
  cout << endl;

  try {
    StopWatch sw(0);

    /// Load the SVF file
    cout << "loading SVF file " << svf_filename << endl;
    JTAGSVFSequencer seq;
    sw.start();
    seq.loadSVFFile(svf_filename.c_str());
    sw.stop();

    // (an error will cause an exception and terminate the program)
    cout << "SVF file loaded into memory without problems. (time elapsed = " << (double)sw.read()/1.e6 << " sec)" << endl;
    cout << endl;

    if (check_only) {
      cout << "check complete. bye." << endl;
      exit(0);
    }
     
    // warn the user
    if (!debug_mode) {
      cout << "Hardware will now be accessed with the above parameters. " << endl;
      cout << " - Please check the parameters carefully. " << endl;
      cout << " - Make sure that the above frequency is not below the actual frequency used in hardware." << endl;
      string answer;
      do {
	cout << endl << "Are you sure to continue (yes/no)? ";
	cin >> answer;
	if (answer=="no") exit(0);
      } while (answer!="yes");
    
    }
  
    /* code from Barbara for VME64 follows */

    int slotId =10;
    VMEConfigurationSpaceHandler* configHandler;
    VME64xCrate *vmeCrate;
    SBS620x86LinuxBusAdapter* SBSbusAdapter;
    VME64xDummyBusAdapter* DUMMYbusAdapter;
    if (! debug_mode) {   //Chosen controller is SBS 
      cout << "*** FDLBoard::FDLBoard - used controller: SBS" << endl;
      SBSbusAdapter = new SBS620x86LinuxBusAdapter(0);
      configHandler = new VMEConfigurationSpaceHandler(*SBSbusAdapter);
    } 
    else {   //Chosen controller is 64xDUMMY
      cout << "*** FDLBoard::FDLBoard - used controller: DUMMY" << endl;
      DUMMYbusAdapter = new VME64xDummyBusAdapter(CRATE_MAPFILE);
      configHandler = new VMEConfigurationSpaceHandler(*DUMMYbusAdapter);
    }  
    VMEConfigurationSpaceAddressReader* configReader = new VMEConfigurationSpaceAddressReader;
    VMEAddressTable* addressTable = new VMEAddressTable("VME Configuration Space", *configReader);  
    ASCIIFileModuleMapper* moduleMapper = new ASCIIFileModuleMapper(MODULE_MAP_FILE);
    ASCIIFileAddressTableContainer* addressTableContainer = new ASCIIFileAddressTableContainer (*moduleMapper, ADDERSSTABLE_CONTAINER_FILE);
    StaticVMEConfigurationASCIIFileReader* staticConfigFilerReader = new StaticVMEConfigurationASCIIFileReader(STATIC_CONFIGURATION_FILE);
    StaticVMEConfiguration* staticVMEConfiguration = new StaticVMEConfiguration(*staticConfigFilerReader);
    if (! debug_mode)
      vmeCrate = new VME64xCrate(*SBSbusAdapter, *addressTableContainer, *moduleMapper, *staticVMEConfiguration);
    else
      vmeCrate = new VME64xCrate(*DUMMYbusAdapter, *addressTableContainer, *moduleMapper, *staticVMEConfiguration);
    vmeCrate->printAddressMap(cout);
    // set stream back to decimal after printAddressMap(cout)
    cout << dec;

    VMEDevice *device = vmeCrate->getVMEDevice(slotId);


    /* code from Barbara for VME64 ends */

    {
      // create a HAL adapter for a ScanPSC type JTAG Controller
      HALScanPSC100Adapter scanpsc_adapter ( *device, "TDO_REG"); 

      // dynamically select JTAG controller type
      ScanPSC100JTAGController *jtag_controller=0;

      //      jtag_controller = new ScanPSC100JTAGController ( scanpsc_adapter, SCK_frequency );
      jtag_controller = new DTTFJTAGController ( scanpsc_adapter, SCK_frequency );

      if (debug_mode) jtag_controller->setDebugFlag(5);
      else if (verbose_mode) jtag_controller->setDebugFlag(4);

      {
	// open JTAG Chain for chain 0 on the controller
	JTAGChain jtag_chain( *jtag_controller, chain_nr );
      
	jtag_chain.resetTAP();

	if (override_errors) 
	  seq.setOverrideError(true);

	// run it
	StopWatch sw1(0);
	sw1.start();
	bool status = seq.execute( jtag_chain );
	jtag_chain.resetTAP();
	sw1.stop();

	if (status)
	  cout << "successfully played SVF file." << endl;
	else
	  cout << "error playing SVF file." << endl;

	cout << "time elapsed = " << (double)sw1.read()/1.e6 << " sec." << endl;

      }
      delete jtag_controller;
    }

    // delete busAdapter; code missing, here

  }
  catch (exception &e) {
    cout << "Unknown Exception caught: " << e.what() << endl;
    return 1;
  }
  return 0;
}

