// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                            *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                *
 *                                                                       *
 * For the licensing terms see LICENSE.		                            *
 * For the list of contributors see CREDITS.   			                *
 *************************************************************************/



const addon = require('../alchemy/build/Release/alchemy');

var express = require('express');

var pta = addon.getPeerTransportAgent();
var xgil = pta.getListener('cgi');
var xoapl = pta.getListener('soap');

module.exports = class Listener {
	constructor() {
	}

	processIncoming (req, res, next) { 
		var xgiinput = {
				'url':  req.headers["request_uri"], 
				'method' : req.method,
				'headers': req.headers, 
				'environment': req.environment,
				//'data' : req.body.toString('base64')
				'data' : req.body.toString() 
		};	
		req.xgiinput = xgiinput;

		console.log( "------> PROCESS INCOMING begin");
		console.log(xgiinput)
		console.log(req.xgiinput.headers)
		console.log(req.xgiinput.data)
		console.log( "------> PROCESS INCOMING end");
		var soapaction = '';
		var soapaction = '';
		if ( req.headers['soapaction'] )
		{
			soapaction = req.headers['soapaction'] ;
		}
		var contype = req.headers['content-type'];
		var xgiout;
		if ( (contype != undefined ) && (( contype.indexOf('application/soap+xml') > -1 ) || (contype.indexOf('text/xml') > -1 ) || (soapaction != '')))
		{
			console.log( "------> Is soap begin");
			xgiout = xoapl.processIncoming(xgiinput);
			console.log(xgiout)
			console.log( "------> Is soap end");
			res.statusMessage = xgiout.reason;
			res.status(xgiout.status);

			xgiout.headers.forEach(function(value){
				var mimetype = value.split(':');
				res.setHeader(mimetype[0],mimetype[1].trim());
			});
			res.xgiout = xgiout;
			res.end(res.xgiout.data, 'utf-8');

		}
		else
		{
			console.log( "------> Is XGI begin");
			xgiout = xgil.processIncoming(req.xgiinput);
			console.log( "------> Is XGI end");


			res.statusMessage = xgiout.reason;
			res.status(xgiout.status);

			xgiout.headers.forEach(function(value){
				var mimetype = value.split(':');
				res.setHeader(mimetype[0],mimetype[1].trim());
			});
			res.xgiout = xgiout;
			next();
		}
		//res.end(xgiout.data, 'utf-8');
	}
}
/*
if ( ( content_type.indexOf('application/soap+xml') > -1 ) || (content_type.indexOf('text/xml') > -1 ) || soapaction != '')
                {
                                //console.log("Got SOAP message" );
                                //console.log (xgiinput);

                                //console.log ("+++++++++++++++++++++++++++++++++++End SOAP message++++++++++++++++++++++++++++++++");
                                var xgiout = xoapl.processIncoming(xgiinput);

                                response.statusCode = xgiout.status;
                                response.statusMessage = xgiout.reason;
                                response.httpVersion = xgiout.version;

                                xgiout.headers.forEach(function(value){
                                        var mimetype = value.split(':');
                                        //console.log(mimetype);
                                        response.setHeader(mimetype[0], mimetype[1]);

                                });
                                response.end(xgiout.data, 'utf-8');
                                //console.log(xgiout.data);


                }      
 */

