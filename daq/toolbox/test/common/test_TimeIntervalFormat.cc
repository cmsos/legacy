#include <iostream>
#include <string>
#include "toolbox/TimeVal.h"
#include "toolbox/TimeInterval.h"


int main() 
{ 
	toolbox::TimeInterval i1;	
	i1.fromString ("P1M1DT1H1M1S");	
	std::cout << "Time interval 'P1M1DT1H1M1S' in xdaq internal format: " << i1.toString() << std::endl;
	
	i1.fromString ("P1MT1H1M1S");	
	std::cout << "Time interval 'P1MT1H1M1S' in xdaq internal format: " << i1.toString() << std::endl;


	// Add 1 Hour 1 Minute 1 second to now
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	
	toolbox::TimeInterval i2;
	i2.fromString ("PT1H1M");
	
	std::cout << "Now is " << now.toString (toolbox::TimeVal::gmt) << std::endl;
	now += i2;
	std::cout << "Now plus 1 hour, 1 minute (PT1H1M): " << now.toString(toolbox::TimeVal::gmt) << std::endl;

	return 0; 
} 
