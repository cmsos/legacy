<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->


	<xp:Application class="executive::Application" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<!-- logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel -->
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>
	
	<xp:Application class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local">
		 <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
		 	<documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
                        <aliasName xsi:type="xsd:string">tmp</aliasName>
                        <aliasPath xsi:type="xsd:string">/tmp</aliasPath>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

	<xp:Application class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
	
	<xp:Application class="pt::utcp::Application" id="11" instance="0" network="local">
    			<properties xmlns="urn:xdaq-application:pt::utcp::Application" xsi:type="soapenc:Struct">
	       			<maxClients xsi:type="xsd:unsignedInt">1024</maxClients>
	       			<autoConnect xsi:type="xsd:boolean">false</autoConnect>
	       			<ioQueueSize xsi:type="xsd:unsignedInt">10000</ioQueueSize>
	       			<maxReceiveBuffers xsi:type="xsd:unsignedInt">128</maxReceiveBuffers>
	       			<maxBlockSize xsi:type="xsd:unsignedInt">2048</maxBlockSize>
    			</properties>
  		</xp:Application>
		
 <xp:Module>/cmsnfshome0/nfshome0/cwakefie/cmsosrad/trunk/daq/pt/utcp/lib/linux/x86_64_slc5/libptutcp.so</xp:Module> 

	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp1"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp2"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp3"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp4"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp5"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp6"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp7"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp8"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp9"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp10"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp11"/>
	<xp:Endpoint protocol="utcp" service="b2in" subnet="10.176.0.0" autoscan="true" port="30010" maxport="30020" network="utcp12"/>

	<!-- XRelay -->
	<xp:Application class="xrelay::Application" id="4"  service="xrelay" group="profile" network="utcp1"/>
	<xp:Module>${XDAQ_ROOT}/lib/libxrelay.so</xp:Module>
	
	<!-- HyperDAQ -->
	<xp:Application class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	
</xp:Profile>
