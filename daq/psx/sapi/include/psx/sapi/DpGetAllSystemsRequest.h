// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and A. Petrucci	     			 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_DpGetAllSystemsRequest_h_
#define _psx_sapi_DpGetAllSystemsRequest_h_

#include <string>
#include <set>

#include "psx/sapi/Request.h"
     
namespace psx {
	namespace sapi {
	
		class ApplicationService;
		
		class DpGetAllSystemsRequest : public psx::sapi::Request
		{
      			public:
			
			DpGetAllSystemsRequest( psx::sapi::ApplicationService * as);
			
			void setPattern(const std::string& pattern);

                        const char* getPattern();

                        private:

                        std::string pattern_;

			
		};
	}
}

#endif 

