// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/Notification.h"
     

psx::mapi::Notification::Notification
				(
				const std::string& object,
				const std::string& url,
				const std::string& action,
				const std::string& context,
				const std::string& id,
				const std::string& state
				)
{
	object_ = object;
	url_ = url;
	action_ = action;
	context_ = context;
	id_ = id;
	state_ = state;
}

std::string psx::mapi::Notification::getObjectName()
{
	return object_;
}

std::string psx::mapi::Notification::getURL()
{
	return url_;
}

std::string psx::mapi::Notification::getAction()
{
	return action_;
}

std::string psx::mapi::Notification::getContext()
{
	return context_;
}

std::string psx::mapi::Notification::getId()
{
	return id_;
}

std::string psx::mapi::Notification::getState()
{
	return state_;
}
