#ifndef _tcds_utils_version_h_
#define _tcds_utils_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSUTILS_VERSION_MAJOR 3
#define TCDSUTILS_VERSION_MINOR 13
#define TCDSUTILS_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSUTILS_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSUTILS_VERSION_CODE PACKAGE_VERSION_CODE(TCDSUTILS_VERSION_MAJOR,TCDSUTILS_VERSION_MINOR,TCDSUTILS_VERSION_PATCH)
#ifndef TCDSUTILS_PREVIOUS_VERSIONS
#define TCDSUTILS_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSUTILS_VERSION_MAJOR,TCDSUTILS_VERSION_MINOR,TCDSUTILS_VERSION_PATCH)
#else
#define TCDSUTILS_FULL_VERSION_LIST TCDSUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSUTILS_VERSION_MAJOR,TCDSUTILS_VERSION_MINOR,TCDSUTILS_VERSION_PATCH)
#endif

namespace tcdsutils {

  const std::string package = "tcdsutils";
  const std::string versions = TCDSUTILS_FULL_VERSION_LIST;
  const std::string description = "CMS TCDS helper software.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS TCDS helper software.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdsutils

#endif
