# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2018, CERN.                                        #
# All rights reserved.                                                  #
# Authors:  L. Orsini, A. Forrest and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

BUILD_HOME:=$(shell pwd)/../../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

ifndef MFDEFS_SUPPORT
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.extern_coretools
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.coretools
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.extern_powerpack
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.powerpack
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.database_worksuite
else
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)
endif

#
# Packages to be built
#
Project=$(PROJECT_NAME)
Package=sentinel/arc/utils

ifdef ORACLE_HOME
	ORACLE_INCLUDE_PREFIX=${ORACLE_HOME}/rdbms/demo/ ${ORACLE_HOME}/rdbms/public/
	ORACLE_LIB_PREFIX=${ORACLE_HOME}/lib
endif

UserCCFlags  	+= -DORACLE__
ExternalObjects	+= -locci -lclntsh

Sources= \
		PolicyCache.cc \
		AlarmStore.cc \
		Node.cc \
		Model.cc \
		version.cc

Executables= 

IncludeDirs = \
	$(B2IN_UTILS_INCLUDE_PREFIX) \
	$(B2IN_NUB_INCLUDE_PREFIX) \
	$(XERCES_INCLUDE_PREFIX) \
	$(LOG4CPLUS_INCLUDE_PREFIX) \
	$(CGICC_INCLUDE_PREFIX) \
	$(XCEPT_INCLUDE_PREFIX) \
	$(CONFIG_INCLUDE_PREFIX) \
	$(TOOLBOX_INCLUDE_PREFIX) \
	$(PT_INCLUDE_PREFIX) \
	$(XDAQ_INCLUDE_PREFIX) \
	$(XDATA_INCLUDE_PREFIX) \
	$(XOAP_INCLUDE_PREFIX) \
	$(XGI_INCLUDE_PREFIX) \
	$(I2O_INCLUDE_PREFIX) \
	$(XI2O_INCLUDE_PREFIX) \
	$(XMAS_INCLUDE_PREFIX) \
	$(SENTINEL_INCLUDE_PREFIX) \
	$(SENTINEL_UTILS_INCLUDE_PREFIX) \
	$(SQLITE_INCLUDE_PREFIX) \
	$(XSLP_INCLUDE_PREFIX) \
	$(XPLORE_INCLUDE_PREFIX) \
	$(XPLORE_UTILS_INCLUDE_PREFIX) \
	$(WS_ADDRESSING_INCLUDE_PREFIX) \
	$(WS_UTILS_INCLUDE_PREFIX) \
	$(WS_EVENTING_INCLUDE_PREFIX) \
	$(JANSSON_INCLUDE_PREFIX) \
	$(ORACLE_INCLUDE_PREFIX)
	
DependentLibraryDirs = $(ORACLE_LIB_PREFIX)

LibraryDirs = $(ORACLE_LIB_PREFIX)

UserSourcePath = 

UserCFlags =
UserCCFlags = 
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =

# These libraries can be platform specific and
# potentially need conditional processing
#
Libraries =

#
# Compile the source files and create a shared library
#
DynamicLibrary=sentinelarcutils
StaticLibrary=

TestLibraryDirs=
TestExecutables=
TestLibraries=
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
