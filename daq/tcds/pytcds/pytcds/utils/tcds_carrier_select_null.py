#!/usr/bin/env python

###############################################################################
## Configure the clocking circuitry to use the signal from port 3 on
## the uTCA backplane (i.e., the TTC clock) as the 40 MHz TTC clock.
###############################################################################

from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class ClockCfg(CmdLineBase):

    def main(self):
        carrier = get_carrier_hw(self.ip_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Switching TTC clock source to FCLKA."

        carrier.choose_clock_source("null")
        # End of main().

    # End of class ClockCfg.

###############################################################################

if __name__ == "__main__":

    ClockCfg().run()
    print "Done"

###############################################################################
