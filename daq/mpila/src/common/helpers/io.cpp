/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * helpers.cpp
 *
 *  Created on: Nov 17, 2017
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/helpers/io.hpp"

#include <stdexcept>
#include <pthread.h>
#include <mpi.h>

#include <string>
#include <sstream>
#include <iomanip>

namespace mpila
{

namespace helpers
{


void io::setThreadAffinity(size_t cpuId)
{
    // Create a cpu_set_t object representing a set of CPUs. Clear it and mark
    // only CPU i as set.
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(cpuId, &cpuset);
    int rc = pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset);
    if (rc != 0)
    {
        std::stringstream ss;
        ss << "Error pinning thread to core : " << cpuId << ". Error code: " << rc;
        std::cerr << ss.str() << std::endl;
    }
}
} /* namespace helpers */
}
