#ifndef _gevb2g_ResourcePool_h_
#define _gevb2g_ResourcePool_h_

#include <vector>

#include "xdaq/Object.h"
#include "toolbox/mem/Reference.h"
#include "interface/shared/i2ogevb2g.h"

// Log4CPLUS
#include "log4cplus/logger.h"

namespace gevb2g
{

	const size_t MaxHistoryRecords = 100;

	class ResourcePool : public xdaq::Object
	{
		public:

			ResourcePool (xdaq::Application * owner);

			typedef struct FragmentHistoryRecord
			{
					FragmentHistoryRecord ();

					FragmentHistoryRecord (size_t totalSize, size_t partSize, size_t tid);

					size_t totalSize_;
					size_t partSize_;
					size_t tid_;

			} FragmentHistoryRecordType;

			typedef struct
			{
					size_t totalSize;
					size_t currentSize;
					toolbox::mem::Reference* ref;
					toolbox::mem::Reference* last; // remember the last reference to which a fragment was written
					size_t blockOffset; // remember the place in the ref to which we wrote
					size_t currentBlock; // remember the number of allocated MTUs
					std::vector<FragmentHistoryRecord> history;
					size_t historyIndex;
					size_t completion;
			} FragmentDescriptor;

			typedef struct
			{
					size_t context;
					size_t totalSize;
					size_t currentFragments;
					size_t eventCompletion;
					size_t sglSize;
					size_t reservedSize;
			} ResourceDescriptor;

			void init (size_t maxResources, size_t maxFragments, size_t mtuSize, size_t memcopyEnabled);

			void showResourceHistory (size_t reasourceId);

			void showFragmentHistory (size_t r, size_t f);

			size_t sglLen (size_t r);

			// the retrieve function should be capable of generating SGL according to
			// a given output MTU size independently of the block size used to build an event
			// (for ex. cache size different than mtu size)
			// in such a case we need an additional parameter that gives the size of the blocks
			// in the event build structure.

			void retrieve (size_t r, I2O_V2_SGE_FRAGMENT_ELEMENT_U32 * sgl);

			void reset ();

			void available (size_t r, size_t context);

			void clear (size_t r);

			size_t size (size_t r);

			// !returns true if the resource (event is completed) false otherwise
			//
			bool appendWithCopy (size_t r, size_t f, char* data, size_t length, size_t totalSize, size_t tid) ;

			bool appendZeroCopy (size_t r, size_t f, toolbox::mem::Reference * ref, size_t length, size_t totalSize, size_t tid, size_t context, size_t reservedSize) ;

			inline toolbox::mem::Reference* getRef (size_t resource, size_t fragment);

		protected:

			std::vector<std::vector<FragmentDescriptor> > fragmentTable_;
			std::vector<ResourceDescriptor> resourceTable_;
			size_t maxFragments_;
			size_t maxResources_;
			size_t mtuSize_;
			size_t memcopyEnabled_;

	};
}

#endif
