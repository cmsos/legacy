#ifndef ApplicationFSMInterface_h
#define ApplicationFSMInterface_h

#include "xcept/Exception.h"

namespace tcds {
  namespace utils {

    class ApplicationFSMInterface
    {
    public:
      virtual void gotoFailedAsynchronously(xcept::Exception& err) = 0;
      virtual ~ApplicationFSMInterface() {};

    protected:
      ApplicationFSMInterface() {};

    private:
      // Private copy constructor to prevent copying.
      ApplicationFSMInterface(ApplicationFSMInterface const&);
    };

  } // namespace utils
} // namespace tcds

#endif // ApplicationFSMInterface_h
