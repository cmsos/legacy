#!/bin/bash

XDAQ_PLATFORM=$1

GSLLIB=gsl-1.9-1.i386.rpm
GSLDEBUG=gsl-debuginfo-1.9-1.i386.rpm
GSLDEV=gsl-devel-1.9-1.i386.rpm

FFTW3=fftw3-3.1.1-1.rf.i386.rpm
FFTW3DEBUG=fftw3-debuginfo-3.1.1-1.rf.i386.rpm
FFTW3DEV=fftw3-devel-3.1.1-1.rf.i386.rpm

XMLLIB=libxml-1.8.17-13.i386.rpm
XMLLIBDEBUG=libxml-debuginfo-1.8.17-13.i386.rpm
XMLLIBDEV=libxml-devel-1.8.17-13.i386.rpm

XMLRPC=xmlrpc-c-1.06.16-20070731_xdaq.i386.rpm
XMLRPCDEBUG=xmlrpc-c-debuginfo-1.06.16-20070731_xdaq.i386.rpm

test_and_install()
{
if [ "$XDAQ_PLATFORM" = "x86_slc4" ]; then
   echo "Installing $1 ..... "
   rpm --install ./reqrpms/$1
fi
}

if [ "$2" = "install" ]; then

test_and_install $GSLLIB 
test_and_install $GSLDEBUG 
test_and_install $GSLDEV 

test_and_install $FFTW3 
test_and_install $FFTW3DEBUG 
test_and_install $FFTW3DEV 

test_and_install $XMLLIB 
test_and_install $XMLLIBDEBUG 
test_and_install $XMLLIBDEV 

test_and_install $XMLRPC
test_and_install $XMLRPCDEBUG

exit 0

elif [ "$2" = "remove" ]; then

rpm --erase gsl-devel
rpm --erase gsl-debuginfo
rpm --erase gsl 

rpm --erase fftw3-devel 
rpm --erase fftw3-debuginfo
rpm --erase fftw3

rpm --erase libxml-devel
rpm --erase libxml-debuginfo
rpm --erase libxml

rpm --erase --allmatches xmlrpc-c
rpm --erase --allmatches xmlrpc-c-debuginfo

fi
