// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "es/api/Stream.h"

#include "xoap/domutils.h"
#include <sstream>
#include "es/api/CurlURLInputStream.hpp"
#include <xercesc/util/XMLNetAccessor.hpp>
#include "toolbox/TimeVal.h"

#include "xoap/domutils.h"

#include <memory>

XERCES_CPP_NAMESPACE_USE

es::api::Stream::Stream (xdaq::Application * owner, const std::string & url, toolbox::Properties & properties) 
	: xdaq::Object(owner), url_(url), properties_(properties), counter_(0)
{
	curl_ = curl_easy_init();
	// Raw tet support is not in use . We rely on libcurl
	//toolbox::net::URL dest(url);
	//connection_ = new es::api::HTTPURLConnection();
	//connection_->connect(dest.getHost(), dest.getPort());
}

es::api::Stream::~Stream ()
{
	   curl_easy_cleanup(curl_);
}

json_t * es::api::Stream::get(const std::string & path, const std::string & query,  const std::string & fragment, json_t * json, long * httpcode) 
{

	std::stringstream esURL;
	esURL << url_ << "/" << path;

	if ( query != "")
	{
		esURL << "?" << query;
	}

	if ( fragment != "")
	{
		esURL << "#" << fragment;
	}
	XMLURL xmlUrl;
	try
	{
			xmlUrl = esURL.str().c_str();
	}
	catch (MalformedURLException& mue)
	{
			XCEPT_RAISE(es::api::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}

	try
	{

		json_t * result = this->request(xmlUrl,es::api::GET, json, httpcode);
		return result;
	}
	catch(es::api::exception::Exception & e)
	{
		XCEPT_RETHROW(es::api::exception::Exception, "Failed http request", e);
	}

}

json_t * es::api::Stream::put(const std::string & path, const std::string & query,  const std::string & fragment, json_t * json, long * httpcode) 
{

	std::stringstream esURL;
	esURL << url_ << "/" << path;

	if ( query != "")
	{
		esURL << "?" << query;
	}

	if ( fragment != "")
	{
		esURL << "#" << fragment;
	}
	XMLURL xmlUrl;
	try
	{
			xmlUrl = esURL.str().c_str();
	}
	catch (MalformedURLException& mue)
	{
			XCEPT_RAISE(es::api::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}


	try
	{
		json_t * result = this->request(xmlUrl,es::api::PUT,json, httpcode);
		return result;
	}
	catch(es::api::exception::Exception & e)
	{
		XCEPT_RETHROW(es::api::exception::Exception, "Failed http request", e);
	}

}

json_t * es::api::Stream::post(const std::string & path, const std::string & query,  const std::string & fragment, json_t * json, long * httpcode) 
{

	std::stringstream esURL;
	esURL << url_ << "/" << path;

	if ( query != "")
	{
		esURL << "?" << query;
	}

	if ( fragment != "")
	{
		esURL << "#" << fragment;
	}
	XMLURL xmlUrl;
	//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(),"esUrl : " << esURL.str());

	try
	{
		xmlUrl = esURL.str().c_str();

	}
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE(es::api::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}

	try
	{
		json_t * result = this->request(xmlUrl, es::api::POST, json, httpcode );
		return result;
	}
	catch(es::api::exception::Exception & e)
	{
		XCEPT_RETHROW(es::api::exception::Exception, "Failed http request", e);
	}


}

json_t * es::api::Stream::binaryPost(const std::string & path, const std::string & query, const std::string & fragment, char * buffer, long length, long * httpcode) 
{

	std::stringstream esURL;
	esURL << url_ << "/" << path;

	if ( query != "")
	{
		esURL << "?" << query;
	}

	if ( fragment != "")
	{
		esURL << "#" << fragment;
	}
	XMLURL xmlUrl;
	//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(),"esUrl : " << esURL.str());

	try
	{
		xmlUrl = esURL.str().c_str();

	}
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE(es::api::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}

	try
	{

		json_t * result = this->request(xmlUrl,es::api::POST, buffer, length, httpcode);
		return result;
	}
	catch(es::api::exception::Exception & e)
	{
		XCEPT_RETHROW(es::api::exception::Exception, "Failed http request", e);
	}

}



void es::api::Stream::head(const std::string & path, long * httpcode) 
{
	 /*//Test if index exists
	 	 bool ElasticSearch::exist(const std::string& index){
	 	 return (200 == _http.head(index.c_str(), 0, 0));
	 	 } his*/

	 	 //curl -XHEAD 'http://localhost:9200/flashlist' -v
	 //should form and then return response

	 std::stringstream esURL;
	 esURL << url_ << "/" << path;

	 XMLURL xmlUrl;

	try
	 {
	 	xmlUrl = esURL.str().c_str();
	 }
	 catch (MalformedURLException& mue)
	 {
	 	XCEPT_RAISE(es::api::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	 }

		std::list<std::string> headers;

	try
	{
		json_t * result = this->request(xmlUrl, es::api::HEAD, 0, httpcode);

		if ( result != 0 )
		{
			json_decref ( result);
		}
	}
	catch(es::api::exception::Exception & e)
	{
		XCEPT_RETHROW(es::api::exception::Exception, "Failed http request", e);
	}

}

json_t * es::api::Stream::del(const std::string & path, const std::string & query,  const std::string & fragment, long * httpcode) 
{

	std::stringstream esURL;
	esURL << url_ << "/" << path;

	if ( query != "")
	{
		esURL << "?" << query;
	}

	if ( fragment != "")
	{
		esURL << "#" << fragment;
	}
	XMLURL xmlUrl;

	try
	{
		xmlUrl = esURL.str().c_str();

	}
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE(es::api::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}

	try
	{

		json_t * result = this->request(xmlUrl,es::api::DELETE, 0, httpcode);
		return result;
	}
	catch(es::api::exception::Exception & e)
	{

		XCEPT_RETHROW(es::api::exception::Exception, "Failed http request", e);
	}

}

/* Raw test support, we use libcurl instead
json_t * es::api::Stream::request(XMLURL & xmlUrl,  es::api::HTTPMethod fHTTPMethod , json_t * payload,  long * httpcode) 
{
	std::unique_ptr<char> outputBuffer;
	if ( payload)
	{
			outputBuffer.reset(json_dumps(payload, 0));
			fPayload_ = outputBuffer.get();
			fPayloadLen_ = ::strlen(fPayload_);

		}

	std::string path = xoap::XMLCh2String(xmlUrl.getPath());
	std::string host = xoap::XMLCh2String(xmlUrl.getHost());
	connection_->connect(host, xmlUrl.getPortNum());
	//std::cout << "request path : " << path << std::endl;


	//std::cout << "sending payload: " << fPayload_ << std::endl;

	connection_->sendTo((char*)path.c_str(),(char*)host.c_str(),xmlUrl.getPortNum(),fPayload_,fPayloadLen_);
	std::string response = connection_->receiveFrom();
	//std::cout << "returning response : " << response << std::endl;
	json_t *root = 0;

		if ( response.size() > 0 )
		{
			json_error_t status;
			//std:: cout << "actual response data size:  " << inputstream_.size() << std::endl;
			root = json_loads(response.c_str(), response.size(), &status);
			if (!root)
			{
				long http_code =0;
				std::stringstream msg;
				msg << " Failed to parse json: " << status.text << ", " << status.source << " line: " << status.line <<  " column :" << status.column << "postion : " << status.position << " http info: "<< *httpcode  << " body: " << inputstream_;
				XCEPT_RAISE(es::api::exception::Exception, msg.str());
			}
		}
		//std:: cout << "actual reply data:  " << memBuf << std::endl;
		//std::cout << "parsed jansson response : "<< json_dumps(root, 0) << std::endl;
		connection_->close();
		return root;

}
*/
json_t * es::api::Stream::request(XMLURL & xmlUrl,  es::api::HTTPMethod fHTTPMethod , json_t * json, long * httpcode) 
{
	std::unique_ptr<char> outputBuffer;
	char * buffer = 0;
	long length = 0;
	if (json)
	{
		outputBuffer.reset(json_dumps(json, 0));
		buffer = outputBuffer.get();
		length = ::strlen(buffer);
	}

	try
	{
		json_t * result = this->request(xmlUrl,fHTTPMethod, buffer, length, httpcode);
		return result;
	}
	catch(es::api::exception::Exception & e)
	{
		XCEPT_RETHROW(es::api::exception::Exception, "Failed http request", e);
	}
}

// Stream curl integrated
json_t * es::api::Stream::request(XMLURL & xmlUrl,  es::api::HTTPMethod fHTTPMethod , char * buffer, long length, long * httpcode) 
{
	counter_++;

	curl_easy_reset(curl_);


	// send all data to this function
	curl_easy_setopt(curl_, CURLOPT_WRITEFUNCTION, es::api::Stream::staticWriteCallback);

	// we pass our 'chunk' struct to the callback function
	curl_easy_setopt(curl_, CURLOPT_WRITEDATA, (void *)this);

	// some servers don't like requests that are made without a user-agent
	//	     field, so we provide one
	curl_easy_setopt(curl_, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	long timeout = 30; // default
	curl_easy_setopt(curl_, CURLOPT_TIMEOUT, timeout);
	curl_easy_setopt(curl_, CURLOPT_CONNECTTIMEOUT, timeout);

	if (properties_.hasProperty("urn:es-api-stream:CURLOPT_VERBOSE"))
	{
		curl_easy_setopt(curl_, CURLOPT_VERBOSE, 1);
	}

	curl_easy_setopt(curl_, CURLOPT_FAILONERROR, 0);
	curl_easy_setopt(curl_, CURLOPT_MAXREDIRS, 30);

	curl_easy_setopt(curl_, CURLOPT_READDATA, this);
	curl_easy_setopt(curl_, CURLOPT_READFUNCTION, staticReadCallback);
	curl_easy_setopt(curl_, CURLOPT_INFILESIZE_LARGE, (curl_off_t)0);

	if (properties_.hasProperty("urn:es-api-stream:CURLOPT_TCP_NODELAY"))
	{
		curl_easy_setopt(curl_, CURLOPT_TCP_NODELAY, 1);
	}

	//curl_easy_setopt(curl_,CURLOPT_MAXCONNECTS, 1024);

	if (properties_.hasProperty("urn:es-api-stream:CURLOPT_FORBID_REUSE"))
	{
		curl_easy_setopt(curl_, CURLOPT_FORBID_REUSE, 1);
	}

	//Code to add field to HTTP header
	//struct curl_slist * chunk = NULL;
	//chunk = curl_slist_append(chunk, "Connection: keep-alive");
	//curl_easy_setopt(curl_, CURLOPT_HTTPHEADER, chunk);


	std::unique_ptr<char> outputBuffer;
	CURLcode res;

	//struct curl_slist *slist = NULL;

	//slist = curl_slist_append(slist, "Expect:");
	//slist = curl_slist_append(slist, "Transfer-Encoding:");

	//res = curl_easy_setopt(curl_, CURLOPT_HTTPHEADER, slist);

	//prepare input
	inputstream_.clear();

	fPayload_ = 0;
	fPayloadLen_ = 0;
	curl_easy_setopt(curl_, CURLOPT_INFILESIZE_LARGE, (curl_off_t)0);
	// prepare output
	if ( buffer != 0)
	{
		fPayload_ = buffer;
		fPayloadLen_ = length;
		//curl_easy_setopt(curl_, CURLOPT_READDATA, this);
		//curl_easy_setopt(curl_, CURLOPT_READFUNCTION, staticReadCallback);
		curl_easy_setopt(curl_, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fPayloadLen_);
		//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(),"Reoutput result len : " << fPayloadLen_ << " data: " << fPayload_ );
	}

	std::string url = xoap::XMLCh2String(xmlUrl.getURLText());
	// specify URL to get
	curl_easy_setopt(curl_, CURLOPT_URL, url.c_str());

	//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "will executing request : " << url << " with command " );

	// Set the correct HTTP method
	switch(fHTTPMethod)
	{
		case es::api::GET:
			curl_easy_setopt(curl_, CURLOPT_HTTPGET, (long)1);
			//std::cout <<  " GET" << std::endl;
			break;
		case es::api::PUT:
			//std::cout <<  " PUT" << std::endl;
			curl_easy_setopt(curl_, CURLOPT_UPLOAD, 1L);
			curl_easy_setopt(curl_, CURLOPT_PUT, (long)1);
			break;
		case es::api::POST:
			//std::cout <<  " POST" << std::endl;

			curl_easy_setopt(curl_, CURLOPT_POST, (long)1);
			curl_easy_setopt(curl_, CURLOPT_POSTFIELDSIZE_LARGE, fPayloadLen_);
			break;
		case es::api::HEAD:
			//std::cout <<  " HEAD" << std::endl;

			curl_easy_setopt(curl_, CURLOPT_NOBODY, (long)1);
			break;
		case es::api::DELETE:
			//std::cout <<  " DELETE" << std::endl;

			curl_easy_setopt(curl_, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
	}

	// get it!
	//std::cout <<  "performing request" << std::endl;
	res = curl_easy_perform(curl_);

	//Freeing memory. Related to the code for adding field to HTTP header
	//curl_slist_free_all(chunk);

	// check for errors
	if(res != CURLE_OK)
	{
		std::stringstream msg;
		msg << " Failed to perform request: " << curl_easy_strerror(res);
		XCEPT_RAISE(es::api::exception::Exception, msg.str());
	}
	//long http_code;
	curl_easy_getinfo(curl_, CURLINFO_RESPONSE_CODE, httpcode);
	//std::cout <<  "done" << std::endl;
	//curl_slist_free_all(slist);

	json_t *root = 0;

	if ( inputstream_.size() > 0 )
	{
		json_error_t status;
		//std:: cout << "actual response data size:  " << inputstream_.size() << std::endl;
		root = json_loads(inputstream_.c_str(), inputstream_.size(), &status);
		if (!root)
		{
			std::stringstream msg;
			msg << " Failed to parse json: " << status.text << ", " << status.source << " line: " << status.line <<  " column :" << status.column << "postion : " << status.position << " http info: "<< *httpcode  << " body: " << inputstream_;
			XCEPT_RAISE(es::api::exception::Exception, msg.str());
		}
	}
	//std:: cout << "actual reply data:  " << memBuf << std::endl;
	//std::cout << "parsed jansson response : "<< json_dumps(root, 0) << std::endl;

	return root;
}


size_t es::api::Stream::receive(char *buffer, size_t size, size_t nmemb)
{
	//std:: cout << "receive" << std::endl;
	  size_t realsize = size * nmemb;
	  inputstream_.append((const char*) buffer, realsize);
	  return realsize;
}

size_t es::api::Stream::send(char *buffer, size_t size, size_t nmemb)
{
	//std::cout << "es::api::Stream::send" << fPayloadLen_  << " data " << fPayload_[0] << " => "<< fPayload_[1] << std::endl;
	  size_t len = size * nmemb;
	    if(len > fPayloadLen_) len = fPayloadLen_;

	    memcpy(buffer, fPayload_, len);

	    fPayload_ += len;
	    fPayloadLen_ -= len;

	    return len;
	/*
	std:: cout << "send" << std::endl;

	 size_t len = size * nmemb;
	 if(len > outputstream_.size()) len = outputstream_.size();

	 memcpy(buffer, outputstream_.c_str(), len);
	 outputstream_ = outputstream_.substr (len);
*/
	 return len;
}

// libCURL specific read/write handlers
size_t es::api::Stream::staticReadCallback(char *buffer, size_t size,  size_t nitems, void *stream)
{
	return ((Stream*)stream)->send(buffer, size, nitems);
}
size_t es::api::Stream::staticWriteCallback(char *buffer, size_t size,  size_t nitems, void *stream)
{
	return ((Stream*)stream)->receive(buffer, size, nitems);
}

