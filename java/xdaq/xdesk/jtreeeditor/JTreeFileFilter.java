package xdaq.xdesk.jtreeeditor;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class JTreeFileFilter
    extends javax.swing.filechooser.FileFilter {
//******************************************************************************
   /**Whether the given file is accepted by this filter*/
   public boolean accept(File f) {
     boolean accept = f.isDirectory();
     if (!accept) {
       String suffix = getSuffix(f);
       if (suffix != null) {
         accept = suffix.equals("xml");
       }
     }
     return accept;
   }

//******************************************************************************
   /**The description of this filter.*/
   public String getDescription() {
     return "XML Files (*.xml)";
   }

//******************************************************************************
   /**Returns file extension*/
   private String getSuffix(File f) {
     String s = f.getPath(), suffix = null;
     int i = s.lastIndexOf('.');
     if (i > 0 && i < s.length() - 1) {
       suffix = s.substring(i + 1).toLowerCase();
     }
     return suffix;
   }

//******************************************************************************
}
