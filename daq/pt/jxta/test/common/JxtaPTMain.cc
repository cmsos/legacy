#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "pt/PeerTransportAgent.h"
#include "jxta/ptjxtaV.h"
#include "jxta/PeerTransportReceiver.h"
#include "jxta/PeerTransportSender.h"
#include "jxta/Platform.h"
#include "jxta/PeerGroup.h"
#include "jxta/DiscoveryService.h"
#include "jxta/RdvService.h"

#include "TestJxtaListener.h"

using namespace std;


void standaloneBootstrap() {
	std::ostringstream ass;

	ass << "jxta://localhost:9701/";
	/*if(argc == 1)
		ass << "9701/pipe";
	else
		ass << argv[1] << "/pipe";*/

	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();

	jxta::PeerTransportReceiver* jpr = new jxta::PeerTransportReceiver();
	pta->addPeerTransport(jpr);
	jxta::PeerTransportSender* jps = new jxta::PeerTransportSender();
	pta->addPeerTransport(jps);
	cout << "Jxta PeerTransport receiver and sender created. Starting platform...\n";

	jpr->config(jpr->createAddress(ass.str()));    // this creates a jxta::Platform
	//alternatively, jpl = jxta::getPlatform(9701); skipping the jpr->config(...) step
}


int main(int argc, char** argv) {
	jxta::Platform* jpl;
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	jxta::AdvertisementList::Reference advList;
	char c;
	
	try {
	
	standaloneBootstrap();
	jpl = jxta::getPlatform();
	
	pta->addListener(new TestJxtaListener());

	jxta::PeerGroup* netPG = jpl->getNetPeerGroup();
	jxta::DiscoveryService* discSvc = netPG->getDiscoveryService();

	cout << "===== JXTA PT peer up and running =====\n";
	cout << "XDAQ JXTA PT version: " << ptjxta::versions << endl;
	cout << "Peer name: " << netPG->getPeerName() << " - Group name: " << netPG->getPeerGroupName() << endl;
	cout << "JXTA ID: " << netPG->getPeerID() << endl << endl;

	{		// provisional - only for test
		string rdv;
		if(argc == 2) {
			rdv = "128.141.38.";
			rdv += argv[1];
		}
		else
			rdv = "137.138.74.37";     // this is Bobby's PC
		netPG->getRdvService()->addRdvPeer(rdv);
	}

	do {
		cout << "\n[Press any key to refresh peer view, f to flush, s to send msg, j to join group, q to quit] ";
		cin >> c;

		if(c == 'f') {
			cout << "Refreshing advertisement cache...\n";
			discSvc->flushAdvertisements();
			discSvc->searchRemoteAdvertisements(jxta::DiscoveryService::PEER);
			discSvc->searchRemoteAdvertisements(jxta::DiscoveryService::GROUP);
		}

		if(c == 's') {
			cout << "Sending a message, destination (IP):";
			string s;
			cin >> s;
			if(s == "x") continue;

			try {
				pt::Messenger::Reference msgr = pta->getMessenger(pta->createAddress("jxta://" + s + ":9701/"), pt::Address::Reference(0));  // default service to /pipe
				s = "<tag attr='hello'>Test message from '" + netPG->getPeerName() + "' to a Jxta peer at " + s + "</tag>";
				
				(dynamic_cast<jxta::JxtaMessenger&>(*msgr)).send(s);
				cout << "Message successfully sent.\n\n";
			} catch(std::exception& failed) {
				cout << "Failed to send message, exception: " << failed.what() << endl << endl;
			}
		}

		if(c == 'j') {
			cout << "Joining a group, name:";
			string s;
			cin >> s;
			if(s == "x") continue;
			try {
				jpl->joinPeerGroup(s);
			} catch(jxta::exception::Exception& failed) {
				cout << "Could not join the group, exception: " << failed.what() << endl << endl;
			}
		}

		if(c != 'q') {
			jxta::AdvertisementList::Reference advList = discSvc->getKnownAdvertisements(jxta::DiscoveryService::PEER);
			if(advList->getLength() == 0)
				cout << "No advertisements retrieved\n";
			else {
				cout << advList->getLength() << " advertisement(s) retrieved:\n";
				for ( int i = 0; i < advList->getLength(); i++ ) {
					jxta::Advertisement::Reference adv = advList->getItem(i);
					cout << i << ": " << adv->getName() << endl;
				}
			}
			advList = discSvc->getKnownAdvertisements(jxta::DiscoveryService::GROUP);
			if(advList->getLength() == 0)
				cout << "No group advertisements retrieved\n";
			else {
				cout << advList->getLength() << " group advertisement(s) retrieved:\n";
				for ( int i = 0; i < advList->getLength(); i++ ) {
					jxta::Advertisement::Reference adv = advList->getItem(i);
					cout << i << ": " << adv->getName() << endl;
				}
			}
			cout << "Rdv connection: " << (netPG->getRdvService()->isConnectedToRdv() ? "ok\n" : "NOT YET established\n");
		}
	}
	while(c != 'q');

	}
	catch(pt::exception::Exception& any)     // catch-all statement for debugging purpose
	{
		cout << "### Thrown PT/Jxta exception:\n" << any.what() << " at " << any.module() << ":" << any.line() << " (" << any.function() << ")\n";
		// any.getHistory() 
	}
	catch(std::exception& other)             // catch-all statement for debugging purpose
	{
		cout << "### Thrown GENERAL exception:\n" << other.what() << endl;
	}

	cout << "Exiting...\n\n";
	if(jpl != 0) delete jpl;
	delete pta;
}
