// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_core_PublisherReadyEvent_h_
#define _eventing_core_PublisherReadyEvent_h_

#include "toolbox/Event.h"

namespace eventing
{
	namespace core
	{
		class PublisherReadyEvent : public toolbox::Event
		{
			public:

				PublisherReadyEvent (void* originator, std::string bus)
					: toolbox::Event("eventing::core::PublisherReadyEvent", originator), bus_(bus)
			{

			}

				std::string getBusName()
				{
					return bus_;
				}

			protected:

				std::string bus_;

		};
	}
}

#endif
