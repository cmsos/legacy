###############################################################################
## Utilities related to the CPM board.
###############################################################################

from pytcds.cpm.tcds_cpm_constants import ADDRESS_TABLE_FILE_NAME_CPMT1
from pytcds.cpm.tcds_cpm_constants import ADDRESS_TABLE_FILE_NAME_CPMT2
from pytcds.utils.tcds_amc13 import TCDSAMC13T1
from pytcds.utils.tcds_amc13 import TCDSAMC13T2
from pytcds.utils.tcds_constants import BOARD_TYPE_AMC13
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT1
from pytcds.utils.tcds_constants import BOARD_TYPE_CPMT2
from pytcds.utils.tcds_constants import IPM
from pytcds.utils.tcds_utils_uhal import get_hw_tca

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class CPM(TCDSAMC13T1):

    pass

    # End of class CPM.

class CPMT2(TCDSAMC13T2):

    pass

    # End of class CPMT2.

###############################################################################

def get_cpm_hw(address_or_name,
               controlhub_address=None,
               verbose=False,
               hw_cls=CPM):

    # NOTE: Little trick here: on the CPM T1/T2 there are no separate
    # carrier and board identifiers, as there are on the FC7-based
    # boards.
    cpm = get_hw_tca(BOARD_TYPE_CPMT1,
                     BOARD_TYPE_CPMT1,
                     ADDRESS_TABLE_FILE_NAME_CPMT1,
                     address_or_name,
                     controlhub_address,
                     verbose,
                     hw_cls)

    return cpm

def get_cpmt2_hw(address_or_name,
                 controlhub_address=None,
                 verbose=False,
                 hw_cls=CPMT2):

    # NOTE: Little trick here: on the CPM T1/T2 there are no separate
    # carrier and board identifiers, as there are on the FC7-based
    # boards.
    t2 = get_hw_tca(BOARD_TYPE_CPMT2,
                    BOARD_TYPE_CPMT2,
                    ADDRESS_TABLE_FILE_NAME_CPMT2,
                    address_or_name,
                    controlhub_address,
                    verbose,
                    hw_cls)

    return t2

###############################################################################
