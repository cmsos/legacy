#include <stdint.h>
#include <string>

#include "toolbox/string.h"

#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdEnableRandomTriggers<T>::SOAPCmdEnableRandomTriggers(T& controller) :
  SOAPCmdBase<T>(controller, "EnableRandomTriggers", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdEnableRandomTriggers<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Extract the desired random-trigger frequency.
  uint32_t const freqDesired =
    tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "frequency");

  // Keep track of history.
  std::string const histMsg = toolbox::toString("%s: frequency = %d Hz",
                                                this->commandName().c_str(),
                                                freqDesired);
  this->controller_.appStateInfoSpace_.addHistoryItem(histMsg);

  // Do what we were asked to do.
  uint32_t const freqRealised =
    this->controller_.getHw().enableRandomTriggers(freqDesired);

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  // Add the realised frequency to the reply.
  tcds::utils::soap::addSOAPReplyParameter(reply, "frequency", freqRealised);
  return reply;
}
