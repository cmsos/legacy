package xdaq.xdesk.xdesk;

import java.io.*;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Properties;
import javax.xml.parsers.*;
import org.xml.sax.*;
import org.w3c.dom.*;

import org.apache.log4j.*;


public class DeskletFileScanner 
{
	static Logger logger = Logger.getLogger (DeskletFileScanner.class);

	FileFilter filter_;
	DocumentBuilderFactory domFactory_;
	

	public DeskletFileScanner ()
	{
		// Prepare DOM environment
		domFactory_ = DocumentBuilderFactory.newInstance();		
	}
	
	// Return DOM Document from specified XML file
	protected org.w3c.dom.Document getDom (File file)
	{
		try {
			DocumentBuilder builder = domFactory_.newDocumentBuilder();
			return builder.parse ( file );
		} catch (SAXParseException spe)
		{
			logger.error ("Could not parse file " + file.toString());
			logger.error ("Parse error info: " + spe.getMessage());
			logger.error ("Parse error line: " + spe.getLineNumber() + ", uri: " + spe.getSystemId());
			
			Exception x = spe;
			if (spe.getException() != null)
			{
				x = spe.getException();
			}
			
			//x.printStackTrace();
			
		} catch (SAXException sxe) 
		{
			// Error during parsing
			Exception x = sxe;
			if (sxe.getException() != null)
				x = sxe.getException();
				
			//x.printStackTrace();
		} catch (ParserConfigurationException pce) 
		{
			logger.error ("Failed to create DOM parser: " + pce.toString());
			//pce.printStackTrace();
		} catch (IOException ioe) 
		{
			logger.error ("Failed to read XML file: " + ioe.toString());
		}
		
		// Failed to parse XML file, return null
		return null;
	}
	
	// Rescan the files and rebuild the properties list
	// The XML files are expceted to have the following format:
	// <APPLET name="AppletName" code="XYZApp.class" codebase="full URL to code" align="baseline" width="200" height="200)>
	// 	<PARAM NAME="name" VALUE="value"/>
	// </APPLET>
	//
	// Optionally the <APPLET> tag can have an attribute archive="XYZ.jar" indicating that the class
	// specified in code is found in a jar file.
	//
	public Properties getProperties (String filename)
	{
		org.w3c.dom.Document doc = getDom (new File(filename) );
		if (doc != null)
		{
			NodeList l = doc.getElementsByTagName ("APPLET");
			// Excpect one or more applet tags in the file!!!
			if (l.getLength() == 1)
			{
				// Create new property Object for every applet
				Properties p = new Properties();
				org.w3c.dom.Node node = l.item(0);
				org.w3c.dom.NamedNodeMap attributes = node.getAttributes();
				int a;
				String name = null;
				for (a = 0; a < attributes.getLength(); a++)
				{
					org.w3c.dom.Node attributeNode = attributes.item(a);

					if (attributeNode.getNodeName().equals("name"))
					{
						name = attributeNode.getNodeValue();
					}

					p.setProperty (
						attributeNode.getNodeName(),
						attributeNode.getNodeValue()
						);
				}
				
				// Get children of <Applet> tag called <PARAM>
				NodeList params = doc.getElementsByTagName ("PARAM");
				int param;
				for (param = 0; param < params.getLength(); param++)
				{
					org.w3c.dom.NamedNodeMap paramAttributes = params.item(param).getAttributes();
					String pName = null;
					String pValue = null;
					
					int i;
					for (i = 0; i < paramAttributes.getLength(); i++)
					{
						org.w3c.dom.Node attributeNode = paramAttributes.item(i);

						if (attributeNode.getNodeName().equals("name"))
						{
							pName = attributeNode.getNodeValue();
						}
						if (attributeNode.getNodeName().equals("value"))
						{
							pValue = attributeNode.getNodeValue();
						}
						
					}
					
					if ((pName == null) ||(pValue == null))  
					{
						logger.error ("Missing name/value <PARAM> attribute pair for applet " + filename);
					} else 
					{					
						p.setProperty (pName,pValue);
					}
				
				}
				
				return p;			
			}
		}
		return null;
	}
	
	
	

}
