// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "xoap/DOMParser.h"
#include "xoap/DOMParserFactory.h"

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/las2g/Application.h"
#include "xmas/las2g/exception/Exception.h"

#include "xplore/DiscoveryEvent.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "xdata/InfoSpaceFactory.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"

#include "xdata/Table.h"
#include "xdata/TableAlgorithms.h"
#include "xdata/exdr/Serializer.h"
#include "xdaq/ApplicationDescriptorImpl.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/utils.h"
#include "toolbox/regex.h"
#include "toolbox/BSem.h"
#include "toolbox/exception/Handler.h"

#include "xmas/las2g/exception/Exception.h"
#include "b2in/nub/Method.h"
#include "b2in/nub/Messenger.h"
#include "b2in/utils/exception/Exception.h"


// Required for Namespace declaration used in configuration file
#include "xmas/xmas.h"


XDAQ_INSTANTIATOR_IMPL(xmas::las2g::Application);

xmas::las2g::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), repositoryLock_(toolbox::BSem::FULL)
{	
	b2in::nub::bind(this, &xmas::las2g::Application::onMessage );

	s->getDescriptor()->setAttribute("icon", "/xmas/las2g/images/xmas-las2g-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/las2g/images/xmas-las2g-icon.png");

	reportLostCounter_ = 0;
	lastRetrieveTime_ = toolbox::TimeVal::gettimeofday();
	minDeltaBetweenRetrieve_ = 0.2; // By default allow 20 retrieveCollection requests per second
	
	// In which group to search for a ws-eventing
	subscribeGroup_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeGroup", &subscribeGroup_);
	
	// Which flashlist tags to retrieve from the ws-eventing
	topic_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("topic", &topic_);
	
	scanPeriod_ = "PT10S";
	this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod",&scanPeriod_);
	subscribeExpiration_ = "PT30S";
	this->getApplicationInfoSpace()->fireItemAvailable("subscribeExpiration",&subscribeExpiration_);

	// bind HTTP callback
	xgi::bind(this, &xmas::las2g::Application::retrieveCatalog,    "retrieveCatalog");
	xgi::bind(this, &xmas::las2g::Application::retrieveCollection, "retrieveCollection");

	xgi::framework::deferredbind(this, this, &xmas::las2g::Application::retrieveCatalogHTML,    "retrieveCatalogHTML");
	xgi::framework::deferredbind(this, this, &xmas::las2g::Application::retrieveCollectionHTML, "retrieveCollectionHTML");
	xgi::framework::deferredbind(this, this, &xmas::las2g::Application::Default, "Default");
	
	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	getApplicationContext()->addActionListener(this); // attach to endpoint available events
}

xmas::las2g::Application::~Application()
{

}
void xmas::las2g::Application::timeExpired(toolbox::task::TimerEvent& e)
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Timer callback");
	
	try
	{
		b2inEventingProxy_->scan();		
	}
	catch (b2in::utils::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
	
	
	try
	{
		this->refreshSubscriptionsToEventing();
	}
	catch (xmas::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
	
	
}

void xmas::las2g::Application::asynchronousExceptionNotification(xcept::Exception& e)
{

	std::stringstream msg; 
	msg << "Failed to subscribe to b2in eventing";
	XCEPT_DECLARE_NESTED(xmas::las2g::exception::Exception, ex, msg.str(),e);
	this->notifyQualified("error",ex);

}


void xmas::las2g::Application::actionPerformed( xdata::Event& event) 
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{	
		b2inEventingProxy_ = new b2in::utils::ServiceProxy(this,"b2in-eventing",subscribeGroup_.toString(),this);
		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:las2g-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:las2g-timer");
			
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:las2g-timer");
		}
	
		
		// submit scan task
		toolbox::TimeInterval interval;
		interval.fromString(scanPeriod_);
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		timer->scheduleAtFixedRate( start, this, interval, 0, "urn:xmas:las2g-task" );

	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}

void xmas::las2g::Application::actionPerformed(toolbox::Event& e) 
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "Received event " << e.type());
	/*
	if ( e.type() == "xdaq::EndpointAvailableEvent" )
	{
		// If the available endpoint is a b2in endpoint we are ready to
		// discover other b2in endpoints on the network
		//
		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");
		
        	xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(e);
        	// xdaq::Endpoint* endpoint = ie.getEndpoint();
        	xdaq::Network* network = ie.getNetwork();
		
		LOG4CPLUS_INFO (this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());
		
		if (network->getName() == networkName)
		{		
			try
			{
				LOG4CPLUS_INFO (this->getApplicationLogger(), "Discover b2in endpoints on network " << networkName);
				this->discoverEndpoints();
				this->refreshSubscriptionsToEventing();
			}
			catch (xdaq::exception::Exception& e)
			{
				LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
			}
		}
	}
	*/
}

void xmas::las2g::Application::onMessage (toolbox::mem::Reference *  msg, xdata::Properties & plist) 
	
{

	std::string protocol =  plist.getProperty("urn:b2in-eventing:protocol");
        if ( protocol == "xmas" )
	{
		std::string action =  plist.getProperty("urn:xmas-protocol:action");
		if ( action  == "reset" )
		{
			repositoryLock_.take();
        		for ( std::map<std::string, xmas::las2g::FlashlistData*>::iterator fl = repository_.begin(); fl != repository_.end(); ++fl)
        		{
                		delete (*fl).second;
        		}
        		repository_.clear();
        
        		repositoryLock_.give();

		}
		else
		{
			if (msg != 0 ) msg->release();
			std::stringstream msg;
			msg << "Invalid xmas protocol action  " <<  action;
			XCEPT_RAISE (b2in::nub::exception::Exception, msg.str());
		} 
		if (msg != 0 ) msg->release();
		return;

	}	

	std::string flashlistName = plist.getProperty("urn:xmas-flashlist:name");
	
	if (flashlistName == "")
	{
		XCEPT_RAISE (b2in::nub::exception::Exception, "Incoming B2IN message corrupted, 'urn:xmas-flashlist:name' property is empty");
	}
	else if ( msg != 0 )
	{
		/* LAS does not de-serialize
		xdata::exdr::FixedSizeInputStreamBuffer inBuffer((char*) msg->getDataLocation(), msg->getDataSize());		
		xdata::Table * t = new xdata::Table();
		xdata::Table::Reference  table (t);
	

		try
		{			
			serializer_.import(t, &inBuffer);			
			
		}
		catch (xdata::exception::Exception& e)
		{
			delete t;
			++reportLostCounter_;
			msg->release();
			XCEPT_RETHROW (b2in::nub::exception::Exception, "Failed to deserialize flashlist table", e);
		}
		// done with the buffer
		msg->release();
		*/
		
		std::map<std::string, FlashlistData*>::iterator fl;
		xmas::las2g::FlashlistData* dataEntry = 0;
		repositoryLock_.take();
		
		fl = repository_.find(flashlistName);
		if (fl == repository_.end())
		{
			dataEntry = new xmas::las2g::FlashlistData();
			repository_[flashlistName] = dataEntry;
			
		}
		else
		{
			dataEntry = (*fl).second;
		}
		repositoryLock_.give();
		
		dataEntry->setData(msg, plist);
	}
	else
	{
		XCEPT_RAISE (b2in::nub::exception::Exception, "Empty data for flashlist " + flashlistName);
	}
	
	// simple perfromance meauserment
	static size_t counter = 0;
	static toolbox::TimeVal start_ = toolbox::TimeVal::gettimeofday();

	if ((counter % 10000) == 9999)
	{
			//toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();
			//std::cout << "Time: " << (double)(stop - start_) << ", rate: " << 10000/((double)(stop - start_)) << std::endl;
			start_ = toolbox::TimeVal::gettimeofday();
	}
	counter++;
		

}
void xmas::las2g::Application::refreshSubscriptionsToEventing() 
	
{
	if (subscriptions_.empty())
	{
		LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Prepare subscription records");
		const xdaq::Network * network = 0;
		std::string networkName =  this->getApplicationDescriptor()->getAttribute("network");
		try
		{
			network = this->getApplicationContext()->getNetGroup()->getNetwork(networkName);
		}
		catch (xdaq::exception::NoNetwork& nn)
		{
			std::stringstream msg;
			msg << "Failed to access b2in network " << networkName << ", not configured";
			XCEPT_RETHROW (xmas::exception::Exception, msg.str(), nn);
		}
		pt::Address::Reference localAddress = network->getAddress(this->getApplicationContext()->getContextDescriptor());
		
		
		std::set<std::string> topics = toolbox::parseTokenSet ( topic_.toString(), "," );		
		std::set<std::string>::iterator i;
		for (i = topics.begin(); i != topics.end(); ++i)
		{
			if ( subscriptions_.find(*i) == subscriptions_.end())
			{
				xdata::Properties plist;
				toolbox::net::UUID identifier;
				plist.setProperty("urn:b2in-protocol:service", "b2in-eventing"); // for remote dispatching

				plist.setProperty("urn:b2in-eventing:action", "subscribe");
				plist.setProperty("urn:b2in-eventing:id", identifier.toString());
				plist.setProperty("urn:b2in-eventing:topic", (*i)); // Subscribe to collected data
				plist.setProperty("urn:b2in-eventing:subscriberurl", localAddress->toString());
				plist.setProperty("urn:b2in-eventing:subscriberservice", "xmaslas2g");
				plist.setProperty("urn:b2in-eventing:expires", subscribeExpiration_.toString("xs:duration"));
				subscriptions_[(*i)] = plist;

			}
		}
	}
	
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "subscribe/Renew");
	b2in::utils::MessengerCache * messengerCache = 0;
	try
	{
		messengerCache = b2inEventingProxy_->getMessengerCache();
	}
	catch(b2in::utils::exception::Exception & e)
	{
			XCEPT_RETHROW (xmas::exception::Exception, "cannot access messenger cache", e);
	}
	
	// This lock is available all the time excepted when an asynchronous send will fail and a cache invalidate is performed
	// Therefore there is no loss of efficiency.
	//
	
	std::list<std::string> destinations = messengerCache->getDestinations();	
	
	for (std::list<std::string>::iterator j = destinations.begin(); j != destinations.end(); j++ )
	{
		//
		// Subscribe to OR Renew all existing subscriptions
		//
		std::map<std::string, xdata::Properties>::iterator i;
		for ( i = subscriptions_.begin(); i != subscriptions_.end(); ++i)
		{
			try
			{
				// plist is already prepared for a subscribe/resubscribe message
				//
				LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Subscribe to topic " << (*i).first << " on url " << (*j));
				messengerCache->send((*j),0,(*i).second);
			}
			catch (b2in::nub::exception::InternalError & e)
                        {
				std::stringstream msg;
				msg << "failed to send subscription for topic (internal error) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());

				XCEPT_DECLARE_NESTED(xmas::las2g::exception::Exception, ex , msg.str(), e);
				this->notifyQualified("fatal",ex);
				return;
                        }
                        catch ( b2in::nub::exception::QueueFull & e )
                        {                                
				std::stringstream msg;
				msg << "failed to send subscription for topic (queue full) " << (*i).first << " to url " << (*j);

				LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
                                
				XCEPT_DECLARE_NESTED(xmas::las2g::exception::Exception, ex , msg.str(), e);
				this->notifyQualified("error",ex);
				return;
                        }
                        catch ( b2in::nub::exception::OverThreshold & e)
                        {
				// ignore just count to avoid verbosity                                
				std::stringstream msg;
				msg << "failed to send subscription for topic (over threshold) " << (*i).first << " to url " << (*j);
				LOG4CPLUS_ERROR (this->getApplicationLogger(), msg.str());
				return; 
                        }			
		}
		LOG4CPLUS_DEBUG(this->getApplicationLogger(), "done with subscription");
	}
}

//
// XGI SUpport
//
void xmas::las2g::Application::retrieveCatalog(xgi::Input * in, xgi::Output * out ) 
{
	this->retrieveCatalogHTML(in, out); // Out of framework version
}

void xmas::las2g::Application::retrieveCatalogHTML(xgi::Input * in, xgi::Output * out ) 
{
	try
	{
			cgicc::Cgicc cgi(in);

	std::string format = xgi::Utils::getFormElement(cgi, "fmt")->getValue();
	if (format == "plain")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
	}
	else if (format == "csv")
	{
		std::string disposition = "attachment; filename=catalogue.csv;";
		out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
		out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
	}
	else if (format == "html")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
	}
	else if (format == "json")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");
	}
	else
	{
		std::stringstream msg;
		msg << "Unknown requested format '" <<  format << "'";
		(void) out->getHTTPResponseHeader().getStatusCode(415);
		(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(415));
		(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(415),msg.str());
		return;
	}

	if ( format == "html" )
	{
		this->displayFlashListCatalogToHTML(out);
	}
	else if ( format == "xml" )
	{
		this->displayFlashListCatalogToXML(out);
	}
	else if ( format == "json" )
	{
		this->displayFlashListCatalogToJSON(out);
	}
	else
	{
		this->displayFlashListCatalogToCSV(out);
	}
	}
	catch (const std::exception & e)
	{
			XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}

void xmas::las2g::Application::displayFlashListCatalogToHTML( xgi::Output * out ) 
{
	*out << cgicc::table().set("class", "xdaq-table");
	/*
			.set("cellpadding","0")
			.set("cellspacing","0")
			.set("border","")
			.set("style","font-size: 8pt; font-family: arial; text-align: left; width: 100%; background-color: rgb(255,255,255);");
	*/
	
	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Name"); //.set("title","FlashList name").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::tr(); // << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();
	
	repositoryLock_.take();
	
	for ( std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); ++i )
	{
		if ( (*i).second->hasData() )
		{
			*out << cgicc::tr();
			*out << cgicc::td( (*i).first ); // .set("style","vertical-align: top; font-weight: normal; background-color: rgb(255,255,255);") << std::endl;
			*out << cgicc::tr(); // << std::endl;
		}
	}
	
	repositoryLock_.give();

	
	*out << cgicc::tbody();	
	*out << cgicc::table();
}

void xmas::las2g::Application::displayFlashListCatalogToJSON( xgi::Output * out ) 
{
	*out << "{\"table\":{";
	// definition
	*out << "\"definition\":[{\"key\":\"Name\", \"type\":\"string\"},";
	*out << "{\"key\":\"LastUpdate\", \"type\":\"time\"},";
	*out << "{\"key\":\"Version\", \"type\":\"string\"},";
	*out << "{\"key\":\"LastOriginator\", \"type\":\"string\"},";
	*out << "{\"key\":\"Tag\", \"type\":\"string\"},";
	*out << "{\"key\":\"Rows\", \"type\":\"unsigned int\"}";
	*out << "],";
	// rows
	*out << "\"rows\":[";
	
	repositoryLock_.take();

	std::map<std::string, FlashlistData*>::iterator i = repository_.begin();
	while ( i != repository_.end() )
	{
		xmas::las2g::FlashlistData* data = (*i).second;
		
		data->lock();
		if ( data->hasData() )
		{
			*out << "{\"Name\":\"" << (*i).first << "\", \"LastUpdate\":\"" << data->getLastUpdate().toString("%a, %b %d %Y %H:%M:%S GMT", toolbox::TimeVal::gmt) << "\"";
			*out << ", \"Version\":\"" << data->getVersion() << "\"";
			*out << ", \"Tag\":\"" << data->getTags() << "\"";
			*out << ", \"LastOriginator\":\"" << data->getLastOriginator() << "\"";
			*out << ", \"Size\":" << data->getSize();
			*out << "}";
		}
		data->unlock();
		++i;
		if ( i != repository_.end() )
			*out << ",";
	}
	
	repositoryLock_.give();

	
	*out << "]}";	
	*out << "}";
}


void xmas::las2g::Application::displayFlashListCatalogToXML( xgi::Output * out ) 
{
	*out << "<?xml version=\"1.0\"?><table>";
	
	repositoryLock_.take();

	for ( std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); ++i )
	{
		if ( (*i).second->hasData() )
		{
			*out << "<tr><td>" << (*i).first << "</td></tr>";
		}
	}
	
	repositoryLock_.give();

	
	*out << "</table>";
}

void xmas::las2g::Application::displayFlashListCatalogToCSV( xgi::Output * out ) 
{
	*out << "Name" << std::endl;	
	repositoryLock_.take();
	
	for ( std::map<std::string, FlashlistData*>::iterator i = repository_.begin(); i != repository_.end(); ++i )
	{
		if ( (*i).second->hasData() )
		{		
			*out << (*i).first << std::endl;
		}
	}
	
	repositoryLock_.give();

}

void xmas::las2g::Application::retrieveCollection(xgi::Input * in, xgi::Output * out ) 
{
	this->retrieveCollectionHTML(in, out); // This should do the same thing but without the framework binding
}

void xmas::las2g::Application::retrieveCollectionHTML(xgi::Input * in, xgi::Output * out ) 
{
	std::string name = "";
	std::string format = "";
	std::map<std::string,std::string> filter;
	try
	{
		cgicc::Cgicc cgi(in);
		
		// May raise an xgi::exception::Exception that will be returned automatically
		//
		cgicc::const_form_iterator i = cgi.getElement("delay");
		
		// Delay the request to avoid overload of the las (delay in msec)
		//
		unsigned long delay;
		if (i != cgi.getElements().end())
		{
			delay = 1000 * ((*i).getIntegerValue());
		}
		else
		{
			delay = 100000;
		}
		
		toolbox::u_sleep(delay);
		
		
		const std::vector<cgicc::FormEntry>& elements = cgi.getElements();
		for (size_t ei = 0; ei < elements.size(); ++ei)
		{
			if ( elements[ei].getName() == "flash" )
			{
				name = elements[ei].getValue();
			}
			else if ( elements[ei].getName() == "fmt" )
			{
				format = elements[ei].getValue();
			}
			else if ( elements[ei].getName() == "delay" )
			{
				// ignore this parameter
			}
			else
			{
				filter[ elements[ei].getName() ] = elements[ei].getValue();
			}
		}
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
	
	if (format == "plain")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
		
	}
	else if (format == "csv")
	{
		std::string disposition = toolbox::toString("attachment; filename=%s.csv;", name.c_str());
		out->getHTTPResponseHeader().addHeader("Content-Type", "application/csv");
		out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		
	}
	else if (format == "html")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		
	}
	else if (format == "exdr")
	{
		std::string disposition = toolbox::toString("attachment; filename=%s.exdr;", name.c_str());
		out->getHTTPResponseHeader().addHeader("Content-Type", "application/x-xdata+exdr");
		out->getHTTPResponseHeader().addHeader("Content-transfer-encoding", "binary");
		out->getHTTPResponseHeader().addHeader("Content-Disposition", disposition);
		
	}
	else if (format == "json")
	{
		out->getHTTPResponseHeader().addHeader("Content-Type", "text/json");	
	}
	
	else
	{
		std::stringstream msg;
		msg << "Unknown requested format '" <<  format << "'";
		(void) out->getHTTPResponseHeader().getStatusCode(415);
		(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(415));
		(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(415),msg.str());
		return;
	}
	
	out->getHTTPResponseHeader().addHeader("Expires", "0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "no-store, no-cache, must-revalidate, max-age=0");
	out->getHTTPResponseHeader().addHeader("Cache-Control", "post-check=0, pre-check=0");
	out->getHTTPResponseHeader().addHeader("Pragma", "no-cache");
	
	// Monitoring reports are indexed by flashlist names
	// Each report has properties - if no filter on the properties
	// is specified, a merged data set is delivered
	//
	xmas::las2g::FlashlistData* dataEntry = 0;
	
	repositoryLock_.take();
	
	std::map<std::string, FlashlistData*>::iterator r = repository_.find(name);
	if (r != repository_.end())
	{
		dataEntry = (*r).second;
		
	}
	
	repositoryLock_.give();
	
	
	if (dataEntry == 0)
	{
		std::stringstream msg;
		msg << "Failed to find flashlist '" <<  name << "' in 'retrieveCollection' command";
		(void) out->getHTTPResponseHeader().getStatusCode(500);
		(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(500));
		(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(500), msg.str());
		return;
	}		
	
	try 
	{
		
		if ( format == "html" )
		{
			*out << "<div style=\"color: blue\">Flashlist: " << name << "</div><br />";
			dataEntry->toHTML(*out, filter);
			
		}
		else if ( format == "csv"  || format == "plain"  )
		{
			
			dataEntry->toCSV(*out, filter);
			
		} 
		else if ( format == "json"  )
		{
			dataEntry->toJSON(*out, filter);
			
		} 
		else if ( format == "exdr" )
		{
			
			dataEntry->toEXDR(*out, filter);
		}
	}
	catch(xmas::las2g::exception::Exception & me)
	{
		std::stringstream msg;
		msg << "Failed to retrieve flashlist '" <<  name << "' in 'retrieveCollection' command";
		XCEPT_DECLARE_NESTED (xmas::exception::Exception, exception , msg.str(), me);
		
		(void) out->getHTTPResponseHeader().getStatusCode(500);
		(void) out->getHTTPResponseHeader().getReasonPhrase(xgi::Utils::getResponsePhrase(500));
		(void) out->getHTTPResponseHeader().addHeader("Content-Type", "text/html");
		*out << xgi::Utils::getFailurePage(xgi::Utils::getResponsePhrase(500), xcept::htmlformat_exception_history(exception));
		return;
	}
}


void xmas::las2g::Application::Default(xgi::Input * in, xgi::Output * out ) 
{
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;") << std::endl;
	*out << cgicc::caption("Flash Lists");
	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::th("Flashlist").set("class", "xdaq-sortable") << std::endl;
	*out << cgicc::th("Last Update") << std::endl;
	*out << cgicc::th("Version") << std::endl;
	*out << cgicc::th("Tags") << std::endl;
	*out << cgicc::th("Originator") << std::endl;
	*out << cgicc::th("Size") << std::endl;
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;
	
	*out << cgicc::tbody() << std::endl;
	
	std::map<std::string, xmas::las2g::FlashlistData*>::iterator si; 
	for (si = repository_.begin(); si != repository_.end(); ++si)
	{
		*out << cgicc::tr() << std::endl;

		// output flashlist name
		std::stringstream link;
		link << "<a href=\"";
		link << this->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/";
		link <<  this->getApplicationDescriptor()->getURN() << "/";
		link << "retrieveCollectionHTML?fmt=html&flash=" << (*si).first << "\">" << (*si).first << "</a>";
		
		*out << cgicc::td( link.str() )  << std::endl;
		
		xmas::las2g::FlashlistData* data = (*si).second;
		
		*out << cgicc::td( data->getLastUpdate().toString(toolbox::TimeVal::gmt) )  << std::endl;
		*out << cgicc::td( data->getVersion() )  << std::endl;
		*out << cgicc::td( data->getTags() )  << std::endl;
		*out << cgicc::td( data->getLastOriginator() )  << std::endl;
			
		std::stringstream rows;
		rows << data->getSize();
		*out << cgicc::td( rows.str() )  << std::endl;
		
		*out << cgicc::tr() << std::endl;
	}
	
	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;
}


