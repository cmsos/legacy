package xdaq.xdesk.xcontrol;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.table.*;
import javax.infobus.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.util.*;
import java.applet.*;
import java.beans.*;
import java.io.*;

import org.apache.log4j.*;

import xdaq.xdesk.xtree.*;
import xdaq.xdesk.tools.*;

//
// This class does not only implement a window, but also
// acts as a Infobus producer. It can put configuration trees
// on the Infobus as soon as they are loaded. The tree
// is published as a DefaultMutableTree node, accessed by
// the ImmediateAccess Infobus interface.
//
public class XControlViewer extends JPanel
	implements InfoBusDataProducer, InfoBusSubscriberListener
{
	Logger logger = Logger.getLogger (XControlViewer.class);

	
	XTree configTree_;
	InfoBusMemberSupport infobusSupport_;
	DataItemChangeManagerSupport selectionChangeManager_;
	XControlViewer viewer_ = this; // give access to this class from embedded classes
	XControl controller_;
	String name_;  // name of current open partition
	XPartition xpartition_; // current open partition
	//Container container_; // containing frame pane
	
	// Infobus stuff
	InfoBusPublisher commandPublisher_;
	InfoBusSubscriber replySubscriber_;
	Advertiser scriptAdvertiser_;
	
	JFrame frame_ = null;
	JToolBar toolBar_ = null;
	
	class Script implements ImmediateAccess
	{
		String name_;
		Data data_;
		
		public Script (String name, Data data)
		{
			name_ = name;			
			data_ = data;
		}
		
		// Immediate Access interfaces
		public String getPresentationString (java.util.Locale locale)
		{
			return name_;
		}

		// Returns the TreeModel of the configuration tree
		public Object getValueAsObject()
		{
			return data_;
		}

		// Return name of partition 
		public String getValueAsString()
		{
			
			return name_;
		}

		public void setValue ( Object newValue ) throws InvalidDataException
		{
			// no alterations allowed by clients
			throw new InvalidDataException ("Object is not editable");
		}


	};
	
	// Reply to a previously issued SOAP message
	//
	public synchronized void  update (Object data, Object source)
	{
		MessageSet ms = (MessageSet) data;
		// Check if this reply has been associated with a previous command
		// being sent.
		// For the moment only row 0 is inspected, since it
		// is assumed that a reply table is corresponding to a
		// single request table.
		int failed = 0;

		for (int i = 0; i < ms.size(); i++)
		{
			Message msg = ms.getMessage(i);

			if (msg.getSource() == viewer_)
			{				
				String status = msg.getStatus();
				if (status != null)
				{
					failed++;
					logger.error ("SOAP msg error ("+msg.getUrl().toString()+"): " + status);
				}
			}

		}

		if (failed > 0)
		{
			JOptionPane dialog = new JOptionPane();
			String str = failed + " out of " + ms.size() + " SOAP messages failed. See log window for more information";
			dialog.showMessageDialog (null, str, "SOAP Error", JOptionPane.ERROR_MESSAGE);
		}
  	}
	
	// -----------------------------------------------------------------
	// Inner class implements a wrapper to access the configuration Tree XTree
	// with Infobus interfaces ImmediateAccess and DataItem. Also, the change manager
	// for listeners to changes in the partition tree is implemented here.
	//
	class XPartition extends DataItemChangeManagerSupport implements ImmediateAccess, DataItem
	{
		String name_;
	
		public XPartition (Object source, String name)
		{
			super(source);
			name_ = name;
		}
	
	 	// Immediate Access interfaces
		public String getPresentationString (java.util.Locale locale)
		{
			return new String ("URL to XDAQ configuration schema here");
		}

		// Returns the TreeModel of the configuration tree
		public Object getValueAsObject()
		{
			return viewer_.configTree_.getModel();
		}

		// Return name of partition 
		public String getValueAsString()
		{
			
			return name_;
		}

		public void setValue ( Object newValue ) throws InvalidDataException
		{
			// no alterations allowed by clients
			throw new InvalidDataException ("Configuration tree is not editable");
		}

		// DataItem Interfaces
		public Object getProperty (String propertyName)
		{
			// empty, no properties
			return null;
		}

		// Return a pointer to this class instance. It is the source of the publishing events
		public InfoBusEventListener getSource()
		{
			// Return the super class as a source of the event
			return viewer_;
		}

		public void release()
		{
			// empty, not synchronized
		}

	 
	 }
	 
	 
	// End of inner class
	// -----------------------------------------------------------------


	public JFrame getFrame()
	{
		return frame_;
	}
	public XControl getController ()
	{
		return controller_;
	}

	public XControlViewer ( JFrame father, XControl controller)
	{	
		controller_ = controller;
		frame_ = father;
		
		name_ = null;
                this.setLayout (new BorderLayout());
		
			
		initInfoBus();
		
		//frame.setSize (300, 200);
		xpartition_ = null;	
		
		// --------------------------------------------------
		// InfoBus attachment
		commandPublisher_ = new InfoBusPublisher ("xcontrolbus");
		replySubscriber_ = new InfoBusSubscriber ("xcontrolbus");
		commandPublisher_.publishItem ("xcommand");
		replySubscriber_.subscribeItem ("xreply", this );
		scriptAdvertiser_ = new Advertiser ("xdatabus", "script");
		// --------------------------------------------------
		
	}
    
	JMenuBar getJMenuBar()
	{
		return frame_.getJMenuBar();
	}
	
	JToolBar getJToolBar()
	{
		return toolBar_;
	}
        
	public void displayConfiguration (String filename)
	{
		if (controller_.getPartition(filename) != null)
		{
			logger.error ("Partition " + filename + " already open");
			JOptionPane dialog = new JOptionPane();
			String str = "Partition already open. See log window for more information";
			dialog.showMessageDialog (null, str, "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		controller_.removePartition (name_);
		controller_.addPartition (filename, this);
		name_ = filename;
		
		frame_.setTitle ("XControl: " + filename);
	
		// revoke previous partition
		if ( xpartition_ != null )
		{
			// revoke
			infobusSupport_.getInfoBus().fireItemRevoked("xtree",this);
			// revoke all previous advertised scripts
			scriptAdvertiser_.revokeAll();
		} 
	
		//File name = new File(filename);
		//name_ = name.getName(); // partition name		
	
		// Load file, create JTree and display it.
		configTree_ = new XTree ( filename );
		configTree_.addTreeSelectionListener ( new XControlTreeSelectionListener ( this ) );
		//Container container = super.getContentPane();
		this.removeAll();
		toolBar_ = new XControlToolBar(this);
		this.add(toolBar_, BorderLayout.NORTH);
		this.add ( new JScrollPane(configTree_), BorderLayout.CENTER);
		this.doLayout();
		this.revalidate();
		
		xpartition_ = new XPartition(this, name_) ;
		
		// Makes the configuration available, because this class is also a DataITem and ImmediateAccess
		infobusSupport_.getInfoBus().fireItemAvailable ("xtree",null,this);	
		
		// Find all scripts and advertise them
		
		org.w3c.dom.Document doc = configTree_.getDocument();
		org.w3c.dom.NodeList commands = doc.getElementsByTagName ("Script");
		for (int i = 0; i < commands.getLength(); i++)
		{
			org.w3c.dom.Node cmd = commands.item(i);
			org.w3c.dom.Node n = cmd.getFirstChild();
			String script = null;

			while (n != null)
			{
       				if (n instanceof org.w3c.dom.CDATASection)
                        	{
                       	 		org.w3c.dom.CDATASection cdata = (org.w3c.dom.CDATASection) n;
                               		script = cdata.getData();
                               		break;
                        	} else
                        	{
                               		 // Otherwise, the script is in the value section of an
                               		 // ordinary text node.
                               		 org.w3c.dom.Text text = (org.w3c.dom.Text) n;
                               		 script = text.getNodeValue();
                        	}
				n = n.getNextSibling();
			}
			
			String scriptName = filename + "/" + cmd.getAttributes().getNamedItem ("name").getNodeValue();
			
			// scriptTYpe should follow MIME spec. e.g: script/tcl
			//
			String scriptType = cmd.getAttributes().getNamedItem ("language").getNodeValue();
			
			try {
				Data d = new Data();
				d.addDataFlavor (script, DataFlavor.stringFlavor);
				DataFlavor df = new DataFlavor( scriptType );
				d.addDataFlavor (script, df );
				Script s = new Script (scriptName, d);
				scriptAdvertiser_.advertise (s);
			} catch (Exception e)
			{
				logger.error (e.toString());
			}
		}
	}
	
	
	public void publishCommand (String command)
	{
		// PUT dataitem on infobus
		// getPaths(), command, source=this
		MessageSet ms = MessageFactory.createMessageSet (this.getPaths(), command, viewer_);
		commandPublisher_.fireItemValueChanged ( "xcommand", ms);
	}
	
	// Fire 
	public void publishTreeChanged ()
	{
		// PUT dataitem on infobus
		// getPaths(), command, source=this
	//	SelectionTableModel selection = new SelectionTableModel( getPaths(), "tree", controller_);	
	//	propertiesChangeManager_.fireItemValueChanged ( selection, null);
	}
	
	
	public TreePath[] getPaths()
	{
		return configTree_.getSelectionModel().getSelectionPaths();
	}
		
	protected void initInfoBus()
	{
		infobusSupport_ = new InfoBusMemberSupport (null);
		try 
		{
			infobusSupport_.joinInfoBus("xcontrolbus");
			infobusSupport_.getInfoBus().addDataProducer (this);
			selectionChangeManager_ = new DataItemChangeManagerSupport (this);

		} catch (Exception e) 
		{
			logger.error ("Cannot access infobus xcontrolbus: " + e.toString());
		}
	}
	
	// -- Infobus interfaces
  
  	// A consumer requests to listen to xcommand. Pass it the change manager for subscription
	public void dataItemRequested (InfoBusItemRequestedEvent e) 
	{
		if (e.getDataItemName().equals ("xtree"))
		{
			if ( name_ != null ) 
			{
				e.setDataItem (  xpartition_ );
			}	
		}
 	 }

	public void propertyChange (PropertyChangeEvent e)
  	{
  
  	}
	
	
	
	void shutdown()
	{
		// revoke partition
		if ( xpartition_ != null )
		{
			System.out.println("revoke partition");
			// revoke
			scriptAdvertiser_.leave();
			infobusSupport_.getInfoBus().fireItemRevoked("xtree",this);
			controller_.removePartition(name_);
			xpartition_ = null;
			name_ = null;
		}
		 
		try 
		{
			infobusSupport_.getInfoBus().removeDataProducer (this);
			infobusSupport_.leaveInfoBus();
		} catch (Exception e)
		{
			logger.error (e.toString());
		}
	}
	
}	
