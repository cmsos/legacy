package xdaq.xdesk.xscript;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.infobus.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.Graphics;
import java.io.*;

import org.apache.log4j.*;
import xdaq.xdesk.tools.*;
import xdaq.xdesk.xtree.*;


public class XScript extends JApplet implements MenuBarListener, 
						DiscoveryListener
{
	Logger logger = Logger.getLogger (XScript.class);	
	JFrame frame_ = null;
	
	// Infobus stuff
	InfoBusPublisher frameworkCommandPublisher_ = new InfoBusPublisher("framework");
	Discovery partitionInfoBus_;
	DefaultComboBoxModel partitionComboBoxModel_;
	HashMap partitionMap_;
	
	XScriptViewer viewer_;
	
	class WindowListener extends WindowAdapter
	{	
		public WindowListener ()
		{
	
		}
	
		// window adapter overriding method
		public void windowClosing (WindowEvent e) 
		{
			frame_.setVisible(false);			
		}
	}
	
	public void init() 
	{
		partitionComboBoxModel_ = new DefaultComboBoxModel();
		partitionMap_ = new HashMap();
		frameworkCommandPublisher_.publishItem ("command");
		partitionInfoBus_ = new Discovery ("xcontrolbus", "xtree", this);
		

	}
	
	

	
	
	public void start() 
	{
		if ( frame_ == null ) 
		{
			frame_ = new JFrame();
			frame_.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
			frame_.addWindowListener (new WindowListener());
			XScriptViewer viewer = new XScriptViewer(frame_, this);
			viewer_ = viewer;
			frame_.getContentPane().add( viewer );
			frame_.setSize(this.getWidth(),this.getHeight()); // should use properties from applet spec
			frame_.setJMenuBar(new XScriptMenuBar(this));
			frame_.show();
			viewer.setDividerLocation();
			
			viewer.setPartitionComboBoxModel ( partitionComboBoxModel_ );
		}
		else
		{
			frame_.setVisible(true);
		}	
		
		
  	}

  /**
     Removes self as InfoBus data consumer. Removes self as data item changed 
     listener so it will no longer receive item value changed updates.
   */
  	public void stop() 
	{
   
  	}

  /**
     Removes the label, stops the applet, and leaves the InfoBus.
   */
  	public void destroy() 
	{
  		frame_.setVisible(false);
		frame_.dispose();
		
		viewer_.shutdown();
		
		// Leave the Info Bus	
		frameworkCommandPublisher_.shutdown();
		partitionInfoBus_.leave();
  	}
	
	
	public XScriptViewer getViewer()
	{
		return viewer_;
	}
	
	
	
	// Menu bar Listener methods	
	public void aboutAction()
	{
		System.out.println("About for this Daqlet");
	}
	
	public void switchAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("tasks",this) );
	}
	
	public void exitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("exit",this) );
	}
	public void quitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("quit",this) );
	}
	
	public void closeAction()
	{
		// close the window frame
		frame_.setVisible(false);
		frame_.validate();	
	}
	
	
	
	
	// Store the partition tree in the model
	//
	public void objectAdvertised(DataItem item)
	{
		ImmediateAccess ia = (ImmediateAccess) item;
		String name = ia.getValueAsString();
		TreeModel tree = (TreeModel) ia.getValueAsObject();
		partitionMap_.put (name, tree);
		partitionComboBoxModel_.addElement(name);
		
	}
	
	public void objectChanged(DataItem item)
	{
		ImmediateAccess ia = (ImmediateAccess) item;
		String name = ia.getValueAsString();
		TreeModel tree = (TreeModel) ia.getValueAsObject();
		partitionMap_.put (name, tree);
	}
	
	public void objectRevoked(DataItem item)
	{
		ImmediateAccess ia = (ImmediateAccess) item;
		String name = ia.getValueAsString();
		partitionMap_.remove (name);
		partitionComboBoxModel_.removeElement(name);
	}
	//
	// ---- End of Infobus support
	
}
