package xdaq.xdesk.jtreeeditor;

public class JTreeEditableText {
  private String text;
  public JTreeEditableText(String str) {
    text = str;
  }

  public String toString() {
    return text;
  }
}
