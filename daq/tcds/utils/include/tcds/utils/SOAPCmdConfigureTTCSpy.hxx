#include "tcds/utils/SOAPUtils.h"

#include <string>

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/pi/TCADevicePI.h"
#include "tcds/pi/TTXLogHelper.h"

template <class T>
tcds::utils::SOAPCmdConfigureTTCSpy<T>::SOAPCmdConfigureTTCSpy(T& controller) :
  SOAPCmdBase<T>(controller, "ConfigureTTCSpy", false)
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdConfigureTTCSpy<T>::executeImpl(xoap::MessageReference const& msg)
{
  // Extract the command parameters and check their values.

  // The logging mode.
  tcds::pi::TCADevicePI::TTC_LOGGING_MODE loggingMode =
    tcds::pi::TCADevicePI::TTC_LOGGING_MODE_ONLY;
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "loggingMode"))
    {
      std::string const tmpMode =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, "loggingMode");
      if (tmpMode == "LogOnly")
        {
          loggingMode = tcds::pi::TCADevicePI::TTC_LOGGING_MODE_ONLY;
        }
      else if (tmpMode == "LogAllExcept")
        {
          loggingMode = tcds::pi::TCADevicePI::TTC_LOGGING_MODE_EXCEPT;
        }
      else
        {
          XCEPT_RAISE(tcds::exception::ValueError,
                      toolbox::toString("The value '%s' is not valid as logging mode.",
                                        tmpMode.c_str()));
        }
    }
  this->controller_.getHw().setTTCSpyLoggingMode(loggingMode);

  // The logging logic operator.
  tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC loggingLogic =
    tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC_OR;
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "triggerTermCombinationOperator"))
    {
      std::string const tmpLogic =
        tcds::utils::soap::extractSOAPCommandParameterString(msg, "triggerTermCombinationOperator");
      if (tmpLogic == "OR")
        {
          loggingLogic = tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC_OR;
        }
      else if (tmpLogic == "AND")
        {
          loggingLogic = tcds::pi::TCADevicePI::TTC_LOGGING_LOGIC_AND;
        }
      else
        {
          XCEPT_RAISE(tcds::exception::ValueError,
                      toolbox::toString("The value '%s' is not valid as trigger-term combination operator.",
                                        tmpLogic.c_str()));
        }
    }
  this->controller_.getHw().setTTCSpyLoggingLogic(loggingLogic);

  // Now all the trigger mask components.
  bool brcBC0 = false;
  bool brcEC0 = false;
  uint32_t brcVal0 = 0x0;
  uint32_t brcVal1 = 0x0;
  uint32_t brcVal2 = 0x0;
  uint32_t brcVal3 = 0x0;
  uint32_t brcVal4 = 0x0;
  uint32_t brcVal5 = 0x0;
  uint32_t brcDDDD = 0x0;
  uint32_t brcTT = 0x0;
  bool brcDDDDAll = false;
  bool brcTTAll = false;
  bool brcAll = false;
  bool addAll = false;
  bool l1a = false;
  bool brcZeroData = false;
  bool adrZeroData = false;
  bool errCom = false;

  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcBC0"))
    {
      brcBC0 = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "brcBC0");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcEC0"))
    {
      brcEC0 = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "brcEC0");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcVal0"))
    {
      brcVal0 = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcVal0");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcVal1"))
    {
      brcVal1 = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcVal1");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcVal2"))
    {
      brcVal2 = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcVal2");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcVal3"))
    {
      brcVal3 = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcVal3");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcVal4"))
    {
      brcVal4 = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcVal4");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcVal5"))
    {
      brcVal5 = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcVal5");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcDDDD"))
    {
      brcDDDD = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcDDDD");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcTT"))
    {
      brcTT = tcds::utils::soap::extractSOAPCommandParameterUnsignedInteger(msg, "brcTT");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcDDDDAll"))
    {
      brcDDDDAll = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "brcDDDDAll");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcTTAll"))
    {
      brcTTAll = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "brcTTAll");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcAll"))
    {
      brcAll = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "brcAll");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "addAll"))
    {
      addAll = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "addAll");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "l1a"))
    {
      l1a = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "l1a");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "brcZeroData"))
    {
      brcZeroData = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "brcZeroData");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "adrZeroData"))
    {
      adrZeroData = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "adrZeroData");
    }
  if (tcds::utils::soap::hasSOAPCommandParameter(msg, "errCom"))
    {
      errCom = tcds::utils::soap::extractSOAPCommandParameterBool(msg, "errCom");
    }

  // Do what we were asked to do.
  this->controller_.getHw().disableTTCSpy();
  this->controller_.getHw().resetTTCSpy();
  this->controller_.piDAQLoop_.clearTTCSpyHistory();
  this->controller_.getHw().setTTCSpyTriggerMask(brcBC0,
                                                 brcEC0,
                                                 brcVal0,
                                                 brcVal1,
                                                 brcVal2,
                                                 brcVal3,
                                                 brcVal4,
                                                 brcVal5,
                                                 brcDDDD,
                                                 brcTT,
                                                 brcDDDDAll,
                                                 brcTTAll,
                                                 brcAll,
                                                 addAll,
                                                 l1a,
                                                 brcZeroData,
                                                 adrZeroData,
                                                 errCom);
  this->controller_.getHw().enableTTCSpy();

  // Send reply.
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
