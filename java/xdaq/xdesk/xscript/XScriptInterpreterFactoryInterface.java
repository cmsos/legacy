package xdaq.xdesk.xscript;


public interface  XScriptInterpreterFactoryInterface {

	XScriptInterpreter createInterpreter(DataFlavor flavor);

}


