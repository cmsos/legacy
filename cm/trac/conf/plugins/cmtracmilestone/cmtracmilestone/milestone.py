from trac.core import *
from trac.db import DatabaseManager
from trac.util.html import html
from trac.perm import IPermissionRequestor
from trac.web import IRequestHandler
from trac.web.chrome import add_ctxtnav, add_script, add_stylesheet, INavigationContributor, ITemplateProvider
from trac.env import IEnvironmentSetupParticipant
from trac.ticket import Milestone
from trac.db import Table, Column, Index
from trac.util.translation import _
from trac.util.datefmt import parse_date

from datetime import datetime
from trac.util.datefmt import to_timestamp, utc

from genshi.builder import tag

import re

import subprocess

class MilestonePluginError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

class MilestonePlugin(Component):
	implements(INavigationContributor, IRequestHandler, ITemplateProvider, IPermissionRequestor)

	# IPermissionRequestor methods
	def get_permission_actions(self):
		return [
			'CM_TRAC_MILESTONE_VIEW',
			'CM_TRAC_MILESTONE_CREATE',
			'CM_TRAC_MILESTONE_DELETE',
			'CM_TRAC_MILESTONE_MODIFY',
      ('CM_TRAC_MILESTONE_ADMIN', ['CM_TRAC_MILESTONE_VIEW', 'CM_TRAC_MILESTONE_CREATE', 'CM_TRAC_MILESTONE_DELETE', 'CM_TRAC_MILESTONE_MODIFY'])
			]

	# INavigationContributor methods
	def get_active_navigation_item(self, req):
		return 'cmtracmilestone'

	def get_navigation_items(self, req):
		if 'CM_TRAC_MILESTONE_VIEW' in req.perm:
			yield ('mainnav', 'milestone', html.A('Milestone', href=req.href.cmtracmilestone()))

#	def parseMilestoneIdentifier(self, identifier)
#		m=re.match('^(.*) v([0-9]+).([0-9]+).([0-9]+)$', milestone.lower())
#		if m == True:
#			return (m.group(1), int(m.group(2)), int(m.group(3)), int(m.group(4)))
#		else
#			return None

	# IRequestHandler methods
	def match_request(self, req):
		if req.path_info == '/cmtracmilestone':
			return True
		elif req.path_info == '/cmtracmilestone/create':
			return True
		elif req.path_info.startswith('/cmtracmilestone/view/'):
			return True
		else:
			return False

	def createMilestone(self, req):
		req.perm.require('CM_TRAC_MILESTONE_CREATE')
		name = req.args.get('name')
		version = req.args.get('version')
		due = req.args.get('due')
		description = req.args.get('description')

		# check arguments
		versions=re.findall('^([0-9]+.[0-9]+.[0-9]+)$', version)
		if len(versions) != 1 :
			raise MilestonePluginError('Please enter a proper version')

		if due == None:
			duetime = 0
		elif due == '':
			duetime = 0
		else:
			try:
				duetime = to_timestamp(parse_date(due, req.tz))
			except ValueError:
				raise MilestonePluginError('The due date you specified is invalid or not compliant to DD/MM/YY format')

		identifier = name + " V" + version;

		db = self.env.get_db_cnx()
		cursor = db.cursor()
		try:
			cursor.execute("INSERT INTO milestone(name, due, completed, description) \
				VALUES ('%s', '%ld', 0, '%s')" % (identifier, duetime, description))
			db.commit()
		except:
			db.rollback()
			raise
			#raise MilestonePluginError('The milestone you are trying to create is already existing')

		return identifier

	def modifyMilestone(self, req):
		req.perm.require('CM_TRAC_MILESTONE_MODIFY')
		identifier = req.args.get('id')
		name = req.args.get('name')
		version = req.args.get('version')
		due = req.args.get('due')
		description = req.args.get('description')

		#check arguments
		versions=re.findall('^([0-9]+.[0-9]+.[0-9]+)$', version)
		if len(versions) != 1 :
			raise MilestonePluginError('Invalid version string specified')

		if due == None:
			duetime = 0
		elif due == '':
			duetime = 0
		else:
			try:
				duetime = to_timestamp(parse_date(due, req.tz))
			except ValueError:
				raise MilestonePluginError('Modification of milestone '+identifier+' failed due to invalid due date')

		if req.args.get('completed') == 'true':
			now = datetime.now(utc)
			completed = to_timestamp(now)
		else:
			completed = 0

		newidentifier = name+" V"+version
		db = self.env.get_db_cnx()
		cursor = db.cursor()
		try:
			cursor.execute("UPDATE milestone \
				SET name='%s', due='%ld', completed='%ld', description='%s' \
				WHERE name='%s'" % (newidentifier, duetime, completed, description, identifier))
			# Update all links to this ticket
			cursor.execute("UPDATE ticket \
				SET milestone='%s' \
				WHERE milestone = '%s'" % (newidentifier, identifier))
			db.commit()
		except:
			db.rollback()
			raise
			#raise MilestonePluginError('Modification of milestone '+identifier+' failed')

		return newidentifier

	def deleteMilestone(self, req):
		req.perm.require('CM_TRAC_MILESTONE_DELETE')

		identifier = req.args.get('id')

		db = self.env.get_db_cnx()
		cursor = db.cursor()
		cursor.execute("SELECT count(*) FROM ticket WHERE milestone='%s'" %(identifier))
		row = cursor.fetchone()
		if not row:
			raise MilestonePluginError('ticket table could not be queried')
		elif int(row[0]) != 0:
			raise MilestonePluginError('Milestone '+identifier+' cannot be deleted as there are tickets associated')
		else:
			try:
				cursor.execute("DELETE FROM milestone \
					WHERE name = '%s'" % (identifier))
				db.commit()
			except:
				db.rollback()
				raise
				#raise MilestonePluginError('Deletion of milestone '+identifier+' failed')

	def process_request(self, req):
		data = {}
		template = 'cmtracmilestoneerrorpage.html'

		add_ctxtnav(req, tag.a(_('View Milestone List'), href=req.href.cmtracmilestone()))
		add_ctxtnav(req, tag.a(_('Create Milestone'), href=req.href.cmtracmilestone('create')))

		db = self.env.get_db_cnx()

		# check all posts (modify, delete and create)
		if req.method == 'POST':
			action = req.args.get('action')
			if action == 'modify':
				try:
					identifier = self.modifyMilestone(req)
					req.redirect(req.href.cmtracmilestone('view/'+identifier))
				except MilestonePluginError, e:
					return ('cmtracmilestoneerrorpage.html', {'error': e.__str__()}, None)
			elif action == 'delete':
				try:
					self.deleteMilestone(req)
					req.redirect(req.href.cmtracmilestone())
				except MilestonePluginError, e:
					return ('cmtracmilestoneerrorpage.html', {'error': e.__str__()}, None)
			elif action == 'create':
				try:
					identifier = self.createMilestone(req)
					req.redirect(req.href.cmtracmilestone('view/'+identifier))
				except MilestonePluginError, e:
					return ('cmtracmilestoneerrorpage.html', {'error': e.__str__()}, None)
			else:
				msg = "Plugin called with unknown action '%s'" % (action)
				return ('cmtracmilestoneerrorpage.html', {'error': msg}, None)

		if req.path_info == '/cmtracmilestone':
			req.perm.require('CM_TRAC_MILESTONE_VIEW')
			template = 'cmtracmilestonelist.html'

			cursor = db.cursor()
			cursor.execute("SELECT name, due, completed, description FROM milestone")

			milestones = []
			for row in cursor:
				m=re.match('^(.*) v([0-9]+.[0-9]+.[0-9]+)$', row[0].lower())
				if m != None and len(m.groups()) == 2:
					groups = m.groups()
					milestones.append({
						'name':        groups[0],
						'version':     groups[1],
						'identifier':  row[0],
						'due':         row[1],
						'completed':   row[2],
						'description': row[3],
					})

			data = {
				'milestones': milestones,
			}
		elif req.path_info.startswith('/cmtracmilestone/view/'):
			req.perm.require('CM_TRAC_MILESTONE_VIEW')
			identifier = req.path_info[len('/cmtracmilestone/view/'):]
			cursor = db.cursor()
			cursor.execute("SELECT name, due, completed, description \
				FROM milestone \
				WHERE name='%s'" % (identifier))

			row = cursor.fetchone()
			if row:
				m=re.match('^(.*) v([0-9]+.[0-9]+.[0-9]+)$', row[0].lower())
				if m != None and len(m.groups()) == 2:
					groups = m.groups()
					template = 'cmtracmilestoneview.html'
					data = {
						'name':        groups[0],
						'version':     groups[1],
						'identifier':  row[0],
						'due':         row[1],
						'completed':   row[2],
						'description': row[3],
					}
		elif req.path_info == '/cmtracmilestone/create':
			req.perm.require('CM_TRAC_MILESTONE_CREATE')

			template = 'cmtracmilestonecreate.html'
			data = {
				'error':       '',
				'name':        '',
				'version':     '',
				'due':         '',
				'description': '',
			}

		return (template, data, None)

	# ITemplateProvider methods
	def get_htdocs_dirs(self):
		return []

	def get_templates_dirs(self):
		from pkg_resources import resource_filename
		return [resource_filename(__name__, 'templates')]
