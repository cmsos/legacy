#include "ttc/utils/LockMutex.hh"


// class ttc::Mutex

ttc::Mutex::Mutex(bool recursive)
{
  pthread_mutexattr_init(&attr_);

  if (recursive)
  {
    pthread_mutexattr_settype(&attr_, PTHREAD_MUTEX_RECURSIVE);
  }

  pthread_mutex_init(&mutex_, &attr_);
}


ttc::Mutex::~Mutex()
{
  pthread_mutex_destroy(&mutex_);
  pthread_mutexattr_destroy(&attr_);
}


void ttc::Mutex::lock()
{
  pthread_mutex_lock(&mutex_);
}


void ttc::Mutex::unlock()
{
  pthread_mutex_unlock(&mutex_);
}


// class ttc::MutexHandler

ttc::MutexHandler::MutexHandler(Mutex& mutex)
:
    mutex_(mutex)
{
  mutex_.lock();
}


ttc::MutexHandler::~MutexHandler()
{
  mutex_.unlock();
}
