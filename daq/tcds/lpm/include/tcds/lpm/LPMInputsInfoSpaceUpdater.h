#ifndef _tcds_lpm_LPMInputsInfoSpaceUpdater_h_
#define _tcds_lpm_LPMInputsInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace lpm {

    class TCADeviceLPM;

    class LPMInputsInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      LPMInputsInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                TCADeviceLPM const& hw);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      TCADeviceLPM const& getHw() const;

      TCADeviceLPM const& hw_;

    };

  } // namespace lpm
} // namespace tcds

#endif // _tcds_lpm_LPMInputsInfoSpaceUpdater_h_
