#ifndef __SlinkExpressCore
#define __SlinkExpressCore

#include "hal/HardwareDeviceInterface.hh"
#include "hal/AddressTableInterface.hh"

namespace ferol40
{
    class SlinkExpressCore {
    public:
        enum Slink_ReadWrite { READ, WRITE };
        SlinkExpressCore( HAL::HardwareDeviceInterface *ferol40, HAL::AddressTableInterface *addressTable, std::vector<std::string> dataSource );
        void slinkExpressCommand( uint32_t adr, uint32_t &data, Slink_ReadWrite rw, uint32_t linkno );

        HAL::AddressTableInterface & getAddressTableInterface();
        void read(  std::string item, uint32_t *result, uint32_t linkno = 0 );
        void write( std::string item, uint32_t data,    uint32_t linkno = 0 );

    private:
        HAL::HardwareDeviceInterface *ferol40Device_P;
        HAL::AddressTableInterface *addressTable_P;
	std::vector<std::string> dataSource_;
    };
};

#endif /* __SlinkExpressCore */
