
#include <log4cplus/logger.h>
#include <log4cplus/configurator.h>
#include <appender4oracle/OracleAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>

using namespace log4cplus;
using namespace log4cplus::helpers;


using namespace log4cplus;

int main(int argc, char**argv)
{
	if (argc < 3)
	{
		std::cout << argv[0] << " propertiesFile loopCount" << std::endl;
		return 1;
	}

	std::string properties = argv[1];
	unsigned int loopCount = atoi(argv[2]);

	try 
	{
		helpers::LogLog::getLogLog()->setInternalDebugging(true);
		log4cplus::registerOracleAppenderFactory();
        PropertyConfigurator::doConfigure(properties);
	}
	catch(...) 
	{
		std::cout << "Configuration error." << std::endl;
		return 1;
	}

    Logger root = Logger::getRoot();
    Logger test = Logger::getInstance("test");
    Logger subTest = Logger::getInstance("test.subtest");

	std::cout << "Perform " << loopCount << " log insertions, one every second." << std::endl;

	for (int i = 0; i < loopCount; i++)
	{
		LOG4CPLUS_INFO(subTest, "Thread test log message number " << i);
		::sleep(1);
	}

    return 0;
}


