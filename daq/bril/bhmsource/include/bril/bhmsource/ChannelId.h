#include <string>
#include <iostream>

#ifndef BRIL_BHM_CHANNELID_H
#define BRIL_BHM_CHANNELID_H

namespace bril
 {

  namespace bhm
   {

    enum Beam
     {
      Beam1 = 1,
      Beam2 = 2
     };

    enum Side
     {
      Near = 0,
      Far = 1
     };

    struct ChannelId
     {

      private:
      enum
       {
        s_channel = 0x0F,
        s_side = 0x10,
        s_beam = 0x20,
        s_cal = 0x40,
        s_valid = 0x80
       };

      public:
      ChannelId( unsigned char c = 0 ) :
       m_value( c )
       {}

      ChannelId( int b, int s, int c, bool is_cal = false )
       {
        m_value = c & s_channel;
        m_value |= ( s & 1 ) << 4;
        m_value |= ( ( b - 1 ) & 1 ) << 5;
        m_value |= is_cal ? s_cal : 0;
        m_value |= ( ( c < 10 ) && ( s == 0 || s == 1 ) && ( b == 1 || b == 2 ) ) ? s_valid : 0;
       }

      ChannelId( const std::string& str ) :
       m_value( 0 )
       {
        if ( str.size() < 4 || str.size() > 5 )
          return;
        switch ( str[0] )
         {
          case 'P':
          break;
          case 'M':
          m_value |= s_beam;
          break;
          default:
          return;
         }
        switch ( str[1] )
         {
          case 'N':
          break;
          case 'F':
          m_value |= s_side;
          break;
          default:
          return;
         }
        if ( ( str[2] == 'R' ) && ( str[3] == 'E' ) )
         {
          m_value |= s_cal | s_valid;
          return;
         }
        int c = 10 * ( str[2] - '0' ) + ( str[3] - '0' ) - 1;
        m_value |= c & s_channel;
        m_value |= ( c < 10 ) ? s_valid : 0;
       }

      std::string to_string() const
       {
        std::string ret;
        if ( !( m_value & s_valid ) )
          return ret;
        ret.reserve( 5 );
        ret.push_back( ( m_value & s_beam ) ? 'M' : 'P' );
        ret.push_back( ( m_value & s_side ) ? 'F' : 'N' );
        if ( m_value & s_cal )
          return ret += "REF";
        if ( channel() == 9 )
          return ret += "10";
        ret.push_back( '0' );
        ret.push_back( '1' + channel() );
        return ret;
       }

      unsigned char linearized() const
       {
        return m_value;
       }

      unsigned int quarter() const
       {
        return beam() * 2 + side();
       }

      unsigned int channel() const
       {
        return m_value & s_channel;
       }

      Side side() const
       {
        return ( m_value & s_side ) ? Far : Near;
       }

      Beam beam() const
       {
        return ( m_value & s_beam ) ? Beam2 : Beam1;
       }

      bool calibration() const
       {
        return m_value & s_cal;
       }

      bool valid() const
       {
        return m_value & s_valid;
       }

      static ChannelId qie_channel( Beam b, unsigned int ch )
       {
        return ChannelId( b, ch / 12, ch % 12 );
       }

      private:
      unsigned char m_value;

     };

    inline bool operator < ( ChannelId a, ChannelId b ) //for use in std::map
     {
      return a.linearized() < b.linearized();
     }

    inline bool operator == ( ChannelId a, ChannelId b )
     {
      return a.linearized() == b.linearized();
     }

   }

 }

#endif // BRIL_BHM_CHANNELID_H
