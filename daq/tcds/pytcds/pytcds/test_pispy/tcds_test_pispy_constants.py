###############################################################################
## Various constants used in the TCDS <-> HCAL integration test setup in B904.
###############################################################################

# The network addresses of the physical boards to connect to.
TTCSPY = "ttcspy"
LPM = "ici11"
PI = "pi11"

# # The iCI number (in the LPM) to use.
# ICI_NUMBER = 1

# # The number of the LPM input the PI is connected to (1 or 2).
# LPM_INPUT = 1

# # The pause (in seconds) between two successive B-gos in a software sequence.
# INTER_BGO_PAUSE = .1

# # The spacing (in orbits) between successive B-gos in the firmware B-go trains.
# BGO_TRAIN_SPACING = 10

###############################################################################
