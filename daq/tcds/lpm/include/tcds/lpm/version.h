#ifndef _tcdslpm_version_h_
#define _tcdslpm_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSLPM_VERSION_MAJOR 3
#define TCDSLPM_VERSION_MINOR 13
#define TCDSLPM_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSLPM_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSLPM_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSLPM_VERSION_CODE PACKAGE_VERSION_CODE(TCDSLPM_VERSION_MAJOR,TCDSLPM_VERSION_MINOR,TCDSLPM_VERSION_PATCH)
#ifndef TCDSLPM_PREVIOUS_VERSIONS
#define TCDSLPM_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSLPM_VERSION_MAJOR,TCDSLPM_VERSION_MINOR,TCDSLPM_VERSION_PATCH)
#else
#define TCDSLPM_FULL_VERSION_LIST TCDSLPM_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSLPM_VERSION_MAJOR,TCDSLPM_VERSION_MINOR,TCDSLPM_VERSION_PATCH)
#endif

namespace tcdslpm
{
  const std::string package = "tcdslpm";
  const std::string versions = TCDSLPM_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS LPM.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS LPM.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
