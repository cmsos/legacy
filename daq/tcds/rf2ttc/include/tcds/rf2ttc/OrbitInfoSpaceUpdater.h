#ifndef _tcds_rf2ttc_OrbitInfoSpaceUpdater_h_
#define _tcds_rf2ttc_OrbitInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace rf2ttc {

    class VMEDeviceRF2TTC;

    class OrbitInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      OrbitInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                            tcds::rf2ttc::VMEDeviceRF2TTC const& hw);
      virtual ~OrbitInfoSpaceUpdater();

      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      void resetCounters();

    private:
      tcds::rf2ttc::VMEDeviceRF2TTC const& getHw() const;
      bool nextUpdateIsReset_;

    };

  } // namespace rf2ttc
} // namespace tcds

#endif // _tcds_rf2ttc_OrbitInfoSpaceUpdater_h_
