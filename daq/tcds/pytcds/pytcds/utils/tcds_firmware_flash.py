#!/usr/bin/env python

###############################################################################
## Program the firmware on a TCDS carrier board. Works for both GLIB
## and FC7, although the underlying process is different.
###############################################################################

import sys

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_networking import resolve_network_address

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Flasher(CmdLineBase):

    IMAGE_CHOICES = ["golden", "user", "test", "t1", "t2", "header"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(Flasher, self).__init__(description, usage, epilog)
        self.image_name = None
        self.image_file_name = None
        # End of __init__().

    def setup_parser_custom(self):
        super(Flasher, self).setup_parser_custom()
        parser = self.parser

        # Add the image name argument.
        help_str = "Name of the firmware image to flash"
        parser.add_argument("image_name",
                            type=str,
                            choices=Flasher.IMAGE_CHOICES,
                            help=help_str)

        # Add the image file name argument.
        help_str = "Name of the file containing the firmware image to flash"
        parser.add_argument("image_file_name",
                            type=str,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(Flasher, self).handle_args()
        # Three arguments are expected:
        # - the target device,
        # - the firmware image name,
        # - and the file name of the firmware image.
        # Extract the target.
        self.target = self.args.target
        # Extract the firmware image name.
        self.image_name = self.args.image_name
        self.image_file_name = self.args.image_file_name
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        print "Programming '{0:s}'.".format(self.target)
        carrier = get_carrier_hw(network_address, controlhub_address, verbose=self.verbose)
        carrier.write_firmware(self.image_name,
                               self.image_file_name,
                               verbose=True)
        msg = "Successfully programmed {0:s} image on {1:s}."
        print msg.format(self.image_name, self.target)

        # End of main().

    # End of class Flasher.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to program the firmware of a GLIB/FC7 TCDS carrier board."
    # usage_str = "usage: %prog [OPTIONS] IP IMAGE_NAME FILE_NAME"
    usage_str = None
    tmp = "', '".join(Flasher.IMAGE_CHOICES)
    epilog_str = "IMAGE_NAME: '{0:s}'.".format(tmp)

    res = Flasher(desc_str, usage_str, epilog_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
