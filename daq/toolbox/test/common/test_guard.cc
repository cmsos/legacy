#include <iostream>
#include <string>
#include "toolbox/BSem.h"
#include "toolbox/task/Guard.h"

toolbox::BSem mutex (toolbox::BSem::FULL);

void f()
{
	toolbox::task::Guard<toolbox::BSem> guard(mutex);

	return;
}
int main(int argc, char** argv)
{


	return 0;
}
