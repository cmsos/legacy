#ifndef _bril_dipbridge_Application_h_
#define _bril_dipbridge_Application_h_
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"
#include "xgi/exception/Exception.h"
#include "eventing/api/Member.h"
#include "xdata/Properties.h"
#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Integer32.h"
#include "xdata/Table.h"
#include "xdata/exdr/Serializer.h"
#include "dip/Dip.h"
#include "dip/DipSubscription.h"
#include "dip/DipSubscriptionListener.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/BSem.h"
#include "toolbox/squeue.h"
#include <map>

namespace toolbox{
  namespace task{
    class WorkLoop;
    class ActionSignature;
  }
}

namespace bril{
  namespace dipbridge{
    class dipDataInsert;
    class PubErrorHandler;
    class ServerErrorHandler;
    
    /**
       receive xdata::Table from eventing and publish it to dip
       subscribe to dip topic and send it as xdata::Table to eventing
    */
    class Application : public xdaq::Application,public xgi::framework::UIManager,public eventing::api::Member,public xdata::ActionListener,public toolbox::ActionListener, public DipSubscriptionListener, public toolbox::task::TimerListener{
      
    public:
      XDAQ_INSTANTIATOR();
      // constructor
      Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception);
      // destructor
      ~Application();
      // xgi(web) callback
      void Default(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception);
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // b2in callback
      void onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception);
      // timer callback
      void timeExpired(toolbox::task::TimerEvent& e);
      // dip message handler
      void handleMessage(DipSubscription* dipsub, DipData& message);
      // DipSubscriptionListener methods
      void connected(DipSubscription* dipsub);
      // DipSubscriptionListener methods
      void disconnected(DipSubscription* subscription,char *reason);
      // DipSubscriptionListener methods
      void handleException(DipSubscription* subscription, DipException& ex);    
      // workloop method
      bool publishingToDip( toolbox::task::WorkLoop* wl );

    private:
      void publishDipMessageToEventing( const std::string& dipname, xdata::Table& dipmessage);
      
    protected:      
      std::string m_processuuid;
      std::string m_startsubtimer_name;
      toolbox::BSem m_applock;
      bool m_busready;
      DipFactory* pm_dip;
      toolbox::mem::Pool* pm_memPool;
      PubErrorHandler* pm_puberrorhandler;
      ServerErrorHandler* pm_servererrorhandler;
      // config parameters      
      xdata::String m_bus;   
      xdata::Vector< xdata::String > m_dipSubTopics;
      xdata::String m_dipDataTopic; 
      xdata::Integer32 m_throttle_threshold_millisec;
      // registries
      std::map< std::string,DipSubscription* > m_dipsubs;
      std::map< std::string,dipDataInsert* > m_dippubs;
      std::map< std::string,long long > m_millisecsincelast;
      xdata::exdr::Serializer m_serializer;
      // outdata queue
      std::map< dipDataInsert*, toolbox::squeue< xdata::Table::Reference >* > m_dippubqueuestore;
      // workloops
      toolbox::task::WorkLoop* pm_pubtodip_wl;
      toolbox::task::ActionSignature* pm_pubtodip_as;
    };
}}
#endif
