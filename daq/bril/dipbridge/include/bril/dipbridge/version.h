#ifndef _bril_dipbridge_version_h_
#define _bril_dipbridge_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILDIPBRIDGE_VERSION_MAJOR 1
#define BRILDIPBRIDGE_VERSION_MINOR 8
#define BRILDIPBRIDGE_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILDIPBRIDGE_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILDIPBRIDGE_VERSION_CODE PACKAGE_VERSION_CODE(BRILDIPBRIDGE_VERSION_MAJOR,BRILDIPBRIDGE_VERSION_MINOR,BRILDIPBRIDGE_VERSION_PATCH)
#ifndef BRILDIPBRIDGE_PREVIOUS_VERSIONS
#define BRILDIPBRIDGE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILDIPBRIDGE_VERSION_MAJOR,BRILDIPBRIDGE_VERSION_MINOR,BRILDIPBRIDGE_VERSION_PATCH)
#else
#define BRILDIPBRIDGE_FULL_VERSION_LIST  BRILDIPBRIDGE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILDIPBRIDGE_VERSION_MAJOR,BRILDIPBRIDGE_VERSION_MINOR,BRILDIPBRIDGE_VERSION_PATCH)
#endif

namespace brildipbridge
{
	const std::string package = "brildipbridge";
	const std::string versions = BRILDIPBRIDGE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ brilDAQ dipbridge";
	const std::string description = "bril dipbridge";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
