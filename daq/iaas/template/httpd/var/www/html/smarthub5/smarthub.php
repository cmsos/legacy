<?php
require '../utils/vendor/autoload.php';
include_once ('config.php');

function searchSmarthubServices($url) {
	$response = Network::httpget ( $url . "/" . "urn:xdaq-application:service=xplore/search?filter=(service%3Dxmasheartbeat)&service=peer" );
	$json = json_decode ( $response->getBody(), true );
	return $json;
}

function findAllZones($url)
{
	try
	{
		$zones = searchSmarthubServices($url);
	}
	catch (Exception $e)
	{
		return NULL;
	}
	
	foreach ( $zones ['table'] ['rows'] as $row ) {

		$context = $row ['context'];
		$lid = $row ['id'];
		$service = $row ['service'];
		$zone = $row ['zone'];

		$zonesurls[$zone] = $context;
	}
	return $zonesurls;
}

function searchARCServices($url) {
	//print ("going to retrieve " . $url . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?classname=sentinel::arc::Application\n") ;
	$response = Network::httpget ( $url . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?classname=sentinel::arc::Application" );
	$json = json_decode ( $response->getBody(), true );
	//echo  $response->getBody();
	return $json;
}

?>
