// $Id$
#ifndef _bril_timesource_version_h_
#define _bril_timesource_version_h_
#include "config/PackageInfo.h"
#define BRILTIMESOURCE_VERSION_MAJOR 1
#define BRILTIMESOURCE_VERSION_MINOR 11
#define BRILTIMESOURCE_VERSION_PATCH 0
#define BRILTIMESOURCE_PREVIOUS_VERSIONS

// Template macros
//
#define BRILTIMESOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRILTIMESOURCE_VERSION_MAJOR,BRILTIMESOURCE_VERSION_MINOR,BRILTIMESOURCE_VERSION_PATCH)
#ifndef BRILTIMESOURCE_PREVIOUS_VERSIONS
#define BRILTIMESOURCE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILTIMESOURCE_VERSION_MAJOR,BRILTIMESOURCE_VERSION_MINOR,BRILTIMESOURCE_VERSION_PATCH)
#else
#define BRILTIMESOURCE_FULL_VERSION_LIST  BRILTIMESOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILTIMESOURCE_VERSION_MAJOR,BRILTIMESOURCE_VERSION_MINOR,BRILTIMESOURCE_VERSION_PATCH)
#endif
namespace briltimesource{
  const std::string package = "briltimesource";
  const std::string versions = BRILTIMESOURCE_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ timesource";
  const std::string description = "brildaq nibble frequency clocks. There are simulated clock and real clock following tcds signals.";
  const std::string authors = "Z. Xie";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
