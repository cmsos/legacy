#include "toolboxV.h"
#include "xoapV.h"
#include "xdaqV.h"
#include "ptNTCPV.h"

GETPACKAGEINFO(ptNTCP)

void ptNTCP::checkPackageDependencies() 
{
        CHECKDEPENDENCY(toolbox)
        CHECKDEPENDENCY(xoap)
	CHECKDEPENDENCY(xdaq)
}

set<string, less<string> > ptNTCP::getPackageDependencies()
{
    set<string, less<string> > dependencies;
    ADDDEPENDENCY(dependencies,toolbox);
    ADDDEPENDENCY(dependencies,xoap);
    ADDDEPENDENCY(dependencies,xdaq);
    return dependencies;
}	
