package xdaq.tools.daqlet;

import xdaq.tools.daqlet.Daqlet;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.net.*;

public class DaqletTableModel extends AbstractTableModel
{
	protected String [] columnsNames = { "Daqlet",  "Sharing", "Name", "Class",  "Codebase"};
	
	protected static Icon sharedIcon   =     new ImageIcon ("./icons/shared.gif"); 
	protected static Icon multipleIcon =     new ImageIcon ("./icons/folder.gif"); 
	protected static Icon defaultIcon  =     new ImageIcon ("./icons/daqlet.gif");
			
	class DaqletData
	{
		public String name;
		public String codebase;
		public Icon shared;
		public String code;
		public Daqlet daqlet;
		public String iconName;
		public Icon icon = null;
               
		DaqletData (Daqlet d, Properties p)
		{
		
		
			codebase = p.getProperty("codebase");
			name  = p.getProperty("name");
			if ( p.getProperty("shared").equals("true") ) {
				shared = DaqletTableModel.sharedIcon;
			} else {
				shared = DaqletTableModel.multipleIcon;
			}
			code = p.getProperty("code");
			iconName = d.getParameter("iconURL");
			//System.out.println("got iconName"+iconName);
                       
			if ( iconName != null ) {
                                // set user specified icon
				URL url = null;
    				try {
    					url = new URL(iconName);
					icon = new ImageIcon (url);

				} 
				catch (java.net.MalformedURLException e)
				{
					icon = defaultIcon;
				}
			} else {
                                //set default icon
				 icon = defaultIcon;
			}	
			
			daqlet = d;
		}
		public Object getValueOf(String attribute)
		{
			if ( attribute.equals("Daqlet") ) {
				return icon;
			}
			else if (attribute.equals("Class") ) {
				return code;
			}
			else if (attribute.equals("Sharing") ) {
				return shared;
			}
			else if (attribute.equals("Codebase") ) {
				return codebase;
			}
			else if (attribute.equals("Name") ) {
				return name;
			}
			else 
			{
				return "Unknown Attribute";
			}
		
		}
	};
	
	Vector daqlets_; // stores daqlet pointers
	
	public DaqletTableModel()
	{
		daqlets_ = new Vector();
		
		//multipleIcon = new ImageIcon ("./icons/folder.gif");
		//singleIcon =   new ImageIcon ("./icons/shared.gif");
		//defaultIcon =   new ImageIcon ("./icons/daqlet.gif");
	}
	
	public void add (Daqlet d, Properties p)
	{
		daqlets_.add ( new DaqletData (d, p) );
	}
	
	public Daqlet getDaqlet (int row)
	{
		DaqletData d =  (DaqletData) daqlets_.elementAt(row);
		return d.daqlet;
	}
	
	public int getRowCount()
	{
		return daqlets_.size();
	}
	
	public int getColumnCount()
	{
		return columnsNames.length;
	} 
	
	public String getColumnName (int column)
	{
		
		return columnsNames[column];
	}
	
	public boolean isCellEditable (int nRow, int nCol)
	{
		return false;
	}
	
	public Object getValueAt (int nRow, int nCol)
	{
	
		return ((DaqletData)daqlets_.elementAt (nRow)).getValueOf(getColumnName(nCol));
	}
	
	public String getTitle()
	{
		return "Daqlet list";
	}
	
	public Class getColumnClass(int c ) {
	   Object o = getValueAt(0, c);
           if (o == null) {
                return Object.class;
            } else {
                return o.getClass();
            }
	}
	
}
