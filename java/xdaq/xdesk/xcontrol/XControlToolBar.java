package xdaq.xdesk.xcontrol;


import javax.swing.*;
import java.awt.event.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import java.io.*;
import org.w3c.dom.*;
import java.awt.*;
import org.apache.log4j.*;

import javax.swing.*;
import java.util.*;
import org.apache.log4j.*;

public class XControlToolBar extends JToolBar
{
	Logger logger = Logger.getLogger (XControlMenuBar.class);
	XControlViewer xcontrolViewer_;
	
	public XControlToolBar(XControlViewer xcontrolViewer)
	{
		xcontrolViewer_ = xcontrolViewer;
		this.add(new JLabel("Click host or application to view the toolbar."));
		this.setRollover(true);
		this.setFloatable(false);
	}
	
	// Function to show the host buttons
	public void showHostButtons()
	{
		System.out.println ("Shoiw host buttons.");
		this.removeAll();
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Configure")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Enable")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Disable")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Suspend")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Resume")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Halt")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Clear")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Reset")));
		
		// Force update of graphics
		this.invalidate();
		this.validate();
		this.repaint();
	}
	
	// Function to show the application buttons
	public void showApplicationButtons() 
	{
		this.removeAll();
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Configure")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Enable")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Disable")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Suspend")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Resume")));
		this.add(new JButton(new XControlAction(xcontrolViewer_, "Halt")));
		
		// Force update of graphics
		this.invalidate();
		this.validate();
		this.repaint();
	}
}
