#ifndef _tcds_ttcspy_HwStatusInfoSpaceUpdater_h_
#define _tcds_ttcspy_HwStatusInfoSpaceUpdater_h_

#include "tcds/hwutilstca/HwStatusInfoSpaceUpdaterTCA.h"

namespace tcds {
  namespace utils {
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace ttcspy {

    class TCADeviceTTCSpy;

    class HwStatusInfoSpaceUpdater : public tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA
    {

    public:
      HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::ttcspy::TCADeviceTTCSpy const& hw);
      virtual ~HwStatusInfoSpaceUpdater();

    private:
      tcds::ttcspy::TCADeviceTTCSpy const& getHw() const;

      tcds::ttcspy::TCADeviceTTCSpy const& hw_;

    };

  } // namespace ttcspy
} // namespace tcds

#endif // _tcds_ttcspy_HwStatusInfoSpaceUpdater_h_
