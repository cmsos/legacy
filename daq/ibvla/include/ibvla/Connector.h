// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _ibvla_Connector_h
#define _ibvla_Connector_h

#include "ibvla/exception/Exception.h"

#include <infiniband/verbs.h>

namespace ibvla
{
	class QueuePair;

	class Connector
	{
		public:
			Connector (size_t mtu, bool sendWithTimeout);

			void connect (ibvla::QueuePair &qp, const std::string & destname, const std::string & destport, size_t path) ;

			ibv_mtu mtu_;
			bool sendWithTimeout_;
	};
}

#endif
