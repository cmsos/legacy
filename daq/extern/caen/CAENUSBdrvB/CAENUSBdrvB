#!/bin/bash

# initfile for automatic driver loading for the CAEN VME bridge.
#
# description Driver loading at boot time for the CAEN VME bridge.
#
# processname: tinyproxy
#
# chkconfig: 345 80 20
# description: loads the driver CAENUSBdrvB for the CAEN VME bridge
# 

. /etc/rc.d/init.d/functions

load_driver() {
  device="CAENUSBdrvB"
  group="root"
  mode="777"
  script_path=`dirname $0`
  2> /dev/null
  
  unload_driver
  
  echo "KERNEL==\"v1718_[0-9]\", SYMLINK+=\"usb/%k\", MODE=\"0666\"" > /etc/udev/rules.d/10-CAEN-USB.rules
  udevadm control --reload-rules
  echo CAENUSBdrvB >> /etc/modules
  depmod -a
  modprobe CAENUSBdrvB

  return 0
  
}

checkDriver () {
  device="CAENUSBdrvB"

  /sbin/lsmod | grep -q ${device} || return 1
  ls /dev/usb/${device}_* 2> /dev/null  | grep -q ${device} || return 1

  return 0
}


unload_driver() {
  device="CAENUSBdrvB"

  modprobe -r CAENUSBdrvB
  rm /etc/udev/rules.d/10-CAEN-USB.rules
  udevadm control --reload-rules
  sed -i '/CAENUSBdrvB/d' /etc/modules
  depmod -a  

  return 0
}

case "$1" in
    start)
	echo -n "loading CAEN VME driver CAENUSBdrvB: "
	load_driver
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success
	[ $RETVAL -ne 0 ] && echo_failure
	echo
	;;
    stop)
	echo -n "Unloading CAEN VME driver CAENUSBdrvB: "
	unload_driver
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success
	[ $RETVAL -ne 0 ] && echo_failure
	echo
	;;
    restart)
	$0 stop
	$0 start
	;;
    status)
	echo -n "Check if CAENUSBdrvB driver is loaded: "
	checkDriver
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success
	[ $RETVAL -ne 0 ] && echo_failure
	echo
	;;
    *)
	echo "Usage: caenvme {start|stop|restart|status}"
	exit 1
esac

exit 0


