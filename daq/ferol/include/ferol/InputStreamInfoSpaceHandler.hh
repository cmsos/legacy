#ifndef __InputStreamInfoSpaceHandler
#define __InputStreamInfoSpaceHandler

#include "ferol/FerolStreamInfoSpaceHandler.hh"

namespace ferol
{
    class InputStreamInfoSpaceHandler:public FerolStreamInfoSpaceHandler {
    public:
        InputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS );
    private:
    };
}

#endif /* __InputStreamInfoSpaceHandler */
