#!/usr/bin/env python

###############################################################################
## A simple test application for the TCDS XDAQ control software.
###############################################################################

import ast
import ConfigParser
import json
import os
import random
import signal
import sys
import threading
import time
import urllib2
from bisect import bisect

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_networking import host_ip
from pytcds.utils.tcds_utils_soap import build_xdaq_soap_command_message
from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_1_1
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_1_2
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_DEFAULT

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The (string) identifier of the RunControl session 'owning' the TCDS
# hardware.
RC_SESSION_ID_BASE = "TEST_JEROEN"

# A minimal time period to wait between successive iterations of the
# main, infinite loop (in seconds).
SLEEP_INTERVAL_MAIN = 1.

# The time period to wait between successive 'hardware lease renewal'
# requests (in seconds).
SLEEP_INTERVAL_HW_LEASE_RENEWAL = 5.

# The SOAP protocol version to use.
SOAP_PROTOCOL_VERSION = SOAP_PROTOCOL_VERSION_DEFAULT

###############################################################################

def weighted_choice(choices):
    (values, weights) = zip(*choices)
    total = 0.
    cum_weights = []
    for w in weights:
        total += w
        cum_weights.append(total)
    x = random.random() * total
    i = bisect(cum_weights, x)
    # End of weighted_choice().
    return values[i]

###############################################################################

class FSMTester(CmdLineBase):

    # The mean time spent in each of the states, after which we'll
    # drive on to the next state.
    # STATE_DURATIONS = {
    #     "Halted" : (2., 1.),
    #     "Configured" : (10., 1.),
    #     "Enabled" : (10., 1.),
    #     "Paused" : (5., 1.),
    #     "Failed" : (1., 1.)
    # }
    STATE_DURATIONS = {
        "Halted" : (1., 0.),
        "Configured" : (1., 0.),
        "Enabled" : (1., 0.),
        "Paused" : (1., 0.),
        "Failed" : (1., 0.)
    }

    NEXT_COMMAND = {
        "Halted" : [("Configure", .9), ("Halt", .1)],
        "Configured" : [("Enable", .8), ("Halt", .2)],
        "Enabled" : [("Pause", .4), ("Stop", .4), ("Halt", .2)],
        "Paused" : [("Resume", .3), ("Stop", .3), ("Halt", .1),
                    ("TTCResync", .15), ("TTCHardReset", .15)],
        "Failed" : [("Halt", 1.)],
        }

    #----------

    def pick_rc_session_id(self, val=None):
        if val is None:
            val = random.randint(0, 255)
        self.rc_session_id = "{0:s}_{1:d}".format(RC_SESSION_ID_BASE, val)
        # End of pick_rc_session_id().

    def pick_command_parameters(self, command_name):
        parameters = {}
        if command_name == "Configure":
            parameters = {"hardwareConfigurationString" : ("string", "DEBUG JGH")}
        elif command_name == "Enable":
            parameters = {"runNumber" : ("unsignedInt", self.run_number)}
        # End of pick_command_parameters().
        return parameters

    def send_command_to_all_targets(self, command_name):
        parameters = self.pick_command_parameters(command_name)
        soap_msg = build_xdaq_soap_command_message(self.rc_session_id,
                                                   command_name,
                                                   parameters,
                                                   SOAP_PROTOCOL_VERSION)
        self.send_soap_message_to_all_targets(soap_msg)
        # End of send_command_to_all_targets().

    def send_soap_message_to_all_targets(self, soap_msg):
        for target in self.targets:
            host_name = self.possible_targets[target]["host_ip"]
            port_number = self.possible_targets[target]["port"]
            lid_number = self.possible_targets[target]["lid"]
            soap_reply = send_xdaq_soap_message(host_name,
                                                port_number,
                                                lid_number,
                                                soap_msg,
                                                soap_protocol_version=SOAP_PROTOCOL_VERSION,
                                                verbose=self.verbose)
            soap_fault = extract_xdaq_soap_fault(soap_reply)
            serious_problem = True
            if soap_fault is None:
                serious_problem = False
            else:
                # Handle some known errors that are not necessarily a
                # problem in a testing setup.
                known_error_no_lease_holder = "Without hardware lease holder " \
                                              "only 'Configure' and 'Halt' " \
                                              "FSM commands are allowed"
                known_error_no_callback = "No callback method found for incoming request"
                known_methods_no_callback = [
                    ("tcds::ici::ICIController",
                     ["[urn:xdaq-soap:3.0:TTCHardReset]", "[urn:xdaq-soap:3.0:TTCResync]"])
                ]
                # Handle the known case of a lost hardware lease.
                if soap_fault.find(known_error_no_lease_holder) > -1:
                    msg = "!!! For some reason we lost the hardware lease. " \
                          "Issuing 'Halt' from current state'."
                    print msg
                    self.pick_rc_session_id(0)
                    soap_msg = build_xdaq_soap_command_message(self.rc_session_id,
                                                               "Halt",
                                                               soap_protocol_version=SOAP_PROTOCOL_VERSION)
                    soap_reply = send_xdaq_soap_message(host_name,
                                                        port_number,
                                                        lid_number,
                                                        soap_msg,
                                                        soap_protocol_version=SOAP_PROTOCOL_VERSION,
                                                        verbose=self.verbose)
                    soap_fault = extract_xdaq_soap_fault(soap_reply)
                    if soap_fault is None:
                        serious_problem = False
                elif soap_fault.find(known_error_no_callback) > -1:
                    # Handle all cases of nonexistent callbacks.
                    for (source, methods) in known_methods_no_callback:
                        if soap_fault.find(source) > -1:
                            for method in methods:
                                if soap_fault.find(method) > -1:
                                    serious_problem = False
                                    break
                    if not serious_problem:
                        break

            if serious_problem:
                msg = "SOAP reply indicates a problem: '{0:s}'"
                print >> sys.stderr, msg.format(soap_fault)
                self.exit(1)

        # End of send_soap_message_to_all_targets().

    def resolve_target_host_names(self):
        for (target_name, target_details) in self.possible_targets.iteritems():
            if not target_details.has_key("host_ip"):
                target_details["host_ip"] = host_ip(target_details["host"])
                self.possible_targets[target_name] = target_details
        # End of resolve_target_host_names().

    #----------

    def pre_hook(self):
        self.pick_rc_session_id()
        self.run_number = 0
        self.thread = None
        # The list of possible targets is read from the config file.
        self.possible_targets = {}
        # The list of actual targets comes from the command line.
        self.targets = []
        # End of pre_hook().

    def setup_parser(self):
        # Start with the basic parser configuration.
        super(FSMTester, self).setup_parser()
        # Add the option to choose a custom cfg file.
        help_str = "Configuration file to use [default: %default]"
        self.parser.add_option("--cfg-file",
                               action="store",
                               default="tcds_control.cfg",
                               help=help_str)
        # Add the option to force all targets to 'Halted' at the
        # start.
        help_str = "Force 'Halt' all targets at the start [default: %default]"
        self.parser.add_option("--force-halt-at-start",
                               action="store_true",
                               default=False,
                               help=help_str)
        # End of setup_parser().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).

        # On the command line we require simply a list of targets (but
        # at least one).
        if len(self.args) < 1:
            self.error("At least one target is required.")
        else:
            self.targets = self.args

        self.cfg_file_name = self.opts.cfg_file
        self.force_halt_at_start = self.opts.force_halt_at_start
        self.verbose = self.opts.verbose
        # End of handle_args().

    def main(self):

        # Read the list of all possible targets from the config file.
        cfg_parser = ConfigParser.SafeConfigParser()
        config_file_name = self.cfg_file_name
        if self.verbose:
            print "Reading configuration from '{0:s}'".format(config_file_name)
        if not os.path.exists(config_file_name):
            self.error("Config file '{0:s}' does not exist.".format(config_file_name))
        cfg_parser.read(config_file_name)
        targets = cfg_parser.items("targets")
        for (target_name, target_dict) in targets:
            self.possible_targets[target_name] = ast.literal_eval(target_dict)
        # Resolve all host names once and for all into IP
        # addresses. Otherwise the DNS server gets stressed.
        self.resolve_target_host_names()
        if self.verbose:
            if len(self.possible_targets):
                print "Found the following target applications: "
                target_names = self.possible_targets.keys()
                target_names.sort()
                for target_name in target_names:
                    print "  {0:s}".format(target_name)
            else:
                print "No target applications found"

        # Check that all targets are defined in the config file.
        target_names = self.possible_targets
        for target in self.targets:
            if not target in target_names:
                msg = "'{0:s}' is not a valid target. Possible targets are: '{1:s}'."
                self.error(msg.format(target, "', '".join(sorted(self.possible_targets))))

        ##########

        # Start with halting everything, if requested.
        if self.force_halt_at_start:
            if self.verbose:
                print "Halting all targets."
            self.send_command_to_all_targets("Halt")

        # Then: start the hardware lease updater thread.
        self.stop_thread = False
        self.thread = threading.Thread(target=self.renew_hw_lease, args=())
        self.thread.start()

        url_base = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/update"
        print "Starting main loop. Hit CTRL-C to stop."
        try:
            while True:
                fsm_states = {}
                for target in self.targets:
                    host_name = self.possible_targets[target]["host_ip"]
                    port_number = self.possible_targets[target]["port"]
                    lid_number = self.possible_targets[target]["lid"]
                    url = url_base.format(host_name, port_number, lid_number)
                    tmp = urllib2.urlopen(url)
                    json_data = tmp.read()
                    application_data = json.loads(json_data)
                    fsm_state = application_data["Application state"]["Application FSM state"]
                    fsm_states[target] = fsm_state

                print "Current FSM states:"
                target_names = fsm_states.keys()
                target_names.sort()
                for target in target_names:
                    fsm_state = fsm_states[target]
                    print "  {0:s}: {1:s}.".format(target, fsm_state)

                time_to_wait = None
                # See if all targets are in the same FSM state or not.
                tmp = list(set(fsm_states.values()))
                if len(tmp) != 1:
                    time.sleep(SLEEP_INTERVAL_MAIN)
                    continue
                else:
                    fsm_state = tmp[0]
                    print "  --> all targets are in '{0:s}'".format(fsm_state)
                    (mean, rms) = FSMTester.STATE_DURATIONS.get(fsm_state, (0., 0.))
                    if mean <= 0.:
                        time.sleep(SLEEP_INTERVAL_MAIN)
                        continue
                    else:
                        time_to_wait = mean
                        if rms > 0.:
                            time_to_wait = random.gauss(mean, rms)
                        time_to_wait = max(0., time_to_wait)

                print "  Waiting {0:.1f} seconds.".format(time_to_wait)
                time.sleep(time_to_wait)

                # Move on to the next state.
                transitions = FSMTester.NEXT_COMMAND.get(fsm_state, [])
                msg = "ERROR No possible state transition found from state '{0:s}'."
                if len(transitions) < 1:
                    self.error(msg.format(fsm_state))
                command_name = weighted_choice(transitions)

                # If we're about to send a Configure: update the RC session id.
                if command_name == "Configure":
                    self.pick_rc_session_id()

                # If we're about to send an Enable: increase the run number.
                if command_name == "Enable":
                    self.run_number += 1

                self.send_command_to_all_targets(command_name)

        except KeyboardInterrupt:
            self.exit(0)
        # End of main().

    def renew_hw_lease(self):
        while not self.stop_thread:
            try:
                for target in self.targets:
                    host_name = self.possible_targets[target]["host_ip"]
                    port_number = self.possible_targets[target]["port"]
                    lid_number = self.possible_targets[target]["lid"]
                    soap_msg = build_xdaq_soap_command_message(self.rc_session_id,
                                                               "RenewHwLease",
                                                               soap_protocol_version=SOAP_PROTOCOL_VERSION)
                    soap_reply = send_xdaq_soap_message(host_name,
                                                        port_number,
                                                        lid_number,
                                                        soap_msg,
                                                        verbose=self.verbose)
                    soap_fault = extract_xdaq_soap_fault(soap_reply)
                time.sleep(SLEEP_INTERVAL_HW_LEASE_RENEWAL)
            except KeyboardInterrupt:
                self.exit(0)
        # End of renew_hw_lease()

    def exit(self, status_code):
        print "Ok, stopping..."
        self.stop_thread = True
        if self.thread and self.thread.isAlive():
            self.thread.join()
        sys.exit(status_code)
        # End of exit().

    # End of class FSMTester.

###############################################################################

if __name__ == "__main__":

    desc_str = "A simple test application for the TCDS XDAQ control software."
    FSMTester(desc_str).run()

    print "Done"

###############################################################################
