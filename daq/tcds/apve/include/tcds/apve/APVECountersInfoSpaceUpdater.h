#ifndef _tcds_apve_APVECountersInfoSpaceUpdater_h_
#define _tcds_apve_APVECountersInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace apve {

    class TCADeviceAPVE;

    class APVECountersInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      APVECountersInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                   TCADeviceAPVE const& hw);
      virtual ~APVECountersInfoSpaceUpdater();

    protected:
      tcds::apve::TCADeviceAPVE const& getHw();

      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    };

  } // namespace apve
} // namespace tcds

#endif // _tcds_apve_APVECountersInfoSpaceUpdater_h_
