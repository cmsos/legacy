package xdaq.xdesk.econtrol;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;

import xdaq.xdesk.tools.*;

public class EControlTreeSelectionListener
    implements TreeSelectionListener {
  EControlViewer viewer_;

  EControlTreeSelectionListener(EControlViewer viewer) {
    viewer_ = viewer;
  }

  public void valueChanged(TreeSelectionEvent e) {

    TreePath[] paths = null;
    JTree tree = null;
    int selectionMode = 1;
    if (e.getSource()instanceof JTree) {
      tree = (JTree) e.getSource();
      selectionMode = tree.getSelectionModel().
          getSelectionMode();
      paths = tree.getSelectionPaths();
    }

    // Protect against empty selection
    if (paths == null) {
      return;
    }

    String association = "";
    String instance = "";

    // consistency check
    // remember the last item of the first selection.
    // all other selected items must be the same

    DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[paths.length -
        1].getLastPathComponent();

    IUserObject adapterNode = (IUserObject) node.getUserObject();
    String lastElement = adapterNode.getName();
    System.out.println("HERE " + (paths.length - 1));
    //System.out.println("lastElement = " + lastElement);

    for (int i = 0; i < paths.length - 1; i++) {
      node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();

      String currentElement = ( (IUserObject) node.getUserObject()).getName();
      System.out.println(lastElement + ", " + currentElement + " ," +
                         selectionMode);

      // Transport and Application are equivalent
      if (currentElement.startsWith("Transport")) {
        currentElement = "Application";
      }

      if (!lastElement.startsWith(currentElement) &&
          selectionMode == 4) {
        JOptionPane.showMessageDialog(null,
                                      "Must select nodes of the same type",
                                      "Selection failed",
                                      JOptionPane.ERROR_MESSAGE);
        if (tree != null) {
          tree.clearSelection();

        }
        return;
        //tree.getSelectionModel().removeSelectionPath (paths[i]);
      }
    }

    if (lastElement.startsWith("Host")) {
      ( (EControlMenuBar) viewer_.getJMenuBar()).setHostMenu();
      ( (EControlToolBar) viewer_.getJToolBar()).showHostButtons();

      association = "Host";
      instance = adapterNode.getAttribute("id");
      //System.out.println("Host instance " + instance);

    }
    else if (lastElement.startsWith("Application") ||
             lastElement.startsWith("Transport")) {
      // Check for plugin
      association = adapterNode.getAttribute("class");
      instance = adapterNode.getAttribute("instance");
      //   association="";
      //   instance="";
      ( (EControlMenuBar) viewer_.getJMenuBar()).setApplicationMenu();
      ( (EControlToolBar) viewer_.getJToolBar()).showApplicationButtons();

    }
    else if (lastElement.startsWith("Partition")) {
      //((EControlMenuBar) control_.getJMenuBar()).setPartitionMenu();
      association = "Partition";
      instance = "";
      ( (EControlMenuBar) viewer_.getJMenuBar()).setEmptyMenu();
      ( (EControlToolBar) viewer_.getJToolBar()).removeAll();

    }
    else if (lastElement.startsWith("Script")) {
      //win_.getMenu().setScriptMenu();
      //((EControlMenuBar) control_.getJMenuBar()).setScriptMenu();

      association = "Script";
      instance = "";
    }
    else {
      //association = adapterNode.getNodeName();
      association = adapterNode.getName();
      instance = "";
      ( (EControlMenuBar) viewer_.getJMenuBar()).setEmptyMenu();
      ( (EControlToolBar) viewer_.getJToolBar()).removeAll();
    }
  }
}
