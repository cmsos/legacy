#include "tcds/cpm/TCADeviceCPMT2.h"

#include "tcds/hwlayer/Utils.h"
#include "tcds/hwlayertca/TCACarrierBase.h"
#include "tcds/hwlayertca/TCACarrierAMC13.h"

tcds::cpm::TCADeviceCPMT2::TCADeviceCPMT2() :
  TCADeviceBase(std::auto_ptr<tcds::hwlayertca::TCACarrierBase>(new tcds::hwlayertca::TCACarrierAMC13(hwDevice_)))
{
}

tcds::cpm::TCADeviceCPMT2::~TCADeviceCPMT2()
{
}

std::string
tcds::cpm::TCADeviceCPMT2::readSystemIdImpl() const
{
  // The system ID consists of four characters encoded as a single
  // 32-bit word.
  uint32_t val = hwDevice_.readRegister("system.system_id");
  std::string res = tcds::hwlayer::uint32ToString(val);
  return res;
}

std::string
tcds::cpm::TCADeviceCPMT2::regNamePrefixImpl() const
{
  // All the CPM T2 registers start with 'cpmt2.'.
  return "cpmt2.";
}

void
tcds::cpm::TCADeviceCPMT2::reloadT1() const
{
  writeRegister("control.reload_t1", 0x1);
}

void
tcds::cpm::TCADeviceCPMT2::reloadT2() const
{
  writeRegister("control.reload_t2", 0x1);
}

void
tcds::cpm::TCADeviceCPMT2::resetTTCClockDCM() const
{
  std::string const regName = "control.reset_ttc_clock_dcm";
  writeRegister(regName, 0x0);
  writeRegister(regName, 0x1);
  writeRegister(regName, 0x0);
}

void
tcds::cpm::TCADeviceCPMT2::setupForTCDS() const
{
  // - Make sure we're in TCDS (as opposed to AMC13) mode.
  writeRegister("cpm_t2_control_and_monitor.tcds_mode", 1);
  // - Make sure we're using the clock from the front panel (and not
  //   the one recovered from the BST).
  writeRegister("cpm_t2_control_and_monitor.select_t3_clock", 1);
  // - Make sure we're inverting the right signals.
  writeRegister("cpm_t2_control_and_monitor.t1_system_clock_invert", 0);
  writeRegister("cpm_t2_control_and_monitor.t3_signal_invert", 0);
  // - Make sure all AMC backplane slots are enabled.
  writeRegister("cpm_t2_control_and_monitor.amc_enable", 0xfff);
}
