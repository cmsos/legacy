#include <sstream>
#include "hal/linux/StopWatch.hh"
#include "ferol/OpticalDataSource.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/ferolConstants.h"
#include "ferol/loggerMacros.h"

ferol::OpticalDataSource::OpticalDataSource( HAL::HardwareDeviceInterface *device_P,
                                             ferol::MdioInterface *mdioInterface_P,
                                             utils::InfoSpaceHandler &appIS,
                                             Logger logger)
    : DataSourceIF( device_P, appIS, logger ),
      mdioInterface_P( mdioInterface_P )
{    
}


void
ferol::OpticalDataSource::setup6GInputs() const
{
    // setup the 6G links
    // 
    uint32_t pollres, linkNo = 0;
    try
        {
            if ( stream0_ || ( operationMode_ == FEDKIT_MODE ) ) 
                {
                    DEBUG( "Requiring that link 0 has a SFP plugged in." );
                    if ( ! device_P->isSet("Link5gb_SFP_detected_L0") )
                        {
                            std::stringstream msg;
                            msg << "There seems to be no SFP plugged into the 6Gbps Link number 0. But I need an SFP there to continue! Giving up...";
                            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
                        }
                    // this should be always the case... but just to check...                    
                    linkNo = 0;
                    DEBUG( "Check that the SERDES in link 0 is up." );
                    device_P->pollItem("Link5gb_serdes_up_L0", 1, 1000, &pollres );

                    DEBUG( "Re-Sync sequence numbers on link 0 and clear all buffer memories/fifos." );
                    device_P->writePulse("L5gb_resync_SlX0");
                    
                } 
            
            
            if ( stream1_ || ( (operationMode_ == FEDKIT_MODE) && (dataSource_ == L6G_LOOPBACK_GENERATOR_SOURCE) ) )
                {

                    DEBUG( "Requiring that link 1 has a SFP plugged in." );
                    if ( ! device_P->isSet("Link5gb_SFP_detected_L1") )
                        {
                            std::stringstream msg;
                            msg << "There seems to be no SFP plugged into the 6Gbps Link number 1. But I need an SFP there to continue! Giving up...";
                            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
                        }
                    // this should be always the case... but just to check...
                    linkNo = 1;
                    DEBUG( "Check that the SERDES in link 1 is up." );
                    device_P->pollItem("Link5gb_serdes_up_L1", 1, 1000, &pollres );

                    DEBUG( "Re-Sync sequence numbers on link 1 and clear all buffer memories/fifos." );
                    device_P->writePulse("L5gb_resync_SlX1");

                }
        }
    catch ( HAL::TimeoutException &e )
        {
            std::stringstream err;
            err << "Failed to bring up SERDES of 5Gb link number " << linkNo << "."
                << " (Waited 1000ms.). Check your configuration parameters and if this does not help, check the hardware (fibers plugged correctly? SFP broken?)";
            XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
        }            
}


void
ferol::OpticalDataSource::setup10GInput() const
{
    if ( stream0_ || ( operationMode_ == FEDKIT_MODE ) ) 
        {
            // first read a register from the Vitesse and verify that the settings are
            // as expected. This is important since it sometimes seems to loose the 
            // configuration (especially when the configuration changes)
            uint32_t tmp;
            DEBUG( "Testing the settings of Vitesse 1." );
            mdioInterface_P->mdioRead( 1, 0x8000, &tmp, 1);
            tmp = tmp & 0xffff;
            if ( tmp != 0xb55f )
                {
                    WARN("The Vitesse 1 (input) seems to have lost its configuration. Read 0x" <<
                         std::hex << tmp << " instead of 0xb55f.");
                    device_P->write( "L10gb_SERDES_DOWN_SlX0", 1);
                    ::usleep( 5 ); // to propagate the signal in the internal logic. The internal
                                   // clock is relatively slow
                    device_P->write( "L10gb_SERDES_UP_SlX0", 1 ); 
                    mdioInterface_P->mdioRead( 1, 0x8000, &tmp, 1);
                    tmp = tmp & 0xffff;
                    if ( tmp != 0xb55f ) 
                        {
                            std::stringstream err;
                            err << "The Vitesse 1 did not load after an extra reset (read " << std::hex << tmp << " instead of 0xb55f) ... bailing out" ;
                            ERROR( err.str() );
                            XCEPT_RAISE( utils::exception::FerolException, err.str() );
                        }
                }
            DEBUG( "Settings of Vitesse 1 (10G input link) are ok");


            DEBUG( "Check that the XAUI link for Vitesse 1 (10G input link) is up" );

            uint32_t value;
            device_P->read( "L10gb_SERDES_STATUS_SlX0", &value );
            if ( value == 0 ) 
                {
                    std::stringstream err;
                    err << "The XAUI link between FPGA and Vitesse is not up! This is a hardware failure; cannot continue!";
                    ERROR( err.str() );
                    XCEPT_RAISE( utils::exception::FerolException, err.str());
                }

            DEBUG("serdes ok, now polling for Vitesse link (the 10G input link)");
            uint32_t stop = 0;
            HAL::StopWatch  watch(1);
            stop = 0;
            DEBUG("appis is " << &appIS_ );
            uint32_t vitesseTimeout = appIS_.getuint32( "Vitesse_Timeout_Ms" );
            //uint32_t vitesseTimeout = 30000;
            uint32_t pollres;
            watch.reset();
            watch.start();
            try
                {
                    mdioInterface_P->mdiolock();
                    // See that the MDIO bus is free (It is interally used after a reset)
                    device_P->pollItem( "MDIO_LINK10GB_POLL_A0", 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                    // Now read the status of the Vitesse 
                    device_P->write("MDIO_LINK10GB_A0", 0x00060001 );
                    device_P->pollItem( "MDIO_LINK10GB_POLL_A0", 1, 10000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                    // The setting of the address needs 180us. Experience shows that the polling above does not
                    // work correctly but immediately returns. 
                    watch.stop();
                    while ( (stop == 0) && ( (watch.read()) < 1000 * vitesseTimeout) )
                        {                                    
                            device_P->write("MDIO_LINK10GB_A0", 0x30040000 );
                            device_P->pollItem( "MDIO_LINK10GB_POLL_A0", 1, 1000, &pollres, HAL::HAL_POLL_UNTIL_EQUAL );
                            device_P->read("MDIO_LINK10GB_A0", &pollres );
                            if ( pollres & 0x4 ) 
                                {
                                    stop = 1;
                                    DEBUG("10G input link UP according to Vitesse");
                                }
                            watch.stop();
                        }
                    mdioInterface_P->mdiounlock();
                }
            catch( HAL::TimeoutException &e )
                {
                    std::stringstream err;
                    err << "Could not poll on optical link status. (poll result: " << pollres << " )";
                    ERROR( err.str() );
                    mdioInterface_P->mdiounlock();
                    XCEPT_RETHROW( utils::exception::FerolException, err.str(), e);
                }
        

        
            if ( stop == 1 )
                {
                    DEBUG( "10 Gb input input link is up now! " << std::hex << pollres );
                } 
            else
                {
                    std::stringstream err;
                    err << "The 10 Gb input link did not come up within "<< (watch.read()) << " us." ;
                    ERROR( err.str() );
                    XCEPT_RAISE( utils::exception::FerolException, err.str() );
                }


            DEBUG( "Re-Sync sequence numbers on link 0 and clear all buffer memories/fifos." );
            device_P->writePulse("L10gb_resync_SlX0");

            watch.stop();
            watch.reset();
        }
}
