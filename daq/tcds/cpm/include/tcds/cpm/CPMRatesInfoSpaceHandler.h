#ifndef _tcds_cpm_CPMRatesInfoSpaceHandler_h_
#define _tcds_cpm_CPMRatesInfoSpaceHandler_h_

#include <map>
#include <string>
#include <vector>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cpm {

    class CPMRatesInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      CPMRatesInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                               tcds::utils::InfoSpaceUpdater* updater);
      virtual ~CPMRatesInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    private:
      // These are for internal use, for convenience.
      std::vector<std::string> versions_;
      std::vector<std::string> types_;
      std::map<std::string, std::string> typeDescriptions_;
      std::map<std::string, std::string> typeExplanations_;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_CPMRatesInfoSpaceHandler_h_
