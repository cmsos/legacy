# $Id: Makefile,v 1.27 2009/04/06 15:01:40 gutleber Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# This is the TriDAS/extern/slp Makefile
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Project=$(PROJECT_NAME)
Package=extern/slp

PackageName=slp

PACKAGE_VER_MAJOR=2
PACKAGE_VER_MINOR=0
PACKAGE_VER_PATCH=2

Summary=Service Location Protocol

Description=The OpenSLP project is an effort to develop an open-source implementation of \
Service Location Protocol suitable for commercial and non-commercial application.

Link=http://sourceforge.net/projects/openslp

export CC
export CXX
LD:=$(LDD)
export LD

SLPCONFIG=slp

_all: all

default: all


openslp-2.0.0/configure:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf ./openslp-2.0.0
	tar -zxf openslp-2.0.0.tar.gz
	cd ./openslp-2.0.0; \
	patch -p1 < ../slptool.patch
	
all: openslp-2.0.0/configure
	cd ./openslp-2.0.0; \
	./configure --with-pic --prefix $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM); \
	make;  make install
	mkdir -p $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/etc
	cp slp.conf $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/etc

_installall: install

install: openslp-2.0.0/configure
	cd openslp-2.0.0; \
	./configure --with-pic --prefix $(INSTALL_PREFIX)/$(XDAQ_PLATFORM); \
	make; make install
	mkdir -p $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/etc
	cp slp.conf $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM)/etc

_cleanall: clean

clean:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf ./openslp-2.0.0

load:
	sudo $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/sbin/slpd -c $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/etc/$(SLPCONFIG).conf -l /tmp/slpd.log
	sudo chmod +r /tmp/slpd.log

unload:
	sudo killall slpd

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules
