/**
 * @brief VDM scan xdaq Application.
 *
 * @author Olena
 *
 * This Application collect data from VDM scan, make fast analysis and fire the results to the monitoring service
 *
 */

#ifndef _bril_vdmmonitor_VDMMonitor2_h_
#define _bril_vdmmonitor_VDMMonitor2_h_

#include <string>
#include <vector>
#include <map>
#include <set>

#include "b2in/nub/exception/Exception.h"
#include "eventing/api/Member.h"
#include "log4cplus/logger.h"
#include "toolbox/ActionListener.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/Condition.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/squeue.h"

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xdata/InfoSpace.h"
#include "xdata/String.h"
#include "xdata/Integer.h"
#include "xdata/TimeVal.h"

#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xgi/Method.h"
#include "xgi/Output.h"

#include "bril/vdmmonitor/ScanData.h"
#include "bril/vdmmonitor/ShapeData.h"
#include "bril/vdmmonitor/SigmaData.h"
#include "bril/vdmmonitor/StepData.h"

namespace toolbox
 {
  namespace task
   {
    class WorkLoop;
    class ActionSignature;
   }
 }

namespace bril
 {

  namespace vdmmonitor
   {

     /**
	Subscribe to bril eventing topics: 
	    signal topic vdmflag //mandatory
	    compound topic vdmscan  //mandatory
            compound topic beam //mandatory
            compound topic pltlumizero //configurable
	    compound topic bcm1flumi //configurable
	    compound topic hflumi //configurable
	    compound topic hfetlumi //configurable
	    
         On vdmflag change from 0 to 1 for CMS:
	     start collect data into cache         
	     launch workloop to analyze data collected for the last 30 seconds 
	     then analyze them for each detector
	     
	On vdmflag change from 1 to 0 for CMS:
             fire x,y scan data or xection results to monitoring infospace 
             clean cache after firing
	     
      */
    class VDMMonitor2 : public xdaq::Application,
                        public xgi::framework::UIManager,
                        public eventing::api::Member,
                        public xdata::ActionListener,
                        public toolbox::ActionListener,
                        public toolbox::EventDispatcher
       //public toolbox::task::TimerListener

     {

      XDAQ_INSTANTIATOR();

      VDMMonitor2( xdaq::ApplicationStub* ) throw ( xdaq::exception::Exception );

      ~VDMMonitor2(){}

      virtual void Default( xgi::Input* in, xgi::Output* out ) throw ( xgi::exception::Exception ); // xgi(web) callback
      
      virtual void actionPerformed( xdata::Event& ); // infospace event callback

      virtual void actionPerformed( toolbox::Event& ); // toolbox event callback

      virtual void onMessage( toolbox::mem::Reference*, xdata::Properties& ) throw ( b2in::nub::exception::Exception ); // b2in message callback
      
      //virtual void timeExpired( toolbox::task::TimerEvent& ); // timer callback
      
     private:
      // workloop to fit gaussian to get shape data
      bool fitting(toolbox::task::WorkLoop* wl);
      // workloop to analyse all shapes to get scan sigma data
      bool analyzing(toolbox::task::WorkLoop* wl);      
      // publish StepData to eventing
      void do_publishToEventing( const std::string& provider, const std::string& planename, int stepid, int fillnum, int runnum, const StepData& thisstep );      
      // publish result to eventing
      void do_publishResultToEventing( const std::string& provider, const ShapeData& yShape, const ShapeData& xShape, const SigmaData& sigma);
      // publish result to dip
      void do_publishResultToDip( const std::string& provider, const ShapeData& yShape, const ShapeData& xShape, const SigmaData& sigma);
      
      void clearAllCache();
      void resetStepSumBeam();
      void resetStepSumRate();
      void resetStepCache();
      void resetShapeCache();
      void resetSigmaCache();

      private: 

      VDMMonitor2( const VDMMonitor2& );
      VDMMonitor2& operator = ( const VDMMonitor2& );      

      private: //xdaq stuff

      toolbox::BSem m_applock;

      //configuration params
      xdata::String m_bus;
      xdata::String m_dipbus;
      xdata::String m_diptopic;
      xdata::String m_dipPubRoot;
      xdata::String m_vdmflagTopic;
      xdata::String m_vdmscanTopic;
      xdata::String m_beamTopic;
      xdata::String m_bunchlengthTopic;
      xdata::String m_lumiTopicStr;
      xdata::String m_outtopic;
      xdata::Integer m_maxIntervalSec;
      xdata::Integer m_sleepBeforeSigma;

      //workloop
      toolbox::task::WorkLoop* m_fitting_wl;
      toolbox::task::ActionSignature* m_as_fitting;
      toolbox::task::WorkLoop* m_analyzing_wl;
      toolbox::task::ActionSignature* m_as_analyzing;
      //toolbox::task::Timer* m_timer;    
      
      //monitoring 
      xdata::InfoSpace* m_IS_vdmmon;//monitoring infospace      
      xdata::InfoSpace* m_IS_vdmsigmamon;//monitoring infospace      

      std::list<std::string>  m_vdmshape_flashfields; //field definition for vdmshape flashlist
      std::vector< xdata::Serializable*>   m_vdmshape_flashcontents; //placeholder for vdmshape flash variables
      
      std::list<std::string>  m_vdmsigma_flashfields; //field definition for vdmsigma flashlist
      std::vector< xdata::Serializable*>  m_vdmsigma_flashcontents; //placeholder for vdmshape flash variables   
      
      //eventing publisher
      bool m_canpublish;
      bool m_dipcanpublish;
      toolbox::mem::MemoryPoolFactory *m_poolFactory;
      toolbox::mem::Pool* m_memPool;
      
      private: //cache for processing 
      
      std::set<std::string> m_lumitopics;

      bool m_vdmflag;
      bool m_stepOnFired;

      bool m_scancms;
      bool m_stepon;
      toolbox::TimeVal m_lastvdmoff_time;

      bool m_fit_ongoing_flag;

      ScanData m_scandata; //cache for scan data

      std::map< std::string, std::vector< ShapeData > > m_shapedata_cache; //luminometer shape result cache , vector length is number of planes=2

      std::map< std::string, SigmaData > m_sigmadata_cache; //luminometer sigma result cache

      std::map< std::string, std::vector< StepData > > m_stepcache_luminometer; //luminometer step data cache, vector length is number of steps

      int m_beammessage_counter;

      std::map< std::string,  std::vector< std::vector<float> > > m_stepsum_rate; //luminometer rate cache, vector size is the number of messages of this step, inner vector of float  is fixed size of 3564

      std::vector<float> m_stepsum_beam1intensities; //beam1 cache of fixed length

      std::vector<float> m_stepsum_beam2intensities; //beam2 cache of fixed length

     private:      
      // declare variables in the mon infospace
      void declareFlashlists();
      // initialize infospace variables so that they start from zero
      void init_shapeFlashlist();
      // initialize infospace variables so that they start from zero
      void init_sigmaFlashlist();
      //test if stepcache is empty 
      bool isEmptyStepCache()const;
      //test if shapecache is empty 
      bool isEmptyShapeCache()const;      
      
     }; //end class VDMMonitor2

   } //end ns vdmmonitor

 } //end ns bril

#endif

