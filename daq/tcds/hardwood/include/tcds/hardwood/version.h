#ifndef _tcds_hardwood_version_h_
#define _tcds_hardwood_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSHARDWOOD_VERSION_MAJOR 3
#define TCDSHARDWOOD_VERSION_MINOR 13
#define TCDSHARDWOOD_VERSION_PATCH 1

// If any previous versions available:
// #define HARDWOOD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef HARDWOOD_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSHARDWOOD_VERSION_CODE PACKAGE_VERSION_CODE(TCDSHARDWOOD_VERSION_MAJOR,TCDSHARDWOOD_VERSION_MINOR,TCDSHARDWOOD_VERSION_PATCH)
#ifndef TCDSHARDWOOD_PREVIOUS_VERSIONS
#define TCDSHARDWOOD_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSHARDWOOD_VERSION_MAJOR,TCDSHARDWOOD_VERSION_MINOR,TCDSHARDWOOD_VERSION_PATCH)
#else
#define TCDSHARDWOOD_FULL_VERSION_LIST TCDSHARDWOOD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSHARDWOOD_VERSION_MAJOR,TCDSHARDWOOD_VERSION_MINOR,TCDSHARDWOOD_VERSION_PATCH)
#endif

namespace tcdshardwood {

  const std::string package = "tcdshardwood";
  const std::string versions = TCDSHARDWOOD_FULL_VERSION_LIST;
  const std::string description = "Reference implementation for a XDAQ application communicating with a TCDS control application.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "TCDS control application reference implementation.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdshardwood

#endif
