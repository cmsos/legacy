#!/usr/bin/env python

###############################################################################
## Configure the PI to report OOS on its TTS output.
###############################################################################

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PIOOS(CmdLineBase):

    def main(self):
        pi = get_pi_hw(self.ip_address, verbose=self.verbose)
        if not pi:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        if self.verbose:
            print "Forcing TTS OOS state."

        pi.write("pi.tts_output_control.tts_out_disabled_value", 0x2)
        pi.write("pi.tts_output_control.tts_out_enable", 0x0)

        # End of main().

    # End of class PIOOS.

###############################################################################

if __name__ == "__main__":

    PIOOS().run()
    print "Done"

###############################################################################
