package xdaq.tools.controller;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.table.*;
import java.security.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.event.*;
import java.io.*;
import java.util.*;


import javax.xml.parsers.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import javax.swing.*;
import javax.swing.text.*;



public class Controller extends JApplet implements ItemListener
{
	URL url;
	JEditorPane requestArea = null;
	JEditorPane responseArea = null;
	JTable table = null;
	Controller c = this;

	String soapEnvelopeOpen  = "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">\n";
	String soapHeaderOpen    = "\t<SOAP-ENV:Header>\n";
	String soapHeaderClose   = "\t</SOAP-ENV:Header>\n";
	String soapBodyOpen      = "\t<SOAP-ENV:Body>\n";
	String soapBodyClose     = "\t</SOAP-ENV:Body>\n";
	String soapEnvelopeClose = "</SOAP-ENV:Envelope>";
	String currentBody       =  "";
	String soapRelayOpen    = "\t\t<xr:relay SOAP-ENV:actor=\"http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10\" xmlns:xr=\"http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10\">\n";
	String currentRelay     = "";
	String soapRelayClose   = "\t\t</xr:relay>\n";
	
	class CommandRecord 
	{
		String prefix = "";
		String name = "";
		String namespaceURI = "";
		
		public CommandRecord(String prefix, String name, String namespaceURI )
		{
			this.prefix = prefix.trim();
			this.name = name.trim();
			this.namespaceURI = namespaceURI.trim();
		}
		
		public String toString()
		{
		
			return prefix+":"+name;
		}
		
		public String toXML()
		{
		
			return "\t\t<" + prefix + ":" + name + " xmlns:" + 
			prefix + "=\"" + namespaceURI + "\">\n\t\t</" + prefix + ":" + name + ">";
		}
	}
	
	
	
	public void displaySOAP()
	{
	
		requestArea.setText(soapEnvelopeOpen + 
				    soapHeaderOpen + 
				    soapRelayOpen +
				    currentRelay +
				    soapRelayClose +
				    soapHeaderClose +
				    soapBodyOpen + 
				    currentBody +
				    soapBodyClose +
			            soapEnvelopeClose );
	}
	
	// combo box listener callback
	public void itemStateChanged (ItemEvent e )
	{
		currentBody =  ((CommandRecord)e.getItem()).toXML() + "\n";
		this.displaySOAP();
		
	}
	
	public void init()
	{	
		//ControlTableModel model = new ControlTableModel();
	GTableModel model = new GTableModel();
		model.addColumn(new String("class"));
		model.addColumn(new String("instance"));
		model.addColumn(new String("id"));
		model.addColumn(new String("context"));
		model.addColumn(new String("selection"));
		
		// Load table from parameters
	int numberOfRows = Integer.parseInt(getParameter("numberOfRows"));
		for (int i = 0; i < numberOfRows; i++ )
		{
			StringTokenizer st = new StringTokenizer(getParameter("row_" + String.valueOf(i)),",");
			/*--while ( st.hasMoreTokens() ) {			
				model.addRow( st.nextToken().trim(), st.nextToken().trim(), st.nextToken().trim(), st.nextToken().trim(),
				Boolean.valueOf(st.nextToken().trim()) );
			}
			*/
			while ( st.hasMoreTokens() ) {			
			
				Vector row = new Vector();
				 
                		row.add( st.nextToken().trim());
                		row.add(st.nextToken().trim());
                		row.add(new Integer(st.nextToken().trim()));
               	 		row.add(st.nextToken().trim());
				row.add(new Boolean(st.nextToken().trim()));
                		model.addRow(row);
			}
		}		
		
		table = new JTable(model);
		model.addMouseListenerToHeaderInTable(table);
		
		TableColumn col = table.getColumnModel().getColumn(2);
		col.setCellRenderer(new ControlTableCellRenderer());
		
		table.getTableHeader().setDefaultRenderer(new SortArrowHeaderRenderer(table));
		//Ask to be notified of selection changes.
		ListSelectionModel rowSM = table.getSelectionModel();
		rowSM.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		rowSM.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				//Ignore extra messages.
				// if (e.getValueIsAdjusting()) return;
				 //System.out.println("Selection occurred");
				 ListSelectionModel lsm = (ListSelectionModel)e.getSource();
				
				int [] selectedRows = table.getSelectedRows();
				TableModel model= table.getModel();
				currentRelay = "";
				for ( int i =0; i < selectedRows.length; i++ )
				{
					Boolean selected = (Boolean)model.getValueAt(selectedRows[i],4);
					if (selected.booleanValue() )
					{
						String lid = ((Integer)model.getValueAt(selectedRows[i],2)).toString();
						String context = (String)model.getValueAt(selectedRows[i],3);
						currentRelay = currentRelay + "\t\t<xr:to url=\""+context+"\" urn=\"urn:xdaq-application:lid="+lid+"\"/>\n";
					}	

				}

				c.displaySOAP();
				
		}});
		
	
		
		table.setPreferredScrollableViewportSize(new Dimension(500, 100));
		JScrollPane tableScrollingArea = new JScrollPane(table);
		
		ActionListener postAction = new ActionListener()
		{
			
			public void actionPerformed ( ActionEvent e) 
			{
				// send the message
				String urn = getParameter("urn");
				String url = getParameter("url");
				String response = c.executeCommand(url, urn, requestArea.getText() );
				responseArea.setText(response);
			}
		};
		
		ActionListener clearAction = new ActionListener()
		{
			public void actionPerformed ( ActionEvent e) 
			{
				c.displaySOAP();
				responseArea.setText("");
			}
		};
		
		JTabbedPane tabbed = new JTabbedPane();
		requestArea = new JEditorPane();
		Document d = requestArea.getDocument();
		d.putProperty(new String("tabSize"),new Integer(3));
		//requestArea.setText(sampleSOAP);
		JScrollPane requestScrollingArea = new JScrollPane(requestArea);
		
		
		
		
		JComboBox comboBox = new JComboBox();
		// add listener to combo box selection
		comboBox.addItemListener(this);
		//comboBox.setEditable(true);
		// Load table from parameters
		int numberOfCommands = Integer.parseInt(getParameter("numberOfCommands"));
		for (int i = 0; i < numberOfCommands; i++ )
		{
		
			StringTokenizer st = new StringTokenizer(getParameter("command_" + String.valueOf(i)),",");
			while ( st.hasMoreTokens() ) {			
				comboBox.addItem( new CommandRecord(st.nextToken(),st.nextToken(),st.nextToken())  );
			}
		}
		
		
		JPanel commandPanel = new JPanel();
		JLabel commandLabel = new JLabel("Command");
		//JTextField textField = null;
		//textField = new JTextField("type here your command",20);
		//commandPanel.add(textField);
		commandPanel.add(commandLabel);
		commandPanel.add(comboBox);
		
		tabbed.addTab("Predefined", commandPanel);
		tabbed.addTab("SOAP",  requestScrollingArea);

		

		responseArea = new JEditorPane();
		responseArea.setText("");
		JScrollPane responseScrollingArea = new JScrollPane(responseArea);
		
		JPanel windowButtons = new JPanel();
		JButton postButton = new JButton ("Post");
		postButton.addActionListener ( postAction );
		windowButtons.add ( postButton );

		JButton clearButton = new JButton ("Clear");
		clearButton.addActionListener ( clearAction );
		windowButtons.add ( clearButton );		
		
		// Add all GUI elements to the applet
		//
		Box b = Box.createVerticalBox();
		//b.add(Box.createRigidArea(new Dimension(2,25)));
	TableSelectionBox selectionBox = new TableSelectionBox(table);
		b.add(selectionBox);
		b.add(tableScrollingArea);
		b.add(tabbed);
		b.add(responseScrollingArea);
		this.getContentPane().setBackground(Color.white);					
		this.getContentPane().add (b, BorderLayout.CENTER);
		this.getContentPane().add (windowButtons, BorderLayout.SOUTH);
	
	}
	
	String executeCommand(String to, String urn, String soapRequest )
	{
		
		URL url = null;
		try 
		{
			//java.security.AccessController.checkPermission( new java.net.SocketPermission("lxcmd101:40000", "connect,accept"));
			url = new URL(to);
			
			//url = new URL("http://lxcmd101:40000/urn:xdaq-application:id=0");
		} catch (MalformedURLException mue)
		{
			System.out.println (mue.toString());
			return mue.toString();
		} catch (java.io.IOException ioe) {
			System.out.println (ioe.toString());
			return ioe.toString();
		}
		

		String soapResponse = "";
		try {

			// Connect 
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			
			connection.setUseCaches(false);
			//connection.connect();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			
			connection.setRequestMethod("POST");
			// Send here

			connection.setRequestProperty("Content-length", Integer.toString(soapRequest.length()));
			connection.setRequestProperty("SOAPAction",urn);
			
			//System.out.println ("Sending message:\n");
			//System.out.println (soapRequest);
			connection.connect();
			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
			writer.write(soapRequest);

			writer.flush();
			writer.close();

			// Get the response

			InputStream istream = connection.getInputStream();			
			BufferedReader ireader = new BufferedReader( new InputStreamReader(istream));
			String inputLine;
			
			while ( ( inputLine = ireader.readLine()) != null ) 
			{
				
				soapResponse = soapResponse.concat(inputLine);
			}

			//System.out.println ("Received response\n");
			//System.out.println (soapResponse);
			//System.out.println ("-----------------------------------------------------\n");

			istream.close();
			
			
		} 
		catch (IOException ioe) {
			ioe.printStackTrace();
			return ioe.toString();
		}
		return soapResponse;
	}
	
	
	
	
} // end of class
