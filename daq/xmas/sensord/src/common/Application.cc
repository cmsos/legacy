// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/sensord/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "xmas/sensord/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "xoap/DOMParserFactory.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

XDAQ_INSTANTIATOR_IMPL (xmas::sensord::Application);

XERCES_CPP_NAMESPACE_USE

xmas::sensord::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this)
{
	dialup_ = 0;
	splitup_ = 0;

	brokerDialupWatchdog_ = "PT30S";
	brokerTopicsWatchdog_ = "PT5S";
	eventingURL_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("brokerDialupWatchdog", &brokerDialupWatchdog_);
	this->getApplicationInfoSpace()->fireItemAvailable("brokerTopicsWatchdog", &brokerTopicsWatchdog_);
	this->getApplicationInfoSpace()->fireItemAvailable("eventingURL", &eventingURL_);

	b2in::nub::bind(this, &xmas::sensord::Application::onMessage);

	publishGroup_ = "eventing";

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/xmas/sensord/images/sensord-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/sensord/images/sensord-icon.png ");

	this->getApplicationInfoSpace()->fireItemAvailable("publishGroup", &publishGroup_);

	// optional broker properties
	autoConfSearchPath_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("autoConfSearchPath", &autoConfSearchPath_);

	brokerGroup_ = "";
	brokerURL_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("brokerURL", &brokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("brokerGroup", &brokerGroup_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this,  &xmas::sensord::Application::Default, "Default");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);
}

xmas::sensord::Application::~Application ()
{

}

void xmas::sensord::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string timerTaskName = e.getTimerTask()->name;
	if (timerTaskName == "sensord:scan")
	{
	}
}

void xmas::sensord::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) 
{
	if (plist.getProperty("urn:b2in-protocol:action") == "flashlist")
	{
		this->publishReport(msg, plist);
	}
	else
	{
		std::string action = plist.getProperty("urn:xmasbroker2g:action");
		if (action == "allocate")
		{
			std::string model = plist.getProperty("urn:xmasbroker2g:model");
			if (model == "eventing" && dialup_ != 0)
			{
				dialup_->onMessage(msg, plist);
			}
			else if (model == "topic" && splitup_ != 0)
			{
				splitup_->onMessage(msg, plist);
			}
			else
			{
				LOG4CPLUS_INFO(this->getApplicationLogger(), "unknown broker response: " << model);
			}
		}
		if (msg != 0) msg->release();
	}
}

void xmas::sensord::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		std::string path = autoConfSearchPath_.toString();

		std::vector < std::string > files;

		if (path.find("http") != std::string::npos)
		{
			try
			{
				files = this->getFileListing(path);
				for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
				{
					(*j) = path + "/" + (*j);
				}
			}
			catch (xmas::sensord::exception::Exception & e)
			{
				XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, "Failed to get sensor settings", e);
				this->notifyQualified("fatal", q);
				return;
			}
		}
		else
		{
			// if no search path provided, rely on runtime information
			if (path != "")
			{
				try
				{
					path = path + "/*.sensor";
					files = toolbox::getRuntime()->expandPathName(path);
				}
				catch (toolbox::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to import flashlist definitions from '" << path << "', ";
					XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
					this->notifyQualified("fatal",q);
					return;
				}
			}
		}

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Found " << files.size() << " sensor files");

		this->applySensorSettings(files);
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(xmas::sensord::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void xmas::sensord::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "xdaq::EndpointAvailableEvent")
	{
		toolbox::TimeInterval intervalDialup;
		toolbox::TimeInterval intervalTopics;
		try
		{
			intervalDialup.fromString(brokerDialupWatchdog_.toString());
		}
		catch (toolbox::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to parse the watchdog brokerDialupWatchdog parameter '" << brokerDialupWatchdog_.toString() << "', ";
			XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
		}

		try
		{
			intervalTopics.fromString(brokerTopicsWatchdog_.toString());
		}
		catch (toolbox::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to parse the watchdog brokerTopicsWatchdog_ parameter '" << brokerTopicsWatchdog_.toString() << "', ";
			XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
		}

		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(event);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

		if (network->getName() == networkName)
		{

			dialup_ = new b2in::utils::EventingDialup(this, publishGroup_, brokerURL_, eventingURL_, networkName, intervalDialup);
			splitup_ = new b2in::utils::TopicSplitupIncremental(this, brokerURL_, networkName, intervalTopics, flashlists_);
		}

	}
}

// TBD check when this sensor setting was called ( in sensro 2g - rendez vous style)
//void xmas::sensord::Application::applySensorSettings (const std::string & path)
void xmas::sensord::Application::applySensorSettings (std::vector < std::string > & files)
{
	for (std::vector<std::string>::iterator j = files.begin(); j != files.end(); j++)
	{
		DOMDocument* doc = 0;
		try
		{
			LOG4CPLUS_DEBUG(this->getApplicationLogger(), "apply sensor setting for file  '" << (*j) << "'");
			doc = xoap::getDOMParserFactory()->get("configure")->loadXML(*j);
			std::vector<xmas::MonitorSettings*> settings = xmas::MonitorSettingsFactory::create(doc);
			std::vector<xmas::MonitorSettings *>::iterator k;
			for (k = settings.begin(); k != settings.end(); k++)
			{
				std::string name = (*k)->getFlashListDefinition()->getProperty("name");
				if (std::find(flashlists_.begin(), flashlists_.end(), name) != flashlists_.end())
          			{
					flashlists_.push_back(name);
					b2in::utils::Statistics r;
					counters_[name] = r;
					LOG4CPLUS_INFO(this->getApplicationLogger(), "Installed flashlist '" << name << "'");
				}
				delete (*k); // the vector goes out of scope at the end of the function
			}
			doc->release();
			doc = 0;
		}
		catch (xoap::exception::Exception& e)
		{
			// WE THINK, if directory services are not running, this is where it will fail. not fatal
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to load configuration file from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			return;
		}
		catch (xmas::exception::Exception& e)
		{
			// this is a parse error
			if (doc != 0) doc->release();
			std::stringstream msg;
			msg << "Failed to parse configuration from '" << (*j) << "'";
			XCEPT_DECLARE_NESTED(xmas::sensord::exception::Exception, q, msg.str(), e);
			this->notifyQualified("error", q);
			continue;
		}
	}

}

std::vector<std::string> xmas::sensord::Application::getFileListing (const std::string& directoryURL) 
{
	std::vector < std::string > list;
	try
	{
		XMLURL xmlUrl(directoryURL.c_str());
		//BinInputStream* s = accessor->makeNew (xmlUrl);
		BinInputStream* s = xmlUrl.makeNewStream();

		if (s == 0)
		{
			std::string msg = "Failed to access ";
			msg += directoryURL;
			XCEPT_RAISE(xoap::exception::Exception, msg);
		}

		int r = 0;
		int curPos = 0;

		int bufferSize = 2048;
		XMLByte * buffer = new XMLByte[bufferSize];
		std::stringstream ss;
		while ((r = s->readBytes(buffer, bufferSize)) > 0)
		{
			int read = s->curPos() - curPos;
			curPos = s->curPos();
			ss.write((const char*) buffer, read);
		}

		delete s;
		delete[] buffer;

		while (!ss.eof())
		{
			std::string fileName;
			std::getline(ss, fileName);
			// apply blob
			// if "filename.sensorsomehtingelse" exists, we dont care
			if (fileName.find(".sensor") != std::string::npos)
			{
				list.push_back(fileName);
			}
		}

		return list;
	}
	catch (NetAccessorException& nae)
	{
		XCEPT_RAISE(xmas::sensord::exception::Exception, xoap::XMLCh2String(nae.getMessage()));
	}
	catch (MalformedURLException& mue)
	{
		XCEPT_RAISE(xmas::sensord::exception::Exception, xoap::XMLCh2String(mue.getMessage()));
	}
	catch (std::exception& se)
	{
		XCEPT_RAISE(xmas::sensord::exception::Exception, se.what());
	}
	catch (...)
	{
		std::string msg = "Caught unknown exception during 'get' operation on url ";
		msg += directoryURL;
		XCEPT_RAISE(xmas::sensord::exception::Exception, msg);
	}
}

void xmas::sensord::Application::publishReport (toolbox::mem::Reference * msg, xdata::Properties & plist)
{

	std::string name = plist.getProperty("urn:xmas-flashlist:name");
	counters_[name].incrementPulseCounter();

	if (dialup_ == 0)
	{
		counters_[name].incrementCommunicationLossCounter();
		msg->release();
		return;
	}

	std::string topic = "";
	if (splitup_ != 0)
	{
		topic = splitup_->getTopic(name);
	}

	if (topic != "")
	{
		plist.setProperty("urn:b2in-eventing:topic", topic);
	}
	else
	{
		// update flash list
		LOG4CPLUS_INFO(this->getApplicationLogger(), "Updating flashlist '" << name << "'");
		std::list<std::string> flashlists;
		flashlists.push_back(name);
		if (splitup_ != 0)
		{
			splitup_->update(flashlists);
		}

		//////////

		counters_[name].incrementUnassignedLossCounter();
		msg->release();
		return; // give up, no topic to report
	}

	// override properties forward to b2in-eventing 
	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");
	plist.setProperty("urn:b2in-eventing:action", "notify");

	try
	{
		//debug
		//std::cout << "sending message" << std::endl;
		//std::map<std::string, std::string, std::less<std::string> >& pmap = plist.getProperties();
		//for (std::map<std::string, std::string, std::less<std::string> >::iterator i = pmap.begin(); i != pmap.end(); i++ )
		//{
		//		 std::cout << "Name: " << (*i).first << " Value:" << (*i).second << std::endl;
		//	}
		//debug

		// LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Send report for flashlist " << plist.getProperty("urn:xmas-flashlist:name"));
		dialup_->send(msg, plist);
		counters_[name].incrementFireCounter();
		return;
	}
	catch (b2in::utils::exception::Exception & e)
	{
		counters_[name].incrementInternalLossCounter();
		msg->release();
		return;
	}

}

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void xmas::sensord::Application::TabPanel (xgi::Output * out)
{
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\" id=\"tabPage2\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void xmas::sensord::Application::StatisticsTabPage (xgi::Output * out)
{
	//Dialup

	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Eventing Dialup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getEventingURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total outgoing reports";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	//Splitup

	*out << cgicc::table().set("class","xdaq-table-vertical");
	*out << cgicc::caption("Topic Splitup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	if (splitup_ != 0)
	{
		*out << splitup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (splitup_ != 0)
	{
		*out << splitup_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if (splitup_ != 0)
	{
		*out << splitup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (splitup_ != 0)
	{
		*out << splitup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	// Per flashlist loss of reports
	*out << cgicc::table().set("class","xdaq-table");
	*out << cgicc::caption("Flashlist");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Flashlist").set("class","xdaq-sortable").set("style", "min-width: 100px;");
	*out << cgicc::th("Topic").set("class","xdaq-sortable").set("style", "min-width: 100px;");
	*out << cgicc::th("Received Counter");
	*out << cgicc::th("Enqueued Counter");
	*out << cgicc::th("Enqueuing Loss");
	*out << cgicc::th("No Network Loss");
	*out << cgicc::th("No Topic Loss");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();

	*out << cgicc::tbody();

	if (splitup_ != 0)
	{
		std::list<std::string> splitupFlashlists = splitup_->getFlashLists();
		for (std::list<std::string>::iterator w = splitupFlashlists.begin(); w != splitupFlashlists.end(); w++)
		{

			*out << cgicc::tr() << std::endl;
			*out << cgicc::td(*w) << std::endl;
			std::string topic = "";
			topic = splitup_->getTopic(*w);
			*out << cgicc::td(topic) << std::endl;

			xdata::UnsignedInteger64 num;
			num = 0;
			num = counters_[*w].getPulseCounter(); // used a received
			*out << cgicc::td(num.toString()) << std::endl;

			num = 0;
			num = counters_[*w].getFireCounter(); // used as enqueued
			*out << cgicc::td(num.toString()) << std::endl;

			num = counters_[*w].getInternalLossCounter();
			*out << cgicc::td(num.toString()) << std::endl;

			num = counters_[*w].getCommunicationLossCounter(); // no network
			*out << cgicc::td(num.toString()) << std::endl;

			num = counters_[*w].getUnassignedLossCounter();
			*out << cgicc::td(num.toString()) << std::endl;

			*out << cgicc::tr() << std::endl;
		}
	}
	*out << cgicc::tbody();
	*out << cgicc::table();
}

void xmas::sensord::Application::Default (xgi::Input * in, xgi::Output * out) 
{

	this->TabPanel(out);

}
