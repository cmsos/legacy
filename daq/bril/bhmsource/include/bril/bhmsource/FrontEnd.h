/**
 * @brief BHM Source xdaq Application.
 *
 * @author N. Tosi \<nicko@cern.ch\>
 *
 * This class provides access to the BHM Front End configuration
 *
 */

#ifndef _bril_bhmsource_FrontEnd_h_
#define _bril_bhmsource_FrontEnd_h_

#include "bril/bhmsource/Configuration.h"

namespace xdaq
 {

  class Application;

 }

namespace bril
 {

  namespace bhmsource
   {

    class ngCCMClient;

    class FrontEnd
     {

      public:
      FrontEnd( Configuration&, xdaq::Application* );

      ~FrontEnd();

      bool alive();

      void loadConfig();

      void setQieThreshold( std::string );

      void readConfig();

      void startCalibration();

      void stopCalibration();

      std::vector<float> readTemperatures();

      bool hasCalibration() const
       {
        return m_calpresent;
       }

      protected:
      uint16_t max5841_cvt( float, uint16_t = 0xFFFF );

      uint16_t lt3582_cvt( float );

      private:
      Configuration& r_cfg;
      ngCCMClient* m_ngccm;
      std::string m_varprefix;
      bool m_calpresent;

     };

   }

 }

#endif

