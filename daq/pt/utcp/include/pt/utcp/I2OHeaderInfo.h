// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.			                 	       *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			             *
 *************************************************************************/

#ifndef _pt_utcp_i2onheader_info_h_
#define _pt_utcp_i2onheader_info_h_

#include "i2o/i2o.h"
//#include <cstddef>

namespace pt
{
	namespace utcp
	{
		class I2OHeaderInfo
		{
			public:

				static size_t getLength (char * bufferPtr)
				{
					PI2O_MESSAGE_FRAME frame = (PI2O_MESSAGE_FRAME) bufferPtr;

					return (frame->MessageSize << 2);
				}

				static size_t getHeaderSize ()
				{
					return sizeof(I2O_MESSAGE_FRAME);
				}
		};
	}
}
#endif
