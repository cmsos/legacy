#include "tcds/deadwood/DummyDevice.h"

tcds::deadwood::DummyDevice::DummyDevice() :
  DeviceBase()
{
}

tcds::deadwood::DummyDevice::~DummyDevice()
{
}

std::vector<std::string>
tcds::deadwood::DummyDevice::getRegisterNamesImpl() const
{
  std::vector<std::string> const dummy;
  return dummy;
}

tcds::hwlayer::RegisterInfo::RegInfoVec
tcds::deadwood::DummyDevice::getRegisterInfosImpl() const
{
  tcds::hwlayer::RegisterInfo::RegInfoVec const dummy;
  return dummy;
}

uint32_t
tcds::deadwood::DummyDevice::readRegisterImpl(std::string const& regName) const
{
  uint32_t const dummy = 0;
  return dummy;
}

void
tcds::deadwood::DummyDevice::writeRegisterImpl(std::string const& regName,
                                               uint32_t const regVal) const
{
}

std::vector<uint32_t>
tcds::deadwood::DummyDevice::readBlockImpl(std::string const& regName,
                                           uint32_t const nWords) const
{
  std::vector<uint32_t> const dummy;
  return dummy;
}

std::vector<uint32_t>
tcds::deadwood::DummyDevice::readBlockOffsetImpl(std::string const& regName,
                                                 uint32_t const nWords,
                                                 uint32_t const offset) const
{
  std::vector<uint32_t> const dummy;
  return dummy;
}

void
tcds::deadwood::DummyDevice::writeBlockImpl(std::string const& regName,
                                            std::vector<uint32_t> const& regVals) const
{
}

uint32_t
tcds::deadwood::DummyDevice::getBlockSizeImpl(std::string const& regName) const
{
  uint32_t const dummy = 0;
  return dummy;
}

void
tcds::deadwood::DummyDevice::writeHardwareConfigurationImpl(tcds::hwlayer::ConfigurationProcessor::RegValVec const& cfg) const
{
}

tcds::hwlayer::DeviceBase::RegContentsVec
tcds::deadwood::DummyDevice::readHardwareConfigurationImpl(tcds::hwlayer::RegisterInfo::RegInfoVec const& regInfos) const
{
  tcds::hwlayer::DeviceBase::RegContentsVec const dummy;
  return dummy;
}

tcds::hwlayer::RegDumpVec
tcds::deadwood::DummyDevice::dumpRegisterContentsImpl() const
{
  tcds::hwlayer::RegDumpVec const dummy;
  return dummy;
}

void
tcds::deadwood::DummyDevice::sendBCommand(tcds::definitions::BCOMMAND_TYPE const bcommandType,
                                          tcds::definitions::bcommand_data_t const data,
                                          tcds::definitions::bcommand_address_t const address,
                                          tcds::definitions::bcommand_address_t const subAddress,
                                          tcds::definitions::BCOMMAND_ADDRESS_TYPE const addressType) const
{
}

void
tcds::deadwood::DummyDevice::sendBgo(tcds::definitions::BGO_NUM const bgoNumber) const
{
}

void
tcds::deadwood::DummyDevice::sendL1A() const
{
}

bool
tcds::deadwood::DummyDevice::isReadyForUseImpl() const
{
  return true;
}
