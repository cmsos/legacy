#include "tcds/rfdippub/version.h"

#include "config/version.h"
#include "hyperdaq/version.h"
#include "pt/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdaq/version.h"
#include "xdata/version.h"
#include "xgi/version.h"
#include "xoap/version.h"
#include "xoap/filter/version.h"

#include "tcds/exception/version.h"
#include "tcds/hwlayer/version.h"
#include "tcds/utils/version.h"

GETPACKAGEINFO(tcdsrfdippub)

void
tcdsrfdippub::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(hyperdaq);
  CHECKDEPENDENCY(pt);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
  CHECKDEPENDENCY(xgi);
  CHECKDEPENDENCY(xoap);
  CHECKDEPENDENCY(xoapfilter);

  CHECKDEPENDENCY(tcdsexception);
  CHECKDEPENDENCY(tcdshwlayer);
  CHECKDEPENDENCY(tcdsutils);
}

std::set<std::string, std::less<std::string> >
tcdsrfdippub::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, hyperdaq);
  ADDDEPENDENCY(dependencies, pt);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);
  ADDDEPENDENCY(dependencies, xdaq);
  ADDDEPENDENCY(dependencies, xdata);
  ADDDEPENDENCY(dependencies, xgi);
  ADDDEPENDENCY(dependencies, xoap);
  ADDDEPENDENCY(dependencies, xoapfilter);

  ADDDEPENDENCY(dependencies, tcdsexception);
  ADDDEPENDENCY(dependencies, tcdshwlayer);
  ADDDEPENDENCY(dependencies, tcdsutils);

  return dependencies;
}
