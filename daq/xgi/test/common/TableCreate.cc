#include <string>
#include <iostream>
#include "xgi/Table.h"

int main(int argc, char** argv)
{
	xgi::Table t("default");

	t.addColumn ("Description");
	t.addColumn ("URL");
	t.addColumn ("State");

	std::vector<xgi::Table::Row>::iterator row = t.addRow ("default");
	(*row).addColumn("Database manager");
	(*row).addColumn("http://a.b.c:40000");
	(*row).addColumn("FINTO");

	row = t.addRow ("default");
	(*row).addColumn("Event manager");
	
	(*row).addColumn("http://c.d.e:40000");
	(*row).addColumn("FINTO");

	std::cout << t << std::endl;

	return 0;
}
