package xdaq.xdesk.xtree;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.tree.*;
import javax.infobus.*;
import org.apache.log4j.*;
import xdaq.xdesk.tools.*;


public class PartitionSelector extends JPanel implements ItemListener, 
							PartitionModelListener,  
							TreeSelectionListener
{
	Logger logger = Logger.getLogger (PartitionSelector.class);

	JComboBox partitionComboBox_;
	JScrollPane currentTreeScrollPane_;
	JPanel dummyTreePanel_;
	DefaultComboBoxModel comboBoxModel_;
	PartitionDefaultModel defaultModel_;
	PartitionSelectorCache cache_;
	boolean waitingForReply_ = false;
	PartitionSelectorListener selectionListener_;
	
	public PartitionSelector (PartitionDefaultModel model, String [] filters)
	{	
		
		cache_ = new PartitionSelectorCache(this, filters);			
		defaultModel_ = model;		
		defaultModel_.addPartitionModelListener(this);
		
		this.setLayout(new BorderLayout());	
				
		// Combo Box, left top
		comboBoxModel_ = new DefaultComboBoxModel();
		partitionComboBox_ = new JComboBox(comboBoxModel_);
		partitionComboBox_.addItemListener (this);
		this.add(partitionComboBox_, BorderLayout.NORTH);
		
		// Tree, left lower - empty now, only put scroll pane
		dummyTreePanel_ = new JPanel();
		currentTreeScrollPane_ = new JScrollPane(dummyTreePanel_);
		this.add (currentTreeScrollPane_, BorderLayout.CENTER);
				
		Dimension minSize = new Dimension (200,200);
		this.setMinimumSize (minSize);
		selectionListener_ = null;
	}
	
	public void setPartitionSelectorListener ( PartitionSelectorListener selectionListener )
	{
		selectionListener_ = selectionListener;
	}
	
	public void removePartitionSelectorListener ()
	{
		selectionListener_ = null;
	}
	
	public String getSelectedPartition()
	{
		// Update the parameter table
		// Get partition
		return (String)comboBoxModel_.getSelectedItem(); 
	}
	
	protected String getSelectedTarget(String selectedPartition)
	{	
		// Get selected element in partition
		TreePath[] paths = cache_.getJTree(selectedPartition).getSelectionModel().getSelectionPaths();
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
                return treeNode.toString();
	}

	
	public void setModel(PartitionDefaultModel model) 
	{
		defaultModel_ = model;
	}
        
        
	// listener interface implementation
	public void partitionChanged(PartitionModelEvent event)
	{
                // remember the selected item
                //String currentSelection = (String)comboBoxModel_.getSelectedItem(); 
		String p = this.getSelectedPartition();
		
                boolean stillPresent = false;
                String name = null;
		
                // update combobox
		comboBoxModel_.removeAllElements();
		Enumeration names = defaultModel_.getConfigurationNames();
		while( names.hasMoreElements() )
		{
                        name = (String)names.nextElement();
			comboBoxModel_.addElement(name);
                        if (p != null && p  == name ) {
                            // selected item still in list
                            stillPresent = true;
			}
		}
                
                // if list of partition is empty then reset tree scroll pane to empty
                if (comboBoxModel_.getSize() == 0 ) 
		{		
                    this.remove(currentTreeScrollPane_);
                    currentTreeScrollPane_ =  new JScrollPane(dummyTreePanel_); 
                    this.add(currentTreeScrollPane_);
                    currentTreeScrollPane_.setVisible(true);
		} else
                {
                     if ( stillPresent ) 
		     {
                            comboBoxModel_.setSelectedItem(p); 
                            this.refreshSelection(p);
                     }
                     else
                     {
                            comboBoxModel_.setSelectedItem(name);  // set the last inserted
                            this.refreshSelection(name);
                     }                  
		}				
	}
	
	// A new partition has been selected in the Combo box.
	// Update the tree from the cache
	//
        protected void refreshSelection (String partitionName)
        {                       
		currentTreeScrollPane_.setVisible(false);
		// remove previous Tree pane
		this.remove(currentTreeScrollPane_);
		
		
		TreeModel model = (TreeModel) defaultModel_.getValue(partitionName);

		// fecch required scroll pane
		//XProp.XPropertiesListener configuration = (XProp.XPropertiesListener)defaultModel_.getValue(partitionName);
		//ImmediateAccess item = (ImmediateAccess) configuration.getItem();
		//TreeModel model = (TreeModel)item.getValueAsObject();
		// put new selection, get tree pane from model (if cached )
		
		currentTreeScrollPane_ = (JScrollPane)cache_.getScrollPane(partitionName, model);			

		// add new selection to tree panel
		this.add(currentTreeScrollPane_);
		currentTreeScrollPane_.setVisible(true);

                //force  refresh according current selection in tree
                JTree tree = cache_.getJTree(partitionName);
                this.refreshTreeSelection(tree);
        }
        
            
        protected void refreshTreeSelection(JTree tree)
        {
                TreePath[] paths = tree.getSelectionModel().getSelectionPaths();
               
                 // Protect against empty selection
		if (paths == null) return;
               
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[0].getLastPathComponent();

                String currentElement = node.toString();
                
		// Here I should create the properties editor
		//String currentSelection = (String)comboBoxModel_.getSelectedItem(); 
		String p = this.getSelectedPartition();

		// User application callback to indicate that an element in the
		// tree has been selected. The information passed is the 
		// 1) selected partition
		// 2) selected node	
		if (selectionListener_ != null)		 
			selectionListener_.selectionChanged ();

		System.out.println("Going to revalidate");
		this.invalidate();
		this.revalidate();
		this.setVisible(true);
        }
	
        // Event generated by ComboBox
	public void itemStateChanged (ItemEvent e)
	{
		if ( e.getStateChange() == ItemEvent.SELECTED ) 
		{
                    this.refreshSelection((String) e.getItem());
                       
                }
	}
        
        // Event generated by Tree selection
        public void valueChanged (TreeSelectionEvent e)
	{
                JTree tree = (JTree) e.getSource();
		this.refreshTreeSelection(tree);		
        }
        
	
	public void setEnabled (boolean enabled)
	{
		this.setEnabled (this, enabled);
	}
	
	protected void setEnabled ( Container c, boolean enabled )
	{
		Component[] list = c.getComponents();
		
		for (int i = 0; i < list.length; i++)
		{
			Container cont = (Container) list[i];
			cont.setEnabled(enabled);
			setEnabled ( cont, enabled );
		}
	}
	
	public JTree getJTree(String partitionName)
	{
		return cache_.getJTree(partitionName);
	}
}

