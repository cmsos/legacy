//
//  XPropToolBar.java
//  
//
//  Created by Luciano Orsini on Sat Jun 28 2003.
//  Copyright (c) 2003 CERN. All rights reserved.
//
package xdaq.xdesk.xcommander;

import javax.swing.JToolBar;
import javax.swing.JButton;
import javax.swing.ImageIcon;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.net.*;


public class XCommanderToolBar extends JToolBar
{   
    XCommanderToolBarListener listener_ = null;
    
    JButton executeButton_;
    JButton queryButton_;
    JButton stopButton_;
    
    JComboBox commands_;    
    
    public XCommanderToolBar ()
    {
        this.setFloatable(false);
        
        //first button
	URL url = null;
	Image img = null;

	url = this.getClass().getResource("/xdaq/xdesk/xcommander/icons/SendMail24.gif");
        executeButton_ = new JButton(new ImageIcon(url));
        executeButton_.setEnabled(false);
        executeButton_.setToolTipText("Perform selected operation on Executive/Application");
        executeButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("execute");
            }
            });
        this.add(executeButton_);
        
        
	url = this.getClass().getResource("/xdaq/xdesk/xcommander/icons/Information24.gif");
        queryButton_ = new JButton(new ImageIcon(url));
        queryButton_.setEnabled(false);
        queryButton_.setToolTipText("Query the command interface of an Executive/Application");
        queryButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("query");
            }
            });
        this.add(queryButton_);

	
	url = this.getClass().getResource("/xdaq/xdesk/xcommander/icons/Stop24.gif");
        stopButton_ = new JButton(new ImageIcon(url));
        stopButton_.setEnabled(false);
        stopButton_.setToolTipText("Interrupt currently running operation");
        stopButton_.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispatch("stop");
            }
            });
        this.add(stopButton_);

	// Command combo box
	commands_ = new JComboBox();
	commands_.setMaximumSize   ( new Dimension (200,25 ) );
	commands_.setPreferredSize ( new Dimension (200, 25) );
	this.add (commands_);
    }  
    

    protected void dispatch(String name) 
    {
            if ( listener_ != null )
                listener_.buttonPressed(name);
    }
    
    void setToolBarListener(XCommanderToolBarListener l ) 
    {
            listener_ = l;
    }

    // Toggle all buttons enabled/stop button disabled and vice versa
    public void stopButtonEnable (boolean enabled)
    {
     	stopButton_.setEnabled(enabled);
    }


    public void setEnabled(boolean enabled)
    {
        queryButtonEnable(enabled);
	executeButtonEnable(enabled);	
    }
    
    public void queryButtonEnable (boolean enabled)
    {
   	 queryButton_.setEnabled(enabled);
    }
    
    public void executeButtonEnable (boolean enabled)
    {
    	
	  executeButton_.setEnabled(enabled);
	
    }
    
    public boolean hasCommands()
    {
    	return commands_.getItemCount() > 0;
    }
   

	public String getSelectedCommand()
	{
		return (String) commands_.getSelectedItem();
	}
	
	public void setCommandModel (DefaultComboBoxModel m)
	{
		commands_.setModel (m);
	}
    
}
