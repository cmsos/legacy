#!/usr/bin/env python

###############################################################################

import ConfigParser
import curses
import sys
import time

from pyspy import PySpy

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def hide_cursor():
    try:
        curses.curs_set(0)
    except curses.error:
        pass
    # End of hide_cursor().

def show_cursor():
    try:
        curses.curs_set(1)
    except curses.error:
        pass
    # End of show_cursor().

###############################################################################

def chunkify(input_list, chunk_size):
    res = [input_list[i:i + chunk_size] for i in range(0, len(input_list), chunk_size)]
    # End of chunkify().
    return res

###############################################################################

def interpret_int(int_like_string):
    tmp = int_like_string.lower()
    if tmp.startswith("0x"):
        radix = 16
    elif tmp.startswith("0o"):
        radix = 8
    elif tmp.startswith("0b"):
        radix = 2
    elif tmp.startswith("0"):
        radix = 8
    else:
        radix = 10
    res = int(tmp, radix)
    # End of interpret_int().
    return res

###############################################################################

class PySpyRunner(object):

    SCROLL_DIR_UP = -1
    SCROLL_DIR_DOWN = 1

    def __init__(self, scr, config):
        self.scr = scr
        self.config = config

        self.pad = None
        self.top_line = 0
        self.current_line = self.top_line
        self.continuous_mode = False
        self.verbose_mode = False
        self.contents = []
        self.spy_mem_empty = None
        self.spy_mem_full = None
        self._cache_tmp = None

        # Extract the configuration parameters.
        spy_address = self.get_config_option("connection", "address")
        spy_port = int(self.get_config_option("connection", "port"))
        address_table_file_name = self.get_config_option("connection", "address_table")

        # Connect to the TTCSpy.
        try:
            self.spy = PySpy(address_table_file_name, spy_address, spy_port)
        except Exception, err:
            msg = "Problem connecting to the TTCSpy: '{0:s}'.".format(str(err))
            sys.exit(msg)

        # Configure the TTCSpy.
        self.configure_spy()

        # And go!
        self.run()
        # End of __init__().

    @property
    def win_height(self):
        return self.scr.getmaxyx()[0] - 2

    @property
    def win_width(self):
        return self.scr.getmaxyx()[1] - 2

    @property
    def first_line(self):
        return 0

    @property
    def last_line(self):
        return len(self.contents) - 1

    @property
    def num_lines_visible(self):
        return self.win_height

    @property
    def bottom_line(self):
        tmp = self.top_line + self.num_lines_visible - 1
        res = min(len(self.contents), tmp)
        # End of bottom_line().
        return res

    def get_config_option(self, section, option):
        try:
            res = self.config.get(section, option)
        except ConfigParser.NoSectionError:
            msg = "Missing '{0:s}' section in PySpy-runner configuration file."
            msg = msg.format(section)
            sys.exit(msg)
        except ConfigParser.NoOptionError:
            msg = "Missing option '{0:s}' in PySpy-runner configuration file " \
                "(in section '{1:s}')."
            msg = msg.format(option, section)
            sys.exit(msg)
        # End of get_config_option().
        return res

    def configure_spy(self):
        self.logging_source = self.get_config_option("logging", "source")
        self.logging_mode = self.get_config_option("logging", "mode")
        self.bc0_val = interpret_int(self.get_config_option("logging", "bc0_val"))
        self.bc0_bx = interpret_int(self.get_config_option("logging", "bc0_bx"))

        # The TTCSpy trigger mask.
        self.logging_logic = self.get_config_option("trigger_mask", "trigger_combination_operator")
        trg_msk_brc_b = interpret_int(self.get_config_option("trigger_mask", "brc_b"))
        trg_msk_brc_e = interpret_int(self.get_config_option("trigger_mask", "brc_e"))
        trg_msk_brc_dddd = interpret_int(self.get_config_option("trigger_mask", "brc_dddd"))
        trg_msk_brc_tt = interpret_int(self.get_config_option("trigger_mask", "brc_tt"))
        trg_msk_brc_dddd_all = interpret_int(self.get_config_option("trigger_mask", "brc_dddd_all"))
        trg_msk_brc_tt_all = interpret_int(self.get_config_option("trigger_mask", "brc_tt_all"))
        trg_msk_brc_all = interpret_int(self.get_config_option("trigger_mask", "brc_all"))
        trg_msk_adr_all = interpret_int(self.get_config_option("trigger_mask", "adr_all"))
        trg_msk_l1accept = interpret_int(self.get_config_option("trigger_mask", "l1accept"))
        trg_msk_brc_zero_data = interpret_int(self.get_config_option("trigger_mask", "brc_zero_data"))
        trg_msk_adr_zero_data = interpret_int(self.get_config_option("trigger_mask", "adr_zero_data"))
        trg_msk_err_com = interpret_int(self.get_config_option("trigger_mask", "err_com"))
        trg_msk_brc_val0 = interpret_int(self.get_config_option("trigger_mask", "brc_val0"))
        trg_msk_brc_val1 = interpret_int(self.get_config_option("trigger_mask", "brc_val1"))
        trg_msk_brc_val2 = interpret_int(self.get_config_option("trigger_mask", "brc_val2"))
        trg_msk_brc_val3 = interpret_int(self.get_config_option("trigger_mask", "brc_val3"))
        trg_msk_brc_val4 = interpret_int(self.get_config_option("trigger_mask", "brc_val4"))
        trg_msk_brc_val5 = interpret_int(self.get_config_option("trigger_mask", "brc_val5"))

        # Perform TTCSpy configuration.
        # NOTE: Selecting a TTC stream source also determines the
        # source of the clock used to drive some of the
        # registers. Without signal present some registers cannot be
        # accessed. So we first switch to the on-board crystal, then
        # do the configuration, and finally select the correct input
        # source. But only if necessary!
        try:
            self.spy.select_source(self.logging_source)
        except AssertionError:
            msg = "Configuration setting '{0:s}' is not a valid logging source."
            msg = msg.format(self.logging_source)
            sys.exit(msg)
        if not self.spy.is_clk_ok():
            self.spy.configure_glib_xpoint_switch("sma_or_xtal")
            if not self.spy.is_clk_ok():
                msg = "Somehow the GLIB on-board crystal is not delivering a clock. " \
                    "Could it be that the jumper settings are not correct?"
                sys.exit(msg)

        try:
            self.spy.set_logging_mode(self.logging_mode)
        except AssertionError:
            msg = "Configuration setting '{0:s}' is not a valid logging mode."
            msg = msg.format(self.logging_mode)
            sys.exit(msg)

        try:
            self.spy.set_bc0_val(self.bc0_val)
        except AssertionError:
            msg = "Configuration setting '0x{0:x}' is not a valid BC0 data value."
            msg = msg.format(self.bc0_val)
            sys.exit(msg)

        try:
            self.spy.set_bc0_bx(self.bc0_bx)
        except AssertionError:
            msg = "Configuration setting '{0:d}' is not a valid BC0 BX number."
            msg = msg.format(self.bc0_bx)
            sys.exit(msg)

        # NOTE: The PySpy is always in 'single-shot' acquisition mode.
        self.spy.set_logging_buffer_mode("single_shot")

        # Configure the TTCSpy trigger mask.
        try:
            self.spy.set_logging_logic(self.logging_logic)
        except AssertionError:
            msg = "Configuration setting '{0:s}' is not a valid logging logic setting."
            msg = msg.format(self.logging_logic)
            sys.exit(msg)

        self.spy.set_trigger_mask(brc_b         = trg_msk_brc_b,
                                  brc_e         = trg_msk_brc_e,
                                  brc_dddd      = trg_msk_brc_dddd,
                                  brc_tt        = trg_msk_brc_tt,
                                  brc_dddd_all  = trg_msk_brc_dddd_all,
                                  brc_tt_all    = trg_msk_brc_tt_all,
                                  brc_all       = trg_msk_brc_all,
                                  adr_all       = trg_msk_adr_all,
                                  l1accept      = trg_msk_l1accept,
                                  brc_zero_data = trg_msk_brc_zero_data,
                                  adr_zero_data = trg_msk_adr_zero_data,
                                  err_com       = trg_msk_err_com,
                                  brc_val0      = trg_msk_brc_val0,
                                  brc_val1      = trg_msk_brc_val1,
                                  brc_val2      = trg_msk_brc_val2,
                                  brc_val3      = trg_msk_brc_val3,
                                  brc_val4      = trg_msk_brc_val4,
                                  brc_val5      = trg_msk_brc_val5)

        # NOTE: The following three commands are in a bit of a strange
        # place, but this is the only way to guarantee that the
        # logging has been reset and started even when there is no
        # signal on the final TTC source. So these have to be before
        # the source selection.
        self.spy.stop_logging()
        self.spy.reset_logging()
        self.spy.start_logging()

        # Finally: the selection of the TTC stream source.
        self.spy.select_source(self.logging_source)
        # End of configure_spy().

    def run(self):
        self.init_curses()
        self.update()
        self.run_input_handler()
        # End of run().

    def init_curses(self):
        curses.use_default_colors()
        self.scr.timeout(100)
        hide_cursor()
        # End of init_curses().

    def init_pad(self):
        num_lines = 1
        num_columns = 0
        if self.contents:
            num_lines = len(self.contents)
            num_columns = max([len(i) for i in self.contents])
        self.pad = curses.newpad(num_lines, num_columns + 1)
        # End of init_pad().

    def update(self):
        if self.spy.is_clk_ok():
            self.update_contents()
        self.update_screen()
        curses.doupdate()
        # Add a little bit of time for curses to finish its buffering
        # of the virtual screen. Otherwise we can run into trouble in
        # continuous mode where updates happen in very quick
        # succession.
        time.sleep(.1)
        # End of update().

    def update_contents(self):
        tmp = self.spy.read("ttcspy.logging_status.spy_memory_address_pointer")
        cache_still_valid = True
        if tmp != self._cache_tmp:
            cache_still_valid = False
        if not cache_still_valid:
            self.contents = self.spy.get_spy_mem_contents_formatted(self.verbose_mode)
            self.spy_mem_empty = (len(self.contents) == 0)
            self.spy_mem_full = self.spy.is_spy_mem_full()
            self._cache_tmp = tmp
            if self.spy_mem_full:
                if self.continuous_mode:
                    self.spy.reset_logging()
        # End of update_contents().

    def update_pad(self):
        self.init_pad()
        for (i, line) in enumerate(self.contents):
            attr = curses.A_NORMAL
            if i == self.current_line:
                attr = curses.A_STANDOUT
            self.pad.addstr(i, 0, line, attr)
        num_lines = self.win_height
        num_columns = self.win_width
        self.pad.noutrefresh(self.top_line, 0,
                             1, 1,
                             num_lines, num_columns)
        # End of update_pad().

    def update_screen(self):
        # Redraw window and border.
        self.scr.erase()
        self.scr.box()

        # Add a window title.
        title = " TTCSpy @ {0:s} ".format(self.spy.ip_address)
        self.scr.addstr(0, 1, title)

        # Add the firmware versions (system- and user logic) to the
        # right of the title line.
        version_str = " FW version: {0:s}/{1:s} (sys/usr) "
        version_str = version_str.format(self.spy.fw_version_sys(),
                                         self.spy.fw_version_usr())
        x_pos = self.win_width - len(version_str) + 1
        self.scr.addstr(0, x_pos, version_str)

        # Add some info to the window footer.
        foot_pos_clk_status = 1
        foot_pos_mem_status = 25
        foot_pos_acq_status = 60
        footer_clk_status = "No clue about the clock"
        if self.logging_source == "frontpanel":
            check_method = self.spy.is_cdr_ok
        elif self.logging_source == "backplane":
            check_method = self.spy.is_clk_ok
        if check_method():
            footer_clk_status = "Input signal locked"
        else:
            footer_clk_status = "No input signal"
        if self.spy_mem_empty:
            footer_mem_status = "Spy memory is empty"
        elif self.spy_mem_full:
            footer_mem_status = "Spy memory is full"
        else:
            footer_mem_status = "Spy memory is not yet full"
        if self.continuous_mode:
            footer_acq_status = "Acquisition: continuous mode (i.e., auto-restart when memory full)"
        else:
            if self.spy_mem_full:
                footer_acq_status = "Acquisition: manual (hit 'r' to reset and restart logging)"
            else:
                footer_acq_status = "Acquisition: manual (will stop when memory is full)"
        self.scr.addstr(self.scr.getmaxyx()[0] - 1, foot_pos_clk_status, footer_clk_status)
        self.scr.addstr(self.scr.getmaxyx()[0] - 1, foot_pos_mem_status, footer_mem_status)
        self.scr.addstr(self.scr.getmaxyx()[0] - 1, foot_pos_acq_status, footer_acq_status)

        self.scr.noutrefresh()
        self.update_pad()
        # End of update_screen().

    def run_input_handler(self):
        while True:
            input = None
            # This is a bit of a weird construct, but this works
            # around a bug in getkey(). Otherwise there is no way to
            # catch the KeyboardInterrupt.
            try:
                try:
                    input = self.scr.getkey()
                except:
                    pass
            except KeyboardInterrupt:
                break

            if not input is None:
                # # Handling of terminal resizing.
                # if input == "KEY_RESIZE":
                #     pdb.set_trace()

                # Handling of scroll keys.
                if input == "KEY_DOWN":
                    self.scroll(PySpyRunner.SCROLL_DIR_DOWN, 1)
                elif input == "KEY_UP":
                    self.scroll(PySpyRunner.SCROLL_DIR_UP, 1)
                elif input == "KEY_NPAGE":
                    self.scroll(PySpyRunner.SCROLL_DIR_DOWN, self.num_lines_visible)
                elif input == "KEY_PPAGE":
                    self.scroll(PySpyRunner.SCROLL_DIR_UP, self.num_lines_visible)
                elif input == "KEY_HOME":
                    self.scroll_home()
                elif input == "KEY_END":
                    self.scroll_end()

                # From here on we only handle single characters, so we
                # can lowercase them without breaking anything (unlike
                # with the keys above).
                else:
                    input = input.lower()

                    # C toggles the 'continuous updating' mode.
                    if input == "c":
                        self.toggle_continuous_mode()

                    # H shows the help screen.
                    if input == "h":
                        self.show_help()

                    # Q goes quit.
                    if input == "q":
                        break

                    # R forces a manual reset of the TTCSpy memory.
                    if input == "r":
                        self.scroll_home()
                        self.spy.reset_logging()

                    # V toggles the verbosity mode.
                    if input == "v":
                        self.toggle_verbose_mode()

            self.update()
        # End of run_input_handler().

    def scroll(self, direction, step):
        if direction == PySpyRunner.SCROLL_DIR_DOWN:
            if (self.current_line + step) <= self.bottom_line:
                self.current_line += step
            else:
                tmp_step = min(step, self.last_line - self.bottom_line)
                self.top_line += tmp_step
                if tmp_step != step:
                    self.current_line = self.bottom_line
                else:
                    self.current_line += step
        elif direction == PySpyRunner.SCROLL_DIR_UP:
            if (self.current_line - step) >= self.top_line:
                self.current_line -= step
            else:
                tmp_step = min(step, self.top_line)
                self.top_line -= tmp_step
                if tmp_step != step:
                    self.current_line = self.top_line
                else:
                    self.current_line -= step
        else:
            # ASSERT ASSERT ASSERT
            assert False
            # ASSERT ASSERT ASSERT end
        # End of scroll().

    def scroll_home(self):
        self.top_line = self.first_line
        self.current_line = self.top_line
        # End of scroll_home().

    def scroll_end(self):
        self.current_line = self.last_line
        self.top_line = self.current_line - self.num_lines_visible + 1
        # End of scroll_end().

    def toggle_continuous_mode(self):
        self.continuous_mode = (not self.continuous_mode)

    def toggle_verbose_mode(self):
        self.verbose_mode = (not self.verbose_mode)
        self._cache_tmp = None

    def window(self, height, width, message="", title=""):
        height = min(self.win_height, height)
        width  = min(self.win_width, width)
        pos_y = ((self.win_height - height) / 2) + 1
        pos_x = ((self.win_width  - width) / 2) + 1
        win = curses.newwin(height, width, pos_y, pos_x)
        win.box()
        win.bkgd(" ", curses.A_REVERSE + curses.A_BOLD)
        if width >= len(title) and title != "":
            win.addstr(0, 1, " " + title + " ")
        pos_y = 1
        for line in message.split("\n"):
            if pos_y < height - 1:
                win.addstr(pos_y, 2, line)
                pos_y += 1
            else:
                # Do not write outside of frame border
                win.addstr(pos_y, 2, " More... ")
                break
        # End of window
        return win

    def show_help(self):
        help_text = """

        The PySpy is a simple, Pyhon-only implementation of a TTCSpy
        readout. It can run a 'spy' acquisition on TTC traffic in one
        of two modes:

        - Manual mode: acquisition continues until the TTCSpy memory
          is full, and then stops. Hitting 'r' restarts the
          acquisition.

        - Continuous mode: acquisition automatically restarts
          (overwriting the previous acquisition data in the TTCSpy
          memory) every time the TTCSpy memory is full.

        The acquisition mode used at startup time, as well as the
        TTCSpy hardware configuration are configurable using the
        pyspy_runner.cfg file. The documentation format is explained
        in this file.

        Keys:
          'c' -> Toggles 'continuous updating' mode on/off.
          'h' -> Shows this help screen.
          'q' -> Quits the application.
          'r' -> Forces a manual reset of the TTCSpy memory and
                 restarts the acquisition.
          'v' -> Toggles verbose mode on/off.

        Hit any key to return to the main screen.
        """
        width  = self.win_width
        height = self.win_height
        title = "TCDS HwHacker Help"
        win = self.window(height, width, help_text, title)
        while True:
            if win.getkey():
                break
        # End of show_help().

    # End of class PySpyRunner.

###############################################################################

if __name__ == "__main__":

    # Read the configuration file.
    cfg_file_name = "pyspy_runner.cfg"
    print "Reading PySpy-runner configuration from file '{0:s}'.".format(cfg_file_name)
    config = ConfigParser.SafeConfigParser()
    config.read(cfg_file_name)

    # # DEBUG DEBUG DEBUG
    # # Some helpers for debugging without upsetting curses too much.
    # dbg_file_name = "pyspy_runner_debug.log"
    # dbg_file = open(dbg_file_name, "w")
    # dbg_file.close()
    # def log(msg):
    #     dbg_file = open(dbg_file_name, "a")
    #     dbg_file.write("{0:s}\n".format(msg))
    #     dbg_file.close()
    # # DEBUG DEBUG DEBUG end

    # Now instantiate and run the curses framework.
    try:
        curses.wrapper(PySpyRunner, config)
    except curses.error, err:
        if str(err) in ["addstr() returned ERR",
                        "pnoutrefresh() returned ERR"]:
            print >> sys.stderr, "The curses library encountered a problem. " \
                "Could it be that your terminal is a bit too small?"
        else:
            print >> sys.stderr, "Something went wrong: '{0:s}'.".format(str(err))
        sys.exit(1)
    except Exception, err:
        err_str = str(err)
        if err_str.find("timed out") > -1:
            print >> sys.stderr, "Lost connection to the hardware."
        else:
            print >> sys.stderr, "Something went wrong: '{0:s}'.".format(err_str)
        sys.exit(1)
    except BaseException, err:
        print >> sys.stderr, str(err)
        sys.exit(1)

    print "Done"

###############################################################################
