package test.wse;

import wsmg.NotificationHandler;
import wsmg.util.WsmgUtil;
import xsul.MLogger;

public class Consumer implements NotificationHandler
{
	private final static MLogger logger = MLogger.getLogger();

	public Consumer()
	{
	}

	public void handleNotification(String message)
	{
		System.out.println(message);
  }
}

