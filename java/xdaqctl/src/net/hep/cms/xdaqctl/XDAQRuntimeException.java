/*
 * XDAQRuntimeException
 *
 * $Id: XDAQRuntimeException.java,v 4.1 2007/03/26 14:40:24 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

/**
 * Runtime exception for system errors
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XDAQRuntimeException extends RuntimeException
{
	public XDAQRuntimeException() {}
	public XDAQRuntimeException(String message) { super(message); }
	public XDAQRuntimeException(Throwable cause) { super(cause); }
	public XDAQRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}

// End of file
// vim: set ts=4 sw=4:
