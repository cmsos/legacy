package xdaq.xdesk.xdesk;

import java.util.*;
import java.net.MalformedURLException;
import java.net.URLClassLoader;
import java.net.URL;
import java.lang.*;
import java.io.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;

import org.apache.log4j.*;

import java.applet.*;

public class DeskletRegistry
{
	static Logger logger = Logger.getLogger (DeskletRegistry.class);

	Hashtable desklets_;
	HashMap filenameMapper_;

	public DeskletRegistry()
	{
		desklets_ = new Hashtable();
		filenameMapper_ = new HashMap();
	}
	
	public void addApplet (String name, DeskletStub stub, String pathname)
	{
		desklets_.put (name, stub);
		filenameMapper_.put (pathname, name);
		
		this.printMap(desklets_);
	}
	
	public String getName (String pathname)
	{
		return (String) filenameMapper_.get (pathname);
	}
	
	public void removeApplet (String name)
	{
		this.printMap(desklets_);
		this.printMap2(filenameMapper_);
		desklets_.remove (name);
		Set keySet = filenameMapper_.keySet();
		Object [] names = keySet.toArray(new Object  [] {});
		for ( int i=0; i < names.length; i++ ) {
			
			if ( ((String)(filenameMapper_.get (names[i]))).equals(name) )
			{
				filenameMapper_.remove(names[i]);
				System.out.println("Removinf fullpath " + (String)names[i]+ " Daqlet: " + name +" is removed from registry");
				this.printMap(desklets_);
				return;
			}
		}
		
			
	}
	
	void printMap(Hashtable map )
	{
		Set keySet = map.keySet();
		Object [] names = keySet.toArray(new Object  [] {});	
		for ( int i=0; i < names.length; i++ ) {
			DeskletStub stub = (DeskletStub)map.get(names[i]);
			System.out.println(" Key: " + (String)names[i]);
			System.out.println(" Value: " + stub.getApplet().getName());
		}
	}	
	void printMap2(HashMap map )
	{
		Set keySet = map.keySet();
		Object [] names = keySet.toArray(new Object  [] {});	
		for ( int i=0; i < names.length; i++ ) {
			//DeskletStub stub = (DeskletStub)map.get(names[i]);
			System.out.println(" --Key: " + (String)names[i]);
			System.out.println(" --Value: " + (String)map.get(names[i]) );
		}
	}	
	
	public DeskletStub getApplet (String name)
	{
		return (DeskletStub) desklets_.get (name);
	}
	
	public Enumeration getApplets ()
	{
		return desklets_.elements();
	}
	
	public int size()
	{
		return desklets_.size();
	}
	
	/*
	public DeskletStub getApplet (int r)
	{
		Enumeration e = desklets_.elements();
		for (int i = 0; i < r; i++)
		{
			if (e.hasMoreElements())
				e.nextElement();
			else
				return null;
		}
		return (DeskletStub) e.nextElement();
	}
	*/
	
}
