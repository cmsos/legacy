#include "psx/sapi/Utils.h"

std::string
psx::sapi::ErrTypeToString(ErrClass::ErrType const errType)
{
  std::string res = "unknown error";

  // Based on info found in
  // /opt/WinCC_OA/3.15/api/include/Basics/Utilities/ErrClass.hxx.
  switch (errType)
    {
    case ErrClass::ERR_IMPL:
      res = "implementation error";
      break;
    case ErrClass::ERR_PARAM:
      res = "parametrisation error";
      break;
    case ErrClass::ERR_SYSTEM:
      res = "system error";
      break;
    case ErrClass::ERR_CONTROL:
      res = "control runtime error";
      break;
    case ErrClass::ERR_REDUNDANCY:
      res = "redundancy error";
      break;
    default:
      res = "unspecified error";
      break;
    }

  return res;
}

