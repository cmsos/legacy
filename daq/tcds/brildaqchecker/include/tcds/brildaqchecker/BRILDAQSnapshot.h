#ifndef _tcds_brildaqchecker_BRILDAQSnapshot_h_
#define _tcds_brildaqchecker_BRILDAQSnapshot_h_

#include <deque>
#include <vector>

#include "xdata/Table.h"

namespace tcds {
  namespace brildaqchecker {

    class BRILDAQSnapshot
    {

    public:
      BRILDAQSnapshot(std::deque<xdata::Table> const& nibbles);
      ~BRILDAQSnapshot();

      std::vector<xdata::Table> recentNibbles() const;

    private:
      std::deque<xdata::Table> nibbles_;

    };

  } // namespace brildaqchecker
} // namespace tcds

#endif // _tcds_brildaqchecker_BRILDAQSnapshot_h_
