// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius		                             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/


#include <string>
#include <stack>
#include <set>
#include <map>
#include <list>

#include "xcept/tools.h"
#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xoap/exception/Exception.h"
#include "xoap/MessageReference.h"
#include "xoap/domutils.h"
#include "xoap/DOMParserFactory.h"

#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Table.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"


#include "toolbox/Properties.h"
#include "toolbox/ActionListener.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/WaitingWorkLoop.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/BSem.h"
#include "xplore/utils/DescriptorsCache.h"
#include "xplore/Interface.h"
#include "xplore/exception/Exception.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"
#include "toolbox/Runtime.h"


#include "pt/PeerTransportAgent.h"
#include "pt/tcp/Address.h"
#include "b2in/nub/Messenger.h"
#include "xgi/Method.h"

#include "elastic/loadtest/Application.h"


XDAQ_INSTANTIATOR_IMPL(elastic::loadtest::Application)


elastic::loadtest::Application::Application(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) : xdaq::Application(s), mutex_(toolbox::BSem::FULL)
{
	xgi::bind(this,&elastic::loadtest::Application::start, "start");
	xgi::bind(this,&elastic::loadtest::Application::stop, "stop");

	toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(0x100000 * 100);
	toolbox::net::URN urn("toolbox-mem-pool", "loadtest");

	pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);


	size_t committedSize = a->getCommittedSize();
	pool_->setHighThreshold((unsigned long) (committedSize * 0.9));
	pool_->setLowThreshold((unsigned long) (committedSize * 0.7));

	pt::tcp::Address::Reference remote = pt::getPeerTransportAgent()->createAddress ( "utcp://localhost:60000", "b2in" );
	pt::Address::Reference local = pt::getPeerTransportAgent()->createAddress ( "utcp://localhost:50000", "b2in" );
	messenger_ = pt::getPeerTransportAgent()->getMessenger(remote, local);


	// Define FSM
	fsm_.addState('I', "Idle");
	fsm_.addState('R', "Running");
	fsm_.addStateTransition('I', 'R', "start", this, &elastic::loadtest::Application::running);
	fsm_.addStateTransition('R', 'I', "stop", this, &elastic::loadtest::Application::stopped);
	fsm_.setInitialState('I');
	fsm_.reset();

	process_ = toolbox::task::bind(this, &elastic::loadtest::Application::process, "process");

	toolbox::task::getWorkLoopFactory()->getWorkLoop("elastic-loadtest-workloop", "waiting")->activate();
	toolbox::task::getWorkLoopFactory()->getWorkLoop("elastic-loadtest-workloop", "waiting")->submit(process_);

	flashlistSettingsBaseUrl_ = "";
	getApplicationInfoSpace()->fireItemAvailable("flashlistSettingsBaseUrl",&flashlistSettingsBaseUrl_);

	frequency_ = 1;
	bulkSize_ = 1;
	flashlistDef_ = 0;
	timeLapse_ = 1;

}

std::string elastic::loadtest::Application::generateString(const int len)
{
	std::string s;

	for (int i = 0; i < len; ++i)
	{
		s += elastic::loadtest::alphanum[rand() % (sizeof(alphanum) - 1)];
	}
	return s;

}

unsigned long elastic::loadtest::Application::generateNumber(unsigned long nMin, unsigned long nMax)
{
	return nMin + (unsigned long)( (double)rand() / (9007199254740992 + 1) * (nMax - nMin+1) );
}


xdata::Serializable * elastic::loadtest::Application::generateValue ( const std::string & type )
{
	if ( type == "unsigned long")
	{
		// max value than 9007199254740992 (2^53).
		return new xdata::UnsignedLong(this->generateNumber(0,9007199254740992));

	}
	else if (type == "string")
	{
		xdata::String * value = new xdata::String();
		*value = this->generateString(32);
		return value;
	}
	else
	{
		std::stringstream msg;
		msg << "type: '" << type << "' is not supported";
		XCEPT_RAISE(elastic::loadtest::exception::Exception, msg.str());
	}
	/*
	xdata::UnsignedLong  number;
	number = 123;
	table.setValueAt(0,"Number", number);

	xdata::UnsignedInteger  numberInt;
	numberInt = (123 + 0xFFFF0000);
	table.setValueAt(0,"NumberInt", numberInt);

	xdata::UnsignedInteger64  numberInt64;
	numberInt64 = (((xdata::UnsignedInteger64T)123) + 0xFFFFFFFF00000000LL);
	table.setValueAt(0,"NumberInt64", numberInt64);

	xdata::String  time;
	time = "10:00:00";
	table.setValueAt(0,"Time", time);

	xdata::Double  b;
	b = 100.4;
	table.setValueAt(0,"Bandwidth", b);

	xdata::Vector<xdata::Double>  plotDouble;
	xdata::Double  numberDouble;
	for (size_t j = 0; j< 10; j++)
	{
		numberDouble = j;
		plotDouble.push_back(numberDouble);
	}
	table.setValueAt(0,"PlotDouble", plotDouble);
	*/


}


bool elastic::loadtest::Application::process ( toolbox::task::WorkLoop* wl )
{
	//
	// send according to frequency and available memory
	//

	for (;;)
	{
		mutex_.take();

		if (fsm_.getCurrentState() == 'R')
		{
			mutex_.give();


			for (size_t n = 0; n < frequency_; n++)
			{
				if (!pool_->isHighThresholdExceeded())
				{
					xdata::Table table;
					// add table definitions
					xdata::String context;
					context = getApplicationContext()->getContextDescriptor()->getURL();
					std::vector<xmas::ItemDefinition*> items = flashlistDef_->getItems();
					for (std::vector<xmas::ItemDefinition*>::iterator i= items.begin(); i != items.end(); i++ )
					{
						std::string name = (*i)->getProperty("name");
						std::string type = (*i)->getProperty("type");
						table.addColumn(name, type);

						for (size_t i = 0; i < bulkSize_; i++)
						{
							if ( name == "context")
							{
								table.setValueAt(i, name, context);
							}
							else
							{
								xdata::Serializable * object = this->generateValue(type);
								table.setValueAt(i, name, *object);
								delete object;
							}
						}
					}

					// Send here
					this->send(table);
				}
				else
				{
					while (pool_->isLowThresholdExceeded())
					{
						::sched_yield();
					}
				}
			}

			sleep(timeLapse_);
		}
		else
		{
			mutex_.give();
			return true;
		}
	}
	return true;
}

void elastic::loadtest::Application::send(xdata::Table & table ) throw (elastic::loadtest::exception::Exception)
{
	// debugging
	//this->toCSV(&table, std::cout);

	//
	// allocate buffer
	//
	toolbox::mem::Reference* ref = 0;
	try
	{
		ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, 0x400000);
	}
	catch (toolbox::mem::exception::Exception & ex)
	{
		//XCEPT_RETHROW(elastic::loadtest::exception, "Failed to allocate message for monitor report", ex);
	}
	//
	// serialize table into buffer
	//
	xdata::exdr::FixedSizeOutputStreamBuffer outBuffer((char*) ref->getDataLocation(), 0x400000);
	xdata::exdr::Serializer serializer;
	serializer.exportAll(&table, &outBuffer);

	ref->setDataSize(outBuffer.tellp());

	//
	// Prepare send
	//
    toolbox::net::URL at(getApplicationContext()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN());

    xdata::Properties plist;
    plist.setProperty("urn:b2in-protocol:service", "sensord");
    plist.setProperty("urn:b2in-protocol:action", "flashlist");

    plist.setProperty("urn:xmas-flashlist:name", "urn:xdaq-flashlist:" + flashlist_);
    plist.setProperty("urn:xmas-flashlist:version", "");
    plist.setProperty("urn:xmas-flashlist:tag", "");
    plist.setProperty("urn:xmas-flashlist:originator", at.toString());

	dynamic_cast<b2in::nub::Messenger&>(*messenger_).send(ref,plist,0,0);

}

void elastic::loadtest::Application::start(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	if (fsm_.getCurrentState() == 'I')
	{

		cgicc::Cgicc cgi(in);

		flashlist_ = xgi::Utils::getFormElement(cgi, "flashlist")->getValue();
		bulkSize_ =  toolbox::toLong(xgi::Utils::getFormElement(cgi, "bulksize")->getValue());
		frequency_ =  toolbox::toLong(xgi::Utils::getFormElement(cgi, "frequency")->getValue());
		std::string value =  xgi::Utils::getFormElement(cgi, "timelapse")->getValue();

		if (value != "" )
		{
			timeLapse_ =  toolbox::toLong(value);
		}

		if ( flashlistDef_ != 0 )
				delete flashlistDef_;
		try
		{
			flashlistDef_ = this->loadFlashlistDefinition(flashlist_);

			std::string qname = flashlistDef_->getProperty("name");
			std::set<std::string> key = toolbox::parseTokenSet(flashlistDef_->getProperty("key"), ",");
		}

		catch(elastic::loadtest::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to load flashlist definition for '" << flashlist_ << "' ";
			XCEPT_RETHROW(elastic::loadtest::exception::Exception, msg.str(), e);
			//LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		}

		toolbox::Event::Reference e(new toolbox::Event("start", this));
		fsm_.fireEvent(e);
	}
}

void elastic::loadtest::Application::stop(xgi::Input * in, xgi::Output * out ) throw (xgi::exception::Exception)
{
	if (fsm_.getCurrentState() == 'R')
	{
		toolbox::Event::Reference e(new toolbox::Event("stop", this));
		fsm_.fireEvent(e);
	}
}

void elastic::loadtest::Application::stopped (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{

}
void elastic::loadtest::Application::running (toolbox::Event::Reference e) throw (toolbox::fsm::exception::Exception)
{
}

xmas::FlashListDefinition* elastic::loadtest::Application::loadFlashlistDefinition(const std::string & fname) throw (elastic::loadtest::exception::Exception)
{

	std::string baseUrl = flashlistSettingsBaseUrl_.toString();
	std::vector<std::string> paths;
	try
	{
		paths = toolbox::getRuntime()->expandPathName(baseUrl);
	}
	catch (toolbox::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to expand path name '" << baseUrl << "'";
		XCEPT_RETHROW(elastic::loadtest::exception::Exception, msg.str(), e);

	}

	if ( paths.size() != 1 )
	{
		std::stringstream msg;
		msg << "Ambiguous path name '" << baseUrl << "' resolve to multiple files";
		XCEPT_RAISE(elastic::loadtest::exception::Exception, msg.str());

	}
	std::string base = paths[0];


	std::string href	= base + "/flash/" + fname + ".flash";
	std::string qname = "urn:xdaq-flashlist:"+ fname;
	DOMDocument* doc = 0;
	try
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), "load flashlist definition from file  '" << href << "'");
		//std::string filename = "file://" + href;
		//std::cout << "Load XML " << filename << std::endl;
		doc = xoap::getDOMParserFactory()->get("configure")->loadXML(href);
		if ( doc == 0 )
		{
			std::stringstream msg;
			msg << "Failed to load document ( null document) : '" << href << "'";
			XCEPT_RAISE(elastic::loadtest::exception::Exception, msg.str());
		}
		xmas::FlashListDefinition* flashlist = new elastic::loadtest::Application::FlashListDefinition(doc, href + "#" + qname );

		doc->release();
		doc = 0;

		return flashlist;
	}
	catch (xoap::exception::Exception& e)
	{
		if ( doc != 0 )
			doc->release();
		std::stringstream msg;
		msg << "Failed to load flashlist file from '" << href << "'";
		XCEPT_RETHROW(elastic::loadtest::exception::Exception, msg.str(), e);
	}
	catch (xmas::exception::Exception& e)
	{
		// this is a parse error
		if ( doc != 0 )
			doc->release();
		std::stringstream msg;
		msg << "Failed to parse configuration from '" << href << "'";
		XCEPT_RETHROW(elastic::loadtest::exception::Exception, msg.str(), e);
	}
}

elastic::loadtest::Application::FlashListDefinition::FlashListDefinition(DOMDocument* doc, const std::string& href): xmas::FlashListDefinition(0)
{
	this->setProperty("location", href); // must be url#qname
	this->setProperty ("local", "false");
	this->parse(doc, href);
}


void elastic::loadtest::Application::toCSV(xdata::Table * t, std::ostream& out)

{
	char delimiter = ',';

	std::vector<std::string> columns = t->getColumns();
	std::sort(columns.begin(), columns.end());

	for (std::vector<std::string>::size_type i = 0; i < columns.size(); i++ )
	{
		if (i != 0)
		{
			out << delimiter;
		}
		out << columns[i];
	}

	out << std::endl;

	for ( size_t j = 0; j < t->getRowCount(); ++j )
	{

		for (std::vector<std::string>::size_type k = 0; k < columns.size(); k++ )
		{
			if (k != 0)
			{
				out << delimiter;
			}
			xdata::Serializable * s = t->getValueAt(j, columns[k]);
			if (s != 0)
			{
				out << '"' << s->toString() << '"';
			}
			else
			{
				out << "null";
			}
		}
		out << std::endl;

	}

}



