<?php
    
    
    require 'vendor/autoload.php';
    
    include_once('xbeat-utils.php');
    include_once('config/config.php');
    
    header('Content-type: application/json');
    header('Content-Disposition: attachment; filename="testFile.json"');
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    
    $filter = array();
    $json = topHits($config['host'], $config['port'], $config['heart_index'], "Application", 5000, $filter);
    
    $first = TRUE;
    
    echo '{"table":{';
    echo '"definition":[';
    echo '{"key":"context", "type":"string"},';
    echo '{"key":"uuid", "type":"string"},';
    echo '{"key":"id", "type":"string"},';
    echo '{"key":"class", "type":"string"},';
    echo '{"key":"age", "type":"double"},';
    echo '{"key":"expires", "type":"string"},';
    echo '{"key":"updated", "type":"string"},';
    echo '{"key":"group", "type":"string"},';
    echo '{"key":"service", "type":"string"},';
    echo '{"key":"icon", "type":"string"}';
    echo '],';
    
    echo '"rows":[';
    
    foreach ($json['aggregations']['group_by_context']['buckets'] as $bucket) {
        foreach ($bucket ['top_metrics']['hits']['hits'] as  $hits) {
            
            if (!$first)
            {
                echo ',';
            }
            
            echo '{ "class" : "' .$hits['_source']['urn:xdaq-application-descriptor:class']. '"';
            echo ', "context" : "' .$hits['_source']['urn:xdaq-application-descriptor:context']. '"';
            echo ', "uuid" : "' .$hits['_source']['urn:xdaq-application-descriptor:uuid']. '"';
            echo ', "id" : "' .$hits['_source']['urn:xdaq-application-descriptor:id']. '"';
            echo ', "group" : "' .$hits['_source']['urn:xdaq-application-descriptor:group']. '"';
            if (isset($hits['_source']['urn:xdaq-application-descriptor:service']))
            {echo ', "service" : "' .$hits['_source']['urn:xdaq-application-descriptor:service']. '"';}
            else
            {echo ', "service" : ""';}
            $icon = "images/" . basename($hits['_source']['urn:xdaq-application-descriptor:icon']);
            if(!file_exists($icon))
            {
                $icon = "http://cms-service-xmas.web.cern.ch/cms-service-xmas/escaped-devel/images/default.png";
            }
            echo ', "icon" : "' .$icon. '"';
            echo ', "age" : 1.0'; //this is explicitly set
            echo ', "expires" : ""';
            echo ', "updated" : ""';
            echo '}';
            
            $first = FALSE;
        }
        
    }
    echo ']}';
    echo '}';


?>