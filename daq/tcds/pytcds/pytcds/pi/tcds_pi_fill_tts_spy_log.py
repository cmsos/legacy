#!/usr/bin/env python

###############################################################################
## Flipper about with the (forced) PI TTS in order to fill (part of)
## the TTS history.
###############################################################################

import random
import sys
import time

from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

def check_value(val_str):
    value = int(val_str)
    if value < 1:
        msg_base = "Sorry, but {0:d} as duration" \
                   " or number of transitions" \
                   " does not make sense."
        raise argparse.ArgumentTypeError(msg_base.format(value))
    return value

###############################################################################

class PITTS(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(PITTS, self).__init__(description, usage, epilog)
        self.num_transitions = None
        # End of pre_hook().

    def setup_parser_custom(self):
        super(PITTS, self).setup_parser_custom()
        parser = self.parser

        # Add the number of transitions to generate.
        help_str = "The number of TTS transitions to generate. " \
                   "Default: 1."
        parser.add_argument("--num-transitions",
                            type=check_value,
                            default=1,
                            help=help_str)

        # Add the desired (approximate) time window the whole sequence
        # should take.
        help_str = "Desired (approximate) time window " \
                   "for the full sequence (in seconds). " \
                   "Default: as little as possible."
        parser.add_argument("--duration",
                            type=check_value,
                            default=None,
                            help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        super(PITTS, self).handle_args()
        # Extract the number of transitions requested.
        self.num_transitions = int(self.args.num_transitions)
        self.duration = self.args.duration
        # End of handle_args().

    def main(self):
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)

        #----------

        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        pi = get_pi_hw(network_address, controlhub_address, verbose=self.verbose)

        if not pi:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        sleep_time = None
        if not self.duration is None:
            sleep_time = self.duration
            if self.num_transitions > 1:
                sleep_time = 1. * self.duration / (self.num_transitions - 1)

        # Configure the TTS player.
        desired_values = {
            "pi.tts_output_control.tts_force_player.wrap_around": 0,
            "pi.tts_output_control.tts_force_player.last_entry": 0
            }
        current_values = {}
        for reg_name in desired_values.keys():
            current_values[reg_name] = pi.read(reg_name)

        for (reg_name, val) in desired_values.iteritems():
            pi.write(reg_name, val)

        # val_prev = None
        helper = max(self.num_transitions / 10, 1)
        for i in xrange(self.num_transitions):
            # tts_state = val_prev
            # while tts_state == val_prev:
            #     tts_state = random.randint(0x00, 0xff)
            # val_prev = tts_state

            val_prev = pi.read("pi.tts_output_control.tts_force_player.pattern.entry0.value")
            tts_state = val_prev + 1
            valid_states = [0, 1, 2, 4, 8, 0xc, 0xf]
            while not tts_state in valid_states:
                tts_state += 1
            if tts_state == max(valid_states):
                tts_state = min(valid_states)
            if self.verbose:
                print "Forcing TTS state to {0:d}.".format(tts_state)

            pi.write("pi.tts_output_control.tts_force_player.pattern.entry0.value",
                     tts_state)

            if (i == 0):
                # Now switch from the TTS OR output to the TTS player output.
                pi.write("pi.tts_output_control.tts_out_enable", 0x0)
                # And make sure the TTS player is enabled. (And force
                # disable-enable the player to load the above settings.)
                if current_values != desired_values:
                    pi.write("pi.tts_output_control.tts_force_player.enabled", 0)
                pi.write("pi.tts_output_control.tts_force_player.enabled", 1)

            if sleep_time:
                time.sleep(sleep_time)

            if (i + 1) >= helper:
                perc = 100. * (i + 1) / self.num_transitions
                print "{0:5.1f}% done".format(perc)
                helper += max(self.num_transitions / 10, 1)

        # End of main().

    # End of class PITTS.

###############################################################################

if __name__ == "__main__":

    desc_str = "Flipper about with the (forced) PI TTS " \
               "in order to fill (part of) the TTS history."
    res = PITTS(desc_str).run()
    print "Done"
    sys.exit(res)

###############################################################################
