###############################################################################
## Utilities related to the BOBR VME board.
###############################################################################

import time

from pytcds.bobr.tcds_bobr_constants import ADDRESS_TABLE_FILE_NAME_BOBR
from pytcds.utils.tcds_constants import board_names
from pytcds.utils.tcds_constants import BOARD_TYPE_BOBR
from pytcds.utils.tcds_vmeboard import TCDSVME
from pytcds.utils.tcds_utils_vme import get_hw_vme

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class BOBR(TCDSVME):

    @staticmethod
    def get_base_address(slot_number):
        res = 0
        # End of get_base_address().
        return res

    def __init__(self, board_type, vme_chain, vme_unit, slot_number, nodes, verbose=False):
        base_address = BOBR.get_base_address(slot_number)
        super(BOBR, self).__init__(board_type, vme_chain, vme_unit, slot_number, base_address, nodes, verbose)
        self.slot_number = slot_number
        # End of __init__().

    def get_firmware_version(self, which="system"):
        res = "n/a"
        if which == "system":
            tmp = self.read("firmware_id")
            res = "{0:x}".format(tmp)
        # End of get_firmware_version()
        return res

    def get_firmware_date(self, which=None):
        res = "n/a"
        # End of get_firmware_date()
        return res

    # End of class BOBR.

###############################################################################

def get_bobr_hw(vme_chain,
                vme_unit,
                vme_slot,
                verbose=False):

    bobr = get_hw_vme(BOARD_TYPE_BOBR,
                      ADDRESS_TABLE_FILE_NAME_BOBR,
                      vme_slot,
                      vme_chain,
                      vme_unit,
                      verbose,
                      BOBR)

    return bobr

###############################################################################
