// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "pheaps/Application.h"
#include "pheaps/cmem_rcc/Allocator.h"

#include "xcept/tools.h"
#include "xgi/framework/Method.h"

//
// provides factory method for instantiation of Pheaps application
//
XDAQ_INSTANTIATOR_IMPL(pheaps::Application)

pheaps::Application::Application(xdaq::ApplicationStub * s)  
	: xdaq::Application(s), xgi::framework::UIManager(this) 
{	

	s->getDescriptor()->setAttribute("icon","/pheaps/images/hardware_chip_64.png");

	s->getDescriptor()->setAttribute("icon16","/pheaps/images/hardware_chip_16.png");
	s->getDescriptor()->setAttribute("icon24","/pheaps/images/hardware_chip_24.png");
	s->getDescriptor()->setAttribute("icon32","/pheaps/images/hardware_chip_32.png");
	s->getDescriptor()->setAttribute("icon48","/pheaps/images/hardware_chip_48.png");
	s->getDescriptor()->setAttribute("icon64","/pheaps/images/hardware_chip_64.png");
	s->getDescriptor()->setAttribute("icon128","/pheaps/images/hardware_chip_128.png");

	xgi::framework::deferredbind(this, this, &pheaps::Application::Default, "Default");
	xgi::bind(this, &pheaps::Application::add, 	  "add");
	xgi::bind(this, &pheaps::Application::remove,  "remove");
	
	getApplicationInfoSpace()->fireItemAvailable ("autoInstalledPools", &autoInstalledPools_);
	getApplicationInfoSpace()->addItemChangedListener ("autoInstalledPools", this);
}

void pheaps::Application::actionPerformed(xdata::Event& e)
{
	if (e.type() == "ItemChangedEvent")
	{
		std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
		if (item == "autoInstalledPools")
		{
			for (xdata::Vector<xdata::Bag<pheaps::Application::PoolConfiguration> >::size_type i = 0; i < autoInstalledPools_.size(); i++)
			{
				try
				{
					this->addPool(
						autoInstalledPools_[i].bag.nss,
						autoInstalledPools_[i].bag.type,
						autoInstalledPools_[i].bag.size );
				}
				catch(xdaq::exception::Exception & xe)
				{
					LOG4CPLUS_ERROR (getApplicationLogger(), xcept::stdformat_exception_history(xe));
				}	
			}
		}
	}
}

	
void pheaps::Application::Default(xgi::Input * in, xgi::Output * out ) 
{
	
	this->heapSetupPage(in,out);
}

void pheaps::Application::addPool (const std::string& name, const std::string& type, double size)
	
{
	try
	{			
		if ( heaps_.find(name) != heaps_.end() )
		{
			XCEPT_RAISE(xdaq::exception::Exception, "heap name already existing" );
		}

		if ( size < 0.0 )
		{
			XCEPT_RAISE(xdaq::exception::Exception, "invalid memory size" );			
		}

		size_t committedSize = (size_t)(size * 0x100000); // convert to bytes			

		toolbox::net::URN urn("toolbox-mem-pool",name);
		if ( type == "cmem_rcc" )
                {
                        pheaps::cmem_rcc::Allocator* a = new pheaps::cmem_rcc::Allocator(committedSize);
                        heaps_[urn.toString()] = toolbox::mem::getMemoryPoolFactory()->createPool(urn,a);
                }
		else
		{
			XCEPT_RAISE(xdaq::exception::Exception,"unsupported memory type");
		}
	}
	catch(toolbox::mem::exception::Exception & mex)
	{
		XCEPT_RETHROW(xdaq::exception::Exception, "cannot add pool", mex);
	}
}
       		
void pheaps::Application::add(xgi::Input * in, xgi::Output * out ) 
{
	try
	{			
		cgicc::Cgicc cgi(in);

		std::string name = cgi["heapName"]->getValue();
		double size = cgi["committedSize"]->getDoubleValue();
		std::string type = cgi["type"]->getValue();
		
		this->addPool(name, type, size);

		this->heapSetupPage(in,out);
	}
	catch(xdaq::exception::Exception& xe)
	{
		XCEPT_RETHROW(xgi::exception::Exception,"Failed to add pool", xe);
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}
	
void pheaps::Application::remove(xgi::Input * in, xgi::Output * out ) 
{
	#warning "check that there are no blocks in use by other  applications"
	try
	{			
		cgicc::Cgicc cgi(in);

		std::string heapURN = cgi["heapURN"]->getValue();
		toolbox::net::URN urn(heapURN);
		toolbox::mem::Allocator *allocator = heaps_[heapURN]->getAllocator();
		toolbox::mem::getMemoryPoolFactory()->destroyPool(urn);
		// allocator can only be delete after destruction of pool
		delete allocator;
		heaps_.erase(heapURN);

		this->heapSetupPage(in,out);

	}
	catch (toolbox::mem::exception::Exception& mex)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "Failed to remove pool", mex);
	}
	catch (const std::exception & e)
	{
		XCEPT_RAISE(xgi::exception::Exception, e.what());
	}
}
	
void pheaps::Application::heapSetupPage(xgi::Input * in, xgi::Output * out )
{
	std::string url = "/";
        url += getApplicationDescriptor()->getURN();
	
	*out << "<a href=\"" ;
	
	*out << url << "\"><img border=\"0\" src=\"/daq/pheaps/images/Pheaps.jpg\" title=\"XMem\" alt=\"\" style=\"width: 95px; height: 95px;\"></a></td>";
	
        *out << cgicc::br() << cgicc::br();

	// Add memory heap form

	std::string method = url;
	method += "/add";

	*out << cgicc::fieldset();
	*out << cgicc::legend("Add Memory Heap");


	// Memory heap table
	*out << cgicc::table()
		.set("cellpadding","5")
		.set("cellspacing","0")
		.set("border","")
		.set("style","width: 100%;");

	*out << cgicc::tbody();
	*out << cgicc::tr();
	*out << cgicc::td("Heap Name").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::td("Size").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::td("Type").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::td("Action").set("style","vertical-align: top; font-weight: bold;");
	*out << cgicc::tr() << std::endl;;
	std::map<std::string, toolbox::mem::Pool* , std::less<std::string> >::iterator i = heaps_.begin();
	for ( i = heaps_.begin(); i != heaps_.end(); i++ )
	{
		std::string removeHeapURL = toolbox::toString("<a href=\"%s/remove?heapURN=%s\">remove</a>",url.c_str(), ((*i).first).c_str());
		*out << cgicc::tr();
		*out << cgicc::td((*i).first) << std::endl;
		*out << cgicc::td(toolbox::toString("%d",((*i).second)->getAllocator()->getCommittedSize())) << std::endl;
		*out << cgicc::td(((*i).second)->getAllocator()->type()) << std::endl;
		*out << cgicc::td(removeHeapURL) << std::endl;
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();
	*out << cgicc::br();

	bool cmem_rccPresent = this->isDriverPresent("/dev/cmem_rcc0");
	if ( cmem_rccPresent)
	{
		*out << cgicc::table()
			.set("cellpadding","0")
			.set("cellspacing","0")
			.set("border","0")
			.set("style","width: 100%;");
		*out << cgicc::tr();
		*out << cgicc::td();
		*out << cgicc::form().set("method","GET").set("action", method) << std::endl;
		*out << " Heap Name" << std::endl;
		*out << cgicc::input().set("type","text").set("name","heapName")  << std::endl;
		*out << "Installation mode" << std::endl;
		*out << cgicc::select().set("name","type") << std::endl;
		if ( cmem_rccPresent )
			*out << cgicc::option("cmem_rcc")  << std::endl;

		*out << cgicc::select()  << std::endl;
		*out << "Committed Size (MB)" << std::endl;
		*out << cgicc::input().set("type","text").set("name","committedSize").set("size","4")  << std::endl;
		*out << cgicc::input().set("type","submit").set("value","Add")  << std::endl;
		*out << cgicc::tr();
		*out << cgicc::form() << std::endl;
		*out << cgicc::table();
	}
	else
	{
		*out << cgicc::p("WARNING! no driver available for creating physical heaps").set("style","font-size: 10pt; font-family: arial; text-align: left; width: 100%; color: red;");
	}

	*out << cgicc::br();
	*out << cgicc::fieldset();

	*out << cgicc::br() << cgicc::br();
}
	
	
bool pheaps::Application::isDriverPresent(const std::string & name)
{
	int fd = ::open(name.c_str(), O_RDWR);
  	if (fd <= 0) 
	{			
	  	return false;
	}
	::close(fd);
	return true;
}
