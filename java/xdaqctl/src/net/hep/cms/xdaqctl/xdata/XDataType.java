package net.hep.cms.xdaqctl.xdata;

import java.lang.String;
import java.lang.Integer;
import java.util.HashMap;
import java.util.Map;

/**
 * Enumerated list of xdata data types.
 * '_'(under-score) delimited upper-case name corresponds to
 * a camel-cased class name, like UNSIGNED_INTEGER64 -> UnsignedInteger64 .
 * 
 * @author A. Oh &lt;Alexander.Oh@cern.ch&gt; and I. Suzuki &lt;ichiro@fnal.gov&gt;
 */
public enum XDataType {
	INTEGER(1, "Integer"), 
	FLOAT(2, "Float"),
	DOUBLE(3, "Double"),
	STRING(4, "String"),
	UNSIGNED_LONG(5, "UnsignedLong"),
	UNSIGNED_SHORT(6, "UnsignedShort"),
	BOOLEAN(7, "Boolean"),
	VECTOR(9, "Vector"),
	TABLE(11, "Table"),
	UNSIGNED_INTEGER(13, "UnsignedInteger"),	
	UNSIGNED_INTEGER64(14, "UnsignedInteger64"),
	UNSIGNED_INTEGER32(15, "UnsignedInteger32"),
	INTEGER32(16, "Integer32"),	
	INTEGER64(17, "Integer64"), 
	TIME_VAL(18, "TimeVal")	
	;
	
	static Map<Integer, XDataType> codeMap;
	
	static {
		codeMap = new HashMap<Integer, XDataType>();
		
		for (XDataType item: XDataType.class.getEnumConstants()) {
			codeMap.put(new Integer(item.getEXDRCode()), item);
		}
	}
	
	private int exdrCode;
	private String name;
	
	XDataType(int exdrCode, String name) {
		this.exdrCode = exdrCode;
		this.name = name;
	}
	
	int getEXDRCode() { return exdrCode; }
	
	XDataItem getInstance() {
		String fullname = XDataType.class.getPackage().getName() + "." + name;
		XDataItem item = null;
		
		try {
			Class klass = Class.forName(fullname);
			item = (XDataItem)klass.newInstance();
		} catch (Exception ignored) {}
		
		return item;
	}
	
	static XDataItem getInstance(int exdrCode) {
		return getEnum(exdrCode).getInstance();
	}

	public static XDataType getEnum(int exdrCode) {
		return (XDataType)codeMap.get(exdrCode);
	}
}

// End of file
// vim: set sw=4 ts=4:
