package xdaq.tools.dstorebrowser;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.event.*;

import javax.xml.parsers.*;


import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;


import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;

import xdaq.tools.dstorebrowser.ConnectListener;



//import xdaq.tools.win.xdaqWinMenuBar;
import xdaq.tools.win.xdaqDOMDocument;
//import xdaq.tools.win.xdaqWinTreeSelectionListener;
import xdaq.tools.dstorebrowser.dstoreTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.xdaqSOAPException;
import xdaq.tools.win.xdaqDOMDocumentException;
import xdaq.tools.win.xdaqWinException;

import xdaq.tools.soaputils.HTTPServer;
import xdaq.tools.dstorebrowser.ConnectResponseListener;
import xdaq.tools.dstorebrowser.ConnectListener;

import org.apache.log4j.helpers.LogLog;

public class dstorebrowser extends JFrame
{
    Container pane_;
    Box inputPanel_;
    JToolBar controlPane_;
    Box connectionPane_;
    dstoreTree tree_=null;
    JTextField urlXDAQInput_;
    JTextField urlDstoreInput_;
    JTextField tidDstoreInput_, tiddstorebrowserInput_;
    JComboBox sourceTypeComboBox;
    JScrollPane treeScrollPane;
    JButton connect_, new_, commit_, cut_, copy_, paste_, rollback_;
    HTTPServer server_;
    String targetAddr="", initiatorAddr="", xdaqurl="", sourceType="", dstoreurl, XDAQIPAddr_;
    HashMap requestedNodes;
    int context = 0;
    int dstoreAppTid_, dstoreBrowserTid_, dstoreBrowserPort_;
    Vector changeList = new Vector();

    public dstorebrowser ( String XDAQIPAddr, int dstoreAppTid, int dstoreBrowserTid, int dstoreBrowserPort, String initialURL )
	{
	    super ("XDAQ Data Store Browser");
	    dstoreAppTid_ = dstoreAppTid;
	    dstoreBrowserTid_ = dstoreBrowserTid;
	    XDAQIPAddr_ = XDAQIPAddr;
	    dstoreBrowserPort_ = dstoreBrowserPort;
	    dstoreurl = initialURL;
	    setSize ( 800, 600);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	    LogLog.setQuietMode ( true );

	    requestedNodes = new HashMap();

	    inputPanel_ = Box.createVerticalBox();
	    drawConnectionPane();
	    drawControlPane();
	    
	    pane_ = this.getContentPane();
	    pane_.add(inputPanel_,BorderLayout.NORTH);
	    pane_.add(controlPane_,BorderLayout.SOUTH);
	    pane_.add(new JPanel(),BorderLayout.WEST);
	    pane_.add(new JPanel(),BorderLayout.EAST);
	    
	    server_ = new HTTPServer (dstoreBrowserPort);
	    server_.registerListener ( "", "receiveDataStoreData", new ConnectResponseListener (this) );
	    server_.registerListener ( "", "receiveBranch", new ExpandResponseListener (this) );
	    this.show();
	}
    
    private void drawConnectionPane()
	{
	    connectionPane_ = Box.createVerticalBox();
//	    connectionPane_.setBorder (new TitledBorder ("DStore Connection"));
	    
// xdaq related inputs 
	    JPanel xdaqInputPanel_ = new JPanel();
	    xdaqInputPanel_.setLayout(new GridLayout(2,2));
	    
	    JPanel tidDstoreLabelPanel_ = new JPanel();
	    tidDstoreLabelPanel_.setLayout(new FlowLayout(FlowLayout.LEFT));
	    JLabel tidDstoreLabel = new JLabel ("Data Store application tid:");
	    tidDstoreLabelPanel_.add (tidDstoreLabel);
	    tidDstoreInput_ = new JTextField (Integer.toString(dstoreAppTid_),3);
	    tidDstoreLabelPanel_.add (tidDstoreInput_);
	    xdaqInputPanel_.add(tidDstoreLabelPanel_);

	    JPanel urlXDAQLabelPanel_ = new JPanel();
	    urlXDAQLabelPanel_.setLayout(new FlowLayout(FlowLayout.LEFT));
	    JLabel urlXDAQLabel = new JLabel ("XDAQ URL:");
	    urlXDAQLabelPanel_.add(urlXDAQLabel);
	    urlXDAQInput_ = new JTextField (XDAQIPAddr_,20);
	    urlXDAQLabelPanel_.add(urlXDAQInput_);
	    xdaqInputPanel_.add(urlXDAQLabelPanel_);
	    
	    JPanel tiddstorebrowserLabelPanel_ = new JPanel();
	    tiddstorebrowserLabelPanel_.setLayout(new FlowLayout(FlowLayout.LEFT));
	    JLabel tiddstorebrowserLabel = new JLabel ("Data Store browser tid:");
	    tiddstorebrowserLabelPanel_.add (tiddstorebrowserLabel);
	    tiddstorebrowserInput_ = new JTextField (Integer.toString(dstoreBrowserTid_),3);
	    tiddstorebrowserLabelPanel_.add (tiddstorebrowserInput_);
	    xdaqInputPanel_.add(tiddstorebrowserLabelPanel_);

	    connectionPane_.add(xdaqInputPanel_);
	    
// data source related inputs

	    JPanel urlDstoreLabelPanel_ = new JPanel();
	    urlDstoreLabelPanel_.setLayout(new FlowLayout(FlowLayout.LEFT));
	    JLabel urlDstoreLabel = new JLabel ("Data Store URL:");
	    urlDstoreLabelPanel_.add(urlDstoreLabel);
	    urlDstoreInput_ = new JTextField (dstoreurl,50);
	    urlDstoreLabelPanel_.add (urlDstoreInput_);
	    connectionPane_.add (urlDstoreLabelPanel_);

//	    sourceInputPanel_.add(new JPanel());
	    
	    JPanel sourceInputPanel_ = new JPanel();
	    sourceInputPanel_.setLayout(new GridLayout(1,2));

	    JPanel sourceTypeComboBoxLablePanel_ = new JPanel();
	    sourceTypeComboBoxLablePanel_.setLayout(new FlowLayout(FlowLayout.LEFT));
	    JLabel sourceTypeComboBoxLable = new JLabel ("Data source type:");
	    sourceTypeComboBoxLablePanel_.add (sourceTypeComboBoxLable);
	    sourceTypeComboBox = new JComboBox();
	    sourceTypeComboBox.addItem("FILE");
	    sourceTypeComboBox.addItem("ORACLE");
	    sourceTypeComboBox.addItem("MYSQL");
	    sourceTypeComboBox.setSelectedIndex(1);
	    sourceTypeComboBoxLablePanel_.add (sourceTypeComboBox);
	    sourceInputPanel_.add (sourceTypeComboBoxLablePanel_);

	    connect_ = new JButton ("Connect");
	    connect_.setToolTipText("Connect to data source and retrieve data");
	    connect_.addActionListener ( new ConnectListener(this) );
	    JPanel butFram = new JPanel();
	    butFram.add(connect_);
	    sourceInputPanel_.add(butFram);

	    connectionPane_.add(sourceInputPanel_);
	    inputPanel_.add (connectionPane_);
	}

	private void drawControlPane()
	{
		controlPane_ = new JToolBar();
	
		commit_ = new JButton ("Commit");
		commit_.setEnabled(false);
		commit_.addActionListener ( new ConnectListener(this) );
		rollback_ = new JButton ("Rollback");
		rollback_.setEnabled(false);
		rollback_.addActionListener ( new ConnectListener(this) );
		new_ = new JButton ("New");
		new_.setEnabled(false);
		cut_ = new JButton ("Cut");
		cut_.setEnabled(false);
		copy_ = new JButton ("Copy");
		copy_.setEnabled(false);
		paste_ = new JButton ("Paste");
		paste_.setEnabled(false);

		controlPane_.add(commit_);
		controlPane_.add(rollback_);
		controlPane_.add(new_);
		controlPane_.add(cut_);
		controlPane_.add(copy_);	
		controlPane_.add(paste_);
	}
	
	
    public void setConnectionParameters()
	{
	    targetAddr = tidDstoreInput_.getText();
	    initiatorAddr = tiddstorebrowserInput_.getText();
	    xdaqurl = urlXDAQInput_.getText();
	    sourceType = (String) sourceTypeComboBox.getSelectedItem();
	    dstoreurl = urlDstoreInput_.getText();
	}

    public SOAPMessage requestData(DefaultMutableTreeNode dfTopNode, String constraints)
        {
	    try {
		xdaqSOAPMessage msg =  new xdaqSOAPMessage ( "getData", targetAddr );

		// This has to go into xdaqSOAPMessage!
		msg.addAttribute ("originator", initiatorAddr);

		SOAPBody body = msg.getSOAPMessage().getSOAPPart().getEnvelope().getBody();
		SOAPElement contextElement = body.addChildElement("context");
		SOAPElement dbtypeElement = body.addChildElement("dbtype");
		dbtypeElement.addTextNode(sourceType);
		SOAPElement levelsElement = body.addChildElement("levels");
		SOAPElement callbackElement = body.addChildElement("callback");

		java.util.Iterator i = body.getChildElements();
		SOAPElement commandName = (SOAPElement) i.next();
		if (dfTopNode == null) 
		{
		    if (tree_ != null) 
		    {
//			tree_.remove();
			tree_.setVisible(false);
		    }
		    context++;
		    levelsElement.addTextNode("1");
		    commandName.addTextNode (dstoreurl);
		    callbackElement.addTextNode("receiveDataStoreData");
		}
		else
		{
		    levelsElement.addTextNode("1");
		    callbackElement.addTextNode("receiveBranch");
		    xdaqWinTreeAdapterNode topNode = (xdaqWinTreeAdapterNode)(dfTopNode.getUserObject());
		    String nodeIdent = topNode.getNodeName()+"?"+constraints;
		    requestedNodes.put(nodeIdent,dfTopNode);
		    try{
			URL rootNodeUrl = new URL(dstoreurl);
			URL leafNodeUrl= new URL(rootNodeUrl.getProtocol(),
						 rootNodeUrl.getUserInfo()+"@"+rootNodeUrl.getHost(),
						 rootNodeUrl.getPort(),
						 "/"+nodeIdent);
			commandName.addTextNode (leafNodeUrl.toString());
		    }
		    catch(MalformedURLException mue)
		    {
			System.out.println("Exception occured: "+ mue.getMessage());
		    }
		}
		contextElement.addTextNode("dbBrowser"+Integer.toString(context));
		xdaqSOAPConnection con = new xdaqSOAPConnection();
//  		try{msg.getSOAPMessage().writeTo(System.out);}
//  		catch(IOException e) {}
		return con.call ( msg, xdaqurl );
		
	    } catch (xdaqSOAPException e) {
		System.out.println (e);
	    } catch (SOAPException ex) {
		System.out.println (ex);
	    }
	    
	    return null;
        }

    void writeTree()
	{
	    try {
		xdaqSOAPMessage msg =  new xdaqSOAPMessage ( "putData", targetAddr );
		
		SOAPMessage soapMsg = msg.getSOAPMessage();

		// This has to go into xdaqSOAPMessage!
		msg.addAttribute ("originator", initiatorAddr);

		SOAPBody body = msg.getSOAPMessage().getSOAPPart().getEnvelope().getBody();
		java.util.Iterator i = body.getChildElements();
		SOAPElement commandName = (SOAPElement) i.next();
		NodeList childNodes = tree_.getDocument().getElementsByTagName ("DataSource").item(0).getChildNodes();
		for(int j=0 ; j<changeList.size() ; j++)
		{
		    DefaultMutableTreeNode nodeToWrite = (DefaultMutableTreeNode) (changeList.get(j));
		    xdaqWinTreeAdapterNode xdaqNodeToWrite = (xdaqWinTreeAdapterNode)nodeToWrite.getUserObject();
		    msg.setParameters(xdaqNodeToWrite.domNode, commandName);
		}

		// Cut away all nodes from the SOAP message that have an attribute "expandable"
		// Those are nodes that have not been modified
		msg.detachElementsByAttribute ("_type", "Expandable");

		SOAPElement contextElement = body.addChildElement("context");
		contextElement.addTextNode("dbBrowser"+Integer.toString(context));
		SOAPElement dbtypeElement = body.addChildElement("dbtype");
		dbtypeElement.addTextNode(sourceType);
// 		try{msg.getSOAPMessage().writeTo(System.out);}
// 		catch(IOException e) {}
		
		xdaqSOAPConnection con = new xdaqSOAPConnection();
		SOAPMessage reply = con.call ( msg, xdaqurl );
		if (reply == null)
		{
		    System.out.println ("SOAP call failed.");
		}
		commit_.setEnabled(false);
		rollback_.setEnabled(false);
		
	    } catch (xdaqSOAPException e) {
		System.out.println (e);
	    } catch (SOAPException ex) {
		System.out.println (ex);
	    }
	    while (! changeList.isEmpty())
	    {
		DefaultMutableTreeNode nodeToWrite = (DefaultMutableTreeNode)changeList.remove(0);
		xdaqWinTreeAdapterNode xdaqNodeToWrite = (xdaqWinTreeAdapterNode)nodeToWrite.getUserObject();
		NamedNodeMap attributes = xdaqNodeToWrite.domNode.getAttributes();
		String constraints = "", separator = "";
		for (int j = 0; j < attributes.getLength(); j++) {
		    String attrName = attributes.item(j).getNodeName();
		    if (! attrName.startsWith("_")) 
		    {
			constraints = constraints + separator + attrName + "=" + attributes.item(j).getNodeValue();
			separator = "&";
		    }
		}
		requestData(nodeToWrite,constraints);
	    }
	}

	TreePath[] getPaths()
	{
		return tree_.getSelectionModel().getSelectionPaths();
	}
		
	// Load an XML configuration
	//
	public void displayConfiguration (SOAPMessage msg) throws xdaqWinException
	{
	    //try {
	    xdaqSOAPMessage DOMTree = new xdaqSOAPMessage(msg);
	    org.w3c.dom.Document document = DOMTree.getDOM();
	    if (document != null) 
	    {
		if (tree_ != null) 
		{
		    pane_.remove (treeScrollPane );
		    commit_.setEnabled(false);
		    rollback_.setEnabled(false);
		}
		tree_ = new dstoreTree ( document, dstoreurl, this );
		ToolTipManager.sharedInstance().registerComponent(tree_);
		//tree_.addTreeSelectionListener ( new xdaqWinTreeSelectionListener( this ) );
		tree_.addTreeExpansionListener(new dstoreTreeWillExpandListener(this));
		treeScrollPane = new JScrollPane( tree_);
		pane_.add (treeScrollPane );
		pane_.validate();
	    } else {
	    }
	    //	} catch (xdaqDOMDocumentException e)
	    //{
	    //JOptionPane.showMessageDialog ( null, e.toString(), "Configuration load", JOptionPane.ERROR_MESSAGE );
	    //throw new xdaqWinException();
	    //} 	
	}
    
	public void expandLeaf (SOAPMessage msg) throws xdaqWinException
	{
	    xdaqSOAPMessage DOMTree = new xdaqSOAPMessage(msg);
	    org.w3c.dom.Document document = DOMTree.getDOM();
	    org.w3c.dom.Node rootNode = document.getElementsByTagName("receiveBranch").item(0).getFirstChild();
	    
	    NamedNodeMap attributes = rootNode.getAttributes();

	    if (attributes.getLength() == 0) {
		System.out.println ("Error. No attributes in SOAP message received.");
		return;
	    }

            // create identification string to get old leaf node from hash map
	    String nodeIdent = rootNode.getNodeName() +"?";
	    String separator="";
            for (int j = 0; j < attributes.getLength(); j++) {
		String attrName = attributes.item(j).getNodeName();
		if (! attrName.startsWith("_")) 
		{
		    nodeIdent = nodeIdent + separator + attrName + "=" + attributes.item(j).getNodeValue();
		    separator = "&";
		}
            }

	    DefaultMutableTreeNode leafNode = (DefaultMutableTreeNode)requestedNodes.get(nodeIdent);
	    requestedNodes.remove(nodeIdent);

	    if (leafNode == null) {
		System.out.println ("leaf node is null.");
		return;
	    }
 		org.w3c.dom.Node oldDomNode = ((xdaqWinTreeAdapterNode) leafNode.getUserObject()).domNode;
		org.w3c.dom.Node importedNode = oldDomNode.getOwnerDocument().importNode(rootNode,true);
		oldDomNode.getParentNode().replaceChild(importedNode,oldDomNode);
		
		//int index =  ((DefaultMutableTreeNode)leafNode.getParent()).getIndex (leafNode);
		leafNode.removeAllChildren(); // remove "retrieving data..." node

		// Create a tree of DefaultMutableTreeNodes that reflects a tree of xdaqWinTreeAdapterNodes
		xdaqWinTreeAdapterNode adapterNode = new xdaqWinTreeAdapterNode (importedNode,null);
		DefaultMutableTreeNode dfAdapterNode = tree_.createTree ( adapterNode );

		int nOfChildren = dfAdapterNode.getChildCount();
		for (int nElm=0 ; nElm<nOfChildren ; nElm++)
		{
		    leafNode.add ((DefaultMutableTreeNode) dfAdapterNode.getChildAt(0));
		}
		((xdaqWinTreeAdapterNode) leafNode.getUserObject()).domNode = importedNode;
		DefaultTreeModel model = (DefaultTreeModel) tree_.getModel();
		//model.reload ( (DefaultMutableTreeNode) dfAdapterNode.getParent() );
		model.nodeStructureChanged (leafNode  );
	}

	public dstoreTree getTree()
	{
		return tree_;
	}

}	
