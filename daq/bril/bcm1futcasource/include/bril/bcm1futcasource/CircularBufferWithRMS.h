//double getRMS(const double * buffer, size_t size, size_t )

#include <iostream>
#include <numeric>
#include <algorithm>
#include <iterator>
#include <boost/circular_buffer.hpp>
#include <math.h>

template <typename T> class square_accumulate
{
public:

    square_accumulate(void) : _sum(0) {}

    const T& result(void) const
    {
       return _sum;
    }

    void operator()(const T& val)
    {
        _sum += val * val;
    }

private:

    T _sum;
};

template <typename T> class CircularBufferWithRMS : public boost::circular_buffer<T>
{
public:

    CircularBufferWithRMS(T buffer_capacity) : boost::circular_buffer<T>(buffer_capacity) {}

    double calculateRMS(size_t N)
    {
        return sqrt(static_cast<double>(std::for_each(boost::circular_buffer<T>::end()-N, boost::circular_buffer<T>::end(), square_accumulate<T>()).result())/static_cast<double>(N));
    }
};

int main()
{
    CircularBufferWithRMS<int> cb(5);

    const int N = 2;
    
    cb.push_back(1);
    cb.push_back(2);
    cb.push_back(3);
    cb.push_back(4);
    cb.push_back(5);

    std::cout << cb.back() << std::endl;
    std::copy(cb.begin(), cb.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout<<std::endl;
    cb.push_back(6);    

    std::cout << cb.back() << std::endl;
    std::copy(cb.begin(), cb.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout<<std::endl;

    cb.push_back(7);    

    std::cout << cb.back() << std::endl;
    std::copy(cb.begin(), cb.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout<<std::endl;

    boost::circular_buffer<int>::iterator it = cb.end() - N;    

    std::copy(it, cb.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout<<std::endl;

    double rms = sqrt(static_cast<double>(std::for_each(it, cb.end(), square_accumulate<int>()).result())/static_cast<double>(N));

    std::cout << rms << " ?= " << cb.calculateRMS(N) << std::endl;



    return 0;
}