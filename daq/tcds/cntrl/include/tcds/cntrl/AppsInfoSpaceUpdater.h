#ifndef _tcds_cntrl_AppsInfoSpaceUpdater_h_
#define _tcds_cntrl_AppsInfoSpaceUpdater_h_

#include <string>

#include "xdata/Table.h"

#include "tcds/utils/InfoSpaceUpdater.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cntrl {

    class TCDSCentral;

    class AppsInfoSpaceUpdater : public tcds::utils::InfoSpaceUpdater
    {

    public:
      AppsInfoSpaceUpdater(tcds::cntrl::TCDSCentral& xdaqApp,
                           std::string const& groupName);
      virtual ~AppsInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    private:
      xdata::Table getAppsInfo() const;
      std::string getAppsInfoString() const;

      std::string const groupName_;
      xdata::Table appsInfo_;
      bool shouldCheckConfig_;
      std::string lasURL_;
      std::string flashlistName_;

    };

  } // namespace cntrl
} // namespace tcds

#endif // _tcds_cntrl_AppsInfoSpaceUpdater_h_
