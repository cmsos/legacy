#ifndef _tcds_hwlayervme_version_h_
#define _tcds_hwlayervme_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSHWLAYERVME_VERSION_MAJOR 3
#define TCDSHWLAYERVME_VERSION_MINOR 13
#define TCDSHWLAYERVME_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSHWLAYERVME_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSHWLAYERVME_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSHWLAYERVME_VERSION_CODE PACKAGE_VERSION_CODE(TCDSHWLAYERVME_VERSION_MAJOR,TCDSHWLAYERVME_VERSION_MINOR,TCDSHWLAYERVME_VERSION_PATCH)
#ifndef TCDSHWLAYERVME_PREVIOUS_VERSIONS
#define TCDSHWLAYERVME_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSHWLAYERVME_VERSION_MAJOR,TCDSHWLAYERVME_VERSION_MINOR,TCDSHWLAYERVME_VERSION_PATCH)
#else
#define TCDSHWLAYERVME_FULL_VERSION_LIST TCDSHWLAYERVME_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSHWLAYERVME_VERSION_MAJOR,TCDSHWLAYERVME_VERSION_MINOR,TCDSHWLAYERVME_VERSION_PATCH)
#endif

namespace tcdshwlayervme {

  const std::string package = "tcdshwlayervme";
  const std::string versions = TCDSHWLAYERVME_FULL_VERSION_LIST;
  const std::string description = "CMS TCDS helper software.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "Part of the CMS TCDS software.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdshwlayervme

#endif
