#ifndef _tcds_utils_Lock_h_
#define _tcds_utils_Lock_h_

#include "toolbox/BSem.h"

namespace tcds {
  namespace utils {

    class Lock
    {

    public:
      Lock(toolbox::BSem::State state=toolbox::BSem::EMPTY, bool recursive=true);
      ~Lock();

      void lock();
      void unlock();

    private:
      toolbox::BSem semaphore_;

      // Prevent copying.
      Lock(Lock const&);
      Lock& operator=(Lock const&);

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_Lock_h_
