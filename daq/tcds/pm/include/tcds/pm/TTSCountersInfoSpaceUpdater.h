#ifndef _tcds_pm_TTSCountersInfoSpaceUpdater_h_
#define _tcds_pm_TTSCountersInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class TCADevicePMCommonBase;

    class TTSCountersInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      TTSCountersInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::pm::TCADevicePMCommonBase const& hw);
      virtual ~TTSCountersInfoSpaceUpdater();

    protected:
      virtual void updateInfoSpaceImpl(tcds::utils::InfoSpaceHandler* const infoSpaceHandler);
      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::pm::TCADevicePMCommonBase const& getHw() const;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_TTSCountersInfoSpaceUpdater_h_
