/*
 * TestUtil
 *
 * $Id: TestUtil.java,v 4.3 2007/06/12 16:20:50 aohcms Exp $
 */

package net.hep.cms.xdaqctl;

import java.io.File;
import java.io.StringWriter;
import java.io.StringReader;

// DOM
import javax.xml.parsers.*;
import org.w3c.dom.*;
import com.sun.org.apache.xpath.internal.XPathAPI;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

/**
 * TestUtil
 */
class TestUtil
{
	static String compareDOMs(Node expectedDOM, Node testedDOM)
			throws TransformerException
	{
		return compareDOMs(expectedDOM, testedDOM, true);
	}

	static String compareDOMs(Node expectedDOM, Node testedDOM, boolean print)
			throws TransformerException
	{
		deleteEmptyHeader(expectedDOM);
		deleteEmptyHeader(testedDOM);
		normalizeTextNodes(expectedDOM);
		normalizeTextNodes(testedDOM);

		NodeList expectedList, testedList;

		// First, check only element nodes
		expectedList = XPathAPI.selectNodeList(expectedDOM, "//*");
		testedList = XPathAPI.selectNodeList(testedDOM, "//*");
		
		// #nodes are same?
		if (expectedList.getLength() != testedList.getLength()) {
			if (print) { printDifference(expectedDOM, testedDOM); }
			return "expected " + expectedList.getLength() + " element nodes, " +
					"but found " + testedList.getLength() + " nodes.";
		}

		for (int i = 0; i < expectedList.getLength(); ++i) {
			Element expected = (Element)expectedList.item(i);
			Element tested = (Element)testedList.item(i);

			// Is the name correct?
			if (!expected.getLocalName().equals(tested.getLocalName())) {
				if (print) { printDifference(expectedDOM, testedDOM); }
				return "expected <" + showNodeStack(expected) +
						">, but found <" + tested.getLocalName() + ">.";
			}
			if (expected.getNamespaceURI() != null &&
					!expected.getNamespaceURI().equals(
					tested.getNamespaceURI())) {
				if (print) { printDifference(expectedDOM, testedDOM); }
				return "expected <" + showNodeStack(expected) +
						">, but found <" + tested.getLocalName() + ">" +
						" in a wrong name space " + tested.getNamespaceURI() +
						".";
			}

			NamedNodeMap expectedMap = expected.getAttributes();
			NamedNodeMap testedMap   = tested.getAttributes();
			// the returned maps are non-null, because we traverse
			// only element nodes.

			for (int j = 0; j < expectedMap.getLength(); ++j) {
				Node expectedAttr = expectedMap.item(j);

				if (expectedAttr.getNodeName().indexOf("xmlns") != 0) {
					// Non-namespace attribute
					Node testedAttr = testedMap.getNamedItem(
							expectedAttr.getNodeName());
					if (testedAttr == null) {
						if (print) { printDifference(expectedDOM, testedDOM); }
						return "expected attribute @" +
								showNodeStack(expectedAttr) +
								" was not found.";
					}
					if (!expectedAttr.getNodeValue().equals(
							testedAttr.getNodeValue())) {
						if (print) { printDifference(expectedDOM, testedDOM); }
						return "expected '" + expectedAttr.getNodeValue() +
								"' for attribute @" +
								showNodeStack(expected) +
								", but found '" +
								testedAttr.getNodeValue() + "'.";
					}
				}
			}
		}

		// Second, check text nodes
		expectedList = XPathAPI.selectNodeList(expectedDOM, "//text()");
		testedList = XPathAPI.selectNodeList(testedDOM, "//text()");

		// #nodes are same?
		if (expectedList.getLength() != testedList.getLength()) {
			if (print) { printDifference(expectedDOM, testedDOM); }
			return "expected " + expectedList.getLength() + "text nodes, " +
					"but found " + testedList.getLength() + " nodes.";
		}

		for (int i = 0; i < expectedList.getLength(); ++i) {
			Node expectedText = expectedList.item(i);
			Node testedText = testedList.item(i);

			// Does the node exist?
			if (testedText == null) {
				if (print) { printDifference(expectedDOM, testedDOM); }
				return "expected '" + showNodeStack(expectedText) +
					"', but not found.";
			}

			// Is the value same?
			if (!expectedText.getNodeValue().equals(
					testedText.getNodeValue())) {
				if (print) { printDifference(expectedDOM, testedDOM); }
				return "expected '" + showNodeStack(expectedText) +
						"', but found '" + testedText.getNodeValue() + "'.";
			}
		}

		return null;
	}

	private static void printDifference(Node expected, Node tested)
	{
		try {
			System.out.println("==== Expected:");
			System.out.println(domToString(expected));
			System.out.println("==== Tested:");
			System.out.println(domToString(tested));
		} catch (Exception ignored) {}
	}

	private static void deleteEmptyHeader(Node node)
			throws TransformerException
	{
		Node header = XPathAPI.selectSingleNode(
				node, "//*[local-name()='Envelope']/*[local-name()='Header']");

		if (header != null && !header.hasChildNodes()) {
			header.getParentNode().removeChild(header);
		}
	}

	private static void normalizeTextNodes(Node node)
			throws TransformerException
	{
		node.normalize();

		// repeat 'remove white space text node' cycles
		// until it doesn't find any more, up to 3 times
		int loop = 0;
		boolean deleted = false;
		do {
			NodeList l = XPathAPI.selectNodeList(node, "//text()");

			for (int i = 0; i < l.getLength(); ++i) {
				Node n = l.item(i);

				// trim out white spaces
				n.setNodeValue(n.getNodeValue().trim());

				// remove if empty
				if (n.getNodeValue().length() == 0) {
					n.getParentNode().removeChild(n);
					deleted = true;
				}
			}
			++loop;
			if (loop > 3) { break; }
		} while (deleted);
	}

	private static String showNodeStack(Node node)
	{
		String names;

		switch (node.getNodeType()) {
			case Node.TEXT_NODE:
				names = "|" + node.getNodeValue() + "|";
				break;
			case Node.ATTRIBUTE_NODE:
				names = node.getNodeValue();
				node = ((Attr)node).getOwnerElement();
				names += "<" + node.getNodeName();
				break;
			default:
				names = node.getNodeName();
				break;
		}

		Node parent;
		while ((parent = node.getParentNode()) != null) {
			names += "<" + parent.getNodeName();
			node = parent;
		}
		
		return names;
	}

	static Element fileToDOM(String filename)
	{
		Document d = null;

		try {
			DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
			f.setNamespaceAware(true);
			DocumentBuilder b = f.newDocumentBuilder();
			d = b.parse(new File(filename));
		} catch (Exception e) {
			e.printStackTrace();
		}
		d.normalize();

		return d.getDocumentElement();
	}

	static String domToString(Node element) throws TransformerException
	{
		StringWriter result = new StringWriter();

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		t.transform(new DOMSource(element), new StreamResult(result));

		return result.toString();
	}

	static Node stringToDOM(String str) throws TransformerException
	{
		DOMResult result = new DOMResult();

		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer t = tf.newTransformer();
		t.transform(new StreamSource(new StringReader(str)), result);

		Document document = (Document)result.getNode();
		document.normalize();

		return document;
	}

	static void maskUUID(Node document) throws TransformerException
	{
		NodeList list = XPathAPI.selectNodeList(
				document, "//*[local-name()='uuid']");

		if (list.getLength() != 1) {
			System.out.println("==== uuid? " + list.getLength());
			return;
		}

		Element element = (Element)list.item(0);
		element.setTextContent("uuid");
	}
}

// End of file
// vim: set ts=4 sw=4:
