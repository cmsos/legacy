
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_RdvConnectionListener_h
#define _jxta_RdvConnectionListener_h

namespace jxta {

//! This class represents a Rendezvous connection listener.
class RdvConnectionListener 
{
	public:
	//! Callback for a rdv connection event.
	virtual void rdvConnectionEvent() = 0;
};


}

#endif

