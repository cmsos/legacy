package xdaq.tools.daqlet;

import java.awt.event.ActionEvent;

public class CallbackReplyEvent extends ActionEvent
{
	protected String destination_;

	public CallbackReplyEvent (Object source, int id, String command, String destination)
	{
		super (source, id, command);
		destination_ = destination;
	}
	
	public String getDestination()
	{
		return destination_;
	}
};
