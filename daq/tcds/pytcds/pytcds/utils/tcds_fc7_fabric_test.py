#!/usr/bin/env python

###############################################################################
## Runs the FC7 FPGA fabric tester.
###############################################################################

import os
import sys
import time

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_hw_connect import get_carrier_hw
from pytcds.utils.tcds_utils_misc import uint32_to_string

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

REG_NAME_CONTROL = "user.pv_control"
REG_NAME_STATUS = "user.pv_status"

SLEEP_TIME = .1

###############################################################################

class TestFabric(CmdLineBase):

    def main(self):

        # The test-mode. Read from the firmware. Should be either top
        # or bottom.
        mode = None

        # The test values to use.
        test_values = [0xaaaaaaaa, 0x55555555]

        # The number of iterations of the test to perform.
        num_iter = 3

        #----------

        carrier = get_carrier_hw(self.ip_address, verbose=self.verbose)
        if not carrier:
            self.error("Could not connect to {0:s}.".format(self.ip_address))

        base_address_control = carrier.get_node(REG_NAME_CONTROL).getAddress()
        base_address_status = carrier.get_node(REG_NAME_STATUS).getAddress()

        tmp = carrier.read(base_address_status)
        user_id = uint32_to_string(tmp)
        if (user_id != "scan"):
            print >> sys.stderr, \
                "ERROR This does not look like a fabric tester firmware image"
            sys.exit(1)
        tmp = carrier.read(base_address_status + 1)
        mode = uint32_to_string(tmp).strip()

        #----------

        print self.sep_line
        print "Test mode: {0:s}".format(mode)
        print "Test values: {0:s}".format(", ".join([hex(i) for i in test_values]))
        print "Number of iterations: {0:d}".format(num_iter)
        print self.sep_line

        #----------

        regions = []
        x_min = None
        x_max = None
        y_min = None
        y_max = None
        if mode == "sop":
            x_min = 0
            x_max = 1
            y_min = 4
            y_max = 7
        elif mode == "bot":
            x_min = 0
            x_max = 1
            y_min = 0
            y_max = 3

        for x in xrange(x_min, x_max + 1):
            for y in xrange(y_min, y_max + 1):
                regions.append((x, y))

        for region in regions:
            print "Testing region {0:s}: ".format(region),
            (x, y) = region
            offset = 16 + 8 * x + y

            for i in xrange(num_iter):
                for test_value in test_values:
                    carrier.write(base_address_control + offset, test_value)
                    time.sleep(SLEEP_TIME)
                    res = carrier.read(base_address_status + offset)
                    if res == test_value:
                        sys.stdout.write("+")
                    else:
                        sys.stdout.write("-")
                    print "  wrote {0:08x}, read {1:08x}".format(test_value, res)
            print
        print self.sep_line

        # End of main().

    # End of class TestFabric.

###############################################################################

if __name__ == "__main__":

    desc_str = "Helper script to run the FC7 FPGA fabric tester firmeware."
    usage_str = "usage: %prog device"
    epilog_str_tmp = "Example: '{0:s} 192.168.0.170"
    epilog_str = epilog_str_tmp.format(os.path.basename(sys.argv[0]))

    res = TestFabric(desc_str, usage_str, epilog_str).run()

    sys.exit(res)

###############################################################################
