// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _nodejs_ApplicationDescriptor_h_
#define _nodejs_ApplicationDescriptor_h_

#include <node.h>
#include <node_object_wrap.h>

namespace nodejs
{

class ApplicationDescriptor: public node::ObjectWrap {

	public:

	  static void Init(v8::Local<v8::Object> exports);

	 private:

	  explicit ApplicationDescriptor();
	  ~ApplicationDescriptor();

	  static void New(const v8::FunctionCallbackInfo<v8::Value>& args);
	  static v8::Persistent<v8::Function> constructor;

};

}

#endif
