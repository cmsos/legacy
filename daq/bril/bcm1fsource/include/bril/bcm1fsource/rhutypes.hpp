/***************************************************************************** 
 * File  rhutypes.h
 * created on 04.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/

#ifndef RHUTYPES_HPP_
#define RHUTYPES_HPP_

//#include "tools/types.h"

#include "types.h"

#ifdef __MAKECINT__

#else
#ifdef __CINT__
#else
#include <boost/date_time/posix_time/posix_time_types.hpp>
//#include "tools/bits.hpp"
#include "bits.hpp"
#define USE_BOOST_TIMETYPE

#endif
#endif

namespace rhu {

#ifdef USE_BOOST_TIMETYPE

  // Data Container for a Timestamp
  typedef boost::posix_time::ptime TimeStamp;
#else
  // Data Container for a Timestamp
  // This will contain a boost::posix_time::ptime timestamp
  // but it is not explicitly declared here to
  // reduce boost dependency
  typedef struct {
    char data[8];
  } TimeStamp;

#endif

  typedef enum {
    RHU_MODE_CONFIGURATION = 0,
    RHU_MODE_ACQUISITION = 1,
    RHU_MODE_BEAMABORT = 2
  } DeviceMode;

  static const int HIST_FAST_BIN_COUNT = 14256;
  static const int HIST_SLOW_BIN_COUNT = 3564;
  /* static const int HIST_FAST_CHANNEL_COUNT = 9; */
  /* static const int HIST_SLOW_CHANNEL_COUNT = 1; */
  static const int HIST_FAST_CHANNEL_COUNT = 8;
  static const int HIST_SLOW_CHANNEL_COUNT = 0;
  static const int POSTMORTEM_MAX_ORBIT_COUNT = 50;
  static const int POSTMORTEM_CHANNEL = 8;

  // Structure that contains all Histogram Data of a single fast channel
struct HistogramFastChannelData
{
  // >0 if data is valid
  char valid;
  // Channel index
  char channel;
  // Orbit Counter at readout
  u64_t orbitCounter;
  // Number of Orbits covered by this histogram
  u16_t orbits;
  // BINs filled with data
  u16_t bins[HIST_FAST_BIN_COUNT];

  // returns if data is valid
  bool isValid() const
  {
    return valid>0;
  }
};


// Structure that contains all Histogram Data of a single slow channel
struct HistogramSlowChannelData
{
  // >0 if data is valid
  char valid;
  // Channel index
  char channel;
  // Orbit Counter at readout
  u64_t orbitCounter;
  // Number of Orbits covered by this histogram
  u16_t orbits;
  // BINs filled with data
  u16_t bins[HIST_SLOW_BIN_COUNT];

  // returns if data is valid
  bool isValid() const
  {
    return valid>0;
  }
};


// Contains all Histogram Data of all Channels
struct HistogramData
{
  // Contains the timestamp of the data
  TimeStamp timeStamp;

  HistogramFastChannelData fastChannel[HIST_FAST_CHANNEL_COUNT];
  HistogramSlowChannelData slowChannel[HIST_SLOW_CHANNEL_COUNT];
};

// *** Postmortem Data

// Structure Contains the Postmortem Data of one channel and a single orbit
struct PostmortemOrbit
{
  // >0 if data is valid
  char valid;
  // Orbit Counter at readout
  u64_t orbitCounter;
  // recorded data itself, each bit corresponts with 6.25ns timeslot
  // thus 891 Data Words a 16Bit or 1702 bytes
  u8_t data[HIST_FAST_BIN_COUNT/8];

  // Helper inline functions

  // returns if data is valid
  bool isValid()
  {
    return valid>0;
  }

  // returns associated orbit counter
  u64_t getOrbitCounter()
  {
    return orbitCounter;
  }

  // returns content of a single bin
  bool getBinData(u16_t bin)
  {
    if (bin>=HIST_FAST_BIN_COUNT) return false; // error, out of bounds
    // extract bit value from storage
    return (data[bin/8] & (1<<(bin & 0x7)))?true:false;
  }
};

// Structure Contains all postmortem information about a single channel
struct PostmortemChannelData
{
  // >0 if data is valid
  char valid;
  // Channel index
  char channel;

  // Stores the last two Histograms
  HistogramFastChannelData hist0; // Histogram at BeamAbort (not completely filled)
  HistogramFastChannelData hist1; // Histogram before BeamABort (completely filled)

  // Stores the Postmortem Data
  PostmortemOrbit orbits[POSTMORTEM_MAX_ORBIT_COUNT];

  // returns if data is valid
  bool isValid()
  {
    return valid>0;
  }
};

// Structure that contains all postmortem data of all fast channesl
struct PostmortemData
{
  TimeStamp timeStamp;
  PostmortemChannelData fastChannel[POSTMORTEM_CHANNEL];
};


struct DeviceConfig
{
  enum {
    FEATURE_SAMPLING_MODE_EDGE = 0,
    FEATURE_SIMULATOR_ENABLE = 1,
    FEATURE_POSTMORTEM_RESTART = 2,
  };

  bool isSamplingModeEdge() const
  {
    return tools::getBit(featureRegister, FEATURE_SAMPLING_MODE_EDGE);
  }
  void setSamplingModeEdge(bool value)
  {
    featureRegister=tools::setBit(featureRegister, FEATURE_SAMPLING_MODE_EDGE);
  }
  bool isSimulatorEnabled() const
  {
    return tools::getBit(featureRegister, FEATURE_SIMULATOR_ENABLE);
  }
  void setSimulatorEnabled(bool value)
  {
    featureRegister=tools::setBit(featureRegister, FEATURE_SIMULATOR_ENABLE);
  }
  bool isPostmortemRestart() const
  {
    return tools::getBit(featureRegister, FEATURE_POSTMORTEM_RESTART);
  }
  void setPostmortemRestart(bool value)
  {
    featureRegister=tools::setBit(featureRegister, FEATURE_POSTMORTEM_RESTART);
  }

  // Parameter controlled by server and client
  rhu::DeviceMode mode;
  u16_t channelEnableMask;
  unsigned short featureRegister;
  unsigned short orbitTriggerDelay;
  unsigned short beamAbortOrbits;
  unsigned short beamAbortTimeout;
  unsigned short histogramOrbitsThreshold;

  void setChannelEnable(int index, bool enable)
  {
    if (index<0) throw std::range_error("channel index < 0");
    if (index>9) throw std::range_error("channel index > 9");
    channelEnableMask=tools::setBit(channelEnableMask, index, enable);
  }
  // returns true, if given channel is enabled
  // Index must be >=0 <10 or an exception is thrown
  // virtual
  bool isChannelEnabled(int index) const
  {
    if (index<0) throw std::range_error("channel index < 0");
    if (index>9) throw std::range_error("channel index > 9");
    return tools::getBit(channelEnableMask, index);
  }

};

struct DeviceStatus
{
  enum {
    STATUS_BUNCH_CLK_LOCK = 0,
    STATUS_ORBIT_TRIG_OK = 1,
  };

  bool isBunchClockLocked() const
  {
    return tools::getBit(statusRegister, STATUS_BUNCH_CLK_LOCK);
  }
  bool isOrbitTriggerOk() const
  {
    return tools::setBit(statusRegister, STATUS_ORBIT_TRIG_OK);
  }

  // Status send by server
  int histogramOverflowCount;// number of histogram where an overflow has been detected
  int irqCount;// number of total irq

  // *** Status
  unsigned short firmwareRevision;
  unsigned short statusRegister;
  unsigned char  temperature;

  u64_t actualOrbitCounter;

  bool isOnline;
};

} // namespace rhu

#endif /* RHUTYPES_H_ */
