var relayTop = '<xr:relay SOAP-ENV:actor="http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10" xmlns:xr="http://xdaq.web.cern.ch/xdaq/xsd/2004/XRelay-10">';
	
var relayBottom =  '</xr:relay>';

function xdaqCCGetXRELAYTo(url, urn)
{
	return '		<xr:to url="' + url + '" urn="' + urn + '"/>';
}

//function xdaqWindowPostLoad() 
//{
$(document).on("xdaq-post-load", function(){
/*
	$(document).on('change', '#xdaq-cc-cmdSelector', function(e) {
		console.log("xdaq-cc-cmdSelector value changed");
	});
*/
	$(document).on('click', '#xdaq-cc-submit', function(e) 
	{
		console.log("xdaq-cc-submit clicked");
				
		var content = $("#xdaq-cc-soapmsg").val();
		var soapAction = "urn:xdaq-application:service=xrelay";
		var url = $("#xdaq-cc-table").attr("data-url") + "/" + soapAction;
		
		var options = {
			url: url,
			data: content,
			headers: {
				"SOAPAction": soapAction
			},
			error: function (xhr, textStatus, errorThrown) {
				console.log("message failed");
				$("#xdaq-cc-soapreply").append("Failed : " + xhr.responseText + "\n\n");
			}
		};

		$('#xdaq-cc-submit').removeClass("xdaq-submit-button xdaq-green");
		$('#xdaq-cc-submit').addClass("xdaq-red");
		
		xdaqAJAX(options, function (data, textStatus, xhr) {
			$('#xdaq-cc-submit').removeClass("xdaq-red");
			$('#xdaq-cc-submit').addClass("xdaq-green");
		});
		
		return false;
	});
	
	$(document).on('change', '#xdaq-cc-regex-selector, #xdaq-cc-col-selector, #xdaq-cc-regex-modifier-selector', function(e) {
		//console.log("xdaq-cc-selector changed");
		xdaqCCSelectorChanged();
	});
	
	$(document).on("keyup", '#xdaq-cc-regex-selector', function(e) {
		//console.log("xdaq-cc-selector changed");
		xdaqCCSelectorChanged();
	});
	
	$(document).on('click', '#xdaq-cc-clear-reply', function(e) {
		//console.log("xdaq-cc-selector changed");
		$("#xdaq-cc-soapreply").val("");
	});
	
	$(document).on('change', '#xdaq-cc-table input[type=checkbox]', function(e) {
		xdaqCCSelectionChanged();
	});
});

function xdaqCCSelectionChanged()
{
		//console.log("xdaq-cc-selection changed");

		var toPrepend = $("#xdaq-cc-soapmsg").val().substring(0, $("#xdaq-cc-soapmsg").val().indexOf(relayTop) + relayTop.length) + "\n";
		//console.log(toPrepend);
		
		var toInsert = "";
		
		$("#xdaq-cc-table input[type=checkbox]").each(function() {
			if ($(this).prop("checked") == true)
			{
				toInsert = toInsert + xdaqCCGetXRELAYTo($(this).attr("data-cc-url"), $(this).attr("data-cc-urn")) + '\n';
			}
		});
		//console.log(toInsert);
		
		var toAppend = $("#xdaq-cc-soapmsg").val().substring($("#xdaq-cc-soapmsg").val().indexOf(relayBottom));
		//console.log(toAppend);
		
		$("#xdaq-cc-soapmsg").val(toPrepend + toInsert + "\t" + toAppend);
}

function xdaqCCSelectorChanged()
{
	console.log("xdaq cluster control selector changed");
	
	colSel = $("#xdaq-cc-col-selector").val();
	regSel = $("#xdaq-cc-regex-modifier-selector").val();
	regex = $("#xdaq-cc-regex-selector").val();
	
	console.log(colSel + " " + regSel + " " + regex);
	
	rows = $("#xdaq-cc-table").find("tbody tr");
	console.log("found " + rows.length + " applications");
	rows.each(function () {
		
		if (regex == "")
		{
			$(this).find("input[type=checkbox]").prop("checked", false);	
		}
		else
		{
			col = $(this).children("[data-cc-col=\""+colSel+"\"]");
			if (col.length != 1)
			{
				console.log("Error: found " + col.length + " cells with the data-cc-col value " + colSel);
			}
			else
			{
				cellValue = col.first().attr("data-cc-val");
				
				console.log("Checking : " + cellValue);
				
				if (regSel == "starts")
				{
					//console.log("Looking for startwith");
	
					//if ($(this).children("[data-cc-val^=\""+regex+"\"]").length == 1)
					if (cellValue.indexOf(regex) == 0)
					{
						//console.log("Match : " + cellValue);
						$(this).find("input[type=checkbox]").prop("checked", true);
					}
					else
					{
						$(this).find("input[type=checkbox]").prop("checked", false);	
					}
				}
				else if (regSel == "contains")
				{
					//console.log("Looking for contains");
					
					//if ($(this).children("[data-cc-val*=\""+regex+"\"]").length == 1)
					if (cellValue.indexOf(regex) >= 0)
					{
						//console.log("Match : " + cellValue);
						$(this).find("input[type=checkbox]").prop("checked", true);
					}
					else
					{
						$(this).find("input[type=checkbox]").prop("checked", false);	
					}
				}
				else if (regSel == "ends")
				{
					//console.log("Looking for endswith");
					
					//if ($(this).children("[data-cc-val$=\""+regex+"\"]").length == 1)
					if (cellValue.lastIndexOf(regex) == cellValue.length - regex.length)
					{
						//console.log("Match : " + cellValue);
						$(this).find("input[type=checkbox]").prop("checked", true);
					}
					else
					{
						$(this).find("input[type=checkbox]").prop("checked", false);	
					}
				}
				else
				{
					console.log("Error: unknown regex selector : " + regSel);
				}
			}
		}
	});
	xdaqCCSelectionChanged();
}