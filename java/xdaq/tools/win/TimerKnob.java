package xdaq.tools.win;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

import xdaq.tools.win.DKnob;

class TimerKnob extends DKnob implements ChangeListener
{
	JLabel valueLabel;
	int value;
	Timer timer;
	
	public TimerKnob(ActionListener l)
	{
		timer = new Timer(0, l);
		timer.stop();
		valueLabel = new JLabel ("Seconds: 0");
		value = 0;
		this.addChangeListener ( this );
	}
	
	public int getIntValue ()
	{
		return value;
	}
	
	public JLabel getLabel()
	{
		return valueLabel;
	}

	public void stateChanged(ChangeEvent e) 
	{
		timer.stop();
		
		DKnob t = (DKnob) e.getSource();
		value = (int) (t.getValue() * 100.0);
		valueLabel.setText( "Seconds: " + value );
		
		if (value > 0)
		{
			// Set 5 seconds initial delay
			timer.setInitialDelay ( 5000 );
			
			timer.setDelay  ( value * 1000 );
			timer.setRepeats ( true );
			timer.start();
		}
	}
}
