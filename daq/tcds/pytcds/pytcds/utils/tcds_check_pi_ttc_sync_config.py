#!/usr/bin/env python

###############################################################################
## A helper script to go through all partitions and check if the PIs
## are correctly configured to synchronize to their TTC streams.
###############################################################################

import sys
import time

from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.pi.tcds_pi import get_pi_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_setup_description import sorter_helper

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

NUM_B_CHANNELS = 16
B_CHANNEL_NUMBER_LUMINIBBLE = 0
B_CHANNEL_NUMBER_BC0 = 1
B_CHANNEL_NUMBER_TESTENABLE = 2
B_CHANNEL_NUMBER_PRIVATEGAP = 3
B_CHANNEL_NUMBER_PRIVATEORBIT = 4
B_CHANNEL_NUMBER_RESYNC = 5
B_CHANNEL_NUMBER_HARDRESET = 6
B_CHANNEL_NUMBER_EC0 = 7
B_CHANNEL_NUMBER_OC0 = 8
B_CHANNEL_NUMBER_START = 9
B_CHANNEL_NUMBER_STOP = 10
B_CHANNEL_NUMBER_STARTOFGAP = 11
B_CHANNEL_NUMBER_BGO12 = 12
B_CHANNEL_NUMBER_WTE = 13
B_CHANNEL_NUMBER_BGO14 = 14
B_CHANNEL_NUMBER_BGO15 = 15

CPM_EMISSION_BX = {}

CPM_EMISSION_BX[B_CHANNEL_NUMBER_LUMINIBBLE] = 2256
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BC0] = 3540
CPM_EMISSION_BX[B_CHANNEL_NUMBER_TESTENABLE] = 3283
CPM_EMISSION_BX[B_CHANNEL_NUMBER_PRIVATEGAP] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_PRIVATEORBIT] = 0
CPM_EMISSION_BX[B_CHANNEL_NUMBER_RESYNC] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_HARDRESET] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_EC0] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_OC0] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_START] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_STOP] = 2000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_STARTOFGAP] = 3440
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BGO12] = 3000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_WTE] = 2886
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BGO14] = 3000
CPM_EMISSION_BX[B_CHANNEL_NUMBER_BGO15] = 2000

GOOD = 0
BAD = 1
IGNORED = 2

###############################################################################

class BChannelInfo(object):
    def __init__(self,
                 b_channel_number,
                 is_single,
                 is_double,
                 is_block,
                 b_command,
                 emission_bx,
                 b_channel_ram):

        self.b_channel_number = b_channel_number
        self.is_single = is_single
        self.is_double = is_double
        self.is_block = is_block
        self.b_command = b_command
        # NOTE: Some subsystems delay their BC0 to the next orbit...
        self.raw_emission_bx = emission_bx
        self.emission_bx = self.raw_emission_bx
        while (self.emission_bx > 3564):
            self.emission_bx -= 3564
        self.b_channel_ram = b_channel_ram
        # End of __init__().

    def is_off(self):
        res = (not self.is_single) \
              and (not self.is_double) \
              and (not self.is_block)
        # End of is_off().
        return res

    def b_channel_mode_str(self):
        res = "unknown"
        is_single = self.is_single
        is_double = self.is_double
        is_block = self.is_block
        if (is_single + is_double + is_block) == 0:
            res = "off"
        elif (is_single + is_double + is_block) > 1:
            res = "misconfigured (i.e., multiple emission modes enabled)"
        else:
            if is_single:
                res = "single"
            elif is_double:
                res = "double"
            elif is_block:
                res = "block"
        # End of b_channel_mode_str().
        return res

    # End of class BChannelInfo.

###############################################################################

def get_b_channel_info(ici_info, ici, b_channel_number):
    reg_name_base = "ici{0:d}.bchannels.bchannel{1:d}".format(ici_info.ici_number, b_channel_number)

    tmp_is_single = ici.read("{0:s}.configuration.single".format(reg_name_base))
    tmp_is_double = ici.read("{0:s}.configuration.double".format(reg_name_base))
    tmp_is_block = ici.read("{0:s}.configuration.block".format(reg_name_base))
    is_single = (tmp_is_single == 0x1)
    is_double = (tmp_is_double == 0x1)
    is_block = (tmp_is_block == 0x1)

    tmp_is_bx_sync = ici.read("{0:s}.configuration.bx_sync".format(reg_name_base))
    is_bx_sync = (tmp_is_bx_sync == 0x1)
    bx_or_delay = ici.read("{0:s}.configuration.bx_or_delay".format(reg_name_base))
    emission_bx = bx_or_delay
    if not is_bx_sync:
        emission_bx += CPM_EMISSION_BX[b_channel_number]

    tmp_ram = ici.read_block("{0:s}.ram".format(reg_name_base), 4)
    b_command = tmp_ram[0]
    b_channel_info = BChannelInfo(b_channel_number,
                                  is_single,
                                  is_double,
                                  is_block,
                                  b_command,
                                  emission_bx,
                                  tmp_ram)
    # End of get_b_channel_info().
    return b_channel_info

###############################################################################

class TCDSChecker(CmdLineBase):

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def main(self):

        # Find all ICIs, in order to determine a list of partitions.
        hardware = [j for (i, j) in self.setup.hw_targets.iteritems()]
        all_icis = [i for i in hardware if i.type == "ici"]
        all_pis = [i for i in hardware if i.type == "pi"]

        # BUG BUG BUG
        # For the moment, let's skip all the secondary ICIs.
        target_icis = [i for i in all_icis \
                       if (i.identifier.find("-sec") < 0)]
        # BUG BUG BUG end

        # There are certain partitions that we don't care about,
        # TTS-history-wise. For example: all the BRIL stuff.
        partitions_to_ignore = [
            "milliqan",
            "plt",
            "bcmutca",
            "bcmvme",
            "bhm",
            ]

        target_icis.sort(key=sorter_helper)

        results = {}
        for target_ici in target_icis:
            print target_ici.identifier
            partition_name = target_ici.identifier.split("-")[1]

            # if partition_name != "totdet":
            #     continue

            results[partition_name] = []

            if partition_name in partitions_to_ignore:
                msg = "ignored"
                results[partition_name].append((IGNORED, target_ici.identifier, msg))
                continue

            try:

                board_info_ici = self.setup.hw_targets[target_ici.identifier]
                network_address_ici = board_info_ici.dns_alias
                controlhub_address_ici = board_info_ici.controlhub

                ici = get_lpm_hw(network_address_ici,
                                 controlhub_address_ici,
                                 verbose=self.verbose)
                if not ici:
                    raise RuntimeError("Could not connect to {0:s}.".format(network_address_ici))

                #----------

                # Get the configuration of all B-channels on this ICI.
                b_channel_infos = {}
                for b_channel_number in xrange(NUM_B_CHANNELS):
                    b_channel_infos[b_channel_number] = get_b_channel_info(target_ici, ici, b_channel_number)

                #----------

                # Find out the OC0 B-channel configuration.
                oc0_b_channel_info = b_channel_infos[B_CHANNEL_NUMBER_OC0]
                ici_oc0_command = oc0_b_channel_info.b_command

                # Find out the BC0 B-channel configuration.
                bc0_b_channel_info = b_channel_infos[B_CHANNEL_NUMBER_BC0]
                ici_bc0_command = bc0_b_channel_info.b_command
                ici_bc0_bx = bc0_b_channel_info.emission_bx

                #----------

                # Start with some ICI-only cross-checks.
                if not bc0_b_channel_info.is_single:
                    msg_base = "BC0 B-channel" \
                               " is not configured in single emission mode" \
                               " (but in '{0:s}' mode)."
                    msg = msg_base.format(bc0_b_channel_info.b_channel_mode_str())
                    results[partition_name].append((BAD, target_ici.identifier, msg))
                else:
                    msg = "BC0 B-channel configured in single emission mode."
                    results[partition_name].append((GOOD, target_ici.identifier, msg))

                if bc0_b_channel_info.is_double or\
                   bc0_b_channel_info.is_block:
                    msg = "BC0 B-channel" \
                          " is misconfigured: both single-" \
                          " and double-/block- emission mode are enabled."
                    results[partition_name].append((BAD, target_ici.identifier, msg))
                else:
                    msg = "BC0 B-channel correctly configured."
                    results[partition_name].append((GOOD, target_ici.identifier, msg))

                bc0_ram = bc0_b_channel_info.b_channel_ram
                if bc0_ram[2:] != [0, 6]:
                    msg_base = "Found unexpected BC0 B-channel RAM contents: {0:s}."
                    msg = msg_base.format(str(bc0_ram))
                    results[partition_name].append((BAD, target_ici.identifier, msg))
                else:
                    msg = "BC0 B-channel RAM correctly configured."
                    results[partition_name].append((GOOD, target_ici.identifier, msg))

                if not oc0_b_channel_info.is_single:
                    msg_base = "OC0 B-channel" \
                               " is not configured in single emission mode" \
                               " (but in '{0:s}' mode)."
                    msg = msg_base.format(oc0_b_channel_info.b_channel_mode_str())
                    results[partition_name].append((BAD, target_ici.identifier, msg))
                else:
                    msg = "OC0 B-channel configured in single emission mode."
                    results[partition_name].append((GOOD, target_ici.identifier, msg))

                if oc0_b_channel_info.is_double or\
                   oc0_b_channel_info.is_block:
                    msg = "OC0 B-channel" \
                          " is misconfigured: both single-" \
                          " and double-/block- emission mode are enabled."
                    results[partition_name].append((BAD, target_ici.identifier, msg))
                else:
                    msg = "OC0 B-channel correctly configured."
                    results[partition_name].append((GOOD, target_ici.identifier, msg))

                oc0_ram = oc0_b_channel_info.b_channel_ram
                if oc0_ram[2:] != [0, 6]:
                    msg_base = "Found unexpected OC0 B-channel RAM contents: {0:s}."
                    msg = msg_base.format(str(oc0_ram))
                    results[partition_name].append((BAD, target_ici.identifier, msg))
                else:
                    msg = "OC0 B-channel RAM correctly configured."
                    results[partition_name].append((GOOD, target_ici.identifier, msg))

                #----------

                # Find the PI(s) corresponding to this ICI.
                # NOTE: Some partitions contain more than one PI.
                target_pis = [i for i in all_pis \
                              if (i.ici_pri == target_ici)]
                if not len(target_pis):
                    raise RuntimeError("Failed to find PI '{0:s}'.".format(target_pi_id))

                pi_configs = []
                for target_pi in sorted(target_pis, key=sorter_helper):
                    board_info_pi = self.setup.hw_targets[target_pi.identifier]
                    network_address_pi = board_info_pi.dns_alias
                    controlhub_address_pi = board_info_pi.controlhub

                    pi = get_pi_hw(network_address_pi,
                                   controlhub_address_pi,
                                   verbose=self.verbose)
                    if not pi:
                        raise RuntimeError("Could not connect to {0:s}.".format(network_address_pi))

                    #----------

                    # Get the PI synchronisation configuration.
                    reg_name_base = "pi.sync"
                    pi_oc0_command = pi.read("{0:s}.oc0_bcommand".format(reg_name_base))
                    pi_bc0_command = pi.read("{0:s}.bc0_bcommand".format(reg_name_base))
                    pi_bc0_reset_val = pi.read("{0:s}.bc0_reset_val".format(reg_name_base))

                    pi_configs.append((pi_oc0_command, pi_bc0_command, pi_bc0_reset_val))

                    #----------

                    # Now do the comparison.
                    if pi_oc0_command != ici_oc0_command:
                        msg_base = "ICI '{0:s}' and PI '{1:s}'" \
                                   " differ in OC0 command configuration." \
                                   " ICI: 0x{2:08x}, PI: 0x{3:08x}."
                        msg = msg_base.format(target_ici.identifier,
                                              target_pi.identifier,
                                              ici_oc0_command,
                                              pi_oc0_command)
                        results[partition_name].append((BAD, target_pi.identifier, msg))
                    else:
                        msg = "OC0 command configuration matches."
                        results[partition_name].append((GOOD, target_pi.identifier, msg))

                    if pi_bc0_command != ici_bc0_command:
                        msg_base = "ICI '{0:s}' and PI '{1:s}'" \
                                   " differ in BC0 command configuration." \
                                   " ICI: 0x{2:08x}, PI: 0x{3:08x}."
                        msg = msg_base.format(target_ici.identifier,
                                              target_pi.identifier,
                                              ici_bc0_command,
                                              pi_bc0_command)
                        results[partition_name].append((BAD, target_pi.identifier, msg))
                    else:
                        msg = "BC0 command configuration matches."
                        results[partition_name].append((GOOD, target_pi.identifier, msg))

                    if pi_bc0_reset_val != ici_bc0_bx:
                        msg_base = "ICI '{0:s}' and PI '{1:s}'" \
                                   " differ in BC0 BX configuration." \
                                   " ICI: {2:d}, PI: {3:d}."
                        msg = msg_base.format(target_ici.identifier,
                                              target_pi.identifier,
                                              ici_bc0_bx,
                                              pi_bc0_reset_val)
                        results[partition_name].append((BAD, target_pi.identifier, msg))
                    else:
                        msg = "BC0 BX configuration matches."
                        results[partition_name].append((GOOD, target_pi.identifier, msg))

                    problem_found = False
                    for b_channel_number in xrange(NUM_B_CHANNELS):
                        if not b_channel_number in [B_CHANNEL_NUMBER_BC0,
                                                    B_CHANNEL_NUMBER_OC0]:
                            tmp_b_command = b_channel_infos[b_channel_number].b_command
                            if (tmp_b_command == pi_bc0_command) \
                               and not b_channel_infos[b_channel_number].is_off():
                                msg_base = "B-channel {0:d} will reset PI '{1:s}' BX counter."
                                msg = msg_base.format(b_channel_number, target_pi.identifier)
                                results[partition_name].append((BAD, target_ici.identifier, msg))
                                problem_found = True
                            if (tmp_b_command == pi_oc0_command) \
                               and not b_channel_infos[b_channel_number].is_off():
                                msg_base = "B-channel {0:d} will reset PI '{1:s}' orbit counter."
                                msg = msg_base.format(b_channel_number, target_pi.identifier)
                                results[partition_name].append((BAD, target_ici.identifier, msg))
                                problem_found = True
                    if not problem_found:
                        msg_base = "No B-channel cross-configurations found for PI '{0:s}'."
                        msg = msg_base.format(target_pi.identifier)
                        results[partition_name].append((GOOD, target_ici.identifier, msg))

                if len(target_pis) > 1:
                    if len(set(pi_configs)) != 1:
                        msg = "Not all PIs have the same configuration."
                        results[partition_name].append((GOOD, target_ici.identifier, msg))
                    else:
                        msg = "All PIs have the same configuration."
                        results[partition_name].append((GOOD, target_ici.identifier, msg))

            #----------

            except Exception as err:
                results[partition_name] = [(0, "?", "ERROR: " + str(err))]

            #----------

            time.sleep(1)

            #----------

        # Dump results.
        print self.sep_line
        print "Results by partition:"
        print self.sep_line
        max_len_ident = max([len(i) for i in results.keys()])
        for (ident, res) in sorted(results.iteritems()):
            summary = "UNKNOWN"
            tmp = set([i[0] for i in res])
            all_ok = (tmp == set([GOOD]))
            ignored = (tmp == set([IGNORED]))
            if all_ok:
                summary = "OK"
            elif ignored:
                summary = "IGNORED"
            else:
                summary = "BAD"
            print "  {1:{0:d}s}: {2:s}".format(max_len_ident, ident, summary)
            max_len_name = max([len(i[1]) for i in res])
            if not all_ok and not ignored:
                for (flag, name, err_line) in res:
                    flag_str = "UNKNOWN"
                    if flag == GOOD:
                        flag_str = "OK"
                    elif flag == BAD:
                        flag_str = "BAD"
                    tmp = "  {1:{0:d}s}: {2:3s} - {4:{3:d}s} - {5:s}"
                    print tmp.format(max_len_ident, "",
                                     flag_str,
                                     max_len_name, name,
                                     err_line)
        print self.sep_line

        # End of main().

    # End of class TCDSChecker.

###############################################################################

if __name__ == "__main__":

    desc_str = "A helper script" \
               " to go through all partitions" \
               " and check if the PIs are correctly configured" \
               " to synchronize to their TTC streams."
    res = TCDSChecker(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
