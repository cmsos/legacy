#ifndef _tcds_rfrxd_RFRXDController_h_
#define _tcds_rfrxd_RFRXDController_h_

#include <memory>

#include "tcds/rfrxd/VMEDeviceRFRXD.h"
#include "tcds/utils/SOAPCmdBase.h"
#include "tcds/utils/SOAPCmdDumpHardwareState.h"
#include "tcds/utils/XDAQAppWithFSMBasic.h"

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace hwutilsvme {
    class HwIDInfoSpaceHandlerVME;
    class HwIDInfoSpaceUpdaterVME;
  }
}

namespace tcds {
  namespace rfrxd {

    class SignalsInfoSpaceHandler;
    class SignalsInfoSpaceUpdater;

    class RFRXDController : public tcds::utils::XDAQAppWithFSMBasic
    {

    public:
      XDAQ_INSTANTIATOR();

      RFRXDController(xdaq::ApplicationStub* stub);
      virtual ~RFRXDController();

    protected:
      virtual void setupInfoSpaces();

      /**
       * Access the hardware pointer as VMEDeviceRFRXD&.
       */
      virtual VMEDeviceRFRXD& getHw() const;

      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::auto_ptr<tcds::hwutilsvme::HwIDInfoSpaceUpdaterVME> hwIDInfoSpaceUpdaterP_;
      std::auto_ptr<tcds::hwutilsvme::HwIDInfoSpaceHandlerVME> hwIDInfoSpaceP_;
      std::auto_ptr<tcds::rfrxd::SignalsInfoSpaceUpdater> signalsInfoSpaceUpdaterP_;
      std::auto_ptr<tcds::rfrxd::SignalsInfoSpaceHandler> signalsInfoSpaceP_;

      // The SOAP commands.
      template<typename> friend class tcds::utils::SOAPCmdBase;
      template<typename> friend class tcds::utils::SOAPCmdDumpHardwareState;
      tcds::utils::SOAPCmdDumpHardwareState<RFRXDController> soapCmdDumpHardwareState_;

    };

  } // namespace rfrxd
} // namespace tcds

#endif // _tcds_rfrxd_RFRXDController_h_
