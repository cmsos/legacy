// $Id: version.h,v 1.43 2009/05/28 12:46:19 cschwick Exp $
// tracId : 2243
#ifndef _amc13controller_version_h_
#define _amc13controller_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!
#define AMC13CONTROLLER_VERSION_MAJOR 1
#define AMC13CONTROLLER_VERSION_MINOR 0
#define AMC13CONTROLLER_VERSION_PATCH 1
// If any previous versions available E.g. #define AMC13_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef AMC13CONTROLLER_PREVIOUS_VERSIONS

//
// Template macros
//
#define AMC13CONTROLLER_VERSION_CODE PACKAGE_VERSION_CODE(AMC13CONTROLLER_VERSION_MAJOR,AMC13CONTROLLER_VERSION_MINOR,AMC13CONTROLLER_VERSION_PATCH)
#ifndef AMC13CONTROLLER_PREVIOUS_VERSIONS
#define AMC13CONTROLLER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(AMC13CONTROLLER_VERSION_MAJOR,AMC13CONTROLLER_VERSION_MINOR,AMC13CONTROLLER_VERSION_PATCH)
#else 
#define AMC13CONTROLLER_FULL_VERSION_LIST  AMC13CONTROLLER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(AMC13CONTROLLER_VERSION_MAJOR,AMC13CONTROLLER_VERSION_MINOR,AMC13CONTROLLER_VERSION_PATCH)
#endif 

namespace amc13controller
{
	const std::string package  =  "amc13controller";
	const std::string versions =  AMC13CONTROLLER_FULL_VERSION_LIST;
	const std::string description = "Contains the amc13 controller library.";
	const std::string authors = "Christoph Schwick, Jonathan Fulcher";
	const std::string summary = "CMS AMC13 Controller Software.";
	const std::string link = "http://makerpmhappy";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
