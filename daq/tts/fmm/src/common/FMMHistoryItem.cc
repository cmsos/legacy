/**
*      @file FMMHistoryItem.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.10 $
*     $Date: 2007/03/27 07:53:27 $
*
*
**/
#include "tts/fmm/FMMHistoryItem.hh"

#include <iostream>
#include <iomanip>
#include <sstream>


tts::FMMHistoryItem::FMMHistoryItem()
  : _wrapcount(0) {
  for (uint32_t i=0; i<4; i++)
    _histdata[i] = 0;
}

tts::FMMHistoryItem::FMMHistoryItem( uint32_t histdata[4], uint32_t wrapcount ) {

  set(histdata, wrapcount);

};


tts::FMMHistoryItem::FMMHistoryItem( uint64_t extendedTimeStamp, std::vector<tts::TTSState> const& states, bool transitionMissBit ) {

  for (uint32_t i=0; i<4; i++)
    _histdata[i] = 0;

  for (std::vector<tts::TTSState>::size_type i =0; ( i < states.size() ) && (i < 20); ++i) {

    if ( ( (uint32_t) states[i] ) & 0x8 ) _histdata[0] |= (1 << i);
    if ( ( (uint32_t) states[i] ) & 0x4 ) _histdata[1] |= (1 << i);
    if ( ( (uint32_t) states[i] ) & 0x2 ) _histdata[2] |= (1 << i);
    if ( ( (uint32_t) states[i] ) & 0x1 ) _histdata[3] |= (1 << i);

  } 

  for (uint32_t i = 0; i < 4; i++) {

    uint32_t timebits = ( extendedTimeStamp & ( ((uint64_t) 0x3ff) << (10*i) ) ) >> (10*i);
    _histdata[i] |= timebits << 20;
  }

  _wrapcount = ( extendedTimeStamp & ( (uint64_t) (0xffffff) << 40 ) ) >> 40;
  
  if (transitionMissBit) 
    _histdata[3] |= 0x40000000;
}

tts::FMMHistoryItem::FMMHistoryItem( tts::FMMHistoryItem const & item ) {

  for (uint32_t i=0; i<4; i++)
    _histdata[i] = item._histdata[i];

  _wrapcount = item._wrapcount;

};

tts::FMMHistoryItem::~FMMHistoryItem() {};

void tts::FMMHistoryItem::set( uint32_t histdata[4], uint32_t wrapcount ) {

  for (uint32_t i=0; i<4; i++)
    _histdata[i] = histdata[i];

  _wrapcount = wrapcount;
};

std::vector<tts::TTSState> tts::FMMHistoryItem::getInputStates() const {

  std::vector<tts::TTSState> ts;

  // Memory layout of history memory is:
  //
  // address N0,   Bits[29:0]   time_tag[ 9: 0]   Ready    [19:0]
  // address N0+4, Bits[29:0]   time_tag[19:10]   Busy     [19:0]
  // address N0+8, Bits[29:0]   time_tag[29:20]   OutOfSync[19:0]
  // address N0+C, Bits[29:0]   time_tag[39:30]   Warning  [19:0]
  //
  // State = Warning + 2 * OutOfSync + 4 * Busy + 8 * Ready;

  for (uint32_t i=0; i<20; i++) {
    
    uint32_t ready = ( _histdata[0] & (1<<i) ) >> i;
    uint32_t busy  = ( _histdata[1] & (1<<i) ) >> i;
    uint32_t sync  = ( _histdata[2] & (1<<i) ) >> i;
    uint32_t warn  = ( _histdata[3] & (1<<i) ) >> i;

    ts.push_back ( tts::TTSState( warn + (sync << 1) + (busy<<2) + (ready << 3) ) );
  }
  
  return ts;
};

uint64_t tts::FMMHistoryItem::getTimestamp() const {

  uint64_t t = 0;

  for (uint32_t i = 0; i < 4; i++) {
    t |= ((uint64_t)((_histdata[i] & 0x3ff00000) >> 20)) << i*10;
  }

  return t;
};

uint64_t tts::FMMHistoryItem::getExtendedTimestamp() const {

  return getTimestamp() | (uint64_t) _wrapcount << 40;;

};

std::string tts::FMMHistoryItem::getTimestampString() const {

  uint64_t t = getExtendedTimestamp();
  std::stringstream ts;

  uint64_t hr =  t /  (40000000ll*60ll*60ll);
  t -= hr * 40000000ll*60ll*60ll;
  uint64_t min = t / (40000000ll*60ll);
  t -= min * 40000000ll*60ll;
  uint64_t sec = t / 40000000ll;
  t -= sec * 40000000ll;
  uint64_t milsec = t / 40000ll;
  t -= milsec * 40000ll;
  uint64_t musec =  t / 40ll;
  t -= musec * 40ll;
  uint64_t nsec = t*25ll;
  ts << std::setw(2) << std::setfill('0') << std::dec << hr << ":"
     << std::setw(2) << std::setfill('0') << std::dec << min << ":"
     << std::setw(2) << std::setfill('0') << std::dec << sec << "."
     << std::setw(3) << std::setfill('0') << std::dec << milsec << "."
     << std::setw(3) << std::setfill('0') << std::dec << musec << "."
     << std::setw(3) << std::setfill('0') << std::dec << nsec;

  
  return ts.str();
};


bool tts::FMMHistoryItem::getTransitionMissBit() const {
  return (_histdata[3] & 0x40000000) != 0;
};

std::string tts::FMMHistoryItem::toString() const {

  std::ostringstream os;
  
  os << "t=" << getTimestampString() << " s[19..0]";

  std::vector<tts::TTSState> states = getInputStates();

  std::vector<tts::TTSState>::reverse_iterator it1 = states.rbegin();
  for (; it1!=states.rend(); it1++)
    os << " " << (*it1).getShortName();

  return os.str();
}


