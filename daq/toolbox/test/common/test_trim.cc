#include <iostream>
#include <string>
#include "toolbox/string.h"

int main(int argc, char** argv)
{

	try
	{
		std::string myString = " \"urn:urn:xdaq-application:service=hyperdaq\" ";
		std::cout << "original string was [" << myString << "]" << std::endl;
		std::cout << "trimmed string is [" << toolbox::trim(myString," \t\n\r\v\"") << "]" << std::endl;
		myString = "\"\"";
		std::cout << "original string was [" << myString << "]" << std::endl;
                std::cout << "trimmed string is [" << toolbox::trim(myString," \t\n\r\v\"") << "]" << std::endl;

		myString = "    urn:urn:xdaq-application:service=hyperdaq    ";
		std::cout << "original string was [" << myString << "]" << std::endl;
                std::cout << "trimmed string is [" << toolbox::trim(myString," \t\n\r\v\"") << "]" << std::endl;
	} 
	catch (toolbox::exception::Exception& mfu)
	{
		std::cout << "Exception: " << mfu.what() << std::endl;
		return 1;
	}

	return 0;
}
