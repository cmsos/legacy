package xdaq.xdesk.xprop;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.tree.*;
import javax.infobus.*;
import org.apache.log4j.*;
import xdaq.xdesk.tools.*;
import xdaq.xdesk.xtree.*;
//
// XPropViewer is the main application class that allows
// viewing XDAQ properties.

public class XPropViewer extends JPanel implements ItemListener, XPropModelListener,  TreeSelectionListener,
                                                   XPropToolBarListener, InfoBusSubscriberListener
{
	Logger logger = Logger.getLogger (XPropViewer.class);

	JSplitPane splitPane_;
	JPanel partitionNavigator_;
	JComboBox partitionComboBox_;
	JScrollPane currentTreeScrollPane_;
	JScrollPane propertiesScrollPane_;
	JPanel dummyTreePanel_;
	DefaultComboBoxModel comboBoxModel_;
        XPropToolBar toolBar_;
	XPropDefaultModel defaultModel_;
	XPropViewerCache cache_;
	boolean waitingForReply_ = false;
	XProp xprop_;
	
	// Infobus stuff
	InfoBusPublisher commandPublisher;
	InfoBusSubscriber replySubscriber;
	JFrame frame_;
		
	public XPropViewer(JFrame frame, XProp xprop)
	{	
		frame_ = frame;
		xprop_ = xprop;
        
		cache_ = new XPropViewerCache(this);
			
		defaultModel_ = xprop.getDefaultModel();
		
		defaultModel_.addXPropModelListener(this);
		
		//this.setLayout (new BoxLayout(this, BoxLayout.Y_AXIS));	
                this.setLayout (new BorderLayout());	
                
		partitionNavigator_ = new JPanel();
		partitionNavigator_.setLayout(new BoxLayout(partitionNavigator_,BoxLayout.Y_AXIS));	
				
		// Combo Box, left top
		comboBoxModel_ = new DefaultComboBoxModel();
		partitionComboBox_ = new JComboBox(comboBoxModel_);
		partitionComboBox_.addItemListener (this);
		partitionNavigator_.add(partitionComboBox_);
		
		// Tree, left lower - empty now, only put scroll pane
		dummyTreePanel_ = new JPanel();
		currentTreeScrollPane_ = new JScrollPane(dummyTreePanel_);
		partitionNavigator_.add (currentTreeScrollPane_);
		
		// Properties, right
		propertiesScrollPane_ = new JScrollPane();
		
		// Split Pane
		splitPane_ = new JSplitPane (	JSplitPane.HORIZONTAL_SPLIT, 
						partitionNavigator_, propertiesScrollPane_ );
		splitPane_.setOneTouchExpandable(true);
		
		Dimension minSize = new Dimension (200,200);
		partitionNavigator_.setMinimumSize (minSize);
		propertiesScrollPane_.setMinimumSize (minSize);

		splitPane_.setContinuousLayout (true);

                // Create ToolBar
                toolBar_ =  new XPropToolBar();
                toolBar_.setXPropToolBarListener(this);
                
                this.add(toolBar_,BorderLayout.NORTH);	
		this.add (splitPane_, BorderLayout.CENTER);
           
		
		// --------------------------------------------------
		// InfoBus attachment
		
		commandPublisher = new InfoBusPublisher ("xcontrolbus");
		replySubscriber = new InfoBusSubscriber ("xcontrolbus");
		commandPublisher.publishItem ("xcommand");
		replySubscriber.subscribeItem ("xreply", this );
		
		// --------------------------------------------------
	}
	
	XProp getController()
	{
		return xprop_;
	}
	
	JFrame getFrame()
	{
		return frame_;
	}


	// InfoBusAdapter callback function
	//
	public synchronized void  update (Object data, Object source)
	{
		System.out.println ("Received infobus data");
		
		// Check:
		// If somebody has pressed stop, the incoming reply is ignored.
		// For that purpose, we check if the buttonbar is greyed out.
		// if it is greyed out, the reply is accepted and the button bur is re-activated.
		
		if ( waitingForReply_ == false)
		{
			return;
		}
		
		waitingForReply_ = false;
		
		toolBar_.setEnabled(true);
		toolBar_.stopButtonEnable(false);	
		currentTreeScrollPane_.setEnabled(true);
		this.setEnabledComponent(splitPane_, true);

		
		MessageSet ms = (MessageSet) data;

		String replyStr = new String("");
		for (int i = 0; i < ms.size(); i++)
		{
			Message msg = ms.getMessage(i);

			Object sourceObj = msg.getSource();
			System.out.println ("Source: " + source.toString());

			if (sourceObj == this)
			{				
				String status = msg.getStatus();
				
				System.out.println ("This message is for xprop: " + status);
				
				if (status != null)
				{
					replyStr += msg.getUrl().toString() + ": " + status+"\n";
					System.out.println ("Error reply: " + replyStr);
				} else {
					this.processReply (msg);
				}
			}

		}
		
	}
	
	

       
	
	protected String getSelectedPartition()
	{
		// Update the parameter table
		// Get partition
		return (String)comboBoxModel_.getSelectedItem(); 
	}
	
	protected String getSelectedTarget(String selectedPartition)
	{
	
	// Get selected element in partition
		TreePath[] paths = cache_.getJTree(selectedPartition).getSelectionModel().getSelectionPaths();
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
                return treeNode.toString();
	}

	protected void processReply (Message msg)
	{
		// Update the parameter table
		// Get partition
		//String currentSelection = (String)comboBoxModel_.getSelectedItem(); 

		// Get selected element in partition
		//TreePath[] paths = cache_.getJTree(currentSelection).getSelectionModel().getSelectionPaths();
                //DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
                //String currentElement = treeNode.toString();

		// Get the editor pane for the two items
		String p = this.getSelectedPartition();
		String t = this.getSelectedTarget(p);
		
		XPropEditor e = cache_.getEditor(p, t);		

		String replyCommand = msg.getFunction() + "Response";
		org.w3c.dom.NodeList nodeList = msg.getDOM().getElementsByTagName (replyCommand);
		if (nodeList.getLength() == 1)
		{
			try 
			{
				if (replyCommand.equals ("ParameterQueryResponse"))
				{
					org.w3c.dom.NodeList parameters=nodeList.item(0).getChildNodes();
					for (int j = 0; j < parameters.getLength(); j++)
					{
						if (parameters.item(j).getNodeName().equals("Parameter"))
						{
							System.out.println ("Create new model for ParameterQuery.");
							e.setModel ( new ParameterTableModel (parameters.item(j)) );
							return;
						}
					}
				} else  if (replyCommand.equals ("ParameterGetResponse"))
				{
					System.out.println ("Update model for ParameterGet.");
					try {
					
						org.w3c.dom.NodeList parameters=nodeList.item(0).getChildNodes();
						for (int j = 0; j < parameters.getLength(); j++)
						{
							if (parameters.item(j).getNodeName().equals("Parameter"))
							{					
								( (ParameterTableModel) e.getModel() ).update (parameters.item(j));
								return;
							}
						}
					} catch (ParameterTableModelException ex)
					{
						System.out.println (ex.toString());
					}
					return;
				} else {
					System.out.println ("Wrong response: " + replyCommand);
				}

			} catch (ParameterTableModelException ex)
			{
				System.out.println (ex.toString());
			}
		} else
		{
			System.out.println ("Invalid responde. Multiple Parameter nodes in reply.");
		}
	}
        

	/*
	public void setModel(XPropDefaultModel model) {
		defaultModel_ = model;
	}
        */
        
	// listener interface implementation
	public void xpropChanged(XPropModelEvent event)
	{
                // remember the selected item
                //String currentSelection = (String)comboBoxModel_.getSelectedItem(); 
		String p = this.getSelectedPartition();
		
                boolean stillPresent = false;
                String name = null;
		
                // update combobox
		comboBoxModel_.removeAllElements();
		Enumeration names = defaultModel_.getConfigurationNames();
		while( names.hasMoreElements() )
		{
                        name = (String)names.nextElement();
			comboBoxModel_.addElement(name);
                        if (p != null && p  == name ) {
                            // selected item still in list
                            stillPresent = true;
			}
		}
                
                               
                // 
                
                // if list of partition is empty then reset tree scroll pane to empty
                if (comboBoxModel_.getSize() == 0 ) 
		{
		
		frame_.setTitle ("XProp");
				
                    partitionNavigator_.remove(currentTreeScrollPane_);
                    currentTreeScrollPane_ =  new JScrollPane(dummyTreePanel_); 
                    partitionNavigator_.add(currentTreeScrollPane_);
                    splitPane_.revalidate();
                    currentTreeScrollPane_.setVisible(true);
		    
                    toolBar_.setEnabled(false);
		} else
                {
                     if ( stillPresent ) 
		     {
                            comboBoxModel_.setSelectedItem(p); 
                            this.refreshSelection(p);
                     }
                     else
                     {
                            comboBoxModel_.setSelectedItem(name);  // set the last inserted
                            this.refreshSelection(name);
                     }                  
		}				
	}
	
        protected void refreshSelection (String itemName )
        {                       
		currentTreeScrollPane_.setVisible(false);
		// remove previous Tree pane
		partitionNavigator_.remove(currentTreeScrollPane_);
		// fecth required scroll pane
		XProp.XPropertiesListener configuration = (XProp.XPropertiesListener)defaultModel_.getValue(itemName);
		ImmediateAccess item = (ImmediateAccess) configuration.getItem();
		TreeModel model = (TreeModel)item.getValueAsObject();
		// put new selection, get tree pane from model (if cached )
		currentTreeScrollPane_ = (JScrollPane)cache_.getScrollPane(itemName, model);			

		// add new selection to tree panel
		partitionNavigator_.add(currentTreeScrollPane_);
		splitPane_.revalidate();
		currentTreeScrollPane_.setVisible(true);

		frame_.setTitle("XProp: " + itemName);

                //force  refresh according current selection in tree
                JTree tree = cache_.getJTree(itemName);

                this.refreshTreeSelection(tree);
        }
        
            
        protected void refreshTreeSelection(JTree tree)
        {
                TreePath[] paths = tree.getSelectionModel().getSelectionPaths();
               
                 // Protect against empty selection
		if (paths == null) return;

               
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[0].getLastPathComponent();

                String currentElement = node.toString();
                
		// Transport and Application are equivalent

                if (	currentElement.startsWith("Host") ||
			currentElement.startsWith("Application") || 
			currentElement.startsWith("Transport") )
		{
			 toolBar_.setEnabled(true);
                         
			 // Here I should create the properties editor
			 //String currentSelection = (String)comboBoxModel_.getSelectedItem(); 
			 String p = this.getSelectedPartition();
			 
			 XPropEditor e = cache_.getEditor(p, currentElement);
			 splitPane_.setRightComponent (e);
		}
                else 
		{
                    toolBar_.setEnabled(false);
		    splitPane_.setRightComponent ( new JScrollPane() );
                }
                // set current title in containing frame
                // show into a label .(lastElement);
              
        }
	
        // Event generated by ComboBox
	public void itemStateChanged (ItemEvent e)
	{
		if ( e.getStateChange() == ItemEvent.SELECTED ) 
		{
                    this.refreshSelection((String) e.getItem());
                       
                }
	}
        
        // Event generated by Tree selection
        public void valueChanged (TreeSelectionEvent e)
	{
                JTree tree = (JTree) e.getSource();
		this.refreshTreeSelection(tree);		
        }
        
        // event generated bx ToolBar
        public synchronized void  buttonPressed(String buttonName)
        {
             // here I perfrom the action on the current displayed properties editor if any
	      
		if (buttonName.equals ("default"))
		{
	      		// Get default parameters from tree and put into window
	      		// buttonName: "default"
			try 
			{
				String p = this.getSelectedPartition(); 
				String t = this.getSelectedTarget(p);			
				JTree tree = cache_.getJTree(p);
				TreePath[] paths = tree.getSelectionModel().getSelectionPaths();
               			DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
				XTreeNode xn = (XTreeNode) treeNode.getUserObject();
				XTreeNode defaultParameters = xn.firstChild("DefaultParameters");
				XPropEditor e = cache_.getEditor(p, t);	
				
				if (defaultParameters != null)
				{
					ParameterTableModel m = new ParameterTableModel (defaultParameters.domNode);
					e.setModel (m);					
				} else {
					e.setModel ( new ParameterTableModel() );
				}
			} catch (ParameterTableModelException ptme)
			{
				System.out.println (ptme.toString());
			}
		} 
		else if (buttonName.equals ("get"))
		{	    
	      		// ParameterGet, buttonName: "get"
			
			
			toolBar_.setEnabled(false);
			toolBar_.stopButtonEnable(true);
			
			currentTreeScrollPane_.setEnabled(false);
			this.setEnabledComponent(splitPane_, false);
			
			waitingForReply_ = true;
			
			String p = this.getSelectedPartition(); 
			String t = this.getSelectedTarget(p);
			
			JTree tree = cache_.getJTree(p);
			
			MessageSet ms = MessageFactory.createMessageSet (
				tree.getSelectionModel().getSelectionPaths(), 
				"ParameterGet", this);
				
			XPropEditor e = cache_.getEditor(p, t);	
		
			org.w3c.dom.Document doc = ( (ParameterTableModel) e.getModel() ).exportSelection(true);
			org.w3c.dom.Node node = doc.getDocumentElement();
			Message msg = ms.getMessage(0);
			msg.setData(node);	
			commandPublisher.fireItemValueChanged ("xcommand", ms);			
			
		}	
		else if (buttonName.equals ("set"))
		{
			// ParameterSet: buttonName: "set"
			
			toolBar_.setEnabled(false);
			toolBar_.stopButtonEnable(true);
			
			currentTreeScrollPane_.setEnabled(false);
			this.setEnabledComponent(splitPane_, false);
			
			waitingForReply_ = true;
			
			String p = this.getSelectedPartition(); 
			String t = this.getSelectedTarget(p);
			
			JTree tree = cache_.getJTree(p);
			
			MessageSet ms = MessageFactory.createMessageSet (
				tree.getSelectionModel().getSelectionPaths(), 
				"ParameterSet", this);
				
			XPropEditor e = cache_.getEditor(p, t);	
			org.w3c.dom.Document doc = ( (ParameterTableModel) e.getModel() ).exportSelection(true);
			org.w3c.dom.Node node = doc.getDocumentElement();
			
			Message msg = ms.getMessage(0);
			msg.setData(node);	
			commandPublisher.fireItemValueChanged ("xcommand", ms);	
			
		}    
	      	else if (buttonName.equals ("query"))
		{
			// ParameterQuery: buttonName: "query"
			
			toolBar_.setEnabled(false); // disactivate toolbar
			toolBar_.stopButtonEnable(true); // activate stop button
			this.setEnabledComponent(splitPane_, false); // grey out panel
			waitingForReply_ = true;
			
			String currentPartition = (String)comboBoxModel_.getSelectedItem(); 
			
			JTree tree = cache_.getJTree(currentPartition);
			
			MessageSet ms = MessageFactory.createMessageSet (
				tree.getSelectionModel().getSelectionPaths(), 
				"ParameterQuery", this);
							
			commandPublisher.fireItemValueChanged ("xcommand", ms);
			
			
			
		}
		else if (buttonName.equals ("stop"))
		{
			// ParameterQuery: buttonName: "query"
			
			toolBar_.setEnabled(true);
			toolBar_.stopButtonEnable(false);
			
			// prepare call her
			
			currentTreeScrollPane_.setEnabled(true);
			setEnabledComponent(splitPane_, true);
			waitingForReply_ = false;
		}
	      
	
        }
	
	public void setEnabledComponent ( Container c, boolean enabled )
	{
		Component[] list = c.getComponents();
		
		for (int i = 0; i < list.length; i++)
		{
			Container cont = (Container) list[i];
			cont.setEnabled(enabled);
			setEnabledComponent ( cont, enabled );
		}
	}
	
	
	void shutdown()
	{
		try {
			commandPublisher.shutdown();
			replySubscriber.shutdown();
		} catch (Exception e)
		{
			logger.error (e.toString());
		}
	}
	

}

