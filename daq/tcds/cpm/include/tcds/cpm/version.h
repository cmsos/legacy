#ifndef _tcdscpm_version_h_
#define _tcdscpm_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSCPM_VERSION_MAJOR 3
#define TCDSCPM_VERSION_MINOR 13
#define TCDSCPM_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSCPM_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSCPM_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSCPM_VERSION_CODE PACKAGE_VERSION_CODE(TCDSCPM_VERSION_MAJOR,TCDSCPM_VERSION_MINOR,TCDSCPM_VERSION_PATCH)
#ifndef TCDSCPM_PREVIOUS_VERSIONS
#define TCDSCPM_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSCPM_VERSION_MAJOR,TCDSCPM_VERSION_MINOR,TCDSCPM_VERSION_PATCH)
#else
#define TCDSCPM_FULL_VERSION_LIST TCDSCPM_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSCPM_VERSION_MAJOR,TCDSCPM_VERSION_MINOR,TCDSCPM_VERSION_PATCH)
#endif

namespace tcdscpm
{
  const std::string package = "tcdscpm";
  const std::string versions = TCDSCPM_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS CPM.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS CPM.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
