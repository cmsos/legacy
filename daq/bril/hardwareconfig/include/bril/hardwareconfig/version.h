// $Id$

/*************************************************************************
 * XDAQ Application Template                     						 *
 * Copyright (C) 2000-2009, CERN.			               				 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci							 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         	 *
 * For the list of contributors see CREDITS.   					         *
 *************************************************************************/

#ifndef _bril_hardwareconfig_version_h_
#define _bril_hardwareconfig_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILHARDWARECONFIG_VERSION_MAJOR 1
#define BRILHARDWARECONFIG_VERSION_MINOR 7
#define BRILHARDWARECONFIG_VERSION_PATCH 0
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILHARDWARECONFIG_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILDIPPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILDIPPROCESSOR_VERSION_MAJOR,BRILDIPPROCESSOR_VERSION_MINOR,BRILDIPPROCESSOR_VERSION_PATCH)
#ifndef BRILDIPPROCESSOR_PREVIOUS_VERSIONS
#define BRILDIPPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILDIPPROCESSOR_VERSION_MAJOR,BRILDIPPROCESSOR_VERSION_MINOR,BRILDIPPROCESSOR_VERSION_PATCH)
#else
#define BRILDIPPROCESSOR_FULL_VERSION_LIST  BRILDIPPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILDIPPROCESSOR_VERSION_MAJOR,BRILDIPPROCESSOR_VERSION_MINOR,BRILDIPPROCESSOR_VERSION_PATCH)
#endif

namespace brildipprocessor
{
	const std::string package = "brilhardwareconfig";
	const std::string versions = BRILHARDWARECONFIG_FULL_VERSION_LIST;
	const std::string summary = "brilDAQ hardware config files";
	const std::string description = "collection of brilDAQ hardware config files. Only xml directory is used. The deployment location /opt/xdaq/htdocs/bril/hardwareconfig/xml .";
	const std::string authors = " ";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () throw (config::PackageInfo::VersionException);
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
