<html>

<head>
<meta charset="utf-8">
<title>CMS - Flashweb</title>
<link href="css/flashweb.css" rel="stylesheet" />
</head>

<body>
<?php
include_once ('tools.php');

if (isset ( $_POST ['delete'] )) { // to run PHP script on submit
	if (! empty ( $_POST ['chk_group'] )) {
		// Loop to store and display values of individual checked checkbox.
		foreach ( $_POST ['chk_group'] as $selected ) {
			echo "Deleting zone '" . $selected . "':";
			echo '<br>';
			echo "Deleting templates " . $selected . '-*-template';
			$json = deleteTemplates ( $_POST ['elasticsearchurl'], $selected . '-*-template' );
			if ($json ["acknowledged"]) {
				echo " OK <br>";
			} else {
				print_r ( $json );
			}
			
			echo "Deleting indices " . $selected . '-*';
			$json = deleteIndices ( $_POST ['elasticsearchurl'], $selected . '-*' );
			if ($json ["acknowledged"]) {
				echo " OK <br>";
			} else {
				print_r ( $json );
			}
		}
	}
}

echo '<br>';

echo '<form action="index.php">';
echo '<input type="submit" value="Go back" method="get">';
echo '</form>'?>

</body>
</html>