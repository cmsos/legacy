#include "bril/CalibFormat.h"
#include <iostream>
#include <cstdlib>

using namespace interface::bril;
int main(int argc, char** argv){
 
  void* v = std::malloc(4096);
  CalibObj* ov = static_cast<CalibObj*>(v);
  ov->setValidity(1,20);
  std::cout<<" valid since "<<ov->validSince()<<" valid till "<<ov->validTill()<<std::endl;
  std::free(v);
}
