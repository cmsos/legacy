package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Double extends SimpleItem {
	
	private java.lang.Double value;
	
	@Override
	public java.lang.Double getValue() {
		return value;
	}
	
	@Override
	public void setValue(Object value) {
		this.value = ((Number)value).doubleValue();
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		out.writeDouble(value);
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = in.readDouble();
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.DOUBLE.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:

