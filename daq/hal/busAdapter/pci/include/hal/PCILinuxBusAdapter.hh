#ifndef __PCILinuxBusAdapter
#define __PCILinuxBusAdapter

#include <vector>
#include "hal/HardwareDeviceInterface.hh"
#include "hal/PCIBusAdapterInterface.hh"
#include "hal/PCILinuxDeviceIdentifier.hh"
#include "hal/NoSuchDeviceException.hh"
#include "hal/BusAdapterException.hh"
#include "hal/UnsupportedException.hh"
#include "xpci/Bus.h"
#include "xpci/Address.h"
#include "byteswap.h"

namespace HAL {

/**
*
*
*     @short A memory mapped PCI bus adapter for use in Linux PCs.
*            
*            This  class implements  the  PCIBusAdapterInterface using
*            the  I2O-core library  developed  for accessing  hardware
*            plugged into  Linux PCs.  The functions are  described in
*            detail in the PCIBusAdapterInterface.
*            
*            This  BusAdapter  uses memory  mapping  in  order to  map 
*            memory space  accesses to the user  space. This optimizes 
*            the access time for the user. 
*
*       @see PCIBusAdapterInterface, I2O core library documentation
*    @author Christoph Schwick
* $Revision: 1.1 $
*     $Date: 2007/03/05 17:54:12 $
*
*
**/

class PCILinuxBusAdapter : public PCIBusAdapterInterface {
public:

  PCILinuxBusAdapter();

  virtual ~PCILinuxBusAdapter();

  /**
   * Registers the device with the i2ocore driver and maps it into
   * the address space of the user program. The vector barRegisters
   * which is returned by this routine contains the base addresses 
   * to be used by the user in order to access the memory reagions
   * corresponding to the various PCI-BARs. (All this is of course 
   * automatically handled by the HAL-library.)
   * @param swapFlag can be given if the device does only handle 
   *        big endian accesses. (The Myrinet Lanai9 card is such
   *        an exotic card.) If this option is set to true all 
   *        data accesses for this device are byte-swapped.
   */
  void findDeviceByVendor( uint32_t vendorID, 
			   uint32_t deviceID,
			   uint32_t index,
			   const PCIAddressTable& pciAddressTable,
			   PCIDeviceIdentifier** deviceIdentifierPtr,
			   std::vector<uint32_t>& barRegisters,
			   bool swapFlag = false );

  void findDeviceByVendor( uint32_t vendorID,
                           uint32_t deviceID,
                           uint32_t index,
                           const PCIAddressTable& pciAddressTable,
                           PCIDeviceIdentifier** deviceIdentifierPtr,
                           std::vector<uint64_t>& barRegisters,
                           bool swapFlag = false);

  
  /**
   * Registers the device with the i2ocore driver and maps it into
   * the address space of the user program. The vector barRegisters
   * which is returned by this routine contains the base addresses 
   * to be used by the user in order to access the memory reagions
   * corresponding to the various PCI-BARs. (All this is of course 
   * automatically handled by the HAL-library.)
   * @param swapFlag can be given if the device does only handle 
   *        big endian accesses. (The Myrinet Lanai9 card is such
   *        an exotic card.) If this option is set to true all 
   *        data accesses for this device are byte-swapped.
   */
  void findDeviceByBus( uint32_t busID, 
			uint32_t slotID,
			uint32_t functionID,
			const PCIAddressTable& pciAddressTable,
			PCIDeviceIdentifier** deviceIdentifierPtr,
			std::vector<uint32_t>& barRegisters,
			bool swapFlag = false );

  void findDeviceByBus( uint32_t busID, 
			uint32_t slotID,
			uint32_t functionID,
			const PCIAddressTable& pciAddressTable,
			PCIDeviceIdentifier** deviceIdentifierPtr,
			std::vector<uint64_t>& barRegisters,
			bool swapFlag = false );
  
  /**
   * Destroys the deviceIdentifier and releases the resources taken
   * for the memory mapping.
   */
  void closeDevice( PCIDeviceIdentifier* deviceIdentifier );

  void write( PCIDeviceIdentifier& device,
              uint32_t address,
              uint32_t data);
  
  void read( PCIDeviceIdentifier& device,
             uint32_t address,
             uint32_t* result);

  void configWrite( PCIDeviceIdentifier& device, 
                    uint32_t address, 
                    uint32_t data );

  void configRead( PCIDeviceIdentifier& device,
                   uint32_t address, 
                   uint32_t* result );

  void writeBlock(  PCIDeviceIdentifier& device,
                    uint32_t startAddress,
                    uint32_t length,
                    char *buffer,
                    HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) ;

  void readBlock(  PCIDeviceIdentifier& device,
                   uint32_t startAddress,
                   uint32_t length,
                   char *buffer,
                   HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT );


  void write64( PCIDeviceIdentifier& device,
              uint64_t address,
              uint64_t data);
  
  void read64( PCIDeviceIdentifier& device,
             uint64_t address,
             uint64_t* result);

  void configWrite64( PCIDeviceIdentifier& device, 
                    uint64_t address, 
                    uint64_t data );

  void configRead64( PCIDeviceIdentifier& device,
                   uint64_t address, 
                   uint64_t* result );

  void writeBlock64(  PCIDeviceIdentifier& device,
                    uint64_t startAddress,
                    uint64_t length,
                    char *buffer,
                    HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT ) ;

  void readBlock64(  PCIDeviceIdentifier& device,
                   uint64_t startAddress,
                   uint64_t length,
                   char *buffer,
                   HalAddressIncrement addressBehaviour = HAL_DO_INCREMENT );


private:
  /**
   * Common Helper funcition for the public findDeviceByXXX methods. 
   */
  void findDevice( xpci::Address& deviceConfigAddress,
                   const PCIAddressTable& pciAddressTable,
                   PCIDeviceIdentifier** deviceIdentifierPtr,
                   std::vector<uint64_t>& barRegisters,
		   bool swapFlag = false );

  xpci::Bus pciBus_; 
};

} /* namespace HAL */

#endif /* __PCILinuxBusAdapter */
