// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_heartbeat_heartbeatd_version_h_
#define _xmas_heartbeat_heartbeatd_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define XMASHEARTBEATHEARTBEATD_VERSION_MAJOR 2
#define XMASHEARTBEATHEARTBEATD_VERSION_MINOR 0
#define XMASHEARTBEATHEARTBEATD_VERSION_PATCH 1
// If any previous versions available E.g. #define XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS


//
// Template macros
//
#define XMASHEARTBEATHEARTBEATD_VERSION_CODE PACKAGE_VERSION_CODE(XMASHEARTBEATHEARTBEATD_VERSION_MAJOR,XMASHEARTBEATHEARTBEATD_VERSION_MINOR,XMASHEARTBEATHEARTBEATD_VERSION_PATCH)
#ifndef XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS
#define XMASHEARTBEATHEARTBEATD_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(XMASHEARTBEATHEARTBEATD_VERSION_MAJOR,XMASHEARTBEATHEARTBEATD_VERSION_MINOR,XMASHEARTBEATHEARTBEATD_VERSION_PATCH)
#else 
#define XMASHEARTBEATHEARTBEATD_FULL_VERSION_LIST  XMASHEARTBEATHEARTBEATD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(XMASHEARTBEATHEARTBEATD_VERSION_MAJOR,XMASHEARTBEATHEARTBEATD_VERSION_MINOR,XMASHEARTBEATHEARTBEATD_VERSION_PATCH)
#endif 

namespace xmasheartbeatheartbeatd
{
	const std::string package  =  "xmasheartbeatheartbeatd";
	const std::string versions =  XMASHEARTBEATHEARTBEATD_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andy Forrest";
	const std::string summary = "XDAQ Monitoring and Alarming System heartbeatd";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
