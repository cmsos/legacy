<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>
<link href="css/xdaq-tables.css" rel="stylesheet" />
<link href="css/xdaq-fonts.css" rel="stylesheet" />
<link href="css/xdaq-button.css" rel="stylesheet" />
<link type="text/css" href="css/ESlayout.css" rel="stylesheet" />
</head>
<body>

<?php

    include_once('config/config.php');
    include_once('tools.php');
   
    
    ini_set('display_errors', 'Off');
    error_reporting(E_ALL | E_STRICT);
    
    
   // $ccresponse = \Httpful\Request::get( "http://" . $config['root'] . "/_nodes/stats/http")->send();
    
    //$ccjson = json_decode($ccresponse, true);
     echo '<div class="header">';
     echo '<h2><b><center>FLASHLIST DATA</center></b></h2>';
     echo '<p> Zone: ' . $config['zone'] . '</p>';
     echo '</div>';
    
     echo '<div id="options-div">';
     echo '<center>';
     echo '<a target="_blank" href="https://cms-service-xmas.web.cern.ch/cms-service-xmas/ESClient-master/esQueryClient.html"><button id="clientm">Visit ESClient-master</button></a>';
     echo '<a target="_blank" href="nodeExtract.php"><button id="node-check">Check Cluster</button></a>';
     echo '<a target="_blank" href="connectionExtract.php"><button id="node-check">Check Connections</button></a>';
     echo '<button id="reset-flash" onclick="location.href=\'clearAllCollection.php?index=' . $config['flash_index'] .'\'">Clear all flashlists</button>';
     echo '<button id="reset-shelf" onclick="location.href=\'clearAllCollection.php?index=' . $config['shelf_index'] .'\'">Clear all shelflists</button>';
     echo '<button id="on-xtreme" onclick="location.href=\'enableServices.php\'">Enable Xtreme Services</button>';
     echo '<button id="off-xtreme" onclick="location.href=\'disableServices.php\'">Disable Xtreme Services</button>';
     echo '</center>';
     echo '</div>';
    
    
    // retrieve full set of flashlists
    // parse response into object code
    $response = retrieveCatalog($config['host'], $config['port'],  $config['flash_index']);
    
    $json = json_decode($response, true);
    
    $get_flash_url = getIndexInfo($config['host'], $config['port'],  $config['flash_index']);
    $shelf_url = getIndexInfo($config['host'], $config['port'], "shelflist");
    $flash_url = $config['host'] . ":" . $config['port'] . "/" .  $config['flash_index'] . "/_stats";
    
    $filter  =array();
    echo '<br />';

    //results table
    echo '<div id = "main-div">';
    echo '<table id = "flashlist-table" class="xdaq-table">';
    echo '<thead>';
    echo '<tr>';
    //echo '<th><a href= "http://' . $flash_url . '">Flashlist</a></th>';
    echo '<th><a href= "displayIndex.php?index=' . $config['flash_index'] . '">Flashlist</a></th>';
    echo '<th>Hits</th>';
    echo '<th>Sources</th>';
    echo '<th>Size</th>';
    echo '<th>CSV</th>';
    echo '<th>JSON</th>';
    echo '<th>Mapping</th>';
     echo '<th>Clear</th>';
    echo '<th>Delete Map</th>';
    echo '</tr>';
    echo '</thead>';
    
    echo '<tbody>';

        foreach ($json['flashlist']['mappings'] as $key => $val) {
            if ( $key=='jobcontrol')
            {
               // continue;
            }
            $qname = "urn:xdaq-flashlist:" . $key;
            echo "<tr>";
            echo '<td>';
            echo '<a href="retrieveCollection.php?fmt=html&flash=' . $qname . '">';
            echo $qname;
            echo '</a>';
            echo '</td>';
            
            echo '<td>';
            $jsonhits = totalHitsCounter($config['host'], $config['port'], $config['flash_index'], $key);
            echo $jsonhits['count'];
            echo '</td>';
            
            echo '<td>';
            $jsonhits = topHits($config['host'], $config['port'],  $config['flash_index'], $key, $config['maxsources'], $filter);
            $hitsCount = count($jsonhits['aggregations']['group_by_context']['buckets']);
            echo $hitsCount;        
            echo '</td>';
            
            echo '<td>';
            echo "unknown";
            echo '</td>';
            
            echo '<td>';
            echo '<a href="displayCollection.php?fmt=csv&flash=' . $qname . '"><button>View</button></a>';
            echo '</td>';
            
            echo '<td>';    
            echo '<a href="displayCollection.php?fmt=json&flash=' . $qname . '"><button>View</button></a>';
            echo '</td>';
            
            echo '<td>';
            echo '<a href="displayMapping.php?fmt=json&flash=' . $qname . '"><button>View</button></a>';
            echo '</td>';
            
            echo '<td>';
            echo '<a href="clearCollection.php?index=' . $config['flash_index'] . '&flash=' . $qname . '"><button>Clear</button></a>';
            echo '</td>';
            
            echo '<td>';
            echo '<button id="delete-map" onclick="location.href=\'deleteMapping.php?index=' . $config['flash_index'] . '&flash=' . $qname . '\'">Delete</button>';
            echo '</td>';
            
            echo "</tr>";
        }

    echo '</tbody>';
    echo '</table>';
    
    $response = retrieveCatalog($config['host'], $config['port'],  $config['shelf_index']);
    
    $json = json_decode($response, true);

    echo '<table id = "shelflist-table" class="xdaq-table">';
    echo '<thead>';
    echo '<tr>';
    //echo '<th><a href= "http://' . $shelf_url . '">Flashlist</a></th>';
    echo '<th><a href= "displayIndex.php?index=' .  $config['shelf_index'] . '">Shelflist</a></th>';
    echo '<th>Hits</th>';
    echo '<th>Sources</th>';
    echo '<th>Size</th>';
    echo '<th>CSV</th>';
    echo '<th>JSON</th>';
    echo '<th>Mapping</th>';
    echo '<th>Clear</th>';
    echo '<th>Delete Map</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    
    foreach ($json['shelflist']['mappings'] as $key => $val) {
        
        $qname = "urn:xdaq-flashlist:" . $key;
        echo "<tr>";
        echo '<td>';
        echo '<a href="retrieveCollection.php?tophits=true&index=' . $config['shelf_index'] . '&delimiter=,&fmt=html&flash=' . $qname . '">';
        echo $qname;
        echo '</a>';
        echo '</td>';
        
        echo '<td>';
        $jsonhits = totalHitsCounter($config['host'], $config['port'], $config['shelf_index'], $key);
        echo $jsonhits['count'];
        echo '</td>';
        
        echo '<td>';
        //$jsonhits = topHits($config['host'], $config['port'],  $config['shelf_index'], $key, $config['maxsources'], $filter);
        //$hitsCount = count($jsonhits['aggregations']['group_by_context']['buckets']);
        //echo $hitsCount;
        echo "disabled";
        echo '</td>';
        
        echo '<td>';
        echo "unknown";
        echo '</td>';
        
        echo '<td>';
        echo '<a href="displayCollection.php?tophits=true&index=' . $config['shelf_index'] . '&delimiter=,&fmt=csv&flash=' . $qname . '"><button>View</button></a>';
        echo '</td>';
        
        echo '<td>';
        echo '<a href="displayCollection.php?tophits=true&index=' . $config['shelf_index'] . '&delimiter=,&fmt=json&flash=' . $qname . '"><button>View</button></a>';
        echo '</td>';
        
        echo '<td>';
        echo '<a href="displayMapping.php?index=' . $config['shelf_index'] . '&fmt=json&flash=' . $qname . '"><button>View</button></a>';
        echo '</td>';
        
        echo '<td>';
        echo '<button onclick="location.href=\'clearCollection.php?index=' . $config['shelf_index'] . '&flash=' . $qname . '\'">Clear</button>';
        echo '</td>';
        
        echo '<td>';
        echo '<button id="delete-map" onclick="location.href=\'deleteMapping.php?index=' . $config['shelf_index'] . '&flash=' . $qname . '\'">Delete</button>';
        echo '</td>';

        echo "</tr>";
    }
    
    echo '</tbody>';
    echo '</table>';

    echo '</div>';
    
    echo "</br>";
    echo "</br>";
    echo "</br>";
    
?>

</body>
</html>



