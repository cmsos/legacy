// Module:  Log4CPLUS
// File:    factory.cxx
// Created: 2/2002
// Author:  Tad E. Smith
//
//
// Copyright (C) Tad E. Smith  All rights reserved.

#include <log4cplus/spi/factory.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/helpers/threads.h>

#include <appender4oracle/OracleAppender.h>

using namespace std;
using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;


///////////////////////////////////////////////////////////////////////////////
// LOCAL file class definitions
///////////////////////////////////////////////////////////////////////////////

namespace log4cplus 
{
	class OracleAppenderFactory : public AppenderFactory 
	{
		public:
        
		SharedAppenderPtr createObject(const Properties& props)
        {
            return SharedAppenderPtr(new log4cplus::OracleAppender(props));
        }

        tstring getTypeName() 
		{ 
            return LOG4CPLUS_TEXT("log4cplus::OracleAppender"); 
        }
    };
}


void log4cplus::registerOracleAppenderFactory()
{
	AppenderFactoryRegistry& reg = getAppenderFactoryRegistry();
	auto_ptr<AppenderFactory> ptr(new OracleAppenderFactory());
	reg.put(ptr);
}
