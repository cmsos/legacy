#include "jal/jtagChain/JTAGChain.h"
#include "jal/jtagController/JTAGController.h"

#include "jal/jtagChain/JTAGScanData.h"

#include <stdio.h>
#include <stdint.h>

#include "tap_tran.h"


using namespace std;

jal::JTAGChain::JTAGChain(JTAGController& ctrl, uint32_t ichain, double desiredTCKfrequency) 
  : _controller(ctrl),
    _current_chain_state(jal::JTAGState::UNDEF), 
    _desiredTCKfrequency(desiredTCKfrequency) { 

  if (ichain < _controller.numberOfChains())
    _chainNumber = ichain;
  else 
    XCEPT_RAISE(jal::OutOfRangeException, "JTAGChain(): invalid chain number specified.");

}

void jal::JTAGChain::runTest(uint32_t nclk, 
			     jal::JTAGState runstate, 
			     jal::JTAGState endstate,
			     double mintime, 
			     double maxtime,
			     jal::RunTestStage stage)
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  if ( !runstate.isStable() )
    XCEPT_RAISE(jal::OutOfRangeException, "runTest(): invalid runstate specified.");

  if ( !endstate.isStable() )
    XCEPT_RAISE(jal::OutOfRangeException, "runTest(): invalid endstate specified.");

  // get a lock on the controller & select the chain
  _controller.lock(); _controller.selectChain(_chainNumber); 
  _controller.setDesiredTCKFrequency(_desiredTCKfrequency);

  //
  // check minimum time if it was specified
  //
  uint32_t minclocks = 0;
  if (mintime != -1.) {
    // if a desired frequency is set, use this, 
    // otherwise get the frequency from the controller
    double frequency = (_desiredTCKfrequency != -1.) ? _desiredTCKfrequency : _controller.getSCKFrequency();
    minclocks = (uint32_t) (frequency * mintime);
//     cout << "mintime = " << mintime
// 	 << "  converted to clock = " << minclocks << endl;
  }

  if (nclk < minclocks) nclk = minclocks;

  // ignore maxtime

  if (stage == jal::RTSTAGE_PRE || stage == jal::RTSTAGE_ALL) {

    toState(runstate);
    _controller.pulseTCK(nclk, 
			 runstate == jal::JTAGState::RESET, 
			 stage == jal::RTSTAGE_PRE ? jal::PULSESTAGE_PRE : jal::PULSESTAGE_ALL);

  }

  if (stage == jal::RTSTAGE_PAUSE)
    _controller.pulseTCK(nclk, 
			 runstate == jal::JTAGState::RESET, 
			 jal::PULSESTAGE_PAUSE);

  if (stage == jal::RTSTAGE_POST || stage == jal::RTSTAGE_ALL) {
    toState(endstate);
  }

  // unlock the controller
  _controller.unlock();

}

void jal::JTAGChain::gotoState(jal::JTAGState state) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  if ( !state.isStable() )
    XCEPT_RAISE(jal::OutOfRangeException, "gotoState(): invalid state specified.");

  // get a lock on the controller & select the chain
  _controller.lock(); _controller.selectChain(_chainNumber);
  _controller.setDesiredTCKFrequency(_desiredTCKfrequency);

  toState(state);

  // unlock the controller
  _controller.unlock();
}

void jal::JTAGChain::gotoStateByPath(vector<jal::JTAGState> statepath) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {
  
  static const struct {       /* This structure is used for next state     */
    jal::JTAGState tms0;           /* logic implementation of the TAP state     */
    jal::JTAGState tms1;           /* Machine.                                  */
  } next_state_lookup[] = {
    {jal::JTAGState::IDLE,    jal::JTAGState::RESET},                      /* Present state = RESET  */
    {jal::JTAGState::IDLE,    jal::JTAGState::SEDR},                       /* Present state = IDLE  */
    {jal::JTAGState::PAUSEDR, jal::JTAGState::E2DR},                       /* Present state = PAUSEDR  */
    {jal::JTAGState::PAUSEIR, jal::JTAGState::E2IR},                       /* Present state = PIR  */
            	              	
    {jal::JTAGState::CPDR,    jal::JTAGState::SEIR},                       /* Present state = SEDR */
    {jal::JTAGState::SHDR,    jal::JTAGState::E1DR},                       /* Present state = CPDR */
    {jal::JTAGState::SHDR,    jal::JTAGState::E1DR},                       /* Present state = SHDR */
    {jal::JTAGState::PAUSEDR, jal::JTAGState::UPDR},                       /* Present state = E1DR */
    {jal::JTAGState::SHDR,    jal::JTAGState::UPDR},                       /* Present state = E2DR */
    {jal::JTAGState::IDLE,    jal::JTAGState::SEDR},                       /* Present state = UPDR */
            	              	
    {jal::JTAGState::CPIR,    jal::JTAGState::RESET},                      /* Present state = SEIR */
    {jal::JTAGState::SHIR,    jal::JTAGState::E1IR},                       /* Present state = CPIR */
    {jal::JTAGState::SHIR,    jal::JTAGState::E1IR},                       /* Present state = SHIR */
    {jal::JTAGState::PAUSEIR, jal::JTAGState::UPIR},                       /* Present state = E1IR */
    {jal::JTAGState::SHIR,    jal::JTAGState::UPIR},                       /* Present state = E2IR */
    {jal::JTAGState::IDLE,    jal::JTAGState::SEDR}                        /* Present state = UPIR */
  };

  // get a lock on the controller & select the chain
  _controller.lock(); _controller.selectChain(_chainNumber); 
  _controller.setDesiredTCKFrequency(_desiredTCKfrequency);


  // work-around for problem with Xilinx files generated with IMPACT ( 6.1 )
  // problem: if two operations are put in a single SVF file, the second 
  //          operation starts with STATE RESET IDLE. This is illegal when 
  //          the current state is anything other than RESET. After a previous 
  //          operation however the state is usually IDLE.
  //
  // fix:     if the current state is stable and the first state is a different 
  //          stable state then do a state transition to the first state.
  
  if (_current_chain_state.isStable() &&
      (statepath.begin())->isStable() &&
      _current_chain_state != (*statepath.begin()) )
    toState( *statepath.begin() );


  uint32_t states_to_go = statepath.size();
  vector<jal::JTAGState>::const_iterator it = statepath.begin();

  do {
    uint32_t num_bits = (states_to_go - 1)%16 + 1;
      
    // make a TMS word

    uint32_t tms_word = 0;
    jal::JTAGState tap_state = _current_chain_state;

    for (uint32_t i = 0; i<num_bits; i++) {
      if (next_state_lookup[tap_state].tms1 == *it) tms_word |= (uint32_t)1 << i;
      else if (next_state_lookup[tap_state].tms0 != *it) {
	char tmp[200];
	sprintf(tmp, "gotoStateByPath(): invalid state transition requested. current state = %d, new state = %d", int(tap_state),int( *it));
	XCEPT_RAISE(jal::OutOfRangeException, tmp);
      }

      tap_state = *(it++);
    }

    _controller.sequenceTMS(num_bits, tms_word);
    
    // set current state after sequence has been sent 
    _current_chain_state = tap_state;

    states_to_go -= num_bits;

  } while ( states_to_go > 0);

  // unlock the controller
  _controller.unlock();

}

void jal::JTAGChain::resetTAP() 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  // get a lock on the controller & select the chain
  _controller.lock(); _controller.selectChain(_chainNumber);
  _controller.setDesiredTCKFrequency(_desiredTCKfrequency);

  _controller.sequenceTMS(8, 0xff);            /* 8 TCK's with TMS high */
  _current_chain_state = jal::JTAGState::RESET;

  // unlock the controller
  _controller.unlock();
}

void jal::JTAGChain::scanIR(uint32_t numBits, 
			    jal::JTAGScanData const& input, 
			    jal::JTAGScanData & response, 
			    bool doRead, 
			    jal::JTAGState endstate) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  doScan(true, numBits, input, response, doRead, endstate);

}

void jal::JTAGChain::scanDR(uint32_t numBits, 
			    jal::JTAGScanData const& input, 
			    jal::JTAGScanData & response, 
			    bool doRead, 
			    jal::JTAGState endstate) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  doScan(false, numBits, input, response, doRead, endstate);

}

/// lock the chain (in order to perform operations without interruptions)
void jal::JTAGChain::lock(){
  //TBD
}

/// unlock the chain
void jal::JTAGChain::unlock(){
  //TBD
}


//////////////////////////////////////////////////////////////////////////////////////////////////////
//
// private methods
//
//////////////////////////////////////////////////////////////////////////////////////////////////////

void jal::JTAGChain::doScan(bool irscan, 
			    uint32_t numBits, 
			    jal::JTAGScanData const& input, 
			    jal::JTAGScanData & response, 
			    bool doRead, 
			    jal::JTAGState endstate) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  if ( !endstate.isStable() )
    XCEPT_RAISE(jal::OutOfRangeException, "doScan(): invalid endstate specified.");

  // get a lock on the controller & select the chain
  _controller.lock(); _controller.selectChain(_chainNumber);
  _controller.setDesiredTCKFrequency(_desiredTCKfrequency);

  if (numBits != 0) {

    // go to shift state
    toState(irscan ? jal::JTAGState::SHIR : jal::JTAGState::SHDR);

    // scan the data through the chain
    _controller.shift(numBits, input, response, doRead, true);

    // using autoTMS we end up in the exit state
    _current_chain_state = irscan ? jal::JTAGState::E1IR : jal::JTAGState::E1DR;

  } 
  else { // numBits == 0

    // go to update state
    toState(irscan ? jal::JTAGState::UPIR : jal::JTAGState::UPDR);
  }
  
  // go to endstate
  toState( endstate );
  
  // unlock the controller
  _controller.unlock();
}


void jal::JTAGChain::toState(jal::JTAGState state) 
  throw(jal::HardwareException,
	jal::TimeoutException,
	jal::OutOfRangeException) {

  if (state == jal::JTAGState::UNDEF) return;


  // do a reset first if we are in UNDEF state
  if (_current_chain_state == jal::JTAGState::UNDEF) {
    _controller.sequenceTMS(8, 0xff);            /* 8 TCK's with TMS high */
    _current_chain_state = jal::JTAGState::RESET;
  }

  if (_current_chain_state == state) return;

  // perform allowed types of state transitions
  
  if ( _current_chain_state.isStable() && state.isStable() ) // stable=>stable
    _controller.sequenceTMS(Tap_transitions[_current_chain_state][state].num_tcks,
			    Tap_transitions[_current_chain_state][state].tms);
 
  else if ( _current_chain_state.isStable() && state == jal::JTAGState::SHDR ) // stable=>SHDR
    _controller.sequenceTMS(Tms_to_shdr[_current_chain_state].num_tcks, Tms_to_shdr[_current_chain_state].tms);

  else if ( _current_chain_state.isStable() && state == jal::JTAGState::SHIR ) // stable=>SHIR
    _controller.sequenceTMS(Tms_to_shir[_current_chain_state].num_tcks, Tms_to_shir[_current_chain_state].tms);

  else if ( _current_chain_state.isStable() && state == jal::JTAGState::UPDR ) // stable=>UPDR ?need it?
    _controller.sequenceTMS(Tms_to_updr[_current_chain_state].num_tcks, Tms_to_updr[_current_chain_state].tms);

  else if ( _current_chain_state.isStable() && state == jal::JTAGState::UPIR ) // stable=>UPIR ?need it?
    _controller.sequenceTMS(Tms_to_upir[_current_chain_state].num_tcks, Tms_to_upir[_current_chain_state].tms);

  else if ( _current_chain_state == jal::JTAGState::E1DR && state.isStable() ) // E1DR=>stable
    _controller.sequenceTMS(Tms_from_e1dr[state].num_tcks, Tms_from_e1dr[state].tms);

  else if ( _current_chain_state == jal::JTAGState::E1IR && state.isStable() ) // E1IR=>stable
    _controller.sequenceTMS(Tms_from_e1ir[state].num_tcks, Tms_from_e1ir[state].tms);

  else if ( _current_chain_state == jal::JTAGState::UPDR && state.isStable() ) // UPDR=>stable ?need it?
    _controller.sequenceTMS(Tms_from_update[state].num_tcks, Tms_from_update[state].tms);

  else if ( _current_chain_state == jal::JTAGState::UPIR && state.isStable() ) // UPIR=>stable ?need it?
    _controller.sequenceTMS(Tms_from_update[state].num_tcks, Tms_from_update[state].tms);

  else 
    XCEPT_RAISE(jal::OutOfRangeException, "toState(): internal error. invalid state transition requested.");

  _current_chain_state = state;
}

// About JTAG state transitions
// ============================
//
//
// ** SVF/EVF
//
// - In SVF, four (stable) endstates are supported: RESET, IDLE, DRPAUSE, IRPAUSE
// 
// - In EVF, scanIR/DP-pause commands exist. However, they are not encoded by svf2evf and svf3evf
//
// - The EVF player (pg_ctrl.c, evf_lib.c, scanlib.c) does support the scanIR/DR-pause commands
//  
// - In normal IR/DRscan commands, scanlib does the following steps:
//   - go from stable state to shift state (if in PAUSE state, then thro EXIT2, otherwise via capture)
//   - go to exit1, then to endstate via update state
//     
//   - if 0 bits are specified, go form current state to update state and then to end state 
//
// - In scanIR/DR-pause commands
//   - go from stable state to shift state (if in PAUSE state, then thro EXIT2, otherwise via capture)
//   - go to exit1, then pause
//
//   - if 0 bits are specified, go form current state to PAUSE state (if already there, just do 1 clock)
//
//
//
// ** JAM
//
// - In JAM any endstate is allowed
// 
// - Before a scan, go from current state to start state:
//   RESET, IDLE => IDLE
//   DR* => DRPAUSE
//   IR* => IRPAUSE
//
// - Go to shift via capture (does not resume from pause)
//
// - Go to exit1, pause
//
// - go to specified endstate
//
//
//
// Problem: Altera SVF files use ENDSTAE IRPAUSE
//
//   - probably expected behavior
//     - after scan go to pause
//     - at beginning of next scan go through update, capture
// 
//   - behavior in EVF/SVF:
//     - after scan go to upd, capture, exit1, pause
//     - successive drscan does not read anything useful.
//
// Solutions:
// 
//     1) change scanlib to go directly to pause and not through upd and capture
//        this may be a problem for two successive IRSCANS !!
//
//         
//     2) ignore IRPAUSE endstate and always go to IDLE (may be slightly slower)
//
//     3) change scanlib to go directly to pause and not through upd and capture
//        also change it not to resume from pause but to go through udp and capture.
//
//        commands may be exectuted only later., on the other hand a runtest goes to some runstate anyways
//
//
//        pausing does not seem to be supported by SVF, anyways, so what do we want to do when the endstate is pause
//        staying in PAUSE and not resuming from PAUSE seems like a good solution 
// 
//        only problem: if some SVF file runs RUNTEST in PAUSE state, then the command/data is not updated before the runtest
//                       
