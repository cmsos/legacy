// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors:L. Orsini, P. Roberts, D. Simelevicius    		             *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#if HAVE_CONFIG_H
  #include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if HAVE_ERRNO_H
  #include <errno.h>
#endif
#if HAVE_UNISTD_H
  #include <unistd.h>
#endif
#if HAVE_SYS_TYPES_H
  #include <sys/types.h>
#endif
#if HAVE_SYS_TIME_H
  #include <sys/time.h>
#endif
#include <iostream>
#include <xercesc/util/XercesDefs.hpp>
#include <xercesc/util/XMLNetAccessor.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLExceptMsgs.hpp>
#include <xercesc/util/Janitor.hpp>
#include <xercesc/util/XMLUniDefs.hpp>
#include <xercesc/util/TransService.hpp>
#include <xercesc/util/TranscodingException.hpp>
#include <xercesc/util/PlatformUtils.hpp>

#include "elastic/api/CurlURLInputStream.hpp"
#include "elastic/api/CurlNetAccessor.hpp"


#include "elastic/api/exception/NotFound.h"


elastic::api::CurlURLInputStream::CurlURLInputStream(const XMLURL& urlSource, HTTPMethod fHTTPMethod, std::list<std::string> & headers, const NetHTTPInfo* httpInfo)
      : fMulti(0)
      , fEasy(0)
      , fMemoryManager(urlSource.getMemoryManager())
      , fURLSource(urlSource)
      , fTotalBytesRead(0)
      , fWritePtr(0)
      , fBytesRead(0)
      , fBytesToRead(0)
      , fDataAvailable(false)
      , fBufferHeadPtr(fBuffer)
      , fBufferTailPtr(fBuffer)
      , fPayload(0)
      , fPayloadLen(0)
      , fContentType(0)
{
	std::cout << "1" <<std::endl;
	// Allocate the curl multi handle
	fMulti = curl_multi_init();

	// Allocate the curl easy handle
	fEasy = curl_easy_init();

	// Set URL option
    TranscodeToStr url(fURLSource.getURLText(), "ISO8859-1", fMemoryManager);
	curl_easy_setopt(fEasy, CURLOPT_URL, (char*)url.str());

    // Set up a way to recieve the data
	curl_easy_setopt(fEasy, CURLOPT_WRITEDATA, this);						// Pass this pointer to write function
	curl_easy_setopt(fEasy, CURLOPT_WRITEFUNCTION, staticWriteCallback);	// Our static write function

	// Do redirects
	curl_easy_setopt(fEasy, CURLOPT_FOLLOWLOCATION, (long)1);
	curl_easy_setopt(fEasy, CURLOPT_MAXREDIRS, (long)6);

	long timeout = 5; // default
	char* pPath;
	pPath = getenv ("XDAQ_XERCES_CURL_REQUEST_TIMEOUT");
	if (pPath!=NULL)
	{
		timeout = atoi(pPath);
	}
	curl_easy_setopt(fEasy, CURLOPT_TIMEOUT, timeout);
	curl_easy_setopt(fEasy, CURLOPT_CONNECTTIMEOUT, timeout);

	std::cout << "2" <<std::endl;
    // Add username and password if authentication is required
    const XMLCh *username = urlSource.getUser();
    const XMLCh *password = urlSource.getPassword();
    if(username && password) {
        XMLBuffer userPassBuf(256, fMemoryManager);
        userPassBuf.append(username);
        userPassBuf.append(chColon);
        userPassBuf.append(password);

        TranscodeToStr userPass(userPassBuf.getRawBuffer(), "ISO8859-1", fMemoryManager);

        curl_easy_setopt(fEasy, CURLOPT_HTTPAUTH, (long)CURLAUTH_ANY);
        curl_easy_setopt(fEasy, CURLOPT_USERPWD, (char*)userPass.str());
    }
    std::cout << "3" <<std::endl;

    // Set the correct HTTP method
    switch(fHTTPMethod) {
    	case elastic::api::GET:
    		break;
    	case elastic::api::PUT:
    		curl_easy_setopt(fEasy, CURLOPT_PUT, (long)1);
    		break;
    	case elastic::api::POST:
    		curl_easy_setopt(fEasy, CURLOPT_POST, (long)1);
    		break;
    	case elastic::api::HEAD:
    		curl_easy_setopt(fEasy, CURLOPT_NOBODY, (long)1);
    		break;
    	case elastic::api::DELETE:
    		curl_easy_setopt(fEasy, CURLOPT_CUSTOMREQUEST, "DELETE");
    	    break;
    }

    std::cout << "4" <<std::endl;
    if ( headers.size() > 0 )
    {
    	std::cout << "4.1" <<std::endl;
    	struct curl_slist *headersList = 0;

    	for (std::list<std::string>::iterator i = headers.begin(); i != headers.end(); i++ )
    	{
    		std::cout << "4.1.1" <<std::endl;
    		headersList = curl_slist_append(headersList, (*i).c_str());
    	}
    	curl_easy_setopt(fEasy, CURLOPT_HTTPHEADER, headersList);
    	std::cout << "4.2" <<std::endl;
    	//curl_slist_free_all(headersList);
    }


	std::cout << "5" <<std::endl;

	// Set up the payload
	if(httpInfo->fPayload) {
		curl_easy_setopt(fEasy, CURLOPT_UPLOAD, (long)1);

		std::cout << "if theres payload" <<std::endl;
		fPayload = httpInfo->fPayload;
		fPayloadLen = httpInfo->fPayloadLen;
		curl_easy_setopt(fEasy, CURLOPT_READDATA, this);
		curl_easy_setopt(fEasy, CURLOPT_READFUNCTION, staticReadCallback);
		curl_easy_setopt(fEasy, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fPayloadLen);
	}
	else
	{
		curl_easy_setopt(fEasy, CURLOPT_HTTP_TRANSFER_DECODING, 0);
		//curl_easy_setopt(fEasy, CURLOPT_TRANSFER_ENCODING, 0);
	}


	// Add easy handle to the multi stack
	curl_multi_add_handle(fMulti, fEasy);
	std::cout << "6" <<std::endl;
    // Start reading, to get the content type
	while(fBufferHeadPtr == fBuffer)
	{
		int runningHandles = 0;
		 std::cout << "rmore begin " <<std::endl;
        readMore(&runningHandles);
        std::cout << "rmore end :" << runningHandles <<std::endl;
		if(runningHandles == 0) break;
	}
	std::cout << "7" <<std::endl;
    // Find the content type
    char *contentType8 = 0;
    curl_easy_getinfo(fEasy, CURLINFO_CONTENT_TYPE, &contentType8);
    if(contentType8)
        fContentType = TranscodeFromStr((XMLByte*)contentType8, XMLString::stringLen(contentType8), "ISO8859-1", fMemoryManager).adopt();

}


elastic::api::CurlURLInputStream::~CurlURLInputStream()
{
	// Remove the easy handle from the multi stack
	curl_multi_remove_handle(fMulti, fEasy);

	// Cleanup the easy handle
	curl_easy_cleanup(fEasy);

	// Cleanup the multi handle
	curl_multi_cleanup(fMulti);

    if(fContentType) fMemoryManager->deallocate(fContentType);
}


size_t
elastic::api::CurlURLInputStream::staticWriteCallback(char *buffer,
                                        size_t size,
                                        size_t nitems,
                                        void *outstream)
{
	return ((CurlURLInputStream*)outstream)->writeCallback(buffer, size, nitems);
}

size_t
elastic::api::CurlURLInputStream::staticReadCallback(char *buffer,
                                       size_t size,
                                       size_t nitems,
                                       void *stream)
{
    return ((CurlURLInputStream*)stream)->readCallback(buffer, size, nitems);
}

size_t
elastic::api::CurlURLInputStream::writeCallback(char *buffer,
                                  size_t size,
                                  size_t nitems)
{
	size_t cnt = size * nitems;
	size_t totalConsumed = 0;

	// Consume as many bytes as possible immediately into the buffer
	size_t consume = (cnt > fBytesToRead) ? fBytesToRead : cnt;
	memcpy(fWritePtr, buffer, consume);
	fWritePtr		+= consume;
	fBytesRead		+= consume;
	fTotalBytesRead	+= consume;
	fBytesToRead	-= consume;

	//printf("write callback consuming %d bytes\n", consume);

	// If bytes remain, rebuffer as many as possible into our holding buffer
	buffer			+= consume;
	totalConsumed	+= consume;
	cnt				-= consume;
	if (cnt > 0)
	{
		size_t bufAvail = sizeof(fBuffer) - (fBufferHeadPtr - fBuffer);
		consume = (cnt > bufAvail) ? bufAvail : cnt;
		memcpy(fBufferHeadPtr, buffer, consume);
		fBufferHeadPtr	+= consume;
		buffer			+= consume;
		totalConsumed	+= consume;
		//printf("write callback rebuffering %d bytes\n", consume);
	}

	// Return the total amount we've consumed. If we don't consume all the bytes
	// then an error will be generated. Since our buffer size is equal to the
	// maximum size that curl will write, this should never happen unless there
	// is a logic error somewhere here.
	return totalConsumed;
}

size_t
elastic::api::CurlURLInputStream::readCallback(char *buffer,
                                 size_t size,
                                 size_t nitems)
{
    size_t len = size * nitems;
    if(len > fPayloadLen) len = fPayloadLen;

    memcpy(buffer, fPayload, len);

    fPayload += len;
    fPayloadLen -= len;

    return len;
}

bool elastic::api::CurlURLInputStream::readMore(int *runningHandles)
{
	 std::cout << "10" <<std::endl;
    // Ask the curl to do some work
    CURLMcode curlResult = curl_multi_perform(fMulti, runningHandles);

    std::cout << "after 10" <<std::endl;
    //&& curlResult != CURLE_ABORTED_BY_CALLBACK

     // Process messages from curl
    int msgsInQueue = 0;
    for (CURLMsg* msg = NULL; (msg = curl_multi_info_read(fMulti, &msgsInQueue)) != NULL; )
    {
    	 std::cout << "10.1" <<std::endl;
    	long http_code;
    	 curl_easy_getinfo(msg->easy_handle, CURLINFO_RESPONSE_CODE, &http_code);
    	    	if (http_code == 404 )
    	    	{
    	    	   XCEPT_RAISE(elastic::api::exception::NotFound, "404 Not Found");
    	    	}

        //printf("msg %d, %d from curl\n", msg->msg, msg->data.result);

        if (msg->msg != CURLMSG_DONE)
            return true;

        switch (msg->data.result)
        {
        case CURLE_OK:
            // We completed successfully. runningHandles should have dropped to zero, so we'll bail out below...
            break;

        case CURLE_UNSUPPORTED_PROTOCOL:
            ThrowXMLwithMemMgr(MalformedURLException, XMLExcepts::URL_UnsupportedProto, fMemoryManager);
            break;

        case CURLE_COULDNT_RESOLVE_HOST:
        case CURLE_COULDNT_RESOLVE_PROXY:
          {
            if (fURLSource.getHost())
              ThrowXMLwithMemMgr1(NetAccessorException, XMLExcepts::NetAcc_TargetResolution, fURLSource.getHost(), fMemoryManager);
            else
              ThrowXMLwithMemMgr1(NetAccessorException, XMLExcepts::File_CouldNotOpenFile, fURLSource.getURLText(), fMemoryManager);
            break;
          }

        case CURLE_COULDNT_CONNECT:
            ThrowXMLwithMemMgr1(NetAccessorException, XMLExcepts::NetAcc_ConnSocket, fURLSource.getURLText(), fMemoryManager);
            break;

        case CURLE_RECV_ERROR:
            ThrowXMLwithMemMgr1(NetAccessorException, XMLExcepts::NetAcc_ReadSocket, fURLSource.getURLText(), fMemoryManager);
            break;

        default:
            ThrowXMLwithMemMgr1(NetAccessorException, XMLExcepts::NetAcc_InternalError, fURLSource.getURLText(), fMemoryManager);
            break;
        }
    }

    // If nothing is running any longer, bail out
    if(*runningHandles == 0)
    {
    	std::cout << "10.2" <<std::endl;
        return false;
    }

    // If there is no further data to read, and we haven't
    // read any yet on this invocation, call select to wait for data
    if (curlResult != CURLM_CALL_MULTI_PERFORM && fBytesRead == 0)
    {
    	std::cout << "10.3" <<std::endl;
        fd_set readSet;
        fd_set writeSet;
        fd_set exceptSet;
        int fdcnt=0;

        FD_ZERO(&readSet);
        FD_ZERO(&writeSet);
        FD_ZERO(&exceptSet);

        // Ask curl for the file descriptors to wait on
        curl_multi_fdset(fMulti, &readSet, &writeSet, &exceptSet, &fdcnt);

        // Wait on the file descriptors
        timeval tv;
        tv.tv_sec  = 2;
        tv.tv_usec = 0;
        select(fdcnt+1, &readSet, &writeSet, &exceptSet, &tv);
    }

    std::cout << "10000000----" <<std::endl;
    return curlResult == CURLM_CALL_MULTI_PERFORM;
}

size_t
elastic::api::CurlURLInputStream::readBytes(XMLByte* const          toFill
                                     , const size_t maxToRead)
{
	fBytesRead = 0;
	fBytesToRead = maxToRead;
	fWritePtr = toFill;

	for (bool tryAgain = true; fBytesToRead > 0 && (tryAgain || fBytesRead == 0); )
	{
		// First, any buffered data we have available
		size_t bufCnt = fBufferHeadPtr - fBufferTailPtr;
		bufCnt = (bufCnt > fBytesToRead) ? fBytesToRead : bufCnt;
		if (bufCnt > 0)
		{
			memcpy(fWritePtr, fBufferTailPtr, bufCnt);
			fWritePtr		+= bufCnt;
			fBytesRead		+= bufCnt;
			fTotalBytesRead	+= bufCnt;
			fBytesToRead	-= bufCnt;

			fBufferTailPtr	+= bufCnt;
			if (fBufferTailPtr == fBufferHeadPtr)
				fBufferHeadPtr = fBufferTailPtr = fBuffer;

			//printf("consuming %d buffered bytes\n", bufCnt);

			tryAgain = true;
			continue;
		}

		// Ask the curl to do some work
		int runningHandles = 0;
        tryAgain = readMore(&runningHandles);

		// If nothing is running any longer, bail out
		if (runningHandles == 0)
			break;
	}




	return fBytesRead;
}

const XMLCh *elastic::api::CurlURLInputStream::getContentType() const
{
    return fContentType;
}
