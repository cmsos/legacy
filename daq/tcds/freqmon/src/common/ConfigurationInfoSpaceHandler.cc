#include "tcds/freqmon/ConfigurationInfoSpaceHandler.h"

// #include <string>

#include "xdaq/Application.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::freqmon::ConfigurationInfoSpaceHandler::ConfigurationInfoSpaceHandler(xdaq::Application& xdaqApp) :
  tcds::utils::ConfigurationInfoSpaceHandler(xdaqApp)
{
  // Where to find the frequency counter (via telnet).
  createString("hostName",
               "freqmeter1.cms",
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
  createUInt32("portNumber",
               4001,
               "",
               tcds::utils::InfoSpaceItem::NOUPDATE,
               true);
}

void
tcds::freqmon::ConfigurationInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  std::string itemSetName = "itemset-app-config";
  monitor.newItemSet(itemSetName);
  monitor.addItem(itemSetName,
                  "hostName",
                  "Host name of the frequency counter telnet accessor",
                  this);
  monitor.addItem(itemSetName,
                  "portNumber",
                  "Port number of the frequency counter telnet accessor",
                  this);
}

void
tcds::freqmon::ConfigurationInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                            tcds::utils::Monitor& monitor,
                                                                            std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Configuration" : forceTabName;

  webServer.registerTab(tabName,
                        "Configuration parameters",
                        2);
  webServer.registerTable("Application configuration",
                          "Application configuration parameters",
                          monitor,
                          "itemset-app-config",
                          tabName);
}
