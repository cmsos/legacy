#ifndef _tcdsbobr_version_h_
#define _tcdsbobr_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSBOBR_VERSION_MAJOR 3
#define TCDSBOBR_VERSION_MINOR 13
#define TCDSBOBR_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSBOBR_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSBOBR_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSBOBR_VERSION_CODE PACKAGE_VERSION_CODE(TCDSBOBR_VERSION_MAJOR,TCDSBOBR_VERSION_MINOR,TCDSBOBR_VERSION_PATCH)
#ifndef TCDSBOBR_PREVIOUS_VERSIONS
#define TCDSBOBR_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSBOBR_VERSION_MAJOR,TCDSBOBR_VERSION_MINOR,TCDSBOBR_VERSION_PATCH)
#else
#define TCDSBOBR_FULL_VERSION_LIST TCDSBOBR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSBOBR_VERSION_MAJOR,TCDSBOBR_VERSION_MINOR,TCDSBOBR_VERSION_PATCH)
#endif

namespace tcdsbobr
{
  const std::string package = "tcdsbobr";
  const std::string versions = TCDSBOBR_FULL_VERSION_LIST;
  const std::string description = "CMS software for the BOBR.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the BOBR.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
