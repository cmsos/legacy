#!/usr/bin/env python

###############################################################################
## Master script to configure and enable an CPM driving several iCIs
## and PIs and check the TTS-triggered actions.
###############################################################################

import sys

from pytcds.test_cpm_tts_actions.tcds_test_cpm_tts_actions_constants import APVES
from pytcds.test_cpm_tts_actions.tcds_test_cpm_tts_actions_constants import CPM
from pytcds.test_cpm_tts_actions.tcds_test_cpm_tts_actions_constants import ICIS
from pytcds.test_cpm_tts_actions.tcds_test_cpm_tts_actions_constants import PIS
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_command_runner import add_cwd_to_cmds
from pytcds.utils.tcds_command_runner import CommandRunner

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

PATIENCE_TIME = 5

###############################################################################

def append_piece_to_cmds(cmds, piece):
    res = []
    for (cmd, doc) in cmds:
        full_cmd = cmd
        if cmd.endswith(".py"):
            full_cmd = "%s %s" % (cmd, piece)
        res.append((full_cmd, doc))
    # End of append_piece_to_cmds().
    return res

###############################################################################

class TCDSConfigForTestCPMTTSActions(CmdLineBase):

    def handle_args(self):
        # NOTE: This is a slight abuse of the CmdLineBase base class
        # since we don't use an IP address argument. Don't tell
        # anyone.
        pass
        # End of handle_args().

    def main(self):
        sep_line = "-" * 70

        fsm_ctrl_cmd = "../utils/tcds_fsm_control.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        fsm_poll_cmd = "../utils/tcds_fsm_poller.py --cfg-file ../utils/tcds_setup_tcdslab.cfg"
        pi_force_ready_cmd = "./tcds_pi_force_ready.py"
        pi_force_oos_cmd = "./tcds_pi_force_oos.py"
        pi_force_warning_cmd = "./tcds_pi_force_warn.py"

        cmds = []

        #----------

        # Halt everything.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Halting all controllers'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = [CPM]
        targets.extend(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Halt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Halted state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Halted".format(fsm_poll_cmd, target), ""))

        #----------

        # Configure CPM.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Configure hw_cfg_cpm.txt".format(fsm_ctrl_cmd, CPM), ""))

        # Wait till the CPM is in the Configured state.
        cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, CPM), ""))

        # Configure iCIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring iCIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_ici.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure APVEs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring APVEs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(APVES)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_apve.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Configure PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Configuring PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Configure hw_cfg_pi.txt".format(fsm_ctrl_cmd, target), ""))

        # Wait till everything is in the Configured state.
        for target in targets:
            cmds.append(("{0:s} {1:s} --wait-until Configured".format(fsm_poll_cmd, target), ""))

        # Force PIs to be TTS READY.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s}".format(pi_force_ready_cmd, target), ""))

        #----------

        # Enable iCIs, APVEs, PIs.
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling iCIs, APVEs, and PIs'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        targets = list(ICIS)
        targets.extend(APVES)
        targets.extend(PIS)
        for target in targets:
            cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, target), ""))

        # Enable CPM (as last!).
        cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
                     ("python -c \"print 'Enabling CPM'\"", ""),
                     ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        cmds.append(("{0:s} {1:s} Enable".format(fsm_ctrl_cmd, CPM), ""))

        #----------

        # Wait till the CPM is in the Enabled state.
        cmds.append(("{0:s} {1:s} --wait-until Enabled".format(fsm_poll_cmd, CPM), ""))

        #----------

        # # Wait a little.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Practicing patience...'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("python -c \"import time; time.sleep({0:d})\"".format(PATIENCE_TIME), ""))

        # #----------

        # # Force PIs to be TTS OOS.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Forcing PI TTS states to OOS'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # targets = list(PIS)
        # for target in targets:
        #     cmds.append(("{0:s} {1:s}".format(pi_force_oos_cmd, target), ""))

        # #----------

        # # Wait a little.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Practicing patience...'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("python -c \"import time; time.sleep({0:d})\"".format(PATIENCE_TIME), ""))

        # #----------

        # # Force PIs to be TTS READY.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # targets = list(PIS)
        # for target in targets:
        #     cmds.append(("{0:s} {1:s}".format(pi_force_ready_cmd, target), ""))

        # #----------

        # # Wait a little.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Practicing patience...'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("python -c \"import time; time.sleep({0:d})\"".format(PATIENCE_TIME), ""))

        # #----------

        # # Force PIs to be TTS WARNING.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Forcing PI TTS states to WARNING'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # targets = list(PIS)
        # for target in targets:
        #     cmds.append(("{0:s} {1:s}".format(pi_force_warning_cmd, target), ""))

        # #----------

        # # Wait a little.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Practicing patience...'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # cmds.append(("python -c \"import time; time.sleep({0:d})\"".format(PATIENCE_TIME), ""))

        # #----------

        # # Force PIs to be TTS READY.
        # cmds.extend([("python -c \"print '{0:s}'\"".format(sep_line), ""),
        #              ("python -c \"print 'Forcing PI TTS states to READY'\"", ""),
        #              ("python -c \"print '{0:s}'\"".format(sep_line), "")])
        # targets = list(PIS)
        # for target in targets:
        #     cmds.append(("{0:s} {1:s}".format(pi_force_ready_cmd, target), ""))

        #----------

        cmds = add_cwd_to_cmds(cmds)
        if self.verbose:
            cmds = append_piece_to_cmds(cmds, "--verbose")

        runner = CommandRunner(cmds)
        runner.run()

        # End of main().

    # End of class TCDSConfigForTestCPMTTSActions.

###############################################################################

if __name__ == "__main__":

    description = "Master script to test the TTS-triggered actions."
    usage = "usage: %prog [options]"
    res = TCDSConfigForTestCPMTTSActions(description, usage).run()
    print "Done"
    sys.exit(res)

###############################################################################
