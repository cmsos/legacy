
// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: P. roberts					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_smarthub_version_h_
#define _xmas_smarthub_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define XMASSMARTHUB_VERSION_MAJOR 1
#define XMASSMARTHUB_VERSION_MINOR 1
#define XMASSMARTHUB_VERSION_PATCH 0
// If any previous versions available E.g. #define XMASSMARTHUB_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef XMASSMARTHUB_PREVIOUS_VERSIONS


//
// Template macros
//
#define XMASSMARTHUB_VERSION_CODE PACKAGE_VERSION_CODE(XMASSMARTHUB_VERSION_MAJOR,XMASLAS2_VERSION_MINOR,XMASSMARTHUB_VERSION_PATCH)
#ifndef XMASSMARTHUB_PREVIOUS_VERSIONS
#define XMASSMARTHUB_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(XMASSMARTHUB_VERSION_MAJOR,XMASSMARTHUB_VERSION_MINOR,XMASSMARTHUB_VERSION_PATCH)
#else
#define XMASSMARTHUB_FULL_VERSION_LIST  XMASSMARTHUB_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(XMASSMARTHUB_VERSION_MAJOR,XMASSMARTHUB_VERSION_MINOR,XMASSMARTHUB_VERSION_PATCH)
#endif

namespace xmassmarthub
{
	const std::string package  =  "xmassmarthub";
	const std::string versions =  XMASSMARTHUB_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "P.Roberts";
	const std::string summary = "Discover zones through the xplore applications and heartbeat services";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
