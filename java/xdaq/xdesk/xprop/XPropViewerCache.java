package xdaq.xdesk.xprop;

import javax.swing.*;
import java.util.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import xdaq.xdesk.xtree.*;

public class XPropViewerCache
{
	HashMap scrollPanes_;	
	HashMap treeModels_;
        HashMap  treePanels_;
        TreeSelectionListener listener_;
	HashMap editorCache_; // stores XPropEditorCache objects
        XTreeFilter filter_;	
	
	public XPropViewerCache(TreeSelectionListener listener)
	{
                listener_ = listener;
		scrollPanes_ = new HashMap();
		treeModels_ = new HashMap();
                treePanels_ = new HashMap();
		editorCache_ = new HashMap();
		filter_ = new XTreeFilter();
		filter_.addFilter ("urlTransport");
		filter_.addFilter ("Definitions");
		filter_.addFilter ("Commands");			
		filter_.addFilter ("urlApplication");
		filter_.addFilter ("Address");
		
		// This can be filtered out, cause the XTreeNode functions
		// to get children operates on the underlying DOM tree.
		filter_.addFilter ("DefaultParameters");
	}
	
	
	
	public JScrollPane getScrollPane(String name, TreeModel model)
	{
		JScrollPane pane = (JScrollPane)scrollPanes_.get(name);
		TreeModel current = (TreeModel)treeModels_.get(name);
		
		if ( pane == null )
		{
			XTreeModel submodel = ((XTreeModel)model).cloneModelWithFilter(filter_);
			XTree tree = new XTree(submodel);
			tree.setRootVisible(false); // don't show root node
			
                        tree.addTreeSelectionListener(listener_);
			scrollPanes_.put(name, new JScrollPane(tree));
			
			// Create a new editor cache for this partition
			editorCache_.put (name, new XPropEditorCache(tree) );
			
			treeModels_.put(name,model);
                        treePanels_.put(name,tree);
                        tree.setSelectionRow(0);
                      			
		}
		else if ( model != current ) 
		{
			// Tree has changed, invalidate existing (remove from cache)
		
			this.invalidate(name);
                        
                        XTreeModel submodel = ((XTreeModel)model).cloneModelWithFilter(filter_);
			XTree tree = new XTree(submodel);
			tree.setRootVisible(false); // don't show root node
			
                        tree.addTreeSelectionListener(listener_);
			scrollPanes_.put(name, new JScrollPane(tree));
			
			// Create a new editor cache for this partition
			editorCache_.put (name, new XPropEditorCache(tree) );
			
			treeModels_.put(name,model);
                        treePanels_.put(name,tree);
                        tree.setSelectionRow(0);
			
		}
                        

		return (JScrollPane)scrollPanes_.get(name);	
	}
        
        
	
	public XPropEditor getEditor (String partition, String item)
	{
		XPropEditorCache c = (XPropEditorCache) editorCache_.get (partition);
		return c.get (item);
	}
	
	protected  void invalidate(String name)
	{
		scrollPanes_.remove(name);
		treeModels_.remove(name);
                treePanels_.remove(name);
		editorCache_.remove(name);
	}
        
        public JTree getJTree(String name )
        {
		
                return (JTree)treePanels_.get(name);
	}
}
