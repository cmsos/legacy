###########################################################
#
# TTCci configuration.
#
# (from TTCciControl::WriteConfigurationFile())
#
#
# Automatically created with TTCci::WriteConfiguration() 
# from current configuration on Fri Aug 24 15:55:07 2012 
#
# (Comments (will be ignored) are indicated by "#". Lines
#  can be split using the '\' character at end of 1st line.)
#
###########################################################

# THE SOURCES (What should the TTCci be listening to?):
# options: TTC, LTC, CTC, VME, INTERNAL, FRONTPANEL0|1, CYCLIC, or BCHANNEL
TRIGGER_SOURCE FRONTPANEL0 # CTC, LTC, SCOM, BCHANNEL, CYCLIC, FRONTPANEL0, FRONTPANEL1, INTERNAL
BGO_SOURCE CYCLIC # CTC, LTC, VME, CYCLIC
ORBIT_SOURCE   TTC          # CTC | LTC | TTC | INTERNAL

# CLOCK: possible interfaces:  CTC | LTC | TTC | INTERNAL
# parameters: QPLLReset=YES|NO       (ignored if not INTERNAL)
#             QPLLAutoRestart=YES|NO (ignored if not INTERNAL)
#             FreqBits==0x??
# CLOCK_SOURCE INTERNAL FreqBits=0x30#  CTC | LTC | TTC | INTERNAL
CLOCK_SOURCE TTC QPLLReset=YES QPLLAutoRestart=YES FreqBits=0x0

# TRIGGER SECTION (ignored if TRIGGER_SOURCE is not set to INTERNAL)
# Set (last) trigger to 1 to stop trigger sequence.
# (last) trigger = 0 ==> repetitive sequence (default)
# e.g. with N>0 lines like: 'TRIGGER_INTERVAL 0x20'
# or with 'TRIGGER_FREQUENCY_HZ 1.5 MODE=EQUI'
# where 'MODE' can be 'EQUI' (equidistant) or 'RANDOM'
# TRIGGER_INTERVAL 0x5784
TRIGGER_FREQUENCY_HZ 10000 MODE=EQUI   # in Hz, MODE=EQUI or MODE=RANDOM

# Set The BGO Channels, e.g. for channel 9 (=Start) with : 
#   BGO_CHANNEL 9 L=SINGLE|DOUBLE|BLOCK REPETITIVE=YES|NO \
#     DELAY1=255 DELAY2=3 PRESCALE=10 POSTSCALE=20
#   ADDBGODATA 9 DATA=0x1234 MODE=S|L|A  # S=SHORT, L=LONG, A=A-Command
# The first integer is the bgo channel number (0..15). 
# Alternatively, one can use the channel name instead of the 
# number. The channels 1 to 10 are reserved for BC0, TestEnable, 
# PrivateGap, PrivateOrbit, Resynch, HardReset, EC0, OC0, Start,
# and Stop, respectively.
#
# In order to use per word (individual) postscale values, add a parameter
# POSTSCALE==xy to each ADDBGODATA command. Note that
# you'll need a firmware version that supports it. The TTCci XDAQ application
# will show you the corresponding fields in the 'BGO Configuration' page if
# it does.


# Channel # 1 (BC0):
BGO_CHANNEL BC0 L=SINGLE REPETITIVE=YES STARTAFTER=0
ADDBGODATA BC0 DATA=0x9 MODE=S

# Channel # 5 (Resync):
BGO_CHANNEL Resync L=SINGLE REPETITIVE=YES STARTAFTER=0
ADDBGODATA Resync DATA=0x8 MODE=S

# Channel # 7 (EC0):
BGO_CHANNEL EC0 L=SINGLE REPETITIVE=YES STARTAFTER=0
ADDBGODATA EC0 DATA=0x2 MODE=S

# Channel # 8 (OC0):
BGO_CHANNEL OC0 L=SINGLE REPETITIVE=YES STARTAFTER=0
ADDBGODATA OC0 DATA=0x4 MODE=S

# Channel # 11 (StartOfGap):
BGO_CHANNEL StartOfGap L=SINGLE REPETITIVE=YES STARTAFTER=0 DELAY1=3440 PRESCALE=13
ADDBGODATA StartOfGap DATA=0x4 MODE=S

################################################################
# Configuration of sequences. Individual sequences can be defined for 
# 'coldReset', 'configure', 'enable', 'stop', 'suspend', 'periodic' 
# and 'user' (predefined)
# or for any user-defined sequence (use "ADDSEQUENCE ChooseName" first)
# Example: 
#   BEGINSEQUENCE enable 	# or configure, suspend, ...
#   ResetCounters 		# resets evt+orb cntrs. (on TTCci only!)
#   Sleep 5 			# sleep for 5 sec
#   mSleep 10 			# sleep for 10 ms
#   uSleep 100			# sleep for 10 us
#   EnableL1A 			# or DisableL1A e.g. for 'Suspend' Sequence
#   BGO 5 			# activates BGO-Channel 5 through VME	
#   BGO Start 			# activates BGO-Channel 9 through VME	
#   Periodic On 		# (On|Off) Use Periodic seq. with care!!!
#   Periodic 60  		# periodicity of Periodic sequence: 60 sec
#   SendBST 			# Take network time and send it as BST (to emulate BST)
#   ResumeCyclicAtStart yes|no	# Do|don't resume from where you left of
#   SendShortBDATA 0x5 		# Send short BDATA
#   SendLongBDATA 0x555 	# Send long BDATA
#   BGOSource CTC|LTC|VME|CYCLIC# To change the BGO source (VME ~= none)
#   Write [i] ADDRESS 0xfff 	# Write 0xffff to reg. ADDRESS (offset i)
#   Read [i] ADDRESS 		# Read from reg. ADDRESS (offset i)
#   ENDSEQUENCE			# closes this sequence

BEGINSEQUENCE coldReset
ENDSEQUENCE

BEGINSEQUENCE configure
  DisableL1A
  ResetCounters
  EnableL1A
ENDSEQUENCE

BEGINSEQUENCE enable
ENDSEQUENCE

BEGINSEQUENCE stop
ENDSEQUENCE

BEGINSEQUENCE suspend
ENDSEQUENCE

BEGINSEQUENCE periodic
ENDSEQUENCE

BEGINSEQUENCE user
  uSleep 10
ENDSEQUENCE

################################################################
# Cyclic Trigger and BGO Generators
# Usage: CYCLICTRIGGER id [arguments]
#    or: CYCLICBGO id [arguments]
# where id denotes the generator, i.e. 0..2 for TRIGGER and 
# 0..4 for BGO generators.
# The following arguments can be appended to these commands:
#   STARTBX=i 		# i=Offset in BX
#   PRES=i 		# i=prescale
#   POST=i 		# i=postscale (# of times)
#   INITWAIT=i 		# i=initial orbits to wait
#   REPETITIVE=y|n 	# repeat sequence
#   PAUSE=i 		# pause i orbits (for REPETITIVE=y)
#   PERMANENT=y|n 	# don't listen to BGO Start/Stop
#   CH=i|Name 		# BGO channel or name. For BGO only!
# The argument CH denotes the BGO channel to be requested.
# Its value can either be a number (0..15) or the channel 
# name, e.g. Resynch, HardReset, EC0, OC0, Start, Stop, TestEnable, 
# PrivateGap, or PrivateOrbit
#
CYCLICBGO 0 CH=BC0 REPETITIVE=y PERMANENT=y
CYCLICBGO 1 CH=StartOfGap STARTBX=3440 REPETITIVE=y PERMANENT=y


# TRIGGER RULE SETTINGS ###################################
# There are 7 that can be changed using e.g.:
# TRIGGERRULE i N_BX   # i = 1...7, N_BX = Min no. of clocks for i triggers


# EXTERNAL TRIGGER INPUT(S) DELAY(S) ##########################
#
# This value is used to set the external trigger 1 and or 0 input
# delay in units of bunch crossings.
# valid values are from 0 to 255
EXTTRIG0DELAY 0
EXTTRIG1DELAY 0
########### CONFIGURATION END #############################
