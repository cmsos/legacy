// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/Application.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 
#include "xcept/tools.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPHeader.h"
#include "xoap/domutils.h"
#include "pt/PeerTransportAgent.h"
#include "pt/SOAPMessenger.h"
 
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"

#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/Method.h"
#include "xoap/domutils.h"
#include "xoap/DOMParser.h"
#include "xoap/SOAPHeader.h"

#include "xgi/Table.h"
#include "xgi/framework/Method.h"

#include "psx/sapi/ApplicationService.h"
#include "psx/sapi/DpDisconnectRequest.h"
#include "psx/sapi/Hotlink.h"
#include "psx/sapi/DpConnectRequest.h"

#include "psx/mapi/ApplicationService.h" 
#include "psx/mapi/Monitor.h"

#include "toolbox/string.h"
 
XDAQ_INSTANTIATOR_IMPL(psx::Application);

 
psx::Application::Application(xdaq::ApplicationStub* s)   : xdaq::Application(s), xgi::framework::UIManager(this)
{
	mapi_ = 0;
	sapi_ = 0;

	s->getDescriptor()->setAttribute("icon","/psx/images/psx-icon.png");
	s->getDescriptor()->setAttribute("service","psx");
		
	xoap::bind(this, &psx::Application::getWinCCOASubscriptions, "getWinCCOASubscriptions", psx::NSURI );
	xoap::bind(this, &psx::Application::getSMISubscriptions, "getSMISubscriptions", psx::NSURI );

	xoap::bind(this, &psx::Application::onWinCCOARequest, "*", psx::sapi::NSURI );
	xoap::bind(this, &psx::Application::onSMIRequest, "*", psx::mapi::NSURI );
	
	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &psx::Application::Default, "Default");
	xgi::framework::deferredbind(this, this, &psx::Application::actionWinCCOA, "actionWinCCOA");
	xgi::framework::deferredbind(this, this, &psx::Application::actionSMI, "actionSMI");
	
	// Detect when the setting of defualt paramaters has been perfomed
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	
	projectName_     = "";
	projectNumber_   = "";
	databaseManager_ = "";
	eventManager_    = "";

	this->getApplicationInfoSpace()->fireItemAvailable("projectName",&projectName_);
	this->getApplicationInfoSpace()->fireItemAvailable("projectNumber",&projectNumber_);
	this->getApplicationInfoSpace()->fireItemAvailable("databaseManager",&databaseManager_);
        this->getApplicationInfoSpace()->fireItemAvailable("eventManager",&eventManager_);
	
	dns_    = "";
	
	this->getApplicationInfoSpace()->fireItemAvailable("dns",&dns_);
	
}

psx::Application::~Application()
{

}
	

// web interface
void psx::Application::Default(xgi::Input * in, xgi::Output * out ) 
{
	// Put a menu for the two services PVSS and SMI
	
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	
	*out << "<div style=\"border: 1px solid #FFFFFF;font-family: arial; font-size: 10pt;\">" << std::endl;
	
	// embedded applications
	//xgi::Utils::getWidget(out, "PVSS", "PVSS service specific actions", url, "MenuPVSS", "/psx/images/PVSS.png" );
	//xgi::Utils::getWidget(out, "SMI", "SMI service specific actions", url, "MenuSMI", "/psx/images/SMI.png" );	
	 // Begin of tabs
	 *out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;
	
	*out << "<div class=\"xdaq-tab\" title=\"WinCC OA\">" << std::endl;                                                                                                                                                                                                              
        this->WinCCOATabPage(in,out);
        *out << "</div>";

	*out << "<div class=\"xdaq-tab\" title=\"SMI\">" << std::endl;
        this->SMITabPage(in, out);
        *out << "</div>";

	*out << "</div>";
}

// web interface
void psx::Application::WinCCOATabPage(xgi::Input * in, xgi::Output * out ) 
{

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	
	// Show the following status information:
	// Name and port of event manager connected to, reachable?
	// Name and port of database manager connected to, reachable?
	*out << "<div id=\"default\">" << std::endl;
	*out << "Project number " << projectNumber_.toString() << ": " << std::endl;
	*out << "<a id=\"default\" target=\"_blank\" href=\"/daq/psx/sapi/" << projectName_.toString() << "/log/PVSS_II.log\">view</a> PVSS log<p>" << std::endl;
	*out << "</div>" << std::endl;
	
	xgi::Table t("default");
	t.addColumn ("Description");
	t.addColumn ("URL");
	t.addColumn ("Connection");
		
	std::vector<xgi::Table::Row>::iterator row = t.addRow ("default");
	row->addColumn("Database manager");
	row->addColumn(databaseManager_);
	
	if ((sapi_ != 0) && (sapi_->isDataManagerConnected()))
	{
		row->addColumn("OK");
	}
	else
	{
		row->addColumn("Not OK");
	}
	
	row = t.addRow ("default");
	row->addColumn("Event manager");
	row->addColumn(eventManager_);
	
	if ((sapi_ != 0) && (sapi_->isEventManagerConnected()))
	{
		row->addColumn("OK");
	}
	else
	{
		row->addColumn("Not OK");
	}

	*out << t << std::endl;
	
	// Table of subscriptions with button to delete (unsubscribe)
		
	*out << cgicc::br();	
	
	// Show all the subscriptions
	xgi::Table subscriptionTable ("default");
	subscriptionTable.addColumn ("Transaction");
	subscriptionTable.addColumn ("URL");
	subscriptionTable.addColumn ("Action");
	subscriptionTable.addColumn ("dpNames");
	std::vector<std::string> subscriptions = sapi_->getTransactions();
	
	if (subscriptions.empty())
	{
		std::vector<xgi::Table::Row>::iterator subscriptionTableRow = subscriptionTable.addRow ("default");
		subscriptionTableRow->addColumn( "n/a" );
		subscriptionTableRow->addColumn( "no transactions registered" );
		subscriptionTableRow->addColumn( "" );
		subscriptionTableRow->addColumn( "" );
	}
	else
	{
		for (std::vector<std::string>::size_type i = 0; i < subscriptions.size(); ++i)
		{
			std::vector<xgi::Table::Row>::iterator subscriptionTableRow = subscriptionTable.addRow ("default");
			subscriptionTableRow->addColumn( subscriptions[i] );
			subscriptionTableRow->addColumn( sapi_->getTransactionUrl(subscriptions[i]) );
			std::string link = "<a id=\"default\" href=\"";
			link += url;
			link += "/actionWinCCOA?action=clearTransaction&transaction=";
			link += subscriptions[i];
			link += "\">Delete</a>";
			subscriptionTableRow->addColumn( link );

			//
			psx::sapi::Hotlink * hotlink = sapi_->getTransaction(subscriptions[i]);
			psx::sapi::Request * originalRequest = hotlink->getRequest();
			std::set<std::string>& s = dynamic_cast<psx::sapi::DpConnectRequest*>(originalRequest)->getDpNames();
			subscriptionTableRow->addColumn(toolbox::printTokenSet(s,","));
		}

	}
	*out << subscriptionTable << std::endl;
}

void psx::Application::actionWinCCOA(xgi::Input * in, xgi::Output * out ) 
{	
	try
	{
	 	cgicc::Cgicc cgi(in);           
                std::string action = xgi::Utils::getFormElement(cgi, "action")->getValue();
		
		if (action == "clearTransaction")
		{
			std::string transaction = xgi::Utils::getFormElement(cgi, "transaction")->getValue();
			if (sapi_->hasTransaction(transaction))
			{
				//std::cout << "************ IN CLEAR TRANSACTION ***************" << std::endl;
				psx::sapi::DpDisconnectRequest* r = new psx::sapi::DpDisconnectRequest(sapi_);
				r->setAck(false); // do not require a SOAP reply
				r->setTransactionId(transaction);
				sapi_->submit(r);
				delete r;
				this->Default(in,out);
			}
			else
			{
				std::string msg = "Failed to clear non existing transaction ";
				msg += transaction;
				XCEPT_RAISE (xgi::exception::Exception, msg);
			}
		}
		else
		{
			std::string msg = "Failed to process unknown action ";
			msg += action;
			XCEPT_RAISE (xgi::exception::Exception, msg);
		}
	}
	catch (psx::sapi::exception::Exception& xe)
        {
                XCEPT_RETHROW (xgi::exception::Exception, "Failed to process PVSS request", xe);
        }
	catch (xgi::exception::Exception& xe)
        {
                XCEPT_RETHROW (xgi::exception::Exception, "CGI processing error", xe);
        }
        catch (xcept::Exception & e)
        {
                XCEPT_RETHROW (xgi::exception::Exception, "Failed to process invalid CGI request", e);
        }
        catch (std::exception e)
        {
		std::string msg = "Failed to interpret incoming CGI content, reason ";
		msg += e.what();
                XCEPT_RAISE (xgi::exception::Exception, msg);
        }
	catch (...)
	{
		XCEPT_RAISE (xgi::exception::Exception, "Failed to proces CGI request");
	}
}

// web interface
void psx::Application::SMITabPage(xgi::Input * in, xgi::Output * out ) 
{
	std::string url = "/";
	url += getApplicationDescriptor()->getURN();
	
	// Show the following status information:
	// DNS node reachable or not
	//	
	xgi::Table t("default");
	t.addColumn ("Description");
	t.addColumn ("URL");
	t.addColumn ("Connection");
		
	std::vector<xgi::Table::Row>::iterator row = t.addRow ("default");
	row->addColumn("DNS");
	row->addColumn(dns_);
	
	if ((mapi_ != 0) && (mapi_->isConnected()))
	{
		row->addColumn("OK");
	}
	else
	{
		row->addColumn("Not OK");
	}
	
	*out << t << std::endl;
	
	// Table of subscriptions with button to delete (unsubscribe)
		
	*out << cgicc::br();	
	
	// Show all the subscriptions
	xgi::Table subscriptionTable ("default");
	subscriptionTable.addColumn ("Transaction");
	subscriptionTable.addColumn ("URL");
	subscriptionTable.addColumn ("Owner");
	subscriptionTable.addColumn ("Action");
	subscriptionTable.addColumn ("Name");
	std::vector<std::string> subscriptions = mapi_->getTransactions();
	
	if (subscriptions.empty())
	{
		std::vector<xgi::Table::Row>::iterator subscriptionTableRow = subscriptionTable.addRow ("default");
		subscriptionTableRow->addColumn( "" );
		subscriptionTableRow->addColumn( "no transactions registered" );
		subscriptionTableRow->addColumn( "" );
		subscriptionTableRow->addColumn( "" );
		subscriptionTableRow->addColumn( "" );
	}
	else
	{
		for (std::vector<std::string>::size_type i = 0; i < subscriptions.size(); ++i)
		{
			std::vector<xgi::Table::Row>::iterator subscriptionTableRow = subscriptionTable.addRow ("default");
			subscriptionTableRow->addColumn( subscriptions[i] );
			subscriptionTableRow->addColumn( mapi_->getTransactionUrl(subscriptions[i]) );
			subscriptionTableRow->addColumn( mapi_->getTransactionOwner(subscriptions[i]) );
			std::string link = "<a id=\"default\" href=\"";
			link += url;
			link += "/actionSMI?action=clearTransaction&transaction=";
			link += subscriptions[i];
			link += "\">Delete</a>";
			subscriptionTableRow->addColumn( link );

			psx::mapi::Monitor* monObject = mapi_->getTransaction( subscriptions[i] );
			std::string name = monObject->getName();
			subscriptionTableRow->addColumn( name );

		}
	}
	*out << subscriptionTable << std::endl;
}

void psx::Application::actionSMI(xgi::Input * in, xgi::Output * out ) 
{	
	try
	{
	 	cgicc::Cgicc cgi(in);           
                std::string action = xgi::Utils::getFormElement(cgi, "action")->getValue();
		
		if (action == "clearTransaction")
		{
			std::string transaction = xgi::Utils::getFormElement(cgi, "transaction")->getValue();
			if (mapi_->hasTransaction(transaction))
			{
				mapi_->clearTransaction(transaction);
				this->Default(in,out);
			}
			else
			{
				std::string msg = "Failed to clear non existing transaction ";
				msg += transaction;
				XCEPT_RAISE (xgi::exception::Exception, msg);
			}
		}
		else
		{
			std::string msg = "Failed to process unknown action ";
			msg += action;
			XCEPT_RAISE (xgi::exception::Exception, msg);
		}
	}
	catch (xgi::exception::Exception& xe)
        {
                XCEPT_RETHROW (xgi::exception::Exception, "CGI processing error", xe);
        }
        catch (xcept::Exception & e)
        {
                XCEPT_RETHROW (xgi::exception::Exception, "Failed to process invalid CGI request", e);
        }
        catch (std::exception e)
        {
		std::string msg = "Failed to interpret incoming CGI content, reason ";
		msg += e.what();
                XCEPT_RAISE (xgi::exception::Exception, msg);
        }
	catch (...)
	{
		XCEPT_RAISE (xgi::exception::Exception, "Failed to proces CGI request");
	}
}


void psx::Application::actionPerformed (xdata::Event& e) 
{
	LOG4CPLUS_INFO(getApplicationLogger(), "all default values have been set, therefore start the services" );
	if ( e.type() == "urn:xdaq-event:setDefaultValues" )
	{
		// Static function to initialize application service before create object 
		psx::sapi::ApplicationService::init(projectName_,projectNumber_,databaseManager_,eventManager_);
		
		sapi_= new psx::sapi::ApplicationService(this);
		sapi_->activate();
		
		// Static function to initialize application service before create object 
		psx::mapi::ApplicationService::init(dns_);
		// mapi service is only created once, just like the HandlerService is only created once
		//
		mapi_= new psx::mapi::ApplicationService(this);
		mapi_->activate();
	}
}

xoap::MessageReference psx::Application::post(const std::string & url, xoap::MessageReference  request) 
{
	LOG4CPLUS_INFO(getApplicationLogger(), "asynchronous notification from application services" );

	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	
	try
	{
		// Currently the addresses are created, but not deleted.
		// Either the messenger has to take care of that or the addresses need to be reference counted
		pt::Address::Reference destAddress = pta->createAddress(url, "soap");
		pt::Address::Reference localAddress =
			pt::getPeerTransportAgent()->createAddress(getApplicationContext()->getContextDescriptor()->getURL(), "soap");
		// These two lines cannot be merged, since a reference that is a temporary object
		// would delete the contained object pointer immediately after use.
		//
		pt::Messenger::Reference mr = pta->getMessenger(destAddress,localAddress);

		pt::SOAPMessenger& m = dynamic_cast<pt::SOAPMessenger&>(*mr);

		
		xoap::MessageReference r = m.send (request);
		return r;
	}
	catch (pt::exception::Exception& pte)
	{	
		LOG4CPLUS_ERROR(getApplicationLogger(), "asynchronous notification error " << pte.what() );

		XCEPT_RETHROW(psx::exception::Exception, "failed to relay message", pte);
	}
	catch(xcept::Exception & e)
	{
		LOG4CPLUS_ERROR(getApplicationLogger(), "asynchronous notification error " << e.what() );
		XCEPT_RETHROW(psx::exception::Exception, "failed to relay message", e);
	}
	catch(std::exception & e)
	{
		LOG4CPLUS_ERROR(getApplicationLogger(), "asynchronous notification error " << e.what() );
		XCEPT_RAISE(psx::exception::Exception,  e.what());
	}
	catch (...)
	{
		LOG4CPLUS_ERROR(getApplicationLogger(), "unknown exception " );
		XCEPT_RAISE(psx::exception::Exception, "failed to relay message, unknown exception");
	}
}

xoap::MessageReference psx::Application::getWinCCOASubscriptions (xoap::MessageReference msg)
{
	if (sapi_ != 0)
	{
		// Create SOAP reply
		xoap::MessageReference reply = xoap::createMessage();
		xoap::SOAPPart soap = reply->getSOAPPart();
		xoap::SOAPEnvelope envelope = soap.getEnvelope();
		xoap::SOAPBody responseBody = envelope.getBody();

		xoap::SOAPName response = envelope.createName("getWinCCOASubscriptionsResponse","psx", psx::NSURI);
		xoap::SOAPElement responseElement = responseBody.addBodyElement(response);

		std::vector<std::string> subscriptions = sapi_->getTransactions();

		for (std::vector<std::string>::size_type i = 0; i < subscriptions.size(); ++i)
		{
			//std::cout <<  "Retrieved dpName: " << (*i).first << " value: " << (*i).second << std::endl;
			xoap::SOAPName subscriptionTag = envelope.createName("subscription","psx", psx::NSURI);
			xoap::SOAPElement subscriptionElement = responseElement.addChildElement(subscriptionTag);
			xoap::SOAPName subscriptionId = envelope.createName("id");
			subscriptionElement.addAttribute(subscriptionId, subscriptions[i] );
			xoap::SOAPName subscriptionURL = envelope.createName("url");
			subscriptionElement.addAttribute(subscriptionURL,  sapi_->getTransactionUrl(subscriptions[i]) );
		}

		return reply;
	}
	else
	{
		XCEPT_RAISE (xoap::exception::Exception, "Service to access PVSS not available");
	}
}

xoap::MessageReference psx::Application::getSMISubscriptions (xoap::MessageReference msg)
{
	if (mapi_ != 0)
	{
		// Create SOAP reply
		xoap::MessageReference reply = xoap::createMessage();
		xoap::SOAPPart soap = reply->getSOAPPart();
		xoap::SOAPEnvelope envelope = soap.getEnvelope();
		xoap::SOAPBody responseBody = envelope.getBody();

		xoap::SOAPName response = envelope.createName("getSMISubscriptionsResponse","psx", psx::NSURI);
		xoap::SOAPElement responseElement = responseBody.addBodyElement(response);

		std::vector<std::string> subscriptions = mapi_->getTransactions();

		for (std::vector<std::string>::size_type i = 0; i < subscriptions.size(); ++i)
		{
			xoap::SOAPName subscriptionTag = envelope.createName("subscription","psx", psx::NSURI);
			xoap::SOAPElement subscriptionElement = responseElement.addChildElement(subscriptionTag);
			xoap::SOAPName subscriptionId = envelope.createName("id");
			subscriptionElement.addAttribute(subscriptionId, subscriptions[i] );
			xoap::SOAPName subscriptionURL = envelope.createName("url");
			subscriptionElement.addAttribute(subscriptionURL, mapi_->getTransactionUrl(subscriptions[i]));
			xoap::SOAPName subscriptionOwner = envelope.createName("owner");
			subscriptionElement.addAttribute(subscriptionOwner, mapi_->getTransactionOwner(subscriptions[i]));
		}

		return reply;
	}
	else
	{
		XCEPT_RAISE (xoap::exception::Exception, "Service to access SMI not available");
	}
}

xoap::MessageReference psx::Application::onWinCCOARequest (xoap::MessageReference msg)
{
	if (sapi_ != 0)
	{
		return sapi_->onRequest(msg);
	}
	else
	{
		XCEPT_RAISE (xoap::exception::Exception, "Service to access PVSS not available");
	}
}

xoap::MessageReference psx::Application::onSMIRequest (xoap::MessageReference msg)  
{
	if (mapi_ != 0)
	{
		return mapi_->onRequest(msg);
	}
	else
	{
		XCEPT_RAISE (xoap::exception::Exception, "Service to access SMI not available");
	}
}
				
