/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * OpenRequest.hpp
 *
 *  Created on: Jan 17, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/WorkRequest.hpp"
#include "mpila/rlist.hpp"

#include <mpi.h>
#include <ostream>

namespace mpila
{

struct PendingRequest
{
    PendingRequest();

    void fromWorkRequest(const WorkRequest& req);

    WorkRequest workRequest;
    MPI_Request mpiRequest;
};

using PendingRequestQueue = rlist<PendingRequest*>;

} /* namespace mpila */
