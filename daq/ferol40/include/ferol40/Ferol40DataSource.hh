#ifndef __Ferol40DataSource
#define __Ferol40DataSource

#include "ferol40/OpticalDataSource.hh"
#include "ferol40/MdioInterface.hh"

namespace ferol40
{
    class Ferol40DataSource : public OpticalDataSource {
    public:
        Ferol40DataSource( HAL::HardwareDeviceInterface *device_P,
                         utils::InfoSpaceHandler &appIS,
                         Logger logger );
	~Ferol40DataSource ();
        void setDataSource() const;
    };
}

#endif /* __Ferol40DataSource */
