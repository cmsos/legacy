#ifndef _tcds_freqmon_FreqCounterReader_h_
#define _tcds_freqmon_FreqCounterReader_h_

#include <stdint.h>
#include <string>

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace freqmon {

    class FreqCounterReader
    {

    public:
      FreqCounterReader(xdaq::Application& xdaqApp,
                        std::string const& hostName,
                        uint32_t const portNumber);
      ~FreqCounterReader();

      uint32_t readValue();

    private:
      void connect() const;
      void disconnect() const;

      void configure() const;

      void sendCommand(std::string const& cmd) const;
      std::string readRawData() const;

      std::string const hostName_;
      uint32_t const portNumber_;
      int mutable socket_;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqCounterReader_h_
