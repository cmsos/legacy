// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _sentinel_spotlightocci_DataBase_h_
#define _sentinel_spotlightocci_DataBase_h_

#include <string>
#include <map>
//#include <sqlite3.h>
//#include <ttclasses/TTInclude.h>
#include <occi.h>

#include "xgi/Method.h"
#include "sentinel/spotlightocci/exception/FailedToStore.h"
#include "sentinel/spotlightocci/exception/FailedToRead.h"
#include "sentinel/spotlightocci/exception/FailedToOpen.h"
#include "sentinel/spotlightocci/exception/FailedToClose.h"
#include "sentinel/spotlightocci/exception/NotFound.h"
#include "sentinel/spotlightocci/exception/FailedToArchive.h"
#include "sentinel/spotlightocci/exception/FailedToRemove.h"
#include "sentinel/spotlightocci/exception/ConstraintViolated.h"
#include "toolbox/Properties.h"
#include "toolbox/TimeVal.h"
#include "toolbox/BSem.h"

namespace sentinel 
{
	namespace spotlightocci 
	{
		class DataBase
		{
			public:

			DataBase( const std::string& username, const std::string& password, const std::string& tnsname, bool readOnly, unsigned int scatterReadNum )
				;
			~DataBase();			

			static void hasInsertedfirstRow(bool b);
															
			/*! Store an exception object
				This function is thread safe (read/write lock)
			*/
			void fire (xcept::Exception& ex)
				;

			std::pair<std::string, std::string> getLastEvent (const std::string& uuid, const std::string& time) ;
				
			void rearm (const std::string & exception, const std::string & source)
				;
	
			void revoke (xcept::Exception& ex)
				;
	
			/*! Store an exception, all values are in the properties object
				The function does not store a chained exception (blob) 
			*/
			void store (toolbox::Properties& properties, const std::string & blob)
				;
				
			void event (const std::string & type, toolbox::Properties& properties)
				;	
			
			/*! Check if database contain the exception for uuid
                        */
			bool hasException (const std::string& uuid) ;

			/*! Retrieve a single exception by uuid in the format it has been stored.
				The name of the format is returned in \param format.
			*/
			void retrieve (const std::string& uuid, const std::string& format, xgi::Output* out) 
				;

			/*! Return a list of stored exceptions in the \param ex set that match the search query 
				This function is thread safe (read lock)*/
			
			void catalog (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format)
				;
			
			/*! Return a list of stored exceptions in the \param ex set that match the search query , order desc by dateTime
			*/	
			void catalog (toolbox::TimeVal & age, int (*callback)(void*,int,char**,char**)  , void * context)
				;
				
			void events (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format)
				;
	
			toolbox::TimeVal lastStoredEvents(xgi::Output* out, toolbox::TimeVal & since, const std::string& format)
				;
	
			void remove(toolbox::TimeVal & age)
					;

			/*! Retrieve the time at which the last exception has been stored from the database */
			toolbox::TimeVal getLatestStoreTime();

			/*! Retrieve the time of the most recent event in the database */
			toolbox::TimeVal getLatestEventTime() ;

			/*! Retrieve the time of the most recent exception in the database */
			toolbox::TimeVal getLatestExceptionTime() ;

			/*! Retrieve the time of the oldest exception in the database */
			toolbox::TimeVal getOldestExceptionTime();

			/*! Retrieve the number of exceptions stored */
			std::string getNumberOfExceptions()  ;

			size_t getSize();
						
			/*! Average time to store a single exception */
			double getAverageTimeToStore();
			
			/*! Average time to retrieve a catalog */
			double getAverageTimeToRetrieveCatalog();
			
			/*! Average time to retrieve a single exception by uuid */
			double getAverageTimeToRetrieveException();
			
			void writeBlob(const std::string & zKey, const std::string & zBlob)
				;
				
			void readBlob(const std::string& zKey, std::string& pzBlob)
				;
			
			void query ( xgi::Output* out, const std::string& query) ;
	
			
			void lock();
			
			void unlock();
			
			// Vacuum the database, other operations TBD
			//
			void maintenance() ;
			
			private:
			bool exists(const std::string& zKey) 
				;


			toolbox::TimeVal outputCatalogJSON(xgi::Output* out, oracle::occi::ResultSet* occiResult)
        			;

			// Use for coldspot query
			toolbox::TimeVal outputCatalogJSONExtended(xgi::Output* out, oracle::occi::ResultSet* occiResult)
        			;

			
			void prepareDatabase (bool readOnly)
				;
					
			std::string escape(const std::string& s);
			
			toolbox::BSem lock_; // used for db management operations

			
			double averageTimeToStore_;
			double averageTimeToRetrieveCatalog_;
			double averageTimeToRetrieveException_;
			toolbox::TimeVal lastExceptionTime_;	
			toolbox::TimeVal maintenanceSinceTime_;
		
			unsigned int scatterReadNum_;	

			oracle::occi::Environment* environment_;
			oracle::occi::Connection* connection_;
			
			oracle::occi::Statement* insertCatalogStmt_;
			oracle::occi::Statement* insertExceptionStmt_;
			oracle::occi::Statement* insertApplicationStmt_;
			oracle::occi::Statement* insertEventStmt_;
			oracle::occi::Statement* updateEventStmt_;
		};
	}
}
#endif
