package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author A. Oh &lt;Alexander.Oh@cern.ch&gt; and I. Suzuki &lt;ichiro@fnal.gov&gt;
 */

public abstract class XDataItem {
	/**
	 * write out EXDR binary sequence to the output stream.
	 * </p>
	 * EXDR (Extended-XDR) is a type-tagged XDR format,
	 * described in <a href="http://xdaqwiki.cern.ch/index.php/EXDR">
	 * a XDAQ wiki page</a>.
	 *
	 * @param out DataOutputStream to write the EXDR binary data
	 */
	public abstract void writeEXDR(DataOutputStream out) throws IOException;

	/**
	 * read in the value from binary input stream.
	 *
	 * @param in DataInputStream to write the EXDR binary data
	 */
	public abstract void readEXDR(DataInputStream in) throws IOException;

	abstract int getEXDRCode();
}

// End of file
// vim: set sw=4 ts=4:
