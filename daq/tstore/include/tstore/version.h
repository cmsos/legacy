// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2016, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tstore_version_h_
#define _tstore_version_h_

#include "config/PackageInfo.h"

#define TSTORE_VERSION_MAJOR 2
#define TSTORE_VERSION_MINOR 1
#define TSTORE_VERSION_PATCH 2
// If any previous versions available E.g. #define TSTORE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define TSTORE_PREVIOUS_VERSIONS "2.1.0,2.1.1"

//
// Template macros
//
#define TSTORE_VERSION_CODE PACKAGE_VERSION_CODE(TSTORE_VERSION_MAJOR,TSTORE_VERSION_MINOR,TSTORE_VERSION_PATCH)
#ifndef TSTORE_PREVIOUS_VERSIONS
#define TSTORE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(TSTORE_VERSION_MAJOR,TSTORE_VERSION_MINOR,TSTORE_VERSION_PATCH)
#else 
#define TSTORE_FULL_VERSION_LIST  TSTORE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TSTORE_VERSION_MAJOR,TSTORE_VERSION_MINOR,TSTORE_VERSION_PATCH)
#endif 

namespace tstore
{
	const std::string package  =  "tstore";
	const std::string versions =  TSTORE_FULL_VERSION_LIST;
	const std::string description = "Table store XDAQ application for use with Oracle RDBMS";
	const std::string summary = "Table store XDAQ application for use with Oracle RDBMS";
	const std::string authors = "Angela Brett";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/TStore";
	
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
