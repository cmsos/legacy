// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2018, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: M. Lettrich, L.Orsini                    			            *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "toolbox/string.h"
#include "pt/mpi/Application.h"
#include "i2o/Method.h"
#include "i2o/utils/AddressMap.h"
#include "pt/PeerTransportAgent.h"
#include "xoap/Method.h"

#include "xdaq/NamespaceURI.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/domutils.h"
#include "xcept/tools.h"

#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "mpila/mpila.hpp"
#include "pt/mpi/PeerTransport.h"
#include "pt/mpi/I2OMessenger.h"

#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL (pt::mpi::Application)
//
//mpirun -np 1 /opt/xdaq/bin/xdaq.exe  -p 40000 -c /nfshome0/lorsini/devel/trunk/daq/pt/mpi/xml/roundtrip.xml : -np 1 /opt/xdaq/bin/xdaq.exe  -p 40001 -c /nfshome0/lorsini/devel/trunk/daq/pt/mpi/xml/roundtrip.xml
//
pt::mpi::Application::Application (xdaq::ApplicationStub* stub)
	: xdaq::Application(stub), xgi::framework::UIManager(this)
{
	stub->getDescriptor()->setAttribute("icon", "/pt/mpi/images/open-mpi-logo.png");
	stub->getDescriptor()->setAttribute("icon16x16", "/pt/mpi/images/open-mpi-logo.png");
	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &pt::mpi::Application::Default, "Default");

	this->ioQueueSize_ = 2048;
	this->pipeLineDepth_ = 16;

	this->maxReceiveBuffers_ = 1024;
	this->maxBlockSize_ = 0x40000; //256kb
	this->protocol_ = MPI_PROTOCOL;
	this->committedPoolSize_ = (maxReceiveBuffers_ * maxBlockSize_) * 1.2; // to allow for the pool threshold
	this->commSize_ = 0;

	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);
	getApplicationInfoSpace()->fireItemAvailable("ioQueueSize", &ioQueueSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxReceiveBuffers", &maxReceiveBuffers_);
	getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);
	getApplicationInfoSpace()->fireItemAvailable("pipeLineDepth", &pipeLineDepth_);
	getApplicationInfoSpace()->fireItemAvailable("commSize", &commSize_);


	getApplicationInfoSpace()->fireItemAvailable("protocol", &protocol_);

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

//
// run control requests current paramater values
//
void pt::mpi::Application::actionPerformed (xdata::Event& e)
{

	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{

		//poolName_ = urn.toString();

		toolbox::mem::Pool * pool = 0;
		try
		{
			toolbox::net::URN urn("toolbox-mem-pool", "mpi");
			toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(committedPoolSize_);
			pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

			pool->setHighThreshold((unsigned long) (committedPoolSize_ * 0.9));
			pool->setLowThreshold((unsigned long) (committedPoolSize_ * 0.5));
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(this->getApplicationLogger(),xcept::stdformat_exception_history(e));
			return;
		}

		int provided;
		MPI_Init_thread(NULL, NULL, MPI_THREAD_SERIALIZED, &provided);
		if (provided < MPI_THREAD_SERIALIZED)
		{
			throw std::runtime_error("MPI implementation does not support MPI_THREAD_SERIALIZED");
		}
		//MPI_Errhandler_set(MPI_COMM_WORLD,MPI_ERRORS_RETURN);
		MPI_Comm_set_errhandler(MPI_COMM_WORLD,MPI_ERRORS_RETURN);

		// MPI parameters
		int commSize;
		MPI_Comm_size(MPI_COMM_WORLD, &commSize);
		commSize_ = commSize;
		pt_ = 0;
		try
		{
			pt_ = new pt::mpi::PeerTransport(this, pool, (xdata::UnsignedIntegerT) commSize_, (xdata::UnsignedIntegerT) maxReceiveBuffers_, (xdata::UnsignedIntegerT) ioQueueSize_, (xdata::UnsignedIntegerT) maxBlockSize_, (xdata::UnsignedIntegerT) pipeLineDepth_, protocol_);

		}
		catch (pt::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
			return;
		}

		try
		{
			pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
			pta->addPeerTransport(pt_);
		}
		catch(...)
		{
			XCEPT_RAISE(xdaq::exception::Exception, "Cannot add peer transport for MPI");
		}
		//auto connect timer

		toolbox::task::Timer * timer = 0;

		if (!toolbox::task::getTimerFactory()->hasTimer("urn:pt::mpi-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:pt::mpi-timer");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:pt::mpi-timer");
		}


		toolbox::TimeInterval interval;
		interval.fromString("PT5S");

		toolbox::TimeVal start;

		timer->scheduleAtFixedRate(start, this, interval, 0, "urn:mpi-watchdog:stats");
	}
}

void pt::mpi::Application::Default (xgi::Input * in, xgi::Output * out)
{
	// Begin of tabs
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tab 1
	*out << "<div class=\"xdaq-tab\" title=\"Events\">" << std::endl;
	this->EventsTabPage(out);
	*out << "</div>";

	// Tab 2
	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
	this->SettingsTabPage(out);
	*out << "</div>";


	// Tab 3
	*out << "<div class=\"xdaq-tab\" title=\"Workloops\">" << std::endl;
	this->WorkloopsTabPage(out);
	*out << "</div>";

	*out << "</div>"; // end of tab pane
}


void pt::mpi::Application::SettingsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Memory Pool");
	*out << cgicc::td(committedPoolSize_.toString());
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Maximum Clients Per Address");
	*out << cgicc::td();
	*out << commSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("I/O Queue");
	*out << cgicc::td();
	*out << ioQueueSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Event Queue");
	*out << cgicc::td();
	*out << pipeLineDepth_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Receive Buffers");
	*out << cgicc::td();
	*out << maxReceiveBuffers_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Default Receive Buffer Block Size");
	*out << cgicc::td();
	*out << maxBlockSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;


	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table();
}

void pt::mpi::Application::EventsTabPage (xgi::Output * out)
{
	*out << (*pt_) << std::endl;
}

void pt::mpi::Application::WorkloopsTabPage (xgi::Output * out)
{
	std::list<toolbox::task::WorkLoop *> workloops = toolbox::task::getWorkLoopFactory()->getWorkLoops();

	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << "<caption>pt::mpi workloops</caption>";

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th("Name");
	*out << cgicc::th("Type");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;
	for (std::list<toolbox::task::WorkLoop *>::iterator i = workloops.begin(); i != workloops.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td((*i)->getName());
		*out << cgicc::td((*i)->getType());
		*out << cgicc::tr();
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table();
}

void pt::mpi::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	// collect statistics
}

