package xdaq.tools.win;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.filechooser.FileFilter;
import java.util.*;
import java.lang.*;
import java.io.*;

import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.FileSelectionListener;
import xdaq.tools.win.SuffixFilter;

public class OpenFileAction extends AbstractAction
{
	JFileChooser fc_;
	FileSelectionListener l_;
	JFrame frame_;
	
	public OpenFileAction (JFrame frame, FileSelectionListener l, String fileSuffix, String fileTitle)
	{
		super ("Open");
		frame_ = frame;
		l_ = l;
		String startDir = System.getProperty("user.dir");
		fc_ = new JFileChooser (new File (startDir));
		fc_.setFileFilter (new SuffixFilter (fileSuffix, fileTitle));
	}
	
	public void actionPerformed (ActionEvent e)
	{	
		int retVal = fc_.showOpenDialog (frame_);
		if (retVal == JFileChooser.APPROVE_OPTION) 
		{
			File dir = fc_.getCurrentDirectory ();
			File fname = fc_.getSelectedFile();
			if ((fname != null) && (!fname.isDirectory() )) 
			{
				l_.fileSelected (fname);
				
			}
		}
	}
	
	
	

}
