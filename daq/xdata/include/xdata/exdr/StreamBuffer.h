// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_exdr_StreamBuffer_h_
#define _xdata_exdr_StreamBuffer_h_

#include <rpc/types.h>
#include <rpc/xdr.h>

#include "xdata/exception/Exception.h"

namespace xdata 
{
	namespace exdr 
	{
		class StreamBuffer 
		{
			public:

			virtual ~StreamBuffer() {}
			
			//! Integer 8 bit
			void encodeInt8(const  int8_t & i) ;
			void decodeInt8( int8_t & i) ;

			//! Integer 16 bit
			void encodeInt16(const  int16_t & i) ;
			void decodeInt16( int16_t & i) ;

			//! Integer 32 bit
			void encodeInt32(const  int32_t & i) ;
			void decodeInt32( int32_t & i) ;

			//! Integer 64 bit
			void encodeInt64(const  int64_t & i) ;
			void decodeInt64( int64_t & i) ;

			//! Integer unsigned 8 bit
			void encodeUInt8(const  uint8_t & i) ;
			void decodeUInt8( uint8_t & i) ;

			//! Integer unsigned 16 bit
			void encodeUInt16(const  uint16_t & i) ;
			void decodeUInt16( uint16_t & i) ;

			//! Integer unsigned 32 bit
			void encodeUInt32(const  uint32_t & i) ;
			void decodeUInt32( uint32_t & i) ;

			//! Integer unsigned 64 bit
			void encodeUInt64(const  uint64_t & i) ;
			void decodeUInt64( uint64_t & i) ;

			//! Float
			void encode(const float & i) ;
			void decode(float & i) ;

			//! Double
			void encode(const double & i) ;
			void decode(double  & i) ;
			
			//! Boolean
			void encode(const bool & i) ;
			void decode(bool & i) ;

			//! String
			void decode(std::string & s) ;
			void encode(const std::string & s) ;

			//! Opaque - character array
			void decode(char* c, unsigned int len) ;
			void encode(char* c, unsigned int len) ;

			//! Return current position of stream pointer
			virtual u_int tellp();

			protected:

			virtual void overflow()  = 0;

			XDR xdr_;
		};
	}
}
#endif
