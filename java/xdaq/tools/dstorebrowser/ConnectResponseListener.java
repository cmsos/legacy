package xdaq.tools.dstorebrowser;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.event.*;

import javax.xml.parsers.*;

import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;

import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPException;
import xdaq.tools.win.xdaqWinException;


import xdaq.tools.dstorebrowser.dstorebrowser;

public class ConnectResponseListener implements ReqRespListener
{
    dstorebrowser browser_;

    public ConnectResponseListener (dstorebrowser browser)
	{
	    browser_ = browser;
	}

    public SOAPMessage onMessage (SOAPMessage msg)
	{
	    try {
		browser_.displayConfiguration(msg);
		xdaqSOAPMessage retVal = new xdaqSOAPMessage ("receiveDataStoreDataResponse", "0");
		return retVal.getSOAPMessage();
 	    } 
//		catch (SOAPException e)
// 	    {
// 		System.out.println (e.toString());
// 	    }
	    catch (xdaqSOAPException ex)
	    {
		System.out.println (ex.toString());
	    }
	    catch (xdaqWinException ex)
	    {
		System.out.println (ex.toString());
	    }
// 	    catch (java.io.IOException ey)
// 	    {
// 		System.out.println (ey.toString());
// 	    }
	    return null;
	}
};
