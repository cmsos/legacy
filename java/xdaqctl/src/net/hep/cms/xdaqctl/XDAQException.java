/*
 * XDAQException
 *
 * $Id: XDAQException.java,v 4.1 2007/03/26 14:40:24 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

/**
 * Exception caused by the XDAQ control library
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class XDAQException extends Exception
{
	public XDAQException() {}
	public XDAQException(String message) { super(message); }
	public XDAQException(Throwable cause) { super(cause); }
	public XDAQException(String message, Throwable cause) {
		super(message, cause);
	}
}

// End of file
// vim: set ts=4 sw=4:
