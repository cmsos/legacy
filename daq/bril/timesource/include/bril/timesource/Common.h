#ifndef _bril_timesource_Common_h_
#define _bril_timesource_Common_h_

#include "toolbox/TimeInterval.h"
namespace bril{ namespace timesource{
    // 1 nibble 25e-9*3564*4096=.365sec
    static const unsigned int s_nbmillisec = 365;// 1 nibble duration in milli sec
    static const unsigned int s_nbperls = 64; // 64 nibbles = 1 lumi section
    static const toolbox::TimeInterval s_nbduration(0,s_nbmillisec*1000);
    static const toolbox::TimeInterval s_lsduration((s_nbduration)*double(s_nbperls));

  }}
#endif
