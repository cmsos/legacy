// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xmas_store2g_Stager_h_
#define _xmas_store2g_Stager_h_

#include <string>
#include <map>

#include "toolbox/task/TimerListener.h"
#include "xdaq/ApplicationDescriptorImpl.h" 
#include "xdaq/Object.h" 
#include "xmas/store2g/Repository.h"
#include "xmas/store2g/Settings.h"

#include "xmas/store2g/TStoreProxy.h"
#include "xmas/store2g/exception/Exception.h"
#include "toolbox/TimeInterval.h"

namespace xmas 
{
	namespace store2g 
	{
		class Stager : xdaq::Object, public toolbox::task::TimerListener
		{
			public:
			
			Stager(xdaq::Application * owner, xmas::store2g::Repository * repository, xmas::store2g::TStoreProxy * proxy,
				xmas::store2g::Settings *storeSettings );
			
			void timeExpired(toolbox::task::TimerEvent& e);
			void enable(toolbox::TimeInterval& e, const std::string& storeGroup,const std::string& storeView, const std::string& database) 
				;
			void disable() 
				;

			private:
			
			xmas::store2g::Repository * repository_;
			xmas::store2g::TStoreProxy * proxy_;
			std::string storeGroup_;
			std::string storeView_;
			xmas::store2g::Settings *storeSettings_;
			std::string database_;

		};
	}
}

#endif

