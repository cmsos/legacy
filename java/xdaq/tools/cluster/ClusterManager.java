package xdaq.tools.cluster;

import javax.swing.*;
import java.awt.*;
import javax.swing.table.*;
import java.awt.event.*;
import java.util.*;
import java.net.*;
import java.io.*;

import xdaq.tools.cluster.ClusterTable;
import xdaq.tools.cluster.ClusterLogger;
import xdaq.tools.cluster.CommandHistoryFile;
import xdaq.tools.cluster.Control;
import xdaq.tools.cluster.Identification;

public class ClusterManager extends JFrame
{
	private Identification identificationPanel_;

	public ClusterManager()
	{
		this.setSize(600,400);	
		
		identificationPanel_ = new Identification();
		final Control controlPanel_ = new Control(identificationPanel_);
		
		
		
		JTabbedPane tabbedPane = new JTabbedPane();
		tabbedPane.addTab ("Preferences", identificationPanel_);
		tabbedPane.addTab ("Control",controlPanel_);
		
		Container contentPane = getContentPane();
		contentPane.add (  tabbedPane, "Center" );
		
		this.addWindowListener ( new WindowAdapter() {
			public void windowClosing ( WindowEvent e ) {
				identificationPanel_.close();
				System.exit(0);
			}
		} );
	}
	
}




