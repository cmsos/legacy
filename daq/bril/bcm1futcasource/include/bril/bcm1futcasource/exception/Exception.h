#ifndef _bril_bcm1futcasource_exception_Exception_h_
#define _bril_bcm1futcasource_exception_Exception_h_
#include "xcept/Exception.h"

namespace bril 
{ 
  namespace bcm1futcasource 
  {
    namespace exception 
    {       
      class Exception: public xcept::Exception
      {
      public: 

        Exception( std::string name, std::string message, std::string module, int line, std::string function ):xcept::Exception(name, message, module, line, function) {} 
        
        Exception( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e ):xcept::Exception(name, message, module, line, function, e) {} 
      };
    }
  }
}

namespace bril {
  //
  XCEPT_DEFINE_EXCEPTION(bcm1futcasource,ubcmException)
}
#endif
