// $Id$
#ifndef _bril_bcm1futcasource_version_h_
#define _bril_bcm1futcasource_version_h_
#include "config/PackageInfo.h"
#define BRILBCM1FUTCASOURCE_VERSION_MAJOR 1
#define BRILBCM1FUTCASOURCE_VERSION_MINOR 6
#define BRILBCM1FUTCASOURCE_VERSION_PATCH 3
#define BRILBCM1FUTCASOURCE_PREVIOUS_VERSIONS

// Template macros
//
#define BRILBCM1FUTCASOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRILBCM1FUTCASOURCE_VERSION_MAJOR,BRILBCM1FUTCASOURCE_VERSION_MINOR,BRILBCM1FUTCASOURCE_VERSION_PATCH)
#ifndef BRILBCM1FUTCASOURCE_PREVIOUS_VERSIONS
#define BRILBCM1FUTCASOURCE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBCM1FUTCASOURCE_VERSION_MAJOR,BRILBCM1FUTCASOURCE_VERSION_MINOR,BRILBCM1FUTCASOURCE_VERSION_PATCH)
#else
#define BRILBCM1FUTCASOURCE_FULL_VERSION_LIST  BRILBCM1FUTCASOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBCM1FUTCASOURCE_VERSION_MAJOR,BRILBCM1FUTCASOURCE_VERSION_MINOR,BRILBCM1FUTCASOURCE_VERSION_PATCH)
#endif
namespace brilbcm1futcasource{
  const std::string package = "brilbcm1futcasource";
  const std::string versions = BRILBCM1FUTCASOURCE_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bcm1futcasource";
  const std::string description = "collect histogram and orbit raw data";
  const std::string authors = "Spandan";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
