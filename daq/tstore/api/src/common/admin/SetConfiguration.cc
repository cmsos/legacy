// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "tstore/api/admin/SetConfiguration.h"
#include "tstore/api/NS.h"
#include "xoap/MessageFactory.h"

tstore::api::admin::SetConfiguration::SetConfiguration( const std::string & viewId, const std::string & viewType,  const std::string & xpath):
	viewId_(viewId),viewType_(viewType), xpath_(xpath)
{
	aheaders_.setAction(tstore::api::NamespaceUri + "/setConfiguration");
}

tstore::api::admin::SetConfiguration::~SetConfiguration()
{
}
	
tstore::api::admin::SetConfiguration::SetConfiguration(xoap::MessageReference& msg) 
	
{
	
	aheaders_.fromSOAP(msg);

	xoap::SOAPPart part = msg->getSOAPPart();
	xoap::SOAPEnvelope envelope = part.getEnvelope();
	xoap::SOAPBody body = envelope.getBody();
	//DOMNode* node = body.getDOMNode();
	//DOMNodeList* bodyList = node->getChildNodes();
	xoap::SOAPHeader header = envelope.getHeader();
	std::vector<xoap::SOAPHeaderElement> elements = header.examineHeaderElements ();

	// Extract header fields to determine the subscription
	std::vector<xoap::SOAPHeaderElement>::iterator i;
	for (i = elements.begin(); i != elements.end(); ++i)
	{
	
	/*
		std::string name = (*i).getElementName().getLocalName();
		std::string value = toolbox::trim((*i).getValue());
		if (name == "Identifier")
		{
			if (value.find("uuid:") == std::string::npos)
			{
				std::stringstream msg;
				msg << "Identifier '" << value << "' not tagged with uuid: keyword" << std::endl;
				XCEPT_RAISE (ws::eventing::exception::Exception, msg.str());
			}

			try
			{
				ws::eventing::Identifier id(value.substr(5));
				identifier_ = id;
			}
			catch (toolbox::net::exception::Exception& e)
			{
				XCEPT_RETHROW(ws::eventing::exception::Exception, "Failed to interpret identifier", e);
			}
		}
	*/	
		
	}

	// parse message body and fill Renew fields
	xoap::SOAPName setConfigurationName("setConfiguration", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	std::vector<xoap::SOAPElement> bodyElements = body.getChildElements(setConfigurationName);

	if (bodyElements.size() != 1)
	{
		XCEPT_RAISE (tstore::api::admin::exception::Exception, "Invalid message: missing or bad <setConfiguration/> element" );
	}

	#warning "Add check if two attributes exists"
	xoap::SOAPName viewId = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName xpath = envelope.createName("path", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	xoap::SOAPName viewType = envelope.createName("viewtype", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	viewId_ = bodyElements[0].getAttributeValue(viewId);
	xpath_ = bodyElements[0].getAttributeValue(xpath);
	viewType_ = bodyElements[0].getAttributeValue(viewType);
	
	
	// import configuration from SOAP
	DOMDocument * document = configuration_.getDocument();
	DOMElement * element = document->getDocumentElement();
	DOMNodeList* bodyList = bodyElements[0].getDOM()->getChildNodes();
	for (unsigned int itemIndex = 0; itemIndex < bodyList->getLength(); itemIndex++) 
	{
		element->appendChild(document->importNode(bodyList->item(itemIndex),true));
	}
}


std::string tstore::api::admin::SetConfiguration::getViewId()
{
	return viewId_;
}

std::string tstore::api::admin::SetConfiguration::getXPath()
{
	return xpath_;
}

std::string tstore::api::admin::SetConfiguration::getViewType()
{
	return viewType_;
}

xoap::MessageReference tstore::api::admin::SetConfiguration::toSOAP()
{
	xoap::MessageReference request = xoap::createMessage();
	xoap::SOAPPart soap = request->getSOAPPart();
	xoap::SOAPEnvelope envelope = soap.getEnvelope();
	xoap::SOAPBody responseBody = envelope.getBody();

	aheaders_.toSOAP(request);

	xoap::SOAPName command = envelope.createName
		("setConfiguration",tstore::api::NamespacePrefix, tstore::api::NamespaceUri);

	xoap::SOAPElement element = responseBody.addBodyElement(command);
	element.addNamespaceDeclaration(tstore::api::admin::NamespacePrefix, tstore::api::admin::NamespaceUri);
	element.addNamespaceDeclaration(tstore::api::sql::NamespacePrefix, tstore::api::sql::NamespaceUri);
	element.addNamespaceDeclaration(tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	
	xoap::SOAPName viewId = envelope.createName("id", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(viewId, viewId_);
	xoap::SOAPName xpath = envelope.createName("path", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(xpath, xpath_);
	xoap::SOAPName viewType = envelope.createName("viewtype", tstore::api::NamespacePrefix, tstore::api::NamespaceUri);
	element.addAttribute(viewType, viewType_);
	
	configuration_.toSOAP(element, envelope);
	
	return request;
}

tstore::api::admin::Configuration &  tstore::api::admin::SetConfiguration::setConfiguration(tstore::api::admin::Configuration & configuration)
{

	configuration_ .import( dynamic_cast<tstore::api::admin::ConfigurationImpl&>(configuration) );
	return 	configuration_;

}

tstore::api::admin::Configuration &  tstore::api::admin::SetConfiguration::getConfiguration()
{

	return 	configuration_;

}	
	
	
