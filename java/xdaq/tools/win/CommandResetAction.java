package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import xdaq.tools.win.xdaqWin;

public class CommandResetAction extends AbstractAction
{
	xdaqWin win_;

	public CommandResetAction (xdaqWin win)
	{
		super ("Reset");
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		try {
			win_.executeCommandOnSelection("Reset", null, win_.getPaths());
		}
		catch(xdaqWinException ex )
		{
		
		}

	}
}
