/**
*      @file FMMTimer.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.6 $
*     $Date: 2007/03/27 07:53:28 $
*
*
**/
#include "tts/fmm/FMMTimer.hh"

#include "sys/time.h"


uint64_t tts::FMMTimer::getMicroTime() {

  struct timeval tv;

  gettimeofday( &tv, 0 );
  return (uint64_t) tv.tv_sec * 1000000 + (uint64_t) tv.tv_usec;

}
