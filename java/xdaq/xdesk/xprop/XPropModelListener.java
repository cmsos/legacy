package xdaq.xdesk.xprop;


import java.util.EventListener;

public interface XPropModelListener extends EventListener {
	public void xpropChanged(XPropModelEvent event);
}
