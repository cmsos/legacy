#ifndef _bril_lumistore_hdf5_Node_h
#define _bril_lumistore_hdf5_Node_h
#include "hdf5.h"
#include <string>
namespace bril{ namespace lumistore{ namespace hdf5{
      class File;      
      /**
	 Abastract base class 
       */
      class Node{
      public:
	Node(const std::string& parentpath, hid_t parentid, const std::string& name,File* file);
	virtual ~Node();
	virtual void close() = 0;
	hid_t id() const;
	bool isopen() const;
	std::string pathname() const;
	std::string parentpath() const;
	File* file();
      protected:
	std::string m_parentpath;
	hid_t m_parentid;
	std::string m_name;
	std::string m_pathname;
	File* m_file;
	hid_t m_id;
	bool m_isopen;       	
      };
    }}}

#endif
