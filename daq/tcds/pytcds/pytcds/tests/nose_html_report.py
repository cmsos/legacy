###############################################################################
## A Python nose plugin to produce a report in HTML format. Based on
## the example in:
##   http://python-nose.googlecode.com/svn/trunk/examples/html_plugin/htmlplug.py
## and on inspiration found in several places on the interwebs.
###############################################################################

import datetime
import nose
import os
import platform
import pwd
import rpm
import traceback

###############################################################################

TEST_STATUS_PASSED = 0
TEST_STATUS_FAILED = 1
TEST_STATUS_ERROR = 2
TEST_STATUS_SKIPPED = 3

###############################################################################

class HtmlOutput(nose.plugins.Plugin):

    name = "html-output"

    # Run before the skip plugin in order to catch skipped tests.
    score = nose.plugins.skip.Skip.score + 50

    def __init__(self):
        super(HtmlOutput, self).__init__()
        self.time_begin = None
        self.num_passed = 0
        self.num_failed = 0
        self.num_error = 0
        self.num_skipped = 0
        self.test_results = []
        # End of __init__().

    def begin(self):
        self.time_begin = datetime.datetime.now()
        # End of begin().

    def finalize(self, result):
        self.time_end = datetime.datetime.now()
        output_lines = []

        # Add overall system information.
        output_lines.append("System information:")
        user_name = pwd.getpwuid(os.getuid()).pw_name
        dist_info = platform.linux_distribution()
        processor = platform.processor()
        output_lines.append("  User: {0:s}".format(user_name))
        output_lines.append("  Host: {0:s}".format(platform.node()))
        output_lines.append("  OS: {0:s} release {1:s} ({2:s})". \
                          format(*dist_info))
        output_lines.append("  Architecture: {0:s}".format(processor))

        # Add information on which XDAQ and TCDS RPMS are
        # installed. This basically does the same as 'rpm -qa'.
        transaction_set = rpm.TransactionSet()
        matches = transaction_set.dbMatch()
        tmp_rpm_info = [i for i in matches if i["name"].startswith("daq-")]
        tcds_rpms = [i for i in tmp_rpm_info if (i["name"].find("tcds") > -1)]
        # xdaq_rpms = [i for i in tmp_rpm_info if (not i in tcds_rpms)]
        tmp = [i["release"].split(".") for i in tmp_rpm_info]
        xdaq_releases = sorted(list(set([i[1] for i in tmp])))
        xdaq_platforms = sorted(list(set([i[2] for i in tmp])))
        xdaq_compilers = sorted(list(set([i[3] for i in tmp])))
        xdaq_archs = sorted(list(set([i["arch"] for i in tmp_rpm_info])))
        tcds_rpms.sort()
        output_lines.append("Found installed XDAQ RPMs for:")
        output_lines.append("  Releases: {0:s}".format(", ".join(xdaq_releases)))
        output_lines.append("  Platforms: {0:s}".format(", ".join(xdaq_platforms)))
        output_lines.append("  Compilers: {0:s}".format(", ".join(xdaq_compilers)))
        output_lines.append("  Architectures: {0:s}".format(", ".join(xdaq_archs)))
        output_lines.append("Installed TCDS RPMs:")
        output_lines.extend(["  {0:s}".format(i["name"]) for i in tcds_rpms])

        ##########

        output_lines.append("Tests summary:")

        # Add some information on when and for how long the tests ran.
        # NOTE: Strip off the microseconds. They're useless and distracting here.
        duration = self.time_end - self.time_begin
        self.time_begin = self.time_begin.replace(microsecond=0)
        self.time_end = self.time_end.replace(microsecond=0)
        output_lines.append("  Tests started: {0:s}".format(self.time_begin.isoformat()))
        output_lines.append("  Tests finished: {0:s}".format(self.time_end.isoformat()))
        output_lines.append("    --> Duration: {0:s}".format(duration))

        # Add global (i.e., count) info on the test results.
        num_total = self.num_passed + self.num_failed + self.num_error + self.num_skipped
        output_lines.append("  Ran a total of {0:d} tests".format(num_total))
        output_lines.append("    {0:d} tests passed".format(self.num_passed))
        output_lines.append("    {0:d} tests failed".format(self.num_failed))
        output_lines.append("    {0:d} tests had errors while running".format(self.num_error))
        output_lines.append("    {0:d} tests were skipped".format(self.num_skipped))

        ##########

        # Now actually add the test results.
        # for (test_status, test) in self.test_results:
        #     # import pdb
        #     # pdb.set_trace()
        #     output_lines.append(test)

        # Finally dump everything to our output stream.
        for line in output_lines:
            self.stream.writeln(line)
        # End of finalize().

    def addSuccess(self, test):
        self.num_success += 1
        self.tests_results.append((TEST_STATUS_PASSED, test))
        # End of addSuccess().

    def addFailure(self, test, err):
        self.num_failed += 1
        self.tests_results.append((TEST_STATUS_FAILED, test))
        # End of addFailure().

    def addError(self, test, err):
        test_status = None
        if err[0] == nose.SkipTest:
            self.num_skipped += 1
            test_status = TEST_STATUS_SKIPPED
        else:
            self.num_error += 1
            test_status = TEST_STATUS_ERROR
        self.test_results.append((test_status, test))
        # End of addError().

    def formatErr(self, err):
        (exc_type, exc_value, exc_traceback) = err
        return "".join(traceback.format_exception(exc_type, exc_value, exc_traceback))

    def setOutputStream(self, stream):
        # Grab the output stream for our own use (later, in
        # finalize()).
        self.stream = stream
        # Return a dummy stream to keep the test runner happy.
        class dummy:
            def flush(self, *arg):
                pass
            def write(self, *arg):
                pass
            def writeln(self, *arg):
                pass
        # End of setOutputStream().
        #return dummy()
        return stream

    # End of class HtmlOutput.

###############################################################################
