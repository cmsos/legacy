/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "b2in/eventing/Statistics.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/Runtime.h"

#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/ItemGroupEvent.h"
#include "xdata/TableIterator.h"

#include "xcept/tools.h"

#include <iomanip>

b2in::eventing::Statistics::Statistics(xdaq::Application* app, const std::string & endpointURL) :
	mutex_(toolbox::BSem::FULL)
{
	app_ = app;

	const xdaq::ApplicationDescriptor* descriptor = app->getApplicationDescriptor();

	messagecount_ = 0;
	totalmessagecount_ = 0;
	datacount_ = 0;
	lostmessagecount_ = 0;
	totallostmessagecount_ = 0;
	lostdatacount_ = 0;
	brokenConnections_ = 0;
	queueSize_ = 0;

	// snapshots of the last servicetime got from the peer transport
	servicetimeincoming_ = 0;
	servicetimeoutgoing_ = 0;

	// TODO configureable?
	xdata::UnsignedInteger32 count=0;
	size_t maxcount=40;
	messages_.resize(maxcount);
	for(size_t i=0;i<maxcount;i++)
	{
		messages_[i] = count;
	}

	xdata::Vector<xdata::UnsignedInteger32> *messages = new xdata::Vector<xdata::UnsignedInteger32>();
	*messages = messages_;

	components_.addColumn("name",        "string");
	components_.addColumn("servicetime", "double");
	components_.addColumn("load",        "double");
	
	this->addComponent("eventing");
	this->addComponent("ptincoming");
	this->addComponent("ptoutgoing");

	xdata::Table *components = new xdata::Table();
	*components = components_;

	// Create/Retrieve an infospace
	toolbox::net::URN monitorable = app_->createQualifiedInfoSpace("eventing-statistics");
	is_ = xdata::getInfoSpaceFactory()->get(monitorable.toString());

	is_->fireItemAvailable("url",              new xdata::String(endpointURL));
	is_->fireItemAvailable("group",            new xdata::String(descriptor->getAttribute("group")));

	// general service properties  
	is_->fireItemAvailable("load",             new xdata::Double(0));
	is_->fireItemAvailable("components",       components);
	is_->fireItemAvailable("systemtime",       new xdata::Double(0));

	// add statistics table (Network)
	is_->fireItemAvailable("messagecount",     new xdata::UnsignedInteger32(0));
	is_->fireItemAvailable("lostmessagecount", new xdata::UnsignedInteger32(0));
	is_->fireItemAvailable("datacount",        new xdata::UnsignedInteger32(0));
	is_->fireItemAvailable("lostdatacount",    new xdata::UnsignedInteger32(0));
	is_->fireItemAvailable("brokenconnections",    new xdata::UnsignedInteger32(0));
	is_->fireItemAvailable("messagesizehisto", messages);
	is_->fireItemAvailable("clients",          new xdata::UnsignedInteger32(0));

	// add statistics table (CPU)
	is_->fireItemAvailable("cpuusage",         new xdata::Double(0));
	// add statistics table (Memory)
	is_->fireItemAvailable("rsize",            new xdata::UnsignedInteger32(0));
	is_->fireItemAvailable("vsize",            new xdata::UnsignedInteger32(0));

	// 1 second histograms
	is_->fireItemAvailable("messagecounthisto",new xdata::Vector<xdata::UnsignedInteger32>());
	is_->fireItemAvailable("lostmessagecounthisto",new xdata::Vector<xdata::UnsignedInteger32>());
	is_->fireItemAvailable("datacounthisto",   new xdata::Vector<xdata::UnsignedInteger32>());
	is_->fireItemAvailable("lostdatacounthisto",new xdata::Vector<xdata::UnsignedInteger32>());

	starttime_ = toolbox::TimeVal::gettimeofday();

	// watch own PID (CPU usage, memory consumption)
	pid_t pid = toolbox::getRuntime()->getPid();
	processinfo_ = toolbox::getRuntime()->getProcessInfo(pid);
	processinfo_->sample();

	// Timer for 1 second histograms
	toolbox::task::Timer * timer = toolbox::task::getTimerFactory()->createTimer("HistoTimer");
	toolbox::TimeInterval histoInterval(1,0); // period of 1 second for histogram updates
	toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
	timer->scheduleAtFixedRate( start, this, histoInterval,  0, "histo" );
	
	is_->addGroupRetrieveListener (this);
}

b2in::eventing::Statistics::~Statistics()
{
}

/*
void b2in::eventing::Statistics::incQueueSize()
{
	mutex_.take();
	queueSize_ = queueSize_ + 1;
	mutex_.give();
}

void b2in::eventing::Statistics::decQueueSize()
{
	mutex_.take();
	queueSize_ = queueSize_ - 1;
	mutex_.give();
}

xdata::UnsignedInteger32 b2in::eventing::Statistics::getQueueSize()
{
	return queueSize_;
}
*/

void b2in::eventing::Statistics::safeUpdateStats()
{
	mutex_.take();
	this->updateStats();
	mutex_.give();
}

void b2in::eventing::Statistics::updateStats()
{
	// sample
	processinfo_->sample();
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

	// Get PeerTransport performance information
	xdata::Double servicetimeincoming = 0;
	xdata::Double servicetimeoutgoing = 0;

  	xdata::getInfoSpaceFactory()->lock();

	std::map<std::string, xdata::Serializable *, std::less<std::string> > spaces = xdata::getInfoSpaceFactory()->match("pt-tcp-receiver");
	for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
	{
		xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);
	        xdata::Double* receiveTime = dynamic_cast<xdata::Double*>((is)->find("receiveTime"));
                servicetimeincoming = *receiveTime + servicetimeincoming;

		
	}

        //std::vector<xdata::InfoSpace*>  ir =  xdata::InfoSpace::search("pt-tcp-receiver");
        //std::vector<xdata::InfoSpace*>  is =  xdata::InfoSpace::search("pt-tcp-sender");

	// std::vector<xdata::InfoSpace*>::iterator j;
        // for ( j = ir.begin(); j != ir.end(); j++ )
        // {
        //        xdata::Double* receiveTime = dynamic_cast<xdata::Double*>((*j)->find("receiveTime"));
	//		servicetimeincoming = *receiveTime + servicetimeincoming;
	// }

	spaces = xdata::getInfoSpaceFactory()->match("pt-tcp-sender");
        for (  std::map<std::string, xdata::Serializable *, std::less<std::string> >::iterator i = spaces.begin(); i != spaces.end(); ++i)
        {
		xdata::InfoSpace* is = dynamic_cast<xdata::InfoSpace *> ((*i).second);
		xdata::Double* sendTime = dynamic_cast<xdata::Double*>((is)->find("sendTime"));
		servicetimeoutgoing = *sendTime + servicetimeoutgoing;
	}
	// std::vector<xdata::InfoSpace*>::iterator k;
        // for ( k = is.begin(); k != is.end(); k++ )
        // {
        //        xdata::Double* sendTime = dynamic_cast<xdata::Double*>((*k)->find("sendTime"));
	//		servicetimeoutgoing = *sendTime + servicetimeoutgoing;
	//	}

	 xdata::getInfoSpaceFactory()->unlock();

	addServiceTime("ptincoming", servicetimeincoming-servicetimeincoming_);
	addServiceTime("ptoutgoing", servicetimeoutgoing-servicetimeoutgoing_);

	// get access to all veriables in the flashlist
	xdata::UnsignedInteger32 *messagecount = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("messagecount"));
	xdata::UnsignedInteger32 *lostmessagecount = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("lostmessagecount"));
	xdata::UnsignedInteger32 *datacount = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("datacount"));
	xdata::UnsignedInteger32 *lostdatacount = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("lostdatacount"));
	xdata::Vector<xdata::UnsignedInteger32> *messages = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(is_->find("messagesizehisto"));
	xdata::Double *load = dynamic_cast<xdata::Double*>(is_->find("load"));
	xdata::Table *components = dynamic_cast<xdata::Table*>(is_->find("components"));
	xdata::UnsignedInteger32 *connectedclients = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("clients"));
	xdata::Double *cpuusage = dynamic_cast<xdata::Double*>(is_->find("cpuusage"));
	xdata::UnsignedInteger32 *rsize = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("rsize"));
	xdata::UnsignedInteger32 *vsize = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("vsize"));
	xdata::Double *systemtime = dynamic_cast<xdata::Double*>(is_->find("systemtime"));

	xdata::Vector<xdata::UnsignedInteger32> *messagecounthisto = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(is_->find("messagecounthisto"));
	xdata::Vector<xdata::UnsignedInteger32> *lostmessagecounthisto = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(is_->find("lostmessagecounthisto"));
	xdata::Vector<xdata::UnsignedInteger32> *datacounthisto = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(is_->find("datacounthisto"));
	xdata::Vector<xdata::UnsignedInteger32> *lostdatacounthisto = dynamic_cast<xdata::Vector<xdata::UnsignedInteger32>*>(is_->find("lostdatacounthisto"));

	xdata::UnsignedInteger32 tmpcount;

	tmpcount = 0;
	for(size_t i=0 ; i < messagecounthisto_.elements() ; i++)
	{
		tmpcount = tmpcount + messagecounthisto_[i];
	}
	*messagecount = tmpcount;
	*messagecounthisto = messagecounthisto_;

	tmpcount = 0;
	for(size_t i=0 ; i < lostmessagecounthisto_.elements() ; i++)
	{
		tmpcount = tmpcount + lostmessagecounthisto_[i];
	}
	*lostmessagecount = tmpcount;
	*lostmessagecounthisto = lostmessagecounthisto_;

	tmpcount = 0;
	for(size_t i=0 ; i < datacounthisto_.elements() ; i++)
	{
		tmpcount = tmpcount + datacounthisto_[i];
	}
	*datacount = tmpcount;
	*datacounthisto = datacounthisto_;

	tmpcount = 0;
	for(size_t i=0 ; i < lostdatacounthisto_.elements() ; i++)
	{
		tmpcount = tmpcount + lostdatacounthisto_[i];
	}
	*lostdatacount = tmpcount;
	*lostdatacounthisto = lostdatacounthisto_;


	*messages = messages_;

	*systemtime = (double)(now - starttime_);

	// load calculation is specific to the service
	// overall load is the maximum of all threads (threads may contain multiple components)
	xdata::Double loadThread1 = 0;
	xdata::Double loadThread2 = 0;

	*load = 0;
	for( size_t i=0 ; i<components->getRowCount() ; i++)
	{
		xdata::Double* compservicetime = (xdata::Double*)components_.getValueAt(i, "servicetime");
		xdata::Double* compload = (xdata::Double*)components_.getValueAt(i, "load");
		xdata::String* compname = (xdata::String*)components_.getValueAt(i, "name");
		*compload = (*compservicetime)/(*systemtime);

		// thread 1 contains 2 components: pttcp receiverloop and onMessage on eventing
		if(((*compname) == "ptincoming") || ((*compname) == "eventing"))
		{
			loadThread1 = loadThread1 + (*compload);
		}
		// thread 1 contains 1 component: pttcp sender
		else if((*compname) == "ptoutgoing")
		{
			loadThread2 = loadThread2 + (*compload);
		}
	}
	*components = components_;

	if(loadThread1>loadThread2)
	{
		*load = loadThread1;
	}
	else
	{
		*load = loadThread2;
	}

	// recalculate some properties
	*connectedclients = clients_.size();

	*cpuusage = processinfo_->cpuUsage();
	*rsize = processinfo_->rsize();
	*vsize = processinfo_->vsize();

	// set new starting time to only measure between intervals
	starttime_ = now;
	messagecount_ = 0;
	lostmessagecount_ = 0;
	datacount_ = 0;
	lostdatacount_ = 0;
	clients_.clear();

	for( size_t i=0 ; i<messages_.elements() ; i++)
	{
		messages_[i] = 0;
	}

	for( size_t i=0 ; i<components_.getRowCount() ; i++)
	{
		xdata::Double* servicetime = (xdata::Double*)components_.getValueAt(i, "servicetime");
		*servicetime = 0;
		xdata::Double* load = (xdata::Double*)components_.getValueAt(i, "load");
		*load = 0;
	}

	messagecounthisto_.clear();
	lostmessagecounthisto_.clear();
	datacounthisto_.clear();
	lostdatacounthisto_.clear();

	servicetimeincoming_ = servicetimeincoming;
	servicetimeoutgoing_ = servicetimeoutgoing;
}

xdata::UnsignedInteger32 b2in::eventing::Statistics::getTotalMessageCount()
{
	return totalmessagecount_;
}

xdata::UnsignedInteger32 b2in::eventing::Statistics::getTotalLostMessageCount()
{
	return totallostmessagecount_;
}

void b2in::eventing::Statistics::addLostPacket(size_t length)
{
	mutex_.take();
	lostmessagecount_++;
	totallostmessagecount_++;
	lostdatacount_=lostdatacount_+length;
	mutex_.give();
}

void b2in::eventing::Statistics::incBrokenConnections()
{
	mutex_.take();
	brokenConnections_++;
	mutex_.give();
}

xdata::UnsignedInteger32 b2in::eventing::Statistics::getBrokenConnections()
{
	return brokenConnections_;
}

toolbox::TimeVal b2in::eventing::Statistics::getLastMsgReceived()
{
	return lastMsgReceived_;
}

xdata::Double b2in::eventing::Statistics::getRate()
{
	// Need to access infospace, since the infospace is updated every n seconds and remains unchanged for that period
	//
	mutex_.take();
	xdata::UnsignedInteger32 *messagecount = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("messagecount"));
	xdata::Double *systemtime = dynamic_cast<xdata::Double*>(is_->find("systemtime"));
	
	xdata::Double rate = (*messagecount)/(*systemtime);

	//std::cout << "messagecount : " << (*messagecount) << " : system time : " << (*systemtime) << std::endl;

	mutex_.give();
	return rate;
}

xdata::Double b2in::eventing::Statistics::getMessageServiceTime()
{
	// Need to access infospace, since the infospace is updated every n seconds and remains unchanged for that period
	//
	mutex_.take();
	xdata::UnsignedInteger32 *messagecount = dynamic_cast<xdata::UnsignedInteger32*>(is_->find("messagecount"));
	
	xdata::Table *components = dynamic_cast<xdata::Table*>(is_->find("components"));
	for( size_t i=0 ; i<components->getRowCount() ; i++)
	{
		xdata::String* name = (xdata::String*)components->getValueAt(i, "name");
		if ( (*name) == "eventing")
		{
			xdata::Double* servicetime = (xdata::Double*)components->getValueAt(i, "servicetime");
			xdata::Double t = (*servicetime)/(*messagecount);
			mutex_.give();
			return t;
		}
	}
	mutex_.give();
	return 0.0;	
}

void b2in::eventing::Statistics::addIncomingPacket
(
	toolbox::TimeVal& receivetimestamp,
	size_t length,
	const std::string& sourceid 
)
{
	mutex_.take();
	
	lastMsgReceived_ = receivetimestamp;
	messagecount_++;
	totalmessagecount_++;
	datacount_=datacount_+length;

	unsigned long bin = 0;
	if(length != 0)
	{
		bin = (unsigned long)log2((double)length); // bin size in histogram
		size_t binsize = messages_.elements();
		if( bin >= (binsize-1) )
		{
			bin = binsize;
		}
	}
	xdata::UnsignedInteger32* count = (xdata::UnsignedInteger32*)messages_.elementAt(bin);
	*count = (*count + 1);

	toolbox::TimeVal queuetimeval = toolbox::TimeVal::gettimeofday();

	std::map<std::string, double>::iterator iter = clients_.find(sourceid);
	if(iter == clients_.end())
	{
		clients_[sourceid] = receivetimestamp;
	}
	else
	{
		(*iter).second = receivetimestamp;
	}


	toolbox::TimeVal servicetime = queuetimeval-receivetimestamp;
	
	this->addServiceTime("eventing",servicetime);
	mutex_.give();	
}

void b2in::eventing::Statistics::addServiceTime(toolbox::TimeVal& receivetimestamp )
{
	mutex_.take();
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();

	toolbox::TimeVal servicetime = now-receivetimestamp;
	this->addServiceTime("eventing",servicetime);
	mutex_.give();	
}

void b2in::eventing::Statistics::addServiceTime(const std::string& name, toolbox::TimeVal timedelta)
{
	LOG4CPLUS_DEBUG(app_->getApplicationLogger(), "Servicetime [" << (double)timedelta << "]");
	for( size_t i=0 ; i<components_.getRowCount() ; i++)
	{
		xdata::String* tmpname = (xdata::String*)components_.getValueAt(i, "name");
		if(((std::string)*tmpname)==name)
		{
			xdata::Double* servicetime = (xdata::Double*)components_.getValueAt(i, "servicetime");
			*servicetime = (*servicetime)+timedelta;
		}
	}
}

void b2in::eventing::Statistics::actionPerformed (xdata::Event& e)
{	
	if ( e.type() == "urn:xdata-event:ItemGroupRetrieveEvent" )
	{
		//LOG4CPLUS_INFO (app_->getApplicationLogger(), "Incoming pulse for eventing-statistics flashlist data");
		xdata::ItemGroupRetrieveEvent & event = dynamic_cast<xdata::ItemGroupRetrieveEvent&>(e);
		if ( event.infoSpace()->name().find("urn:eventing-statistics") != std::string::npos )
		{
			mutex_.take();
			updateStats();
			mutex_.give();
		}
	}
}

void b2in::eventing::Statistics::timeExpired (toolbox::task::TimerEvent& e)
{
	mutex_.take();

	if ( e.getTimerTask()->name == "histo")
	{
		messagecounthisto_.push_back(messagecount_);
		lostmessagecounthisto_.push_back(lostmessagecount_);
		datacounthisto_.push_back(datacount_);
		lostdatacounthisto_.push_back(lostdatacount_);

		messagecount_ = 0;
		lostmessagecount_ = 0;
		datacount_ = 0;
		lostdatacount_ = 0;
	}
	
	mutex_.give();
} 

void b2in::eventing::Statistics::addComponent(const std::string& name)
{
	xdata::String tmpname = name;
	xdata::Double tmp=0;

	size_t size = components_.getRowCount();
	components_.setValueAt(size, "name",        tmpname);
	components_.setValueAt(size, "servicetime", tmp);
	components_.setValueAt(size, "load",        tmp);
}

