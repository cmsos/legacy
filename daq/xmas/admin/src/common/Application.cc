// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include "xmas/admin/Application.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "xcept/tools.h"

#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/Table.h"

#include "xplore/DiscoveryEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/Timer.h"

#include "xmas/admin/exception/Exception.h"
#include "xgi/framework/Method.h"

#undef XMAS_ADMIN_DEFAULT_HYPERDAQ

XDAQ_INSTANTIATOR_IMPL(xmas::admin::Application);

xmas::admin::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this), collectorProxyXMAS_(0), collectorProxyCollection_(0), lasProxy_(0), heartbeatProxy_(0)
{	
	s->getDescriptor()->setAttribute("icon", "/xmas/admin/images/xmas-admin-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/admin/images/xmas-admin-icon.png");

	scanPeriod_          = "PT10S";

	this->getApplicationInfoSpace()->fireItemAvailable("scanPeriod",          &scanPeriod_);

	// bind HTTP callbacks
#ifdef XMAS_ADMIN_DEFAULT_HYPERDAQ
	xgi::framework::deferredbind(this, this, &xmas::admin::Application::Default, "Default");
#endif

 	xgi::bind(this, &xmas::admin::Application::clear, "clear");
    xgi::bind(this, &xmas::admin::Application::reset, "reset");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	lasClearEnable_ = false;
	this->getApplicationInfoSpace()->fireItemAvailable("lasClearEnable", &lasClearEnable_);
}

xmas::admin::Application::~Application()
{
}


//
// Infospace listener
//

void xmas::admin::Application::actionPerformed( xdata::Event& event) 
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		// the following ( error handlers) might leak if process is not restarted for initialization
		collectorProxyXMAS_  =  new b2in::utils::ServiceProxy(this, "xmascollector2g", "xmas", new xmas::admin::CollectorProxyErrorHandler(this));
		collectorProxyCollection_  =  new b2in::utils::ServiceProxy(this, "xmascollector2g", "collection", new xmas::admin::CollectorProxyErrorHandler(this));
		lasProxy_  =  new b2in::utils::ServiceProxy(this, "xmaslas2g", "collection", new xmas::admin::LASProxyErrorHandler(this));
		heartbeatProxy_  =  new b2in::utils::ServiceProxy(this, "xmasheartbeat", "xmas", new xmas::admin::HeartbeatProxyErrorHandler(this));

		toolbox::task::Timer * timer = 0;
		// Create timer for refreshing subscriptions
		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:admin-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:admin-timer");		
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:admin-timer");
		}
	
		// submit task
		toolbox::TimeInterval interval;
		interval.fromString(scanPeriod_.toString()); // in seconds
		toolbox::TimeVal start;
		start = toolbox::TimeVal::gettimeofday();
		timer->scheduleAtFixedRate( start, this, interval, 0, "urn:xmas:admin-task" );

	}
	else
	{
		LOG4CPLUS_ERROR (this->getApplicationLogger(), "Received unsupported event type '" << event.type() << "'");
	}
}

//
// XGI SUpport
//
void xmas::admin::Application::Default(xgi::Input * in, xgi::Output * out ) 
{
#ifdef XMAS_ADMIN_DEFAULT_HYPERDAQ
	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
	
	// Tabbed pages
	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	std::map<std::string, xmas::broker2g::Model*> models = modelRegistry_->getModels();
	std::map<std::string, xmas::broker2g::Model*>::iterator iter;
	for( iter=models.begin() ; iter!=models.end() ; iter++)
	{
		std::string name = (*iter).first;

		*out << "<div class=\"xdaq-tab\" title=\"" << name << "\">" << std::endl;
		modelRegistry_->view(name, out);
		*out << "</div>";
	}

	*out << "</div>"
#endif
}

void xmas::admin::Application::StatisticsTabPage( xgi::Output * out )
{
#ifdef XMAS_ADMIN_DEFAULT_HYPERDAQ
	// print eventings
	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Eventing");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();

	if(b2inEventingProxyInput_)
	{
		try
		{
			b2in::utils::MessengerCache* messengerCache = b2inEventingProxyInput_->getMessengerCache();
			std::list<std::string> destinations = messengerCache->getDestinations();
			std::list<std::string>::iterator iter;
			for( iter=destinations.begin() ; iter!=destinations.end() ; iter++)
			{
				*out << cgicc::tr();
				*out << cgicc::td(*iter) << std::endl;
				*out << cgicc::tr() << std::endl;
			}
		}
		catch(b2in::utils::exception::Exception& e)
		{
			// no messenger cache created -> ignore
		}
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();

	// print topics
	*out << cgicc::table().set("class", "xdaq-table");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Topics");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();

	std::set<std::string> topics = toolbox::parseTokenSet(topics_.toString(), ",");
	std::set<std::string>::iterator i;
	for (i = topics.begin(); i != topics.end(); ++i)
	{
		*out << cgicc::tr();
		*out << cgicc::td(*i) << std::endl;
		*out << cgicc::tr() << std::endl;
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();
#endif
}

//
// Discovery service
//

void xmas::admin::Application::timeExpired(toolbox::task::TimerEvent& event)
{
	LOG4CPLUS_DEBUG (this->getApplicationLogger(), "Timer callback");
	
	try
	{
		collectorProxyXMAS_->scan();		
		collectorProxyCollection_->scan();		
		lasProxy_->scan();		
		heartbeatProxy_->scan();		
	}
	catch (b2in::utils::exception::Exception& e)
	{
		LOG4CPLUS_FATAL (this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
	
}

void xmas::admin::Application::reset(xgi::Input * in, xgi::Output * out ) 
{
	// forward reset request to all collectors and LASs
 	xdata::Properties plist;
        plist.setProperty("urn:b2in-eventing:protocol", "xmas");
        plist.setProperty("urn:xmas-protocol:action", "reset");

	try
	{
        	this->forwardRequest(plist);
	}
	catch(xmas::admin::exception::Exception & e)
	{
		XCEPT_RETHROW(xgi::exception::Exception, "failed to foward reset request to monitoring", e);
	}


}

void xmas::admin::Application::clear(xgi::Input * in, xgi::Output * out ) 
{
	std::string flashlist = "";
	std::string column = "";
	std::string value = "";
	
	try
	{
        	cgicc::Cgicc cgi(in);           
        	cgicc::const_form_iterator i = cgi.getElement("flashlist");
       	
        	if (i != cgi.getElements().end())
        	{
                	flashlist = i->getValue();
        	}
       	
        	i = cgi.getElement("column");           
        	if (i != cgi.getElements().end())
        	{
                	column = i->getValue();
                	i = cgi.getElement("value");
                	if (i != cgi.getElements().end())
                	{
                        	value = i->getValue();
                	}
                	else
                	{
                        	std::stringstream msg;
                        	msg << "Specified column '" << column << "', but value field is missing";
                        	XCEPT_RAISE (xgi::exception::Exception, msg.str());
                	}
        	}
	}
	catch (const std::exception & e)
	{
        	XCEPT_RAISE(xgi::exception::Exception, e.what());
	}   
	
	
       // forward clear request to all collectors and LASs
	xdata::Properties plist;
        plist.setProperty("urn:b2in-eventing:protocol", "xmas");
        plist.setProperty("urn:xmas-protocol:action", "clear");

	if (flashlist != "")
	{
		plist.setProperty("urn:xmas-protocol:flashlist", flashlist);
	}

/* LO AP disable bad feature
	if (column != "")
	{
		plist.setProperty("urn:xmas-protocol:column", column);
		plist.setProperty("urn:xmas-protocol:value", value);
	}
*/

	try
	{
		this->forwardRequest(plist);
	}
        catch(xmas::admin::exception::Exception & e)
        {
                this->notifyQualified( "error", e);
        }


}
	
	
void xmas::admin::Application::forwardRequest(xdata::Properties & plist) 
{
	b2in::utils::MessengerCache * messengerCacheCollectorXMAS = 0;
	b2in::utils::MessengerCache * messengerCacheCollectorCollection = 0;
	b2in::utils::MessengerCache * messengerCacheLAS = 0;
	b2in::utils::MessengerCache * messengerCacheHeartbeat = 0;
	try
	{
		messengerCacheCollectorXMAS = collectorProxyXMAS_->getMessengerCache();
		messengerCacheCollectorCollection = collectorProxyCollection_->getMessengerCache();
		messengerCacheLAS = lasProxy_->getMessengerCache();
		messengerCacheHeartbeat = heartbeatProxy_->getMessengerCache();
	}
	catch(b2in::utils::exception::Exception & e)
	{
		XCEPT_RETHROW (xmas::admin::exception::Exception, "cannot access messenger cache", e);
	}
	
	std::list<std::string> collectorServicesXMAS = messengerCacheCollectorXMAS->getDestinations();	
	
        plist.setProperty("urn:b2in-protocol:service", "xmascollector2g");
	for (std::list<std::string>::iterator j = collectorServicesXMAS.begin(); j != collectorServicesXMAS.end(); j++ )
	{
			try
			{
				messengerCacheCollectorXMAS->send((*j),0,plist);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				std::stringstream msg;
				msg << "failed to send request to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified("error", g);
			}
			catch ( b2in::nub::exception::QueueFull & e )
			{
				std::stringstream msg;
				msg << "failed to send (queue full) to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified("error", g);
			}
			catch ( b2in::nub::exception::OverThreshold & e)
			{
				std::stringstream msg;
				msg << "failed to send (over threshold) to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified("error", g);
			}
	}

	std::list<std::string> collectorServicesCollection = messengerCacheCollectorCollection->getDestinations();

        plist.setProperty("urn:b2in-protocol:service", "xmascollector2g");
        for (std::list<std::string>::iterator j = collectorServicesCollection.begin(); j != collectorServicesCollection.end(); j++ )
        {
                        try
                        {
                                messengerCacheCollectorCollection->send((*j),0,plist);
                        }
                        catch (b2in::nub::exception::InternalError & e)
                        {
                                std::stringstream msg;
                                msg << "failed to send request to " << (*j);
                                XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
                                this->notifyQualified("error", g);
                        }
                        catch ( b2in::nub::exception::QueueFull & e )
                        {
                                std::stringstream msg;
                                msg << "failed to send (queue full) to " << (*j);
                                XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
                                this->notifyQualified("error", g);
                        }
                        catch ( b2in::nub::exception::OverThreshold & e)
                        {
                                std::stringstream msg;
                                msg << "failed to send (over threshold) to " << (*j);
                                XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
                                this->notifyQualified("error", g);
                        }
        }


	// Heartbeat
	std::list<std::string> heartbeatServices = messengerCacheHeartbeat->getDestinations();	
	
        plist.setProperty("urn:b2in-protocol:service", "xmasheartbeat");
	for (std::list<std::string>::iterator j = heartbeatServices.begin(); j != heartbeatServices.end(); j++ )
	{
			try
			{
				messengerCacheHeartbeat->send((*j),0,plist);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				std::stringstream msg;
				msg << "failed to send request to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified("error", g);
			}
			catch ( b2in::nub::exception::QueueFull & e )
			{
				std::stringstream msg;
				msg << "failed to send (queue full) to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified("error", g);
			}
			catch ( b2in::nub::exception::OverThreshold & e)
			{
				std::stringstream msg;
				msg << "failed to send (over threshold) to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified("error", g);
			}
	}


	// end of heartbeat process

	if (  plist.getProperty("urn:xmas-protocol:action")  == "clear")
	{
		// LAS accepts only reset command
		if ( ! lasClearEnable_ )
			return;
	}
	


	std::list<std::string> lasServices = messengerCacheLAS->getDestinations();	
	
        plist.setProperty("urn:b2in-protocol:service", "xmaslas2g");
	for (std::list<std::string>::iterator j = lasServices.begin(); j != lasServices.end(); j++ )
	{
			try
			{
				messengerCacheLAS->send((*j),0,plist);
			}
			catch (b2in::nub::exception::InternalError & e)
			{
				std::stringstream msg;
				msg << "failed to send request to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified( "error", g);
			}
			catch ( b2in::nub::exception::QueueFull & e )
			{
				std::stringstream msg;
				msg << "failed to send (queue full) to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified("error", g);
			}
			catch ( b2in::nub::exception::OverThreshold & e)
			{
				std::stringstream msg;
				msg << "failed to send (over threshold) to " << (*j);
				XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, g, msg.str(), e);
               			this->notifyQualified( "error", g);
			}
	}

}


// ---------------------------------------------------------

xmas::admin::CollectorProxyErrorHandler::CollectorProxyErrorHandler(xmas::admin::Application* app)
	: application_(app)
{	
}

void xmas::admin::CollectorProxyErrorHandler::asynchronousExceptionNotification(xcept::Exception& e)
{
	std::stringstream msg;
	msg << "failed to send message to collector";
	XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, ex , msg.str(), e);
	application_->notifyQualified("error",ex);
}

xmas::admin::LASProxyErrorHandler::LASProxyErrorHandler(xmas::admin::Application* app)
	: application_(app)
{	
}

void xmas::admin::LASProxyErrorHandler::asynchronousExceptionNotification(xcept::Exception& e)
{
	std::stringstream msg;
	msg << "failed to send message to LAS";
	XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, ex , msg.str(), e);
	application_->notifyQualified("error",ex);
}

xmas::admin::HeartbeatProxyErrorHandler::HeartbeatProxyErrorHandler(xmas::admin::Application* app)
        : application_(app)
{               
}               
                
void xmas::admin::HeartbeatProxyErrorHandler::asynchronousExceptionNotification(xcept::Exception& e)
{               
        std::stringstream msg;
        msg << "failed to send message to Heartbeat";
        XCEPT_DECLARE_NESTED(xmas::admin::exception::Exception, ex , msg.str(), e);
        application_->notifyQualified("error",ex);
}


