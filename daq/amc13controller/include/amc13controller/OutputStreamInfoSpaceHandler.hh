#ifndef __OutputStreamInfoSpaceHandler
#define __OutputStreamInfoSpaceHandler

#include "d2s/utils/StreamInfoSpaceHandler.hh"

namespace amc13controller
{
    class OutputStreamInfoSpaceHandler:public utils::StreamInfoSpaceHandler {
    public:
        OutputStreamInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceHandler *appIS );
  
    private:
    };
}

#endif /* __OutputStreamInfoSpaceHandler */
