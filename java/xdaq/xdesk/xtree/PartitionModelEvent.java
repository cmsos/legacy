package xdaq.xdesk.xtree;

import java.util.EventObject;

public class PartitionModelEvent extends EventObject 
{	
	public  PartitionModelEvent( PartitionDefaultModel model)
	{
		super(model);
	}
}
