package xdaq.xdesk.jfinder;

import javax.swing.*;
import javax.swing.tree.*;
import java.util.*;
import java.io.*;

public class FinderTreeNode
implements TreeNode {
	File file_;
	Vector children_;
	FinderTreeNode parent_;
	boolean isSelected = false;
	
	static public final Enumeration EMPTY_ENUMERATION
		= new Enumeration() {
			public boolean hasMoreElements() {
				return false;
			}
			
			public Object nextElement() {
				throw new NoSuchElementException("no more elements");
			}
		};
	
	public File getFile() {
		return file_;
	}
	
	public String toString() {
		return file_.getName();
	}
	
	public void setSelected(boolean selected) {
		isSelected = selected;
	}
	
	public boolean getSelected() {
		return isSelected;
	}
	
	/*
		public class ArrayEnumeration implements Enumeration
	 {
		 protected Object array_[];
		 protected int curpos_;
		 public ArrayEnumeration (Object array[])
		 {
			 array_ = array;
			 curpos_ = 0;
		 }
		 public boolean hasMoreElements()
		 {
			 return curpos_ < array_.length;
		 }
		 public Object nextElement()
		 {
			 return new FinderTreeNode( array_[curpos_++]);
		 }
	 };
	 */
	
	public FinderTreeNode(File file) {
		this(file, null);
	}
	
	public FinderTreeNode(File file, FinderTreeNode parent) {
		file_ = file;
		children_ = null;
		parent_ = parent;
	}
	
	public Enumeration children() {
		if (children_ != null) {
			return children_.elements();
		}
		else {
			if (file_.isDirectory()) {
				File[] files = file_.listFiles();
				children_ = new Vector();
				for (int i = 0; i < files.length; i++) {
					children_.add(new FinderTreeNode(files[i], this));
				}
				return children_.elements();
			}
			else {
				return EMPTY_ENUMERATION;
			}
		}
	}
	
	public boolean getAllowsChildren() {
		if (file_.isDirectory()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public TreeNode getChildAt(int childIndex) {
		if (this.getAllowsChildren()) {
			if (children_ == null) {
				this.children();
			}
			return (TreeNode) children_.elementAt(childIndex);
		}
		else {
			throw new ArrayIndexOutOfBoundsException("node has no children");
		}
	}
	
	public int getChildCount() {
		if (this.getAllowsChildren()) {
			if (children_ == null) {
				this.children();
			}
			return children_.size();
		}
		return 0;
		
	}
	
	public int getIndex(TreeNode node) {
		if (node == null) {
			throw new IllegalArgumentException("argument is null");
		}
		
		if (this.getAllowsChildren()) {
			if (children_ == null) {
				this.children();
			}
			return children_.indexOf(node);
		}
		
		return -1;
	}
	
	public TreeNode getParent() {
		return parent_;
	}
	
	public boolean isLeaf() {
		return (this.getChildCount() == 0);
	}
	
	// invalidates any cached children nodes
	public void invalidate() {
		children_ = null;
	}
	
}
