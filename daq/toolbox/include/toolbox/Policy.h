 // $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2013, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#ifndef _toolbox_Policy_h_
#define _toolbox_Policy_h_

#include <string>
#include <list>
#include "toolbox/Properties.h"

namespace toolbox
{
	class Policy: public toolbox::Properties
	{
		public:
		Policy();
		virtual ~Policy(){}
		virtual std::string getType() = 0;
		virtual std::string getPackage() = 0;
		virtual void setDefaultPolicy() = 0;
		std::string toString();
	
		size_t counter_;	
		std::list<std::string> users_;

	};
}

#endif
