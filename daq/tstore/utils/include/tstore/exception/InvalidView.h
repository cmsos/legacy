// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tstore_exception_InvalidView_h_
#define _tstore_exception_InvalidView_h_

#include "tstore/exception/Exception.h"

namespace tstore {
	namespace exception { 
		class InvalidView: public tstore::exception::Exception 
		{
			public: 
			InvalidView( std::string name, std::string message, std::string module, int line, std::string function ): 
					tstore::exception::Exception(name, message, module, line, function) 
			{} 
			
			InvalidView( std::string name, std::string message, std::string module, int line, std::string function,
				xcept::Exception& e ): 
					tstore::exception::Exception(name, message, module, line, function, e) 
			{} 
		}; 
	} 
}

#endif
