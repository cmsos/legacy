package net.hep.cms.xdaqctl;

import net.hep.cms.xdaqctl.xdata.FlashList;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Document;

import javax.mail.util.ByteArrayDataSource;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * XMAS SOAP message class
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @author  Alexander Oh (alexander.oh@cern.ch)
 * @version $Name:  $
 */

public class XMASMessage extends Message {
	
	private FlashList flashlist;
	private String originator, tag="tag";
	
	/**
	 * Create a XMAS SOAP message.
	 * The message contains a serialized FlashList object as a MIME attachment.
	 * 
	 * @param flashlist FlashList to send.
	 * @param originator 
	 */
	public XMASMessage(FlashList flashlist, String originator)
			throws XDAQMessageException
	{
		super();
		
		this.flashlist = flashlist;
		this.originator = originator;
		
		constructMessage("ENDPOINT_NOT_DEFINED");
	}
	
	/**
	 * Create a XMAS SOAP message from SOAPMessage.
	 * Used when receiving the XMAS report messages.
	 */
	public XMASMessage(SOAPMessage soapMessage)
			throws XDAQMessageException
	{
		super(soapMessage);
		
		flashlist = getFlashList();
		originator = getOriginator();
		tag = getTag();
	}
	
	/**
	 * Returns the "tag" element value of the XMAS Message.
	 * The tag is used to route message to XMAS Collectors.
	 * 
	 * @return tag value
	 * @throws XDAQMessageException
	 */
	public String getTag() throws XDAQMessageException {
		// need to retrieve from the SOAP message
		// for the use in XMASMessage(SOAPMessage).
		String tag = null;
		try {
			SOAPElement sample = getSampleElement();
			tag = sample.getAttribute("tag");
		} catch (Exception e) {
			throw new XDAQMessageException(
					"Failed to get a tag", e);
		}
		
		return tag;
	}
	
	/**
	 * Sets the "tag" element value of the XMAS Message.
	 * The tag is used to route message to XMAS Collectors.
	 * 
	 * @param tag
	 * @throws XDAQMessageException
	 */
	public void setTag(String tag) throws XDAQMessageException {
		try {
			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
			SOAPElement sample = getSampleElement();
			
			Name tagName = envelope.createName("tag");
			sample.removeAttribute(tagName);
			sample.addAttribute(tagName, tag);
		} catch (Exception e) {
			throw new XDAQMessageException(
					"Failed to get a tag", e);
		}

		this.tag = tag;
	}
	
	/**
	 * Return the originator address of XMAS message.
	 * 
	 * @return Originator
	 * @throws XDAQMessageException
	 */
	public String getOriginator() 
			throws XDAQMessageException {

		String originator = null;
		try {
			SOAPElement sample = getSampleElement();
			originator = sample.getAttribute("originator");
		} catch (Exception e) {
			throw new XDAQMessageException(
					"Failed to get a originator", e);
		}
		
		return originator;
	}
	
	/**
	 * Return the name of the flash list.
	 * 
	 * @return name of flash list
	 * @throws XDAQMessageException
	 */
	public String getFlashListName() throws XDAQMessageException {

		String name = null;
		try {
			SOAPElement sample = getSampleElement();
			name = sample.getAttribute("flashlist");
		} catch (Exception e) {
			throw new XDAQMessageException(
					"Failed to get a name", e);
		}

		return name.substring("urn:xdaq-flashlist:".length());
	}
	
	/**
	 * Retrieve the attached flash list.
	 */
	public FlashList getFlashList() throws XDAQMessageException {
		FlashList fl = new FlashList(getFlashListName());		
		byte[] buffer = null;
		try {
			// Convert underlying InputStream to ByteArrayInputStream
			InputStream in = getAttachmentContent();
			buffer = new byte[in.available()];
			in.read(buffer, 0, buffer.length);
			fl.deserialize(new ByteArrayInputStream(buffer));			
		} catch (Exception e) {
			String name = "NA";
			try {
				name = getFlashListName();
			} catch (Exception ignore) {}
			// dump content
			String dump = "";
			String sBuffer = "";
			if (buffer!=null) {
				sBuffer = new String(buffer);
				for (int i = 0; i<buffer.length ; i++) {
					dump+= i+":"+buffer[i] + " ";
				}
			}
			throw new XDAQMessageException(
					"Failed to deserialize a flash list: "+name+ "\nDump: \n>>>"+sBuffer+"<<<\n "+dump, e);
		}		
		return fl;
	}

	/**
	 * Returns the sample element (used to construct XMAS SOAP message).
	 *  
	 * @return
	 * @throws SOAPException
	 */
	private SOAPElement getSampleElement() throws SOAPException {
		SOAPElement sample;
		
		SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
		SOAPBody body = envelope.getBody();
		
		SOAPElement report = (SOAPElement)body.getChildElements(
				envelope.createName("report", "xmas", XMAS_NS))
				.next();
		sample = (SOAPElement)report.getChildElements(
				envelope.createName("sample", "xmas", XMAS_NS))
				.next();

		return sample;
	}

	/**
	 * Flash list is serialized just before sending.
	 */
	public Document send(String endpoint)
			throws XDAQTimeoutException, XDAQMessageException {
		
		constructMessage(endpoint);
		
		return super.send(endpoint, "urn:xdaq-application:service=ws-eventing");
	}

	/**
	 * Construct "empty" XMAS message
	 * @param endpoint
	 * @throws XDAQMessageException
	 */
	private void constructMessage(String endpoint) throws XDAQMessageException {

		// "SOAPAction" MIME header should be added when the message is sent.
		// Set SOAPAction header with a default target
		initialize();
		message.getMimeHeaders().setHeader("SOAPAction", WSE_TARGET);
		
		// add additional headers if available.
		// this is a specialty of xmas collectors which require
		// additional info for faster routing.
		// head looks like:
		// x-xdaq-tags: flashlist, urn:xdaq-flashlist:[FL_NAME],[TAG]
		message.getMimeHeaders().addHeader("x-xdaq-tags",
				"flashlist, urn:xdaq-flashlist:"+flashlist.getName()+","+tag); 

		try {
			SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
			SOAPHeader header = envelope.getHeader();
			//header.removeContents();
			SOAPElement e;
			e = header.addChildElement("Action", "wsa", WSA_NS);
			e.addTextNode(WSE_TARGET);
			e = header.addChildElement("To", "wsa", WSA_NS);
			e.addTextNode(endpoint);
			
			// SOAPBody->report->sample
			SOAPBody body = envelope.getBody();
			//body.removeContents();
			SOAPElement report = body.addBodyElement(envelope.createName(
					"report", "xmas", XMAS_NS));
			report.addNamespaceDeclaration("xmas-sensor", XMAS_SENSOR_NS);
			SOAPElement sample = report.addChildElement("sample", "xmas",
					XMAS_NS);
			sample.addAttribute(envelope.createName("flashlist"),
					"urn:xdaq-flashlist:" + flashlist.getName() );
			sample.addAttribute(envelope.createName("tag"), this.tag);
			sample.addAttribute(envelope.createName("originator"),
					originator);
		} catch (SOAPException e) {
			throw new XDAQMessageException(
					"Failed to create XMAS SOAP message", e);
		}
		
		//message.removeAllAttachments();
		try {
			// binary flashlsit attachment
			//AttachmentPart attachment = message.createAttachmentPart(
			//		flashlist.serialize(), "application/x-xdata+exdr");
			DataSource ds = new ByteArrayDataSource(flashlist.serialize(), "application/x-xdata+exdr");
			DataHandler dh = new DataHandler(ds);
			AttachmentPart attachment = message.createAttachmentPart(dh);
			
			attachment.setMimeHeader("Content-Disposition",
					"attachment; filename=" + "listname" + ".exdr; "
					+ "creation-date=\"" + getDateString() + "\"");
			message.addAttachmentPart(attachment);
		} catch (Exception e) {
			throw new XDAQMessageException(
					"Failed to attach a serialized flash list", e);
		}
	}
		
	/**
	 * construct current data in string format compliant with XMAS.
	 * 
	 * @return
	 */
	private String getDateString() {

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		
		return format.format(new Date());
	}

	/**
	 * XMAS contains one binary attachment with serialized data.
	 * 
	 * @return
	 * @throws SOAPException
	 */
	private InputStream getAttachmentContent() throws SOAPException {
		// binary flashlist attachment
		Iterator i = message.getAttachments();
		
		if (!i.hasNext()) { return null; }
				// need to call hasNext() due to Axis SOAP bug?
		
		return (InputStream)((AttachmentPart)i.next()).getContent();
	}
	
	/**
	 * write message content to a file.
	 * 
	 * @param filename
	 */
	protected void dump(String filename) {
		try {
			java.io.FileOutputStream out = new java.io.FileOutputStream(filename);
			message.writeTo(out);
		} catch (Exception ignored) {
			ignored.printStackTrace();
		}
	}

	
}

// End of file
// vim: set sw=4 ts=4:
