#include <iostream>
#include <string>
#include "toolbox/net/URN.h"
#include "toolbox/net/exception/MalformedURN.h"

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << "Usage: " << argv[0] << " urn " << std::endl;
		return 1;
	}

	try
	{
		toolbox::net::URN u(argv[1]);
		std::cout << "Namespace is " << u.getNamespace() << std::endl;
		std::cout << " NSS is " << u.getNSS() << std::endl;
		std::cout << "String representatio is " << u.toString() << std::endl; 
	} catch (toolbox::net::exception::MalformedURN& mfu)
	{
		std::cout << "Exception: " << mfu.what() << std::endl;
		return 1;
	}

	return 0;
}
