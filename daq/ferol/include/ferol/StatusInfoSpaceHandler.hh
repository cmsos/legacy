#ifndef __StatusInfoSpaceHandler
#define __StatusInfoSpaceHandler

#include "xdaq/Application.h"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"

namespace ferol {

    class StatusInfoSpaceHandler : public utils::InfoSpaceHandler 
    {
    public:
        StatusInfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater = NULL);
    };
}

#endif /* __StatusInfoSpaceHandler */
