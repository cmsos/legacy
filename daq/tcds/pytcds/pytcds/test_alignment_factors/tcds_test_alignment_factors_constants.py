###############################################################################

# The addresses of the boards/applications to connect to.
CPM = "cpm"
LPM = "lpm-1"
ICIS = [
    "ici-11",
    # "ici-12",
    # "ici-13",
    # "ici-14",
    # "ici-15",
    # "ici-16",
    # "ici-17",
    # "ici-18"
]
APVES = ["apve-11"]
PIS = ["pi-11"]
# PIS = ["pi-11", "pi-12"]

# The FED id of the CPM/LPM, so we can disable the readout.
FED_ID_CPM = 1024
FED_ID_LPM = 0

# Some misc. FED ids, so we can enable/disable 'random' inputs on the
# PI.
FED_IDS_MISC = [[10, 2]]

###############################################################################
