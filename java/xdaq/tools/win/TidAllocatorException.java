package xdaq.tools.win;

public class TidAllocatorException extends Exception
{
	TidAllocatorException () { super(); }

	TidAllocatorException ( String msg )
	{
		super (msg);
	}
}
