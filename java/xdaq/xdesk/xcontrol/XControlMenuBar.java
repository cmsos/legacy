package xdaq.xdesk.xcontrol;

import javax.swing.*;
import java.awt.event.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import java.io.*;
import org.w3c.dom.*;
import java.awt.*;
import org.apache.log4j.*;

import xdaq.xdesk.tools.*;

public class XControlMenuBar extends xdaq.xdesk.tools.MenuBar
{
	Logger logger = Logger.getLogger (XControlMenuBar.class);		
	JMenu applicationMenu_;

	XControl xcontrol_;
	XControlViewer xcontrolViewer_;
	HashMap items_;
	HashMap partitionItems_;	
		
	public XControlMenuBar(XControlViewer xcontrolViewer)
	{
		super("XControl",(MenuBarListener)xcontrolViewer.getController());
		
		xcontrol_ = xcontrolViewer.getController();
		xcontrolViewer_ = xcontrolViewer;
		items_ = new HashMap();
	
		JMenu menu;
		JMenuItem item;
		
		// FILE menu
		//
		menu = new JMenu ("File");
		menu.setMnemonic ('f');
				
		item = new JMenuItem ( new AbstractAction ("New Window"){
					public void actionPerformed(ActionEvent e) {
						xcontrol_.newWindow();
					}} );
		menu.add(item);
		items_.put ( "New Window", item);
		
		item = new JMenuItem (new OpenFileAction( xcontrolViewer_ ) );
		menu.add (item);
		items_.put ( "Open" , item );
		
		item = new JMenuItem (new AbstractAction ("Close"){
					public void actionPerformed(ActionEvent e) {
						xcontrol_.closeWindow(xcontrolViewer_);
					}} );
		menu.add (item);
		items_.put ("Close", item);
		
		
	        this.add(menu);
		
		// -----------------
		
		applicationMenu_ = new JMenu ("Command", true);
		applicationMenu_.setMnemonic ('c');

		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Configure") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Configure", item);
		
		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Enable") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Enable", item);
		
		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Disable") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Disable", item);

		
		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Suspend") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Suspend", item);
		
		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Resume") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Resume", item);
		
		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Halt") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Halt", item);
		
		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Clear") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Clear", item);
		
		item = new JMenuItem (new XControlAction(xcontrolViewer_, "Reset") );
		item.setEnabled(false);
		applicationMenu_.add (item);
		items_.put ("Reset", item);

		this.add (applicationMenu_);
		
		
	}
	
	public void enable (String menuItem, boolean enabled)
	{
		JMenuItem item = (JMenuItem) items_.get ( (Object) menuItem );
		if (item != null) 
		{
			item.setEnabled ( enabled );
		} else 
		{
			logger.debug ("Did not find menu item: " + menuItem);
		}
	}
	
	
	
	void setItem(String menuItem) 
	{
		applicationMenu_.add((JMenuItem) items_.get ( (Object) menuItem ));
		enable(menuItem, true);
	}
	
	
	
	public void setApplicationMenu() 
	{
		applicationMenu_.removeAll();
		setItem ( "Configure");
		setItem ( "Enable" );
		setItem ( "Disable");
		setItem ( "Suspend");
		setItem ( "Resume" );
		setItem ( "Halt" );
	}
	
	
	public void setHostMenu() 
	{
		applicationMenu_.removeAll();
		setItem ( "Configure");
		setItem ( "Enable" );
		setItem ( "Disable");
		setItem ( "Suspend");
		setItem ( "Resume" );
		setItem ( "Halt" );
		setItem ( "Reset" );
		setItem ( "Clear");
	}
	
	
	
	public void setEmptyMenu() 
	{
		applicationMenu_.removeAll();
	}

}
