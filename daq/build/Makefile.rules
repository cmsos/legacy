# $Id$

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2017, CERN.                                        #
# All rights reserved.                                                  #
# Authors: J. Gutleber, L. Orsini and D. Simelevicius                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

#----------------------------------------------------------------------------
#
# Check if XDAQ_ROOT ist set (environment).
#
# Check if XDAQ_OS and XDAQ_PLATFORM are set (environment).
#
# Check if Project and Package are defined (Makefiles of project and package).
#
# The BUILD_HOME is the directory where the packages are built, by default
# is XDAQ_ROOT
#----------------------------------------------------------------------------

#
# This is the default target when make is called without parameters
# it has to be the very first target in the rules
#

MakeFilename		= Makefile

default: build 

BUILD_HOME              := $(shell cd $(BUILD_HOME);pwd)
InstallationIncludeDirs  = $(BUILD_HOME)/$(XDAQ_PLATFORM)/include $(BUILD_HOME)/$(XDAQ_PLATFORM)/include/$(XDAQ_OS) $(XDAQ_ROOT)/include $(XDAQ_ROOT)/include/$(XDAQ_OS)
InstallationLibraryDirs  = $(BUILD_HOME)/$(XDAQ_PLATFORM)/lib $(XDAQ_ROOT)/lib

SourceDir		= $(BUILD_HOME)

BuidSupportDir		= $(XDAQ_ROOT)/build

ifdef Packages
PackageList		= $(Packages)

else
# find package name by cutting of the SourceDir from the
# current directory, e.g. TriDAS/$(PROJECT_NAME)/xoap -> xoap
#
#CurrentDir		= $(CURDIR)
#Package		= $(subst $(SourceDir)/,,$(CurrentDir))

PackageList		= $(Package)
PackageSourceDir 	= $(SourceDir)/$(Package)/src


#-----------------------------------------------------------------------------
# re-assign variable with test software descriptions
#-----------------------------------------------------------------------------

#ifeq ($findstring(tests, $(MAKECMDGOALS)), tests)
#ifeq ($(MAKECMDGOALS), tests)
ifneq (,$(findstring tests,$(MAKECMDGOALS)))
PackageSourceDir = $(SourceDir)/$(Package)/test
Sources=$(TestSources)

ifdef TestExecutables
Executables=$(TestExecutables)
endif

ifdef TestDynamicLibrary
DynamicLibrary=$(TestDynamicLibrary)
endif

StaticLibrary=$(TestStaticLibrary)
Libraries=$(TestLibraries)
IncludeDirs += $(TestIncludeDirs)
LibraryDirs += $(TestLibraryDirs)
UserSourcePath = $(TestUserSourcePath)
endif

#ifeq ($findstring(_tests, $(MAKECMDGOALS)), _tests)
#ifeq ($(MAKECMDGOALS), _tests)
ifneq (,$(findstring _tests,$(MAKECMDGOALS)))
PackageSourceDir = $(SourceDir)/$(Package)/test
Sources=$(TestSources)

ifdef TestExecutables
Executables=$(TestExecutables)
endif

ifdef TestDynamicLibrary
DynamicLibrary=$(TestDynamicLibrary)
endif

StaticLibrary=$(TestStaticLibrary)
Libraries=$(TestLibraries)
IncludeDirs += $(TestIncludeDirs)
LibraryDirs += $(TestLibraryDirs)
UserSourcePath = $(TestUserSourcePath)
endif


PackageLibDir		= $(SourceDir)/$(Package)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)
PackageBinDir		= $(SourceDir)/$(Package)/bin/$(XDAQ_OS)/$(XDAQ_PLATFORM)
PackageTargetDir 	= $(PackageSourceDir)/$(XDAQ_OS)/$(XDAQ_PLATFORM)
PackageIncludeDirs	= $(BuildSupportDir) $(SourceDir)/$(Package)/include/$(XDAQ_OS) $(SourceDir)/$(Package)/include $(IncludeDirs) \
			 $(InstallationIncludeDirs)
endif

INSTALL_CROSS_PLATFORM = yes
ifeq ($(INSTALL_CROSS_PLATFORM),yes)
LibInstallDir     = $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/lib
BinInstallDir     = $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/bin
IncludeInstallDir = $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/include
htdocsInstallDir  = $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/htdocs
scriptsInstallDir = $(INSTALL_PREFIX)/$(XDAQ_PLATFORM)/scripts
else
LibInstallDir     = $(INSTALL_PREFIX)/lib
BinInstallDir     = $(INSTALL_PREFIX)/bin
IncludeInstallDir = $(INSTALL_PREFIX)/include
htdocsInstallDir  = $(INSTALL_PREFIX)/htdocs
scriptsInstallDir = $(INSTALL_PREFIX)/scripts
endif

TargetDir		= $(SourceDir)
LibDir			= $(BUILD_HOME)/$(Package)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)
BinDir			= $(BUILD_HOME)/$(Package)/bin/$(XDAQ_OS)/$(XDAQ_PLATFORM)
DocsDir			= $(BUILD_HOME)/$(Package)/doc
XMLDir			= $(BUILD_HOME)/$(Package)/xml
ImagesDir		= $(BUILD_HOME)/$(Package)/images
HTMLDir			= $(BUILD_HOME)/$(Package)/html
ScriptsDir		= $(BUILD_HOME)/$(Package)/scripts


# look in installation dirs if libraries are not found in development dirs.
#LibraryDirs += $(LibInstallDir)

ifeq ($(XDAQ_OS),macosx)
Copy			= cp -p
else
Copy			= cp -pd
endif
CopyDir 		= cp -rp
Move			= mv
MakeDir 		= mkdir -p
Install			= install
Empty			=
Space			= $(Empty) $(Empty)
Delete			= rm -f
Print			= @echo
# JavaCompiler		= javac -target 1.3
JavaCompiler		= javac
ClassPath		= $(SourceDir):$(Libraries)
JavaFlags		= -classpath $(ClassPath)
# JavaFlags		= -d $(SourceDir) -classpath $(ClassPath) -sourcepath $(SourceDir)


# Add -I in front of every include directory
IncludePaths		= $(PackageIncludeDirs:%=-I%)

# Add -L in front of every library directory
LibraryPaths		=-L$(PackageLibDir) $(LibraryDirs:%=-L%) $(InstallationLibraryDirs:%=-L%)
DependentLibraryPaths	=-L$(PackageLibDir) $(DependentLibraryDirs:%=-L%) $(InstallationLibraryDirs:%=-L%)
LibraryList		= $(SystemLibraries:%=-l%) $(Libraries:%=-l%)
DependentLibraryList	= $(DependentLibraries:%=-l%)




#----------------------------------------------------------------------------
#
# Classification of files
#
#----------------------------------------------------------------------------
CCSources			= $(filter %.cc,	$(Sources))
CppSources			= $(filter %.cpp,	$(Sources))
CxxSources			= $(filter %.cxx,	$(Sources))
CSources			= $(filter %.c,		$(Sources))
JavaSources			= $(filter %.java,	$(Sources))
AsmSources			= $(filter %.S,		$(Sources))

# Sources are in a directory common to the package and common to the operating system
#
SourcePath		= $(PackageSourceDir)/common $(PackageSourceDir)/$(XDAQ_OS)/common $(UserSourcePath)

#----------------------------------------------------------------------------
#
# Define all search pathes for source and object files
#
#----------------------------------------------------------------------------
vpath  %.cc  $(SourcePath)
vpath  %.cpp  $(SourcePath)
vpath  %.cxx  $(SourcePath)
vpath  %.c   $(SourcePath)
vpath  %.S   $(SourcePath)
vpath  %.o   $(PackageTargetDir)
vpath  %.d   $(PackageTargetDir)


#----------------------------------------------------------------------------
#
# For each entry in PACKAGES, create a dependency variable, called
# TriDAS/$(PROJECT_NAME)/PACKAGENAME/.loop and put the dependency in the 
# target "build". How to build a %.loop is outlined right below:
# make -C TriDAS/$(PROJECT_NAME)/Packagename/ all
#
#----------------------------------------------------------------------------

PackageListLoop = $(patsubst %,$(SourceDir)/%/.loop,$(PackageList)) 
//ReversePackageListLoop = $(shell reverse.sh $(PackageListLoop))
ReversePackageListLoop = $(shell echo $(PackageListLoop) | awk '{c=split($$0,a," ");for(i=c;i>0;i--)printf "%s ",a[i]}')

%.loop:
	@echo Calling make all in directory $@ using $(MAKECMDGOALS)
	$(MAKE) -C $(subst .loop,,$@) -f $(MakeFilename) _$(MAKECMDGOALS)all


##----------------------------------------------------------------------------
# General rules
##----------------------------------------------------------------------------

# Create targest for object files
#
Objects =            ${CCSources:.cc=.o} ${CppSources:.cpp=.o} ${CxxSources:.cxx=.o} ${CSources:.c=.o} ${AsmSources:.S=.o}
TargetObjects =      $(patsubst %.o, $(PackageTargetDir)/%.o, $(Objects) )

#
# Need to replace also .cxx and .c and .cpp files with .exe suffix. 
# Need to use different variables, like in CCSources, CPPSources...
#
CCMainModules = 	      $(filter %.cc,		$(Executables))
CppMainModules = 	      $(filter %.cpp,		$(Executables))
CxxMainModules = 	      $(filter %.cxx,		$(Executables))
CMainModules = 	      	      $(filter %.c,		$(Executables))

MainModules = 		      $(CCMainModules:.cc=.exe) $(CppMainModules:.cpp=.exe) $(CxxMainModules:.cxx=.exe) $(CMainModules:.c=.exe)
TargetMainModules =           $(patsubst %.exe,  $(PackageTargetDir)/%.exe, $(MainModules) )

Dependencies =       ${CCSources:.cc=.d} ${CppSources:.cpp=.d} ${CxxSources:.cxx=.d} ${CSources:.c=.d} ${MainModules:.exe=.d} ${AsmSources:.S=.d}
TargetDependencies = $(patsubst %.d, $(PackageTargetDir)/%.d, $(Dependencies) )



#----------------------------------------------------------------------------
# Create the library
# Object files are looked up in the platform dependent directory PackageTargetDir using vpath
#----------------------------------------------------------------------------

ifdef StaticLibrary
StaticLibraryName = $(StaticLibrary:%=$(PackageLibDir)/$(LibraryPrefix)%$(StaticSuffix))

$(StaticLibraryName): $(TargetObjects)
	$(MakeDir) $(PackageLibDir)
	$(AR) $(UserStaticLinkFlags) -r $@ $(TargetObjects) $(ExternalObjects)
	$(RANLIB) $@
endif

ifdef DynamicLibrary
DynamicLibraryName = $(DynamicLibrary:%=$(PackageLibDir)/$(LibraryPrefix)%$(DynamicSuffix))
LocalDynamicLibrary=-l$(DynamicLibrary)

$(DynamicLibraryName): $(TargetObjects)
	$(MakeDir) $(PackageLibDir)
	$(LD) $(DynamicLinkFlags) $(UserDynamicLinkFlags) -o $@ $(TargetObjects) $(ExternalObjects) $(DependentLibraryPaths) $(DependentLibraryList)
endif


#----------------------------------------------------------------------------
# Install the packages in $(BUILD_HOME)/lib and /bin
# _installall depends on _buildall, the leaf in the recursion
# (comment: depending on build would lead to another, erroneous
# recursion step)
#----------------------------------------------------------------------------
ifdef ReleaseDir
release: $(PackageListLoop)

ifdef JavaLibrary
# release code for Java
_releaseall: _buildall
	$(Print) Release Java packages
	$(MakeDir) $(ReleaseDir)/$(Project)
	$(Copy) $(BUILD_HOME)/$(JavaLibrary).jar $(ReleaseDir)/$(Project)
ifeq ($(IncludeSources),yes)
	$(Print) Copying sources...
	$(MakeDir) $(ReleaseDir)/$(Project)/$(Package)
	$(Copy)  $(BUILD_HOME)/$(Package)/Makefile* $(ReleaseDir)/$(Project)/$(Package)
#
# Copy the whole src tree and tidy-up afterward
#
	$(Copy) -r  $(BUILD_HOME)/$(Package)/* $(ReleaseDir)/$(Project)/$(Package)
	@find $(ReleaseDir)/$(Project)/$(Package) -name "*.class" -exec rm {} \;
endif
#
# Tidy up CVS dirs
#
	@find $(ReleaseDir)/$(Project) -name "CVS" -exec rm -rf {} \; -print 2>&1  | cat > /dev/null
	@echo Java package released

else
# Release code for C++


_releaseall: _buildall
	$(Print) Release C++ packages
ifndef Packages
	$(MakeDir) $(ReleaseDir)/$(Project)/$(Package)/include/$(XDAQ_OS)
	$(MakeDir) $(ReleaseDir)/$(Project)/$(Package)/bin/$(XDAQ_OS)/$(XDAQ_PLATFORM)
	$(MakeDir) $(ReleaseDir)/$(Project)/$(Package)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)
ifdef StaticLibrary
	$(Print) Installing static library
	$(Copy) $(StaticLibraryName) $(ReleaseDir)/$(Project)/$(Package)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)
endif
ifdef DynamicLibrary
	$(Print) Installing dynamic library
	$(Copy) $(DynamicLibraryName) $(ReleaseDir)/$(Project)/$(Package)/lib/$(XDAQ_OS)/$(XDAQ_PLATFORM)
endif
ifdef Executables
	$(Print) Installing executable binaries
	$(Copy) $(PackageBinDir)/*.exe $(ReleaseDir)/$(Project)/$(Package)/bin/$(XDAQ_OS)/$(XDAQ_PLATFORM)
endif
# Packages
endif

	$(CopyDir) $(BUILD_HOME)/$(Package)/include $(ReleaseDir)/$(Project)/$(Package)

ifeq ($(IncludeSources),yes)
	$(Print) Copying sources...	
	$(Copy)  $(BUILD_HOME)/$(Package)/Makefile* $(ReleaseDir)/$(Project)/$(Package)
#
# Copy the whole src tree and tidy-up afterward
#
	if test -d  $(BUILD_HOME)/$(Package)/test; then \
		$(CopyDir)  $(BUILD_HOME)/$(Package)/test $(ReleaseDir)/$(Project)/$(Package); \
        fi
	$(CopyDir)  $(BUILD_HOME)/$(Package)/src $(ReleaseDir)/$(Project)/$(Package)
	@find $(ReleaseDir)/$(Project)/$(Package) -name "*.d" -exec rm {} \;
	@find $(ReleaseDir)/$(Project)/$(Package) -name "*.o" -exec rm {} \;

endif

#
# tidy-up
#
	@find $(ReleaseDir)/$(Project)/$(Package) -name "CVS" -exec rm -rf {} \; -print 2>&1  | cat > /dev/null

	$(Print) Release done.	

# endif of C++ release code
endif

# ReleaseDir
endif


#----------------------------------------------------------------------------
# If there is an EXECUTABLES = name.exe entry in the Makefile,
# create a dependency: name.exe: name.o
# Therefore trigger the build of name.o from name.cc or .c and link the .o file (@:.exe=.o) with 
# with the indicated libraries.
#----------------------------------------------------------------------------


$(PackageTargetDir)/%.exe : $(PackageTargetDir)/%.o $(StaticLibraryName) $(DynamicLibraryName) 
	$(MakeDir) $(PackageBinDir)
	$(LD) $(ExecutableLinkFlags) $(UserExecutableLinkFlags) -o  $@  $(@:.exe=.o) $(StaticLibraryName) $(LibraryPaths) \
$(LocalDynamicLibrary) $(LibraryList)
	$(Copy) $@ $(PackageBinDir)

#----------------------------------------------------------------------------
# Depency rules to create the object files using the vpath to look for the sources
# Look for the target in the directory OS/PLATFORM. If it doesnt exist build
# it and put the object file in the platform dependent directory.
#----------------------------------------------------------------------------
$(PackageTargetDir)/%.o:%.cc
	$(CXX)  $(CCFlags) $(UserCCFlags) $(CCDefines) $(IncludePaths) -c -o $@ $<

$(PackageTargetDir)/%.o:%.cpp
	$(CXX)  $(CCFlags) $(UserCCFlags) $(CCDefines)  $(IncludePaths) -c -o $@ $<

$(PackageTargetDir)/%.o:%.cxx
	$(CXX)  $(CCFlags) $(UserCCFlags) $(CCDefines)  $(IncludePaths) -c -o $@ $<

$(PackageTargetDir)/%.o:%.c
	$(CC)  $(CFlags) $(UserCFlags) $(CDefines)  $(IncludePaths) -c -o $@ $<

$(PackageTargetDir)/%.o:%.S
	$(CC)  $(CFlags) $(UserCFlags) $(CDefines)  $(IncludePaths) -c -o $@ $<

#----------------------------------------------------------------------------
# Rules for building Java classes and jar files
#----------------------------------------------------------------------------


#
# Look for a b c and replace it by java/a/*.class java/b/*.class java/c/*.class
#
JavaClasses 	= $(JavaSources:%.java=$(SourceDir)/$(Package)/%.class)
#JavaClassesRel  = $(JavaSources:%.java=$(Package)/%.class)

$(SourceDir)/$(Package)/%.class: $(SourceDir)/$(Package)/%.java
	$(JavaCompiler) $(JavaFlags) $<

#%.class : $(SourceDir)/$(JavaPackage)/%.java
#	@$(MAKE) $(MakeOptions) $(JavaTargetDir)/$@	

ifdef JavaLibrary
JavaLibraryName = $(JavaLibrary).jar
endif

%.jar: $(JavaClasses)
	$(Print) $@
	$(Print) JavaClasses: $(JavaClasses)
	$(MakeDir) $(SourceDir)/$(Package)/icons
	$(MakeDir) $(SourceDir)/$(Package)/audio
	$(MakeDir) $(SourceDir)/$(Package)/META-INF/services
	cd $(SourceDir); jar -cvf $@ $(Package)/*.class $(Package)/icons $(Package)/audio $(Package)/META-INF/services

#%.jar: $(JavaClasses)
#	$(MAKE) $(MakeOptions) $(JavaTargetDir)/$@


#jar : $(JavaLibraryName)



#-------------------------------------------------------------------------------------
# Install XDAQ application executables/libraries info BUILD_HOME/{include,bin,lib}
#-------------------------------------------------------------------------------------

# Recursive install in all packages
install : $(PackageListLoop)

# Forced install of executables and libraries ever created in the build process
_installall: _buildall
ifndef Packages
	$(Install) -d $(LibInstallDir) $(BinInstallDir) $(IncludeInstallDir) $(htdocsInstallDir)
ifeq ($(XDAQ_OS),macosx)
	cd include; find . \( -name "*.[hi]" -o -name "*.hpp" -o -name "*.hh" -o -name "*.hxx" \) -exec dirname $(IncludeInstallDir)/{} \; | xargs -n1 mkdir -p 
	cd include; find . \( -name "*.[hi]" -o -name "*.hpp" -o -name "*.hh" -o -name "*.hxx" \) -exec cp {} $(IncludeInstallDir)/{} \;
else
	cd include; find ./ \( -name "*.[hi]" -o -name "*.hpp" -o -name "*.hh" -o -name "*.hxx" \) -exec install -p -D {} $(IncludeInstallDir)/{} \;
endif
ifeq (exists,$(shell [ -d $(ImagesDir) ] && echo exists))
	$(Print) Installing image files.
	$(MakeDir) $(htdocsInstallDir)/$(Package)/images
	cd $(ImagesDir); find ./ -name ".svn" -prune -o -name "*" -type f -exec install -D -m 655 {} $(htdocsInstallDir)/$(Package)/images/{} \;
endif
ifeq (exists,$(shell [ -d $(HTMLDir) ] && echo exists))
	$(Print) Installing html files.
	$(MakeDir) $(htdocsInstallDir)/$(Package)/html
	cd $(HTMLDir); find ./ -name ".svn" -prune -o -name "*" -type f -exec install -D -m 655 {} $(htdocsInstallDir)/$(Package)/html/{} \;
endif
ifeq (exists,$(shell [ -d $(ScriptsDir) ] && echo exists))
	$(Print) Installing scripts files.
	$(MakeDir) $(scriptsInstallDir)/$(Package)/scripts
	cd $(ScriptsDir); find ./ -name ".svn" -prune -o -name "*" -type f -exec install -D -m 655 {} $(scriptsInstallDir)/$(Package)/scripts/{} \;
endif
ifeq (exists,$(shell [ -d $(XMLDir) ] && echo exists))
	$(Print) $(XMLDIR) Installing xml files.
	$(MakeDir) $(htdocsInstallDir)/$(Package)/xml
	cd $(XMLDir); find ./ -name ".svn" -prune -o -name "*" -type f -exec install -D -m 655 {} $(htdocsInstallDir)/$(Package)/xml/{} \;
endif
ifdef StaticLibraryName
	$(Print) Installing static library.
	$(Install) -p $(StaticLibraryName) $(LibInstallDir)/
endif
ifdef DynamicLibraryName
	$(Print) Installing dynamic library.
	$(Copy) -p $(DynamicLibraryName)* $(LibInstallDir)/
endif
ifdef Executables
	$(Print) Installing executable binaries.
	$(Install) -p $(PackageBinDir)/*.exe $(BinInstallDir)/
endif
# Packages
endif


#----------------------------------------------------------------------------
# Cleaning up build directories
#----------------------------------------------------------------------------

# Recursive clean in all packages
clean : $(ReversePackageListLoop)

# Forced clean of everything ever created in the build process
_cleanall:
ifdef JavaLibrary
	$(Delete) *.class \
	$(SourceDir)/$(JavaLibraryName)
else
	$(Delete) $(PackageLibDir)/*{.so,.a,.dylib}
	$(Delete) $(TargetObjects:.o=.d)
	$(Delete) $(TargetObjects)
	$(Delete) $(TargetMainModules:.exe=.d) 
	$(Delete) $(TargetMainModules:.exe=.o) 
	$(Delete) $(TargetMainModules)
endif

#$(PackageTargetDir)/*{.o,.d,.exe} \
#$(SourceDir)/$(Package)/test/$(XDAQ_OS)/$(XDAQ_PLATFORM)/*{.o,.d} \


#----------------------------------------------------------------------------
# Cleaning up test directories
#----------------------------------------------------------------------------

# Recursive clean in all packages
testsclean : $(ReversePackageListLoop)

# Forced clean of everything ever created in the build process
_testscleanall:
	$(Delete) $(DynamicLibraryName)
	$(Delete) $(StaticLibraryName)
	$(Delete) $(TargetObjects:.o=.d)
	$(Delete) $(TargetObjects)
	$(Delete) $(TargetMainModules:.exe=.d) 
	$(Delete) $(TargetMainModules)


#----------------------------------------------------------------------------
# Building all packages
#----------------------------------------------------------------------------

build: $(PackageListLoop)

_all: _buildall

_buildall: \
	$(TargetDependencies) \
	$(TargetMainModules) \
	$(DynamicLibraryName) \
	$(StaticLibraryName) \
	$(JavaLibraryName) \
	$(JavaClasses)
	@echo Build done. $(JavaLibraryName)


tests: 	 $(PackageListLoop)

_testsall: _buildall


#----------------------------------------------------------------------------
# Execute programs in bin/$(XDAQ_OS)/$(XDAQ_PLATFORM)/
#----------------------------------------------------------------------------

run:
	LD_LIBRARY_PATH="$(subst $(space),:,$(LibDir) $(LibraryDirs) $(TestLibraryDirs))" $(BinDir)/$(Program)

debug:
	LD_LIBRARY_PATH="$(subst $(space),:,$(LibDir) $(LibraryDirs) $(TestLibraryDirs))" gdb --args $(BinDir)/$(Program)


#----------------------------------------------------------------------------
# Automatic dependency generation
#----------------------------------------------------------------------------
# The sed command replace in the dependency file the simple 
# filename.o: 
# with the fully qualified 
# path/filename.o path/filename.d: 
#
$(PackageTargetDir)/%.d:%.cc
	$(MakeDir) $(@D)
	$(SHELL) -ec '$(CXX)  ${DependencyFlags} $(CCFlags) $(UserCCFlags) $(CCDefines) $(IncludePaths)  $< > $@' 
	@sed -e 's/\($(subst /,\/,$(*F))\)\.o[:]*/$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g' $@ >>  $@.bak
	@mv $@.bak $@
	@cp -f $@ $@.tmp
	@sed -e 's/.*://' -e 's/\\$$//' < $@.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $@
	@rm -f $@.tmp

$(PackageTargetDir)/%.d:%.cpp
	$(MakeDir)  $(@D)
	$(SHELL) -ec '$(CXX) ${DependencyFlags} $(CCFlags) $(UserCCFlags) $(CCDefines) $(IncludePaths)  $< > $@' 
	@sed -e 's/\($(subst /,\/,$(*F))\)\.o[:]*/$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g' $@ >>  $@.bak
	@mv $@.bak $@
	@cp -f $@ $@.tmp
	@sed -e 's/.*://' -e 's/\\$$//' < $@.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $@
	@rm -f $@.tmp

$(PackageTargetDir)/%.d:%.cxx
	$(MakeDir)  $(@D)
	$(SHELL) -ec '$(CXX) ${DependencyFlags} $(CCFlags) $(UserCCFlags) $(CCDefines) $(IncludePaths)  $< > $@' 
	@sed -e 's/\($(subst /,\/,$(*F))\)\.o[:]*/$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g' $@ >>  $@.bak
	@mv $@.bak $@
	@cp -f $@ $@.tmp
	@sed -e 's/.*://' -e 's/\\$$//' < $@.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $@
	@rm -f $@.tmp

$(PackageTargetDir)/%.d:%.c
	$(MakeDir)  $(@D)
	$(SHELL) -ec '$(CC) ${DependencyFlags} $(CFlags) $(UserCFlags) $(CDefines) $(IncludePaths)  $< > $@' 
	@sed -e 's/\($(subst /,\/,$(*F))\)\.o[:]*/$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g' $@ >>  $@.bak
	@mv $@.bak $@	
	@cp -f $@ $@.tmp
	@sed -e 's/.*://' -e 's/\\$$//' < $@.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $@
	@rm -f $@.tmp


$(PackageTargetDir)/%.d:%.S
	$(MakeDir)  $(@D)
	$(SHELL) -ec '$(CC) ${DependencyFlags} $(CFlags) $(UserCFlags) $(CDefines) $(IncludePaths)  $< > $@' 
	@sed -e 's/\($(subst /,\/,$(*F))\)\.o[:]*/$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g' $@ >>  $@.bak
	@mv $@.bak $@	
	@cp -f $@ $@.tmp
	@sed -e 's/.*://' -e 's/\\$$//' < $@.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $@
	@rm -f $@.tmp


#---------------------------------------------------------------------------- 
#  
# Include the dependency files:
#
# include for each .o file the corresponding .d file.
# the same for all .exe files
#
# -include has to be written to continue the execution of the make
# even though the dependency files do not yet exist.
#
#----------------------------------------------------------------------------

MAKEFILE_TARGETS_WITHOUT_INCLUDE := clean _cleanall rpm _rpmall cleanrpm _cleanrpmall

# Include only if the goal needs it
ifeq ($(filter $(MAKECMDGOALS),$(MAKEFILE_TARGETS_WITHOUT_INCLUDE)),)

ifdef TargetObjects
-include $(TargetObjects:.o=.d) 
endif

ifdef Executables
-include $(TargetMainModules:.exe=.d)
endif

endif

#----------------------------------------------------------------------------
# End of File
#
#----------------------------------------------------------------------------
