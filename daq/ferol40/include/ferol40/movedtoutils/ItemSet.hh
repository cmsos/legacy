#ifndef __ItemSet
#define __ItemSet

class utils::ItemSet {
public:
    utils::ItemSet();
    
private:
    typedef struct {
        std::string name;
        InfospaceHandler *is;
        std::string formatString;
    } isItem;
    std::list< isItem > itemList;
};

#endif /* __ItemSet */
