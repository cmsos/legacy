package xdaq.xdesk.jfinder;

import javax.swing.JLabel;
import javax.swing.event.*;
import java.awt.event.*;
import java.util.*;

public class FinderSelectionEvent extends EventObject
{
	JLabel[] oldSelection_ = null;
	JLabel[] newSelection_ = null;
	
	public FinderSelectionEvent (Object source, JLabel[] oldSelection, JLabel[] newSelection)
	{
		super(source);
		oldSelection_ = oldSelection;
		newSelection_ = newSelection;
	}
	
	public JLabel getNewSelectionCell ()
	{
		if (newSelection_.length > 0)
		{
			return newSelection_[0];
		} else
		{
			return null;
		}
	}
	
	public JLabel getOldSelectionCell ()
	{
		if (oldSelection_.length > 0)
		{
			return oldSelection_[0];
		} else
		{
			return null;
		}
	}
	
	public JLabel[] getNewSelection ()
	{
		return newSelection_;
	}
	
	public JLabel[] getOldSelection ()
	{
		return oldSelection_;
	}
	
	public int getNewSelectionCellCount ()
	{
		return newSelection_.length;
	}
	
	public int getOldSelectionCellCount ()
	{
		return oldSelection_.length;
	}
	
}
