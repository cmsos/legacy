#include "SerializerTestSuite.h"

CPPUNIT_TEST_SUITE_REGISTRATION( SerializerTestSuite );

void SerializerTestSuite::setUp()
{
 
}

void SerializerTestSuite::tearDown()
{
}

void SerializerTestSuite::loader()
{
	xdata::XMLDOM utils;
	
	try{ 
		DOMDocument * doc = utils.load("../../data/loaderTestOK.xml");
	} 
	catch (xdata::Exception & e )
	{
		cout << "Error in loading" << e.what() << endl;
	
	}	
	
}

