#ifndef _tcds_utils_InfoSpaceHandlerLocker_h_
#define _tcds_utils_InfoSpaceHandlerLocker_h_

#include "xdata/InfoSpace.h"

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {

    class InfoSpaceHandlerLocker
    {

    public:
      InfoSpaceHandlerLocker(tcds::utils::InfoSpaceHandler const* const infoSpaceHandler);
      ~InfoSpaceHandlerLocker();

    private:
      void lock() const;
      void unlock() const;

      tcds::utils::InfoSpaceHandler const* const infoSpaceHandler_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_InfoSpaceHandlerLocker_h_
