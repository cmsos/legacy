// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _nodejs_HTTPSender_h
#define _nodejs_HTTPSender_h

#include <node.h>
#include <node_object_wrap.h>

using v8::Persistent;
using v8::Function;

#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "pt/http/SOAPMessenger.h"

#include "toolbox/BSem.h"


#include "log4cplus/logger.h"

using namespace log4cplus;

namespace nodejs
{

class HTTPSender: public pt::PeerTransportSender
{
	public:
	
	HTTPSender(Logger & logger);
	
	virtual ~HTTPSender();
	
	pt::TransportType getType();
	
	pt::Address::Reference createAddress( const std::string& url, const std::string& service )
		throw (pt::exception::InvalidAddress);
	
	pt::Address::Reference createAddress( std::map<std::string, std::string, std::less<std::string> >& address )
		throw (pt::exception::InvalidAddress);
	
	std::string getProtocol();
	
	std::vector<std::string> getSupportedServices();
	
	bool isServiceSupported(const std::string& service );
	
	pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local)
		throw (pt::exception::UnknownProtocolOrService);
	
	protected:
		
 	toolbox::BSem * sync_;
	toolbox::BSem * mutex_;
	Logger logger_;

	public:
	//https://nodeaddons.com/c-processing-from-node-js-part-4-asynchronous-addons/#comment-2728737416
	//https://www.codeschool.com/discuss/t/how-do-i-return-a-value-from-my-node-js-module-after-it-has-been-updated-by-an-async-function/27392
	Persistent< Function > callback_;

};

}
#endif
