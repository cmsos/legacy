#ifndef _tcds_hwlayer_version_h_
#define _tcds_hwlayer_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSHWLAYER_VERSION_MAJOR 3
#define TCDSHWLAYER_VERSION_MINOR 13
#define TCDSHWLAYER_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSHWLAYER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSHWLAYER_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSHWLAYER_VERSION_CODE PACKAGE_VERSION_CODE(TCDSHWLAYER_VERSION_MAJOR,TCDSHWLAYER_VERSION_MINOR,TCDSHWLAYER_VERSION_PATCH)
#ifndef TCDSHWLAYER_PREVIOUS_VERSIONS
#define TCDSHWLAYER_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSHWLAYER_VERSION_MAJOR,TCDSHWLAYER_VERSION_MINOR,TCDSHWLAYER_VERSION_PATCH)
#else
#define TCDSHWLAYER_FULL_VERSION_LIST TCDSHWLAYER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSHWLAYER_VERSION_MAJOR,TCDSHWLAYER_VERSION_MINOR,TCDSHWLAYER_VERSION_PATCH)
#endif

namespace tcdshwlayer {

  const std::string package = "tcdshwlayer";
  const std::string versions = TCDSHWLAYER_FULL_VERSION_LIST;
  const std::string description = "CMS TCDS helper software.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "Part of the CMS TCDS software.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();

} // namespace tcdshwlayer

#endif
