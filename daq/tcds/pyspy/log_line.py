#!/usr/bin/env python

from log_entry import FIRST_BX
from log_entry import LAST_BX

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

# The number of bunch crossings in a single orbit.
NUM_BX_PER_ORBIT = (LAST_BX - FIRST_BX) + 1

###############################################################################

class LogLine(object):

    TTC_TRAFFIC_TYPE_L1A = 0x1
    TTC_TRAFFIC_TYPE_BCOMMAND_SHORT = 0x2
    TTC_TRAFFIC_TYPE_BCOMMAND_LONG = 0x3

    # Mapping of B-go numbers (i.e., the data value in TCDS-mode) and
    # B-go names.
    BGO_NAMES = {
        0 : "LumiNibble",
        1 : "BC0",
        2 : "TestEnable",
        3 : "PrivateGap",
        4 : "PrivateOrbit",
        5 : "Resync",
        6 : "HardReset",
        7 : "EC0",
        8 : "OC0",
        9 : "Start",
        10 : "Stop",
        11 : "StartOfGap",
        12 : "B-go 12",
        13 : "WarningTestEnable",
        14 : "B-go 14",
        15 : "B-go 15"
        }

    def __init__(self,
                 traffic_type,
                 timestamp,
                 bx_number,
                 data=0x0,
                 address=0x0,
                 sub_address=0x0,
                 address_external=0x0,
                 raw_data=None,
                 tcds_mode=False) :
        self.traffic_type = traffic_type
        self.timestamp = timestamp
        self.bx_number = bx_number
        self.data = data
        self.address = address
        self.sub_address = sub_address
        self.address_external = address_external
        self.raw_data = raw_data
        self._tcds_mode = tcds_mode
        # End of __init__().

    def __cmp__(self, other):
        res = cmp(self.timestamp, other.timestamp)
        # End of __cmp__().
        return res

    def __str__(self):
        # The timestamp in the TTCSpy memory is the number of bunch
        # crossings with respect to the first entry in memory.
        delta_bx = self.timestamp
        num_orbits = delta_bx / NUM_BX_PER_ORBIT
        num_bx = delta_bx % NUM_BX_PER_ORBIT
        delta_str_pieces = []
        if num_orbits > 0:
            delta_str_pieces.append("{0:d} orbit".format(num_orbits))
            if num_orbits > 1:
                delta_str_pieces.append("s")
        if (num_bx > 0) or \
           (not len(delta_str_pieces)):
            if num_bx > 0:
                if len(delta_str_pieces):
                    delta_str_pieces.append(" + ")
            delta_str_pieces.append("{0:d} BX".format(num_bx))
        delta_str = "".join(delta_str_pieces)
        bx_num = self.bx_number
        bx_str = "in BX {0:d}".format(bx_num)
        timestamp_str = "{0:s}, {1:s}".format(delta_str, bx_str)
        info_str_pieces = ["at {0:s}".format(timestamp_str)]
        is_l1a = self.is_l1a()
        is_bcommand = self.is_bcommand()
        type_str = "unknown"
        details = None
        if is_l1a:
            type_str = "L1A"
        elif is_bcommand:
            is_broadcast = self.is_bcommand_short()
            data = self.get_bcommand_data()
            is_bcnt_reset = self.is_bcnt_reset()
            is_evcnt_reset = self.is_evcnt_reset()
            if is_broadcast:
                type_str = "broadcast command"
                details = "data=0x{0:x}"
                details = details.format(data)
            else:
                type_str = "addressed command"
                address = self.get_long_bcommand_address()
                sub_address = self.get_long_bcommand_subaddress()
                is_external = self.get_long_bcommand_external_flag()
                ext_str = "internal"
                if is_external:
                    ext_str = "external"
                details = "address=0x{0:04x}-0x{1:02x}, {2:s}, data=0x{3:x}"
                details = details.format(address, sub_address, ext_str, data)

            if self._tcds_mode:
                if is_broadcast:
                    bgo_name = LogLine.BGO_NAMES.get(data, "unknown B-go")
                    details = "{0:s}, {1:s}".format(details, bgo_name)
            else:
                if is_broadcast:
                    if is_bcnt_reset:
                        details = "{0:s}, BCntRst".format(details)
                    if is_evcnt_reset:
                        details = "{0:s}, EvCntRst".format(details)
        else:
            type_str = "unknown"
        info_str_pieces.append(type_str)
        if not details is None:
            info_str_pieces.append(details)
        res = "{0:s}: {1:s}".format(info_str_pieces[0], ", ".join(info_str_pieces[1:]))
        # End of __str__().
        return res

    def is_l1a(self):
        return (self.traffic_type == LogLine.TTC_TRAFFIC_TYPE_L1A)

    def is_bcommand(self):
        return (self.is_bcommand_short() or self.is_bcommand_long())

    def is_bcommand_short(self):
        return (self.traffic_type == LogLine.TTC_TRAFFIC_TYPE_BCOMMAND_SHORT)

    def is_bcommand_long(self):
        return (self.traffic_type == LogLine.TTC_TRAFFIC_TYPE_BCOMMAND_LONG)

    def get_bcommand_data(self):
        return self.data

    def is_bcnt_reset(self):
        return (self.is_bcommand_short() and ((self.data & 0x1) != 0x0));

    def is_evcnt_reset(self):
        return (self.is_bcommand_short() and ((self.data & 0x2) != 0x0));

    def get_long_bcommand_address(self):
        return self.address

    def get_long_bcommand_subaddress(self):
        return self.sub_address

    def get_long_bcommand_external_flag(self):
        return self.address_external

    def raw_data_as_string(self):
        words = [0, 0, 0, 0]
        words[0] = (self.raw_data >> 96) & 0xffffffff
        words[1] = (self.raw_data >> 64) & 0xffffffff
        words[2] = (self.raw_data >> 32) & 0xffffffff
        words[3] = self.raw_data & 0xffffffff
        res = "0x{0[0]:0>8x} 0x{0[1]:0>8x} 0x{0[2]:0>8x} 0x{0[3]:0>8x} ".format(words)
        # End of raw_data_as_string().
        return res

    def subtract_offset(self, offset):
        self.timestamp -= offset
        # End of subtract_offset().

    # End of class LogLine.

###############################################################################
