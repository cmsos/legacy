#ifndef _tcds_cpm_HwStatusInfoSpaceHandler_h_
#define _tcds_cpm_HwStatusInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace cpm {

    class HwStatusInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                               tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwStatusInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_HwStatusInfoSpaceHandler_h_
