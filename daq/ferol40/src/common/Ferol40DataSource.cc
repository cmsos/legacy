#include <sstream>
#include <vector>
#include "ferol40/Ferol40DataSource.hh"
#include "d2s/utils/Exception.hh"
#include "ferol40/ferol40Constants.h"
#include "ferol40/loggerMacros.h"

ferol40::Ferol40DataSource::Ferol40DataSource(HAL::HardwareDeviceInterface *device_P,
					      utils::InfoSpaceHandler &appIS,
					      Logger logger)
    : OpticalDataSource(device_P, appIS, logger)
{
}

void ferol40::Ferol40DataSource::setDataSource() const
{
    //JRF Loop over all streams
    uint32_t stream_offset(0), index(0);
    for (std::vector<bool>::const_iterator streams_it = streams_.begin(); streams_it != streams_.end(); ++streams_it)
    {
	index = streams_it - streams_.begin();
	stream_offset = (index)*0x4000;

	if (dataSource_[index] == GENERATOR_SOURCE) //JRF TODO do we want to be able to mix modes? i.e. use one stream in generator and one in real? If yes, then we need a vector of dataSources_
	{
	    //JRF TODO put this in a loop. We need to have info about which streams are enabled and which are not.
	    device_P->setBit("select_emulator", HAL::HAL_NO_VERIFY, stream_offset);

	    DEBUG("Setting select_emulator to Emulator for Ferol40-Generator source operation");
	}
	else if ((dataSource_[index] == L10G_SOURCE) || (dataSource_[index] == L10G_CORE_GENERATOR_SOURCE))
	{
	    device_P->resetBit("select_emulator", HAL::HAL_NO_VERIFY, stream_offset);
	    DEBUG("Setting select_emulator to 0 for 10G-input operation");

	    this->setup10GInput(index); //this method this also resyncs i.e. cleans the input fifos.
	}
	else
	{
	    std::stringstream msg;
	    msg << "Encountered illegal DataSource for operationMode \""
		<< operationMode_
		<< "\" : \""
		<< dataSource_[index]
		<< "\". Cannot continue!";
	    ERROR(msg);
	    XCEPT_RAISE(utils::exception::Ferol40Exception, msg.str());
	}
    } //end for loop
}

ferol40::Ferol40DataSource::~Ferol40DataSource()
{
}
