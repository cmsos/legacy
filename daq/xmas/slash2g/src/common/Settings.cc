// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/slash2g/Settings.h"
#include <sstream>

xmas::slash2g::Settings::Settings(DOMNode* flashNode) 
{
	// name of the flashlist
	std::string name = xoap::getNodeAttribute(flashNode, "name");
	this->setProperty("name", name);
	
	std::string hashkey = xoap::getNodeAttribute(flashNode, "hashkey");
	this->setProperty("hashkey", hashkey);
	
	std::string clear = xoap::getNodeAttribute(flashNode, "clear");
        if (clear == "")
        {
            clear = "true";
        }	
	this->setProperty("clear", clear);

	std::string tag = xoap::getNodeAttribute(flashNode, "tag");
	this->setProperty("tag", tag);
	
	// this attribute is optional
	std::string filter = xoap::getNodeAttribute(flashNode, "filter");
	this->setProperty("filter", filter);		
}

xmas::slash2g::Settings::Settings()
	
{
}

xmas::slash2g::Settings::~Settings()
{
}

toolbox::net::UUID& xmas::slash2g::Settings::getId()
{
	return id_;
}
