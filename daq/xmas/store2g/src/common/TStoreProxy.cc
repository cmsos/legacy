// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/


#include <algorithm>
#include "xmas/store2g/TStoreProxy.h"
#include "tstore/api/NS.h"
#include "tstore/api/InsertNested.h"
#include "tstore/api/Disconnect.h"
#include "tstore/api/Connect.h"
#include "tstore/api/Renew.h"
#include "tstore/api/ConnectResponse.h"
#include "tstore/api/admin/GetViews.h"
#include "tstore/api/admin/GetViewsResponse.h"
#include "tstore/api/admin/AddNestedView.h"
#include "tstore/api/admin/Sync.h"
#include "tstore/api/admin/SetConfiguration.h"
#include "tstore/api/admin/GetNestedViewConfiguration.h"
#include "tstore/api/admin/GetConfigurationResponse.h"
#include "tstore/api/ClearNested.h"
#include "xdaq/XceptSerializer.h"



xmas::store2g::TStoreProxy::TStoreProxy
(
	xdaq::Application * owner,
	DescriptorsCache * descriptorsCache
)
	: 
	xdaq::Object(owner), 
	descriptorsCache_(descriptorsCache), 
	isConnected_(false), 
	connectionId_(""),  
	authentication_ (""), 
	credentials_(""),
        timeout_("PT600S"),
	connections_(0)
{
}

bool xmas::store2g::TStoreProxy::isConnected()
{
	return isConnected_;
}

std::string xmas::store2g::TStoreProxy::getConnectionId()
{
	return connectionId_;
}

void xmas::store2g::TStoreProxy::configureView
(
	xmas::FlashListDefinition * definition,
	const std::string& storeGroup,
	const std::string& storeView
)
	
{
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");
	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		std::string zone = this->getOwnerApplication()->getApplicationContext()->getDefaultZoneName();
		std::string service = this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service");	
                std::string tstoreViewConf = service + "_" + storeView +"_" + zone;

		tstore::api::admin::GetNestedViewConfiguration getConf("",tstoreViewConf);
		xoap::MessageReference msg = getConf.toSOAP();

		//std::cout << "GetConfiguration request: " << std::endl;
		//msg->writeTo(std::cout);

		try
		{
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Get TStore configuration '" << tstoreViewConf << "'");
			const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			xoap::MessageReference reply = 
				this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );

			if (!reply.isNull())
			{
				xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
				if (rb.hasFault())
				{
					// The remote application failed. It is an XDAQ application and therefore
					// embeds an XML serialized exception in the SOAP Fault detail field
					//
					std::stringstream msg;
					msg << "Failed to get configuration '" << tstoreViewConf << "' from remote TStore";
					if ( rb.getFault().hasDetail() )
					{
						xoap::SOAPElement detail = rb.getFault().getDetail();
						xcept::Exception rae;					
						xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
						XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
					}
					else
					{
						msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
						XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
					}
				}
				else
				{
					//std::cout << "****** GetConfigurtion response BEGIN " << std::endl;
					//reply->writeTo(std::cout);
					//std::cout << std::endl;
					//std::cout << "****** GetConfigurtion response END " << std::endl;
					
					// edit configuration
					tstore::api::admin::GetConfigurationResponse gcr(reply);
					tstore::api::admin::SetConfiguration setConf(tstore::api::nested::NamespaceUri + ":" + tstoreViewConf, "urn:view-class", "");
					setConf.setConfiguration(gcr.getConfiguration());
					tstore::api::admin::Configuration & configuration = setConf.getConfiguration();
					
					// check if table already exists, remove it since it might be a new version
					toolbox::net::URN nameURN(definition->getProperty("name"));
					std::string name = nameURN.getNSS();
					if ( configuration.hasTableDefinition(name) )
					{						
						configuration.removeTableDefinition(name); // and associated nested tables
					}
					
					this->addTableDefinition(configuration.getDocument() , definition );
					
					
					// meta data for this flashlist
					if ( configuration.hasTableDefinition(name+ "_dwell") )
					{						
						configuration.removeTableDefinition(name+ "_dwell"); // and associated nested tables
					}
					
					this->addDwellTableDefinition(configuration.getDocument() , definition );
										
					xoap::MessageReference msg = setConf.toSOAP();
					
					//std::cout << "******* configureView SetConfiguration request BEGIN " << std::endl;
					//msg->writeTo(std::cout);
					//std::cout << std::endl;
					//std::cout << "******* configureView SetConfiguration request END " << std::endl;
					
					LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Set table '" << name << "' into TStore configuration '" << tstoreViewConf << "'");

					reply = this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );
					if (!reply.isNull())
					{
						xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
						if (rb.hasFault())
						{
							// The remote application failed. It is an XDAQ application and therefore
							// embeds an XML serialized exception in the SOAP Fault detail field
							//
							std::stringstream msg;
							msg << "Failed to set table '" << name << "' into configuration '" << tstoreViewConf << "' of remote TStore";
							if ( rb.getFault().hasDetail() )
							{
								xoap::SOAPElement detail = rb.getFault().getDetail();					
								xcept::Exception rae;
								xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
								XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
							}
							else
							{
								msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
								XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
							}
						}
						else
						{
							return;
						}
					}
				}	
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			XCEPT_RETHROW(xmas::store2g::exception::Exception, "failed to insert flashlist", e);
		}
	}
}


bool xmas::store2g::TStoreProxy::hasTableDefinition
(
	xmas::FlashListDefinition * definition,
	const std::string& storeGroup,
	const std::string& storeView
)
	
{
	toolbox::net::URN flashlistURN(definition->getProperty("name"));
	std::string  name = toolbox::toupper (flashlistURN.getNSS());
					
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");
	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::FailedToCheckTable, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::FailedToCheckTable, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		std::string zone = this->getOwnerApplication()->getApplicationContext()->getDefaultZoneName();
		std::string service = this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service");
		std::string tstoreViewConf = service + "_" + storeView +"_" + zone;	
				
		tstore::api::admin::GetNestedViewConfiguration getConf("",tstoreViewConf);
		xoap::MessageReference msg = getConf.toSOAP();

		//std::cout << "hasTableDefinition GetConfiguration request: " << std::endl;
		//msg->writeTo(std::cout);
		//std::cout << std::endl;
		try
		{
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Check if TStore configuration '" << tstoreViewConf << "' has table definition '" << name << "'");

			const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			xoap::MessageReference reply = 
				this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );

			if (!reply.isNull())
			{
				xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
				if (rb.hasFault())
				{
					// The remote application failed. It is an XDAQ application and therefore
					// embeds an XML serialized exception in the SOAP Fault detail field
					//
					std::stringstream msg;
					msg << "Failed to check if configuration '" << tstoreViewConf << "' of remote TStore has table '" << name << "'";
					if ( rb.getFault().hasDetail() )
					{
						xoap::SOAPElement detail = rb.getFault().getDetail();					
						xcept::Exception rae;
						xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
						XCEPT_RETHROW (xmas::store2g::exception::FailedToCheckTable, msg.str(), rae );
					}
					else
					{
						msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
						XCEPT_RAISE (xmas::store2g::exception::FailedToCheckTable, msg.str());
					}
				}
				else
				{
					//std::cout << "hasTableDefinition GetConfigurtion response: " << std::endl;
					//reply->writeTo(std::cout);
					//std::cout << std::endl;
					
					// edit configuration
					tstore::api::admin::GetConfigurationResponse gcr(reply);
					
					bool found = gcr.getConfiguration().hasTableDefinition(name);
					
					return found;

				}	
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to check if configuration '" << tstoreViewConf << "' of remote TStore has table '" << name << "'";
			XCEPT_RETHROW(xmas::store2g::exception::FailedToCheckTable, msg.str(), e);
		}
	}
	return false;
}



void  xmas::store2g::TStoreProxy::validate
(
	xmas::FlashListDefinition * definition,
	const std::string& storeGroup,
	const std::string& storeView,
	const std::string& database
)
	
{
	toolbox::net::URN flashlistURN(definition->getProperty("name"));
	std::string  name = toolbox::toupper (flashlistURN.getNSS());
	
	if (!this->isConnected())
	{		
		try
		{
			if (!this->hasView(storeGroup,storeView))
			{
				this->createView(storeGroup,storeView, database);				
			}
			
			this->connect(storeGroup,storeView);

			// sync from for flashlist-metadata  if not available create it and sync to DB
			 if (!this->hasMetaData(storeGroup,database))
                        {
                                this->createMetaData(storeGroup, database);
                        }


		}
		catch (xmas::store2g::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to validate flashlist '" << name << "' in database '" << database << ", disabling storage";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}
	}
	
	// already connected
	// verify that table definition for this flash list is already available
	try
	{
		// synchronize from data base
		this->syncFromDB(definition,storeGroup);
	}
	catch (xmas::store2g::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to validate definition for flashlist '" << name << "' with database '" << database << "', disabling storage";
		XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
	}
		
	try
	{
		// must be present both in metadata and configuration
		if ( ! this->hasTableDefinition(definition,storeGroup,storeView) )
		{
			// configure (create) table defintion for this flash list
			this->configureView( definition, storeGroup,storeView);

			// synchronize to DB
			this->syncToDB(definition, storeGroup);

                        this->insertMetaData(definition, storeGroup, database);

		}
	}
	catch (xmas::store2g::exception::IncompatibleDefinition & e)
	{
		std::stringstream msg;
		msg << "Failed to validate definition for flashlist '" << name << "' with database '" << database << "', disabling storage";
		XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
	}
	catch (xmas::store2g::exception::FailedToCheckTable & e)
	{
		std::stringstream msg;
		msg << "Failed to validate definition for flashlist '" << name << "' with database '" << database << "', disabling storage";
		XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
	}
}
				
void xmas::store2g::TStoreProxy::insert 
(
	xmas::store2g::Settings::Properties & p, 
	xdata::Table::Reference& table, 
	const std::string& storeGroup, 
	const std::string& storeView, 
	const std::string& database
)
	
{
	toolbox::net::URN flashlistURN(p.definition->getProperty("name"));
	std::string  name = toolbox::toupper (flashlistURN.getNSS());

	// Check if flashlist has been vaildated	
	if ( ! p.validated )
	{
		try
		{
			this->validate(p.definition,storeGroup,storeView, database);
		}
		catch (xmas::store2g::exception::Exception & e)
		{			
			std::stringstream msg;
			msg << "Failed to validate flashlist '" << name << "' for database '" << database << "'";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e );		
		}
		p.depth = p.definition->getNestedDepth();
		p.validated = true;
	}
	
	// flashlist insertion into TSTore has been validated therefore insert according configuration
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		try
		{			
			// clear data before inserting, if 'insert' mode
			if ( p.mode == "instant"  )
			{
				try
				{
					this->clearTable (descriptors, flashlistURN.getNSS());
				}
				catch (xmas::store2g::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to clear flashlist table '" << name << "' into database '" << database << "' before insert in 'instant' mode";
					XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), e );					
				}
			}
			
			
		
			try
			{
				this->insertTable (descriptors, flashlistURN.getNSS(), table, p.depth);
			}
			catch (xmas::store2g::exception::Exception& e)
			{
				std::stringstream logmsg;
				logmsg << "************ Table dump ************" << std::endl;
				table->writeTo(logmsg);
				logmsg << "************************************" << std::endl;
				LOG4CPLUS_DEBUG (this->getOwnerApplication()->getApplicationLogger(), logmsg.str());

				std::stringstream msg;
				msg << "Failed to insert table for flashlist '" << name << "' into database '" << database << "' before insert in 'instant' mode";
				XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), e);
			}
			
			if ( p.mode == "append"  ) // in append mode , automatically reset dwell table
			{
				try
				{
					this->clearTable (descriptors, flashlistURN.getNSS() + "_dwell");
				}
				catch (xmas::store2g::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to clear dwell table for flashlist '" << name << "' into database '" << database << "' before insert in 'instant' mode";
					XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), e );					
				}
			
				try
				{
					// Reinsert the same table, key columns only
					//
					std::set<std::string> keys;	
					if ( p.definition->getProperty("key") != "" )
					{
						keys = toolbox::parseTokenSet( p.definition->getProperty("key"),",");
					}

					std::vector<std::string> columns = table->getColumns();
					for (std::vector<std::string>::iterator ci = columns.begin(); ci != columns.end(); ci++)
					{
						if ( keys.find( *ci ) == keys.end() )
						{ 
							table->removeColumn ( *ci );
						}
					}
					this->insertTable (descriptors, flashlistURN.getNSS() + "_dwell", table, 0);
				}
				catch (xmas::store2g::exception::Exception& e)
				{
					std::stringstream msg;
					msg << "Failed to insert dwell table for flashlist '" << name << "' into database '" << database << "' before insert in 'instant' mode";
					XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), e );					
				}
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to insert flashlist table '" << name << "' into database '" << database << "'";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}
	}
}

void xmas::store2g::TStoreProxy::connect (
	const std::string& storeGroup,
	const std::string& storeView
	)
	
{
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");
	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		std::string zone = this->getOwnerApplication()->getApplicationContext()->getDefaultZoneName();
		std::string service = this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service");
		std::string tstoreViewConf = service + "_" + storeView +"_" + zone;

		tstore::api::Connect connect(tstore::api::nested::NamespaceUri + ":" + tstoreViewConf, authentication_, credentials_ ,timeout_);
		xoap::MessageReference msg = connect.toSOAP();
		try
		{
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Connecting to TStore in zone '" << zone << "' as '" << authentication_ << "'");
			const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			xoap::MessageReference reply = 
				this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );						

			if (!reply.isNull())
			{
				xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
				if (rb.hasFault())
				{
					// The remote application failed. It is an XDAQ application and therefore
					// embeds an XML serialized exception in the SOAP Fault detail field
					//
					std::stringstream msg;
					msg << "Failed to conncect to TStore configuration '" << tstoreViewConf << "' using '" << authentication_ << "' authentication";
					if ( rb.getFault().hasDetail() )
					{
						xoap::SOAPElement detail = rb.getFault().getDetail();					
						xcept::Exception rae;
						xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
						XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
					}
					else
					{
						msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
						XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
					}
				}
				else
				{
					tstore::api::ConnectResponse cr(reply);
					connectionId_ = cr.getConnectionId();
				}
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to conncect to TStore configuration '" << tstoreViewConf << "' using '" << authentication_ << "' authentication";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}
		
		connections_++;
		isConnected_ = true;
	}
}

size_t xmas::store2g::TStoreProxy::getConnectionCounter()
{
	return connections_;
}

void xmas::store2g::TStoreProxy::syncToDB
(
	xmas::FlashListDefinition * definition,
	const std::string& storeGroup
)
	
{
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		std::set<std::string> tables = definition->getTableNames();
		
		// Create a regular expression pattern from the set of table names
		// to serve as a filter for the sync command. Only tables that match
		// the filter IN the DATABASE will be affected by the sync command
		//
		std::string pattern;
		if (tables.size() == 1 )
		{
			//pattern = "^" + toolbox::toupper(*(tables.begin())) + "$";
			
			pattern = "(" + toolbox::toupper(*(tables.begin())) + "|" + toolbox::toupper(*(tables.begin())) + "_dwell)";
		
		}
		else
		{
			pattern = "(";		
			std::set<std::string>::iterator si = tables.begin();
			while (si != tables.end())
			{
				pattern += toolbox::toupper(*si);
				
				if (++si != tables.end())
				{
					pattern += "|";
				}			
			}
			
			toolbox::net::URN def(definition->getProperty("name"));
			pattern += "|";
			pattern += toolbox::toupper(def.getNSS());
			pattern += "_dwell";
			
			pattern += ")";
		}
		
		tstore::api::admin::Sync sync(this->getConnectionId(), "to database", pattern);
		xoap::MessageReference msg = sync.toSOAP();

		//std::cout << "SyncToDB request: " << std::endl;
		//msg->writeTo(std::cout);
		//std::cout << std::endl;

		try
		{
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Sync to TStore with pattern '" << pattern << "'");
			
			const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			xoap::MessageReference reply = 
				this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );						

			if (!reply.isNull())
			{
				//std::cout << "Sync response: " << std::endl;
				//reply->writeTo(std::cout);
				//std::cout << std::endl;
			
				xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
				if (rb.hasFault())
				{
					// The remote application failed. It is an XDAQ application and therefore
					// embeds an XML serialized exception in the SOAP Fault detail field
					//
					std::stringstream msg;
					msg << "Failed to synchronize to database with pattern '" << pattern << "'";
					if ( rb.getFault().hasDetail() )
					{
						xoap::SOAPElement detail = rb.getFault().getDetail();					
						xcept::Exception rae;
						xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
						XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
					}
					else
					{
						msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
						XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
					}
				}
				else
				{
					return;
				}
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to synchronize to database with pattern '" << pattern << "'";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}		
	}
}


void xmas::store2g::TStoreProxy::syncFromDB
(
	xmas::FlashListDefinition * definition, 
	const std::string& storeGroup
)
	
{
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		std::set<std::string> tables = definition->getTableNames();
		
		// Create a regular expression pattern from the set of table names
		// to serve as a filter for the sync command. Only tables that match
		// the filter IN the DATABASE will be affected by the sync command
		//
		std::string pattern;
		if (tables.size() == 1 )
		{
			//pattern = "^" + toolbox::toupper(*(tables.begin())) + "$";
			pattern = "(" + toolbox::toupper(*(tables.begin())) + "|" + toolbox::toupper(*(tables.begin())) + "_dwell)";
		}
		else
		{
			pattern = "(";		
			std::set<std::string>::iterator si = tables.begin();
			while (si != tables.end())
			{
				pattern += toolbox::toupper(*si);			
				if (++si != tables.end())
				{
					pattern += "|";
				}			
			}
			
			toolbox::net::URN def(definition->getProperty("name"));
			pattern += "|";
			pattern += toolbox::toupper(def.getNSS());
			pattern += "_dwell";
			
			pattern += ")";
		}
		
		tstore::api::admin::Sync sync(this->getConnectionId(), "from database", pattern);
		xoap::MessageReference msg = sync.toSOAP();

		//std::cout << "syncFromDB request: " << std::endl;
		//msg->writeTo(std::cout);
		//std::cout << std::endl;

		try
		{
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Sync from TStore with pattern '" << pattern << "'");
		
			const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			xoap::MessageReference reply = 
				this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );						

			if (!reply.isNull())
			{
				//std::cout << "Sync from response: " << std::endl;
				//reply->writeTo(std::cout);
				//std::cout << std::endl;
			
				xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
				if (rb.hasFault())
				{
					// The remote application failed. It is an XDAQ application and therefore
					// embeds an XML serialized exception in the SOAP Fault detail field
					//
					std::stringstream msg;
					msg << "Failed to synchronize from database with pattern '" << pattern << "'";
					if ( rb.getFault().hasDetail() )
					{
						xoap::SOAPElement detail = rb.getFault().getDetail();					
						xcept::Exception rae;
						xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
						XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
					}
					else
					{
						msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
						XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
					}
				}
				else
				{
					return;
				}
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to synchronize from database with pattern '" << pattern << "'";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}		
	}
}

void xmas::store2g::TStoreProxy::createView
(
	const std::string& storeGroup,
	const std::string& storeView,
	const std::string& database
)
	
{
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		std::string zone = this->getOwnerApplication()->getApplicationContext()->getDefaultZoneName();
		std::string service = this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service");
		std::string tstoreViewConf = service + "_" + storeView +"_" + zone;

		std::string filename = tstoreViewConf;
		
		tstore::api::admin::AddNestedView addView(tstoreViewConf, filename, database);
		xoap::MessageReference msg = addView.toSOAP();
		
		LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Create new TStore view '" << filename << "'");
		
		try
		{
			const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			xoap::MessageReference reply = 
				this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );						

			if (!reply.isNull())
			{
				xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
				if (rb.hasFault())
				{
					// The remote application failed. It is an XDAQ application and therefore
					// embeds an XML serialized exception in the SOAP Fault detail field
					//
					std::stringstream msg;
					msg << "Failed to create new TStore view '" << filename << "' in database '" << database << "'";
					if ( rb.getFault().hasDetail() )
					{
						xoap::SOAPElement detail = rb.getFault().getDetail();					
						xcept::Exception rae;
						xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
						XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
					}
					else
					{
						msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
						XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
					}
				}
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to create new TStore view '" << filename << "' in database '" << database << "'";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}		
	}
}

bool xmas::store2g::TStoreProxy::hasView (
	const std::string& storeGroup,
	const std::string& storeView
	)
	
{
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");	
	if (descriptors.size() == 0)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
	}
	else if (descriptors.size() > 1)
	{
		XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
	}
	else
	{
		std::string service = this->getOwnerApplication()->getApplicationDescriptor()->getAttribute("service");
	
		std::string viewLocalId = service + "_" + storeView +"_" + this->getOwnerApplication()->getApplicationContext()->getDefaultZoneName();		
		tstore::api::admin::GetViews getViews;
		xoap::MessageReference msg = getViews.toSOAP();
		try
		{
			LOG4CPLUS_INFO (this->getOwnerApplication()->getApplicationLogger(), "Check if TStore has view file '" << viewLocalId << "'");
		
			const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			xoap::MessageReference reply = 
				this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );						

			if (!reply.isNull())
			{
				xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
				if (rb.hasFault())
				{
					// The remote application failed. It is an XDAQ application and therefore
					// embeds an XML serialized exception in the SOAP Fault detail field
					//
					std::stringstream msg;
					msg << "Failed to retrieves view '" << viewLocalId << "' from TStore";
					if ( rb.getFault().hasDetail() )
					{
						xoap::SOAPElement detail = rb.getFault().getDetail();					
						xcept::Exception rae;
						xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
						XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
					}
					else
					{
						msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
						XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
					}
				}
				else
				{
					try
					{
						tstore::api::admin::GetViewsResponse gvr(reply);
						if (std::find ( gvr.getNestedViews().begin(), gvr.getNestedViews().end(), viewLocalId ) != gvr.getNestedViews().end() )
						{
							return true;
						}
					}
					catch (tstore::api::admin::exception::Exception & e)
					{
						// work around for missing Response tag
						return false;					
					}
				}
			}
		}
		catch(xdaq::exception::Exception & e)
		{
			std::stringstream msg;
			msg << "Failed to retrieves view '" << viewLocalId << "' from TStore";
			XCEPT_RETHROW(xmas::store2g::exception::Exception, msg.str(), e);
		}
		return false;
	}
}


void xmas::store2g::TStoreProxy::authenticate
(
	const std::string & authentication, 
	const std::string & credentials
)
{
	 authentication_ = authentication;
	 credentials_ = credentials;	
}

void xmas::store2g::TStoreProxy::closeConnection(const std::string& storeGroup) 
{
    if (!this->isConnected())
    {
        return;
    }

    std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");
    std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");
    if (descriptors.size() == 0)
    {
        XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
    }
    else if (descriptors.size() > 1)
    {
        XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
    }

	const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			
	tstore::api::Disconnect disconnect(this->getConnectionId());
	xoap::MessageReference msg = disconnect.toSOAP();
	
	xoap::MessageReference reply;
	try
	{
		reply = this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );
	}
	catch(xdaq::exception::Exception & e)
        {
                XCEPT_RETHROW(xmas::store2g::exception::Exception, "failed to renew connection", e);
        }


	if (!reply.isNull())
	{
		xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
		if (rb.hasFault())
		{
			// The remote application failed. It is an XDAQ application and therefore
			// embeds an XML serialized exception in the SOAP Fault detail field
			//
			std::stringstream msg;
			msg << "Failed to close connection '" << this->getConnectionId() << "'";
			if ( rb.getFault().hasDetail() )
			{
				xoap::SOAPElement detail = rb.getFault().getDetail();					
				xcept::Exception rae;
				xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
				XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
			}
			else
			{
				msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
				XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
			}
		}
	}
}

void xmas::store2g::TStoreProxy::renewConnection(const std::string& storeGroup) 
{
 	if (!this->isConnected())
    {
        return;
    }

    std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");
    std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");
    if (descriptors.size() == 0)
    {
        XCEPT_RAISE (xmas::store2g::exception::Exception, "Failed to find an application descriptor for TSTore");
    }
    else if (descriptors.size() > 1)
    {
        XCEPT_RAISE (xmas::store2g::exception::Exception, "More than one application descriptor for TStore found, selection not possible");
    }

	const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			
	tstore::api::Renew renew(this->getConnectionId(), timeout_);
	xoap::MessageReference msg = renew.toSOAP();
	
	xoap::MessageReference reply;
	try
	{
		reply = this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );
	}
	catch(xdaq::exception::Exception & e)
        {
                XCEPT_RETHROW(xmas::store2g::exception::Exception, "failed to renew connection", e);
        }


	if (!reply.isNull())
	{
		xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
		if (rb.hasFault())
		{
			// The remote application failed. It is an XDAQ application and therefore
			// embeds an XML serialized exception in the SOAP Fault detail field
			//
			std::stringstream msg;
			msg << "Failed to renew connection '" << this->getConnectionId() << "'";
			if ( rb.getFault().hasDetail() )
			{
				xoap::SOAPElement detail = rb.getFault().getDetail();					
				xcept::Exception rae;
				xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
				XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
			}
			else
			{
				msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
				XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
			}
		}
	}
}


void xmas::store2g::TStoreProxy::addTableDefinition
(
	DOMElement * tableNode, 
	const std::string & name, 
	const std::string key, 
	xmas::ItemDefinition * inner, 
	const std::string & outer,
	std::list<xmas::ItemDefinition *> keyItems
)
{
	// Create new <table> node in document
	DOMDocument* document = tableNode->getOwnerDocument();
	DOMElement * element = document->getDocumentElement();
	DOMElement * innerTableNode = document->createElementNS( xoap::XStr(tstore::api::admin::NamespaceUri), xoap::XStr(tstore::api::admin::NamespacePrefix+ ":table") );
	innerTableNode->setAttribute ( xoap::XStr("name"), xoap::XStr(name) );
	element->appendChild(innerTableNode);
	
	std::set<std::string> keys;	
	if ( key != "" )
	{
		innerTableNode->setAttribute ( xoap::XStr("key"), xoap::XStr(key) );
		keys = toolbox::parseTokenSet( inner->getProperty("key"),",");
	}
	
	std::list<xmas::ItemDefinition *> innerKeyItems;
	for (std::set<std::string>::iterator i=keys.begin(); i!=keys.end(); ++i) 
	{
		innerKeyItems.push_back(inner->getItem(*i));		
	}			
		
	std::vector<xmas::ItemDefinition*> columns= inner->getItems();
	for (std::vector<xmas::ItemDefinition*>::iterator i= columns.begin(); i!=columns.end(); ++i) 
	{
		DOMElement *columnNode=document->createElementNS(xoap::XStr(tstore::api::admin::NamespaceUri), 
								 xoap::XStr(tstore::api::admin::NamespacePrefix+":column"));
		if ((*i)->getProperty("type") != "table")
		{			
			columnNode->setAttribute(xoap::XStr("name"),xoap::XStr((*i)->getProperty("name")));
			columnNode->setAttribute(xoap::XStr("type"),xoap::XStr((*i)->getProperty("type")));
			innerTableNode->appendChild(columnNode);			
		}
		else
		{
			this->addTableDefinition ( columnNode, (*i)->getProperty("name"), (*i)->getProperty("key"), (*i),
						   inner->getProperty("name"),innerKeyItems  );
		}			
	}
	// Now add a foreign reference to the outer table
	// <foreignkey references="A"><keycolumn column="A" references="B"/></foreignkey>	
	for ( std::list<xmas::ItemDefinition *>::iterator i = keyItems.begin(); i != keyItems.end(); i++ )
	{
		DOMElement *outerKey=document->createElementNS(xoap::XStr(tstore::api::admin::NamespaceUri),xoap::XStr(tstore::api::admin::NamespacePrefix+":column"));
		outerKey->setAttribute(xoap::XStr("name"),xoap::XStr(outer + "_" + (*i)->getProperty("name")));
		outerKey->setAttribute(xoap::XStr("type"),xoap::XStr((*i)->getProperty("type")));
		innerTableNode->appendChild(outerKey);	
	}
	
	// foreign key statments	
	DOMElement *foreignKey=document->createElementNS(xoap::XStr(tstore::api::admin::NamespaceUri),xoap::XStr(tstore::api::admin::NamespacePrefix+":foreignkey"));
	foreignKey->setAttribute(xoap::XStr("references"),xoap::XStr(outer));
	innerTableNode->appendChild(foreignKey);	

	for ( std::list<xmas::ItemDefinition *>::iterator i = keyItems.begin(); i != keyItems.end(); i++ )
	{
		DOMElement *keycolumn=document->createElementNS(xoap::XStr(tstore::api::admin::NamespaceUri),xoap::XStr(tstore::api::admin::NamespacePrefix+":keycolumn"));
		keycolumn->setAttribute(xoap::XStr("column"),xoap::XStr(outer + "_" + (*i)->getProperty("name")));
		keycolumn->setAttribute(xoap::XStr("references"),xoap::XStr((*i)->getProperty("name")));
		foreignKey->appendChild(keycolumn);
	}			
}

void xmas::store2g::TStoreProxy::addTableDefinition
(
	DOMDocument * document,
	xmas::FlashListDefinition * flashlist
) 
{	
	toolbox::net::URN nameURN(flashlist->getProperty("name"));
	std::string name = nameURN.getNSS();
	
	DOMElement * element = document->getDocumentElement();
	DOMElement * tableNode = document->createElementNS( xoap::XStr(tstore::api::admin::NamespaceUri), xoap::XStr(tstore::api::admin::NamespacePrefix+ ":table") );
	tableNode->setAttribute ( xoap::XStr("name"), xoap::XStr(name) );
	element->appendChild(tableNode);
	
	tableNode->setAttribute ( xoap::XStr("xmlns:" + tstore::api::admin::NamespacePrefix), xoap::XStr(tstore::api::admin::NamespaceUri) );
	
	std::set<std::string> keys;	
	if ( flashlist->getProperty("key") != "" )
	{
		tableNode->setAttribute ( xoap::XStr("key"), xoap::XStr(flashlist->getProperty("key")) );
		keys = toolbox::parseTokenSet( flashlist->getProperty("key"),",");
	}
	
	std::list<xmas::ItemDefinition *> keyItems;
	for (std::set<std::string>::iterator i=keys.begin(); i!=keys.end(); ++i) 
	{
		keyItems.push_back(flashlist->getItem(*i));		
	}		
	
	std::vector<xmas::ItemDefinition *> columns = flashlist->getItems();
	for (std::vector<xmas::ItemDefinition *>::iterator i=columns.begin(); i!=columns.end(); ++i) 
	{		
		DOMElement *columnNode=document->createElementNS(xoap::XStr(tstore::api::admin::NamespaceUri), 
										     xoap::XStr(tstore::api::admin::NamespacePrefix+":column"));
		if ((*i)->getProperty("type") != "table")
		{
		
			columnNode->setAttribute(xoap::XStr("name"),xoap::XStr((*i)->getProperty("name")));
			columnNode->setAttribute(xoap::XStr("type"),xoap::XStr((*i)->getProperty("type")));
			tableNode->appendChild(columnNode);
		}
		else
		{
			// Add a new table node to the document for the inner table			
			//
			this->addTableDefinition ( columnNode, (*i)->getProperty("name"), (*i)->getProperty("key"), (*i), name, keyItems  );
		}			
	}
	
	std::stringstream val;
	val << flashlist->getNestedDepth();
	tableNode->setAttribute ( xoap::XStr("depth"), xoap::XStr(val.str()) );													
}

void xmas::store2g::TStoreProxy::addDwellTableDefinition
(
	DOMDocument * document,
	xmas::FlashListDefinition * flashlist
) 
{	
	toolbox::net::URN nameURN(flashlist->getProperty("name"));
	std::string name = nameURN.getNSS();
	
	DOMElement * element = document->getDocumentElement();
	DOMElement * tableNode = document->createElementNS( xoap::XStr(tstore::api::admin::NamespaceUri), xoap::XStr(tstore::api::admin::NamespacePrefix+ ":table") );
	tableNode->setAttribute ( xoap::XStr("name"), xoap::XStr(name + "_dwell") );
	element->appendChild(tableNode);
	
	tableNode->setAttribute ( xoap::XStr("xmlns:" + tstore::api::admin::NamespacePrefix), xoap::XStr(tstore::api::admin::NamespaceUri) );
	
	std::set<std::string> keys;	
	if ( flashlist->getProperty("key") != "" )
	{
		tableNode->setAttribute ( xoap::XStr("key"), xoap::XStr(flashlist->getProperty("key")) );
		keys = toolbox::parseTokenSet( flashlist->getProperty("key"),",");
	}
	else
	{
		LOG4CPLUS_WARN (this->getOwnerApplication()->getApplicationLogger(), "No key defined for flashlist '" << name << "', dwell table will not be created");
	}
	
	std::list<xmas::ItemDefinition *> keyItems;
	for (std::set<std::string>::iterator i=keys.begin(); i!=keys.end(); ++i) 
	{
		keyItems.push_back(flashlist->getItem(*i));		
	}		
	
	for (std::list<xmas::ItemDefinition *>::iterator i=keyItems.begin(); i!=keyItems.end(); ++i) 
	{		
		DOMElement *columnNode=document->createElementNS(xoap::XStr(tstore::api::admin::NamespaceUri), 
										     xoap::XStr(tstore::api::admin::NamespacePrefix+":column"));
		
		columnNode->setAttribute(xoap::XStr("name"),xoap::XStr((*i)->getProperty("name")));
		columnNode->setAttribute(xoap::XStr("type"),xoap::XStr((*i)->getProperty("type")));
		tableNode->appendChild(columnNode);
			
	}
}


size_t xmas::store2g::TStoreProxy::discoveredTStores(const std::string  & storeGroup)
{
	std::set<std::string> groups = toolbox::parseTokenSet(storeGroup,",");	
	std::list<xdaq::ApplicationDescriptorImpl> descriptors = descriptorsCache_->getDescriptors(groups, "tstore");	
	return descriptors.size();
} 

void xmas::store2g::TStoreProxy::clearTable(std::list<xdaq::ApplicationDescriptorImpl>& descriptors, const std::string& name)
	
{
	const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();
			
	tstore::api::ClearNested clear(this->getConnectionId(), name);
	xoap::MessageReference msg = clear.toSOAP();
	//std::cout << "Clear request: " << std::endl;
	//msg->writeTo(std::cout);


	xoap::MessageReference reply;
	try
	{
		reply = this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );
	}
	 catch(xdaq::exception::Exception & e)
        {
                XCEPT_RETHROW(xmas::store2g::exception::Exception, "failed to clear table", e);
        }


	if (!reply.isNull())
	{
		xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
		if (rb.hasFault())
		{
			// The remote application failed. It is an XDAQ application and therefore
			// embeds an XML serialized exception in the SOAP Fault detail field
			//
			std::stringstream msg;
			msg << "Failed to clear table '" << name << "'";
			if ( rb.getFault().hasDetail() )
			{
				xoap::SOAPElement detail = rb.getFault().getDetail();					
				xcept::Exception rae;
				xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
				XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
			}
			else
			{
				msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
				XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
			}
		}
	}
}

void xmas::store2g::TStoreProxy::insertTable
(
	std::list<xdaq::ApplicationDescriptorImpl>& descriptors, 
	const std::string& name,
	xdata::Table::Reference& table,
	size_t depth
)
	
{
	const xdaq::ApplicationDescriptor * from = this->getOwnerApplication()->getApplicationDescriptor();

	tstore::api::InsertNested insert(this->getConnectionId(), name, table, depth);
	xoap::MessageReference msg = insert.toSOAP();

	xoap::MessageReference reply;
	try
	{
		reply = this->getOwnerApplication()->getApplicationContext()->postSOAP( msg, *from, *(descriptors.begin()) );
	}
 	catch(xdaq::exception::Exception & e)
        {
                XCEPT_RETHROW(xmas::store2g::exception::Exception, "failed to insert table", e);
        }


	if (!reply.isNull())
	{
		xoap::SOAPBody rb = reply->getSOAPPart().getEnvelope().getBody();
		if (rb.hasFault())
		{
			if (rb.hasFault())
			{
				// The remote application failed. It is an XDAQ application and therefore
				// embeds an XML serialized exception in the SOAP Fault detail field
				//
				std::stringstream msg;
				msg << "Failed to insert into table '" << name << "'";
				if ( rb.getFault().hasDetail() )
				{
					xoap::SOAPElement detail = rb.getFault().getDetail();					
					xcept::Exception rae;
					xdaq::XceptSerializer::importFrom (detail.getDOM(), rae);
					XCEPT_RETHROW (xmas::store2g::exception::Exception, msg.str(), rae );
				}
				else
				{
					msg << ", reason: '" << rb.getFault().getFaultString() << "'.";
					XCEPT_RAISE (xmas::store2g::exception::Exception, msg.str());
				}
			}					
		}
	}
}


bool xmas::store2g::TStoreProxy::hasMetaData ( const std::string& storeGroup, const std::string& database ) 
	
{
	return true;
}

void xmas::store2g::TStoreProxy::createMetaData ( const std::string& storeGroup, const std::string& database ) 
	
{
}


void xmas::store2g::TStoreProxy::insertMetaData ( xmas::FlashListDefinition * definition, const std::string& storeGroup, const std::string& database) 
	
{

}
