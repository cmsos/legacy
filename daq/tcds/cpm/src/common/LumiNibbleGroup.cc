#include "tcds/cpm/LumiNibbleGroup.h"

tcds::cpm::LumiNibbleGroup::LumiNibbleGroup(tcds::cpm::LumiNibble const& nibble) :
  tcds::cpm::LumiNibbleGroupBase(nibble)
{
}

tcds::cpm::LumiNibbleGroup::LumiNibbleGroup(std::vector<tcds::cpm::LumiNibble> const& nibbles) :
  tcds::cpm::LumiNibbleGroupBase(nibbles)
{
}

tcds::cpm::LumiNibbleGroup::~LumiNibbleGroup()
{
}
