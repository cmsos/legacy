
 const uint64_t* NextEvent (uint64_t* CDF_Header)  {
	uint64_t* ptr =(CDF_Header)+1;
	const int nAMC = (*ptr>>52)&0xF;
	ptr+=1;
	int *AMCData = new int[nAMC];
	int stop = 0;	

	while (stop == 0) {
		if (((*ptr>>61)&0x1)==0) stop++;
		for(int i=0; i<nAMC; i++) {
			AMCData[i] = int((*ptr>>32)&0xFFFFFF);
			ptr++;
			}
		for(int j=0; j<nAMC; j++) ptr+=AMCData[j];
		ptr++;
		}
	ptr++;
	return ptr;
	}  
