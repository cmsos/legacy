// $Id$
#ifndef _bril_bptxprocessor_version_h_
#define _bril_bptxprocessor_version_h_
#include "config/PackageInfo.h"
#define BRILBPTXPROCESSOR_VERSION_MAJOR 1
#define BRILBPTXPROCESSOR_VERSION_MINOR 5
#define BRILBPTXPROCESSOR_VERSION_PATCH 1
#define BRILBPTXPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRILBPTXPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILBPTXPROCESSOR_VERSION_MAJOR,BRILBPTXPROCESSOR_VERSION_MINOR,BRILBPTXPROCESSOR_VERSION_PATCH)
#ifndef BRILBPTXPROCESSOR_PREVIOUS_VERSIONS
#define BRILBPTXPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBPTXPROCESSOR_VERSION_MAJOR,BRILBPTXPROCESSOR_VERSION_MINOR,BRILBPTXPROCESSOR_VERSION_PATCH)
#else
#define BRILBPTXPROCESSOR_FULL_VERSION_LIST  BRILBPTXPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBPTXPROCESSOR_VERSION_MAJOR,BRILBPTXPROCESSOR_VERSION_MINOR,BRILBPTXPROCESSOR_VERSION_PATCH)
#endif
namespace brilbptxprocessor{
  const std::string package = "brilbptxprocessor";
  const std::string versions = BRILBPTXPROCESSOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bptxprocessor";
  const std::string description = "Read the data from scope monitoring scripts and republish to eventing and hyperDaq";
  const std::string authors = "Andrey Pozdnyakov";
  const std::string link = "http://vmebptx.cms:58008/urn:xdaq-application:lid=121";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
