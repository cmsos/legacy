package xmltreeeditor;

import javax.swing.JTree;
import javax.swing.tree.*;
import java.awt.*;

//Empty constructor

public class XMLTree extends JTree {
  public XMLTree() {
   super();
   this.setModel(new XMLTreeModel(new DefaultMutableTreeNode(new XMLTreeNodeUserObject("ROOT","",(short) 1))));
   this.setEditable(true);
   this.setRowHeight(25);
 }
// Constructor by TreeModel
public XMLTree (XMLTreeModel model){
    super(model);
    this.setEditable(true);
    this.setRowHeight(25);
  }

}