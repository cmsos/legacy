package xdaq.xdesk.jfinder;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;
import java.io.*;
import java.util.*;
import javax.xml.transform.*;
import org.xml.sax.*;
import javax.xml.transform.*;
import javax.xml.parsers.*;

public class JFinder
extends JPanel
implements TreeModelListener, FinderSelectionListener {
	
	protected TreePath currentPath_ = null; // remember current folder being displayed
	
	protected FinderModel model_;
	protected FlowLayout flowLayout_;
	protected FinderRenderer renderer_;
	protected ActionListener actionListener_ = null;
	
	protected DefaultFinderSelectionModel selectionModel_; // acts as FinderSelectionModel
	
	Color selectionForeground;
	Color selectionBackground;
	
	protected FinderMouseAdapter mouseAdapter_;
	
	protected HashMap mapper_;
	private FinderGUIConfiguration config;
	
	public JFinder() {
		mapper_ = new HashMap();
		
		//super(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		flowLayout_ = new FlowLayout(FlowLayout.LEFT, 10, 10);
		this.setLayout(flowLayout_);
		
		renderer_ = new DefaultFinderRenderer();
		
		selectionForeground = SystemColor.textHighlightText;
		selectionBackground = SystemColor.textHighlight; // Color.lightBlue;
		
		selectionModel_ = new DefaultFinderSelectionModel();
		selectionModel_.addFinderSelectionListener(this);
		mouseAdapter_ = new FinderMouseAdapter(this);
		
		try {
			config = new FinderGUIConfiguration();
		}
		catch (Exception ex) {
			System.out.println("Cannot load configuration : " + ex);
		}
	}
	
	public JFinder(FinderModel model) {
		this();
		
		// Must be the last call, since the model update
		// triggers the display
		this.setModel(model);
	}
	
	public void setGUIConfiguration(FinderGUIConfiguration config_) {
		this.config = config_;
		this.refresh();
	}
	
	public void setGUIConfiguration(File file) {
		try {
			this.config = new FinderGUIConfiguration(file);
			this.refresh();
		}
		catch (Exception ex) {
			System.out.println(
							   "Cannot load GUI configuration. An exception occured :" + ex);
		}
	}
	
	public FinderGUIConfiguration getConfiguration() {
		return config;
	}
	
	public void setRenderer(FinderRenderer renderer) {
		renderer_ = renderer;
	}
	
	public void setActionListener(ActionListener l) {
		actionListener_ = l;
	}
	
	public DefaultFinderSelectionModel getSelectionModel() {
		return selectionModel_;
	}
	
	public FinderRenderer getRenderer() {
		return renderer_;
	}
	
	public void setModel(FinderModel model) {
		if (model == null) {
			throw new IllegalArgumentException("Cannot set a null FinderModel");
		}
		if (this.model_ != model) {
			FinderModel old = this.model_;
			if (old != null) {
				old.removeTreeModelListener(this);
			}
			this.model_ = model;
			model_.addTreeModelListener(this);
			TreePath path = new TreePath(model.getPathToRoot( (TreeNode) model.
															  getRoot()));
			finderChanged(new TreeModelEvent(model, path));
			
			// firePropertyChange ("model", old, model_);
		}
		
	}
	
	public FinderModel getModel() {
		return model_;
	}
	
	// Return the currently selected FinderTreeNode
	public FinderTreeNode getSelectedFinderTreeNode() {
		JLabel cell = selectionModel_.getSelectionCell();
		return (FinderTreeNode) mapper_.get(cell);
	}
	
	public void finderFolderOpen() {
		JLabel cell = selectionModel_.getSelectionCell();
		
		FinderTreeNode node = (FinderTreeNode) mapper_.get(cell);
		
		if (node.getAllowsChildren()) {
			// if children are allowed it is a directory, otherwise
			// the node represents a file.
			TreeModelEvent e = new TreeModelEvent(model_, model_.getPathToRoot(node));
			
			finderChanged(e);
		}
		else {
			// fire user callback activation here for file opened
			// User callback must query the JFinder selection model
			// to find out, which while was opened
			if (actionListener_ != null) {
				ActionEvent e = new ActionEvent(this, 0, "open");
				actionListener_.actionPerformed(e);
			}
		}
	}
	
	public void refresh() {
		if (currentPath_ != null) {
			TreeModelEvent e = new TreeModelEvent(model_, currentPath_);
			this.finderChanged(e);
		}
		
	}
	
	public void finderChanged(TreeModelEvent e) {
		// if event is null, means the whole thing has changed
		//if (e == null)
		//{
		this.clearSelection();
		//} else
		//{
		TreePath path = e.getTreePath();
		currentPath_ = path;
		FinderTreeNode folder = (FinderTreeNode) path.getLastPathComponent();
		
		this.removeAll();
		
		mapper_.clear();
		
		// Insert a dummy node that represents the parent directory if the father is not yet the root
		// The UP folder!!
		if (folder.getParent() != null) {
			FinderTreeNode parent = (FinderTreeNode) folder.getParent();
			JLabel newLabel = (JLabel) renderer_.getFinderRendererComponent(this,
																			parent, false, false, null);
			mapper_.put(newLabel, parent);
			
			//FinderCell parent = new FinderCell ( (FinderTreeNode) folder.getParent());
			newLabel.addMouseListener(mouseAdapter_);
			//JLabel upLabel = (JLabel) renderer_.getFinderRendererComponent (this, parent, false, false);
			newLabel.setText("Up");
			newLabel.setIcon(this.getConfiguration().getIcon("Up", true));
			this.add(newLabel);
		}
		
		Enumeration children = folder.children();
		while (children.hasMoreElements()) {
			FinderTreeNode c = (FinderTreeNode) children.nextElement();
			//FinderCell cell = new FinderCell (c);
			
			JLabel toDraw = (JLabel) renderer_.getFinderRendererComponent(this, c, false, false, null);
			mapper_.put(toDraw, c);
			toDraw.addMouseListener(mouseAdapter_);
			this.add(toDraw);
		}
		
		this.revalidate();
		
		//}
	}
	
	public Color getSelectionForeground() {
		return selectionForeground;
	}
	
	public Color getSelectionBackground() {
		return selectionBackground;
	}
	
	public Color getForeground() {
		return Color.black;
	}
	
	public Color getBackground() {
		return Color.white;
	}
	
	public void clearSelection() {
		selectionModel_.clearSelection();
	}
	
	// Prototype main program. args[1] is the path to be displayed
	public static void main(String[] args) {
		if (args.length < 2) {
			System.out.println("Directory path missing args:"+ args.length);
			System.exit(1);
		}
		
		JFrame frame = new JFrame();
		frame.setSize(400, 400);
		
		System.out.println("Path: " + args[0]);
		
		File file = new File(args[0]);
		FinderTreeNode rootNode = new FinderTreeNode(file);
		FinderModel m = new FinderModel(rootNode);
		JFinder finder = new JFinder(m);
		finder.setGUIConfiguration(new File(args[1]));
		JScrollPane p = new JScrollPane(finder,
										JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
										JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		frame.getContentPane().add(p);
		
		frame.setVisible(true);
	}
	
	// -------------------------------------------------------------
	// TreeModelListener callbacks
	// -------------------------------------------------------------
	
	public void treeNodesChanged(TreeModelEvent e) {
		
		FinderTreeNode c = (FinderTreeNode) e.getTreePath().getLastPathComponent();
		
		FinderTreeNode n;
		Object[] keys = mapper_.keySet().toArray(new Object[] {});
		for (int i = 0; i < keys.length; i++) {
			n = (FinderTreeNode) mapper_.get(keys[i]);
			if (n == c) {
				JLabel selected = selectionModel_.getSelectionCell();
				if (selected == (JLabel) keys[i]) {
					renderer_.getFinderRendererComponent(this, c, true, true,
														 (JLabel) keys[i]);
				}
				else {
					renderer_.getFinderRendererComponent(this, c, false, false,
														 (JLabel) keys[i]);
				}
			}
		}
		
		this.revalidate();
	}
	
	public void treeNodesInserted(TreeModelEvent e) {
		
	}
	
	public void treeNodesRemoved(TreeModelEvent e) {
		
	}
	
	public void treeStructureChanged(TreeModelEvent e) {
		
	}
	
	// -------------------------------------------------------------
	// Scrollable interfaces
	// -------------------------------------------------------------
	
	public Dimension getPreferredSize() {
		Dimension dim = super.getPreferredSize();
		Container parent = getParent();
		
		Dimension flDim = flowLayout_.preferredLayoutSize(this);
		
		double w = flDim.getWidth();
		double h = flDim.getHeight();
		
		double rows = (w / dim.getWidth()) + 1;
		h = h * rows;
		
		if ( (parent != null) && (parent instanceof JViewport)) {
			// dim.setSize ( (double) parent.getWidth(), (double) dim.getHeight() );
			//dim.setSize ( (double) parent.getWidth(), (double) 1000 );
			
			// The scrollbar comes, if the size of the component is bigger than
			// the viewport. Unfortunately this only works, if from the first
			// time on the size is larger than the window. The commented out
			// on-the-fly calculation shown below does not cause the scrollbar
			// to be created.
			
			//if (h > parent.getHeight())
			//{
			//	System.out.println ("h is larger: " + h);
			dim.setSize( (double) parent.getWidth(), h + (double) parent.getHeight());
			//} else {
			//	System.out.println ("h is smaller!: " + h);
			//	dim.setSize ((double) parent.getWidth(), (double) parent.getHeight());
			//}
		}
		//this.setPreferredSize ( new Dimension(250,250) );
		return dim;
	}
	
	// Callback implementation for FinderSelectionListener interface
	//
	public void valueChanged(FinderSelectionEvent e) {
		JLabel newLabel = e.getNewSelectionCell();
		JLabel oldLabel = e.getOldSelectionCell();
		
		if (oldLabel != null) {
			System.out.println("oldLabel is " + oldLabel.toString());
			FinderTreeNode oldCell = (FinderTreeNode) mapper_.get(oldLabel);
			
			System.out.println("oldCell is " + oldCell.toString());
			renderer_.getFinderRendererComponent(this, oldCell, false, false,
												 oldLabel);
		}
		
		if (newLabel != null) {
			System.out.println("NewLabel");
			FinderTreeNode newCell = (FinderTreeNode) mapper_.get(newLabel);
			renderer_.getFinderRendererComponent(this, newCell, true, false, newLabel);
		}
		
		//this.validate();
	}
}
