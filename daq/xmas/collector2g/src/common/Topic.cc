// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xmas/collector2g/Topic.h"

bool xmas::collector2g::Topic::operator==(const xmas::collector2g::Topic &other) const
{
	if( this->flashlistName_ != other.flashlistName_ )
	{
		return false;
	}

	if( this->publishTopicName_ != other.publishTopicName_ )
	{
		return false;
	}

	if( this->subscribeTopicName_ != other.subscribeTopicName_ )
	{
		return false;
	}

	if( this->hashKey_ != other.hashKey_ )
	{
		return false;
	}

	if( this->stagingPeriod_ != other.stagingPeriod_ )
	{
		return false;
	}

	if( this->autoClear_ != other.autoClear_ )
	{
		return false;
	}

	return true;
}

bool xmas::collector2g::Topic::operator!=(const xmas::collector2g::Topic &other) const
{
	return !(this->operator==(other));
}

std::string xmas::collector2g::Topic::getPublishTopic()
{
	return publishTopicName_;
}

std::string xmas::collector2g::Topic::getSubscribeTopic()
{
	return subscribeTopicName_;
}

std::string xmas::collector2g::Topic::getFlashlistName()
{
	return flashlistName_;
}

std::set<std::string> xmas::collector2g::Topic::getHashKey()
{
	return hashKey_;
}

toolbox::TimeInterval xmas::collector2g::Topic::getStagingPeriod()
{
	return stagingPeriod_;
}

bool xmas::collector2g::Topic::getAutoClear()
{
	return autoClear_;
}
