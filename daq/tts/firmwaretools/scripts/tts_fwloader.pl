#! /usr/bin/perl

my $cardtype;
my $revision;

my $loader_args = "";

for my $arg (@ARGV) {
    
    if ( $arg =~ /-c([A-Z]*)/ ) {
	$cardtype = $1;
    }

    if ( $arg =~ /-k(.*)/ ) {
	$revision = $1;
    } 
    else {
	$loader_args = "$loader_args $arg";
    }
}

#print "c=$cardtype\n";
#print "k=$revision\n";
#print "l=$loader_args\n";

$fwloader = "$ENV{XDAQ_ROOT}/daq/d2s/firmwareloader/scripts/cmsdaq_fwloader";
die "$fwloader not found." unless -f $fwloader;
    

my $logfile = "/tmp/fwloadlog.txt";
system "$fwloader $loader_args | tee $logfile";

open (LOGF, "<", $logfile);
my @lines = <LOGF>;
close LOGF;

my $out = join("", @lines);

if ($out =~ /successfully sequenced SVF sequence/m) {

    print "FW load successful. Going to register load in DB.\n";

    for my $line (@lines) {
	if ( $line =~ /\>\>[^\>]*\(type=${cardtype}\)[^\>]*SN=([0-9]*)/ ) {
	    
	    my $sn = $1;
	    print "==> Registering load for SN=$sn\n";
	    my $hostname = `hostname`;
	    my $username = `logname`;
	    chomp ($username);

	    my $cmd = "/usr/bin/perl load_db.pl $sn $revision $username $hostname";
#print "calling cmd: $cmd\n";
	    my $out1 = `$cmd`;
	    print "$out1\n";
	}
    }

}
elsif ($out =~ /"error sequencing"/m) {
    print "FW load error.";
}



