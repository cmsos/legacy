package xdaq.tools.plugin;

import xdaq.tools.plugin.Plugin;
import java.util.*;

public class PluginContainer
{
	public PluginContainer()
	{
		plugins_ = new HashMap();
	}
	
	public void add (Plugin plugin, String association, String instance)
	{
		plugins_.put ( association+instance, plugin );
	}
	
	public Plugin get (String association, String instance)
	{
		return (Plugin) plugins_.get (association+instance);
	}	
	
	public void remove ( String association, String instance )
	{
		plugins_.remove ( association+instance );
	}
	
	public HashMap getAll ()
	{
		return plugins_;
	}
	
	public int size()
	{
		return plugins_.size();
	}

	private HashMap plugins_;
}
