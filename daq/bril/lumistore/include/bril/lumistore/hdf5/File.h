#ifndef _bril_lumistore_hdf5_File_h
#define _bril_lumistore_hdf5_File_h
#include <map>
#include <vector>
#include <string>
#include "hdf5.h"
namespace bril{ namespace lumistore{ namespace hdf5{
      class ComplexTable;
      class Table;
      class Group;
      class Node;
      class RootGroup;
      class File {
      public:
	File(const std::string& name,bool compressed=false);
	~File();
	void set_attrstr(const std::string& path,const std::string& attrname,const std::string& attrvalue);	
	void flush();
	void close();
	hid_t id() const;
	bool isopen() const;	
	size_t get_filesize() const;
	Group* create_group(const std::string& parentpath,const std::string& name);
	Node* create_table(const std::string& parentpath,const std::string& name,size_t datacolumnlen,hid_t datacolumntype,size_t rowsperblock,bool shrink);
	Node* create_complextable(const std::string& parentpath,const std::string& name,const std::string& dictionary,size_t rowsperblock,bool shrink);
	Node* get_node(const std::string& path);
	Node* get_rootnode();
      private:
	std::string m_filename;
	hid_t m_fileid;
	std::map<std::string,Node*> m_noderegistry; //
	bool m_compressed;
	bool m_isopen;
	RootGroup* m_root;
      };
    }}}

#endif
