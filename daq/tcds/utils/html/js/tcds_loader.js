//-----------------------------------------------------------------------------

// Load some (more) stuff we need.

// NOTE: Only do this for TCDS control applications. The check here is
// necessary because the call to
// xgi::framework::UIManager::setLayout() in the WebServer globally
// replaces the HyperDAQ layout for all applications in the XDAQ
// executive.

if (isTCDSApplication())
{
    // The splash screen.
    getScript(baseUrl + "/tcds/utils/html/please-wait/please-wait.min.js");
    getScript(baseUrl + "/tcds/utils/html/js/tcds_loading_screen.js");
    getCSS(baseUrl + "/tcds/utils/html/please-wait/please-wait.css");

    // JQuery friends.
    // NOTE: JQuery itself is already loaded by the XDAQ/HyperDAQ
    // framework JS.
    getScript(baseUrl + "/tcds/utils/html/jquery-ui/jquery-ui.min.js");
    getCSS(baseUrl + "/tcds/utils/html/jquery-ui/jquery-ui.css");

    // SlickGrid-related.
    getScript(baseUrl + "/tcds/utils/html/slickgrid/lib/jquery.event.drag-2.2.js");
    getScript(baseUrl + "/tcds/utils/html/slickgrid/slick.core.js");
    getScript(baseUrl + "/tcds/utils/html/slickgrid/slick.grid.js");
    getScript(baseUrl + "/tcds/utils/html/slickgrid/plugins/slick.rowselectionmodel.js");
    getScript(baseUrl + "/tcds/utils/html/slickgrid/plugins/slick.autotooltips.js");
    getCSS(baseUrl + "/tcds/utils/html/slickgrid/slick.grid.css");

    // PNotify.
    getScript(baseUrl + "/tcds/utils/html/pnotify/pnotify.custom.min.js");
    getCSS(baseUrl + "/tcds/utils/html/pnotify/pnotify.custom.min.css");

    // Flot.
    // NOTE: Loading order is important here.
    getScript(baseUrl + "/tcds/utils/html/flot/jquery.flot.js", true);
    getScript(baseUrl + "/tcds/utils/html/flot/jquery.flot.selection.js", true);
    getScript(baseUrl + "/tcds/utils/html/flot/jquery.flot.stack.js", true);
    getScript(baseUrl + "/tcds/utils/html/flot/jquery.flot.axislabels.js", true);

    // doT.
    getScript(baseUrl + "/tcds/utils/html/dot/doT.js");

    // TCDS stuff.
    getCSS(baseUrl + "/tcds/utils/html/css/tcds.css");
}

//-----------------------------------------------------------------------------
