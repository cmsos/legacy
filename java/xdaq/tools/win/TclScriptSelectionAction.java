package xdaq.tools.win;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import xdaq.tools.win.*;
import javax.swing.tree.*;
import org.w3c.dom.*;

public class TclScriptSelectionAction extends AbstractAction
{
	xdaqWin win_;
	TreePath[] paths_;
	
	public TclScriptSelectionAction (String name, xdaqWin win)
	{
		super (name);
		win_ = win;
	}
	
	public void actionPerformed (ActionEvent e)
	{
		// get the current selection of scripts ans execute them
		rememberTreeSelection();

		System.out.println ("Execute function for Tcl.");

	
		int pathLength = paths_.length;
		for (int k = 0; k < pathLength; k++)
		{
			 DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths_[k].getLastPathComponent();

			 org.w3c.dom.Node domNode = ((xdaqWinTreeAdapterNode)node.getUserObject()).domNode.getFirstChild();

			String script = null;

			while (domNode != null)
			{
       				if (domNode instanceof org.w3c.dom.CDATASection)
                        	{
                       	 		org.w3c.dom.CDATASection cdata = (CDATASection) domNode;
                               		 script = cdata.getData();
                               		 break;
                        	} else
                        	{
                               		 // Otherwise, the script is in the value section of an
                               		 // ordinary text node.
                               		 org.w3c.dom.Text text = (Text) domNode;
                               		 script = text.getNodeValue();
                        	}
				domNode = domNode.getNextSibling();
			}

			 TclInterpreter interp = new TclInterpreter (win_);
			 String status = interp.evalScript (script);
			 if (! status.equals(""))
			 {
				JOptionPane.showMessageDialog (null, status, "Script error", JOptionPane.WARNING_MESSAGE);
			 }	
			 interp.dispose();
		}
	}
	
	public void rememberTreeSelection()
	{
		TreePath[] paths = (TreePath[]) win_.getTree().getSelectionModel().getSelectionPaths();
		
		int pathLength = paths.length;
		paths_ = (TreePath[]) new TreePath [pathLength];
		for (int k = 0; k < pathLength; k++)
		{
			paths_[k] =  new TreePath ( paths[k].getPath() );
		}
	}
}
