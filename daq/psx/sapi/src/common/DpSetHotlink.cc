// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/



#include "psx/sapi/DpSetHotlink.h"
#include "psx/sapi/DpSetResult.h"
#include "psx/sapi/Utils.h"
#include "psx/sapi/exception/Exception.h"
#include "xdaq/Application.h"

psx::sapi::DpSetHotlink::DpSetHotlink(psx::sapi::Request * request) : psx::sapi::Hotlink(request)
{

}

void psx::sapi::DpSetHotlink::hotLinkCallBack(DpMsgAnswer &answer)
{
	psx::sapi::DpSetResult * result = new psx::sapi::DpSetResult(request_);	

	// MsgType msgType = answer.isAnswerOn();	
	// std::cout << "Entering DPSet hotlink callback: " << Msg::getMsgName(msgType) << std::endl;
	const AnswerGroup* group = answer.getFirstGroup();
    	if ( group != 0 )
	{
		ErrClass* err = group->getErrorPtr();
		if (err != 0)
		{
			/* Message discarded due to an active/passive switch
			   In this particular case DpSetHotlink object will be reused by WinCC_OA a second time (hotLinkCallBack will be called again)
			   to confirm data point set was successful */
			if ( (err->getErrorType() == ErrClass::ERR_REDUNDANCY) && (err->getErrorId() == ErrClass::REDU_CHANGES_ABORTED) )
			{
				xdaq::Application * application;
				application = dynamic_cast<xdaq::Application *>(request_->getApplicationService()->getPeerTransportService());
				std::stringstream msg;
				msg << "Applying redundancy: errType = " << err->getErrorType() << " errCode = " << err->getErrorId() << " errPrio = " << err->getPriority() << " err = '" << err->getErrorText().c_str();
				XCEPT_DECLARE(psx::sapi::exception::Exception, e, msg.str());
				application->notifyQualified("warning", e);
				return;
			}
                        std::stringstream msg;
                        msg << "Encountered an error."
                            << " Type: '" << ErrTypeToString(err->getErrorType())
                            << "', ID: " << err->getErrorId()
                            << ", message: '" << err->getErrorText().c_str() << "'";
			result->setError(msg.str());
		}
	}
	else
	{	
		result->setError("Internal error during dpSet (group is 0)");
	}	

	request_->getApplicationService()->acknowledge(result);
}

void psx::sapi::DpSetHotlink::hotLinkCallBack(DpHLGroup &group)
{
	// never used
}
			


