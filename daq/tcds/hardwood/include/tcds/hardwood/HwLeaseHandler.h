#ifndef _tcds_hardwood_HwLeaseHandler_h_
#define _tcds_hardwood_HwLeaseHandler_h_

#include <string>

#include "toolbox/task/TimerListener.h"
#include "toolbox/TimeInterval.h"
#include "xoap/MessageReference.h"

namespace toolbox {
  namespace exception {
    class Listener;
  }
}

namespace toolbox {
  namespace task {
    class Timer;
    class TimerEvent;
  }
}

namespace xdaq {
  class Application;
  class ApplicationDescriptor;
}

namespace tcds {
  namespace hardwood {

    class HwLeaseHandler : public toolbox::task::TimerListener
    {

    public:
      HwLeaseHandler(xdaq::Application* const xdaqApp,
                     xdaq::ApplicationDescriptor const* const target,
                     std::string const& sessionId,
                     toolbox::TimeInterval const& interval,
                     toolbox::exception::Listener* listener=0);
      ~HwLeaseHandler();

    private:
      void timeExpired(toolbox::task::TimerEvent& event);
      void renewHardwareLease();

      xoap::MessageReference executeSOAPCommand(xoap::MessageReference& cmd);
      xoap::MessageReference sendSOAP(xoap::MessageReference& msg);

      xdaq::Application* const xdaqAppP_;
      xdaq::ApplicationDescriptor const* const targetP_;
      std::string const sessionId_;
      toolbox::TimeInterval interval_;
      std::string const timerName_;
      toolbox::task::Timer* timerP_;

    };

  } // namespace hardwood
} // namespace tcds

#endif // _tcds_hardwood_HwLeaseHandler_h_
