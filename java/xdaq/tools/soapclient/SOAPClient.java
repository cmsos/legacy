package xdaq.tools.soapclient;

import java.awt.*;
import java.net.*;
import java.security.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import org.w3c.dom.*;
import java.util.*;


import javax.xml.parsers.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import org.w3c.dom.*;

import java.awt.datatransfer.*;

public class SOAPClient  extends JApplet
{
	URL url;
	JEditorPane requestArea = null;
	JEditorPane responseArea = null;
	JTextField textField = null;
	SOAPClient c = this;
	
	String sampleSOAP = "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">\n<SOAP-ENV:Header>\n</SOAP-ENV:Header>\n<SOAP-ENV:Body>\n\t<xdaq:helloWorld xmlns:xdaq=\"urn:xdaq-soap:3.0\">\n\t</xdaq:helloWorld>\n</SOAP-ENV:Body>\n</SOAP-ENV:Envelope>";
	public void init()
	{	
		
		
		ActionListener postAction = new ActionListener()
		{
			
			public void actionPerformed ( ActionEvent e) 
			{
				// send the message
				String urn = textField.getText();
				String url = getParameter("url");
				String response = c.executeCommand(url, urn, requestArea.getText() );
				responseArea.setText(response);
			}
		};
		
		ActionListener clearAction = new ActionListener()
		{
			public void actionPerformed ( ActionEvent e) 
			{
				requestArea.setText(sampleSOAP);
				responseArea.setText("");
				textField.setText("urn:xdaq-application:lid=#");
			}
		};

		


		requestArea = new JEditorPane();
		requestArea.setText(sampleSOAP);
		JScrollPane requestScrollingArea = new JScrollPane(requestArea);


		responseArea = new JEditorPane();
		responseArea.setText("");
		

		JScrollPane responseScrollingArea = new JScrollPane(responseArea);

		JPanel windowButtons = new JPanel();
		JPanel urlField = new JPanel();
		JButton postButton = new JButton ("Send");
		postButton.addActionListener ( postAction );
		windowButtons.add ( postButton );

		JButton clearButton = new JButton ("Clear");
		clearButton.addActionListener ( clearAction );
		windowButtons.add ( clearButton );		
		
		JLabel urnLabel = new JLabel(getParameter("url") + "/" );
		urlField.add(urnLabel);
		
		textField = new JTextField("urn:xdaq-application:lid=#",20);
		urlField.add(textField);

		Box b = Box.createVerticalBox();
		Box b2 = Box.createVerticalBox();
		//b.add(Box.createRigidArea(new Dimension(2,25)));
		b.add(requestScrollingArea);
		b.add(responseScrollingArea);
		b2.add (urlField);
		b2.add (windowButtons);
		this.getContentPane().setBackground(Color.white);					
		this.getContentPane().add (b, BorderLayout.CENTER);
		this.getContentPane().add (b2, BorderLayout.SOUTH);
		
	
	}
	
	String executeCommand(String to, String urn, String soapRequest )
	{
		
		URL url = null;
		try 
		{
			//java.security.AccessController.checkPermission( new java.net.SocketPermission("lxcmd101:40000", "connect,accept"));
			url = new URL(to);
			
			//url = new URL("http://lxcmd101:40000/urn:xdaq-application:id=0");
		} catch (MalformedURLException mue)
		{
			System.out.println (mue.toString());
			return mue.toString();
		} catch (java.io.IOException ioe) {
			System.out.println (ioe.toString());
			return ioe.toString();
		}
		

		String soapResponse = "";
		try {

			// Connect 
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			
			connection.setUseCaches(false);
			//connection.connect();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			
			connection.setRequestMethod("POST");
			// Send here

			connection.setRequestProperty("Content-length", Integer.toString(soapRequest.length()));
			connection.setRequestProperty("SOAPAction",urn);
			
			//System.out.println ("Sending message:\n");
			//System.out.println (soapRequest);
			connection.connect();
			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
			writer.write(soapRequest);

			writer.flush();
			writer.close();

			// Get the response

			InputStream istream = connection.getInputStream();			
			BufferedReader ireader = new BufferedReader( new InputStreamReader(istream));
			String inputLine;
			
			while ( ( inputLine = ireader.readLine()) != null ) 
			{
				
				soapResponse = soapResponse.concat(inputLine);
			}

			//System.out.println ("Received response\n");
			//System.out.println (soapResponse);
			//System.out.println ("-----------------------------------------------------\n");

			istream.close();
			
			
		} 
		catch (IOException ioe) {
			ioe.printStackTrace();
			return ioe.toString();
		}
		return soapResponse;
	}
	
	
	
	
} // end of class


/*
		requestArea.getInputMap().put(KeyStroke.getKeyStroke("ctrl c"),"copy");
		requestArea.getActionMap().put("copy", new javax.swing.AbstractAction () { 
          			public void actionPerformed(ActionEvent e) {
	          			
					Clipboard cb = Toolkit.getDefaultToolkit(). getSystemClipboard();
					String s = requestArea.getText();
     					 StringSelection contents =  new StringSelection(s);
					cb.setContents(contents, null);
					requestArea.setText("");
					
					System.out.println("COPY");
				}});
				
		requestArea.getInputMap().put(KeyStroke.getKeyStroke("ctrl v"),"paste");
		requestArea.getActionMap().put("paste", new javax.swing.AbstractAction () { 
				public void actionPerformed(ActionEvent e) {
					Clipboard cb =Toolkit.getDefaultToolkit().getSystemClipboard();
					Transferable content = cb.getContents(this);
					try {
						String s = (String)content.getTransferData(DataFlavor.stringFlavor);
						requestArea.setText(s);
						System.out.println("Paste");
					} 
					catch (Throwable exc) {
						System.err.println(e);
				}
				System.out.println("Paste");
		}});			     
		*/
