#!/bin/bash

ADDRESS="rs232phcme01"
PORT="4001"

if [ $# -lt 1 ]; then
    echo "Current frequency: `(echo FREQUENCY?; sleep 1) | nc ${ADDRESS} ${PORT}` Hz"
else
    echo "Setting frequency to ${1} Hz"
    (echo FREQUENCY ${1}; sleep 1) | nc ${ADDRESS} ${PORT}
fi
