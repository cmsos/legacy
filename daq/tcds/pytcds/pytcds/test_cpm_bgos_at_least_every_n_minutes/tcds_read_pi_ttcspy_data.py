#!/usr/bin/env python

###############################################################################
## A little script to read the TTCSpy-in-the-PI contents.
###############################################################################

import json
import os
import sys
import urllib2

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class UpdateGetter(CmdLineBase):

    def main(self):
        # Check if the target is valid.
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_SW)

        # Now do what we have been asked to do: send a SOAP command.
        target_info = self.setup.sw_targets[self.target]
        host_name = target_info.host
        port_number = target_info.port
        lid_number = target_info.lid

        tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/update"
        url = tmp.format(host_name, port_number, lid_number)

        req = urllib2.Request(url)
        opener = urllib2.build_opener()
        f = opener.open(req)
        contents = json.load(f)
        spy_log_contents = contents["itemset-ttcspylog"]["TTCSpy log contents"]
        for (i, entry) in enumerate(spy_log_contents["data"]):
            # # NOTE: Skip the empty entry presented when the
            # # PIController is in the 'Halted' state, if we happen to
            # # catch it.
            # if entry == '-':
            #     print "PIController is Halted"
            # else:
                msg = "Entry {0:5d}: {1:s} with data 0x{2:x} at orbit {3:10d} and BX {4:d}"
                print msg.format(i,
                                 entry['type'],
                                 int(entry['data'], 16),
                                 int(entry['orbit']),
                                 int(entry['bx']))
            # if entry['Data'] == '0x0b':
            #     pdb.set_trace()

        # End of main().

    # End of class UpdateGetter.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to read the TTCSpy-in-the-PI contents."
    usage_str = "usage: %prog device"
    epilog_str_tmp = ""
    epilog_str = epilog_str_tmp.format(os.path.basename(sys.argv[0]))

    res = UpdateGetter(desc_str, usage_str, epilog_str).run()

    print "Done"

    sys.exit(res)

###############################################################################
