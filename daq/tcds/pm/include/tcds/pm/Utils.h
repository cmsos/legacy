#ifndef _tcds_pm_Utils_h_
#define _tcds_pm_Utils_h_

#include <map>
#include <string>
#include <vector>

#include "tcds/pm/Definitions.h"

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace pm {

    // Mapping of PM states to strings.
    std::string pmStateToString(tcds::definitions::PM_STATE const pmState);

    // Mapping of BST signal status codes to strings.
    std::string bstStatusToString(tcds::definitions::BST_SIGNAL_STATUS const bstStatus);

    // Mapping of DAQ link modes to strings.
    std::string daqLinkModeToString(tcds::definitions::DAQ_LINK_MODE const mode);

    std::string formatICILabel(bool const isCPMMode,
                               unsigned int const lpmNumber,
                               unsigned int const iciNumber,
                               tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                               bool const labelForExternal);
    std::string formatAPVELabel(bool const isCPMMode,
                                unsigned int const lpmNumber,
                                unsigned int const apveNumber,
                                tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                bool const labelForExternal);

    std::vector<tcds::definitions::DEADTIME_SOURCE> deadtimeSourceList();

    std::map<tcds::definitions::DEADTIME_SOURCE, std::string> deadtimeSourceRegNameMap();
    std::map<tcds::definitions::DEADTIME_SOURCE, std::string> deadtimeSourceTitleMap(bool const isShort);
    std::map<tcds::definitions::DEADTIME_SOURCE, std::string> deadtimeSourceDescMap();

    std::string deadtimeSourceToRegName(tcds::definitions::DEADTIME_SOURCE const src);
    std::string deadtimeSourceToTitle(tcds::definitions::DEADTIME_SOURCE const src,
                                      bool const isShort=false);
    std::string deadtimeSourceToDesc(tcds::definitions::DEADTIME_SOURCE const src);

    void verifyCyclicGeneratorNumber(unsigned int const genNumber);

  } // namespace pm
} // namespace tdcs

#endif // _tcds_pm_Utils_h_
