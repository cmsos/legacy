<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>CMS - Escaped (Elasticsearch capability for enhanced data aquisition)</title>
<link href="css/tables.css" rel="stylesheet" />
</head>
<body>

<?php
    
    include_once "config/config.php";
    
    require 'vendor/autoload.php';
    use Httpful\Exception;
    ini_set('display_errors', 'On');
    error_reporting(E_ALL | E_STRICT);
    
    
    // base url for proxy E
    //$hosts = array ("cms-os-escaped-01", "cms-os-escaped-02", "cms-os-escaped-03", "cms-os-escaped-04", "cms-os-escaped-05", "cms-os-escaped-06", "cms-os-escaped-07", "cms-os-escaped-08");
    //$hosts = array ("cms-os-escaped-01", "cms-os-escaped-02", "cms-os-escaped-03", "cms-os-escaped-04", "cms-os-escaped-05", "cms-os-escaped-06", "cms-os-escaped-07", "cms-os-escaped-08");
    //foreach($hosts as $host)
    //{
    $nodes = array ("E63pHLDuRvWV1cpRRpLx8Q", "dU0Dav8DQ0CyVCw50QpJzg", "LjtQZdC0SpGmxsFM-2P-8w", "qDy0HEAkRmaX4RgkIrK8lg", "KguyR6yySVeWUaclOcrMJA", "akXIOMufS6iZJ8ZrE4I8Ww", "PuJzUxJwSd296F5T7LW7NA", "uPGrF9D6QdCO9v5wW2Fu-A");
    // build url
    
    //$url_ =  "http://" . $host . ":9200/_nodes";
    $url_ = "http://cmsusr1:9090/_nodes/stats/http";
    
    //echo $host;
    
    // perform request to ES
    $response = \Httpful\Request::get($url_)->send();
    
    // parse response into object code
    $json = json_decode($response, true);
    
    echo '<br />';
    
    $header_fields = array("current_open", "total_opened");
    
    // Output table in html
    echo '<table class="xdaq-table">';
    
    foreach($nodes as $id)
    {
        //foreach ($json['nodes'][$id]['host'] as $key => $val) {
            
            //echo $val;
            
        //}
        
        echo '<thead>';
        foreach($header_fields as $name)
        {
            
            echo '<th>';
            echo $name;
            echo '</th>';
        }
        echo '</thead>';
        
        echo '<tbody>';

        foreach ($json['nodes'][$id] as $key => $val) {
            
            echo "<tr>";
            //foreach ($val ['_source'] as $key => $field) {
            
            echo '<td>';
            echo $val;
           
            echo '</td>';
            
            //}
            echo "</tr>";
        }
    }
    echo '</tbody>';
    echo '</table>';
    
    
    ?>

</body>
</html>
