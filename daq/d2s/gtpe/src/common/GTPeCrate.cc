/**
*      @file GTPeCrate.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.10 $
*     $Date: 2007/03/27 08:02:35 $
*
*
**/
#include "d2s/gtpe/GTPeCrate.hh"

#include "d2s/gtpe/GTPeCard.hh"
#include "d2s/gtpe/GTPeHardcodedAddressTableReader.hh"

#include "hal/PCIDevice.hh"
#include "hal/PCILinuxBusAdapter.hh"
#include "hal/PCIDummyBusAdapter.hh"
#include "hal/NoSuchDeviceException.hh"


#include "xcept/Exception.h"

#include <stdint.h>
#include <iostream>

d2s::GTPeCrate::GTPeCrate(bool dummy) 
    
  : _busadapter(0),
    _gtpe_addresstable(0),
    _gtpe(0) {
  
  try {
    if (dummy)
      _busadapter = new HAL::PCIDummyBusAdapter();
    else
      _busadapter = new HAL::PCILinuxBusAdapter();
  
    //
    // detect and instantiate GTPes
    //
  
//     char *xdr = getenv("XDAQ_ROOT");

//     if (!xdr)
//       XCEPT_RAISE(xcept::Exception, "XDAQ_ROOT not set. Cannot locate address table file.");

//     std::string gtpe_addresstablefile = std::string(xdr) +
//       "/daq/d2s/gtpe/xml/GTPEmulatorAddressMap.dat";
    
//     // reader is only needed in constructor of address table => ok to use a local variable
//     HAL::PCIAddressTableASCIIReader reader(gtpe_addresstablefile);
    d2s::GTPeHardcodedAddressTableReader reader;
    
    _gtpe_addresstable = new HAL::PCIAddressTable("GTPe address table", reader);

    const uint32_t GTPeVendorID = 0xecd6;
    const uint32_t GTPeDeviceID = 0x9dbe;

//     cout << "*** Detecting GTPes in the crate" << endl;
//     cout << "vendorID : " << hex << GTPeVendorID << endl;
//     cout << "deviceID : " << hex << GTPeDeviceID << endl;
    
    uint32_t index = 0;
    try {
      HAL::PCIDevice* dev = new HAL::PCIDevice(*_gtpe_addresstable, *_busadapter, GTPeVendorID, GTPeDeviceID, index);
      _gtpe = new d2s::GTPeCard(dev);
    }
    catch(HAL::NoSuchDeviceException & e) {
      XCEPT_RETHROW(xcept::Exception, "No GTPe found in the crate.", e); 
    }
    catch(xcept::Exception & e) {
      XCEPT_RETHROW(xcept::Exception, "Error instantiating PCIDecive or GTPeCard.", e); 
    }

  }
  catch (...) {
    cleanup();
    throw;   // rethrow any other exception after cleaning up
  }
}


// clean up all dynamically created objects belonging to GTPeCrate.
// only called upon exception in constructor and in destructor
void d2s::GTPeCrate::cleanup() {
  if (_gtpe) { 
    delete _gtpe; _gtpe=0; 
  }
  if (_gtpe_addresstable) { 
    delete _gtpe_addresstable; _gtpe_addresstable=0; 
  }
  if (_busadapter) {
    delete _busadapter;
    _busadapter=0;
  }
}

d2s::GTPeCrate::~GTPeCrate() {
  cleanup();
}

d2s::GTPeCard& d2s::GTPeCrate::getGTPe() 
   {

  if (_gtpe == 0)
    XCEPT_RAISE (xcept::Exception, "GTPe not present.");

  return *_gtpe;
}

