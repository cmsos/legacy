#include "tcds/brildaqchecker/BRILDAQHistory.h"

#include <iostream>
#include <sstream>

#include "xdata/Serializable.h"
#include "xdata/TimeVal.h"

#include "tcds/utils/Utils.h"

tcds::brildaqchecker::BRILDAQHistory::BRILDAQHistory(std::vector<xdata::Table> const& dataIn) :
  nibbles_(dataIn)
{
}

tcds::brildaqchecker::BRILDAQHistory::~BRILDAQHistory()
{
}

std::string
tcds::brildaqchecker::BRILDAQHistory::getJSONString() const
{
  std::stringstream res;
  res << std::dec;

  res << "[";
  size_t index = 0;
  for (std::vector<xdata::Table>::const_iterator it = nibbles_.begin();
       it != nibbles_.end();
       ++it)
    {
      std::vector<std::string> const columns = it->getColumns();

      res << "{";

      for (std::vector<std::string>::const_iterator col = columns.begin();
           col != columns.end();
           ++col)
        {
          if (col != columns.begin())
            {
              res << ", ";
            }

          std::string valStr = it->getValueAt(0, *col)->toString();
          xdata::Serializable* tmp = it->getValueAt(0, *col);

          // Is this an xdata::TimeVal?
          xdata::TimeVal* const val = dynamic_cast<xdata::TimeVal*>(tmp);
          if (val)
            {
              valStr = tcds::utils::formatTimestamp(*val);
            }

          res << tcds::utils::escapeAsJSONString(*col)
              << ": "
              << tcds::utils::escapeAsJSONString(valStr);
        }

      res << "}";
      if (index != (nibbles_.size() - 1))
        {
          res << ", ";
        }
      ++index;
    }
  res << "]";

  return res.str();
}
