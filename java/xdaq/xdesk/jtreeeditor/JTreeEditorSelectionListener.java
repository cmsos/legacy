package xdaq.xdesk.jtreeeditor;

import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import xdaq.xdesk.tools.IUserObject;

public class JTreeEditorSelectionListener
    implements TreeSelectionListener {
  /**Keeps a last correct selection*/
  private TreePath[] previousPath = null;

//******************************************************************************
   /**Called whenever the value of the selection changes.*/
   public void valueChanged(TreeSelectionEvent e) {
     TreePath[] paths = null;
     JTree tree = null;
     int selectionMode = 1;
     if (e.getSource() instanceof JTree) {
       tree = (JTree) e.getSource();
       selectionMode = tree.getSelectionModel().
           getSelectionMode();
       paths = tree.getSelectionPaths();
     }
     // Protect against empty selection
     if (paths == null) {
       return;
     }
     // consistency check
     // remember the last item of the first selection.
     // all other selected items must be the same
     DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[paths.length -
         1].getLastPathComponent();
     IUserObject adapterNode = null;
     try {
       adapterNode = (IUserObject) node.getUserObject();
     }
     catch (Exception ex) {
       return;
     }
     String lastElement = adapterNode.getName();
     for (int i = 0; i < paths.length - 1; i++) {
       node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();

       String currentElement = ( (IUserObject) node.getUserObject()).getName();

       // Transport and Application are equivalent
       if (currentElement.startsWith("Transport")) {
         currentElement = "Application";
       }
       else if (currentElement.startsWith("Application")) {
         currentElement = "Transport";
       }

       if (!lastElement.startsWith(currentElement) &&
           selectionMode == 4) {
         JOptionPane.showMessageDialog(null,
                                       "Must select nodes of the same type",
                                       "Selection failed",
                                       JOptionPane.ERROR_MESSAGE);
         if (tree != null) {
           tree.setSelectionPaths(previousPath);
         }
         return;
       }
     }
     previousPath = paths;
   }
}
