package xmltreeeditor;

import javax.swing.tree.*;
import org.w3c.dom.*;

class XMLTreeRootNode
    extends DefaultMutableTreeNode {

  public XMLTreeRootNode(Document doc) {
    super(makeRootNode(doc));
    create(doc);

  }

  public void create(Document doc){
    doc.getDocumentElement().normalize();
    Element rootElement = doc.getDocumentElement();
    this.add(buildTree(rootElement));
  }
  private static DefaultMutableTreeNode
      makeRootNode(Document document) {
    try {

      document.getDocumentElement().normalize();
      Element rootElement = document.getDocumentElement();
      DefaultMutableTreeNode rootTreeNode =
          buildTree(rootElement);
      return (rootTreeNode);
    }
    catch (Exception e) {
      String errorMessage =
          "Error making root node: " + e;
      System.err.println(errorMessage);
      e.printStackTrace();
      return (new DefaultMutableTreeNode(errorMessage));
    }
  }

//Creating nodes of the tree

  private static DefaultMutableTreeNode
  buildTree(Element rootElement) {
    DefaultMutableTreeNode rootTreeNode =new DefaultMutableTreeNode(new XMLTreeNodeUserObject(rootElement));
    addChildren(rootTreeNode, rootElement);
    return (rootTreeNode);
  }

//Adding childe nodes to the tree

  private static void addChildren
      (DefaultMutableTreeNode parentTreeNode,
       Node parentXMLElement) {

    NodeList childElements =
        parentXMLElement.getChildNodes();
    for (int i = 0; i < childElements.getLength(); i++) {
      Node childElement = childElements.item(i);

      switch (childElement.getNodeType()) {
        case 1:

          DefaultMutableTreeNode childTreeNode =
              new DefaultMutableTreeNode(new XMLTreeNodeUserObject(childElement));

          NamedNodeMap args = childElement.getAttributes();

          if (args != null && args.getLength() > 0) {
            for (int j = 0; j < args.getLength(); j++) {
              Node attr1 = args.item(j);

              childTreeNode.add(new DefaultMutableTreeNode(new
                  XMLTreeNodeUserObject(attr1)));
              addChildren(childTreeNode, (Node) args.item(j));
            }
          }

          parentTreeNode.add(childTreeNode);
          addChildren(childTreeNode, childElement);

          break;
        case 4:
        case 8:
        case 7:
        case 3:
          if (childElement.getNodeValue().trim().length() != 0) {
            DefaultMutableTreeNode childTreeNode1 =
                new DefaultMutableTreeNode(new XMLTreeNodeUserObject(
                childElement));
            parentTreeNode.add(childTreeNode1);
            addChildren(childTreeNode1, childElement);
          }
          break;
      }

    }
  }

}
