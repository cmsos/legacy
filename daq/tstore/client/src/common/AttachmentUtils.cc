// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "tstore/client/AttachmentUtils.h"
#include "xdata/exdr/AutoSizeOutputStreamBuffer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"

#include <algorithm>

/* Local function, not in any namespace or header file */
std::string MIMETypeFromXdataType(std::string xdataType) {
	std::replace(xdataType.begin(),xdataType.end(),' ','+');
	return "application/xdata+"+xdataType;
}

bool getAttachment(xoap::MessageReference message,xdata::Serializable &attachment,std::string contentID)  {
	std::string type=MIMETypeFromXdataType(attachment.type());
	std::list<xoap::AttachmentPart*> attachments = message->getAttachments();
	std::list<xoap::AttachmentPart*>::iterator attachmentIterator;
	for (attachmentIterator=attachments.begin();attachmentIterator!=attachments.end();++attachmentIterator) {
		if ((*attachmentIterator)->getContentId()==contentID || contentID.empty()) {
			if ((*attachmentIterator)->getContentType() == type) {
			    xdata::exdr::FixedSizeInputStreamBuffer inBuffer((*attachmentIterator)->getContent(),(*attachmentIterator)->getSize());
			    std::string contentEncoding = (*attachmentIterator)->getContentEncoding();
			    std::string contentId = (*attachmentIterator)->getContentId();                          
			    try {
			      xdata::exdr::Serializer serializer;
			      serializer.import(&attachment, &inBuffer );
			      return true;
			    }
			    catch(xdata::exception::Exception & e ) {
			      // failed to import attachment
				  XCEPT_RETHROW(xoap::exception::Exception,"Can not read attachment with ID "+(*attachmentIterator)->getContentId()+". "+e.message(),e);
			    }
			  }
			else {
				if (!contentID.empty()) {
					//the attachment with the specified ID is of the wrong type.
					XCEPT_RAISE(xoap::exception::Exception,"Can not read attachment of type "+(*attachmentIterator)->getContentType()+" into a "+attachment.type());
				}
			}
		}
	}
	return false;
}

//I suppose you know the type of an attachment with a certain ID, but it's still not safe to not check.
bool tstoreclient::getAttachmentWithID(xoap::MessageReference message,xdata::Serializable &attachment,std::string contentID)  {
	if (!contentID.empty()) return getAttachment(message,attachment,contentID);
	else return false;
}

bool tstoreclient::getFirstAttachmentOfType(xoap::MessageReference message,xdata::Serializable &attachment)  {
	return getAttachment(message,attachment,"");
}

void tstoreclient::addAttachment(xoap::MessageReference message,xdata::Serializable &data,std::string contentId)  { //this should perhaps throw a tstore client exception
	try {
		xdata::exdr::AutoSizeOutputStreamBuffer outBuffer;
		xdata::exdr::Serializer serializer;
		serializer.exportAll( &data, &outBuffer );
		xoap::AttachmentPart * attachment = message->createAttachmentPart(outBuffer.getBuffer(), outBuffer.tellp(),MIMETypeFromXdataType(data.type()));
		attachment->setContentEncoding("exdr");
		attachment->setContentId(contentId);
		message->addAttachmentPart(attachment);
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW(xoap::exception::Exception,"Could not add attachment with content ID '"+contentId+"'",e);
	}
}
