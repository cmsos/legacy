// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Mapping_h_
#define _Mapping_h_

#include <string>
#include "exception/Exception.h"

namespace tstore {

class Mapping {
	public:
		Mapping(std::string xdataType,std::string columnName,std::string tableName="",std::string dbType="",std::string format="");
		Mapping() {}
		bool matchesTable(std::string tableName) const ;
		bool matchesColumn(std::string columnName) const ;
		bool matchesExactly() {
			return exactMatch_;
		}
		std::string xdataType() const {
			return xdataType_;
		}
		std::string format() const {
			return format_;
		}
		/*returns a string representing the column data types that this mapping should apply to (or an empty string
		if it applies to all data types.) The types will be ANSI SQL data types, it is up to a Connection subclass
		to match these to the types available in a particular kind of database.*/
		std::string dbType() const {
			return dbType_;
		}
		//if you setMatchesExactly(true) then the mapping will only apply to columns and tables whose names match exactly, with no regular expression matching
		void setMatchesExactly(bool exactMatch=true) {
			exactMatch_=exactMatch;
		}
	private:
		std::string columnName_;
		std::string tableName_;
		std::string xdataType_;
		std::string dbType_;
		std::string format_;
		bool exactMatch_;
};

typedef std::vector<tstore::Mapping> MappingList;

}

#endif
