#ifndef _tcds_pm_SchedulingInfoSpaceUpdater_h_
#define _tcds_pm_SchedulingInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pm {

    class SchedulingInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      SchedulingInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                 tcds::hwlayertca::TCADeviceBase const& hw);
      virtual ~SchedulingInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayertca::TCADeviceBase const& getHw() const;

      tcds::hwlayertca::TCADeviceBase const& hw_;

    };

  } // namespace pm
} // namespace tcds

#endif // _tcds_pm_SchedulingInfoSpaceUpdater_h_
