// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                 			 *
 * All rights reserved.                                                  *
 * Authors:  P. Roberts				 									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _xmem_probe_Application_h_
#define _xmem_probe_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/mem/Pool.h"
#include "toolbox/mem/exception/Exception.h"

namespace xmem
{
	namespace probe
	{

		class Application: public xdaq::Application, public xgi::framework::UIManager
		{

			public:

				XDAQ_INSTANTIATOR();

				Application(xdaq::ApplicationStub * s) ;

				void Default(xgi::Input * in, xgi::Output * out) ;
				void flexDisplay(xgi::Input * in, xgi::Output * out) ;

				void listPools(xgi::Input * in, xgi::Output * out) ;
				void displayUsage(xgi::Input * in, xgi::Output * out) ;
				void selfTest(xgi::Input * in, xgi::Output * out) ;

			protected:

				void allocateBlocks(toolbox::mem::Pool* pool) ;
				void allocateBlocks2(toolbox::mem::Pool* pool) ;
				void allocateBlocks3(toolbox::mem::Pool* pool) ;
				std::string qualifiedSize(size_t size);

		};

	}
}

#endif
