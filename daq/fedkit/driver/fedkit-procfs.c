/* ---------------------------------------------------------------------- */
/* procfs interface for the fedkit kernel driver */
/* ---------------------------------------------------------------------- */

#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/seq_file.h>
#include <linux/pci.h>
#include <linux/mutex.h>

//#include <asm/semaphore.h>

#include "../include/fedkit-private.h"

extern struct kfedkit_receiver_t  *kfedkit_receivers;
#ifdef UNLOCKED
extern struct mutex kfedkit_receivers_lock;
#else
extern struct semaphore kfedkit_receivers_lock;
#endif

extern struct kfedkit_sender_t  *kfedkit_senders;
#ifdef UNLOCKED
extern struct mutex kfedkit_senders_lock;
#else
extern struct semaphore kfedkit_senders_lock;
#endif

static struct proc_dir_entry *kfedkit_procfs_topdir;
static struct proc_dir_entry *kfedkit_procfs_rxdir;
static struct proc_dir_entry *kfedkit_procfs_txdir;


/* forward declarations */
static int kfedkit_proc_open(struct inode *inode, struct file *file);

static struct file_operations kfedkit_proc_ops = {
  .owner = THIS_MODULE,
  .open = kfedkit_proc_open,
  .read = seq_read,              // then uses the newer interface ?
  .llseek = seq_lseek,
  .release = seq_release
};

//----------------------------------------

ssize_t kfedkit_procfs_reset_file_write(struct file* file, const char* buffer, size_t count, loff_t* data);
static int kfedkit_procfs_reset_file_open(struct inode *inode, struct file *file);

static struct file_operations kfedkit_proc_reset_file_ops = {
  .owner = THIS_MODULE,
  .open = kfedkit_procfs_reset_file_open,
  .write = kfedkit_procfs_reset_file_write,
  // .llseek = seq_lseek,
  // .release = seq_release
};

//----------------------------------------

ssize_t kfedkit_procfs_block_file_read(struct file* file, const char* buffer, size_t count, loff_t* data);
static int kfedkit_procfs_block_file_open(struct inode *inode, struct file *file);


static struct file_operations kfedkit_proc_block_ops = {
  .owner = THIS_MODULE,
  .open = kfedkit_procfs_block_file_open,
  .read = seq_read,              // then uses the newer interface ?
  .llseek = seq_lseek,
  .release = seq_release
};


static void *kfedkit_seq_start(struct seq_file *s, loff_t *pos);
static void *kfedkit_seq_next(struct seq_file *s, void *iterator_data, loff_t *pos);
static void kfedkit_seq_stop(struct seq_file *p, void *v);
static int kfedkit_seq_block_show(struct seq_file *s, void *iterator_data);

static struct seq_operations kfedkit_block_seq_ops = {
  .start = kfedkit_seq_start,
  .next = kfedkit_seq_next,
  .stop = kfedkit_seq_stop,
  .show = kfedkit_seq_block_show
};



static int kfedkit_procfs_block_file_open(struct inode *inode, struct file *file)
{
  int retval;

  /* declare that we are using the new interface ?! */
  retval = seq_open(file, &kfedkit_block_seq_ops);

  /* get the pointer to the corresponding proc dir entry */
  if (!retval) 
    {
      struct seq_file *seq = file->private_data;
      if (seq != NULL)
#ifdef UNLOCKED
	seq->private = PDE_DATA(inode);
#else
	seq->private = PDE(inode);
#endif
    }

  return retval;
}

static int kfedkit_seq_block_show(struct seq_file *seq, void *iterator_data)
{
  size_t device_index;
  struct kfedkit_receiver_t *receiver;
  int receiver_index;
#ifdef UNLOCKED
  device_index = (size_t) seq->private;
#else
  struct proc_dir_entry *pde;
  pde = (struct proc_dir_entry*) seq->private;
  device_index = (size_t) pde->data;
#endif
  receiver_index = device_index - MAX_FEDKIT_SENDER_NUMBER;

  // lock access to the list of receivers data structure
#ifdef UNLOCKED
  if (mutex_lock_interruptible(&kfedkit_receivers_lock))
    return -ERESTARTSYS;
#else
  if (down_interruptible(&kfedkit_receivers_lock))
    return -ERESTARTSYS;
#endif

  /* find the receiver struct */
  for (receiver = kfedkit_receivers; receiver != NULL; receiver=receiver->next) 
    {
      if (receiver->index == receiver_index) break;
    }

  if (receiver == NULL)
    // should we return -ENOENT ?
    goto end;


  // lock this particular receiver
#ifdef UNLOCKED
  if (mutex_lock_interruptible(&receiver->lock))
  {
    // acquisition of lock failed (should be -EINTR)
    mutex_unlock(&kfedkit_receivers_lock);
    return -ERESTARTSYS;
  }
#else
  if (down_interruptible(&receiver->lock))
  {
    // acquisition of lock failed (should be -EINTR)
    up (&kfedkit_receivers_lock);
    return -ERESTARTSYS;
  }
#endif

  // check whether we have at least one buffer
  // allocated
  if (receiver->block_number > 0)
  {
    // physical/bus address of first block
    // this is actually the next block to be read ?!
    void *blockAddr = receiver->data_fifo_tab[
					      // receiver->data_fifo->read // next block to be read
					      0       // first allocated block
                                              ];

    // for simplicity (it's not clear to me whether in the case
    // when the user program provides the blocks, e.g. using cmem_rcc
    // that the kernel driver also knows the virtual address ?!)
    // convert the physical address back to a kernel virtual address
    void *blockAddrVirt = phys_to_virt((unsigned long)blockAddr);

    // TODO: how to make sure that the buffer we write to contains
    //       enough space ?
    // seq_write(seq, blockAddrVirt, receiver->block_size);
    size_t j;
    for (j = 0; j < receiver->block_size; ++j)
      {
	seq_putc(seq, ((const unsigned char *)(blockAddrVirt))[j]);
      }
  }

#ifdef UNLOCKED
  mutex_unlock(&receiver->lock);
#else
  up(&receiver->lock);
#endif

 end:

  // printk(KERN_CRIT __FILE__ " :%d\n"  , __LINE__);
#ifdef UNLOCKED
  mutex_unlock(&kfedkit_receivers_lock);
#else
  up (&kfedkit_receivers_lock);
#endif

  // printk(KERN_CRIT __FILE__ " :%d\n"  , __LINE__);
  return 0;
  
}


//----------------------------------------------------------------------


/* ---------------------------------------------------------------------- */

void kfedkit_init_procfs(void)
{
    size_t i;

    struct proc_dir_entry *entry;

    /* setup /proc files */
    kfedkit_procfs_topdir = proc_mkdir("driver/fedkit",
			    NULL /* parent directory */
			    );

    kfedkit_procfs_rxdir = proc_mkdir("driver/fedkit/receiver",
			    NULL /* parent directory */
			    );

    kfedkit_procfs_txdir = proc_mkdir("driver/fedkit/sender",
			    NULL /* parent directory */
			    );


  
    for (i = 0; i < MAX_FEDKIT_SENDER_NUMBER; ++i)
      {
	char buf[100];

	//	sprintf(buf,"driver/fedkit/sender/%d",i);

//	create_proc_read_entry(buf,
//			       0, /* default mode */
//			       NULL, /* parent directory */
//			       NULL, /* reading function */
//			       NULL  /* user data */
//			       /* kfedkit_procfs_read*/
//			       );
//

	sprintf(buf,"driver/fedkit/receiver/%zu",i);
	proc_mkdir(buf,
		   NULL /* parent directory */
		   );

	//--------------------
	// create per receiver proc file with general information
	//--------------------

	sprintf(buf,"driver/fedkit/receiver/%zu/info",i);
#ifdef UNLOCKED
	entry = proc_create_data(buf, 0, NULL, &kfedkit_proc_ops, (void*) (i + MAX_FEDKIT_SENDER_NUMBER));
#else
	entry = create_proc_entry(buf, 0, NULL);

	if (entry != NULL)
	  {
	    entry->proc_fops = &kfedkit_proc_ops;    
	    entry->data = (void*) (i + MAX_FEDKIT_SENDER_NUMBER); /* offset for receivers */
	  }
#endif

	//--------------------
	// create per receiver proc file for reading block 0
	//--------------------

	sprintf(buf,"driver/fedkit/receiver/%zu/block%u",i,0);
#ifdef UNLOCKED
        entry = proc_create_data(buf, 0, NULL, &kfedkit_proc_block_ops, (void*) (i + MAX_FEDKIT_SENDER_NUMBER));
#else
	entry = create_proc_entry(buf, 0, NULL);

	if (entry != NULL)
	  {
	    entry->proc_fops = &kfedkit_proc_block_ops;    
	    entry->data = (void*) (i + MAX_FEDKIT_SENDER_NUMBER); /* offset for receivers */
	  }
#endif


	//--------------------
	// create a file for resetting the card
	//--------------------

	sprintf(buf,"driver/fedkit/receiver/%zu/reset",i);
#ifdef UNLOCKED
        entry = proc_create_data(buf, S_IWUSR, NULL, &kfedkit_proc_reset_file_ops, (void*) (i + MAX_FEDKIT_SENDER_NUMBER));
#else
	entry = create_proc_entry(buf, S_IWUSR, NULL);

	if (entry != NULL)
	  {
	    entry->proc_fops = &kfedkit_proc_reset_file_ops;    
	    entry->data = (void*) (i + MAX_FEDKIT_SENDER_NUMBER); /* offset for receivers */
	  }
#endif

	//--------------------
	// create per sender proc file
	//--------------------
	sprintf(buf,"driver/fedkit/sender/%zu",i);
	proc_mkdir(buf,
		   NULL /* parent directory */
		   );

	sprintf(buf,"driver/fedkit/sender/%zu/info",i);
#ifdef UNLOCKED
        entry = proc_create_data(buf, 0, NULL,  &kfedkit_proc_ops, (void*) i);
#else
	entry = create_proc_entry(buf, 0, NULL);

	if (entry != NULL)
	  {
	    entry->proc_fops = &kfedkit_proc_ops;    
	    entry->data = (void*) i; /* no offset for senders */
	  }
#endif

      }
}

/* ---------------------------------------------------------------------- */

/** removes entries from procfs */
void kfedkit_release_procfs(void)
{
  unsigned i;

  /* remove directories in /proc */
  for (i = 0; i < MAX_FEDKIT_SENDER_NUMBER; ++i)
    {
      char buf[100];

      // sprintf(buf,"driver/fedkit/sender/%d",i);
      // remove_proc_entry(buf,NULL);

      sprintf(buf,"driver/fedkit/receiver/%u/block%u",i,0);
      remove_proc_entry(buf,NULL);

      sprintf(buf,"driver/fedkit/receiver/%u/info",i);
      remove_proc_entry(buf,NULL);

      sprintf(buf,"driver/fedkit/receiver/%u/reset",i);
      remove_proc_entry(buf,NULL);

      sprintf(buf,"driver/fedkit/sender/%u/info",i);
      remove_proc_entry(buf,NULL);

      // remove per card directories
      sprintf(buf,"driver/fedkit/receiver/%u",i);
      remove_proc_entry(buf, NULL /* kfedkit_procfs_topdir */);

      sprintf(buf,"driver/fedkit/sender/%u",i);
      remove_proc_entry(buf, NULL /* kfedkit_procfs_topdir */);
    }


  remove_proc_entry("driver/fedkit/receiver", NULL /* kfedkit_procfs_topdir */);
  remove_proc_entry("driver/fedkit/sender", NULL /* kfedkit_procfs_topdir */);
  remove_proc_entry("driver/fedkit", NULL);
}

/* ---------------------------------------------------------------------- */

/** @return what is later used as iterator_data */
static void *kfedkit_seq_start(struct seq_file *s, loff_t *pos)
{
  //  printk(KERN_INFO "MY: entering seq_start *pos=%lld\n", *pos);

  //  if (*pos >= kfedkit_nr_devs)
  if (*pos >= 1)
    return NULL; /* Nothing more to read */
  // return kfedkit_devices + *pos;
  return (void *)1;
}

/* ---------------------------------------------------------------------- */

static void *kfedkit_seq_next(struct seq_file *s, void *iterator_data, loff_t *pos)
{
  //  printk(KERN_INFO "MY: entering seq_next *pos=%lld\n", *pos);

  (*pos)++;
  if (*pos >= 1)
    return NULL;
  // return kfedkit_devices + *pos;
  return (void *)1;
}

/* ---------------------------------------------------------------------- */

#define CUSTOMBIT(value, mask) ((value & mask) ? 1 : 0)

static int kfedkit_seq_show_receiver(struct seq_file *s, int receiver_index)
{
  // struct kfedkit_qset *d;
  // int i;
  struct kfedkit_receiver_t *receiver;

  //  printk(KERN_INFO "MY: entering show_receiver, s=%p\n",s);

#ifdef UNLOCKED
  if (mutex_lock_interruptible(&kfedkit_receivers_lock))
    return -ERESTARTSYS;
#else
  if (down_interruptible(&kfedkit_receivers_lock))
    return -ERESTARTSYS;
#endif

  /* find the receiver struct */
  for (receiver = kfedkit_receivers; receiver != NULL; receiver=receiver->next) 
    {
      if (receiver->index == receiver_index) break;
    }

  if (receiver == NULL)
    // should we return -ENOENT ?
    seq_printf(s, "Receiver %d is not open\n", receiver_index);
  else
    {
      uint32_t reg;

      // lock this particular receiver
#ifdef UNLOCKED
      if (mutex_lock_interruptible(&receiver->lock))
      {
	// failed to acquire the lock
	mutex_unlock(&kfedkit_receivers_lock);
	return -ERESTARTSYS;
      }
#else
      if (down_interruptible(&receiver->lock))
      {
	// failed to acquire the lock
	up (&kfedkit_receivers_lock);
	return -ERESTARTSYS;
      }
#endif

      seq_printf(s,"Receiver %d is open\n", receiver_index);
      seq_printf(s,"\n");
      seq_printf(s,"in use:                        %s\n",receiver->inUse ? "yes" : "no");
      seq_printf(s,"\n");
      seq_printf(s,"FPGA version:                  0x%08X\n",receiver->FPGA_version);
      seq_printf(s,"\n");
      seq_printf(s,"interrupt_registered:          %s\n",receiver->interrupt_registered ? "yes" : "no");
      seq_printf(s,"number of received interrupts: %u\n",receiver->num_interrupts);
      seq_printf(s,"number of received interrupts while receiver not active: %u\n",receiver->num_interrupts_with_receiver_inactive);

      seq_printf(s,"irq number: %d\n",receiver->device->irq);

      seq_printf(s,"\n");
      seq_printf(s,"number of writes to free block fifo: %u\n",receiver->num_free_block_fifo_entries_written);
      seq_printf(s,"\n");

      /* -------------------- */

      seq_printf(s,"\n");
      seq_printf(s,"receiver->block_number:         %d\n",receiver->block_number);
      seq_printf(s,"receiver->block_size:           %d\n",receiver->block_size);
      seq_printf(s,"do allocate blocks:             %d\n",receiver->do_allocate_blocks);
      seq_printf(s,"\n");

      seq_printf(s,"receiver->block_fifo (kern addr): 0x%16p\n",receiver->block_fifo);
      seq_printf(s,"receiver->block_fifo (bus addr):  0x%16p\n",(void*)virt_to_bus(receiver->block_fifo));
      seq_printf(s,"receiver->block_fifo (phys addr): 0x%16p\n",(void*)virt_to_phys(receiver->block_fifo));
      seq_printf(s,"\n");

      seq_printf(s,"receiver->blocks     (kern addr): 0x%p\n",receiver->blocks);
      seq_printf(s,"receiver->blocks     (bus addr):  0x%16p\n",(void*)virt_to_bus(receiver->blocks));
      seq_printf(s,"receiver->blocks     (phys addr): 0x%16p\n",(void*)virt_to_phys(receiver->blocks));
      seq_printf(s,"\n");

      seq_printf(s,"receiver->wc_fifo    (kern addr): 0x%p\n",receiver->wc_fifo);
      seq_printf(s,"receiver->wc_fifo    (bus addr):  0x%16p\n",(void*)virt_to_bus(receiver->wc_fifo));
      seq_printf(s,"receiver->wc_fifo    (phys addr): 0x%16p\n",(void*)virt_to_phys(receiver->wc_fifo));
      seq_printf(s,"\n");
      
      seq_printf(s,"\n");
      /* -------------------- */

      if (receiver->data_fifo != NULL)
	{
	  // this is actually _fedkit_fifo_free_slot_count but fedkit-wc-fifo.h includes
	  // many other, user space things, so we copy the functionality here..
	  int used = receiver->data_fifo->read - receiver->data_fifo->write - 1;
	  if (used < 0)
	    used += receiver->data_fifo->size;
	  else if (used >= receiver->data_fifo->size)
	    used -= receiver->data_fifo->size;
	  
	  seq_printf(s,"\n");
	  seq_printf(s,"data fifo information:\n");

	  seq_printf(s,"  interrupt disabled:             %d\n",receiver->data_fifo->int_disabled);
	  seq_printf(s,"  receiver->data_fifo->read       %d\n",receiver->data_fifo->read);
	  seq_printf(s,"  receiver->data_fifo->write      %d\n",receiver->data_fifo->write);
	  seq_printf(s,"  receiver->data_fifo->done_read  %d\n",receiver->data_fifo->done_read);
	  seq_printf(s,"  receiver->data_fifo->done_write %d\n",receiver->data_fifo->done_write);

	  seq_printf(s,"  used                            %d\n",used);
	} 
      else
	seq_printf(s,"receiver->data_fifo is NULL\n");

      /* -------------------- */

      if (receiver->wc_fifo != NULL)
	{
	  struct _FK_wc_fifo *wc_fifo = (struct _FK_wc_fifo *) receiver->wc_fifo;
	  int wr = wc_fifo->write & _FK_wc_fifo_mask;
	  int rd = wc_fifo->read & _FK_wc_fifo_mask;
	  int used = (wr - rd) & _FK_wc_fifo_mask;

	  seq_printf(s,"\n");
	  seq_printf(s,"wc fifo information:\n");
	  seq_printf(s,"  wc fifo address                 0x%p\n", receiver->wc_fifo);
	  seq_printf(s,"  struct size                     %lu\n", sizeof (struct _FK_wc_fifo));
	  


	  seq_printf(s,"  receiver->wc_fifo->read         %d\n",rd);
	  seq_printf(s,"  receiver->wc_fifo->write        %d\n",wr);

	  seq_printf(s,"  used                            %d\n",used);
	  seq_printf(s,"  free slot count                 %d\n",_FK_wc_fifo_depth - used);
	  seq_printf(s,"  _FK_wc_fifo_depth               %d\n",_FK_wc_fifo_depth);
	}

      /* -------------------- */

      // seq_printf(s,"\n");

      // this is write-only
      // seq_printf(s,"content of block size register: %d\n", receiver->map0[_FK_BKSZ_OFFSET / sizeof(uint32_t)]);

      // write only ?
      // seq_printf(s,"word count fifo address:        0x%08x\n", receiver->map0[_FK_WCFADDR_OFFSET / sizeof(uint32_t)]);

      // write only ?
      // seq_printf(s,"WCFWADDR                        0x%08x\n",receiver->map0[_FK_WCFWADDR_OFFSET/4]);

      // write only ?
      // seq_printf(s,"WCFR                            0x%08x\n",receiver->map0[_FK_WCFR_OFFSET/4]);

      // write only ?
      // seq_printf(s,"FRBKFIFO                        0x%08x\n",receiver->map0[_FK_FRBKFIFO_OFFSET/4]);

      /* -------------------- */

      seq_printf(s,"\n");

      reg = receiver->map0[_FK_CSR_OFFSET / sizeof(uint32_t)];
      seq_printf(s,"CSR register content:           0x%08x\n", reg);
      seq_printf(s,"   0: EMPTY_INT_STATUS          %d\n", CUSTOMBIT(reg, _FK_CSR_EMPTY_INT_STATUS)); 
      seq_printf(s,"   1: HALF_EMPTY_INT_STATUS     %d\n", CUSTOMBIT(reg, _FK_CSR_HALF_EMPTY_INT_STATUS)); 
      seq_printf(s,"   8: EMPTY_INT_ENABLE          %d\n", CUSTOMBIT(reg, _FK_CSR_EMPTY_INT_ENABLE)); 
      seq_printf(s,"   9: HALF_EMPTY_INT_ENABLE     %d\n", CUSTOMBIT(reg, _FK_CSR_HALF_EMPTY_INT_ENABLE)); 
      seq_printf(s,"  14: SLINK_DOWN                %d\n", CUSTOMBIT(reg, _FK_CSR_SLINK_DOWN));
      seq_printf(s,"  15: SLINK_UP                  %d\n", CUSTOMBIT(reg, _FK_CSR_SLINK_UP));
      seq_printf(s,"  16: ABORT                     %d\n", CUSTOMBIT(reg, _FK_CSR_ABORT));
      seq_printf(s,"  17: SOFT_RESET                %d\n", CUSTOMBIT(reg, _FK_CSR_SOFT_RESET));
      seq_printf(s,"  18: LINK_CTRL                 %d\n", CUSTOMBIT(reg, _FK_CSR_LINK_CTRL));
      seq_printf(s,"  19: AUTOSEND_FULL             %d\n", CUSTOMBIT(reg, _FK_CSR_AUTOSEND_FULL));
      seq_printf(s,"  25: LINK0_ENABLE              %d\n", CUSTOMBIT(reg, _FK_CSR_LINK0_ENABLE));
      seq_printf(s,"  26: LINK1_ENABLE              %d\n", CUSTOMBIT(reg, _FK_CSR_LINK1_ENABLE));
      seq_printf(s,"  28: LINK_DEBUG                %d\n", CUSTOMBIT(reg, _FK_CSR_LINK_DEBUG));
      seq_printf(s,"  31: LINK_IS_UP                %d\n", CUSTOMBIT(reg, _FK_CSR_LINK_IS_UP));
      seq_printf(s,"  other bits:                   0x%08x\n", reg & ~(_FK_CSR_EMPTY_INT_STATUS |
								       _FK_CSR_HALF_EMPTY_INT_STATUS |
								       _FK_CSR_EMPTY_INT_ENABLE |
								       _FK_CSR_HALF_EMPTY_INT_ENABLE |
								       _FK_CSR_SLINK_DOWN |
								       _FK_CSR_SLINK_UP |
								       _FK_CSR_ABORT |
								       _FK_CSR_SOFT_RESET |
								       _FK_CSR_LINK_CTRL |
								       _FK_CSR_AUTOSEND_FULL |
								       _FK_CSR_LINK0_ENABLE |
								       _FK_CSR_LINK1_ENABLE |
								       _FK_CSR_LINK_DEBUG |
								       _FK_CSR_LINK_IS_UP
								       ));


#ifdef UNLOCKED
      mutex_unlock(&receiver->lock);
#else
      up(&receiver->lock);
#endif
    }

  // printk(KERN_CRIT __FILE__ " :%d\n"  , __LINE__);
#ifdef UNLOCKED
   mutex_unlock(&kfedkit_receivers_lock);
#else
  up (&kfedkit_receivers_lock);
#endif

  // printk(KERN_CRIT __FILE__ " :%d\n"  , __LINE__);
  return 0;
  
}


/* ---------------------------------------------------------------------- */
static int kfedkit_seq_show_sender(struct seq_file *s, int sender_index)
{
  // struct kfedkit_qset *d;
  // int i;
  struct kfedkit_sender_t *sender;

  //  printk(KERN_INFO "MY: entering show_receiver, s=%p\n",s);

#ifdef UNLOCKED
  if (mutex_lock_interruptible(&kfedkit_senders_lock))
    return -ERESTARTSYS;
#else
  if (down_interruptible(&kfedkit_senders_lock))
    return -ERESTARTSYS;
#endif

  /* find the receiver struct */
  for (sender = kfedkit_senders; sender != NULL; sender = sender->next) 
    {
      if (sender->index == sender_index) 
	break;
    }

  if (sender == NULL)
    seq_printf(s, "Sender %d is not open\n", sender_index);
  else
    {
      uint32_t reg;

#ifdef UNLOCKED
      if (mutex_lock_interruptible(&sender->lock))
      {
	// failed to acquire the lock
	mutex_unlock (&kfedkit_senders_lock);
	return -ERESTARTSYS;
      }
#else
      if (down_interruptible(&sender->lock))
      {
	// failed to acquire the lock
	up (&kfedkit_senders_lock);
	return -ERESTARTSYS;
      }
#endif

      seq_printf(s,"Sender %d is open\n", sender_index);
      seq_printf(s,"\n");

      seq_printf(s,"interrupt_registered: %s\n",sender->interrupt_registered ? "yes" : "no");
      seq_printf(s,"number of received interrupts: %u\n",sender->num_interrupts);
      seq_printf(s,"irq number: %d\n",sender->device->irq);
      seq_printf(s,"\n");


      seq_printf(s,"master fifo information:\n");
      seq_printf(s,"  read:                         %d\n", sender->master_fifo->read);
      seq_printf(s,"  write:                        %d\n", sender->master_fifo->write);
      seq_printf(s,"  size:                         %d\n", sender->master_fifo->size);
      seq_printf(s,"  interrupt disabled:           %d\n", sender->master_fifo->int_disabled);
      seq_printf(s,"  first DMA done:               %d\n", sender->master_fifo->first_dma_done);
      seq_printf(s,"\n");
      seq_printf(s,"  sender->master_fifo         (kern addr) 0x%p\n", sender->master_fifo);
      seq_printf(s,"  sender->master_fifo         (bus addr)  0x%16p\n", (void*)virt_to_bus(sender->master_fifo));
      seq_printf(s,"  sender->master_fifo         (phys addr) 0x%16p\n", (void*)virt_to_phys(sender->master_fifo));
      
//
//
//      // this is write-only
//      // seq_printf(s,"content of block size register: %d\n", receiver->map0[_FK_BKSZ_OFFSET / sizeof(uint32_t)]);
//      seq_printf(s,"word count fifo address:        0x%08x\n", receiver->map0[_FK_WCFADDR_OFFSET / sizeof(uint32_t)]);
//      seq_printf(s,"\n");
//
      reg = sender->map0[_FK_SENDER_MCTRL / sizeof(uint32_t)];
      
      seq_printf(s,"MCTRL/GCTRL register content:   0x%08x\n", reg);
      seq_printf(s,"   0: FIFO_EMPTY                %d\n", CUSTOMBIT(reg, _FK_SENDER_FIFO_EMPTY));
      seq_printf(s,"   1: FIFO_HALF_FULL            %d\n", CUSTOMBIT(reg, _FK_SENDER_FIFO_HALF_FULL));
      seq_printf(s,"   2: FIFO_FULL                 %d\n", CUSTOMBIT(reg, _FK_SENDER_FIFO_FULL));
      seq_printf(s,"   3: MASTER_DMA_DONE           %d\n", CUSTOMBIT(reg, _FK_SENDER_MASTER_DMA_DONE));

      seq_printf(s,"   8: ENABLE_FIFO_EMPTY         %d\n", CUSTOMBIT(reg, _FK_SENDER_ENABLE_FIFO_EMPTY));
      seq_printf(s,"   9: ENABLE_FIFO_HALF_FULL     %d\n", CUSTOMBIT(reg, _FK_SENDER_ENABLE_FIFO_HALF_FULL));
      seq_printf(s,"  10: ENABLE_FIFO_FULL          %d\n", CUSTOMBIT(reg, _FK_SENDER_ENABLE_FIFO_FULL));
      seq_printf(s,"  11: ENABLE_FIFO_DMA_DONE      %d\n", CUSTOMBIT(reg, _FK_SENDER_ENABLE_FIFO_DMA_DONE));

      seq_printf(s,"  16: GENERATOR_MODE            %d\n", CUSTOMBIT(reg, _FK_SENDER_GENERATOR_MODE));
      seq_printf(s,"  17: DMA_INT_CLEAR             %d\n", CUSTOMBIT(reg, _FK_SENDER_DMA_INT_CLEAR));
      seq_printf(s,"  18: DMA_IN_PROGRESS           %d\n", CUSTOMBIT(reg, _FK_SENDER_DMA_IN_PROGRESS));
      seq_printf(s,"  19: SOFT_RESET                %d\n", CUSTOMBIT(reg, _FK_SENDER_SOFT_RESET));                 
      seq_printf(s,"\n");
      seq_printf(s,"  BIT 15                        %d\n", CUSTOMBIT(reg, (1<<15)));
      seq_printf(s,"  BIT 20                        %d\n", CUSTOMBIT(reg, (1<<20)));
      seq_printf(s,"  BIT 21                        %d\n", CUSTOMBIT(reg, (1<<21)));
      seq_printf(s,"  BIT 22                        %d\n", CUSTOMBIT(reg, (1<<22)));
      seq_printf(s,"  BIT 23 (BACKPRESSURE ?)       %d\n", CUSTOMBIT(reg, (1<<23)));
      seq_printf(s,"  other bits:                   0x%08x\n", reg & ~(
								       _FK_SENDER_FIFO_EMPTY |		     
								       _FK_SENDER_FIFO_HALF_FULL |
								       _FK_SENDER_FIFO_FULL |
								       _FK_SENDER_MASTER_DMA_DONE |
								       _FK_SENDER_ENABLE_FIFO_EMPTY |
								       _FK_SENDER_ENABLE_FIFO_HALF_FULL |
								       _FK_SENDER_ENABLE_FIFO_FULL |
								       _FK_SENDER_ENABLE_FIFO_DMA_DONE |
								       _FK_SENDER_GENERATOR_MODE |
								       _FK_SENDER_DMA_INT_CLEAR |
								       _FK_SENDER_DMA_IN_PROGRESS |	     
								       _FK_SENDER_SOFT_RESET |
								       (1<<15) |
								       (1<<20) |
								       (1<<21) |
								       (1<<22) |
								       (1<<23)
								       ));

#ifdef UNLOCKED
     mutex_unlock(&sender->lock);
#else
      up(&sender->lock);
#endif
    }

#ifdef UNLOCKED
  mutex_unlock (&kfedkit_senders_lock);
#else
  up (&kfedkit_senders_lock);
#endif
  return 0;
  
}


/* ---------------------------------------------------------------------- */



static int kfedkit_seq_show(struct seq_file *seq, void *iterator_data)
{
  // struct kfedkit_dev *dev = (struct kfedkit_dev *) userdata;
  // unsigned device_index = (unsigned) userdata;
  size_t device_index;
#ifdef UNLOCKED
  device_index = (size_t) seq->private;
#else
  struct proc_dir_entry *pde;
  pde = (struct proc_dir_entry*) seq->private;
  device_index = (size_t) pde->data;
#endif

  if (device_index >= MAX_FEDKIT_SENDER_NUMBER)
    return kfedkit_seq_show_receiver(seq, device_index - MAX_FEDKIT_SENDER_NUMBER);
  else
    return kfedkit_seq_show_sender(seq, device_index);

  return 0;
}

/* ---------------------------------------------------------------------- */

static void kfedkit_seq_stop(struct seq_file *p, void *v)
{
}
static struct seq_operations kfedkit_seq_ops = {
  .start = kfedkit_seq_start,
  .next = kfedkit_seq_next,
  .stop = kfedkit_seq_stop,
  .show = kfedkit_seq_show
};



/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */

static int kfedkit_proc_open(struct inode *inode, struct file *file)
{
  int retval;

  /* declare that we are using the new interface ?! */
  retval = seq_open(file, &kfedkit_seq_ops);

  /* get the pointer to the corresponding proc dir entry */
  if (!retval) 
    {
      struct seq_file *seq = file->private_data;
      if (seq != NULL)
#ifdef UNLOCKED
	seq->private = PDE_DATA(inode);
#else
	seq->private = PDE(inode);
#endif
    }

  return retval;
}

/* ---------------------------------------------------------------------- */
ssize_t kfedkit_procfs_reset_file_write(struct file* file, const char* buffer, size_t count, loff_t* data)
{
  struct kfedkit_receiver_t *receiver;
  size_t device_index = (size_t) file->private_data;

  // printk(KERN_INFO "kfedkit: kfedkit_procfs_reset_file_write called %p\n", file->private_data);

  if (device_index < MAX_FEDKIT_SENDER_NUMBER)
    // it's a sender, currently not supported
    return -ENOENT;

  device_index -= MAX_FEDKIT_SENDER_NUMBER;

  // find the receiver
#ifdef UNLOCKED
  if (mutex_lock_interruptible(&kfedkit_receivers_lock))
    return -ERESTARTSYS;
#else
  if (down_interruptible(&kfedkit_receivers_lock))
    return -ERESTARTSYS;
#endif

  /* find the receiver struct */
  for (receiver = kfedkit_receivers; receiver != NULL; receiver=receiver->next) 
    {
      if (receiver->index == device_index) break;
    }

#ifdef UNLOCKED
  mutex_unlock(&kfedkit_receivers_lock);
#else
  up(&kfedkit_receivers_lock);
#endif
  if (receiver == NULL)
    return -ENOENT;

  if (receiver->map0 == NULL)
    return -ENOENT;

#ifdef UNLOCKED
  mutex_lock(&receiver->lock);
#else
  down(&receiver->lock);
#endif

  printk(KERN_INFO "fedkit receiver %zu: resetting from procfs\n",device_index);
  receiver->map0[_FK_CSR_OFFSET/4] |= _FK_CSR_SOFT_RESET;

#ifdef UNLOCKED
  mutex_unlock(&receiver->lock);
#else
  up(&receiver->lock);
#endif

//  struct proc_dir_entry *pde;
//  unsigned device_index;
//
//  //printk(KERN_INFO "MY: in seq_show: seq=%p\n",seq);
//
//  pde = (struct proc_dir_entry*) seq->private;
//
//  // printk(KERN_INFO "MY: in seq_show: pde=%p\n",pde);
//
//  device_index = (unsigned) pde->data;
//
//  if (device_index >= MAX_FEDKIT_SENDER_NUMBER)
//    return kfedkit_seq_show_receiver(seq, device_index - MAX_FEDKIT_SENDER_NUMBER);
//  else
//    return kfedkit_seq_show_sender(seq, device_index);
//
  return (int)count;


}
/* ---------------------------------------------------------------------- */
static int kfedkit_procfs_reset_file_open(struct inode *inode, struct file *file)
{
#ifdef UNLOCKED
  void *data = PDE_DATA(inode);
  // printk(KERN_INFO "in kfedkit_procfs_reset_file_open, pde->data=%p\n",pde->data);
  file->private_data = data;
  return 0;
#else
  struct proc_dir_entry *pde = PDE(inode);

  size_t device_number = (size_t) pde->data;
  // printk(KERN_INFO "in kfedkit_procfs_reset_file_open, pde->data=%p\n",pde->data);
  file->private_data = (void *) device_number;
  return 0;
#endif
}
