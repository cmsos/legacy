#include <sstream> 
#include <cmath>
#include "ferol/FrlEventGenerator.hh"
#include "d2s/utils/Exception.hh"
#include "ferol/loggerMacros.h"

ferol::FrlEventGenerator::FrlEventGenerator( HAL::HardwareDeviceInterface *bridge,
                                             HAL::HardwareDeviceInterface *frl,
                                             Logger logger )
    : utils::EventGenerator( logger ),
      bridge_P( bridge ),
      frl_P( frl )
{
}

void
ferol::FrlEventGenerator::checkParams( uint32_t &nevt, uint32_t streamNo )
{
    if ( (streamNo != 0) && (streamNo != 1) )
        {
            std::stringstream msg;
            msg << "Illegal stream number " << streamNo
                << " encountered in the FrlEventGenerator. Cannot generate event descriptors. " ;
            ERROR( msg.str() );
            XCEPT_RAISE( utils::exception::FerolException, msg.str() );
        }

    uint32_t mask      = int(log2( nevt ));
    uint32_t nevt_corr = int(pow(2,mask));

    if ( nevt != nevt_corr )
        {
            std::stringstream msg;
            msg << "You requested " << nevt << " events to be generated which is not a power of 2. I generate only " << nevt_corr << " events since the Frl can only generate descriptors in powers of 2.";
            WARN( msg.str() );
            nevt = nevt_corr;
        }

    uint32_t maxevt = 0x10000;
    if ( nevt > maxevt ) 
        {
            std::stringstream msg;
            msg << "You requested " << nevt << " events to be generated but the Frl has only space for " << maxevt << " descriptors. Therefore the generation of descriptors will be limited to this maximum. "; 
            WARN( msg.str() );
            nevt = maxevt;
        }


    return;
}

void
ferol::FrlEventGenerator::writeDescriptors( uint32_t streamNo )
{
    std::stringstream item;
    item << "stream" << streamNo << "_descriptor0";

    std::vector<descriptor>::const_iterator it;
    uint32_t ic = 0;
    bool useDelay = false;
    for ( it = descriptors_.begin(); it != descriptors_.end(); it++ )
        {
            uint32_t size  =  (*it).size;             // in 8 bit words
            uint32_t delay =  (*it).delay / 10;       // in units of 10ns
            if ( delay > 0 ) useDelay = true;
            if (delay >= 0x100000 ) 
                {
                    delay = 0xFFFFF;
                    WARN( "The requested delay is too large. Max delay is 10485750ns (10.48ms). Delay has been set to this value.");
                }
            uint32_t desc1 = 0x00ffffff & size;
            uint32_t desc2 = ( (0x00ff & (*it).seed) << 16 ) + ( 0x0fff & (*it).fedId );
            uint32_t desc3 = (( 0xfffff & delay ) << 12) + (0x0fff & (*it).bx);
            uint32_t desc4 = 0;

            bridge_P->write( item.str(), desc1, HAL::HAL_DO_VERIFY, ( 0x0 + ic*0x10 ) );
            bridge_P->write( item.str(), desc2, HAL::HAL_DO_VERIFY, ( 0x4 + ic*0x10 ) );
            bridge_P->write( item.str(), desc3, HAL::HAL_DO_VERIFY, ( 0x8 + ic*0x10 ) );
            bridge_P->write( item.str(), desc4, HAL::HAL_DO_VERIFY, ( 0xc + ic*0x10 ) );

            ic++;

        }

    // if at least one delay > 10ns was specified in the descriptors we consider delays:
    if ( useDelay ) 
        frl_P->setBit( "gen_considerDelays");                 
    else
        frl_P->resetBit( "gen_considerDelays"); 
        
    frl_P->write("gen_Mask_SRAM", (descriptors_.size()-1) );
    INFO( "Set descriptor mask to " << ((descriptors_.size()-1) ) );
    frl_P->write( "gen_LD_evt", 0x01 );   // we start with event number '1'. This might be done 
    // twice if we use both streams, but it does not do harm.
}
