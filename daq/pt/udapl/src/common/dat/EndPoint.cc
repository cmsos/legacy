// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <stdio.h>
#include <string.h>
#include <sstream>
#include "pt/udapl/dat/EndPoint.h"
#include "pt/udapl/dat/Utils.h"
#include "pt/udapl/dat/MemoryRegion.h"
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netinet/tcp.h>

pt::udapl::dat::EndPoint::EndPoint (pt::udapl::dat::InterfaceAdapter * ia) 
	: mutex_(toolbox::BSem::FULL, true), ia_(ia)
{
       urgentQueue_= toolbox::rlist<Descriptor>::create("udapl-urgent-queue");


	//DAT_EP_ATTR attr = default_ep_attr; // need to create default from a empty endpoint

	//   attr.service_type = DAT_SERVICE_TYPE_RC;
	//   attr.max_message_size = 0x100000000;
	//   attr.max_rdma_size = 0x100000000;
	//   attr.qos = DAT_QOS_BEST_EFFORT;
	//   attr.recv_completion_flags = DAT_COMPLETION_DEFAULT_FLAG;
	//   attr.request_completion_flags = DAT_COMPLETION_DEFAULT_FLAG;
	//attr.max_recv_dtos = DEFAULT_EP_QUEUE_LENGTH;
	//attr.max_request_dtos = DEFAULT_EP_QUEUE_LENGTH;
	//   attr.max_recv_iov = 4;
	//   attr.max_request_iov = 4;
	//   attr.max_rdma_read_in = 100;
	//   attr.max_rdma_read_out = 100;
	//   attr.srq_soft_hw = 100;
	//   attr.max_rdma_read_iov = 100;
	//   attr.max_rdma_write_iov = 100;
	//   attr.ep_transport_specific_count = 0;
	//   attr.ep_transport_specific = 0;
	//   attr.ep_provider_specific_count = 0;
	//   attr.ep_provider_specific = 0;
	/*
	 DAT_SEG_LENGTH              max_message_size = 0x40000;
	 DAT_COUNT                   max_recv_dtos = 64;
	 DAT_COUNT                   max_request_dtos = 64;
	 DAT_COUNT                   max_recv_iov = 64;
	 DAT_COUNT                   max_request_iov = 64;
	 */

	DAT_RETURN ret = dat_ep_create(ia->ia_handle_,       // IA
		ia->pz_handle_,       // PZ
		ia->recv_evd_hdl_,    // recv
		ia->reqt_evd_hdl_,    // request
		ia->conn_evd_hdl_,    // connect
		0,      // attributes
		&ep_handle_);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create EP for IA " << ia->name_ << " with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

	DAT_EP_PARAM ep_params;
	ret = dat_ep_query(ep_handle_, DAT_EP_FIELD_EP_ATTR_ALL, &ep_params);
	if (ret != DAT_SUCCESS)
	{
		std::cout << "Failed to retrieve EP attributes" << std::endl;
	}

	ret = dat_ep_free(ep_handle_);

	//ep_params.ep_attr.max_message_size = 0x40000;
	//ep_params.ep_attr.max_recv_dtos = MAX_UDAPL_RECEIVE_BUFFERS * 4;
	//ep_params.ep_attr.max_recv_iov = MAX_UDAPL_RECEIVE_BUFFERS;
	//ep_params.ep_attr.max_request_dtos = MAX_UDAPL_RECEIVE_BUFFERS *4;
	//ep_params.ep_attr.max_request_iov = MAX_UDAPL_RECEIVE_BUFFERS;
	ep_params.ep_attr.max_recv_dtos = 8192;
	//ep_params.ep_attr.max_recv_iov = 512;
	ep_params.ep_attr.max_request_dtos = 8192;
	//ep_params.ep_attr.max_request_iov = 4;

	credits_ = MAX_UDAPL_RECEIVE_BUFFERS;

	ret = dat_ep_create(ia->ia_handle_,       // IA 
		ia->pz_handle_,       // PZ
		ia->recv_evd_hdl_,    // recv
		ia->reqt_evd_hdl_,    // request
		ia->conn_evd_hdl_,    // connect
		//0,      // attributes
		&ep_params.ep_attr,      // attributes
		&ep_handle_);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to create EP for IA " << ia->name_ << " with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

	std::cout << "max_message_size " << ep_params.ep_attr.max_message_size << std::endl;
	std::cout << "max_recv_dtos " << ep_params.ep_attr.max_recv_dtos << std::endl;
	std::cout << "max_request_dtos " << ep_params.ep_attr.max_request_dtos << std::endl;
	std::cout << "max_recv_iov " << ep_params.ep_attr.max_recv_iov << std::endl;
	std::cout << "max_request_iov " << ep_params.ep_attr.max_request_iov << std::endl;

}

pt::udapl::dat::EndPoint::~EndPoint ()
{
	toolbox::rlist<Descriptor>::destroy(urgentQueue_);

	if (ep_handle_ != DAT_HANDLE_NULL)
	{
		DAT_RETURN ret = dat_ep_free(ep_handle_);

		if (ret != DAT_SUCCESS)
		{
			std::stringstream msg;
			msg << "Failed to free EP for IA " << ia_->name_ << " with code:" << pt::udapl::dat::errorToString(ret);

			std::cerr << msg.str() << std::endl;
		}
	}
}

void pt::udapl::dat::EndPoint::incrementCredits () 
{
#ifdef PT_UDAPL_FLOW_CONTROL
	mutex_.take();
	credits_++;

	if ( ! urgentQueue_->empty() )
	{
		//std::cout << " Urgent re-send" << credits_ << std::endl;
		pt::udapl::dat::Descriptor d = urgentQueue_->front();
		urgentQueue_->pop_front();

		d.local_iov->virtual_address = (DAT_VADDR) (uintptr_t) d.mr->buffer_;
		d.local_iov->segment_length = d.size;
		d.local_iov->lmr_context = d.mr->lmr_context_;

		DAT_COMPLETION_FLAGS flags = DAT_COMPLETION_DEFAULT_FLAG;

		DAT_RETURN ret;
		ret = dat_ep_post_send(this->ep_handle_, 1, d.local_iov, d.cookie, flags);
		if (ret != DAT_SUCCESS)
		{

			mutex_.give();
			std::stringstream msg;
			msg << "Failed to to post send buffer with code:" << pt::udapl::dat::errorToString(ret);

			unsigned long major = (ret & 0x3fff0000);
			unsigned long minor = (ret & 0x0000ffff);
			std::cerr << "--->>>>>>> ret " << ret << " major " << major << " minor" << minor << std::endl;
			if ( major == DAT_INSUFFICIENT_RESOURCES && minor == DAT_RESOURCE_MEMORY)
			{
				XCEPT_RAISE(pt::udapl::exception::InsufficientResources, msg.str() );
			}

			XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
		}

		// posted successfully, consumed one credit
		credits_--;

	}
	mutex_.give();
#endif
}

//typedef union dat_context
//{
//    DAT_PVOID                   as_ptr;
//    DAT_UINT64                  as_64;
//    DAT_UVERYLONG               as_index;
//} DAT_CONTEXT;

void pt::udapl::dat::EndPoint::postSendBuffer (pt::udapl::dat::MemoryRegion * mr, DAT_DTO_COOKIE & cookie, DAT_LMR_TRIPLET * local_iov, DAT_UINT64 size) 
{
#ifdef PT_UDAPL_FLOW_CONTROL 
	mutex_.take();

	if ( credits_ == 0 || ! urgentQueue_->empty() )
	{
		pt::udapl::dat::Descriptor d;

		d.mr = mr;
		d.cookie = cookie;
		d.local_iov = local_iov;
		d.size = size;
		//std::cout << "urgent post " << credits_ << std::endl;	
		urgentQueue_->push_back(d);
		mutex_.give();
		return;
	}
#endif

	local_iov->virtual_address = (DAT_VADDR) (uintptr_t) mr->buffer_;
	local_iov->segment_length = size;
	local_iov->lmr_context = mr->lmr_context_;
	DAT_COMPLETION_FLAGS flags = DAT_COMPLETION_DEFAULT_FLAG;

	DAT_RETURN ret;
	ret = dat_ep_post_send(this->ep_handle_, 1, local_iov, cookie, flags);
	if (ret != DAT_SUCCESS)
	{

		mutex_.give();
		std::stringstream msg;
		msg << "Failed to to post send buffer with code:" << pt::udapl::dat::errorToString(ret);

		unsigned long major = (ret & 0x3fff0000);
		unsigned long minor = (ret & 0x0000ffff);
		std::cerr << "--->>>>>>> ret " << ret << " major " << major << " minor" << minor << std::endl;
		if (major == DAT_INSUFFICIENT_RESOURCES && minor == DAT_RESOURCE_MEMORY)
		{
			XCEPT_RAISE(pt::udapl::exception::InsufficientResources, msg.str());
		}

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}

#ifdef PT_UDAPL_FLOW_CONTROL
	// posted successfully, consumed one credit
	credits_--;
	mutex_.give();
#endif
}

void pt::udapl::dat::EndPoint::postRecvBuffer (pt::udapl::dat::MemoryRegion * mr, DAT_DTO_COOKIE & cookie, DAT_LMR_TRIPLET * local_iov, DAT_UINT64 size) 
{

	//DAT_LMR_TRIPLET local_iov;
	local_iov->virtual_address = (DAT_VADDR) (uintptr_t) mr->buffer_;
	local_iov->segment_length = size;
	local_iov->lmr_context = mr->lmr_context_;
	DAT_COMPLETION_FLAGS flags = DAT_COMPLETION_DEFAULT_FLAG;

	DAT_RETURN ret;
	ret = dat_ep_post_recv(this->ep_handle_, 1, local_iov, cookie, flags);
	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to to post receive buffer with code:" << pt::udapl::dat::errorToString(ret);

		unsigned long major = (ret & 0xffff0000);
		unsigned long minor = (ret & 0x0000ffff);

		std::cout << "--->>>>>>> ret " << ret << " major " << major << " minor" << minor << std::endl;
		if (major == DAT_INSUFFICIENT_RESOURCES && minor == DAT_RESOURCE_MEMORY)
		{
			XCEPT_RAISE(pt::udapl::exception::InsufficientResources, msg.str());
		}

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());
	}
}

void pt::udapl::dat::EndPoint::connect (const std::string & destination, size_t port) 
{
	DAT_SOCK_ADDR server_netaddr;
	pt::udapl::dat::netAddrLookupHostAddress(&server_netaddr, (char*) destination.c_str());

	DAT_RETURN ret;

	char ipaddr[100];
	DAT_SOCK_ADDR6* ip6_addr = (DAT_SOCK_ADDR6 *) ia_->ia_attr_.ia_address_ptr;
	if (ip6_addr->sin6_family != AF_INET6)
	{
		struct sockaddr_in *ip_addr = (struct sockaddr_in *) ia_->ia_attr_.ia_address_ptr;

		int rval = ip_addr->sin_addr.s_addr;

		sprintf(ipaddr, "%d.%d.%d.%d", (rval >> 0) & 0xff, (rval >> 8) & 0xff, (rval >> 16) & 0xff, (rval >> 24) & 0xff);

	}
	else
	{
		strcpy(ipaddr, "localhost");
	}

	ret = dat_ep_connect(this->ep_handle_, &server_netaddr, port, DAT_TIMEOUT_INFINITE, strlen(ipaddr) + 1, (DAT_PVOID) ipaddr,
	// 0, (DAT_PVOID) 0,  /* no private data */
		DAT_QOS_BEST_EFFORT, DAT_CONNECT_DEFAULT_FLAG);

	if (ret != DAT_SUCCESS)
	{
		std::stringstream msg;
		msg << "Failed to connect to " << destination << ", with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}
}

void pt::udapl::dat::EndPoint::disconnect () 
{

	DAT_RETURN ret;
	ret = dat_ep_disconnect(this->ep_handle_, DAT_CLOSE_ABRUPT_FLAG);
	if (ret != DAT_SUCCESS)
	{
		const char *major_msg, *minor_msg;
		dat_strerror(ret, &major_msg, &minor_msg);
		std::stringstream msg;
		msg << "Failed to disconnect " << ", with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

	// synchronize  it shoud be done outside this method
	//DT_disco_event_wait(conn_evd_hdl, NULL);
}

void pt::udapl::dat::EndPoint::getStatus (DAT_EP_STATE * ep_state, DAT_BOOLEAN * recv_idle, DAT_BOOLEAN * request_idle) 
{
	DAT_RETURN ret;
	ret = dat_ep_get_status(this->ep_handle_, ep_state, recv_idle, request_idle);
	if (ret != DAT_SUCCESS)
	{
		const char *major_msg, *minor_msg;
		dat_strerror(ret, &major_msg, &minor_msg);
		std::stringstream msg;
		msg << "Failed to get EP status " << ", with code:" << pt::udapl::dat::errorToString(ret);

		XCEPT_RAISE(pt::udapl::exception::Exception, msg.str());

	}

}

/*
 std::ostream &operator<<(std::ostream &cout, pt::udapl::dat::EndPoint emp)
 {
 DAT_EP_PARAM ep_params;
 DAT_RETURN ret = dat_ep_query (emp.ep_handle_, DAT_EP_FIELD_EP_ATTR_ALL, &ep_params);
 if (ret!=DAT_SUCCESS) {
 cout << "Failed to retrieve EP attributes" << std::endl;
 }

 cout << "EP (default S/R size is " << ep_params.ep_attr.max_request_dtos << "/" << ep_params.ep_attr.max_recv_dtos << std::endl;


 return cout;
 }
 */
