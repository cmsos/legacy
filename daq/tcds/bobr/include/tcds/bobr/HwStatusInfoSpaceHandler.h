#ifndef _tcds_bobr_HwStatusInfoSpaceHandler_h_
#define _tcds_bobr_HwStatusInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace bobr {

    class HwStatusInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwStatusInfoSpaceHandler(xdaq::Application& xdaqApp,
                               tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwStatusInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      /* virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const; */

    };

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_HwStatusInfoSpaceHandler_h_
