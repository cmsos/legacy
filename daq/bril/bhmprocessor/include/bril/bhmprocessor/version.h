// $Id$
#ifndef _bril_bhmprocessor_version_h_
#define _bril_bhmprocessor_version_h_
#include "config/PackageInfo.h"
#define BRILBHMPROCESSOR_VERSION_MAJOR 1
#define BRILBHMPROCESSOR_VERSION_MINOR 5
#define BRILBHMPROCESSOR_VERSION_PATCH 0
#define BRILBHMPROCESSOR_PREVIOUS_VERSIONS

// Template macros
//
#define BRILBHMPROCESSOR_VERSION_CODE PACKAGE_VERSION_CODE(BRILBHMPROCESSOR_VERSION_MAJOR,BRILBHMPROCESSOR_VERSION_MINOR,BRILBHMPROCESSOR_VERSION_PATCH)
#ifndef BRILBHMPROCESSOR_PREVIOUS_VERSIONS
#define BRILBHMPROCESSOR_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILBHMPROCESSOR_VERSION_MAJOR,BRILBHMPROCESSOR_VERSION_MINOR,BRILBHMPROCESSOR_VERSION_PATCH)
#else
#define BRILBHMPROCESSOR_FULL_VERSION_LIST  BRILBHMPROCESSOR_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILBHMPROCESSOR_VERSION_MAJOR,BRILBHMPROCESSOR_VERSION_MINOR,BRILBHMPROCESSOR_VERSION_PATCH)
#endif
namespace brilbhmprocessor{
  const std::string package = "brilbhmprocessor";
  const std::string versions = BRILBHMPROCESSOR_FULL_VERSION_LIST;
  const std::string summary = "BRIL DAQ bhmprocessor";
  const std::string description = "collect and process histograms from bhmsource";
  const std::string authors = "Nicolo, Stella";
  const std::string link = "";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}
#endif
