package xdaq.xdesk.xcommander;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.util.*;
import javax.infobus.*;
import javax.swing.*;
import javax.swing.tree.*;
import java.awt.Graphics;
import java.io.*;

import org.apache.log4j.*;
import xdaq.xdesk.tools.*;
import xdaq.xdesk.xtree.*;


public class XCommander extends JApplet implements MenuBarListener, DiscoveryListener
{
	Logger logger = Logger.getLogger (XCommander.class);	
	JFrame frame_ = null;
	XCommanderViewer viewer_ = null;
	// Infobus stuff
	InfoBusPublisher frameworkCommandPublisher_ = new InfoBusPublisher("framework");
	Discovery partitionInfoBus_ = null;
	PartitionDefaultModel defaultModel_ = null;	
	
	class WindowListener extends WindowAdapter
	{	
		public WindowListener ()
		{
	
		}
	
		// window adapter overriding method
		public void windowClosing (WindowEvent e) 
		{
			frame_.setVisible(false);			
		}
	}
	
	public void init() 
	{
		defaultModel_ = new PartitionDefaultModel();
		frameworkCommandPublisher_.publishItem ("command");
		
	}
	
	public PartitionDefaultModel getPartitionModel()
	{
		return defaultModel_;
	}
	
	public void start() 
	{
		if ( frame_ == null ) 
		{
			System.out.println("----> Createing frame");
			frame_ = new JFrame();
			frame_.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);
			frame_.addWindowListener (new WindowListener());
			// the Discovery trigger action on the viewer therfore it must be created afterward.
			viewer_ = new XCommanderViewer(frame_, this);

			partitionInfoBus_ = new Discovery ("xcontrolbus", "xtree", this);
			frame_.getContentPane().add( viewer_ );
			frame_.setSize(this.getWidth(),this.getHeight()); // should use properties from applet spec
			frame_.setJMenuBar(new XCommanderMenuBar(this));
			frame_.show();
			defaultModel_.firePartitionModelDataChanged();
			viewer_.setDividerLocation();
		}
		else
		{
			frame_.setVisible(true);
		}	
		
		
  	}

  /**
     Removes self as InfoBus data consumer. Removes self as data item changed 
     listener so it will no longer receive item value changed updates.
   */
  	public void stop() 
	{
   
  	}

  /**
     Removes the label, stops the applet, and leaves the InfoBus.
   */
  	public void destroy() 
	{
  		frame_.setVisible(false);
		frame_.dispose();
		
		// Leave the Info Bus	
		frameworkCommandPublisher_.shutdown();
		partitionInfoBus_.leave();
  	}
	
	
	
	
	
	// Menu bar Listener methods	
	public void aboutAction()
	{
		System.out.println("About for this Daqlet");
	}
	
	public void switchAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("tasks",this) );
	}
	
	public void exitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("exit",this) );
	}
	public void quitAction()
	{
		frameworkCommandPublisher_.fireItemValueChanged("command", new Command("quit",this) );
	}
	
	public void closeAction()
	{
		// close the window frame
		frame_.setVisible(false);
		//frame_.validate();	
	}
	
	
	// Store the partition tree in the model
	//
	public void objectAdvertised(DataItem item)
	{
		ImmediateAccess ia = (ImmediateAccess) item;
		String name = ia.getValueAsString();
		TreeModel tree = (TreeModel) ia.getValueAsObject();
		defaultModel_.setValue(name, tree);
		defaultModel_.firePartitionModelDataChanged();
		viewer_.setVisible(true);
		
	}
	
	public void objectChanged(DataItem item)
	{
		ImmediateAccess ia = (ImmediateAccess) item;
		String name = ia.getValueAsString();
		TreeModel tree = (TreeModel) ia.getValueAsObject();
		defaultModel_.setValue(name, tree);
		defaultModel_.firePartitionModelDataChanged();
		viewer_.setVisible(true);		
	}
	
	public void objectRevoked(DataItem item)
	{
		ImmediateAccess ia = (ImmediateAccess) item;
		String name = ia.getValueAsString();
		defaultModel_.removeValue(name);
		defaultModel_.firePartitionModelDataChanged();
		viewer_.setVisible(true);		
	}
	//
	// ---- End of Infobus support
	
}
