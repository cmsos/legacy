package xdaq.xdesk.jtreeeditor;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JViewport;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellEditor;
import javax.swing.tree.TreePath;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JTreePopupAction
    extends AbstractAction {
//-------------------------------Variables Definition---------------------------

  private static DefaultMutableTreeNode buffer = null;
  private static boolean isCuted = false;
  private JTreeEditor m_tree;
  private DefaultMutableTreeNode currentNode;
  private DefaultMutableTreeNode parentNode;
  private JTreeUserObject currentNodeUserObject;
  private TreePath currentPath;
  private Document document_;
  private Node currentDomNode;
  private Node parentDomNode;
  private JTreeEditorModel model;

//----------------------------Constructor---------------------------------------
  /**Constructor*/
  public JTreePopupAction(String name, ImageIcon icon, JTreeEditor tree) {
    super(name, icon);
    m_tree = tree;
    try {
      model = (JTreeEditorModel) m_tree.getModel();
    }
    catch (Exception e) {
    }
    currentPath = m_tree.getSelectionPath();
    if (currentPath == null) {
      return;
    }
    currentNode = (DefaultMutableTreeNode) currentPath.getLastPathComponent();
    currentNodeUserObject = (JTreeUserObject) currentNode.getUserObject();
    parentNode = (DefaultMutableTreeNode) currentNode.getParent();

    document_ = currentNodeUserObject.DomDocument;
    if (currentNodeUserObject.getType() == Node.ATTRIBUTE_NODE) {
      document_ = ( (JTreeUserObject) parentNode.getUserObject()).
          DomDocument;
    }
    currentDomNode = currentNodeUserObject.domNode;
    if (parentNode != null) {
      parentDomNode = ( (JTreeUserObject) parentNode.getUserObject()).
          domNode;
    }
  }

//******************************************************************************
   /**Returns buffer value*/
   public DefaultMutableTreeNode getBuffer() {
     return buffer;
   }

//******************************************************************************

   /**Returns true if the node was cuted (usualy true before first paste action)*/
   public boolean isCuted() {
     return isCuted;
   }

//******************************************************************************
   public void actionPerformed(ActionEvent e) {
     String actionName = e.getActionCommand();
//collapse selected node
     if (actionName == "Collapse") {
       m_tree.collapsePath(currentPath);
     }
//expand selected node
     if (actionName == "Expand") {
       m_tree.expandPath(currentPath);
     }
//delete selected node
     if (actionName == "Delete") {
       deleteNode();
     }
//move selected node upstairs by the tree structure
     if (actionName == "Move Up") {
       moveUp();
     }
//move selected node downstairs by the tree structure
     if (actionName == "Move Down") {
       moveDown();
     }
//insert an element after selected node
     if (actionName == "Insert Element After") {
       insertElementAfter();
     }
//insert an element before selected node
     if (actionName == "Insert Element Before") {
       insertElementBefore();
     }
//Add an attribute to selected element.
     if (actionName == "Attribute") {
       addAttribute();
     }
//Add a textual node to the selected element.
     if ( (actionName == "Text") || (actionName == "Comment") ||
         (actionName == "CData") || (actionName == "Processing Instruction")) {
       addTextualNode(actionName);
     }
//Add element to the selected node
     if (actionName == "Element") {
       addElement();
     }
//Clean up the tree. Delete incorrect nodes added by user.
     if (actionName == "Normalize tree") {
       normalizeTree();
     }
//Make tree editable or uneditable
     if (actionName == "Start editing" || actionName == "Stop editing") {
       setTreeEditable(!m_tree.isEditable());
     }
//Load an XML File to configure look an fill
     if (actionName.compareTo("Load GUI configuration") == 0) {
       loadGUI();
     }
//Save GUI configuration into
     if (actionName.compareTo("Save GUI configuration") == 0) {
       saveGUI();
     }
//Load default GUI
     if (actionName.compareTo("Load default GUI configuration") == 0) {
       loadDefaltConfiguration();
     }
//Create a new Tree
     if (actionName.compareTo("New file") == 0) {
       startNewTree();
     }
//Load a file for editing
     if (actionName.compareTo("Load file") == 0) {
       loadTree();
     }
//Save XML document
     if (actionName.compareTo("Save as..") == 0) {
       saveTreeAs();
     }
//Cut a node
     if (actionName.compareTo("Cut") == 0) {
       cutNode();
     }
//Copy a node
     if (actionName.compareTo("Copy") == 0) {
       copyNode();
     }
//Paste a node
     if (actionName.compareTo("Paste") == 0) {
       pasteNode();
     }
   }

//******************************************************************************

//All these routins modify both the tree presentation and DOM object.

   /**Delete selected tree node*/
   private void deleteNode() {
     if (parentNode == null) {
       return;
     }
     if (currentNodeUserObject.getType() != Node.ATTRIBUTE_NODE) {
       parentDomNode.removeChild(currentDomNode); //remove from DOM
       model.removeNodeFromParent(currentNode); //Remove from tree
     }
     else {
       document_.normalize();
       //remove attribute from a DOM node
       ( (Element) parentDomNode).removeAttribute(currentDomNode.getNodeName());
       //remove attribute from JTree
       model.removeNodeFromParent(currentNode);
       updateAttributesList(parentNode);
     }
     m_tree.revalidate();
   }

//******************************************************************************
   /**Moves a selected node up*/
   private void moveUp() {
     DefaultMutableTreeNode newNode = currentNode;
     int index = parentNode.getIndex(currentNode);
     if (currentNodeUserObject.getType() != Node.ATTRIBUTE_NODE) {
       DefaultMutableTreeNode previousNode = currentNode.getPreviousSibling();
       Node previousDomNode = ( (JTreeUserObject) previousNode.
                               getUserObject()).domNode;
       parentDomNode.removeChild(currentDomNode);
       parentDomNode.insertBefore(currentDomNode, previousDomNode);
     }
     model.removeNodeFromParent(currentNode);
     model.insertNodeInto(newNode, parentNode, index - 1);

     m_tree.revalidate();
     m_tree.setSelectionPath(currentPath);
   }

//******************************************************************************
   /**Moves selected node down*/
   private void moveDown() {
     DefaultMutableTreeNode newNode = currentNode;
     int index = parentNode.getIndex(currentNode);
     if (currentNodeUserObject.getType() != Node.ATTRIBUTE_NODE) {
       int currentIndex = parentNode.getIndex(currentNode);
       int childCount = parentNode.getChildCount();
       if ( (currentIndex + 2) <= childCount - 1) {
         DefaultMutableTreeNode _node = (DefaultMutableTreeNode) parentNode.
             getChildAt(currentIndex + 2);
         Node newDomNode = ( (JTreeUserObject) _node.getUserObject()).
             domNode;
         parentDomNode.removeChild(currentDomNode);
         parentDomNode.insertBefore(currentDomNode, newDomNode);
       }
       else {
         parentDomNode.removeChild(currentDomNode);
         parentDomNode.appendChild(currentDomNode);
       }
     }
     model.removeNodeFromParent(currentNode);
     model.insertNodeInto(newNode, parentNode, index + 1);
     m_tree.revalidate();
     m_tree.setSelectionPath(currentPath);
   }

//******************************************************************************
   /**Insert an element before selected node*/
   private void insertElementBefore() {
     Node newDomNode = document_.createElement("New_Element");
     JTreeUserObject newUserObject = new JTreeUserObject(newDomNode);
     DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(newUserObject);
     model.insertNodeInto(newNode, parentNode, parentNode.getIndex(currentNode));
     parentDomNode.insertBefore(newDomNode, currentDomNode);
     TreePath newPath = new TreePath(newNode.getPath());
     m_tree.setSelectionPath(newPath);
     m_tree.revalidate();
   }

//******************************************************************************
   /**Insert an element after selected node*/
   private void insertElementAfter() {
     Node newDomNode = document_.createElement("New_Element");
     JTreeUserObject newUserObject = new JTreeUserObject(newDomNode);
     DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(newUserObject);
     if (currentNode.getNextSibling() == null) {
       parentDomNode.appendChild(newDomNode);
     }
     else {
       DefaultMutableTreeNode nextSiblingNode = (DefaultMutableTreeNode)
           currentNode.getNextSibling();
       JTreeUserObject nextSiblingObject = (JTreeUserObject)
           nextSiblingNode.getUserObject();
       Node nextDomSiblingNode = nextSiblingObject.domNode;
       parentDomNode.insertBefore(newDomNode, nextDomSiblingNode);
     }
     model.insertNodeInto(newNode, parentNode,
                          parentNode.getIndex(currentNode) + 1);
     TreePath newPath = new TreePath(newNode.getPath());
     m_tree.setSelectionPath(newPath);
     m_tree.revalidate();
   }

//******************************************************************************
   /**Adds an attriburte to the XML tag whihc is associated with selected tre node
    * and represent it in the tree
    * */
   private void addAttribute() {
     int insert_index = 0;
     DefaultMutableTreeNode tmpnode;
     JTreeUserObject tmpUserObject;
     for (int i = 0; i < currentNode.getChildCount(); i++) {
       tmpnode = (DefaultMutableTreeNode) currentNode.getChildAt(i);
       tmpUserObject = (JTreeUserObject) tmpnode.getUserObject();
       if (tmpUserObject.getType() == 2) {
         insert_index++;
       }
     }
     Node attrNode = document_.createAttribute("New_Attribute");
     attrNode.setNodeValue("New Value");
     ( (Element) currentDomNode).setAttribute("New_Attribute", "New Value");
     DefaultMutableTreeNode new_node = new DefaultMutableTreeNode(new
         JTreeUserObject(attrNode));
     model.insertNodeInto(new_node, currentNode, insert_index);
     updateAttributesList(currentNode);
     m_tree.revalidate();
     //  m_tree.scrollPathToVisible(new TreePath(new_node.getPath()));
     m_tree.setSelectionPath(new TreePath(new_node.getPath()));
   }

//******************************************************************************
   /**Adds an element to the XML element contained at selected tree node*/
   private void addElement() {
     Node newElement = document_.createElement("New_Element");
     currentDomNode.appendChild(newElement);
     DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(new
         JTreeUserObject(newElement));
     model.insertNodeInto(newNode, currentNode, currentNode.getChildCount());
     m_tree.revalidate();
     m_tree.scrollPathToVisible(new TreePath(newNode.getPath()));
     m_tree.setSelectionPath(new TreePath(newNode.getPath()));
   }

//******************************************************************************
   /**Adds selected node to the buffer*/
   private void cutNode() {
     buffer = (DefaultMutableTreeNode) m_tree.getSelectionPath().
         getLastPathComponent();
     isCuted = true;
   }

//******************************************************************************
   /**Adds selected node to the buffer*/
   private void copyNode() {
     buffer = (DefaultMutableTreeNode) m_tree.getSelectionPath().
         getLastPathComponent();
     isCuted = false;
   }

//******************************************************************************
   /**Attachs the node from the buffer to the selected node*/
   private void pasteNode() {
     DefaultMutableTreeNode newNode = null;
     if (isCuted) {
       Node newDomNode = ( (JTreeUserObject) buffer.getUserObject()).getDomNode();
       try {
         ( (JTreeUserObject) currentNode.getUserObject()).getDomNode().
             appendChild(
             newDomNode);
       }
       catch (Exception e) {
         JOptionPane.showMessageDialog(null,
                                       "An error occured : \n" + e.getMessage());
         return;
       }
       newNode = getNewNode(newDomNode);
       try {
         model.removeNodeFromParent(buffer);
       }
       catch (Exception e) {
       }
       buffer = newNode;
       isCuted = false;
     }
     else {
       Node newDomNode = ( (JTreeUserObject) buffer.getUserObject()).getDomNode();
       Node tmpNode = newDomNode.cloneNode(true);
       try {
         ( (JTreeUserObject) currentNode.getUserObject()).getDomNode().
             appendChild(
             tmpNode);
       }
       catch (Exception e) {
         JOptionPane.showMessageDialog(null,
                                       "An error occured : \n" + e.getMessage());
         return;
       }

       newNode = getNewNode(tmpNode);
       buffer = newNode;
     }

     model.insertNodeInto(newNode, currentNode, currentNode.getChildCount());

     m_tree.revalidate();
     m_tree.repaint();
     m_tree.setSelectionPath(new TreePath(model.getPathToRoot(newNode)));
   }

//******************************************************************************
   /**Creates a new tree node by provaded XML node*/
   private DefaultMutableTreeNode getNewNode(Node xmlNode) {
     DefaultMutableTreeNode retNode = new DefaultMutableTreeNode(new
         JTreeUserObject(xmlNode));
     addChildren(retNode, xmlNode, 0);
     return retNode;
   }

//******************************************************************************
   private static void addChildren
       (DefaultMutableTreeNode parentTreeNode,
        Node parentXMLElement, int count) {
     if (count == 0) { //for the first time : add attributes to root node if it has them
       NamedNodeMap attr = parentXMLElement.getAttributes();
       if (attr != null && attr.getLength() > 0) {
         for (int j = 0; j < attr.getLength(); j++) {
           Node attr1 = attr.item(j);
           parentTreeNode.add(new DefaultMutableTreeNode(new
               JTreeUserObject(attr1)));
           addChildren(parentTreeNode, (Node) attr.item(j), 1);
         }
       }
       count++;
     }
     NodeList childElements =
         parentXMLElement.getChildNodes();
     for (int i = 0; i < childElements.getLength(); i++) {
       Node childElement = childElements.item(i);
       switch (childElement.getNodeType()) {
         case 1:
           DefaultMutableTreeNode childTreeNode =
               new DefaultMutableTreeNode(new JTreeUserObject(childElement));
           NamedNodeMap args = childElement.getAttributes();
           if (args != null && args.getLength() > 0) {
             for (int j = 0; j < args.getLength(); j++) {
               Node attr1 = args.item(j);
               childTreeNode.add(new DefaultMutableTreeNode(new
                   JTreeUserObject(attr1)));
               addChildren(childTreeNode, (Node) args.item(j), 1);
             }
           }
           parentTreeNode.add(childTreeNode);
           addChildren(childTreeNode, childElement, 1);
           break;
         case 4:
         case 8:
         case 7:
         case 3:
           if (childElement.getNodeValue().trim().length() != 0) {
             DefaultMutableTreeNode childTreeNode1 =
                 new DefaultMutableTreeNode(new JTreeUserObject(
                 childElement));
             parentTreeNode.add(childTreeNode1);
             addChildren(childTreeNode1, childElement, 1);
           }
           break;
       }
     }
   }

//******************************************************************************
   /**Adds any Text, Comment,CData or Processing Instruction node*/
   private void addTextualNode(String _actionName) {
     Node newDomNode = null;
     DefaultMutableTreeNode newNode = null;

     if (_actionName.compareTo("Text") == 0) {
       newDomNode = document_.createTextNode("New Text");
     }
     else if (_actionName.compareTo("Comment") == 0) {
       newDomNode = document_.createComment("New Comment");
     }
     else if (_actionName.compareTo("CData") == 0) {
       newDomNode = document_.createCDATASection("New Cdata section");
     }
     else if (_actionName.compareTo("Processing Instruction") == 0) {
       newDomNode = document_.createProcessingInstruction("Target", "Data");
     }
     else {
       return;
     }
     newNode = new DefaultMutableTreeNode(new JTreeUserObject(newDomNode));
     currentDomNode.appendChild(newDomNode);
     model.insertNodeInto(newNode, currentNode, currentNode.getChildCount());
     m_tree.revalidate();
     m_tree.scrollPathToVisible(new TreePath(newNode.getPath()));
     m_tree.setSelectionPath(new TreePath(newNode.getPath()));
   }

//******************************************************************************
   /**Updates current node attributes list after deleting or adding an attribute*/
   private void updateAttributesList(DefaultMutableTreeNode node) {

     DefaultMutableTreeNode parent = node;
     JTreeUserObject userObject = (JTreeUserObject) node.
         getUserObject();
     String atrrString = "";
     DefaultMutableTreeNode tmpnode;
     JTreeUserObject tmpUserObject;
     for (int i = 0; i < parent.getChildCount(); i++) { //update list of attributes
       tmpnode = (DefaultMutableTreeNode) parent.getChildAt(i);
       tmpUserObject = (JTreeUserObject) tmpnode.getUserObject();
       if (tmpUserObject.getType() == 2) {
         atrrString = atrrString + tmpUserObject.getName() + "=" +
             tmpUserObject.getValue() + " ,";
       }
     }
     if (atrrString.length() > 0) {
       atrrString = atrrString.substring(0, atrrString.length() - 2);
       userObject.setAttrList("(" + atrrString.trim() + ")");
     }
     else {
       userObject.setAttrList("");
     }
   }

//******************************************************************************
   /**Switchs between editing and readonly states of the tree*/
   public void setTreeEditable(boolean isEditable) {
     m_tree.setEditable(isEditable);
     m_tree.revalidate();
     m_tree.repaint();
   }

//******************************************************************************
   /**Normalize DOM document and revalidate Tree view*/
   private void normalizeTree() {
     Document document;
     JTreeEditorModel model = (JTreeEditorModel) m_tree.getModel();
     document = model.getDocument();
     document.normalize();
     JViewport viewPort = (JViewport) m_tree.getParent();
     viewPort.remove(0);
     JTreeConfig config = m_tree.getConfiguration();
     m_tree = new JTreeEditor(new JTreeEditorModel(document));
     m_tree.addMouseListener(new JTreePopupTrigger());
     JTreeCellRenderer treeCellRenderer = new JTreeCellRenderer();
     m_tree.setCellRenderer(treeCellRenderer);
     m_tree.setCellEditor( (TreeCellEditor) treeCellRenderer);
     m_tree.setRowHeight(m_tree.getConfiguration().getRowHeight());
     m_tree.setConfiguration(config);
     viewPort.add(m_tree);
     m_tree.revalidate();
     m_tree.repaint();
     //printOut(((JTreeEditorModel)m_tree.getModel()).getDocument());
   }

//******************************************************************************
   /**Loads an XML file and revalidate the
    * tree according to the new GUI rules provided by XML document*/
   private void loadGUI() {
     JFileChooser chooser = new JFileChooser(new File(System.getProperty(
         "user.dir")));
     JTreeFileFilter filter = new JTreeFileFilter();
     chooser.setFileFilter(filter);
     chooser.setAcceptAllFileFilterUsed(false);
     chooser.setMultiSelectionEnabled(false);
     int retValue = chooser.showOpenDialog(m_tree);
     if (retValue == chooser.APPROVE_OPTION) {
       File file = chooser.getSelectedFile();
       if (filter.accept(file)) {
         try {
           JTreeConfig config = new JTreeConfig(file);
           m_tree.setConfiguration(config);
           m_tree.revalidate();
           m_tree.repaint();
         }
         catch (Exception e) {
         }
       }
     }
   }

//******************************************************************************
   /**Saves GUI configuration as an XML file*/
   private void saveGUI() {
     m_tree.getConfiguration().save();
   }

//******************************************************************************
   /**Switchs the GUI to the default*/
   private void loadDefaltConfiguration() {
     m_tree.setConfiguration(new JTreeConfig());
   }

//******************************************************************************
   /**Creates a new Tree*/
   private void startNewTree() {
     Document doc = null;
     try {
       DocumentBuilder builder = DocumentBuilderFactory.newInstance().
           newDocumentBuilder();
       doc = builder.newDocument();
     }
     catch (Exception e) {

     }
     Element main = doc.createElement("Partition");
     doc.appendChild(main);
     JTreeEditorModel model = new JTreeEditorModel(doc);
     JViewport viewPort = (JViewport) m_tree.getParent();
     m_tree.setModel(model);
     viewPort.add(m_tree);
     viewPort.revalidate();
   }

//******************************************************************************
   /**Recieves an XML DOM document and creates a tree*/
   private void loadTree() {
     Document doc = null;
     try {
       JFileChooser chooser = new JFileChooser(System.getProperty("user.dir"));
       JTreeFileFilter filter = new JTreeFileFilter();
       chooser.setFileFilter(filter);
       chooser.setAcceptAllFileFilterUsed(false);
       int retValue = chooser.showOpenDialog(m_tree);
       if (retValue == chooser.APPROVE_OPTION) {
         if (filter.accept(chooser.getSelectedFile())) {
           DocumentBuilder builder = DocumentBuilderFactory.newInstance().
               newDocumentBuilder();
           doc = builder.parse(chooser.getSelectedFile());
           JTreeEditorModel model = new JTreeEditorModel(doc);

           JViewport viewPort = (JViewport) m_tree.getParent();
           boolean editable = m_tree.isEditable();

           m_tree.setModel(model);
           if (editable) {
             m_tree.setEditable(editable);
           }

           viewPort.add(m_tree);
           viewPort.revalidate();
         }
       }
     }
     catch (Exception e) {
       System.out.println("Cannot load file.An exception occured : " +
                          e.getMessage());
     }
   }

//******************************************************************************
   /**Saves the Tree as an XML document*/
   private void saveTreeAs() {
     try {
       Document doc = m_tree.getDocument();
       String startDir = System.getProperty("user.dir");
       JFileChooser fc_ = new JFileChooser(new File(startDir));
       JTreeFileFilter filter = new JTreeFileFilter();
       fc_.setFileFilter(filter);
       fc_.setAcceptAllFileFilterUsed(false); //dont accept All Files option
       fc_.setMultiSelectionEnabled(false); //multyselection not accepted
       File file = null;
       int returnVal = fc_.showSaveDialog(new JFrame());

       if (returnVal == JFileChooser.APPROVE_OPTION) {
         file = fc_.getSelectedFile();
         if (!file.getPath().endsWith(".xml")) { //must be an xml file
           file = new File(file.getPath() + ".xml");
         }

         boolean isSaveAccepted = false;

         if (file.exists()) { //have to confirm an overwriting
           int retVal = JOptionPane.showConfirmDialog(new JFrame(),
               "File " + file.getName() +
               " already exists. Do you want to overwrite?",
               "File already exists", JOptionPane.YES_NO_OPTION);
           isSaveAccepted = (retVal == 0); //yes option equals 0
         }
         else { //confirm creation of new file at directory
           int retVal = JOptionPane.showConfirmDialog(new JFrame(),
               "Do you want to create file " + file.getName() + " at " +
               file.getPath().substring(0, file.getAbsolutePath().length() -
                                        (file.getName().length() + 1)),
               "File creation confirmation", JOptionPane.YES_NO_OPTION);
           isSaveAccepted = (retVal == 0); //yes option equals 0
         }
         if (isSaveAccepted) {
           StreamResult result = new StreamResult(file);
           DOMSource sourse = new DOMSource(m_tree.getDocument());
           Transformer transformer = TransformerFactory.newInstance().
               newTransformer();
           transformer.setOutputProperty("indent", "yes");
           transformer.transform(sourse, result);
         }
       }
     }
     catch (Exception e) {
       System.out.println("Saving GUI configuration error " + e.getMessage());
     }
   }

//******************************************************************************
//Optional routine used for testing tree modifications and printing out the XML document

    /*   private void printOut(Document doc) {
         try {
           TransformerFactory factory = TransformerFactory.newInstance();
           Transformer transformer = factory.newTransformer();
           DOMSource source = new DOMSource(doc);
           File res = new File("C:\\1.xml");
           StreamResult result = new StreamResult(res);
           transformer.setOutputProperty("indent", "yes");
           transformer.transform(source, result);
         }
         catch (Exception e) {
           System.out.println(e.getMessage());
         }
       }*/

//******************************************************************************
}
