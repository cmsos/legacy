#!/usr/bin/env python

###############################################################################
## A little script to check the PI TTCSpy log for signs of the phase-1
## pixel ROC reset sequence.
###############################################################################

import ConfigParser
import ast
import datetime
import json
import os
import pickle
import re
import sys
import urllib2

from collections import namedtuple

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class UTC(datetime.tzinfo):
    """UTC"""

    def utcoffset(self, dt):
        return datetime.timedelta(0)

    def tzname(self, dt):
        return "UTC"

    def dst(self, dt):
        return datetime.timedelta(0)

utc = UTC()

###############################################################################

# 0: Entry number.
# 1: Entry type.
# 2: Data payload.
# 3: Delta w.r.t. first TTCSpy entry.
Entry = namedtuple('Entry', ['number', 'type', 'data', 'delta'])

###############################################################################

# def pairwise(iterable):
#     it = iter(iterable)
#     a = next(it, None)

#     for b in it:
#         yield (a, b)
#         a = b

###############################################################################

def decode_delta(delta_str_in):

    orbits = 0
    bxs = 0
    try:
        if delta_str_in.find('orbit') > -1:
            orbits = int(delta_str_in.split('orbit')[0].strip())
        if delta_str_in.find('BX') > -1:
            bxs = int(delta_str_in.split('BX')[0].split()[-1].strip())
    except Exception, err:
        pdb.set_trace()

    # End of decode_delta().
    return (orbits, bxs)

###############################################################################

def calc_delta_delta((orbits0, bxs0), (orbits1, bxs1)):

    delta_orbits = orbits1 - orbits0
    delta_bxs = bxs1 - bxs0
    if delta_bxs < 0:
        delta_orbits -= 1
        delta_bxs += 3564
    elif delta_bxs >= 3564:
        delta_orbits += 1
        delta_bxs -= 3564

    # End of calc_delta().
    return (delta_orbits, delta_bxs)

###############################################################################

class Analyzer(CmdLineBase):

    def setup_parser_custom(self):
        # Add the data file choice.
        help_str = "The name of the captured data file to analyze."
        self.parser.add_argument("data_file",
                                 type=str,
                                 action="store",
                                 help=help_str)

        # End of setup_parser_custom().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).

        self.ttcspy_file_name = self.args.data_file
        # End of handle_args().

    def load_setup(self):
        pass

    def main(self):

        # Timestamp.
        TIMESTAMP = datetime.datetime.now(utc)

        ttcspy_data_raw = None
        try:
            in_file = open(self.ttcspy_file_name, "r")
            ttcspy_data_raw = in_file.read()
            in_file.close()
        except IOError, err:
            msg = "Could not read file '{0:s}': '{1:s}'."
            self.error(msg.format(self.ttcspy_file_name, err))

        ttcspy_data = []
        # regexp = re.compile("Entry ([0-9]+): Addressed command with data (0x[0-9a-f]{2}) to (0x[0-9a-f]{4})/(0x[0-9a-f]{2}) ([^ ]+)")
        regexp = re.compile("Entry ([0-9]+): (.*) with data (.*) at (.*) from first entry")
        for line in ttcspy_data_raw.split(os.linesep):
            match = regexp.search(line)
            if match:
                # 0: Entry number.
                # 1: Entry type.
                # 2: Data payload.
                # 3: Delta w.r.t. first TTCSpy entry.
                entry_number = int(match.group(1))
                entry_type = match.group(2)
                data_payload = match.group(3)
                delta = match.group(4)
                ttcspy_data.append(Entry(entry_number,
                                         entry_type,
                                         data_payload,
                                         delta))

        if not len(ttcspy_data):
            self.error("Nothing found in the TTCSpy capture data")

        #----------

        # Crunch through the data now...

        # Quick-n-dirty way to find the ROC reset command (B-go 14).

        # NOTE: The pixels emit B-command 0x1c as ROC reset upon B-go
        # 14. In the lab we use 0xe.
        results = []
        roc_resets = [x for x in ttcspy_data if x.data == '0x1c']
        if not len(roc_resets):
            # Not found. This is possible. Try again.
            print "Did not find any signs of a ROC reset in the TTCSpy data"

        else:
            # NOTE: At 100 kHz, 10 L1as is roughly one orbit's worth.
            num_l1as = 10
            lo = 0
            for (i, roc_reset) in enumerate(roc_resets):
                # Find some L1As before the ROC reset.
                ind_hi = roc_reset.number - 1
                ind_lo = max(lo, ind_hi - num_l1as + 1)
                l1as_before = ttcspy_data[ind_lo:ind_hi + 1]

                # Find some L1As after the ROC reset.
                ind_lo = roc_reset.number + 1
                if i != (len(roc_resets) - 1):
                    ind_hi = min(roc_resets[i+1].number, ind_lo + num_l1as - 1)
                else:
                    ind_hi = min(len(roc_resets) - 1, ind_lo + num_l1as - 1)
                l1as_after = ttcspy_data[ind_lo:ind_hi + 1]

                lo = roc_reset.number + 1

                results.append([l1as_before, roc_reset, l1as_after])

            print "Found {0:d} ROC resets".format(len(results))

            data_file_name = self.ttcspy_file_name.replace(".txt", ".pkl")

            ##########

            # Read the previously-stored results.
            data = None
            try:
                # print "Reading previous data from '{0:s}'.".format(data_file)
                in_file = open(data_file_name, 'rb')
                data = pickle.load(in_file)
                in_file.close()
            except (IOError, EOFError):
                # Nothing to be done. Something bad happened and our file went
                # AWOL. Let's try again.
                data = []

            ##########

            # Add the current results.
            tmp = [TIMESTAMP, results]
            prev = data
            prev.append(tmp)

            ##########

            # Store the results.
            # print "Writing data to '{0:s}'.".format(data_file_name)
            out_file = open(data_file_name, 'wb')
            pickle.dump(data, out_file)
            out_file.close()

            # delta_roc_reset = decode_delta(roc_reset.delta)
            # delta_l1a_before = decode_delta(l1a_before.delta)
            # delta_l1a_after = decode_delta(l1a_after.delta)

            # delta_before = calc_delta_delta(delta_l1a_before, delta_roc_reset)
            # delta_after = calc_delta_delta(delta_roc_reset, delta_l1a_after)

            # print "DEBUG JGH l1a_before: '{0:s}'".format(str(l1a_before))
            # print "DEBUG JGH roc_reset: '{0:s}'".format(str(roc_reset))
            # print "DEBUG JGH l1a_after: '{0:s}'".format(str(l1a_after))
            # print "Separation between ROC reset and last L1A before: {0:2d} orbits and {1:4d} BX".format(delta_before[0], delta_before[1])
            # print "Separation between ROC reset and first L1A after: {0:2d} orbits and {1:4d} BX".format(delta_after[0], delta_after[1])

        #----------

        # End of main().

    # End of class Analyzer.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to check the PI TTCSpy log " \
               "for signs of the phase-1 pixel ROC reset sequence."

    res = Analyzer(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
