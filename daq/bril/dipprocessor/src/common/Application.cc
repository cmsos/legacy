#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"

#include "xdata/InfoSpaceFactory.h"
#include "toolbox/BSem.h"

#include "b2in/nub/Method.h"
#include "bril/dipprocessor/Application.h"
#include "bril/dipprocessor/DipAnalyzer.h"
#include "bril/dipprocessor/DipAnalyzerFactory.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"

#include "interface/bril/TCDSTopics.hh"
#include "interface/bril/BCM1FTopics.hh"
#include "interface/bril/BHMTopics.hh"
#include "interface/bril/BCMTopics.hh"
#include "interface/bril/PLTTopics.hh"

XDAQ_INSTANTIATOR_IMPL (bril::dip::Application)

bril::dip::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
  xgi::framework::deferredbind(this,this,&bril::dip::Application::Default, "Default");
  b2in::nub::bind(this, &bril::dip::Application::onMessage);
  m_appInfoSpace = getApplicationInfoSpace();
  m_runnum = 0;
  m_lsnum = 0;
  m_nbnum = 0;
  m_dipRoot = "dip/CMS/";
  m_busName = "brildata";
  m_signalTopic = "NB4";//no need to configure, we always listen to NB4
  m_dipconnected = false;
  m_bkgd1 = -1.;
  m_bkgd2 = -1.;
  m_bkgd3 = -1.;
  m_bkgd4 = -1.;
  m_bkgd5 = -1.;
  m_bkgd6 = -1.;
  m_bkgd7 = -1.;
  m_bkgd8 = -1.;
  m_bkgd9 = -1.;
  toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
  m_lastreceive_bkgd12_sec = now.sec();
  try{
    m_appInfoSpace->fireItemAvailable("dipAnalyzers",&m_analyzerconf);
    m_appInfoSpace->fireItemAvailable("fillnum",&m_fillnum);
    m_appInfoSpace->fireItemAvailable("runnum",&m_runnum);
    m_appInfoSpace->fireItemAvailable("lsnum",&m_lsnum);
    m_appInfoSpace->fireItemAvailable("nbnum",&m_nbnum);
    m_appInfoSpace->fireItemAvailable("dipServiceName",&m_dipServiceName);
    m_appInfoSpace->fireItemAvailable("dipRoot",&m_dipRoot);
    m_appInfoSpace->fireItemAvailable("busName",&m_busName);
    m_appInfoSpace->fireItemAvailable("bkgd1",&m_bkgd1);
    m_appInfoSpace->fireItemAvailable("bkgd2",&m_bkgd2);
    m_appInfoSpace->fireItemAvailable("bkgd3",&m_bkgd3);
    m_appInfoSpace->fireItemAvailable("bkgd4",&m_bkgd4);
    m_appInfoSpace->fireItemAvailable("bkgd5",&m_bkgd5);
    m_appInfoSpace->fireItemAvailable("bkgd6",&m_bkgd6);
    m_appInfoSpace->fireItemAvailable("bkgd7",&m_bkgd7);
    m_appInfoSpace->fireItemAvailable("bkgd8",&m_bkgd8);
    m_appInfoSpace->fireItemAvailable("bkgd9",&m_bkgd9);
    m_appInfoSpace->addListener(this, "urn:xdaq-event:setDefaultValues");
  }catch(xdata::exception::Exception& e){
    std::string msg("Failed to setup appInfoSpace ");
    LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
    XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);
  }
  toolbox::net::UUID uuid;
  std::string processuuid = uuid.toString();
  std::string bkgd12_timerurn = "urn:dipApplication_bkgd12_timer"+processuuid;
  toolbox::task::Timer* t = toolbox::task::TimerFactory::getInstance()->createTimer( bkgd12_timerurn );
  const std::string taskname("bkgd12");
  toolbox::TimeInterval vsec(5,0);//start timer 5 sec later
  toolbox::TimeInterval checkinterval (4, 0); // check every 4 seconds
  toolbox::TimeVal startT = now + vsec;
  t->scheduleAtFixedRate (startT, this, checkinterval, 0, taskname);
}

bril::dip::Application::~Application (){
  destroyAnalyzers();
}

void bril::dip::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){

}

void bril::dip::Application::timeExpired( toolbox::task::TimerEvent& e ){
  toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
  xdata::UnsignedInteger timeelapsed = now.sec() - m_lastreceive_bkgd12_sec;
  if( timeelapsed.value_ > 5 && m_bkgd1.value_!=-1 && m_bkgd2.value_!=-1 ){
    LOG4CPLUS_WARN(getApplicationLogger(),timeelapsed.toString()+" sec passed with no bkgd1,2, resetting them to initial value");
    m_bkgd1 = -1;
    m_bkgd2 = -1;
  }
}

void bril::dip::Application::actionPerformed(xdata::Event& e){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){
    if( !m_dipRoot.value_.empty() ){
      char lastChar = *(m_dipRoot.value_.rbegin());
      if( lastChar!='/' ){
          m_dipRoot.value_ += '/';
      }
    }
    std::stringstream ss;
    this->getEventingBus(m_busName.value_).addActionListener(this);
    size_t nanalyzers = m_analyzerconf.elements();
    ss<<"Analyzers: ";
    for(size_t i=0; i<nanalyzers; ++i){
      xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_analyzerconf.elementAt(i));
      if(p){
  std::string analyzername = p->getProperty("analyzerName");
  ss<<analyzername<<" ";
  std::string dipFrequencyStr = p->getProperty("dipFrequency");
  xdata::UnsignedInteger dipFrequency;
  dipFrequency.fromString(dipFrequencyStr);
  createAnalyzer(analyzername,dipFrequency.value_,m_dipServiceName.value_);
      }
    }
    LOG4CPLUS_INFO(getApplicationLogger(), ss.str() );
    try{
      this->getEventingBus(m_busName.value_).subscribe(m_signalTopic);
      this->getEventingBus(m_busName.value_).subscribe(interface::bril::bcm1fbkgT::topicname());
      this->getEventingBus(m_busName.value_).subscribe(interface::bril::bhmbkgT::topicname());
      this->getEventingBus(m_busName.value_).subscribe(interface::bril::bcmT::topicname());
      this->getEventingBus(m_busName.value_).subscribe(interface::bril::pltbkgAT::topicname());
      this->getEventingBus(m_busName.value_).subscribe(interface::bril::pltbkgBT::topicname());
    }catch(eventing::api::exception::Exception& e){
      std::string msg("Failed to subscribe to eventing bus brildata");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
      XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);
    }

  }
}

void bril::dip::Application::createAnalyzer(const std::string& analyzername, unsigned int dipFrequency,
  const std::string& dipServiceName){
  DipAnalyzer* a = DipAnalyzerFactory::create(analyzername,dipFrequency,dipServiceName,this);
  if(!a){
    LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create analyzer "+analyzername);
  }else{
    m_analyzers.push_back(a);
  }
}

void bril::dip::Application::destroyAnalyzers(){
  for(std::vector<DipAnalyzer*>::iterator it=m_analyzers.begin();it!=m_analyzers.end();++it){
    delete *it;
  }
}

void bril::dip::Application::actionPerformed(toolbox::Event& e){
}

void bril::dip::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception){
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from (IN APPLICATION) "+topic);
    std::string data_version  = plist.getProperty("DATA_VERSION");
    std::string payload_dict = plist.getProperty("PAYLOAD_DICT");
    if(data_version.empty() || data_version!=interface::bril::DATA_VERSION){
      std::string msg("Received brildaq message has no or wrong version, do not process.");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);
      ref->release(); ref=0;
      return;
    }

    if(topic == m_signalTopic.toString()){
      if(ref!=0){
  //parse timing signal message header to get timing info
  interface::bril::DatumHead * thead = (interface::bril::DatumHead*)(ref->getDataLocation());
  unsigned int fillnum = thead->fillnum;
  unsigned int runnum = thead->runnum;
  unsigned int lsnum = thead->lsnum;
  unsigned int nbnum = thead->nbnum;

  //if(nbnum != m_nbnum.value_ || runnum != m_runnum.value_ ){ // even if nb number is not changed, it could be from different runs. so run number change should also trigger nbnum change signal
  m_nbnum = nbnum;
  m_lsnum = lsnum;
  m_runnum = runnum;
  m_fillnum = fillnum;
  LOG4CPLUS_INFO(getApplicationLogger(), "Application fire nbnum changed");
  if( m_dipconnected==false ){
    for(std::vector<DipAnalyzer*>::iterator it=m_analyzers.begin();it!=m_analyzers.end();++it){
      (*it)->subscribeToDip();
      (*it)->createDipPublication();
    }
    m_dipconnected = true;
  }
  m_appInfoSpace->fireItemValueChanged("nbnum",this);
  //}
      }
    }
    if(topic == interface::bril::bhmbkgT::topicname()){
      if(ref!=0){
  interface::bril::DatumHead * thead = (interface::bril::DatumHead*)(ref->getDataLocation());
  if( payload_dict.empty() ){
    std::string msg("Received bhmbkg message has no dictionary, do not process.");
          LOG4CPLUS_ERROR(getApplicationLogger(),msg);
          ref->release(); ref=0;
          return;
        }
  interface::bril::CompoundDataStreamer tc(payload_dict);
  float bkgd4 = -1;
  float bkgd5 = -1;
  tc.extract_field( &bkgd4, "beam1", thead->payloadanchor );
  tc.extract_field( &bkgd5, "beam2", thead->payloadanchor );
  m_bkgd4 = bkgd4;
  m_bkgd5 = bkgd5;
      }
    }
    if(topic == interface::bril::bcm1fbkgT::topicname()){
      if(ref!=0){
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	m_lastreceive_bkgd12_sec = now.sec();
	
  interface::bril::DatumHead * thead = (interface::bril::DatumHead*)(ref->getDataLocation());
  if( payload_dict.empty() ){
    std::string msg("Received bcm1fbkg message has no dictionary, do not process.");
          LOG4CPLUS_ERROR(getApplicationLogger(),msg);
          ref->release(); ref=0;
          return;
        }
  interface::bril::CompoundDataStreamer tc(payload_dict);
  float bkgd1 = -1;
  float bkgd2 = -1;
  tc.extract_field( &bkgd1, "plusz", thead->payloadanchor );
  tc.extract_field( &bkgd2, "minusz", thead->payloadanchor );
  m_bkgd1 = bkgd1;
  m_bkgd2 = bkgd2;
      }
    }
    if(topic == interface::bril::bcmT::topicname()){
      if(ref!=0){
  interface::bril::DatumHead * thead = (interface::bril::DatumHead*)(ref->getDataLocation());
  if( payload_dict.empty() ){
    std::string msg("Received bcm message has no dictionary, do not process.");
          LOG4CPLUS_ERROR(getApplicationLogger(),msg);
          ref->release(); ref=0;
          return;
        }
  interface::bril::CompoundDataStreamer tc(payload_dict);
  float bkgd3 = -1;
  tc.extract_field( &bkgd3, "pa1max", thead->payloadanchor );
  m_bkgd3 = bkgd3;
      }
    }
    if(topic == interface::bril::pltbkgAT::topicname()){
      if(ref!=0){
	interface::bril::DatumHead * thead = (interface::bril::DatumHead*)(ref->getDataLocation());  	
	if( payload_dict.empty() ){
	  std::string msg("Received pltbkgA message has no dictionary, do not process.");
          LOG4CPLUS_ERROR(getApplicationLogger(),msg);
          ref->release(); ref=0;
          return;
        }
	interface::bril::CompoundDataStreamer tc(payload_dict);
	float bkgd6 = -1;
	tc.extract_field( &bkgd6, "beam1bkg", thead->payloadanchor );
	m_bkgd6 = bkgd6;
	float bkgd7 = -1;
	tc.extract_field( &bkgd7, "beam2bkg", thead->payloadanchor );
	m_bkgd7 = bkgd7;
      }
    }
    if(topic == interface::bril::pltbkgBT::topicname()){
      if(ref!=0){
	interface::bril::DatumHead * thead = (interface::bril::DatumHead*)(ref->getDataLocation());  	
	if( payload_dict.empty() ){
	  std::string msg("Received pltbkgB message has no dictionary, do not process.");
          LOG4CPLUS_ERROR(getApplicationLogger(),msg);
          ref->release(); ref=0;
          return;
        }
	interface::bril::CompoundDataStreamer tc(payload_dict);
	float bkgd8 = -1;
	tc.extract_field( &bkgd8, "beam1bkg", thead->payloadanchor );
	m_bkgd8 = bkgd8;
	float bkgd9 = -1;
	tc.extract_field( &bkgd9, "beam2bkg", thead->payloadanchor );
	m_bkgd9 = bkgd9;
      }
    }
  }
  if(ref!=0){
    ref->release();
    ref=0;
  }
}
