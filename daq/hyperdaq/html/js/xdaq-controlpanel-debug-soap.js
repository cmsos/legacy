function xdaqSendSoap()
{
	var url = $("#soapurl").val();
	var msg = $("#soapmsg").val();
	
	var options = {
		url: url,
		data: msg,
		contentType: "text/xml",
		error: function (xhr, textStatus, errorThrown) {
			$("#soaprespdiv").val("ERROR : " + xhr.responseText);
		}
	};

	$("#submitbut").removeClass("xdaq-submit-button xdaq-green");
	$("#submitbut").addClass("xdaq-red");
	
	xdaqAJAX(options, function (data, textStatus, xhr) {
		$("#soaprespdiv").val(xhr.responseText);

		$("#submitbut").removeClass("xdaq-red");
		$("#submitbut").addClass("xdaq-green");
	});
}