#include "amc13controller/StatusInfoSpaceHandler.hh"
#include "d2s/utils/loggerMacros.h"

amc13controller::StatusInfoSpaceHandler::StatusInfoSpaceHandler( xdaq::Application* xdaq, 
                                                       utils::InfoSpaceUpdater *updater )
    : AMC13BaseInfoSpaceHandler( xdaq, "Status_IS", updater )
{
//Application state:
createuint32("testCounter", 0, "", NOUPDATE, "This is a dummy parameter which is counting up to see that the AJAX monitoring is working. It is useless to set this parameter to some value.");
createuint32( "instance", 0, "", NOUPDATE );
createuint32( "slotNumber", 0, "", NOUPDATE );
createstring( "stateName", "uninitialized" );
createstring( "subState", "" );
createstring( "failedReason", "");
createstring( "lockStatus", "unknown" );

`!
}
