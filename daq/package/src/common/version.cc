// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "config/version.h"

GETPACKAGEINFO(config)

void config::checkPackageDependencies() 
{
	// CHECKDEPENDENCY(toolbox);   
}

std::set<std::string, std::less<std::string> > config::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;

    // ADDDEPENDENCY(dependencies,toolbox); 
  
    return dependencies;
}	
	
