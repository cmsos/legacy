package xdaq.tools.daqlet;

import java.util.HashMap;
import java.util.Vector;


public class DaqletInstanceInfo
{
	HashMap daqlets_;
	
	public DaqletInstanceInfo()
	{
		daqlets_ = new HashMap();
	}
	
	public void put (String association, String instance, int daqletNum,  Daqlet daqlet)
	{
                Integer daqleti = new Integer(daqletNum);
		Vector v = (Vector) daqlets_.get(daqleti+association+instance);
		if (v == null)
		{
			v = new Vector();
			v.addElement (daqlet);
			daqlets_.put (daqleti+association+instance, v);
		} else {
			v.addElement (daqlet);
		}
	}
	
	public Vector get (String association, String instance, int daqletNum)
	{
        
                Integer daqlet = new Integer(daqletNum);
		return  (Vector) daqlets_.get(daqlet+association+instance);
	}
	
	public boolean exists (String association, String instance, String code, int daqletNum)
	{
                Integer daqlet = new Integer(daqletNum);
		Vector daqletVector = (Vector) daqlets_.get ( daqlet+association+instance);
		if (daqletVector == null) return false;
		
		for (int i = 0; i < daqletVector.size(); i++) 
		{
			Daqlet d = (Daqlet)daqletVector.elementAt(i);
			if (d.getClass().getName().equals(code)) return true;
		}
		return false;
	}
}
