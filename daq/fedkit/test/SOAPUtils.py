import re

import urllib2

#----------------------------------------------------------------------
class SOAPClient:
   """ a SOAP client for a given xdaq executive instance """

   #----------------------------------------

   def __init__(self, host, port, classname, instance):
      self.host = host
      self.port = port
      self.classname = classname
      self.instance = instance
      self.url = "http://%s:%d" % (host, port)

   #----------------------------------------



   def getSoapActionFromClassAndInstance(self):
   #     return "urn:xdaq-application:class=" + classname + ",lid=11"
      retval = "urn:xdaq-application:class=" + self.classname

      # if instance:
      retval += ",instance=" + str(self.instance)

      return retval

   #----------------------------------------

   def send(self,soap_msg, parseResponse = True):
      # produce the soap action
      soap_action = self.getSoapActionFromClassAndInstance()

      req = urllib2.Request(self.url, soap_msg,
                            { "SOAPAction": soap_action }
                            )
      
      u = urllib2.urlopen(req)
      headers = u.info()
      data = u.read()

      if parseResponse:
         # return a parsed XML document
         import xml.dom.minidom
         data = xml.dom.minidom.parseString(data)

      return data                         # the soap response


   #----------------------------------------

   def makeMessage(self, body):
      """ generates a SOAP message.

          body must be the text including the opening part of the body tag
          and including the its closing part.

          """

      msg = """<soap-env:Envelope soap-env:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
      xmlns:soap-env="http://schemas.xmlsoap.org/soap/envelope/"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
      xmlns:xdaq="urn:xdaq-soap:3.0"
      xmlns:p="urn:xdaq-application:%s" 
   >
     <soap-env:Header/>
     """ % self.classname + body + """
   </soap-env:Envelope>
   """
      return msg

   #----------------------------------------

   def makeParameterQueryMessage(self):

       body = "<soap-env:Body>" + \
              "<xdaq:ParameterQuery/>" + \
              "</soap-env:Body>" 

       return self.makeMessage(body)

   #----------------------------------------

   def makeSimpleCommandMessage(self,command):
      return self.makeMessage('"<soap-env:Body><xdaq:%s xmlns:xdaq="urn:xdaq-soap:3.0"/>"</soap-env:Body>"' % command)


#----------------------------------------------------------------------
def getSoapActionFromAppId(appid):
    return "urn:xdaq-application:lid=" + str(appid)

#----------------------------------------------------------------------

def makeInterfaceQuerySoapMessage():

    body = "<soap-env:Body>" + \
           "<xdaq:InterfaceQuery/>" + \
           "</soap-env:Body>" 

    return makeSoapMessage(body)

#----------------------------------------------------------------------

def makeParameterSetSoapMessage(app_class_name, parameter_name, parameter_type, parameter_value):
   return "<soap-env:Body>" + \
          "<xdaq:ParameterSet>" + \
          "<properties xmlns=\"urn:xdaq-application:" + app_class_name + "\" xsi:type=\"soapenc:Struct\">" + \
          "<" + parameter_name + " xsi:type=\"xsd:" + parameter_type + "\">" + str(parameter_value) + "</" + parameter_name + ">" + \
          "</properties>" + \
          "</xdaq:ParameterSet>" + \
          "</soap-env:Body>" 

#----------------------------------------------------------------------

def makeParameterGetMessage(app_class_name, parameter_name, parameter_type):
   return \
          "<soap-env:Body>" + \
          "<xdaq:ParameterGet>" + \
          "<properties xmlns=\"urn:xdaq-application:" + app_class_name + "\" xsi:type=\"soapenc:Struct\">" + \
          "<" + parameter_name + " xsi:type=\"xsd:" + parameter_type + "\" />" + \
          "</properties>" + \
          "</xdaq:ParameterGet>" + \
          "</soap-env:Body>" 


#----------------------------------------------------------------------

import xml

#----------------------------------------------------------------------

def getXmlNodePath(xmlnode):
    """ tries to produce a path back to the root node from the given
    node """

    retval = ""

    while xmlnode != None and xmlnode.nodeType != xmlnode.DOCUMENT_NODE:

        if retval != "":
            retval = "/" + retval

        retval = xmlnode.nodeName + retval

        xmlnode = xmlnode.parentNode

    return "/" + retval


#----------------------------------------------------------------------

def makeXPathContextForSoapParameterResponse(xmldoc):
   context = xml.xpath.Context.Context(xmldoc)

   p_namespace = None

   # this is a hack to find out the namespace of the 'p' prefix
   # (which depends on the application which created the xml
   # response)
   for node in xml.xpath.Evaluate('//*', xmldoc, context):
      if re.search('^p:',node.nodeName):
         p_namespace = node.namespaceURI
         break

   context.setNamespaces({
      "p" : p_namespace,
      "soapenc": "http://schemas.xmlsoap.org/soap/encoding/",
      "xsd": "http://www.w3.org/2001/XMLSchema",
      "xsi": "http://www.w3.org/2001/XMLSchema-instance",
      "xdaq": 'urn:xdaq-soap:3.0',
      })

   return context


#----------------------------------------------------------------------

def getResultsFromParameterQueryOrGetSoapResponse(xmldoc):
    """ given an xml document (which is a SOAP response
    to a ParameterQuery or ParamterGet SOAP commands), this returns a
    dictionary of parameter,value pairs of the data
    found in the soap response"""

    result = {}

    # check whether this is a response to a ParameterQuery or ParameterGet
    # command

    response_nodes = [ prop_group_node for prop_group_node in xml.xpath.Evaluate('//xdaq:ParameterQueryResponse', xmldoc, makeXPathContextForSoapParameterResponse(xmldoc))]

    if len(response_nodes) == 0:
       response_nodes = [ prop_group_node for prop_group_node in xml.xpath.Evaluate('//xdaq:ParameterGetResponse', xmldoc, makeXPathContextForSoapParameterResponse(xmldoc))]

    assert(len(response_nodes) == 1)

    def getValues(prefix, xmlnode):

        # print "MAKING CONTEXT"
        context = makeXPathContextForSoapParameterResponse(xmlnode)
        # print "DONE MAKING CONTEXT",context.node.nodeName

        # print "PREFIX",prefix,xmlnode.nodeName

        prop_group_nodes = [ prop_group_node for prop_group_node in xml.xpath.Evaluate('p:properties', xmlnode, context)]

        # for prop_group_node in xml.xpath.Evaluate('//p:properties', xmlnode, context):
        for prop_group_node in prop_group_nodes:

            # print "PROP",prop_group_node.nodeName
            # print "PATH",getXmlNodePath(prop_group_node)

            for url in prop_group_node.childNodes:

                # print "URL",url.nodeName

                if url.nodeType != url.ELEMENT_NODE:
                   continue

                if url.hasAttribute("xsi:type"):
                   data_type = url.getAttribute("xsi:type")
                else:
                   data_type = None
                # print "ZZZ",data_type

                if data_type == "soapenc:Struct":
                    # print "YYY",getXmlNodePath(url)
                    getValues(prefix + url.nodeName + "/",url)
                    continue

                if len(url.childNodes) > 0:
                    result[prefix + url.nodeName] = url.childNodes[0].nodeValue
                else:
                    result[prefix + url.nodeName] = None

    # for url in xml.xpath.Evaluate('p:properties/*', xmldoc, context):
    getValues("",response_nodes[0])
        

    return result

#----------------------------------------------------------------------

import xml
import xml.xpath
import xml.dom
import xml.dom.minidom

def performParameterQuery(url,lid):
    """Given a xdaq url and an application lid, this performs
    a ParameterQuery via soap and returns a dictionary
    """

    soap_action = getSoapActionFromAppId(lid)

    # create the ParameterQuery SOAP message
    msg = makeParameterQuerySoapMessage()

    # send the soap command and read back the response
    response = sendSoapMessage(url,msg,soap_action)

    # parse the response into an xml document
    doc = xml.dom.minidom.parseString(response)

    # convert this into a dictionary of name,value pairs
    values = getResultsFromParameterQueryOrGetSoapResponse(doc)

    return values
    

#----------------------------------------------------------------------

def getStrobeAndEventCounter(host,port,xdaq_app_lid):

    import urllib

    # reload main page in order to reread the counters (probably
    # not the way it should be but this works)
    dummy = urllib.urlopen(("http://%s:%d/" % (host,port)) +
                           "urn:xdaq-application:lid=%d/Default" % xdaq_app_lid).read()

    # get the parameters
    values = performParameterQuery("http://" + host + ":" + str(port),
                                   xdaq_app_lid)

    if values.has_key('p:Location'):
        location = int(values['p:Location'])

        if values.has_key('p:StrobeCounter'):
            strobe_counter = long(values['p:StrobeCounter'])
        else:
            strobe_counter = None

        if values.has_key('p:EventCounter'):
            event_counter = long(values['p:EventCounter'])
        else:
            event_counter = None

        return [ location, strobe_counter, event_counter ]

    # ParameterQuery probably returns an empty set (e.g.
    # because the XDAQ application is not in a good state)
    return [ None, None, None ]


#----------------------------------------------------------------------

def prettyFormatXmlString(xmlstring):
    """ formats the xmlstring in a pretty way. This probably
    also can be used to 'canonicalize' xml trees. """

    import StringIO
    import xml.dom.minidom
    import xml.dom.ext

    # parse the xml string (convert it
    # into a dom object)
    
    doc = xml.dom.minidom.parseString(xmlstring)

    # create a stringstream buffer
    buf = StringIO.StringIO()
    xml.dom.ext.PrettyPrint(doc,buf)

    return buf.getvalue()

#----------------------------------------------------------------------

class ExceptionFromSoapServer(Exception):
    """ Class which can be thrown if the server responds with a faultstring tag """
    pass

#----------------------------------------------------------------------

def getXdaqApplicationClass(url, appid):
    """ performs a parameter query soap command and tries to
    get the application's class from it """

    msg = makeParameterQuerySoapMessage()

    soap_action = getSoapActionFromAppId(appid)
    response = sendSoapMessage(url,msg, soap_action)

    # analyse the response
    doc = xml.dom.minidom.parseString(response)
    res = getResultsFromParameterQueryOrGetSoapResponse(doc)

    return res['p:descriptor/p:class']

#----------------------------------------------------------------------

