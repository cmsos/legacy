#!/usr/bin/env python

###############################################################################
## A little script to read and store the RF2TTCController state.
###############################################################################

import ast
import ConfigParser
import os
import pickle
import urllib2
import simplejson
import sys
import time

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_soap import build_xdaq_soap_command_message
from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Snapshotter(CmdLineBase):

    # The (string) identifier of the RunControl session 'owning' the
    # TCDS hardware.
    RC_SESSION_ID = "TEST_JEROEN"

    FILE_NAME = "rf2ttc_snapshots.pkl"

    def pre_hook(self):
        # The target comes from the command line.
        self.target = None
        # The following are read from the config file.
        self.targets = {}
        # End of pre_hook().

    def setup_parser(self):
        # Start with the basic parser configuration.
        super(Snapshotter, self).setup_parser()
        # Add the option to choose a custom cfg file.
        help_str = "Configuration file to use [default: %default]"
        self.parser.add_option("--cfg-file",
                               action="store",
                               default="tcds_setup.cfg",
                               help=help_str)
        # End of setup_parser().

    def handle_args(self):
        # NOTE: Do not call the parent handle_args() since we are not
        # requiring an IP address (unlike pretty much all other TCDS
        # scripts).

        # On the command line we require one target application.
        if len(self.args) < 1:
            self.error("One arguments is required: " \
                       "the target application.")
        else:
            self.target = self.args[0]

        self.cfg_file_name = self.opts.cfg_file
        # End of handle_args().

    def main(self):

        # Host parameters etc. come from the configuration file.
        cfg_parser = ConfigParser.SafeConfigParser()
        config_file_name = self.cfg_file_name
        if self.verbose:
            print "Reading configuration from '{0:s}'".format(config_file_name)
        if not os.path.exists(config_file_name):
            self.error("Config file '{0:s}' does not exist.".format(config_file_name))
        cfg_parser.read(config_file_name)

        # Find all possible target applications.
        targets = cfg_parser.items("targets")
        for (target_name, target_dict) in targets:
            self.targets[target_name] = ast.literal_eval(target_dict)
        if self.verbose:
            if len(self.targets):
                print "Found the following target applications: "
                target_names = self.targets.keys()
                target_names.sort()
                for target_name in target_names:
                    print "  {0:s}".format(target_name)
            else:
                print "No target applications found"

        #----------

        # Check if the target name is valid.
        if not self.target in self.targets.keys():
            msg = "'{0:s}' is not a valid target application. " \
                  "Valid targets are read from the config file '{1:s}' " \
                  "and are '{2:s}'."
            self.error(msg.format(self.target,
                                  self.cfg_file_name,
                                  "', '".join(sorted(self.targets.keys()))))

        #----------

        # Now actually do what we have been asked to do.
        target_details = self.targets[self.target]
        host_name = target_details["host"]
        port_number = target_details["port"]
        lid_number = target_details["lid"]

        tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/update"
        url = tmp.format(host_name, port_number, lid_number)

        # Get data of current snapshot.
        req = urllib2.Request(url)
        opener = urllib2.build_opener()
        f = opener.open(req)
        contents = simplejson.load(f)
        timestamp_pre = contents["Application state"]["Latest monitoring update time"]
        print "Pre-timestamp: {0:s}.".format(timestamp_pre)

        # Wait for the timestamp to change (indicating a new
        # acquisition).
        print "Waiting for next acquisition to appear."
        timestamp_post = timestamp_pre
        while (timestamp_post == timestamp_pre):
            req = urllib2.Request(url)
            opener = urllib2.build_opener()
            f = opener.open(req)
            contents = simplejson.load(f)
            timestamp_post = contents["Application state"]["Latest monitoring update time"]
            time.sleep(1)
        print "Post-timestamp: {0:s}.".format(timestamp_post)
        bcmainSrc = contents["itemset-current-sources"]["Current BC-main source"]

        # Append current snapshot to measurement history.
        snapshots = []
        file_name = Snapshotter.FILE_NAME
        if os.path.exists(file_name):
            with open(file_name, "rb") as snapshot_file:
                snapshots = pickle.load(snapshot_file)
        snapshots.append((timestamp_post, bcmainSrc))
        with open(file_name, "wb") as snapshot_file:
            pickle.dump(snapshots, snapshot_file)

        # End of main().

    # End of class Snapshotter.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to read and store the RF2TTCController state."
    usage_str = "usage: %prog application"
    epilog_str_tmp = ""
    epilog_str = epilog_str_tmp.format(os.path.basename(sys.argv[0]))

    Snapshotter(desc_str, usage_str, epilog_str).run()

    print "Done"

###############################################################################
