/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * mpila.h
 *
 *  Created on: Jan 26, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/CompletionEvent.hpp"
#include "mpila/CompletionQueue.hpp"
#include "mpila/InterfaceAdapter.hpp"
#include "mpila/MemoryPool.hpp"
#include "mpila/PendingRequest.hpp"
#include "mpila/WorkRequest.hpp"
#include "mpila/rlist.hpp"
