#ifndef _tcds_utils_SOAPCmdBase_h_
#define _tcds_utils_SOAPCmdBase_h_

#include <string>

#ifndef XDAQ_TARGET_XDAQ15
#include "xoap/exception/Exception.h"
#endif

#include "toolbox/lang/Class.h"
#include "xoap/MessageReference.h"

namespace tcds {
  namespace utils {

    template <class T>
    class SOAPCmdBase : public toolbox::lang::Class
    {

    public:
      virtual ~SOAPCmdBase();

      std::string commandName() const;
#ifndef XDAQ_TARGET_XDAQ15
      xoap::MessageReference execute(xoap::MessageReference msg) throw (xoap::exception::Exception);
#else
      xoap::MessageReference execute(xoap::MessageReference msg);
#endif

      virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg) = 0;

    protected:
      SOAPCmdBase(T& controller,
                  std::string const& commandName,
                  bool const requiresHwLease=true);

      T& controller_;
      std::string const commandName_;
      bool const requiresHwLease_;

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdBase.hxx"

#endif // _tcds_utils_SOAPCmdBase_h_
