#include "tcds/utils/MonitorItem.h"

tcds::utils::MonitorItem::MonitorItem(std::string const& name,
                                      std::string const& description,
                                      InfoSpaceHandler* const infoSpaceHandler,
                                      std::string const& docString) :
  name_(name),
  description_(description),
  infoSpaceHandlerP_(infoSpaceHandler),
  docString_(docString)
{
}

std::string
tcds::utils::MonitorItem::getName() const
{
  return name_;
}

std::string
tcds::utils::MonitorItem::getDescription() const
{
  return description_;
}

tcds::utils::InfoSpaceHandler*
tcds::utils::MonitorItem::getInfoSpaceHandler() const
{
  return infoSpaceHandlerP_;
}

std::string
tcds::utils::MonitorItem::getDocString() const
{
  return docString_;
}
