#include "ptNTCP.h"
#include "TaskAttributes.h"
#include "i2oProtocolOut.h"
#include "SocketSendEntry.h"
#include "SocketReceiveEntry.h"
#include "BufRef.h"
#include "pta.h"
#include "Transmitter.h"
#include <netdb.h>

ptNTCP::ptNTCP (pta * ptap): pt(ptap), Task("ptNTCP")
{
	transmitter_->activate();
}

void ptNTCP::plugin()
{
	transmitter_ = new Transmitter(15000, logger_);
	 pout_ = new i2oProtocolOut(ptap_, logger_);
}

void ptNTCP::ParameterSetDefault(list<string> & paramNames) 
{
	// user cannot change polling mode, TCP is always threaded control
	pollingMode_ = false;
}
	
ptNTCP::~ptNTCP () 
{
	unsigned int i;

	delete pout_;

	for (i=0; i < pinTable_.size(); i++) {
		delete pinTable_[i];
	}
	for (i=0; i < recvSocketTable_.size(); i++) {
		delete recvSocketTable_[i];
	}

	for (i=0; i < sendSocketTable_.size(); i++) {
		delete sendSocketTable_[i];
	}

}


SendObj * ptNTCP::createSendObj(Address * local, Address * remote) 
{
	LOG4CPLUS_DEBUG (logger_, toolbox::toString("NTCP: local %x, remote %x", (unsigned int)local, (unsigned int)remote));

	SocketSendEntry * tmpEntry = new SocketSendEntry(local, remote, ptap_, transmitter_, logger_);
	sendSocketTable_.push_back(tmpEntry);
	return 	new SendObj(tmpEntry,pout_,this, logger_);
}
	

	
Address * ptNTCP::createAddress (DOMNode * addressNode)
{
	tcpAddr* addr = new tcpAddr();
	unsigned char ipnumber[4];
	
	string hostname = getNodeAttribute(addressNode, "hostname");
	string port = getNodeAttribute (addressNode, "port");
	
	if ((hostname == "") || (port == ""))
	{
		XDAQ_LOG_AND_RAISE (xdaqException, logger_, toolbox::toString("TCP Address definition invalid."));
		return (Address*) 0;
	}
	
#ifdef HAS_GETHOSTBYNAME					
	struct hostent* hP = gethostbyname(hostname.c_str());
	if ( hP != (struct hostent*)0 ) {
		// it could resolve the name to a IP number
		char* inAddr = hP->h_addr_list[0];
		ipnumber[0] = inAddr[0];
		ipnumber[1] = inAddr[1];
		ipnumber[2] = inAddr[2];
		ipnumber[3] = inAddr[3];
	} else { // oit assumes it is a valid ip number
		unsigned long inAddr = inet_addr(hostname.c_str());

		ipnumber[0] = (unsigned char)(((char*)&inAddr)[0]);
		ipnumber[1] = (unsigned char)(((char*)&inAddr)[1]);
		ipnumber[2] = (unsigned char)(((char*)&inAddr)[2]);
		ipnumber[3] = (unsigned char)(((char*)&inAddr)[3]);	
	}
#else // understand only IP numbers
	unsigned long inAddr = inet_addr(hostname.c_str());

	ipnumber[0] = (unsigned char)(((char*)&inAddr)[0]);
	ipnumber[1] = (unsigned char)(((char*)&inAddr)[1]);
	ipnumber[2] = (unsigned char)(((char*)&inAddr)[2]);
	ipnumber[3] = (unsigned char)(((char*)&inAddr)[3]);
#endif
	inet_quadtostr(addr->host,ipnumber);

	addr->port = atoi(port.c_str());
	LOG4CPLUS_DEBUG(logger_,toolbox::toString("created address for TCP host:%s port:%ld",addr->host,addr->port));
	return addr;
}
    
RecvObj * ptNTCP::createRecvObj(Address * address) 
{
	i2oProtocolIn * pin = new i2oProtocolIn(ptap_, logger_);
	pinTable_.push_back(pin);

	SocketReceiveEntry * tmpEntry = new SocketReceiveEntry(address, ptap_, pin, pollingMode_, logger_);

	recvSocketTable_.push_back(tmpEntry);
	RecvObj * tmpRecvObj = new RecvObj(tmpEntry , pout_, logger_);
	TaskAttributes att;
	att.name("ptTcp");
	tmpRecvObj->set(&att);
	if (! pollingMode_ )
		tmpRecvObj->activate();
	return 	tmpRecvObj;
}


int ptNTCP::svc() 
{
	return 0;
}
