//
//  XPropMenuBar.java
//  
//
//  Created by Luciano Orsini on Sun Jun 29 2003.
//  Copyright (c) 2003 __CERN__. All rights reserved.
//

package xdaq.xdesk.xcommander;

import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import java.io.*;
import org.w3c.dom.*;
import java.awt.*;
import java.awt.event.*;
import org.apache.log4j.*;
import xdaq.xdesk.tools.*;

public class XCommanderMenuBar extends xdaq.xdesk.tools.MenuBar 
{
        Logger logger = Logger.getLogger (XCommanderMenuBar.class);
	XCommander controller_;
	
	public XCommanderMenuBar(XCommander controller)
	{
		super("XCommander",(MenuBarListener)controller);
			
		controller_ = controller;
			
		JMenu menu;
		JMenuItem item;
		
		// FILE menu
		//
		menu = new JMenu ("File");
		menu.setMnemonic ('f');
						
		item = new JMenuItem (new AbstractAction ("Close") 
		{
                    public void actionPerformed (ActionEvent e)
                    {
		    	// Dispose the frame into which the viewer panel has been put.
			// This will invoke the proper shutdown function of the XPropWindowListener
                             controller_.closeAction();

                    }});

		menu.add (item);
				
		this.add (menu);
		
        }        
}
