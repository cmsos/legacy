// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <vector>
#include <map>
#include <string>
#include <sstream>
#include "xmas/exception/Exception.h"
#include "toolbox/regex.h"
#include "toolbox/Properties.h"
#include "xmas/FlashListDefinition.h"
#include "toolbox/BSem.h"
#include "xoap/domutils.h"
#include "xmas/xmas.h"
#include "toolbox/Runtime.h"
#include "xoap/DOMParserFactory.h"
#include "xoap/DOMParser.h"
#include "toolbox/net/URN.h"
#include "xercesc/util/XMLUri.hpp"

xmas::FlashListDefinition::FlashListDefinition(DOMNode* flash)
	
{
	if ( flash != 0 )
	{
		this->create(flash);
	}
}

void xmas::FlashListDefinition::parse(DOMDocument* doc ,const std::string & href) 
{
	// look in owner document
	DOMNodeList* flashLists = doc->getElementsByTagNameNS (
		xoap::XStr(xmas::NamespaceUri), 
		xoap::XStr("flash"));
	for (XMLSize_t i = 0; i < flashLists->getLength(); ++i)
	{
		DOMNode* flashNode = flashLists->item(i);
		std::string id = xoap::getNodeAttribute(flashNode, "id");
		
		if ( id == "" ) continue; // not a valid flash definitiion
		
		std::string version = xoap::getNodeAttribute(flashNode, "version");
		if ( version == "" ) 
		{
			std::stringstream msg;
			msg << "Missing 'version' attribute for flashlist '" << id << "'";
			XCEPT_RAISE (xmas::exception::Exception, msg.str());
			
		}
		else
		{
			this->setProperty("version", version);
		}

		std::string key = xoap::getNodeAttribute(flashNode, "key");
                if ( key != "" )
                {
                        this->setProperty("key", key);
                }


		if ( href.find (id) != std::string::npos )
		{
			this->setProperty("name", id);

			DOMNodeList* items = flashNode->getChildNodes();
			for (XMLSize_t j = 0; j < items->getLength(); j++)
			{
				DOMNode* item = items->item(j);
				if ((item->getNodeType() == DOMNode::ELEMENT_NODE) &&
				    (xoap::XMLCh2String(item->getLocalName()) == "item"))
				{

					// If it's not an infospace item, do something else

					xmas::ItemDefinition  * itemDefinition = new xmas::ItemDefinition(item);
					//
					// if name already existing override
					//
					std::string name = itemDefinition->getProperty("name");
					
					std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i = items_.find(name);
					if (i != items_.end())
					{
						delete itemDefinition;
						std::stringstream msg;
						msg << "Ambiguous item name '";
						msg << name << "', already existing declaration in list of item";
						XCEPT_RAISE (xmas::exception::Exception, msg.str());
					}
					items_[name] = itemDefinition;
					
				}
			}
			return;	
		}					
	}
	std::stringstream msg;
	msg << "Cannot find flashlist href '" << href  << "'";
	XCEPT_RAISE (xmas::exception::Exception, msg.str());
}
void xmas::FlashListDefinition::create(DOMNode* flash)
	
{
	try
	{
		// check if it is a link to a flashlist definition
		//
		if (xoap::getNodeAttribute(flash, "xlink:type") == "locator")
		{
			std::string href = xoap::getNodeAttribute(flash, "xlink:href");
			this->setProperty("location", href);
			if (href.find_first_of("#") == 0)
			{
				this->setProperty ("local", "true");
				// look in owner document
				DOMDocument* doc = flash->getOwnerDocument();
				this->parse(doc,href);
			}
			else
			{
				// The profile may be a pattern, so expand it before
				std::vector<std::string> files;
				try
				{
					files = toolbox::getRuntime()->expandPathName(href);
				} 
				catch (toolbox::exception::Exception& e)
				{
					std::stringstream msg; 
					msg << "Failed to expand path name '" << href << "'";
					XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);	
				
				}
				if ( files.size() != 1 )
				{
					std::stringstream msg; 
					msg << "Ambiguous path name '" << href << "' resolve to multiple files";
					XCEPT_RAISE (xmas::exception::Exception, msg.str());	
				}
				// skip #<identifier> for loading file
				
				std::string::size_type i = files[0].find_first_of("#");
				
				this->setProperty ("local", "false");
				
				DOMDocument* doc = 0;
				std::string normaluri = files[0].substr(0,i);	
				if (normaluri.find_first_of("/") == 0)
                        	{ 
					DOMDocument* owner = flash->getOwnerDocument();
					// set relative to the website root
					XMLURL rooturi (owner->getDocumentURI(),xoap::XStr(normaluri)) ;
					normaluri = xoap::XMLCh2String(rooturi.getURLText()); 
					
				}
					
				try
				{
					doc = xoap::getDOMParserFactory()->get("configure")->loadXML( normaluri );
				}
				catch(xoap::exception::Exception & e)
				{
					if (doc != 0) doc->release();
					std::stringstream msg;
					msg << "Failed to load flashlist from document href '";
					msg << href << "'";
					XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);	
				}
				
				try
				{
					this->parse(doc,href);
					doc->release();	
				}
				catch(xmas::exception::Exception & e)
				{
					if (doc != 0) doc->release();
					std::stringstream msg;
					msg << "Failed to parse flashlist in document href '";
					msg << href << "'";
					XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);		
				}
			}
		}
	}
	catch (xmas::exception::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create flashlist definition";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}

xmas::FlashListDefinition::~FlashListDefinition()
{
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i = items_.begin();
	while (i != items_.end())
	{
		delete (*i).second;
		++i;
	}
}

xmas::ItemDefinition* xmas::FlashListDefinition::getItem
	(
		const std::string& id
	) 
	
{
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i = items_.find(id);
	if (i != items_.end())
	{
		return (*i).second;
	}
	else
	{
		std::string msg = "Item ";
		msg += id;
		msg += "not found in flash list";
		XCEPT_RAISE (xmas::exception::Exception, msg);
	}
}

std::vector<xmas::ItemDefinition* > xmas::FlashListDefinition::getItems()
{
	std::vector<xmas::ItemDefinition* > v;
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); i++)
	{
		v.push_back( (*i).second );
	}
	return v;
}

std::vector<std::string> xmas::FlashListDefinition::getItemNames()
{
	std::vector<std::string> v;
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); i++)
	{
		v.push_back( (*i).first );
	}
	return v;
}

std::vector<std::string> xmas::FlashListDefinition::getItems
	(
		const std::string & infospace
	)
{
	std::vector<std::string> v;
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); i++)
	{
		if (((*i).second)->hasProperty("infospace")) // infospace type item only, otherwise skip it
		{
			if ( ((*i).second)->getProperty("infospace") == infospace )
			{
				v.push_back( (*i).first );
			}
		}	
	}
	return v;
}

std::vector<xmas::ItemDefinition*> xmas::FlashListDefinition::matchInfoSpaceItemProperties
	(
		const std::string & pattern
	)
{
	std::vector<xmas::ItemDefinition*> v;
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); ++i)
	{
		if (((*i).second)->hasProperty("infospace")) // infospace type item only, otherwise skip it
		{
			std::string infospaceRegex = ((*i).second)->getProperty("infospace");

			if ( toolbox::regx_match(pattern, infospaceRegex) )
			{
				v.push_back( (*i).second );
			}
		}
	}
	return v;
}

std::vector<xmas::ItemDefinition*> xmas::FlashListDefinition::getInfoSpaceItemProperties()
{
	std::vector<xmas::ItemDefinition*> v;
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); ++i)
	{
		if (((*i).second)->hasProperty("infospace")) // infospace type item only, otherwise skip it
		{
			v.push_back( (*i).second );
		}
	}
	return v;
}

std::vector<xmas::ItemDefinition*> xmas::FlashListDefinition::getFunctionItemProperties()
{
	std::vector<xmas::ItemDefinition*> v;
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
	for (i = items_.begin(); i != items_.end(); ++i)
	{	
		if ( ((*i).second)->getProperty("function") != "" )
		{
			v.push_back( (*i).second );
		}
	}
	return v;
}

std::set<std::string> xmas::FlashListDefinition::getInfospacesRegexSet()
 {
        std::set<std::string> v;
        std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i;
        for (i = items_.begin(); i != items_.end(); ++i)
        {
		if ( ((*i).second)->hasProperty("infospace") ) // infospace type item only, otherwise skip it
		{
                	v.insert( ((*i).second)->getProperty("infospace"));
		}	
        }
        return v;
}


void xmas::FlashListDefinition::removeItem
	(
		const std::string& id
	) 
	
{
	std::map<std::string, xmas::ItemDefinition*, toolbox::stl::ci_less>::iterator i = items_.find(id);
	if (i != items_.end())
	{
		delete (*i).second;
		items_.erase(i);
	}
	else
	{
		std::string msg = "Cannot remove, item ";
		msg += id;
		msg += "not found in flash list";
		XCEPT_RAISE (xmas::exception::Exception, msg);
	}
}

std::set<std::string> xmas::FlashListDefinition::getTableNames()
{
	std::set<std::string> tnames;
	
	toolbox::net::URN def(this->getProperty("name"));
	tnames.insert(def.getNSS());

	// Sync all tables that belong to this flashlist (flashlist name and inner tables)
	//
	std::vector<xmas::ItemDefinition* > v = this->getItems();

	// Traverse the items and figure out inner tables
	for (std::vector<xmas::ItemDefinition*>::iterator i = v.begin(); i != v.end(); ++i)
	{

		if ((*i)->getProperty("type") == "table")
		{
			tnames.insert((*i)->getProperty("name"));
			this->getTableNames(*i, tnames);
		}
	}
	return tnames;
}

	

void xmas::FlashListDefinition::getTableNames(xmas::ItemDefinition* item, std::set<std::string>& tnames)
{
	// If item has children that are inner tables, add them to the tables set
	std::vector<xmas::ItemDefinition*> v = item->getItems();
	for (std::vector<xmas::ItemDefinition*>::iterator i = v.begin(); i != v.end(); ++i)
	{			
		if ((*i)->getProperty("type") == "table")
		{
			tnames.insert((*i)->getProperty("name"));
			this->getTableNames((*i), tnames);
		}
	}
}


size_t xmas::FlashListDefinition::getNestedDepth()
{
	size_t maxdepth = 0;

	// If item has children that are inner tables, add them to the tables set
	std::vector<xmas::ItemDefinition*> v = this->getItems();
	for (std::vector<xmas::ItemDefinition*>::iterator i = v.begin(); i != v.end(); ++i)
	{			
		if ((*i)->getProperty("type") == "table")
		{
			(*i)->getNestedDepth(0, maxdepth);
		}
	}
	return maxdepth;

}

