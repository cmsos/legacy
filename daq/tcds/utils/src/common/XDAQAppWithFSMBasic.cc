#include "tcds/utils/XDAQAppWithFSMBasic.h"

#include "xdaq/NamespaceURI.h"
#include "xoap/Method.h"

tcds::utils::XDAQAppWithFSMBasic::XDAQAppWithFSMBasic(xdaq::ApplicationStub* const stub,
                                                      std::auto_ptr<tcds::hwlayer::DeviceBase> hw) :
  XDAQAppWithFSMBase(stub, hw)
{
  // These bindings expose the state machine to the outside world.
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "ColdReset",   XDAQ_NS_URI);
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "Configure",   XDAQ_NS_URI);
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "Enable",      XDAQ_NS_URI);
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "Halt",        XDAQ_NS_URI);
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "Pause",       XDAQ_NS_URI);
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "Reconfigure", XDAQ_NS_URI);
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "Resume",      XDAQ_NS_URI);
  xoap::bind<XDAQAppWithFSMBasic>(this, &XDAQAppWithFSMBasic::changeState, "Stop",        XDAQ_NS_URI);
}

tcds::utils::XDAQAppWithFSMBasic::~XDAQAppWithFSMBasic()
{
}
