/*=============================================================================*
 *  Copyright 2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *=============================================================================*/
package org.apache.ws.notification.topics.impl;

import org.apache.ws.notification.base.NotificationProducerResource;
import org.apache.ws.notification.topics.expression.SimpleTopicExpression;
import org.apache.ws.resource.lifetime.ResourceTerminationEvent;
import org.apache.ws.resource.lifetime.ResourceTerminationListener;
import org.apache.ws.resource.lifetime.impl.ResourceTerminationEventImpl;
import org.apache.xmlbeans.XmlObject;
import javax.xml.namespace.QName;

/**
 * A listener for resource termination events that publishes the events to
 * an associated WSN {@link org.apache.ws.pubsub.NotificationProducer} resource.
 */
public class ResourceTerminationListenerImpl
   implements ResourceTerminationListener
{
   /** DOCUMENT_ME */
   public static final String TOPIC_NAME = "ResourceTermination";

   /** DOCUMENT_ME */
   public static final String           NSPREFIX_WSRL      = "wsrl";
   private NotificationProducerResource m_producerResource;
   private SimpleTopicExpression        m_topicExpr;

   /**
    * Create a resource termination topic for the given WSRF namespace set.
    */
   public ResourceTerminationListenerImpl( NotificationProducerResource producerResource )
   {
      m_producerResource    = producerResource;
      m_topicExpr =
         new SimpleTopicExpression( new QName( m_producerResource.getNamespaceSet(  ).getLifetimeXsdNamespace(  ),
                                               ResourceTerminationListenerImpl.TOPIC_NAME,
                                               NSPREFIX_WSRL ) );
   }

   /**
    * DOCUMENT_ME
    *
    * @return DOCUMENT_ME
    */
   public SimpleTopicExpression getTopicExpression(  )
   {
      return m_topicExpr;
   }

   /**
    * DOCUMENT_ME
    *
    * @param event DOCUMENT_ME
    */
   public void terminationOccurred( ResourceTerminationEvent event )
   {
      XmlObject msgXBean    =
         ( (ResourceTerminationEventImpl) event ).getTerminationNotifDocXmlBean( m_producerResource
                                                                                 .getNamespaceSet(  ) );
      try
      {
         m_producerResource.publish( m_topicExpr, msgXBean );
      }
      catch ( Exception e )
      {
         throw new RuntimeException( e );
      }
   }
}