#!/bin/sed -f

# First replace tabs with spaces, in case someone tried those...
s/\t/        /g

# The BC0 settings have now moved to central registers shared by the
# TTC spy and the TTS logger.
s/ttcspy\.logging_control\.bc0_val/sync.bc0_bcommand             /
s/ttcspy\.logging_control\.expected_bc0_bx/sync.bc0_reset_val                    /

# The ttcspy.logging_control.logging_buffer_mode register no longer
# exists.
/ttcspy\.logging_control\.logging_buffer_mode/d

# The other ttcspy.logging_control registers were renamed as
# ttcspy.control.
s/ttcspy\.logging_control\([^ ]*\)\(.*\)/ttcspy.control\1        \2/

# In order to match tts_log, the ttcspy.xxx registers were renamed to
# ttc_spy.xxx.
# NOTE: This is a bit tricky. In order to maintain alignment, one
# space between the register name and the register value is removed,
# but we have to ensure there is at least one space left.
s/ttcspy\([^ ]*\)  \(.*\)/ttc_spy\1 \2/
s/ttcspy\([^ ]*\) \(.*\)/ttc_spy\1 \2/
