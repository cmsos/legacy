/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * ChannelTest.hpp
 *
 *  Created on: Mar 22, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include "mpila/Channel.hpp"

namespace mpila
{

class ChannelTest : public Channel
{
   public:
    ChannelTest(size_t pipelineDepth, WorkRequestType type);
    PendingRequestQueue& getFreeList();
    PendingRequestQueue& getUrgentList();

   protected:
    std::vector<PendingRequest> pendingRequestRefCount_;
    PendingRequestQueue freeList_;
    PendingRequestQueue urgentList_;
};

} /* namespace mpila */
