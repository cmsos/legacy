#!/bin/bash
#-------------------------------------------------------------------------------
# Some useful shortcuts
#-------------------------------------------------------------------------------

prepend() {
  if [ ".${LD_LIBRARY_PATH}" == "." ] ; then
    export LD_LIBRARY_PATH=$1
  else
    export LD_LIBRARY_PATH=$1:${LD_LIBRARY_PATH}
  fi

}
#-------------------------------------------------------------------------------
if test ".$XDAQ_ROOT" = "."; then
  echo "Error: XDAQ_ROOT environment variable not set"
  exit 1
fi

if test ".$XDAQ_PLATFORM" = "."; then
  export XDAQ_PLATFORM=`uname -m`
  if test ".$XDAQ_PLATFORM" != ".x86_64"; then
    export XDAQ_PLATFORM=x86
  fi
  checkos=`$XDAQ_ROOT/config/checkos.sh`
  echo $checkos
  export XDAQ_PLATFORM=$XDAQ_PLATFORM"_"$checkos
  echo "Warning: PLATFORM env. variable not set, guessed to " ${XDAQ_PLATFORM}
fi

if test ".$XDAQ_OS" = "."; then
  export XDAQ_OS=`uname`
  if test ".$XDAQ_OS" = ".Linux"; then
    export XDAQ_OS=linux
  fi
  echo "Warning: OS env. variable not set, guessed to " ${XDAQ_OS}
fi

# Start with a clean slate
# export LD_LIBRARY_PATH="";

# Add the various paths
prepend "$XDAQ_ROOT/lib"

echo Run xdaq with options: $@
echo "LD_LIBRARY_PATH is " ${LD_LIBRARY_PATH}
exec ${XDAQ_ROOT}/bin/xdaq.exe $@

