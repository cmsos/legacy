// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, P. Roberts										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/
#if !defined(XERCESC_INCLUDE_GUARD_CURLURLINPUTSTREAM_HPP)
#define XERCESC_INCLUDE_GUARD_CURLURLINPUTSTREAM_HPP

#include <iostream>

#include <string.h>
#include <curl/curl.h>
#include <curl/multi.h>
#include <curl/easy.h>
#include <list>
#include <xercesc/util/XMLURL.hpp>
#include <xercesc/util/XMLExceptMsgs.hpp>
#include <xercesc/util/Janitor.hpp>
//#include <xercesc/util/BinInputStream.hpp>
//#include <xercesc/util/XMLNetAccessor.hpp>

#include "es/api/CurlNetAccessor.hpp"

#include "es/api/Curl.h"

XERCES_CPP_NAMESPACE_USE
//
// This class implements the BinInputStream interface specified by the XML
// parser.
//
namespace es
{
	namespace api
	{
		class NetHTTPInfo;

		class CurlURLInputStream //: public BinInputStream
		{
			public :
				CurlURLInputStream(const XMLURL&  urlSource, es::api::HTTPMethod fHTTPMethod, std::list<std::string> & headers, const es::api::NetHTTPInfo* httpInfo=0);
				~CurlURLInputStream();

				virtual XMLFilePos curPos() const;
				virtual size_t readBytes
				(
						XMLByte* const  toFill
						, const size_t       maxToRead
				);

				virtual const XMLCh *getContentType() const;

			private :
				// -----------------------------------------------------------------------
				//  Unimplemented constructors and operators
				// -----------------------------------------------------------------------
				CurlURLInputStream(const CurlURLInputStream&);
				CurlURLInputStream& operator=(const CurlURLInputStream&);

				static size_t staticWriteCallback(char *buffer,
						size_t size,
						size_t nitems,
						void *outstream);
				size_t writeCallback(			  char *buffer,
						size_t size,
						size_t nitems);

				static size_t staticReadCallback(char *buffer,
						size_t size,
						size_t nitems,
						void *stream);
				size_t readCallback(             char *buffer,
						size_t size,
						size_t nitems);

				bool readMore(int *runningHandles);

				// -----------------------------------------------------------------------
				//  Private data members
				//
				//  fSocket
				//      The socket representing the connection to the remote file.
				//  fBytesProcessed
				//      Its a rolling count of the number of bytes processed off this
				//      input stream.
				//  fBuffer
				//      Holds the http header, plus the first part of the actual
				//      data.  Filled at the time the stream is opened, data goes
				//      out to user in response to readBytes().
				//  fBufferPos, fBufferEnd
				//      Pointers into fBuffer, showing start and end+1 of content
				//      that readBytes must return.
				// -----------------------------------------------------------------------

				CURLM*				fMulti;
				CURL*				fEasy;

				MemoryManager*      fMemoryManager;

				XMLURL				fURLSource;

				size_t           fTotalBytesRead;
				XMLByte*			fWritePtr;
				size_t           fBytesRead;
				size_t           fBytesToRead;
				bool				fDataAvailable;

				// Overflow buffer for when curl writes more data to us
				// than we've asked for.
				XMLByte				fBuffer[CURL_MAX_WRITE_SIZE];
				XMLByte*			fBufferHeadPtr;
				XMLByte*			fBufferTailPtr;

				// Upload data
				const char*         fPayload;
				size_t           fPayloadLen;

				XMLCh *             fContentType;

		}; // CurlURLInputStream


		inline XMLFilePos
		CurlURLInputStream::curPos() const
		{
			return fTotalBytesRead;
		}
	}
}

#endif // CURLURLINPUTSTREAM_HPP

