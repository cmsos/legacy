package xdaq.tools.win;

import java.io.*;

public interface FileSelectionListener
{
	public void fileSelected (File f);
}
