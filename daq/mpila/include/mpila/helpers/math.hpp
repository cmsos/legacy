/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * math.hpp
 *
 *  Created on: Feb 20, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#pragma once

#include <vector>
#include <numeric>
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <iomanip>

namespace mpila
{

namespace helpers
{
namespace math
{
template <class InputIt>
double E(InputIt begin, InputIt end)
{
    return static_cast<double>(std::accumulate(begin, end, 0)) / static_cast<double>(std::distance(begin, end));
}
template <class InputIt>
double VAR(InputIt begin, InputIt end)
{
    double ex2 = 0;
    for (auto i = begin; i != end; i++)
    {
        ex2 += std::pow(*i, 2);
    }
    ex2 /= static_cast<double>(std::distance(begin, end));
    auto e2x = std::pow(E(begin, end), 2);

    return ex2 - e2x;
}
template <class InputIt>
double STD(InputIt begin, InputIt end)
{
    return std::sqrt(VAR(begin, end));
}
}  // namespace math
}  // namespace helpers
}
