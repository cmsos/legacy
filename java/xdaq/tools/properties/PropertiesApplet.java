package xdaq.tools.properties;

import java.awt.*;
import java.net.*;
import java.security.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import javax.swing.JApplet;

import javax.xml.parsers.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import org.w3c.dom.*;

//-- public class PropertiesApplet extends JFrame implements ActionListener
public class PropertiesApplet extends JApplet implements ActionListener
{
	URL url;

	JPanel informationPane_;
	JPanel buttonPane_;
	JButton refreshButton_;
	JButton applyButton_  ;
	JButton defaultButton_;
	
	JTreeTable parameterTable_;
	
	public void init()
	{	
	
	// For 1.3.1?
	// System.setProperty("javax.xml.parsers.DocumentBuilderFactory","org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
	
	// For 1.4.2?
	System.setProperty("javax.xml.parsers.DocumentBuilderFactory","org.apache.crimson.jaxp.DocumentBuilderFactoryImpl");
		//-- setSize(300,300);
			
		//JTabbedPane tabbed = new JTabbedPane();
		
		// General information tab
		//
		//informationPane_ = new JPanel();
		//tabbed.addTab ("General", informationPane_);
		
		// Parameter tab
		//
		Box parameterBox = Box.createVerticalBox();
		
		parameterTable_ = new JTreeTable();
		
		JScrollPane parameterScrollPane = new JScrollPane ( parameterTable_ );
		//parameterScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		parameterBox.add ( parameterScrollPane );
		
		//tabbed.addTab ("Parameters", parameterBox);
		
		// Parameter Buttons
		//
		Box parameterButtons = Box.createHorizontalBox();
		refreshButton_ = new JButton ("Retrieve");
		refreshButton_.addActionListener (this);
		applyButton_ = new JButton ("Apply");
		applyButton_.addActionListener (this);
		defaultButton_ = new JButton ("Query");
		defaultButton_.addActionListener (this);
		
		parameterButtons.add ( refreshButton_ );
		parameterButtons.add ( applyButton_   );
		parameterButtons.add ( defaultButton_ );
		
		refreshButton_.setEnabled (false);
		applyButton_.setEnabled (false);
		defaultButton_.setEnabled (true);
		
		parameterBox.add ( parameterButtons );
		
		
		this.getContentPane().add (parameterBox, BorderLayout.CENTER);
		
		//this.show();
		this.executeCommand("Query");		
	}
	
	
	void executeCommand(String command )
	{
		String soapCommand = "";

		if ( command.equals("Retrieve") )
		{
			soapCommand = "ParameterGet";
		}
		else if ( command.equals("Apply") )
		{
			soapCommand = "ParameterSet";
		}
		else if ( command.equals("Query") )
		{
			soapCommand = "ParameterQuery";
		}
		
		
		//Example "urn:xdaq-application:lid=#num";
		//
		URL url = null;
		try 
		{
			//java.security.AccessController.checkPermission( new java.net.SocketPermission("lxcmd101:40000", "connect,accept"));
			url = new URL(getParameter("url"));
			
			//url = new URL("http://lxcmd101:40000/urn:xdaq-application:id=0");
		} catch (MalformedURLException mue)
		{
			System.err.println (mue.toString());
			return;
		} catch (java.io.IOException ioe) {
			System.err.println (ioe.toString());
			return;
		}
		
		String urn = url.getPath().substring(1);
		String className = getParameter("className");
		
		//--String urn = "urn:xdaq-application:id=0";
		//--String className = "Executive";
		
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = builder.newDocument();
			Element envelope = document.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "soap-env:Envelope");
			envelope.setAttribute("soap-env:encodingStyle","http://schemas.xmlsoap.org/soap/encoding/");
			envelope.setAttribute("xmlns:soapenc","http://schemas.xmlsoap.org/soap/encoding/");
			envelope.setAttribute("xmlns:soap-env","http://schemas.xmlsoap.org/soap/envelope/");
			envelope.setAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
			envelope.setAttribute("xmlns:xsd","http://www.w3.org/2001/XMLSchema");
			
			document.appendChild(envelope);
			Element body = document.createElementNS("http://schemas.xmlsoap.org/soap/envelope/", "soap-env:Body");
			envelope.appendChild(body);
			
			//System.out.println("Command["+soapCommand+"]");
			
			Element commandElement = document.createElementNS(urn, "xdaq:"+ soapCommand);
			commandElement.setAttribute("xmlns:xdaq","urn:xdaq-soap:3.0");
			body.appendChild(commandElement);

			if ( ! soapCommand.equals("ParameterQuery") ) {
				ParameterTableModel model = this.getModel();
				// Deep export -> export of vector elements
			if ( ! model.exportSelection (commandElement, true,"urn:xdaq-application:"+className, "p" ) )
				return;
			}

			// Connect 			
			// System.err.println ("Connect to " + url.toString());
			
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setUseCaches(false);
			//connection.connect();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			
			connection.setRequestMethod("POST");
			// Send here

			String soapRequest = DOMtoString.serialize(document);
			connection.setRequestProperty("Content-length", Integer.toString(soapRequest.length()));
			connection.setRequestProperty("SOAPAction",urn);
			connection.connect();
			OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
			writer.write(soapRequest);
			writer.flush();
			writer.close();

			// Get the response
			InputStream istream = connection.getInputStream();			
			BufferedReader ireader = new BufferedReader( new InputStreamReader(istream));
			String inputLine;
			String soapResponse = "";
			while ( ( inputLine = ireader.readLine()) != null ) {
				
				soapResponse = soapResponse.concat(inputLine);
			}


			//System.out.println ("Received response\n");
			//System.out.println (soapResponse);
			//System.out.println ("-----------------------------------------------------\n");

			istream.close();

			// Parse response
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder responseBuilder = factory.newDocumentBuilder();
			ByteArrayInputStream inBuffer = new ByteArrayInputStream ( soapResponse.getBytes() );			
			Document responseDoc = responseBuilder.parse ( inBuffer );

			// check for SOAP faults
			//			
			NodeList faultList = responseDoc.getElementsByTagNameNS ("*", "faultstring");
			if (faultList.getLength() == 1)	{
				System.err.println ("Received a SOAP fault");
				System.err.println (faultList.item(0).getFirstChild().getNodeValue());					
			} 
			else {

				NodeList l = responseDoc.getElementsByTagNameNS ("*", soapCommand + "Response");
				if (l.getLength() == 1)
				{
					// find bag
					NodeList parameters = l.item(0).getChildNodes();
					for (int j=0; j < parameters.getLength(); j++ ) 
					{
							//System.out.println("found parameters:"+parameters.item(j).getNodeName());

						if ( parameters.item(j).getLocalName().equals("properties") ) 
						{
							if ( soapCommand.equals("ParameterQuery") )
							{
								this.setModel(new ParameterTableModel(parameters.item(j)));
							}
							else
							{	
								ParameterTableModel model = this.getModel();
								model.update(parameters.item(j));
							}	
							break;
						}
					}
				} 
			}
		} catch (SAXParseException spe) {
			// Error generated by parser
			System.err.println ("Parse error in line " + spe.getLineNumber() + ", uri " + spe.getSystemId());
			System.err.println (spe.getMessage());
			// use contained exception
			Exception x = spe;
			if (spe.getException() != null) {
				x = spe.getException();
			}
			x.printStackTrace();
		} catch (SAXException sxe) {
			// Error during parsing
			Exception x = sxe;
			if (sxe.getException() != null) {
				x = sxe.getException();
			}
			x.printStackTrace();
		} catch (ParserConfigurationException pce) {
			// Parser with specified options can't be built
			pce.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
	
	public void setModel ( ParameterTableModel model )
	{
		parameterTable_.setModel (model);
		applyButton_.setEnabled (true);
		refreshButton_.setEnabled (true);
	}
	
	
	public ParameterTableModel getModel()
	{
		TreeTableModelAdapter adapter = (TreeTableModelAdapter)parameterTable_.getModel();
		return (ParameterTableModel) adapter.getModel();
	}
	
	public void actionPerformed (ActionEvent e)
	{		
		String command = e.getActionCommand();
		this.executeCommand(command);
		
	}
	
	
	//--public static void main(String [] args) 
	//--{
	//--	PropertiesApplet f = new PropertiesApplet();
	//--	f.init();
	//--} 
	
} // end of class
