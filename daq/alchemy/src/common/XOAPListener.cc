// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini					                                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/
#include "XOAPListener.h"

#include <iostream>
#include <string>

#include "toolbox/string.h"
#include "xcept/tools.h"

#include "xoap/domutils.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPFault.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPConstants.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"

#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/Utils.h"

using v8::Context;
using v8::Function;
using v8::FunctionCallbackInfo;
using v8::FunctionTemplate;
using v8::Isolate;
using v8::Local;
using v8::Number;
using v8::Object;
using v8::Persistent;
using v8::String;
using v8::Value;
using v8::Exception;
using v8::Array;
using v8::External;
using v8::Handle;


Persistent<Function> nodejs::soap::Listener::constructor;

nodejs::soap::Listener::Listener():listener_(0)
{
}

nodejs::soap::Listener::~Listener() {
}

void nodejs::soap::Listener::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Prepare constructor template
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, New);
  tpl->SetClassName(String::NewFromUtf8(isolate, "nodejs::soap::Listener"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);

  // Prototype
   NODE_SET_PROTOTYPE_METHOD(tpl, "processIncoming", processIncoming);

  constructor.Reset(isolate, tpl->GetFunction());
  exports->Set(String::NewFromUtf8(isolate, "nodejs::soap::Listener"), tpl->GetFunction());
}

void nodejs::soap::Listener::New(const FunctionCallbackInfo<Value>& args) {

 std::cout << "nodejs::soap::Listener::New" << std::endl;

  Isolate* isolate = args.GetIsolate();

  if (args.IsConstructCall()) {

	  std::cout << "nodejs::soap::Listener::New is ctor" << std::endl;

    // Invoked as constructor: `new Listener(...)`
    //double value = args[0]->IsUndefined() ? 0 : args[0]->NumberValue();
    // Method 2 to set listener
    if (args[0]->IsExternal()) 
    {
	
	v8::Local<v8::External> _listener = v8::Local<v8::External>::Cast(args[0]);
	pt::SOAPListener * listener = static_cast<pt::SOAPListener*>(_listener->Value());

	//std::cout << "received External args for listener of type " << listener->getService()  << std::endl;
	

    } 

	  //https://stackoverflow.com/questions/40539693/how-do-i-use-stdstring-in-a-c-addon-for-node-js
    //xgi::Listener * listener = static_cast<nodejs::soap::Listener*>(args[0]->ToObject());
    nodejs::soap::Listener* obj = new nodejs::soap::Listener();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
	  std::cout << "nodejs::soap::Listener::New done" << std::endl;

  } else {
    // Invoked as plain function `Listener(...)`, turn into construct call.
    const int argc = 1;
    Local<Value> argv[argc] = { args[0] };
    Local<Context> context = isolate->GetCurrentContext();
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    Local<Object> result =  cons->NewInstance(context, argc, argv).ToLocalChecked();
    args.GetReturnValue().Set(result);
  }
}
void nodejs::soap::Listener::NewInstance(const FunctionCallbackInfo<Value>& args, pt::SOAPListener * listener) {
  Isolate* isolate = args.GetIsolate();

  const unsigned argc = 1;
  // MEthod 2 to set listener
  Local<Value> argv[argc];
  argv[0] = v8::External::New(isolate, static_cast<void*>(listener));
  Local<Function> cons = Local<Function>::New(isolate, constructor);
  Local<Context> context = isolate->GetCurrentContext();
  Local<Object> instance = cons->NewInstance(context, argc, argv).ToLocalChecked();

  // MEthod 1 to set listener
  nodejs::soap::Listener* object = ObjectWrap::Unwrap<nodejs::soap::Listener>(instance);
  object->listener_ = listener;

  args.GetReturnValue().Set(instance);
}

void nodejs::soap::Listener::processIncoming(const FunctionCallbackInfo<Value>& args) {
		if (args.Length() > 0) {
	    if (args[0]->IsString()) {
	      String::Utf8Value str(args[0]->ToString());
	      //printf("(cc)>>>> qmn [%s].\n", (const char*)(*str));
	      std::cout << "nodejs::soap::Listener::processIncoming " <<  (const char*)(*str) <<  std::endl;
	    }
	  }

	Isolate* isolate = args.GetIsolate();

	if(args.Length() < 1 || !args[0]->IsObject()) {
		isolate->ThrowException(Exception::TypeError(
				String::NewFromUtf8(isolate, "Error: One object expected")));
		return;
	}

	Local<Context> context = isolate->GetCurrentContext();
	Local<Object> obj = args[0]->ToObject(context).ToLocalChecked();
	Local<Array> props = obj->GetOwnPropertyNames(context).ToLocalChecked();

	for(int i = 0, l = props->Length(); i < l; i++) {
		//Local<Value> localKey = props->Get(i);
		// Local<Value> localVal = obj->Get(context, localKey).ToLocalChecked();
		//std::string key = *String::Utf8Value(localKey);
		//std::string val = *String::Utf8Value(localVal);
		//std::cout << key << ":" << val << std::endl;

	}
	/*

	xgi::Input in(0, 0);
	xgi::Output out;
	 */
	//std::cout << "Environment: " << std::endl;
	//Handle<Object> parameter = Handle<Object>::Cast(args[0]);
	Handle<Value> eh = obj->Get(String::NewFromUtf8(isolate,"headers"));



	Local<Object> headers = Local<Object>::Cast(eh);
	//std::cout << "list all vars: " << std::endl;
	Local<Array> vars = headers->GetOwnPropertyNames(context).ToLocalChecked();
	for(int v = 0, vl = vars->Length(); v < vl; v++) {
		Local<Value> localKey = vars->Get(v);
		Local<Value> localVal = headers->Get(context, localKey).ToLocalChecked();
		std::string key = *String::Utf8Value(localKey);
		std::string val = *String::Utf8Value(localVal);
		std::cout << key << ":" << val << std::endl;
		//in.putenv(key, val);

	}
    return;

	// return same object
	// Local<Object> returnObject =  args[0]->ToObject();
	//args.GetReturnValue().Set(returnObject);
#ifdef CUT

	nodejs::soap::Listener* object = ObjectWrap::Unwrap<nodejs::soap::Listener>(args.Holder());
	// ------------ XOAP -------
	xgi::Output out; // stream for html/soap page reply
	out.str("");
	out.clear();

	xoap::MessageFactory * soapFactoryImpl = 0;

	if  (headers["content-type"].find(xoap::SOAPConstants::SOAP_1_1_CONTENT_TYPE) != std::string::npos)
	{
		soapFactoryImpl = xoap::MessageFactory::getInstance(xoap::SOAPConstants::SOAP_1_1_PROTOCOL);
	}
	else if (headers["content-type"].find(xoap::SOAPConstants::SOAP_1_2_CONTENT_TYPE) != std::string::npos)
	{
		// if soap action present in SOAP 1.2 it is ignored
		soapFactoryImpl = xoap::MessageFactory::getInstance(xoap::SOAPConstants::SOAP_1_2_PROTOCOL);
	}
	else if (headers.find("soapaction") != headers.end())
	{
		soapFactoryImpl = xoap::MessageFactory::getInstance(xoap::SOAPConstants::SOAP_1_1_PROTOCOL);
	}


	xoap::MessageReference reply;

	if (requestBuffer.get() == 0)
	{
		reply = soapFactoryImpl->createMessage();
		xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
		xoap::SOAPFault f = b.addFault();
		if ( soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
		{
			f.setFaultCode ("Server");
			f.setFaultString ("Empty SOAP message");
		}
		else
		{
			xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
			f.setFaultCode (faultCodeQName);
			f.addFaultReasonText("Empty SOAP message",std::locale("en_US"));
		}
		reply->writeTo (out); // write SOAP contents to a buffer
	}
	else if (object->listener_ == 0)
	{
		LOG4CPLUS_FATAL(logger_, "No listener for HTTP/SOAP available, message discarded");

		reply = soapFactoryImpl->createMessage();
		xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
		xoap::SOAPFault f = b.addFault();
		if ( soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
		{
			f.setFaultCode ("Server");
			f.setFaultString ("No listener for HTTP/SOAP available, message discarded");
		}
		else
		{
			xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
			f.setFaultCode (faultCodeQName);
			f.addFaultReasonText("No listener for HTTP/SOAP available, message discarded",std::locale("en_US"));

		}
		reply->writeTo (out); // write SOAP contents to a buffer
	}
	else
	{

		try
		{
			xoap::MessageReference msg = soapFactoryImpl->createMessage(requestBuffer.get(), requestSize);
			// Extract urn from http header and pass it together with the SOAP message
			// msg->getMimeHeaders()->addHeader("Content-Location", urn);

			// Add all mime headers to the xoap message
			xoap::MimeHeaders* mimeHeaders = msg->getMimeHeaders();
			std::map<std::string,std::string, std::less<std::string> >::iterator hi;
			for (hi = headers.begin(); hi != headers.end(); ++hi)
			{
				mimeHeaders->addHeader( (*hi).first, (*hi).second );
			}

			reply = object->listener_->->processIncomingMessage( msg );
		}
		catch (xoap::exception::Exception & soe)
		{
			reply = soapFactoryImpl->createMessage();
			xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
			xoap::SOAPFault f = b.addFault();
			if ( soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
			{
				f.setFaultCode ("Server");
				f.setFaultString (xcept::stdformat_exception_history(soe));
			}
			else
			{
				xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
				f.setFaultCode (faultCodeQName);
				f.addFaultReasonText(xcept::stdformat_exception_history(soe), std::locale("en_US"));

			}
		}
		catch (pt::exception::Exception & pte)
		{
			reply = soapFactoryImpl->createMessage();
			xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
			xoap::SOAPFault f = b.addFault();
			if ( soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
			{
				f.setFaultCode ("Server");
				f.setFaultString (xcept::stdformat_exception_history(pte));
			}
			else
			{
				xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
				f.setFaultCode (faultCodeQName);
				f.addFaultReasonText(xcept::stdformat_exception_history(pte), std::locale("en_US"));

			}

		}
		catch (...)
		{
			reply = soapFactoryImpl->createMessage();
			xoap::SOAPBody b = reply->getSOAPPart().getEnvelope().getBody();
			xoap::SOAPFault f = b.addFault();
			if ( soapFactoryImpl->getProtocolVersion() == xoap::SOAPConstants::SOAP_1_1_PROTOCOL )
			{
				f.setFaultCode ("Server");
				f.setFaultString ("Caught unknown exception while processing incoming SOAP message");
			}
			else
			{
				xoap::SOAPName faultCodeQName ( "Receiver", soapFactoryImpl->getEnvelopePrefix(), xoap::SOAPConstants::URI_NS_SOAP_1_2_ENVELOPE);
				f.setFaultCode (faultCodeQName);
				f.addFaultReasonText("Caught unknown exception while processing incoming SOAP message", std::locale("en_US"));

			}

		}

		// serialize the SOAP reply message into the reply text buffer
		// reply->writeTo (replyStringBuffer);

		if (reply->countAttachments() > 0)
		{
			//std::string contentType = "multipart/related; type=\"text/xml\"; boundary=\"";
			std::string contentType = "multipart/related; type=\"" + soapFactoryImpl->getMediaType() + "\"; boundary=\"";
			contentType += reply->getMimeBoundary();
			contentType += "\"";
			out.getHTTPResponseHeader().addHeader("Content-Type", contentType );
			out.getHTTPResponseHeader().addHeader("Content-Description", "SOAP Message with attachments");
		}
		else
		{
			out.getHTTPResponseHeader().addHeader("Content-Type", soapFactoryImpl->getMediaType());
			out.getHTTPResponseHeader().addHeader("Content-Description", "SOAP Message");
		}

		std::map<std::string,std::string, std::less<std::string> >::iterator hi;
		for (hi = headers_.begin(); hi != headers_.end(); ++hi)
		{
			out.getHTTPResponseHeader().addHeader( (*hi).first, (*hi).second );
		}

		reply->writeTo (out); // write SOAP contents to a buffer


		Local<Object> xgiout = Object::New(isolate);
		Local<Array> xgiheaders = Array::New(isolate);


		cgicc::HTTPResponseHeader header =  out.getHTTPResponseHeader();
		std::vector< std::string > headers =  header.getHeaders();
		int j = 0;
		for  ( std::vector<std::string>::iterator i = headers.begin(); i != headers.end(); i++  )
		{
				xgiheaders->Set(j++, String::NewFromUtf8(isolate, (*i).c_str()));
		}

		const std::string &  version =	header.getHTTPVersion();
		int status  = 	header.getStatusCode ();
		std::string reason = header.getReasonPhrase ();


		xgiout->Set(String::NewFromUtf8(isolate, "data"), String::NewFromUtf8(isolate, out.str().c_str()));
		xgiout->Set(String::NewFromUtf8(isolate, "version"), String::NewFromUtf8(isolate, version.c_str()));
		xgiout->Set(String::NewFromUtf8(isolate, "reason"), String::NewFromUtf8(isolate, reason.c_str()));
		xgiout->Set(String::NewFromUtf8(isolate, "status"), Number::New(isolate, status) );
		xgiout->Set(String::NewFromUtf8(isolate, "headers"), xgiheaders);


		args.GetReturnValue().Set(xgiout);

	}
#endif
}

/*

readableStream.on('data', function(chunk) {
  console.log(chunk);
  var size = chunk.length;//<---Size of chunk, thanks Matt
  console.log(size);
  var obj1 = addon.buffering(chunk,size);
  console.log('pass chunk');
});
Addon:

void buffering(const FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  int size = args[1]->NumberValue();

  Local<Object> bufferObj = args[0]->ToObject();
  char *buf = node::Buffer::Data(bufferObj);
  if (i == 0){ fp = fopen("copy.wav", "wb"); i++;}

  fwrite(buf, 1, size, fp);
  fflush(fp);
  if (i == 3){ fclose(fp); }
  i++;
  Local<String> devolve = String::NewFromUtf8(isolate,"buffering_sucess");//C++--->JS
  args.GetReturnValue().Set(devolve);
}
I will remove the iterator, to some "generic" solution. It is here because I now the number of iterations that will occur.

*/

