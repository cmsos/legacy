#include "toolbox/regex.h"
#include <string>
#include <vector>

using namespace std;
using namespace toolbox;

void DoRegexpMatch()
{
	vector<string> m ;
	bool match = toolbox::regx_match("controlLed_HDz_P_Z2_Z4_P3", "ledHD_([a-zA-Z]+)", m);
	if(!match)
		std::cout << "Unmatched true" << std::endl;
	else
		std::cout << "Unmatched false" << std::endl;
	bool match2 = toolbox::regx_match("controlLed_HD_P_Z2_Z4_P3", "controlLed_HD_([a-zA-Z])_Z([0-9])_Z([0-9])_P([0-9])", m);
	if(match2)
		std::cout << "Matched   true" << std::endl;
	else
		std::cout << "Matched   false" << std::endl;
}

int main(int argc, char * argv[])
{
	DoRegexpMatch();
	DoRegexpMatch();
	DoRegexpMatch();
	DoRegexpMatch();
	DoRegexpMatch();
	DoRegexpMatch();
	DoRegexpMatch();
	DoRegexpMatch();
	DoRegexpMatch();

	return(0);
}

