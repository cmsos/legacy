#include "tcds/pi/Utils.h"

#include <algorithm>
#include <climits>
#include <stdint.h>
#include <vector>

// NOTE: This stores the bits in the 'reverse' order from what is
// normally expected. This allows much simpler use of iterators
// (instead of reverse iterators) though.
template <typename T>
std::vector<bool>
tcds::pi::intToVecBool(T const input)
{
  std::vector<bool> res;
  T tmp = input;
  for (size_t i = 0; i < sizeof(T) * CHAR_BIT; ++i)
    {
      res.push_back(tmp & T(1));
      tmp >>= 1;
    }
  return res;
}

// NOTE: See the note at the top of intToVecBool().
template <typename T>
T
tcds::pi::vecBoolToUInt(std::vector<bool> const input)
{
  T res = 0;
  T mask = 1;
  for (std::vector<bool>::const_iterator it = input.begin();
       it != input.end();
       ++it)
    {
      if (*it == true)
        {
          res |= mask;
        }
      mask <<= 1;
    }
  return res;
}
