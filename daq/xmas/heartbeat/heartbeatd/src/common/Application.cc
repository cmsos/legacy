// $Id$

/*************************************************************************
 * XDAQ XMAS Heartbeat Daemon              								 *
 * Copyright (C) 2000-2014, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#include <sstream>

#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/stl.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 

#include "pt/PeerTransportAgent.h"
#include "xdaq/ApplicationGroup.h" 
#include "xdaq/ApplicationRegistry.h" 

#include "xgi/Table.h" 
#include "xgi/framework/Method.h" 
#include "xcept/tools.h"

#include "xmas/heartbeat/heartbeatd/Application.h"

#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/utils.h"
#include "xdata/exdr/Serializer.h"
#include "toolbox/Runtime.h"
#include "xmas/xmas.h"
#include "xmas/heartbeat/heartbeatd/exception/Exception.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"
#include "xdata/Double.h"
#include "xdaq/ApplicationDescriptorImpl.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/XceptSerializer.h"
#include "xdaq/EndpointAvailableEvent.h"

#include "toolbox/TimeVal.h"
#include "toolbox/stl.h"
#include "toolbox/regex.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"

#include "xoap/DOMParserFactory.h"

#include "xercesc/util/PlatformUtils.hpp"
#include "xercesc/util/XMLNetAccessor.hpp"
#include "xercesc/util/BinInputStream.hpp"
#include "xercesc/util/XMLURL.hpp"

XDAQ_INSTANTIATOR_IMPL (xmas::heartbeat::heartbeatd::Application);

XERCES_CPP_NAMESPACE_USE

xmas::heartbeat::heartbeatd::Application::Application (xdaq::ApplicationStub* s) 
	: xdaq::Application(s), xgi::framework::UIManager(this)
{

	heartbeatInternalLossCounter_ = 0;
	heartbeatEnqueuedCounter_ = 0;
	heartbeatNoNetworkLossCounter_ = 0;
	heartbeatIncomingCounter_ = 0;

	dialup_ = 0;

	brokerDialupWatchdog_ = "PT30S";
	eventingURL_ = "";

	this->getApplicationInfoSpace()->fireItemAvailable("brokerDialupWatchdog", &brokerDialupWatchdog_);
	this->getApplicationInfoSpace()->fireItemAvailable("eventingURL", &eventingURL_);

	b2in::nub::bind(this, &xmas::heartbeat::heartbeatd::Application::onMessage);

	publishGroup_ = "eventing";

	std::srand((unsigned) time(0));

	s->getDescriptor()->setAttribute("icon", "/xmas/heartbeat/heartbeatd/images/heartbeatd-icon.png");
	s->getDescriptor()->setAttribute("icon16x16", "/xmas/heartbeat/heartbeatd/images/heartbeatd-icon.png");

	this->getApplicationInfoSpace()->fireItemAvailable("publishGroup", &publishGroup_);

	brokerGroup_ = "";
	brokerURL_ = "";
	this->getApplicationInfoSpace()->fireItemAvailable("brokerURL", &brokerURL_);
	this->getApplicationInfoSpace()->fireItemAvailable("brokerGroup", &brokerGroup_);

	// heartbeat report interval
	//heartbeatWatchdog_ = "PT30S";
	//this->getApplicationInfoSpace()->fireItemAvailable("heartbeatWatchdog", &heartbeatWatchdog_);

	// bind HTTP callbacks
	xgi::framework::deferredbind(this, this, &xmas::heartbeat::heartbeatd::Application::Default, "Default");

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	// Listen to applications instantiation events
	this->getApplicationContext()->addActionListener(this);

}

xmas::heartbeat::heartbeatd::Application::~Application ()
{

}

void xmas::heartbeat::heartbeatd::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	std::string timerTaskName = e.getTimerTask()->name;
	if (timerTaskName == "heartbeatd:scan")
	{
		// was self heartbeat
	}
}

void xmas::heartbeat::heartbeatd::Application::publishHeartbeat (toolbox::mem::Reference * msg, xdata::Properties & plist)
{
	heartbeatIncomingCounter_++;

	if (dialup_ == 0)
	{
		// heart beat lost, increment counter here TBD
		heartbeatNoNetworkLossCounter_++;
		return;
	}

	// override properties to forward message
	plist.setProperty("urn:b2in-protocol:service", "b2in-eventing");
	plist.setProperty("urn:b2in-eventing:action", "notify");
	plist.setProperty("urn:b2in-eventing:topic", "heartbeat");

	try
	{
		dialup_->send(0, plist);
		heartbeatEnqueuedCounter_++;
	}
	catch (b2in::utils::exception::Exception & e)
	{
		// heart beat lost, increment counter here TBD
		heartbeatInternalLossCounter_++;
		return;
	}
}

void xmas::heartbeat::heartbeatd::Application::onMessage (toolbox::mem::Reference * msg, xdata::Properties & plist) 
{

	if (plist.getProperty("urn:b2in-protocol:action") == "heartbeat")
	{
		this->publishHeartbeat(0, plist);
		if (msg != 0) msg->release();
	}
	else
	{
		std::string action = plist.getProperty("urn:xmasbroker2g:action");
		if (action == "allocate")
		{
			std::string model = plist.getProperty("urn:xmasbroker2g:model");
			if (model == "eventing" && dialup_ != 0)
			{
				dialup_->onMessage(msg, plist);
			}
			else
			{
				LOG4CPLUS_INFO(this->getApplicationLogger(), "unknown broker response: " << model);
			}
		}
		if (msg != 0) msg->release();
	}
}

void xmas::heartbeat::heartbeatd::Application::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		/*
		toolbox::task::Timer * timer = 0;

		if (!toolbox::task::getTimerFactory()->hasTimer("urn:xmas:sensord-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:xmas:sensord-timer");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:xmas:sensord-timer");
		}


		toolbox::TimeInterval interval;

		int delay = (rand() % 14) + 1;
		toolbox::TimeVal start(toolbox::TimeVal::gettimeofday().sec() + delay);
		interval.fromString(heartbeatWatchdog_);

		timer->scheduleAtFixedRate(start, this, interval, 0, "heartbeatd:scan");
		*/
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(xmas::heartbeat::heartbeatd::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}

void xmas::heartbeat::heartbeatd::Application::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "xdaq::EndpointAvailableEvent")
	{
		toolbox::TimeInterval intervalDialup;
		try
		{
			intervalDialup.fromString(brokerDialupWatchdog_.toString());
		}
		catch (toolbox::exception::Exception& e)
		{
			std::stringstream msg;
			msg << "Failed to parse the watchdog brokerDialupWatchdog parameter '" << brokerDialupWatchdog_.toString() << "', ";
			XCEPT_DECLARE_NESTED(xmas::heartbeat::heartbeatd::exception::Exception, q, msg.str(), e);
			this->notifyQualified("fatal", q);
		}

		std::string networkName = this->getApplicationDescriptor()->getAttribute("network");

		xdaq::EndpointAvailableEvent& ie = dynamic_cast<xdaq::EndpointAvailableEvent&>(event);
		const xdaq::Network* network = ie.getNetwork();

		LOG4CPLUS_INFO(this->getApplicationLogger(), "Received endpoint available event for network " << network->getName());

		if (network->getName() == networkName)
		{

			dialup_ = new b2in::utils::EventingDialup(this, publishGroup_, brokerURL_, eventingURL_, networkName, intervalDialup);
		}

	}
}

//------------------------------------------------------------------------------------------------------------------------
// CGI
//------------------------------------------------------------------------------------------------------------------------
void xmas::heartbeat::heartbeatd::Application::TabPanel (xgi::Output * out)
{

	*out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

	// Tabbed pages

	*out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
	this->StatisticsTabPage(out);
	*out << "</div>";

	*out << "</div>";
}

void xmas::heartbeat::heartbeatd::Application::StatisticsTabPage (xgi::Output * out)
{
	//Dialup

	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Eventing Dialup");
	*out << cgicc::tbody() << std::endl;

	// State
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "State";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	if (dialup_ != 0)
	{
		*out << dialup_->getStateName();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Broker Lost counter
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Broker messages lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getBrokerLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Eventing URL
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Eventing address";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getEventingURL();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports sent
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total outgoing reports";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	// Reports lost
	//
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Total reports lost";
	*out << cgicc::th();
	*out << cgicc::td();
	if (dialup_ != 0)
	{
		*out << dialup_->getOutgoingReportLostCounter();
	}
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

	std::string url = "/";
	url += getApplicationDescriptor()->getURN();

	// Heartbeat	
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Heartbeats");
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Received";
	*out << cgicc::th();
	*out << cgicc::td().set("style", "min-width: 100px;");
	*out << heartbeatIncomingCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "No network loss";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << heartbeatNoNetworkLossCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Enqueued";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << heartbeatEnqueuedCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Internal loss";
	*out << cgicc::th();
	*out << cgicc::td();
	*out << heartbeatInternalLossCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table() << std::endl;

}

void xmas::heartbeat::heartbeatd::Application::Default (xgi::Input * in, xgi::Output * out) 
{

	this->TabPanel(out);

}
