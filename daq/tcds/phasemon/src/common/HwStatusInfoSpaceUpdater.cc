#include "tcds/phasemon/HwStatusInfoSpaceUpdater.h"

#include "tcds/phasemon/TCADevicePhaseMon.h"

tcds::phasemon::HwStatusInfoSpaceUpdater::HwStatusInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                   tcds::phasemon::TCADevicePhaseMon const& hw) :
  tcds::hwutilstca::HwStatusInfoSpaceUpdaterTCA(xdaqApp, hw),
  hw_(hw)
{
}

tcds::phasemon::HwStatusInfoSpaceUpdater::~HwStatusInfoSpaceUpdater()
{
}

tcds::phasemon::TCADevicePhaseMon const&
tcds::phasemon::HwStatusInfoSpaceUpdater::getHw() const
{
  return hw_;
}
