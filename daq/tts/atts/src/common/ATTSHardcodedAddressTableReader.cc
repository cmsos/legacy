/**
*      @file ATTSHardcodedAddressTableReader.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.3 $
*     $Date: 2007/03/27 07:53:25 $
*
*
**/
#include "tts/atts/ATTSHardcodedAddressTableReader.hh"

tts::ATTSHardcodedAddressTableReader::ATTSHardcodedAddressTableReader() 
  : HAL::PCIAddressTableDynamicReader() {

  // ******************************************************************************************************************
  // * ATTS Interface Card register definitions $Revision: 1.3 $
  // * Vendor ID : 0xECD6
  // * Device ID : 0xa115
  // *****************************************************************************************************************
  // *  key				Access		BAR	Offset		mask		read	write	description
  // *****************************************************************************************************************
  // * 
  createItem("bar0", 				HAL::CONFIGURATION, 0, 0x00000010, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("bar1", 				HAL::CONFIGURATION, 0, 0x00000014, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("GeoSlot",                          HAL::CONFIGURATION, 0, 0x00000050, 0x000000ff, 1, 0, "The geographic slot of the aTTS (0..20)");
  createItem("sn_a", 				HAL::CONFIGURATION, 0, 0x0000005c, 0xFFFFFFFF, 1, 0, "serial number a (8 digit)");
  createItem("sn_b", 				HAL::CONFIGURATION, 0, 0x00000060, 0xFFFFFFFF, 1, 0, "serial number b (8 digit)");
  createItem("sn_c", 				HAL::CONFIGURATION, 0, 0x00000064, 0x00000FFF, 1, 0, "serial number c (3 digit)");
  // *
  // *
  // *
  createItem("REPROG_XILINX", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000004, 0, 1, "reprogram bit for Xilinx reconfiguration (toggle to 1, wait, toggle to 0)");
  createItem("REPROG_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 0, 1, "reprogram bit for Xilinx reconfiguration (toggle to 1, wait, toggle to 0)");
  // 
  createItem("JTAG_ENABLE", 		        HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 0, 1, "enable bit for JTAG");
  // 
  createItem("JTAG_TDI_TMS_CLK", 		HAL::CONFIGURATION, 0, 0x00000044, 0x0000007f, 0, 1, "[6:0] 6:TDI 1:TMS 0:CLK bits sent to JTAG chain");
  createItem("JTAG_TDO", 		        HAL::CONFIGURATION, 0, 0x00000044, 0x00000080, 1, 0, "TDO bit read from JTAG chain");
  createItem("FWID_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000048, 0xFFFFFFFF, 1, 0, "firmware ID ALTERA");
  // 
  // *
  // * Register definition
  // *
  createItem("CNTL", 				HAL::MEMORY, 0, 0x00000100, 0xFFFFFFFF, 1, 1, "Control register, 32 bits are used");
  createItem("STAT", 				HAL::MEMORY, 0, 0x00000200, 0xFFFFFFFF, 1, 0, "Status register, 17 bits are used");
  createItem("Test_in_ready", 			HAL::MEMORY, 0, 0x00000300, 0x000FFFFF, 1, 1, "Test register for ready(19:0), 20 bits");
  createItem("Test_in_busy", 			HAL::MEMORY, 0, 0x00000304, 0x000FFFFF, 1, 1, "Test register for busy(19:0), 20 bits");
  createItem("Test_in_synch", 			HAL::MEMORY, 0, 0x00000308, 0x000FFFFF, 1, 1, "Test register for out_of_synch(19:0), 20 bits");
  createItem("Test_in_overflow", 		HAL::MEMORY, 0, 0x0000030c, 0x000FFFFF, 1, 1, "Test register for overflow(19:0), 20 bits");
  createItem("Update_test_reg", 			HAL::MEMORY, 0, 0x00000310, 0x00000001, 0, 1, "a write updates the permanent output of the Test reg (80 bits)");
  // *MASK				memory		0	00000400	000FFFFF	1	1	When 1, the corresponding FED input is masked , 20 bits
  // *ZBT_WR_ADD			memory		0	00000500	0001FFFF	1	1	History address register/pointer, 19 bits
  // *TRT_MISS			memory		0	00000600	FFFFFFFF	1	0	Transition miss counter, 32 bits
  // *Synch_thres			memory		0	00000700	0000FF1F	1	1	Synch threshold register
  // *Synch_thres_20			memory		0	00000700	0000001F	1	1	Synch threshold register, 5 bits [20->1 mode]
  // *Synch_thres_10A			memory		0	00000700	00000F00	1	1	Synch threshold register, 4 bits [10->1 mode, channel 0..9]
  // *Synch_thres_10B     		memory		0	00000700	0000F000	1	1	Synch threshold register, 4 bits [10->1 mode, channel 10..19]
  createItem("ID", 				HAL::MEMORY, 0, 0x00000800, 0xFFFFFFFF, 1, 0, "Identification register, holds version number, etc...");
  // *HW_MON				memory		0	00000900	FFFFFFFF	1	0	Hardware dead time monitors (40 locations of 32 bits)
  // *HW_MON_last			memory		0	0000099C	FFFFFFFF	1	0	Hardware dead time monitors (40 locations of 32 bits)
  // 
  createItem("Output0",                          HAL::MEMORY, 0, 0x00000a00, 0x0000000F, 1, 1, "test output 0 (IO12)");
  createItem("Output1",                          HAL::MEMORY, 0, 0x00000a04, 0x0000000F, 1, 1, "test output 1 (IO13)");
  createItem("Output2",                          HAL::MEMORY, 0, 0x00000a08, 0x0000000F, 1, 1, "test output 2 (IO14)");
  createItem("Output3",                          HAL::MEMORY, 0, 0x00000a0C, 0x0000000F, 1, 1, "test output 3 (IO15)");
  createItem("Output4",                          HAL::MEMORY, 0, 0x00000a10, 0x0000000F, 1, 1, "test output 4 (IO16)");
  createItem("Output5",                          HAL::MEMORY, 0, 0x00000a14, 0x0000000F, 1, 1, "test output 5 (IO17)");
  createItem("Output6",                          HAL::MEMORY, 0, 0x00000a18, 0x0000000F, 1, 1, "test output 6 (IO18)");
  createItem("Output7",                          HAL::MEMORY, 0, 0x00000a1C, 0x0000000F, 1, 1, "test output 7 (IO19)");
  createItem("Output8",                          HAL::MEMORY, 0, 0x00000a20, 0x0000000F, 1, 1, "test output 8 (IO20)");
  createItem("Output9",                          HAL::MEMORY, 0, 0x00000a24, 0x0000000F, 1, 1, "test output 9 (IO21)");
  createItem("Output10",                         HAL::MEMORY, 0, 0x00000a28, 0x0000000F, 1, 1, "test output 10 (IO22)");
  createItem("Output11",                         HAL::MEMORY, 0, 0x00000a2C, 0x0000000F, 1, 1, "test output 11 (IO23)");
  createItem("Input_ready", 			HAL::MEMORY, 0, 0x00000b00, 0x000FFFFF, 1, 0, "Spy register for ready(11:0), 12 bits");
  createItem("Input_busy", 			HAL::MEMORY, 0, 0x00000b04, 0x000FFFFF, 1, 0, "Spy register for busy(11:0), 12 bits");
  createItem("Input_synch", 			HAL::MEMORY, 0, 0x00000b08, 0x000FFFFF, 1, 0, "Spy register for out_of_synch(11:0), 12 bits");
  createItem("Input_overflow",    		HAL::MEMORY, 0, 0x00000b0c, 0x000FFFFF, 1, 0, "Spy register for overflow(11:0), 12 bits");
  // *ZBT				memory		1	00000000	FFFFFFFF	1	1	2 MB of contiguous memory (max. is base + 0x1F_FFFC)
  // *ZBT_max				memory		1	001ffffc	FFFFFFFF	1	1	Last address of ZBT memory
  // *
  // *
  // *
  // *
  // *
  // 
  // * Field definition for Control reg.
  // *
  createItem("reset_all",                 	HAL::MEMORY, 0, 0x00000100, 0x0000007F, 1, 1, "resets everything in FPGA");
  createItem("reset_fifos", 			HAL::MEMORY, 0, 0x00000100, 0x00000001, 1, 1, "resets all FIFOs (derand + Altera)");
  createItem("reset_hw_mons", 			HAL::MEMORY, 0, 0x00000100, 0x00000002, 1, 1, "resets all hardware monitors");
  createItem("reset_transdet", 			HAL::MEMORY, 0, 0x00000100, 0x00000004, 1, 1, "resets transition detector");
  createItem("reset_time_tag", 			HAL::MEMORY, 0, 0x00000100, 0x00000008, 1, 1, "resets time counter (40 bits)");
  createItem("reset_zbt_add", 			HAL::MEMORY, 0, 0x00000100, 0x00000010, 1, 1, "resets history memory address generator");
  createItem("reset_trt_miss", 			HAL::MEMORY, 0, 0x00000100, 0x00000020, 1, 1, "resets transtion miss counter");
  createItem("reset_fifo_sm", 			HAL::MEMORY, 0, 0x00000100, 0x00000040, 1, 1, "resets fifo managment state machine");
  // *enable_timetag_reset		memory		0	00000100	00000100	1	1	enable time tag reset from backplane
  createItem("leds", 				HAL::MEMORY, 0, 0x00000100, 0x00000e00, 1, 1, "control of the leds L2 to L4 on front panel");
  createItem("led2", 				HAL::MEMORY, 0, 0x00000100, 0x00000200, 1, 1, "control of LED L2 on front panel");
  createItem("led3", 				HAL::MEMORY, 0, 0x00000100, 0x00000400, 1, 1, "control of LED L3 on front panel");
  createItem("led4", 				HAL::MEMORY, 0, 0x00000100, 0x00000800, 1, 1, "control of LED L4 on front panel");
  // *TestOutput_enable		memory		0	00000100	0000F000	1	1	switch outputs (IO20 to IO23) to test mode
  // *TestOutput_enable_IO20		memory		0	00000100	00001000	1	1	switch outputs IO20 to test mode
  // *TestOutput_enable_IO21		memory		0	00000100	00002000	1	1	switch outputs IO21 to test mode
  // *TestOutput_enable_IO22		memory		0	00000100	00004000	1	1	switch outputs IO22 to test mode
  // *TestOutput_enable_IO23		memory		0	00000100	00008000	1	1	switch outputs IO23 to test mode
  createItem("Test_select", 			HAL::MEMORY, 0, 0x00000100, 0x00010000, 1, 1, "switch between FED inputs and test inputs");
  // *History_mode			memory		0	00000100	00020000	1	1	switch between ZBT and Altera DMA for history
  // *Dual_mode			memory		0	00000100	80000000	1	1	switch to dual FMM mode. 
  // *
  // *
  // *
  // * Field definition for Status reg.
  // *
  createItem("PLL_int", 				HAL::MEMORY, 0, 0x00000200, 0x00000001, 1, 0, "Status of PLL for internal clock, must be at 1");
  createItem("PLL_ext", 				HAL::MEMORY, 0, 0x00000200, 0x00000002, 1, 0, "Status of PLL for external clock, must be at 1");
  // *DFifo_empty			memory		0	00000200	00000004	1	0	When equal to 1, derand. fifos are empty
  // *DFifo_full			memory		0	00000200	00000008	1	0	When equal to 1, derand. fifos are full
  // *AFifo_empty			memory		0	00000200	00000010	1	0	When equal to 1, Altera fifo is empty
  // *AFifo_full			memory		0	00000200	00000020	1	0	When equal to 1, Altera fifo is full
  // *
  // *
  // *
  // * Field definition for ID reg.
  // *
  createItem("ID_year", 				HAL::MEMORY, 0, 0x00000800, 0xFF000000, 1, 0, "year since 2000 BCD");
  createItem("ID_month", 			HAL::MEMORY, 0, 0x00000800, 0x00FF0000, 1, 0, "month (1..12) BCD");
  createItem("ID_day", 				HAL::MEMORY, 0, 0x00000800, 0x0000FF00, 1, 0, "day (1..31) BCD ");
  createItem("ID_rev_in_day", 			HAL::MEMORY, 0, 0x00000800, 0x000000FF, 1, 0, "revision in this day (0..99) BCD");
  // *****************************************************************************************************************
}
