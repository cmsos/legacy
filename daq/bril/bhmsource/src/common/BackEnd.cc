#include <algorithm>

#include "xdaq/Application.h"

#include "toolbox/utils.h"

#include "uhal/log/exception.hpp"
#include "uhal/ConnectionManager.hpp"
#include "uhal/HwInterface.hpp"

#include "bril/bhmsource/exception/Exception.h"

#include "bril/bhmsource/BackEnd.h"

namespace
 {

  struct SafeLocker  // Exception safe RAII style mutex
   {
    SafeLocker( toolbox::BSem& lock ) : m_lock( lock ) { m_lock.take(); }
    ~SafeLocker() { m_lock.give(); }
    void unlockedwait( long us ) { m_lock.give(); toolbox::u_sleep( us ); m_lock.take(); }
    toolbox::BSem& m_lock;
   };

 }

namespace bril
 {

  namespace bhmsource
   {

    Uhtr::Uhtr( Configuration& cfg, xdaq::Application* app ) :
     m_lock( toolbox::BSem::FULL ),
     m_uhtr( 0 ),
     r_cfg( cfg ),
     m_app( app )
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      std::string addrtable = "file:///opt/xdaq/ipbus/hcal/uHTR.xml";
      std::string uhtrname( "bhm_uhtr_beam " );
      uhtrname += r_cfg.beam.toString();
      LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Connecting to uHTR at " << r_cfg.uhtr_uri.toString() );
      try
       {
        SafeLocker locker( m_lock );
        uhal::HwInterface hw = uhal::ConnectionManager::getDevice( uhtrname, r_cfg.uhtr_uri, addrtable );
        m_uhtr = new hcal::uhtr::uHTR( uhtrname, hw );
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
     }

    Uhtr::~Uhtr()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      delete m_uhtr;
     }

/**
 * This function Initializes the hardware. It also resets the links.
 */
    void Uhtr::reset()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      try
       {
        SafeLocker locker( m_lock );
        uint8_t flavor, major, minor, patch;
        m_uhtr->firmwareRevision( true, flavor, major, minor, patch );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Front FPGA fw " << ( flavor == hcal::uhtr::uHTR::FL_BHM ? "BHM v" : "non BHM v" ) << int( major ) << '.' << int( minor ) << '.' << int( patch ) );
        m_uhtr->firmwareRevision( false, flavor, major, minor, patch );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Back FPGA fw " << ( flavor == hcal::uhtr::uHTR::FL_BHM ? "BHM v" : "non BHM v" ) << int( major ) << '.' << int( minor ) << '.' << int( patch  ) );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": PCB revision " << m_uhtr->pcbRevision() << " - Serial #" << m_uhtr->serialNumber() );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Resetting Lumi links" );
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Initializing clocks with 5.0 Gbps async scheme" );
        m_uhtr->setupClock( 1, 320 ); //copied from uHTRtool.cc
        m_uhtr->setupClock( 2, 250 );
        toolbox::u_sleep( 500000 ); //settle clocks
        //These parameters need not be runtime configurable.
        uint32_t orbitdelay = 34; //not to be confused with orbit phase; this is a parameter for the 5 Gbps links.
        bool auto_align = true;
        bool enable_onthefly = false;
        bool enable_cdr_autoreset = true;
        bool enable_gtx_autoreset = true;
        bool use_qiereset_asmark = false;
        bool do_resets = false;
        bool text_info = false;
        LOG4CPLUS_INFO( m_app->getApplicationLogger(), __func__ << ": Initializing links ( orbitdelay=" << orbitdelay << ", autorealign=" << auto_align << ", OTFalign=" << enable_onthefly << ", cdrrst=" << enable_cdr_autoreset << ", gtxrst=" << enable_gtx_autoreset << ", qiemark=" << use_qiereset_asmark << ", doresets=" << do_resets << " )" );
        m_uhtr->link_init_setup( orbitdelay, auto_align, enable_onthefly, enable_cdr_autoreset, enable_gtx_autoreset, use_qiereset_asmark ) ;
        m_uhtr->link_init( do_resets, text_info ) ;
        toolbox::u_sleep( 500000 ); //settle links
        m_uhtr->lumi_link_reset();
        toolbox::u_sleep( 500000 ); //settle links
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
     }

/**
 * This function runs all possible diagnostics. Making no attempt to be clever, if it finds one problem it returns false, in which case re-init should be called
 */
    bool Uhtr::sanityCheck()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      try
       {
        SafeLocker locker( m_lock );
        if ( !m_uhtr->isBHM() )
         {
          LOG4CPLUS_WARN( m_app->getApplicationLogger(), __func__ << ": uHTR has incorrect firmware loaded !!!" );
          return false;
         }
        //This double query was copied from uHTRtool.cc #8686, reason is unknown to me
        m_uhtr->link_query();
        toolbox::u_sleep( 10000 );
        m_uhtr->link_query();
        std::vector<uint32_t> gtxerr = m_uhtr->link_gtx_reset_count();
        //only fibers 3-8 are connected in BHM
        for ( int link = 3; link != 9; ++link )
          if ( m_uhtr->info_link_BadDataCounterOn( link ) != 1 )
           {
            LOG4CPLUS_WARN( m_app->getApplicationLogger(), __func__ << ": Link " << link << " is not active" );
            return false;
           }
        //at the moment, I don't know what limits to set, so in order to keep the system up, I left large margins. These parameters need not be runtime configurable.
        const double max_bad_data_rate = 0.1;
        for ( int link = 3; link != 9; ++link )
          if ( m_uhtr->info_link_BadDataRate( link ) > max_bad_data_rate )
           {
            LOG4CPLUS_WARN( m_app->getApplicationLogger(), __func__ << ": Link " << link << " bad data rate too high " << m_uhtr->info_link_BadDataRate( link ) );
            return false;
           }
        //bad align, delta, resetinfo, alignstatus, ...
        const float min_orb_rate = 11.240f;
        const float max_orb_rate = 11.255f;
        for ( int link = 3; link != 9; ++link )
          if ( ( m_uhtr->info_link_orbitRate( link ) < min_orb_rate ) || ( m_uhtr->info_link_orbitRate( link ) > max_orb_rate ) )
           {
            LOG4CPLUS_WARN( m_app->getApplicationLogger(), __func__ << ": Link " << link << " orbit rate incorrect " << m_uhtr->info_link_orbitRate( link ) );
            return false;
           }
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      return true;
     }

/**
 * Load Configuration into the uHTR
 */
    void Uhtr::loadConfig()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      int n_orbits = 0x1000 * r_cfg.integrate_nibbles;
      //thresholds
      std::vector<uint8_t> thresholds( 24 );
      for ( size_t ch = 0; ch != 24; ++ch )
        thresholds[ch] = toolbox::toUnsignedLong( r_cfg.channels[ch].getProperty( "uhtr_threshold" ) );
      //tdcmap
      auto lst = toolbox::parseTokenList( r_cfg.uhtr_tdcmap, "," );
      if ( lst.size() != unsigned( hcal::uhtr::uHTR::BHM_N_TDCMAP ) )
        XCEPT_RAISE( exception::ConfigurationError, "There should be 64 values in the TDC map" );
      std::vector<uint8_t> tdcmap( hcal::uhtr::uHTR::BHM_N_TDCMAP );
      std::transform( lst.begin(), lst.end(), tdcmap.begin(), toolbox::toUnsignedLong );
      if ( *std::max_element( tdcmap.begin(), tdcmap.end() ) > 4 )
        XCEPT_RAISE( exception::ConfigurationError, "TDC map values should be in the range 0-4" );
      try
       {
        SafeLocker locker( m_lock );
        m_uhtr->bhm_setup( n_orbits, r_cfg.uhtr_orbit_phase, thresholds, tdcmap );
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
     }

/**
 * Retrieve configuration from the uHTR
 */
    void Uhtr::readConfig()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      int n_orbits, orbitphase;
      std::vector<uint8_t> thresholds, tdcmap;
      try
       {
        SafeLocker locker( m_lock );
        m_uhtr->bhm_read_setup( n_orbits, orbitphase, thresholds, tdcmap );
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      r_cfg.integrate_nibbles = n_orbits / 0x1000;
      r_cfg.uhtr_orbit_phase = orbitphase;
      //tdcmap
      std::ostringstream ostr;
      std::copy( tdcmap.begin(), tdcmap.end() - 1, std::ostream_iterator<int>( ostr, "," ) );
      ostr << int( tdcmap.back() );
      r_cfg.uhtr_tdcmap = ostr.str();
      //thresholds
      for ( size_t ch = 0; ch != 24; ++ch )
        r_cfg.channels[ch].setProperty( "uhtr_threshold", std::to_string( (unsigned long long) thresholds[ch] ) );
     }

/**
 * Occupancy workloop main function.
 *
 * This function retrieves occupancy histograms when available, and pushes them into the publication queue.
 *
 * @returns An std::vector of Histograms, empty if they were not retrieved
 */
    Uhtr::OccupancyHistogramList Uhtr::getOccupancy()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      std::vector<bool> available, overflow;
      OccupancyHistogramList histos;
      try
       {
        SafeLocker locker( m_lock );
        m_uhtr->bhm_histo_status( available, overflow );
        int ava = std::count( available.begin(), available.end(), true );
        int ovf = std::count( overflow.begin(), overflow.end(), true );
        std::vector<uint32_t> maxes;
        if ( ava || ovf )
         {
          m_uhtr->bhm_read_histograms( histos );
          for ( auto it = histos.begin(); it != histos.end(); ++it )
            maxes.push_back( *std::max_element( it->h.begin(), it->h.end() ) );
          if ( ovf ) //overflow is invalid for me, return empty
            histos.clear();
         }
        return histos;
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      return histos;
     }

/**
 * Amplitude workloop main function.
 *
 * This function retrieves amplitude histograms, and pushes them into the publication queue.
 * The interface to acquire amplitude histograms requires timing control by sw, which makes it imprecise.
 * As a result, histograms will not have a perfect correspondance to nibbles, nor an exact entry count.
 *
 * @note The uhtr lock is only held during actual access, and released while sleeping or doing other things...
 *
 * @returns A structure holding the list of histogram and some run info
 */
    Uhtr::AmplitudeHistogramList Uhtr::getAmplitude()
     {
      LOG4CPLUS_DEBUG( m_app->getApplicationLogger(), __func__ );
      AmplitudeHistogramList list;
      try
       {
        SafeLocker locker( m_lock );
        m_uhtr->bhm_ampl_histo_control( false, true ); //set the clear flag
        locker.unlockedwait( 100000 ); //allow time to clear (conservative)
        m_uhtr->bhm_ampl_histo_control( true, false ); //set run flag and reset clear flag
        locker.unlockedwait( 1300000 ); //integration time (approximately four nibbles), replace with loop checking m_uhtr run info and counting 4 nibbles...
        m_uhtr->bhm_ampl_histo_control( false, false ); //stop everything
        m_uhtr->get_run_info( true, list.nb, list.ls, list.run, list.fill );
        for ( int link = 3; link != 9; ++link ) //fiber index
          for ( int lc = 0; lc != 4; ++lc ) //channel within fiber
           {
            int ch = ( link - 3 ) * 4 + lc;
            for ( int tdcbin = 1; tdcbin != 5; ++tdcbin )
              list.histo[ch][tdcbin - 1] = m_uhtr->bhm_ampl_histo_read( link, lc, tdcbin );
           }
       }
      catch ( uhal::exception::exception& e )
       {
        XCEPT_RAISE( exception::HardwareError, std::string( "Caught uhal Exception while accessing uHTR: " ) + e.what() );
       }
      return list;
     }

   }

 }

