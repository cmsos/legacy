#ifndef _tcds_hwutilsvme_HwStatusInfoSpaceUpdaterVME_h_
#define _tcds_hwutilsvme_HwStatusInfoSpaceUpdaterVME_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayervme {
    class VMEDeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace hwutilsvme {

    class HwStatusInfoSpaceUpdaterVME : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      HwStatusInfoSpaceUpdaterVME(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::hwlayervme::VMEDeviceBase const& hw);
      virtual ~HwStatusInfoSpaceUpdaterVME();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    private:
      tcds::hwlayervme::VMEDeviceBase const& getHw() const;

    };

  } // namespace hwutilsvme
} // namespace tcds

#endif // _tcds_hwutilsvme_HwStatusInfoSpaceUpdater_h_
