#! /bin/awk -f
BEGIN {
	FS=":"
	"hostname -s" | getline hostname;
	#print "HOSTNAME IS: [",hostname, "]";
	pattern = "^" hostname;
}

$1 ~ pattern { 
	for (i = 2; i < ARGC; i++) {
			
		split(ARGV[i], fname, ".");
		filename = fname[1] "." fname[2];

		system ("sed 's/%row/" $2 "/g' " ARGV[i] "> " filename);
		system ("sed --in-place 's/%nodetype/" $3 "/g' " filename );
		system ("sed --in-place 's/%service/" $4 "/g' " filename );
	}
}

END {
	#print "HOSTNAME WAS: [", hostname,"]";
}
