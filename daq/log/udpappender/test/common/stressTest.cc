
#include <log4cplus/logger.h>
#include <appender4oracle/OracleAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>

using namespace log4cplus;

int main(int argc, char**argv)
{
	if (argc < 6)
	{
		cout << argv[0] << " dbname username password sessionname loopCount" << endl;
		return 1;
	}

	std::string db = argv[1];
	std::string uname = argv[2];
	std::string pwd = argv[3];
	std::string session = argv[4];
	unsigned int loopCount = atoi(argv[5]);

    helpers::LogLog::getLogLog()->setInternalDebugging(true);
    SharedAppenderPtr a(new OracleAppender(db, uname, pwd, session));
	a->setName("OracleAppender");
    Logger::getRoot().addAppender(a);

    Logger root = Logger::getRoot();
    Logger test = Logger::getInstance("test");
    Logger subTest = Logger::getInstance("test.subtest");

	cout << "Start measuring " << loopCount << " log message insertions." << endl;

	time_t start = time(0);

	for (int i = 0; i < loopCount; i++)
	{
		LOG4CPLUS_INFO(subTest, "Test log message");
	}
	
	time_t end = time(0);
	
	cout << "Stopped measuring " << loopCount << " log message insertions." << endl;
	
	unsigned long duration = end-start;
	if (duration < 1)
	{
		cout << "Cannot calculate insertions per second. Test run too short." << endl;
	} 
	else
	{
		cout << "Test time: " << duration << " seconds." << endl;
		cout << "Insertions per second: " << (double) loopCount/ (double) duration << endl;
	}

    return 0;
}


