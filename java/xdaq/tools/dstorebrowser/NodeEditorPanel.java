package xdaq.tools.dstorebrowser;

import java.awt.Dimension;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.tree.*;
import xdaq.tools.win.*;
import org.w3c.dom.Node;
import xdaq.tools.dstorebrowser.dstoreTree;

import java.net.URL;
import java.awt.*;

public class NodeEditorPanel extends JPanel implements ActionListener
{
    DefaultMutableTreeNode selectedNode_;
    JLabel label_;
    JTextField value_;
    JButton okButton_, cancelButton_;
    dstoreTree tree_;
    dstorebrowser mainWindow_;

    JScrollBar xBar_;
    JScrollBar yBar_;

    public NodeEditorPanel (DefaultMutableTreeNode selectedNode, dstorebrowser mainWindow)
	{
	    selectedNode_ = selectedNode;
	    tree_ = mainWindow.tree_;
	    mainWindow_ = mainWindow;
                                    
	    xBar_ = ( (JScrollPane) tree_.getParent().getParent()).getHorizontalScrollBar();
	    yBar_ = ( (JScrollPane) tree_.getParent().getParent()).getVerticalScrollBar();

	    URL okURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/ok.gif"); 
	    URL cancelURL = getClass().getResource("/xdaq/tools/dstorebrowser/icons/cancel.gif");

	    String name = ((xdaqWinTreeAdapterNode) selectedNode.getUserObject()).toString();
	    String value = ((xdaqWinTreeAdapterNode) selectedNode.getUserObject()).domNode.getFirstChild().getNodeValue();

	    label_ = new JLabel( name );
	    value_ = new JTextField( value, value.toString().length()+3 );
	    value_.setHorizontalAlignment(JTextField.RIGHT);

	    okButton_ = new JButton( new ImageIcon(okURL));
	    cancelButton_ =  new JButton(new ImageIcon(cancelURL));
	    this.add (label_);
	    this.add (value_);
	    this.add (okButton_);
	    this.add (cancelButton_);

	    okButton_.addActionListener ( this );
	    cancelButton_.addActionListener ( this );
	    
	    enableBackground(false);
	}

    public void enableBackground (boolean mode)
	{
	    xBar_.setEnabled(mode);
	    yBar_.setEnabled(mode);
	    tree_.setEnabled(mode);
	}

    public void actionPerformed (ActionEvent e)
	{
	    JButton command = (JButton) e.getSource();
	    if (command == okButton_)
	    {
		org.w3c.dom.Node  node = ((xdaqWinTreeAdapterNode)  selectedNode_.getUserObject()).domNode;
		org.w3c.dom.Node valueText = node.getFirstChild();
		valueText.setNodeValue( value_.getText() );
                // set _operation attribute of parent to change
		((org.w3c.dom.Element) node.getParentNode()).setAttribute("_operation","change");
		mainWindow_.changeList.add((DefaultMutableTreeNode)selectedNode_.getParent());
		// force update of tree 
		((DefaultTreeModel) tree_.getModel()).nodeChanged ( selectedNode_ );
		((DefaultTreeModel) tree_.getModel()).reload ( selectedNode_.getParent() );
		mainWindow_.commit_.setEnabled(true);
		mainWindow_.rollback_.setEnabled(true);
	    } else if (command == cancelButton_)
	    {
		
	    }

	    // Rmove panel
	    setVisible (false);
	    getParent().remove(this);
	    enableBackground(true);
	}
    

	    
	    
	    
};
