#ifndef _tcds_utils_WebServer_h
#define _tcds_utils_WebServer_h

#include <cstddef>
#include <memory>
#include <string>
#include <vector>

#ifndef XDAQ_TARGET_XDAQ15
#include "xgi/exception/Exception.h"
#endif
#include "xgi/framework/UIManager.h"

#include "tcds/utils/Layout.h"
#include "tcds/utils/TCDSObject.h"
#include "tcds/utils/Uncopyable.h"
#include "tcds/utils/WebObject.h"
#include "tcds/utils/WebTab.h"

namespace log4cplus {
  class Logger;
}

namespace xgi {
  class Input;
  class Output;
}

namespace tcds {
  namespace utils {

    class Monitor;
    class XDAQAppBase;

    class WebServer :
      public xgi::framework::UIManager,
      public TCDSObject,
      private Uncopyable

    /**
     * Webserver class: allows access to all monitorable content in
     * the TCDS XDAQ applications.
     *
     * The HTML produced by this class is based on HTML5 BoilerPlate
     * (http://html5boilerplate.com/), via a bit of playing and
     * creative copy-paste from initializr
     * (http://www.initializr.com/).
     */
    {

    public:
      WebServer(tcds::utils::XDAQAppBase& xdaqApp);
      ~WebServer();

      void registerTab(std::string const& name,
                       std::string const& description,
                       size_t const numColumns);
      template <class O>
      void registerWebObject(std::string const& name,
                             std::string const& description,
                             Monitor const& monitor,
                             std::string const& itemSetName,
                             std::string const& tabName,
                             size_t const colSpan=1);
      void registerTable(std::string const& name,
                         std::string const& description,
                         Monitor const& monitor,
                         std::string const& itemSetName,
                         std::string const& tabName,
                         size_t const colSpan=1);
      void registerSpacer(std::string const& name,
                          std::string const& description,
                          Monitor const& monitor,
                          std::string const& itemSetName,
                          std::string const& tabName,
                          size_t const colSpan=1);

#ifndef XDAQ_TARGET_XDAQ15
      void jsonUpdate(xgi::Input* const in, xgi::Output* const out) throw (xgi::exception::Exception);
      void monitoringWebPage(xgi::Input* const in, xgi::Output* const out) throw (xgi::exception::Exception);
#else
      void jsonUpdate(xgi::Input* const in, xgi::Output* const out);
      void monitoringWebPage(xgi::Input* const in, xgi::Output* const out);
#endif

    private:
      /**
       * Returns the class name of the owner XDAQ application (without
       * namespaces).
       */
      std::string getApplicationName() const;

      std::string getServiceName() const;

      WebTab& getTab(std::string const& tabName) const;
      bool tabExists(std::string const& tabName) const;

      void printTabs(xgi::Output* const out) const;

      void jsonUpdateCore(xgi::Input* const in, xgi::Output* const out) const;
      void monitoringWebPageCore(xgi::Input* const in, xgi::Output* const out) const;

      log4cplus::Logger& logger_;
      std::vector<WebTab*> tabs_;
      tcds::utils::Layout layout_;

    };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/WebServer.hxx"

#endif // _tcds_utils_WebServer_h
