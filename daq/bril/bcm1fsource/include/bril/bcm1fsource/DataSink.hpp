/***************************************************************************** 
 * File  DataSink.hpp
 * created on 13.07.2012
 *****************************************************************************
 * Author:M.Eng. Dipl.-Ing(FH) Marek Penno, EL/1L23, Tel:033762/77275 marekp
 * Email:marek.penno@desy.de
 * Mail:DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 * 
 ****************************************************************************/

// #ifndef TOOLS_DATASINK_HPP_
// #define TOOLS_DATASINK_HPP_

#ifndef DATASINK_HPP_
#define DATASINK_HPP_

#include "DataSource.hpp"

template <class DATA>
class DataSink
{
private:
  DataSource<DATA> const* m_source;
  unsigned int changeCounter;
  DATA data;

public:
  // returns data change counter
  unsigned int getChangeCounter() const
  {
    return changeCounter;
  }

  // allow to copy data sinks
  DataSink()
    : m_source(0), changeCounter(0)
  {
  }

  // allow to copy data sinks
  DataSink(const DataSink<DATA>& other)
    : m_source(other.m_source), changeCounter(0)
  {
  }

  //
  DataSink(const DataSource<DATA>& other)
  {
    setSource(&other);
  }

  void setSource(DataSource<DATA> const* newSource)
  {
    m_source=newSource;
    if (m_source) {
      //DataSourceLock lock(m_source->getMutex());
      changeCounter=m_source->getChangeCounter()-1; // to force update next read cycle
    }
  }

  // returns true, if data source has changed
  bool hasChanged()
  {
    if (m_source) {
      //DataSourceLock lock(m_source->getMutex());
      if (m_source->getChangeCounter()!=changeCounter) return true;
    }
    return false;
  }

  // Performs an update
  void doUpdate()
  {
    if (!hasChanged() || !m_source) return;
    //DataSourceLock lock(m_source->getMutex());
    data=m_source->getData();
    changeCounter=m_source->getChangeCounter();
  }

  // returns actual data
  const DATA& getData()
  {
    assert(m_source!=NULL);
    //DataSourceLock lock(m_source->getMutex());
    if (hasChanged()) {
      // ok change detected, get new data
      doUpdate();
    }
    return data;
  }

};

// A Listener for Data Change Events

template <class DATA>
class DataSinkListener
{
private:
  DataSink<DATA> const* m_source;
  unsigned int changeCounter;
public:
  // allow to copy data sinks
  DataSinkListener()
    : m_source(0), changeCounter(0)
  {
  }

  // allow to copy data sinks
  DataSinkListener(const DataSinkListener<DATA>& other)
  {
    setSource(other.m_source);
  }

  //
  DataSinkListener(const DataSink<DATA>& other)
  {
    setSource(&other);
  }

  void setSource(DataSink<DATA> const* newSource)
  {
    m_source=newSource;
    if (m_source) changeCounter=m_source->getChangeCounter()-1; // to force update next read cycle
  }

  // returns true, if data source has changed
  bool hasChanged()
  {
    if (m_source && (m_source->getChangeCounter()!=changeCounter)) return true;
    return false;
  }

  // Performs an update
  void doUpdate()
  {
    if (!hasChanged() || !m_source) return;
    changeCounter=m_source->getChangeCounter();
  }

  // returns actual data
  const DATA& getData()
  {
    if (hasChanged()) {
      // ok change detected, get new data
      doUpdate();
    }
    return m_source->getData();
  }
};



#endif /* TOOLS_DATASINK_HPP_ */
