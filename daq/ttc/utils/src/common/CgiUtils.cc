#include "ttc/utils/CgiUtils.hh"

#include <boost/lexical_cast.hpp>
#include <cgicc/Cgicc.h>
#include <xgi/Input.h>
#include <stdexcept>


using namespace std;


// class XgiOutputHandler

XgiOutputHandler::XgiOutputHandler(xgi::Output& xo, bool flushIfException)
:
    xgi::Output(),
    xo_(xo),
    flushIfException_(flushIfException)
{
}


XgiOutputHandler::~XgiOutputHandler()
{
  if (!std::uncaught_exception() || flushIfException_)
  {
    xo_.getHTTPResponseHeader().addHeader("Content-Type", "text/html");
    xo_ << str();
  }
}


// class CgiUtils

string CgiUtils::GetCgiParam(cgicc::Cgicc &cgi, const string &param_name)
{
  vector<cgicc::FormEntry> result;
  if (cgi.getElement(param_name, result))
  {
    if (result.size() != 1)
      throw std::invalid_argument("parameter '" + param_name + "' given more than once");

    return result[0].getValue();
  }
  throw std::invalid_argument("parameter '" + param_name + "' not found");
}


bool CgiUtils::GetBoolCgiParam(cgicc::Cgicc &cgi, const string &param_name)
{
  string value_string = GetCgiParam(cgi, param_name);

  if (value_string == "true")
    return true;
  else if (value_string == "false")
    return false;
  else
    throw std::invalid_argument("Illegal boolean value '" + value_string + "' (must be 'true' or 'false')");
}


int CgiUtils::GetIntCgiParam(cgicc::Cgicc &cgi, const string &param_name)
{
  string value_string = GetCgiParam(cgi, param_name);

  try
  {
    return boost::lexical_cast<int>(value_string);
  }
  catch (boost::bad_lexical_cast &e)
  {
    // rethrow with a different message
    throw std::invalid_argument("Malformed integer value '" + value_string + "' for parameter '" + param_name + "'");
  }

  return 0;
}

void CgiUtils::FixContentType(xgi::Input* in)
{
  string buf = in->getenv("CONTENT_TYPE");
  string templ = "application/x-www-form-urlencoded";

  if (buf.find(templ) == 0)
    in->putenv("CONTENT_TYPE", templ);
}


void CgiUtils::PrintParameters(ostream &os, cgicc::Cgicc &cgi)
{
  vector<cgicc::FormEntry> all_entries = *cgi;
  for (vector<cgicc::FormEntry>::const_iterator
      it = all_entries.begin();
      it != all_entries.end(); ++it)
  {
    cout << " " << it->getName() << "=" << it->getValue() << endl;
  }
}


string CgiUtils::CgiccGetParameterWithDefault(
    cgicc::Cgicc& cgi,
    const string& param_name,
    const string& default_value)
{
  vector<cgicc::FormEntry> buf;

  if (cgi.getElement(param_name, buf))
    return buf[0].getValue();
  else
    return default_value;
}


bool CgiUtils::CgiccHasParameter(cgicc::Cgicc& cgi, const string& param_name)
{
  vector<cgicc::FormEntry> buf;
  return cgi.getElement(param_name, buf);
}
