/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2007, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "benchmark/soapping/Server.h"
#include "benchmark/soapping/Client.h"

XDAQ_INSTANTIATOR_IMPL(benchmark::soapping::Server)
XDAQ_INSTANTIATOR_IMPL(benchmark::soapping::Client)

