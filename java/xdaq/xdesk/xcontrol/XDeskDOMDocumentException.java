package xdaq.xdesk.xcontrol;

public class XDeskDOMDocumentException extends Exception
{
	XDeskDOMDocumentException () { super(); }

	XDeskDOMDocumentException ( String msg )
	{
		super (msg);
	}
}
