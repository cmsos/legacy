#include "tcds/ttcspy/TTCSpyInfoSpaceHandler.h"

#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/WebServer.h"

tcds::ttcspy::TTCSpyInfoSpaceHandler::TTCSpyInfoSpaceHandler(xdaq::Application& xdaqApp,
                                                             tcds::utils::InfoSpaceUpdater* const updater) :
  InfoSpaceHandler(xdaqApp, "tcds-ttcspy", updater)
{
  // Some hardware state information.
  createBool("logging_enabled", false, "", tcds::utils::InfoSpaceItem::PROCESS);
  createBool("ttc_fmc_signal_present", false, "", tcds::utils::InfoSpaceItem::PROCESS);
  createBool("ttc_fmc_cdr_locked", false, "", tcds::utils::InfoSpaceItem::PROCESS);
  createBool("ttc_clock_up", false, "", tcds::utils::InfoSpaceItem::PROCESS);

  // The contents of the TTC spy memory.
  createString("spy_memory", "-", "", tcds::utils::InfoSpaceItem::PROCESS);
}

void
tcds::ttcspy::TTCSpyInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Information on the current state of the hardware.
  monitor.newItemSet("Hardware state");
  monitor.addItem("Hardware state",
                  "logging_enabled",
                  "TTC spy logging enabled?",
                  this);
  monitor.addItem("Hardware state",
                  "ttc_fmc_signal_present",
                  "TTC FMC CDR has signal?",
                  this);
  monitor.addItem("Hardware state",
                  "ttc_fmc_cdr_locked",
                  "TTC FMC CDR locked?",
                  this);
  monitor.addItem("Hardware state",
                  "ttc_clock_up",
                  "TTC stream clock ok?",
                  this);

  // The spy-memory contents.
  monitor.newItemSet("Spy memory");
  monitor.addItem("Spy memory",
                  "spy_memory",
                  "Full, raw dump",
                  this);
}

void
tcds::ttcspy::TTCSpyInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                    tcds::utils::Monitor& monitor,
                                                                    std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "Spy memory" : forceTabName;

  // One tab showing the spy memory contents.
  webServer.registerTab(tabName,
                        "Spy memory contents",
                        1);
  // webServer.registerDiv("Spy Memory",
  //                        "The contents of the TTC spy memory",
  //                          monitor,
  //                        "Spy Memory",
  //                        "Spy Memory");
}
