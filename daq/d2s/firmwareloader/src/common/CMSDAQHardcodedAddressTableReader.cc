/**
*      @file CMSDAQHardcodedAddressTableReader.cc
*
*       @see ---
*    @author Hannes Sakulin
* $Revision: 1.3 $
*     $Date: 2007/03/27 08:02:32 $
*
*
**/
#include "d2s/firmwareloader/CMSDAQHardcodedAddressTableReader.hh"

d2s::CMSDAQHardcodedAddressTableReader::CMSDAQHardcodedAddressTableReader() 
  : HAL::PCIAddressTableDynamicReader() {
  // ******************************************************************************************************************
  // * Generic address table for CMS DAQ PCI cards
  // * Vendor ID : 0xECD6
  // *****************************************************************************************************************
  // *  key				Access		BAR	Offset		mask		read	write	description
  // *****************************************************************************************************************
  // * 
  createItem("configspace_begin", 		HAL::CONFIGURATION, 0, 0x00000000, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("BAR0", 				HAL::CONFIGURATION, 0, 0x00000010, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("BAR1", 				HAL::CONFIGURATION, 0, 0x00000014, 0xFFFFFFFF, 1, 1, "config. space reg.");
  createItem("GeoSlot",                          HAL::CONFIGURATION, 0, 0x00000050, 0x000000ff, 1, 0, "The geographic slot of the FMM (0..20)");
  createItem("sn_a", 				HAL::CONFIGURATION, 0, 0x0000005c, 0xFFFFFFFF, 1, 0, "serial number a (8 digit)");
  createItem("sn_b", 				HAL::CONFIGURATION, 0, 0x00000060, 0xFFFFFFFF, 1, 0, "serial number b (8 digit)");
  createItem("sn_c", 				HAL::CONFIGURATION, 0, 0x00000064, 0x00000FFF, 1, 0, "serial number c (3 digit)");
  // *
  // *
  // *
  createItem("REPROG_MAIN", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000004, 1, 1, "reprogram bit for main FPGA (Altera/Xilinx) (toggle to 1, wait, toggle to 0)");
  createItem("REPROG_BRIDGE", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00000002, 1, 1, "reprogram bit for bridge (Altera) (toggle to 1, wait, toggle to 0)");
  createItem("DESIGN_VERSION", 			HAL::CONFIGURATION, 0, 0x00000040, 0x00070000, 1, 1, "design version to load into FRL Main FPGA from EPC4/8");
  // 
  createItem("JTAG_SECONDARY", 		        HAL::CONFIGURATION, 0, 0x00000040, 0x00000008, 1, 1, "use secondary JTAG chain (Trigger distributor only)");
  createItem("JTAG_ENABLE", 		        HAL::CONFIGURATION, 0, 0x00000040, 0x00000001, 1, 1, "enable bit for JTAG");
  // 
  createItem("JTAG_TDI_TMS_CLK", 		HAL::CONFIGURATION, 0, 0x00000044, 0x0000007f, 0, 1, "[6:0] 6:TDI 1:TMS 0:CLK bits sent to JTAG chain");
  createItem("JTAG_TDO", 		        HAL::CONFIGURATION, 0, 0x00000044, 0x00000080, 1, 0, "TDO bit read from JTAG chain");
  createItem("FWID_ALTERA", 			HAL::CONFIGURATION, 0, 0x00000048, 0xFFFFFFFF, 1, 0, "firmware ID ALTERA");
  // 
  // 
  // * the following four regisers are in the Altera. They are used of turbo access to the JTAG Chain.
  createItem("RESET_FRL",                        HAL::MEMORY, 0, 0x00000000, 0x00020000, 1, 1, "reset the FRL (just write a 1)");
  createItem("JTAG_MASK32",           		HAL::MEMORY, 0, 0x00008090, 0xFFFFFFFF, 1, 1, "Mask for 32-bit JTAG shifting");
  createItem("JTAG_TMS32",           		HAL::MEMORY, 0, 0x00008098, 0xFFFFFFFF, 1, 1, "TMS for 32-bit JTAG shifting");
  createItem("JTAG_TDI32",           		HAL::MEMORY, 0, 0x00008094, 0xFFFFFFFF, 1, 1, "TDI to JTAG chain for 32-bit JTAG shifting");
  createItem("JTAG_TDO32",           		HAL::MEMORY, 0, 0x0000809C, 0xFFFFFFFF, 1, 0, "TDO from JTAG chain for 32-bit JTAG shifting");
  
 createItem("Geographic_Address",  HAL::MEMORY, 0, 0x0001C010, 0x000000FF, 1, 0, "return the geographical address of the FEROL40 in the uTCA crate. Slot 0 is the system slot at the left possition.");
  // 
  // *
  // * Register definition
  // *
  createItem("ID", 				HAL::MEMORY, 0, 0x00000800, 0xFFFFFFFF, 1, 0, "Identification register, holds version number, etc...");
}
