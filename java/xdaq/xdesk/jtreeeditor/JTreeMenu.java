package xdaq.xdesk.jtreeeditor;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import org.w3c.dom.Node;

public class JTreeMenu
    extends JMenu {
//-------------------------Variables definitions--------------------------------
  private JTreeConfig config;
  private JTreeEditor tree;
  private DefaultMutableTreeNode currentNode;
  private JTreeUserObject currentNodeUserObject;
  private DefaultMutableTreeNode previousNode;
  private JTreeUserObject previousNodeNodeUserObject;
  private DefaultMutableTreeNode nextNode;
  private JTreeUserObject nextNodeUserObject;
  private int index, childCount;
  private DefaultMutableTreeNode parentNode;
  private TreePath currentPath;

//==============================================================================
  /**Constructor*/
  public JTreeMenu(JTreeEditor tree_, String menuName) {
    super(menuName);
    this.tree = tree_;
    this.addMenuListener(new menuEditListener(tree));
  }

//==============================================================================
  /**Implementation of MenuListener*/
  class menuEditListener
      implements MenuListener {
    JTreeEditor adaptee = null;
    /**Constructor*/
    public menuEditListener(JTreeEditor adaptee_) {
      adaptee = adaptee_;
    }

    /**Invoked when a menu is selected. */
    public void menuSelected(MenuEvent e) {
      if (adaptee == null) {
        return;
      }
      if (adaptee.getSelectionPath() != null) {
        buildEditMenu();
      }
      else {
        adaptee.setSelectionPath(adaptee.getPathForRow(0));
        buildEditMenu();
      }
    }

    /**Not implemented*/
    public void menuDeselected(MenuEvent e) {}

    /**Not implemented*/
    public void menuCanceled(MenuEvent e) {}

  }

//******************************************************************************
   /**Sets values to the common variebles*/
   /**Returns true if all the values were successful added*/
   private boolean setValues() {
     config = tree.getConfiguration();
     currentPath = tree.getSelectionPath();
     if (currentPath == null) {
       return false;
     }
     currentNode = (DefaultMutableTreeNode) currentPath.getLastPathComponent();
     try {
       currentNodeUserObject = (JTreeUserObject) currentNode.getUserObject();
     }
     catch (Exception e) {
       return false;
     }
     previousNode = null;
     previousNodeNodeUserObject = null;
     nextNode = null;
     nextNodeUserObject = null;

     parentNode = (DefaultMutableTreeNode) currentNode.getParent();
     try {
       index = parentNode.getIndex(currentNode);
       childCount = parentNode.getChildCount();
     }
     catch (Exception ex) {
       index = 0;
       childCount = 0;
     }
     previousNode = ( (index - 1) >= 0) ?
         (DefaultMutableTreeNode) parentNode.getChildAt(index - 1) : null;
     nextNode = ( (index + 1) < childCount) ?
         (DefaultMutableTreeNode) parentNode.getChildAt(index + 1) : null;

     previousNodeNodeUserObject = (previousNode == null) ?
         currentNodeUserObject :
         (JTreeUserObject) previousNode.getUserObject();
     nextNodeUserObject = (nextNode == null) ? currentNodeUserObject :
         (JTreeUserObject) nextNode.getUserObject();

     tree.setSelectionPath(currentPath);
     currentNode = (DefaultMutableTreeNode) currentPath.
         getLastPathComponent();
     return true;
   }

//==============================================================================
  /**Dynamicly creates a JMenu according to the selected tree node*/
  private void buildEditMenu() {
    this.removeAll();

    if (!setValues()) {
      return;
    }

    /*--------------------If the tree is not editable----------------------*/
    if (!tree.isEditable()) {
      this.add(getAction("Start editing", "", true));
      this.addSeparator();
      if (!currentNode.isLeaf() && !currentNode.isRoot()) {
        if (tree.isExpanded(currentPath)) {
          this.add(getAction("Collapse", "Collapse", true));
        }
        else {
          this.add(getAction("Expand", "Expand", true));
        }
      }

      if (!lastItemIsSeparator()) {
        this.addSeparator();
      }
      this.add(getAction("Load GUI configuration", "LoadGUIconfiguration", true));

      this.add(getAction("Save GUI configuration", "SaveGUIconfiguration", true));

      this.add(getAction("Load default GUI configuration",
                         "LoaddefaultGUIconfiguration", true));
      return;
    }

    /*--------------------If the tree is editable--------------------------*/

    this.add(getAction("Stop editing", "", true));
    this.addSeparator();

    if (!currentNode.isLeaf() && !currentNode.isRoot()) {
      String tmpActionName = (tree.isExpanded(currentPath)) ? "Collapse" :
          "Expand";
      this.add(getAction(tmpActionName, tmpActionName, true));
    }

    if (!lastItemIsSeparator()) {
      this.addSeparator();
    }

    this.add(getAction("Cut", "Cut", isCutAllowed()));

    this.add(getAction("Copy", "Copy", isCopyAllowed()));

    JTreePopupAction pasteAction = getAction("Paste", "Paste", true);
    pasteAction.setEnabled(isPasteAllowed(pasteAction));
    this.add(pasteAction);

    this.addSeparator();

    if (isAllowInsertElementBefore()) {
      this.add(getAction("Insert Element Before", "InsertElementBefore", true));

    }
    if (isAllowInsertElementAfter()) {
      this.add(getAction("Insert Element After", "InsertElementAfter", true));
    }

    if (!lastItemIsSeparator()) {
      this.addSeparator();
    }
//submenu
    if ( ( (JTreeUserObject) currentNode.getUserObject()).getType() ==
        org.w3c.dom.Node.ELEMENT_NODE) {
      JMenu addMenu = new JMenu(getAction("Add..", "Add", true));
      if (isAllowElement()) {
        addMenu.add(getAction("Element", "Element", true));
      }
      if (isAllowAttributes()) {
        addMenu.add(getAction("Attribute", "Attribute", true));
      }
      if (isAllowText()) {
        addMenu.add(getAction("Text", "Text", true));
      }
      addMenu.add(getAction("Comment", "Comment", true));
      addMenu.add(getAction("CData", "CData", true));
      addMenu.add(getAction("Processing Instruction", "ProcessingInstruction", true));
      this.add(addMenu);
    }

    if (!lastItemIsSeparator()) {
      this.addSeparator();
    }

    if (isMoveUpAllowed()) {
      this.add(getAction("Move Up", "MoveUp", true));
    }
    if (isMoveDownAllowed()) {
      this.add(getAction("Move Down", "MoveDown", true));
    }

    if (!lastItemIsSeparator()) {
      this.addSeparator();
    }

    if (isDeleteAllowed()) {
      this.add(getAction("Delete", "Delete", true));
    }
    if (!lastItemIsSeparator()) {
      this.addSeparator();
    }

    this.add(getAction("Normalize tree", "Normalizetree", true));

    if (!lastItemIsSeparator()) {
      this.addSeparator();
    }

    this.add(getAction("Load GUI configuration", "LoadGUIconfiguration", true));
    this.add(getAction("Save GUI configuration", "SaveGUIconfiguration", true));
    this.add(getAction("Load default GUI configuration",
                       "LoaddefaultGUIconfiguration", true));
  }

//******************************************************************************
   /**Returns true if the last item of menu is Separator*/
   private boolean lastItemIsSeparator() {
     if (this.getItemCount() > 0) {
       if (this.getItem(this.getItemCount() - 1) == null) {
         return true;
       }
     }
     return false;
   }

//******************************************************************************
   /**Creates menu Action and associates an icon according to tag name*/
   private JTreePopupAction getAction(String actionName, String iconName,
                                      boolean isEnabled) {
     JTreePopupAction retAction = null;
     ImageIcon icon = null;
     if (iconName.length() > 0) {
       icon = (ImageIcon) config.getPupupIcon(iconName);
     }
     retAction = new JTreePopupAction(actionName, icon, tree);
     retAction.setEnabled(isEnabled);
     return retAction;
   }

//******************************************************************************
   /**Returns true if copy action is allowed for this type of node*/
   boolean isCopyAllowed() {
     return (!currentNode.isRoot() && currentNodeUserObject.getType() != 2);
   }

//******************************************************************************
   /**Returns true if cut action is allowed for this type of node*/
   boolean isCutAllowed() {
     return (!currentNode.isRoot() && currentNodeUserObject.getType() != 2);
   }

//******************************************************************************
   /**Returns true if paste action is allowed for this type of node*/
   boolean isPasteAllowed(JTreePopupAction action) {
     if (action.getBuffer() == null) {
       return false;
     }
     if (currentNodeUserObject.getType() != 1 ||
         (action.getBuffer().equals(currentNode) && action.isCuted())) {
       return false;
     }
     return true;
   }

//******************************************************************************
   /**Returns true if there is a possibility to move the node down*/
//returns true if current selected node is not a root
//and not the last element in parent's child collections
   boolean isMoveDownAllowed() {
     if (currentNode.isRoot()) {
       return false;
     }
     if (index == (childCount - 1)) {
       return false;
     }
     if (nextNode == null) {
       return false;
     }
     if ( (currentNodeUserObject.getType() != nextNodeUserObject.getType()) &&
         (currentNodeUserObject.getType() == Node.ATTRIBUTE_NODE ||
          nextNodeUserObject.getType() == Node.ATTRIBUTE_NODE)) {
       return false;
     }
     return true;
   }

//==============================================================================
//return true if current selected node is not a root
//and not the top element in parent's child collections
  /**Returns true if there is a pissibility to move the node up*/
  boolean isMoveUpAllowed() {
    if (currentNode.isRoot()) {
      return false;
    }
    if ( (currentNodeUserObject.getType() !=
          previousNodeNodeUserObject.getType()) &&
        (currentNodeUserObject.getType() == Node.ATTRIBUTE_NODE ||
         previousNodeNodeUserObject.getType() == Node.ATTRIBUTE_NODE)) {
      return false;
    }
    if (previousNode == null) {
      return false;
    }
    return true;
  }

//==============================================================================

  /**Returns true if delete action is allowed for the current node*/
  boolean isDeleteAllowed() {
    return! (currentNode.isRoot());
  }

//==============================================================================
  /**Returns true if there is a possibility to add a node before the current node*/
  boolean isAllowInsertElementBefore() {
    JTreeUserObject userObject = (JTreeUserObject) currentNode.
        getUserObject();
    boolean returnValue = true;
    if (currentNode.isRoot() || userObject.getType() != 1) {
      returnValue = false;
    }
    return returnValue;
  }

//==============================================================================
  /**Returns true if there is a possibility to add a node after the current node*/
  boolean isAllowInsertElementAfter() {
    return isAllowInsertElementBefore();
  }

//==============================================================================
  /**Returns true if an XML tag attached to the current node allows attributes*/
  boolean isAllowAttributes() {
    JTreeUserObject userObject = (JTreeUserObject) currentNode.
        getUserObject();
    return (userObject.getType() == 1);
  }

//==============================================================================
  /**Return true if there is a possibility to attache element to the current node*/
  boolean isAllowElement() {
    JTreeUserObject userObject = (JTreeUserObject) currentNode.
        getUserObject();
    return (userObject.getType() == 1);
  }

//==============================================================================
  /**Returns true if there is a posibility to attach a text element to the DOM
   * element which is attached to the current tree node
   */
  boolean isAllowText() {
    boolean retValue = true;
    if (childCount > 0) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode) parentNode.
          getLastChild();
      JTreeUserObject object = (JTreeUserObject) node.getUserObject();
      if (object.getType() == org.w3c.dom.Node.TEXT_NODE) {
        return false;
      }
    }
    return retValue;
  }

//==============================================================================
}
