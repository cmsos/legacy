/*
 * XDAQNThreadTest
 *
 * $Id: XDAQNThreadTest.java,v 4.1 2007/03/26 14:40:33 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import junit.framework.*;
import junit.extensions.TestSetup;

import java.io.*;
import java.util.regex.*;

// DOM
import org.w3c.dom.*;

// SOAP
import javax.xml.soap.*;

/**
 * XDAQNThreadTest
 */
public class XDAQNThreadTest extends TestCase
{
	private static final long WAIT = 1000; // [ms]
	private static final String XDAQ_NS = "urn:xdaq-soap:3.0";

	public XDAQNThreadTest(String name)
	{
		super(name);
	}

	public static void main(String args[])
	{
		junit.textui.TestRunner.run(XDAQNThreadTest.class);
	}

	public void testSend() throws Exception
	{
		XDAQMessage message;
		Document document;
		String good_result;
		String result = "";

		System.getProperties().setProperty("xdaqctl.pool.nthreads", "90");

		// First, configure the executive
		message = new XDAQMessage(
				"Configure", TestUtil.fileToDOM("test/xdaq/config.xml"));
		message.setTimeout(5000);

		document = message.send("http://localhost:40000", 0);
		assertTrue(document != null);

		good_result =
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<xdaq:ConfigureResponse xmlns:xdaq=\"urn:xdaq-soap:3.0\"/>\n";
		result = TestUtil.domToString(document);

		assertEquals(good_result, result);
	}

	private static void oneTimeSetUp() throws Exception
	{
		Process boot = Runtime.getRuntime().exec("./test/xdaq/bootxdaq start");
		boot.waitFor();
		String line = new BufferedReader(
				new InputStreamReader(boot.getInputStream())).readLine();
		assertNull("Failed to boot a XDAQ executive", line);

		Thread.sleep(WAIT);
	}

	private static void oneTimeTearDown() throws Exception
	{
		Runtime.getRuntime().exec("./test/xdaq/bootxdaq stop").waitFor();
		Thread.sleep(WAIT);
	}

	public static Test suite()
	{
		TestSuite suite = new TestSuite(XDAQNThreadTest.class);

		TestSetup wrapper = new TestSetup(suite) {
			protected void setUp() throws Exception { oneTimeSetUp() ; }
			protected void tearDown() throws Exception { oneTimeTearDown(); }
		};

		return wrapper;
	}
}

// End of file
// vim: set ts=4 sw=4:
