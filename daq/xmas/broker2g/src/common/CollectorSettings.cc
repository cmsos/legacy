/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xmas/broker2g/CollectorSettings.h"
#include "xmas/broker2g/Utils.h"
#include <sstream>
#include <set>

#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/Vector.h"
#include "xdata/TimeVal.h"

#include "toolbox/string.h"
#include "toolbox/Runtime.h"

xmas::broker2g::CollectorSettings::CollectorSettings(DOMNode* collectorNode, const std::string& flashlistName)
{
	flashlistName_ = flashlistName;

	publish_   = xoap::getNodeAttribute (collectorNode, "publish");
	subscribe_ = xoap::getNodeAttribute (collectorNode, "subscribe");
	hashkey_   = xoap::getNodeAttribute (collectorNode, "hashkey");
	flush_     = xoap::getNodeAttribute (collectorNode, "flush");
	clear_     = xoap::getNodeAttribute (collectorNode, "clear");

	expires_ = 0.0;
}

xmas::broker2g::CollectorSettings::~CollectorSettings()
{
}

const std::string& xmas::broker2g::CollectorSettings::getFlashlistName()
{
	return flashlistName_;
}

const std::string& xmas::broker2g::CollectorSettings::getPublish()
{
	return publish_;
}

const std::string& xmas::broker2g::CollectorSettings::getSubscribe()
{
	return subscribe_;
}

const std::string& xmas::broker2g::CollectorSettings::getHashKey()
{
	return hashkey_;
}

const std::string& xmas::broker2g::CollectorSettings::getFlush()
{
	return flush_;
}

const std::string& xmas::broker2g::CollectorSettings::getClear()
{
	return clear_;
}

const toolbox::TimeVal& xmas::broker2g::CollectorSettings::getExpires()
{
	return expires_;
}

const std::string& xmas::broker2g::CollectorSettings::getCollectorAddress()
{
	return collectorAddress_;
}

bool xmas::broker2g::CollectorSettings::isExpired()
{
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	if(now > expires_)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void xmas::broker2g::CollectorSettings::setExpires(const toolbox::TimeInterval& value)
{
	toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
	expires_ = now + value;
}

void xmas::broker2g::CollectorSettings::setCollectorAddress(const std::string& value)
{
	collectorAddress_ = value;
}

