// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <string>
#include "xmas/xmas.h"
#include "xmas/MonitorSettingsFactory.h"
#include "xoap/domutils.h"

std::vector<xmas::MonitorSettings *> xmas::MonitorSettingsFactory::create(DOMDocument* doc) 

{	
	std::vector<xmas::MonitorSettings *> settings;
	
	try
	{
		DOMNodeList* lists = doc->getElementsByTagNameNS(xoap::XStr(xmas::NamespaceUri), xoap::XStr("monitor"));
		// Retrieve all monitors statments	
		//			
		for (XMLSize_t i = 0; i < lists->getLength(); i++)
		{
			DOMNode* monitorNode = lists->item(i);
			xmas::MonitorSettings* m = new xmas::MonitorSettings(monitorNode);
			settings.push_back(m);
		}
		return settings;
	}
	catch (xcept::Exception& e)
	{
		std::stringstream msg;
		msg << "Failed to create monitor settings";
		XCEPT_RETHROW (xmas::exception::Exception, msg.str(), e);
	}
}


	
