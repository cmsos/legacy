package net.hep.cms.xdaqctl.xdata;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/*
 * XDAQ table is a vector of columns.
 * Each column has a name, a type, and vector of values.
 * </p>
 * <emph>Table of tables is not yet supported</emph>
 *
 * @author A. Oh &lt;Alexander.Oh@cern.ch&gt; and I. Suzuki &lt;ichiro@fnal.gov&gt;
 */
public class Table extends XDataItem {
	
	private class Column {
		public java.lang.String name;
		public XDataType type;
		public Vector values;
	}

	private java.util.Vector<Column> columns;
	
	public Table() {
		columns = new java.util.Vector<Column>();
	}
	
	/**
	 * @return number of columns
	 */
	public int columns() {
		return columns.size();
	}

	/**
	 * This assumes length of all the columns are same
	 * and returns the length of the first column.
	 *
	 * @return number of rows
	 */
	public int rows() {
		int rows = 0;
		
		if (columns.size() > 0) {
			rows = columns.get(0).values.size();
		}
		
		return rows;
	}
	
	/**
	 * define a column, giving its name and type
	 * </p>
	 * Example:
	 * <pre>
	 * addColumn("controlFlag", XDataType.UNSIGNED_INTEGER64);
	 * </pre>
	 *
	 * @param name
	 * @param type
	 */
	public boolean addColumn(java.lang.String name, XDataType type) {
		Column column = new Column();
		column.name = name;
		column.type = type;
		column.values = new Vector();
		
		columns.add(column);
		
		return true;
	}
	
	/**
	 * get an ordered list of names.
	 */
	public java.util.Vector<java.lang.String> getNames() {
		java.util.Vector<java.lang.String> names = new java.util.Vector<java.lang.String>();
		
		for (Column col : columns) {
			names.add(col.name);
		}
		
		return names;
	}

	/**
	 * get a type of a column.
	 * Use #getNames to retrieve a name of the column.
	 */
	public XDataType getType(java.lang.String name) {
		for (Column col : columns) {
			if (col.name.equals(name)) return col.type;
		}
		
		return null;
	}
	
	/**
	 * get a row as a vector of Java native objects
	 *
	 * @param index 
	 * @return Vector of Java objects.
	 *         The value
	 *         An empty vector is returned when index > rows().
	 * @throws ClassCastException
	 *         When there is a non-simple type column (vector or table).
	 */
	public java.util.Vector<Object> getRow(int index) {
		java.util.Vector<Object> row = new java.util.Vector<Object>();
		
		if (index < rows()) {
			for (Column column: columns) {
				row.add(((SimpleItem)column.values.get(index)).getValue());
			}
		}
		
		return row;
	}

	/**
	 * add a row
	 *
	 * @param row Each element of the row vector is put in the table
	 *       using SimpleItem.setValue();
	 * @return true
	 * @throws ClassCastException
	 *         When there is a non-simple type column (vector or table).
	 */
	public boolean add(java.util.Vector row) {
		if (row.size() != columns.size()) { return false; }
		
		int index = 0;
		for (Column column: columns) {
			SimpleItem item = (SimpleItem)column.type.getInstance();
			item.setValue(row.get(index));
			column.values.add(item);
			++index;
		}
		
		return true;
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(11);      // table type descriptor
		out.writeInt(15);      // #col, uint32
		out.writeInt(columns());
		out.writeInt(15);      // #row, uint32
		out.writeInt(rows());
		
		for (Column column: columns) {		
			String name = new String();
			name.setValue(column.name);
			name.writeEXDR(out);
			
			UnsignedInteger32 type = new UnsignedInteger32();
			type.setValue(column.type.getEXDRCode());
			type.writeEXDR(out);
			
			column.values.writeEXDR(out);
		}
	}
	
	@Override 
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code

		int ncol;
		UnsignedInteger32 n = new UnsignedInteger32();
		n.readEXDR(in);
		ncol = n.getValue().intValue();
		n.readEXDR(in);  // #row, not used here
		
		columns.clear();
		
		for (int i = 0; i < ncol; ++i) {
			Column column = new Column();

			String name = new String();
			name.readEXDR(in);
			column.name = name.getValue();

			UnsignedInteger32 type = new UnsignedInteger32();
			type.readEXDR(in);
			column.type = XDataType.getEnum(type.getValue().intValue());

			column.values = new Vector();
			column.values.readEXDR(in);
			
			columns.add(column);
		}
		
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.TABLE.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
