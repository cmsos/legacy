#!/usr/bin/env python

###############################################################################
## Utility to hammer an APVE with Resyncs to see if it catches them all.
###############################################################################

import sys
import time
import uhal

from pytcds.lpm.tcds_lpm import get_lpm_hw

import pdb

###############################################################################

def build_uhal_uri(ip_address, controlhub_address=None):
    uri = None

    if controlhub_address is None:
        uri = "ipbusudp-2.0://%s:50001" % ip_address
    else:
        uri = "chtcp-2.0://%s:10203?target=%s:50001" % \
              (controlhub_address, ip_address)

    # End of build_uhal_uri().
    return uri

###############################################################################

if __name__ == "__main__":

    apve_address = "lpm4"

    uhal.disableLogging()

    lpm = get_lpm_hw(apve_address)
    if not lpm:
        self.error("Could not connect to {0:s}.".format(self.ip_address))

    lpm.write("ici1.main.inselect.combined_software_bgo_enable", 0x1)

    num_iter = 100
    for i in xrange(num_iter):

        print "DEBUG JGH iteration ", i

        # Reconfigure APVE.
        lpm.write("apve1.config.apv_latency", 0x92)
        status = lpm.read("apve1.diagnostics.apv_oos_reason.apv_configuration_change")
        if status != 0x1:
            print "ERROR Expected APV OOS after reconfigure"
            # sys.exit(1)

        # Send a Resync (via the ICI) and see that the APVE goes READY.
        lpm.write("ici1.main.ipbus_requests", 0x205)
        time.sleep(1)
        status = lpm.read("apve1.diagnostics.apv_oos_reason.apv_configuration_change")
        if status != 0x0:
            print "ERROR Expected APV READY after Resync"
            # sys.exit(1)

    print "Done"

###############################################################################
