package xdaq.tools.cluster;

import java.io.*;
import java.util.Vector;

class CommandHistoryFile
{
	PrintWriter out_;
	String file_;
	
	public CommandHistoryFile ( String file )
	{
		file_ = file;
		try {
			out_ = new PrintWriter(new FileWriter ( file_ , true));
		} catch (IOException e) 
		{
			System.out.println ("FileWriter: " + e );
		}
	}
	
	public void store ( String url)
	{
		System.out.println ("Store: " + url);
		out_.println ( url );
	}
	
	public String[] retrieve ()
	{
		Vector stringVector = new Vector();
		
		try {
			FileReader fileIn = new FileReader ( file_ );
			BufferedReader in = new BufferedReader (fileIn);
			String currentLine;
			while ( (currentLine = in.readLine() ) != null)
			{
				stringVector.add ( currentLine );	
			}
			fileIn.close();
			
		} catch (IOException e) {
			System.out.println ("Retrieve: " + e);
		}
		String retVal[] = new String[stringVector.size()];
		return  (String[]) stringVector.toArray(retVal);
	}
	
	public void clear()
	{
		try {
			out_.close();
			out_ = new PrintWriter(new FileWriter ( file_ ));
		} catch (IOException e) 
		{
			System.out.println ("FileWriter: " + e );
		}	
	}
	
	protected void close () 
	{
		out_.close();
	}
}
