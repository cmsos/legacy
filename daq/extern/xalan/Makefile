# $Id: Makefile,v 1.10 2009/03/04 10:44:24 lorsini Exp $

#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2004, CERN.			                #
# All rights reserved.                                                  #
# Authors: J. Gutleber and L. Orsini					#
#                                                                       #
# For the licensing terms see LICENSE.		                        #
# For the list of contributors see CREDITS.   			        #
#########################################################################

##
#
# This is the TriDAS/extern/xalan
#
##


#
# Packages to be built
#
BUILD_HOME:=$(shell pwd)/../../..

ifndef BUILD_SUPPORT
BUILD_SUPPORT=config
endif

ifndef PROJECT_NAME
PROJECT_NAME=daq
endif

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

ifndef MFDEFS_SUPPORT
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.extern_coretools
else
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)
endif

Project=$(PROJECT_NAME)
Package=extern/xalan

PackageName=xalan

PACKAGE_VER_MAJOR=1
PACKAGE_VER_MINOR=11
PACKAGE_VER_PATCH=0

Summary=Xalan is an XSLT processor for transforming XML documents into HTML, text, or other XML document types.

Description=Xalan-C++ version 1.11 is a robust implementation of the W3C Recommendations for XSL Transformations (XSLT) and the XML Path Language (XPath). \
It works with the Xerces-C++ version 3.1.1 release of XML parsers. The focus for this Xalan release is on bug fixes and compatibility with newer development platforms.

Link=https://xalan.apache.org/xalan-c/

CFLAGS:=${CFlags}
CXXFLAGS:=${CCFlags}
export CFLAGS
export CXXFLAGS

export XERCESCROOT=$(XERCES_PREFIX)
export XALANCROOT=$(BUILD_HOME)/$(PROJECT_NAME)/extern/xalan/xalan-c-1.11/c


# Check if we are on 64 bits or 32 bits platform
ifneq (,$(findstring x86_64,$(XDAQ_PLATFORM)))
BITS=64
else
ifeq ($(XDAQ_OS),macosx)
BITS=64
else
BITS=32
endif
endif

build: _buildall

_buildall: all

_all: all

default: all

xalan-c-1.11/c/runConfigure:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf xalan-c-1.11 
	tar -zxf xalan_c-1.11-src.tar.gz
	cd xalan-c-1.11 /c

all: xalan-c-1.11/c/runConfigure
	cd ./xalan-c-1.11/c; \
 	chmod +x ./runConfigure; \
 	./runConfigure -p $(XDAQ_OS) -c $(CC) -x $(CXX) -b $(BITS) -P $(BUILD_HOME)/$(Project)/$(Package)/$(XDAQ_PLATFORM); \
 	make; make install

_installall: install

install:
	cd xalan-c-1.11/c; \
	chmod +x ./runConfigure; \
	./runConfigure -p $(XDAQ_OS) -c $(CC) -x $(CXX) -b $(BITS) -P $(INSTALL_PREFIX)/$(XDAQ_PLATFORM); \
	make; make install

_cleanall: clean

clean:
	-rm -rf $(XDAQ_PLATFORM)
	-rm -rf ./xalan-c-1.11;

samples: 
	cd ./xalan-c-1.11/c/samples; \
        chmod +x ./runConfigure; \
        ./runConfigure -p $(XDAQ_OS) -c $(CC) -x $(CXX) -b $(BITS) -P $(INSTALL_PREFIX)/$(XDAQ_PLATFORM); \
        make; make install

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfExternRPM.rules
