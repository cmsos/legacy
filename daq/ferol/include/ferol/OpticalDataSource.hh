#ifndef __OpticalDataSource
#define __OpticalDataSource

#include "ferol/DataSourceIF.hh"
#include "ferol/MdioInterface.hh"

namespace ferol
{
    /**
     *
     *
     *     @short Abstract class providing services  to set up optical link
     *            inputs.
     *            
     *            This  class  does  not  implement  the  DataSourceIF  but 
     *            provides methods to configure  either the 6Gbps inputs or
     *            the 10Gbps inputs.
     *
     *   $Author$
     * $Revision$
     *     $Date$
     *
     **/
    class OpticalDataSource : public DataSourceIF {
    public:
        OpticalDataSource( HAL::HardwareDeviceInterface *device_P,
                           MdioInterface *mdioInterface_P,
                           utils::InfoSpaceHandler &appIS,
                           Logger logger );
        void setDataSource() const = 0;
    protected:

        /**
         *
         *     @short Configure the 6Gbps input(s) of the Ferol.
         *
         **/
        void setup6GInputs() const;

        /**
         *
         *     @short Configure the 10Gbps input of the Ferol.
         *
         **/
        void setup10GInput() const;
    private:
        MdioInterface *mdioInterface_P;
    };
}

#endif /* __FRLDataSource */
