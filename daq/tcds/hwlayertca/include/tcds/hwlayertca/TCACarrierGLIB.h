#ifndef _tcds_hwlayertca_TCACarrierGLIB_h_
#define _tcds_hwlayertca_TCACarrierGLIB_h_

#include <string>
#include <vector>

#include "tcds/hwlayer/I2CAccessor.h"
#include "tcds/hwlayertca/TCACarrierBase.h"

namespace tcds {
  namespace hwlayertca {

    class HwDeviceTCA;

    /**
     * FMC carrier implementation for the GLIB board.
     */
    class TCACarrierGLIB : public TCACarrierBase
    {

    public:
      TCACarrierGLIB(HwDeviceTCA& device);
      ~TCACarrierGLIB();

    protected:
      virtual std::string registerNamePrefix() const;
      virtual std::vector<std::string> fmcNamesImpl() const;
      virtual bool isFMCPowerGoodImpl(std::string const& fmcName) const;
      virtual void loadFirmwareImageImpl(std::string const& imageName) const;
      virtual void powerUpImpl() const;
      virtual void selectClockSourceImpl(tcds::hwlayertca::TCACarrierBase::CLOCK_SOURCE const clkSrc) const;
      virtual void enableSFPsImpl() const;
      virtual void disableSFPsImpl() const;

      virtual std::string expectedCarrierType() const;

    private:
      tcds::hwlayer::I2CAccessor::I2CBus fmcBusSelect(std::string const& fmcName) const;
      void switchFMCSFPs(std::string const& fmcName, bool const switchOn) const;

    };

  } // namespace hwlayertca
} // namespace tcds

#endif // _tcds_hwlayertca_TCACarrierGLIB_h_
