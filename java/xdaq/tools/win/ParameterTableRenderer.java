package xdaq.tools.win;
import java.awt.Component;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;

import  xdaq.tools.win.ParameterTableModel;

public class ParameterTableRenderer extends DefaultTableCellRenderer
{
	public Component getTableCellRendererComponent
		(JTable table, Object value, boolean isSelected,
			boolean hasFocus, int row, int column)
	{
		ParameterTableModel model = (ParameterTableModel) table.getModel();
		
		System.out.println ("ParameterTableRenderer: " + value.toString);
		
		Component cell = super.getTableCellRendererComponent
			( table, value, isSelected, hasFocus, row, column);
			
		
		
		//System.out.println ("Renderer for row " + row + " col " + column);
		
		if (model.getActive ( row ) == Boolean.FALSE)
		{	
			//model.setValueAt ( Boolean.FALSE, row, 3);
			cell.setForeground ( Color.gray );
			return new JLabel ("*");
		} else {
			cell.setForeground ( Color.black );
		}
		return cell;
	}
}
