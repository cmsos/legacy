package net.hep.cms.xdaqctl.xdata;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class Integer64 extends SimpleItem {
	
	private java.lang.Long value;
	
	@Override
	public java.lang.Long getValue() {
		return value;
	}
	
	@Override
	public void setValue(Object value) {
		this.value = ((Number)value).longValue();
	}

	@Override
	public void writeEXDR(DataOutputStream out) throws IOException {
		out.writeInt(getEXDRCode());
		out.writeLong(value);
	}
	
	@Override
	public void readEXDR(DataInputStream in) throws IOException {
		in.readInt(); // EXDR code
		value = in.readLong();
	}
	
	@Override
	int getEXDRCode() {
		return XDataType.INTEGER64.getEXDRCode();
	}
}

// End of file
// vim: set sw=4 ts=4:
