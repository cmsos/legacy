#ifndef _interface_bril_LUMITopics_hh_
#define _interface_bril_LUMITopics_hh_
#include "interface/bril/CommonDataFormat.h"
namespace interface{ namespace bril{

  DEFINE_COMPOUND_TOPIC(bestlumi,"provider:str8:1 calibtag:str32:1 delivered:float:1 recorded:float:1 avgpu:float:1 bxdelivered:float:3564","best instantaneous luminosity","Hz/ub");  
   
  DEFINE_COMPOUND_TOPIC(atlaslumi,"delivered:float:1 bxdelivered:float:3564","atlas instantaneous luminosity","Hz/ub");  
   
  DEFINE_COMPOUND_TOPIC(dtlumi,"calibtag:str32:1 avgraw:float:1 avg:float:1","instantaneous luminosity", "Hz/ub");

 }}//ns interface/bril

#endif
