package xdaq.tools.win;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.*;
import javax.swing.*;

public class SoundFile
{
	AudioClip clip;

	public SoundFile (String file)
	{
		try {
			URL soundFile = new URL ("file:"+file);
			clip = Applet.newAudioClip (soundFile);
		} catch (MalformedURLException e) {
			JOptionPane.showMessageDialog (null,
				"Cannot load sound file " + file + '\n' +
				e.getMessage(),
				"Load Error",
				JOptionPane.ERROR_MESSAGE);
		}
	}

	public void play() { if (clip != null) clip.play(); }
}
