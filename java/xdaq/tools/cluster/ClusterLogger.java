package xdaq.tools.cluster;

import java.lang.Thread;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;
import java.awt.*;
import xdaq.tools.cluster.JoinClusterInputStream;
import xdaq.tools.cluster.ClusterControl;

class LogFrame extends JFrame
{
	JTextArea textPane = null;
	
	public  LogFrame()
	{	
		this.setSize (200,200);
		textPane = new JTextArea(10,40);
		//textPane.setText ("Log messages are displayed here.");
		textPane.setBorder (BorderFactory.createEmptyBorder (10,5,10,5));
		
		//textPane.setFont ( new Font ("Courier", Font.PLAIN, 12));
		//textPane.setEditable (false);
		
		JScrollPane scrollPane = new JScrollPane(textPane);
		
		Container content = this.getContentPane();
		content.setLayout (new BorderLayout());
		content.add (scrollPane, BorderLayout.CENTER);
		
		this.setTitle ("Log Window");
		//this.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
	
	public void write ( String s )
	{
		textPane.append (s);
		int pos = textPane.getDocument().getLength();
		textPane.setCaretPosition(pos);
		textPane.moveCaretPosition(pos);
	}
}

public class ClusterLogger extends Thread {
	ClusterControl cluster = null;
	boolean killed;
	int debug;
	LogFrame logFrame;
	
	JoinClusterInputStream mios;
	public ClusterLogger ( ClusterControl c) {
	   
	   	logFrame = new LogFrame();
		
		
		//System.out.println ("Generated log frame.");
		
		cluster = c;
		killed = false;
		
		InputStream[] s = new InputStream[cluster.istreams.size()];
		for (int i = 0; i < cluster.istreams.size(); i++) {
			s[i] = (InputStream) cluster.istreams.elementAt(i);
		}

		
		mios = new JoinClusterInputStream(s,255);
		this.start();
		//logFrame.write ("Hello World...");
	
	}
	
	
	public void run() {
		byte[] line = null;
		
        	while( (! killed ) ) {
				try { 
				    	line  = new byte[256];
					int len = mios.read(line,0,255);
					String s;
					if (len > 0) {
						s = new String (line, 0, len);
					} else {
						if (len == -1) {
							killed = true;
						}
						s = new String ("closed connection.");
					}
					logFrame.write(s);
				} catch (IOException ex) {
					System.out.println ("CAUGHT AN IOSTREAM EXCEPTION!");
					System.out.println (ex.toString());
				} 
				
					
			}
	
		//System.out.println("Exiting logger...");
	}
	
	public void kill() {
			//System.out.println ("Closing Log window.");
			killed = true;
			logFrame.setVisible(false);
			logFrame.dispose();
	}


}


