#ifndef _tcds_pi_PITTSInfoSpaceUpdater_h_
#define _tcds_pi_PITTSInfoSpaceUpdater_h_

#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace hwlayertca {
    class TCADeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace pi {

    class PITTSInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      PITTSInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                            tcds::hwlayertca::TCADeviceBase const& hw);

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    };

  } // namespace pi
} // namespace tcds

#endif // _tcds_pi_PITTSInfoSpaceUpdater_h_
