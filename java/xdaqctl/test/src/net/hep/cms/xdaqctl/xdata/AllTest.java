package net.hep.cms.xdaqctl.xdata;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.lang.String;

public class AllTest extends TestCase {
	public AllTest(String name) {
		super(name);
	}
	
	public static Test suite() {
		TestSuite suite = new TestSuite();
		
		suite.addTest(new TestSuite(XDataItemTest.class));	
		suite.addTest(new TestSuite(FlashListTest.class));
		
		return suite;
	}
}
