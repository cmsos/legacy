#ifndef _tcds_utils_SOAPCmdDisableRandomTriggers_h_
#define _tcds_utils_SOAPCmdDisableRandomTriggers_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdDisableRandomTriggers : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdDisableRandomTriggers(T& controller);

        /**
         * Sends a single B-command. Bound to the SOAP command DisableRandomTriggers.
         * - SOAP command parameters: none
         * - SOAP command return value: none.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdDisableRandomTriggers.hxx"

#endif // _tcds_utils_SOAPCmdDisableRandomTriggers_h_
