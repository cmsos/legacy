#include "tcds/hwutilsvme/Utils.h"

#include <stdint.h>
#include <string>

#include "tcds/hwlayervme/VMEDeviceBase.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"

void
tcds::hwutilsvme::vmeDeviceHwConnectImpl(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                         tcds::hwlayervme::VMEDeviceBase& hw)
{
  std::string const addressTableFileName = cfgInfoSpace.getString("addressTableFile");
  int const unitNumber = cfgInfoSpace.getUInt32("caenVMEUnitNumber");
  int const chainNumber = cfgInfoSpace.getUInt32("caenVMEChainNumber");
  uint32_t const slotNumber = cfgInfoSpace.getUInt32("slotNumber");

  hw.hwConnect(addressTableFileName, unitNumber, chainNumber, slotNumber);
}
