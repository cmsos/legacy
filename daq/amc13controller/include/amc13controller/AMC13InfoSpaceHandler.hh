#ifndef __AMC13InfoSpaceHandler
#define __AMC13InfoSpaceHandler

#include "xdaq/Application.h"
#include "amc13controller/AMC13BaseInfoSpaceHandler.hh"
#include "d2s/utils/InfoSpaceUpdater.hh"
//#include "amc13controller/DataTracker.hh"

namespace amc13controller {

    class AMC13InfoSpaceHandler : public AMC13BaseInfoSpaceHandler 
    {
    public:
        AMC13InfoSpaceHandler( xdaq::Application *xdaq, utils::InfoSpaceUpdater *updater );
        //void registerTrackerItems( DataTracker &dataTracker );
    };
}

#endif /* __AMC13InfoSpaceHandler */
