package xdaq.xdesk.xtree;

import java.util.EventListener;

public interface PartitionModelListener extends EventListener 
{
	public void partitionChanged(PartitionModelEvent event);
}
