package xdaq.tools.win;

import xdaq.tools.daqlet.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import javax.swing.event.*;

import javax.xml.parsers.*;


import org.xml.sax.SAXException;  
import org.xml.sax.SAXParseException;  
import org.xml.sax.InputSource;
import org.w3c.dom.*;


import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;


import xdaq.tools.win.xdaqWinMenuBar;
import xdaq.tools.win.xdaqDOMDocument;
import xdaq.tools.win.xdaqWinTreeSelectionListener;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.xdaqSOAPException;
import xdaq.tools.win.xdaqWinStatusBar;
import xdaq.tools.win.xdaqSounds;
import xdaq.tools.win.xdaqDOMDocumentException;
import xdaq.tools.win.TidAllocator;

import org.apache.log4j.helpers.LogLog;



public class xdaqWin extends JFrame implements Framework
{
	xdaqWinSplitPane splitPane_;
	xdaqWinMenuBar menuBar_;
	xdaqWinTree tree_;
	xdaqWinStatusBar  statusPane_;
	xdaqSounds sounds_;
	JPanel emptyPanel_;
	DaqletList previousDaqletList_ = null;
	
	DaqletRegistry daqletRegistry_;
	
	public xdaqWin ()
	{
		super ("xdaqWin");
		setSize ( 600, 400);
		LogLog.setQuietMode ( true );
		
		UIManager.put ("AuditoryCues.playList",
			UIManager.get ("AuditoryCues.allAuditoryCues"));
			
		UIManager.put ("MenuItem.commandSound", "./audio/menudown.wav");
		
		// Create sounds;
		sounds_ = new xdaqSounds();
		
		// Create the menubar
		menuBar_ = new xdaqWinMenuBar( this );
		this.getContentPane().add (menuBar_, BorderLayout.NORTH);
		
		// Create split pane
		splitPane_ = new xdaqWinSplitPane();
		
		this.closeConfiguration();
		
		this.getContentPane().add (splitPane_, BorderLayout.CENTER);
		
		statusPane_ = new xdaqWinStatusBar();
		
		this.getContentPane().add (statusPane_, BorderLayout.SOUTH);
		
		statusPane_.setStatus ("Open an XML configuration file to start.", "NONE");
		//statusPane_.setProgressItems (10);
		//statusPane_.advanceProgressBar (4);
		
		this.show();
	}
	
	public void displayEmptyDaqletList() 
	{
		splitPane_.setEmptyDaqletList ();
		previousDaqletList_ = null;
	
	}
	
	// Display the list of Daqlets for an association instance
	//
	public void displayDaqlet (String association, String instance)
	{
		DaqletList t = daqletRegistry_.getDaqletTable (association, instance);
		
		if (previousDaqletList_ != null) previousDaqletList_.stopCurrentDaqlet();
		
		if (t == null)
		{
			splitPane_.setEmptyDaqletList ();
			previousDaqletList_ = null;
		} else 
		{
			previousDaqletList_ = t;
			splitPane_.setDaqletList (t);
			t.displayCurrentDaqlet();
			t.startCurrentDaqlet();
		}
	}
	
	
	xdaqSounds getSounds() { return sounds_; }
	
	public void closeConfiguration ()
	{
		splitPane_.addTreePane ( new JPanel() );
		menuBar_.enable ("Close", false);
		menuBar_.enable ("Save", false);
		menuBar_.enable ("SaveAs", false);
		menuBar_.enable ("Select", false);
		menuBar_.enable ("Script...", false);
		menuBar_.enable ("User command...", false);
	}
	
	TreePath[] getPaths()
	{
		return tree_.getSelectionModel().getSelectionPaths();
	}
	
	// Send a SOAP command to the selected nodes and return the reply as DOM
	//
	// Pass a TreePath[] paths to this command
	//
	public Vector executeCommandOnSelection(String command, org.w3c.dom.Node userNode, TreePath[] paths) 
	{
		Vector replyV = new Vector(); 
		
		//TreePath[] paths = tree_.getSelectionModel().getSelectionPaths();
		
		statusPane_.setProgressItems ( paths.length );
		statusPane_.resetProgressBar();
		
		String errorString = new String();
		
		for (int i = 0; i < paths.length; i++) 
		{
			statusPane_.advanceProgressBar (1);
			
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			String url = null;
			xdaqSOAPMessage msg = null;
			
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();

			try {
				if (node.toString().equals("Host")) 
				{
					url = aNode.getAttribute ("url");
					msg = new xdaqSOAPMessage ( command, "0" );  // executive tid
					msg.addAttribute ("id", aNode.getAttribute ("id"));
					if ( command.equals("Configure"))
						msg.setParameters ( aNode.domNode.getOwnerDocument().getDocumentElement() );
				} 
				else if (node.toString().equals ("Application") || node.toString().equals ("Transport"))
				{
					xdaqWinTreeAdapterNode father = ((xdaqWinTreeAdapterNode) node.getUserObject()).getParent();
					url = father.getAttribute ("url");
					String targetAddress = aNode.getAttribute ("targetAddr");
					msg = new xdaqSOAPMessage ( command, targetAddress );
				}
				// add user parameters
				if ( userNode != null ) {
					msg.setParameters ( userNode);
				}
			
				statusPane_.setStatus ("Connecting: " + url, "NONE");
				SOAPMessage reply = null;
				//try {
					xdaqSOAPConnection con = new xdaqSOAPConnection();
					reply = con.call ( msg, url );
					//System.out.println ("Received reply. Printing it.");
					//reply.writeTo (System.out);
				//} catch (javax.xml.soap.SOAPException e)
				//{
				//	System.out.println ("Exception: " + e.toString());
				//} catch (java.io.IOException ex)
				//{
				//	System.out.println ("Exception: " + ex.toString());
				//}
				
				if (reply != null)
				{
					// Check for a server error
					//
					xdaqSOAPMessage replyMsg = new xdaqSOAPMessage(reply);
					Document document = replyMsg.getDOM();
					replyV.add ( document );
					NodeList faultList = document.getElementsByTagName ("faultstring");
					
					if (faultList.getLength() == 1)
					{
						errorString += url + " (";
						errorString += faultList.item(0).getFirstChild().getNodeValue();
						errorString += ")\n";
						statusPane_.setStatus (url+": "+faultList.item(0).getFirstChild().getNodeValue(), "ERROR");
					}
					else {
						statusPane_.setStatus ("Done", "NONE");
					}
				} else {
					statusPane_.setStatus ("Did not receive reply from " + url, "ERROR");
				}
			} catch (xdaqSOAPException e) {
				errorString += url + ": Cannot connect, are xdaq executives running?";
				errorString += "\n";
				statusPane_.setStatus ("Failed: sending " + command, "ERROR");
			}
		}
		if (!errorString.equals(""))
		{
			JOptionPane.showMessageDialog ( null, errorString, "Configure error", JOptionPane.ERROR_MESSAGE );
		}
		statusPane_.resetProgressBar();
		return replyV;
	}
	
	// Load an XML configuration
	//
	public void displayConfiguration ( String url ) throws xdaqWinException
	{
		statusPane_.setStatus ("Load: " + url, "NONE" );
		statusPane_.setProgressItems ( 5 );
		statusPane_.resetProgressBar();
		statusPane_.advanceProgressBar(1);
		
		try {
			Document document = xdaqDOMDocument.load (url);
			// clear any previous display if any
			this.displayEmptyDaqletList();
			statusPane_.advanceProgressBar(1);
			if (document != null) 
			{
				// Get all plugins
				daqletRegistry_ = new DaqletRegistry ( splitPane_.getDaqletPane(), this );
				NodeList daqlets = document.getElementsByTagName ("Daqlet");				
				
				for (int i = 0; i < daqlets.getLength(); i++)
				{
					Properties daqletProperty = new Properties();
					daqletProperty.setProperty( "shared", "true");
					// possible values are onLoad and onSelection
					daqletProperty.setProperty( "activation", "onLoad");
					// if not specified alway belong to partition
					daqletProperty.setProperty( "association", "Partition");
					daqletProperty.setProperty( "name", "");
					
					Element e = (Element) daqlets.item(i);
					
					NodeList params = daqlets.item(i).getChildNodes();
					for (int j = 0; j < params.getLength(); j++)
					{
						if (params.item(j).getNodeType() != org.w3c.dom.Node.TEXT_NODE)
						{
							Element p = (Element) params.item(j);
							daqletProperty.setProperty(  p.getAttribute ("name"),
									p.getAttribute ("value") );
						}
					}
					
					// mandatory properties
					
					daqletProperty.setProperty( "code", e.getAttribute ("code"));
					daqletProperty.setProperty( "archive", e.getAttribute ("archive"));
					daqletProperty.setProperty( "codebase", e.getAttribute ("codebase"));
					daqletProperty.setProperty( "name", e.getAttribute ("name"));
					Integer daqletClassNumber = new Integer(i);
					daqletProperty.setProperty( "daqletClassNumber", daqletClassNumber.toString());
					daqletRegistry_.loadDaqlet (daqletProperty);
				}

				//
				// Check if any of the targetAddr attributes is "auto"
				// and replace by automatically generated tid
				//
				TidAllocator tidAllocator = new TidAllocator();
				
				NodeList[] l = new NodeList[2];
				l[0] = document.getElementsByTagName ("Application");
				l[1] = document.getElementsByTagName ("Transport");
				
				for (int j = 0; j < l.length; j++) 
				{
					for (int i = 0; i < l[j].getLength(); i++)
					{
						Element e = (Element) l[j].item(i);
						String className = e.getAttribute ("class");
						String instance = e.getAttribute ("instance");
						String targetAddr = e.getAttribute ("targetAddr");

						targetAddr = tidAllocator.allocate ( className, instance, targetAddr );

						e.removeAttribute ("targetAddr");
						e.setAttribute ("targetAddr", targetAddr);						
					}
				}
			
				tree_ = new xdaqWinTree ( document );
				statusPane_.advanceProgressBar(1);
				tree_.addTreeSelectionListener ( new xdaqWinTreeSelectionListener( this ) );
				statusPane_.advanceProgressBar(1);
				splitPane_.addTreePane ( tree_ );
				statusPane_.advanceProgressBar(1);
			
				//menuBar_.enable ("Close", true);
				//menuBar_.enable ("Save", true);
				//menuBar_.enable ("SaveAs", true);
				menuBar_.enable ("Select", true);
				menuBar_.enable ("Script...", true);
				statusPane_.setStatus ("Done", "NONE");
			} else {
				statusPane_.setStatus ("Load failed.", "ERROR");
			}
		} catch (xdaqDOMDocumentException e)
		{
			JOptionPane.showMessageDialog ( null, e.toString(), "Configuration load", JOptionPane.ERROR_MESSAGE );
			statusPane_.setStatus ("Load failed.", "ERROR");
			throw new xdaqWinException();
		} catch (TidAllocatorException e1)
		{
			JOptionPane.showMessageDialog ( null, e1.toString(), "Configuration parse", JOptionPane.ERROR_MESSAGE );
			statusPane_.setStatus ("Load failed.", "ERROR");
			throw new xdaqWinException();
		}
		
		statusPane_.resetProgressBar();
	}
	
	public xdaqWinMenuBar getMenu ()
	{
		return menuBar_;
	}
	
	
	public xdaqWinTree getTree()
	{
		return tree_;
	}
	
	public xdaqWinStatusBar getStatusBar()
	{
		return statusPane_;
	}
	
	 protected EventListenerList listenerList = new EventListenerList();
	 protected HashMap callbackEvents_ = new HashMap();
		  /**
     * Adds an actionListener to the Timer
     */
    public void addActionListener(ActionListener listener) {
        listenerList.add(ActionListener.class, listener);
    }


    /**
     * Removes an ActionListener from the Timer.
     */
    public void removeActionListener(ActionListener listener) {
        listenerList.remove(ActionListener.class, listener);
    }
	
   /**
     * Notify all listeners that have registered interest for
     * notification on this event type.  The event instance 
     * is lazily created using the parameters passed into 
     * the fire method.
     * @see EventListenerList
     */
    public void fireActionPerformed(ActionEvent e) 
    {
    	// Check, if the event has a target Daqlet inside
	// If so, dispatch only to this Daqlet if it is
	// in the list of registered callback events.
	if (e instanceof CallbackReplyEvent)
	{
		String destination = ((CallbackReplyEvent) e).getDestination();
		Vector events = (Vector) callbackEvents_.get (destination);
		if ( events != null)
		{
			// Just take the next best event from the vector
			// and remove it. In the future we might implement
			// A real matching of request/reply events. Important
			// is that the previously enqueued event is dequeued now.
			if (events.size() > 0) 
			{
				CallbackRequestEvent o = (CallbackRequestEvent) events.firstElement();
				events.remove(o);
				if (o.getSource() instanceof ActionListener) {
                			((ActionListener)o.getSource()).actionPerformed(e);
            			} else {
					System.out.println ("Registered callback is not an ActionListener:" + o.getSource().toString() );
				}
				return;
			} else {
				System.out.println ("Empty vector for callback event");
				return;
			}
			
		} else {
			System.out.println ("No destination for callback event found.");
			return;
		}
	}
	
	
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();

        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i=listeners.length-2; i>=0; i-=2) {
            if (listeners[i]==ActionListener.class) {
                ((ActionListener)listeners[i+1]).actionPerformed(e);
            }          
        }
    }
    
    
    /**
    	* This function acts as a callback for action events. Action events
	* that were fired by Daqlets can fall into two groups. Ordinary ones
	* and CallbackRequest events. The latter will rely on a callback after
	* some framework operation has finished. In this case, we do not
	* propagate the reply event as a multicast to all listeners, but only
	* to the one, that fired the event.
     **/
	public void actionPerformed (ActionEvent e)
	{
		if (e instanceof CallbackRequestEvent)
		{
			// Remember the source. A single source may
			// fire multiple events one after the other, so
			// all requests have to be remembered.
			Object source = e.getSource();
			
			if (source instanceof ActionListener)
			{
				System.out.println ("actionPerformed. Source is action listener.");
			} else {
				System.out.println ("actionPerformed. Source is: " + source.toString() );
			}
			
			Integer hash = new Integer(source.hashCode());
			
			// This source (= hashCode) must be passed as a transaction
			// to outgoing SOAP commands. We expect, that they are present
			// in incoming messages that are replies to callbackRequests.
			//
			System.out.println ("Source of callback event: " + hash.toString());
			
			Vector events = (Vector) callbackEvents_.get (hash.toString());
			if ( events == null)
			{
				// create vector for events
				events = new Vector();
				callbackEvents_.put (hash.toString(), events);
			}
			
			events.add (e);
			
		
	
		System.out.println ("xdaqWin, recevied action event: " + e.getActionCommand());
		
		// THIS PARt IS ONLY FOR TESTING PURPOSES!
		
		System.out.println ("firing it back to Daqlets.");
		String destination = new String ( hash.toString() );
		fireActionPerformed ( new CallbackReplyEvent (source, e.getID(), e.getActionCommand(), destination) );
		}
	}
    
	
	

	
}	
