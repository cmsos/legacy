#include "tcds/pi/PIDAQLoop.h"

#include <cmath>
#include <memory>
#include <sstream>
#include <stdint.h>
#include <unistd.h>
#include <vector>

#include "config/PackageInfo.h"
#include "toolbox/BSem.h"
#include "toolbox/string.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/exception/Exception.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/TimeVal.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/pi/PIController.h"
#include "tcds/pi/TCADevicePI.h"
#include "tcds/pi/version.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/LockGuard.h"

tcds::pi::PIDAQLoop::PIDAQLoop(tcds::pi::PIController& piController) :
  piDAQAlarmName_("PIDAQAlarm"),
  piController_(piController),
  daqWorkLoopP_(0),
  workLoopName_(""),
  isTTSHistoryLoggingEnabled_(false),
  lock_(toolbox::BSem::FULL),
  ttcSpyAddressPointer_(0),
  ttsLogAddressPointer_(0),
  isFirstRound_(true)
{
  std::stringstream workLoopName;
  uint32_t const localId = piController_.getApplicationDescriptor()->getLocalId();
  workLoopName << "PIDAQWorkLoop_lid" << localId;
  workLoopName_ = workLoopName.str();
  daqWorkLoopP_ = std::auto_ptr<toolbox::task::WorkLoop>(toolbox::task::getWorkLoopFactory()->getWorkLoop(workLoopName_, "waiting"));
  toolbox::task::ActionSignature* const updateAction =
    toolbox::task::bind(this, &tcds::pi::PIDAQLoop::update, "updateAction");
  daqWorkLoopP_->submit(updateAction);
}

tcds::pi::PIDAQLoop::~PIDAQLoop()
{
  stop();
  daqWorkLoopP_.reset();
}

void
tcds::pi::PIDAQLoop::start(bool const enableTTSHistoryLogging)
{
  // The assumption is that by the time start() is called, the
  // PIController has already connected to the hardware.

  if (!isHwConnected())
    {
      // Raise the alarm.
      std::string const problemDesc =
        toolbox::toString("Failed to start PI DAQ loop. Hardware not connected?");
      XCEPT_DECLARE(tcds::exception::PIDAQFailureAlarm, alarmException, problemDesc);
      piController_.raiseAlarm(piDAQAlarmName_, "error", alarmException);
    }

  //----------

  // Forget what happened until now.
  clearHistory();

  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    piController_.getConfigurationInfoSpaceHandler();

  // Make sure the TTS history and logging settings are correct.
  ttsLogHistory_.updateConfiguration(cfgInfoSpace);

  if (enableTTSHistoryLogging)
    {
      isTTSHistoryLoggingEnabled_ = true;

      config::PackageInfo packageInfo = tcdspi::getPackageInfo();
      std::string const softwareVersion =
        packageInfo.getName() + " " + packageInfo.getLatestVersion();
      tcds::pi::TCADevicePI const& hw = piController_.getHw();
      std::string const firmwareVersion =
        hw.readBoardId() + " " + hw.readSystemId() +
        " " +
        hw.readUserFirmwareVersion() + "/" + hw.readUserFirmwareDate();
      ttsLogWriter_.updateConfiguration(softwareVersion,
                                        firmwareVersion,
                                        cfgInfoSpace);

      // Start the TTS log writer.
      try
        {
          ttsLogWriter_.openLog();
        }
      catch (xcept::Exception const& err)
        {
          // Raise the alarm.
          std::string const problemDesc =
            toolbox::toString("Failed to start logging PI TTS history to file: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::PIDAQFailureAlarm, alarmException, problemDesc);
          piController_.raiseAlarm(piDAQAlarmName_, "error", alarmException);
        }
    }

  if (daqWorkLoopP_.get())
    {
      try
        {
          daqWorkLoopP_->activate();
        }
      catch (toolbox::task::exception::Exception const& err)
        {
          // Raise the alarm.
          std::string const problemDesc =
            toolbox::toString("Failed to start PI TTC/TTS DAQ workloop: '%s'.", err.what());
          XCEPT_DECLARE(tcds::exception::PIDAQFailureAlarm, alarmException, problemDesc);
          piController_.raiseAlarm(piDAQAlarmName_, "error", alarmException);
        }
    }
  else
    {
      // Raise the alarm.
      std::string const problemDesc = "Failed to start PI TTC/TTS DAQ workloop: no workloop available.";
      XCEPT_DECLARE(tcds::exception::PIDAQFailureAlarm, alarmException, problemDesc);
      piController_.raiseAlarm(piDAQAlarmName_, "error", alarmException);
    }
}

void
tcds::pi::PIDAQLoop::stop()
{
  // Cancel our workloop.
  if (daqWorkLoopP_.get() && daqWorkLoopP_->isActive())
    {
      daqWorkLoopP_->cancel();
    }

  if (isTTSHistoryLoggingEnabled_)
    {
      // BUG BUG BUG
      // Should really find a way to ensure that the log is up-to-date before stopping...
      // Stop the TTS log writer.
      ttsLogWriter_.closeLog();
      // BUG BUG BUG
    }

  isTTSHistoryLoggingEnabled_ = false;

  // Revoke a possible left-over alarm.
  piController_.revokeAlarm(piDAQAlarmName_);
}

void
tcds::pi::PIDAQLoop::clearHistory()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  clearTTCSpyHistory();
  clearTTSLogHistory();

  ttcSpyAddressPointer_ = 0;
  ttsLogAddressPointer_ = 0;
  isFirstRound_ = true;

  tcds::pi::TCADevicePI const& hw = piController_.getHw();
  hw.resetTTCSpy();
  hw.resetTTSLog();
}

void
tcds::pi::PIDAQLoop::clearTTCSpyHistory()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  ttcSpyHistory_.clear();
}

void
tcds::pi::PIDAQLoop::clearTTSLogHistory()
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  ttsLogHistory_.clear();
}

bool
tcds::pi::PIDAQLoop::isHwConnected() const
{
  tcds::pi::TCADevicePI const& hw = piController_.getHw();
  bool const tmp = hw.isHwConnected();
  return tmp;
}

bool
tcds::pi::PIDAQLoop::update(toolbox::task::WorkLoop* workLoop)
{
  try
    {
      updateTTCInfo();
      updateTTSInfo();
    }
  catch (tcds::exception::Exception const& err)
    {
      // Raise the alarm.
      std::string const problemDesc =
        toolbox::toString("PI TTC/TTS DAQ workloop failed: '%s'.", err.what());
      XCEPT_DECLARE(tcds::exception::PIDAQFailureAlarm, alarmException, problemDesc);
      piController_.raiseAlarm(piDAQAlarmName_, "error", alarmException);
    }

  //----------

  if (isFirstRound_)
    {
      isFirstRound_ = false;
    }

  //----------

  // Wait a little in order not to completely hog the CPU.
  ::usleep(kLoopRelaxTime);

  // Return true in order to schedule the next iteration.
  return true;
}

void
tcds::pi::PIDAQLoop::updateTTCInfo()
{
  tcds::pi::TCADevicePI const& hw = piController_.getHw();

  //----------

  if (isFirstRound_)
    {
      ttcSpyAddressPointer_ = 0;
    }

  //----------

  // Check if we need to do anything: either if the log is full, or if
  // the address pointer has moved since our last read.
  bool const isTTCSpyFull = hw.isTTCSpyFull();
  std::vector<uint32_t> tmp;
  if (isTTCSpyFull)
    {
      tmp = hw.readTTCSpyRaw(ttcSpyAddressPointer_);
    }
  else
    {
      uint32_t const ttcSpyAddressPointerNew = hw.readTTCSpyAddressPointer();
      // NOTE: We know that the TTC spy runs in single-shot mode, so
      // in principle the thing should not have rolled over. Unless
      // there has been a reset/Configure in between. This latter case
      // we hope to catch by looking for the first loop...
      if (isFirstRound_)
        {
          if (ttcSpyAddressPointerNew != 0)
            {
              uint32_t const first = ttcSpyAddressPointer_;
              uint32_t const last = ttcSpyAddressPointerNew;
              tmp = hw.readTTCSpyRaw(first, last);
            }
        }
      else
        {
          bool const isNewTTCLogData = (ttcSpyAddressPointerNew != ttcSpyAddressPointer_);
          if (isNewTTCLogData)
            {
              uint32_t const first = ttcSpyAddressPointer_;
              uint32_t const last = ttcSpyAddressPointerNew;
              tmp = hw.readTTCSpyRaw(first, last);
            }
        }
      // Store the current address pointer for the next time
      // around.
      ttcSpyAddressPointer_ = ttcSpyAddressPointerNew;
    }
  addTTCData(tmp);

  // If the log is full: reset and start over.
  if (isTTCSpyFull)
    {
      hw.resetTTCSpy();
      ttcSpyAddressPointer_ = 0;
    }
}

void
tcds::pi::PIDAQLoop::updateTTSInfo()
{
  tcds::pi::TCADevicePI const& hw = piController_.getHw();

  //----------

  if (isFirstRound_)
    {
      addTTSComment("Start of TTS DAQ loop");
      ttsLogAddressPointer_ = 0;
    }

  //----------

  // Check if we need to do anything: either if the log is full, or if
  // the address pointer has moved since our last read.
  bool const isTTSLogFull = hw.isTTSLogFull();
  std::vector<uint32_t> tmp;
  if (isTTSLogFull)
    {
      tmp = hw.readTTSLogRaw(ttsLogAddressPointer_);
    }
  else
    {
      uint32_t const ttsLogAddressPointerNew = hw.readTTSLogAddressPointer();
      // NOTE: We know that the TTS logging runs in single-shot mode,
      // so in principle the thing should not have rolled over. Unless
      // there has been a reset/Configure in between. This latter case
      // we hope to catch by looking for the first loop...
      if (isFirstRound_)
        {
          if (ttsLogAddressPointerNew != 0)
            {
              uint32_t const first = ttsLogAddressPointer_;
              uint32_t const last = ttsLogAddressPointerNew;
              tmp = hw.readTTSLogRaw(first, last);
            }
        }
      else
        {
          bool const isNewTTSLogData = (ttsLogAddressPointerNew != ttsLogAddressPointer_);
          if (isNewTTSLogData)
            {
              uint32_t const first = ttsLogAddressPointer_;
              uint32_t const last = ttsLogAddressPointerNew;
              tmp = hw.readTTSLogRaw(first, last);
            }
        }
      // Store the current address pointer for the next time around.
      ttsLogAddressPointer_ = ttsLogAddressPointerNew;
    }
  addTTSData(tmp);

  // If the log is full: reset and start over.
  if (isTTSLogFull)
    {
      uint32_t const numMissedEntries = hw.resetTTSLog();
      std::string comment = "Log was reset after it filled up.";
      if (numMissedEntries == 0)
        {
          comment += " No lost entries.";
        }
      else
        {
          comment += toolbox::toString(" Lost %d entries.", numMissedEntries);
        }
      addTTSComment(comment);
      ttsLogAddressPointer_ = 0;
    }
}

void
tcds::pi::PIDAQLoop::addTTCData(std::vector<uint32_t> const& rawData)
{
  // Step 1: turn raw data into entries.
  std::vector<tcds::pi::TTCSpyEntry> const entries = convertRawTTCSpyData(rawData);

  // Step 2: add entries to the list.
  for (std::vector<TTCSpyEntry>::const_iterator it = entries.begin();
       it != entries.end();
       ++it)
    {
      addTTCEntry(*it);
    }
}

void
tcds::pi::PIDAQLoop::addTTSData(std::vector<uint32_t> const& rawData)
{
  // Step 1: turn raw data into entries.
  std::vector<tcds::pi::TTSLogEntry> const entries = convertRawTTSLogData(rawData);

  // Step 2: add entries to the list.
  for (std::vector<TTSLogEntry>::const_iterator it = entries.begin();
       it != entries.end();
       ++it)
    {
      addTTSEntry(*it);
    }
}

void
tcds::pi::PIDAQLoop::addTTSComment(std::string const& comment)
{
  tcds::pi::TTSLogEntry tmp(comment);
  addTTSEntry(tmp);
}

void
tcds::pi::PIDAQLoop::addTTCEntry(tcds::pi::TTCSpyEntry const& logEntry)
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  ttcSpyHistory_.addEntry(logEntry);
}

void
tcds::pi::PIDAQLoop::addTTSEntry(tcds::pi::TTSLogEntry const& logEntry)
{
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);
  ttsLogHistory_.addEntry(logEntry);
  ttsLogWriter_.addEntry(logEntry);
}

std::vector<tcds::pi::TTCSpyEntry>
tcds::pi::PIDAQLoop::convertRawTTCSpyData(std::vector<uint32_t> const& rawData) const
{
  // First step: process the raw entries that come straight from the
  // firmware logger.
  size_t const numWordsPerEntry = TTCSpyEntryRaw::kNumWordsPerLogEntry;
  size_t const numEntriesRaw = rawData.size() / numWordsPerEntry;
  std::vector<tcds::pi::TTCSpyEntryRaw> entriesRaw;
  entriesRaw.reserve(numEntriesRaw);
  std::vector<uint32_t>::const_iterator it = rawData.begin();
  while (it < rawData.end())
    {
      std::vector<uint32_t> const tmp(it, it + numWordsPerEntry);
      entriesRaw.push_back(TTCSpyEntryRaw(tmp));
      it += numWordsPerEntry;
    }

  // Second step: unpack the raw entries (since each one of those can
  // contain both a trigger and a B-command).
  std::vector<tcds::pi::TTCSpyEntry> entries;
  for (std::vector<TTCSpyEntryRaw>::const_iterator entry = entriesRaw.begin();
       entry != entriesRaw.end();
       ++entry)
    {
      // Check if this raw entry contains an L1A.
      if (entry->isL1A())
        {
          TTCSpyEntry tmp(TTCSpyEntry::TTC_TRAFFIC_TYPE_L1A,
                          entry->orbitNumber(),
                          entry->bxNumber(),
                          entry->hasSngErrStrSet(),
                          entry->hasDblErrStrSet(),
                          entry->hasCommErrorBitSet());
          entries.push_back(tmp);
        }
      // Check if this raw entry contains a short B-command.
      if (entry->isShortCommand())
        {
          TTCSpyEntry tmp(TTCSpyEntry::TTC_TRAFFIC_TYPE_BCOMMAND_SHORT,
                          entry->orbitNumber(),
                          entry->bxNumber(),
                          entry->hasSngErrStrSet(),
                          entry->hasDblErrStrSet(),
                          entry->hasCommErrorBitSet(),
                          entry->shortData());
          entries.push_back(tmp);
        }
      // Check (if it does not contain a short B-command) if this raw
      // entry contains a long B-command.
      else if (entry->isLongCommand())
        {
          TTCSpyEntry tmp(TTCSpyEntry::TTC_TRAFFIC_TYPE_BCOMMAND_LONG,
                          entry->orbitNumber(),
                          entry->bxNumber(),
                          entry->hasSngErrStrSet(),
                          entry->hasDblErrStrSet(),
                          entry->hasCommErrorBitSet(),
                          entry->longData(),
                          entry->longAddress(),
                          entry->longSubAddress(),
                          entry->isLongExternal());
          entries.push_back(tmp);
        }
    }

  if (entries.size() > 0)
    {
      // Third step: (re)sort in order of ascending arrival time.
      std::sort(entries.begin(), entries.end());
    }

  return entries;
}

std::vector<tcds::pi::TTSLogEntry>
tcds::pi::PIDAQLoop::convertRawTTSLogData(std::vector<uint32_t> const& rawData) const
{
  size_t const numWordsPerEntry = TTSLogEntry::kNumWordsPerLogEntry;
  size_t const numEntries = rawData.size() / numWordsPerEntry;
  std::vector<tcds::pi::TTSLogEntry> entries;
  entries.reserve(numEntries);
  std::vector<uint32_t>::const_iterator it = rawData.begin();
  while (it < rawData.end())
    {
      std::vector<uint32_t> const tmp(it, it + numWordsPerEntry);
      entries.push_back(TTSLogEntry(tmp));
      it += numWordsPerEntry;
    }

  return entries;
}

tcds::pi::TTCSpyContents
tcds::pi::PIDAQLoop::ttcSnapshot() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  return ttcSpyHistory_;
}

tcds::pi::TTSLogHistory
tcds::pi::PIDAQLoop::ttsSnapshot() const
{
  // Lock to prevent being interrupted by updates.
  tcds::utils::LockGuard<tcds::utils::Lock> guardedLock(lock_);

  return ttsLogHistory_;
}
