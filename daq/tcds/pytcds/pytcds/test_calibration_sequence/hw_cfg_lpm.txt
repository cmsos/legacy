#----------------------------------------------------------------------
# LPM configuration for tests.
#----------------------------------------------------------------------

#----------------------------------------
# Inputs and sources.
#----------------------------------------
main.inselect.ttcmi_orbit_select                                        0x00000001
main.inselect.cpm_orbit_select                                          0x00000000
main.orbit_alignment_config.orbit_offset                                0x00000001

main.inselect.external_trigger0_enable                                  0x00000000
main.inselect.external_trigger1_enable                                  0x00000000
main.inselect.cyclic_trigger_enable                                     0x00000001
main.inselect.combined_software_trigger_and_bgo_enable                  0x00000001

# Delays for external triggers
main.trigger_delay.external_trigger0_delay                              0x00000000
main.trigger_delay.external_trigger1_delay                              0x00000000

# Random triggers (off).
main.random_trigger_config                                              0x00000000

# Ignore DAQ backpressure.
main.inselect.daq_backpressure_enable                                   0x00000000

#----------------------------------------
# ReTri configuration.
#----------------------------------------
# NOTE: Switching off the ReTri will trigger it to be configured with
# default settings.
main.inselect.retri_enable                                              0x00000000

#----------------------------------------
# Trigger rules according to CMS note 2002/033.
#----------------------------------------
# Not more than one L1As in 3 BX.
trigger_rules.one_trigger_deadtime                                      0x00000002
# Not more than two L1As in 25 BX.
trigger_rules.two_trigger_deadtime                                      0x00000018
# Not more than three L1As in 100 BX.
trigger_rules.three_trigger_deadtime                                    0x00000063
# Not more than four L1As in 240 BX.
trigger_rules.four_trigger_deadtime                                     0x000000ef
trigger_rules.five_trigger_deadtime                                     0x00000000
trigger_rules.six_trigger_deadtime                                      0x00000000
trigger_rules.seven_trigger_deadtime                                    0x00000000
trigger_rules.eight_trigger_deadtime                                    0x00000000
trigger_rules.nine_trigger_deadtime                                     0x00000000

#----------------------------------------
# Cyclic generator 0: LumiNibble B-go.
#----------------------------------------
# NOTE: Off for the moment.
cyclic_generator0.configuration.enabled                                 0x00000000
cyclic_generator0.configuration.initial_prescale                        0x00000000
cyclic_generator0.configuration.pause                                   0x00000000
cyclic_generator0.configuration.permanent                               0x00000001
cyclic_generator0.configuration.postscale                               0x00000000
cyclic_generator0.configuration.prescale                                0x00001000
cyclic_generator0.configuration.repeat_cycle                            0x00000001
cyclic_generator0.configuration.start_bx                                0x00000001
cyclic_generator0.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator0.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator0.configuration.trigger_word.id                         0x00000000
cyclic_generator0.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 1: BC0 B-go.
#----------------------------------------
cyclic_generator1.configuration.enabled                                 0x00000001
cyclic_generator1.configuration.initial_prescale                        0x00000000
cyclic_generator1.configuration.pause                                   0x00000000
cyclic_generator1.configuration.permanent                               0x00000001
cyclic_generator1.configuration.postscale                               0x00000000
cyclic_generator1.configuration.prescale                                0x00000000
cyclic_generator1.configuration.repeat_cycle                            0x00000001
cyclic_generator1.configuration.start_bx                                0x00000001
cyclic_generator1.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator1.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator1.configuration.trigger_word.id                         0x00000001
cyclic_generator1.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 2: StartOfGap B-go.
#----------------------------------------
# NOTE: Off for the moment.
cyclic_generator2.configuration.enabled                                 0x00000000
cyclic_generator2.configuration.initial_prescale                        0x00000000
cyclic_generator2.configuration.pause                                   0x00000000
cyclic_generator2.configuration.permanent                               0x00000001
cyclic_generator2.configuration.postscale                               0x00000000
cyclic_generator2.configuration.prescale                                0x00000000
cyclic_generator2.configuration.repeat_cycle                            0x00000001
cyclic_generator2.configuration.start_bx                                0x00000d76
cyclic_generator2.configuration.trigger_word.bgo_command                0x00000001
cyclic_generator2.configuration.trigger_word.bgo_sequence_command       0x00000000
cyclic_generator2.configuration.trigger_word.id                         0x0000000b
cyclic_generator2.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generator 3: Calibration sequence.
#----------------------------------------
cyclic_generator3.configuration.enabled                                 0x00000001
cyclic_generator3.configuration.initial_prescale                        0x00000000
cyclic_generator3.configuration.pause                                   0x00000000
cyclic_generator3.configuration.permanent                               0x00000000
cyclic_generator3.configuration.postscale                               0x00000000
# Prescale 0x70 = 112 -> rate becomes 100 Hz.
cyclic_generator3.configuration.prescale                                0x00000070
cyclic_generator3.configuration.repeat_cycle                            0x00000001
cyclic_generator3.configuration.start_bx                                0x00000001
cyclic_generator3.configuration.trigger_word.bgo_command                0x00000000
cyclic_generator3.configuration.trigger_word.bgo_sequence_command       0x00000001
cyclic_generator3.configuration.trigger_word.id                         0x00000006
cyclic_generator3.configuration.trigger_word.l1a                        0x00000000

#----------------------------------------
# Cyclic generators 4-14: off.
#----------------------------------------
cyclic_generator4.configuration.enabled                                 0x00000000
cyclic_generator5.configuration.enabled                                 0x00000000
cyclic_generator6.configuration.enabled                                 0x00000000
cyclic_generator7.configuration.enabled                                 0x00000000
cyclic_generator8.configuration.enabled                                 0x00000000
cyclic_generator9.configuration.enabled                                 0x00000000
cyclic_generator10.configuration.enabled                                0x00000000
cyclic_generator11.configuration.enabled                                0x00000000
cyclic_generator12.configuration.enabled                                0x00000000
cyclic_generator13.configuration.enabled                                0x00000000
cyclic_generator14.configuration.enabled                                0x00000000

#----------------------------------------
# Cyclic generator 15: Trigger.
#----------------------------------------
cyclic_generator15.configuration.enabled                                0x00000001
cyclic_generator15.configuration.initial_prescale                       0x00000000
cyclic_generator15.configuration.pause                                  0x00000000
cyclic_generator15.configuration.permanent                              0x00000001
cyclic_generator15.configuration.postscale                              0x00000000
cyclic_generator15.configuration.prescale                               0x00000000
cyclic_generator15.configuration.repeat_cycle                           0x00000001
cyclic_generator15.configuration.start_bx                               0x00000dd5
cyclic_generator15.configuration.trigger_word.bgo_command               0x00000000
cyclic_generator15.configuration.trigger_word.bgo_sequence_command      0x00000000
cyclic_generator15.configuration.trigger_word.id                        0x00000000
cyclic_generator15.configuration.trigger_word.l1a                       0x00000001

#----------------------------------------
# Overall sequence configuration.
#----------------------------------------
bgo_trains.bgo_train_config.firing_bx_calib_trigger                     0x00000dd8
bgo_trains.bgo_train_config.firing_bx_bgo_requests                      0x00000001

#----------------------------------------
# Sequence TTS sensitivity.
#----------------------------------------
bgo_trains.bgo_train_tts_config.bgo_train0                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train1                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train2                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train3                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train4                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train5                              0x00000000
# Sequence #6 is the calibration sequence, which should brake for TTS.
bgo_trains.bgo_train_tts_config.bgo_train6                              0x00000001
bgo_trains.bgo_train_tts_config.bgo_train7                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train8                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train9                              0x00000000
bgo_trains.bgo_train_tts_config.bgo_train10                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train11                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train12                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train13                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train14                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train15                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train16                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train17                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train18                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train19                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train20                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train21                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train22                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train23                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train24                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train25                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train26                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train27                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train28                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train29                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train30                             0x00000000
bgo_trains.bgo_train_tts_config.bgo_train31                             0x00000000

#----------------------------------------------------------------------
# NOTE: Each sequence that disables/pauses triggers, _has to_
# enable/unpause them again at the end of the sequence.
#----------------------------------------------------------------------

#----------------------------------------
# Sequence 0: Start (Resync, OC0, Start, EC0).
#----------------------------------------
# Pause the system and wait 1 orbit.
bgo_trains.bgo_train0_ram                                               0x0001000b
# Send B-go Resync and wait 8 orbits.
bgo_trains.bgo_train0_ram                                               0x00080504
# Send B-go OC0 and wait 1 orbit.
bgo_trains.bgo_train0_ram                                               0x00010804
# Send B-go Start and wait 1 orbit.
bgo_trains.bgo_train0_ram                                               0x00010904
# Send B-go EC0 and wait 1 orbit.
bgo_trains.bgo_train0_ram                                               0x00010704
# Unpause the system.
bgo_trains.bgo_train0_ram                                               0x0000000c
# End-of-sequence.
bgo_trains.bgo_train0_ram                                               0x00000005

#----------------------------------------
# Sequence 1: Stop (Stop).
#----------------------------------------
# Pause the system and wait 1 orbit.
bgo_trains.bgo_train1_ram                                               0x0001000b
# Send B-go Stop.
bgo_trains.bgo_train1_ram                                               0x00000a04
# Unpause the system.
bgo_trains.bgo_train1_ram                                               0x0000000c
# End-of-sequence.
bgo_trains.bgo_train1_ram                                               0x00000005

#----------------------------------------
# Sequence 2: Pause.
#----------------------------------------
# NOTE: Pause is a pure-software action.
# End-of-sequence.
bgo_trains.bgo_train2_ram                                               0x00000005

#----------------------------------------
# Sequence 3: Resume.
#----------------------------------------
# NOTE: Resume is a pure-software action.
# End-of-sequence.
bgo_trains.bgo_train3_ram                                               0x00000005

#----------------------------------------
# Sequence 4: Resync (Resync, EC0).
#----------------------------------------
# Pause the system and wait 8 orbits.
bgo_trains.bgo_train4_ram                                               0x0008000b
# Send B-go Resync and wait 8 orbits.
bgo_trains.bgo_train4_ram                                               0x00080504
# Send B-go EC0 and wait 1 orbit.
bgo_trains.bgo_train4_ram                                               0x00010704
# Unpause the system.
bgo_trains.bgo_train4_ram                                               0x0000000c
# End-of-sequence.
bgo_trains.bgo_train4_ram                                               0x00000005

#----------------------------------------
# Sequence 5: HardReset (HardReset, Resync, EC0).
#----------------------------------------
# Pause the system and wait 255 orbits.
bgo_trains.bgo_train5_ram                                               0x00ff000b
# Send B-go HardReset and wait 1232 orbits.
bgo_trains.bgo_train5_ram                                               0x04d00604
# Send B-go Resync and wait 8 orbits.
bgo_trains.bgo_train5_ram                                               0x00080504
# Send B-go EC0 and wait 1 orbit.
bgo_trains.bgo_train5_ram                                               0x00010704
# Unpause the system.
bgo_trains.bgo_train5_ram                                               0x0000000c
# End-of-sequence.
bgo_trains.bgo_train5_ram                                               0x00000005

#----------------------------------------
# Sequence 6: Calibration (WarningTestEnable, TestEnable, L1A).
#----------------------------------------
# Send B-go WarningTestEnable and wait 1 orbit.
bgo_trains.bgo_train6_ram                                               0x00010d04
# Send B-go TestEnable.
# NOTE: The two-orbit delay is a 'feature.' (It works.)
bgo_trains.bgo_train6_ram                                               0x00020204
# Disable triggers and go into 'calibration-trigger mode.'
bgo_trains.bgo_train6_ram                                               0x00000009
# Send calibration trigger.
bgo_trains.bgo_train6_ram                                               0x00010007
# Enable triggers and wait one orbit before going back to 'normal-trigger mode.'
bgo_trains.bgo_train6_ram                                               0x0000000a
# End-of-sequence.
bgo_trains.bgo_train6_ram                                               0x00000005

#----------------------------------------
# B-go firing BX settings.
#----------------------------------------
# LumiNibble.
bgo_channels.bgo_channel0.firing_bx                                     0x00000100
# BC0.
bgo_channels.bgo_channel1.firing_bx                                     0x00000dd4
# TestEnable.
bgo_channels.bgo_channel2.firing_bx                                     0x00000ce0
# PrivateGap.
bgo_channels.bgo_channel3.firing_bx                                     0x000007d0
# PrivateOrbit.
bgo_channels.bgo_channel4.firing_bx                                     0x00000000
# Resync.
bgo_channels.bgo_channel5.firing_bx                                     0x000007d0
# HardReset.
bgo_channels.bgo_channel6.firing_bx                                     0x000007d0
# EC0.
bgo_channels.bgo_channel7.firing_bx                                     0x000007d0
# OC0.
bgo_channels.bgo_channel8.firing_bx                                     0x000007d0
# Start.
bgo_channels.bgo_channel9.firing_bx                                     0x000007d0
# Stop.
bgo_channels.bgo_channel10.firing_bx                                    0x000007d0
# StartOfGap.
bgo_channels.bgo_channel11.firing_bx                                    0x00000d70
# B-go 12.
bgo_channels.bgo_channel12.firing_bx                                    0x00000000
# WarningTestEnable.
bgo_channels.bgo_channel13.firing_bx                                    0x00000b53
# B-go 14.
bgo_channels.bgo_channel14.firing_bx                                    0x00000000
# B-go 15.
bgo_channels.bgo_channel15.firing_bx                                    0x00000000

bgo_channels.bgo_channel16.firing_bx                                    0x00000000
bgo_channels.bgo_channel17.firing_bx                                    0x00000000
bgo_channels.bgo_channel18.firing_bx                                    0x00000000
bgo_channels.bgo_channel19.firing_bx                                    0x00000000
bgo_channels.bgo_channel20.firing_bx                                    0x00000000
bgo_channels.bgo_channel21.firing_bx                                    0x00000000
bgo_channels.bgo_channel22.firing_bx                                    0x00000000
bgo_channels.bgo_channel23.firing_bx                                    0x00000000
bgo_channels.bgo_channel24.firing_bx                                    0x00000000
bgo_channels.bgo_channel25.firing_bx                                    0x00000000
bgo_channels.bgo_channel26.firing_bx                                    0x00000000
bgo_channels.bgo_channel27.firing_bx                                    0x00000000
bgo_channels.bgo_channel28.firing_bx                                    0x00000000
bgo_channels.bgo_channel29.firing_bx                                    0x00000000

bgo_channels.bgo_channel30.firing_bx                                    0x00000000
bgo_channels.bgo_channel31.firing_bx                                    0x00000000
bgo_channels.bgo_channel32.firing_bx                                    0x00000000
bgo_channels.bgo_channel33.firing_bx                                    0x00000000
bgo_channels.bgo_channel34.firing_bx                                    0x00000000
bgo_channels.bgo_channel35.firing_bx                                    0x00000000
bgo_channels.bgo_channel36.firing_bx                                    0x00000000
bgo_channels.bgo_channel37.firing_bx                                    0x00000000
bgo_channels.bgo_channel38.firing_bx                                    0x00000000
bgo_channels.bgo_channel39.firing_bx                                    0x00000000

bgo_channels.bgo_channel40.firing_bx                                    0x00000000
bgo_channels.bgo_channel41.firing_bx                                    0x00000000
bgo_channels.bgo_channel42.firing_bx                                    0x00000000
bgo_channels.bgo_channel43.firing_bx                                    0x00000000
bgo_channels.bgo_channel44.firing_bx                                    0x00000000
bgo_channels.bgo_channel45.firing_bx                                    0x00000000
bgo_channels.bgo_channel46.firing_bx                                    0x00000000
bgo_channels.bgo_channel47.firing_bx                                    0x00000000
bgo_channels.bgo_channel48.firing_bx                                    0x00000000
bgo_channels.bgo_channel49.firing_bx                                    0x00000000

bgo_channels.bgo_channel50.firing_bx                                    0x00000000
bgo_channels.bgo_channel51.firing_bx                                    0x00000000
bgo_channels.bgo_channel52.firing_bx                                    0x00000000
bgo_channels.bgo_channel53.firing_bx                                    0x00000000
bgo_channels.bgo_channel54.firing_bx                                    0x00000000
bgo_channels.bgo_channel55.firing_bx                                    0x00000000
bgo_channels.bgo_channel56.firing_bx                                    0x00000000
bgo_channels.bgo_channel57.firing_bx                                    0x00000000
bgo_channels.bgo_channel58.firing_bx                                    0x00000000
bgo_channels.bgo_channel59.firing_bx                                    0x00000000

bgo_channels.bgo_channel60.firing_bx                                    0x00000000
bgo_channels.bgo_channel61.firing_bx                                    0x00000000
bgo_channels.bgo_channel62.firing_bx                                    0x00000000
bgo_channels.bgo_channel63.firing_bx                                    0x00000000

#----------------------------------------
# TTS input selection.
#----------------------------------------
main.inselect.tts_enable                                                0x00000000
# NOTE: The per-source enable/disable is determined by the TCDS function manager.

#----------------------------------------
# Trigger bunch mask.
# - Value 0 means 'nothing special in this BX'.
# - Value 1 means 'trigger in this BX'.
# - Value 2 means 'suppress triggers in this BX'.
#----------------------------------------
main.inselect.bunch_mask_enable                                         0x00000000

#----------------------------------------------------------------------
