#ifndef __i2oDFCMsgs_h__
#define __i2oDFCMsgs_h__

#include "i2o/i2o.h"


/**
 * \defgroup ExternalDFCMsgs External DFC I2O message frames
 * \brief <br>
 */

/**
 * \defgroup InternalDFCMsgs Internal DFC I2O message frames
 * \brief <br>
 */


///////////////////////////////////////////////////////////////////////////////
// External Data Flow Controller I2O messages                                //
///////////////////////////////////////////////////////////////////////////////

/**
 * \ingroup ExternalDFCMsgs
 * \brief Sent from DFC to RUI, giving the one or more credits to send
 * trigger or event data to the RUI's EVM or RUI depending on the RUI in
 * question.
 */
typedef struct _I2O_RUI_CREDIT_MESSAGE_FRAME
{
    /**
     * I2O header.
     */
    I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame;

    /**
     * Number of credits.
     */
    U32 nbCredits;

} I2O_RUI_CREDIT_MESSAGE_FRAME, *PI2O_RUI_CREDIT_MESSAGE_FRAME;


///////////////////////////////////////////////////////////////////////////////
// Internal Data Flow Controller I2O messages                                //
///////////////////////////////////////////////////////////////////////////////

/**
 * \ingroup InternalDFCMsgs
 * \brief I2O message used to send a command to the DFC.
 */
typedef struct _I2O_DFC_COMMAND_MESSAGE_FRAME
{

    /**
     * I2O header.
     */
    I2O_PRIVATE_MESSAGE_FRAME PvtMessageFrame;

    /**
     * Id of the partition.
     */
    U32 partitionId;

} I2O_DFC_COMMAND_MESSAGE_FRAME, *PI2O_DFC_COMMAND_MESSAGE_FRAME;


#endif
