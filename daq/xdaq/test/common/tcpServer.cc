#include "sdru.h"
#include "sdevm.h"

extern "C" int Main(char * host)
{
  	TcpDispatcherAcceptorTaskS * d1; 
	XFU * fu ;              
	SDRU * sdru ;          
	SDEVM * sdevm ;        

	Try
	{
		d1 = new TcpDispatcherAcceptorTaskS(host,20300);
		fu = new XFU(d1->getXIO());
		sdru = new SDRU(fu, 100);
		sdevm = new SDEVM(fu, sdru, 100); //local
  		TaskGroup g;
  		d1->initTaskGroup(&g);
		sdevm->initTaskGroup(&g);
  		d1->addListener(sdru);
		d1->addListener(sdevm); //local
    
  		d1->activate();
		sdevm->activate();
		
 		g.join();
	}
	Catch(vxException e)
	{
		cout << e;
		
	};
	delete d1;
	delete fu;
	delete sdru;
	return(0);	
}
#ifndef vxworks
int main(int argc, char* argv[])
{

	return Main(argv[1]);
}
#endif
