#ifndef _tcdsbrildaqchecker_version_h_
#define _tcdsbrildaqchecker_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSBRILDAQCHECKER_VERSION_MAJOR 3
#define TCDSBRILDAQCHECKER_VERSION_MINOR 13
#define TCDSBRILDAQCHECKER_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSBRILDAQCHECKER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSBRILDAQCHECKER_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSBRILDAQCHECKER_VERSION_CODE PACKAGE_VERSION_CODE(TCDSBRILDAQCHECKER_VERSION_MAJOR,TCDSBRILDAQCHECKER_VERSION_MINOR,TCDSBRILDAQCHECKER_VERSION_PATCH)
#ifndef TCDSBRILDAQCHECKER_PREVIOUS_VERSIONS
#define TCDSBRILDAQCHECKER_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSBRILDAQCHECKER_VERSION_MAJOR,TCDSBRILDAQCHECKER_VERSION_MINOR,TCDSBRILDAQCHECKER_VERSION_PATCH)
#else
#define TCDSBRILDAQCHECKER_FULL_VERSION_LIST TCDSBRILDAQCHECKER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSBRILDAQCHECKER_VERSION_MAJOR,TCDSBRILDAQCHECKER_VERSION_MINOR,TCDSBRILDAQCHECKER_VERSION_PATCH)
#endif

namespace tcdsbrildaqchecker
{
  const std::string package = "tcdsbrildaqchecker";
  const std::string versions = TCDSBRILDAQCHECKER_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS BRILDAQChecker.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS BRILDAQChecker.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
