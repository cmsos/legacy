// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini									 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#include "sentinel/arc/utils/AlarmStore.h"

#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <sstream>
#include <iomanip>

#include "toolbox/net/URL.h"
#include "toolbox/Runtime.h"
#include "toolbox/string.h"
#include "toolbox/hexdump.h"
#include "toolbox/task/Guard.h"


#include "sentinel/arc/utils/version.h"

#include "xcept/tools.h"

// Access to this function must be protected to the repositoryLock over the whole catalog query
//
/*
static bool hasInsertedfirstRow_ = false;

void sentinel::arc::utils::AlarmStore::hasInsertedfirstRow (bool b)
{
	hasInsertedfirstRow_ = b;
}
*/
/*
 toolbox::TimeVal sentinel::arc::utils::AlarmStore::outputCatalogJSONExtended (xgi::Output* out, oracle::occi::ResultSet* occiResult) 
 {
 double maxStoreTime = 0.0;
 try
 {
 bool firstRow = true;

 size_t rowCounter = 0;
 while (occiResult->next())
 {
 std::vector < oracle::occi::MetaData > metaData = occiResult->getColumnListMetaData();
 int columnCount = metaData.size();

 rowCounter++;

 // If this is not the first row streamed out, prepend a comma
 if (firstRow)
 *out << "{";
 else
 *out << ",{";

 for (int column = 1; column <= columnCount; column++)
 {
 *out << "\"";
 std::string columnName = toolbox::tolower(metaData[column - 1].getString(oracle::occi::MetaData::ATTR_NAME));
 if (columnName == "datetime")
 columnName = "dateTime";
 else if (columnName == "storetime")
 columnName = "storeTime";
 else if (columnName == "class")
 columnName = "class";
 else if (columnName == "function") columnName = "func";
 *out << columnName;
 *out << "\":\"";
 if (columnName == "storeTime")
 {
 *out << std::fixed << std::setprecision(6) << occiResult->getDouble(column);
 }
 else
 {
 *out << toolbox::jsonquote(occiResult->getString(column));
 }
 *out << "\"";

 // add a comma if there are one or more columns yet to come
 if (column < columnCount) *out << ",";

 // calculate max storeTime

 if (columnName == "storeTime")
 {
 double storeTime = occiResult->getDouble(column);

 if (storeTime > maxStoreTime) maxStoreTime = storeTime;
 //std::cout << " outputCatalogJSON:" <<   std::fixed << std::setprecision(6) << storeTime << " maxStoreTime" <<   std::fixed << std::setprecision(6) << maxStoreTime <<  std::endl;
 }

 // end calculate
 }

 *out << "}";
 firstRow = false;

 // Disable and use row numer in sql query for optimization
 //if ( rowCounter >= maxRowCounter )
 //break;
 }

 }
 catch (oracle::occi::SQLException &e)
 {
 XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to extract column data. ") + e.what());
 }

 try
 {
 toolbox::TimeVal t(maxStoreTime);
 return t;
 }
 catch (toolbox::exception::Exception& e)
 {
 // invalid time if conversion fails
 std::stringstream msg;
 msg << "Failed to convert maxStoreTime:" << maxStoreTime;
 XCEPT_RETHROW(sentinel::arc::utils::exception::Exception, msg.str(), e);
 }

 }
 */

toolbox::TimeVal sentinel::arc::utils::AlarmStore::outputCatalog (sentinel::arc::utils::AlarmStoreListener * listener, oracle::occi::ResultSet* occiResult) 
{

	std::vector < toolbox::Properties > exceptions;
	double maxStoreTime = 0.0;
	try
	{
		size_t rowCounter = 0;
		while (occiResult->next())
		{
			//std::cout << "{" <<std::endl;

			//std::cout << std::endl;
			//std::cout << std::endl;

			std::vector < oracle::occi::MetaData > metaData = occiResult->getColumnListMetaData();
			int columnCount = metaData.size();

			rowCounter++;

			// for efficiency to avoid lots of copying
			toolbox::Properties exception;
			exceptions.push_back(exception);
			toolbox::Properties& ex = exceptions.back();

			ex.setProperty("sentinel-arc-db", this->getName());

			for (int column = 1; column <= columnCount; column++)
			{
				std::string columnName = toolbox::tolower(metaData[column - 1].getString(oracle::occi::MetaData::ATTR_NAME));


				if (columnName == "datetime")
				{
					columnName = "dateTime";
				}
				else if (columnName == "storetime")
				{
					columnName = "storeTime";
				}
				/*
				else if (columnName == "function")
				{
					columnName = "func";
				}
				*/

				// calculate max storeTime
				if (columnName == "storeTime")
				{
					double storeTime = occiResult->getDouble(column);

					if (storeTime > maxStoreTime)
					{
						maxStoreTime = storeTime;
					}
				}

				ex.setProperty(columnName, occiResult->getString(column));

				// calculate max storeTime
				if (columnName == "expiry" || columnName == "storeTime" || columnName == "dateTime")
				{
					double xdaqTime = occiResult->getDouble(column);
					toolbox::TimeVal xdaqTimeT(xdaqTime);
					ex.setProperty(columnName, xdaqTimeT.toString(toolbox::TimeVal::gmt));
					//std::cout << "extracted " << columnName << " as " << ex.getProperty(columnName) << std::endl;
				}
				//std::cout << "'" << columnName << "' :  '" << ex.getProperty(columnName) << "'" << std::endl;


				// end calculate
			}

			//std::cout << "}" <<std::endl;
// Check to see if inner join solve problem
			if ( ! ex.hasProperty("identifier")  )
			{
				std::cerr <<  "*** Panic: could not complete join  exception with uniqueid:" <<  ex.hasProperty("uniqueid") << std::endl;

				exceptions.pop_back();
			}


			// Disable and use row numer in sql query for optimization
			//if ( rowCounter >= maxRowCounter )
			//break;
		}

	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to extract column data. ") + e.what());
	}

	listener->lastStoredEventsCallback(this, exceptions);

	try
	{
		toolbox::TimeVal t(maxStoreTime);
		return t;
	}
	catch (toolbox::exception::Exception& e)
	{
		// invalid time if conversion fails
		std::stringstream msg;
		msg << "Failed to convert maxStoreTime:" << maxStoreTime;
		XCEPT_RETHROW(sentinel::arc::utils::exception::Exception, msg.str(), e);
	}

}

// -- End of SQLite C callback definition

sentinel::arc::utils::AlarmStore::AlarmStore (xdaq::Application * application, const std::string& username, const std::string& password, const std::string& tnsname, bool readOnly, unsigned int scatterReadNum,
		unsigned int maxInitExceptionNum, unsigned int flashbackInitWindow ) 
	: mutex_(toolbox::BSem::FULL,true), application_(application)
{
	name_ = tnsname + "-" + username;
	averageTimeToStore_ = 0.0;
	averageTimeToRetrieveCatalog_ = 0.0;
	averageTimeToRetrieveException_ = 0.0;

	scatterReadNum_ = scatterReadNum;
	maxInitExceptionNum_ = maxInitExceptionNum;
	flashbackInitWindow_ = flashbackInitWindow;

	//maxRowCounter_ = 100;

	// Used for loading entries from Oracle to Cache, incremented when some entries found
	maintenanceSinceTime_ = toolbox::TimeVal::gettimeofday();

	// Initialise Oracle environment
	try
	{
		environment_ = oracle::occi::Environment::createEnvironment(oracle::occi::Environment::DEFAULT);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Error creating environment
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Could not create environment. ") + e.what());
	}

	// Connect to the database
	try
	{
		connection_ = environment_->createConnection(username.c_str(), password.c_str(), tnsname.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToOpen, std::string("Could not connect to database. ") + e.what());
	}

	// Prepare the Oracle db if necessary

	// 2) Prepare tables for write and pre-compile SQL statements for read and write
	// parameter 'true' means read only, 'false' means read and write
	//
	try
	{
		this->prepareDatabase(false);
	}
	catch (sentinel::arc::utils::exception::Exception &e)
	{
		// Error creating tables in Oracle
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToOpen, std::string("Could not prepare database. ") + e.what());
	}

	try
	{
		//lastExceptionTime_ = this->getLatestExceptionTime();
#warning "what is this? we dont know? find out we must!"
		lastExceptionTime_ = toolbox::TimeVal::gettimeofday();
	}
	catch (sentinel::arc::utils::exception::Exception & e)
	{
		// Error connecting
		std::stringstream msg;
		msg << "Failed to retrieve latest exception time";
		XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToOpen, msg.str(), e);
	}
}

std::string sentinel::arc::utils::AlarmStore::getName ()
{
	return name_;
}

sentinel::arc::utils::AlarmStore::~AlarmStore ()
{
	connection_->terminateStatement(insertCatalogStmt_);
	connection_->terminateStatement(insertEventStmt_);
	connection_->terminateStatement(insertExceptionStmt_);
	connection_->terminateStatement(insertApplicationStmt_);
	connection_->terminateStatement(updateEventStmt_);
	environment_->terminateConnection(connection_);
}

void sentinel::arc::utils::AlarmStore::maintenance () 
{

	try
	{
		lastExceptionTime_ = this->getLatestExceptionTime();
		if (lastExceptionTime_ != toolbox::TimeVal::zero())
		{
			maintenanceSinceTime_ = lastExceptionTime_;
		}
	}
	catch (sentinel::arc::utils::exception::Exception & e)
	{
		// Error connecting
		std::stringstream msg;
		msg << "Failed to retrieve latest exception time";
		XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToOpen, msg.str(), e);
	}

}

/* Macro function to prepare the tables */
void sentinel::arc::utils::AlarmStore::prepareDatabase (bool readOnly) 
{
	oracle::occi::Statement* occiStmt = 0;

	bool debugStatements = false;

	std::stringstream estatement;
	estatement << "CREATE TABLE event(uniqueid varchar(37), storeTime double precision, exception varchar(37), source varchar(1024), type varchar(2000), creationTime double precision, PRIMARY KEY (uniqueid))";

	try
	{
		occiStmt = connection_->createStatement(estatement.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Table already exists
		if (debugStatements)
		{
			std::cout << "Statement execution failed : estatement : " << e.what() << std::endl;
		}
	}

	std::stringstream statement;
	statement << "CREATE TABLE catalog(uniqueid varchar(37), dateTime double precision, identifier varchar(80), occurrences varchar(10), notifier varchar(1024), severity varchar(80), message varchar(4000), schema varchar(1024), sessionid varchar(10), tag varchar(80), version varchar(10), module varchar(1024), line varchar(10), func varchar(1024), expiry double precision, args varchar(256), PRIMARY KEY (uniqueid))";

	try
	{
		occiStmt = connection_->createStatement(statement.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Table already exists
		if (debugStatements)
		{
			std::cout << "Statement execution failed : statement : " << e.what() << std::endl;
		}
	}

	// Specialized property tables
	//
	std::stringstream xstatement;
	xstatement << "CREATE TABLE xdaq_application(uniqueid varchar(37), class varchar(1024), instance varchar(10), lid varchar(10), context varchar(1024)," << "groups varchar(1024), service varchar(80), zone varchar(80), uuid varchar(37), PRIMARY KEY (uniqueid))";

	try
	{
		occiStmt = connection_->createStatement(xstatement.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Table already exists
		if (debugStatements)
		{
			std::cout << "Statement execution failed : xstatement : " << e.what() << std::endl;
		}
	}

	// end of specialized property tables

	std::stringstream statement2;
	statement2 << "CREATE TABLE exceptions(uniqueid varchar(37), exception blob, storeTimestamp timestamp default SYSTIMESTAMP NOT NULL, PRIMARY KEY (uniqueid))";

	try
	{
		occiStmt = connection_->createStatement(statement2.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Table already exists
		if (debugStatements)
		{
			std::cout << "Statement execution failed : statement2 : " << e.what() << std::endl;
		}
	}

	// Create index on dateTime for catalog table
	std::stringstream createIndexStatement;
	createIndexStatement << "CREATE INDEX dateTimeIndex ON catalog(dateTime DESC)";

	try
	{
		occiStmt = connection_->createStatement(createIndexStatement.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Index already exists
		if (debugStatements)
		{
			std::cout << "Statement execution failed : createIndexStatement dateTimeIndex : " << e.what() << std::endl;
		}
	}

	// Create index on exception id for catalog table
	std::stringstream createIndexStatementException;
	createIndexStatementException << "CREATE INDEX exceptionIDIndex ON event(exception DESC)";

	try
	{
		occiStmt = connection_->createStatement(createIndexStatementException.str().c_str());
		occiStmt->executeUpdate();
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		// Index already exists
		if (debugStatements)
		{
			std::cout << "Statement execution failed : createIndexStatement exceptionIDIndex : " << e.what() << std::endl;
		}
	}

	std::string insertCatalogStmtStr("insert into catalog (uniqueid,dateTime,identifier,occurrences,notifier,severity,message,schema,sessionid,tag,version,module,line,func,expiry,args) values(:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16)");

	try
	{
		insertCatalogStmt_ = connection_->createStatement(insertCatalogStmtStr.c_str());
		insertCatalogStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to prepare insert statement for catalog table. ") + e.what());
	}

	std::string insertEventStmtStr("insert into event (uniqueid,storeTime,exception,source,type,creationTime) values(:1,:2,:3,:4,:5,:6)");

	try
	{
		insertEventStmt_ = connection_->createStatement(insertEventStmtStr.c_str());
		insertEventStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to prepare insert statement for event table. ") + e.what());
	}

	std::string insertApplicationStmtStr("insert into xdaq_application (uniqueid,class,instance,lid,context,groups,service,zone,uuid) values(:1,:2,:3,:4,:5,:6,:7,:8,:9)");

	try
	{
		insertApplicationStmt_ = connection_->createStatement(insertApplicationStmtStr.c_str());
		insertApplicationStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to prepare insert statement for xdaq_application table. ") + e.what());
	}

	std::string insertExceptionsStr("insert into exceptions(uniqueid, exception) values(:1, :2)");

	try
	{
		insertExceptionStmt_ = connection_->createStatement(insertExceptionsStr.c_str());
		insertExceptionStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to prepare insert statement for exceptions table. ") + e.what());
	}

	// 1: storetime, 2: exceptionid, 3: type
	std::stringstream updateEventStr;
	updateEventStr << "update event SET storetime=:1 WHERE uniqueid=:2";

	try
	{
		updateEventStmt_ = connection_->createStatement(updateEventStr.str().c_str());
		updateEventStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to prepare insert statement for exceptions table. ") + e.what());
	}

	// 1: typeon, 2: typeoff, 3: time
	std::stringstream openRulesStr;
	openRulesStr << "SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid) ";
	openRulesStr << "WHERE event.type=:1  ";
	openRulesStr << "AND event.creationtime < :3  ";
	openRulesStr << "AND catalog.expiry > :3  ";
	openRulesStr << "AND event.exception NOT IN (SELECT exception FROM event WHERE type=:2 AND creationtime < :3)";

	try
	{
		openRulesStmt_ = connection_->createStatement(openRulesStr.str().c_str());
		openRulesStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to prepare select statement for open rules") + e.what());
	}

	// 1: time
	std::stringstream openExceptionsStr;
	openExceptionsStr << "SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid)  ";
	openExceptionsStr << "WHERE event.type='" << sentinel::arc::utils::EVENT_FIRE_STR << "'  ";
	openExceptionsStr << "AND event.creationtime < :1  ";
	openExceptionsStr << "AND event.exception NOT IN (SELECT exception FROM event WHERE type='" << sentinel::arc::utils::EVENT_REVOKE_STR << "' AND creationtime < :1)";

	try
	{
		openExceptionsStmt_ = connection_->createStatement(openExceptionsStr.str().c_str());
		openExceptionsStmt_->setAutoCommit(true);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to prepare select statement for open exceptions") + e.what());
	}
}

/*
 Storage of an exception into the catalog and schema specific paramaters
 */
void sentinel::arc::utils::AlarmStore::storeNewException (toolbox::Properties& properties, const std::string & blob) 
{
	// Event insert preparation
	// { uniqueid , storeTime , exception , source, type}

	//LOMB-BEGIN
	std::string uniqueid = properties.getProperty("uniqueid");

	try
	{
		this->storeNewEvent(sentinel::arc::utils::EVENT_FIRE_STR, properties);
	}
	catch (sentinel::arc::utils::exception::FailedToStore& e)
	{
		XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToStore, "Failed to store event for new exception", e);
	}
	catch (sentinel::arc::utils::exception::ConstraintViolated& e)
	{
		XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToStore, "Failed to store event for new exception", e);
	}

	// Full exception prperties into blob
	try
	{
		this->writeBlob(uniqueid, blob);
	}
	catch (sentinel::arc::utils::exception::FailedToStore& e)
	{
		std::stringstream msg;
		msg << "Failed to store blob into archive";
		XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToStore, msg.str(), e);
	}
	//LOMB-END

	// Catalog insert preparation
	// { uniqueid,dateTime,identifier,occurrences,notifier,severity,message,schema,sessionid,tag,version }
	//

	std::string propschema = properties.getProperty("qualifiedErrorSchemaURI");
	if (propschema == "")
	{
		properties.setProperty("qualifiedErrorSchemaURI", properties.getProperty("schema"));
		propschema = properties.getProperty("schema");
	}

	this->storeNewCatalogEntry(properties);

	//std::cout << "QualifiedErrorSchemaURI: " << properties.getProperty("qualifiedErrorSchemaURI") << std::endl;

	// This exception is qualified as a XDAQ application exception, therefore add specific schema property into xdaq-application table
	if (propschema == "http://xdaq.web.cern.ch/xdaq/xsd/2005/QualifiedSoftwareErrorRecord-10.xsd")
	{

		this->storeNewXDAQEntry(uniqueid, properties);

	}

	//AP add auto commit
	//connection_->commit();
}
void sentinel::arc::utils::AlarmStore::storeNewXDAQEntry (const std::string& uniqueid, toolbox::Properties& properties) 
{
	std::string propclass = properties.getProperty("urn:xdaq-application:class");
	if (propclass == "")
	{
		propclass = properties.getProperty("class");
	}

	std::string propinstance = properties.getProperty("urn:xdaq-application:instance");
	if (propinstance == "")
	{
		propinstance = properties.getProperty("instance");
	}

	std::string propid = properties.getProperty("urn:xdaq-application:id");
	if (propid == "")
	{
		propid = properties.getProperty("lid");
	}

	std::string propcontext = properties.getProperty("urn:xdaq-application:context");
	if (propcontext == "")
	{
		propcontext = properties.getProperty("context");
	}

	std::string propgroup = properties.getProperty("urn:xdaq-application:group");
	if (propgroup == "")
	{
		propgroup = properties.getProperty("groups");
	}

	std::string propservice = properties.getProperty("urn:xdaq-application:service");
	if (propservice == "")
	{
		propservice = properties.getProperty("service");
	}

	std::string propzone = properties.getProperty("urn:xdaq-application:zone");
	if (propzone == "")
	{
		propzone = properties.getProperty("zone");
	}

	std::string propuuid = properties.getProperty("urn:xdaq-application:uuid");
	if (propuuid == "")
	{
		propuuid = properties.getProperty("uuid");
	}

	try
	{
		insertApplicationStmt_->setString(1, uniqueid.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind uniqueid value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(2, propclass.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:class value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(3, propinstance.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:instance value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(4, propid.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:id value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(5, propcontext.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:context value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(6, propgroup.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:group value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(7, propservice.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:service value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(8, propzone.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:zone value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->setString(9, propuuid.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind urn:xdaq-application:uuid value to insert xdaq_application statement. ") + e.what());
	}

	try
	{
		insertApplicationStmt_->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		std::stringstream msg;
		msg << "Failed to execute insert application statement - " << e.what();
		msg << ", uniqueid: " << uniqueid;
		msg << ", class: " << propclass;
		msg << ", instance: " << propinstance;
		msg << ", id: " << propid;
		msg << ", context: " << propcontext;
		msg << ", group: " << propgroup;
		msg << ", service: " << propservice;
		msg << ", zone: " << propzone;
		msg << ", uuid(xdaq application): " << propuuid;

		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, msg.str());
	}
}

//luciano
void sentinel::arc::utils::AlarmStore::storeNewCatalogEntry (toolbox::Properties& properties) 
{
	std::string uniqueid = properties.getProperty("uniqueid");
	try
	{
		insertCatalogStmt_->setString(1, uniqueid.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind uniqueid value to insert catalog statement. ") + e.what());
	}

	/*
	 std::stringstream dateTimeStream;
	 dateTimeStream << properties.getProperty("dateTime");
	 double dateTime;
	 dateTimeStream >> dateTime;
	 */

	double dateTime = 0;
	if (properties.getProperty("dateTime") != "")
	{
		toolbox::TimeVal dateTimeT;
		dateTimeT.fromString(properties.getProperty("dateTime"), "", toolbox::TimeVal::gmt);
		dateTime = (double) dateTimeT;
	}

	try
	{
		insertCatalogStmt_->setDouble(2, dateTime);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind timestamp value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(3, properties.getProperty("identifier").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind identifier value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(4, properties.getProperty("occurrences").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind occurences value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(5, properties.getProperty("notifier").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind notifier value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(6, properties.getProperty("severity").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind severity value to insert catalog statement. ") + e.what());
	}

	std::string encodedMessage = this->escape(properties.getProperty("message"));

	if (encodedMessage.size() > 4000)
	{
		/*	 debug

		 std::cout << "Dump message isize: " << encodedMessage.size() << " dump:";
		 toolbox::hexdump((void*)encodedMessage.c_str(),encodedMessage.size());
		 std::cout << " <- end of dump" << std::endl;;
		 */
		encodedMessage.resize(4000);
		std::cerr << "warning: message size too large for database catalog (full message stored in BLOB )" << std::endl;
	}

	try
	{
		insertCatalogStmt_->setString(7, encodedMessage.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind message value to insert catalog statement. ") + e.what());
	}

	std::string propschema = properties.getProperty("qualifiedErrorSchemaURI");

	if (propschema == "")
	{
		propschema = properties.getProperty("schema");
	}

	try
	{
		insertCatalogStmt_->setString(8, propschema.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind schema value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(9, properties.getProperty("sessionID").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind sessionid value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(10, properties.getProperty("tag").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind tag value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(11, properties.getProperty("version").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind version value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(12, properties.getProperty("module").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind module value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(13, properties.getProperty("line").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind line value to insert catalog statement. ") + e.what());
	}

	std::string funcName = properties.getProperty("function");

	if (funcName == "")
	{
		funcName = properties.getProperty("func");
	}

	try
	{
		insertCatalogStmt_->setString(14, funcName.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind func value to insert catalog statement. ") + e.what());
	}

	try
	{
		//std::cout << "inserting into database : " << properties.getProperty("expiry") << std::endl;

		if (properties.getProperty("expiry") == "")
		{
			insertCatalogStmt_->setDouble(15, 0);
		}
		else
		{
			toolbox::TimeVal expiryT;
			expiryT.fromString(properties.getProperty("expiry"), "", toolbox::TimeVal::gmt);
			double expiry = (double) expiryT;

			//std::cout << "inserting : " << (double) expiryT << std::endl;

			//insertCatalogStmt_->setString(15, properties.getProperty("expiry").c_str());
			insertCatalogStmt_->setDouble(15, expiry);
		}
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind func value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->setString(16, properties.getProperty("args").c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind func value to insert catalog statement. ") + e.what());
	}

	try
	{
		insertCatalogStmt_->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		std::stringstream msg;
		msg << "Failed to execute insert caalog statement - " << e.what();
		msg << "properties are:";
		msg << "dateTime: " << properties.getProperty("dateTime");
		msg << ", notifier: " << properties.getProperty("notifier");
		msg << ", uniqueid: " << uniqueid;
		msg << ", identfier: " << properties.getProperty("identifier");
		msg << ", severity: " << properties.getProperty("severity");
		msg << ", message: " << properties.getProperty("message");
		msg << ", sessionid: " << properties.getProperty("sessionID");
		msg << ", schema: " << properties.getProperty("qualifiedErrorSchemaURI");
		msg << ", tag: " << properties.getProperty("tag");
		msg << ", module: " << properties.getProperty("module");
		msg << ", line: " << properties.getProperty("line");
		msg << ", func: " << properties.getProperty("function");
		msg << ", version: " << properties.getProperty("version");

		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, msg.str());
	}
}

void sentinel::arc::utils::AlarmStore::storeNewEvent (const std::string & type, toolbox::Properties& properties) 
{
// Event insert preparation
// { uniqueid , storeTime , exception , source, type = "revoke"}

// create UUID on the fly
	toolbox::net::UUID euid;
	std::string event_uuid = euid.toString();

	try
	{
		insertEventStmt_->setString(1, event_uuid.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind euid value to insert event statement. ") + e.what());
	}

	/*
	 std::stringstream storeTimeStream;
	 storeTimeStream << properties.getProperty("storeTime");
	 double lastExceptionTime;
	 storeTimeStream >> lastExceptionTime;
	 */

	toolbox::TimeVal storeTimeT;
	storeTimeT.fromString(properties.getProperty("storeTime"), "", toolbox::TimeVal::gmt);
	double lastExceptionTime = (double) storeTimeT;

	try
	{
		insertEventStmt_->setDouble(2, lastExceptionTime);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
	}

	std::string exception_uuid = properties.getProperty("uniqueid");	// from exception

	try
	{
		insertEventStmt_->setString(3, exception_uuid.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind euid(uuid) value to insert event statement. ") + e.what());
	}

	std::string source = properties.getProperty("source");	// event source

	try
	{
		insertEventStmt_->setString(4, source.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind source value to insert event statement. ") + e.what());
	}

	try
	{
		insertEventStmt_->setString(5, type.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to bind type value to insert event statement. ") + e.what());
	}

	try
	{
		insertEventStmt_->setDouble(6, lastExceptionTime);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
	}

	try
	{
		insertEventStmt_->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		std::stringstream msg;
		msg << "Failed to execute insert event statement - " << e.what();
		msg << ", event_uuid: " << event_uuid;
		msg << ", storeTime: " << properties.getProperty("storeTime");
		msg << ", exception(uniqueid): " << exception_uuid;
		msg << ", source: " << properties.getProperty("source");
		msg << ", type: " << type;

		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, msg.str());
	}
}

void sentinel::arc::utils::AlarmStore::accept (std::string const & exception, std::string const & source) 
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

//std::cout << "rearm " << exception << " source " << source  << std::endl;
// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();

	toolbox::Properties properties;

	properties.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));

// for fire event this identify the originator
	properties.setProperty("source", source);
	properties.setProperty("uniqueid", exception);

	this->storeNewEvent(sentinel::arc::utils::EVENT_ACCEPT_STR, properties);

	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double) (stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double) (stop - lastExceptionTime_)) / 2;
	}
}

void sentinel::arc::utils::AlarmStore::clear (std::string const & exception, std::string const & source) 
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

//std::cout << "rearm " << exception << " source " << source  << std::endl;
// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();

	toolbox::Properties properties;

	properties.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));

// for fire event this identify the originator
	properties.setProperty("source", source);
	properties.setProperty("uniqueid", exception);

	this->storeNewEvent(sentinel::arc::utils::EVENT_CLEAR_STR, properties);

	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double) (stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double) (stop - lastExceptionTime_)) / 2;
	}
}

void sentinel::arc::utils::AlarmStore::revoke (xcept::Exception& ex) 
{
// This is the store time (not the exception time)
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();

// FORMAT
//"error":{"field1":"value1", "field2":"value2", ... ,"error":{...}}
//
	toolbox::Properties properties;

	properties.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));

// for fire event this identify the originator
	properties.setProperty("source", ex.getProperty("notifier"));

	std::vector < xcept::ExceptionInformation > &history = ex.getHistory();
	std::vector<xcept::ExceptionInformation>::reverse_iterator ri = history.rbegin();
	std::stringstream blob;
	while (ri != history.rend())
	{
		std::map<std::string, std::string, std::less<std::string> >& p = (*ri).getProperties();
		std::map<std::string, std::string, std::less<std::string> >::iterator mi;
		mi = p.begin();
		while (mi != p.end())
		{
			// if leading exception then keep properties
			if (ri == history.rbegin())
			{
				properties.setProperty((*mi).first, (*mi).second);
			}
			++mi;
		}
		++ri;
	}

	this->storeNewEvent(sentinel::arc::utils::EVENT_REVOKE_STR, properties);

	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double) (stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double) (stop - lastExceptionTime_)) / 2;
	}
}

/*
 */
// andyfire
void sentinel::arc::utils::AlarmStore::fire (xcept::Exception& ex) 
{
	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();

//LOMB-BEGIN
	std::string uniqueid = ex.getProperty("uniqueid");

	std::stringstream timeStream;
	timeStream << std::fixed << std::setprecision(6) << (double) lastExceptionTime_;
	std::string timeStr = timeStream.str();

	std::pair < std::string, std::string > lastEvent = this->getLastEvent(uniqueid, timeStr);

	if (lastEvent.second == EVENT_FIRE_STR)
	{
		try
		{

			toolbox::net::UUID eventUUID;

			//std::cout << "found in catalog, updating" << std::endl;
			//std::cout << "ALARM STORE updating storeTime to " << (double) lastExceptionTime_ << std::endl;

			// 1: storetime, 2: exceptionid, 3: type

			try
			{
				// current time (used as creationtime and store time everywhere in query)
				updateEventStmt_->setDouble(1, (double) lastExceptionTime_);
			}
			catch (oracle::occi::SQLException &e)
			{
				XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to bind storeTime value to insert event statement. ") + e.what());
			}

			try
			{
				// event id
				updateEventStmt_->setString(2, lastEvent.first.c_str());
			}
			catch (oracle::occi::SQLException &e)
			{
				XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to bind euid value to insert event statement. ") + e.what());
			}

			//std::cout << updateEventStr.str() << std::endl;

			try
			{
				updateEventStmt_->executeUpdate();
			}
			catch (oracle::occi::SQLException &e)
			{
				std::stringstream msg;
				msg << "Failed to execute update event statement - " << e.what();

				XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, msg.str());
			}

		}
		catch (sentinel::arc::utils::exception::Exception &e)
		{
			XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToStore, "Failed to fire exception", e);
		}
	}
	else if (lastEvent.second == EVENT_CLEAR_STR)
	{
		// fire event only
		toolbox::Properties properties;

		properties.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));
		properties.setProperty("uniqueid", uniqueid);
		properties.setProperty("source", ex.getProperty("notifier"));

		try
		{
			this->storeNewEvent(sentinel::arc::utils::EVENT_FIRE_STR, properties);
		}
		catch (sentinel::arc::utils::exception::FailedToStore& e)
		{
			XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToStore, "Failed to store event for new exception", e);
		}
		catch (sentinel::arc::utils::exception::ConstraintViolated& e)
		{
			XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToStore, "Failed to store event for new exception", e);
		}
	}
	else
	{
		try
		{

			toolbox::Properties properties;

			std::stringstream version;
			version << SENTINELARCUTILS_VERSION_MAJOR << "." << SENTINELARCUTILS_VERSION_MINOR << "." << SENTINELARCUTILS_VERSION_PATCH;
			properties.setProperty("version", version.str());

			properties.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));

			//std::cout << "generate: " << properties.getProperty("storeTime") << std::endl;

			// for fire event this identify the originator
			properties.setProperty("source", ex.getProperty("notifier"));

			std::vector < xcept::ExceptionInformation > &history = ex.getHistory();
			std::vector<xcept::ExceptionInformation>::reverse_iterator ri = history.rbegin();
			std::stringstream blob;
			blob << "{";
			while (ri != history.rend())
			{
				std::map<std::string, std::string, std::less<std::string> >& p = (*ri).getProperties();
				blob << "\"label\":\"" << p["identifier"] << "\",";
				blob << "\"properties\":[";
				std::map<std::string, std::string, std::less<std::string> >::iterator mi;
				mi = p.begin();
				while (mi != p.end())
				{
					// if leading exception then keep properties
					if (ri == history.rbegin())
					{
						properties.setProperty((*mi).first, (*mi).second);

					}
					//
					if ((*mi).first == "message")
					{
						blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" << this->escape((*mi).second) << "\"}";
					}
					else
					{
						blob << "{\"name\":\"" << (*mi).first << "\",\"value\":\"" << toolbox::jsonquote((*mi).second) << "\"}";
					}

					++mi;
					if (mi != p.end())
					{
						blob << ",";
					}
				}
				++ri;
				blob << "]";
				if (ri != history.rend())
				{
					blob << ",\"children\":[{";
				}
			}

			for (size_t s = 0; s < (history.size() - 1); ++s)
			{
				blob << "}]";
			}
			blob << "}";

			this->storeNewException(properties, blob.str());
			//////////

		}
		catch (sentinel::arc::utils::exception::Exception &e)
		{
			XCEPT_RETHROW(sentinel::arc::utils::exception::FailedToStore, "Failed to fire exception", e);
		}
	}

	toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

	if (averageTimeToStore_ == 0.0)
	{
		// Initialize running average
		averageTimeToStore_ = (double) (stop - lastExceptionTime_);
	}
	else
	{
		// Calculate running average
		averageTimeToStore_ = (averageTimeToStore_ + (double) (stop - lastExceptionTime_)) / 2;
	}
}

bool sentinel::arc::utils::AlarmStore::hasException (const std::string& uuid)
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::stringstream zSQL;

	try
	{
		zSQL << "select count(*) from exceptions where uniqueid='" << uuid << "'";

		oracle::occi::Statement* occiStmt = 0;

		occiStmt = connection_->createStatement(zSQL.str().c_str());
		oracle::occi::ResultSet* result = occiStmt->executeQuery();

		int count = 0;

		if (result->next())
		{
			count = result->getInt(1);
		}

		occiStmt->closeResultSet(result);
		connection_->terminateStatement(occiStmt);

		if (count > 0) return true;
	}
	catch (oracle::occi::SQLException &e)
	{
		// not found ignore and let return false
	}

	return false;
}

// check if exception alreadi exists
bool sentinel::arc::utils::AlarmStore::inCatalog (const std::string& zKey) 
{
	std::stringstream zSQL;

	zSQL << "select count(*) from catalog where uniqueid='" << zKey << "'";

	int count = 0;

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;
	try
	{

		occiStmt = connection_->createStatement(zSQL.str());
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			count = occiResult->getInt(1);
		}

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to count from catalog table. ") + e.what());
	}

	return count > 0;
}

// check if exception alreadi exists
std::pair<std::string, std::string> sentinel::arc::utils::AlarmStore::getLastEvent (const std::string& uuid, const std::string& time) 
{
	std::stringstream zSQL;

	zSQL << "SELECT uniqueid,type FROM (SELECT * FROM event where (exception='" << uuid << "' AND creationtime<=" << time << " AND (type='" << sentinel::arc::utils::EVENT_CLEAR_STR << "' OR type='" << sentinel::arc::utils::EVENT_FIRE_STR << "')) ORDER BY creationtime DESC) WHERE ROWNUM <= 1";

	std::pair < std::string, std::string > result;
	result.first = EVENT_NULL_STR;
	result.second = EVENT_NULL_STR;

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;
	try
	{

		occiStmt = connection_->createStatement(zSQL.str());
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			result.first = occiResult->getString(1);
			result.second = occiResult->getString(2);
		}

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to count from catalog table. ") + e.what());
	}

	return result;
}

bool sentinel::arc::utils::AlarmStore::ruleOnExists (const std::string& ruleUUID, const std::string& ruleType) 
{
	std::stringstream zSQL;

	zSQL << "select count(*) from event where exception='" << ruleUUID << "' AND type='" << ruleType << "'";

	int count = 0;

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;
	try
	{

		occiStmt = connection_->createStatement(zSQL.str());
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			count = occiResult->getInt(1);
		}

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to count from catalog table. ") + e.what());
	}

	return count > 0;
}

/*
 void sentinel::arc::utils::AlarmStore::retrieve (const std::string& uuid, const std::string& format, xgi::Output* out) 
 {
 toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();

 try
 {
 std::string jsonException;
 this->readBlob(uuid, jsonException);
 *out << jsonException;
 }
 catch (sentinel::arc::utils::exception::FailedToRead& e)
 {
 std::stringstream msg;
 msg << "Failed to read exception '" << uuid << "'";
 XCEPT_RETHROW(sentinel::arc::utils::exception::NotFound, msg.str(), e);
 }

 toolbox::TimeVal stop = toolbox::TimeVal::gettimeofday();

 if (averageTimeToRetrieveException_ == 0.0)
 {
 // Initialize running average
 averageTimeToRetrieveException_ = (double) (stop - start);
 }
 else
 {
 // Calculate running average
 averageTimeToRetrieveException_ = (averageTimeToRetrieveException_ + (double) (stop - start)) / 2;
 }
 }
 */
void sentinel::arc::utils::AlarmStore::catalog (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format) 
{
	/*
	 toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();

	 std::stringstream selectCatalogStmt;
	 // Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	 // make a full precision time comparison afterwards

	 selectCatalogStmt << "select * from catalog where dateTime<=" << std::fixed << std::setprecision(6) << (double) end;
	 selectCatalogStmt << " and dateTime>=" << (double) start;

	 oracle::occi::Statement* occiStmt = 0;
	 try
	 {
	 occiStmt = connection_->createStatement(selectCatalogStmt.str().c_str());
	 }
	 catch (oracle::occi::SQLException &e)
	 {
	 XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	 }

	 oracle::occi::ResultSet* result = 0;

	 try
	 {
	 result = occiStmt->executeQuery();
	 }
	 catch (oracle::occi::SQLException &e)
	 {
	 XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	 }

	 this->outputCatalogJSON(out, result); // return a maximum of 1000 exceptions

	 //ttStmt_.Close(ttstatus)
	 occiStmt->closeResultSet(result);
	 connection_->terminateStatement(occiStmt);

	 // Assume synchronous processing of the callback
	 //
	 toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();

	 if (averageTimeToRetrieveCatalog_ == 0.0)
	 {
	 // Initialize running average
	 averageTimeToRetrieveCatalog_ = (double) (stopChrono - startChrono);
	 }
	 else
	 {
	 // Calculate running average
	 averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double) (stopChrono - startChrono)) / 2;
	 }
	 */
}

//

//
toolbox::TimeVal sentinel::arc::utils::AlarmStore::lastStoredEvents (sentinel::arc::utils::AlarmStoreListener * listener, toolbox::TimeVal & since) 
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();
	toolbox::TimeVal t;

	std::stringstream selectCatalogStmt;
// Format specification needed in order to prefent +eXX scientific formatting that does now allow to
// make a full precision time comparison afterwards

	//std::cout << "ALARM STORE since = " << std::fixed << std::setprecision(6) << (double) since << std::endl;
	selectCatalogStmt << "select * from (select event.*,dateTime,identifier,occurrences,notifier,severity,message" << ",schema,sessionid,tag,version,class,instance,lid,context,groups,service,zone,uuid,expiry,args,func,line,module" << " from event inner join catalog on (event.exception=catalog.uniqueid) " << " inner join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid) " << " where event.storeTime>" << std::fixed << std::setprecision(6) << (double) since << " ORDER BY event.storeTime) where rownum <= " << scatterReadNum_;

    //std::cout << selectCatalogStmt.str() << std::endl;

// Assume synchronous processing of the callback
//
	oracle::occi::Statement* occiStmt = 0;

	try
	{
		occiStmt = connection_->createStatement(selectCatalogStmt.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	}

	oracle::occi::ResultSet* occiResult = 0;
	try
	{
		occiResult = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	}

	t = this->outputCatalog(listener, occiResult);

	if (t == toolbox::TimeVal::zero())
	{
		t = since;
	}

	occiStmt->closeResultSet(occiResult);
	connection_->terminateStatement(occiStmt);

	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();

	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double) (stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double) (stopChrono - startChrono)) / 2;
	}

	return t;
}

toolbox::TimeVal sentinel::arc::utils::AlarmStore::initialize (sentinel::arc::utils::AlarmStoreListener * listener, toolbox::TimeVal & since) 
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	toolbox::TimeInterval exceptionWindow(flashbackInitWindow_,0);
	toolbox::TimeVal flashback;
	flashback = since - exceptionWindow;

	std::stringstream timeWW;
	timeWW << std::fixed << std::setprecision(6) << (double) flashback;
	std::string window = timeWW.str();

	toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();

	std::stringstream timeSS;
	timeSS << std::fixed << std::setprecision(6) << (double) since;
	std::string time = timeSS.str();

	std::stringstream openShelfRulesStr;
	openShelfRulesStr << "SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid) ";
	openShelfRulesStr << "WHERE event.type='" << EVENT_SHELFON_STR << "' ";
	openShelfRulesStr << "AND event.creationtime < " << time << " ";
	//openShelfRulesStr << "AND catalog.expiry > " << time << " ";
	openShelfRulesStr << "AND (catalog.expiry > " << time << " OR catalog.expiry=0) ";
	openShelfRulesStr << "AND event.exception NOT IN (SELECT exception FROM event WHERE type='" << EVENT_SHELFOFF_STR << "' AND creationtime < " << time << ")";

	std::stringstream openClearRulesStr;
	openClearRulesStr << "SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid) ";
	openClearRulesStr << "WHERE event.type='" << EVENT_CLEARON_STR << "' ";
	openClearRulesStr << "AND event.creationtime < " << time << " ";
	openShelfRulesStr << "AND (catalog.expiry > " << time << " OR catalog.expiry=0) ";
	openClearRulesStr << "AND event.exception NOT IN (SELECT exception FROM event WHERE type='" << EVENT_CLEAROFF_STR << "' AND creationtime < " << time << ")";
	/*
	 std::stringstream openClearRulesStr;
	 openClearRulesStr << "SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid) ";
	 openClearRulesStr << "WHERE event.type='" << EVENT_CLEARON_STR << "' ";
	 openClearRulesStr << "AND event.creationtime < " << time << " ";
	 */

	std::stringstream openDebounceRulesStr;
	openDebounceRulesStr << "SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid) ";
	openDebounceRulesStr << "WHERE event.type='" << EVENT_DEBOUNCEON_STR << "' ";
	openDebounceRulesStr << "AND event.creationtime < " << time << " ";
	openShelfRulesStr << "AND (catalog.expiry > " << time << " OR catalog.expiry=0) ";
	openDebounceRulesStr << "AND event.exception NOT IN (SELECT exception FROM event WHERE type='" << EVENT_DEBOUNCEOFF_STR << "' AND creationtime < " << time << ")";

	std::stringstream openExceptionsStr;
	openExceptionsStr << "SELECT * FROM (SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid) ";
	openExceptionsStr << "WHERE (";
	openExceptionsStr << "((event.type='" << EVENT_FIRE_STR << "' OR event.type='" << EVENT_CLEAR_STR << "') AND event.creationtime = (SELECT MAX(creationtime) FROM event lookup WHERE lookup.exception = event.exception AND (type='" << EVENT_FIRE_STR << "' OR type='" << EVENT_CLEAR_STR << "'))) ";
	openExceptionsStr << "OR ";
	// option to get only the lastest accept if needed
	//--((event.type='rearm' AND event.creationtime = (SELECT MAX(creationtime) FROM event lookup WHERE lookup.exception = event.exception AND type='rearm'))))
	openExceptionsStr << "(event.type='" << EVENT_ACCEPT_STR << "')) ";
	openExceptionsStr << "AND event.creationtime <= " << time << " ";
	openExceptionsStr << "AND event.creationtime > " << window << " ";
	openExceptionsStr << "AND event.exception NOT IN ";
	openExceptionsStr << "(SELECT exception FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) WHERE (type='" << EVENT_REVOKE_STR << "' OR (type='" << EVENT_CLEAR_STR << "' AND (catalog.args<>'alarm' OR catalog.args IS NULL))) AND creationtime <= " << time << ") ";
	openExceptionsStr << "ORDER BY event.creationtime) WHERE rownum <= " << maxInitExceptionNum_;

	//where rownum <= " << scatterReadNum_;
	std::cout << openExceptionsStr.str() << std::endl;
	//Year = (SELECT MAX(Year) FROM myTable AS lookup WHERE lookup.UserID = myTable.UserID)

	/*
	 std::stringstream openAcceptsStr;
	 openAcceptsStr << "SELECT * FROM event INNER JOIN catalog ON (event.exception=catalog.uniqueid) LEFT JOIN xdaq_application ON (catalog.uniqueid=xdaq_application.uniqueid)  ";
	 openAcceptsStr << "WHERE type='" << EVENT_ACCEPT_STR << "'  ";
	 openAcceptsStr << "AND creationtime < " << time << "  ";
	 openAcceptsStr << "AND event.exception IN  ";
	 openAcceptsStr << "(SELECT exception FROM event WHERE event.type='" << EVENT_FIRE_STR << "' AND event.creationtime < " << time << "   ";
	 openAcceptsStr << "	AND event.exception NOT IN (SELECT exception FROM event WHERE type='" << EVENT_REVOKE_STR << "' AND creationtime < " << time << "))  ";
	 openAcceptsStr << "ORDER BY event.creationtime";
	 */
	//std::cout << openShelfRulesStr.str() << std::endl;
	//std::cout << openClearRulesStr.str() << std::endl;
	//std::cout << openDebounceRulesStr.str() << std::endl;
	//std::cout << openExceptionsStr.str() << std::endl;
	//std::cout << openAcceptsStr.str() << std::endl;
	// Assume synchronous processing of the callback
	oracle::occi::Statement* occiStmtShelf = 0;
	oracle::occi::Statement* occiStmtClear = 0;
	oracle::occi::Statement* occiStmtDebounce = 0;
	oracle::occi::Statement* occiStmtExceptions = 0;
	//oracle::occi::Statement* occiStmtAccepts = 0;

	try
	{
		occiStmtShelf = connection_->createStatement(openShelfRulesStr.str().c_str());
		occiStmtClear = connection_->createStatement(openClearRulesStr.str().c_str());
		occiStmtDebounce = connection_->createStatement(openDebounceRulesStr.str().c_str());
		occiStmtExceptions = connection_->createStatement(openExceptionsStr.str().c_str());
		//occiStmtAccepts = connection_->createStatement(openAcceptsStr.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	}

	oracle::occi::ResultSet* occiResultShelf = 0;
	oracle::occi::ResultSet* occiResultClear = 0;
	oracle::occi::ResultSet* occiResultDebounce = 0;
	oracle::occi::ResultSet* occiResultExceptions = 0;
	//oracle::occi::ResultSet* occiResultAccepts = 0;
	try
	{
		occiResultShelf = occiStmtShelf->executeQuery();
		occiResultClear = occiStmtClear->executeQuery();
		occiResultDebounce = occiStmtDebounce->executeQuery();
		occiResultExceptions = occiStmtExceptions->executeQuery();
		//occiResultAccepts = occiStmtAccepts->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	}

	this->outputCatalog(listener, occiResultShelf);
	LOG4CPLUS_INFO(application_->getApplicationLogger(), "Shelf rules loaded");
	this->outputCatalog(listener, occiResultClear);
	LOG4CPLUS_INFO(application_->getApplicationLogger(), "Clear rules loaded");
	this->outputCatalog(listener, occiResultDebounce);
	LOG4CPLUS_INFO(application_->getApplicationLogger(), "De-bounce rules loaded");
	this->outputCatalog(listener, occiResultExceptions);
	LOG4CPLUS_INFO(application_->getApplicationLogger(), "Exceptions and alarms loaded");
	//this->outputCatalog(listener, occiResultAccepts);

	occiStmtShelf->closeResultSet(occiResultShelf);
	occiStmtClear->closeResultSet(occiResultClear);
	occiStmtDebounce->closeResultSet(occiResultDebounce);
	occiStmtExceptions->closeResultSet(occiResultExceptions);
	//occiStmtAccepts->closeResultSet(occiResultAccepts);

	connection_->terminateStatement(occiStmtShelf);
	connection_->terminateStatement(occiStmtClear);
	connection_->terminateStatement(occiStmtDebounce);
	connection_->terminateStatement(occiStmtExceptions);
	//connection_->terminateStatement(occiStmtAccepts);

	toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();

	if (averageTimeToRetrieveCatalog_ == 0.0)
	{
		// Initialize running average
		averageTimeToRetrieveCatalog_ = (double) (stopChrono - startChrono);
	}
	else
	{
		// Calculate running average
		averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double) (stopChrono - startChrono)) / 2;
	}

	return since;
}

void sentinel::arc::utils::AlarmStore::query (xgi::Output* out, const std::string& query) 
{
	/*
	 //std::cout << "Database::query: " << query << std::endl;
	 toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();

	 oracle::occi::Statement* occiStmt = 0;
	 try
	 {
	 occiStmt = connection_->createStatement(query);
	 }
	 catch (oracle::occi::SQLException &e)
	 {
	 XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	 }

	 oracle::occi::ResultSet* occiResult = 0;

	 try
	 {
	 occiResult = occiStmt->executeQuery();
	 }
	 catch (oracle::occi::SQLException &e)
	 {
	 XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	 }

	 this->outputCatalogJSONExtended(out, occiResult); // max number of returned exceptions is 1000

	 occiStmt->closeResultSet(occiResult);
	 connection_->terminateStatement(occiStmt);

	 toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();

	 if (averageTimeToRetrieveCatalog_ == 0.0)
	 {
	 // Initialize running average
	 averageTimeToRetrieveCatalog_ = (double) (stopChrono - startChrono);
	 }
	 else
	 {
	 // Calculate running average
	 averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double) (stopChrono - startChrono)) / 2;
	 }
	 */
}

void sentinel::arc::utils::AlarmStore::events (xgi::Output* out, toolbox::TimeVal & start, toolbox::TimeVal & end, const std::string& format) 
{
	/*
	 toolbox::TimeVal startChrono = toolbox::TimeVal::gettimeofday();

	 std::stringstream selectCatalogStmt;
	 // Format specification needed in order to prefent +eXX scientific formatting that does now allow to
	 // make a full precision time comparison afterwards

	 selectCatalogStmt << "select event.*,dateTime,identifier,occurrences,notifier,severity,message" << ",schema,sessionid,tag,version,class,instance,lid,context,groups,service,zone,uuid" << " from event left join catalog on (event.exception=catalog.uniqueid)" << " left join xdaq_application on (catalog.uniqueid=xdaq_application.uniqueid)" << " where catalog.dateTime<=" << std::fixed << std::setprecision(6) << (double) end << " and catalog.dateTime>=" << (double) start;

	 //std::cout << selectCatalogStmt.str() << std::endl;

	 oracle::occi::Statement* occiStmt = 0;
	 try
	 {
	 occiStmt = connection_->createStatement(selectCatalogStmt.str().c_str());
	 }
	 catch (oracle::occi::SQLException &e)
	 {
	 XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to prepare statement for select catalog. ") + e.what());
	 }

	 oracle::occi::ResultSet* occiResult = 0;

	 try
	 {
	 occiResult = occiStmt->executeQuery();
	 }
	 catch (oracle::occi::SQLException &e)
	 {
	 XCEPT_RAISE(sentinel::arc::utils::exception::NotFound, std::string("Failed to select from catalog. ") + e.what());
	 }

	 this->outputCatalogJSON(out, occiResult); // max number of returned exceptions is 1000

	 occiStmt->closeResultSet(occiResult);
	 connection_->terminateStatement(occiStmt);

	 toolbox::TimeVal stopChrono = toolbox::TimeVal::gettimeofday();

	 if (averageTimeToRetrieveCatalog_ == 0.0)
	 {
	 // Initialize running average
	 averageTimeToRetrieveCatalog_ = (double) (stopChrono - startChrono);
	 }
	 else
	 {
	 // Calculate running average
	 averageTimeToRetrieveCatalog_ = (averageTimeToRetrieveCatalog_ + (double) (stopChrono - startChrono)) / 2;
	 }
	 */
}

/*
 ** Store a blob in database db. Return an SQLite error code.
 **
 ** This function inserts a new row into the blobs table. The 'key' column
 ** of the new row is set to the string pointed to by parameter zKey. The
 ** blob pointed to by zBlob,is stored in the 'value'
 ** column of the new row.
 */
void sentinel::arc::utils::AlarmStore::writeBlob (const std::string & zKey, /* Null-terminated key string */
const std::string & zBlob /*  blob of data */
) 
{
	/* Bind the key and value data for the new table entry to SQL variables
	 ** (the ? characters in the sql statement) in the compiled INSERT
	 ** statement.
	 **
	 ** NOTE: variables are numbered from left to right from 1 upwards.
	 ** Passing 0 as the second parameter of an sqlite3_bind_XXX() function
	 ** is an error.
	 */

	try
	{
		insertExceptionStmt_->setString(1, zKey.c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to set zKey parameter for insert exception statement. ") + e.what());
	}

	try
	{
		// Create an empty BLOB here, fill it later
		oracle::occi::Blob blob = oracle::occi::Blob(connection_);
		blob.setEmpty();

		insertExceptionStmt_->setBlob(2, blob);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to set zBlob parameter for insert exception statement. ") + e.what());
	}

	try
	{
		insertExceptionStmt_->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to insert exception statement. ") + e.what());
	}

// Create BLOB to be inserted in the new record
	try
	{
		// Lock the blob column so it can be updated
		std::string qry = "SELECT exception FROM exceptions WHERE uniqueid = '" + zKey + "' FOR UPDATE";
		//std::cout << qry << std::endl;

		oracle::occi::Statement *blobStmt = connection_->createStatement(qry);
		oracle::occi::ResultSet *rset = blobStmt->executeQuery();
		rset->next();

		// Retrieve the existing empty blob to edit it
		oracle::occi::Blob blob = rset->getBlob(1);
		blob.open(oracle::occi::OCCI_LOB_READWRITE);
		blob.write(zBlob.length(), (unsigned char*) zBlob.c_str(), zBlob.length());
		blob.close();

		// Clean up
		blobStmt->closeResultSet(rset);
		connection_->terminateStatement(blobStmt);
		connection_->commit();
	}
	catch (oracle::occi::SQLException &e)
	{
		// Failed writing the blob
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, std::string("Failed to insert exception BLOB into exceptions table. ") + e.what());
	}
}

/*
 ** Read a blob from database db. Return an SQLite error code.
 */
void sentinel::arc::utils::AlarmStore::readBlob (const std::string& zKey, /* Null-terminated key to retrieve blob for */
std::string& pzBlob /* Set *pzBlob to point to the retrieved blob */
) 
{
	toolbox::task::Guard < toolbox::BSem > guard(mutex_);

	std::stringstream zSQL;

	zSQL << "select exception from exceptions where uniqueid='" << zKey << "'";

	oracle::occi::Statement* occiStmt = 0;

	try
	{
		occiStmt = connection_->createStatement(zSQL.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToRead, std::string("Failed to prepare statement for select exceptions. ") + e.what());
	}

	oracle::occi::ResultSet* occiResult = 0;
	try
	{
		occiResult = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToRead, std::string("Failed to select from exceptions for key: ") + zKey + std::string(" with oracle error as:") + e.what());
	}

	try
	{
		occiResult->next();

		unsigned char* blobPtr;
		unsigned int size;

		// Get the blob and read it into a char array
		oracle::occi::Blob blob = occiResult->getBlob(1);
		blob.open(oracle::occi::OCCI_LOB_READONLY);
		size = blob.length();

		blobPtr = new unsigned char[size];
		blob.read(size, blobPtr, size);

		// Load char array into pzBlob string
		pzBlob.assign((char*) blobPtr, size);
		delete[] blobPtr;
		blob.close();
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToRead, std::string("Failed to read blob from exceptions. ") + e.what());
	}

	occiStmt->closeResultSet(occiResult);
	connection_->terminateStatement(occiStmt);
}

/*! Retrieve the number of exceptions stored */
std::string sentinel::arc::utils::AlarmStore::getNumberOfExceptions () 
{
	int count = 0;

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;
	try
	{

		occiStmt = connection_->createStatement("select count(*) from catalog");
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			count = occiResult->getInt(1);
		}

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to count from catalog table. ") + e.what());
	}

	std::stringstream strcount;
	strcount << count;

	return strcount.str();
}

size_t sentinel::arc::utils::AlarmStore::getSize ()
{
	return 0;
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::arc::utils::AlarmStore::getLatestStoreTime ()
{
	return lastExceptionTime_;
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::arc::utils::AlarmStore::getLatestEventTime () 
{
	toolbox::TimeVal latestExceptionTime = toolbox::TimeVal::zero(); // invalid time

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;

	try
	{

		occiStmt = connection_->createStatement("select count(storeTime), max(storeTime) from event");
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			int count = 0;
			count = occiResult->getInt(1);

			if (count != 0)
			{
				double maxStoreTime = 0.0;

				try
				{
					maxStoreTime = occiResult->getDouble(2);
					toolbox::TimeVal t(maxStoreTime);
					latestExceptionTime = t;
				}
				catch (toolbox::exception::Exception& e)
				{
					occiStmt->closeResultSet(occiResult);
					connection_->terminateStatement(occiStmt);

					// invalid time if conversion fails
					std::stringstream msg;
					msg << "Failed to convert latestEventTime maxStoreTime:" << maxStoreTime;
					XCEPT_RETHROW(sentinel::arc::utils::exception::Exception, msg.str(), e);
				}
			}
		}
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to count storeTime in event. ") + e.what());
	}

	occiStmt->closeResultSet(occiResult);
	connection_->terminateStatement(occiStmt);

	return latestExceptionTime;

// - LO
}
/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::arc::utils::AlarmStore::getLatestExceptionTime () 
{
	toolbox::TimeVal latestExceptionTime = toolbox::TimeVal::zero(); // invalid time

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;

	try
	{

		occiStmt = connection_->createStatement("select count(storeTimestamp), max(storeTimestamp) from exceptions");
		occiResult = occiStmt->executeQuery();

		if (occiResult->next())
		{
			int count = 0;

			count = occiResult->getInt(1);

			if (count != 0)
			{
				std::string timestring;

				oracle::occi::Timestamp maxStoreTime;

				try
				{
					maxStoreTime = occiResult->getTimestamp(2);
					timestring = maxStoreTime.toText("yyyy/mm/dd hh:mi:ss", 0);

					toolbox::TimeVal t;
					t.fromString(timestring, "%Y-%m-%d %H:%M:%S", toolbox::TimeVal::loc);

					latestExceptionTime = t;
				}
				catch (toolbox::exception::Exception& e)
				{
					occiStmt->closeResultSet(occiResult);
					connection_->terminateStatement(occiStmt);

					// invalid time if conversion fails
					std::stringstream msg;
					//msg << "Failed to convert latestExceptionTime maxStoreTime:" << maxStoreTime;
					XCEPT_RETHROW(sentinel::arc::utils::exception::Exception, msg.str(), e);
				}
			}
		}

		occiStmt->closeResultSet(occiResult);
		connection_->terminateStatement(occiStmt);
	}
	catch (oracle::occi::SQLException &e)
	{
		connection_->terminateStatement(occiStmt);
		XCEPT_RAISE(sentinel::arc::utils::exception::Exception, std::string("Failed to count storeTime in exceptions. ") + e.what());
	}

	return latestExceptionTime;
}

/*! Retrieve the time at which the last exception has been stored */
toolbox::TimeVal sentinel::arc::utils::AlarmStore::getOldestExceptionTime ()
{
	toolbox::TimeVal oldestExceptionTime = toolbox::TimeVal::zero(); // invalid time

	std::stringstream query;
	query << "select count(dateTime), min (dateTime) from catalog";

	oracle::occi::Statement* occiStmt = 0;
	oracle::occi::ResultSet* occiResult = 0;

	try
	{
		occiStmt = connection_->createStatement(query.str().c_str());
		occiResult = occiStmt->executeQuery();
	}
	catch (oracle::occi::SQLException &e)
	{
		return oldestExceptionTime;
	}

	if (occiResult->next())
	{
		int count = occiResult->getInt(1);

		if (count != 0)
		{
			try
			{
				double datec = occiResult->getDouble(2);
				toolbox::TimeVal t(datec);
				oldestExceptionTime = t;
			}
			catch (toolbox::exception::Exception &e)
			{
				// invalid time if conversion fails
			}
		}
	}

	return oldestExceptionTime;
}

std::string sentinel::arc::utils::AlarmStore::escape (const std::string& s)
{
	std::stringstream ss;
	size_t len = s.length();
	for (size_t i = 0; i < len; i++)
	{
		char c = s[i];
		switch (c)
		{
			case 'a' ... 'z':
			case 'A' ... 'Z':
			case '0' ... '9':
			case ';':
			case '/':
			case '?':
			case ':':
			case '@':
			case '&':
			case '=':
			case '+':
			case '$':
			case ',':
			case '-':
			case '_':
			case '.':
			case '!':
			case '~':
			case '*':
			case '\'':
			case '(':
			case ')':
				ss << c;
				break;
			default:
				ss << '%' << std::setbase(16) << std::setfill('0') << std::setw(2) << (int) (c);
				break;
		}
	}
	return ss.str();
}

void sentinel::arc::utils::AlarmStore::lock ()
{
	mutex_.take();
}

void sentinel::arc::utils::AlarmStore::unlock ()
{
	mutex_.give();
}

/*! Average time to store a single exception */
double sentinel::arc::utils::AlarmStore::getAverageTimeToStore ()
{
	return averageTimeToStore_;
}

/*! Average time to retrieve a catalog */
double sentinel::arc::utils::AlarmStore::getAverageTimeToRetrieveCatalog ()
{
	return averageTimeToRetrieveCatalog_;
}

/*! Average time to retrieve a single exception by uuid */
double sentinel::arc::utils::AlarmStore::getAverageTimeToRetrieveException ()
{
	return averageTimeToRetrieveException_;
}

void sentinel::arc::utils::AlarmStore::remove (toolbox::TimeVal & age) 
{
// clear aged exceptions
	std::stringstream removeCatalogStmt;
	removeCatalogStmt << "delete from catalog where dateTime<" << std::fixed << std::setprecision(6) << (double) age;

	oracle::occi::Statement* occiStmt = 0;

	try
	{
		occiStmt = connection_->createStatement(removeCatalogStmt.str().c_str());
	}
	catch (oracle::occi::SQLException &e)
	{
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToRemove, std::string("Failed to prepare statement to remove from catalog statement. ") + e.what());
	}

	try
	{
		occiStmt->executeUpdate();
	}
	catch (oracle::occi::SQLException &e)
	{
		connection_->terminateStatement(occiStmt);
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToRemove, std::string("Failed to execute remove from catalog statement. ") + e.what());
	}

	connection_->terminateStatement(occiStmt);
}

std::list<toolbox::Properties> sentinel::arc::utils::AlarmStore::getOpenArchivesInfo ()
{
	std::list<toolbox::Properties> empty; // makes compiler happy
	return empty;
	/*
	 std::list < toolbox::Properties > plist;

	 repositoryLock_.take();

	 for (std::map<std::string, sentinel::arc::DataBase *>::iterator i = archiveDataBases_.begin(); i != archiveDataBases_.end(); i++)
	 {
	 toolbox::Properties p;
	 try
	 {
	 p.setProperty("latest", (*i).second->getLatestExceptionTime().toString(toolbox::TimeVal::gmt));
	 p.setProperty("count", (*i).second->getNumberOfExceptions());
	 }
	 catch (sentinel::arc::exception::Exception& e)
	 {
	 std::stringstream msg;
	 msg << "Failed to retrieve archive information from db, ";
	 msg << xcept::stdformat_exception_history(e);
	 LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), msg.str());
	 }
	 p.setProperty("oldest", (*i).second->getOldestExceptionTime().toString(toolbox::TimeVal::gmt));
	 std::stringstream size;
	 size << (*i).second->getSize();
	 p.setProperty("size", size.str());

	 std::stringstream readexceptiontime;
	 readexceptiontime << (*i).second->getAverageTimeToRetrieveException();
	 p.setProperty("readexceptiontime", readexceptiontime.str());

	 std::stringstream readcatalogtime;
	 readcatalogtime << (*i).second->getAverageTimeToRetrieveCatalog();
	 p.setProperty("readcatalogtime", readcatalogtime.str());

	 std::stringstream writeexceptiontime;
	 writeexceptiontime << (*i).second->getAverageTimeToStore();
	 p.setProperty("writeexceptiontime", writeexceptiontime.str());

	 if ((*i).second->getAverageTimeToRetrieveException() > 0)
	 {
	 std::stringstream rate;
	 rate << 1 / (*i).second->getAverageTimeToRetrieveException();
	 p.setProperty("readexceptionrate", rate.str());
	 }
	 else
	 {
	 p.setProperty("readexceptionrate", "0");
	 }

	 if ((*i).second->getAverageTimeToRetrieveCatalog() > 0)
	 {
	 std::stringstream rate;
	 rate << 1 / (*i).second->getAverageTimeToRetrieveCatalog();
	 p.setProperty("readcatalograte", rate.str());
	 }
	 else
	 {
	 p.setProperty("readcatalograte", "0");

	 }

	 if ((*i).second->getAverageTimeToStore() > 0)
	 {
	 std::stringstream rate;
	 rate << 1 / (*i).second->getAverageTimeToStore();
	 p.setProperty("writeexceptionrate", rate.str());
	 }
	 else
	 {
	 p.setProperty("writeexceptionrate", "0");
	 }

	 plist.push_back(p);
	 }
	 repositoryLock_.give();

	 return plist;
	 */
}

void sentinel::arc::utils::AlarmStore::ruleon (toolbox::Properties& rule, const std::string& ruleType, const std::string & source) 
{
	toolbox::net::UUID ruid;
	std::string rule_uuid = ruid.toString();

	lastExceptionTime_ = toolbox::TimeVal::gettimeofday();

	toolbox::Properties event;
	event.setProperty("uniqueid", rule_uuid); // this is actually stored in the exception column
	event.setProperty("source", source);
	event.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));

	//std::cout << "storeNewEvent" << std::endl;
	this->storeNewEvent(ruleType, event);

	rule.setProperty("uniqueid", rule_uuid);
	//std::cout << "storeNewCatalogEntry" << std::endl;
	this->storeNewCatalogEntry(rule);
	//std::cout << "storeNewXDAQEntry" << std::endl;

	this->storeNewXDAQEntry(rule_uuid, rule);
	//std::cout << "END" << std::endl;

//std::cout << "Stored rule with uuid = " << rule_uuid << std::endl;
}

void sentinel::arc::utils::AlarmStore::ruleoff (const std::string & ruleUUID, const std::string & ruleType, const std::string & source) 
{
	std::string searchType = "";
	if (ruleType == sentinel::arc::utils::EVENT_SHELFOFF_STR)
	{
		searchType = sentinel::arc::utils::EVENT_SHELFON_STR;
	}
	else if (ruleType == sentinel::arc::utils::EVENT_CLEAROFF_STR)
	{
		searchType = sentinel::arc::utils::EVENT_CLEARON_STR;
	}
	else if (ruleType == sentinel::arc::utils::EVENT_DEBOUNCEOFF_STR)
	{
		searchType = sentinel::arc::utils::EVENT_DEBOUNCEON_STR;
	}
	else
	{
		std::stringstream ss;
		ss << "Failed to match rule type " << ruleType;
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, ss.str());
	}

	if (ruleOnExists(ruleUUID, searchType))
	{
		toolbox::net::UUID ruid;
		std::string rule_uuid = ruid.toString();

		lastExceptionTime_ = toolbox::TimeVal::gettimeofday();

		toolbox::Properties event;
		event.setProperty("uniqueid", ruleUUID);
		event.setProperty("source", source);
		event.setProperty("storeTime", lastExceptionTime_.toString(toolbox::TimeVal::gmt));

		this->storeNewEvent(ruleType, event);
	}
	else
	{
		std::stringstream ss;
		ss << "Failed to find active rule with UUID " << ruleUUID;
		XCEPT_RAISE(sentinel::arc::utils::exception::FailedToStore, ss.str());
	}
}
