// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_header_info_h_
#define _pt_blit_header_info_h_

#include <cstddef>

namespace pt
{
	namespace blit
	{

		class HeaderInfoFD
		{
			public:
				static size_t fdLength; //fixed datagram

				static size_t getLength (char * bufferPtr)
				{

					return fdLength;
				}

				static size_t getHeaderSize ()
				{
					return fdLength;
				}
		};
	}


}
#endif
