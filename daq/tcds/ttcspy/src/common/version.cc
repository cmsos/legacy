#include "tcds/ttcspy/version.h"

#include "config/version.h"
#include "toolbox/version.h"
#include "xcept/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"

#include "tcds/exception/version.h"
#include "tcds/hwlayertca/version.h"
#include "tcds/hwutilstca/version.h"
#include "tcds/utils/version.h"

GETPACKAGEINFO(tcdsttcspy)

void
tcdsttcspy::checkPackageDependencies()
{
  CHECKDEPENDENCY(config);
  CHECKDEPENDENCY(tcdsexception);
  CHECKDEPENDENCY(tcdshwlayertca);
  CHECKDEPENDENCY(tcdshwutilstca);
  CHECKDEPENDENCY(tcdsutils);
  CHECKDEPENDENCY(toolbox);
  CHECKDEPENDENCY(xcept);
  CHECKDEPENDENCY(xdaq);
  CHECKDEPENDENCY(xdata);
}

std::set<std::string, std::less<std::string> >
tcdsttcspy::getPackageDependencies()
{
  std::set<std::string, std::less<std::string> > dependencies;

  ADDDEPENDENCY(dependencies, config);
  ADDDEPENDENCY(dependencies, tcdsexception);
  ADDDEPENDENCY(dependencies, tcdshwlayertca);
  ADDDEPENDENCY(dependencies, tcdshwutilstca);
  ADDDEPENDENCY(dependencies, tcdsutils);
  ADDDEPENDENCY(dependencies, toolbox);
  ADDDEPENDENCY(dependencies, xcept);
  ADDDEPENDENCY(dependencies, xdaq);
  ADDDEPENDENCY(dependencies, xdata);

  return dependencies;
}
