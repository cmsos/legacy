#include "tcds/utils/SOAPUtils.h"

template <class T>
tcds::utils::SOAPCmdSendL1A<T>::SOAPCmdSendL1A(T& controller) :
SOAPCmdBase<T>(controller, "SendL1A")
{
}

template <class T>
xoap::MessageReference
tcds::utils::SOAPCmdSendL1A<T>::executeImpl(xoap::MessageReference const& msg)
{
  this->controller_.getHw().sendL1A();
  xoap::MessageReference reply = tcds::utils::soap::makeCommandSOAPReply(msg);
  return reply;
}
