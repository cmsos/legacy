#ifndef _tcds_utils_SOAPCmdEnableCyclicGenerator_h_
#define _tcds_utils_SOAPCmdEnableCyclicGenerator_h_

#include "xoap/MessageReference.h"

#include "tcds/utils/SOAPCmdBase.h"

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    template <class T>
      class SOAPCmdEnableCyclicGenerator : public SOAPCmdBase<T>
      {

      public:
        SOAPCmdEnableCyclicGenerator(T& controller);

        /**
         * Enables the specified cyclic generator. Bound to the SOAP
         * command EnableCyclicGenerator.
         *
         * - SOAP command parameters:
         *   -  xsd:unsignedInt `genNumber': the cyclic generator number (e.g., '1').
         * - SOAP command return value: none.
         */
        virtual xoap::MessageReference executeImpl(xoap::MessageReference const& msg);

      };

  } // namespace utils
} // namespace tcds

#include "tcds/utils/SOAPCmdEnableCyclicGenerator.hxx"

#endif // _tcds_utils_SOAPCmdEnableCyclicGenerator_h_
