#ifndef _tcds_ici_ICIBchannelInfoSpaceUpdater_h_
#define _tcds_ici_ICIBchannelInfoSpaceUpdater_h_

#include <string>

#include "tcds/utils/Definitions.h"
#include "tcds/utils/HwInfoSpaceUpdaterBase.h"

namespace tcds {
  namespace utils {
    class InfoSpaceHandler;
    class InfoSpaceItem;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace ici {

    class TCADeviceICI;

    class ICIBchannelInfoSpaceUpdater : public tcds::utils::HwInfoSpaceUpdaterBase
    {

    public:
      ICIBchannelInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                  TCADeviceICI const& hw);
      virtual ~ICIBchannelInfoSpaceUpdater();

      virtual bool updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                       tcds::utils::InfoSpaceHandler* const infoSpaceHandler);

    protected:
      tcds::ici::TCADeviceICI const& getHw();

    private:
      tcds::definitions::BGO_NUM extractBchannelNumber(std::string const& regName) const;

    };

  } // namespace ici
} // namespace tcds

#endif // _tcds_ici_ICIBchannelInfoSpaceUpdater_h_
