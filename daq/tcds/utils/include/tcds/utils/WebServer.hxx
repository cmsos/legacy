template <class O>
void
tcds::utils::WebServer::registerWebObject(std::string const& name,
                                          std::string const& description,
                                          Monitor const& monitor,
                                          std::string const& itemSetName,
                                          std::string const& tabName,
                                          size_t const colSpan)
{
  WebTab& tab = getTab(tabName);

  // NOTE: This is a bit tricky, but from here the WebTab takes
  // ownership of the created object. So the auto_ptr makes sense.
  std::auto_ptr<O> object = std::auto_ptr<O>(new O(name, description, monitor, itemSetName, tabName, colSpan));
  tab.addWebObject(std::auto_ptr<WebObject>(object));
}
