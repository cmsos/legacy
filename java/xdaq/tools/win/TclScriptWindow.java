package xdaq.tools.win;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import java.util.*;
import java.lang.*;

import org.w3c.dom.*;

import xdaq.tools.win.xdaqWin;

import xdaq.tools.win.QueryInterfaceAction;

import xdaq.tools.win.TimerKnob;
import xdaq.tools.win.ComboRenderer;
import xdaq.tools.win.ComboListener;
import xdaq.tools.win.ComboItem;

import xdaq.tools.win.DOMtoString;
import xdaq.tools.win.TclInterpreter;
import xdaq.tools.win.OpenFileAction;

public class TclScriptWindow extends JFrame implements ActionListener,  Runnable, FileSelectionListener
{
	xdaqWin win_;
		 
	JComboBox urlField_;
	JButton executeButton_;
	JButton loadButton_;
	JButton browseButton_;
	TclInterpreter interp;
	JEditorPane scriptTextPane;
	Thread currentThread;
	
	public void fileSelected (File f)
	{
		urlField_.addItem (f.toString());
		this.load(f);
	}
	
	public TclScriptWindow (xdaqWin frame)
	{
		super ("Script Window");
		setSize (300, 300);
		win_ = frame;
		
		JTabbedPane tabbed = new JTabbedPane();
		
		//
		// General information tab
		//
		Box commandBox = Box.createVerticalBox();

		tabbed.addTab ("Script", commandBox);
		
		JLabel urlLabel;
	 	
		Box urlBox = new Box (BoxLayout.X_AXIS);
		Box buttonBox = new Box (BoxLayout.X_AXIS);
		urlLabel = new JLabel ("URL: ", JLabel.CENTER);
		urlField_ = new JComboBox ();
		urlField_.setEditable(true);
		urlField_.addActionListener(this);
		loadButton_ = new JButton ("Go");
		loadButton_.addActionListener (this);
		loadButton_.setEnabled(true);
		browseButton_ = new JButton ("Open...");
		browseButton_.addActionListener ( new OpenFileAction (this,this, "tcl", "TCL script"));
		browseButton_.setEnabled(true);
		urlBox.add (urlLabel);
		urlBox.add (urlField_);
		urlBox.add (loadButton_);
		urlBox.add (browseButton_);
		
		executeButton_ = new JButton ("Execute");
		executeButton_.addActionListener (this);
		executeButton_.setEnabled(true);
		
		buttonBox.add ( executeButton_);
		
		//		
		// Parameter Buttons
		//
		ActionListener doneAction = new ActionListener()
		{
			public void actionPerformed ( ActionEvent e) 
			{
				//setVisible ( false );
				dispose();
			}
		};
		
		JPanel windowButtons = new JPanel();
		JButton doneButton = new JButton ("Done");
		doneButton.addActionListener ( doneAction );
		windowButtons.add ( doneButton );
		doneButton.setEnabled (true);
		//
		// Script text panel
		//JPanel scriptPanel = new JPanel( new BorderLayout() );
		scriptTextPane = new JEditorPane("text/plain", "");
		JScrollPane scrollPane = new JScrollPane( scriptTextPane );
		//scriptPanel.add ( scrollPane, BorderLayout.NORTH);
		commandBox.add (urlBox);
		commandBox.add (scrollPane);
		commandBox.add (buttonBox);
		
		this.getContentPane().add (tabbed, BorderLayout.CENTER);
		this.getContentPane().add (windowButtons, BorderLayout.SOUTH);
		
		this.show();		
	}
	
	
	public void load (File f)
	{
		try {
			FileReader freader = new FileReader(f);
			int filesize = (int)f.length();
			char [] data = new char[filesize];
			freader.read(data,0,filesize);
			scriptTextPane.setText(new String(data));
			executeButton_.setEnabled(true);
		}
		catch (FileNotFoundException fne)
		{
			System.out.println(fne.toString());
			JOptionPane.showMessageDialog (null,"",fne.toString() , JOptionPane.WARNING_MESSAGE);

		}
		catch (IOException ioe)
		{
			System.out.println(ioe.toString());
			JOptionPane.showMessageDialog (null,"",ioe.toString() , JOptionPane.WARNING_MESSAGE);
		}

		//String status = interp.eval(filename);
		// open file and put in window
	}
	
	
	public void actionPerformed (ActionEvent evt) {
		Object source = evt.getSource();
		if (source == loadButton_) 
		{
			String filename = (String)urlField_.getSelectedItem();
			File f = new File (filename);
			this.load(f);
		}

		
		// run script into a tread so we can kill any time
		//
		if (source == executeButton_) 
		{
			String filename = (String)urlField_.getSelectedItem();
			executeButton_.setEnabled(false);
			currentThread = new Thread(this,"script");
			currentThread.start();
		}

		// Combo Box selection
		if (source == urlField_) 
		{
			String url = (String)urlField_.getSelectedItem();
			
			// chek if url is already specified
			for ( int i =0; i< urlField_.getItemCount(); i++ ) 
			{
				if ( urlField_.getItemAt(i).equals(url) )
					return;
			}	
			// new item provided then store to file	
			urlField_.addItem(url);	
		}
	}
	
	
	// The thread service routine executes
	// the script with an interpreter
	//
	public void run() 
	{
		interp = new TclInterpreter(win_);
		String scriptText = scriptTextPane.getText();
		String status = interp.evalScript(scriptText);
		
		if ( ! status.equals("") ) 
		{
		JOptionPane.showMessageDialog (null,status, "Script error", JOptionPane.WARNING_MESSAGE);
		}
		
		interp.dispose();			
		executeButton_.setEnabled(true);			
	}
	
}
