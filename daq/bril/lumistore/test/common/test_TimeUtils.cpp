#include "toolbox/TimeVal.h"
//#include <ctime>
#include "bril/lumistore/TimeUtils.h"
#include <string>
#include <iostream>
#include <sstream>
#include <set>
#include <vector>
using namespace bril::lumistore;
int main(){
  toolbox::TimeVal v = toolbox::TimeVal::gettimeofday();
  //time_t now;time(&now);
  TimeUtils t(v.sec(),v.millisec());
  std::cout<<"month "<<t.month()<<std::endl;
  std::cout<<"year "<<t.year()<<std::endl;
  std::cout<<"week of year "<<t.weekofyear()<<std::endl;
  std::cout<<"day of year "<<t.dayofyear()<<std::endl;
  std::cout<<"day of week "<<t.dayofweek()<<std::endl;
  std::cout<<"hour of day "<<t.hourofday()<<std::endl;

  std::set<std::string> topics;
  topics.insert("pltlumizero_%1%16");
  topics.insert("bcm1flumi_%0%0");
  topics.insert("%-7%5");
  topics.insert("hflumi%08%09");
  std::vector<std::string> tosubscribe;
  for(std::set<std::string>::iterator topicit=topics.begin(); topicit!=topics.end(); ++topicit){    
      std::string truename = *topicit;
      size_t firstmark_pos = topicit->find_first_of('%');
      if( firstmark_pos!=std::string::npos ){
	size_t lastmark_pos = topicit->find_last_of('%');
	std::string begStr = truename.substr( firstmark_pos+1,lastmark_pos-firstmark_pos-1 );	 
	std::string endStr = truename.substr( lastmark_pos+1 );
	int begint ;
	std::stringstream convertbeg( begStr );
	if( !(convertbeg >> begint) ){
	  std::cout<<"Failed to convert "+begStr+" to number in "+truename <<std::endl;
	  continue;
	}
	
	int endint ;
	std::stringstream convertend( endStr );
	if( !(convertend >> endint) ){
	  std::cout<<"Failed to convert "+endStr+" to number in "+truename<<std::endl;
	  continue;
	}
	for(int id=begint; id<=endint; ++id){
	  std::string t = truename.substr(0,firstmark_pos);
	  std::stringstream topicnamess;
	  topicnamess << t << id;
	  tosubscribe.push_back( topicnamess.str() );
	  std::cout<<topicnamess.str()<<std::endl;
	}
      }      
  }
}
 
