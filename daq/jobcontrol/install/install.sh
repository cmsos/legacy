#!/bin/sh
#
# install.sh
# installation of jobControl daemon as a service
#
# aoh 03122004
#
echo 'Installing : /etc/init.d/xdaqd ' 
echo '             /etc/xdaq-jc.conf   '

# SET THIS PATH TO YOUR XDAQ INSTALLATION
XDAQ_ROOT=SET_ME

echo 'Using XDAQ_ROOT='$XDAQ_ROOT

JC_INSTALL=$XDAQ_ROOT/daq/jobcontrol/install/
XDAQD_INSTALL=$XDAQ_ROOT/daq/xdaq/scripts/

# test for existence of installation files
if [ -f $JC_INSTALL/xdaq-jc.conf ] ; then
  echo 'found ./xdaq-jc.conf'
else
  echo -n 'FATAL ERROR : xdaq-jc.conf not found in '$JC_INSTALL', exiting'
  exit -1
fi

if [ -f $JC_INSTALL/xdaqd.conf ] ; then
  echo 'found ./xdaqd.conf'
else
  echo -n 'FATAL ERROR : xdaqd.conf not found, exiting'
  exit -1
fi

if [ -f $XDAQD_INSTALL/xdaqd ] ; then
  echo 'found xdaqd'
else
  echo 'FATAL ERROR : xdaqd not found, exiting'
  exit -1
fi

# test for previous installations
if [ -f /etc/init.d/xdaqd ] ; then
  echo 'WARNING : /etc/init.d/xdaqd already exists, saved in /etc/init.d/xdaqd.old'
  mv /etc/init.d/xdaqd /etc/init.d/xdaqd.old
fi

if [ -f /etc/xdaq-jc.conf ] ; then
  echo 'WARNING : /etc/xdaq-jc.conf already exists, saved in /etc/xdaq-jc.conf.old'
  mv /etc/xdaq-jc.conf /etc/xdaq-jc.conf.old
fi

if [ -f /etc/xdaqd.conf ] ; then
  echo 'WARNING : /etc/xdaqd.conf already exists, saved in /etc/xdaqd.conf.old'
  mv /etc/xdaqd.conf /etc/xdaqd.conf.old
fi

# copy files
cp $XDAQD_INSTALL/xdaqd /etc/init.d/xdaqd
cp $JC_INSTALL/xdaq-jc.conf /etc/xdaq-jc.conf
cp $JC_INSTALL/xdaqd.conf /etc/xdaqd.conf

# add to services
/sbin/chkconfig --add xdaqd
echo 'added xdaqjc to services:'
/sbin/chkconfig --list xdaqd

# re-start service
echo 're-starting xdaqjc service'
/sbin/service xdaqd restart




