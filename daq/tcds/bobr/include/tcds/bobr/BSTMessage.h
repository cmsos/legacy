#ifndef _tcds_bobr_BSTMessage_h_
#define _tcds_bobr_BSTMessage_h_

#include <cstddef>
#include <stdint.h>
#include <vector>

namespace tcds {
  namespace bobr {

    class BSTMessage
    {

    public:
      BSTMessage(std::vector<uint8_t> const& rawData);
      ~BSTMessage();

      uint32_t timestamp() const;
      uint8_t bstMaster() const;
      uint32_t orbitNumber() const;
      uint32_t fillNumber() const;
      uint16_t beamMode() const;
      double beamMomentum() const;
      double intensityBeam1() const;
      double intensityBeam2() const;

    private:
      // Strictly speaking the first four bytes contain the sub-second
      // part of the timestamp (in microseconds) and the second four
      // bytes the number of seconds sinds 01-01-1970. We only care
      // for the second part.
      static size_t const kIndexTimestamp = 4;
      static size_t const kIndexBSTMaster = 17;
      static size_t const kIndexOrbitNumber = 18;
      static size_t const kIndexFillNumber = 22;
      static size_t const kIndexBeamMode = 26;
      static size_t const kIndexBeamMomentum = 30;
      static size_t const kIndexIntensityBeam1 = 32;
      static size_t const kIndexIntensityBeam2 = 36;

      std::vector<uint8_t> rawData_;

    };

  } // namespace bobr
} // namespace tcds

#endif // _tcds_bobr_BSTMessage_h_
