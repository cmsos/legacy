#include "tcds/pm/DAQInfoSpaceHandler.h"

#include <stdint.h>

#include "tcds/pm/Definitions.h"
#include "tcds/pm/Utils.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::pm::DAQInfoSpaceHandler::DAQInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                                   tcds::utils::InfoSpaceUpdater* updater) :
  InfoSpaceHandler(xdaqApp, "tcds-pm-daq", updater)
{
  // Items related to the DAQ interface.
  createBool("main.daq_interface_status.event_in_buffer",
             false,
             "true/false");
  createBool("main.daq_interface_status.slink_header_in_buffer",
             false,
             "true/false");
  createDouble("main.daq_interface_status.derandomiser_watermark",
               0,
               "percentage",
               tcds::utils::InfoSpaceItem::PROCESS);

  createBool("main.daq_interface_status.fifo_empty",
             false,
             "true/false");
  createBool("main.daq_interface_status.fifo_full",
             false,
             "true/false");
  createDouble("main.daq_interface_status.fifo_watermark",
               0,
               "percentage",
               tcds::utils::InfoSpaceItem::PROCESS);

  // createBool("main.daq_interface_status.daq_link_ldn",
  //            false,
  //            "false/true");
  // createBool("main.daq_interface_status.daq_link_lff",
  //            false,
  //            "false/true");

  // Items related to the S-link express sender core.
  createUInt64("main.daq_link_status.version_number_lo",
               0,
               "x");
  createUInt32("main.daq_link_status.link_status.mode");
  createBool("main.daq_link_status.link_status.not_link_down",
             false,
             "false/true");
  createBool("main.daq_link_status.link_status.not_backpressured",
             false,
             "false/true");
  createBool("main.daq_link_status.link_status.has_space_available",
             false,
             "true/false");
  createUInt64("main.daq_link_status.data_counter_lo");
  createUInt64("main.daq_link_status.event_counter_lo");
  createUInt64("main.daq_link_status.crc_error_cnt_lo");
  createUInt64("main.daq_link_status.block_counter_lo");
  createUInt64("main.daq_link_status.retrans_cnt_lo");
  createUInt64("main.daq_link_status.pckt_rcv_cnt_lo");
  createUInt64("main.daq_link_status.pckt_snd_cnt_lo");
}

tcds::pm::DAQInfoSpaceHandler::~DAQInfoSpaceHandler()
{
}

tcds::utils::XDAQAppBase&
tcds::pm::DAQInfoSpaceHandler::getOwnerApplication() const
{
  return static_cast<tcds::utils::XDAQAppBase&>(tcds::utils::InfoSpaceHandler::getOwnerApplication());
}

void
tcds::pm::DAQInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Items related to the DAQ interface.
  monitor.newItemSet("itemset-daq-interface");
  monitor.addItem("itemset-daq-interface",
                  "main.daq_interface_status.event_in_buffer",
                  "Event in buffer",
                  this,
                  "If true, there is at least one event fragment in the DAQ interface input buffer.");
  monitor.addItem("itemset-daq-interface",
                  "main.daq_interface_status.slink_header_in_buffer",
                  "S-link header in buffer",
                  this,
                  "If true, there is at least one event header in the DAQ interface input buffer.");
  monitor.addItem("itemset-daq-interface",
                  "main.daq_interface_status.derandomiser_watermark",
                  "Buffer water level (%)",
                  this,
                  "The relative fill level of the input buffer of the the DAQ interface before the S-link sender.");

  monitor.addItem("itemset-daq-interface",
                  "main.daq_interface_status.fifo_empty",
                  "FIFO empty",
                  this,
                  "If true, the FIFO connecting the DAQ interface to the S-link sender is empty.");
  monitor.addItem("itemset-daq-interface",
                  "main.daq_interface_status.fifo_full",
                  "FIFO-full",
                  this,
                  "If true, the FIFO connecting the DAQ interface to the S-link sender is full.");
  monitor.addItem("itemset-daq-interface",
                  "main.daq_interface_status.fifo_watermark",
                  "FIFO water level (%)",
                  this,
                  "The relative fill level of FIFO connecting the the DAQ interface to the S-link sender.");

  // monitor.addItem("itemset-daq-interface",
  //                 "main.daq_interface_status.daq_link_ldn",
  //                 "Link is down",
  //                 this,
  //                 "If true, the S-link connection to the FEROL is down/disabled.");
  // monitor.addItem("itemset-daq-interface",
  //                 "main.daq_interface_status.daq_link_lff",
  //                 "Link is full",
  //                 this,
  //                 "If true, the buffer in the S-link sender is (almost) full.");

  // Items related to the DAQ interface.
  monitor.newItemSet("itemset-daq-sender");
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.version_number_lo",
                  "Sender core version number",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.link_status.mode",
                  "Operating mode",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.link_status.not_link_down",
                  "Link is down",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.link_status.not_backpressured",
                  "Link is backpressured",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.link_status.has_space_available",
                  "Buffer space available",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.data_counter_lo",
                  "Number of 64-bit words sent",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.event_counter_lo",
                  "Number of events sent",
                  this,
                  "NOTE: This counter increments when the S-link sender receives an event header.");
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.crc_error_cnt_lo",
                  "Number of events with incorrect CRC flagged by the S-link sender",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.block_counter_lo",
                  "Number of blocks sent",
                  this,
                  "NOTE: This counter increments when the S-link sender receives an event trailer.");
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.retrans_cnt_lo",
                  "Number of blocks retransmitted",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.pckt_rcv_cnt_lo",
                  "Number of packets received",
                  this);
  monitor.addItem("itemset-daq-sender",
                  "main.daq_link_status.pckt_snd_cnt_lo",
                  "Number of packets sent",
                  this);
}

void
tcds::pm::DAQInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                             tcds::utils::Monitor& monitor,
                                                             std::string const& forceTabName)
{
  std::string const tabName = forceTabName.empty() ? "DAQ" : forceTabName;

  webServer.registerTab(tabName,
                        "Information on DAQ interface and link status",
                        4);

  // Items related to the DAQ interface.
  webServer.registerTable("DAQ interface",
                          "Status of the DAQ interface and FIFO before the S-link sender",
                          monitor,
                          "itemset-daq-interface",
                          tabName);

  // Items related to the S-link express sender core.
  webServer.registerTable("S-link sender ",
                          "Status of the S-link sender",
                          monitor,
                          "itemset-daq-sender",
                          tabName);
}

std::string
tcds::pm::DAQInfoSpaceHandler::formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const
{
  std::string res = tcds::utils::escapeAsJSONString(tcds::utils::InfoSpaceHandler::kInvalidItemString);
  if (item->isValid())
    {
      std::string name = item->name();
      if (name == "main.daq_link_status.link_status.mode")
        {
          uint32_t const value = getUInt32(name);
          tcds::definitions::DAQ_LINK_MODE const valueEnum =
            static_cast<tcds::definitions::DAQ_LINK_MODE>(value);
          res = tcds::utils::escapeAsJSONString(tcds::pm::daqLinkModeToString(valueEnum));
        }
      else
        {
          // For everything else simply call the generic formatter.
          res = InfoSpaceHandler::formatItem(item);
        }
    }
  return res;
}
