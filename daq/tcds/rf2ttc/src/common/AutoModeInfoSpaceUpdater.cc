#include "tcds/rf2ttc/AutoModeInfoSpaceUpdater.h"

// #include <string>

#include "tcds/hwlayervme/VMEDeviceBase.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/Utils.h"

tcds::rf2ttc::AutoModeInfoSpaceUpdater::AutoModeInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp,
                                                                 tcds::hwlayervme::VMEDeviceBase const& hw) :
  tcds::utils::HwInfoSpaceUpdaterBase(xdaqApp, hw)
{
}

tcds::rf2ttc::AutoModeInfoSpaceUpdater::~AutoModeInfoSpaceUpdater()
{
}

bool
tcds::rf2ttc::AutoModeInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
                                                             tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
{
  bool updated = false;
  tcds::hwlayervme::VMEDeviceBase const& hw = getHw();
  if (hw.isReadyForUse())
    {
      // std::string name = item.name();
      // tcds::utils::InfoSpaceItem::UpdateType updateType = item.updateType();
      // if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
      //   {
      //     // The 'PROCESS' update type means that there is something
      //     // special to the variable. Figure out what to do based on the
      //     // variable name.
      //     if (tcds::utils::stringBeginsWith(name, "beam_no_beam_def"))
      //       {
      //         beamModeNumber = extractBeamModeNumber(name);
      //         uint32_t const newVal = 0x0;
      //         infoSpaceHandler->setUInt32(name, newVal);
      //         updated = true;
      //       }
      //   }
      if (!updated)
        {
          updated = tcds::utils::HwInfoSpaceUpdaterBase::updateInfoSpaceItem(item, infoSpaceHandler);
        }
    }

  if (updated)
    {
      item.setValid();
    }
  else
    {
      item.setInvalid();
    }

  return updated;
}

tcds::hwlayervme::VMEDeviceBase const&
tcds::rf2ttc::AutoModeInfoSpaceUpdater::getHw() const
{
  return dynamic_cast<tcds::hwlayervme::VMEDeviceBase const&>(tcds::utils::HwInfoSpaceUpdaterBase::getHw());
}
