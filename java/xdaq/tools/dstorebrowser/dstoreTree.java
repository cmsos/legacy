package xdaq.tools.dstorebrowser;

import javax.swing.*;
import javax.swing.tree.*;
import org.w3c.dom.*;

import xdaq.tools.dstorebrowser.dstoreTreeRenderer;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.dstorebrowser.NodeEditorPopup;

public class dstoreTree extends JTree 
{
    dstoreTreeRenderer renderer_;
    Document currentDocument;
    DefaultTreeModel treeModel;
    dstorebrowser mainWindow_;
    
    public dstoreTree( Document document, String rootName, dstorebrowser mainWindow) 
    {
	mainWindow_ = mainWindow;
	currentDocument = document;
	this.putClientProperty ("JTree.lineStyle", "Angled");
	this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	
	this.setEditable(false);
	
	// The following doesn't work in contrary to the documentation
	//this.setRowHeight ( 0 );
		
	org.w3c.dom.Node treeTop = document.getDocumentElement();
	org.w3c.dom.Element treeRoot = document.createElement("DataSource");
	treeTop.appendChild(treeRoot);
	treeRoot.appendChild(document.createTextNode(rootName));
	NodeList childNodes = document.getDocumentElement().getChildNodes();
	for (int i=0 ; i<childNodes.getLength() ; i++)
	{
	    if(childNodes.item(i).getNodeName().equals("soap-env:Body"))
	    {
		treeTop = childNodes.item(i).getFirstChild();
		break;
	    }
	}
	childNodes = treeTop.getChildNodes();
	int nChildNodes = childNodes.getLength();
	for (int i=0 ; i<nChildNodes ; i++)
	{
	    treeRoot.appendChild(treeTop.removeChild(childNodes.item(0)));
	}
	xdaqWinTreeAdapterNode adapterNode = new xdaqWinTreeAdapterNode (
	    treeRoot, null);
	
	DefaultMutableTreeNode rootNode = this.createTree ( adapterNode );
	
	treeModel = new DefaultTreeModel ( rootNode );
	this.setModel ( treeModel );
	
	treeModel.setRoot ( rootNode );
	this.treeDidChange();
	
	// Theese two lines have to be the last ones in the CTOR.
	// Otherwise the JVM on Linux and Sun report an exception.
	//
	renderer_ = new dstoreTreeRenderer();
	this.setCellRenderer ( renderer_ );
	
	this.addMouseListener ( new NodeEditorPopup ( mainWindow_ ) );
    }
    
    // Recursivly traverse all DOM nodes and build a tree from DefaultMutableTreeNodes
    //
    public DefaultMutableTreeNode createTree ( xdaqWinTreeAdapterNode adapterNode )
	{
	    DefaultMutableTreeNode node = new DefaultMutableTreeNode ( adapterNode );
	    
	    if ( adapterNode.isLeaf() )
		{
			return node;
		}
		else {
			NodeList children = adapterNode.domNode.getChildNodes();
			
			// use adapterNode::childCount instead of the equivalent DOM
			// node function, since the DOM document also contains empty
			// text nodes.
			//
			for (int k = 0; k < adapterNode.childCount(); k++)
			{
				node.add ( createTree ( adapterNode.child (k) ) );
			}
			return node;
		}
	}
	
	Document getDocument() 
	{
		return currentDocument;
	}
	
}
