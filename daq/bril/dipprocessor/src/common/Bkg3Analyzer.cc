#include "bril/dipprocessor/Bkg3Analyzer.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "xcept/tools.h"
bril::dip::Bkg3Analyzer::Bkg3Analyzer(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName,xdaq::Application* owner):DipAnalyzer(name,dipfrequency,dipServiceName,owner){
  m_fill = 0;
  m_run = 0;
  m_ls = 0;
  m_nb = 0;
  m_bkgd1 = -1;
  m_bkgd2 = -1;
  m_bkgd3 = -1;
  m_bkgd4 = -1;
  m_bkgd5 = -1;
  m_bkgd6 = -1;
  m_bkgd7 = -1;
  m_bkgd8 = -1;
  m_bkgd9 = -1;
  try{
    //if need to read configuraiton, listen also to defaultparameters
    getOwnerApplication()->getApplicationInfoSpace()->addItemChangedListener("nbnum",this);
    getOwnerApplication()->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
  }catch( xcept::Exception & e){
    LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(), "Failed to listen to app infospace");
  }
 
  m_dipRoot="dip/CMS/";
}

bril::dip::Bkg3Analyzer::~Bkg3Analyzer(){
}

void bril::dip::Bkg3Analyzer::actionPerformed(xdata::Event& e){
  if( e.type()== "urn:xdaq-event:setDefaultValues" ){
    try{
      xdata::String* dipRoot = dynamic_cast<xdata::String*>(getOwnerApplication()->getApplicationInfoSpace()->find("dipRoot")) ;
      if(dipRoot) m_dipRoot= dipRoot->value_;
    }catch(xdata::exception::Exception& e){
      std::string msg("Failed to find dipRoot in ApplicationInfoSpace ");
      LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
    }

    m_dippubs.insert(std::make_pair(m_dipRoot+"LHC/BKGD",(DipPublication*)0 ));
  }
  if( e.type()== "ItemChangedEvent" ){
    std::string item = dynamic_cast<xdata::ItemChangedEvent&>(e).itemName();
    if(item =="nbnum" ){
      std::stringstream ss;
      try{
	m_fill = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("fillnum"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find fillnum in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }   
      try{
	m_run = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("runnum"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find runnum in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }   
      try{
	m_ls = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("lsnum"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find lsnum in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }  	
      try{
	m_nb = dynamic_cast<xdata::UnsignedInteger*>(getOwnerApplication()->getApplicationInfoSpace()->find("nbnum"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find nbnum in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }  	
      try{
	m_bkgd1 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd1"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd1 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }  
      try{
	m_bkgd2 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd2"))->value_;	
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd2 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      } 
      try{
	m_bkgd3 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd3"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd3 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      } 
      try{
	m_bkgd4 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd4"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd4 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }  
      try{
	m_bkgd5 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd5"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd5 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }
      try{
	m_bkgd6 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd6"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd6 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }
      try{
	m_bkgd7 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd7"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd7 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }
      try{
	m_bkgd8 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd8"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd8 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }
      try{
	m_bkgd9 = dynamic_cast<xdata::Float*>(getOwnerApplication()->getApplicationInfoSpace()->find("bkgd9"))->value_;
      }catch(xdata::exception::Exception& e){
	std::string msg("Failed to find bkgd9 in ApplicationInfoSpace ");
	LOG4CPLUS_ERROR(getOwnerApplication()->getApplicationLogger(),stdformat_exception_history(e));
      }
      ss<<"bril::dip::Bkg3Analyzer "<<m_fill<<" "<<m_run<<" "<<m_ls<<" "<<m_nb<<" bkgd1 "<<m_bkgd1<<" bkgd2 "<<m_bkgd2<<" bkgd3 "<<m_bkgd3<<" bkgd4 "<<m_bkgd4<<" bkgd5 "<<m_bkgd5<<" bkgd6 "<<m_bkgd6<<" bkgd7 "<<m_bkgd7<<" bkgd8 "<<m_bkgd8<<" bkgd9 "<<m_bkgd9;
      LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),ss.str());
      publish_to_dip();      
    }
  }
}

void bril::dip::Bkg3Analyzer::actionPerformed(toolbox::Event& e){
}


void bril::dip::Bkg3Analyzer::handleMessage(DipSubscription* dipsub, DipData& message){
  std::string subname(dipsub->getTopicName());
  LOG4CPLUS_DEBUG(getOwnerApplication()->getApplicationLogger(),"Bkg3Analyzer::handleMessage from "+subname);
  if(message.size()==0) return; 
}

void bril::dip::Bkg3Analyzer::publish_to_dip(){
  // you can use common data such as beamegev in calculation from infospace.
  LOG4CPLUS_INFO(getOwnerApplication()->getApplicationLogger(),"Bkg3Analyzer::publish_to_dip");
  DipData* dipdata = m_dip->createDipData();
  std::string diptopicname(m_dipRoot+"LHC/BKGD");
  DipFloat bkgd1 = m_bkgd1.value_; 
  DipFloat bkgd2 = m_bkgd2.value_; 
  DipFloat bkgd3 = m_bkgd3.value_; 
  DipFloat bkgd4 = m_bkgd4.value_; 
  DipFloat bkgd5 = m_bkgd5.value_; 
  DipFloat bkgd6 = m_bkgd6.value_; 
  DipFloat bkgd7 = m_bkgd7.value_; 
  DipFloat bkgd8 = m_bkgd8.value_; 
  DipFloat bkgd9 = m_bkgd9.value_; 
  DipFloat bkgd10 = -1;     
    
  dipdata->insert(bkgd1,"BKGD1");
  dipdata->insert("Beam1Gas_BCMF","BKGD1_Source");
  dipdata->insert(bkgd2,"BKGD2");
  dipdata->insert("Beam2Gas_BCMF","BKGD2_Source");
  dipdata->insert(bkgd3,"BKGD3");
  dipdata->insert("AbortPercent_BCML","BKGD3_Source");
  dipdata->insert(bkgd4,"BKGD4");
  dipdata->insert("Beam1Halo_BHM","BKGD4_Source");
  dipdata->insert(bkgd5,"BKGD5");
  dipdata->insert("Beam2Halo_BHM","BKGD5_Source");
  dipdata->insert(bkgd6,"BKGD6");
  dipdata->insert("PLT_NC_B1","BKGD6_Source");
  dipdata->insert(bkgd7,"BKGD7");
  dipdata->insert("PLT_NC_B2","BKGD7_Source");
  dipdata->insert(bkgd8,"BKGD8");
  dipdata->insert("PLT_Leading_B1","BKGD8_Source");
  dipdata->insert(bkgd9,"BKGD9");
  dipdata->insert("PLT_Leading_B2","BKGD9_Source");
  dipdata->insert(bkgd10,"BKGD10");
  dipdata->insert("unused","BKGD10_Source");
  DipTimestamp t; 
  m_dippubs[diptopicname]->send(*dipdata,t);
  delete dipdata;
  m_bkgd1 = -1;
  m_bkgd2 = -1;
  m_bkgd3 = -1;
  m_bkgd4 = -1;
  m_bkgd5 = -1;
  m_bkgd6 = -1;
  m_bkgd7 = -1;
  m_bkgd8 = -1;
  m_bkgd9 = -1;      
}
