#!/usr/bin/env python

###############################################################################
## A little script to break the LPM->CPM TTS links.
###############################################################################

# NOTE: This is a bit quick-n-dirty.

import sys
import time

from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.cpm.tcds_cpm import get_cpmt2_hw
from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Breaker(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Breaker, self).__init__(description, usage, epilog)
        # End of pre_hook().

    def handle_args(self):
        # NOTE: All targets are hard-coded at the moment, so this does
        # not take any arguments.
        pass
        # End of handle_args().

    def main(self):

        CPM = "cpm-t1"
        LPMS = [
            # ("lpm-2", 2),
            ("lpm-4", 4)
        ]

        self.validate_target(CPM, target_type=CmdLineBase.TARGET_TYPE_HW)
        cpm_info = self.setup.hw_targets[CPM]
        cpm = get_cpm_hw(cpm_info.dns_alias,
                         cpm_info.controlhub,
                         verbose=self.verbose)
        cpmt2 = get_cpmt2_hw(cpm_info.dns_alias.replace('t1', 't2'),
                             cpm_info.controlhub,
                             verbose=self.verbose)

        lpms = []
        for (tmp, slot) in LPMS:
            self.validate_target(tmp, target_type=CmdLineBase.TARGET_TYPE_HW)
            lpm_info = self.setup.hw_targets[tmp]
            lpm = get_lpm_hw(lpm_info.dns_alias,
                             lpm_info.controlhub,
                             verbose=self.verbose)
            lpms.append((lpm, slot, lpm_info._identifier))

        cycle_number = 0
        while True:
            print "Cycle {0:d}".format(cycle_number)

            # Read counters twice and look for a difference.
            for (lpm, slot, lpm_id) in lpms:
                reg_name = "cpmt1.backplane_tts_links_status" \
                           ".lpm{0:d}.sequence_error_count".format(slot)
                tmp0 = cpm.read(reg_name)
                tmp1 = cpm.read(reg_name)
                print "  {0:s} {1:10d} {2:10d}".format(lpm_id, tmp0, tmp1)

            # #----------
            # # CPM: Resetting GTXs.
            # #----------
            # for i in [0, 1, 2]:
            #     reg_name = "cpmt1.backplane_tts_links_status.backplane_tts_control" \
            #                ".gtx_resets.gtx{0:d}".format(i)
            #     cpm.write(reg_name, 1)
            #     cpm.write(reg_name, 0)
            #     # time.sleep(1)

            # #----------
            # # CPM: Power-on reset.
            # #----------
            # reg_name = "cpmt1.resets.poweron_reset"
            # cpm.write(reg_name, 1)
            # cpm.write(reg_name, 0)

            # #----------
            # # CPM: TTC clock DCM reset.
            # #----------
            # reg_name = "cpmt2.control.reset_ttc_clock_dcm"
            # cpmt2.write(reg_name, 1)
            # cpmt2.write(reg_name, 0)

            #----------
            # CPM: T2 reset.
            #----------
            print "A"
            reg_name = "cpmt2.control.reload_t2"
            cpmt2.write(reg_name, 1)
            time.sleep(15)
            # print "b"
            # reg_name = "cpmt2.control.reload_t1"
            # cpmt2.write(reg_name, 1)
            # time.sleep(15)

            # #----------
            # # LPM: Jumping between firmware images.
            # #----------
            # for (lpm, slot, lpm_id) in lpms:
            #     print lpm_id
            #     lpm.load_firmware("golden")
            # time.sleep(4)

            # for (lpm, slot, lpm_id) in lpms:
            #     print lpm_id
            #     lpm.load_firmware("user")
            # time.sleep(4)

            # # ----------
            # # LPM: Switching clocks.
            # # ----------
            # for (lpm, slot, lpm_id) in lpms:
            #     lpm.choose_clock_source("xtal")
            #     lpm.choose_clock_source("fclka")
            #     # time.sleep(1)

            cycle_number += 1

        # End of main().

    # End of class Breaker.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to break the TTS links from the LPM to the CPM."
    usage_str = "usage: %prog [options]"

    res = Breaker(desc_str, usage_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
