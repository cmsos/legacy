package xdaq.xdesk.xprop;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.tree.*;


public class XPropEditor extends JComponent
{
	TreePath[] paths_;	
	JTreeTable parameterTable_;
	
	public XPropEditor(JTree tree)
	{
		this.setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		parameterTable_ = new JTreeTable();
		//parameterTable_.setAutoResizeMode ( JTreeTable.AUTO_RESIZE_ALL_COLUMNS );
		
		JScrollPane parameterScrollPane = new JScrollPane ( parameterTable_ );
		
		this.add ( parameterScrollPane );
		//this.add ( parameterTable_ );
		
		rememberTreeSelection(tree);
	}
	
	
	public void rememberTreeSelection(JTree tree)
	{
		TreePath[] paths = (TreePath[]) tree.getSelectionModel().getSelectionPaths();
		
		int pathLength = paths.length;
		paths_ = (TreePath[]) new TreePath [pathLength];
		for (int k = 0; k < pathLength; k++)
		{
			paths_[k] =  new TreePath ( paths[k].getPath() );
		}
	}
	
	public TreePath[] getSelection()
	{
		return paths_;
	}
	
	
	// must be called when the reply of ParameterQuery is received
	public void setModel ( ParameterTableModel model )
	{
		parameterTable_.setModel (model);
	}
	
	
	public ParameterTableModel getModel()
	{
		TreeTableModelAdapter adapter = (TreeTableModelAdapter)parameterTable_.getModel();
		return (ParameterTableModel) adapter.getModel();
	}
		
}
