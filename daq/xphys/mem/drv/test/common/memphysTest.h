// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, S. Markan and L. Orsini                         *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

/*
 * memphysTest.h
 *
 * This file declares the unit tests run by tester.cpp
 *
 * The tests are implemented and documented in memphysTest.cpp
 *
 * More information in readme.html
 */

#ifndef MEMPHYSTEST_H
#define MEMPHYSTEST_H

#include <cppunit/extensions/HelperMacros.h>

class memphysTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(memphysTest);
  CPPUNIT_TEST(testInsertAndRemove);
  CPPUNIT_TEST(testOpenAndClose);            // 2.1
  CPPUNIT_TEST(testSimpleAllocation);        // 2.2
  CPPUNIT_TEST(testSimpleMmap);              // 2.3-6
  CPPUNIT_TEST(testOpenAndKill);             // 3.1
  CPPUNIT_TEST(testAllocAndKill);            // 3.2
  CPPUNIT_TEST(testMmapAndKill);             // 3.3
  CPPUNIT_TEST(testCyclicAndKill);           // 3.4
  CPPUNIT_TEST(testMultipleOpen);            // 3.5-6

  // This test runs for a long time, so it's commented out.  Other
  // tests should provide roughly equivalent coverage
  // CPPUNIT_TEST(testCyclic);

  CPPUNIT_TEST_SUITE_END();

 public:
  void setUp();
  void tearDown();

  // Helper functions - see implementations for details
  void testInsertAndRemove();
  void testOpenAndClose();
  void testOpenAndKill();
  void testAllocAndKill();
  void testMmapAndKill();
  void testCyclicAndKill();
  void insertModule();
  void insertModule(int, int, bool);
  void removeModule();
  void testSimpleAllocation();
  void testSimpleAllocationAlreadyInserted(bool, int, int);
  void testSimpleAllocationAlreadyOpened(bool, int, int, int);
  void testCyclic();
  void testMultipleOpen();
  void testSimpleAllocation(bool);
  void testSimpleMmap();
  void tryClose(int);
  int tryAlloc(int, struct alloc_info *);
  int tryFree(int, struct alloc_info *);
  int tryOpen();
  void numberOfFreeChunksShouldBe(int fd, int n);
  int numFreeChunks(int);
  void chunkOrderShouldBe(int fd, int n);
};

#endif
