#ifndef _tcds_utils_InfoSpaceUpdaterPlain_h_
#define _tcds_utils_InfoSpaceUpdaterPlain_h_

#include "tcds/utils/InfoSpaceUpdater.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {

    class InfoSpaceUpdaterPlain : public tcds::utils::InfoSpaceUpdater
    {

    public:
      InfoSpaceUpdaterPlain(xdaq::Application& xdaqApp);
      virtual ~InfoSpaceUpdaterPlain();

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_InfoSpaceUpdaterPlain_h_
