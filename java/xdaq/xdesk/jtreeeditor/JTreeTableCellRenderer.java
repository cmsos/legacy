package xdaq.xdesk.jtreeeditor;

import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

public class JTreeTableCellRenderer
    extends DefaultTableCellRenderer {
//-------------------------------------Variables Declaration--------------------

  protected static Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

//******************************************************************************
   /**Sets the String object for the cell being rendered to value.*/
   public void setValue(Object value) {
     this.setBorder(noFocusBorder);
     this.setFont(new Font("Arial", Font.PLAIN, 11));
     if (value instanceof ImageIcon) {
       setIcon( (ImageIcon) value);
     }
     else if (value instanceof JTreeIconWithText) {
       setIcon( ( (JTreeIconWithText) value).getIcon());
       setText( ( (JTreeIconWithText) value).toString());
     }
     else {
       setText(value.toString());
     }
     this.setForeground(Color.BLUE);
   }

//******************************************************************************
}
