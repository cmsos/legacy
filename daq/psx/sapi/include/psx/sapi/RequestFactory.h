// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#ifndef _psx_sapi_RequestFactory_h_
#define _psx_sapi_RequestFactory_h_

#include "xoap/domutils.h"
#include "psx/sapi/exception/Exception.h"
#include "psx/sapi/Request.h"   
#include "xoap/SOAPMessage.h"
#include "xoap/MessageReference.h"

namespace psx {
	namespace sapi {
	
		class ApplicationService;
	
		class RequestFactory
		{
			public:
			
			static psx::sapi::Request* createRequest(xoap::MessageReference message,
										psx::sapi::ApplicationService* as) 
				;

		};
	
	}
}

#endif
