package xdaq.tools.win;

import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;

import javax.xml.soap.*;
import javax.xml.messaging.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.naming.*;
import org.w3c.dom.*;
import xdaq.tools.win.xdaqWin;
import xdaq.tools.win.xdaqWinTree;
import xdaq.tools.win.xdaqWinTreeAdapterNode;
import xdaq.tools.win.xdaqSOAPMessage;
import xdaq.tools.win.xdaqSOAPConnection;
import xdaq.tools.win.ParameterTableModel;

public class SetParametersAction implements ActionListener
{
	xdaqWin win_;
	ParameterTableModel model_;
	PropertiesWindow propertiesWin_;

	public SetParametersAction (xdaqWin win, PropertiesWindow propertiesWin)
	{
		win_ = win;
		propertiesWin_ = propertiesWin;
	}
	
	public SOAPMessage sendCommand (String targetAddr, String url, String className) 
	{
			try {
				xdaqSOAPMessage msg =  new xdaqSOAPMessage ( "ParameterSet", targetAddr );
			
				org.w3c.dom.Node root = model_.exportSelection (true, "urn:xdaq-application:"+ className, className); // export deep -> also vector elements
				if ( root.hasChildNodes() ) {
					msg.setParameters ( root.getFirstChild() );
				xdaqSOAPConnection con = new xdaqSOAPConnection();
				
			//statusPane_.advanceProgressBar (1);
			
					return con.call ( msg, url );
				}		
				
			} catch (xdaqSOAPException e) {
				System.out.println (e);
			} 
			
			return null;
	}
	
	public void actionPerformed (ActionEvent e)
	{	
		String errorString = new String();	
		model_ = propertiesWin_.getModel();
		
		TreePath[] paths = propertiesWin_.getSelection();
		
		for (int i = 0; i < paths.length; i++) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths[i].getLastPathComponent();
			
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();

			if (node.toString().equals("Context") || node.toString().equals ("Application") || node.toString().equals ("Transport"))
			{
				String targetAddress;
				String url;
				String className;
	
				if (node.toString().equals ("Application") )
				{
				  	xdaqWinTreeAdapterNode father = ((xdaqWinTreeAdapterNode) node.getUserObject()).getParent();
				  	targetAddress = aNode.getAttribute ("id");
				  	url = father.getAttribute ("url");
					className = aNode.getAttribute("class");
				} else {
					targetAddress = new String ("0"); // executive
					url = aNode.getAttribute ("url");
					className = "Executive";
				}
				
				SOAPMessage reply = sendCommand ( targetAddress, url, className );
				if (reply != null)
				{	
					xdaqSOAPMessage replyMsg = new xdaqSOAPMessage(reply);
					Document document = replyMsg.getDOM();
					
					//NodeList faultList = document.getElementsByTagName ("faultstring");

					// match faultstring in all namespaces
					NodeList faultList = document.getElementsByTagNameNS ("*", "faultstring");
					
					if (faultList.getLength() == 1)
					{
						errorString += url + " (";
						errorString += faultList.item(0).getFirstChild().getNodeValue();
						errorString += ")\n";
					}
				}
			} else {
				return; // don't update information
			}
		}
		
		if (!errorString.equals(""))
		{
			JOptionPane.showMessageDialog ( null, errorString, "Send error", JOptionPane.ERROR_MESSAGE );
		}
		
	}
			
}
