package xdaq.tools.cluster;

import xdaq.tools.cluster.JobController;
import xdaq.tools.cluster.REXECJobController;
import xdaq.tools.cluster.TELNETJobController;

class JobControllerFactoryException extends Exception
{
	JobControllerFactoryException () { super(); }
	JobControllerFactoryException ( String s ) { super(s); }
}

class JobControllerFactory
{
	public static JobController getInstance (String protocol) throws JobControllerFactoryException
	{
		if (protocol.equals("rexec")) {
			return new REXECJobController();
		}
		if (protocol.equals("telnet")) {
			return new TELNETJobController();
		}
		throw new JobControllerFactoryException ("unsupported protocol: " + protocol);
	}
}
