#ifndef _tcds_ici_version_h_
#define _tcds_ici_version_h_

#include <string>

#include "config/PackageInfo.h"

// !!! Edit these lines to reflect the latest package version. !!!
#define TCDSICI_VERSION_MAJOR 3
#define TCDSICI_VERSION_MINOR 13
#define TCDSICI_VERSION_PATCH 1

// If any previous versions available:
// #define TCDSICI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
// else:
#undef TCDSICI_PREVIOUS_VERSIONS

//
// Template macros and boilerplate code.
//
#define TCDSICI_VERSION_CODE PACKAGE_VERSION_CODE(TCDSICI_VERSION_MAJOR,TCDSICI_VERSION_MINOR,TCDSICI_VERSION_PATCH)
#ifndef TCDSICI_PREVIOUS_VERSIONS
#define TCDSICI_FULL_VERSION_LIST PACKAGE_VERSION_STRING(TCDSICI_VERSION_MAJOR,TCDSICI_VERSION_MINOR,TCDSICI_VERSION_PATCH)
#else
#define TCDSICI_FULL_VERSION_LIST TCDSICI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(TCDSICI_VERSION_MAJOR,TCDSICI_VERSION_MINOR,TCDSICI_VERSION_PATCH)
#endif

namespace tcdsici
{
  const std::string package = "tcdsici";
  const std::string versions = TCDSICI_FULL_VERSION_LIST;
  const std::string description = "CMS software for the TCDS iCI.";
  const std::string authors = "Jeroen Hegeman";
  const std::string summary = "CMS software for the TCDS iCI.";
  const std::string link = "https://twiki.cern.ch/twiki/bin/view/CMS/TcdsNotes";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies();
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
