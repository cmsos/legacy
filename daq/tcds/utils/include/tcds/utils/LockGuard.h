#ifndef _tcds_utils_LockGuard_h_
#define _tcds_utils_LockGuard_h_

#include <iostream>

#include "toolbox/stacktrace.h"

namespace tcds {
  namespace utils {

    template <class L>
    class LockGuard
    {

    public:
      LockGuard(L& lock);
      ~LockGuard();

    private:
      L& lock_;

      // Prevent copying.
      LockGuard(LockGuard const&);
      LockGuard& operator=(LockGuard const&);

    };

  } // namespace utils
} // namespace tcds

template <class L>
tcds::utils::LockGuard<L>::LockGuard(L& lock) :
lock_(lock)
{
  /* std::cout << "DEBUG JGH LockCheck 0 Calling for monitoring lock:" << std::endl; */
  /* toolbox::stacktrace(2, std::cout); */
  /* std::cout << "DEBUG JGH LockCheck 1" << std::endl; */
  lock_.lock();
}

template <class L>
tcds::utils::LockGuard<L>::~LockGuard()
{
  lock_.unlock();
}

#endif // _tcds_utils_LockGuard_h_
