#!/usr/bin/env python

###############################################################################
## Script to help track down/diagnose the issue with the varying
## Phase-1 pixel digital curents as a function of CPM (or LPM)
## activity.
###############################################################################

import signal
import sys

from pytcds.cpm.tcds_cpm import CPM
from pytcds.cpm.tcds_cpm import get_cpm_hw
from pytcds.lpm.tcds_lpm import LPM
from pytcds.lpm.tcds_lpm import get_lpm_hw
from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_constants import BGO_12
from pytcds.utils.tcds_constants import BGO_BC0
from pytcds.utils.tcds_constants import BGO_EC0
from pytcds.utils.tcds_constants import BGO_EC0
from pytcds.utils.tcds_constants import BGO_HARDRESET
from pytcds.utils.tcds_constants import BGO_LUMINIBBLE
from pytcds.utils.tcds_constants import BGO_NAMES
from pytcds.utils.tcds_constants import BGO_OC0
from pytcds.utils.tcds_constants import BGO_PIX_RESYNC
from pytcds.utils.tcds_constants import BGO_PIX_RESYNC
from pytcds.utils.tcds_constants import BGO_PRIVATEGAP
from pytcds.utils.tcds_constants import BGO_PRIVATEORBIT
from pytcds.utils.tcds_constants import BGO_RESYNC
from pytcds.utils.tcds_constants import BGO_ROC_RESET
from pytcds.utils.tcds_constants import BGO_ROC_RESET
from pytcds.utils.tcds_constants import BGO_START
from pytcds.utils.tcds_constants import BGO_STARTOFGAP
from pytcds.utils.tcds_constants import BGO_STOP
from pytcds.utils.tcds_constants import BGO_TESTENABLE
from pytcds.utils.tcds_constants import BGO_WARNINGTESTENABLE
from pytcds.utils.tcds_step_runner import Step
from pytcds.utils.tcds_step_runner import StepPrintMessage
from pytcds.utils.tcds_step_runner import StepRunner
from pytcds.utils.tcds_step_runner import StepSleep
from pytcds.utils.tcds_step_runner import StepWaitForEnter

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

ONE_MINUTE = 60
TWO_MINUTES = 2 * 60
FIVE_MINUTES = 5 * 60
STEP_TIME = ONE_MINUTE

XPM = "cpm-t1"
# XPM = "lpm-pixel-pri"
# XPM = "cpm-pri-t1"

#----------

CPM_EMISSION_BX = {}

CPM_EMISSION_BX[BGO_LUMINIBBLE] = 2256
CPM_EMISSION_BX[BGO_BC0] = 3540
CPM_EMISSION_BX[BGO_TESTENABLE] = 3283
CPM_EMISSION_BX[BGO_PRIVATEGAP] = 2000
CPM_EMISSION_BX[BGO_PRIVATEORBIT] = 0
CPM_EMISSION_BX[BGO_RESYNC] = 2000
CPM_EMISSION_BX[BGO_HARDRESET] = 2000
CPM_EMISSION_BX[BGO_EC0] = 2000
CPM_EMISSION_BX[BGO_OC0] = 2000
CPM_EMISSION_BX[BGO_START] = 2000
CPM_EMISSION_BX[BGO_STOP] = 2000
CPM_EMISSION_BX[BGO_STARTOFGAP] = 3440
CPM_EMISSION_BX[BGO_12] = 3000
CPM_EMISSION_BX[BGO_WARNINGTESTENABLE] = 2886
CPM_EMISSION_BX[BGO_ROC_RESET] = 3000
CPM_EMISSION_BX[BGO_PIX_RESYNC] = 2000

###############################################################################

def exit_strategy(signal, frame):
    print
    sys.exit()
    # End of exit_strategy().

###############################################################################

class StepPMBase(Step):

    def __init__(self, description, function, hw):
        # Figure out if we're running on a CPM or an LPM.
        if isinstance(hw, CPM):
            self._mode = "CPM"
            self._prefix = "cpmt1."
        elif isinstance(hw, LPM):
            self._mode = "LPM"
            self._prefix = ""
        else:
            raise ValueError("Class {0:s}" \
                             " expects either a CPM or an LPM".format(self.__class__.__name__))
        super(StepPMBase, self).__init__(description, function)
        self.hw = hw
        # End of __init__().

    # End of class StepPMBase.

###############################################################################

class StepPMPause(StepPMBase):

    def __init__(self, hw):
        description = "Pausing the system"
        function = self.pause
        super(StepPMPause, self).__init__(description, function, hw)
        # End of __init__().

    def pause(self):
        self.hw.write("{0:s}ipm.main.inselect.system_pause".format(self._prefix), 0x1)
        # End of pause().
        return True

    # End of class StepPMPause.

###############################################################################

class StepPMUnpause(StepPMBase):

    def __init__(self, hw):
        description = "Unpausing the system"
        function = self.unpause
        super(StepPMUnpause, self).__init__(description, function, hw)
        # End of __init__().

    def unpause(self):
        self.hw.write("{0:s}ipm.main.inselect.system_pause".format(self._prefix), 0x0)
        # End of unpause().
        return True

    # End of class StepPMUnpause.

###############################################################################

class StepPMDisableTriggerSources(StepPMBase):

    def __init__(self, hw):
        description = "Disabling all trigger sources"
        function = self.tweak_inselect
        super(StepPMDisableTriggerSources, self).__init__(description, function, hw)
        # End of __init__().

    def tweak_inselect(self):
        self.hw.write("{0:s}ipm.main.inselect.external_trigger0_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.external_trigger1_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.external_trigger_from_cpm_l1a_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.random_trigger_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.bunch_mask_trigger_enable".format(self._prefix), 0x0)
        self.hw.write("{0:s}ipm.main.inselect.cyclic_trigger_enable".format(self._prefix), 0x0)
        # End of tweak_inselect().
        return True

    # End of class StepPMDisableTriggerSources.

###############################################################################

class StepPMDisableBGo(StepPMBase):

    def __init__(self, hw, bgo_number):
        description = "Disabling B-go {0:d} ({1:s})".format(bgo_number, BGO_NAMES[bgo_number])
        function = self.disable_bgo
        super(StepPMDisableBGo, self).__init__(description, function, hw)
        self.bgo_number = bgo_number
        # End of __init__().

    def disable_bgo(self):
        self.hw.write("{0:s}ipm.bgo_channels.bgo_channel{1:d}.firing_bx".format(self._prefix,
                                                                                self.bgo_number), 0x0)
        # End of disable_bgo().
        return True

    # End of class StepPMDisableBGo.

###############################################################################

class StepPMDisableTTSActions(StepPMBase):

    def __init__(self, hw):
        description = "Disabling TTS-triggered actions"
        function = self.disable_tts_actions
        super(StepPMDisableTTSActions, self).__init__(description, function, hw)
        # End of __init__().

    def disable_tts_actions(self):
        self.hw.write("{0:s}ipm.tts_actions.auto_activate.enabled".format(self._prefix), 0x0)
        # End of disable_tts_actions().
        return True

    # End of class StepPMDisableTTSActions.

###############################################################################

class StepPMSetRandomTriggerRate(StepPMBase):

    def __init__(self, hw, rate):
        description = "Disabling random triggers"
        if rate > 0:
            description = "Enabling random trigger rate at {0:.1f} Hz".format(rate)
        function = self.set_random_trigger_rate
        super(StepPMSetRandomTriggerRate, self).__init__(description, function, hw)
        self.rate = rate
        # End of __init__().

    def set_random_trigger_rate(self):
        enable = 0
        val = 0
        if self.rate > 0:
            enable = 1
            val = int(self.rate / (3564. * 11246 / 2**32))
        self.hw.write("{0:s}ipm.main.random_trigger_config".format(self._prefix), val)
        self.hw.write("{0:s}ipm.main.inselect.random_trigger_enable".format(self._prefix), enable)
        # End of set_random_trigger_rate().
        return True

    # End of class StepPMSetRandomTriggerRate.

###############################################################################

class StepPMSetCyclicGenRate(StepPMBase):

    def __init__(self, description, hw, cyclic_gen_number, rate):
        if not description:
            description = "Disabling cyclic generator {0:d}".format(cyclic_gen_number)
            if rate > 0:
                description = "Enabling cyclic generator {0:d} at {1:.1f} Hz".format(cyclic_gen_number, rate)
        function = self.set_rate
        super(StepPMSetCyclicGenRate, self).__init__(description, function, hw)
        self.rate = rate
        self.cyclic_gen_number = cyclic_gen_number
        # End of __init__().

    def set_rate(self):
        enable = 0
        val = 0
        if self.rate > 0:
            enable = 1
            val = int(round(11246. / self.rate))
        self.hw.write("{0:s}ipm.cyclic_generator{1:d}.configuration.prescale".format(self._prefix, self.cyclic_gen_number), val)
        self.hw.write("{0:s}ipm.cyclic_generator{1:d}.configuration.enabled".format(self._prefix, self.cyclic_gen_number), enable)
        # End of set_rate().
        return True

    # End of class StepPMSetCyclicGenRate.

###############################################################################

class StepPMSetROCResetRate(StepPMSetCyclicGenRate):

    def __init__(self, hw, rate):
        description = "Disabling periodic ROC reset sequence"
        if rate > 0:
            description = "Enabling periodic ROC reset sequence at {0:.1f} Hz".format(rate)
        # Normally, cyclic generator 6 in the CPM/LPM is the ROC
        # reset.
        cyclic_gen_number = 6
        super(StepPMSetROCResetRate, self).__init__(description, hw, cyclic_gen_number, rate)
        # End of __init__().

    # End of class StepPMSetROCResetRate.

###############################################################################

class StepPMDisableCalibrationSequence(StepPMSetCyclicGenRate):

    def __init__(self, hw):
        description = "Disabling calibration sequence"
        # Normally, cyclic generator 3 in the CPM/LPM is the
        # calibration sequence.
        cyclic_gen_number = 3
        rate = 0
        super(StepPMDisableCalibrationSequence, self).__init__(description, hw, cyclic_gen_number, rate)
        # End of __init__().

    # End of class StepPMDisableCalibrationSequence.

###############################################################################

class StepPMFireL1A(StepPMBase):

    def __init__(self, hw):
        description = "Firing a single L1A"
        function = self.fire_l1a
        super(StepPMFireL1A, self).__init__(description, function, hw)
        # End of __init__().

    def fire_l1a(self):
        self.hw.write("{0:s}ipm.main.ipbus_requests".format(self._prefix), 0x00000100)
        # End of fire_l1a().
        return True

    # End of class StepPMFireL1A.

###############################################################################

class StepPMFireBGo(StepPMBase):

    def __init__(self, hw, bgo_number):
        description = "Firing a single B-go {0:d} ({1:s})".format(bgo_number, BGO_NAMES[bgo_number])
        function = self.fire_bgo
        super(StepPMFireBGo, self).__init__(description, function, hw)
        self.bgo_number = bgo_number
        # End of __init__().

    def fire_bgo(self):
        # NOTE: There is a lot of fiddling here, all to cover the case
        # in which there already is a B-go waiting to be emitted.
        tmp = self.hw.read("{0:s}ipm.bgo_channels.bgo_channel{1:d}.firing_bx".format(self._prefix,
                                                                                     self.bgo_number))

        count_before = 0
        count_after = 0
        if tmp == 0:
            bx = CPM_EMISSION_BX[self.bgo_number]
            count_before = self.hw.read("{0:s}ipm.bgo_channels.bgo_channel{1:d}.request_counter".format(self._prefix,
                                                                                                        self.bgo_number))
            self.hw.write("{0:s}ipm.bgo_channels.bgo_channel{1:d}.firing_bx".format(self._prefix,
                                                                                    self.bgo_number), bx)
            count_after = self.hw.read("{0:s}ipm.bgo_channels.bgo_channel{1:d}.request_counter".format(self._prefix,
                                                                                                       self.bgo_number))
        if count_after == count_before:
            self.hw.write("{0:s}ipm.main.ipbus_requests".format(self._prefix),
                          0x00000200 + self.bgo_number)
        if tmp != 0:
            self.hw.write("{0:s}ipm.bgo_channels.bgo_channel{1:d}.firing_bx".format(self._prefix,
                                                                                    self.bgo_number), tmp)
        # End of fire_bgo().
        return True

    # End of class StepPMFireBGo.

###############################################################################

class StepPMFireBGoTrain(StepPMBase):

    def __init__(self, hw, bgo_train_number):
        description = "Firing a single B-go train {0:d}".format(bgo_train_number)
        function = self.fire_bgo_train
        super(StepPMFireBGoTrain, self).__init__(description, function, hw)
        self._bgo_train_number = bgo_train_number
        # End of __init__().

    def fire_bgo_train(self):
        self.hw.write("{0:s}ipm.main.ipbus_requests".format(self._prefix),
                      0x00000400 + self.bgo_train_number)
        # End of fire_bgo_train().
        return True

    # End of class StepPMFireBGoTrain.

###############################################################################

class Runner(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        super(Runner, self).__init__(description, usage, epilog)
        self.target = XPM
        # End of __init__().

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def handle_args(self):
        self.args.target = self.target
        super(Runner, self).handle_args()
        # End of handle_args().

    def main(self):

        # Validate the target and connect to the hardware.
        print "Validating hardware target, and connecting to the hardware"
        self.validate_target(target_type=CmdLineBase.TARGET_TYPE_HW)
        board_info = self.setup.hw_targets[self.target]
        network_address = board_info.dns_alias
        controlhub_address = board_info.controlhub
        if self.target.lower().startswith("cpm"):
            f = get_cpm_hw
        if self.target.lower().startswith("lpm"):
            f = get_lpm_hw
        pm = f(network_address, controlhub_address, verbose=self.verbose)
        if not pm:
            self.error("Could not connect to {0:s}".format(self.ip_address))

        #----------

        print "Building procedure steps"
        steps = [
            StepPrintMessage("Starting"),

            StepWaitForEnter("Please 'Configure' a run" \
                             " _with_ pixel included" \
                             " and then press Enter..."),
            StepWaitForEnter("Please 'destroy' pixel" \
                             " and 'Configure' a run" \
                             " _without_ pixel included" \
                             " and then press Enter..."),

            # Disable all kinds of stuff.
            StepPMDisableTriggerSources(pm),
            StepPMDisableCalibrationSequence(pm),
            StepPMDisableBGo(pm, BGO_EC0),
            StepPMDisableTTSActions(pm),

            # Take a baseline with 70 Hz of ROC resets.
            StepPMSetROCResetRate(pm, 70),
            StepWaitForEnter("Please 'Start' the run" \
                             " and then press Enter..."),
            StepSleep(STEP_TIME),

            # Disable ROC resets.
            StepPMSetROCResetRate(pm, 0),
            StepSleep(STEP_TIME),

            # Fire an L1A.
            StepPMFireL1A(pm),
            StepSleep(STEP_TIME),

            # Fire a single ROC reset.
            StepPMFireBGo(pm, BGO_ROC_RESET),
            StepSleep(STEP_TIME),

            # Fire a single EC0.
            StepPMFireBGo(pm, BGO_EC0),
            StepSleep(STEP_TIME),

            # Fire a single ROC reset.
            StepPMFireBGo(pm, BGO_ROC_RESET),
            StepSleep(STEP_TIME),

            # Pause-unpause test.
            StepPMPause(pm),
            StepSleep(STEP_TIME),
            StepPMUnpause(pm),
            StepSleep(STEP_TIME),

            StepPrintMessage("Done")
        ]

        #----------

        print "Running procedure steps"
        stepper = StepRunner(steps)
        stepper.run()

        #----------

        # End of main().

    # End of class Runner.

###############################################################################

if __name__ == "__main__":

    desc_str = "Script to help track down/diagnose the issue" \
               " with the varying Phase-1 pixel digital curents" \
               " as a function of CPM/LPM activity."

    signal.signal(signal.SIGINT, exit_strategy)
    res = Runner(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
