// $Id$
//
// /*************************************************************************
// * XDAQ Components for Distributed Data Acquisition                       *
// * Copyright (C) 2000-2013, CERN.                                         *
// * All rights reserved.                                                   *
// * Authors: L. Orsini, A. Petrucci, C.Wakefield                           *
// *                                                                        *
// * For the licensing terms see LICENSE.                                   *
// * For the list of contributors see CREDITS.                              *
// *************************************************************************/

#include <iostream>
#include <sstream>
#include "toolbox/Policy.h" 

toolbox::Policy::Policy ()
{
	counter_ = 0;
	this->setProperty("matches","on"); // record all matching names by default
}

std::string toolbox::Policy::toString()
{
	std::stringstream plist;

	std::vector<std::string> names = this->propertyNames();

        for ( std::vector<std::string>::iterator j = names.begin(); j != names.end(); j++)
        {
        	if ( *j != "pattern" && *j != "package" && *j != "type" )
                {
                	plist << (*j) << "=" << this->getProperty(*j) << " ";
                }
        }

	return plist.str();
}
