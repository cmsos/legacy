#include "bril/lumistore/hdf5/H5Utils.h"
#include "bril/lumistore/hdf5/Row.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
const size_t ndata = 3564;
const size_t NUMBER_OF_BLOCKS = 10;
const size_t ROWS_PER_BLOCK = 10;

//typedef bril::lumistore::hdf5::Row<float,3564> rowdata;
using namespace bril::lumistore::hdf5;
int main(){
  size_t rowsize = sizeof(Row)-sizeof(char)*4+sizeof(float)*ndata; 
  void* item = malloc(rowsize*ROWS_PER_BLOCK);
  FILE* pFile;
  const char* filename="test_TableRow.bin";
  pFile = fopen(filename,"wb");
  Row* current=0;
  memset(item, 0xff, sizeof(item));
  static unsigned int nbnum = 0;
  char* buffc = (char*)item;
  float payload[ndata];
  for(size_t l = 0; l<ndata; ++l){ 
    payload[l]=0.5*float(l);
  }
  for(size_t b = 0; b<NUMBER_OF_BLOCKS; ++b){
    for (size_t i = 0; i < ROWS_PER_BLOCK; ++i){
      nbnum = i+1;
      current = (Row*)(buffc+rowsize*i);
      current->fillnum = 2322;
      current->runnum = (unsigned int)(nbnum/10) + 1;
      current->lsnum = 1;
      current->nbnum = nbnum;
      current->timestampsec = 1;
      current->timestampmsec = 5;
      current->algoid = 1;
      current->channelid = 12;
      memcpy(current->data,payload,sizeof(payload));      
    }
    fwrite(item,1,rowsize*ROWS_PER_BLOCK,pFile);
  }
  fclose(pFile);
  free (item);  
}
//raw bin 6.912s , 1463500800 bytes, 201.92MB/s
//raw bin 2m9.976s, 14635008000 bytes, 107.36MB/s
//WDC WD2500AAKX-753CA1,http://www.wdc.com/wdproducts/library/SpecSheet/ENG/2879-701277.pdf , RPM 7200 sustained to/from drive 125-150MB/s
