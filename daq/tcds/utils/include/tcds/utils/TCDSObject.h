#ifndef _tcds_utils_TCDSObject_h_
#define _tcds_utils_TCDSObject_h_

/* #include "tcds/utils/XDAQObject.h" */
/* #include "tcds/utils/XDAQAppBase.h" */

namespace tcds {
  namespace utils {

    class XDAQAppBase;

    class TCDSObject// : public tcds::utils::XDAQObject
    {

    public:
      virtual ~TCDSObject();

    protected:
      TCDSObject(tcds::utils::XDAQAppBase& owner);

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    private:
      tcds::utils::XDAQAppBase& owner_;

    };

  } // namespace utils
} // namespace tcds

#endif // _tcds_utils_TCDSObject_h_
