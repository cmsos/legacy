package xdaq.tools.memory;

import java.awt.*;
import java.net.*;
import java.security.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import org.w3c.dom.*;
import java.util.*;

import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import org.jfree.data.xy.*;
import org.jfree.data.general.*;
import org.jfree.data.category.*;
import org.jfree.chart.axis.*;

public class Plot extends JApplet
{
	URL url;

	
	public void init()
	{	
		
		ChartPanel p = new ChartPanel(this.createPie("Memory Format","format"), true, true, true, true, true);
		ChartPanel b = new ChartPanel(this.createBar("blocks"), true, true, true, true, true);
		ChartPanel u = new ChartPanel(this.createPie("Memory Usage","usage"), true, true, true, true, true);
		
		Box parameterBox = Box.createHorizontalBox();
		parameterBox.add(p);
		parameterBox.add(Box.createRigidArea(new Dimension(2,25)));
		parameterBox.add(b);
		parameterBox.add(Box.createRigidArea(new Dimension(2,25)));
		parameterBox.add(u);
		this.getContentPane().setBackground(Color.white);					
		this.getContentPane().add (parameterBox, BorderLayout.CENTER);
	
	}
	/*
	JFreeChart createPie(String name) {
	
		StringTokenizer st = new StringTokenizer(getParameter(name),",");
		DefaultPieDataset dataset = new DefaultPieDataset();
		while ( st.hasMoreTokens() ) {			
			dataset.setValue( st.nextToken(), Double.parseDouble(st.nextToken()) );
			
		}
		JFreeChart chart = ChartFactory.createPieChart3D ("Memory Format",// Title
								  dataset, // Dataset
								    true, // Show legend
 								    true,
								    true	
								);
								
		PiePlot3D plot = (PiePlot3D) chart.getPlot();								
		plot.setCircular(false);
		plot.setIgnoreNullValues(true);
		plot.setDepthFactor(0.1);
		plot.setLabelGap(0);
		//plot.setMaximumLabelWidth(0.5);
	   plot.setStartAngle(2.5);
		return chart;
	}
	*/
	
	JFreeChart createBar(String name) {
		StringTokenizer st = new StringTokenizer(getParameter(name),",");
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		int category = 0;
		while ( st.hasMoreTokens() ) {	
			String label =  st.nextToken();
			double value =  Double.parseDouble(st.nextToken());		
			if (category == 0)
			dataset.addValue(value, "Allocated", label);
			else
			dataset.addValue(value, "Free", label);
			category = ((category + 1) % 2);
		}	

		JFreeChart chart = ChartFactory.createStackedBarChart3D ("Number of Blocks",// Title
	   //JFreeChart chart = ChartFactory.createBarChart3D ("Number of Blocks",// Title
								   "Block Size",
								   "Number Of Blocks",
								    dataset, // Dataset
								     PlotOrientation.VERTICAL,
								    true, // Show legend
 								    true,
								    true	
								);
								
		CategoryAxis axis = chart.getCategoryPlot().getDomainAxis();
		axis.setCategoryLabelPositions(org.jfree.chart.axis.CategoryLabelPositions.UP_45);
								
		return chart;
	}
	
	JFreeChart createPie(String name, String paramater) {
	
		StringTokenizer st = new StringTokenizer(getParameter(paramater),",");
		DefaultPieDataset dataset = new DefaultPieDataset();
		while ( st.hasMoreTokens() ) {			
			dataset.setValue( st.nextToken(), Double.parseDouble(st.nextToken()) );
			
		}
		JFreeChart chart = ChartFactory.createPieChart3D (name,// Title
								  dataset, // Dataset
								    true, // Show legend
 								    true,
								    true	
								);
								
		PiePlot3D plot = (PiePlot3D) chart.getPlot();								
		plot.setCircular(false);
		plot.setIgnoreNullValues(true);
		plot.setDepthFactor(0.1);
		plot.setLabelGap(0);
		//plot.setMaximumLabelWidth(0.5);
	   plot.setStartAngle(2.5);
		return chart;
	}

	
	
} // end of class
