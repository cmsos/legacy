#!/usr/bin/perl

die "Usage sendSimpleCmdToApp.pl host port class instance cmdName\n" if @ARGV != 5;

$host     = $ARGV[0];
$port     = $ARGV[1];
$class    = $ARGV[2];
$instance = $ARGV[3];
$cmdName  = $ARGV[4];

$cmd = "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\\\"http://schemas.xmlsoap.org/soap/encoding/\\\" xmlns:SOAP-ENV=\\\"http://schemas.xmlsoap.org/soap/envelope/\\\" xmlns:xsi=\\\"http://www.w3.org/2001/XMLSchema-instance\\\" xmlns:xsd=\\\"http://www.w3.org/2001/XMLSchema\\\" xmlns:SOAP-ENC=\\\"http://schemas.xmlsoap.org/soap/encoding/\\\"><SOAP-ENV:Header></SOAP-ENV:Header><SOAP-ENV:Body><xdaq:$cmdName xmlns:xdaq=\\\"urn:xdaq-soap:3.0\\\"/></SOAP-ENV:Body></SOAP-ENV:Envelope>";

$curlCmd  = "curl --stderr /dev/null -H \"Content-Type: application/soap+xml\" -H \"Content-Description: SOAP Message\" -H \"Content-Location: urn:xdaq-application:class=$class,instance=$instance\" http://$host:$port -d \"$cmd\"";

print "$class $instance $cmdName: ";
print "\n$curlCmd\n\n";


open CURL, "$curlCmd|";
while(<CURL>) {
  chomp;
  $reply .= $_;
}

if($reply =~ m#<(\w+):${cmdName}Response\s[^>]*>(.*)</\1:${cmdName}Response>#) {
  $returnValue = $2;
  print "$returnValue\n";
} elsif($reply =~ m#<\w+:${cmdName}Response\s[^>]*\>#) {
  print "EMPTY SOAP MESSAGE\n";
} else {
  print "ERROR\n";
  print "$reply\n";
}
