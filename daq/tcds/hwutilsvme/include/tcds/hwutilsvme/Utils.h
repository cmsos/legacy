#ifndef _tcds_hwutilsvme_Utils_h_
#define _tcds_hwutilsvme_Utils_h_

namespace tcds {
  namespace hwlayervme {
    class VMEDeviceBase;
  }
}

namespace tcds {
  namespace utils {
    class ConfigurationInfoSpaceHandler;
  }
}

namespace tcds {
  namespace hwutilsvme {

    void vmeDeviceHwConnectImpl(tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace,
                                tcds::hwlayervme::VMEDeviceBase& hw);

  } // namespace hwutilsvme
} // namespace tdcs

#endif // _tcds_hwutilsvme_Utils_h_
