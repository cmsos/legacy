//
//  XPropFrameEditor.java
//  
//
//  Created by Luciano Orsini on Sun Jun 29 2003.
//  Copyright (c) 2003 __CERN__. All rights reserved.
//

package xdaq.xdesk.xcontrol;

import javax.swing.*;
import java.awt.*;

public interface XControlFrameEditor 
{
        public Container getContentPane();
        public void setJMenuBar(JMenuBar menuBar );
}
