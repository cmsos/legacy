// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: A. Brett, J. Gutleber and L. Orsini			         *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "tstore/SQLQuery.h" 
#include "tstore/exception/Exception.h"
#include "xdata/String.h"
#include <sstream>

tstore::SQLStatement::SQLStatement(const std::string statement ){
	this->setProperty("statement", statement);
} 

tstore::SQLStatement::SQLStatement(const std::string statement,const std::vector<std::string> &bindValues) {
	this->setProperty("statement", statement);
	unsigned int parameterIndex=0;
	unsigned int bindValueCount=bindValues.size();
	for (parameterIndex=0;parameterIndex<bindValueCount;parameterIndex++) {
		setParameterAtIndex(parameterIndex,bindValues[parameterIndex]);
	}
} 

unsigned int tstore::SQLStatement::parameterCount() {
	try {
		return bindValues_.getColumns().size();
	} catch (std::exception &e) {
		XCEPT_RAISE(tstore::exception::Exception, std::string("Could not get parameter count: ")+e.what());
	}
}

std::string tstore::SQLStatement::columnName(unsigned int index)  {
	std::ostringstream columnName;
	columnName << index;
	return columnName.str();
}

void tstore::SQLStatement::setParameterAtIndex(unsigned int parameterIndex,/*const*/ xdata::Serializable *parameterValue)  {
	std::string column=columnName(parameterIndex);
	if (parameterValue==NULL) {
		XCEPT_RAISE(tstore::exception::Exception, "Could not set NULL parameter at index "+column);
	}
	try {
		//to keep the values as Xdata objects, either:
		//keep a vector of Serializer *, which would make object ownership complicated since there is no easy way to copy an object
		//serialize the objects and keep a vector of char *, but there's still a problem deserializing unless the client provides the output object
		//use a table with only one row. This is cheating but it's the easiest in terms of memory management
		const std::map<std::string, std::string, xdata::Table::ci_less >& currentValues = bindValues_.getTableDefinition();
		bool hasParameter=currentValues.count(column)!=0;
		if (hasParameter && currentValues.at(column)!=parameterValue->type()) {
			bindValues_.removeColumn(column);
			hasParameter=false;
		}
		if (!hasParameter) {
			bindValues_.addColumn(column,parameterValue->type());
		}
		xdata::Serializable &v=*parameterValue;
		//std::cout << "v=" << v.type() << v.toString() << std::endl;
		bindValues_.setValueAt(0,column,v/*(xdata::Serializable &)parameterValue*/);
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception, "Could not set parameter at index "+column,e);
	} catch (std::exception &e) {
		XCEPT_RAISE(tstore::exception::Exception, "Could not set parameter at index "+column);
	}
}

void tstore::SQLStatement::setParameterAtIndex(unsigned int parameterIndex,const std::string &parameterValue)  {
	xdata::String s=(xdata::String)parameterValue;
	setParameterAtIndex(parameterIndex,&s);
}

xdata::Serializable *tstore::SQLStatement::parameterAtIndex(unsigned int parameterIndex)  {
	try {
		return bindValues_.getValueAt(0,columnName(parameterIndex));
	} catch (xdata::exception::Exception &e) {
		XCEPT_RETHROW(tstore::exception::Exception, "No parameter at index "+columnName(parameterIndex),e);
	}
}
