//
//  XPropMenuBarListener.java
//  
//
//  Created by Luciano Orsini on Sun Jun 29 2003.
//  Copyright (c) 2003 __CERN__. All rights reserved.
//
package xdaq.xdesk.xprop;

public interface XPropMenuBarListener {
    public void itemSelected(String itemName);
}
