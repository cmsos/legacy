#!/bin/bash

export XDAQ_ROOT=/opt/xdaq
export LD_LIBRARY_PATH=/opt/xdaq/lib
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
export XDAQ_SETUP_ROOT=/opt/xdaq/share

/opt/xdaq/bin/xdaq.exe -p 30000 -e /nfshome0/dsimelev/baseline14/trunk/daq/elastic/loadtest/xml/loadtest.profile -z daqval > /dev/null 2>&1 &
