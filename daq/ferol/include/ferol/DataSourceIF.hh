#ifndef __DataSourceIF
#define __DataSourceIF
#include "hal/HardwareDeviceInterface.hh"
#include "d2s/utils/InfoSpaceHandler.hh"
#include "log4cplus/logger.h"

namespace ferol
{
    /**
     *
     *
     *     @short Sets up the data source of the Ferol.
     *            
     *            The   Ferol  has   three  possible   data  sources:   The 
     *            traditional SLINK based on  LVDS links, the optical 6Gbps
     *            links  (up to  2  input streams),  and  the 10Gbps  input 
     *            link.  The  initialisation  procedures  for  these  three 
     *            inputs  are  different but  to  configure  them a  common 
     *            interface  can   be  used.  This  class   describes  this 
     *            interface (it is a single method).
     *
     *   $Author$
     * $Revision$
     *     $Date$
     *
     *
     **/
    class DataSourceIF {
    public:

        /**
         *
         *     @short The constructor if the DataSourceIF.
         *            
         *     @param device_P  A  pointer  to  the  hardware  device  used  to 
         *            configure   the   data   source.   I.e.   the   PCIDevice 
         *            representing the Ferol.
         *     @param appIS The  reference to the  Application utils::InfoSpaceHandler 
         *            needed to extract  application  parameters needed for the  
         *            the configuration of the data sources.
         *     @param logger The application Logger.
         *
         **/
        DataSourceIF( HAL::HardwareDeviceInterface *device_P,
                      utils::InfoSpaceHandler &appIS,
                      Logger logger );

        virtual ~DataSourceIF() {};
        /**
         *
         *     @short Set up and configure the data source of the Ferol.
         *            
         *            The only function of the  Interface in order to configure
         *            the data source of the Ferol. 
         *
         **/
        virtual void setDataSource() const = 0;

    protected:
        utils::InfoSpaceHandler &appIS_;
        std::string operationMode_;
        std::string dataSource_;
        bool stream0_;
        bool stream1_;
        HAL::HardwareDeviceInterface *device_P;
        Logger logger_;
    };
}

#endif /* __DataSourceIF */
