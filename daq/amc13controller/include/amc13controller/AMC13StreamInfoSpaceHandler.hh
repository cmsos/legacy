#ifndef __AMC13StreamInfoSpaceHandler
#define __AMC13StreamInfoSpaceHandler

#include "d2s/utils/StreamInfoSpaceHandler.hh"

namespace amc13controller
{
    class AMC13StreamInfoSpaceHandler : public utils::StreamInfoSpaceHandler 
    {

    public:
        
        enum HwInput { AMC136G, AMC1310G, FRL };

        AMC13StreamInfoSpaceHandler(  xdaq::Application *xdaq,
                                      std::string name,
                                      utils::InfoSpaceHandler *appIS,
                                      bool noAutoPush = false );

        void createstring( std::string name, std::string frlHwName, std::string amc136GHwName,
                           std::string amc1310GHwName,
                           UpdateType frlUpdateType, UpdateType amc13UpdateType, 
                           std::string format, std::string doc);

        void createuint32( std::string name, std::string frlHwName, std::string amc136GHwName,
                           std::string amc1310GHwName,
                           UpdateType frlUpdateType, UpdateType amc13UpdateType, 
                           std::string format, std::string doc);

        void createuint64( std::string name, std::string frlHwName, std::string amc136GHwName,
                           std::string amc1310GHwName,
                           UpdateType frlUpdateType, UpdateType amc13UpdateType, 
                           std::string format, std::string doc);

        void createdouble( std::string name, std::string frlHwName, std::string amc136GHwName,
                           std::string amc1310GHwName,
                           UpdateType frlUpdateType, UpdateType amc13UpdateType, 
                           std::string format, std::string doc);

        void createbool  ( std::string name, std::string frlHwName, std::string amc136GHwName,
                           std::string amc1310GHwName,
                           UpdateType frlUpdateType, UpdateType amc13UpdateType, 
                           std::string format, std::string doc);


        /**
         *
         *     @short This defines if the items are filled from SLINKexpress or 
         *            SLINK
         *            
         *            This routine is actually filling the infospace. 
         *
         *     @param 
         *    @return 
         *       @see 
         *
         *      Modif 
         *
         **/
        void setInputSource( HwInput inputSource );
        

    private:
        HwInput inputSource_;
        std::tr1::unordered_map< std::string, ISItem& > frlItemMap_;
        std::tr1::unordered_map< std::string, ISItem& > amc136GItemMap_;
        std::tr1::unordered_map< std::string, ISItem& > amc1310GItemMap_;
    };
}

#endif /* __AMC13StreamInfoSpaceHandler */
