#!/usr/bin/env python

###############################################################################
## A little script to 'scrape' the same information from ES that the
## rate meter also shows (at http://es-cdaq.cms/sc/ratemeter.html).
###############################################################################

import argparse
import json
import sys
import urllib2

from pytcds.utils.tcds_cmd_line_base import CmdLineBase

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class Scraper(CmdLineBase):

    def __init__(self, description=None, usage=None, epilog=None):
        self.sep_line = "-" * 80
        self.description = description
        self.usage = usage
        self.epilog = epilog
        self.verbose = False
        self.setup_parser_base()
        self.url_base = "http://es-cdaq.cms/sc/"
        self.daq_setup = "cdaq"
        # End of __init__().

    def setup_parser_base(self):
        parser = argparse.ArgumentParser(description=self.description,
                                         usage=self.usage,
                                         epilog=self.epilog)

        # Add the 'verbose' flag.
        parser.add_argument("-v", "--verbose",
                            action="store_true",
                            dest="verbose",
                            default=False)

        self.parser = parser
        # End of setup_parser_base().

    def setup_parser_custom(self):
        pass
        # End of setup_parser_custom().

    def handle_args(self):
        self.verbose = self.args.verbose
        # End of handle_args().

    def load_setup(self):
        pass

    def main(self):

        # NOTE: The following is basically what the ratemeter.js
        # script also does.

        # Figure out the run number of the current run, if any.
        json_data = self.get_json("php/brun.php?setup=" + self.daq_setup)
        timestamp_run_start = json_data['started']
        timestamp_run_end = json_data['ended']
        if not len(timestamp_run_start) or len(timestamp_run_end):
            print "No {0:s} run going on at the moment".format(self.daq_setup)
        else:
            run_number = int(json_data['number'])
            # Figure out at which lumi section we are at the moment.
            json_data = self.get_json("php/lastls.php?setup={0:s}&run={1:d}".format(self.daq_setup, run_number))
            section_number = int(json_data)
            # Figure out how many events we have in that lumi section.
            json_data = self.get_json("php/rate_overlay.php?ls={0:d}&run={1:d}".format(section_number, run_number))
            num_events = int(json_data['aggregations']['eventcount']['value'])
            # NOTE: Divide by the number of seconds in a lumi section
            # to get a rate.
            l1a_rate = num_events / 23.31
            print "{0:s} run {1:d}, lumi section {2:d}, L1A rate {3:.2f} Hz".format(self.daq_setup, run_number, section_number, l1a_rate)

        #----------

        # End of main().

    def get_json(self, url_append):
        url = self.url_base + url_append
        response = urllib2.urlopen(url)
        raw_data = response.read()
        json_data = json.loads(raw_data)
        # End of get_json().
        return json_data

    # End of class Scraper.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to 'scrape' the same information " \
               "from ES that the rate meter also shows " \
               "(at http://es-cdaq.cms/sc/ratemeter.html)."

    res = Scraper(desc_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
