#ifndef _bril_lumistore_hdf5_Row_h
#define _bril_lumistore_hdf5_Row_h
namespace bril{namespace lumistore{namespace hdf5{
      struct Row 
      {	
	unsigned int fillnum;
	unsigned int runnum;
	unsigned int lsnum;
	unsigned int nbnum;
	unsigned int timestampsec;
	unsigned int timestampmsec;
	unsigned char publishnnb;
	unsigned char datasourceid;
	unsigned char algoid;
	unsigned char channelid;
	char data[4];//self padding
      } ;
    }}}
#endif
