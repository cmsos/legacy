#include "tcds/hwlayer/RegisterInfo.h"

tcds::hwlayer::RegisterInfo::RegisterInfo(std::string const& name,
                                          bool const isReadable,
                                          bool const isWritable) :
  name_(name),
  isReadable_(isReadable),
  isWritable_(isWritable)
{
}

tcds::hwlayer::RegisterInfo::~RegisterInfo()
{
}

std::string
tcds::hwlayer::RegisterInfo::name() const
{
  return name_;
}

bool
tcds::hwlayer::RegisterInfo::isReadable() const
{
  return isReadable_;
}

bool
tcds::hwlayer::RegisterInfo::isWritable() const
{
  return isWritable_;
}
