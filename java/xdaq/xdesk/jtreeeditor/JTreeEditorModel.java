package xdaq.xdesk.jtreeeditor;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.swing.event.EventListenerList;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JTreeEditorModel
    extends DefaultTreeModel {
//---------------------------------Variables Definition-------------------------
  protected static int count;
  EventListenerList listenerList_ = new EventListenerList();
  //------------------------Constructors------------------------------------------
  /**Constructor by File*/
  public JTreeEditorModel(File file) throws Exception {
    this(getDocument(file)); //call constructor by document
  }

  /**Constructor by Document*/
  public JTreeEditorModel(Document doc) {
    this(makeRootNode(doc)); //call constructor by TreeNode
  }

  /**Main constructor by TreeNode*/
  public JTreeEditorModel(DefaultMutableTreeNode treeNode) {
    super(treeNode);
    count = 0;
  }

//******************************************************************************
   /**Returns a current DOM Document according to the last changes at JTreeModel*/
   public Document getDocument() {
     Document document = null;
     DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode)this.getRoot();
     JTreeUserObject userObject = (JTreeUserObject) rootNode.getUserObject();
     document = userObject.getDomNode().getOwnerDocument();
     return document;
   }

//******************************************************************************
   /**Creates a DOM document from file for parsing to the tree*/
   private static Document getDocument(File file) {
     Document doc = null;
     try {
       DocumentBuilder builder = DocumentBuilderFactory.newInstance().
           newDocumentBuilder();
       doc = builder.parse(file);
     }
     catch (Exception ex) {
       System.out.println(
           " JTreeEditorModel:getDocument(File file):Exeption = " +
           ex.getMessage());
     }
     return doc;
   }

//******************************************************************************
   /**Creats a root node of the tree*/
   private static DefaultMutableTreeNode
       makeRootNode(Document document) {
     try {
       document.getDocumentElement().normalize();
       Element rootElement = document.getDocumentElement();
       DefaultMutableTreeNode rootTreeNode =
           buildTree(rootElement);
       return (rootTreeNode);
     }
     catch (Exception e) {
       String errorMessage = "Error making root node: " + e;
       System.err.println(errorMessage);
       e.printStackTrace();
       return (new DefaultMutableTreeNode(errorMessage));
     }
   }

//******************************************************************************
   /**Creats a nodes of the tree*/
   private static DefaultMutableTreeNode
       buildTree(Element rootElement) {
     DefaultMutableTreeNode rootTreeNode = new DefaultMutableTreeNode(new
         JTreeUserObject(rootElement));
     addChildren(rootTreeNode, rootElement);
     return (rootTreeNode);
   }

//******************************************************************************
   /**Adds a child nodes to the tree nodes during creating a tree*/
   private static void addChildren
       (DefaultMutableTreeNode parentTreeNode,
        Node parentXMLElement) {

     if (count == 0) { //for the first time : add attributes to root node if it has them
       NamedNodeMap attr = parentXMLElement.getAttributes();
       if (attr != null && attr.getLength() > 0) {
         for (int j = 0; j < attr.getLength(); j++) {
           Node attr1 = attr.item(j);
           parentTreeNode.add(new DefaultMutableTreeNode(new
               JTreeUserObject(attr1)));
           addChildren(parentTreeNode, (Node) attr.item(j));
         }
       }
       count++;
     }
     NodeList childElements =
         parentXMLElement.getChildNodes();
     for (int i = 0; i < childElements.getLength(); i++) {
       Node childElement = childElements.item(i);
       switch (childElement.getNodeType()) {
         case 1:
           DefaultMutableTreeNode childTreeNode =
               new DefaultMutableTreeNode(new JTreeUserObject(childElement));
           NamedNodeMap args = childElement.getAttributes();
           if (args != null && args.getLength() > 0) {
             for (int j = 0; j < args.getLength(); j++) {
               Node attr1 = args.item(j);
               childTreeNode.add(new DefaultMutableTreeNode(new
                   JTreeUserObject(attr1)));
               addChildren(childTreeNode, (Node) args.item(j));
             }
           }
           parentTreeNode.add(childTreeNode);
           addChildren(childTreeNode, childElement);
           break;
         case 4:
         case 8:
         case 7:
         case 3:
           if (childElement.getNodeValue().trim().length() != 0) {
             DefaultMutableTreeNode childTreeNode1 =
                 new DefaultMutableTreeNode(new JTreeUserObject(
                 childElement));
             parentTreeNode.add(childTreeNode1);
             addChildren(childTreeNode1, childElement);
           }
           break;
       }
     }
   }

//******************************************************************************
   protected void fireTreeNodesChanged(Object source, Object[] path,
                                       int[] childIndices, Object[] children) {
     Object[] listeners = listenerList_.getListenerList();
     TreeModelEvent e = null;
     for (int i = listeners.length - 2; i >= 0; i -= 2) {
       if (listeners[i] == TreeModelListener.class) {
         if (e == null) {
           e = new TreeModelEvent(source, path, childIndices, children);
         }
         ( (TreeModelListener) listeners[i + 1]).treeNodesChanged(e);
       }
     }
   }
}
