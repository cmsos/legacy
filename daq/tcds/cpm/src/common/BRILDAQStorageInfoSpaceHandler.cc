#include "tcds/cpm/BRILDAQStorageInfoSpaceHandler.h"

#include "toolbox/string.h"

#include "tcds/utils/Definitions.h"
#include "tcds/utils/Monitor.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/WebServer.h"

tcds::cpm::BRILDAQStorageInfoSpaceHandler::BRILDAQStorageInfoSpaceHandler(xdaq::Application& xdaqApp) :
  InfoSpaceHandler(xdaqApp, "tcds-cpm-brildaqstorage", 0)
{
  // The book keeping information.
  createUInt32("fill_number");
  createUInt32("run_number");
  createUInt32("section_number");
  createUInt32("num_nibbles");

  // Counts.
  createUInt32("trigger_count_total");
  createUInt32("suppressed_trigger_count_total");
  createUInt32("trigger_count_beamactive_total");
  createUInt32("suppressed_trigger_count_beamactive_total");
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      // The per-type trigger counts:
      // - plain,
      createUInt32(toolbox::toString("trigger_count_trigtype%d", trigType));
      // - gated with beam presence.
      createUInt32(toolbox::toString("trigger_count_beamactive_trigtype%d", trigType));
      // The missed-trigger counts:
      // - plain,
      createUInt32(toolbox::toString("suppressed_trigger_count_trigtype%d", trigType));
      // - gated with beam presence.
      createUInt32(toolbox::toString("suppressed_trigger_count_beamactive_trigtype%d", trigType));
    }

  // Rates.
  createFloat("trigger_rate_total");
  createFloat("suppressed_trigger_rate_total");
  createFloat("trigger_rate_beamactive_total");
  createFloat("suppressed_trigger_rate_beamactive_total");
  for (int trigType = tcds::definitions::kTrigTypeMin;
       trigType <= tcds::definitions::kTrigTypeMax;
       ++trigType)
    {
      // The per-type trigger rates:
      // - plain,
      createFloat(toolbox::toString("trigger_rate_trigtype%d", trigType));
      // - gated with beam presence.
      createFloat(toolbox::toString("trigger_rate_beamactive_trigtype%d", trigType));
      // The missed-trigger rates:
      // - plain,
      createFloat(toolbox::toString("suppressed_trigger_rate_trigtype%d", trigType));
      // - gated with beam presence.
      createFloat(toolbox::toString("suppressed_trigger_rate_beamactive_trigtype%d", trigType));
    }

  // Deadtimes.
  // - Plain.
  createFloat("deadtime_total");
  createFloat("deadtime_tts");
  createFloat("deadtime_trigger_rules");
  createFloat("deadtime_bunch_mask_veto");
  createFloat("deadtime_retri");
  createFloat("deadtime_apve");
  createFloat("deadtime_daq_backpressure");
  createFloat("deadtime_calibration");
  createFloat("deadtime_sw_pause");
  createFloat("deadtime_fw_pause");
  // - Gated with beam presence.
  createFloat("deadtime_beamactive_total");
  createFloat("deadtime_beamactive_tts");
  createFloat("deadtime_beamactive_trigger_rules");
  createFloat("deadtime_beamactive_bunch_mask_veto");
  createFloat("deadtime_beamactive_retri");
  createFloat("deadtime_beamactive_apve");
  createFloat("deadtime_beamactive_daq_backpressure");
  createFloat("deadtime_beamactive_calibration");
  createFloat("deadtime_beamactive_sw_pause");
  createFloat("deadtime_beamactive_fw_pause");
}

tcds::cpm::BRILDAQStorageInfoSpaceHandler::~BRILDAQStorageInfoSpaceHandler()
{
}

void
tcds::cpm::BRILDAQStorageInfoSpaceHandler::registerItemSetsWithMonitor(tcds::utils::Monitor& monitor)
{
  // Nothing to do in this case.
}

void
tcds::cpm::BRILDAQStorageInfoSpaceHandler::registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                                         tcds::utils::Monitor& monitor,
                                                                         std::string const& forceTabName)
{
  // Nothing to do in this case.
}
