// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_Application_h_
#define _psx_Application_h_

#include <string>
#include <vector>
#include <map>
#include <errno.h>

#include "psx/sapi/ApplicationService.h"
#include "psx/mapi/ApplicationService.h"

#include "xgi/framework/UIManager.h" 
#include "xdaq/Application.h" 
#include "xdaq/ApplicationContext.h" 

#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/Method.h"
#include "xgi/Utils.h"

#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xdata/String.h"

#include "psx/PeerTransportService.h"





namespace psx 
{

const std::string NSURI = "http://xdaq.cern.ch/xdaq/xsd/2018/psx-meta.xsd";

class Application: public xdaq::Application, public xgi::framework::UIManager,  public xdata::ActionListener, public psx::PeerTransportService
{
	public:

	XDAQ_INSTANTIATOR();

	Application(xdaq::ApplicationStub* s) ;
	~Application();
	
	// web interface
	void Default(xgi::Input * in, xgi::Output * out ) ;
	
	void WinCCOATabPage(xgi::Input * in, xgi::Output * out ) ;
	void actionWinCCOA(xgi::Input * in, xgi::Output * out ) ;
	void SMITabPage(xgi::Input * in, xgi::Output * out ) ;
	void actionSMI(xgi::Input * in, xgi::Output * out ) ;

	// soap interface
	xoap::MessageReference onWinCCOARequest (xoap::MessageReference msg)  ;
	xoap::MessageReference onSMIRequest (xoap::MessageReference msg)  ;

	// soap meta data
	xoap::MessageReference getWinCCOASubscriptions (xoap::MessageReference msg)  throw (xoap::exception::Exception);
	xoap::MessageReference getSMISubscriptions (xoap::MessageReference msg)  throw (xoap::exception::Exception);

	// event listener for parameterization
	void actionPerformed (xdata::Event& e) ;

	// Implementation for PeerTransportServices
	xoap::MessageReference post(const std::string & url, xoap::MessageReference  request)  ;
	
	protected:
	
	psx::sapi::ApplicationService * sapi_;
	psx::mapi::ApplicationService * mapi_;
	
	xdata::String projectName_;
	xdata::String projectNumber_;
	xdata::String databaseManager_;
	xdata::String eventManager_;
	xdata::String dns_;


};

}

#endif
