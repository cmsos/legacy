#include "tcds/pi/TTSLogger.h"

#include <iostream>
#include <cassert>

#include "tcds/hwlayertca/TCADeviceBase.h"
// #include "tcds/pi/TTSLogContents.h"
#include "tcds/pi/TTSLogEntry.h"

tcds::pi::TTSLogger::TTSLogger(tcds::hwlayertca::TCADeviceBase& hw) :
  hw_(hw)
{
}

tcds::pi::TTSLogger::~TTSLogger()
{
}

// void
// tcds::pi::TTSLogger::startLogging() const
// {
//   hw_.writeRegister("ttcspy.logging_control.enabled", 0x1);
// }

// void
// tcds::pi::TTSLogger::stopLogging() const
// {
//   hw_.writeRegister("ttcspy.logging_control.enabled", 0x0);
// }

// void
// tcds::pi::TTSLogger::resetLogging() const
// {
//   zeroSpyMemory();
//   // NOTE: The actual reset is triggered by the 1 -> 0 transition.
//   hw_.writeRegister("ttcspy.logging_control.reset", 0x1);
//   hw_.writeRegister("ttcspy.logging_control.reset", 0x0);
// }

// void
// tcds::pi::TTSLogger::restartLogging() const
// {
//   stopLogging();
//   resetLogging();
//   startLogging();
// }

// bool
// tcds::pi::TTSLogger::isLoggingEnabled() const
// {
//   uint32_t const tmp = hw_.readRegister("ttcspy.logging_control.enabled");
//   return (tmp != 0x0);
// }

bool
tcds::pi::TTSLogger::isLogFull() const
{
  uint32_t const tmp = hw_.readRegister("tts_log.status.log_full");
  return (tmp != 0x0);
}

void
tcds::pi::TTSLogger::setBufferMode(LOGGING_BUFFER_MODE const val) const
{
  hw_.writeRegister("tts_log.control.logging_buffer_mode", val);
}

// void
// tcds::pi::TTSLogger::setBC0Val(uint8_t const val) const
// {
//   hw_.writeRegister("ttcspy.logging_control.bc0_val", val);
// }

// void
// tcds::pi::TTSLogger::setBC0BX(uint16_t const val)
// {
//   hw_.writeRegister("ttcspy.logging_control.expected_bc0_bx", val);
// }

// uint16_t
// tcds::pi::TTSLogger::bc0BX() const
// {
//   return hw_.readRegister("ttcspy.logging_control.expected_bc0_bx");
// }

uint32_t
tcds::pi::TTSLogger::readLogAddressPointer() const
{
  return hw_.readRegister("tts_log.status.log_address_pointer");
}

std::vector<uint32_t>
tcds::pi::TTSLogger::readLog() const
{
  uint32_t const numEntries = getNumLogEntries();
  uint32_t const blockSize = numEntries * TTSLogEntry::kNumWordsPerLogEntry;
  std::vector<uint32_t> const res = hw_.readBlock("tts_log.log_mem", blockSize);
  return res;
}

// std::string
// tcds::pi::TTSLogger::readLogFormatted() const
// {
//   TTSLogContents tmp;
//   tmp.addData(readLog());
//   std::string const res = tmp.getJSONString();
//   return res;
// }

void
tcds::pi::TTSLogger::zeroMemory() const
{
  std::vector<uint32_t> const zeros(logSize());
  hw_.writeBlock("tts_log.log_mem", zeros);
}

uint32_t
tcds::pi::TTSLogger::logSize() const
{
  // NOTE: This returns the log memory size in 'number of entries.'
  uint32_t const logSize =
    hw_.getBlockSize("tts_log.log_mem") / TTSLogEntry::kNumWordsPerLogEntry;
  return logSize;
}

uint32_t
tcds::pi::TTSLogger::getNumLogEntries() const
{
  // This returns the number of entries currently in the logger
  // memory.

  // NOTE: This is a little tricky. As long as the memory is not full,
  // the address pointer indicates the next entry to be written. Since
  // the first entry has address zero, the address pointer indicates
  // the number of entries currently in the register. Once the memory
  // is full, though, the address pointer will not move forward (and
  // roll over) but still stick to max_entries - 1.

  if (isLogFull())
    {
      return logSize();
    }
  else
    {
      uint32_t const numEntries = hw_.readRegister("tts_log.status.log_address_pointer");
      // ASSERT ASSERT ASSERT
      // Check if the address table matches what the firmware says.
      assert (numEntries <= logSize());
      // ASSERT ASSERT ASSERT end
      return numEntries;
    }
}
