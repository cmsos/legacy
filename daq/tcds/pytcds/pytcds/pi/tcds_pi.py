###############################################################################
## Utilities related to the PI board.
###############################################################################

from pytcds.pi.tcds_pi_constants import ADDRESS_TABLE_FILE_NAME_PI
from pytcds.utils.tcds_constants import BOARD_TYPE_FC7
from pytcds.utils.tcds_constants import BOARD_TYPE_PI
from pytcds.utils.tcds_fc7 import TCDSFC7
from pytcds.utils.tcds_utils_uhal import get_hw_tca

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class PI(TCDSFC7):

    def select_sfp_lpm_input(self, lpm_input):
        # ASSERT ASSERT ASSERT
        # We have two LPM inputs: 1 and 2.
        # NOTE: Numbering starts at 1 since it corresponds to physical
        # objects (i.e., the SFPs on the front-panel.)
        assert (lpm_input in [1, 2])
        # ASSERT ASSERT ASSERT end
        self.write("pi.lpm_input_select", lpm_input - 1)
        # End of select_sfp_lpm_input().

    # End of class PI.

###############################################################################

def get_pi_hw(address_or_name,
              controlhub_address,
              verbose=False,
              hw_cls=PI):

    pi = get_hw_tca(BOARD_TYPE_FC7,
                    BOARD_TYPE_PI,
                    ADDRESS_TABLE_FILE_NAME_PI,
                    address_or_name,
                    controlhub_address,
                    verbose,
                    hw_cls)

    return pi

###############################################################################
