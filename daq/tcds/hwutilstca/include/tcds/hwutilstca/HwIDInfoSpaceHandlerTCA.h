#ifndef _tcds_hwutilstca_HwIDInfoSpaceHandlerTCA_h_
#define _tcds_hwutilstca_HwIDInfoSpaceHandlerTCA_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilstca {

    class HwIDInfoSpaceHandlerTCA : public tcds::utils::InfoSpaceHandler
    {

    public:
      HwIDInfoSpaceHandlerTCA(xdaq::Application& xdaqApp,
                              tcds::utils::InfoSpaceUpdater* updater);
      virtual ~HwIDInfoSpaceHandlerTCA();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_HwIDInfoSpaceHandlerTCA_h_
