package xdaq.tools.daqlet;

import xdaq.tools.daqlet.Daqlet;
import javax.swing.*;
import javax.swing.table.*;
import java.util.*;
import java.awt.*;


public class DaqletTableCellRenderer extends DefaultTableCellRenderer
{

	public Component getTableCellRendererComponent ( JTable table, Object
	value, boolean isSelected, boolean hasFocus, int row, int column ){
		
		JLabel retVal = (JLabel) super.getTableCellRendererComponent (table, value, isSelected, hasFocus, row, column);
		// EXPERIMENTAL JASPLOT CUSTOM RENDERING OF THE TREE
		
		AbstractTableModel a = (AbstractTableModel) table.getModel();
		if (column == 0)
		{
			String iconURL = (String) a.getValueAt (row, 0);
			Icon i = null;
			if ( value != null ) {
				 i = new ImageIcon (iconURL);
			} else {
				 i = new ImageIcon ("./icons/ClassDef.gif");
			}	
				JLabel l = new JLabel ();
				l.setIcon(i);
				l.setOpaque (true);
				//l.setText ( value.toString() );
			
				//l.setFont (retVal.getFont());
				retVal  = l;
			
		} 
		
		
	    if(isSelected)
		retVal.setBackground(table.getSelectionBackground());
	    else
		retVal.setBackground(table.getBackground());
       		
	    return retVal;
	
	}

}
