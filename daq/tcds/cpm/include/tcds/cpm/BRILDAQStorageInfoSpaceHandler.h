#ifndef _tcds_cpm_BRILDAQStorageInfoSpaceHandler_h_
#define _tcds_cpm_BRILDAQStorageInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace cpm {

    class Monitor;
    class WebServer;

    /*! This InfoSpaceHandler is intended especially for the
      per-lumi-section publication and storage of all BRILDAQ data. It
      is supposed to be filled and published once per lumi section
      directly by the BRILDAQLoop. */
    class BRILDAQStorageInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
      {

      public:
        BRILDAQStorageInfoSpaceHandler(xdaq::Application& xdaqApp);
        virtual ~BRILDAQStorageInfoSpaceHandler();

      protected:
        virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
        virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                   tcds::utils::Monitor& monitor,
                                                   std::string const& forceTabName="");

      };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_BRILDAQStorageInfoSpaceHandler_h_
