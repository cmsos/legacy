<?php
require '../utils/vendor/autoload.php';
use GuzzleHttp\Client;
use GuzzleHttp\Subscriber\Cache\CacheSubscriber;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Cache\CacheStorage;
// use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class FilesystemCacheGarbageCollector extends FilesystemCache {
	const TIMESTAMP_LENGTH_BYTES = 10;
	
	// provide smaller delay time and large number of files triggers the algorithm less often
	const MAX_NUMBER_OF_CACHED_FILES = 500;
	const MIN_DELAY_TIME = 60; // 1 minute
	protected function doFlush() {
		
		$removedfiles = 0;
		
		$time = time ();
		
		$objects =  $this->getIterator (); 
		$count =iterator_count($objects);
		
		//error_log("found " . $count . " files and directories");
		
		if ( $count > self::MAX_NUMBER_OF_CACHED_FILES) {
			foreach ( $objects as $name => $file ) {
				
				if (! $file->isDir () && $this->isFilenameEndingWithExtension ( $name )) {
					
					$resource = fopen ( $name, "r" );
					if (false !== ($line = fgets ( $resource, self::TIMESTAMP_LENGTH_BYTES + 1 ))) {
						$expirationtime = ( int ) $line;
					} else {
						//error_log("found file without expiretime  " .  $name);
						$expirationtime = PHP_INT_MAX; // never expires
					}
					
					fclose ( $resource );
					
					if ($expirationtime == 0) {
						//error_log("found file  expiretime  0 for file " .  $name);
						
						$expirationtime = PHP_INT_MAX; // never expires
					}
					// If an extension is set, only remove files which end with the given extension.
					// If no extension is set, we have no other choice than removing everything.
					$delta = $time - self::MIN_DELAY_TIME;
					//error_log(" expiretime  " .  $expirationtime . " currenttime " . $time . " min delay " . self::MIN_DELAY_TIME . " delta " .$delta);
					if ($expirationtime  <  ($time - self::MIN_DELAY_TIME)) {
						//error_log(" remove file  " .  $name . " counter removed " . $removedfiles);
						$removedfiles++;
						@unlink ( $name );
					}
				}
			}
			
			//error_log("removed  " . $removedfiles . " files");
				
		}
	}
	/**
	 * @return \Iterator
	 */
	protected function getIterator()
	{
		return new \RecursiveIteratorIterator(
				new \RecursiveDirectoryIterator($this->directory, \FilesystemIterator::SKIP_DOTS),
				\RecursiveIteratorIterator::CHILD_FIRST
				);
	}
	
	/**
	 * @param string $name The filename
	 *
	 * @return bool
	 */
	protected function isFilenameEndingWithExtension($name)
	{
		return '' === $this->getExtension()
		|| strrpos($name, $this->getExtension()) === (strlen($name) - strlen($this->getExtension()));
	}
}

class Network {
	private static $applicationname = 'unknown';
	static function init($name) {
		self::$applicationname = $name;
	}
	static function httpget($url) {
		$client = new Client(/*['defaults' => ['debug' => true]]*/);
		
		// $cacheStorage = new CacheStorage(new ArrayCache());
		$cacheDriver = new FilesystemCacheGarbageCollector ( "/tmp/guzzle_cache_" . self::$applicationname );
		$cacheStorage = new CacheStorage ( $cacheDriver );
		
		// Use the helper method to attach a cache to the client.
		CacheSubscriber::attach ( $client, [
				'storage' => $cacheStorage
		] );
		
		$response = $client->get ( $url );
		
		$cacheDriver->flushAll ();
		
		
		return $response;
	}
	static function httpgetnocache($url) {
		$client = new Client(/*['defaults' => ['debug' => true]]*/);
		$response = $client->get ( $url );
		
		return $response;
	}
}
// Network::init('hotspot5');

// function httpget($url) {
// $client = new Client(/*['defaults' => ['debug' => true]]*/);
//
// // $cacheStorage = new CacheStorage(new ArrayCache());
// $cacheStorage = new CacheStorage ( new FilesystemCache ( "/tmp/guzzle_cache" ) );
//
// // Use the helper method to attach a cache to the client.
// CacheSubscriber::attach ( $client, [ 'storage' => $cacheStorage ] );
//
// $response = $client->get ( $url );
//
// return $response;
// }

/*
* use Kevinrob\GuzzleCache\CacheMiddleware;
* use Kevinrob\GuzzleCache\Storage\Psr6CacheStorage;
* use Symfony\Component\Cache\Adapter\FilesystemAdapter;
* use Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy;
* use Kevinrob\GuzzleCache\Storage\DoctrineCacheStorage;
*
* $config ['getModelTree_ttl'] = 2;
* $config ['getExceptionList_ttl'] = 2;
* $config ['getException_ttl'] = 60;
* $config ['accept_ttl'] = 60;
* $config ['clear_ttl'] = 60;
* function httpget($url,$ttl) {
*
* // Create a HandlerStack
* $stack = HandlerStack::create ();
*
*
* // Instantiate the cache storage: a PSR-6 file system cache with
* // a default lifetime of 1 minute (60 seconds).
* $cache_storage = new Psr6CacheStorage ( new FilesystemAdapter ( '', 0, '/tmp/hotspot5' ) );
*
* // Add cache middleware to the top of the stack with `push`
* // Choose a cache strategy: the PrivateCacheStrategy is good to start with
*
* $stack->push ( new CacheMiddleware ( new GreedyCacheStrategy ( $cache_storage, $ttl ) ), 'hotspot5-greedy-cache' );
*
*
* $client = new \GuzzleHttp\Client ( [
* 'handler' => $stack
* ] );
*
* $response = $client->request ( 'GET', $url );
*
* return $response;
* }
*/

?>