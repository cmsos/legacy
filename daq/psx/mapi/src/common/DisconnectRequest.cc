// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "psx/mapi/DisconnectRequest.h"
#include "psx/mapi/ApplicationService.h"
     

psx::mapi::DisconnectRequest::DisconnectRequest (psx::mapi::ApplicationService * as) : psx::mapi::Request("disconnect",as)
{
	owner_ = "";
	id_ = "";
}

void psx::mapi::DisconnectRequest::setId(const std::string & id )
{
	id_ = id;
}

std::string psx::mapi::DisconnectRequest::getId()
{
	return id_;
}
                        
std::string psx::mapi::DisconnectRequest::formatObjectName()
{
 	std::string format = "";
        if (domain_ != "")
        {
                format = domain_;
        }
        else
        {
                format = name_;
        }

        format += "::";
        format += name_;
        return format;

}

std::string psx::mapi::DisconnectRequest::formatCommand()
{
	std::string format = "";
	
	/*
	if (exclusive_)
	{
		format += "/EXCLUSIVE=\"yes\"";
	}
	
	if (owner_ != "")
	{
		format += "/OWNER=\"";
		format += owner_;
		format += "\"";
	}
	*/
	
	return format;
}

