// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius                                     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include "pt/blit/PipeConnectionResetByPeer.h"

pt::blit::PipeConnectionResetByPeer::PipeConnectionResetByPeer(tcpla::EndPoint ep_handle, int index, int sid ):
ep_handle_(ep_handle),index_(index),sid_(sid)
{

}

int pt::blit::PipeConnectionResetByPeer::getPipeIndex()
{
	return index_;
}

int pt::blit::PipeConnectionResetByPeer::getSID()
{
	return sid_;
}



tcpla::EndPoint pt::blit::PipeConnectionResetByPeer::getEndPoint()
{
	return ep_handle_;
}

