package xdaq.xdesk.xscript;

import java.io.*;
import java.awt.event.*;
import tcl.lang.*;
import java.lang.*;
import org.apache.log4j.*;

public class TclInterpreter implements Interpreter, Runnable
{
	Logger logger = Logger.getLogger (TclInterpreter.class);
	
	Interp interp_;
	Notifier notifier_;
	boolean running_;
	Thread thread_;
	
	ActionListener listener_ = null;
	
	class XScriptEvent extends TclEvent
	{
		String script_;
		
		public XScriptEvent (String script)
		{
			super();
			script_ = script;
		}
		
		public int processEvent (int flags)
		{
			try {
				interp_.eval (script_);
				// interp_.getResult();
			} catch (TclException ex)
			{
				logger.error (ex.toString());			
			}
			return 1;
		}
		
	}
	
	class StopEvent extends TclEvent
	{
		TclInterpreter interpreter_;
		
		public StopEvent (TclInterpreter interpreter)
		{
			super();
			interpreter_ = interpreter;
		}
		
		public int processEvent (int flags)
		{
			interpreter_.stop();
			return 1;
		}
		
	}
	
	
	
	public TclInterpreter()
	{	
		interp_ = new Interp();
		notifier_ = interp_.getNotifier();
	}
	
	// Activate the thread by calling TclInterpreter.start();
	public void run ()
	{
		running_ = true;
		while (running_)
		{
			int r = notifier_.doOneEvent(TCL.ALL_EVENTS | TCL.DONT_WAIT);
		}

		//int bo = notifier_.doOneEvent(TCL.ALL_EVENTS | TCL.DONT_WAIT);
		//System.out.println ("Processed  event: " + bo);
		//while(  notifier_.doOneEvent(TCL.ALL_EVENTS | TCL.DONT_WAIT) != 0 )
		//{
		//	System.out.println ("---- Clear event queue.");
		//}
		notifier_.deleteEvents(new EventDeleter() {
			public int deleteEvent(TclEvent evt) {
    				return 1;
			}
		});
		
		String result = interp_.getResult().toString();
		listener_.actionPerformed ( new ActionEvent (this, 0, result) );
		
	}

	
	// -------------------------------------------------------------------------------------------
	// Interpreter interface implementation
	
	// Evaluate a script
	public void eval (String script)
	{
		XScriptEvent event = new XScriptEvent(script);		
		interp_.getNotifier().queueEvent (event, TCL.QUEUE_TAIL);
		interp_.getNotifier().queueEvent (new StopEvent(this), TCL.QUEUE_TAIL);
	}
	
	// interrupt corrent eval processing
	public void interrupt ()
	{
		running_ = false;
		interp_.interruptEval();
	}
		
	// Start the interpreter in a separate thread
	public void start ()
	{
		thread_ = new Thread(this);
		thread_.start();
	}
	
	// Stop an interpreter thread
	public void stop ()
	{
		running_ = false;
	}
	
	// Set a callback listener. 
	// Called for the interrupt, stop and finish of eval functions
	public void setActionListener (ActionListener listener)
	{
		listener_ = listener;
	}
	
	// Pass a stream object to which all output is redirected
	// By default System.out
	public void setOutputStream (java.io.InputStream out)
	{
		
	}
	
	// Pass a stream object from which all input is processed
	// By default System.in
	public void setInputStream (java.io.OutputStream in)
	{
		
	}
	
	// Return the currently set input stream object
	public java.io.InputStream getInputStream()
	{
		return null;
	}
	
	// Return the currently set output stream object
	public java.io.OutputStream getOutputStream()
	{
		return null;
	}
}
