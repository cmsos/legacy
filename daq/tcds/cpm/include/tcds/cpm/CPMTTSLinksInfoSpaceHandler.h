#ifndef _tcds_cpm_CPMTTSLinksInfoSpaceHandler_h_
#define _tcds_cpm_CPMTTSLinksInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
    class XDAQAppBase;
  }
}

namespace tcds {
  namespace cpm {

    class CPMTTSLinksInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      CPMTTSLinksInfoSpaceHandler(tcds::utils::XDAQAppBase& xdaqApp,
                                  tcds::utils::InfoSpaceUpdater* updater);
      virtual ~CPMTTSLinksInfoSpaceHandler();

      tcds::utils::XDAQAppBase& getOwnerApplication() const;

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

    };

  } // namespace cpm
} // namespace tcds

#endif // _tcds_cpm_CPMTTSLinksInfoSpaceHandler_h_
