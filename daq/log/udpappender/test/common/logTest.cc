#include <log4cplus/logger.h>
#include <udpappender/UDPAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/ndc.h>
#include <log4cplus/helpers/loglog.h>

using namespace log4cplus;

int main(int argc, char**argv)
{
	if (argc < 3)
	{
		std::cout << argv[0] << "host port" << std::endl;
		return 1;
	}

	std::string host = argv[1];
	int port = atoi(argv[2]);

    helpers::LogLog::getLogLog()->setInternalDebugging(true);
    SharedAppenderPtr a(new UDPAppender(host, port, "myserver", "NULL"));
    a->setName("UDPAppender");
    Logger::getRoot().addAppender(a);

    Logger root = Logger::getRoot();
    Logger test = Logger::getInstance("test");
    Logger subTest = Logger::getInstance("test.subtest");

    LOG4CPLUS_INFO(test, "Hello test");
    LOG4CPLUS_WARN(test, "How are you today?");
    LOG4CPLUS_ERROR(subTest, "This is an error");
    LOG4CPLUS_FATAL(subTest, "Now this is really bad");

    return 0;
}


