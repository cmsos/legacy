// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "PackageInfoExample.h"
#include "PackageInfoModuleV.h"
#include "xdaqV.h"

XDAQ_INSTANTIATOR_IMPL(PackageInfoExample)

GETPACKAGEINFO(PackageInfoModule)

void PackageInfoModule::checkPackageDependencies() 
{
        CHECKDEPENDENCY(xdaq);
}

std::set<std::string, std::less<std::string> > PackageInfoModule::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,xdaq);
    return dependencies;
}

