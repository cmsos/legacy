/*
 * Copyright 2017 CERN for the benefit of the CMS Collaboration
 *
 * WorkRequest.cpp
 *
 *  Created on: Jan 19, 2018
 *      Author: Michael Lettrich (michael.lettrich@cern.ch)
 */

#include "mpila/WorkRequest.hpp"

#include <mpi.h>

#include <iostream>

namespace mpila
{
WorkRequest::WorkRequest()
    : memoryBuffer(nullptr),
      size(0),
      tag(MPI_ANY_TAG),
      workRequestType(WorkRequestType::UNDEFINED),
      cookie(nullptr),
      context(nullptr)
{
}
} /* namespace mpila */
