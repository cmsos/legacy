package xdaq.xdesk.jtreeeditor;

import java.util.EventObject;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;

public class JTreeTableCellEditor
    extends JScrollPane
    implements TableCellEditor, ChangeListener {

//-----------------------Variables Declaration----------------------------------

  public JTable tbl;
  public int col;
  JTextArea textArea = null; //used as editor for table cell;

//-------------------------Constructor------------------------------------------
  /**Constructor*/
  public JTreeTableCellEditor() {
    super();
    this.setAutoscrolls(true);
    textArea = new JTextArea();
    textArea.setFont(new Font("Arial", Font.PLAIN, 11));
    textArea.setBorder(BorderFactory.createLineBorder(Color.white));
    textArea.setLineWrap(true);
    textArea.setBackground(Color.LIGHT_GRAY);
    this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    this.getViewport().add(textArea);
    this.setHorizontalScrollBar(new JScrollBar());
    this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
  }

//******************************************************************************
   /**Not implemented*/
   public void stateChanged(ChangeEvent e) {
     //do nothing
   }

//******************************************************************************
   /**Forwards the message from the CellEditor to the delegate.*/
   public Component getTableCellEditorComponent(JTable table, Object value,
                                                boolean isSelected, int row,
                                                int column) {
     col = column;
     tbl = table;
     textArea.setText(table.getValueAt(row, column).toString());
     return this;
   }

//******************************************************************************
   /**Not implemented*/
   public void removeCellEditorListener(CellEditorListener l) {
   }

//******************************************************************************
   /**Not implemented*/
   public void addCellEditorListener(CellEditorListener l) {
   }

//******************************************************************************
   /**Not implemented*/
   public void cancelCellEditing() {
   }

//******************************************************************************
   /**Forwards the message from the CellEditor to the delegate.*/
   public boolean stopCellEditing() {
     JTreeCellRenderer n_parent = (JTreeCellRenderer)this.getParent();
     JTreeEditor xml_tree = n_parent.get_Tree();
     xml_tree.cancelEditing();
     return true;
   }

//******************************************************************************
   /**Always returns true*/
   public boolean shouldSelectCell(EventObject anEvent) {
     return true;
   }

//******************************************************************************
   /**Always returns true*/
   public boolean isCellEditable(EventObject anEvent) {
     return true;
   }

//******************************************************************************
   /**Returns the Cell editor value*/
   public Object getCellEditorValue() {
     return textArea.getText();
   }

//******************************************************************************
}
