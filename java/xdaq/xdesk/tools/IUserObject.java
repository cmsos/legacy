package xdaq.xdesk.tools;

import javax.swing.tree.*;

import org.w3c.dom.*;

public interface IUserObject {

  public Node domNode = null;

  public IUserObject getParent();

  public int getChildCount();

  public int getIndex(TreeNode node);

  public boolean getAllowsChildren();

  public Node getDomNode();

  public boolean treeElement(String elementName);

  public String getNodeName();

  public String getAttribute(String name);

  public String toString();

  public String content();

  public int childCount();

  public boolean isLeaf();

  public int changeTextElement(String element, String value);

  public String getName();

}
