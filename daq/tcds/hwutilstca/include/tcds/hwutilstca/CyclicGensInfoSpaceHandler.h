#ifndef _tcds_hwutilstca_CyclicGensInfoSpaceHandler_h_
#define _tcds_hwutilstca_CyclicGensInfoSpaceHandler_h_

#include <string>

#include "tcds/utils/InfoSpaceHandler.h"

namespace xdaq {
  class Application;
}

namespace tcds {
  namespace utils {
    class InfoSpaceUpdater;
    class Monitor;
    class WebServer;
  }
}

namespace tcds {
  namespace hwutilstca {

    class CyclicGensInfoSpaceHandler : public tcds::utils::InfoSpaceHandler
    {

    public:
      CyclicGensInfoSpaceHandler(xdaq::Application& xdaqApp,
                                 tcds::utils::InfoSpaceUpdater* updater,
                                 unsigned int const numCyclicGens);
      virtual ~CyclicGensInfoSpaceHandler();

    protected:
      virtual void registerItemSetsWithMonitor(tcds::utils::Monitor& monitor);
      virtual void registerItemSetsWithWebServer(tcds::utils::WebServer& webServer,
                                                 tcds::utils::Monitor& monitor,
                                                 std::string const& forceTabName="");

      virtual std::string formatItem(tcds::utils::InfoSpaceHandler::ItemVec::const_iterator const& item) const;

    private:
      unsigned int const numCyclicGens_;

    };

  } // namespace hwutilstca
} // namespace tcds

#endif // _tcds_hwutilstca_CyclicGensInfoSpaceHandler_h_
