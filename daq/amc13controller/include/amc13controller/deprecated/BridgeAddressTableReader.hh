#ifndef _amc13_BridgeAddressTableReader_hh_
#define _amc13_BridgeAddressTableReader_hh_

#include <string>

#include "hal/PCIAddressTableDynamicReader.hh"

/**
 *
 *
 *     @short A reader which generated the VME64x configuration space
 *
 *            This class is a hardcoded implementation of the address-
 *            items of the VME64x specification.
 *
 *       @see
 *    @author Christoph Schwick
 * $Revision: 1.4 $
 *     $Date: 2008/11/27 17:01:32 $
 *
 *
 **/

namespace amc13controller {

    class BridgeAddressTableReader : public HAL::PCIAddressTableDynamicReader {

    public:

        BridgeAddressTableReader();

    };

} /* namespace amc13controller */

#endif 
