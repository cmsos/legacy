#include "ttc/utils/VMEReadCache.hh"

#include "ttc/utils/Address.hh"
#include "ttc/utils/GenericTTCModule.hh"

#include "hal/VMEDevice.hh"


using namespace std;


// class ttc::VMEReadCache

ttc::VMEReadCache::VMEReadCache(HAL::VMEDevice& _vme)
:
    enabled_level(0),
    vme(_vme)
{}


void ttc::VMEReadCache::EnableReadCache()
{
  ++enabled_level;
}


void ttc::VMEReadCache::DisableReadCache()
{
  assert(enabled_level > 0);
  --enabled_level;

  // Clear the cache if we reached the level zero.
  if (enabled_level == 0)
    cache.clear();
}


uint32_t ttc::VMEReadCache::Read(const Address& add, const uint32_t index)
{
  bool dummy;
  return Read(add, index, dummy);
}


uint32_t ttc::VMEReadCache::Read(const Address& add, const uint32_t index, bool& was_cached)
{
  pair<string, uint32_t> key(add.GetName(), index);

  map<pair<const string, uint32_t>, uint32_t>::const_iterator it = cache.find(key);

  uint32_t retval;

  if (enabled_level == 0 || it == cache.end())
  {
    // Either the cache is disabled or the register is not cached.
    retval = ReadFromHardware(add, index);

    if (enabled_level > 0)
      // Put it in the cache now.
      cache[key] = retval;

    // Set the flag that this is not coming from the cache.
    was_cached = false;
  }
  else
  {
    // We have found it in the cache.
    retval = it->second;

    // Set the flag that this value comes from the cache.
    was_cached = true;
  }

  return retval;
}


uint32_t ttc::VMEReadCache::ReadFromHardware(const Address& add, const uint32_t index)
{
  MutexHandler h(ttc::GenericTTCModule::vme_mutex);

  uint32_t temp = 0;
  vme.hardwareRead(add.VMEAddress(index), &temp);
  return temp;
}
