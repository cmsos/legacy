#!/usr/bin/env python

###############################################################################
## A little script to enable/disable the TTS history logging on the
## PIControllers. To be used just while commissioning the TTS history
## logging.
###############################################################################

import os
import sys
import urllib2

from pytcds.utils.tcds_cmd_line_base import CmdLineBase
from pytcds.utils.tcds_utils_soap import build_xdaq_soap_command_message
from pytcds.utils.tcds_utils_soap import extract_xdaq_soap_fault
from pytcds.utils.tcds_utils_soap import send_xdaq_soap_message
from pytcds.utils.tcds_utils_soap import SOAP_PROTOCOL_VERSION_DEFAULT

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

class ModeSwitcher(CmdLineBase):

    STATE_CHOICES = ["on", "off"]

    def __init__(self, description=None, usage=None, epilog=None):
        super(ModeSwitcher, self).__init__(description, usage, epilog)
        self.state = None
        # End of __init__().

    def setup_parser_custom(self):
        # Start with the basic parser configuration.
        super(ModeSwitcher, self).setup_parser_custom()
        parser = self.parser

        # Add the target maintenance mode state argument.
        help_str = "The target maintenance mode state"
        parser.add_argument("state",
                            type=str,
                            choices=ModeSwitcher.STATE_CHOICES,
                            help=help_str)
        # End of setup_parser_custom().

    def handle_args(self):
        super(ModeSwitcher, self).handle_args()
        # Extract the desired new state maintenance mode state (i.e.,
        # on or off).
        self.state = self.args.state.lower()
        # End of handle_args().

    def main(self):

        if self.target == 'all':
            targets = self.setup.sw_targets.keys()
        else:
            targets = [self.target]

        for target in targets:
            # Check if the target name is valid.
            self.validate_target(target, target_type=CmdLineBase.TARGET_TYPE_SW)

            #----------

            # Now actually do what we have been asked to do: send a SOAP
            # command to some targets.
            target_info = self.setup.sw_targets[target]
            host_name = target_info.host
            port_number = target_info.port
            lid_number = target_info.lid
            command_name = "TTSHistoryLoggingEnable"
            if self.state == 'off':
                command_name = "TTSHistoryLoggingDisable"
            url_tmp = "http://{0:s}:{1:d}/urn:xdaq-application:lid={2:d}/{3:s}"
            url = url_tmp.format(host_name, port_number, lid_number, command_name)
            try:
                response = urllib2.urlopen(url)
                html = response.read()
            except Exception as err:
                print "Encountered a problem: '{}'".format(str(err))

        # End of main().

    # End of class ModeSwitcher.

###############################################################################

if __name__ == "__main__":

    desc_str = "A little script to enable/disable " \
               "the TTS history logging on the PIControllers. " \
               "To be used just while commissioning the TTS history logging."
    usage_str = "usage: %(prog)s target [options] on/off (user target='all' to apply to all targets in the system)"

    res = ModeSwitcher(desc_str, usage_str).run()

    print "Done"
    sys.exit(res)

###############################################################################
