//
//  XPropMenuBar.java
//  
//
//  Created by Luciano Orsini on Sun Jun 29 2003.
//  Copyright (c) 2003 __CERN__. All rights reserved.
//

package xdaq.xdesk.xscript;

import javax.swing.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import java.io.*;
import org.w3c.dom.*;
import java.awt.*;
import java.awt.event.*;
import org.apache.log4j.*;
import xdaq.xdesk.tools.*;

public class XScriptMenuBar extends xdaq.xdesk.tools.MenuBar 
{
        Logger logger = Logger.getLogger (XScriptMenuBar.class);
	XScript controller_;
	
	public XScriptMenuBar(XScript controller)
	{
		super("XScript",(MenuBarListener)controller);
			
		controller_ = controller;
			
		JMenu menu;
		JMenuItem item;
		
		// FILE menu
		//
		menu = new JMenu ("File");
		menu.setMnemonic ('f');
		
		item = new JMenuItem (new AbstractAction ("New...") 
		{
                    public void actionPerformed (ActionEvent e)
                    {		  
                             controller_.getViewer().newScript();

                    }});

		menu.add (item);
		
		// Open a new script
		item = new JMenuItem (new OpenFileAction ( controller_.getViewer() ) );
		 
		menu.add (item);
		
		item = new JMenuItem (new AbstractAction ("Save") 
		{
                    public void actionPerformed (ActionEvent e)
                    {
                             controller_.getViewer().saveScript();

                    }});

		menu.add (item);
		
		item = new JMenuItem (new AbstractAction ("Save As...") 
		{
                    public void actionPerformed (ActionEvent e)
                    {
                             controller_.getViewer().saveAsScript();

                    }});

		menu.add (item);
		
		item = new JMenuItem (new AbstractAction ("Close") 
		{
                    public void actionPerformed (ActionEvent e)
                    {
                             controller_.getViewer().closeScript();

                    }});

		menu.add (item);
						
		item = new JMenuItem (new AbstractAction ("Close Window") 
		{
                    public void actionPerformed (ActionEvent e)
                    {
		    	// Dispose the frame into which the viewer panel has been put.
			// This will invoke the proper shutdown function of the XPropWindowListener
                             controller_.closeAction();

                    }});

		menu.add (item);
				
		this.add (menu);
		
        }        
}
