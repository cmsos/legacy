// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius                                     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include "pt/blit/PipeConnectionAccepted.h"

pt::blit::PipeConnectionAccepted::PipeConnectionAccepted(tcpla::EndPoint ep_handle, int index, int sid, const std::string & peername, size_t port ):
ep_handle_(ep_handle),index_(index),sid_(sid),peername_(peername),port_(port)
{

}

int pt::blit::PipeConnectionAccepted::getPipeIndex()
{
	return index_;
}

int pt::blit::PipeConnectionAccepted::getSID()
{
	return sid_;
}

std::string pt::blit::PipeConnectionAccepted::getPeerName()
{
	return peername_;
}

size_t pt::blit::PipeConnectionAccepted::getPort()
{
	return port_;
}

tcpla::EndPoint pt::blit::PipeConnectionAccepted::getEndPoint()
{
	return ep_handle_;
}

