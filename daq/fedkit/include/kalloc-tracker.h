/* This programs are designed to run in linux kernel as well as in 
user applications (for testing puposes) 
It keeps track of unclaimed memory and wrongly freed memory */
/* fedkit documentation is at http://cern.ch/cano/fedkit/ */
/*
 Maintainer : Eric Cano, CERN-EP/CMD <Eric.Cano@cern.ch>
 
 $Id: kalloc-tracker.h,v 1.1 2007/10/09 08:19:01 cano Exp $
*/

#ifndef __KALLOC_TRACKER_H__
#define __KALLOC_TRACKER_H__

#if 0 /* disable ident info due to link problems */
static char *rcsid_kalloc_h = "@(#) $Id: kalloc-tracker.h,v 1.1 2007/10/09 08:19:01 cano Exp $";
/* The following lines will prevent `gcc' version 2.X
   from issuing an "unused variable" warning. */
#if __GNUC__ >= 2
#define USE(var) void use_##var (void) {var = var;}
USE (rcsid_kalloc_h);
#endif
#endif /* if 0 disable... */


#ifdef DEBUG_ALLOCATION

#ifdef __KERNEL__
#include <linux/kernel.h>
#else
#include <unistd.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
	void * kmalloc_tracked (size_t size, int flags, const char *file, int line);
	void kfree_tracked (void *p, const char *file, int line);
	void alloc_tracker_final_cleanup (void);
	/*struct xdsh_msg * xdsh_alloc_tracked (U32 size, const char *file, int line);
	void xdsh_free_tracked (struct xdsh_msg *msg, const char *file, int line);*/

#ifdef __cplusplus
}

inline
void * operator new (size_t size)
{
	idprintf ("**********************");
  return kmalloc_tracked (size, 0, "unknown file", 0);
}

inline
void * operator new[] (size_t size)
{
	idprintf ("**********************");
  return kmalloc_tracked (size, 0, "unknown file", 0);
}

inline
void operator delete (void *p)
{
	idprintf ("**********************");
 kfree_tracked (p, "unknown file", 0);
}

inline
void operator delete[] (void *p)
{
	idprintf ("**********************");
  kfree_tracked (p, "unknown file", 0);
}


#endif

#ifdef __KERNEL__
#define kmalloc(A,B) kmalloc_tracked((A),(B),__FILE__,__LINE__)
#define kfree(A) kfree_tracked((A),__FILE__,__LINE__)
#else
#define malloc(A) kmalloc_tracked((A),0,__FILE__,__LINE__)
#define free(A) kfree_tracked((A),__FILE__,__LINE__)
#endif

/*#define xdsh_alloc(A) xdsh_alloc_tracked((A),__FILE__,__LINE__)*/
/*#define xdsh_free(A) xdsh_free_tracked((A),__FILE__,__LINE__)*/

#endif /* def DEBUG_ALLOCATION */
#endif /* ndef __KALLOC_TRACKER_H__ */
