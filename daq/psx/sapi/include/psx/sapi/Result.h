// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_sapi_Result_h_
#define _psx_sapi_Result_h_

#include <string>
     
namespace psx {
	namespace sapi {
	
		class Request;
		
		class Result 
		{
      			public:
			
			Result(Request * request);

			virtual ~Result() {}
			
			virtual Request * getRequest();
			
			virtual bool hasError();
			
			virtual void setError(const std::string & message);
			
			virtual std::string  getError();
			
			private:
			
			Request * request_;
			bool hasError_;
			std::string errorMessage_;
		};
	}
}

#endif 

