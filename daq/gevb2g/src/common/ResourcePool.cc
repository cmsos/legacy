#include "gevb2g/ResourcePool.h"
#include "toolbox/utils.h"

// Log4CPLUS
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/layout.h"

gevb2g::ResourcePool::ResourcePool (xdaq::Application * owner)
	: xdaq::Object(owner)
{

}

gevb2g::ResourcePool::FragmentHistoryRecord::FragmentHistoryRecord ()
{
	totalSize_ = 0;
	partSize_ = 0;
	tid_ = 0;
}

gevb2g::ResourcePool::FragmentHistoryRecord::FragmentHistoryRecord (size_t totalSize, size_t partSize, size_t tid)
{
	totalSize_ = totalSize;
	partSize_ = partSize;
	tid_ = tid;
}

void gevb2g::ResourcePool::init (size_t maxResources, size_t maxFragments, size_t mtuSize, size_t memcopyEnabled)
{
	maxFragments_ = maxFragments;
	maxResources_ = maxResources;
	mtuSize_ = mtuSize;
	memcopyEnabled_ = memcopyEnabled;

	fragmentTable_.resize(maxResources);

	ResourceDescriptor resource;
	resource.totalSize = 0;
	resource.currentFragments = 0;
	resource.eventCompletion = 0;
	resourceTable_.resize(maxResources_);
	for (size_t i = 0; i < maxResources; i++)
	{
		resourceTable_[i] = resource;
		resourceTable_[i].context = 0;
		resourceTable_[i].sglSize = 0;
		resourceTable_[i].reservedSize = 0;
		fragmentTable_[i].resize(maxFragments);
		for (size_t j = 0; j < fragmentTable_[i].size(); j++)
		{
			(fragmentTable_[i])[j].ref = 0;
			(fragmentTable_[i])[j].last = 0;
			(fragmentTable_[i])[j].totalSize = 0;
			(fragmentTable_[i])[j].currentSize = 0;
			(fragmentTable_[i])[j].blockOffset = 0;
			(fragmentTable_[i])[j].currentBlock = 0;
			(fragmentTable_[i])[j].completion = 0;
			(fragmentTable_[i])[j].historyIndex = 0;
			(fragmentTable_[i])[j].history.resize(MaxHistoryRecords);
		}
	}
}

void gevb2g::ResourcePool::showResourceHistory (size_t reasourceId)
{
}

void gevb2g::ResourcePool::showFragmentHistory (size_t r, size_t f)
{
	std::string msg = toolbox::toString("History of fragment %d (resource %d)\n", f, r);
	msg += toolbox::toString("current build size: %d\n", (fragmentTable_[r])[f].currentSize);
	msg += toolbox::toString("buffer reference %x\n", (fragmentTable_[r])[f].ref);
	msg += toolbox::toString("expected total size %d\n", (fragmentTable_[r])[f].totalSize);
	msg += toolbox::toString("history index %d\n", (fragmentTable_[r])[f].historyIndex);
	msg += toolbox::toString("current block %d\n", (fragmentTable_[r])[f].currentBlock);
	msg += toolbox::toString("completion value %d\n", (fragmentTable_[r])[f].completion);
	msg += toolbox::toString("event completion %d\n", resourceTable_[r].eventCompletion);
	msg += toolbox::toString("size of sgl %d\n", resourceTable_[r].sglSize);

	for (size_t i = 0; i < (fragmentTable_[r])[f].historyIndex; i++)
	{
		msg += toolbox::toString("fragment part %d, total length %d, part length %d, from tid %d", i, (fragmentTable_[r])[f].history[i].totalSize_, (fragmentTable_[r])[f].history[i].partSize_, (fragmentTable_[r])[f].history[i].tid_);
	}
}

size_t gevb2g::ResourcePool::sglLen (size_t r)
{
	return resourceTable_[r].sglSize;
}

void gevb2g::ResourcePool::retrieve (size_t r, I2O_V2_SGE_FRAGMENT_ELEMENT_U32 * sgl)
{
	//point to first fragment
	I2O_V2_SGE_FRAGMENT_ELEMENT_U32 * sgloffset = sgl;
	I2O_V2_SGE_FRAGMENT_ELEMENT_U32 * sgllast = sgl;
	sgloffset->ElementType = I2O_V2_SGL_FRAGMENT_LIST_ELEMENT;
	sgloffset->ElementLength = 0;
	sgloffset->FragmentCount = 0;

	//*wc = 0;

	size_t reservedSize = resourceTable_[r].reservedSize;

	for (size_t f = 0; f < resourceTable_[r].currentFragments; f++)
	{ // loop over fragment

		size_t i = 0;
		size_t remainingSize = (fragmentTable_[r])[f].totalSize;
		toolbox::mem::Reference * ref = (fragmentTable_[r])[f].ref;

		sgloffset->ElementType = I2O_V2_SGL_FRAGMENT_LIST_ELEMENT;
		sgloffset->FragmentCount = 0;

		while (ref != (toolbox::mem::Reference*) 0)
		{	// loop over blocks 
			sgloffset->FragmentEntry[i].PhysicalAddress = (size_t)((size_t) ref->getDataLocation() + reservedSize);

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), toolbox::toString("Remaining %d, mtuSize %d, reserved %d", remainingSize, mtuSize_, reservedSize));

			sgloffset->FragmentEntry[i].ByteCount = MIN(remainingSize, (mtuSize_ - reservedSize));
			sgloffset->FragmentCount++;
			remainingSize -= sgloffset->FragmentEntry[i].ByteCount;

			//*wc += (sgloffset->FragmentEntry[i].ByteCount >> 2 );

			ref = ref->getNextReference();
			i++;
		}

		// summarize SGL for fragment fragment j
		sgloffset->ElementLength = (sizeof(I2O_V2_SGE_FRAGMENT_ELEMENT_U32) + (sizeof(I2O_FRAGMENT_ENTRY_U32) * (sgloffset->FragmentCount)) - sizeof(I2O_FRAGMENT_ENTRY_U32)) >> 2;

		//Move to next fragment
		sgllast = sgloffset;
		sgloffset = (I2O_V2_SGE_FRAGMENT_ELEMENT_U32*) &sgloffset->FragmentEntry[i];

	}

	//Mark the last SGL element
	sgllast->ElementType = I2O_SGL_FLAGS_LAST_ELEMENT | I2O_V2_SGL_FLAGS_END_OF_BUFFER | sgllast->ElementType;

}

void gevb2g::ResourcePool::reset ()
{
	for (size_t i = 0; i < resourceTable_.size(); i++)
	{
		this->clear(i);
	}
}

void gevb2g::ResourcePool::available (size_t r, size_t context)
{
	resourceTable_[r].context = context;
}

void gevb2g::ResourcePool::clear (size_t r)
{
	for (size_t i = 0; i < fragmentTable_[r].size(); i++)
	{
		(fragmentTable_[r])[i].totalSize = 0;
		(fragmentTable_[r])[i].currentSize = 0;
		(fragmentTable_[r])[i].blockOffset = 0;
		(fragmentTable_[r])[i].currentBlock = 0;
		if ((fragmentTable_[r])[i].ref != (toolbox::mem::Reference*) 0)
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), toolbox::toString("freeing fragment block chain r:%d f:%d", r, i));
			//xdaq::frameFree((fragmentTable_[r])[i].ref);
			((fragmentTable_[r])[i].ref)->release();
		}
		(fragmentTable_[r])[i].ref = 0;
		(fragmentTable_[r])[i].last = 0;
		(fragmentTable_[r])[i].historyIndex = 0;
		(fragmentTable_[r])[i].completion = 0;
	}
	resourceTable_[r].currentFragments = 0;
	resourceTable_[r].totalSize = 0;
	resourceTable_[r].eventCompletion = 0;
	resourceTable_[r].context = 0;
	resourceTable_[r].sglSize = 0;
	resourceTable_[r].reservedSize = 0;
}

size_t gevb2g::ResourcePool::size (size_t r)
{
	return resourceTable_[r].totalSize;
}

bool gevb2g::ResourcePool::appendZeroCopy (size_t r, size_t f, toolbox::mem::Reference * ref, size_t length, size_t totalSize, size_t tid, size_t context, size_t reservedSize) 
{
	resourceTable_[r].reservedSize = reservedSize;

	if (resourceTable_[r].context != context)
	{
		std::stringstream msg;
		msg << toolbox::toString("resource context clash, expected context is: %08x,received context was:%08x", resourceTable_[r].context, context);
		LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), msg.str());
		XCEPT_RAISE(xdaq::exception::Exception, msg.str());
	}

	resourceTable_[r].sglSize += sizeof(I2O_FRAGMENT_ENTRY_U32);

	if ((fragmentTable_[r])[f].ref == (toolbox::mem::Reference*) 0)
	{
		(fragmentTable_[r])[f].ref = ref;
		(fragmentTable_[r])[f].last = ref;
		(fragmentTable_[r])[f].currentBlock = 1;
		(fragmentTable_[r])[f].totalSize = totalSize;
		(fragmentTable_[r])[f].currentSize = 0;
	}
	else
	{
		(fragmentTable_[r])[f].last->setNextReference(ref);
		(fragmentTable_[r])[f].last = ref;
		(fragmentTable_[r])[f].currentBlock++;
	}

	(fragmentTable_[r])[f].currentSize += length;

	// set history
	(fragmentTable_[r])[f].history[(fragmentTable_[r])[f].historyIndex] = FragmentHistoryRecord(totalSize, length, tid);
	(fragmentTable_[r])[f].historyIndex++;

	if ((fragmentTable_[r])[f].totalSize == (fragmentTable_[r])[f].currentSize)
	{
		(fragmentTable_[r])[f].completion = 1;
		resourceTable_[r].totalSize += (fragmentTable_[r])[f].totalSize;
		resourceTable_[r].currentFragments++;

		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), toolbox::toString("fragment %d of resource %d completed", f, r));

		// check resource completion
		if (resourceTable_[r].currentFragments == maxFragments_)
		{
			resourceTable_[r].eventCompletion = 1;
			resourceTable_[r].sglSize += (sizeof(I2O_V2_SGE_FRAGMENT_ELEMENT_U32) - sizeof(I2O_FRAGMENT_ENTRY_U32)) * maxFragments_;

			return true;
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), toolbox::toString("all fragments of resource %d  completed", r));
		}
	}
	else if ((fragmentTable_[r])[f].currentSize > (fragmentTable_[r])[f].totalSize)
	{
		std::stringstream msg;
		msg << "fragment overflow error";
		LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), msg.str());
		XCEPT_RAISE(xdaq::exception::Exception, msg.str());

	}
	return false;
}

toolbox::mem::Reference* gevb2g::ResourcePool::getRef (size_t resource, size_t fragment)
{
	return (fragmentTable_[resource])[fragment].ref;
}
