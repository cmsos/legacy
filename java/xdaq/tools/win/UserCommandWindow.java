package xdaq.tools.win;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import java.util.*;

import org.w3c.dom.*;

import xdaq.tools.win.xdaqWin;

import xdaq.tools.win.QueryInterfaceAction;

import xdaq.tools.win.TimerKnob;
import xdaq.tools.win.ComboRenderer;
import xdaq.tools.win.ComboListener;
import xdaq.tools.win.ComboItem;

import xdaq.tools.win.DOMtoString;

public class UserCommandWindow extends JFrame
{
	xdaqWin win_;
	TreePath[] paths_;
	
	JComboBox commandComboBox_;
	
	Vector    replies_; // stores the replies received from the targets in the order of sending
	JComboBox targets_; // stores the targets to which commands have been sent
	JEditorPane resultTextPane_;
  	JEditorPane parameterTextPane_;
	
	public UserCommandWindow (xdaqWin frame)
	{
		super ("User Command");
		setSize (300, 300);
		win_ = frame;
		
		replies_ = new Vector();
		
		JTabbedPane tabbed = new JTabbedPane();
		
		// General information tab
		//
		JPanel commandBox = new JPanel();
		tabbed.addTab ("Command", commandBox);
		
		commandComboBox_ = new JComboBox();
		commandComboBox_.setRenderer(new ComboRenderer());
    		
		
		commandBox.add(commandComboBox_, BorderLayout.NORTH);
				
		// Parameter Buttons
		//
		ActionListener cancelAction = new ActionListener()
		{
			public void actionPerformed ( ActionEvent e) 
			{
				//setVisible ( false );
				dispose();
			}
		};
		
		JPanel windowButtons = new JPanel();
		JButton okButton = new JButton ("Ok");
		okButton.addActionListener ( cancelAction );
		
		JButton cancelButton = new JButton ("Cancel");
		cancelButton.addActionListener ( cancelAction );
		
		JButton applyButton = new JButton ("Apply");
		
		applyButton.addActionListener ( new ExecuteCommand() );
		
		windowButtons.add ( okButton );
		windowButtons.add ( cancelButton );
		windowButtons.add ( applyButton );
		
		okButton.setEnabled (true);
		applyButton.setEnabled (true);
		cancelButton.setEnabled (true);
		
		this.getContentPane().add (tabbed, BorderLayout.CENTER);
		this.getContentPane().add (windowButtons, BorderLayout.SOUTH);
		
		// Preapre Input Parameter Tab
		//
		JPanel parameterPanel = new JPanel( new BorderLayout() );
		parameterTextPane_ = new JEditorPane("text/plain", "");
		JScrollPane scrollPane = new JScrollPane( parameterTextPane_ );
		parameterPanel.add ( scrollPane, BorderLayout.CENTER);
		tabbed.addTab ("Parameter", parameterPanel);
		
		// Prepare Result tab
		//
		JPanel resultPanel = new JPanel( new BorderLayout() );
		targets_ = new JComboBox();
		
		targets_.addActionListener ( new TargetSelectionListener() );
		resultPanel.add ( targets_, BorderLayout.NORTH);
		
		resultTextPane_ = new JEditorPane("text/plain", "Results of commands are displayed here.");
		scrollPane = new JScrollPane( resultTextPane_ );
		resultPanel.add ( scrollPane, BorderLayout.CENTER);
		
		tabbed.addTab ("Result", resultPanel);
		
		// Query the interface from the selected nodes
		//
		rememberTreeSelection();
		queryInterface();
		
		// the listener is not yet safe. There have to be items in the
		// ComboBox before adding the listener, otherwise the listener
		// will fail to set the items enabled/disabled.
		//
		commandComboBox_.addActionListener(new ComboListener(commandComboBox_));
		
		this.show();		
	}
	
	
	
	public void rememberTreeSelection()
	{
		TreePath[] paths = (TreePath[]) win_.getTree().getSelectionModel().getSelectionPaths();
		
		int pathLength = paths.length;
		paths_ = (TreePath[]) new TreePath [pathLength];
		for (int k = 0; k < pathLength; k++)
		{
			paths_[k] =  new TreePath ( paths[k].getPath() );
		}
		
		// Prepare window title and fill the
		// targets_ combo box for selecting replies
		//
		for ( int i = 0; i < pathLength; i++)
		{
			String info = new String ("");
			
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths_[i].getLastPathComponent();
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();
			if (node.toString().equals("Application") || node.toString().equals("Transport"))
			{
				String className = aNode.getAttribute ("class");
				String instance = aNode.getAttribute ("instance");
				info = new String (className + " (" + instance + ")");
			} else if (node.toString().equals("Context") ) 
			{
				String id = aNode.getAttribute ("id");
				info = new String ("Context (" + id +")");
			}
			
			this.setTitle ( "User command: " + info );
			targets_.addItem ( info );
			
		}
		
		// If there has been more than one node selected, override the
		// title of this window
		//
		if (pathLength > 1)
		{
			this.setTitle ("User command: " + pathLength + " items");
		}  
		{
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) paths_[0].getLastPathComponent();
			xdaqWinTreeAdapterNode aNode = (xdaqWinTreeAdapterNode) node.getUserObject();
			if (node.toString().equals("Application") || node.toString().equals("Transport"))
			{
				String className = aNode.getAttribute ("class");
				String instance = aNode.getAttribute ("instance");
				this.setTitle ("User command: " + className + " (" + instance + ")");
			} else if (node.toString().equals("Context") ) 
			{
				String id = aNode.getAttribute ("id");
				this.setTitle ("Properties: Context (" + id +")");
			}
		}
	}
	
	public void addMethod ( String methodName, boolean enabled )
	{
		ComboItem item = new ComboItem ( methodName, enabled );
		commandComboBox_.addItem ( item );
	}
	
	public TreePath[] getSelection()
	{
		return paths_;
	}
	
	public void queryInterface()
	{
		QueryInterfaceAction action = new QueryInterfaceAction(win_, this);
		action.actionPerformed();
	}
	
	void updateResultTab()
	{
		if (replies_.size() > 0) {
			int i = targets_.getSelectedIndex();
			Document doc = (Document) replies_.elementAt(i);
			String r = DOMtoString.serialize ( doc.getDocumentElement() /*.getFirstChild()*/ );
			resultTextPane_.setText ( r );
		}
		/*else
		{
			JOptionPane.showMessageDialog(null,"No reply for command", "" ,JOptionPane.ERROR_MESSAGE);
			return;
		}
		*/
	}
	
	
	class ExecuteCommand implements ActionListener
	{
		public void actionPerformed (ActionEvent e)
		{
		
			ComboItem item = (ComboItem) commandComboBox_.getSelectedItem();
			
			//retrieve parameter if any
			String parameterText = parameterTextPane_.getText();
			Node node = null;
			if ( ! parameterText.equals("") ){ // if nothing to send just ignore it
				Document doc  = StringToDOM.dom(parameterTextPane_.getText());
				if ( doc == null ) {
					JOptionPane.showMessageDialog(null,"Malformed xml tag", "Parameter error" ,JOptionPane.ERROR_MESSAGE);
					return;
				}
				node = doc.getDocumentElement();

			}
			try {
				replies_ = win_.executeCommandOnSelection ( item.toString(), node , paths_ );
				updateResultTab();
			}
			catch (xdaqWinException ex)
			{
			
			}
		}
	}
	
	class TargetSelectionListener implements ActionListener
	{
		public void actionPerformed (ActionEvent e)
		{
			updateResultTab();
		}
	}	
}
