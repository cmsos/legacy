#!/usr/bin/env python

try:
    import debug_hook
    import pdb
except ImportError:
    pass

###############################################################################

FIRST_BX = 1
LAST_BX = 3564

###############################################################################

class LogEntry(object):

    # An L1A and a B-go are defined to be in the same BX when the two
    # corresponding strobe pins on a TTCrx decoder chip strobe at the
    # same time. The below number is added to each logged B-command
    # timestamp (actually: to anything that is not an L1A) in order to
    # make such signals also appear in the same BX in the spy
    # log. (I.e., this calibrate the spy timing.)
    # NOTE: This differs between backplane mode and front-panel mode.
    TTCSPY_A_TO_B_CHANNEL_OFFSET_FRONTPANEL = 3
    TTCSPY_A_TO_B_CHANNEL_OFFSET_BACKPLANE = TTCSPY_A_TO_B_CHANNEL_OFFSET_FRONTPANEL + 95

    # There are four 32-bit words in a single TTCSpy memory entry.
    NUM_WORDS_PER_SPY_MEM_ENTRY = 4

    def __init__(self, words, tcds_mode=False):
        # NOTE: The assumption is that the words have already been
        # reversed in order after the IPbus read.
        # ASSERT ASSERT ASSERT
        assert len(words) == LogEntry.NUM_WORDS_PER_SPY_MEM_ENTRY
        # ASSERT ASSERT ASSERT end
        self._words = words
        self.raw_data = None
        self._build_raw_data()
        self._tcds_mode = tcds_mode
        self._ttcspy_a_to_b_channel_offset = LogEntry.TTCSPY_A_TO_B_CHANNEL_OFFSET_FRONTPANEL
        if self._tcds_mode:
            self._ttcspy_a_to_b_channel_offset = LogEntry.TTCSPY_A_TO_B_CHANNEL_OFFSET_BACKPLANE
        # End of __init__().

    def _build_raw_data(self):
        words = self._words
        self.raw_data = (words[0] << 96) + \
                        (words[1] << 64) + \
                        (words[2] << 32) + \
                        words[3]
        # End of _build_raw_data().

    def _unbuild_raw_data(self):
        raw_data = self.raw_data
        words = [0, 0, 0, 0]
        words[0] = (raw_data >> 96) & 0xffffffff
        words[1] = (raw_data >> 64) & 0xffffffff
        words[2] = (raw_data >> 32) & 0xffffffff
        words[3] = raw_data & 0xffffffff
        # End of _unbuild_raw_data().
        return words

    def raw_data_as_string(self):
        res = "0x{0[0]:0>8x} 0x{0[1]:0>8x} 0x{0[2]:0>8x} 0x{0[3]:0>8x} ".format(self._words)
        # End of raw_data_as_string().
        return res

    def extract_raw_data_bit(self, bit):
        res = self.extract_raw_data_bits(bit, bit)
        # End of extract_raw_data_bit().
        return res

    def extract_raw_data_bits(self, bit_lo, bit_hi):

        # DEBUG DEBUG DEBUG
        assert bit_hi >= bit_lo
        # DEBUG DEBUG DEBUG end

        num_bits = bit_hi - bit_lo + 1
        tmp_mask = 0
        for i in xrange(num_bits):
            tmp_mask += 2**i
        mask = tmp_mask << bit_lo

        tmp_res = self.raw_data & mask
        res = tmp_res >> bit_lo

        # End of extract_bits().
        return res

    def get_timestamp(self):
        # NOTE: The original timestamp as given by the firmware is the
        # number of BX since the first entry. Of course not much
        # meaning is left after the A-to-B-channel correction.
        tmp = self.extract_raw_data_bits(64, 101)
        if not self.is_l1a():
            tmp += self._ttcspy_a_to_b_channel_offset
        res = tmp
        # End of get_timestamp().
        return res

    def get_bx_number(self):
        tmp = self.extract_raw_data_bits(112, 123)
        # Subtract one because the spy has already reset the
        # bunch-counter when logging the BC0.
        tmp -= 1
        if self.is_l1a():
            tmp -= self._ttcspy_a_to_b_channel_offset
        # Fix up the range.
        if (tmp < FIRST_BX):
            tmp += LAST_BX
        if (tmp > LAST_BX):
            tmp -= LAST_BX
        res = tmp
        # End of get_bx_number().
        return res

    def is_l1a(self):
        bit = self.extract_raw_data_bit(16)
        res = (bit != 0)
        # End of is_l1a().
        return res

    def _is_bcommand(self):
        """Returns true for short and long B-commands."""

        # This is actually a little tricky to do. The basic trick is
        # to look at non-zero data fields for broadcast data in the
        # entry. If any is found, the entry corresponds to a short,
        # broadcast command. Some care has to be taken in the case of
        # a command with zero as data contents. This case will be
        # flagged by a dedicated strobe bit. (One bit for short and
        # one for long commands, in fact.)

        long_contents = self.extract_raw_data_bits(32, 62)
        long_strobe = self.extract_raw_data_bit(12)
        short_contents = self.extract_raw_data_bits(0, 7)
        short_strobe = self.extract_raw_data_bit(8)
        is_long = (long_contents != 0) or (long_strobe != 0)
        is_short = (short_contents != 0) or (short_strobe != 0)

        # DEBUG DEBUG DEBUG
        assert not (is_short and is_long)
        # DEBUG DEBUG DEBUG end

        # End of _is_bcommand().
        return (is_short, is_long)

    def is_bcommand(self):
        """Returns true for short and long B-commands."""
        tmp = self._is_bcommand()
        # End of is_bcommand().
        return (True in tmp)

    def is_bcommand_short(self):
        return self._is_bcommand()[0]

    def is_bcommand_long(self):
        return self._is_bcommand()[1]

    def is_bcnt_reset(self):
        bit = self.extract_raw_data_bit(0)
        res = (bit != 0)
        # End of is_bcnt_reset().
        return res

    def is_evcnt_reset(self):
        bit = self.extract_raw_data_bit(1)
        res = (bit != 0)
        # End of is_evcnt_reset().
        return res

    def extract_short_bcommand_data(self):
        res = self.extract_raw_data_bits(0, 7)
        # End of extract_short_bcommand_data().
        return res

    def extract_long_bcommand_data(self):
        res = self.extract_raw_data_bits(32, 39)
        # End of extract_long_bcommand_data().
        return res

    def get_bcommand_data(self):
        res = None
        tmp = self._is_bcommand()
        is_short = tmp[0]
        is_long = tmp[1]
        if is_short:
            res = self.extract_short_bcommand_data()
        elif is_long:
            res = self.extract_long_bcommand_data()
        # End of extract_bcommand_data().
        return res

    def get_long_bcommand_address(self):
        res = self.extract_raw_data_bits(49, 62)
        # End of get_long_bcommand_address().
        return res

    def get_long_bcommand_subaddress(self):
        res = self.extract_raw_data_bits(40, 47)
        # End of get_long_bcommand_subaddress().
        return res

    def get_long_bcommand_external_flag(self):
        bit = self.extract_raw_data_bit(48)
        res = (bit != 0)
        # End of get_long_bcommand_external_flag().
        return res

    # End of class LogEntry.

###############################################################################
