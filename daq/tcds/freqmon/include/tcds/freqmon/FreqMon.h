#ifndef _tcds_freqmon_FreqMon_h_
#define _tcds_freqmon_FreqMon_h_

#include <memory>

#include "tcds/freqmon/FreqMonLoop.h"
#include "tcds/utils/XDAQAppBase.h"

namespace xdata {
  class Event;
}

namespace xdaq {
  class ApplicationStub;
}

namespace tcds {
  namespace freqmon {

    class FreqMonInfoSpaceHandler;
    class FreqMonInfoSpaceUpdater;

    class FreqMon : public tcds::utils::XDAQAppBase
    {

      friend class FreqMonLoop;

    public:
      XDAQ_INSTANTIATOR();

      FreqMon(xdaq::ApplicationStub* stub);
      virtual ~FreqMon();

    protected:
      virtual void setupInfoSpaces();

      virtual void actionPerformed(xdata::Event& event);

      // Dummy methods in this case.
      virtual void hwConnectImpl();
      virtual void hwReleaseImpl();
      virtual void hwConfigureImpl();

    private:
      // Various InfoSpaces and their InfoSpaceUpdaters.
      std::auto_ptr<tcds::freqmon::FreqMonInfoSpaceUpdater> freqMonInfoSpaceUpdaterP_;
      std::auto_ptr<tcds::freqmon::FreqMonInfoSpaceHandler> freqMonInfoSpaceP_;

      tcds::freqmon::FreqMonLoop freqMonLoop_;

    };

  } // namespace freqmon
} // namespace tcds

#endif // _tcds_freqmon_FreqMon_h_
