/*
 * DefaultNamespaceContext
 *
 * $Id: DefaultNamespaceContext.java,v 4.1 2007/03/26 14:40:24 ichiro Exp $
 */

package net.hep.cms.xdaqctl;

import java.util.HashMap;
import java.util.Iterator;

// XPath
import javax.xml.*;  // for XMLConstants
import javax.xml.namespace.NamespaceContext;

/**
 * a NamespaceContext implementation used by XPathAPI.select*()
 *
 * @author  Ichiro Suzuki (ichiro@fnal.gov)
 * @version $Name:  $
 */
public class DefaultNamespaceContext implements NamespaceContext
{
	private HashMap<String, String> prefixTable;

	public DefaultNamespaceContext() {
		prefixTable = new HashMap<String, String>();
		prefixTable.put("xml", XMLConstants.XML_NS_URI);
		prefixTable.put("",    XMLConstants.NULL_NS_URI);
	}

	public DefaultNamespaceContext(DefaultNamespaceContext original) {
		prefixTable = new HashMap<String, String>();
		prefixTable.putAll(original.prefixTable);
	}

	public void put(String prefix, String uri) {
		prefixTable.put(prefix, uri);
	}

	public String getNamespaceURI(String prefix) {
		if (prefix == null) {
			throw new NullPointerException("Null prefix");
		}

		if (prefixTable.containsKey(prefix)) {
			return prefixTable.get(prefix);
		} else {
			return prefixTable.get("");
		}
	}

	public String getPrefix(String uri) {
		throw new UnsupportedOperationException();
	}
	public Iterator getPrefixes(String uri) {
		throw new UnsupportedOperationException();
	}
}

// End of file
// vim: set ts=4 sw=4:
