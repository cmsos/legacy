#ifndef __WebTabIF
#define __WebTabIF

#include <iostream>

namespace ferol40
{
    class utils::WebTabIF {
    public:
        virtual ~WebTabIF() {};
        virtual void print( std::ostream *out ) = 0;
        virtual void jsonUpdate( std::ostream *out ) = 0;
    };
}

#endif /* __WebTabIF */
