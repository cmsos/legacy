package xdaq.xdesk.xdesk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.applet.*;
import java.io.*;

import org.apache.log4j.*;

public class TaskSwitcher extends JFrame 
{
	static Logger logger = Logger.getLogger (TaskSwitcher.class);
	
	JList taskList_;
	DefaultListModel model_;
	XDesk xdesk_;

	public TaskSwitcher(XDesk xdesk)
	{
		super("Task Switcher");
		
		this.setSize(300,200);
		
		WindowListener l = new WindowAdapter () 
		{
			public void windowClosing (WindowEvent e) 
			{
				hideWindow();
			}
		};
		
		this.setDefaultCloseOperation (WindowConstants.DO_NOTHING_ON_CLOSE);		
		this.addWindowListener (l);
		
		xdesk_ = xdesk;
		taskList_ = new JList();
		model_ = new DefaultListModel();
		taskList_.setModel (model_);
		
		JPanel panel = new JPanel();
		Box vb = Box.createVerticalBox();
		panel.add(vb);
		Box hb = Box.createHorizontalBox();
		
		vb.add ( new JScrollPane (taskList_) );
		
		vb.add (hb);
		
		
		
		JButton goToB = new JButton ( new AbstractAction ("Go to") {
				public void actionPerformed (ActionEvent e) {
						DeskletStub stub = (DeskletStub) taskList_.getSelectedValue();
						if ( stub != null ) {
							xdesk_.goTo (stub);
						}	
					}}
				);

		JButton forceQuitB = new JButton (new AbstractAction ("Force Quit") {
				public void actionPerformed (ActionEvent e) {
						DeskletStub stub = (DeskletStub) taskList_.getSelectedValue();
						if (  stub != null ) {
							xdesk_.forceQuit (stub);
						}
					}});
		
		
		
		hb.add (goToB);
		hb.add (forceQuitB);
		
		this.getContentPane().add (panel);
		
		
	}
	
	public void hideWindow()
	{
		this.setVisible(false);
	}
	
	// Store stubs that have a toString method for displaying in a list
	//
	public void addTask (DeskletStub stub)
	{
		model_.addElement (stub);
		this.validate();
		
	}

	
	public void removeTask (DeskletStub stub)
	{
		model_.removeElement (stub);
		this.validate();
	}
}
