/*
 * frl_api.h  
 * 
 * this file 
 *
 * $Header: /afs/cern.ch/project/cvs/reps/tridas/TriDAS/daq/extern/frlgen/include/frl_api.h,v 1.1 2004/07/12 12:01:06 frans Exp $
 *
 * mods:
 * 09-jul-2003 split from frl.h
 * 09-may-2003 register layout (pending), change page to block 
 * 12-may-2003 control register (soft reset)        
 * 27-may-2003 CS: changed include file asm/bytorder.h to netinet/in.h
 *                 in order to avoid clashes with XDAQ.
 */


#ifndef _FRL_API_H
#define _FRL_API_H

#ifdef __cplusplus
extern "C" {
#endif


/*************************************************************************
 *
 * data structures and associated typedefs
 *
 *************************************************************************/



/*************************************************************************
 *
 * function prototypes
 * in general all functions return non-zero for error
 * 
 *************************************************************************/


/* 
 * frl_xx() functions
 *   
 * _open() opens zfrl device to get user space mapping to FRL board
 *  and initialises internal pointers
 */
 
int frl_zfrl_open (int * nmb_units) ; 



/*************************************************************************
 *
 *   macros
 *
 *************************************************************************/

#define getPCI(address) (ntohl((*(int volatile *)address)))
#define setPCI(address,value) (*(int volatile *)address) = htonl((value))

#define getPCI32swap(address) (ntohl((*(int volatile *)address)))
#define setPCI32swap(address,value) (*(int volatile *)address) = htonl((value))

#define getPCI32noswap(address) ((*(int volatile *)address))
#define setPCI32noswap(address,value) (*(int volatile *)address) = (value)

#define setPCI16swap(address,value) (*(short volatile *)address) = htons((value))

#define setPCI8(address,value) (*(char volatile *)address) = ((value))



#ifdef __cplusplus
}
#endif

#endif  /* _FRL_API_H_ */


