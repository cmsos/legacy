#include "strstream.h"
#include "XRCAdapter.h"
#include "XRUAdapter.h"
#include "XRC.h"
#include "XFUAdapter.h"
#include "TcpDispatcherAcceptorTaskM.h"
#include "vxioslib.h"
#include "vxTiming.h"
#include "TcpDispatcherAcceptorTaskS.h"

vxChron chrono;	
unsigned long loopCounter = 0;
inline void Benchmarks2(int size)
{
	
	
	if ( loopCounter == 1000 )
	{
		chrono.start(0);
	}
	else
	if ( loopCounter == 101000 )
	{
		loopCounter = 0;
		chrono.stop(0);
                printf("Delta Time : %d usecs \n", chrono.dusecs());
		printf("Rate: %f Hz  \n", (float)100000/(float)(chrono.dusecs()/(float)1000000));
		printf("Bandwitdth: %f Mb\n",
		(((float)100000/(float)(chrono.dusecs()/(float)1000000)) * (float)size)/(float)0x100000);
		printf("Time to process one Event: %f usecs\n", (float)(chrono.dusecs()/(float)100000));
	}
	loopCounter++;
}


unsigned long dbg = 0;

class Bench: public XRCAdapter ,public XRUAdapter, public XFUAdapter {

	public:
	Bench() {
		count_ = 0;
	
	}
	void readout (EventIdentifier * eid ) {
	
		Benchmarks2(128);
	
	}
	void stop(char * str) {
		//count_++;
		//if ( count_ > 100000 ) {
			//::logMsg("Single trip 100000\n", 0, 0, 0, 0, 0, 0);
			//count_ = 0;
		//}
		Benchmarks2(128);
		//::logMsg("Single trip 1\n", 0, 0, 0, 0, 0, 0);
	}
	 void cache (FragmentIdentifier* fid, EventIdentifier* eid,  DestinationCookie* dcookie, FragmentData* fdata) {
		//cout << "cache: " << fdata->length << " "<< count_ <<  endl;
		Benchmarks2(fdata->length);
		count_++;
		if ((count_ % 10000) == 0 ) {
		  cout << "COUNT: " << count_ << "." << fdata->data << endl;
		}
		 
		if ( count_ > 100000 )
		{
			cout << "cache: " << count_ << endl;
			count_ = 0;
			
		}
	}
	int count_;

};


extern "C"  int Main( char * host)
{	
	
	TcpDispatcherAcceptorTaskS tcpd(host,20222);

	Bench bench;
	tcpd.addListener(&bench);
	tcpd.activate();

	char str[256];
	pause();
	cout << "press ENTER to stop" << endl;
	cin >> str;
	return(0);
	
	
	
}

int main(int argc, char* argv[])
{
	return Main(argv[1]);
}

