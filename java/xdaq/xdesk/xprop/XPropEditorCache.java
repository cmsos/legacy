package xdaq.xdesk.xprop;

import javax.swing.*;
import java.util.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import xdaq.xdesk.xtree.*;

public class XPropEditorCache
{
	HashMap editorPanes_;
	JTree tree_;
        	
	
	public XPropEditorCache(JTree tree)
	{
		tree_ = tree;
		editorPanes_ = new HashMap();
	}
	
	
	public XPropEditor get (String item)
	{
		// Only one item must be selected
		XPropEditor e = (XPropEditor) editorPanes_.get (item);
		if (e == null)
		{
			e = new XPropEditor(tree_);
			TreePath[] paths = tree_.getSelectionModel().getSelectionPaths();
			if (paths.length == 1)
			{
				DefaultMutableTreeNode n = (DefaultMutableTreeNode) paths[0].getLastPathComponent();
				XTreeNode xn = (XTreeNode) n.getUserObject();

				XTreeNode defaultParameters = xn.firstChild("DefaultParameters");
				if (defaultParameters.domNode != null)
				{
					try {
						ParameterTableModel m = new ParameterTableModel ( defaultParameters.domNode );
						e.setModel ( m );
					} catch (ParameterTableModelException ptme)
					{
						//e.setModel ( new ParameterTableModel () );
						JOptionPane.showMessageDialog ( null,
							ptme.getMessage(),
							"XML Configuration File Error",
							JOptionPane.ERROR_MESSAGE);
						return e;
					}
				} else {
					e.setModel ( new ParameterTableModel () );
				}
			}
			editorPanes_.put (item, e);
		}
		return e;
	}        
}
