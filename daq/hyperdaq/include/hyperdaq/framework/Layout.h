// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, A.Forrest								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _hyperdaq_framework_Layout_h_
#define _hyperdaq_framework_Layout_h_

#include "xgi/exception/Exception.h"
#include "xgi/Output.h"
#include "xgi/Input.h"

#include "xdata/Boolean.h"

#include "xgi/framework/AbstractLayout.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/exception/Exception.h"


namespace hyperdaq
{
	namespace framework
	{
		class UIManager;

		class Layout: public xgi::framework::AbstractLayout
		{

			public:

				Layout () ;

				void getHTMLHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;
				void getHTMLFooter (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;

				void getHTMLControlHeader (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;

				void getHTMLHeaderTop (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;

				// getters for the two different menus
				void getHTMLStandardMenu (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;
				void getHTMLControlMenu (xgi::framework::UIManager * manager,  xgi::Input * in, xgi::Output * out) ;

				void getHTMLHeaderBottom (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;

				void Default (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;

				void noCallbackFound (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out) ;

                void errorPage (xgi::framework::UIManager * manager, xgi::Input * in, xgi::Output * out, xcept::Exception& e) ;

				xdata::Boolean showSidebar_;
				std::string defaultIcon_;
				std::string xdaqStartTime_;

				// Control Panel overrides
				std::string currentAppIcon_;
				std::string currentURL_;

			protected:

				void resetCGICC() ;
		};
	}
}

#endif
