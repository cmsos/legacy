// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_fifo_PeerTransportFifo_h_
#define _pt_fifo_PeerTransportFifo_h_

#include "pt/fifo/PeerTransport.h"
#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

//! this is the XDAQ Peer Transport Appliction Wrapper
//
namespace pt
{
	namespace fifo
	{
		class PeerTransportFifo: public xdaq::Application
		{
			public:

			XDAQ_INSTANTIATOR();

			PeerTransportFifo(xdaq::ApplicationStub * s) ;
			~PeerTransportFifo();

			private:

			pt::fifo::PeerTransport* pt_;
			xgi::framework::UIManager manager_;
		};
	}
}
#endif
