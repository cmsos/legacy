#include "tcds/pm/PMControllerHelper.h"

#include "toolbox/string.h"
#include "xcept/Exception.h"

#include "tcds/exception/Exception.h"
#include "tcds/hwlayer/RegisterInfo.h"
#include "tcds/pm/TCADevicePMCommonBase.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
#include "tcds/utils/XDAQAppWithFSMForPMs.h"

tcds::pm::PMControllerHelper::PMControllerHelper(tcds::utils::XDAQAppWithFSMForPMs const& xdaqApp,
                                                 tcds::pm::TCADevicePMCommonBase const& device) :
  xdaqApp_(xdaqApp),
  device_(device)
{
}

tcds::pm::PMControllerHelper::~PMControllerHelper()
{
}

void
tcds::pm::PMControllerHelper::hwCfgInitialize() const
{
  makeSafe();

  // // Send an 'internal B-go Stop', just to the CPM, in order to block
  // // triggers and stop non-permanent cyclics.
  // device_.stop();

  // // Disable all triggers.
  // device_.disableTrigger();

  // // Software-pause the system.
  // device_.systemPause();

  // Switch off the ReTri but plug in the default settings. This
  // guarantees that if the configuration that we load does not
  // configure and enable the ReTri, it will be enabled with the
  // default settings afterwards.
  device_.setReTriDefaults();
  device_.disableReTri();
}

void
tcds::pm::PMControllerHelper::hwCfgFinalize() const
{
  // Configure the FED id in the hardware.
  uint32_t const fedId = xdaqApp_.getConfigurationInfoSpaceHandler().getUInt32("fedId");
  device_.setFEDId(fedId);

  // If the ReTri is not enabled by the configuration, configure it in
  // the default way and enable it.
  if (!device_.isReTriEnabled())
    {
      // WARN("ReTri is disabled after configuring. Enabling it with default values.");
      device_.setReTriDefaults();
      device_.enableReTri();
    }

  //----------

  // // Enable all triggers.
  // device_.enableTrigger();
}

// void
// tcds::pm::PMControllerHelper::coldResetAction(toolbox::Event::Reference event)
// {
// }

// void
// tcds::pm::PMControllerHelper::configureAction(toolbox::Event::Reference event)
// {
// }

void
tcds::pm::PMControllerHelper::enableAction(toolbox::Event::Reference event)
{
  // Configure the CMS run number in the hardware. (This goes into the
  // DAQ event record and into the BRILDAQ packet.)
  // NOTE: The run number is in principle 64-bits in firmware, but
  // only 32-bits in software. (We only receive 32 bits from
  // RunControl anyway.)
  // NOTE: The run number is received from RunControl in the Enable
  // transition.
  uint32_t const runNumber = xdaqApp_.getConfigurationInfoSpaceHandler().getUInt32("runNumber");
  device_.setRunNumber(runNumber);

  // Software-unpause the system.
  // NOTE: This is _always_ necessary, because the FSM allows the
  // transition from Paused to Halted, to Configured.
  device_.systemUnpause();

  // Enable all triggers.
  device_.enableTrigger();

  // Execute 'Start' sequence.
  device_.sendBgoTrain(tcds::definitions::SEQUENCE_START);
}

// void
// tcds::pm::PMControllerHelper::failAction(toolbox::Event::Reference event)
// {
// }

// void
// tcds::pm::PMControllerHelper::haltAction(toolbox::Event::Reference event)
// {
//   // Just in case (e.g., we missed the stopAction())...
//   if (xdaqApp_.getHw().isReadyForUse())
//     {
//       makeSafe();
//     }
// }

void
tcds::pm::PMControllerHelper::pauseAction(toolbox::Event::Reference event)
{
  // Execute 'Pause' sequence.
  device_.sendBgoTrain(tcds::definitions::SEQUENCE_PAUSE);

  // Software-pause the system.
  device_.systemPause();
}

void
tcds::pm::PMControllerHelper::resumeAction(toolbox::Event::Reference event)
{
  // Software-unpause the system.
  device_.systemUnpause();

  // Execute 'Resume' sequence.
  device_.sendBgoTrain(tcds::definitions::SEQUENCE_RESUME);
}

void
tcds::pm::PMControllerHelper::stopAction(toolbox::Event::Reference event)
{
  // Execute 'Stop' sequence.
  device_.sendBgoTrain(tcds::definitions::SEQUENCE_STOP);

  // Just in case...
  makeSafe();
}

void
tcds::pm::PMControllerHelper::ttcHardResetAction(toolbox::Event::Reference event)
{
  // Execute 'TTCHardReset' sequence.
  device_.sendBgoTrain(tcds::definitions::SEQUENCE_HARDRESET);
}

void
tcds::pm::PMControllerHelper::ttcResyncAction(toolbox::Event::Reference event)
{
  // Execute 'Resync' sequence.
  device_.sendBgoTrain(tcds::definitions::SEQUENCE_RESYNC);
}

void
tcds::pm::PMControllerHelper::zeroAction(toolbox::Event::Reference event)
{
  // // Send an 'internal B-go Stop', just to the PM, in order to block
  // // triggers and stop non-permanent cyclics.
  // device_.stop();

  // // Software-pause the system.
  // device_.systemPause();

  // Reset the DAQ interface.
  // NOTE: This is the TCDS firmware DAQ interface, not the DAQ link
  // firmware block itself. The latter we are _not_ supposed to ever
  // reset.
  device_.initDAQLink();

  // Initialise all cyclic generators in the iPM.
  device_.initCyclicGenerators();

  // Initialise all B-go sequences in the iPM.
  device_.initSequences();

  // Reset all counters.
  device_.resetCounters();

  // Reset the TTS state-change counters.
  // NOTE: This is a bit of a special action. It gets rid of all weird
  // TTS transitions that occur while subsystems configure.
  device_.resetTTSCounters();

  // Reset the orbit realignment monitoring.
  device_.resetOrbitMonitoring();

  // Reset the L1A bunch-crossing histograms.
  device_.resetL1AHistograms();

  // Reset the B-go histories.
  // NOTE: The L1A history is reset automatically by the OC0.
  device_.resetBgoHistory();
}

bool
tcds::pm::PMControllerHelper::isRegisterAllowed(tcds::hwlayer::RegisterInfo const& regInfo) const
{
  // The FED ids are not configurable via the register dumps. They are
  // configured by the controlling XDAQ application.
  bool res = (regInfo.name().find("fed_id") == std::string::npos);

  // The overall trigger_enable register is handled by the XDAQ
  // control application.
  res = res && !toolbox::endsWith(regInfo.name(), ".trigger_enable");

  // The internal BX-alignment registers are not supposed to be
  // touched by the user-config.
  res = res && !toolbox::endsWith(regInfo.name(), ".cyclic_trigger_adjust");
  res = res && !toolbox::endsWith(regInfo.name(), ".bunch_mask_adjust");
  res = res && !toolbox::endsWith(regInfo.name(), ".calib_trigger_adjust");
  res = res && !toolbox::endsWith(regInfo.name(), ".daq_record_adjust");

  // Enabling/disabling DAQ backpressure is done based on the FED-vector.
  res = res && (regInfo.name().find("daq_backpressure_enable") == std::string::npos);

  //----------

  return res;
}

void
tcds::pm::PMControllerHelper::makeSafe() const
{
  // This method puts the (x)PM in a 'safe' state for subsystems: all
  // triggers are blocked, and all non-permanent cyclic generators are
  // disabled.

  // Send an 'internal B-go Stop', just to the CPM, in order to block
  // triggers and stop non-permanent cyclics.
  device_.stop();

  // Disable all triggers.
  device_.disableTrigger();
}

std::string
tcds::pm::PMControllerHelper::buildLPMLabel(unsigned int const lpmNumber) const
{
  std::string res = "";
  tcds::utils::ConfigurationInfoSpaceHandler const& cfgInfoSpace =
    xdaqApp_.getConfigurationInfoSpaceHandler();
  std::string const lpmLabel =
    cfgInfoSpace.getString(toolbox::toString("partitionLabelLPM%d", lpmNumber));
  if (lpmLabel.empty())
    {
      res = toolbox::toString("LPM %d", lpmNumber);
    }
  else
    {
      res = toolbox::toString("LPM %d (%s)", lpmNumber, lpmLabel.c_str());
    }
  return res;
}
