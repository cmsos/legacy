package xdaq.xdesk.tools;

import java.util.*;
import java.lang.*;
import java.awt.datatransfer.*;

// Data put on the databus infobus
//
public class Data implements Transferable
{
	HashMap map_;

	public Data ()
	{
		map_ = new HashMap();
	}
	
	public void addDataFlavor (Object content, DataFlavor flavor)
	{
		map_.put (flavor, content);
	}


	public Object getTransferData (DataFlavor flavor)
	{
		Iterator i = map_.keySet().iterator();
		while (i.hasNext())
		{
			DataFlavor f = (DataFlavor) i.next();
			if (f.match(flavor)) return map_.get(f);
		}
		return null;
	}
	
	public DataFlavor[] getTransferDataFlavors()
	{
		return (DataFlavor[]) map_.keySet().toArray(new DataFlavor[] {});
	}
	
	public boolean isDataFlavorSupported(DataFlavor flavor)
	{
		Iterator i = map_.keySet().iterator();
		while (i.hasNext())
		{
			DataFlavor f = (DataFlavor) i.next();
			if (f.match(flavor)) return true;
		}
		return false;
	}
}
