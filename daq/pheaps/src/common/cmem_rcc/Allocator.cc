// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string>
#include <limits>

#include <iostream>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>


#include "pheaps/cmem_rcc/Allocator.h"
#include "pheaps/Buffer.h"
#include "toolbox/string.h"
#include "toolbox/mem/MemoryPartition.h"
#include "toolbox/mem/exception/FailedCreation.h"
#include "toolbox/mem/SmartBufPool.h"

#define CMEM_RCC_DEVICE "/dev/cmem_rcc0"

pheaps::cmem_rcc::Allocator::Allocator(size_t committedSize)
	
{
	size_t order;

	if ( committedSize > static_cast<size_t>(std::numeric_limits<ssize_t>::max()) )
	{
		std::string msg = toolbox::toString("Cannot allocate size of %d (max allowed size is %d)", committedSize,
			std::numeric_limits<ssize_t>::max());
		XCEPT_RAISE (toolbox::mem::exception::FailedCreation, msg);
	}
	
	committedSize_ = committedSize;
	
  	fd_ = open(CMEM_RCC_DEVICE, O_RDWR);

  	if (fd_ < 0) 
	{
		std::string msg = "Could not open ";
		msg += CMEM_RCC_DEVICE;
		XCEPT_RAISE (toolbox::mem::exception::FailedCreation, msg);
  	}

	// retrieve max segment size 
	size_t system_pagesize =  (size_t)getpagesize();         


	 //Calculate the "order"
  	if (committedSize_ <= system_pagesize)            order = 0;
  	else if (committedSize_ <= 2 * system_pagesize)   order = 1;
  	else if (committedSize_ <= 4 * system_pagesize)   order = 2;
  	else if (committedSize_ <= 8 * system_pagesize)   order = 3;
  	else if (committedSize_ <= 16 * system_pagesize)  order = 4;
  	else if (committedSize_ <= 32 * system_pagesize)  order = 5;
  	else if (committedSize_ <= 64 * system_pagesize)  order = 6;   // 256 KBytes
  	else if (committedSize_ <= 128 * system_pagesize) order = 7;   // 512 KBytes                
  	else if (committedSize_ <= 256 * system_pagesize) order = 8;   // 1 MBytes      
  	else if (committedSize_ <= 512 * system_pagesize) order = 9;   // 2 MBytes 
        else order = 10; // 4 MBytes


	// NEW ALGORITHM, start from biggest order and decrement till minimal order defined
	size_t minOrder = 3; // it will reject allocate under this size
	size_t currentAllocated = 0;
	size_t remainingToAllocateSize = committedSize_;
	// allocate committedsize starting from the biggest order
	size_t currentOrder = order;
	for ( currentOrder = order; currentOrder >= minOrder; currentOrder-- )
	{
		// estimate number of chunks for this order required for remainingToAllocate 			
		size_t currentChunkSize = system_pagesize * BUF_LOG2_TO_BUF_SIZE(currentOrder); // example order of 2^10
		size_t requiredChunks = (size_t)(remainingToAllocateSize / currentChunkSize);
		if ( (size_t)(remainingToAllocateSize % currentChunkSize) != 0 )
                                requiredChunks = requiredChunks + 1;
		if ( requiredChunks == 0 )
        	{
                	::close(fd_);
                	XCEPT_RAISE (toolbox::mem::exception::FailedCreation, "bad number of chunks");
        	}


		// attempt to get all chunks required for this order
		size_t allocatedChunk = 0;
		while ( allocatedChunk  < requiredChunks)
        	{
			
			cmem_rcc_t desc;

       		       	strcpy(desc.name, "");
			desc.order = currentOrder;
                	desc.type = TYPE_GFP;
                	int status;
			// actual alloc here 
                 	if ((status = ioctl(fd_, CMEM_RCC_GET, &desc) < 0))
		        {
				//std::cout << "failed to allocate in order " << currentOrder << std::endl;
				break; // try next lower order
                  	}

			errno = 0;
                	desc.uaddr = (u_long)mmap(0, desc.size, PROT_READ|PROT_WRITE, MAP_SHARED, fd_, (long)desc.paddr);
	                if ((long)desc.uaddr == 0 || (long)desc.uaddr == -1)                                    
                	{
                        	std::string message =  strerror(errno);
                        	::close(fd_);
                        	XCEPT_RAISE(toolbox::mem::exception::FailedCreation,message);
                	}

                	if ((status = ioctl(fd_, CMEM_RCC_SETUADDR, &desc) < 0 ))       
                	{
                        	::close(fd_);
                        	XCEPT_RAISE (toolbox::mem::exception::FailedCreation,"failed set uaddr to driver");
                	} 

                	chunks_.push_back(desc);

        		try
        		{       
                		//baseAddress_ = address;
                		memPartition_.addToPool((void*)desc.paddr, // physycal
                                        		(void*)desc.kaddr, // hernel
                                        		(void*)desc.uaddr,
                                        		currentChunkSize);
        		} 
        		catch (toolbox::mem::exception::Corruption& c)
        		{
                		::close(fd_);
                		XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, "Cannot add physical memory to pool, pool corrupted", c);
        		}
        		catch (toolbox::mem::exception::InvalidAddress& ia)
        		{
                		::close(fd_);
                		XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, "Cannot add physical memory to pool, base address invalid", ia);
        		}
        		catch (toolbox::mem::exception::MemoryOverflow& mo)
        		{
                		::close(fd_);
                		XCEPT_RETHROW (toolbox::mem::exception::FailedCreation, "Cannot add physical memory to pool, size too big", mo);
        		}
        		catch (std::bad_alloc& e)
        		{
                		::close(fd_);
                		XCEPT_RAISE (toolbox::mem::exception::FailedCreation, e.what());
        		}

			currentAllocated += currentChunkSize;	
			if ( committedSize_ > currentAllocated )
				remainingToAllocateSize = committedSize_ - currentAllocated;
			else
				remainingToAllocateSize = 0;
			//std::cout << "Remaining memory to allocate:" << remainingToAllocateSize << " current allocated: " << currentAllocated << std::endl;

			allocatedChunk++;
		}

		std::cout << "obtained " << allocatedChunk << " out of " << requiredChunks << " of size: " << currentChunkSize << "remaining: " << remainingToAllocateSize << std::endl;

		if ( allocatedChunk  == requiredChunks )
			break; // got all memory required , no need to move to lower order
				
	}

	if ( currentOrder < minOrder )
	{
		// failed to allocate , no sufficient memory available
                ::close(fd_);
                XCEPT_RAISE (toolbox::mem::exception::FailedCreation, "insufficient memory");
	}
	
}


pheaps::cmem_rcc::Allocator::~Allocator()
{	

	int ret;

	for (std::list<cmem_rcc_t>::iterator i = chunks_.begin(); i != chunks_.end(); i++ )
	{
		ret = ioctl(fd_, CMEM_RCC_GETPARAMS, (*i));
  		if (ret)
  		{
			#warning "IOCTL of physical memory failed - should never happen	" 
			std::cerr << "IOCTL of physical memory failed" << std::endl;
		}
		ret = munmap((void *)(*i).uaddr, (*i).size); 
		if (ret)
		{
			#warning "Unmap of physical memory failed - should never happen "
			std::cerr << "Unmap of physical memory failed" << std::endl;
		}
	}

	::close(fd_);
}

toolbox::mem::Buffer * pheaps::cmem_rcc::Allocator::alloc(size_t size, toolbox::mem::Pool * pool) 
	
{
	if ( size > static_cast<size_t>(std::numeric_limits<ssize_t>::max()) )
	{
		std::string msg = toolbox::toString("Cannot allocate size of %d (max allowed size is %d)", size,
			std::numeric_limits<ssize_t>::max());
		XCEPT_RAISE (toolbox::mem::exception::FailedCreation, msg);
	}
	#warning "Check that size is not bigger than a chunks, that would lead to failure since memory is not contiguous"
	try
	{
		char* b = (char*) memPartition_.alloc(size);
		#warning "TBD: physical/kernel mapping"
		pheaps::Buffer* bufPtr = new pheaps::Buffer(pool, size, b, memPartition_.virtualToPhysical(b), memPartition_.virtualToKernel(b));			
		return bufPtr;
	}
	catch (toolbox::mem::exception::FailedAllocation& fa)
	{
		BufferSizeType curalloc, totfree, maxfree;
		BufferSizeType nget, nrel;
		memPartition_.stats (&curalloc, &totfree, &maxfree, &nget, &nrel);
		std::string msg = toolbox::toString ("Out of memory in CommittedHeapAllocator while allocating %d bytes. Bytes used %d, bytes free %d, max free %d, number allocated %d, number released %d", size, curalloc, totfree, maxfree, nget, nrel);
		XCEPT_RETHROW (toolbox::mem::exception::FailedAllocation, msg, fa);
	}
}

	
void pheaps::cmem_rcc::Allocator::free (toolbox::mem::Buffer * buffer) 
	
{
	try
	{
		memPartition_.free( (char*) buffer->getAddress());
		delete buffer;
	}
	catch (toolbox::mem::exception::InvalidAddress& ia)
	{
		// just invalid pointer, may still be o.k. to retry, therefore don't delete buffer
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, toolbox::toString("Cannot free buffer %x, invalid pointer",buffer), ia);
	}
	catch (toolbox::mem::exception::Corruption& c)
	{
		XCEPT_RETHROW (toolbox::mem::exception::FailedDispose, toolbox::toString("Cannot free buffer %x, memory corruption",buffer), c);
		delete buffer;	
	}
}

std::string pheaps::cmem_rcc::Allocator::type () 
{
	return "pheaps::cmem_rcc::Allocator";
}


bool pheaps::cmem_rcc::Allocator::isCommittedSizeSupported()
{
	return true;
}
	
size_t pheaps::cmem_rcc::Allocator::getCommittedSize()
{
	return committedSize_;
}

size_t  pheaps::cmem_rcc::Allocator::getUsed()
{
	return memPartition_.getUsed();
}
