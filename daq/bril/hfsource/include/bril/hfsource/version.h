// $Id$

/*************************************************************************
 * XDAQ Application Template                           *
 * Copyright (C) 2000-2009, CERN.                      *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, A. Petrucci                           *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _bril_hfsource_version_h_
#define _bril_hfsource_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define BRILHFSOURCE_VERSION_MAJOR 1
#define BRILHFSOURCE_VERSION_MINOR 5
#define BRILHFSOURCE_VERSION_PATCH 1
// If any previous versions available E.g. #define ESOURCE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define BRILHFSOURCE_PREVIOUS_VERSIONS

//
// Template macros
//
#define BRILHFSOURCE_VERSION_CODE PACKAGE_VERSION_CODE(BRILHFSOURCE_VERSION_MAJOR,BRILHFSOURCE_VERSION_MINOR,BRILHFSOURCE_VERSION_PATCH)
#ifndef BRILHFSOURCE_PREVIOUS_VERSIONS
#define BRILHFSOURCE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(BRILHFSOURCE_VERSION_MAJOR,BRILHFSOURCE_VERSION_MINOR,BRILHFSOURCE_VERSION_PATCH)
#else
#define BRILHFSOURCE_FULL_VERSION_LIST  BRILHFSOURCE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(BRILHFSOURCE_VERSION_MAJOR,BRILHFSOURCE_VERSION_MINOR,BRILHFSOURCE_VERSION_PATCH)
#endif

namespace brilhfsource
{
  const std::string package = "brilhfsource";
  const std::string versions = BRILHFSOURCE_FULL_VERSION_LIST;
  const std::string summary = "XDAQ brilDAQ hfsource";
  const std::string description = "bril hf readout";
  const std::string authors = "C.Palmer";
  const std::string link = "http://xdaqwiki.cern.ch";
  config::PackageInfo getPackageInfo ();
  void checkPackageDependencies () throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif
