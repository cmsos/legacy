<?php
require '../utils/vendor/autoload.php';
include_once ('config.php');

$classcounters = array ();
$keycounter = 0;
$heartbeaturl = $_GET ['heartbeaturl'];

$response = Network::httpget ( $heartbeaturl . "/" . "urn:xdaq-application:service=xmasheartbeat/retrieveHeartbeatTable?fmt=json" );
$json = json_decode ( $response->getBody (), true );
foreach ( $json ['table'] ['rows'] as $row ) {
	
	$classname = $row ['class'];
	if (! isset ( $classcounters [$classname] )) {
		$classcounters [$classname] = array (
				'title' => $classname,
				'key' => "" . $keycounter,
				'id' => $keycounter,
				//'icon' => $row ['context'] . '/' . $row ['icon'], // this icon is directly used by fancy tree
				'usericon' =>  $row ['context'] . '/' . $row ['icon'], // this icon is rendered by us
				'context' => $row ['context'],
				'tags' => array (
						0 
				),
				'services' => array () 
		);
		$keycounter = $keycounter + 1;
	}
	$context = $row ['context'];
	$service = $row ['service'];
	$group = $row ['group'];
	$age = $row ['age'];
	$classcounters [$classname] ['tags'] [0] = $classcounters [$classname] ['tags'] [0] + 1;
	array_push ( $classcounters [$classname] ['services'], array (
			'context' => $context,
			'service' => $service,
			'group' => $group,
			'age' => $age
	) );
}
echo json_encode ( array_values ( $classcounters ) );

?>
