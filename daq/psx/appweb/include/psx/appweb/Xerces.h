// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _psx_appweb_Xerces_h_
#define _psx_appweb_Xerces_h_

// Singleton to initialize XML platform  only once for both SMI and PVVS modules
 
namespace psx 
{
    namespace appweb 
    {    
    
		class Xerces
		{
      			public:
			
			
			Xerces();
			

			static void init();
			
			private:
			
			static Xerces * instance_;
		};
	}	
}
#endif 

