#ifndef _bril_dipprocessor_FillLumi_h_
#define _bril_dipprocessor_FillLumi_h_

#include <map>
#include <fstream>
#include "xdata/Float.h"
#include "xdata/String.h"
#include "bril/dipprocessor/DipAnalyzer.h"

namespace bril{
  namespace dip{

    class FillLumi: public DipAnalyzer{
    public:
      FillLumi(const std::string& name, unsigned int dipfrequency, const std::string& dipServiceName, xdaq::Application* owner);
      // destructor
      ~FillLumi();
      // infospace event callback
      virtual void actionPerformed(xdata::Event& e);
      // toolbox event callback
      virtual void actionPerformed(toolbox::Event& e);
      // concrete implementation
      virtual void handleMessage(DipSubscription* dipsub, DipData& message);
      virtual void connected(DipSubscription* dipsub);
      virtual void disconnected(DipSubscription* dipsub, char *reason);
    private:
      //Variables associated with the FillLumi task

      std::string m_dipRoot;
      std::string m_beamMode;
      std::string m_machineMode;
      std::string m_fillNumber;
      int m_cmsRunNumber;
      float m_deliveredLumi;
      float m_recordedLumi;
      float m_deadfrac;
      std::string m_fillLumiRecoveryFilePath;

    private:
      void publish_to_dip();
      bool recover_fill_lumi(std::string const &);
      bool update_recovery_file();
      // nodefaut constructor
      FillLumi();
      // nocopy protection
      FillLumi(const FillLumi&);
      FillLumi& operator=(const FillLumi&);
    };
  }
}
#endif
