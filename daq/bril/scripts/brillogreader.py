#!/usr/bin/python -u
"""Parse log4j XML files.

Usage:
python brillogreader.py [options] [filename1 filename2
... filenameN] [0 1 2 ... N]

Use filenames OR numbers. If numbers are provided theese files will be parsed:
  * number '0'   - /var/log/rcms/<group>/Logs_<group>.xml
  * other number - /var/log/rcms/<group>/Logs_<group>.xml.<number>.gz
If neither filenames nor numbers are provided, default file will be parsed:
    /var/log/rcms/lumipro/Logs_lumipro.xml

Options:
-h,--help                      help
-v,--verbose                   verbose
-f                             equivalent to 'tail -f'
-n,--new,--only-new            show only newly added records (no
                               output without option -f)
-e,--errors                    show WARNING, ERROR and FATAL
-g <group>,--group=<group>     parse log files corresponding to cms
                               group (default is lumipro if
                               filename omitted)
--host=<host>                  filter by given hostname
-p <port>,--port=<port>        filter by given port
--app=<application>            filter by given application name
--instance=<instance>          filter by given instance number
"""

# TODO: clarify usage maybe edit filename/number parsing
# TODO: add -l option for selecting log levels
# TODO: allow filtering by multiple instances

import sys
import os
import getopt
import re
import gzip
import time
import tempfile


verbose = False


def v_print(to_print):
    global verbose
    if verbose:
        print to_print


def color(level):
    if level == "ERROR":
        return "\033[30;41m"
    elif level == "WARN":
        return "\033[30;43m"
    elif level == "INFO":
        return "\033[30;42m"
    elif level == "DEBUG":
        return "\033[30;42m"
    elif level == "FATAL":
        return "\033[30;45;1m"
    elif level == "DEFAULT":
        return "\033[30;47;0m"
    else:
        return ""


class LogReader(object):
    def __init__(self, filename, tail=False, host=None, port=None,
                 application=None, instance=None,
                 only_errors=False, only_new=False, interval=0.6):
        """Create a LogReader object
        filename: name of the file in lo4j XML format
        tail_f:   if True then the iterator never stops reading
                  the file (tail like)
        """
        self.filename = filename
        self.tail = tail
        self.host = host
        self.port = port
        self.application = application
        if application is not None:
            self.application = application.lower()
        self.instance = instance
        self.only_errors = only_errors
        self.only_new = only_new
        self.interval = interval
        if not os.path.isfile(filename):
            err = Exception("filename '%s' does not exist or is not"
                            "accessible" % filename)
            raise err

    def __step_file(self):
        tail_f = self.tail
        filename = self.filename
        if filename.rfind('.gz') == len(filename)-3:
            v_print("Openning file '%s' in gzip mode" % filename)
            tail_f = False
            z = gzip.open(filename)
            f = tempfile.TemporaryFile()
            v_print("Transferring content to temporal file...")
            f.write(z.read())
            f.seek(0)
        else:
            v_print("Opening file '%s' in normal mode" % filename)
            f = open(filename, 'r')

        where = 0
        if (self.only_new):
            # go to end of file
            f.seek(0, 2)
        line_list = []
        while True:
            for line in f:
                line_list.append(line.rstrip())
                if line.find("</log4j:event>") != -1:
                    line_list = map(lambda u: u.replace("\r", " "), line_list)
                    l = "\n".join(line_list)
                    yield l
                    line_list = []
            if tail_f:
                v_print("Waiting for more data...")
                where = f.tell()
                v_print("File position: %s" % where)
                try:
                    time.sleep(self.interval)
                except KeyboardInterrupt:
                    sys.exit(1)
                if os.stat(filename)[6] < where:
                    v_print("logreader.py: File truncated by 3rd party...")
                    f.close()
                    f = open(filename)
                else:
                    f.seek(where)
            else:
                raise StopIteration

    def logs(self):
        """Returns an iterator of log strings"""
        v_print("Reading file content")

        r_total = re.compile(
            "(\<log4j\:event logger\=\")(.*?)(\" "
            "timestamp\=\")(.*?)(\")(.*?)(level\=\")(.*?)(\")")
        r_msg = re.compile(
            "(\<log4j\:message\>\<\!\[CDATA\[)(.*?)(\]\]\>"
            "\<\/log4j\:message\>)", re.DOTALL)
        r_throwable = re.compile(
            "(\<log4j\:throwable\>\<\!\[CDATA\[)(.*?)(\]\]\>"
            "\<\/log4j\:throwable\>)", re.DOTALL)
        r_logger_info = re.compile("(.*?)\.p\:(\d+)\.(.*?)\.instance\((\d+)\)")
        r_logger_info_fallback = re.compile("(.*?)\.p\:(\d+)\.(.*?)")
        for l in self.__step_file():
            t = r_total.search(l)
            m = r_msg.search(l)
            try:
                level = t.groups()[7]
                if (self.only_errors
                    and level not in ("WARN", "ERROR", "FATAL")):
                    # then:
                    continue
                logger = t.groups()[1]
                logger_match = r_logger_info.search(logger)
                if logger_match is None:
                    logger_match = r_logger_info_fallback.search(logger)
                host = logger_match.groups()[0]
                if (self.host is not None and self.host not in host):
                    continue
                port = logger_match.groups()[1]
                if (self.port is not None and self.port != port):
                    continue
                application = logger_match.groups()[2]
                if (self.application is not None and
                    self.application not in application.lower()):
                    # then:
                    continue
                if (self.instance is not None):
                    instance = logger_match.groups()[3]
                    if (self.instance != instance):
                        continue
                ts = time.ctime(float(t.groups()[3])/1000)
                msg = m.groups()[1]
            except (AttributeError, IndexError):
                v_print(l)
                continue

            p = "%s %s%s%s %s <> - %s" % (
                ts, color(level), level, color("DEFAULT"), logger, msg
            )
            # this part below takes 1/6 of the time
            w = r_throwable.search(l)

            try:
                throwable = w.groups()[1].rstrip().lstrip()
                if throwable:
                    p = "%s %s%s%s %s <> - %s\n%sERROR TRACE%s: %s" % (
                        ts, color(level), level, color("DEFAULT"), logger, msg,
                        color("ERROR"), color("DEFAULT"), throwable
                    )
            except (AttributeError, IndexError):
                pass

            yield p.replace("\n", " ")

        raise StopIteration


def test(filename):
    for l in LogReader(filename).logs():
        pass


def int_args(args):
    try:
        return reduce(
            lambda x, y: x and y, map(
                lambda x: isinstance(int(x), int), args))
    except:
        return False


def extract_filenames(group, args):
    if int_args(args):
        filenames = []
        for n in args:
            if n == "0":
                filenames.append("/var/log/rcms/%s/Logs_%s.xml"
                                 % (group, group))
            else:
                filenames.append("/var/log/rcms/%s/Logs_%s.xml.%s.gz"
                                 % (group, group, n))
    elif len(args) != 0:
        print ("Can not guess filenames (arguments are not int), "
               "using arguments as filenames.")
        filenames = args
    else:
        filenames = []
        filenames.append("/var/log/rcms/%s/Logs_%s.xml" % (group, group))

    for fn in filenames:
        if not os.path.isfile(fn):
            print("ERROR: Filename '%s' does not exist. Bad argument" % fn)
            sys.exit(1)

    return filenames


def main():
    if len(sys.argv) < 2:
        print "ERROR: Missing argument"
        print __doc__
        sys.exit(1)

    try:
        opts, args = getopt.getopt(
            sys.argv[1:],
            "hvfneg:p:",
            ["help", "verbose", "new", "only-new", "errors",
             "group=", "host=", "port=", "app=", "instance="])
    except getopt.GetoptError, err:
        print "ERROR:", str(err)
        print __doc__
        sys.exit(1)

    tail_f = False
    only_errors = False
    only_new = False
    host = None
    port = None
    app_name = None
    instance = None
    group = "lumipro"
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
        elif o in ("-v", "--verbose"):
            global verbose
            verbose = True
        elif o in ("-f") and (len(args) == 1 or len(args) == 0):
            tail_f = True
        elif o in ("-e", "--errors"):
            only_errors = True
        elif o in ("-g", "--group"):
            group = a
        elif o in ("--host"):
            host = a
        elif o in ("-p", "--port"):
            port = a
        elif o in ("--app"):
            app_name = a
        elif o in ("--instance"):
            instance = a
        elif o in ("-n", "--new", "--only-new"):
            only_new = True

    filenames = extract_filenames(group, args)
    # sort files by date in ascendant order (older first)

    def cmp_stat(f1, f2):
        return cmp(os.path.getmtime(f1), os.path.getmtime(f2))

    filenames.sort(cmp_stat)
    print filenames

    for filename in filenames:
        reader = LogReader(
            filename, tail_f, host, port, app_name,
            instance, only_errors, only_new)
        for l in reader.logs():
            print l


if __name__ == '__main__':
    main()
