package xdaq.tools.cluster;

import java.io.*;
import java.util.*;

class JobControllerException extends Exception
{
	JobControllerException () { super(); }
	JobControllerException ( String s ) { super(s); }
}

public interface JobController
{
	public void exec (	String host, 
				String architecture, 
				Vector commands, 
				String username, 
				String password) 
				throws JobControllerException;
			
	public void destroy () throws JobControllerException;
	
	public InputStream getInputStream();
	public OutputStream getOutputStream();
}
