#include "xcept/tools.h"
#include "xgi/framework/Method.h"
#include "toolbox/net/UUID.h"
#include "xdata/InfoSpaceFactory.h"
#include "b2in/nub/Method.h"
#include "dip/DipData.h"
#include "dip/Dip.h"

#include "bril/dipprocessor/LumiLeveling.h"
#include "bril/dipprocessor/DipErrorHandlers.h"
#include "bril/dipprocessor/exception/Exception.h"
#include "interface/bril/LUMITopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"

#include <cmath>
#include <cstdio>

XDAQ_INSTANTIATOR_IMPL (bril::dip::LumiLeveling)
namespace bril{
namespace dip{


LumiLeveling::LumiLeveling(xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
    xgi::framework::deferredbind(this,this,&bril::dip::LumiLeveling::Default, "Default");
    b2in::nub::bind(this, &bril::dip::LumiLeveling::onMessage);
    m_bus.fromString("brildata");//default
    m_dipRoot.fromString("dip/CMSTEST/");//default
    m_mode="UNKNOWN";
    m_lumi = -1;
    m_last_lumi_time_sec = 0;
    m_source="UNKNOWN";
    m_isfirsttcds = true;
    m_run=0;
    m_lastrun=0;
    m_enable=false;
    m_levelStepSigma=0.05;
    m_targetLumi=8000;


    std::stringstream ss;
    ss<<"LUMILEVELING Constructor:  ";
    //std::cout<<ss<<std::endl;
    LOG4CPLUS_DEBUG(getApplicationLogger(),ss.str());
    ss.str("");ss.clear();

    getApplicationInfoSpace()->fireItemAvailable("bus",&m_bus);
    getApplicationInfoSpace()->fireItemAvailable("dipRoot",&m_dipRoot);
    getApplicationInfoSpace()->fireItemAvailable("targetLumi",&M_targetLumi);
    getApplicationInfoSpace()->fireItemAvailable("lumiStep",&M_levelStepSigma);
    getApplicationInfoSpace()->fireItemAvailable("enableLeveling",&M_enable);
    getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

    m_AbortLeveling=false;
    m_StatusFlag=false;





    m_wl_publishing_todip = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_dippublish","waiting");
    m_wl_publishing_todipls = toolbox::task::getWorkLoopFactory()->getWorkLoop(getApplicationDescriptor()->getURN()+"_dippublishLS","waiting");
    m_as_publishing_todip = toolbox::task::bind(this,&bril::dip::LumiLeveling::publishing_todip,"dippublish");
    m_as_publishing_todipls = toolbox::task::bind(this,&bril::dip::LumiLeveling::publishing_todipls,"dippublishLS");

}

LumiLeveling::~LumiLeveling(){}
void LumiLeveling::Default(xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){}

void LumiLeveling::actionPerformed(xdata::Event& e){
    if( e.type()== "urn:xdaq-event:setDefaultValues" ){
        try{
            this->getEventingBus(m_bus.value_).subscribe(interface::bril::beamT::topicname());
            this->getEventingBus(m_bus.value_).subscribe(interface::bril::bestlumiT::topicname());
            this->getEventingBus(m_bus.value_).subscribe(interface::bril::tcdsT::topicname());
        }catch(eventing::api::exception::Exception& e){
            std::string msg("Failed to subscribe to eventing bus "+m_bus.value_);
            LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
            XCEPT_RETHROW(bril::dip::exception::Exception,msg,e);
        }
        toolbox::net::UUID uuid;
        std::string processuuid = uuid.toString();
        m_dip = Dip::create(( std::string("brilLumiTopicPublisher_")+processuuid).c_str() );
        m_puberrorhandler = new PubErrorHandler;
        m_dippubs.insert(std::make_pair(m_dipRoot.value_+"LHC/LumiLeveling",(DipPublication*)0 ));  //Luminosity
        createDipPublication();
        m_wl_publishing_todip->activate();
        m_wl_publishing_todipls->activate();
        //std::cout<<"*******DEFAULTS****** :"<<M_enable<<"  "<<M_targetLumi<<"  "<<M_levelStepSigma<<std::endl;
        m_enable=M_enable;
        m_targetLumi=M_targetLumi;
        m_levelStepSigma=M_levelStepSigma;
    }
}

void LumiLeveling::actionPerformed(toolbox::Event& e){
    delete m_dip;
    if(m_puberrorhandler){
        delete m_puberrorhandler; m_puberrorhandler = 0;
    }
}

void LumiLeveling::createDipPublication(){
    LOG4CPLUS_DEBUG(getApplicationLogger(),"createDipPublication: ");
    for(std::map<std::string,DipPublication*>::iterator it=m_dippubs.begin();it!=m_dippubs.end();++it){
        DipPublication* p=m_dip->createDipPublication(it->first.c_str(),m_puberrorhandler);
        if(!p){
            LOG4CPLUS_FATAL(getApplicationLogger(),"Failed to create dip topic "+it->first);
            m_dippubs.erase(it); // remove from registry
        }else{
            it->second=p;
        }
    }
}




void LumiLeveling::publishToDip(){
    DipTimestamp dip_timestamp_now;
    long now = dip_timestamp_now.getAsMillis()/1000;
    std::stringstream ss;
    ss<<"Entering publishToDip.  Lumi value: "<< m_lumi << ", time: " << m_last_lumi_time_sec;
    bool lumiStatus = true;
    if(m_lumi < 10){
        ss << ". Lumi value bellow 1000 => StatusFlag=false";
        lumiStatus=false;
    }
    LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
    if (now - m_last_lumi_time_sec > 60) {
        ss.str(""); ss.clear();
        ss<<"Lumi value is too old => StatusFlag=false. Data time: " << m_last_lumi_time_sec << ", now: " << now;
        LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
        lumiStatus=false;
    }
    DipData* dipdata = m_dip->createDipData();
    std::string diptopicname(m_dipRoot.value_+"LHC/LumiLeveling");
    dipdata -> insert(m_enable,"Enable");
    dipdata -> insert(lumiStatus,"StatusFlag");
    dipdata -> insert(m_lumi,"CurrentLumi");
    dipdata -> insert(m_targetLumi,"TargetLumi");
    dipdata -> insert(m_levelStepSigma,"LevelStepSigma");

    m_lumi=-1;

    DipTimestamp t;
    m_dippubs[diptopicname]->send(*dipdata,t);
    delete dipdata;

    //reset variables already sent
    m_applock.take();

    m_source ="UNKNOWN";
    m_lumi = -1;
    m_applock.give();
}


void LumiLeveling::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception){
    std::string action = plist.getProperty("urn:b2in-eventing:action");
    if (action == "notify"){
        std::string topic = plist.getProperty("urn:b2in-eventing:topic");
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
        std::string data_version  = plist.getProperty("DATA_VERSION");
        if(data_version.empty() || data_version!=interface::bril::DATA_VERSION){
            std::string msg("Received brildaq message has no or wrong version, do not process.");
            LOG4CPLUS_ERROR(getApplicationLogger(),msg);
            ref->release(); ref=0;
            return;
        }

        if(ref!=0){
            interface::bril::DatumHead * thead = (interface::bril::DatumHead*)(ref->getDataLocation());
            std::string detpayload;
            if (topic == interface::bril::tcdsT::topicname()){
                //Note: the time header of detector messages can NOT be trusted therefore should NOT be written into the common time infornation because for example HF nbnum, lsnum are guaranteed to be different from others. We use tcds's time as the common.
                m_applock.take();
                m_run = thead->runnum;
                m_ls = thead->lsnum;
                m_nb = thead->nbnum;
                m_ts = thead->timestampsec;
                m_tms = thead->timestampmsec;
                m_applock.give();
                if( m_run!=m_lastrun ){
                    m_isfirsttcds = true;
                    m_lastrun = m_run;
                } else {
                    m_isfirsttcds = false;
                }
                if( !m_isfirsttcds ){
                    //On start of process, incomplete set of messages are useless, do not process
                    m_wl_publishing_todip->submit(m_as_publishing_todip);
                }
            } else if(topic == interface::bril::bestlumiT::topicname()){
                float _delivered = 0;
                char _provider[50];
                interface::bril::CompoundDataStreamer tc(interface::bril::bestlumiT::payloaddict());
                tc.extract_field(&_delivered, "delivered",  thead->payloadanchor);
                if( isnan(_delivered) || isinf(_delivered) ){
                    LOG4CPLUS_ERROR(getApplicationLogger(),"onMessage: received delivered NaN or Inf from bestlumi");
                    _delivered = 0;
                }
                if( _delivered<1E-8 ){
                    _delivered = 0;
                }

                m_applock.take();

                m_lumi = _delivered;
                m_last_lumi_time_sec = thead->timestampsec;
                tc.extract_field(_provider, "provider",  thead->payloadanchor);
                m_source=std::string(_provider);
                m_applock.give();
                //publishToDip();


            } else if(topic == interface::bril::beamT::topicname()){
                char machinemode[50];
                interface::bril::CompoundDataStreamer tc(interface::bril::beamT::payloaddict());
                tc.extract_field(machinemode, "status",  thead->payloadanchor);
                m_applock.take();
                m_mode=std::string(machinemode);


                m_applock.give();
            }else if(topic == interface::bril::tcdsT::topicname()){
                interface::bril::CompoundDataStreamer tc(interface::bril::tcdsT::payloaddict());
                //submit workloops
                std::stringstream ss;
            }
        }
    }
    if(ref!=0){
        ref->release();
        ref=0;
    }
}

bool LumiLeveling::publishing_todip(toolbox::task::WorkLoop* wl){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "publishing_todip");
    usleep(400000);
    publishToDip();
    return false;
}

bool LumiLeveling::publishing_todipls(toolbox::task::WorkLoop* wl){
    LOG4CPLUS_DEBUG(getApplicationLogger(), "publishing_todipls");
    //publishLumiSection();
    return false;
}

}
}
