#ifndef _interface_bril_version_h_
#define _interface_bril_version_h_
#include "interface/bril/CommonDataFormat.h"
#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!
// version definition moved to DataFormat.h to synchronize rpm and dataversion numbers
#define INTERFACEBRIL_VERSION_MAJOR 1
#define INTERFACEBRIL_VERSION_MINOR 10
#define INTERFACEBRIL_VERSION_PATCH 0

// If any previous versions available E.g. #define INTERFACEBRIL_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef INTERFACEBRIL_PREVIOUS_VERSIONS
//
// Template macros
//
#define INTERFACEBRIL_VERSION_CODE PACKAGE_VERSION_CODE(INTERFACEBRIL_VERSION_MAJOR,INTERFACEBRIL_VERSION_MINOR,INTERFACEBRIL_VERSION_PATCH)
#ifndef INTERFACEBRIL_PREVIOUS_VERSIONS
#define INTERFACEBRIL_FULL_VERSION_LIST PACKAGE_VERSION_STRING(INTERFACEBRIL_VERSION_MAJOR,INTERFACEBRIL_VERSION_MINOR,INTERFACEBRIL_VERSION_PATCH)
#else
#define INTERFACEBRIL_FULL_VERSION_LIST INTERFACEBRIL_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(INTERFACEBRIL_VERSION_MAJOR,INTERFACEBRIL_VERSION_MINOR,INTERFACEBRIL_VERSION_PATCH)
#endif
namespace interfacebril
{
  const std::string package  =  "interfacebril";
  const std::string versions =  INTERFACEBRIL_FULL_VERSION_LIST;
  const std::string summary = "Header files shared by bril eventing publisher/subscriber and other common data headers";
  const std::string description = "Header files are required for bril eventing publisher/subscriber";
  const std::string authors = "Zhen Xie";
  const std::string link = "https://twiki.cern.ch/twiki/bin/viewauth/CMS/LumiCalc";
  config::PackageInfo getPackageInfo();
  void checkPackageDependencies() throw (config::PackageInfo::VersionException);
  std::set<std::string, std::less<std::string> > getPackageDependencies();
}
#endif
