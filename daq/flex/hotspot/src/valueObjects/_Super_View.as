/**
 * This is a generated class and is not intended for modification.  To customize behavior
 * of this value object you may modify the generated sub-class of this class - View.as.
 */

package valueObjects
{
import com.adobe.fiber.services.IFiberManagingService;
import com.adobe.fiber.valueobjects.IValueObject;
import flash.events.EventDispatcher;
import mx.collections.ArrayCollection;
import mx.events.PropertyChangeEvent;
import valueObjects.View;

import flash.net.registerClassAlias;
import flash.net.getClassByAlias;
import com.adobe.fiber.core.model_internal;
import com.adobe.fiber.valueobjects.IPropertyIterator;
import com.adobe.fiber.valueobjects.AvailablePropertyIterator;

use namespace model_internal;

[ExcludeClass]
public class _Super_View extends flash.events.EventDispatcher implements com.adobe.fiber.valueobjects.IValueObject
{
    model_internal static function initRemoteClassAliasSingle(cz:Class) : void
    {
    }

    model_internal static function initRemoteClassAliasAllRelated() : void
    {
        valueObjects.View.initRemoteClassAliasSingleChild();
    }

    model_internal var _dminternal_model : _ViewEntityMetadata;
    model_internal var _changedObjects:mx.collections.ArrayCollection = new ArrayCollection();

    public function getChangedObjects() : Array
    {
        _changedObjects.addItemAt(this,0);
        return _changedObjects.source;
    }

    public function clearChangedObjects() : void
    {
        _changedObjects.removeAll();
    }

    /**
     * properties
     */
    private var _internal__function : String;
    private var _internal__class : String;
    private var _internal_label : String;
    private var _internal_image : String;
    private var _internal_dateTime : String;
    private var _internal_func : String;
    private var _internal_identifier : String;
    private var _internal_line : String;
    private var _internal_message : String;
    private var _internal_module : String;
    private var _internal_notifier : String;
    private var _internal_occurrences : String;
    private var _internal_qualifiedErrorSchemaURI : String;
    private var _internal_sessionID : String;
    private var _internal_severity : String;
    private var _internal_tag : String;
    private var _internal_uniqueid : String;
    private var _internal_type : String;
    private var _internal_context : String;
    private var _internal_group : String;
    private var _internal_id : String;
    private var _internal_uuid : String;
    private var _internal_zone : String;
    private var _internal_instance : String;
    private var _internal_service : String;
    private var _internal_view : ArrayCollection;
    model_internal var _internal_view_leaf:valueObjects.View;

    private static var emptyArray:Array = new Array();


    /**
     * derived property cache initialization
     */
    model_internal var _cacheInitialized_isValid:Boolean = false;

    model_internal var _changeWatcherArray:Array = new Array();

    public function _Super_View()
    {
        _model = new _ViewEntityMetadata(this);

        // Bind to own data or source properties for cache invalidation triggering

    }

    /**
     * data/source property getters
     */

    [Bindable(event="propertyChange")]
    public function get _function() : String
    {
        return _internal__function;
    }

    [Bindable(event="propertyChange")]
    public function get _class() : String
    {
        return _internal__class;
    }

    [Bindable(event="propertyChange")]
    public function get label() : String
    {
        return _internal_label;
    }

    [Bindable(event="propertyChange")]
    public function get image() : String
    {
        return _internal_image;
    }

    [Bindable(event="propertyChange")]
    public function get dateTime() : String
    {
        return _internal_dateTime;
    }

    [Bindable(event="propertyChange")]
    public function get func() : String
    {
        return _internal_func;
    }

    [Bindable(event="propertyChange")]
    public function get identifier() : String
    {
        return _internal_identifier;
    }

    [Bindable(event="propertyChange")]
    public function get line() : String
    {
        return _internal_line;
    }

    [Bindable(event="propertyChange")]
    public function get message() : String
    {
        return _internal_message;
    }

    [Bindable(event="propertyChange")]
    public function get module() : String
    {
        return _internal_module;
    }

    [Bindable(event="propertyChange")]
    public function get notifier() : String
    {
        return _internal_notifier;
    }

    [Bindable(event="propertyChange")]
    public function get occurrences() : String
    {
        return _internal_occurrences;
    }

    [Bindable(event="propertyChange")]
    public function get qualifiedErrorSchemaURI() : String
    {
        return _internal_qualifiedErrorSchemaURI;
    }

    [Bindable(event="propertyChange")]
    public function get sessionID() : String
    {
        return _internal_sessionID;
    }

    [Bindable(event="propertyChange")]
    public function get severity() : String
    {
        return _internal_severity;
    }

    [Bindable(event="propertyChange")]
    public function get tag() : String
    {
        return _internal_tag;
    }

    [Bindable(event="propertyChange")]
    public function get uniqueid() : String
    {
        return _internal_uniqueid;
    }

    [Bindable(event="propertyChange")]
    public function get type() : String
    {
        return _internal_type;
    }

    [Bindable(event="propertyChange")]
    public function get context() : String
    {
        return _internal_context;
    }

    [Bindable(event="propertyChange")]
    public function get group() : String
    {
        return _internal_group;
    }

    [Bindable(event="propertyChange")]
    public function get id() : String
    {
        return _internal_id;
    }

    [Bindable(event="propertyChange")]
    public function get uuid() : String
    {
        return _internal_uuid;
    }

    [Bindable(event="propertyChange")]
    public function get zone() : String
    {
        return _internal_zone;
    }

    [Bindable(event="propertyChange")]
    public function get instance() : String
    {
        return _internal_instance;
    }

    [Bindable(event="propertyChange")]
    public function get service() : String
    {
        return _internal_service;
    }

    [Bindable(event="propertyChange")]
    public function get view() : ArrayCollection
    {
        return _internal_view;
    }

    public function clearAssociations() : void
    {
    }

    /**
     * data/source property setters
     */

    public function set _function(value:String) : void
    {
        var oldValue:String = _internal__function;
        if (oldValue !== value)
        {
            _internal__function = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_function", oldValue, _internal__function));
        }
    }

    public function set _class(value:String) : void
    {
        var oldValue:String = _internal__class;
        if (oldValue !== value)
        {
            _internal__class = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_class", oldValue, _internal__class));
        }
    }

    public function set label(value:String) : void
    {
        var oldValue:String = _internal_label;
        if (oldValue !== value)
        {
            _internal_label = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "label", oldValue, _internal_label));
        }
    }

    public function set image(value:String) : void
    {
        var oldValue:String = _internal_image;
        if (oldValue !== value)
        {
            _internal_image = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "image", oldValue, _internal_image));
        }
    }

    public function set dateTime(value:String) : void
    {
        var oldValue:String = _internal_dateTime;
        if (oldValue !== value)
        {
            _internal_dateTime = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "dateTime", oldValue, _internal_dateTime));
        }
    }

    public function set func(value:String) : void
    {
        var oldValue:String = _internal_func;
        if (oldValue !== value)
        {
            _internal_func = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "func", oldValue, _internal_func));
        }
    }

    public function set identifier(value:String) : void
    {
        var oldValue:String = _internal_identifier;
        if (oldValue !== value)
        {
            _internal_identifier = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "identifier", oldValue, _internal_identifier));
        }
    }

    public function set line(value:String) : void
    {
        var oldValue:String = _internal_line;
        if (oldValue !== value)
        {
            _internal_line = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "line", oldValue, _internal_line));
        }
    }

    public function set message(value:String) : void
    {
        var oldValue:String = _internal_message;
        if (oldValue !== value)
        {
            _internal_message = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "message", oldValue, _internal_message));
        }
    }

    public function set module(value:String) : void
    {
        var oldValue:String = _internal_module;
        if (oldValue !== value)
        {
            _internal_module = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "module", oldValue, _internal_module));
        }
    }

    public function set notifier(value:String) : void
    {
        var oldValue:String = _internal_notifier;
        if (oldValue !== value)
        {
            _internal_notifier = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "notifier", oldValue, _internal_notifier));
        }
    }

    public function set occurrences(value:String) : void
    {
        var oldValue:String = _internal_occurrences;
        if (oldValue !== value)
        {
            _internal_occurrences = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "occurrences", oldValue, _internal_occurrences));
        }
    }

    public function set qualifiedErrorSchemaURI(value:String) : void
    {
        var oldValue:String = _internal_qualifiedErrorSchemaURI;
        if (oldValue !== value)
        {
            _internal_qualifiedErrorSchemaURI = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "qualifiedErrorSchemaURI", oldValue, _internal_qualifiedErrorSchemaURI));
        }
    }

    public function set sessionID(value:String) : void
    {
        var oldValue:String = _internal_sessionID;
        if (oldValue !== value)
        {
            _internal_sessionID = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "sessionID", oldValue, _internal_sessionID));
        }
    }

    public function set severity(value:String) : void
    {
        var oldValue:String = _internal_severity;
        if (oldValue !== value)
        {
            _internal_severity = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "severity", oldValue, _internal_severity));
        }
    }

    public function set tag(value:String) : void
    {
        var oldValue:String = _internal_tag;
        if (oldValue !== value)
        {
            _internal_tag = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "tag", oldValue, _internal_tag));
        }
    }

    public function set uniqueid(value:String) : void
    {
        var oldValue:String = _internal_uniqueid;
        if (oldValue !== value)
        {
            _internal_uniqueid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "uniqueid", oldValue, _internal_uniqueid));
        }
    }

    public function set type(value:String) : void
    {
        var oldValue:String = _internal_type;
        if (oldValue !== value)
        {
            _internal_type = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "type", oldValue, _internal_type));
        }
    }

    public function set context(value:String) : void
    {
        var oldValue:String = _internal_context;
        if (oldValue !== value)
        {
            _internal_context = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "context", oldValue, _internal_context));
        }
    }

    public function set group(value:String) : void
    {
        var oldValue:String = _internal_group;
        if (oldValue !== value)
        {
            _internal_group = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "group", oldValue, _internal_group));
        }
    }

    public function set id(value:String) : void
    {
        var oldValue:String = _internal_id;
        if (oldValue !== value)
        {
            _internal_id = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "id", oldValue, _internal_id));
        }
    }

    public function set uuid(value:String) : void
    {
        var oldValue:String = _internal_uuid;
        if (oldValue !== value)
        {
            _internal_uuid = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "uuid", oldValue, _internal_uuid));
        }
    }

    public function set zone(value:String) : void
    {
        var oldValue:String = _internal_zone;
        if (oldValue !== value)
        {
            _internal_zone = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "zone", oldValue, _internal_zone));
        }
    }

    public function set instance(value:String) : void
    {
        var oldValue:String = _internal_instance;
        if (oldValue !== value)
        {
            _internal_instance = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "instance", oldValue, _internal_instance));
        }
    }

    public function set service(value:String) : void
    {
        var oldValue:String = _internal_service;
        if (oldValue !== value)
        {
            _internal_service = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "service", oldValue, _internal_service));
        }
    }

    public function set view(value:*) : void
    {
        var oldValue:ArrayCollection = _internal_view;
        if (oldValue !== value)
        {
            if (value is ArrayCollection)
            {
                _internal_view = value;
            }
            else if (value is Array)
            {
                _internal_view = new ArrayCollection(value);
            }
            else if (value == null)
            {
                _internal_view = null;
            }
            else
            {
                throw new Error("value of view must be a collection");
            }
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "view", oldValue, _internal_view));
        }
    }

    /**
     * Data/source property setter listeners
     *
     * Each data property whose value affects other properties or the validity of the entity
     * needs to invalidate all previously calculated artifacts. These include:
     *  - any derived properties or constraints that reference the given data property.
     *  - any availability guards (variant expressions) that reference the given data property.
     *  - any style validations, message tokens or guards that reference the given data property.
     *  - the validity of the property (and the containing entity) if the given data property has a length restriction.
     *  - the validity of the property (and the containing entity) if the given data property is required.
     */


    /**
     * valid related derived properties
     */
    model_internal var _isValid : Boolean;
    model_internal var _invalidConstraints:Array = new Array();
    model_internal var _validationFailureMessages:Array = new Array();

    /**
     * derived property calculators
     */

    /**
     * isValid calculator
     */
    model_internal function calculateIsValid():Boolean
    {
        var violatedConsts:Array = new Array();
        var validationFailureMessages:Array = new Array();

        var propertyValidity:Boolean = true;

        model_internal::_cacheInitialized_isValid = true;
        model_internal::invalidConstraints_der = violatedConsts;
        model_internal::validationFailureMessages_der = validationFailureMessages;
        return violatedConsts.length == 0 && propertyValidity;
    }

    /**
     * derived property setters
     */

    model_internal function set isValid_der(value:Boolean) : void
    {
        var oldValue:Boolean = model_internal::_isValid;
        if (oldValue !== value)
        {
            model_internal::_isValid = value;
            _model.model_internal::fireChangeEvent("isValid", oldValue, model_internal::_isValid);
        }
    }

    /**
     * derived property getters
     */

    [Transient]
    [Bindable(event="propertyChange")]
    public function get _model() : _ViewEntityMetadata
    {
        return model_internal::_dminternal_model;
    }

    public function set _model(value : _ViewEntityMetadata) : void
    {
        var oldValue : _ViewEntityMetadata = model_internal::_dminternal_model;
        if (oldValue !== value)
        {
            model_internal::_dminternal_model = value;
            this.dispatchEvent(mx.events.PropertyChangeEvent.createUpdateEvent(this, "_model", oldValue, model_internal::_dminternal_model));
        }
    }

    /**
     * methods
     */


    /**
     *  services
     */
    private var _managingService:com.adobe.fiber.services.IFiberManagingService;

    public function set managingService(managingService:com.adobe.fiber.services.IFiberManagingService):void
    {
        _managingService = managingService;
    }

    model_internal function set invalidConstraints_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_invalidConstraints;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_invalidConstraints = value;
            _model.model_internal::fireChangeEvent("invalidConstraints", oldValue, model_internal::_invalidConstraints);
        }
    }

    model_internal function set validationFailureMessages_der(value:Array) : void
    {
        var oldValue:Array = model_internal::_validationFailureMessages;
        // avoid firing the event when old and new value are different empty arrays
        if (oldValue !== value && (oldValue.length > 0 || value.length > 0))
        {
            model_internal::_validationFailureMessages = value;
            _model.model_internal::fireChangeEvent("validationFailureMessages", oldValue, model_internal::_validationFailureMessages);
        }
    }


}

}
