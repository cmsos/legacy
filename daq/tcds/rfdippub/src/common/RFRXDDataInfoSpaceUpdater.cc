#include "tcds/rfdippub/RFRXDDataInfoSpaceUpdater.h"

#include <set>

#include "toolbox/string.h"
#include "xcept/Exception.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/Zone.h"
#include "xdata/exception/Exception.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/Table.h"

#include "tcds/exception/Exception.h"
#include "tcds/utils/ConfigurationInfoSpaceHandler.h"
// #include "tcds/utils/CurlGetter.h"
#include "tcds/utils/InfoSpaceHandler.h"
#include "tcds/utils/InfoSpaceItem.h"
#include "tcds/utils/LogMacros.h"
#include "tcds/utils/Utils.h"
#include "tcds/utils/XDAQAppBase.h"

tcds::rfdippub::RFRXDDataInfoSpaceUpdater::RFRXDDataInfoSpaceUpdater(tcds::utils::XDAQAppBase& xdaqApp) :
  tcds::utils::InfoSpaceUpdater(xdaqApp)
{
}

tcds::rfdippub::RFRXDDataInfoSpaceUpdater::~RFRXDDataInfoSpaceUpdater()
{
}

// bool
// tcds::rfdippub::RFRXDDataInfoSpaceUpdater::updateInfoSpaceItem(tcds::utils::InfoSpaceItem& item,
//                                                                tcds::utils::InfoSpaceHandler* const infoSpaceHandler)
// {
//   bool updated = false;
//   std::string const name = item.name();
//   tcds::utils::InfoSpaceItem::UpdateType const updateType = item.updateType();
//   if (updateType == tcds::utils::InfoSpaceItem::PROCESS)
//     {
//       // The 'PROCESS' update type means that there is something
//       // special to the variable. Figure out what to do based on
//       // the variable name.
//       if (name == "apps_info")
//         {
//           std::string newVal = getRFRXDDataInfo();
//           infoSpaceHandler->setString(name, newVal);
//           updated = true;
//         }
//     }
//   if (!updated)
//     {
//       updated = tcds::utils::InfoSpaceUpdater::updateInfoSpaceItem(item, infoSpaceHandler);
//     }
//   if (updated)
//     {
//       item.setValid();
//     }
//   return updated;
// }

// std::string
// tcds::rfdippub::RFRXDDataInfoSpaceUpdater::getRFRXDDataInfo() const
// {
//   // Find all applications, in our own zone, in group 'tcds'.
//   std::string const zoneName = xdaqApp_.getApplicationContext()->getDefaultZoneName();
//   xdaq::ApplicationGroup* const group =
//     xdaqApp_.getApplicationContext()->getZone(zoneName)->getApplicationGroup("tcds");
//   std::set<xdaq::ApplicationDescriptor*> const descriptors =
//     group->getApplicationDescriptors();
//   std::set<xdaq::ApplicationDescriptor*, compareApplications> apps;
//   apps.insert(descriptors.begin(), descriptors.end());

//   //----------

//   // Get the information about all applications from the LAS.
//   std::string const lasURL =
//     xdaqApp_.getConfigurationInfoSpaceHandler().getString("lasURL");
//   std::string const lasServiceName =
//     xdaqApp_.getConfigurationInfoSpaceHandler().getString("lasServiceName");
//   std::string const flashlistName =
//     xdaqApp_.getConfigurationInfoSpaceHandler().getString("flashlistName");
//   xdata::Table flashlist = getFlashList(lasURL, lasServiceName, flashlistName);

//   //----------

//   // Loop over all found applications and collect some info.
//   std::string res;
//   for (std::set<xdaq::ApplicationDescriptor*>::const_iterator i = apps.begin();
//        i != apps.end();
//        ++i)
//     {
//       // Of course we're not interested in our own application
//       // state...
//       if (!(*i)->equals(*xdaqApp_.getApplicationDescriptor()))
//         {
//           // If we're not on the first line, close the previous line
//           // with a comma.
//           if (!res.empty())
//             {
//               res += ", ";
//             }

//           // Start of JSON object.
//           res += "{";

//           // Application service name.
//           std::string serviceName = "unknown";
//           std::string tmp = (*i)->getAttribute("service");
//           if (!tmp.empty())
//             {
//               serviceName = tmp;
//             }
//           res +=
//             tcds::utils::escapeAsJSONString("service") +
//             ": " +
//             tcds::utils::escapeAsJSONString(serviceName);

//           // Full application URL.
//           std::string const url = (*i)->getContextDescriptor()->getURL();
//           unsigned int const lid = (*i)->getLocalId();
//           std::string const lidStr = toolbox::toString("%d", lid);
//           std::string const urn = (*i)->getURN();
//           std::string const fullUrl = url + "/" + urn;
//           res +=
//             ", " +
//             tcds::utils::escapeAsJSONString("url") +
//             ": " +
//             tcds::utils::escapeAsJSONString(fullUrl);

//           // Application-icon URL.
//           std::string iconUrl = xdaqApp_.buildIconPathName(*i);
//           res +=
//             ", " +
//             tcds::utils::escapeAsJSONString("iconUrl") +
//             ": " +
//             tcds::utils::escapeAsJSONString(iconUrl);

//           std::string stateName = "unknown";
//           std::string hwLeaseOwner = "unknown";
//           std::string applicationState = "unknown";
//           std::string problemDescription = "unknown";

//           // Find the correct row in the table.
//           for (size_t j = 0; j != flashlist.getRowCount(); ++j)
//             {
//               if ((flashlist.getValueAt(j, "context")->toString() == url) &&
//                   (flashlist.getValueAt(j, "lid")->toString() == lidStr))
//                 {
//                   stateName = flashlist.getValueAt(j, "state_name")->toString();
//                   hwLeaseOwner = flashlist.getValueAt(j, "hw_lease_owner_id")->toString();
//                   applicationState = flashlist.getValueAt(j, "application_state")->toString();
//                   problemDescription = flashlist.getValueAt(j, "problem_description")->toString();
//                   break;
//                 }
//             }

//           // if (found)
//           //   {
//           //   }

//           // // NOTE: The SOAP parameter-query for the FSM state name is
//           // // used to determine if an application is reachable or not.
//           // std::string stateName = "unknown";
//           // std::string hwLeaseOwner = "unknown";
//           // std::string applicationState = "unknown";
//           // std::string problemDescription = "unknown";
//           // bool isReachable = false;
//           // try
//           //   {
//           //     // The FSM state of the remote application.
//           //     stateName = xdaqApp_.executeParameterQuery("stateName", "xsd:string", **i);
//           //     isReachable = true;
//           //   }
//           // catch (xcept::Exception& err)
//           //   {
//           //     stateName = "Application unreachable";
//           //     hwLeaseOwner = "";
//           //     std::string const msg(toolbox::toString("Could not reach application at '%s'.",
//           //                                             fullUrl.c_str()));
//           //     XCEPT_DECLARE_NESTED(tcds::exception::RuntimeProblem,
//           //                          top,
//           //                          msg,
//           //                          err);
//           //     xdaqApp_.notifyQualified("ERROR", top);
//           //   }

//           // if (isReachable)
//           //   {
//           //     // The hardware lease owner of the remote application.
//           //     try
//           //       {
//           //         hwLeaseOwner = xdaqApp_.executeParameterQuery("hwLeaseOwnerId", "xsd:string", **i);
//           //       }
//           //     catch (xcept::Exception& err)
//           //       {
//           //         // Pass. Not all applications have hardware to lease.
//           //         hwLeaseOwner = "n/a";
//           //       }
//           //     // The overall application state.
//           //     try
//           //       {
//           //         applicationState = xdaqApp_.executeParameterQuery("applicationState", "xsd:string", **i);
//           //       }
//           //     catch (xcept::Exception& err)
//           //       {
//           //         // Pass.
//           //         applicationState = "n/a";
//           //       }
//           //     // The problem description.
//           //     try
//           //       {
//           //         problemDescription = xdaqApp_.executeParameterQuery("problemDescription", "xsd:string", **i);
//           //       }
//           //     catch (xcept::Exception& err)
//           //       {
//           //         // Pass. Not all applications have hardware to lease.
//           //         problemDescription = "n/a";
//           //       }
//           //   }

//           res +=
//             ", " +
//             tcds::utils::escapeAsJSONString("stateName") +
//             ": " +
//             tcds::utils::escapeAsJSONString(stateName);
//           res +=
//             ", " +
//             tcds::utils::escapeAsJSONString("hwLeaseOwnerId") +
//             ": " +
//             tcds::utils::escapeAsJSONString(hwLeaseOwner);
//           res +=
//             ", " +
//             tcds::utils::escapeAsJSONString("applicationState") +
//             ": " +
//             tcds::utils::escapeAsJSONString(applicationState);
//           res +=
//             ", " +
//             tcds::utils::escapeAsJSONString("problemDescription") +
//             ": " +
//             tcds::utils::escapeAsJSONString(problemDescription);

//           // End of JSON object.
//           res += "}";
//         }
//     }

//   res = "[" + res + "]";

//   return res;
// }

// xdata::Table
// tcds::rfdippub::RFRXDDataInfoSpaceUpdater::getFlashList(std::string const& lasURL,
//                                                         std::string const& lasServiceName,
//                                                         std::string const& flashListName) const
// {
//   std::string const lasAddress =
//     "urn:xdaq-application:service=" + lasServiceName + "/retrieveCollection";
//   // The data we get will have (E)XDR format
//   std::string lasQuery =
//     "fmt=exdr&flash=urn:xdaq-flashlist:" + flashListName;
//   // BUG BUG BUG
//   // There seems to be something dodgy with the slash2g. Without
//   // 'filter' expression it returns only a single row.
//   // https://svnweb.cern.ch/trac/cmsos/ticket/3309
//   lasQuery += ";service=.*";
//   // BUG BUG BUG end

//   tcds::rfdippub::CurlGetter getter;
//   std::string rawTableData = getter.get("http", lasURL, lasAddress, "", lasQuery, "");

//   xdata::exdr::FixedSizeInputStreamBuffer
//     inBuffer(static_cast<char*>(&rawTableData[0]), rawTableData.size());

//   xdata::Table table;
//   xdata::exdr::Serializer serializer;
//   try
//     {
//       serializer.import(&table, &inBuffer);
//     }
//   catch (xdata::exception::Exception& err)
//     {
//       std::string const msgBase = "Failed to deserialize incoming flashlist table";
//       std::string const msg =
//         toolbox::toString("%s: '%s'.", msgBase.c_str(), err.message().c_str());
//       XCEPT_RETHROW(tcds::exception::RuntimeProblem, msg, err);
//     }

//   return table;
// }
