// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_udapl_dat_PublicServicePoint_h_
#define _pt_udapl_dat_PublicServicePoint_h_

#include <iostream>
#include <string>

#include "pt/udapl/dat/Utils.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/lang/Class.h"
#include "pt/udapl/exception/Exception.h"

namespace pt
{
	namespace udapl
	{
		namespace dat
		{

			class InterfaceAdapter;

			class PublicServicePoint : public toolbox::lang::Class
			{
					friend class InterfaceAdapter;

				public:

					PublicServicePoint (pt::udapl::dat::InterfaceAdapter * ia, DAT_CONN_QUAL port) ;

					virtual ~PublicServicePoint ();

					/*
					 friend ostream &operator<<(ostream &cout, EndPoint ep);
					 */

				protected:

					bool process (toolbox::task::WorkLoop* wl);

					DAT_PSP_HANDLE psp_handle_;

					toolbox::task::WorkLoop* workLoop_;
					pt::udapl::dat::InterfaceAdapter * ia_;

			};
		}
	}
}

#endif
