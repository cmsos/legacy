#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

#include "xcept/tools.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/TimeInterval.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Timer.h"
#include "b2in/nub/Method.h"
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "xdata/InfoSpaceFactory.h"
#include "xdata/Serializable.h"
#include "xdata/ItemGroupEvent.h"

#include "bril/bcm1fprocessor/Application.h" 
#include "bril/bcm1fprocessor/exception/Exception.h"
#include <math.h>
#include <map>
#include <algorithm>

#include "bril/webutils/WebUtils.h"

XDAQ_INSTANTIATOR_IMPL (bril::bcm1fprocessor::Application)

bril::bcm1fprocessor::Application::Application (xdaq::ApplicationStub* s) throw (xdaq::exception::Exception) : xdaq::Application(s),xgi::framework::UIManager(this),eventing::api::Member(this),m_applock(toolbox::BSem::FULL){
  xgi::framework::deferredbind(this,this,&bril::bcm1fprocessor::Application::Default,"Default");
  xgi::framework::deferredbind(this,this,&bril::bcm1fprocessor::Application::plotting,"plotting");
  xgi::framework::deferredbind(this,this,&bril::bcm1fprocessor::Application::RawRHUhists,"RawRHUhists");
  xgi::deferredbind( this, this, &Application::requestData, "requestData" ); //naked data
  b2in::nub::bind(this, &bril::bcm1fprocessor::Application::onMessage);
  m_appInfoSpace = getApplicationInfoSpace();
  m_appDescriptor= getApplicationDescriptor();
  m_classname    = m_appDescriptor->getClassName();
  m_poolFactory  = toolbox::mem::getMemoryPoolFactory();
  m_collectnb = 4;
  //m_collectnb = 1;
  //  m_collectchannel = interface::bril::BCM1F_NRU*(interface::bril::BCM1F_NCRATE-1);
  //m_collectchannel = 1;
  m_numberOfNibbles = 0; // 4;
  m_nChannelsThisNibble = 0;
  m_nHistsDiscarded = 0;
  m_alignedNB4 = false;
  m_BG1_plus = 0;
  m_BG2_minus = 0;

  m_last4_bkg1[0] = m_last4_bkg1[1] = m_last4_bkg1[2] = m_last4_bkg1[3] = 0;

  m_last4_bkg2[0] = m_last4_bkg2[1] = m_last4_bkg2[2] = m_last4_bkg2[3] = 0;

  // m_lumi_avg = 0;
  // m_lumi_avg_raw = 0;
  memset(m_lumi_avg,0,sizeof(m_lumi_avg));
  memset(m_lumi_avg_raw,0,sizeof(m_lumi_avg_raw));
  memset(m_lumi_bx,0,sizeof(m_lumi_bx));
  memset(m_lumi_bx_raw,0,sizeof(m_lumi_bx_raw));

  // output of albedo workloop
  for (int i=0;i<3564;i++)
  {
  m_albedo_fraction_pcvd[i]=1.0;
  }
  m_raw_noise_pcvd = 0.0;





  //  memset (m_useChannelForLumi,true,sizeof(m_useChannelForLumi));
  memset(m_useChannelForLumi,false,sizeof(m_useChannelForLumi));
  memset(m_useChannelForBkgd,false,sizeof(m_useChannelForBkgd));
  //memset(m_sensorConnected,false,sizeof(m_sensorConnected));
  
  memset(m_isChannelpCVD,false,sizeof(m_isChannelpCVD));
  memset(m_isChannelsCVD,false,sizeof(m_isChannelsCVD));
  memset(m_isChannelSilicon,false,sizeof(m_isChannelSilicon));
  memset(m_isChannelValid,false,sizeof(m_isChannelValid));

  m_isChannelpCVD[0]=m_isChannelpCVD[1]=
    m_isChannelpCVD[4]=m_isChannelpCVD[5]=
    m_isChannelpCVD[8]=m_isChannelpCVD[9]=
    m_isChannelpCVD[12]=m_isChannelpCVD[13]=
    m_isChannelpCVD[16]=m_isChannelpCVD[17]=
    m_isChannelpCVD[24]=m_isChannelpCVD[25]=
    m_isChannelpCVD[28]=m_isChannelpCVD[29]=
    m_isChannelpCVD[32]=m_isChannelpCVD[33]=
    m_isChannelpCVD[36]=m_isChannelpCVD[37]=
    m_isChannelpCVD[40]=m_isChannelpCVD[41]=true;

  m_isChannelsCVD[10]=m_isChannelsCVD[11]=
    m_isChannelsCVD[22]=m_isChannelsCVD[23]=
    m_isChannelsCVD[34]=m_isChannelsCVD[35]=
    m_isChannelsCVD[46]=m_isChannelsCVD[47]=true;

  m_isChannelSilicon[3]=
    m_isChannelSilicon[6]=
    m_isChannelSilicon[15]=
    m_isChannelSilicon[19]=
//    m_isChannelSilicon[21]=  // connected but off by default
    m_isChannelSilicon[27]=
    m_isChannelSilicon[31]=
    m_isChannelSilicon[38]=
    m_isChannelSilicon[42]=true;
//    m_isChannelSilicon[45]=true; // connected but off by default

  for (int channel = 0; channel < 48; channel++)
    {
      m_isChannelValid[channel] = (m_isChannelpCVD[channel] || m_isChannelsCVD[channel] || m_isChannelSilicon[channel]);
    }


  memset (m_bkgd1mask,false,sizeof(m_bkgd1mask));
  memset (m_bkgd2mask,false,sizeof(m_bkgd2mask));
  memset (m_collmask,false,sizeof(m_collmask));
  
  // for (unsigned int bunch = 0; bunch < 3564; bunch++)
  //   {
  //     if (m_bkgd1mask[bunch] == true)
  //       {
  //      std::stringstream ss;
  //         ss << "(memset) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
  //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
  //       }
  //     if (m_bkgd2mask[bunch] == true)
  //       {
  //      std::stringstream ss;
  //         ss << "(memset) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
  //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
  //       }
  //   }
  

  m_iTot1 = 0.;
  m_iTot2 = 0.;

  memset (m_iBunch1,0.,sizeof(m_iBunch1));
  memset (m_iBunch2,0.,sizeof(m_iBunch2));

  //  memset (m_totalChannelRate,0.,sizeof(m_totalChannelRate));
  //  std::fill(begin(m_totalChannelRate), end(m_totalChannelRate), 0.); // not needed?
  //  m_totalChannelRate(64,0.); // initialize xdata::Vector of xdata::Floats? NOPE
  //  m_totalChannelRate = new xdata::Vector();
  //  m_totalChannelRate.resize(s_nAllChannels);
  m_totalChannelRate.resize(s_nAllChannels, 0.);
  // for (int i = 0; i < 64; i++)
  //   {
  //     m_totalChannelRate.push_back(0.);
  //   }
  //  std::fill(m_totalChannelRate.begin(), m_totalChannelRate.end(), 0.);

  m_countAlbedoHigherThanBackground = 0;

  m_beammode = "";

  toolbox::net::URN memurn("toolbox-mem-pool",m_classname+"_mem"); 
  try{
    toolbox::mem::HeapAllocator* allocator = new toolbox::mem::HeapAllocator();
    m_memPool = m_poolFactory->createPool(memurn,allocator);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_datasources"); 
    m_appInfoSpace->fireItemAvailable("eventinginput",&m_datasources);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_outputtopics"); 
    m_appInfoSpace->fireItemAvailable("eventingoutput",&m_outputtopics);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_nRHUs"); 
    m_appInfoSpace->fireItemAvailable("nRHUs",&m_nRHUs);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_calibtag"); 
    m_appInfoSpace->fireItemAvailable("calibtag",&m_calibtag);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_best_sensor_type"); 
    m_appInfoSpace->fireItemAvailable("bestsensortype",&m_best_sensor_type);
    // //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_sigma_vis"); 
    // m_appInfoSpace->fireItemAvailable("sigmavis",&m_sigma_vis);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_sigma_vis_scvd"); 
    m_appInfoSpace->fireItemAvailable("sigmavisscvd",&m_sigma_vis_scvd);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_sigma_vis_pcvd"); 
    m_appInfoSpace->fireItemAvailable("sigmavispcvd",&m_sigma_vis_pcvd);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_sigma_vis_si"); 
    m_appInfoSpace->fireItemAvailable("sigmavissi",&m_sigma_vis_si);
    m_appInfoSpace->fireItemAvailable("SBILcalibscvd",&m_calib_at_SBIL_scvd);
    m_appInfoSpace->fireItemAvailable("SBILcalibpcvd",&m_calib_at_SBIL_pcvd);
    m_appInfoSpace->fireItemAvailable("SBILcalibsi",&m_calib_at_SBIL_si);
    m_appInfoSpace->fireItemAvailable("nonlinearityscvd",&m_nonlinearity_scvd);
    m_appInfoSpace->fireItemAvailable("nonlinearitypcvd",&m_nonlinearity_pcvd);
    m_appInfoSpace->fireItemAvailable("nonlinearitysi",&m_nonlinearity_si);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_verbose"); 
    m_appInfoSpace->fireItemAvailable("verbose",&m_verbose);

    m_appInfoSpace->fireItemAvailable("tp_mask_start",&m_tp_mask_start);
    m_appInfoSpace->fireItemAvailable("tp_mask_end",&m_tp_mask_end);

    m_appInfoSpace->fireItemAvailable("albedo_calculation_time",&m_albedo_calculation_time);
    m_appInfoSpace->fireItemAvailable("albedo_queue_length",&m_albedo_queue_length);
    m_appInfoSpace->fireItemAvailable("noise_calc_start",&m_noise_calc_start);
    m_appInfoSpace->fireItemAvailable("noise_calc_end",&m_noise_calc_end);

    m_appInfoSpace->fireItemAvailable("albedo_model_pcvd",&m_albedo_model_pcvd_str);
    m_appInfoSpace->fireItemAvailable("albedo_model_si",&m_albedo_model_si_str);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_activeChannelsLumi"); 
    m_appInfoSpace->fireItemAvailable("activechannelslumi",&m_activeChannelsLumi);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_activeChannelsBkgd"); 
    m_appInfoSpace->fireItemAvailable("activechannelsbkgd",&m_activeChannelsBkgd);
    //m_appInfoSpace->fireItemAvailable("sensorConnected",&m_sensorConnected);
    // //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_useChannelCalibrations"); 
    // m_appInfoSpace->fireItemAvailable("useChannelCalibrations",&m_useChannelCalibrations);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelCalibrations"); 
    // m_appInfoSpace->fireItemAvailable("channelCalibrations",&m_channelCalibrations);
    // //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelAcceptances"); 
    m_appInfoSpace->fireItemAvailable("channelAcceptances",&m_channelAcceptances);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelEfficiencies"); 
    m_appInfoSpace->fireItemAvailable("channelEfficiencies",&m_channelEfficiencies);
    //LOG4CPLUS_INFO(getApplicationLogger(),"fireItemAvailable: m_channelCorrections"); 
    m_appInfoSpace->fireItemAvailable("channelCorrections",&m_channelCorrections);
    m_appInfoSpace->addListener(this,"urn:xdaq-event:setDefaultValues");
    m_publishing = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_publishing","waiting");
    m_albedo_calculation = toolbox::task::getWorkLoopFactory()->getWorkLoop(m_appDescriptor->getURN()+"_albedo_calculation","waiting");
    setupMonitoring();
  }catch(xcept::Exception& e){
    std::string msg("Failed to setup memory pool ");
    LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
    XCEPT_RETHROW(bril::bcm1fprocessor::exception::Exception,msg,e);      
  }

}

bril::bcm1fprocessor::Application::~Application (){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    delete it->second;
  }
  m_topicoutqueues.clear();
}


// This makes the HyperDAQ page
void bril::bcm1fprocessor::Application::Default (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){
  //  m_applock.take();

  *out << busesToHTML();
  std::string appurl = getApplicationContext()->getContextDescriptor()->getURL()+"/"+m_appDescriptor->getURN();
  *out << "URL: "<< appurl;
  *out << cgicc::br();

  *out << cgicc::table().set("class","xdaq-table-vertical");
  *out << cgicc::caption("input/output");
  *out << cgicc::tbody();

  *out << cgicc::tr();
  *out << cgicc::th("calibtag");
  *out << cgicc::td( m_calibtag );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("Best sensor type");
  *out << cgicc::td( m_best_sensor_type );
  *out << cgicc::tr();

  // *out << cgicc::tr();
  // *out << cgicc::th("sigmaVis");
  // *out << cgicc::td( m_sigma_vis.toString() );
  // *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("sigmaVis (sCVD)");
  *out << cgicc::td( m_sigma_vis_scvd.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("sigmaVis (pCVD)");
  *out << cgicc::td( m_sigma_vis_pcvd.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("sigmaVis (Si)");
  *out << cgicc::td( m_sigma_vis_si.toString() );
  *out << cgicc::tr();

  // *out << cgicc::tr();
  // *out << cgicc::th("useChannelCalibrations");
  // *out << cgicc::td( m_useChannelCalibrations.toString() );
  // *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("in_topic");
  *out << cgicc::td( interface::bril::bcm1fhistT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("in_topic");
  *out << cgicc::td( interface::bril::beamT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("in_topic");
  *out << cgicc::td( interface::bril::NB4T::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("out_topic");
  *out << cgicc::td( interface::bril::bcm1fagghistT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("out_topic");
  *out << cgicc::td( interface::bril::bcm1fbkgT::topicname() );
  *out << cgicc::tr();

  // *out << cgicc::tr();
  // *out << cgicc::th("out_topic");
  // *out << cgicc::td( interface::bril::bcm1fbkghistosT::topicname() );
  // *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("out_topic");
  *out << cgicc::td( interface::bril::bcm1flumiT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("out_topic");
  *out << cgicc::td( interface::bril::bcm1fscvdlumiT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("out_topic");
  *out << cgicc::td( interface::bril::bcm1fpcvdlumiT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("out_topic");
  *out << cgicc::td( interface::bril::bcm1fsilumiT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("out_topic");
  *out << cgicc::td( interface::bril::bxconfigT::topicname() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("Fill number");
  *out << cgicc::td( m_mon_fill.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("Run number");
  *out << cgicc::td( m_mon_run.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("LS number");
  *out << cgicc::td( m_mon_ls.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("Nibble number");
  *out << cgicc::td( m_mon_nb.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("# hists discarded on LS/run boundary");
  *out << cgicc::td( m_nHistsDiscarded.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("# nibbles collected so far this 4NB");
  //*out << cgicc::td( m_numberOfNibbles.toString() );
  *out << cgicc::td( xdata::UnsignedInteger(m_numberOfNibbles).toString() );
  //  *out << cgicc::td( m_numberOfNibbles );
  *out << cgicc::tr();

  *out << cgicc::tbody();  
  *out << cgicc::table();
  *out << cgicc::br();



  *out << "List of all valid (connected) channels: ";
  *out << cgicc::br();
  *out << cgicc::table().set("class","xdaq-table-horizontal");
  *out << cgicc::tbody();

  *out << cgicc::tr();
  //  *out << cgicc::th("Channels");
  for (xdata::Integer channel = 0; channel < 48; channel++)
    {
      if (m_isChannelValid[channel.value_] )
      {
        *out << cgicc::td( channel.toString() );
      }
    }
  *out << cgicc::tr();

  *out << cgicc::tbody();
  *out << cgicc::table();
  *out << cgicc::br();

  *out << "Channels included in luminosity: ";
  *out << cgicc::br();
  *out << cgicc::table().set("class","xdaq-table-horizontal");
  *out << cgicc::tbody();

  *out << cgicc::tr();
  //  *out << cgicc::th("Channels");
  for (xdata::Integer channel = 0; channel < 48; channel++)
    {
      if (m_useChannelForLumi[channel.value_] && (m_isChannelValid[channel.value_] ))
      {
        *out << cgicc::td( channel.toString() );
      }
    }
  *out << cgicc::tr();

  *out << cgicc::tbody();
  *out << cgicc::table();
  *out << cgicc::br();

  *out << "Channels included in background: ";
  *out << cgicc::br();
  *out << cgicc::table().set("class","xdaq-table-horizontal");
  *out << cgicc::tbody();

  *out << cgicc::tr();
  //  *out << cgicc::th("Channels");
  for (xdata::Integer channel = 0; channel < 48; channel++)
    {
      if (m_useChannelForBkgd[channel.value_] && (m_isChannelValid[channel.value_] ))
      {
        *out << cgicc::td( channel.toString() );
      }
    }
  *out << cgicc::tr();

  *out << cgicc::tbody();
  *out << cgicc::table();
  *out << cgicc::br();


  *out << "The current background numbers ARE: ";
  *out << cgicc::br();
  *out << cgicc::table().set("class","xdaq-table-vertical");
  *out << cgicc::tbody();

  *out << cgicc::tr();
  *out << cgicc::th("BG 1");
  *out << cgicc::td( m_BG1_plus.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("BG 2");
  *out << cgicc::td( m_BG2_minus.toString() );
  *out << cgicc::tr();

  *out << cgicc::tr();
  *out << cgicc::th("How many times is albedo higher than background?");
  *out << cgicc::td( xdata::Integer(m_countAlbedoHigherThanBackground).toString() );
  *out << cgicc::tr();

  *out << cgicc::tbody();
  *out << cgicc::table();
  *out << cgicc::br();
 std::string plottingurl2 = "/" + getApplicationDescriptor()->getURN() + "/RawRHUhists";
  *out << cgicc::h4() << cgicc::a( "RAW RHU plots" ).set( "href", plottingurl2 ) << cgicc::h4();

  *out << cgicc::br();

  std::string plottingurl = "/" + getApplicationDescriptor()->getURN() + "/plotting";
  *out << cgicc::h4() << cgicc::a( "Lumi Plots" ).set( "href", plottingurl ) << cgicc::h4();

  *out << cgicc::br();
}

void bril::bcm1fprocessor::Application::plotting (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){

  using namespace cgicc;
  using std::endl;

  bril::webutils::WebChart::pageHeader( out );
  *out << cgicc::div().set( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
  *out << "<div style = \"float:left; width:50%;\">";
  m_chart_lumi_raw_pcvd->writeChart( out );
  *out << "</div> <div style = \"margin-left:50%;\">";
  m_chart_lumi_raw_si->writeChart( out );
  *out << "</div>";
  *out << cgicc::div().set( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
  *out << "<div style = \"float:left; width:50%;\">";
  m_chart_albedoqueue_pcvd->writeChart( out );
  *out << "</div> <div style = \"margin-left:50%;\">";
  m_chart_albedoqueue_si->writeChart( out );
  *out << "</div>";

  m_chart_devel->writeChart( out );
   *out << "</div>";
   m_chart_fourbinsperbx1->writeChart(out);
}
void bril::bcm1fprocessor::Application::RawRHUhists (xgi::Input * in, xgi::Output * out) throw (xgi::exception::Exception){

  using namespace cgicc;
  using std::endl;

  bril::webutils::WebChart::pageHeader( out );
  *out << cgicc::div().set( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
  *out << "<div style = \"float:left; width:50%;\">";
  m_chart_fourbinsperbx1->writeChart( out );
  *out << "</div> <div style = \"margin-left:50%;\">";
  m_chart_fourbinsperbx2->writeChart( out );
  *out << "</div>";
  *out << cgicc::div().set( "style", "float:left; width: 100%; height:5px" ) << hr() << cgicc::div() << endl;
  *out << "<div style = \"float:left; width:50%;\">";
  m_chart_fourbinsperbx3->writeChart( out );
  *out << "</div> <div style = \"margin-left:50%;\">";
  m_chart_fourbinsperbx4->writeChart( out );
  *out << "</div>";
	*out << "<div style = \"float:left; width:50%;\">";
  m_chart_fourbinsperbx5->writeChart( out );
  *out << "</div> <div style = \"margin-left:50%;\">";
  m_chart_fourbinsperbx6->writeChart( out );
   *out << "</div>";
}


void bril::bcm1fprocessor::Application::setupCharts()
{
  using namespace webutils;
  std::string cb = getApplicationDescriptor()->getContextDescriptor()->getURL() + "/" + getApplicationDescriptor()->getURN() + "/requestData";

  WebChart::property_map lumiplotpmap;
  lumiplotpmap["corrected"] = WebChart::series_properties( );//"#33cccc" );
  lumiplotpmap["uncorrected"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_lumi_raw_pcvd = new WebChart( "Lumirawpcvd", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
//  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
  "title: { text: 'Lumi raw pCVD' }, tooltip: { enable : false }", lumiplotpmap );
  m_chart_lumi_raw_si = new WebChart( "Lumirawsi", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
//  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
  "title: { text: 'Lumi raw Silicon' }, tooltip: { enable : false }", lumiplotpmap );

  m_chart_albedoqueue_pcvd = new WebChart( "Albedoqueuepcvd", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
//  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
  "title: { text: 'Albedo Queue pCVD' }, tooltip: { enable : false }", lumiplotpmap );
  m_chart_albedoqueue_si = new WebChart( "Albedoqueuesi", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
//  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
  "title: { text: 'Albedo Queue Silicon' }, tooltip: { enable : false }", lumiplotpmap );


  WebChart::property_map develplotpmap;
  develplotpmap["hist1"] = WebChart::series_properties( );//"#33cccc" );
  develplotpmap["hist2"] = WebChart::series_properties( );//"#33cccc" );
//  develplotpmap["hist3"] = WebChart::series_properties( );//"#33cccc" );
//  develplotpmap["hist4"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_devel = new WebChart( "devel", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
  "title: { text: 'Development Histogram' }, tooltip: { enable : false }", develplotpmap );
  
   WebChart::property_map fourbinsperbxplotpmap1;
  fourbinsperbxplotpmap1["Channel 1"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap1["Channel 2"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap1["Channel 3"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap1["Channel 4"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap1["Channel 5"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap1["Channel 6"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap1["Channel 7"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap1["Channel 8"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_fourbinsperbx1 = new WebChart( "fourbinsperbx1", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
  "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap1 );
  WebChart::property_map fourbinsperbxplotpmap2;
  fourbinsperbxplotpmap2["Channel 09"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap2["Channel 10"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap2["Channel 11"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap2["Channel 12"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap2["Channel 13"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap2["Channel 14"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap2["Channel 15"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap2["Channel 16"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_fourbinsperbx2 = new WebChart( "fourbinsperbx2", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
  "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap2 );
  WebChart::property_map fourbinsperbxplotpmap3;
  fourbinsperbxplotpmap3["Channel 17"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap3["Channel 18"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap3["Channel 19"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap3["Channel 20"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap3["Channel 21"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap3["Channel 22"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap3["Channel 23"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap3["Channel 24"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_fourbinsperbx3= new WebChart( "fourbinsperbx3", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
  "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap3 );
  WebChart::property_map fourbinsperbxplotpmap4;
  fourbinsperbxplotpmap4["Channel 25"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap4["Channel 26"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap4["Channel 27"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap4["Channel 28"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap4["Channel 29"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap4["Channel 30"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap4["Channel 31"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap4["Channel 32"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_fourbinsperbx4 = new WebChart( "fourbinsperbx4", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
  "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap4 );
  WebChart::property_map fourbinsperbxplotpmap5;
  fourbinsperbxplotpmap5["Channel 33"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap5["Channel 34"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap5["Channel 35"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap5["Channel 36"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap5["Channel 37"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap5["Channel 38"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap5["Channel 39"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap5["Channel 40"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_fourbinsperbx5 = new WebChart( "fourbinsperbx5", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
  "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap5 );
  WebChart::property_map fourbinsperbxplotpmap6;
  fourbinsperbxplotpmap6["Channel 41"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap6["Channel 42"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap6["Channel 43"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap6["Channel 44"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap6["Channel 45"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap6["Channel 46"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap6["Channel 47"] = WebChart::series_properties( );//"#33cccc" );
  fourbinsperbxplotpmap6["Channel 48"] = WebChart::series_properties( );//"#33cccc" );
  m_chart_fourbinsperbx6 = new WebChart( "fourbinsperbx6", cb, "type: 'line', zoomType: 'xy', animation: false",
  "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
  "title: { text: 'RHU Histogram' }, tooltip: { enable : false }", fourbinsperbxplotpmap6 );
}

void bril::bcm1fprocessor::Application::requestData( xgi::Input* in, xgi::Output* out ) throw ( xgi::exception::Exception )
{
  //LOG4CPLUS_INFO(getApplicationLogger(), "*************data requested *************");
  using namespace cgicc;
  bril::webutils::WebChart::dataHeader( out );
  Cgicc cgi( in );
  std::map<std::string, std::vector<float>> map_lumiraw_pcvd;
  map_lumiraw_pcvd["corrected"] =filterHistForPlot(m_lumi_bx_raw[1],3564);
  map_lumiraw_pcvd["uncorrected"] =filterHistForPlot(m_raw_uncor_pcvd,3564);
  m_chart_lumi_raw_pcvd->writeDataForQuery( cgi, out,  map_lumiraw_pcvd);

  std::map<std::string, std::vector<float>> map_lumiraw_si;
  map_lumiraw_si["corrected"] =filterHistForPlot(m_lumi_bx_raw[2],3564);
  map_lumiraw_si["uncorrected"] =filterHistForPlot(m_raw_uncor_si,3564);
  m_chart_lumi_raw_si->writeDataForQuery( cgi, out,  map_lumiraw_si);

  std::map<std::string, std::vector<float>> map_albedo_pcvd;
  map_albedo_pcvd["corrected"] =filterHistForPlot(m_albedoqueue_hist_pcvd_cor,3564);
  map_albedo_pcvd["uncorrected"] =filterHistForPlot(m_albedoqueue_hist_pcvd_uncor,3564);
  m_chart_albedoqueue_pcvd->writeDataForQuery( cgi, out,  map_albedo_pcvd);

  std::map<std::string, std::vector<float>> map_albedo_si;
  map_albedo_si["corrected"] =filterHistForPlot(m_albedoqueue_hist_si_cor,3564);
  map_albedo_si["uncorrected"] =filterHistForPlot(m_albedoqueue_hist_si_uncor,3564);
  m_chart_albedoqueue_si->writeDataForQuery( cgi, out,  map_albedo_si);


  std::map<std::string, std::vector<float>> map_devel;
  map_devel["hist1"] =filterHistForPlot(m_devel_hist1,3564);
  map_devel["hist2"] =filterHistForPlot(m_devel_hist2,3564);
  m_chart_devel->newDataReady();
  m_chart_devel->writeDataForQuery( cgi, out,  map_devel);
  
  
  std::map<std::string, std::vector<float>> map_fourbinsperbx1;
  map_fourbinsperbx1["Channel 1"] =filterHistForPlot(m_fourbinsperbx[0], 14256);
  map_fourbinsperbx1["Channel 2"] =filterHistForPlot(m_fourbinsperbx[1], 14256);
  map_fourbinsperbx1["Channel 3"] =filterHistForPlot(m_fourbinsperbx[2], 14256);
  map_fourbinsperbx1["Channel 4"] =filterHistForPlot(m_fourbinsperbx[3], 14256);
  map_fourbinsperbx1["Channel 5"] =filterHistForPlot(m_fourbinsperbx[4], 14256);
  map_fourbinsperbx1["Channel 6"] =filterHistForPlot(m_fourbinsperbx[5], 14256);
  map_fourbinsperbx1["Channel 7"] =filterHistForPlot(m_fourbinsperbx[6], 14256);
  map_fourbinsperbx1["Channel 8"] =filterHistForPlot(m_fourbinsperbx[7], 14256);
  m_chart_fourbinsperbx1->newDataReady();
  m_chart_fourbinsperbx1->writeDataForQuery( cgi, out,  map_fourbinsperbx1);
  
    std::map<std::string, std::vector<float>> map_fourbinsperbx2;
  map_fourbinsperbx2["Channel 09"] =filterHistForPlot(m_fourbinsperbx[8], 14256);
  map_fourbinsperbx2["Channel 10"] =filterHistForPlot(m_fourbinsperbx[9], 14256);
  map_fourbinsperbx2["Channel 11"] =filterHistForPlot(m_fourbinsperbx[10], 14256);
  map_fourbinsperbx2["Channel 12"] =filterHistForPlot(m_fourbinsperbx[11], 14256);
  map_fourbinsperbx2["Channel 13"] =filterHistForPlot(m_fourbinsperbx[12], 14256);
  map_fourbinsperbx2["Channel 14"] =filterHistForPlot(m_fourbinsperbx[13], 14256);
  map_fourbinsperbx2["Channel 15"] =filterHistForPlot(m_fourbinsperbx[14], 14256);
  map_fourbinsperbx2["Channel 16"] =filterHistForPlot(m_fourbinsperbx[15], 14256);
  m_chart_fourbinsperbx2->newDataReady();
  m_chart_fourbinsperbx2->writeDataForQuery( cgi, out,  map_fourbinsperbx2);
  
    std::map<std::string, std::vector<float>> map_fourbinsperbx3;
  map_fourbinsperbx3["Channel 17"] =filterHistForPlot(m_fourbinsperbx[16], 14256);
  map_fourbinsperbx3["Channel 18"] =filterHistForPlot(m_fourbinsperbx[17], 14256);
  map_fourbinsperbx3["Channel 19"] =filterHistForPlot(m_fourbinsperbx[18], 14256);
  map_fourbinsperbx3["Channel 20"] =filterHistForPlot(m_fourbinsperbx[19], 14256);
  map_fourbinsperbx3["Channel 21"] =filterHistForPlot(m_fourbinsperbx[20], 14256);
  map_fourbinsperbx3["Channel 22"] =filterHistForPlot(m_fourbinsperbx[21], 14256);
  map_fourbinsperbx3["Channel 23"] =filterHistForPlot(m_fourbinsperbx[22], 14256);
  map_fourbinsperbx3["Channel 24"] =filterHistForPlot(m_fourbinsperbx[23], 14256);
  m_chart_fourbinsperbx3->newDataReady();
  m_chart_fourbinsperbx3->writeDataForQuery( cgi, out,  map_fourbinsperbx3);
  
    std::map<std::string, std::vector<float>> map_fourbinsperbx4;
  map_fourbinsperbx4["Channel 25"] =filterHistForPlot(m_fourbinsperbx[24], 14256);
  map_fourbinsperbx4["Channel 26"] =filterHistForPlot(m_fourbinsperbx[25], 14256);
  map_fourbinsperbx4["Channel 27"] =filterHistForPlot(m_fourbinsperbx[26], 14256);
  map_fourbinsperbx4["Channel 28"] =filterHistForPlot(m_fourbinsperbx[27], 14256);
  map_fourbinsperbx4["Channel 29"] =filterHistForPlot(m_fourbinsperbx[28], 14256);
  map_fourbinsperbx4["Channel 30"] =filterHistForPlot(m_fourbinsperbx[29], 14256);
  map_fourbinsperbx4["Channel 31"] =filterHistForPlot(m_fourbinsperbx[30], 14256);
  map_fourbinsperbx4["Channel 32"] =filterHistForPlot(m_fourbinsperbx[31], 14256);
  m_chart_fourbinsperbx4->newDataReady();
  m_chart_fourbinsperbx4->writeDataForQuery( cgi, out,  map_fourbinsperbx4);
  
    std::map<std::string, std::vector<float>> map_fourbinsperbx5;
  map_fourbinsperbx5["Channel 33"] =filterHistForPlot(m_fourbinsperbx[32], 14256);
  map_fourbinsperbx5["Channel 34"] =filterHistForPlot(m_fourbinsperbx[33], 14256);
  map_fourbinsperbx5["Channel 35"] =filterHistForPlot(m_fourbinsperbx[34], 14256);
  map_fourbinsperbx5["Channel 36"] =filterHistForPlot(m_fourbinsperbx[35], 14256);
  map_fourbinsperbx5["Channel 37"] =filterHistForPlot(m_fourbinsperbx[36], 14256);
  map_fourbinsperbx5["Channel 38"] =filterHistForPlot(m_fourbinsperbx[37], 14256);
  map_fourbinsperbx5["Channel 39"] =filterHistForPlot(m_fourbinsperbx[38], 14256);
  map_fourbinsperbx5["Channel 40"] =filterHistForPlot(m_fourbinsperbx[39], 14256);
 
  m_chart_fourbinsperbx5->writeDataForQuery( cgi, out,  map_fourbinsperbx5);
   m_chart_fourbinsperbx5->newDataReady();
   
    std::map<std::string, std::vector<float>> map_fourbinsperbx6;
  map_fourbinsperbx6["Channel 41"] =filterHistForPlot(m_fourbinsperbx[40], 14256);
  map_fourbinsperbx6["Channel 42"] =filterHistForPlot(m_fourbinsperbx[41], 14256);
  map_fourbinsperbx6["Channel 43"] =filterHistForPlot(m_fourbinsperbx[42], 14256);
  map_fourbinsperbx6["Channel 44"] =filterHistForPlot(m_fourbinsperbx[43], 14256);
  map_fourbinsperbx6["Channel 45"] =filterHistForPlot(m_fourbinsperbx[44], 14256);
  map_fourbinsperbx6["Channel 46"] =filterHistForPlot(m_fourbinsperbx[45], 14256);
  map_fourbinsperbx6["Channel 47"] =filterHistForPlot(m_fourbinsperbx[46], 14256);
  map_fourbinsperbx6["Channel 48"] =filterHistForPlot(m_fourbinsperbx[47], 14256);
  m_chart_fourbinsperbx6->newDataReady();
  m_chart_fourbinsperbx6->writeDataForQuery( cgi, out,  map_fourbinsperbx6);
}

template <typename T>
std::vector<float> bril::bcm1fprocessor::Application::filterHistForPlot(T in, int len)
{
  std::vector<float> ret;

  for (int i=0;i<len;i++)
  {
    if (!isinf(in[i])||!isnan(in[i]))
    {
      ret.push_back(in[i]);
    }
    else
    {
      ret.push_back(-1.0);
    }
  }
  return ret;
}





// Sets default values of application according to parameters in the 
// xml configuration file.  Listens for xdata::Event and reacts when it's of type
// "urn:xdaq-event:setDefaultValues"
void bril::bcm1fprocessor::Application::actionPerformed(xdata::Event& e){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Received xdata event " << e.type());
  std::stringstream msg;
  if( e.type() == "urn:xdaq-event:setDefaultValues" ){
    size_t nsources = m_datasources.elements();
    try{
      for(size_t i=0;i<nsources;++i){
      xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_datasources.elementAt(i));
      xdata::String databus;
      xdata::String topicsStr;
      if(p){
        databus = p->getProperty("bus");         
        topicsStr = p->getProperty("topics");
        std::set<std::string> topics = toolbox::parseTokenSet(topicsStr.value_,",");
        m_in_busTotopics.insert(std::make_pair(databus.value_,topics));
      }
      }
      subscribeall();
      size_t ntopics = m_outputtopics.elements();
      for(size_t i=0;i<ntopics;++i){
      xdata::Properties* p = dynamic_cast<xdata::Properties*>(m_outputtopics.elementAt(i));
      if(p){
        xdata::String topicname = p->getProperty("topic");
        xdata::String outputbusStr = p->getProperty("buses");
        std::set<std::string> outputbuses = toolbox::parseTokenSet(outputbusStr.value_,",");
        for(std::set<std::string>::iterator it=outputbuses.begin(); it!=outputbuses.end(); ++it){
          m_out_topicTobuses.insert(std::make_pair(topicname.value_,*it));
        }
        m_topicoutqueues.insert(std::make_pair(topicname.value_,new toolbox::squeue<toolbox::mem::Reference*>));
        m_unreadybuses.insert(outputbuses.begin(),outputbuses.end());
      }
      }
      for(std::set<std::string>::iterator it=m_unreadybuses.begin(); it!=m_unreadybuses.end(); ++it){
      this->getEventingBus(*it).addActionListener(this);
      //if(this->getEventingBus(*it).canPublish()) continue;
      }

      // unsigned int myNRhus = (unsigned int) m_nRHUs;
      m_collectchannel = interface::bril::BCM1F_NRU*m_nRHUs;
      std::stringstream ss;
      ss.str("");
      ss << "Number of RHUs is " << m_nRHUs << ", number of channels to collect is " << m_collectchannel
      //<< ", sigmaVis is " << m_sigma_vis;
       << ", sigmaVis (sCVD) is " << m_sigma_vis_scvd
       << ", sigmaVis (pCVD) is " << m_sigma_vis_pcvd
       << ", sigmaVis (Si) is " << m_sigma_vis_si;
      LOG4CPLUS_INFO(getApplicationLogger(),ss.str());
      
      if (!(m_best_sensor_type.toString() == "sCVD" || m_best_sensor_type.toString() == "pCVD" || m_best_sensor_type.toString() == "Si"))
      {
        //std::string msg(" ");
        std::stringstream msg;
        msg.str("");
        msg << "Wrong sensor type, type is not one of sCVD, pCVD, Si but is instead " << m_best_sensor_type.toString();
        //LOG4CPLUS_FATAL(getApplicationLogger(),msg+stdformat_exception_history(e));
        LOG4CPLUS_FATAL(getApplicationLogger(),msg.str());
        //XCEPT_DECLARE(bril::bcm1fprocessor::exception::Exception,e,msg.str());
        XCEPT_RAISE(bril::bcm1fprocessor::exception::Exception,msg.str());
        //this->notifyQualified("fatal",e);
      }
      else
      {
        std::stringstream msg;
        msg.str("");
        msg << "Best sensor type is " << m_best_sensor_type.toString();
        LOG4CPLUS_INFO(getApplicationLogger(),msg.str());
      }
      
      size_t nchannels = m_activeChannelsLumi.elements();
      LOG4CPLUS_DEBUG(getApplicationLogger(),"Now looping to set active channels for lumi and bkgd");
      for(size_t i = 0; i < nchannels; i++)
      {

        std::stringstream ss;
        ss<<"Channel " << i ;
        
        xdata::Boolean* boolLumi = dynamic_cast<xdata::Boolean*>(m_activeChannelsLumi.elementAt(i));
        xdata::Boolean* boolBkgd = dynamic_cast<xdata::Boolean*>(m_activeChannelsBkgd.elementAt(i));
        
        m_useChannelForLumi[i] = boolLumi->value_;
        m_useChannelForBkgd[i] = boolBkgd->value_;

        m_useChannelForLumi[i] ? (ss << "  Lumi: True") : (ss << "  Lumi: False");
        m_useChannelForBkgd[i] ? (ss << "  Bkgd: True") : (ss << "  Bkgd: False");


        xdata::Float* channelCorrection1 = dynamic_cast<xdata::Float*>(m_channelAcceptances.elementAt(i));
        xdata::Float* channelCorrection2 = dynamic_cast<xdata::Float*>(m_channelEfficiencies.elementAt(i));
        xdata::Float* channelCorrection3 = dynamic_cast<xdata::Float*>(m_channelCorrections.elementAt(i));

        m_channelScaleFactor[i] = channelCorrection1->value_ * channelCorrection2->value_ * channelCorrection3->value_;

        ss << "  Acceptance: " << channelCorrection1->value_;
        ss << "  Efficiency: " << channelCorrection2->value_;
        ss << "  Other correction factor: " << channelCorrection3->value_;

        ss << "  Overall correction: " << m_channelScaleFactor[i];

        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        
      }
      LOG4CPLUS_DEBUG(getApplicationLogger(),"***DONE*** Now looping to set active channels for lumi and bkgd: ***DONE***");

      // parse albedo model
      std::stringstream albedo_model_pcvd_ss(m_albedo_model_pcvd_str.value_);
      while(albedo_model_pcvd_ss.good())
      {
        std::string substr;
        getline( albedo_model_pcvd_ss, substr, ',' );
        m_albedo_model_pcvd.push_back( stof(substr) );
      }
      if (m_albedo_model_pcvd.size() > 3564)
      {
        LOG4CPLUS_WARN(getApplicationLogger(),"pCVD albedo model longer than one orbit, entries after 3564 will be ignored");
      }
      std::stringstream ss2;
      ss2 << "pCVD albedo model: ";
      for(int i=0;i<m_albedo_model_pcvd.size();i++)
        ss2<<m_albedo_model_pcvd[i]<<",";
      LOG4CPLUS_DEBUG(getApplicationLogger(),ss2.str());

      std::stringstream albedo_model_si_ss(m_albedo_model_si_str.value_);
      while(albedo_model_si_ss.good())
      {
        std::string substr;
        getline( albedo_model_si_ss, substr, ',' );
        m_albedo_model_si.push_back( stof(substr) );
      }
      if (m_albedo_model_si.size() > 3564)
      {
        LOG4CPLUS_WARN(getApplicationLogger(),"Si albedo model longer than one orbit, entries after 3564 will be ignored");
      }
      std::stringstream ss3;
      ss3 << "Si albedo model: ";
      for(int i=0;i<m_albedo_model_si.size();i++)
        ss3<<m_albedo_model_si[i]<<",";
      LOG4CPLUS_DEBUG(getApplicationLogger(),ss3.str());



      setupCharts();


    }catch(xdata::exception::Exception& e){
      msg<<"Failed to parse application property";
      LOG4CPLUS_ERROR(getApplicationLogger(), msg);
      XCEPT_RETHROW(bril::bcm1fprocessor::exception::Exception, msg.str(), e);
    }      
  }
}



// When buses are ready to publish, start the publishing workloop and 
// timer to check for stale cache. Listens for toolbox::Event
void  bril::bcm1fprocessor::Application::actionPerformed(toolbox::Event& e){
  if(e.type() == "eventing::api::BusReadyToPublish"){
    std::string busname = (static_cast<eventing::api::Bus*>(e.originator()))->getBusName();
    std::stringstream msg;
    msg<< "event Bus '" << busname << "' is ready to publish";
    m_unreadybuses.erase(busname);
    if(m_unreadybuses.size()!=0) return; //wait until all buses are ready
    try{    
      toolbox::task::ActionSignature* as_publishing = toolbox::task::bind(this,&bril::bcm1fprocessor::Application::publishing,"publishing");
      m_publishing->activate();
      m_publishing->submit(as_publishing);
    }catch(toolbox::task::exception::Exception& e){
      msg<<"Failed to start publishing workloop "<<stdformat_exception_history(e);
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      XCEPT_RETHROW(bril::bcm1fprocessor::exception::Exception,msg.str(),e);      
    }    
    try{
      std::string appuuid = m_appDescriptor->getUUID().toString();
      toolbox::TimeInterval checkinterval(10,0);// check every 10 seconds
      std::string timername("stalecachecheck_timer");
      toolbox::task::Timer *timer = toolbox::task::getTimerFactory()->createTimer(timername+"_"+appuuid);
      toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
      timer->scheduleAtFixedRate(start, this, checkinterval, 0, timername);
    }catch(toolbox::exception::Exception& e){
      std::string msg("failed to start timer ");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e) );
      XCEPT_RETHROW(bril::bcm1fprocessor::exception::Exception,msg,e);
    }
    try{    
      toolbox::task::ActionSignature* as_albedoing = toolbox::task::bind(this,&bril::bcm1fprocessor::Application::albedo_calculation,"publishing");
      m_albedo_calculation->activate();
      m_albedo_calculation->submit(as_albedoing);
    }catch(toolbox::task::exception::Exception& e){
      msg<<"Failed to start albedo calculation workloop "<<stdformat_exception_history(e);
      LOG4CPLUS_ERROR(getApplicationLogger(),msg.str());
      XCEPT_RETHROW(bril::bcm1fprocessor::exception::Exception,msg.str(),e);      
    } 
  }
}

// subscribe to all input buses
void bril::bcm1fprocessor::Application::subscribeall(){
  for(std::map<std::string, std::set<std::string> >::iterator bit=m_in_busTotopics.begin(); bit!=m_in_busTotopics.end(); ++bit ){
    std::string busname = bit->first; 
    for(std::set<std::string>::iterator topicit=bit->second.begin(); topicit!= bit->second.end(); ++topicit){       
      if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(),"subscribing "+busname+":"+*topicit); }
      try{
      this->getEventingBus(busname).subscribe(*topicit);
      }catch(eventing::api::exception::Exception& e){
      LOG4CPLUS_ERROR(getApplicationLogger(),"failed to subscribe, remove topic "+stdformat_exception_history(e));
      m_in_busTotopics[busname].erase(*topicit);
      }
    }
  }
}

// Listens for reception of data on the eventing.  
// If full statistics collected, call do_process on the already-accumulated nibbles.
void bril::bcm1fprocessor::Application::onMessage(toolbox::mem::Reference * ref, xdata::Properties & plist) throw (b2in::nub::exception::Exception){
  std::string action = plist.getProperty("urn:b2in-eventing:action");
  if (action == "notify"){
    std::string topic = plist.getProperty("urn:b2in-eventing:topic");
    //LOG4CPLUS_DEBUG(getApplicationLogger(), "Received data from "+topic);
    //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), "Received data from "+topic); }
    std::string v = plist.getProperty("DATA_VERSION");
    if(v.empty()){
      std::string msg("Received data message without version header, do nothing");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg);
      if(ref!=0){
      ref->release();
      ref=0;
      }
      return;
    } 
    
    std::stringstream ss;
    ss<<"Received data from "+topic;
    LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());


    interface::bril::DatumHead* inheader = (interface::bril::DatumHead*)(ref->getDataLocation());


    if( topic == interface::bril::bcm1fhistT::topicname() )
      {
      std::stringstream ss;
      ss<<"Received data from "+topic+", retrieving source histos channelid " 
        << inheader->getChannelID()<< " in (lastheader) nibble " 
        << m_lastheader.nbnum << " (this histo has nibble)" << inheader->nbnum;
      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

      retrieveSourceHistos(ref, inheader);
      }
    else if(topic == interface::bril::beamT::topicname())
      {
      std::stringstream ss;
      ss<<"Received data from "+topic+", making bunch masks";
      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

      makeBunchMasks(inheader);
      }
    else if(topic == interface::bril::NB4T::topicname())
      {
      std::stringstream ss;
      ss<<"Received data from "+topic+", this is 4NB clock tick";
      //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());

      }
  }
  if(ref!=0){
    ref->release();
    ref=0;
  }
}

// When BCM1F data received on eventing, check and store it
void bril::bcm1fprocessor::Application::retrieveSourceHistos(toolbox::mem::Reference * ref, interface::bril::DatumHead * inheader)
{

  unsigned int channelId = inheader->getChannelID();
  unsigned int fillnum = inheader->fillnum;
  unsigned int runnum = inheader->runnum;
  unsigned int lsnum = inheader->lsnum;
  unsigned int nbnum = inheader->nbnum;

  // if new nibble, reset nChannelsThisNibble
  if (nbnum != m_lastheader.nbnum)
    {
      m_nChannelsThisNibble = 0;
    }

    std::stringstream ss;
    ss<<"Received BCM1F hist data #" << m_nChannelsThisNibble 
      << ", run: "<<runnum<<" ls: "<<lsnum<<" nb: "<<nbnum<< " channelId: " << channelId;
    LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());



  std::map<unsigned int,InData*>::iterator channelit = m_bcm1fhistcache.find(channelId);
  // Now, irrespective of whether or not the last block was processed, 
  // check if the current histogram's channelid is already in the list.
  // If not, insert this channelid and the histogram's contents
  if(channelit==m_bcm1fhistcache.end())
    {
      toolbox::mem::Reference*  myref =  m_poolFactory->getFrame(m_memPool,ref->getDataSize());
      memcpy(myref->getDataLocation(),ref->getDataLocation(),ref->getDataSize());
      InData* d = new InData(1,myref);
      m_bcm1fhistcache.insert(std::make_pair(channelId,d));
      m_lastheader = *inheader;
      m_numberOfNibbles = 1;
      m_nChannelsThisNibble++;
    }
  // If it's already there, accumulate this histogram's contents, too
  else
    {
      interface::bril::bcm1fhistT* indata = (interface::bril::bcm1fhistT*)(ref->getDataLocation());
      hist_accumulateInChannel(channelit,indata);
      m_lastheader = *inheader;
      m_nChannelsThisNibble++;
    }
  // MOVE THIS INSIDE THE BCM1FHIST TOPIC CHECK, not relevant for other topics
  toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
  unsigned int nowsec = t.sec();
  m_lastreceivedsec = nowsec;



  // Check if we have collected all channels for all nibbles
  // If so, then process the lot -- starting at last_header
  
  // I DON'T THINK CHECK_CHANNELSTATISTICS IS DOING WHAT WE WANT -- returns true if one histogram each from all channels have been collected, no matter how many nibbles' worth!  So starts constantly returning true after the first nibble, I think
  if( check_channelstatistics( m_collectchannel ) )
    {
      LOG4CPLUS_DEBUG(getApplicationLogger(), "All channels collected for this nibble");

      if( nbnum % 4 == 0) // 1
      {

        //if( check_nbstatistics( m_collectnb ) )
        if( check_nbstatistics_simple( m_collectnb ) )
          {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "Full statistics collected, conclude the previous processing ");
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), "Full statistics collected, conclude the previous processing "); }
            do_process(m_lastheader);
            std::stringstream ss;
            ss.str("");
            ss << "processed last header: run " << m_lastheader.runnum 
             << " ls " << m_lastheader.lsnum 
             << " nb " << m_lastheader.nbnum
             << " (right now in run " << runnum 
             << " ls " << lsnum 
             << " nb " << nbnum 
             << "). BG1: " << m_BG1_plus.value_
             << " BG2: " << m_BG2_minus.value_
             << " sCVD Lumi: " << m_lumi_avg[0] 
             << " sCVD Lumi raw: " << m_lumi_avg_raw[0]
             << " pCVD Lumi: " << m_lumi_avg[1] 
             << " pCVD Lumi raw: " << m_lumi_avg_raw[1]
             << " Si Lumi: " << m_lumi_avg[2] 
             << " Si Lumi raw: " << m_lumi_avg_raw[2]
             << " Luminometer of choice is " << m_best_sensor_type.toString();
            //<< " CountAlbedoHigherThanBG: " << m_countAlbedoHigherThanBackground;
            //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
            LOG4CPLUS_INFO(getApplicationLogger(), ss.str());
            m_numberOfNibbles = 0;
            m_nChannelsThisNibble = 0;
            toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
            // m_mon_timestamp.value_ = t;
            m_mon_timestamp = t;
            m_mon_fill = fillnum;
            m_mon_run = runnum;
            m_mon_ls = lsnum;
            m_mon_nb = nbnum;
            m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this );
            clear_cache();
          }
        // else if not enough nibbles, check if it's a run/ls boundary
        // and process anyway if so
        // (also checks if run and ls numbers are valid)
        else if(( m_lastheader.runnum !=0 && m_lastheader.lsnum !=0 )
              &&( lsnum != m_lastheader.lsnum ))
          {
            //ss.str("");
            //ss<<"current statistics channel "<<m_bcm1fhistcache.size()<<std::endl;
            //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());

            ss.str("");
            ss<<"Detected fewer nibbles than expected on run/ls boundary (" << m_numberOfNibbles 
            << " nibbles), process and scale by nibbles received: run "<<m_lastheader.runnum
            <<" ls "<<m_lastheader.lsnum << " nibble " << m_lastheader.nbnum << std::endl;
            if (m_numberOfNibbles > 0)
            {
              do_process(m_lastheader);
              ss << "processed last header: run " << m_lastheader.runnum 
                 << " ls " << m_lastheader.lsnum 
                 << " nb " << m_lastheader.nbnum
                 << " (right now in run " << runnum 
                 << " ls " << lsnum 
                 << " nb " << nbnum 
                 << "). BG1: " << m_BG1_plus.toString() 
                 << " BG2: " << m_BG2_minus.toString() 
                 << " sCVD Lumi: " << m_lumi_avg[0] 
                 << " sCVD Lumi raw: " << m_lumi_avg_raw[0]
                 << " pCVD Lumi: " << m_lumi_avg[1] 
                 << " pCVD Lumi raw: " << m_lumi_avg_raw[1]
                 << " Si Lumi: " << m_lumi_avg[2] 
                 << " Si Lumi raw: " << m_lumi_avg_raw[2]
                 << " Luminometer of choice is " << m_best_sensor_type.toString();
            }
            else
            {
              ss << "(Not) dividing by 0 collected nibbles, instead ignoring and clearing cache!";
            }
            // if (m_alignedNB4)
            //       ss << " m_alignedNB4 is TRUE ";
            // else
            //       ss << " m_alignedNB4 is FALSE ";
            // m_nHistsDiscarded++;
            // ss << " m_nHistsDiscarded is " << m_nHistsDiscarded;
            if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
            m_numberOfNibbles = 0;
            m_nChannelsThisNibble = 0;
            toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
            // m_mon_timestamp.value_ = t;
            m_mon_timestamp = t;
            m_mon_fill = fillnum;
            m_mon_run = runnum;
            m_mon_ls = lsnum;
            m_mon_nb = nbnum;
            m_mon_infospace->fireItemGroupChanged( m_mon_varlist, this );
            clear_cache();
          }
      }
    }

  if ( !m_alignedNB4)
    {
      if ( m_lastheader.nbnum % 4 == 0 )
      {
        m_alignedNB4 = true;
      }
    }
  
}

// When BEAM data received on eventing, use it to make bunch 
// masks for collision products and both beam backgrounds
void bril::bcm1fprocessor::Application::makeBunchMasks(interface::bril::DatumHead * inheader)
{
  int maxnbx = 3564;
  // DEFINE_COMPOUND_TOPIC(beam,"status:str28:1 egev:float:1 targetegev:uint16:1 
  // bxconfig1:bool:3564 bxconfig2:bool:3564 intensity1:float:1 intensity2:float:1 
  // bxintensity1:float:3564 bxintensity2:float:3564 machinemode:str20:1 fillscheme:str64:1"
  float _beamItot1;
  float _beamItot2;
  float _bx1[maxnbx];
  float _bx2[maxnbx];
  bool _con1[maxnbx];
  bool _con2[maxnbx];
  int lastCollBunch=-300;
  char _machinemode[50];
  char _beammode[50];
  interface::bril::CompoundDataStreamer tc(interface::bril::beamT::payloaddict());
  tc.extract_field(&_beamItot1, "intensity1",  inheader->payloadanchor);
  tc.extract_field(&_beamItot2, "intensity2",  inheader->payloadanchor);
  tc.extract_field(_bx1, "bxintensity1",  inheader->payloadanchor);
  tc.extract_field(_bx2, "bxintensity2",  inheader->payloadanchor);
  tc.extract_field(_con1, "bxconfig1",  inheader->payloadanchor);
  tc.extract_field(_con2, "bxconfig2",  inheader->payloadanchor);
  tc.extract_field(_machinemode, "machinemode",  inheader->payloadanchor);
  tc.extract_field(_beammode, "status",  inheader->payloadanchor);
  m_iTot1 = _beamItot1;
  m_iTot2 = _beamItot2;
  std::string machinemode=std::string(_machinemode);
  std::string beammode=std::string(_beammode);
  m_beammode = beammode;
  bool collidable=false;
  bool nobeam = false;
  // use collision mask for lumi
  if ( beammode=="FLAT TOP" ||
       beammode=="SQUEEZE" || 
       beammode=="ADJUST" ||
       beammode=="STABLE BEAMS" ||
       beammode=="UNSTABLE BEAMS" ||
       beammode=="BEAM DUMP WARNING"
       )
    { 
      collidable=true;
    } 
  // don't use bunch masks for background, no beam with these modes
  if ( beammode == "SETUP" ||
       beammode == "BEAM DUMP" ||
       beammode == "RAMP DOWN" ||
       beammode == "NO BEAM" ||
       beammode == "ABORT" ||
       beammode == "RECOVERY" ||
       beammode == "CYCLING"
       )
    {
      nobeam = true;
    }

  std::stringstream ss;
  ss << "Mode: " << machinemode << " : " << beammode << std::endl;
  if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

  std::stringstream col;
  std::stringstream bk1;
  std::stringstream bk2;
  bk1<<"BKGD1 BX: ";
  bk2<<"BKGD2 BX: ";
  col<<"COLL BX: ";

  int nbk1 = 0;
  int nbk2 = 0;
  int ncoll = 0;

  for (int bx = 0; bx < maxnbx; bx++)
    {
      m_iBunch1[bx] = _bx1[bx];
      m_iBunch2[bx] = _bx2[bx];
      // if ((bx % 100) == 0 && (bx < 1000))
      //       {      
      //          ss.str("");
      //         ss << "Bunch " << bx << " beam 1: " << m_iBunch1[bx] << "  beam 2: " << m_iBunch2[bx] << std::endl;
      //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //       }
      m_collmask[bx] = m_bkgd1mask[bx] = m_bkgd2mask[bx] = false;
      int lastColl = bx - lastCollBunch;
      //      if((_con1[bx] &&(!collidable ||lastColl>30)) || nobeam)
      if(_con1[bx] &&(!collidable ||lastColl>30))
      {
        //ss.str("");
        //ss << "(1) Setting beam 1 mask to TRUE for bunch " << bx << std::endl;
        //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        m_bkgd1mask[bx] = true;
        nbk1++;
        bk1 << "," << bx;
      }
      //      if((_con2[bx] &&(!collidable || lastColl>30)) || nobeam)
      if(_con2[bx] &&(!collidable || lastColl>30))
      {
        //ss.str("");
        //ss << "(1) Setting beam 2 mask to TRUE for bunch " << bx << std::endl;
        //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        m_bkgd2mask[bx] = true;
        nbk2++;
        bk2 << "," << bx;
      }
      if(_con1[bx] && _con2[bx])
      {
        m_collmask[bx] = true;
        lastCollBunch = bx;
        ncoll++;
         col << "," << bx;
      }            
    }
  if (nbk1 < 200)
    ss << bk1.str() <<std::endl;
  if (nbk2 < 200)
    ss << bk2.str() <<std::endl;
  if (ncoll < 200)
    ss << col.str() <<std::endl;

}


// Process the accumulated nibbles:
// Calls functions that create and queue for publishing the following products
//  * agghists
//  * background numbers
//  * luminosity numbers
void bril::bcm1fprocessor::Application::do_process(interface::bril::DatumHead& inheader)
{
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Now entered do_process: VERY BEGINNING");

  // if(inheader.runnum==0) 
  //   {
  //     return;
  //   }

  std::stringstream ss;
  ss.str("");
  ss << "do_process run " << inheader.runnum 
     << " ls " << inheader.lsnum
     << " nb " <<inheader.nbnum
     << " channel " << inheader.getChannelID();
  LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());


  // 1. first make agghists, using same loop calculate total rates
  LOG4CPLUS_DEBUG(getApplicationLogger(), "About to makeAggHistsAndTotalRates");
  makeAggHistsAndTotalRates(inheader);

 
  // 3. channel mask needed for background and luminosity numbers
  LOG4CPLUS_DEBUG(getApplicationLogger(), "About to makeBkgNumbers");
  makeBkgNumbers(inheader);

  LOG4CPLUS_DEBUG(getApplicationLogger(), "About to makeLumiNumbers");
  makeLumiNumbers(inheader);
  //makebxhistnumbers(inheader);

}

// Loops through RHU histograms, makes the agghists for each channel
// and at the same time creates total rates for each channel for
// (monitoring purposes and) making the channel mask. 
// Queues the agghists for publishing
void bril::bcm1fprocessor::Application::makeAggHistsAndTotalRates(interface::bril::DatumHead& inheader)
{

  // first for aggregated histogram

  toolbox::mem::Reference* bcm1fagghist = 0;
  //  toolbox::mem::Reference* bcm1fagghist2 = 0; // for lumi bin only
  toolbox::mem::Reference* bxconfig = 0;

  //  memset (m_totalChannelRate,0.,sizeof(m_totalChannelRate));
  //std::fill(begin(m_totalChannelRate), end(m_totalChannelRate), 0.);
  //  std::fill(m_totalChannelRate.begin(), m_totalChannelRate.end(), 0.);
  m_totalChannelRate.resize(s_nAllChannels, 0.);


  try
    {
      LOG4CPLUS_DEBUG(getApplicationLogger(), "entered try loop agghist");

      // PICKING OUT THE AGGHISTS FROM BPTX 
      // AND PUBLISHING THEM IN THEIR OWN TOPIC
      // ***CHANNELS HARDCODED*** 
      // BPTX RHU Channel listing from Hans' e-mail of Apr. 20, 2016
      // 0    B1
      // 1    B2
      // 2    OR
      // 3    AND
      // 4    B1 AND not(B2)
      // 5    not(B1) AND B2
      // 6    BeamGas B1
      // 7    BeamGas B2 

      bool beam1bptx[3564];
      bool beam2bptx[3564];

      memset(beam1bptx,false,sizeof(beam1bptx));
      memset(beam2bptx,false,sizeof(beam2bptx));

      // MAKE BPTX OUTPUT PRODUCT
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Making BPTX output product");
      // FOR COMPOUND DATA TYPE
      std::string pdict = interface::bril::bxconfigT::payloaddict();
      xdata::Properties plist;
      plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
      plist.setProperty("PAYLOAD_DICT",pdict);
      //size_t totalsize = interface::bril::bxconfigT::maxsize();
      // END COMPOUND-SPECIFIC STUFF

      bxconfig = m_poolFactory->getFrame(m_memPool,interface::bril::bxconfigT::maxsize());
      bxconfig->setDataSize(interface::bril::bxconfigT::maxsize());
      interface::bril::bxconfigT* bxconfighist = (interface::bril::bxconfigT*)(bxconfig->getDataLocation());

      // fill, run, ls, ln, sec, msec
      bxconfighist->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
      bxconfighist->setFrequency(4);

      // sourceid, algo, channel, payloadtype 
      bxconfighist->setResource(interface::bril::DataSource::BCM1F,NULL,NULL,interface::bril::StorageType::COMPOUND);
      bxconfighist->setTotalsize(interface::bril::bxconfigT::maxsize());
      // MORE COMPOUND-SPECIFIC STUFF
      interface::bril::CompoundDataStreamer tc(pdict);
      // END MORE COMPOUND-SPECIFIC STUFF


      // MAKE AGGHIST OUTPUT PRODUCTS AND FILL THEM AND BPTX OUTPUT PRODUCT (loop over all channels)
      for(InDataCache::iterator channelit=m_bcm1fhistcache.begin(); channelit!=m_bcm1fhistcache.end(); ++channelit)
      {
        LOG4CPLUS_DEBUG(getApplicationLogger(), "Accessing this channel of histcache");
        unsigned int channelid = channelit->first;
        std::stringstream ss;
        ss.str("");
        ss << "Channelid is " << channelid;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss);



        unsigned int channelIndex = channelid - 1;
        bcm1fagghist = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fagghistT::maxsize());
        bcm1fagghist->setDataSize(interface::bril::bcm1fagghistT::maxsize());
        interface::bril::bcm1fagghistT* agghist = (interface::bril::bcm1fagghistT*)(bcm1fagghist->getDataLocation());
        
        // fill, run, ls, ln, sec, msec
        agghist->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
        agghist->setFrequency(4);
        
        // sourceid, algo, channel, payloadtype 
        agghist->setResource(interface::bril::DataSource::BCM1F,interface::bril::BCM1FAggAlgos::SUM,channelid,interface::bril::StorageType::UINT16); 
        agghist->setTotalsize(interface::bril::bcm1fagghistT::maxsize());
        
        //initialize array
        for(size_t bin = 0; bin < interface::bril::bcm1fagghistT::n(); ++bin)
          {
            agghist->payload()[bin] = 0.;
          }
        
        toolbox::mem::Reference* histref = channelit->second->dataref;
        interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*)(histref->getDataLocation());
        
        // TODO: DO REAL PROCESSING HERE
        // each output bin should sum the corresponding four input bins
        float bx4sum = 0.;
        size_t nbins = histdataptr->n();

        float nibbleNorm = (4./m_numberOfNibbles);
        
        //         if (channelid > 48)
        //           {
        // for (unsigned int bunch = 0; bunch < 3564; bunch++)
        //   {
        //     if (m_bkgd1mask[bunch] == true)
        //       {
        //      std::stringstream ss;
        //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " before fill) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
        //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //       }
        //     if (m_bkgd2mask[bunch] == true)
        //       {
        //      std::stringstream ss;
        //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " before fill) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
        //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //       }
        //   }
        //           }

        float rateTemp = 0.;
        float bx0bin0 = 0.;

        for(size_t rhubin = 0; rhubin < nbins; ++rhubin )
          {
            // Mask out test pulse bins, but only for channels 1-48 -- although this is in the abort gap anyway
            // Now also need to mask out test pulses for LUT RHU
            //if (!(rhubin >= 14136 && rhubin < 14196) || (channelid >= 49 && channelid <= 56 ))
            //if (!(rhubin >= 14136 && rhubin < 14220) || (channelid >= 49 && channelid <= 56 )) // Hans changed some timing of test pulse, don't know exactly where it is now, other than it comes later
            //if (!(rhubin >= 14136 && rhubin < 14230) || (channelid >= 49 && channelid <= 56 )) // Again 26May16, for some reason??!
          if (!(rhubin >= 14136 && rhubin < 14232) || (channelid >= 49 && channelid <= 56 )) // 03May17, for making bins uniform size
            {        
              bx4sum += histdataptr->payload()[rhubin];
              //m_totalChannelRate[channelIndex] += nibbleNorm*histdataptr->payload()[rhubin];
              //m_totalChannelRate.elementAt(channelIndex) += nibbleNorm*histdataptr->payload()[rhubin];
              rateTemp += nibbleNorm*histdataptr->payload()[rhubin];

              if ((rhubin+1) % 4 == 0) // e.g. we want bins 0,1,2,3 together, so we sum after bin 3, when nbins+1 = 4 and 4%4=0.  
                {
                  unsigned int bxidx = rhubin/4;          
                      if (channelid == 48+0 && bx4sum > 4) // B1 is channel 0
                    {
                       beam1bptx[bxidx] = true;
                    // if (bxidx < 100)
                    //   std::cout << "BPTX 1 filled bunch at bx " << bxidx << std::endl;
                    }
                  if (channelid == 48+1 && bx4sum > 4) // B2 is channel 1
                    {
                       beam2bptx[bxidx] = true;
                    // if (bxidx < 100)
                    //   std::cout << "BPTX 2 filled bunch at bx " << bxidx << std::endl;
                    }
                }


              if (rhubin == 0) // first RHU bin
              {
                    bx0bin0 = bx4sum;
                    bx4sum = 0.;
              }
              else if  (rhubin == (nbins-1))  // last RHU bin
              {
                    bx4sum += bx0bin0;  //add first RHU bin to last BX (cosmetics)
                    unsigned int bxidx = rhubin/4;          
                  agghist->payload()[bxidx] += nibbleNorm*bx4sum; //accumulate 4 nibbles
                  bx4sum = 0.;
                  bx0bin0 = 0.; //probably not necessary

              }
              else if ((rhubin) % 4 == 0) // e.g. we want bins 0,1,2,3 together, so we sum after bin 3, when nbins+1 = 4 and 4%4=0.  
                {
                  unsigned int bxidx = (rhubin/4) - 1; // summing ends already inthe next BX, so we subtract 1, the case of 0/4-1 is protect from in if above
                  agghist->payload()[bxidx] += nibbleNorm*bx4sum; //accumulate 4 nibbles
                  bx4sum = 0.;
                }
              // if (rhubin % 4 == 2) // TAKE BIN 2 FOR COLLISIONS! (out of 0,1,2,3) 
              //   {
              //     unsigned int bxidx = rhubin/4;          
              //     //agghist->payload()[bxidx] = histdataptr->payload()[rhubin]; //accumulate 4 nibbles
              //     //agghist->payload()[bxidx] += bx4sum; //accumulate 4 nibbles
              //     if (channelid == 48+2 && bx4sum > 4)
              //       {
              //         beam1bptx[bxidx] = true;
              //       }
              //     if (channelid == 48+3 && bx4sum > 4)
              //       {
              //         beam2bptx[bxidx] = true;
              //       }
              //     bx4sum = 0.;
              //   }
            }
          }
        //((xdata::Float*) m_totalChannelRate.elementAt(channelIndex)).setValue(rateTemp);
        // xdata::Float* channelRateTemp = (xdata::Float*) m_totalChannelRate.elementAt(channelIndex);
        // channelRateTemp->setValue(rateTemp);
        //m_totalChannelRate.elementAt(channelIndex) = rateTemp;
        m_totalChannelRate[channelIndex] = rateTemp;

        LOG4CPLUS_DEBUG(getApplicationLogger(), "Pushing this channel's agghists to output queue");

        //m_topicoutqueues[interface::bril::bcm1fagghistT::topicname()]->push(bcm1fagghist);
        //if (m_sensorConnected[channelIndex] & channelIndex < 48) // normal running
        if (m_isChannelValid[channelIndex] & channelIndex < 48) 
        //if(channelIndex < 48) // VdM only
            {
            m_topicoutqueues[interface::bril::bcm1fagghistT::topicname()]->push(bcm1fagghist);
          }
        else if(bcm1fagghist)
          {
            bcm1fagghist->release();
            bcm1fagghist= 0;
          }


        //         if (channelid > 48)
        //           {
        // for (unsigned int bunch = 0; bunch < 3564; bunch++)
        //   {
        //     if (m_bkgd1mask[bunch] == true)
        //       {
        //      std::stringstream ss;
        //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " after fill) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
        //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //       }
        //     if (m_bkgd2mask[bunch] == true)
        //       {
        //      std::stringstream ss;
        //         ss << "(makeAgghistsAndTotalRates loop chid " << channelid << " after fill) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
        //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
        //       }
        //   }
        //           }


      }

      // for (unsigned int bunch = 0; bunch < 3564; bunch++)
      //   {
      //     if (m_bkgd1mask[bunch] == true)
      //       {
      //          std::stringstream ss;
      //         ss << "(makeAgghistsAndTotalRates 3) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
      //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //       }
      //     if (m_bkgd2mask[bunch] == true)
      //       {
      //          std::stringstream ss;
      //         ss << "(makeAgghistsAndTotalRates 3) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
      //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //       }
      //   }

      LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing agghists");

      // PUSH BPTX PRODUCT, TOO

      // DEBUUUUUUG
      // std::cout << "BPTX beam 1 bunch pattern before bunch 100" << std::endl;
      // for (unsigned int bunchindex = 0; bunchindex < 100; bunchindex++)
      //       {
      //         std::cout << beam1bptx[bunchindex] << ", ";
      //         if ((bunchindex+1) % 20 == 0)
      //           {
      //             std::cout << std::endl;
      //           }
      //       }
      // std::cout << "BPTX beam 2 bunch pattern before bunch 100" << std::endl;
      // for (unsigned int bunchindex = 0; bunchindex < 100; bunchindex++)
      //       {
      //         std::cout << beam2bptx[bunchindex] << ", ";
      //         if ((bunchindex+1) % 20 == 0)
      //           {
      //             std::cout << std::endl;
      //           }
      //       }

      // MORE COMPOUND-SPECIFIC STUFF
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Put beam1 into topic");
      tc.insert_field( bxconfighist->payloadanchor, "beam1", beam1bptx);
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Put beam2 into topic");
      tc.insert_field( bxconfighist->payloadanchor, "beam2", beam2bptx);
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Put datasource into topic");
      const unsigned short mydatasource = interface::bril::bxconfig_bptx_rhu;
      tc.insert_field( bxconfighist->payloadanchor, "datasource", &mydatasource);
      // END MORE COMPOUND-SPECIFIC STUFF
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Push topic to output queue");
      m_topicoutqueues[interface::bril::bxconfigT::topicname()]->push(bxconfig);
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Done pushing bptx");

    }
  catch(xcept::Exception& e)
    {
      std::string msg("Failed to process data for bcm1fagghist / bxconfig");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
      if(bcm1fagghist)
      {
        bcm1fagghist->release();
        bcm1fagghist= 0;
      }
      if(bxconfig)
        {
          bxconfig->release();
          bxconfig= 0;
        }
    }

  // THIS IS THE STANDARD ONE
  std::stringstream ss;
  ss << "Total channel rates (excl. TP):" << std::flush;
  for (int channel = 0; channel < 48; channel++)
    {
      //ss << " " << m_totalChannelRate[channel];
      xdata::Float* xdatafloattemp = (xdata::Float*) m_totalChannelRate.elementAt(channel);
      //ss << " " << ((xdata::Float*) m_totalChannelRate.elementAt(channel)).toString();
      float floattemp = xdatafloattemp->value_;
      //ss << " " << xdatafloattemp->toString();
      ss << " " << floattemp;
    }
  //ss << std::endl;
  if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

  // ss.str("");
  // ss << "Total channel rates (excl. TP) normalized to typical rate:" << std::flush;
  // for (int channel = 0; channel < 48; channel++)
  //   {
  //  //     ss << " " << (m_totalChannelRate[channel]/m_typicalChannelCounts[channel]);
  //     ss << " " << (m_totalChannelRate.elementAt(channel)/m_typicalChannelCounts[channel]);
  //   }
  // if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

  // for (unsigned int bunch = 0; bunch < 3564; bunch++)
  //   {
  //     if (m_bkgd1mask[bunch] == true)
  //       {
  //      ss.str("");
  //         ss << "(makeAgghistsAndTotalRates 4) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
  //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
  //       }
  //     if (m_bkgd2mask[bunch] == true)
  //       {
  //      ss.str("");
  //         ss << "(makeAgghistsAndTotalRates 4) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
  //      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
  //       }
  //   }

}

// Calculates beam 1 and beam 2 background numbers, 
// queues them for publishing
void bril::bcm1fprocessor::Application::makeBkgNumbers(interface::bril::DatumHead& inheader)
{

  // now for BG numbers

  toolbox::mem::Reference* bcm1fbkg = 0;
  //  toolbox::mem::Reference* bcm1fbkghistos = 0;

  // Each BG number should (initial hack) sum the total of the + and - end histograms, respectively       
  LOG4CPLUS_DEBUG(getApplicationLogger(), "Still in do_process: bkgsums");  
  try
    {
      // FOR COMPOUND DATA TYPE
      std::string pdict = interface::bril::bcm1fbkgT::payloaddict();
      xdata::Properties plist;
      plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
      plist.setProperty("PAYLOAD_DICT",pdict);
      //size_t totalsize = interface::bril::bcm1fbkgT::maxsize();
      // END COMPOUND-SPECIFIC STUFF

      bcm1fbkg = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fbkgT::maxsize());
      bcm1fbkg->setDataSize(interface::bril::bcm1fbkgT::maxsize());
      interface::bril::bcm1fbkgT* bkghist = (interface::bril::bcm1fbkgT*)(bcm1fbkg->getDataLocation());

      // fill, run, ls, ln, sec, msec
      bkghist->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
      bkghist->setFrequency(4);

      // sourceid, algo, channel, payloadtype 
      bkghist->setResource(interface::bril::DataSource::BCM1F,NULL,NULL,interface::bril::StorageType::COMPOUND);
      bkghist->setTotalsize(interface::bril::bcm1fbkgT::maxsize());

      // // FOR COMPOUND DATA TYPE
      // std::string pdicthist = interface::bril::bcm1fbkghistosT::payloaddict();
      // xdata::Properties plisthist;
      // plisthist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
      // plisthist.setProperty("PAYLOAD_DICT",pdicthist);
      // //size_t totalsize = interface::bril::bcm1fbkghistosT::maxsize();
      // // END COMPOUND-SPECIFIC STUFF

      // bcm1fbkghistos = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fbkghistosT::maxsize());
      // bcm1fbkghistos->setDataSize(interface::bril::bcm1fbkghistosT::maxsize());
      // interface::bril::bcm1fbkghistosT* bkgbxhist = (interface::bril::bcm1fbkghistosT*)(bcm1fbkghistos->getDataLocation());

      // // fill, run, ls, ln, sec, msec
      // bkgbxhist->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
      // bkgbxhist->setFrequency(64);

      // // sourceid, algo, channel, payloadtype
      // bkgbxhist->setResource(interface::bril::DataSource::BCM1F,NULL,NULL,interface::bril::StorageType::COMPOUND);
      // bkgbxhist->setTotalsize(interface::bril::bcm1fbkghistosT::maxsize());

      //for(size_t i = 0; i < interface::bril::bcm1fbkgT::n(); ++i){bkghist->payload()[i] = 0.;}//initialize array
      
      float bkg1sum = 0.;
      float bkg2sum = 0.;

      float bunchCurrent1Sum = 0.;
      float bunchCurrent2Sum = 0.;

      float nActiveChannels1 = 0.;
      float nActiveChannels2 = 0.;

      float totActiveArea1 = 0.;
      float totActiveArea2 = 0.;

      // for (unsigned int bunch = 0; bunch < 3564; bunch++)
      //       {
      //         if (m_bkgd1mask[bunch] == true)
      //           {
      //              std::stringstream ss;
      //             ss << "(makeBkgNumbers) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
      //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //           }
      //         if (m_bkgd2mask[bunch] == true)
      //           {
      //              std::stringstream ss;
      //             ss << "(makeBkgNumbers) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
      //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //           }
      //       }
      
      for(InDataCache::iterator channelit=m_bcm1fhistcache.begin(); channelit!=m_bcm1fhistcache.end(); ++channelit)
      {
        toolbox::mem::Reference* histref = channelit->second->dataref;
        interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*)(histref->getDataLocation());
        float nNibbles = (float) channelit->second->nbcount;
        // TODO: DO REAL PROCESSING HERE
        //        // each output bin should sum the corresponding four input bins
        // each of m_BG1_plus and m_BG2_minus should sum together the full histograms 
        // of the 24 channels on each end, over this time period, normalizing by 
        // number of actually-picked-up histograms to account for missed data nibbles
        // Determine which end it's on by the channelId: 0-23 for plus, 24-47 for minus
        size_t nbins = histdataptr->n();
        unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1

        // BKGD 1
        if (channelId >= 1 && channelId <= 24)
          {
            bool useChannel = false;
            if (m_useChannelForBkgd[channelId-1])
            {
              if (m_isChannelpCVD[channelId-1])
                {
                  useChannel = true;
                  totActiveArea1 = totActiveArea1 + s_areaDiamond*m_channelScaleFactor[channelId-1];
                  nActiveChannels1++;
                }
              else if (m_isChannelsCVD[channelId-1])
                {
                  useChannel = true;
                  totActiveArea1 = totActiveArea1 + s_areaDiamond*m_channelScaleFactor[channelId-1];
                  nActiveChannels1++;
                }
              else if (m_isChannelSilicon[channelId-1])
                {
                  useChannel = true;
                  totActiveArea1 = totActiveArea1 + s_areaSilicon*m_channelScaleFactor[channelId-1];
                  nActiveChannels1++;
                }
              // loop over RHU bins
              if (useChannel)
                {
                  for(size_t rhubin = 0; rhubin < nbins; ++rhubin ) 
                  {
                    // Mask out test pulse bins and require filled bunch
                    //if (!(rhubin >= 14136 && rhubin < 14196) && m_bkgd1mask[rhubin/4])
                    if (!(rhubin >= 14136 && rhubin < 14232) && m_bkgd1mask[rhubin/4])
                      {
                        if (rhubin % 4 == 0) // TAKE BIN 0 FOR BACKGROUND! (out of 0,1,2,3)
                        {
                          // Find the albedo level from the previous 20 bins, average
                          // then subtract from the background bin
                          float albedo = 0;
                          float nAlbedoBins = 0.;
                          for (int albedoBinIndex = 0; albedoBinIndex < 20; albedoBinIndex++)
                            {
                              int albedoBin = rhubin - albedoBinIndex - 1;
                              if (albedoBin < 0)
                              {
                                albedoBin = albedoBin + 14256;
                              }
                              if (albedoBin % 2 == 1)
                              {
                                albedo = albedo + (float(histdataptr->payload()[albedoBin]))/nNibbles;
                                nAlbedoBins++;
                              }
                            }
                          //albedo = albedo / 20.;
                          albedo = albedo / nAlbedoBins;
                          float toAdd = (float(histdataptr->payload()[rhubin])/m_channelScaleFactor[channelId-1])/nNibbles;
                          // if (toAdd > 0.)
                          //       {
                          //          ss.str("");
                          //         ss << "BG1 channelid " << channelId << " rhubin " 
                          //                 << rhubin << ": toAdd > 0.: " << toAdd 
                          //                 << " albedo " << albedo;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //       }
                          // if (m_iBunch1[rhubin/4] > 0.)
                          //       {
                          //         toAdd = toAdd/m_iBunch1[rhubin/4]*1e11;
                          //       }
                          toAdd = toAdd - albedo;
                          if (toAdd < 0.)
                            {
                              toAdd = 0;
                              m_countAlbedoHigherThanBackground++;
                            }
                          // if (toAdd > 0.)
                          //       {
                          //          ss.str("")
                          //         ss << "BG1 channelid " << channelId << " rhubin " 
                          //                 << rhubin << ": toAdd > 0.: " << toAdd << std::endl;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //       }
                          bkg1sum += toAdd;
                          //bunchCurrent1Sum += m_iBunch1[rhubin/4]/1e11;
                          // if (m_iBunch1[rhubin/4] > 0.)
                          //       {
                          //          ss.str("");
                          //         ss << "Beam 1 current in bunch " << rhubin/4 << " is " << m_iBunch1[rhubin/4] << std::endl;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //       }
                          // if (toAdd > 0.)
                          //       {
                          //          ss.str("");
                          //         ss << "BG1 channelid " << channelId << " rhubin " 
                          //                 << rhubin << ": bkg1sum " << bkg1sum 
                          //                 << " bunchCurrent1Sum " << bunchCurrent1Sum << std::endl;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //       }
                          //bkg1sum += (float(histdataptr->payload()[rhubin]))/nNibbles;
                        }
                      }
                  }
                }      
            }
          }
        // BKGD 2
        else if (channelId >= 25 && channelId <= 48)
          {
            bool useChannel = false;
            //if (channelId != 25 && channelId != 26)
            if (m_useChannelForBkgd[channelId-1])
            {
              if (m_isChannelpCVD[channelId-1])
                {
                  useChannel = true;
                  totActiveArea2 = totActiveArea2 + s_areaDiamond*m_channelScaleFactor[channelId-1];
                  nActiveChannels2++;
                }
              else if (m_isChannelsCVD[channelId-1])
                {
                  useChannel = true;
                  totActiveArea2 = totActiveArea2 + s_areaDiamond*m_channelScaleFactor[channelId-1];
                  nActiveChannels2++;
                }
              else if (m_isChannelSilicon[channelId-1])
                {
                  useChannel = true;
                  totActiveArea2 = totActiveArea2 + s_areaSilicon*m_channelScaleFactor[channelId-1];
                  nActiveChannels2++;
                }
              // loop over RHU bins
              if (useChannel)
                {
                  for(size_t rhubin = 0; rhubin < nbins; ++rhubin ) 
                  {
                    // Mask out test pulse bins and require filled bunch
                    //if (!(rhubin >= 14136 && rhubin < 14196) && m_bkgd2mask[rhubin/4])
                    if (!(rhubin >= 14136 && rhubin < 14232) && m_bkgd2mask[rhubin/4])
                      {
                        if (rhubin % 4 == 0) // TAKE BIN 0 FOR BACKGROUND! (out of 0,1,2,3)
                        {
                          // Find the albedo level from the previous 20 bins, average
                          // then subtract from the background bin
                          float albedo = 0;
                          float nAlbedoBins = 0.;
                          for (int albedoBinIndex = 0; albedoBinIndex < 20; albedoBinIndex++)
                            {
                              int albedoBin = rhubin - albedoBinIndex - 1;
                              if (albedoBin < 0)
                              {
                                albedoBin = albedoBin + 14256;
                              }
                              if (albedoBin % 2 == 1)
                              {
                                albedo = albedo + (float(histdataptr->payload()[albedoBin]))/nNibbles;
                                nAlbedoBins++;
                              }
                            }
                          //albedo = albedo / 20.;
                          albedo = albedo / nAlbedoBins;
                          float toAdd = (float(histdataptr->payload()[rhubin])/m_channelScaleFactor[channelId-1])/nNibbles;
                          // if (toAdd > 0.)
                          //       {
                          //          ss.str("");
                          //         ss << "BG2 channelid " << channelId << " rhubin " 
                          //                 << rhubin << ": toAdd > 0.: " << toAdd 
                          //                 << " albedo " << albedo << std::endl;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //       }
                          // if (m_iBunch2[rhubin/4] > 0.)
                          //       {
                          //         toAdd = toAdd/m_iBunch2[rhubin/4]*1e11;
                          //       }
                          toAdd = toAdd - albedo;
                          if (toAdd < 0.)
                            {
                              toAdd = 0;
                              m_countAlbedoHigherThanBackground++;
                            }
                          // if (toAdd > 0.)
                          //       {
                          //          ss.str("");
                          //         ss << "BG2 channelid " << channelId << " rhubin " 
                          //                 << rhubin << ": toAdd > 0.: " << toAdd << std::endl;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //       }
                          bkg2sum += toAdd;
                          //bunchCurrent2Sum += m_iBunch2[rhubin/4]/1e11;
                          // if (m_iBunch2[rhubin/4] > 0.)
                          //   {
                          //          ss.str("");
                          //         ss << "Beam 2 current in bunch " << rhubin/4 << " is " << m_iBunch2[rhubin/4] << std::endl;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //   }
                          //bkg2sum += (float(histdataptr->payload()[rhubin]))/nNibbles;
                          // if (toAdd > 0.)
                          //       {
                          //          ss.str(""):
                          //         ss << "BG2 channelid " << channelId << " rhubin " 
                          //                 << rhubin << ": bkg2sum " << bkg2sum 
                          //                 << " bunchCurrent2Sum " << bunchCurrent2Sum << std::endl;
                          //          LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
                          //       }
                        }
                      }
                  }      
                }
            }
          }
      }

      // Each BG number should (initial hack) sum the total of the + and - end histograms, respectively
      // make m_BG1_plus and m_BG2_minus here
      // RIGHT NOW THEY ARE NORMALIZED TO ONE NIBBLE! (to account for different numbers of nibbles from different boards)
      // Okay, *NOW* I am fully accounting for them to be normalized to the number of nibbles that have been accumulated

      // Now normalization to Hz/cm^2
      // 1NB -> 1s: 1 LN = ~0.3 s = 4096 orbits * 3564 bxs/orbit * 25 ns/bx = 364953600 ns = 0.365 s
      // hits/cm^2 = Total hits / nActiveSensors / sensor surface area = total hits / nActiveSensors / (0.5 cm)^2

      // for (unsigned int bunch = 0; bunch < 3564; bunch++)
      //       {
      //         if (m_bkgd1mask[bunch] == true)
      //           {
      //              ss.str("");
      //             ss << "(3) Beam 1 bunch " << bunch << " mask is TRUE" << std::endl;
      //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //           }
      //         if (m_bkgd2mask[bunch] == true)
      //           {
      //              ss.str("");
      //             ss << "(3) Beam 2 bunch " << bunch << " mask is TRUE" << std::endl;
      //              LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      //           }
      //       }
      
      for (unsigned int bunch = 0; bunch < 3564; bunch++)
      {
        if (m_bkgd1mask[bunch])
          {
            // ss.str("");
            // ss << "Beam 1 bunch " << bunch << " adding current " << m_iBunch1[bunch] << std::endl;
            // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            bunchCurrent1Sum = bunchCurrent1Sum + m_iBunch1[bunch]/1e11;
          }
        if (m_bkgd2mask[bunch])
          {
            // ss.str("");
            // ss << "Beam 2 bunch " << bunch << " adding current " << m_iBunch2[bunch] << std::endl;
            // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            bunchCurrent2Sum = bunchCurrent2Sum + m_iBunch2[bunch]/1e11;
          }
      }

      //ss.str("");
      std::stringstream ss;
      ss << "BKGD numbers pre-normalization: 1) " << bkg1sum << "  2) " << bkg2sum << std::endl;
      LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());


      // bkg1sum = (bkg1sum / (0.365 * m_numberOfNibbles)) / nActiveChannels1 / 0.25;
      // bkg2sum = (bkg2sum / (0.365 * m_numberOfNibbles)) / nActiveChannels2 / 0.25;

      bkg1sum = (bkg1sum / (0.365)) / totActiveArea1;
      bkg2sum = (bkg2sum / (0.365)) / totActiveArea2;

      if (bunchCurrent1Sum > 0.01) // units of 10^11 protons
            {
              bkg1sum = bkg1sum/bunchCurrent1Sum;
        ss.str("");
        ss << "bunchCurrent1Sum is " << bunchCurrent1Sum << " (in units of 1e11 protons)" << std::endl;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            }
      else
      {
        bkg1sum = 0.;
        ss.str("");
        ss << "bunchCurrent1Sum is " << bunchCurrent1Sum << ", IGNORED (<0.01e11)" << std::endl;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      }
      if (bunchCurrent2Sum > 0.01) // units of 10^11 protons
            {
              bkg2sum = bkg2sum/bunchCurrent2Sum;
        ss.str("");
        ss << "bunchCurrent2Sum is " << bunchCurrent2Sum <<  " (in units of 1e11 protons)" << std::endl;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            }
      else
      {
        bkg2sum = 0.;
        ss.str("");
        ss << "bunchCurrent2Sum is " << bunchCurrent2Sum << ", IGNORED (<0.01e11)" << std::endl;
        LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      }
      

      // // for doing the current normazliation for all bunches at once
      // if(m_iTot1>1e11)
      //       {
      //         bkg1sum=bkg1sum/m_iTot1*1e11;
      //       }
      // if(m_iTot2>1e11)
      //       {
      //         bkg2sum=bkg2sum/m_iTot2*1e11;
      //       }
      
      //filtering of the incidental spikes in the bkg
      m_last4_bkg1[3] = m_last4_bkg1[2];
      m_last4_bkg1[2] = m_last4_bkg1[1];
      m_last4_bkg1[1] = m_last4_bkg1[0];
      m_last4_bkg1[0] = bkg1sum;

      if(bunchCurrent1Sum > 0.5){
         m_BG1_plus = m_last4_bkg1[0];
      }
      else{
         m_BG1_plus = (m_last4_bkg1[0] + m_last4_bkg1[1] + m_last4_bkg1[2] + m_last4_bkg1[3]) / 4;
      }

      m_last4_bkg2[3] = m_last4_bkg2[2];
      m_last4_bkg2[2] = m_last4_bkg2[1];
      m_last4_bkg2[1] = m_last4_bkg2[0];
      m_last4_bkg2[0] = bkg2sum;

      if(bunchCurrent2Sum > 0.5){
         m_BG2_minus = m_last4_bkg2[0];
      }
      else{
         m_BG2_minus = (m_last4_bkg2[0] + m_last4_bkg2[1] + m_last4_bkg2[2] + m_last4_bkg2[3]) / 4;
      }

      //std::stringstream ss;
      ss.str("");
      ss << "BG1 (plus): " << bkg1sum << "  BG2 (minus): " << bkg2sum 
       << " with " << nActiveChannels1 << " active channels for beam 1 "
       << " and " << nActiveChannels2 << " active channels for beam 2 ";
      //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
      //std::cout << ss.str() << std::endl;

      if (bkg1sum > 20) { ss.str(""); ss << "BG>20! BKGD 1 is " << bkg1sum; LOG4CPLUS_INFO(getApplicationLogger(), ss.str());}
      if (bkg2sum > 20) { ss.str(""); ss << "BG>20! BKGD 2 is " << bkg2sum; LOG4CPLUS_INFO(getApplicationLogger(), ss.str());}

      // MORE COMPOUND-SPECIFIC STUFF
      interface::bril::CompoundDataStreamer tc(pdict);       
      tc.insert_field( bkghist->payloadanchor, "plusz", &m_BG1_plus.value_);
      tc.insert_field( bkghist->payloadanchor, "minusz", &m_BG2_minus.value_);
      // END MORE COMPOUND-SPECIFIC STUFF
      m_topicoutqueues[interface::bril::bcm1fbkgT::topicname()]->push(bcm1fbkg);
    }
  catch(xcept::Exception& e)
    {
      std::string msg("Failed to process data for bcm1fbkg");
      LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
      if(bcm1fbkg)
      {
        bcm1fbkg->release();
        bcm1fbkg= 0;
      }
    }

}

// Calculates the luminosity numbers (full-orbit values 
// and bunch-by-bunch histograms), 
// and queues them for publishing
void bril::bcm1fprocessor::Application::makeLumiNumbers(interface::bril::DatumHead& inheader)
{
  toolbox::mem::Reference* bcm1flumi = 0; // THIS ONE IS DUPLICATE OF WHAT WE WANT
  toolbox::mem::Reference* bcm1fscvdlumi = 0;
  toolbox::mem::Reference* bcm1fpcvdlumi = 0;
  toolbox::mem::Reference* bcm1fsilumi = 0;

  LOG4CPLUS_DEBUG(getApplicationLogger(), "Still in do_process: bcm1fXXlumi");  
  try
  {
    // DUPLICATE OF SENSOR TYPE CONFIGURED BY FLAG
    // FOR COMPOUND DATA TYPE
    std::string pdict = interface::bril::bcm1flumiT::payloaddict();
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",pdict);
    // END COMPOUND-SPECIFIC STUFF

    // FOR COMPOUND DATA TYPE
    std::string pdictscvd = interface::bril::bcm1fscvdlumiT::payloaddict();
    xdata::Properties plistscvd;
    plistscvd.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
    plistscvd.setProperty("PAYLOAD_DICT",pdictscvd);
    // END COMPOUND-SPECIFIC STUFF

    // FOR COMPOUND DATA TYPE
    std::string pdictpcvd = interface::bril::bcm1fpcvdlumiT::payloaddict();
    xdata::Properties plistpcvd;
    plistpcvd.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
    plistpcvd.setProperty("PAYLOAD_DICT",pdictpcvd);
    // END COMPOUND-SPECIFIC STUFF

    // FOR COMPOUND DATA TYPE
    std::string pdictsi = interface::bril::bcm1fsilumiT::payloaddict();
    xdata::Properties plistsi;
    plistsi.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
    plistsi.setProperty("PAYLOAD_DICT",pdictsi);
    // END COMPOUND-SPECIFIC STUFF

    // DUPLICATE OF WHICHEVER SENSOR TYPE WE WANT
    bcm1flumi = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1flumiT::maxsize());
    bcm1flumi->setDataSize(interface::bril::bcm1flumiT::maxsize());
    interface::bril::bcm1flumiT* lumihist = (interface::bril::bcm1flumiT*)(bcm1flumi->getDataLocation());

    bcm1fscvdlumi = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fscvdlumiT::maxsize());
    bcm1fscvdlumi->setDataSize(interface::bril::bcm1fscvdlumiT::maxsize());
    interface::bril::bcm1fscvdlumiT* lumihistscvd = (interface::bril::bcm1fscvdlumiT*)(bcm1fscvdlumi->getDataLocation());

    bcm1fpcvdlumi = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fpcvdlumiT::maxsize());
    bcm1fpcvdlumi->setDataSize(interface::bril::bcm1fpcvdlumiT::maxsize());
    interface::bril::bcm1fpcvdlumiT* lumihistpcvd = (interface::bril::bcm1fpcvdlumiT*)(bcm1fpcvdlumi->getDataLocation());

    bcm1fsilumi = m_poolFactory->getFrame(m_memPool,interface::bril::bcm1fsilumiT::maxsize());
    bcm1fsilumi->setDataSize(interface::bril::bcm1fsilumiT::maxsize());
    interface::bril::bcm1fsilumiT* lumihistsi = (interface::bril::bcm1fsilumiT*)(bcm1fsilumi->getDataLocation());

    // fill, run, ls, ln, sec, msec
    lumihist->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
    lumihist->setFrequency(4);

    // fill, run, ls, ln, sec, msec
    lumihistscvd->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
    lumihistscvd->setFrequency(4);

    // fill, run, ls, ln, sec, msec
    lumihistpcvd->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
    lumihistpcvd->setFrequency(4);

    // fill, run, ls, ln, sec, msec
    lumihistsi->setTime(inheader.fillnum,inheader.runnum,inheader.lsnum,inheader.nbnum,inheader.timestampsec,inheader.timestampmsec);
    lumihistsi->setFrequency(4);

    // DUPLICATE OF WHICHEVER SENSOR TYPE WE WANT
    int bestsource = interface::bril::DataSource::BCM1F;
    if (m_best_sensor_type.toString() == "sCVD")
    bestsource = interface::bril::DataSource::BCM1FSCVD;
    if (m_best_sensor_type.toString() == "pCVD")
    bestsource = interface::bril::DataSource::BCM1FPCVD;
    if (m_best_sensor_type.toString() == "Si")
    bestsource = interface::bril::DataSource::BCM1FSI;
    // sourceid, algo, channel, payloadtype
    //lumihist->setResource(interface::bril::DataSource::BCM1F,NULL,NULL,interface::bril::StorageType::COMPOUND);
    lumihist->setResource(bestsource,NULL,NULL,interface::bril::StorageType::COMPOUND);
    lumihist->setTotalsize(interface::bril::bcm1flumiT::maxsize());

    // sourceid, algo, channel, payloadtype
    lumihistscvd->setResource(interface::bril::DataSource::BCM1FSCVD,NULL,NULL,interface::bril::StorageType::COMPOUND);
    lumihistscvd->setTotalsize(interface::bril::bcm1fscvdlumiT::maxsize());

    // sourceid, algo, channel, payloadtype
    lumihistpcvd->setResource(interface::bril::DataSource::BCM1FPCVD,NULL,NULL,interface::bril::StorageType::COMPOUND);
    lumihistpcvd->setTotalsize(interface::bril::bcm1fpcvdlumiT::maxsize());

    // sourceid, algo, channel, payloadtype
    lumihistsi->setResource(interface::bril::DataSource::BCM1FSI,NULL,NULL,interface::bril::StorageType::COMPOUND);
    lumihistsi->setTotalsize(interface::bril::bcm1fsilumiT::maxsize());

    // resetting values of global variables to zero
    // m_lumi_avg = 0;
    // m_lumi_avg_raw = 0;
    memset(m_lumi_avg,0,sizeof(m_lumi_avg));
    memset(m_lumi_avg_raw,0,sizeof(m_lumi_avg_raw));
    memset(m_lumi_bx,0,sizeof(m_lumi_bx));
    memset(m_lumi_bx_raw,0,sizeof(m_lumi_bx_raw));
    memset(m_raw_uncor_pcvd,0,sizeof(m_raw_uncor_pcvd));
    memset(m_raw_uncor_si,0,sizeof(m_raw_uncor_si));
	memset(m_fourbinsperbx,0,sizeof(m_fourbinsperbx));
    //      double histos4NB[48][3564];
    
    unsigned int maskhigh = 0;
    unsigned int masklow = 0;

    double muBX[48][3564];
    memset (muBX,0,sizeof(muBX));
    double lumiBX[48][3564];
    memset (lumiBX,0,sizeof(lumiBX));
    double lumiSensor[48];
    memset (lumiSensor,0,sizeof(lumiSensor));
    double lumiSensorRaw[48];
    memset (lumiSensorRaw,0,sizeof(lumiSensorRaw));
    double lumiperBX[3][3564]; // for each of 3 different sensor types
    memset (lumiperBX,0,sizeof(lumiperBX));



    //      double nb4Orbits=4*4096;    //4 nibbles per readout
    std::stringstream ss;
    ss << "Scaling for " << m_numberOfNibbles << " nibbles (fill " 
      << inheader.fillnum << " run " << inheader.runnum
      << " section " << inheader.lsnum << "  nibble " << inheader.nbnum << ")"
      << std::endl;
    LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());

    double nb4Orbits=4096.*m_numberOfNibbles;    //4 nibbles per readout
    
    //double sigmaVis=2.825E-28; //this is in cm2 for 14 TeV
    //      double sigmaVis=2.63E-28; //this is in cm2 for 13 TeV
    //double sigmaVis=0.67E-28 ; //this is in cm2 for 900GeV
    double sigmaVis[3]={m_sigma_vis_scvd,m_sigma_vis_pcvd,m_sigma_vis_si}; // single, poly, silicon
    double calibSBIL[3]={m_calib_at_SBIL_scvd,m_calib_at_SBIL_pcvd,m_calib_at_SBIL_si}; // single, poly, silicon
    double nonlinearity[3]={m_nonlinearity_scvd,m_nonlinearity_pcvd,m_nonlinearity_si}; // single, poly, silicon

    int nSensors[3]={0,0,0};
    double instLumi[3]={0.,0.,0.};
    double instLumiRaw[3]={0.,0.,0.};

    // loop over channels -- ALL channels includes BPTX etc, but only channelid 1-48 are taken for lumi
    for(InDataCache::iterator channelit=m_bcm1fhistcache.begin(); channelit!=m_bcm1fhistcache.end(); ++channelit)
    {
      toolbox::mem::Reference* histref = channelit->second->dataref;
      interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*)(histref->getDataLocation());
      float nNibbles = (float) channelit->second->nbcount;
      // TODO: DO REAL PROCESSING HERE
      //        // each output bin should sum the corresponding four input bins
      size_t nbins = histdataptr->n();
      unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1
      unsigned int channel = channelId - 1;
      
      // take this from 14256 bins down to 3564 bins
      double histBxBins[3564];
      memset(histBxBins,0,sizeof(histBxBins));
	  double histfourBxBins[14256];
      memset(histfourBxBins,0,sizeof(histfourBxBins));
      for(size_t bxbin = 0; bxbin < 3564; ++bxbin )
      {
        //if (!(bxbin >= 3534 && bxbin < 3549))
        if (!(bxbin >= 3534 && bxbin < 3564)) // Mask rest of abort gap to easily avoid problems in new algo below
        {
          //histBxBins[bxbin] = (float(histdataptr->payload()[bxbin*4+2])); // TAKE BIN 2 FOR COLLISIONS! (out of 0,1,2,3)
          // NEW ALGORITHM: Take four bins, 1 before peak, peak itself, and two afterwards.
          histBxBins[bxbin] = 0.;
          for (int mybin = 1; mybin < 5; mybin++)
          {
            histBxBins[bxbin] = histBxBins[bxbin] + (float(histdataptr->payload()[bxbin*4+mybin])); // TAKE 4 BINS FOR COLLISIONS! (1,2,3, and 4 out of 0,1,2,3 for a single bunch -- means also take first bin of next bx)
          }
        }
      }
     for(size_t bxbin = 0; bxbin < 14256; ++bxbin )
      { 
		m_fourbinsperbx[channel][bxbin] = histdataptr->payload()[bxbin];
		}
	
      int nonZeroBunches = 0;
      
      // this is going through histogram of 3564 bins
      if(channel >= 0 && channel < 48)
      {
        if(m_useChannelForLumi[channel] && (m_isChannelValid[channel]))
        {
          if (m_isChannelsCVD[channel])
          {
            nSensors[0]++;
          }
          if (m_isChannelpCVD[channel])
          {
            nSensors[1]++;
          }
          if (m_isChannelSilicon[channel])
          {
            nSensors[2]++;
          }
          // Create channel mask to publish, two uint32s
          if (channel < 32)
          {
            masklow = masklow | (1<<channel);
          }
          else if (channel >=32 && channel < 64)
          {
            maskhigh = maskhigh | (1<<(channel-32));
          }
          else
          {
            // wrong channelid!
          }


          // make zero counting
          for (int bxBin=0;bxBin<3564;bxBin++)
          {
            if (histBxBins[bxBin]>0) // FOR DEBUG ONLY
            {
              // it is the number of hits getting scaled, because we're correcting on the rate
              double r0=1-((double)histBxBins[bxBin]/m_channelScaleFactor[channel]/nb4Orbits); // 1- probability of a non-zero (#non-zero/#orbits)
              if (r0 < 0.) r0 = 0.;
              muBX[channel][bxBin]=-log(r0); //the so called mu value or occupancy, this is proportional to luminosity


              // HERE'S WHERE WE NEED TO START SPLITTING THINGS
              if (m_isChannelsCVD[channel])
              {
                //Albedo correction. Albedo fraction should get updated regularily by the workloop
                //need to albedo correct here to properly apply non linear calibration.
                muBX[channel][bxBin] = muBX[channel][bxBin] * 1; 
                //calibration, second part of sum is non linearity term.
                lumiBX[channel][bxBin]=muBX[channel][bxBin]*11246/sigmaVis[0] + pow(muBX[channel][bxBin],2) * pow(11246/sigmaVis[0],2) * nonlinearity[0]*(1+nonlinearity[0]*calibSBIL[0]);    //the 11246 to  get s-1, removed /1E30 to make same unit as brilcal
                lumiperBX[0][bxBin]+=lumiBX[channel][bxBin];
                m_lumi_bx_raw[0][bxBin] = m_lumi_bx_raw[0][bxBin] + muBX[channel][bxBin]*1; 
                m_lumi_bx[0][bxBin] = lumiperBX[0][bxBin];
              }
              if (m_isChannelpCVD[channel])
              {
                //add uncorrected mu to put it into albedo correction
                m_raw_uncor_pcvd[bxBin] = m_raw_uncor_pcvd[bxBin] + muBX[channel][bxBin];
                //Albedo correction. Albedo fraction should get updated regularily by the workloop
                muBX[channel][bxBin] = (muBX[channel][bxBin] * m_albedo_fraction_pcvd[bxBin]) - m_raw_noise_pcvd;
                //calibration, second part of sum is non linearity term.
                lumiBX[channel][bxBin]=muBX[channel][bxBin]*11246/sigmaVis[1] + pow(muBX[channel][bxBin],2) * pow(11246/sigmaVis[1],2) * nonlinearity[1]*(1+nonlinearity[1]*calibSBIL[1]);    //the 11246 to  get s-1
                lumiperBX[1][bxBin]+=lumiBX[channel][bxBin];
                m_lumi_bx_raw[1][bxBin] = m_lumi_bx_raw[1][bxBin] + muBX[channel][bxBin];
                m_lumi_bx[1][bxBin] = lumiperBX[1][bxBin];
              }
              if (m_isChannelSilicon[channel])
              {
                //add uncorrected mu to put it into albedo correction
                m_raw_uncor_si[bxBin] = m_raw_uncor_si[bxBin] + muBX[channel][bxBin];
                //Albedo correction. Albedo fraction should get updated regularily by the workloop
                muBX[channel][bxBin] = (muBX[channel][bxBin] * m_albedo_fraction_si[bxBin]) - m_raw_noise_si;
                lumiBX[channel][bxBin]=muBX[channel][bxBin]*11246/sigmaVis[2] + pow(muBX[channel][bxBin],2) * pow(11246/sigmaVis[2],2) * nonlinearity[2]*(1+nonlinearity[2]*calibSBIL[2]);    //the 11246 to  get s-1
                lumiperBX[2][bxBin]+=lumiBX[channel][bxBin];
                m_lumi_bx_raw[2][bxBin] = m_lumi_bx_raw[2][bxBin] + muBX[channel][bxBin]; 
                m_lumi_bx[2][bxBin] = lumiperBX[2][bxBin];
              }

              // make sums from histogram with bx masking
              if(m_collmask[bxBin])
              {
                lumiSensor[channel]+=lumiBX[channel][bxBin];
                lumiSensorRaw[channel]+=muBX[channel][bxBin];
                nonZeroBunches++;
              }
            }
          } // end loop through bins j
          

          // now sum up all channels from per channel sums
          if (m_isChannelsCVD[channel])
          {
            instLumi[0]+=lumiSensor[channel];
            instLumiRaw[0]+=lumiSensorRaw[channel];
          }
          if (m_isChannelpCVD[channel])
          {
            instLumi[1]+=lumiSensor[channel];
            instLumiRaw[1]+=lumiSensorRaw[channel];
          }
          if (m_isChannelSilicon[channel])
          {
            instLumi[2]+=lumiSensor[channel];
            instLumiRaw[2]+=lumiSensorRaw[channel];
          }
        } // end if use channel for lumi
      } // end only use channels 1-48
    } // end loop through channels i

    //Normalize everything with number of sensors
    if(nSensors[0])
    {
      instLumi[0]/=(float)nSensors[0];
      instLumiRaw[0]/=(float)nSensors[0];
    }
    if(nSensors[1])
    {
      instLumi[1]/=(float)nSensors[1];
      instLumiRaw[1]/=(float)nSensors[1];
    }
    if(nSensors[2])
    {
      instLumi[2]/=(float)nSensors[2];
      instLumiRaw[2]/=(float)nSensors[2];
    }
    for (int bxBin=0;bxBin<3564;bxBin++)
    {
      if(nSensors[0])
      {
        m_lumi_bx[0][bxBin]/=nSensors[0];
        m_lumi_bx_raw[0][bxBin]/=nSensors[0];
      }
      if(nSensors[1])
      {
        m_lumi_bx[1][bxBin]/=nSensors[1];
        m_lumi_bx_raw[1][bxBin]/=nSensors[1];
        m_raw_uncor_pcvd[bxBin]/=nSensors[1];
      }
      if(nSensors[2])
      {
        m_lumi_bx[2][bxBin]/=nSensors[2];
        m_lumi_bx_raw[2][bxBin]/=nSensors[2];
        m_raw_uncor_si[bxBin]/=nSensors[2];
      }
      // THE STANDARD ONE
      if (bxBin < 5)
      {
        ss.str("");
        ss << "sCVD: m_lumi_bx[0][" << bxBin << "] = " <<  m_lumi_bx[0][bxBin] << std::endl;
        ss << "pCVD: m_lumi_bx[1][" << bxBin << "] = " <<  m_lumi_bx[1][bxBin] << std::endl;
        ss << "Silicon: m_lumi_bx[2][" << bxBin << "] = " <<  m_lumi_bx[2][bxBin] << std::endl;
        if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }
      }
    }

    //push raw histogram to albedo correction
    
    std::array<double,3564> raw_uncor_pcvd;
    std::array<double,3564> raw_uncor_si;
    for (int i=0;i<3564;i++)
    {
      raw_uncor_pcvd[i] = m_raw_uncor_pcvd[i];
      raw_uncor_si[i] = m_raw_uncor_si[i];
    }
    m_albedoqueuepcvd.push_back(raw_uncor_pcvd);
    m_albedoqueuesi.push_back(raw_uncor_si);

    // convert variable type
    for (int type = 0; type < 3; type++)
    {
      m_lumi_avg[type].value_ = instLumi[type];
      m_lumi_avg_raw[type].value_ = instLumiRaw[type];
    }
    
    //std::stringstream ss;
    ss.str("");
    ss << "Avg pCVD lumi: " << m_lumi_avg[0] << "  Avg pCVD lumi raw: " << m_lumi_avg_raw[0] << std::endl;
    ss << "Avg sCVD lumi: " << m_lumi_avg[1] << "  Avg sCVD lumi raw: " << m_lumi_avg_raw[1] << std::endl;
    ss << "Avg silicon lumi: " << m_lumi_avg[2] << "  Avg silicon lumi raw: " << m_lumi_avg_raw[2];
    if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

    ss.str("");
    ss << "Current channel mask: ";
    for (int bit = 0; bit < 32; bit++)
    {
      ss << ((masklow >> bit) & 1) << " ";
    }
    for (int bit = 0; bit < 32; bit++)
    {
      ss << ((maskhigh >> bit) & 1) << " ";
    }
    if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), ss.str()); }

    // NEED TO SPLIT IT INTO 3 LUMI TYPES
    // THIS ONE IS DUPLICATE OF WHICHEVER SENSOR TYPE WE WANT
    int sensortype = 99;
    if (m_best_sensor_type.toString() == "sCVD")
    sensortype = 0;
    if (m_best_sensor_type.toString() == "pCVD")
    sensortype = 1;
    if (m_best_sensor_type.toString() == "Si")
    sensortype = 2;
    interface::bril::CompoundDataStreamer tc(pdict);       
    tc.insert_field( lumihist->payloadanchor, "calibtag", m_calibtag.value_.c_str());
    tc.insert_field( lumihist->payloadanchor, "avgraw", &m_lumi_avg_raw[sensortype].value_);
    tc.insert_field( lumihist->payloadanchor, "avg", &m_lumi_avg[sensortype].value_);
    tc.insert_field( lumihist->payloadanchor, "bxraw", m_lumi_bx_raw[sensortype]);
    tc.insert_field( lumihist->payloadanchor, "bx", m_lumi_bx[sensortype]);
	tc.insert_field( lumihist->payloadanchor, "4bxraw", m_fourbinsperbx[sensortype]);
    tc.insert_field( lumihist->payloadanchor, "masklow", &masklow);
    tc.insert_field( lumihist->payloadanchor, "maskhigh", &maskhigh);
    m_topicoutqueues[interface::bril::bcm1flumiT::topicname()]->push(bcm1flumi);
    //

    // sCVD
    interface::bril::CompoundDataStreamer tcscvd(pdictscvd);       
    tcscvd.insert_field( lumihistscvd->payloadanchor, "calibtag", m_calibtag.value_.c_str());
    tcscvd.insert_field( lumihistscvd->payloadanchor, "avgraw", &m_lumi_avg_raw[0].value_);
    tcscvd.insert_field( lumihistscvd->payloadanchor, "avg", &m_lumi_avg[0].value_);
    tcscvd.insert_field( lumihistscvd->payloadanchor, "bxraw", m_lumi_bx_raw[0]);
    tcscvd.insert_field( lumihistscvd->payloadanchor, "bx", m_lumi_bx[0]);
	tcscvd.insert_field( lumihist->payloadanchor, "4bxraw", m_fourbinsperbx[sensortype]);
    tcscvd.insert_field( lumihistscvd->payloadanchor, "masklow", &masklow);
    tcscvd.insert_field( lumihistscvd->payloadanchor, "maskhigh", &maskhigh);
    m_topicoutqueues[interface::bril::bcm1fscvdlumiT::topicname()]->push(bcm1fscvdlumi);
    // pCVD
    interface::bril::CompoundDataStreamer tcpcvd(pdictpcvd);       
    tcpcvd.insert_field( lumihistpcvd->payloadanchor, "calibtag", m_calibtag.value_.c_str());
    tcpcvd.insert_field( lumihistpcvd->payloadanchor, "avgraw", &m_lumi_avg_raw[1].value_);
    tcpcvd.insert_field( lumihistpcvd->payloadanchor, "avg", &m_lumi_avg[1].value_);
    tcpcvd.insert_field( lumihistpcvd->payloadanchor, "bxraw", m_lumi_bx_raw[1]);
    tcpcvd.insert_field( lumihistpcvd->payloadanchor, "bx", m_lumi_bx[1]);
	tcpcvd.insert_field( lumihist->payloadanchor, "4bxraw", m_fourbinsperbx[sensortype]);
    tcpcvd.insert_field( lumihistpcvd->payloadanchor, "masklow", &masklow);
    tcpcvd.insert_field( lumihistpcvd->payloadanchor, "maskhigh", &maskhigh);
    m_topicoutqueues[interface::bril::bcm1fpcvdlumiT::topicname()]->push(bcm1fpcvdlumi);
    // Silicon
    interface::bril::CompoundDataStreamer tcsi(pdictsi);       
    tcsi.insert_field( lumihistsi->payloadanchor, "calibtag", m_calibtag.value_.c_str());
    tcsi.insert_field( lumihistsi->payloadanchor, "avgraw", &m_lumi_avg_raw[2].value_);
    tcsi.insert_field( lumihistsi->payloadanchor, "avg", &m_lumi_avg[2].value_);
    tcsi.insert_field( lumihistsi->payloadanchor, "bxraw", m_lumi_bx_raw[2]);
    tcsi.insert_field( lumihistsi->payloadanchor, "bx", m_lumi_bx[2]);
	tcsi.insert_field( lumihist->payloadanchor, "4bxraw", m_fourbinsperbx[sensortype]);
    tcsi.insert_field( lumihistsi->payloadanchor, "masklow", &masklow);
    tcsi.insert_field( lumihistsi->payloadanchor, "maskhigh", &maskhigh);
    m_topicoutqueues[interface::bril::bcm1fsilumiT::topicname()]->push(bcm1fsilumi);

    //new data for plotting
    m_chart_lumi_raw_pcvd->newDataReady();
    m_chart_lumi_raw_si->newDataReady();
//-------------------------------------------------------------------------------------------------
// loop over channels -- ALL channels includes BPTX etc, but only channelid 1-48 are taken for lumi
    // for(InDataCache::iterator channelit=m_bcm1fhistcache.begin(); channelit!=m_bcm1fhistcache.end(); ++channelit)
    // {
      // toolbox::mem::Reference* histref = channelit->second->dataref;
      // interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*)(histref->getDataLocation());

      // unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1
      // unsigned int channel = channelId - 1;
      // for(size_t bxbin = 0; bxbin < 14265; ++bxbin )
	// {m_fourbinsperbx[channel][bxbin] = histdataptr->payload()[bxbin];}}
    // m_chart_fourbinsperbx->newDataReady();
	// m_chart_lumi_raw_si->newDataReady();
	// -------------------------------------------------------------
	}


	
  catch(xcept::Exception& e)
  {
    std::string msg("Failed to process data for bcm1fXXlumi");
    LOG4CPLUS_ERROR(getApplicationLogger(),msg+stdformat_exception_history(e));
    if(bcm1flumi)
    {
      bcm1flumi->release();
      bcm1flumi= 0;
    }
    if(bcm1fscvdlumi)
    {
      bcm1fscvdlumi->release();
      bcm1fscvdlumi= 0;
    }
    if(bcm1fpcvdlumi)
    {
      bcm1fpcvdlumi->release();
      bcm1fpcvdlumi= 0;
    }
    if(bcm1fsilumi)
    {
      bcm1fsilumi->release();
      bcm1fsilumi= 0;
    }
  }
}
/* void bril::bcm1fprocessor::Application::makebxhistnumbers(interface::bril::DatumHead& inheader)
{
    // loop over channels -- ALL channels includes BPTX etc, but only channelid 1-48 are taken for lumi
    for(InDataCache::iterator channelit=m_bcm1fhistcache.begin(); channelit!=m_bcm1fhistcache.end(); ++channelit)
    {
      toolbox::mem::Reference* histref = channelit->second->dataref;
      interface::bril::bcm1fhistT* histdataptr = (interface::bril::bcm1fhistT*)(histref->getDataLocation());

      unsigned int channelId = (unsigned int) histdataptr->channelid;//Note: ChannelID counts from 1
      unsigned int channel = channelId - 1;
      for(size_t bxbin = 0; bxbin < 14265; ++bxbin )
	{m_fourbinsperbx[channel][bxbin] = histdataptr->payload()[bxbin];}}
    m_chart_fourbinsperbx->newDataReady();
	//m_chart_lumi_raw_si->newDataReady();
}
 */
//Albedo workloop, calculates the albedo fraction regularily and puts the queue to its defined buffer length
bool bril::bcm1fprocessor::Application::albedo_calculation(toolbox::task::WorkLoop* wl){
  // temporary parameters, move to XML
  // int albedo_calculation_time = 10; //seconds
  // int albedo_queue_length = 100; //entires, time = entries * 1.4s
  // int noise_calc_start = 3480;
  // int noise_calc_end = 3530;

  // average all data in queue
  int no_entries_pcvd = 0; //could check queue length, but want to avoid issues when queue is being filled while looping
  double uncorrected_pcvd[3564];
  memset(uncorrected_pcvd,0.0,sizeof(uncorrected_pcvd));
  double corrected_pcvd[3564];
  memset(corrected_pcvd,0.0,sizeof(corrected_pcvd));

  for(AlbedoIt it=m_albedoqueuepcvd.begin();it!=m_albedoqueuepcvd.end();++it)
  {
    for (int bx = 0; bx < 3564; bx++)
    {
      uncorrected_pcvd[bx] += it->at(bx);
    }
    no_entries_pcvd++;
  }

  std::stringstream msg;
  msg << "Number of histograms in the pcvd albedo queue: "<< no_entries_pcvd;
  LOG4CPLUS_INFO(getApplicationLogger(), msg.str());

  int no_entries_si = 0; //could check queue length, but want to avoid issues when queue is being filled while looping
  double uncorrected_si[3564];
  memset(uncorrected_si,0.0,sizeof(uncorrected_si));
  double corrected_si[3564];
  memset(corrected_si,0.0,sizeof(corrected_si));
  for(AlbedoIt it=m_albedoqueuesi.begin();it!=m_albedoqueuesi.end();++it)
  {
    for (int bx = 0; bx < 3564; bx++)
    {
      uncorrected_si[bx] += it->at(bx);
    }
    no_entries_si++;
  }


  // normalize with counted number of elements.
  for (int bx = 0; bx < 3564; bx++)
  {
    uncorrected_pcvd[bx] /= no_entries_pcvd;
    uncorrected_si[bx] /= no_entries_si;
  }



  if ((no_entries_pcvd!=0) || (no_entries_si!=0))
  {
    //calculate correction and update fraction array
    // initialize
    for (int i = 0; i < 3564; i++)
    {
      corrected_pcvd[i] = uncorrected_pcvd[i];
      corrected_si[i] = uncorrected_si[i];
    }
    // correct the data
    for (int i = 0; i < 3564; i++)
    {
      if(m_collmask[i])
      {
        for (int j = i+1; j < i+m_albedo_model_pcvd.size(); j++) 
        {
          if( j >= 3564 )
          {
            corrected_pcvd[j-3564] -= corrected_pcvd[i]*m_albedo_model_pcvd[j-i];
          }
          else
          {
          // correct the data
            corrected_pcvd[j] -= corrected_pcvd[i]*m_albedo_model_pcvd[j-i];
          }
        }
        for (int j = i+1; j < i+m_albedo_model_si.size(); j++) //separate loop becacuse model could be different length, currently use same model
        {
          if( j >= 3564 )
          {
            corrected_si[j-3564] -= corrected_si[i]*m_albedo_model_si[j-i];
          }
          else
          {
          // correct the data
            corrected_si[j] -= corrected_si[i]*m_albedo_model_si[j-i];
          }
        }

      }
    }

    //make ratios
    for (int i = 0; i < 3564; i++)
    {
      m_albedoqueue_hist_pcvd_uncor[i] = uncorrected_pcvd[i];
      m_albedoqueue_hist_pcvd_cor[i] = corrected_pcvd[i];
      m_albedoqueue_hist_si_uncor[i] = uncorrected_si[i];
      m_albedoqueue_hist_si_cor[i] = corrected_si[i];
      if (uncorrected_pcvd[i] != 0.0)
      {
        m_albedo_fraction_pcvd[i] = corrected_pcvd[i]/uncorrected_pcvd[i];
      }
      else
      {
        m_albedo_fraction_pcvd[i] = 1.0;
      }
      if (uncorrected_si[i] != 0.0)
      {
        m_albedo_fraction_si[i] = corrected_si[i]/uncorrected_si[i];
      }
      else
      {
        m_albedo_fraction_si[i] = 1.0;
      }
      m_devel_hist1[i] = m_albedo_fraction_pcvd[i];
      m_devel_hist2[i] = m_albedo_fraction_si[i];
    }




    // calculate noise level in abort gap

    float noise_pcvd = 0;
    float noise_si = 0;
    int count = 0;
    for (int i = m_noise_calc_start; i<=m_noise_calc_end; i++)
    {
      noise_pcvd += corrected_pcvd[i];
      noise_si += corrected_si[i];
      count++;
    }
    m_raw_noise_pcvd = noise_pcvd/count;
    m_raw_noise_si = noise_si/count;

    std::stringstream ss;
    ss << "Noise to subtract: " << m_raw_noise_pcvd;
    LOG4CPLUS_INFO(getApplicationLogger(), ss.str());


    //reduce queue to pre-defined length.
    if (no_entries_pcvd>m_albedo_queue_length)
    {
      for(int i=0; i<(no_entries_pcvd-m_albedo_queue_length);i++)
      {
        m_albedoqueuepcvd.pop_front();
      }
    }
    if (no_entries_si>m_albedo_queue_length)
    {
      for(int i=0; i<(no_entries_si-m_albedo_queue_length);i++)
      {
        m_albedoqueuesi.pop_front();
      }
    }

    //Fire plotting
    m_chart_albedoqueue_pcvd->newDataReady();
    m_chart_albedoqueue_si->newDataReady();



  }

  sleep(m_albedo_calculation_time); // Run albedo correctino every X seconds (defined in XML)
  return true;
}


// Clear the fully-accumulated histograms in preparation
// to accumulate more
void bril::bcm1fprocessor::Application::clear_cache(){
  LOG4CPLUS_DEBUG(getApplicationLogger(), "clear_cache");

  for(InDataCache::iterator it=m_bcm1fhistcache.begin();it!=m_bcm1fhistcache.end();++it){
    if(it->second->dataref!=0){
      it->second->dataref->release();
      it->second->dataref=0;
    }
    delete it->second;
  }
  m_bcm1fhistcache.clear();
}

// Accumulate this nibble histogram's data in the bins of the given channel
void bril::bcm1fprocessor::Application::hist_accumulateInChannel(InDataCache::iterator channelit, interface::bril::bcm1fhistT* indata){
  toolbox::mem::Reference* basedata = channelit->second->dataref;
  interface::bril::bcm1fhistT* basehistdata = (interface::bril::bcm1fhistT*)( basedata->getDataLocation() );
  for(size_t i=0; i<basehistdata->n(); ++i){
    basehistdata->payload()[i] += indata->payload()[i];
  }
  channelit->second->nbcount++;
  if (channelit->second->nbcount > m_numberOfNibbles)
    m_numberOfNibbles = channelit->second->nbcount;
}

bool bril::bcm1fprocessor::Application::check_channelstatistics(unsigned int nchannels){
  if(m_nChannelsThisNibble<nchannels) {
    //std::stringstream ss;
    //ss << "Size of cache is now " << m_bcm1fhistcache.size() << std::endl;
    // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    return false;
  }
  //std::stringstream ss;
  //ss << "Size of cache is now " << m_bcm1fhistcache.size() << std::endl;
  // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
  return true;
}

bool bril::bcm1fprocessor::Application::check_channelstatistics_old(unsigned int nchannels){
  if(m_bcm1fhistcache.size()<nchannels) {
    //std::stringstream ss;
    //ss << "Size of cache is now " << m_bcm1fhistcache.size() << std::endl;
    // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
    return false;
  }
  //std::stringstream ss;
  //ss << "Size of cache is now " << m_bcm1fhistcache.size() << std::endl;
  // LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
  return true;
}

// In this function we want to see if we have accumulated enough nibbles to process
// the data.  This accounts for some channels being inactive due to missing RHU (I think).
// At least one channel must have nibbles accumulated, and all channels with nibbles 
// must have accumulated enough nibbles to process.
// This also accounts for the possibility that some boards are "missing" nibbles and 
// sending too few; the processor will wait until each board sends at least the 
// necessary number (and then normalize by the total number of nibbles sent).
// This situation should never happen during real running (only on internal LN signal
// if counter threshold misconfigured). 
bool bril::bcm1fprocessor::Application::check_nbstatistics(unsigned short nnbs)
{
  bool someChannelHasData = false;
  bool allOccupiedChannelsAboveThreshold = true;
  int maxNibblesCounted = 0;
  for(InDataCache::iterator histchannelit=m_bcm1fhistcache.begin(); histchannelit!=m_bcm1fhistcache.end(); ++histchannelit)
    {
      //if(histchannelit->second->nbcount!=nnbs) {
      // if not enough nibbles received in one channel, not ready to process yet
      // but ignore any channels who have 0 statistics as not active
      if(histchannelit->second->nbcount != 0) 
      {
        someChannelHasData = true;
        if(histchannelit->second->nbcount < nnbs) 
          {
            //std::stringstream ss;
            //ss<<"channel "<<histchannelit->first<<" nbcount "<<histchannelit->second->nbcount<<std::endl;
            //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
            //return false;
            allOccupiedChannelsAboveThreshold = false;
          }
        if(histchannelit->second->nbcount > maxNibblesCounted)
          {
            maxNibblesCounted = histchannelit->second->nbcount;
          }
      }
    }
  // only return true if all channels are either zero or above threshold AND there is at least one channel with data
  if((someChannelHasData == true) && (allOccupiedChannelsAboveThreshold == true))
    {
      //std::stringstream ss;
      //ss<<"nibble stat full"<<std::endl;
      //ss << "maxNibblesCounted full: " << maxNibblesCounted << std::endl;
      //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
      return true;
    }

  //std::stringstream ss;
  //ss << "maxNibblesCounted not full: " << maxNibblesCounted << std::endl;
  //LOG4CPLUS_DEBUG(getApplicationLogger(), ss.str());
  return false;
}

// This function merely checks if the collected number of nibbles is the full amount 
// expected.  There is no workaround for RHUs that have dropped out, which so far
// (early 2017) has not been necessary.  
// This one was written because the regular check_nbstatistics() function does not 
// appear to be working exactly as expected.  
bool bril::bcm1fprocessor::Application::check_nbstatistics_simple(unsigned short nnbs)
{
  // If number of collected nibbles = number of nibbles to collect * number of RHUs * number of channels per RHU
  // then full statisics collected
  // Implemented as: if all channels collected have full set of nibbles collected, then full statistics collected

  bool allChannelsFull = true;
  for(InDataCache::iterator histchannelit=m_bcm1fhistcache.begin(); histchannelit!=m_bcm1fhistcache.end(); ++histchannelit)
    {
      if(histchannelit->second->nbcount < nnbs) 
      {
        allChannelsFull = false;
      }
    }
  return allChannelsFull;
}

bool bril::bcm1fprocessor::Application::publishing(toolbox::task::WorkLoop* wl){
  QueueStoreIt it;
  for(it=m_topicoutqueues.begin();it!=m_topicoutqueues.end();++it){
    if(it->second->empty()) continue;
    std::string topicname = it->first;
    toolbox::mem::Reference* data = it->second->pop();


    std::pair<TopicStoreIt,TopicStoreIt > ret = m_out_topicTobuses.equal_range(topicname);
    for(TopicStoreIt topicit = ret.first; topicit!=ret.second;++topicit){
      if(data){ 
      std::string payloaddict;
      if(topicname=="bcm1fbkg"){
        payloaddict=interface::bril::bcm1fbkgT::payloaddict();
      }else if(topicname=="bcm1flumi"){
        payloaddict=interface::bril::bcm1flumiT::payloaddict();
      }else if(topicname=="bcm1fscvdlumi"){
        payloaddict=interface::bril::bcm1fscvdlumiT::payloaddict();
      }else if(topicname=="bcm1fpcvdlumi"){
        payloaddict=interface::bril::bcm1fpcvdlumiT::payloaddict();
      }else if(topicname=="bcm1fsilumi"){
        payloaddict=interface::bril::bcm1fsilumiT::payloaddict();
      }else if(topicname=="bxconfig"){
        payloaddict=interface::bril::bxconfigT::payloaddict();
      }else if(topicname=="bcm1fagghist"){
        payloaddict=interface::bril::bcm1fagghistT::payloaddict();
      }
      else{
        LOG4CPLUS_ERROR(getApplicationLogger(),"Wrong topic name to publish!");
      }
      do_publish(topicit->second,topicname,payloaddict,data->duplicate());
      }
    }
    if(data) data->release();
  }
  usleep(1000); // Cool down the workloop
  return true;
}

void bril::bcm1fprocessor::Application::do_publish(const std::string& busname,const std::string& topicname,const std::string& pdict,toolbox::mem::Reference* bufRef){
  std::stringstream msg;
  try{
    xdata::Properties plist;
    plist.setProperty("DATA_VERSION",interface::bril::DATA_VERSION);
    plist.setProperty("PAYLOAD_DICT",pdict);
    if ( m_beammode == "SETUP" || m_beammode == "ABORT" || m_beammode == "BEAM DUMP" || m_beammode == "RAMP DOWN" || m_beammode == "CYCLING" || m_beammode == "RECOVERY" || m_beammode == "NO BEAM" ){
      plist.setProperty("NOSTORE","1"); // COMMENT FOR DEVELOPMENT
    }
    msg << "publish to "<<busname<<" , "<<topicname<< ", channelId " << m_lastheader.channelid << ", run " << m_lastheader.runnum<<" ls "<< m_lastheader.lsnum <<" nb "<<m_lastheader.nbnum; 
    LOG4CPLUS_DEBUG(getApplicationLogger(), msg.str());
    //if (m_verbose) { LOG4CPLUS_INFO(getApplicationLogger(), msg.str()); }
    this->getEventingBus(busname).publish(topicname,bufRef,plist);
  }catch(xcept::Exception& e){
    msg<<"Failed to publish "<<topicname<<" to "<<busname;    
    LOG4CPLUS_ERROR(getApplicationLogger(),stdformat_exception_history(e));
    if(bufRef){
      bufRef->release();
      bufRef = 0;
    }
    XCEPT_DECLARE_NESTED(bril::bcm1fprocessor::exception::Exception,myerrorobj,msg.str(),e);
    this->notifyQualified("fatal",myerrorobj);
  }  

}

// When nibble-histogram receiving times out, process the received data 
// and/or clear the stale data
void bril::bcm1fprocessor::Application::timeExpired(toolbox::task::TimerEvent& e){
  if(m_lastheader.runnum==0) return;//cache is clean, do nothing
  toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
  unsigned int nowsec = t.sec();
  if( (nowsec-m_lastreceivedsec)>5 ){
    std::stringstream ss;
    ss<<"Found stale cache. run "<<m_lastheader.runnum<<" ls "<<m_lastheader.lsnum;
    if( check_channelstatistics(m_collectchannel) && check_nbstatistics(m_collectnb) ){
      LOG4CPLUS_DEBUG(getApplicationLogger(), "Full statistics, process the cache ");
      do_process(m_lastheader);
      clear_cache();
    }else{
      LOG4CPLUS_INFO(getApplicationLogger(), ss);
      LOG4CPLUS_INFO(getApplicationLogger(), "Insufficient statistics collected, discard the cache ");
      m_lastheader.runnum = 0;
      m_lastheader.lsnum = 0;
      //m_lastheader.cmslsnum = 0;
      m_lastheader.nbnum = 0;
      clear_cache();
    }
  }
}

void bril::bcm1fprocessor::Application::setupMonitoring()
{
  LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
  std::string monurn = createQualifiedInfoSpace( "bcm1fprocessorMon" ).toString();
  m_mon_infospace = xdata::getInfoSpaceFactory()->get( monurn );
  const int nvars = 12;
  const char* names[nvars] = { "beammode", "fill", "run", "ls", "nb", "timestamp", "background1", "background2", "luminosityscvd","luminositypcvd","luminositysi", "channelrates" };
  xdata::Serializable* vars[nvars] = { &m_beammode, &m_mon_fill, &m_mon_run, &m_mon_ls, &m_mon_nb, &m_mon_timestamp, &m_BG1_plus, &m_BG2_minus, &m_lumi_avg[0], &m_lumi_avg[1], &m_lumi_avg[2], &m_totalChannelRate };
  for ( int i = 0; i != nvars; ++i )
    {
      m_mon_varlist.push_back( names[i] );
      m_mon_vars.push_back( vars[i] );
      m_mon_infospace->fireItemAvailable( names[i], vars[i] );
    }
}

